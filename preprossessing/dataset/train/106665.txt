<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>Maple Valley High School Alumni - Maple Valley, Washington</title>
<meta content="Maple Valley High School Alumni in Maple Valley, Washington. Reunite with old classmates, learn about class reunions and take a look back at your yearbook photos!" name="description"/>
<meta content="Maple Valley High School Alumni, Maple Valley Washington, WA, Alumni, Classmates, Class Reunion, Alumni Finder, Alumni Directory" name="keywords"/>
<meta content="AlumniClass.com - 2021 - all rights reserved" name="author"/>
<meta content="https://cdn.alumniclass.com/v3/_images/misc/class-reunion.jpg" property="og:image"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://cdn.alumniclass.com/v3/_images/logos/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://cdn.alumniclass.com/v3/_images/logos/favicon.ico" rel="icon"/>
<link href="https://www.alumniclass.com/maple-valley-high-school-wa/" rel="canonical"/>
<style media="all">
@import url(/v3/_css/packed-school.min_2019-01-24.css);
body { background-color:#A9A9A9; }
.dropbtn, #mobile-menu, .button-prim a { background-color:#4682B4; }
.main { border:8px solid #4682B4; }
.school-name a, .menu a, .h1-home a, .color1, .color1 a, #page-title { color:#4682B4; }
.underline1, #mobileNav { border-bottom: 1px solid #4682B4; }
.feeBox { border: 4px solid #4682B4; }
</style>
<script src="https://cdn.alumniclass.com/v3/_javascript/packed-school.min_2020-03-05.js" type="text/javascript"></script>
<script src="https://cdn.alumniclass.com/v3/_javascript/remote/sdk.js" type="text/javascript"></script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"WebPage",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/#webpage",
				"inLanguage":"en-US",
				"isPartOf":{
					"@type":"WebSite",
					"@id":"https://www.alumniclass.com/#website",
					"url":"https://www.alumniclass.com/",
					"name":"Alumni Class",
					"inLanguage":"en-US",
					"potentialAction":{
						"@type":"SearchAction",
						"target":"https://www.alumniclass.com/search?q={search_term_string}",
						"query-input":"required name=search_term_string"
					}
				},
				"lastReviewed":"2021-01-14",
				"name":"Maple Valley High School Alumni - Maple Valley, Washington",
				"description":"Maple Valley High School Alumni in Maple Valley, Washington. Reunite with old classmates, learn about class reunions and take a look back at your yearbook photos!",
				"reviewedBy":{
					"@type":"Organization",
					"name":"Alumniclass"
				},
				"url":"https://www.alumniclass.com/maple-valley-high-school-wa"
			}
			</script>
<script type="application/ld+json">
			{
				"@context": "http://schema.org/",
				"@type": "HighSchool",
				"address": {
				  "@type": "PostalAddress",
				  "addressLocality": "Maple Valley",
				  "addressRegion": "Washington",
				  
				  "addressCountry": "USA"
				},
				"areaServed": "Maple Valley",
				"name": "Maple Valley High School",
				
				"url": "http://www.tahoma.wednet.edu/SubSite/MVHS/"
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/eric-smith/7702990/#person",
				"name":"Eric Smith",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/john-oday/9073057/#person",
				"name":"John Oday",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/karen-schwab/7114698/#person",
				"name":"Karen Schwab",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/lynne-vaselaar/6666360/#person",
				"name":"Lynne Vaselaar",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/mark-pluff/7740905/#person",
				"name":"Mark Pluff",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/maryellen-powers/5598209/#person",
				"name":"Maryellen Powers",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/robert-shaw/9091604/#person",
				"name":"Robert Shaw",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<script type="application/ld+json">
			{
				"@context":"https://schema.org",
				"@type":"Person",
				"@id":"https://www.alumniclass.com/maple-valley-high-school-wa/profile/shannon-mcaskill/7516399/#person",
				"name":"Shannon Mcaskill",
				"alumniOf":{
					"@type":"HighSchool",
					"name":"Maple Valley High School",
					"address":{
						"addressLocality":"Maple Valley",
						"addressRegion":"Washington",
						"addressCountry":{
							"@type":"Country",
							"name":"US"
						}
					}
				}
			}
			</script>
<!-- Facebook Pixel Code -->
<script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '533478423752886');
      fbq('track', 'PageView');
    </script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=533478423752886&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<style>
    #myModal {
		cursor:auto;
	}
    </style>
<div id="fb-root"></div>
<div id="modalBox" onclick="location.href = '/maple-valley-high-school-wa/alumni-join';"></div>
<script type="text/javascript">setTimeout("showModalReg('maple-valley-high-school-wa', 'Maple Valley High School Alumni', '#4682B4', '0');", 1000);</script>
<script type="text/javascript">
				FB.init({appId: "291485707632121", version: 'v4.0', status: true, cookie: true});
			
				function postToFeed() {				
					// calling the API ...
					var obj = {
					method: 'feed',
					redirect_uri: 'https://www.alumniclass.com/maple-valley-high-school-wa',
					link: 'https://www.alumniclass.com/maple-valley-high-school-wa/',
		picture: 'https://www.alumniclass.com/v3/_images/photos/share_reunion2.jpg',
					name: 'Maple Valley High School',
					description: 'Join our new  Alumni site! Post class reunions, class photos, alumni profiles, school apparel and more!'
				};
				
				function callback(response) {
					if (response['post_id']) {
		
					}
				}
				
				FB.ui(obj, callback);
			}
			</script>
<div class="container main">
<div class="main-inner">
<header>
<div class="row">
<div class="col-md-9">
<h1 class="school-name"><a href="/maple-valley-high-school-wa/" target="_self" title="Maple Valley High School Alumni">Maple Valley High School Alumni</a></h1>
<h3 class="school-address">Maple Valley, Washington (WA)</h3>
</div>
<div class="col-md-3">
<a href="https://www.alumniclass.com/" id="powered-by" target="_self" title="High School Alumni Reunions by AlumniClass"></a>
<div class="clearfix"></div>
<div class="hdr-button" id="nav-btn-1"><a class="blue" href="/maple-valley-high-school-wa/contact/" target="_self" title="Help &amp; Support">Help</a></div><div class="hdr-button" id="nav-btn-2"><a class="green" href="/maple-valley-high-school-wa/member-login/" target="_self" title="Login to Maple Valley High School Alumni Site">Login</a></div>
</div>
</div>
<div class="menu" id="menu">
<a href="/maple-valley-high-school-wa/" title="Maple Valley High School Alumni Home">Home</a>
<a href="/maple-valley-high-school-wa/find-people/" title="Find Maple Valley High School Alumni">Find Alumni</a>
<a href="/maple-valley-high-school-wa/photos/" title="Maple Valley High School Photos &amp; Yearbooks">Photos &amp; Yearbooks</a>
<a href="/maple-valley-high-school-wa/reunion-search/" title="Find Maple Valley High School Reunions">Find Reunion</a>
<a href="/maple-valley-high-school-wa/reunions/" title="Plan Your Maple Valley High School Reunion">Plan Reunion</a>
<a href="https://apparelnow.com/maple-valley-high-schoolg530-apparel/" title="Maple Valley High School  Apparel Store">School Apparel</a>
</div>
<div class="clearfix"></div>
</header>
<div id="mobile-menu">
<div class="hdr-button"><a class="blue" href="/maple-valley-high-school-wa/contact/" target="_self" title="Help &amp; Support">Help</a></div><div class="hdr-button"><a class="green" href="/maple-valley-high-school-wa/member-login/" target="_self" title="Login to Maple Valley High School Alumni Site">Login</a></div>
<div class="dropdown">
<button aria-expanded="false" aria-haspopup="true" class="dropbtn" data-toggle="dropdown" id="dropdownMenuButton" type="button"><img src="https://cdn.alumniclass.com/v3/_images/misc/menu-btn.png" width="26"/> Menu</button>
<div aria-labelledby="dropdownMenuButton" class="dropdown-menu" id="school-menu">
<a class="dropdown-item" href="/maple-valley-high-school-wa/" title="Maple Valley High School Alumni Home">Home</a>
<a class="dropdown-item" href="/maple-valley-high-school-wa/find-people/" title="Find Maple Valley High School Alumni">Find Alumni</a>
<a class="dropdown-item" href="/maple-valley-high-school-wa/photos/" title="Maple Valley High School Photos &amp; Yearbooks">Photos &amp; Yearbooks</a>
<a class="dropdown-item" href="/maple-valley-high-school-wa/reunion-search/" title="Find Maple Valley High School Reunions">Find Reunion</a>
<a class="dropdown-item" href="/maple-valley-high-school-wa/reunions/" title="Plan Your Maple Valley High School Reunion">Plan Reunion</a>
<a class="dropdown-item" href="https://apparelnow.com/maple-valley-high-schoolg530-apparel/" title="Maple Valley High School  Apparel Store">School Apparel</a>
</div>
</div>
</div>
<div class="container content">
<h1 class="h1-home">
<a href="/maple-valley-high-school-wa/alumni-join/" title="Welcome Alumni!">Welcome Alumni!</a>
</h1>
<p class="txt-lg">
			
						Welcome!  <a href="/maple-valley-high-school-wa/alumni-join/" title="Maple Valley High School Alumni Register Here">Register</a> as an alumni from Maple Valley High School and reunite with old friends and classmates. Share your memories by posting photos or stories, or find out about your next class reunion!
				
					</p>
<div class="home-reg-button">
<div class="button-prim">
<a href="/maple-valley-high-school-wa/alumni-join/" target="_self" title="Register as an Alumni of Maple Valley High School">ALUMNI Registration</a>
</div>
</div>
<div class="home-intro">
						It's a reunion year for Class of '71, Class of '81, Class of '91, Class of '01, and Class of '11. <a href="/maple-valley-high-school-wa/alumni-join/" style="font-weight:bold;" title="Register here for your Maple Valley High School Class Reunion">Register here to join your class.</a>
</div>
<div class="clearfix"></div>
<div class="row three-col">
<div class="col-md-3" id="left-column">
<div id="photoFeature" style="margin-bottom:18px;"><img src="/images/profile-group.jpg" style="width:100%;"/></div>
<div class="module" id="membersBox">
<div class="container">
<h2 class="color1">Recent Members</h2>
<div class="dashed-line"></div>
<table border="0" cellpadding="0" cellspacing="0" class="interest-points" width="100%">
<tbody>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/eric-smith/7702990/" target="_self" title="View Eric Smith's Alumni Profile">Eric Smith</a></td>
<td align="right" class="year" valign="middle">1985</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/john-oday/9073057/" target="_self" title="View John Oday's Alumni Profile">John Oday</a></td>
<td align="right" class="year" valign="middle">2004</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/karen-schwab/7114698/" target="_self" title="View Karen Schwab's Alumni Profile">Karen Schwab</a></td>
<td align="right" class="year" valign="middle">1973</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/lynne-vaselaar/6666360/" target="_self" title="View Lynne Vaselaar's Alumni Profile">Lynne Vaselaar</a></td>
<td align="right" class="year" valign="middle">1983</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/mark-pluff/7740905/" target="_self" title="View Mark Pluff's Alumni Profile">Mark Pluff</a></td>
<td align="right" class="year" valign="middle">1974</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/maryellen-powers/5598209/" target="_self" title="View Maryellen Powers's Alumni Profile">Maryellen Powers</a></td>
<td align="right" class="year" valign="middle">1983</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/robert-shaw/9091604/" target="_self" title="View Robert Shaw's Alumni Profile">Robert Shaw</a></td>
<td align="right" class="year" valign="middle">1969</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/maple-valley-high-school-wa/profile/shannon-mcaskill/7516399/" target="_self" title="View Shannon Mcaskill's Alumni Profile">Shannon Mcaskill</a></td>
<td align="right" class="year" valign="middle">1992</td>
</tr>
</tbody>
</table>
</div>
<div class="clearfix"></div>
</div>
<div class="module" id="militaryBox">
<div class="container">
<h2 class="color1"><a href="/maple-valley-high-school-wa/military-alumni-view/" target="_self" title="View a list classmates in the armed forces">Military Alumni</a></h2>
<div style="text-align:center; margin-top:10px;">
<a href="/maple-valley-high-school-wa/military-alumni-view/" target="_self" title="Military Alumni Profiles"><img alt="Military High School Alumni" class="img-thumbnail" src="https://cdn.alumniclass.com/v3/_images/photos/military-high-school-alumni-1.jpg"/></a>
</div>
<h2 class="color1" style="padding:10px 0 0 0; margin:0;">Honoring Our Heroes
					
					</h2><p style="font-size:12px; padding:5px 0 0 0; margin:0; text-align:justify;">This area is dedicated to our alumni that have served or are serving in our armed forces!</p>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/military-alumni-view/" target="_self" title="View your Maple Valley High School military Alumni photos">View Military Alumni</a>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="module" id="apparelBox">
<div class="container">
<h2 class="color1" style="margin-bottom:10px; color:#4682B4;"><a href="https://apparelnow.com/maple-valley-high-schoolg530-apparel/" style="font-size:18px;" title="Maple Valley High School  Apparel">Maple Valley High School  Apparel!</a></h2>
<div style="text-align:center;">
<a href="https://apparelnow.com/maple-valley-high-schoolg530-apparel/" title="Maple Valley High School  Merchandise"><img border="0" class="img-thumbnail" src="/maple-valley-high-school-wa/merchandise/render?product_id=1&amp;resize_width=172&amp;design=" style="border:none; background:none;"/></a>
</div>
<div class="button-prim" style="margin-top:10px;">
<a href="https://apparelnow.com/maple-valley-high-schoolg530-apparel/" target="_self" title="Visit apparel store">Visit Apparel Store</a>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="module" id="guessBox">
<div class="container">
<h2 class="color1" style="margin-bottom:10px;"><a href="/maple-valley-high-school-wa/photos/?t=profile" target="_self" title="Guess Who!">Guess Who?</a></h2>
<div style="text-align:center;">
<a href="/maple-valley-high-school-wa/profile/rosemary-jarrard/3655435/" target="_self" title="View profile"><img alt="Rosemary Jarrard, class of 2001" class="img-thumbnail" src="https://cdn.alumniclass.com/school_images/8800/profiles/thb_3655435_1261595297.jpg"/></a>
</div>
<div class="button-prim" style="margin-top:15px;">
<a href="/maple-valley-high-school-wa/profile/rosemary-jarrard/3655435/" target="_self" title="View this Maple Valley High School Alumni Profile">View Profile</a>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="module" id="newsletterBox">
<div class="container">
<h2 class="color1">Alumni Newsletters</h2>
<div class="dashed-line"></div>
<div style="height:100px; overflow:auto; padding-right:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="interest-points" width="100%">
<tbody>
<tr>
<td align="left" class="name" valign="middle"><a href="/reunion-newsletter-maplevalleyhighschool-maplevalley-wa-october/?issue=43&amp;id=548391&amp;s=8800" target="_self" title="Maple Valley High School Alumni Newsletter October 2015">October 2015</a></td>
<td align="right" class="year" valign="middle">Vol 4</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/reunion-newsletter-maplevalleyhighschool-maplevalley-wa-august/?issue=42&amp;id=533262&amp;s=8800" target="_self" title="Maple Valley High School Alumni Newsletter August 2015">August 2015</a></td>
<td align="right" class="year" valign="middle">Vol 3</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/reunion-newsletter-maplevalleyhighschool-maplevalley-wa-july/?issue=41&amp;id=518284&amp;s=8800" target="_self" title="Maple Valley High School Alumni Newsletter July 2015">July 2015</a></td>
<td align="right" class="year" valign="middle">Vol 2</td>
</tr>
<tr>
<td align="left" class="name" valign="middle"><a href="/reunion-newsletter-maplevalleyhighschool-maplevalley-wa-june/?issue=40&amp;id=503459&amp;s=8800" target="_self" title="Maple Valley High School Alumni Newsletter June 2015">June 2015</a></td>
<td align="right" class="year" valign="middle">Vol 1</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="col-md-6 three-col-main">
<a name="wall"></a>
<div class="white-box" id="wall-container">
<h3 class="color1">Maple Valley High School Wall</h3>
<div class="clearfix"></div>
<p id="feed-description">View, post and comment on photo's and events with your classmates!</p>
<div id="feed">
<div class="row wall-post" id="f4315254">
<div class="col-md-2 col-sm-2 col-xs-2" id="post-thumb" style="padding:0;">
<div class="thumbnail">
<a href="/maple-valley-high-school-wa/profile/rosemary-jarrard/3655435/" target="_self" title="Rosemary Jarrard '01">
<img alt="Rosemary Jarrard '01" id="img4315254" src="https://cdn.alumniclass.com/school_images/8800/profiles/thb_3655435_1261595297.jpg"/>
</a>
</div>
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="post-wrapper">
<div class="byline"><a href="/maple-valley-high-school-wa/profile/rosemary-jarrard/3655435/" target="_self" title="Rosemary Jarrard '01">Rosemary Jarrard '01</a> uploaded <a href="/maple-valley-high-school-wa/photo-album/?a=551229&amp;t=profile">profile photos</a>:</div>
<div class="attachment photo"><a href="/maple-valley-high-school-wa/photo-album/?a=551229&amp;t=profile"><img alt="Maple Valley High School Profile Photos" src="https://cdn.alumniclass.com/school_images/8800/profiles/thb_3655435_1261595297.jpg"/></a></div>
<div class="post-date">
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/member-login?login-first" id="commentLink4315254" target="_self" title="Comment on this post">Comment</a>
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/report-abuse/?My00MzE1MjU0" target="_self" title="Report Abuse">Report a Problem</a>
<div class="fb-like" data-action="like" data-colorscheme="light" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200" href="https://www.alumniclass.com/maple-valley-high-school-wa/post?p=4315254" style="margin-left:10px;"></div>
</div>
<div class="comments" id="commentBox4315254" style="display:none;">
<div class="pointer"><img border="0" src="https://cdn.alumniclass.com/v3/_images/pointers/comments_gray_up.png"/></div>
<div id="comments4315254">
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="row wall-post" id="f10329424">
<div class="col-md-2 col-sm-2 col-xs-2" id="post-thumb" style="padding:0;">
<div class="thumbnail">
<a href="/maple-valley-high-school-wa/profile/robert-shaw/9091604/" target="_self" title="Robert Shaw '69">
<img alt="Robert Shaw '69" id="img10329424" src="https://cdn.alumniclass.com/v3/_images/misc/no_pic_undisclosed_smlf.gif"/>
</a>
</div>
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="post-wrapper">
<div class="byline"><a href="/maple-valley-high-school-wa/profile/robert-shaw/9091604/" target="_self" title="Robert Shaw '69">Robert Shaw '69</a> joined their classmates and friends on our site!</div>
<div class="post-date">
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/member-login?login-first" id="commentLink10329424" target="_self" title="Comment on this post">Comment</a>
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/report-abuse/?My0xMDMyOTQyNA%3D%3D" target="_self" title="Report Abuse">Report a Problem</a>
<div class="fb-like" data-action="like" data-colorscheme="light" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200" href="https://www.alumniclass.com/maple-valley-high-school-wa/post?p=10329424" style="margin-left:10px;"></div>
</div>
<div class="comments" id="commentBox10329424" style="display:none;">
<div class="pointer"><img border="0" src="https://cdn.alumniclass.com/v3/_images/pointers/comments_gray_up.png"/></div>
<div id="comments10329424">
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="row wall-post" id="f10302767">
<div class="col-md-2 col-sm-2 col-xs-2" id="post-thumb" style="padding:0;">
<div class="thumbnail">
<a href="/maple-valley-high-school-wa/profile/john-oday/9073057/" target="_self" title="John Oday '04">
<img alt="John Oday '04" id="img10302767" src="https://cdn.alumniclass.com/v3/_images/misc/no_pic_undisclosed_smlf.gif"/>
</a>
</div>
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="post-wrapper">
<div class="byline"><a href="/maple-valley-high-school-wa/profile/john-oday/9073057/" target="_self" title="John Oday '04">John Oday '04</a> joined their classmates and friends on our site!</div>
<div class="post-date">
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/member-login?login-first" id="commentLink10302767" target="_self" title="Comment on this post">Comment</a>
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/report-abuse/?My0xMDMwMjc2Nw%3D%3D" target="_self" title="Report Abuse">Report a Problem</a>
<div class="fb-like" data-action="like" data-colorscheme="light" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200" href="https://www.alumniclass.com/maple-valley-high-school-wa/post?p=10302767" style="margin-left:10px;"></div>
</div>
<div class="comments" id="commentBox10302767" style="display:none;">
<div class="pointer"><img border="0" src="https://cdn.alumniclass.com/v3/_images/pointers/comments_gray_up.png"/></div>
<div id="comments10302767">
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="row wall-post" id="f8620963">
<div class="col-md-2 col-sm-2 col-xs-2" id="post-thumb" style="padding:0;">
<div class="thumbnail">
<a href="/maple-valley-high-school-wa/profile/mark-pluff/7740905/" target="_self" title="Mark Pluff '74">
<img alt="Mark Pluff '74" id="img8620963" src="https://cdn.alumniclass.com/school_images/8800/profiles/thb_645642959.jpg"/>
</a>
</div>
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="post-wrapper">
<div class="byline"><a href="/maple-valley-high-school-wa/profile/mark-pluff/7740905/" target="_self" title="Mark Pluff '74">Mark Pluff '74</a> joined their classmates and friends on our site!</div>
<div class="post-date">
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/member-login?login-first" id="commentLink8620963" target="_self" title="Comment on this post">Comment</a>
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/report-abuse/?My04NjIwOTYz" target="_self" title="Report Abuse">Report a Problem</a>
<div class="fb-like" data-action="like" data-colorscheme="light" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200" href="https://www.alumniclass.com/maple-valley-high-school-wa/post?p=8620963" style="margin-left:10px;"></div>
</div>
<div class="comments" id="commentBox8620963" style="display:none;">
<div class="pointer"><img border="0" src="https://cdn.alumniclass.com/v3/_images/pointers/comments_gray_up.png"/></div>
<div id="comments8620963">
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="row wall-post" id="f8575962">
<div class="col-md-2 col-sm-2 col-xs-2" id="post-thumb" style="padding:0;">
<div class="thumbnail">
<a href="/maple-valley-high-school-wa/profile/eric-smith/7702990/" target="_self" title="Eric Smith '85">
<img alt="Eric Smith '85" id="img8575962" src="https://cdn.alumniclass.com/v3/_images/misc/no_pic_male_smlf.gif"/>
</a>
</div>
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
<div class="post-wrapper">
<div class="byline"><a href="/maple-valley-high-school-wa/profile/eric-smith/7702990/" target="_self" title="Eric Smith '85">Eric Smith '85</a> joined their classmates and friends on our site!</div>
<div class="post-date">
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/member-login?login-first" id="commentLink8575962" target="_self" title="Comment on this post">Comment</a>
<span style="font-size:10px;">•</span> <a href="/maple-valley-high-school-wa/report-abuse/?My04NTc1OTYy" target="_self" title="Report Abuse">Report a Problem</a>
<div class="fb-like" data-action="like" data-colorscheme="light" data-layout="button_count" data-send="false" data-show-faces="false" data-width="200" href="https://www.alumniclass.com/maple-valley-high-school-wa/post?p=8575962" style="margin-left:10px;"></div>
</div>
<div class="comments" id="commentBox8575962" style="display:none;">
<div class="pointer"><img border="0" src="https://cdn.alumniclass.com/v3/_images/pointers/comments_gray_up.png"/></div>
<div id="comments8575962">
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div id="feedBottom">
<div class="button-prim"><a href="javascript:;" onclick="location.href='/maple-valley-high-school-wa/alumni-join';">Show More Posts</a></div>
</div>
</div>
</div>
<div class="col-md-3" id="right-column">
<div class="module">
<div class="container">
<h2 class="color1">Classmates Spotlight</h2>
<div style="text-align:center; margin-top:10px; margin-bottom:10px;">
<img alt="Maple Valley High School Classmates" class="img-thumbnail" src="https://cdn.alumniclass.com/school_images/8800/profiles/3655435_1261595297.jpg"/>
</div>
<p>
<a href="/maple-valley-high-school-wa/profile/rosemary-jarrard/3655435/" style="font-size:16px;" title="Rosemary Jarrard">Rosemary Jarrard</a>
<br/>Class of '01
				</p>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/alumni-join/" target="_self" title="See more Maple Valley High School classmates">See More Classmates</a>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="module">
<div class="container">
<h2 class="color1"><a href="/maple-valley-high-school-wa/reunions/" title="Maple Valley High School Class Reunions">Maple Valley High School Class Reunions</a></h2>
<p class="img-thumbnail" style="margin-top:10px; text-align:center;"><strong>FREE CLASS REUNION WEBSITES!</strong></p>
<p style="font-size:12px; margin-top:4px;">Post your Maple Valley High School class reunion information here and receive a free reunion planning website with the reunion tools you need for the perfect class reunion.</p>
<div style="text-align:center;">
<a href="/maple-valley-high-school-wa/reunions/" target="_self" title="Plan Your Maple Valley High School Reunion">
<img alt="Maple Valley High School Reunion" class="img-thumbnail" src="https://cdn.alumniclass.com/v3/_images/misc/alumni-reunion-event.jpg" style="margin-right:4px;" width="100"/>
<img alt="Maple Valley High School Alumni Event" class="img-thumbnail" src="https://cdn.alumniclass.com/v3/_images/misc/high-school-reunion.jpg" width="100"/>
</a>
</div>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/reunions/" target="_self" title="Plan Your Maple Valley High School Reunion">Plan Your Reunion</a>
</div>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/reunion-search/" target="_self" title="Find Your Maple Valley High School Reunion">Find Your Reunion</a>
</div>
</div>
</div>
<div class="module">
<div class="container">
<h2 class="color1" style="margin-bottom:10px;">Alumni Stories</h2>
<img alt="High School Alumni Stores" src="https://cdn.alumniclass.com/v3/_images/misc/high-school-alumni-stores.jpg" style="float:right; border:4px solid #fff; margin:0 10px 0 0;" width="70"/>
<p>
					Read and submit stories about our classmates from Maple Valley High School, post achievements and news about our alumni, and post photos of our classmates.
				</p>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/distinguished-alumni-view/" target="_self" title="Maple Valley High School Alumni Stories">Alumni Stories »</a>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="module" id="newsBox">
<div class="container">
<h2 class="color1" style="margin-bottom:10px;">School News</h2>
<p style="font-size:12px;">
			Do you have any news to share? Keep our classmates informed on news and current events!
	
			<em>Please post all reunion information in the <a href="/maple-valley-high-school-wa/reunions/">reunion area.</a></em>
</p>
<div class="button-prim">
<a href="/maple-valley-high-school-wa/news/?add" target="_self" title="Maple Valley High School News">Post Your News</a>
</div>
<div class="button-prim" style="margin-top:10px;">
<a href="/maple-valley-high-school-wa/in-memory/" target="_self" title="In Memory Of">In Memory Of</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<footer>
<div class="footer-links">
<a href="/maple-valley-high-school-wa/about/" target="_self" title="About our Maple Valley High School Alumni Site">About</a>
<span class="dot">•</span>
<a href="/maple-valley-high-school-wa/terms/" target="_self" title="Terms of Use">Terms of Use</a>
<span class="dot">•</span>
<a href="/maple-valley-high-school-wa/privacy/" target="_self" title="Privacy Policy">Privacy Policy</a>
<span class="dot">•</span>
<a href="/maple-valley-high-school-wa/contact/" target="_self" title="Contact us">Contact</a>
<div style="margin-top:10px; text-align:right;">
				Connect with us:
				<a class="bg-sml_pinterest" href="http://www.pinterest.com/alumniclass/" target="_blank" title="Connect with AlumniClass.com on Pinterest"></a>
<a class="bg-sml_google" href="https://plus.google.com/b/112791094073977611020/112791094073977611020" target="_blank" title="Connect with AlumniClass.com on Google+"></a>
<a class="bg-sml_twitter" href="https://twitter.com/Alumni_Class" target="_blank" title="Connect with AlumniClass.com on Twitter"></a>
<a class="bg-sml_facebook" href="https://www.facebook.com/pages/AlumniClass/162645670426324" target="_blank" title="Connect with AlumniClass.com on Facebook"></a>
</div>
</div>
<div id="copyright">
                    Copyright © 2021 <a href="http://www.alumniclass.com/" target="_blank" title="School Alumni Reunions">AlumniClass, Inc.</a> All rights reserved.
                    <br/>Maple Valley High School, Maple Valley, Washington (WA)<br/>AlumniClass.com (8800) - 10019 E Knox Ave, Spokane Valley WA, 99206.<div style="font-size:12px; margin-top:4px; color:#333;">AlumniClass.com is not affiliated with and is independent of any school, school district, alumni association or any other sites.</div>
</div>
<div class="clearfix"></div>
</footer>
</div>
</div>
<script type="text/javascript">
				var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
				document.write("<script src='https://cdn.alumniclass.com/v3/_javascript/remote/ga.js' type='text/javascript'>\<\/script>" );
				</script>
<script type="text/javascript">
				var pageTracker = _gat._getTracker("UA-3247839-1");
				pageTracker._initData();
				pageTracker._trackPageview();
				</script>
<script type="text/javascript">  window.smartlook||(function(d) {  var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];  var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';  c.charset='utf-8';c.src='https://cdn.alumniclass.com/v3/_javascript/remote/recorder.js';h.appendChild(c);  })(document);  smartlook('init', 'f6ad47a256be0cef262c43caefb4502c34b808ff'); </script>
</body>
</html>