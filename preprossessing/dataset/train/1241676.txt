<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Robert Kenner Films</title>
<link href="css/main_new.css" rel="stylesheet" type="text/css"/>
<link href="index.css" rel="stylesheet" type="text/css"/>
<script src="js/modernizr.custom.86080.js" type="text/javascript"></script>
<style>
		
	
		.cb-slideshow,
		.cb-slideshow:after {
			position: fixed;
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
			z-index: 0;
		}
		 
		.cb-slideshow:after {
			content: '';
			background: transparent url(images/pattern.png) repeat top left;
		}
		
		.cb-slideshow li span {
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0px;
			left: 0px;
			color: transparent;
			background-size: cover;
			background-position: 50% 50%;
			background-repeat: none;
			opacity: 0;
			z-index: 0;
			-webkit-backface-visibility: hidden;
			-webkit-animation: imageAnimation 56s linear infinite 0s;
			-moz-animation: imageAnimation 56s linear infinite 0s;
			-o-animation: imageAnimation 56s linear infinite 0s;
			-ms-animation: imageAnimation 56s linear infinite 0s;
			animation: imageAnimation 56s linear infinite 0s;
		}
		
		.cb-slideshow li div {
			-webkit-animation: titleAnimation 56s linear infinite 0s;
			-moz-animation: titleAnimation 56s linear infinite 0s;
			-o-animation: titleAnimation 56s linear infinite 0s;
			-ms-animation: titleAnimation 56s linear infinite 0s;
			animation: titleAnimation 56s linear infinite 0s;
		}
	 
		

        
            /*//////////////// SLIDE TIMING //////////////////////*/
        .cb-slideshow li:nth-child(1) span {
            opacity: 1;
            background-image: url(images/TheConfessionKiller_Poster.jpg);
            background-size: cover;
            background-position: center;
            filter: blur(20px);
        }
        .cb-slideshow li:nth-child(1) div { 
            opacity: 1;
            z-index: 1000;
            position: absolute;
            top: 0;
            margin-top: 1vw;
            width: 100%;
            text-align: center;
            color: #FFFFFF;
            height: 100vh;
        }
        .cb-slideshow li:nth-child(1) div img { 
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        } 
	
		.cb-slideshow li:nth-child(2) span {
			background-image: url(images/background/01.jpg);
			-webkit-animation-delay: 8s;
			-moz-animation-delay: 8s;
			-o-animation-delay: 8s;
			-ms-animation-delay: 8s;
			animation-delay: 8s;
		}
		.cb-slideshow li:nth-child(2) div {
			-webkit-animation-delay: 8s;
			-moz-animation-delay: 8s;
			-o-animation-delay: 8s;
			-ms-animation-delay: 8s;
			animation-delay: 8s;
		}
		.cb-slideshow li:nth-child(3) span {
			background-image: url(images/background/02.jpg);
			-webkit-animation-delay: 16s;
			-moz-animation-delay: 16s;
			-o-animation-delay: 16s;
			-ms-animation-delay: 16s;
			animation-delay: 16s;
		}
		.cb-slideshow li:nth-child(3) div {
			-webkit-animation-delay: 16s;
			-moz-animation-delay: 16s;
			-o-animation-delay: 16s;
			-ms-animation-delay: 16s;
			animation-delay: 16s;
		}
		.cb-slideshow li:nth-child(4) span {
			background-image: url(images/background/03.jpg);
			-webkit-animation-delay: 24s;
			-moz-animation-delay: 24s;
			-o-animation-delay: 24s;
			-ms-animation-delay: 24s; 
			animation-delay: 24s; 
		}
		.cb-slideshow li:nth-child(4) div {
			-webkit-animation-delay: 24s;
			-moz-animation-delay: 24s;
			-o-animation-delay: 24s;
			-ms-animation-delay: 24s;
			animation-delay: 24s;
		}
		.cb-slideshow li:nth-child(5) span {
			background-image: url(images/background/04.jpg);
			-webkit-animation-delay: 32s;
			-moz-animation-delay: 32s;
			-o-animation-delay: 32s;
			-ms-animation-delay: 32s;
			animation-delay: 32s;
		}
		.cb-slideshow li:nth-child(5) div {
			-webkit-animation-delay: 32s;
			-moz-animation-delay: 32s;
			-o-animation-delay: 32s;
			-ms-animation-delay: 32s;
			animation-delay: 32s;
		}
		.cb-slideshow li:nth-child(6) span {
			background-image: url(images/background/05.jpg);
			-webkit-animation-delay: 40s;
			-moz-animation-delay: 40s;
			-o-animation-delay: 40s;
			-ms-animation-delay: 40s;
			animation-delay: 40s;
		}
		.cb-slideshow li:nth-child(6) div {
			-webkit-animation-delay: 40s;
			-moz-animation-delay: 40s;
			-o-animation-delay: 40s;
			-ms-animation-delay: 40s;
			animation-delay: 40s;
		}
		.cb-slideshow li:nth-child(7) span {
			background-image: url(images/background/06.jpg);
			-webkit-animation-delay: 48s;
			-moz-animation-delay: 48s;
			-o-animation-delay: 48s;
			-ms-animation-delay: 48s;
			animation-delay: 48s;
		}
		.cb-slideshow li:nth-child(7) div {
			-webkit-animation-delay: 48s;
			-moz-animation-delay: 48s;
			-o-animation-delay: 48s;
			-ms-animation-delay: 48s;
			animation-delay: 48s;
		}
		
		/*/////////////////// SLIDE ANIMATION /////////////////*/
		@-webkit-keyframes imageAnimation {
			0% {
				opacity: 0;
				-webkit-animation-timing-function: ease-in;
			}
			8% {
				opacity: 1;
				-webkit-animation-timing-function: ease-out;
			}
			17% {
				opacity: 1
			}
			25% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-moz-keyframes imageAnimation {
			0% {
				opacity: 0;
				-moz-animation-timing-function: ease-in;
			}
			8% {
				opacity: 1;
				-moz-animation-timing-function: ease-out;
			}
			17% {
				opacity: 1
			}
			25% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-o-keyframes imageAnimation {
			0% {
				opacity: 0;
				-o-animation-timing-function: ease-in;
			}
			8% {
				opacity: 1;
				-o-animation-timing-function: ease-out;
			}
			17% {
				opacity: 1
			}
			25% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-ms-keyframes imageAnimation {
			0% {
				opacity: 0;
				-ms-animation-timing-function: ease-in;
			}
			8% {
				opacity: 1;
				-ms-animation-timing-function: ease-out;
			}
			17% {
				opacity: 1
			}
			25% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@keyframes imageAnimation {
			0% {
				opacity: 0;
				animation-timing-function: ease-in;
			}
			8% {
				opacity: 1;
				animation-timing-function: ease-out;
			}
			17% {
				opacity: 1
			}
			25% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		
		
		/*///////////////// TITLE ANIMATION /////////////////////*/
		@-webkit-keyframes titleAnimation {
			0% {
				opacity: 0
			}
			8% {
				opacity: 1
			}
			17% {
				opacity: 1
			}
			19% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-moz-keyframes titleAnimation {
			0% {
				opacity: 0
			}
			8% {
				opacity: 1
			}
			17% {
				opacity: 1
			}
			19% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-o-keyframes titleAnimation {
			0% {
				opacity: 0
			}
			8% {
				opacity: 1
			}
			17% {
				opacity: 1
			}
			19% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@-ms-keyframes titleAnimation {
			0% {
				opacity: 0
			}
			8% {
				opacity: 1
			}
			17% {
				opacity: 1
			}
			19% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		@keyframes titleAnimation {
			0% {
				opacity: 0
			}
			8% {
				opacity: 1
			}
			17% {
				opacity: 1
			}
			19% {
				opacity: 0
			}
			100% {
				opacity: 0
			}
		}
		
		/* Show at least something when animations not supported */
		.no-cssanimations .cb-slideshow li span {
			opacity: 1;
		}

	</style>
<!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.-->
<script>var __adobewebfontsappname__="dreamweaver"</script>
<script src="http://use.edgefonts.net/lato:n1,n4:default.js" type="text/javascript"></script>
</head>
<body id="page">
<!-- slides start -->
<ul class="cb-slideshow">
<!-- slide 1 content start -->
<li id="TheConfessionKiller">
<span class="slideText">
</span>
<div>
<a href="/confession_killer.php"><img alt="" src="/images/TheConfessionKiller_Poster.jpg" style="max-height: 65vh;"/></a>
<!--
					<p class="slideTitle">
						THE CONFESSION KILLER
					</p>
					<section class="awards">
						
					</section>
					<section class="four_laurels">
						<section class="four_laurels_left">
							<img class="laurel" src="images/Tribeca_laurel.png" alt=""/>
							<img class="laurel" src="images/Sheffield_laurel.png" alt=""/>
						</section>
						<section class="four_laurels_right">
							<img class="laurel" src="images/TraverseCity_laurel.png" alt=""/>
							<img class="laurel" src="images/AFIDocs_laurel.png" alt=""/>
						</section>
					</section>
					<section class="reviews">
						"Command and Control is frightening for a whole pants-shitting list of reasons." <br>
						<cite>- The Village Voice</cite><br><br>
						"Combines Cold War-era thriller with post-apocalyptic nightmare. This one will stay with you for days after viewing.<br>
						<cite>- Rolling Stone</cite>
					</section> 
					
				-->
</div>
</li>
<!-- slide 1 content end -->
<!-- slide 2 content start -->
<li id="CommandAndControl">
<span class="slideText">
<div>
<p class="slideTitle">
						COMMAND   AND   CONTROL
					</p>
<section class="awards">
<h1>Best Documentary Feature Shortlist Nominee</h1>
						Academy Awards 2017<br/><br/>
<h1>Best Documentary Screenplay</h1>
						Writers Guild of America Awards 2017
						
					</section>
<section class="four_laurels">
<section class="four_laurels_left">
<img alt="" class="laurel" src="images/Tribeca_laurel.png"/>
<img alt="" class="laurel" src="images/Sheffield_laurel.png"/>
</section>
<section class="four_laurels_right">
<img alt="" class="laurel" src="images/TraverseCity_laurel.png"/>
<img alt="" class="laurel" src="images/AFIDocs_laurel.png"/>
</section>
</section>
<section class="reviews">
						"Command and Control is frightening for a whole pants-shitting list of reasons." <br/>
<cite>- The Village Voice</cite><br/><br/>
						"Combines Cold War-era thriller with post-apocalyptic nightmare. This one will stay with you for days after viewing.<br/>
<cite>- Rolling Stone</cite>
</section>
</div>
</span>
</li>
<!-- slide 2 content end -->
<!-- slide 3 content start -->
<li id="MerchantsOfDoubt">
<span class="slideText">
<div>
<p align="center" class="slideTitle">
						MERCHANTS  OF  DOUBT
					</p>
<section class="awards">
</section>
<section id="MOD_laurels">
<img alt="" class="laurel" src="images/Telluride_laurel.png"/>
<img alt="" class="laurel" src="images/TIFF_Laurel.png"/>
<img alt="" class="laurel" src="images/NYFF_laurel.png"/>
</section>
<section class="reviews">
						"Surprisingly rollicking... a worthy follow-up to his 2008 Oscar-nominated Food Inc."<br/>
<cite>- Washington Post</cite><br/>
<br/>
<br/>
						"This enthralling film is as fascinating as it is horrifying."<br/>
<cite>- New York Times</cite>
</section>
</div>
</span>
</li>
<!-- slide 3 content end -->
<!-- slide 4 content start -->
<li id="FoodInc">
<span class="slideText">
<div>
<p class="slideTitle">
						FOOD,  INC.
					</p>
<section class="awards">
<h1>WINNER OF 2 EMMY AWARDS</h1>
						2010 Best Documentary<br/>
						Outstanding Informational Programming - Long Form
							<br/>
<br/>
<h1>ACADEMY AWARD NOMINEE</h1>
						Best Feature Documentary
					</section>
<section class="four_laurels">
<section class="four_laurels_left">
<img alt="" class="laurel" src="images/TIFF_Laurel.png"/>
<img alt="" class="laurel" src="images/berlinale_laurel.png"/>
</section>
<section class="four_laurels_right">
<img alt="" class="laurel" src="images/TF_laurel.png"/>
<img alt="" class="laurel" src="images/FullFrame_laurel.png"/>
</section>
</section>
<section class="reviews">
						"Food, Inc. does for the supermarket what 'Jaws' did for the beach."<br/>
<cite>- Variety</cite><br/>
<br/>
						"Food, Inc. is more than a terrific movie--it's an important movie."<br/>
<cite>- Entertainment Weekly</cite>
</section>
</div>
</span>
</li>
<!-- slide 4 content end -->
<!-- slide 5 content start -->
<li id="WhenStrangersClick">
<span class="slideText">
<div>
<p class="slideTitle">
						WHEN<br/>STRANGERS<br/>CLICK
					</p>
<section class="awards">
<h1>2011 Emmy Nominee</h1>
</section>
<section class="reviews">
						"A nuanced and enlightened view of romantic coupling...The film beautifully explores the poignant nature of [one couple's] ambivalence toward solitude."<br/>
<cite>- New York Times</cite><br/>
<br/>
						"These are love stories for our time."
						<cite>- New York Daily News</cite><br/>
</section>
</div>
</span>
</li>
<!-- slide 5 content end -->
<!-- slide 6 content start -->
<li id="TheRoadToMemphis">
<span class="slideText">
<div>
<p class="slideTitle">
						THE  ROAD  TO  MEMPHIS
					</p>
<img class="laurel" src="images/background/05_laurels.png"/>
<section class="reviews">
						"The Road to Memphis¡­is as fine as any film ever made about American music."<br/>
<cite>- Newsweek</cite><br/>
<br/>
						"The Road to Memphis the unadulterated gem of the bunch [the Scorsese Blues series]"<br/>
<cite>-The Nation</cite>
</section>
</div>
</span>
</li>
<!-- slide 6 content end -->
<!-- slide 7 content start -->
<li id="TwoDaysInOctober">
<span class="slideText">
<div>
<p class="slideTitle">
						TWO  DAYS  IN  OCTOBER
					</p>
<section class="awards">
<h1>2006 EMMY WINNER</h1>
<br/><br/>
<h1>2005 PEABODY WINNER</h1>
</section>
<section class="reviews">
						"A superb and heart breaking documentary."<br/>
<cite>- San Francisco Chronicle</cite><br/>
<br/><br/>
						"If you could watch only one program to grasp what the Vietnam War did to the U.S. Two Days would be a great choice. It is profound."<br/>
<cite>- Boston Globe</cite>
</section>
</div>
</span>
</li>
<!-- slide 7 content end -->
<!-- slides end -->
</ul>
<div class="headerholder">
<div class="header">
<link href="https://fonts.googleapis.com/css?family=Lato:400,100,200,300,700" rel="stylesheet" type="text/css"/>
<!-- css -->
<link href="css/base.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/mobile_menu.css" rel="stylesheet" type="text/css"/>
<!-- js -->
<script src="https://robertkennerfilms.com/js/jquery-1.9.1.min.js"></script>
<script>
	$( document ).ready( function () {
		
		// Toggle submenu slidedown
		$( '.dropdown' ).hover(
			function () {
				if ($(window).width() > 720){
					$( this ).children( '.sub-menu' ).slideDown( 200 );
				}
			},
			function () {
				if ($(window).width() > 720){
					$( this ).children( '.sub-menu' ).slideUp( 200 );
				}
			}
			
		);

		// Toggle mobile menu slidedown
		$('#mobile_menu_icon').click(
			function() {
			$( "#menu" ).slideToggle( "fast", function() {
				//animation complete
			});
		});
		// Toggle submenu slidedown
		$( '.dropdown' ).click(
			function () {
				if ($(window).width() <= 720){
					$( this ).children( '.sub-menu' ).slideToggle("fast");
				}
			}
		);
		
		$(window).resize(function () {
			if ($(window).width() > 721){
				$('#menu').css("display", "block");
			}
			else {
				$('#menu').css("display", "none");
			}
		});
		
	} ); // end ready
</script>
<div align="center">
<div align="center">
<div id="title">ROBERT KENNER FILMS</div>
</div>
<nav>
<img alt="" id="mobile_menu_icon" src="images/mobile_menu_icon.png"/>
<ul class="clearfix" id="menu">
<li>
<a class="buttonMenu" href="index.php">HOME</a>
</li>
<li class="dropdown">
<a class="buttonMenu" href="#">RECENT RELEASES</a>
<ul class="sub-menu" id="recent_releases_submenu">
<li><a href="confession_killer.php">THE CONFESSION KILLER</a>
</li>
<li><a href="command_and_control.php">COMMAND &amp; CONTROL</a>
</li>
<li><a href="merchants_of_doubt.php">MERCHANTS OF DOUBT</a>
</li>
<li><a href="food_inc.php">FOOD, INC.</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="buttonMenu" href="../films/results_new.php?S_title=&amp;S_synopsis_long=&amp;S_category=DOCUMENTARY&amp;Search=Search">FILMS</a>
<ul class="sub-menu" id="films_submenu">
<li><a href="films/results_new.php?S_title=&amp;S_synopsis_long=&amp;S_category=DOCUMENTARY&amp;Search=Search">DOCUMENTARIES</a>
</li>
<li><a href="films/results_new.php?S_title=&amp;S_synopsis_long=&amp;S_category=COMMERCIAL&amp;Search=Search">COMMERCIALS</a>
</li>
</ul>
</li>
<!-- <li><a href="coming_soon.php" class="buttonMenu">COMING SOON</a>
			</li> -->
<li><a class="buttonMenu" href="about.php">ABOUT</a>
</li>
<li><a class="buttonMenu" href="contact.php">CONTACT</a>
</li>
</ul>
</nav>
</div> </div>
</div>
<div class="footerholder">
<div class="footer">
<div align="center" style="font-size:9px; letter-spacing:4px;">
<div style="padding:40px;"> 
ROBERT KENNER FILMS | © 2021 | ALL RIGHTS RESERVED<br/>
<a href="mailto:info@robertkennerfilms.com">INFO@ROBERTKENNERFILMS.COM</a>
</div>
</div> </div>
</div>
</body>
</html>