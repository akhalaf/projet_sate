<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>TFL | Ridley Turkey Bowl</title>
<link href="img/template/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/menu.css" rel="stylesheet" type="text/css"/>
<link href="css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="js/menu.js" type="text/javascript"></script>
<script src="js/prototype.js" type="text/javascript"></script>
<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="js/effects.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/lightbox.js" type="text/javascript"></script>
<!--[if lte IE 6]>
<link href="css/ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
<link href="css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body class="width_medium" id="page_bg" onload="setNav();Countdown();">
<div align="center" class="center">
<div id="wrapper_center">
<div id="header">
<div id="logo"><img src="img/template/logo.png"/></div>
</div>
<div id="toppathway">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td class="leftbg"><!-- --></td>
<td class="rightbg"><!-- --></td>
</tr>
</table>
</div>
<div id="tabarea">
<div id="tabarea_l">
<div id="tabarea_r">
<table cellpadding="0" cellspacing="0" class="pill">
<tr>
<td class="pill_m">
<div id="pillmenu">
<ul id="mainlevel-nav">
<li><a class="mainlevel-nav" href="index.php">Home</a></li>
<li><a class="mainlevel-nav" href="?c=1">About</a></li>
<li class="collapsed">
<a class="mainlevel-nav" href="?c=7">Players</a>
<div class="dropContainer">
<div class="dropMenu">
<div class="dropItem"><a class="menuItem" href="?c=6&amp;id=11">Blake, Dan</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=23">Bucella, Tuna</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=15">Caldwell, Ken</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=14">Childs, Mike</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=12">Cipalone, Vince</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=29">Conn, Kris</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=20">Cottom, Keith</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=3">Crooks, Chris</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=6">DiNofia, Rich</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=19">Doubet, Joe</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=4">Dougherty, Tim</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=16">Graff, Matt</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=13">Graff, Sean</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=25">Hart, Ian</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=8">Hart, Jon</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=21">Kaufmann, Tim</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=10">Kelly, Sean</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=22">Laughlin, Tony</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=2">Marcelli, Mark</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=1">Marcelli, Matt</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=17">Middleton, Paul</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=33">Orio, Rick</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=27">Orio, Rob</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=24">Orio, Tom</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=18">Pohlig, Bill</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=7">Rayer, Kevin</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=26">Simcox, Joe</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=5">Slaughter, Sergeant</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=30">Stagg, Christian</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=31">Tomczuk, Steve</a></div><div class="dropItem"><a class="menuItem" href="?c=6&amp;id=9">Trill, Mike</a></div> </div>
</div>
</li>
<li class="collapsed">
<a class="mainlevel-nav" href="?c=14">Teams</a>
<div class="dropContainer">
<div class="dropMenu">
<div class="dropItem"><a class="menuItem" href="?c=12&amp;id=27">#golddiggers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=26">#wolfkillers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=33">2 Guys, 1 Shed</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=16">Bad Newz</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=3">Beer</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=4">CK/NL/WU</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=14">Doc F*ckers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=22">Four Lokos Deep</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=31">Golden Shower Rams</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=23">Grease Busters</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=24">Hoagiefest</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=35">Horsedoc</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=19">Linebacker U</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=25">Lou Turk's</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=30">Nappy Headed Trills</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=7">New School (TB4)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=10">New School (TB5)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=12">New School (TB6)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=21">New School (TB10)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=8">Old School (TB4)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=9">Old School (TB5)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=11">Old School (TB6)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=20">Old School (TB10)</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=29">Parkers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=6">Po' Folk</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=17">Sand Naggers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=36">Saturday Night Soldiers</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=39">Team Doc</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=1">Team Possum</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=34">Team Rip, Man</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=38">Team Trill</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=15">Team Zissou</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=28">Termites</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=5">THC</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=2">The UnPhairs</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=37">The Vipahs</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=18">Thug U</a></div><div class="dropItem"><a class="menuItem" href="?c=12&amp;id=32">Wet Bandits</a></div> </div>
</div>
</li>
<li class="collapsed">
<a class="mainlevel-nav" href="?c=4">Games</a>
<div class="dropContainer">
<div class="dropMenu">
<div class="dropItem"><a class="menuItem" href="?c=3&amp;id=1">Turkey Bowl I</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=2">Turkey Bowl II</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=3">Turkey Bowl III</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=4">Turkey Bowl IV</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=5">Turkey Bowl V</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=6">Turkey Bowl VI</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=7">Turkey Bowl VII</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=8">Turkey Bowl VIII</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=9">Turkey Bowl IX</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=10">Turkey Bowl X</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=11">Turkey Bowl XI</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=12">Turkey Bowl XII</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=13">Turkey Bowl XIII</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=14">Turkey Bowl XIV</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=15">Turkey Bowl XV</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=16">Turkey Bowl XVI</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=17">Turkey Bowl XVII</a></div><div class="dropItem"><a class="menuItem" href="?c=3&amp;id=18">Turkey Bowl XVIII</a></div> </div>
</div>
</li>
<li><a class="mainlevel-nav" href="?c=10">Stats</a></li>
<li><a class="mainlevel-nav" href="?c=16">MVPs</a></li>
<li><a class="mainlevel-nav" href="?c=17">Draft</a></li>
<li><a class="mainlevel-nav" href="javascript:openMessageBoards();">Message Boards</a></li>
<li><a class="mainlevel-nav" href="?c=5">Photos</a></li>
<li><a class="mainlevel-nav" href="?c=15">Videos</a></li>
<li><a class="mainlevel-nav" href="?c=8">Rules</a></li>
<li><a class="mainlevel-nav" href="?c=2">Charity</a></li>
</ul>
</div> </td>
</tr>
</table>
</div>
</div>
</div>
<div id="leftbg">
<div id="rightbg">
<div id="whitebox">
<div id="whitebox_m">
<div id="area">
<div id="maincolumn">
<table class="nopad" style="height: 100%;">
<tr valign="top">
<td>
<div>
<span class="header">2G1S Win in OT Thriller</span><br/><span class="author"><b>POSTED</b> Monday November 30th, 2015 <b>by</b> Crooks</span></div><div id="story">In 1986, John Elway engineered a 5 minute and 2 second, 15-play, 98-yard offensive onslaught that culminated in a Denver Broncos' touchdown, tying the AFC Championship Game score at 20-20. The Broncos would then enter overtime and defeat the Cleveland Browns on a field goal, winning 23-20. Elway's heroics, along with those of his teammates, on that fateful game-tying march would forever be known as <a href="https://en.wikipedia.org/wiki/The_Drive"><i>The Drive</i></a>.
<br/><br/>
Let's face it, <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=4">Tim Dougherty</a> is no John Elway; very few people possess the abilities and accolades of the NFL Hall of Fame quarterback. Tim Doc, on the other hand, was at least Elway-esque in <a href="http://ridleyturkeybowl.com/index.php?c=3&amp;id=16">TB XVI</a>.  Dougherty, facing a 6-4 deficit with 5 minutes remaining in the game, made John Elway proud.
<br/><br/>
The 2015 Turkey Bowl marked the sixteenth installment of TFL football over a fifteen year period. Captains <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=6">Rich DiNofia</a> and <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=3">Chris Crooks</a> would lead their respected teams, DiNofia's <a href="http://ridleyturkeybowl.com/index.php?c=12&amp;id=32">Wet Bandits</a> and Crooks' <a href="http://ridleyturkeybowl.com/index.php?c=12&amp;id=33">2 Guys, 1 Shed</a>, in a game that would not disappoint. From the opening kickoff to the final play of overtime, TB XVI would cement itself as one of the best in the short, yet storied, history of the TFL.
<br/><br/>
Out of the gate, TB XVI proved to be interesting. 2G1S, <i>coin toss</i> winners, chose to defer their selection until the second half. Wet Bandits' QB <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=9">Mike Trill</a>, in a move reminiscent of Mike Dollard's Ridley HS shenanigans, selected to kick, rather than receive. Trill's hand was forced by the absence of his starting lineman <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=13">Sean Graff</a> and was not at all a choice of idiocy, but would be representative of the first time in TFL history a team, 2G1S, would receive at the start of both halves. Graff would enter the game shortly after, and Trill's pregame move would not prove to be a costly one as 2G1S would not capitalize on their initial drive of either half. Additionally, TFL <i>founder</i> <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=32">Steve Razzio</a>, whom none of us have seen in, ummmmmm, NEVER, had some of his friends and family pay the competitors a visit prior to the game. Luckily, they dispersed quickly and without incident.
<br/><br/>
Turkey Bowl XVI would begin much like any other Turkey Bowl; competitors would strive to get their footing as quickly as possible, trying valiantly to return to a game they have not played in over a year. <i>(In the interest of complete disclosure, participants rarely acclimate to the point of resembling real football players, but we dress the part well.)</i>
<br/><br/>
After exchanging a few possessions, the Wet Bandits would be the first to dent the scoreboard. Wet Bandits' QB Mike Trill lofted a beautiful ball to 2x TFL MVP <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=17">Paul Middleton</a> for the game's first score. Middleton, who has consistently had a knack for finding open space, would be a constant in the Wet Bandits' passing game. Paul would again visit the end zone in the first half, accounting for both the WB's scores that half.
<br/><br/>
While the Wet Bandits' struck quickly in the first half of TB XVI, 2G1S employed a more pronounced, maybe even lethargic, pace. 2G1S QB Tim Dougherty would slow the game to a crawl, calling individual routes, hitting receivers at short distances. Dougherty's strategy would keep the Wet Bandits' defense on the field at great lengths, as 2G1S dominated time of possession, but would not amount in a halftime lead. 2G1S could only equal the offensive output of the Wet Bandits', as WR Chris Crooks visited pay-dirt twice. The teams would enter the half tied, 2-2.
<br/><br/>
The second half began at odds with the first. 2G1S struggled to move the ball on their first possession and the Wet Bandits' defense was stout and determined. The Wet Bandits' defensive prowess paid off, as it directly influenced and fueled their offensive production. Wet Bandits' WR Rich DiNofia consistently found holes in the 2G1S defense early in the second half. Trill would hit DiNofia twice for second half scores. Sandwiched in between DiNofia's touchdowns was yet another Paul Middleton reception that found the end zone. Before you could blink, the Wet Bandits had put the ball passed the goal line three times and amassed a 5-2 lead.
<br/><br/>
Things began to look bleak for 2G1S, as momentum had swung like a pendulum to the Wet Bandits', but, as we all know, the pendulum will eventually return to its former position. 2G1S QB Tim Dougherty threw caution to the wind. Abandoning his play-calling duties, Dougherty simply let his wide receivers put in work. Behind the stellar line play of <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=20">Keith Cottom</a> and <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=24">Tom Orio</a>, Dougherty brought 2G1S back to life. Doc would find OL/WR Tom Orio and WR <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=30">Christian <i>The Lion</i> Stagg</a> for tallies, desperately trying to answer the Wet Bandits' offensive barrage. 2G1S would cut the deficit to 1, but, at the same time, awaken a napping Wet Bandits' offense.
<br/><br/>
In hopes of putting the game out of reach, Wet Bandits' QB Mike Trill would look to one of his sure-handed receivers: <a href="http://ridleyturkeybowl.com/index.php?c=3&amp;id=15">TB XV</a> MVP <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=29">Kris Conn</a>. Wet Bandits' sole lineman Sean Graff, exhausted from shouldering offensive line duties for the entire game, took to the field for what may have been his team's last offensive drive of the game. Graff, who put together an excellent TB XVI performance, allowing only 2 sacks, gave his quarterback enough time to hit Kris Conn for the Wet Bandits' sixth score. With that touchdown and a 6-4 lead, the Wet Bandits' were one stop away from ending TB XVI victorious.
<br/><br/>
Enter Tim <i>Elway</i> Dougherty. With 5 minutes remaining in regulation, Doc trotted his offense out on to the field. One minute later, he led them off, cutting the deficit from two to one. In three plays and one minute, Doc and 2G1S arose from the ashes of defeat like the Phoenix. Dougherty reeled off two consecutive completions to WR Chris Crooks to get 2G1S to the Wet Bandits' one-yard line. From there, using the patented first-down play, Dougherty hit <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=1">Matt Marcelli</a> for the score. At 11:36am, with 4 minutes remaining in regulation, the Wet Bandits' led 6-5 and, on their next offensive possession, were one first-down away from ending TB XVI.
<br/><br/>
The Wet Bandits' much needed first-down would never come. After misfiring on a 3rd &amp; 2, the Bandits faced a 4th &amp; G in their own territory. Wisely, playing the odds and a game of field position, the Wet Bandits chose to punt. After a short return, 2G1S found themselves in the shadow of their own end zone, but, it was 11:39am, with one minute remaining in the game. As TFL rules state, a game cannont end mid drive; so long as they produced a first-down, 2G1S had all the time in the world. Again, Dougherty went to work. Doc's first pass fell incomplete at the feet of WR Christian Stagg, but he would not miss again. On 2nd &amp; 3 Dougherty hit WR <a href="http://ridleyturkeybowl.com/index.php?c=6&amp;id=31">Steve Tomczuk</a> on a short out pattern. Dougherty then went back to Crooks for his next two completions, moving 2G1S down the field and giving them a new series to work with.<br/><br/>2G1S now had a fresh set of downs, four plays, but they would only need one.<br/><br/>On 1st &amp; 3 Dougherty threw a perfect ball to WR Matt Marcelli in the back of the end zone. Fighting the sun and struggling to stay in bounds, Marcelli corralled the pass for the touchdown, tying the game at 6-6 and forcing overtime.<br/><br/>In the span of 7 minutes, 2G1S QB Tim Dougherty put up statistics that would make any QB proud. Doc went 7-for-8 with 2 TDs. His performance was, rightfully coining the adjective, epic.
<br/><br/>
<i>Spaces or laces?</i>. The call from the Wet Bandits was <i>laces</i>; the ball fell spaces heads-up. 2G1S would choose to go on defense for the first series of overtime. The Wet Bandits offense took the field, hoping to score, then hold for the victory. The latter would occur, but not the former. 2G1S's defense would stop the Wet Bandits from crossing the goal line, but the same would be accomplished on the ensuing 2G1S possession, as the Wet Bandits' defense remained sturdy. As it is with NCAA overtime rules, 2G1S would immediately take the first possession of the second OT; they would strike quickly. Running a short post-flag towards the right pylon, Chris Crooks would get separation and Tim Dougherty would not miss. 2G1S would take a 7-6 lead with only two defensive stands needed to clinch the victory. Those much needed stands would prove obtainable. Unable to connect on their final possession, the Wet Bandits ceded TB XVI glory to 2G1S.
<br/><br/>
Turkey Bowl XVI proved to be one of our better games. Quarterbacks Tim Dougherty and Mike Trill again find themselves tied for the <a href="http://ridleyturkeybowl.com/index.php?c=9&amp;id=10">TFL Career Passing TD record</a> and 10 of the games' 13 scores came by way of 4 players (Chris Crooks (3), Paul Middleton (3), Rich DiNofia (2), Matt Marcelli (2)). Tom Orio scored the first touchdown of his career and <a href="http://ridleyturkeybowl.com/?c=6&amp;id=10">Sean Kelly</a> found himself a couple inches away from yet another TFL score. <a href="http://ridleyturkeybowl.com/?c=6&amp;id=2">Mark Marcelli</a> reminded us all why he is still a great defender and an even better trash talker. I will particularly always remember this Turkey Bowl as I, Chris Crooks, was awarded MVP. As I write this, the countdown clock currently reads 359 days, 22 hours, 22 minutes and 3 seconds, which is 359 days, 22 hours, 22 minutes and 3 seconds too long.</div> </td>
<td class="greyline">
<div class="greyline_bottom" height="100%"></div>
</td>
<td width="27%">
<div class="countdownclock">
<span id="spnCountdownHeader">Countdown to Turkey Bowl XXII</span>
<br/>
<span id="spnCountdownClock"> </span>
</div>
<div class="year">2015</div>
<div id="div2015" style=""><div>
 • <a class="headline" href="?id=83">2G1S Win in OT Thriller</a><br/> • <a class="headline" href="?id=81">Average Draft Position Through 2015</a><br/> • <a class="headline" href="?id=79">TURKEY BOWL XVI UPDATES</a><br/> • <a class="headline" href="?id=78">By The Hashtags</a><br/></div></div><div class="year">2014</div>
<div id="div2014" style=""><div>
 • <a class="headline" href="?id=75">The TFL In Pictures</a><br/> • <a class="headline" href="?id=74">TB XV Preview</a><br/> • <a class="headline" href="?id=73">TB XV Draft In the Books</a><br/> • <a class="headline" href="?id=72">Parkers Prove Terminix Obsolete</a><br/></div></div><div class="year">2013</div>
<div id="div2013" style=""><div>
 • <a class="headline" href="?id=69">2013 Mock Draft</a><br/> • <a class="headline" href="?id=68">The Dan Vincent Code</a><br/></div></div><div class="year">2012</div>
<div id="div2012" style=""><div>
 • <a class="headline" href="?id=65">#wolfkillers Mine for #gold</a><br/> • <a class="headline" href="?id=64">Turkey Bowl XIII News And Notes</a><br/></div></div><div class="year">2011</div>
<div id="div2011" style=""><div>
 • <a class="headline" href="?id=63">Hoagies Not an Overtime Treat</a><br/> • <a class="headline" href="?id=61">2011 TFL Draft &amp; Notes</a><br/></div></div><div class="year">2010</div>
<div id="div2010" style=""><div>
 • <a class="headline" href="?id=62">Four Lokos Deep Wins; Tim Doc Ret...</a><br/> • <a class="headline" href="?id=59">Survey Says...</a><br/> • <a class="headline" href="?id=57">Turkey Bowl XI Factoids</a><br/> • <a class="headline" href="?id=58">Nabisco Releases Orio</a><br/> • <a class="headline" href="?id=56">TB XI Draft Successful Despite Ef...</a><br/> • <a class="headline" href="?id=55">New School Delivers Series Tying Blow</a><br/></div></div><div class="year">2009</div>
<div id="div2009" style=""><div>
 • <a class="headline" href="?id=54">Turkey Bowl X News And Notes</a><br/> • <a class="headline" href="?id=50">TFL Top 10 Games</a><br/> • <a class="headline" href="?id=51">TFL Top 10 Careers</a><br/> • <a class="headline" href="?id=49">TFL Top 10 Individual Performances</a><br/> • <a class="headline" href="?id=48">TFL Top 10 Teams</a><br/> • <a class="headline" href="?id=47">TFL Top 10 Plays</a><br/> • <a class="headline" href="?id=53">TBX: In With The Old, In With The New</a><br/> • <a class="headline" href="?id=52">The Rivalry Continues</a><br/> • <a class="headline" href="?id=46">Ode To Those Who Don't Play</a><br/></div></div><div class="year">2008</div>
<div id="div2008" style=""><div>
 • <a class="headline" href="?id=45">TB IX: It Only Takes One Or Two</a><br/> • <a class="headline" href="?id=44">Turkey Bowl IX News And Notes</a><br/> • <a class="headline" href="?id=41">9 &amp; 9 for Turkey Bowl IX</a><br/> • <a class="headline" href="?id=35">5 Questions That Need Answers</a><br/> • <a class="headline" href="?id=40">The Day TFL Football Will Go To Hell</a><br/> • <a class="headline" href="?id=39">TB IX Draft Diary</a><br/> • <a class="headline" href="?id=38">Carmen-IZERS vs Bad Loads: Mock D...</a><br/> • <a class="headline" href="?id=37">2008 Draft Preview</a><br/> • <a class="headline" href="?id=36">Captain Carmens vs. Captain Kia</a><br/> • <a class="headline" href="?id=23">The TFL. . . Look How Far We've Come</a><br/> • <a class="headline" href="?id=21">BNK Spells Bad Newz for the Sand ...</a><br/></div></div><div class="year">2007</div>
<div id="div2007" style=""><div>
 • <a class="headline" href="?id=20">TFL Charities '07</a><br/> • <a class="headline" href="?id=19">Turkey Bowl VIII - Theoretical Ma...</a><br/> • <a class="headline" href="?id=17">Turkey Bowl VIII - Theoretical Ma...</a><br/> • <a class="headline" href="?id=29">Horse Scored?!: Greatest Moments ...</a><br/> • <a class="headline" href="?id=25">Protect Your Neck!! - Best of the...</a><br/> • <a class="headline" href="?id=26">Can The Desert Boys Keep The Dogg...</a><br/> • <a class="headline" href="?id=28">HEADstrong Foundation Fundraiser</a><br/> • <a class="headline" href="?id=27">Draft Diary '07</a><br/> • <a class="headline" href="?id=24">2007 Draft Preview</a><br/></div></div><div class="year">2006</div>
<div id="div2006" style=""><div>
 • <a class="headline" href="?id=15">"Sorry Im Late": Turkey Bowl VII</a><br/> • <a class="headline" href="?id=14">3rd Annual Toy Drive</a><br/> • <a class="headline" href="?id=13">Scanning The Waiver Wire</a><br/> • <a class="headline" href="?id=12">Its All About Match Ups</a><br/> • <a class="headline" href="?id=11">Aftermath of the Draft</a><br/> • <a class="headline" href="?id=10">The No. 1 Choice</a><br/> • <a class="headline" href="?id=9">Cottom, Old School Leave No Doubt</a><br/></div></div><div class="year">2005</div>
<div id="div2005" style=""><div>
 • <a class="headline" href="?id=8">T.B. Does Toys</a><br/> • <a class="headline" href="?id=7">Episode VI: Return of the Bloody ...</a><br/> • <a class="headline" href="?id=6">The Rubber Match</a><br/> • <a class="headline" href="?id=5">A Day of Redemption for Old School</a><br/> • <a class="headline" href="?id=4">The Strippage at Pritchard</a><br/> • <a class="headline" href="?id=3">Turkey Bowl 3 - Game Summary</a><br/> • <a class="headline" href="?id=2">Turkey Bowl 2 - Game Summary</a><br/> • <a class="headline" href="?id=1">Turkey Bowl 1 - Game Summary</a><br/></div></div> </td>
</tr>
</table>
</div>
<div class="clr"></div>
</div>
<div class="clr"></div>
<div class="clr"></div>
<div class="domainsearch">
<div class="clr"></div>
<div class="Spacer10"></div>
</div>
</div>
</div>
</div>
</div>
<div id="footer_width" style="height: 100%;">
<div id="footer">
<div id="footer_l">
<div id="footer_m"></div>
<div id="footer_r">
<p style="float: left; width: 100%;">
							TFL | Ridley Turkey Bowl © 2021<br/>
							Ridley Township, PA
							<!--Valid <a href="http://validator.w3.org/check/referer">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.-->
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>