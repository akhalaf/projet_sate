<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Angela Desveaux and the Mighty Ship</title>
<style type="text/css">
<!--
#apDiv1 {
	position:absolute;
	left:70px;
	top:17px;
	width:992;
	height:200px;
	z-index:1;
}
#apDiv2 {
	position:absolute;
	left:250px;
	top:317px;
	width:536px;
	height:250px;
	z-index:2;
	overflow: auto;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="ad-new.css" rel="stylesheet" type="text/css"/>
</head>
<body onload="MM_preloadImages('images/banner-over_03.jpg','images/banner-over_04.jpg','images/banner-over_05.jpg','images/banner-over_06.jpg','images/banner-over_07.jpg','images/banner-over_08.jpg','images/banner-over_09.jpg')">
<div id="apDiv1">
<table border="0" cellpadding="0" cellspacing="0" width="802">
<tr>
<td><img height="91" src="images/banner_01.jpg" width="992"/></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img height="27" src="images/banner_02.jpg" width="180"/></td>
<td><a href="home.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','images/banner-over_03.jpg',1)"><img alt="home" border="0" height="27" id="Image5" name="Image5" src="images/banner_03.jpg" width="50"/></a></td>
<td><a href="shows.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/banner-over_04.jpg',1)"><img alt="shows" border="0" height="27" id="Image6" name="Image6" src="images/banner_04.jpg" width="62"/></a></td>
<td><a href="music.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','images/banner-over_05.jpg',1)"><img alt="music" border="0" height="27" id="Image7" name="Image7" src="images/banner_05.jpg" width="54"/></a></td>
<td><a href="photos.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image8','','images/banner-over_06.jpg',1)"><img alt="photos" border="0" height="27" id="Image8" name="Image8" src="images/banner_06.jpg" width="68"/></a></td>
<td><a href="press.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','images/banner-over_07.jpg',1)"><img alt="press" border="0" height="27" id="Image9" name="Image9" src="images/banner_07.jpg" width="49"/></a></td>
<td><a href="links.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/banner-over_08.jpg',1)"><img alt="links" border="0" height="27" id="Image10" name="Image10" src="images/banner_08.jpg" width="46"/></a></td>
<td><a href="contact.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image11','','images/banner-over_09.jpg',1)"><img alt="contact" border="0" height="27" id="Image11" name="Image11" src="images/banner_09.jpg" width="76"/></a></td>
<td><img height="27" src="images/banner_10.jpg" width="407"/></td>
</tr>
</table></td>
</tr>
<tr>
<td><img height="165" src="images/banner_11.jpg" width="992"/></td>
</tr>
</table>
</div>
<div id="apDiv2">
<table border="0" cellpadding="4" cellspacing="0" width="500">
<tr>
<td align="left" valign="top"><h1>Home</h1>
<p>Angela's new release <em>The Mighty Ship</em> will be available September 9th. For pre-order, please visit <a href="http://www.thrilljockey.com" target="_blank">Thrill Jockey</a> (US, Europe) or <a href="http://www.sonicunyon.com" target="_blank">Sonic Unyon</a> (Canada).</p>
<p> CD release shows begin in September. Check the <a href="shows.html">shows page</a>.</p>
<p>Hope to see you.</p>
<p>AD &amp; MS</p>
<p align="right"><br/>
        Site produced in co-operation with <a href="http://www.videofact.ca" target="_blank"><img border="0" height="14" src="images/promofact_logo.gif" width="100"/></a>.<br/>
        Design by <a href="http://www.shortestdistance.net" target="_blank">Shortest Distance</a>.<br/>
      Contents Â© 2008, Angela Desveaux &amp; the Mighty Ship.</p>
</td>
</tr>
</table>
</div>
</body>
</html>
