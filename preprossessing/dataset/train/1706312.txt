<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>404error</title>
<style type="text/css">
    body {background-color: #fff; text-align: center; padding: 0; margin: 0;}
    .content img { width: 100%; max-width: 1080px; padding: 0; margin: 0 auto; }
    </style>
</head>
<body>
<div class="content">
<img src="/media/maintenance/files/404_error.png"/>
</div>
</body>
</html>
