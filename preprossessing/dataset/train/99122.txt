<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Wentworth County, Hamilton, 1911 census Ontario</title>
<meta content="census, Census records,  Wentworth County, Hamilton,  Ontario, Canada,  Canadian,  geneology, ancestors, ancestry" name="keywords"/>
<meta content="Search for ancestors inWentworth County, Hamilton, District 77, Subdistrict 1 , 1911 census Ontario" name="description"/>
</head>
<body>
<table border="0" cellpadding="2" cellspacing="2" width="100%">
<tbody>
<tr>
<td valign="top" width="40%">
<!--coloured box-->
<table bgcolor="#CCCC99" border="0" cellpadding="5" cellspacing="5" width="70%">
<tr bgcolor="#ffffff" valign="TOP">
<td align="LEFT" width="100%">
<font face="Verdana, Arial, Helvetica, sans-serif" size="2">
<img alt="Find ancestors in online census records for USA and Canada" border="0" height="5" src="/graphics/lisquare.gif" width="5"/>  Find ancestors in online census records for <a href="/usa/">USA</a> and <a href="/canada/">Canada</a>
<br/>
<img alt="Find ancestors in online census records for USA and Canada" border="0" height="5" src="/graphics/lisquare.gif" width="5"/>  
            <a href="/usa/census-compare.shtml">Quick Start USA</a>- Census Comparison 
            Charts for those in a hurry<br/><img alt="Find ancestors in online census records for USA and Canada" border="0" height="5" src="/graphics/lisquare.gif" width="5"/>   <a href="/canada/census-compare.shtml">Quick Start Canada</a>- Census Comparison 
            Charts for those in a hurry      <br/><img alt="Find ancestors in online census records for USA and Canada" border="0" height="5" src="/graphics/lisquare.gif" width="5"/>  <a href="http://olivetreegenealogy.com/beginner/" target="_blank">Genealogy Guide - Help for Beginners</a>
</font></td>
</tr>
</table>
<!-- end coloured box-->
<!-- <TR> --> </td><td valign="left" width="59%"> <font face="Garamond" size="+3"><b>AllCensusRecords.com</b></font><br/> <!-- start ancestry--><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Your One-Stop Site for links to USA Census Records,  Canadian Census Records, English Census Records, Census Indexes &amp; Images, Census Transcriptions, Blank Census Forms, City Directories, Tax &amp; Assessment Lists, Voters Registrations, Veterans Census, Questions on Census Records  </font> <br/><br/> <!--*** SB -->
<table border="0" summary="" width="100%"> <tr> <td>
<!--start Jan 2019 search box-->
<center>
<script>
      function FixAncestry() {
        var txt = document.ancestry.select[document.ancestry.select.selectedIndex].text;
        var val = document.ancestry.select[document.ancestry.select.selectedIndex].value;
        if (txt == "Any" || txt == "---" || txt == "INTL") {
          document.ancestry.gs1co.value = "1,All Countries";
          document.ancestry.gs1pl.value = "1, "
        } else if (txt == "USA") {
          document.ancestry.gs1co.value = "2,USA";
          document.ancestry.gs1pl.value = "1,All States";
        } else if (txt == "CAN") {
          document.ancestry.gs1co.value = "3243,Canada";
          document.ancestry.gs1pl.value = "1,All Provinces";
        } else if (val.substr(0,3) == "500" || val.substr(0,3) == "501") {
          document.ancestry.gs1co.value = "3243,Canada";
          document.ancestry.gs1pl.value = val;
        } else {
          document.ancestry.gs1co.value = "2,USA";
          document.ancestry.gs1pl.value = val;
        }
      }

     function SubmitIt() { // this is for new affiliate program (Jan 2019)
       url = 
         "https://prf.hn/click/camref:1011l4wyt/pubref:Census/destination:https://search.ancestry.com/cgi-bin/sse.dll?" +
         "cj=1&gs1pl=" + document.ancestry.select.value + "&rank=0&gl=allgs&pcc=2&prox=1&" +
         "gsfn=" + document.ancestry.gsfn.value + "&gsln=" + document.ancestry.gsln.value + "&" +
         "select=" + document.ancestry.select.value;
       window.open(url);
     }

    </script>
<form action="https://www.dpbolvw.net/interactive" name="ancestry" xtarget="ancestrysearch">
<input name="gs1co" type="hidden" value="1,All Countries"/>
<input name="gs1pl" type="hidden" value="1, "/>
<input name="t" type="hidden" value="9915"/>
<input name="rank" type="hidden" value="0"/>
<input name="pcc" type="hidden" value="2"/>
<input name="gss" type="hidden" value="affiliate"/>
<input name="prox" type="hidden" value="1"/>
<input name="gl" type="hidden" value="allgs"/>
<input name="srchb" type="hidden" value="r"/>
<table border="0" cellpadding="0" cellspacing="0" style="background:url(/graphics/ancestry_468_60_search.jpg) no-repeat; width:468px; height:60px;">
<tr>
<td style="padding-top:18px; padding-left:8px; width:98px;font-family:'Verdana, Arial, Trebuchet MS'; font-size:12px; color:#685f4c;" valign="top">
<input name="gsfn" size="10"/>
<br/>
<font style="font-family:'Verdana, Arial, Trebuchet MS'; font-size:12px; color:#685f4c;">
<!--   FIRST NAME-->
</font>
</td>
<td style="padding-top:18px; width:98px;font-family:'Verdana, Arial, Trebuchet MS'; font-size:12px; color:#685f4c;" valign="top">
<input name="gsln" size="10"/>
<br/>
<font style="font-family:'Verdana, Arial, Trebuchet MS'; font-size:12px; color:#685f4c;">
<!--  LAST NAME-->
</font>
</td>
<td arial="" color:="" font-family:="" font-size:12px="" ms="" style="padding-top:16px; width:70px;" trebuchet="">
<select name="select" onchange="FixAncestry();">
<option value="2,Any Locality">Any</option>
<option value="">USA</option>
<option value="3,Alabama">AL</option>
<option value="4,Alaska">AK</option>
<option value="5,Arizona">AZ</option>
<option value="6,Arkansas">AR</option>
<option value="7,California">CA</option>
<option value="8,Colorado">CO</option>
<option value="9,Connecticut">CT</option>
<option value="10,Delaware">DE</option>
<option value="11,District of Columbia">DC</option>
<option value="12,Florida">FL</option>
<option value="13,Georgia">GA</option>
<option value="14,Hawaii">HI</option>
<option value="15,Idaho">ID</option>
<option value="16,Illinois">IL</option>
<option value="17,Indiana">IN</option>
<option value="18,Iowa">IA</option>
<option value="19,Kansas">KS</option>
<option value="20,Kentucky">KY</option>
<option value="21,Louisiana">LA</option>
<option value="22,Maine">ME</option>
<option value="23,Maryland">MD</option>
<option value="24,Massachusetts">MA</option>
<option value="25,Michigan">MI</option>
<option value="26,Minnesota">MN</option>
<option value="27,Mississippi">MS</option>
<option value="28,Missouri">MO</option>
<option value="29,Montana">MT</option>
<option value="30,Nebraska">NE</option>
<option value="31,Nevada">NV</option>
<option value="32,New Hampshire">NH</option>
<option value="33,New Jersey">NJ</option>
<option value="34,New Mexico">NM</option>
<option value="35,New York">NY</option>
<option value="36,North Carolina">NC</option>
<option value="37,North Dakota">ND</option>
<option value="38,Ohio">OH</option>
<option value="39,Oklahoma">OK</option>
<option value="40,Oregon">OR</option>
<option value="41,Pennsylvania">PA</option>
<option value="42,Rhode Island">RI</option>
<option value="43,South Carolina">SC</option>
<option value="44,South Dakota">SD</option>
<option value="45,Tennessee">TN</option>
<option value="46,Texas">TX</option>
<option value="47,Utah">UT</option>
<option value="48,Vermont">VT</option>
<option value="49,Virginia">VA</option>
<option value="50,Washington">WA</option>
<option value="51,West Virginia">WV</option>
<option value="52,Wisconsin">WI</option>
<option value="53,Wyoming">WY</option>
<option value="2,Any Locality">---</option>
<option value="">CAN</option>
<option value="5001,Alberta">AB</option>
<option value="5002,British Columbia">BC</option>
<option value="5003,Manitoba">MB</option>
<option value="5004,New Brunswick">NB</option>
<option value="5006,Newfoundland and Labrador">NF</option>
<option value="5011,Northwest Territories">NT</option>
<option value="5005,Nova Scotia">NS</option>
<option value="5012,Nunavut">TT</option>
<option value="5007,Ontario">ON</option>
<option value="5008,Prince Edward Islands">PE</option>
<option value="5009,Quebec">QC</option>
<option value="5010,Saskatchewan">SK</option>
<option value="5013,Yukon">YT</option>
<option value="2,Any Locality">---</option>
<option value="2,Any Locality">INTL</option>
</select>
<br/>
<font style="font-family:'Trebuchet MS, Verdana, Arial'; font-size:12px; color:#685f4c;">
                LOCALITY
              </font>
</td>
<td style="padding-top:20px;" valign="top" width="200">
<img height="20" onclick="javascript:SubmitIt();" src="/graphics/search_sm.gif" width="88" xonclick='OpenWindow("", document.ancestry, "ancestrysearch");'/>
<br/>
</td>
</tr>
</table>
</form>
</center>
<!--end Jan 2019 Search Box-->
<font face="Verdana, Arial, Helvetica, sans-serif" size="1">Find your ancestors with <font color="#ff0000">Free Trial</font> on <a href="https://prf.hn/click/camref:1011l4wyt/creativeref:1101l27800" target="_blank">Ancestry.com</a> and <a href="https://prf.hn/click/camref:1011l4wyu/creativeref:1011l28233">Ancestry.ca </a> free trial


<!-- --></font></td>
<!--insert--><td>
<table align="center" border="0" bordercolor="#CCCC99" summary="" width="90%"> <tr> <td><font face="Verdana, Arial, Helvetica, sans-serif" size="1"></font></td> </tr></table></td><!--end insert--> </tr></table> <!--end SB ancestry--> <!--end ancestry-->
</td> </tr> <tr> <td align="middle" colspan="2" width="100%"> <!--start top nav bar--><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="http://allcensusrecords.com/"><b>Home</b></a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/sitemap.shtml">Site Map</a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/help.shtml">FAQ</a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/usa/">USA Census</a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/canada/">Canada Census</a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/england/">England Census</a></font> </td> <td bgcolor="#606135" width="1%"><font class="divider"> </font> </td> <td align="center" bgcolor="#CCCC99" width="12%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><a href="/scotland/">Scotland Census</a></font> </td> </tr> <tr> <td bgcolor="#606135" colspan="11" width="100%"> </td> </tr> </table> <!--end top nav bar--> <table border="0" cellspacing="0" valign="left" width="100%"> <tbody> <tr bgcolor="#CCCC99"> <td align="middle"> <div align="center"> <p><!--
--> <font face="verdana, arial" size="1"> <div align="center"><table border="0" cellpadding="2" cellspacing="5"><tr><td valign="top"><font face="Arial, Verdana,  Times New Roman" size="1"><font face="Verdana, Arial, Times New Roman" size="2"> Read about the <a href="http://olivetreegenealogy.blogspot.com/2009/04/complete-canadian-census-1851-1916.html" target="_blank">COMPLETE Canadian Census 1851-1916</a></font></font></td></tr></table></div></font> </p></div> </td></tr> <div align="center"> <script type="text/javascript"><!--google_ad_client = "pub-7336459545097651";google_ad_width = 468;google_ad_height = 15;google_ad_format = "468x15_0ads_al";google_ad_channel ="";google_color_border = "FFFFFF";google_color_bg = "FFFFFF";google_color_link = "0000FF";google_color_url = "008000";google_color_text = "000000";//--></script><script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script></div>
</tbody></table></td></tr></tbody></table>
<table bgcolor="#ffffff" border="0" cellpadding="5" cellspacing="2" width="100%">
<tbody>
<tr align="left" valign="top">
<td valign="top" width="24%">
<div align="left"> <!--Census Ads--><table border="0" summary="" width="300"> <tr> <td>
</td> </tr></table>
<font face="Verdana, Arial, Helvetica, sans-serif"><script type="text/javascript"><!--google_ad_client = "pub-7336459545097651";google_ad_width = 300;google_ad_height = 250;google_ad_format = "300x250_as";google_ad_type = "text";//2007-01-30: Genealogy - Censusgoogle_ad_channel = "0786897376";google_color_border = "CCCC99";google_color_bg = "FFFFFF";google_color_link = "0000FF";google_color_text = "000000";google_color_url = "666666";//--></script><script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script> </font>
<p>
<font face="Verdana, Arial, Helvetica, sans-serif">
<a href="https://prf.hn/click/camref:1011l4wyt/creativeref:1011l28282" target="_blank"><img height="250" src="http://olivetreegenealogy.com/graphics/AncestryDNAimage300x250.png" width="300"/></a>
<hr/>
<table border="0" cellpadding="2" cellspacing="5"><tr><td valign="top" width="120"><font face="Arial, Verdana,  Times New Roman" size="1">
<!--Ancestry start-->
<script src="https://creative.prf.hn/creative/camref:1011l4wyt/creativeref:1100l28048" type="text/javascript"></script>
<!--Ancestry  end-->
<hr/>
<h4>Spotlight On</h4> <font face="Trebuchet MS, Arial, Verdana, Times New Roman"><img align="left" alt="Olive Tree Genealogy Blog 1 of Top 25 Blogs" border="0" height="90" src="/graphics/progen25blog2009-125x90.gif" width="120"/><a href="http://olivetreegenealogy.blogspot.com/" target="_blank"><b>Olive Tree Genealogy Blog</b></a> Technorati ranks it as one of the <b>25 Most Popular Genealogy Blogs</b> as of April 3, 2009. Don't miss the daily Tips, Announcements &amp; Ideas <br/><br/>JOIN the FREE <a href="http://eepurl.com/c-2Ou9" target="_blank">Olive Tree Genealogy Newsletter</a>. Be the first to know of genealogy events and freebies. Find out when new genealogy databases are put online. Get tips for finding your elusive brick-wall ancestor. <p></p></font>
</font></td></tr></table>
<!-- Search  --> <h4> Spotlight On</h4> <!--3--> <table border="1" bordercolor="#CCB48F" cellpadding="5" summary="" width="250"><tr> <td bgcolor="#CCB48F" width="250"><font color="#000000"><font face="Verdana, Arial, Times New Roman"><div align="center"><b>Your Name in History</b></div></font></font></td> </tr> <tr> <td align="center" bgcolor="ffffff"><script type="text/javascript">function FixKeywords(form) { var keywords = ""; if(form.keywords.value)   keywords = form.keywords.value + " Name in History"; return keywords;}</script><form action="http://www.amazon.com/gp/search" method="GET" onsubmit="this.keywords.value=FixKeywords(this)" target="_blank"><input name="keywords" type="text"/><input name="ie" type="hidden" value="UTF8"/> <input name="tag" type="hidden" value="theolivetreegene"/> <input name="index" type="hidden" value="books"/> <input name="linkCode" type="hidden" value="ur2"/> <input name="camp" type="hidden" value="1789"/> <input type="submit"/> </form> <img alt="" border="0" height="1" src="http://www.assoc-amazon.com/e/ir?t=theolivetreegene&amp;l=ur2&amp;o=1" style="border:none !important; margin:0px !important;" width="1"/><font face="Verdana, Arial, serif" size="1">Find out if your Surname is part of the <b>Our Name in History Collection</b>! Just type your surname into the search box</font></td></tr><tr> <td bgcolor="#CCB48F" width="250"><font color="#000000"><font face="Verdana, Arial, Times New Roman" size="1"><b> </b></font></font></td> </tr></table>
<p>
<font face="Verdana, Arial, Times New Roman"> Check out <a href="http://olivetreegenealogy.com/published.shtml" target="_blank">Olive Tree Genealogy Published Books</a><!-- Search  --></font>
</p><p> </p><h4>Census Online!</h4> <font face="Verdana, Arial, Helvetica, sans-serif">* <a href="http://www.ancestry.com/search/rectype/census/usfedcen/default.aspx" target="_blank">Complete 1790 to 1930     United States Federal Census</a> taken every 10 years    <br/>*
 	  	   
 	  	 
* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=9807">1825 Census of Lower Canada</a><br/>
* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=9808">1842 Census of Canada East</a><br/>
* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=1061">1851 Census of Canada East, Canada West, New Brunswick, and Nova Scotia</a><br/>
* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=1570">1861 Census of Canada</a><br/>




* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=7922">1871  Canada Census, head of house index only</a><br/> 

* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=1578">1871  Canada Census with images</a><br/> 

* <a href="http://www.familysearch.org/Eng/Search/frameset_search.asp?PAGE=census/search_census.asp" target="_blank">1881 Canada Census (no images)</a><br/>

* * <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=1577">1881  Canada Census</a><br/>
<br/>

* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=1274">1891  Canada Census</a><br/>



* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=8826">1901  Canada Census</a><br/>




* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=8827">1906  Canada Census for Manitoba, Saskatchewan, Alberta</a><br/> 



* <a href="https://prf.hn/click/camref:1011l4wyt/destination:https://search.ancestry.com/search/db.aspx?dbid=8947">1911  Canada Census</a>
<br/>* 1841, 1851, 1861, 1871, 1881, 1891, 1901     England, Scotland, Wales, Isle of Man, Channel Islands Census on <a href="https://prf.hn/click/camref:1011l4wyt/creativeref:1101l27800" target="_blank">Ancestry</a> <br/>1850 American    Slave Schedules <br/>    ... <a href="/census-news.shtml">more about updates and additions</a></font>
<hr/> <h4><font face="Arial, Helvetica, sans-serif">Quick Links</font></h4> <img alt="Free genealogy sites from All Census Records" border="0" height="5" src="/graphics/lisquare.gif" width="5"/> <font face="Verdana, Arial, Helvetica, sans-serif"><a href="http://olivetreegenealogy.com/" target="_blank">Olive  Tree Genealogy</a> - Free ships passenger lists, Native American genealogy,   Palatines, Huguenots and much much more</font>
<br/> <img alt="Free genealogy sites from All Census Records" border="0" height="5" src="/graphics/lisquare.gif" width="5"/> <font face="Verdana, Arial, Helvetica, sans-serif"><font color="#333300"><a href="http://naturalizationrecords.com/" target="_blank">NaturalizationRecords.com</a>         - Free naturalization records, passport applications, alien registrations         &amp; voters' registrations</font></font>
<br/> <img alt="Free genealogy sites from All Census Records" border="0" height="5" src="/graphics/lisquare.gif" width="5"/> <font face="Verdana, Arial, Helvetica, sans-serif"><a href="http://ancestorsatrest.com/" target="_blank">AncestorsAtRest.com</a>         - free death records including coffin plates and funeral         cards&gt;</font>
<br/> <img alt="Free genealogy sites from All Census Records" border="0" height="5" src="/graphics/lisquare.gif" width="5"/> <font face="Verdana, Arial, Helvetica, sans-serif"><a href="http://olivetreegenealogy.com/can/" target="_blank">Canada  Search</a></font> <font face="Verdana, Arial, Helvetica, sans-serif">Search for ancestors in   Canadian genealogy records</font> <br/> <br/> <hr/> <!--end Product Watch--></font></p></div>
</td>
<td bgcolor="#ffffff" valign="top" width="76%">
<!--Body Info Here-->
<table border="0" cellspacing="15" width="100%">
<tr>
<td valign="top">
<div align="center"><h3>Wentworth County, Hamilton 1911 census</h3></div>
<h4 align="center">Every Surname Index</h4>
<h5>Wentworth County, Hamilton, District 77, Subdistrict 2 </h5>
37 pages<br/><br/>
1911 Census Transcriber:  Wendy Thompson

<ul>
<li><a href="/canada/ontario/wentworth-hamilton1.shtml">Wentworth County, Hamilton, District 77, Subdistrict 1 </a></li>
<li><a href="/canada/ontario/wentworth-hamilton2.shtml">Wentworth County, Hamilton, District 77, Subdistrict 2 </a></li>
<li> <a href="/canada/ontario/wentworth-hamilton3.shtml">Wentworth County, Hamilton, District 77, Subdistrict 3 </a> </li>
</ul>
<br/><br/><b>Page </b>1:  Nellie, Mac?nal, Bagshaw, Mayer, Morris, Higgins, Hoffman, ?eale, McRoberts, Kanggard, Jeffery, Durris, Hugard, Hansel, Punchau

<br/><br/><b>Page </b>2:  Pierau, Baker, Purcha?, Watkins, Jones, Patterson, Currie, Langur, Godard, Mitts, Webster, Morrison, Hickey, Dodd, Beatty, Mercer

<br/><br/><b>Page </b>3:  Mercer, Hall, Murton, Bass, ?intzel, Ru?h, Page, ?, Le??, Brombridge, Smith, Nettleton, McDonald, Slocum, Mills, Kings?, ?rid

<br/><br/><b>Page </b>4:  Baly, Lee, Willard, Martindale, Harris, Flood, Smity, Davis, Beasley, Curey, Shaw, Burnette, May, ?, Roch?ball, Grant, Hunt, Watson, Dinnis, Cumbell, Marter, Cr?, Cordiner, Bradley

<br/><br/><b>Page </b>5: Web, Holiday, Stephens, Pains, Raynes, Dillon, Barton, Carter, Waterman, ?ayfer, Mitchel, Finlayson, Gardener, Moo???, Willis, Wilson, Maurice

<br/><br/><b>Page </b>6:  Maurice, Smith, Crompton, Dwyer, Fields, ?lley, Logan, Smith, Wilson, Cooke, Smith, ?eagell, Williams, Bass, Love

<br/><br/><b>Page </b>7:  Taylor, Art?, O?, ?art, Smith, Williamson, Construction, Turnbull, Vanerman, McBride, Livingston, crossed out, crossed out, McBride, Construction, Construction, Ford, Lindsey, Buc?an,

<br/><br/><b>Page </b>8:  Kelly, Rodgers, McClement, Morgan, ?ower, Millard, Loa?, M?, ?, L?dy, Graham, Clark, Arnatt, McQue?y, Davis, Cummings, Junor, F?, Ellis, HOlmes, Bettingham, Arvele

<br/><br/><b>Page </b>9:  Turnbull, Lind??y, Young, Hodgins, ?, Skelton, Kett, Hazel, Montray, Ellis, Seneghaugh, Mathe?, M?, Stand, Johnston, Eston

<br/><br/><b>Page </b>10:  Burnes, Latton, F?man, Simpson, Bra?, Webster, Stuart, Marworthy, Mall, Gibson, ?, May, Lunsden, Rigby, Bartman, Jamieson

<br/><br/><b>Page </b>11:  Jameson, Stewart, Muir, Leith, ?lgians, Collin, Johnston, Gayfer, ?fe, Leadley, Martin, Watt, Read

<br/><br/><b>Page </b>12:  Read, Currier, Cherry, Spen?, ?dale, Hunter, Mills, Belmont, Vacant, Howe, Len?, Gray, ? Kent, F?, Ward, Will?, Mair, H?tler, Sharpe, Haase

<br/><br/><b>Page </b>13:  Haas, Watson, ?, ?, Robertson, Campbell, Malony, W?, Frelor, Mullen, Browman, ?, Scott, M?dy, Lam?gar

<br/><br/><b>Page </b>14:  Carse, Facter, C?ment, Messer, Dane, Kent, Ripley, Campbell, Coon, Harris, ?, ?, Ironside, Shaner, ?, B?, Campbell, Ford, Warming?, Small, Marshall

<br/><br/><b>Page </b>15:  Chilton, Burton, Sacllier, Sch?ger; Scott, Heaney, Sutton, Methurd, Brown, Anderson, Megus, ?, Ya?tt, Pilgrim, Henderson, Larder, ?, McFarlane, McPherson, Beer, Park

<br/><br/><b>Page </b>16:  Jarvis, McConnel, Stevenson, Lindley, Watkins, ?, ?, ?, ?, MacKenzie, McLen?, G?row, Bartworth

<br/><br/><b>Page </b>17:  Arkin, Noleton, Loerer, Fraser, Kinsmen, Brown, Coulter, Wilson, Shepherd, Gardener, Walker, Bush, Nichol, Kerr, Hatt?, McKnight, Brown

<br/><br/><b>Page </b>18:  Jan?, Carmeron, Jones, McMurray, Wilks, Donau, Gibson, Shepherd, Bre?ton, Seale, Thompson, Emery, Edwards, March, Claring?, Thompson, Francis, Sanders, Thompson, Simpsons, Brown, Evans

<br/><br/><b>Page </b>19:  Brown, Brubaker, McDonald, Saunders, Green, Kappelle, Smith, Gr?, Hamill, Dawson, Hatt, Young

<br/><br/><b>Page </b>20:  Young, Richards, Webber, Larman, Dods, Leckie, Dallyn, Neal, Edwards, Batte, Smith, Doorath, Gray, Barker, Armstrong, Strong?, Carnes, Hasabrook

<br/><br/><b>Page </b>21:  Manley, Yousent, McMilan, Smith, Cordingly, Pattison, Burns, Kopp, ?, Greason, Tripp, McKelny, Shae, Anderson, Adger, Miller, Lawrie

<br/><br/><b>Page </b>22:  Currie, Y?tman, MacCartney, Walsh, Pearson, Nixon, Sander, Clark, Lyne?, Davis, Stratton, Schultz, Graham, Adams, McD?ds

<br/><br/><b>Page </b>23:  Perina, Conway, Hickey, McGentry, McH?y, ?, Krivinsky, ?, Johnston, Ellis, Guy, Kerr, Peacock, Day, McKay, ?, Swallow

<br/><br/><b>Page </b>24:  G?, Warner, Lang, Heall, Johnson, Nichol, Joice, Mills, Stubbs, Pearce, Crane, Emtrice, Smith, DUggan, Thompson

<br/><br/><b>Page </b>25:  Thompson, Keith, Appl?, Schutz, Armitage, ?, Fenton, Brown, Fitzgerald, McM?ple, Browth?, Dickens

<br/><br/><b>Page </b>26:  Robinson, Towers, Stead, Rolston, Kemper, Bayliss, McMannis, Meyers, Green, Yarfarla? Pactrameter, Hanson, McKenna, Anderson, Sparks, Miller, May, Bradley, May, Bradley

<br/><br/><b>Page </b>27:  Ruse, ?, James, ?back, Lonadale, Rudy, N?, ?, Kirby, Shepherd, Walker, Young, Richardson, Milward

<br/><br/><b>Page </b>28:  Batty, ?line, Leslie, Arth?, Bell, Gillard, Macdonald, Nichol, Patterson, Laudman, Preston, B?rod, Mathews, Brown, Matheson, ?, Thomas, ?, Thomas, Waller

<br/><br/><b>Page </b>29:  Flett, Birde, ?, C?, Street, McFarlanle, ?, Barrett, Patterson, Garland, ?ton, Garland, Houghton, Garland, Patterson, Edmonton, Carrington, Frenak, Phillips, Gray

<br/><br/><b>Page </b>30:  Neale, Wilson, ?, Slattery, Brown, Brown, Cobb, Pickens, Powell, Charles, McKellar, Ellis

<br/><br/><b>Page </b>31:  Hamilton, Newlands, Harte, Derry, Harte, Derry, Roberts, Aspenwood, Reid, Murray, ?, Milan, Crawford, Jackson, Fraser, Hadden

<br/><br/><b>Page </b>32:  Hadden, Mearee, Nugent, Perkins, Miller, ?ders, Earley, Sta?kins, McGowan, Fowerl, Willis, Laws, Wright, Coles, Shaver, Boo?

<br/><br/><b>Page </b>33:  Book, Cole, Johnston, ?ggs, Irwin, Male, Mayer, Lennie, B?thwick, Thompson, Graham, Olds, Fogg, Cocks, Manner

<br/><br/><b>Page </b>34:  Shipton, Wilson, Jones, Laisig, Chalmers, McINture, McDougall, Brown, Narrelees, E?ay, Ryerse, Jamieson, Lang, Hooker, Gibbs, James, Slattery

<br/><br/><b>Page </b>35:  Birkett, Carall, Dalton, Green, Minah?, Nett, Johnston, Downs, ?, Downs, Maynard, Epps, Henshaw, Balfor, Walter, Philips, Berry, ?riandon, Parker, ?ong, ?

<br/><br/><b>Page </b>36:  Kenshaw, Gallath?, P?try, Golding, ?, Cooper, ?, Cole, Ellis, Shaner, Smith, Ballantine, Baker, Keath

<br/><br/><b>Page </b>37:  Gibson, Chisholm, ?rington, Luff, Plummer, Marshall, Millar, Riches, Oaten, Armstrong 






   <p>View <a href="http://www.collectionscanada.ca/archivianet/1911/index-e.html" target="_blank">1911 Census Images</a> online at Library &amp; Archives Canada (only searchable by geographic location) or on  <a href="https://prf.hn/click/camref:1011l4wyt/creativeref:1101l27800" target="_blank">Ancestry</a>  Records include every name  to search every name  indexes and  census images for 1911 Canadian census. 




  
  
		  
      </p></td>
</tr>
</table>
<!--Body Info Ends-->
</td>
</tr></tbody></table>
<div align="center">
<p><br/>
</p>
<table align="center" border="0" cellpadding="5" width="100%"> <tr> <td>
<p align="center"><font face="Verdana, Arial, Helvetica, sans-serif">If           you can't find your ancestor in the records and links on AllCensusRecords.com,           try the <a href="/free-trials.shtml">Free Trials</a> for Subscription           Websites. </font>
</p><hr/> <font face="Verdana, Arial, Helvetica, sans-serif"> </font> <center> <table border="0" cellpadding="5" cellspacing="2" width="100%"> <tr> <td valign="TOP" width="20%"> <div align="center"><font face="Arial, Helvetica, sans-serif">Contact Us<br/> olivetreegenealogy<img alt="" border="0" height="12" src="/graphics/graphic-arial-12px-000000.gif" width="11"/>gmail.com </font></div> </td>
<td height="77" valign="TOP" width="20%"> <div align="center"><font face="Arial, Helvetica, sans-serif">
  © 2004 - present
  
  <br/><a href="/copyright.shtml">Copyright    </a> </font></div> </td>
<td valign="TOP" width="20%"> <div align="center"><font face="Arial, Helvetica, sans-serif"><a href="/links.shtml">Links</a></font> </div></td>
<td valign="TOP" width="20%">
<div align="center"><font face="Arial, Helvetica, sans-serif" size="2"><a href="https://www.olivetreegenealogy.com/privacy.shtml" target="_blank">Privacy Policy</a> </font></div></td>
<td height="77" valign="TOP" width="20%"> <div align="center"><font face="Arial, Helvetica, sans-serif"><a href="/about.shtml">About                  this Site</a></font></div> </td> </tr> </table>
<table align="center" border="0" cellpadding="5" width="100%"> <tr> <td><font size="-2">AllCensusRecords.com is an ongoing Census Records Online Project.  Keep watching these pages for USA Census information for  Alaska Census Records,  Alabama Census Records,  Arizona Census Records,  Arkansas Census Records,  California Census Records,  Colorado Census Records,  Connecticut Census Records,  Delaware Census Records,  District of Columbia Census Records,  Florida Census Records,  Georgia Census Records,  Hawaii Census Records,  Idaho Census Records,  Illinois Census Records,  Indiana Census Records,  Iowa Census Records,  Kansas Census Records,  Kentucky Census Records,  Lousiana Census Records,  Maine Census Records,  Maryland Census Records,  Massachusetts Census Records,  Michigan Census Records,  Minnesota Census Records,  Mississippi Census Records,  Missouri Census Records,  Montana Census Records,  Nebraska Census Records,  Nevada Census Records,  New Hampshire Census Records,  New Jersey Census Records,  New Mexico Census Records,  New York Census Records,  North Carolina Census Records,  North Dakota Census Records,  Ohio Census Records,  Oklahoma Census Records,  Oregon Census Records,  Pennsylvania Census Records,  Rhode Island Census Records,  South Carolina Census Records,  South Dakota Census Records,  Tennessee Census Records,  Texas Census Records,  Utah Census Records,  Vermont Census Records,  Virginia Census Records,  Washington Census Records,  West Virginia Census Records,  Wisconsin Census Records,  Wyoming Census Records, and in Canada - Alberta Census Records, British Columbia Census Records, Manitoba Census Records,  Manitoba Census Records, New Brunswick Census Records, New France (Quebec) Census Records, Newfoundland Census Records,  Nova Scotia Census Records, Ontario Census Records, Prince Edward Island Census Records, Quebec Census Records, Saskatchewan Census Records</font> </td> </tr> </table>
</center>
</td>
</tr>
</table>
</div></body>
</html>