<!DOCTYPE HTML>
<html class="uk-height-1-1 tm-error" dir="ltr" lang="es-es">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>404 - No se encontró esta página.</title>
<link href="/templates/yoo_avanti/apple_touch_icon.png" rel="apple-touch-icon-precomposed"/>
<link href="/templates/yoo_avanti/css/theme.css" rel="stylesheet"/>
</head>
<body class="uk-height-1-1 uk-vertical-align uk-text-center">
<div class="uk-vertical-align-middle uk-container-center">
<i class="tm-error-icon uk-icon-frown-o"></i>
<h1 class="tm-error-headline">404</h1>
<h2 class="uk-h3 uk-text-muted">No se encontró esta página.</h2>
<p>La página que buscas no existe o ha ocurrido un error inesperado.<br class="uk-hidden-small"/> <a href="javascript:history.go(-1)">Vuelve atrás</a>, o dirígete a <a href="https://www.bandasdecaucho.com/">Bandas de Caucho Bogota Colombia | Bandiflex Ltda</a> para ir a otra dirección.</p>
</div>
</body>
</html>