<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta charset="utf-8"/>
<title>BIG BANG EDGE TEST | For Student of Class V to XI</title>
<meta content="Give you preparation a head start for JEE Advanced, JEE Main, Boards, KVPY, BITSAT, Olympiads, NTSE through Big Bang Edge Test. Big bang Edge Test exam date is Sunday 18th October 2020 " name="description"/>
<meta content="big bang edge test 2020, Big Bang Exam Date, FIITJEE Big Bang Edge, JEE Main, JEE Advanced, NTSE, Olympiad, KVPY, FIITJEE Sample Paper, Best IIT Coaching, top Engineering Coaching " name="keyword"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<!-- Favicon -->
<link href="img/icon.png" rel="apple-touch-icon"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/revoulation.css" rel="stylesheet"/>
<link href="css/plugins.min.css" rel="stylesheet"/>
<link href="style.min.css" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=Raleway:400,300,100,200,500,600,800,700,900" rel="stylesheet" type="text/css"/>
<!-- Raleway font -->
<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900&amp;subset=cyrillic,cyrillic-ext,latin,greek-ext,greek,latin-ext,vietnamese" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900&amp;subset=cyrillic,cyrillic-ext,latin,greek-ext,greek,latin-ext,vietnamese" rel="stylesheet" type="text/css"/>
<!-- Roboto -->
<!-- Font icons -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" id="bootstrap-css" rel="stylesheet"/>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
    .rev_slider li.tp-revslider-slidesli
    {
        position: absolute !important;
        height: 590px !important;
    }
    .rev_slider_wrapper
    {
        position: relative;
        z-index: 0;
        width: 100%;
        height: 590px !important;
    }
    .single-motive-speach.text-white .content h4
    {
        color: #000;
    }
    
    .single-motive-speach.text-white .content p
    {
        color: #000;
    }
    
    .p.bk_pra
    {
        margin-bottom: 0;
        word-wrap: break-word;
        max-width: 100%;
        font-weight: 500;
        line-height: 24px !important;
        font-size: 13px !important;
        font-family: Poppins,sans-serif;
        margin-bottom: 15px !important;
        text-align: justify !important;
    }
    
    #footer-wrapper
    {
        position: relative;
        background-color: #252525;
        width: 100%;
        padding-top: 80px;
    }
    
    .footer-widget-container li.widget:last-child
    {
        margin-bottom: 0px;
    }
    
    .widget
    {
        list-style: none;
        display: block;
        margin-bottom: 45px;
        width: 100%;
    }
    
    #footer-wrapper
    {
        position: relative;
        background-color: #252525;
        width: 100%;
        padding-top: 80px;
    }
    #footer-wrapper .col-md-12 .subtitle h3
    {
        padding: 0 15%;
    }
    #footer h2
    {
        color: #ffffff;
    }
    .footer-widget-container li.widget:last-child
    {
        margin-bottom: 0px;
    }
    .footer-widget-container .widget .title h3
    {
        color: #fff;
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 20px;
    }
    .footer-widget-container .widget p, .footer-widget-container .widget a
    {
        color: #A0A0A0;
        font-size: 14px;
    }
    .footer-widget-container .widget span
    {
        font-size: 14px;
    }
    #copyright-container
    {
        position: relative;
        background-color: #ffffff;
        padding: 25px 0;
    }
    #copyright-container .row
    {
        margin-bottom: 0;
    }
    #copyright-container p
    {
        color: #ccc;
        padding-bottom: 0;
        padding-top: 11px;
        font-size: 12px;
        line-height: 12px;
    }
    #copyright-container a
    {
        font-weight: 500;
        line-height: 12px;
    }
    #copyright-container .breadcrumb
    {
        float: right;
        padding: 0;
        margin: 0;
        background: none;
    }
    #copyright-container .breadcrumb li
    {
        float: left;
        list-style: none;
        text-transform: uppercase;
        padding: 9px 12px;
    }
    #copyright-container .breadcrumb li a
    {
        font-size: 12px;
        color: #959595;
    }
    
    #copyright-container img
    {
        width: auto;
        display: inline-block;
        margin-right: 10px;
    }
    .footer-widget-container .widget .social-links
    {
        border-top: 1px solid #3C3C3C;
        padding-top: 20px;
        margin-top: 20px;
    }
    .footer-widget-container .widget .social-links li
    {
        float: left;
        list-style: none;
        background: none;
        border-bottom: none;
        padding: 7px 14px 7px 0;
    }
    .footer-widget-container .widget .social-links a
    {
        font-size: 18px;
    }
    
    
    /* ==========================================================================
    15.1. FOOTER STYLE 2
========================================================================= */
    
    .footer-style-2 .social-links li
    {
        display: inline-block;
        padding: 7px 8px 7px 8px;
    }
    .footer-style-2 .social-links li a
    {
        font-size: 38px;
        color: #656565;
    }
    #copyright-container.footer-style-2
    {
        background-color: #1f1f1f;
        padding: 80px 0;
    }
    #copyright-container.footer-style-2 p
    {
        padding-top: 0;
        color: #656565;
        line-height: 18px;
    }
    #footer-wrapper.footer-style-2
    {
        padding-top: 140px;
        padding-bottom: 100px;
    }
    #footer-wrapper.footer-style-2 .row
    {
        margin-bottom: 0;
    }
    
    
    /*  =========================================================================
    SCROLL UP
    ========================================================================= */
    
    .scroll-up
    {
        width: 30px;
        height: 57px;
        position: absolute;
        top: -18px;
        right: 100px;
        font-size: 10px !important;
        text-align: center;
        line-height: 15px !important;
        padding-top: 22px !important;
        color: #fff !important;
        font-weight: 400 !important;
        display: none;
        background-image: url('../img/to-top.png');
        background-repeat: no-repeat;
        background-color: #A3C93A;
        z-index: 100;
        background-position: center 12px;
    }
    .scroll-up:before
    {
        border-left: transparent solid 16px;
        border-top: solid transparent 18px;
        border-right-style: solid;
        border-right-width: 16px;
        bottom: 0;
        top: 0;
        content: " ";
        height: 0;
        left: -32px;
        position: absolute;
    }
    .scroll-up:after
    {
        border-left: solid transparent 16px;
        border-right: transparent solid 14px;
        border-bottom: solid #ffffff 15px;
        bottom: 0;
        content: " ";
        height: 0;
        left: 0;
        position: absolute;
    }
    .scroll-up:hover
    {
        height: 70px;
        -webkit-transition: all 0.2s ease 0s;
        -moz-transition: all 0.2s ease 0s;
        -o-transition: all 0.2s ease 0s;
        -ms-transition: all 0.2s ease 0s;
        transition: all 0.2s ease 0s;
    }
    .footer-style-2 .scroll-up
    {
        left: 0;
        right: 0;
        margin: 0 auto;
    }
    .footer-style-2 .scroll-up:after
    {
        border-bottom: solid #252525 15px;
    }
    
    .footer-widget-container .contact-info-list li
    {
        color: #bbb;
        padding-bottom: 20px;
        margin-bottom: 20px;
    }
    .footer-widget-container .widget li
    {
        border-bottom: 1px solid #3C3C3C;
        padding-left: 15px;
        padding-bottom: 5px;
        padding-top: 0;
        line-height: 25px;
        list-style: none;
        margin-bottom: 15px !important;
    }
    
    .contact-info-list li
    {
        width: 100%;
        float: left;
        padding-bottom: 30px;
        list-style: none;
        line-height: 20px !important;
        padding-left: 25px !important;
        margin: 0 !important;
    }
    
    .mega__list li
    {
        list-style: none;
        padding: 0;
        margin: 0px;
    }
    
    .mega__list li a
    {
        line-height: 63px;
        background: #f00;
        padding: 10px;
        margin-bottom: 29px !important;
        color: #fff;
    }
    
    .mega__list li:hover a
    {
        color: #000;
    }
    .wrapper3 .menu-toggle33
    {
        color: #fff;
        position: fixed;
        border-right: none !important;
        font-size: 20px;
        top: 275px;
        right: 0;
        font-size: 23px;
        z-index: 5555;
        opacity: 1;
        border: 3px solid #000;
        border-radius: 9px 0 0 9px;
    }
    
    .brook__toolbar .inner a
    {
        vertical-align: baseline;
        text-align: center;
        display: block;
        width: 40px;
        height: 40px;
        line-height: 40px;
        color: #fff;
        -webkit-transition: all .3s cubic-bezier(.645,.045,.355,1);
        -o-transition: all .3s cubic-bezier(.645,.045,.355,1);
        transition: all .3s cubic-bezier(.645,.045,.355,1);
        padding-top: 13px;
    }
    
    .vertical-fullscreen-header.headroom--sticky.headroom--not-top
    {
        background: 0 0;
        background-color: #0f0d1b;
    }
    
    
    /* Carousel Styles */
    .carousel-indicators .active
    {
        background-color: #2980b9;
    }
    
    .carousel-inner img
    {
        width: 100%;
    }
    
    .carousel-control
    {
        width: 0;
    }
    
    .carousel-control.left, .carousel-control.right
    {
        opacity: 1;
        filter: alpha(opacity=100);
        background-image: none;
        background-repeat: no-repeat;
        text-shadow: none;
    }
    
    .carousel-control.left span
    {
        padding: 15px;
    }
    
    .carousel-control.right span
    {
        padding: 15px;
    }
    
    .carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-prev, .carousel-control .icon-next
    {
        position: absolute;
        top: 45%;
        z-index: 5;
        display: inline-block;
    }
    
    .carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev
    {
        left: 0;
    }
    
    .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next
    {
        right: 0;
    }
    
    .carousel-control.left span, .carousel-control.right span
    {
        background-color: #000;
    }
    
    .carousel-control.left span:hover, .carousel-control.right span:hover
    {
        opacity: .7;
        filter: alpha(opacity=70);
    }
    
    .carousel-indicators li
    {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 1px;
        text-indent: -999px;
        cursor: pointer;
        background-color: rgb(29 29 109) !important;
        border: 1px solid #000 !important;
        border-radius: 10px;
    }
</style>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!-- Quora Pixel Code (JS Helper) -->
<script>
    !function (q, e, v, n, t, s) { if (q.qp) return; n = q.qp = function () { n.qp ? n.qp.apply(n, arguments) : n.queue.push(arguments); }; n.queue = []; t = document.createElement(e); t.async = !0; t.src = v; s = document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s); } (window, 'script', 'https://a.quora.com/qevents.js');
    qp('init', 'c85bdd0c2bf74b10af4d159489073bd7');
    qp('track', 'ViewContent');
</script>
<noscript>
<img height="1" src="https://q.quora.com/_/ad/c85bdd0c2bf74b10af4d159489073bd7/pixel?tag=ViewContent&amp;noscript=1" style="display: none" width="1"/></noscript>
<!-- End of Quora Pixel Code -->
<script>    qp('track', 'Generic');</script>
<!-- Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-145606980-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-145606980-1');
</script>
<!-- Facebook Pixel Code -->
<script>

    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return; n = f.fbq = function () {
            n.callMethod ?

n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };

        if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';

        n.queue = []; t = b.createElement(e); t.async = !0;

        t.src = v; s = b.getElementsByTagName(e)[0];

        s.parentNode.insertBefore(t, s)
    } (window, document, 'script',

'https://connect.facebook.net/en_US/fbevents.js');


    fbq('init', '583089091802718');

    fbq('track', 'PageView');

</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=583089091802718&amp;ev=PageView

&amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="template-color-1 template-font-1">
<div class="wrapper3">
<a class="menu-toggle33 mobicon" href="#Enquiry">
<img alt="" class="img-responsive dsten" data-pin-nopin="true" src="img/enquirnow.png"/>
</a>
</div>
<!-- Wrapper -->
<div class="wrapper" id="wrapper">
<!-- Header -->
<header class="br_header vertical-fullscreen-header header-default header-transparent header-bar position-from--top light-logo--version haeder-fixed-width haeder-fixed-150 headroom--sticky headroom headroom--top headroom--bottom header-mega-menu">
<div class="container-fluid">
<div class="row">
<div class="col-12">
<div class="header__wrapper">
<!-- Header Left -->
<div class="header-left">
<div class="logo">
<a href="https://www.fiitjee.com/" target="_blank">
<img alt="Brook Images" class="black" src="images/logo-2.png" style="width: 105px;"/>
<img alt="Brook Images" class="light" src="images/logo-2.png" style="width: 105px;"/>
</a>
</div>
</div>
<div class="col-md-4" style="text-align:right;">
<a href="samplepaper.php" style="line-height:32px; color:#fff; border-radius:10px;border:1px solid #fff; margin-right:15px;    padding: 10px;">Sample Paper</a>
</div>
<!-- Mainmenu Wrap -->
<!-- Header Right -->
</div>
</div>
</div>
</div>
</header>
<!--// Header -->
<!-- Start Popup Menu -->
<div class="popup-mobile-manu popup-mobile-visiable">
<div class="inner">
<div class="mobileheader">
<div class="logo">
<a href="https://www.fiitjee.com/" target="_blank">
<img alt="FIITJEE" src="images/logo-2.png" style="width: 105px;"/>
</a>
</div>
<a class="mobile-close" href="#"></a>
</div>
</div>
</div>
<!-- End Popup Menu -->
<!-- Start Toolbar -->
<div class="demo-option-container">
<!-- Start Toolbar -->
<div class="brook__toolbar">
<div class="inner">
<a class="quick-option " href="#"><i class="fa fa-bars"></i></a>
</div>
</div>
<!-- End Toolbar -->
<div class="demo-option-wrapper">
<ul class="mega__list">
<li><a href="Default.aspx" target="_blank"><span>Home</span></a></li>
<li><a href="https://fiitjee.com/AboutUs" target="_blank"><span>About Us</span></a></li>
<li><a href="https://www.fiitjee.com/Programs" target="_blank"><span>Programs</span></a></li>
<li><a href="https://fiitjee.com/Success-Trail" target="_blank"><span>Results</span></a></li>
<li><a href="https://fiitjee.com/Centres" target="_blank"><span>Our Centres</span></a></li>
<li><a href="https://fiitjee-admissions.mypat.in/" target="_blank"><span>MOCK TEST PAPERS</span></a></li>
</ul>
</div>
</div>
</div>
<!-- End Toolbar -->
<!-- Page Conttent -->
<main class="page-content">
<div class="slider-revoluation" style="height: 590px;
              ">
<!-- START REVOLUTION SLIDER -->
<div class="rev_slider_wrapper fullscreen-container" data-alias="astronomy" data-source="gallery" id="rev_slider_19_1_wrapper" style="background:#000000;padding:0px;">
<!-- START REVOLUTION SLIDER 5.4.7 fullscreen mode -->
<div class="rev_slider fullscreenbanner" data-version="5.4.7" id="rev_slider_19_1" style="display:none;">
<ul>
<!-- SLIDE  -->
<!-- SLIDE  -->
<li data-description="" data-easein="Power2.easeInOut" data-easeout="Power2.easeInOut" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-29" data-masterspeed="3500" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="img/revoulation/180x40_astronomy_02.jpg" data-title="Slide" data-transition="fade">
<!-- MAIN IMAGE -->
<img alt="" class="rev-slidebg" data-bgparallax="off" data-bgposition="center center" data-blurend="0" data-blurstart="0" data-duration="30000" data-ease="Power1.easeOut" data-kenburns="on" data-lazyload="img/revoulation/astronomy_02.jpg" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="-2" data-rotatestart="2" data-scaleend="150" data-scalestart="100" src="img/revoulation/dummy.png"/>
<!-- LAYERS -->
<!-- LAYER NR. 4 --><div class="container-fluid" style="padding:0px;">
<div class="container" style="padding:0px;">
<div class="col-md-12" style="padding:0px;">
<div class="col-md-12" style="padding:0px;">
<img src="img/logo-bbe.png" style="    width:56%;
    left: 22%;
    position: relative;
    top: 20px;"/>
</div>
<div class="col-md-12" style="padding:0px;">
<h2 style="
    color: #fff;
    text-align: center;
    margin-top: -114px;
    font-size: 36px;
">Sunday, 1st November 2020</h2>
</div>
<div class="col-md-12" style="padding:0px;    margin-top: -59px;">
<h6 style="width: 100%;
    color: #fff;
    font-size: 18px;
    line-height: 32px;
    left: -11px;
    text-align: center;
}">
<span style="font-size: 13px;">For Students Presently in</span> <b style="color:yellow; font-size: 21px;">Class V, VI, VII, VIII, IX, X &amp; XI</b>
<span style="font-size: 13px;">(Going to Class VI, VII, VIII, IX, X, XI &amp; XII in 2021)</span>
</h6>
</div>
<div style=" font-size: 18px; color: Yellow; text-align: center;    margin-top: 0px;">
<h6 style="width: 100%;
    color: yellow;
    font-size: 18px;
    line-height: 32px;
    
    text-align: center;
}">
                                       Untap your Veiled Potential
and Discover the Befitting Opportunity !</h6>
</div>
</div>
</div>
</div>
<!-- LAYER NR. 1 -->
</li>
<li data-description="" data-easein="Power2.easeInOut" data-easeout="Power2.easeInOut" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-29" data-masterspeed="3500" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="img/revoulation/180x40_astronomy_02.jpg" data-title="Slide" data-transition="fade">
<!-- MAIN IMAGE -->
<img alt="" class="rev-slidebg" data-bgparallax="off" data-bgposition="center center" data-blurend="0" data-blurstart="0" data-duration="30000" data-ease="Power1.easeOut" data-kenburns="on" data-lazyload="img/revoulation/astronomy_02.jpg" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="-2" data-rotatestart="2" data-scaleend="150" data-scalestart="100" src="img/revoulation/dummy.png"/>
<!-- LAYERS -->
<!-- LAYER NR. 4 --><div class="container-fluid" style="padding:0px;">
<div class="container" style="padding:0px;">
<div class="col-md-12" style="padding:0px;">
<div class="col-md-12" style="padding:0px;">
<img src="img/logo-bbe.png" style="    width:56%;
    left: 22%;
    position: relative;
    top: 20px;"/>
</div>
<div class="col-md-12" style="padding:0px;">
<h2 style="
    color: #fff;
    text-align: center;
    margin-top: -114px;
    font-size: 36px;
">Sunday, 18th October 2020</h2>
</div>
<div class="col-md-12" style="padding:0px;    margin-top: -59px;">
<h6 style="width: 100%;
    color: #fff;
    font-size: 18px;
    line-height: 32px;
    left: -11px;
    text-align: center;
}">
<span style="font-size: 13px;">For Students Presently in</span> <b style="color:yellow; font-size: 21px;">Class V, VI, VII, VIII, IX, X &amp; XI</b>
<span style="font-size: 13px;">(Going to Class VI, VII, VIII, IX, X, XI &amp; XII in 2021)</span>
</h6>
</div>
<div style=" font-size: 18px; color: Yellow; text-align: center;    margin-top: 0px;">
<h6 style="width: 100%;
    color: yellow;
    font-size: 18px;
    line-height: 32px;
    
    text-align: center;
}">
                                       Untap your Veiled Potential
and Discover the Befitting Opportunity !</h6>
</div>
</div>
</div>
</div>
<!-- LAYER NR. 1 -->
</li>
<!-- SLIDE  -->
<!-- SLIDE  -->
<!-- SLIDE  -->
</ul>
</div>
</div>
<!-- END REVOLUTION SLIDER -->
</div>
</main>
<div class="container-fluid" id="sample" style="margin: 20px 0;">
<div class="container" padding-bottom:="" style="background:">
<div class="col-md-2"></div>
<div class="col-md-8">
<table border="0" style="color: #000; font-weight: normal;margin-top: 29px;" width="100%">
<tbody>
<tr>
<td style="text-align: center;" width="35%">
<strong style="font-size: 31px; line-height: 41px; font-weight: bold;
                                    color: #f00; text-align: center;">Result Declaration Dates</strong>
</td>
</tr>
</tbody>
</table>
<table style=" font-family:calibri; font-size:18px; border: 0px; box-shadow: 0px 0px 0px 0px;color:#000;" width="78%">
<tbody>
<tr style="background:#eae6e4; padding:0px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px sol id #fff" width="30%">
<strong>Class XI</strong>
</td>
<td width="10%">:</td>
<td style="">7th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr style="background:#eae6e4; padding:20px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px soli d #fff">
<strong>Class X</strong>
</td>
<td>:</td>
<td style="">8th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr style="background:#eae6e4; padding:20px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px sol id #fff">
<strong>Class IX</strong>
</td>
<td>:</td>
<td style="">9th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr style="background:#eae6e4; padding:20px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px sol id #fff">
<strong>Class VIII</strong>
</td>
<td>:</td>
<td style="">10th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr style="background:#eae6e4; padding:20px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px sol id #fff">
<strong>Class VII  </strong>
</td>
<td>:</td>
<td style="">11th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr style="background:#eae6e4; padding:20px;border-bottom:2px solid #fff">
<td style="padding-left:10px;border-right:2px sol id #fff">
<strong>Class V &amp; VI  </strong>
</td>
<td>:</td>
<td style="">12th November 2020    <a href="http://testresults.fiitjee.com/" style="text-decoration: none;" target="_blank"> <b style="color:#f00">Click here...</b></a></td>
</tr>
<tr>
<td colspan="3" style="background: #eaeaea;padding: 10px; padding-bottom: 0px; text-align: center;">
<!--Results will be announced at 4:00 pm -->
</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-2"></div>
<div class="clearfix"></div>
</div>
</div>
<div class="container-fluid" id="sample" style="margin: 20px 0;">
<div class="container" padding-bottom:="" style="background:">
<div class="col-md-12" style="text-align: right;">
<a href="default.aspx" style="text-align: right;; color: #000; font-size: 21px;">For visit BIGBANG EDGE website, <b style="color:#f00;">Click here...</b></a>
</div>
</div>
</div>
<!--// Page Conttent -->
<div id="footer-wrapper">
<!-- #footer start -->
<footer id="footer">
<!-- .container start -->
<div class="container">
<!-- .row start -->
<div class="row">
<!-- .footer-widget-container start -->
<ul class="col-md-4 col-sm-6 footer-widget-container">
<!-- .widget.widget_text -->
<li class="widget widget_text">
<div class="title">
<h3>About FIITJEE</h3>
</div>
<p>
                                    FIITJEE was created in 1992 by the vision and toil of Mr. D. K. Goel, a Mechanical Engineering Graduate from IIT Delhi. We had a very humble beginning as a forum for IIT-JEE, with a vision to provide an ideal launch pad for serious JEE aspirants.
                                </p>
<a href="https://fiitjee.com/AboutUs" target="_blank"><span>Read more...</span></a>
<ul class="social-links">
<li><a href="https://www.facebook.com/fiitjeeindia123" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
<li><a href="https://twitter.com/fiitjee" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="https://www.youtube.com/user/fiitjeeltd?sub_confirmation=1" target="_blank"><i class="fa fa-youtube"></i></a></li>
<li><a href="http://www.linkedin.com/company/fiitjee" target="_blank"><i class="fa fa-linkedin"></i></a></li>
</ul>
</li><!-- .widget end -->
</ul><!-- .col-md-3.footer-widget-container end -->
<!-- .footer-widget-container start -->
<!-- .footer-widget-container start -->
<div class="col-md-2"></div>
<!-- .footer-widget-container start -->
<ul class="col-md-6 col-sm-6 footer-widget-container contact-info-widget-bkg">
<!-- .widget.contact-info start -->
<li class="widget contact-info">
<div class="title">
<h3>National Admissions Office</h3>
</div>
<ul class="contact-info-list">
<li>
<i class="fa fa-map-marker"></i>
                                        FIITJEE House, 29-A, Kalu Sarai, Sarvapriya Vihar,<br/>    New Delhi -110016
                                    </li>
<i class="fa fa-mobile"></i>
                                        011-49283471 / 73
                                        
                                    </ul></li>--%&gt;

                                    <li>
<i class="fa fa-paper-plane"></i>
<a href="mailto:info@fiitjee.com"><span>info@fiitjee.com</span></a>
</li>
</ul>
<!-- .widget.contact-info end -->
<!-- .footer-widget-container end -->
</div><!-- .row end -->
</div><!-- .container end -->
</footer>
<!-- #footer end -->
</div>
<script src="js/vendor/vendor.min.js" type="text/javascript"></script>
<script src="js/plugins.min.js" type="text/javascript"></script>
<!-- REVOLUTION JS FILES -->
<script src="js/revolution.tools.min.js" type="text/javascript"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
<script src="js/revolution.extension.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
<script src="js/revoulation.js" type="text/javascript"></script>
<script>
        $(document).ready(function () {
            // Activate Carousel
            $("#myCarousel").carousel();

            // Enable Carousel Indicators
            $(".item1").click(function () {
                $("#myCarousel").carousel(0);
            });
            $(".item2").click(function () {
                $("#myCarousel").carousel(1);
            });
            $(".item3").click(function () {
                $("#myCarousel").carousel(2);
            });
            $(".item4").click(function () {
                $("#myCarousel").carousel(3);
            });

            // Enable Carousel Controls
            $(".left").click(function () {
                $("#myCarousel").carousel("prev");
            });
            $(".right").click(function () {
                $("#myCarousel").carousel("next");
            });
        });
    </script>
<script>
        
    $(document).ready(function() {
  //Set the carousel options
  $('#quote-carousel').carousel({
    pause: true,
    interval: 1000,
  });
});
    </script>
</body>
</html>
