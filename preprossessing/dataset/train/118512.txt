<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Amorous</title>
<link href="https://cdn.amorousgame.com/amorous/favicon.ico" rel="shortcut icon"/>
<link href="https://cdn.amorousgame.com/amorous/logo_200_200.png" rel="apple-touch-icon"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
<link href="css/default.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
<script src="js/main.js?v=1" type="text/javascript"></script>
<script>
			(function (i, s, o, g, r, a, m) {
				i["GoogleAnalyticsObject"] = r; i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date(); a = s.createElement(o),
				m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
			})(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");

			ga("create", "UA-20592594-10", "auto");
			ga("send", "pageview");
		</script>
</head>
<body>
<div class="outer-wrapper">
<div class="clouds layer-1"></div>
<div class="clouds layer-2"></div>
<div class="wrapper">
<header>
<img alt="Amorous Logo" src="https://cdn.amorousgame.com/amorous/logo_400.png"/>
</header>
<navigation>
<ul>
<li>
<a class="button start">
<span>Download for</span>
<span class="sprite windows"></span>
<span class="sprite linux"></span>
<span class="sprite mac"></span>
<span>on</span>
</a>
</li>
<li>
<a class="middle button hover" href="https://store.steampowered.com/app/778700/Amorous/" rel="noopener nofollow" target="_blank">
<span class="sprite steam"></span>
<span>Steam</span>
</a>
</li>
<li>
<a class="end button hover" href="https://teamamorous.itch.io/" rel="noopener nofollow" target="_blank">
<span class="sprite itch"></span>
<span>Itch.io</span>
</a>
</li>
</ul>
<ul>
<li>
<a class="button hover" href="https://forums.amorousgame.com/" rel="noopener nofollow" target="_blank">
<span>Visit our Forums</span>
</a>
</li>
</ul>
<div class="clear"></div>
</navigation>
<section class="right">
<div class="image sprite skye"></div>
<div class="content">
<h3>About Amorous</h3>
<p>
							Thrust yourself to the front of the line of <b>Amorous</b>, a bustling furry night club that invites you to explore it's neon lights and eccentric characters. Immerse yourself with a fleshed out character creator and treat yourself to a varied cast of potential dates to explore!
						</p>
</div>
</section>
<section class="left">
<div class="image sprite dustin"></div>
<div class="content">
<h3>What makes Amorous special?</h3>
<p>
							Amorous is a furry dating simulator which aims to provide a high quality graphic novel experience for free. Shake up the formula with animated NPCs, sandbox environments, real choices and interactive mini-games to keep you engaged. Avoid the normal pitfalls of dating games with an incredibly varied roster. Ranging from foxes to felines, male to female and everything in-between.
						</p>
</div>
</section>
<section class="right">
<div class="image sprite seth"></div>
<div class="content">
<h3>So what's in it for me?</h3>
<ul>
<li>9 fully datable characters</li>
<li>Thousands of lines of dialogue with branching trees for replayability</li>
<li>Several mini-games and novelties</li>
<li>Fursona Maker / Character Creator with content for all gender, body-type and personality preferences.</li>
</ul>
</div>
</section>
<section class="left">
<div class="image sprite phone"></div>
<div class="content">
<h3>How can this be free? There must be a catch!</h3>
<p>
							No catch! No depleting in-game currency every time you attempt to date a character, no in-game shop prompting you to buy more parts for your character. It's all there for you to enjoy for free.
						</p>
<p>
							This game has seen many years of spare-time development, and all of it wouldn't have been possible without the support of our loyal Patrons. A free release is our way of thanking them and making sure the game will always remain available for everyone to enjoy.
						</p>
</div>
</section>
<footer>
<p>Copyright © Team Amorous<br/>2014 - 2018</p>
</footer>
</div>
</div>
<div class="modal background">
<section class="window left">
<p class="content"></p>
<p><a class="close" href="#">[Close]</a>
</p></section>
</div>
<iframe height="0" id="downloadFrame" width="0"></iframe>
</body>
</html>
