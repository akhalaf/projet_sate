<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>DICOVAL | Comercializadora de artÃ­culos industriales</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- styles -->
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/bootstrap-responsive.css" rel="stylesheet"/>
<link href="font/stylesheet.css" rel="stylesheet"/>
<link href="css/prettyPhoto.css" rel="stylesheet"/>
<link href="css/jcarousel.css" rel="stylesheet"/>
<link href="css/flexslider.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<!-- Theme skin -->
<link href="color/default.css" rel="stylesheet"/>
<!-- boxed bg -->
<link href="bodybg/bg1.css" id="bodybg" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Fav and touch icons -->
<link href="ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="ico/favicon.png" rel="shortcut icon"/>
</head>
<body>
<div id="wrapper">
<header>
<div id="top-header">
<a href="index.html"><img alt="" class="logo" src="img/dicoval-comercializadora-articulos-industriales.png"/></a> </div>
<div class="navbar navbar-inverse navbar-static-top">
<div class="navbar-inner">
<div class="container">
<div class="navigation">
<nav>
<ul class="nav">
<li class="active dropdown">
<a href="index.html"><i class="icon-home"></i> Home </a> </li>
<li class="dropdown">
<a href="empresa.html"><i class="icon-tasks"></i> Empresa </a> </li>
<li class="dropdown">
<a href="productos.html"><i class="icon-suitcase"></i> Productos </a>
</li>
<li class="dropdown">
<a href="contacto.html"><i class="icon-envelope-alt"></i> Contacto </a>
</li>
</ul>
</nav>
</div><!-- end navigation -->
<div class="contact_info">
<p>T:<a href="tel:+56229046759"> 22 904 6759</a> /<a href="tel:+56229046760"> 22 904 6760</a> C: <a href="mailto:dicoval.ltda@gmail.com" target="_blank">dicoval.ltda@gmail.com</a></p>
</div>
</div>
</div>
</div>
</header>
<section id="featured">
<div class="topline-shadow"></div>
<div class="container">
<div class="row">
<div class="span12">
<!-- start slider -->
<!-- Slider -->
<div id="nivo-slider">
<div class="nivo-slider">
<!-- Slide #1 image -->
<img alt="" src="img/slides/nivo/img4.jpg" title="#caption-1"/>
<!-- Slide #2 image -->
<img alt="" src="img/slides/nivo/img1.jpg" title="#caption-2"/>
<!-- Slide #3 image -->
<img alt="" src="img/slides/nivo/img2.jpg" title="#caption-2"/>
<!-- Slide #4 image -->
<img alt="" src="img/slides/nivo/img3.jpg" title="#caption-2"/>
</div>
<!-- Slide #2 caption -->
<div class="nivo-caption" id="caption-1">
<div>
<h2><strong>Aire acondicionado</strong>Bombas de calor</h2>
<p>Soluciones de climatizaciÃ³n y control climÃ¡tico, desde casas a grandes espacios.</p>
<a class="btn btn-theme" href="productos.html">Ver productos</a>
</div>
</div>
<!-- Slide #3 caption -->
<div class="nivo-caption" id="caption-2">
<div>
<h2><strong>ArtÃ­culos</strong>Industriales</h2>
<p>Para procesos de soldaduras, instalaciÃ³n y mantenciÃ³n de equipos para aire acondicionado, prensado hidrÃ¡ulico, fitting, aislaciÃ³n e insumos asociados.</p>
<a class="btn btn-theme" href="productos.html">Ver productos</a>
</div>
</div>
<!-- Slide #4 caption -->
<div class="nivo-caption" id="caption-3">
<div>
<h2><strong>ArtÃ­culos</strong>Industriales</h2>
<p>Para procesos de soldaduras, instalaciÃ³n y mantenciÃ³n de equipos para aire acondicionado, prensado hidrÃ¡ulico, fitting, aislaciÃ³n e insumos asociados.</p>
<a class="btn btn-theme" href="productos.html">Ver productos</a>
</div>
</div>
</div>
<!-- end slider -->
</div>
</div>
</div>
</section>
<section id="intro-box">
<div class="container">
<div class="row">
<div class="span4">
<div class="box-left">
<h4><img height="64" src="img/inicio_1.png" width="84"/>DICOVAL LTDA</h4>
<p>
						Distribuidora  e importadora comercial, contribuyendo con la gestiÃ³n operacional de las empresas pÃºblicas y privadas.
					    <a href="productos.html">ver mÃ¡s...</a></p>
</div>
</div>
<div class="span4">
<div class="box-center">
<h4><img height="64" src="img/inicio_2.png" width="84"/>Herramientas y accesorios</h4>
<p>
						Contamos con una amplia variedad de productos y artÃ­culos para el suministro de materiales, para grandes y medianas empresas. <a href="productos.html">ver mÃ¡s...</a> </p>
</div>
</div>
<div class="span4">
<div class="box-right">
<h4><img height="64" src="img/inicio_3.png" width="84"/>Mayor informaciÃ³n</h4>
<p>
						Para mayor informaciÃ³n de productos pueden contactarce a nuestro correo <a href="mailto:dicoval.ltda@gmail.com">dicoval.ltda@gmail.com</a> o a nuesto telÃ©fono <a href="tel.:02 2808 6647">02 2808 6647</a>.						</p>
</div>
</div>
</div>
</div>
</section>
<section id="bottom"></section>
<footer>
<div class="container">
<div class="row">
<div class="span3">
<div class="widget">
<h5 class="widgetheading"><i class="icon-folder-open-alt"></i><span></span> MenÃº</h5>
<ul class="link-list">
<li><a href="index.html">Inicio</a></li>
<li><a href="empresa.html">Empresa</a></li>
<li><a href="productos.html">Productos</a></li>
<li><a href="conatcto.html">Contacto</a></li>
</ul>
</div>
</div>
<div class="span3">
<div class="widget">
<h5 class="widgetheading"><i class="icon-folder-open-alt"></i> Productos Destacados<span></span></h5>
<p>
						- Refrigerantes<br/>
					  - Ãrboles de carga<br/>
					  - Bombas de vacio<br/>
					  - Prensado HidrÃ¡ulico<br/>
					  - Ductos Flexibles<br/>
					  - Soldaduras de plata</p>
</div>
</div>
<div class="span3">
<div class="widget">
<h5 class="widgetheading"><i class="icon-folder-open-alt"></i> Destacamos<span></span></h5>
<p><img height="114" src="img/soldaduras.jpg" width="238"/></p>
</div>
</div>
<div class="span3">
<div class="widget">
<h5 class="widgetheading"><i class="icon-folder-open-alt"></i> ContÃ¡ctenos<span></span></h5>
<address>
<i class="icon-home"></i> <strong>DirecciÃ³n Dicoval Ltda.</strong><br/>
							Artemio Gutierrez #1602, Esquina Maule #410, Santiago.<br/>
<a href="tel:+56229046759">22 904 6759</a><br/>
<a href="tel:+56229046760">22 904 6760</a><br/>
<a href="mailto:dicoval.ltda@gmail.com">dicoval.ltda@gmail.com</a>
<img src="img/di-1.jpg"/> <a href="https://www.facebook.com/dicovallimitada/" target="_blank"><img alt="facebook dicoval" src="img/di-2.jpg"/></a><a href="https://instagram.com/dicovalimitada?igshid=1s9czph61o1rz" target="_blank"><img alt="instragram dicoval" src="img/di-3.jpg"/></a></address></div>
</div>
</div>
</div>
<div id="sub-footer">
<div class="copyright">
<p><a href="http://www.avdesign.cl" target="_blank"><img alt="diseÃ±o web, pÃ¡ginas web, diseÃ±o grÃ¡fico" height="23" longdesc="diseÃ±o web, pÃ¡ginas web, diseÃ±o grÃ¡fico" src="img/avdesign.jpg" width="119"/></a></p>
</div>
</div>
</footer>
</div>
<a class="scrollup" href="#"><i class="icon-chevron-up icon-square icon-48 active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jcarousel/jquery.jcarousel.min.js"></script>
<script src="js/prettyPhoto/jquery.prettyPhoto.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/hover/jquery-hover-effect.js"></script>
<script src="js/tweet/jquery.tweet.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.nivo.slider.js"></script>
<script src="js/modernizr.custom.79639.js"></script>
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<script src="js/layerslider/layerslider.transitions.js"></script>
<script src="js/layerslider/layerslider.kreaturamedia.jquery.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>