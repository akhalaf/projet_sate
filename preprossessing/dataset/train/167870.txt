<!DOCTYPE html>
<html lang="it">
<head><meta charset="utf-8"/><meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/><meta content="width=device-width, initial-scale=1.0" name="viewport"/><title>
	GameStop - Attiva la tua protezione
</title><link href="css/normalize.min.css" rel="stylesheet"/><link href="css/jquery-ui.min.css" rel="stylesheet"/>
<script src="js/jquery-ui.min.js"></script>
<link href="css/style.min.css" rel="stylesheet" type="text/css"/>
<!-- DoDialog -->
<link href="vendor/duDialog/css/duDialog.min.css" rel="stylesheet"/>
<script src="js/modernizr.custom.js"></script>
<!-- DoDialog -->
<script src="vendor/duDialog/js/duDialog.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/infotema.js"></script>
</head>
<body>
<form action="./" id="ctl01" method="post">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwUKLTM5OTc2Mzk2Ng9kFgJmDw8WBB4PX19BbnRpWHNyZlRva2VuBSA2M2VhZWMwZDI4OWE0ZDBjOGI0MDI1NWQyMTI0ZGEwMR4SX19BbnRpWHNyZlVzZXJOYW1lZWRkZKS2fR7F4bcWiuyJ1zquFJnUdv1kpxDESMniSwoOtBYn"/>
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="CA0B0334"/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAKr5FdLBNP0BXsJNEfj5RmyA9ZpMIQevN9kU9bwUQ1rQcd5W9WNbKM7O4BLldquT9aDSzZSgYGL0O4n+EN9PUB7"/>
<div class="alert" id="alertWrapper" onclick="this.style.display='none';" style="display: none;">
<span class="closebtn" onclick="this.parentElement.style.display='none';">×</span>
<span class="alertContent"></span>
</div>
<header>
<div class="top-header">
<a href="StepNoteLegali.aspx" id="hylNoteLegali" rel="noopener noreferrer" target="_blank">Note legali</a>
</div>
<div class="bottom-header">
<img alt="GameStop" src="img/logo.png"/>
</div>
</header>
<div class="content">
<div class="top-img">
<img alt="GameStop" src="img/content.png"/>
</div>
<div class="top-content">
<div class="alert info" id="cookyesalert" onclick="this.style.display='none';" style="display: block;">
<span class="closebtn" onclick="this.parentElement.style.display='none';">×</span>
<span class="alertContent">
            Questo sito non utilizza cookies di profilazione. Vengono utilizzati cookies tecnici per migliorare le funzionalità del sito. L'informativa dettagliata è consultabile cliccando sul link <a href="https://adesioni.spbitalia.it/privacy/Cookies.pdf" rel="noopener noreferrer" target="_blank">"Cookies Policy"</a>.
        </span>
</div>
<p>
        Benvenuto sul sito di perfezionamento dell'attivazione per la tua copertura assicurativa
    </p>
</div>
</div>
<div class="bottom-content">
<h2>Numero di contratto e data di acquisto</h2>
<div>Per favore inserisci qui il tuo numero contratto e la data di acquisto</div>
<div>Ti ricordiamo che devi aspettare 3 giorni dalla data di acquisto per poterti attivare su questo sito.</div>
<div class="form2">
<label>Numero di contratto (tutto in MAIUSCOLO) *</label>
<input autocomplete="off" class="input" id="txtNumContratto" name="numContratto" tabindex="1" type="text" value=""/>
<div class="info1">
<a href="#" tabindex="4">?<span>
<img alt="FAQ Numero di Contratto" src="img/Barcode_Gen2016.png"/>
</span></a>
</div>
<label>Data di acquisto (GG/MM/YYYY) *</label>
<input autocomplete="off" class="input date-picker" id="txtDataAcquisto" name="dataAcquisto" tabindex="2" type="text" value=""/>
<div class="info1">
<a href="#" tabindex="5">?<span>
<img alt="FAQ Data di acquisto" src="img/scontrino_nuove_casse_2.png"/>
</span></a>
</div>
</div>
</div>
<div class="bottom-content"></div>
<div class="bt">
<div class="btn-prev"></div>
<div class="btn-next"><a href="javascript:__doPostBack('ctl00$ButtonContent$btnAvanti','')" id="ButtonContent_btnAvanti" tabindex="3">AVANTI</a></div>
</div>
<footer>
            Una protezione distribuita da &amp;nbsp<img alt="GameStop" height="21" src="img/gslogo_wr250.png" width="75"/>&amp;nbsp e messa a disposizione da ALPHA INSURANCE A.S. e/o WAKAM S.A.
        </footer>
<div id="dialog" style="display: none"></div>
</form>
<script src="js/jquery-ui.min.js"></script>
<script src="js/datepicker-it.js"></script>
<script>
        $(document).ready(function () {
            $(".date-picker").datepicker();
        });
    </script>
</body>
</html>
