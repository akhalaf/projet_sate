<!DOCTYPE html>
<html class="" lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- This is Squarespace. --><!-- creativity-testing-services -->
<base href=""/>
<meta charset="utf-8"/>
<title>Creativity Testing Services</title>
<link href="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1488577894670-682CG6107TO51XLIYJ5F/ke17ZwdGBToddI8pDm48kNjpXgdB2GQ4GXsvq_1og8aoCXeSvxnTEQmG4uwOsdIceAoHiyRoc52GMN5_2H8Wp6_MLhNeL9yWw1amyLjdgohmCNS_LeH4t9qD0d_SmSPc_VumPslpQQgM4-VHAcYf4g/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.creativitytestingservices.com" rel="canonical"/>
<meta content="Creativity Testing Services" property="og:site_name"/>
<meta content="Creativity Testing Services" property="og:title"/>
<meta content="https://www.creativitytestingservices.com" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Creativity Testing Services is a leading creativity assessment company. To help people fulfill their creative potential, CTS has developed a comprehensive battery of tests for measuring creativity. The Runco Creativity Assessment Battery, known as the rCAB, represents more than 35 years of creativit" property="og:description"/>
<meta content="http://static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1489023276449/creativity-testing-services-logo-white-optim%402x.png?format=1500w" property="og:image"/>
<meta content="1500" property="og:image:width"/>
<meta content="520" property="og:image:height"/>
<meta content="Creativity Testing Services" itemprop="name"/>
<meta content="https://www.creativitytestingservices.com" itemprop="url"/>
<meta content="Creativity Testing Services is a leading creativity assessment company. To help people fulfill their creative potential, CTS has developed a comprehensive battery of tests for measuring creativity. The Runco Creativity Assessment Battery, known as the rCAB, represents more than 35 years of creativit" itemprop="description"/>
<meta content="http://static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1489023276449/creativity-testing-services-logo-white-optim%402x.png?format=1500w" itemprop="thumbnailUrl"/>
<link href="http://static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1489023276449/creativity-testing-services-logo-white-optim%402x.png?format=1500w" rel="image_src"/>
<meta content="http://static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1489023276449/creativity-testing-services-logo-white-optim%402x.png?format=1500w" itemprop="image"/>
<meta content="Creativity Testing Services" name="twitter:title"/>
<meta content="http://static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1489023276449/creativity-testing-services-logo-white-optim%402x.png?format=1500w" name="twitter:image"/>
<meta content="https://www.creativitytestingservices.com" name="twitter:url"/>
<meta content="summary" name="twitter:card"/>
<meta content="Creativity Testing Services is a leading creativity assessment company. To help people fulfill their creative potential, CTS has developed a comprehensive battery of tests for measuring creativity. The Runco Creativity Assessment Battery, known as the rCAB, represents more than 35 years of creativit" name="twitter:description"/>
<meta content="Creativity Testing Services is a leading creativity assessment company. To 
help people fulfill their creative potential, CTS has developed a 
comprehensive battery of tests for measuring creativity. The Runco 
Creativity Assessment Battery, known as the rCAB, represents more than 35 
years of creativity research." name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/ku-ZOl1iP2p1gl2gcyChiY45aU5ARRkQBEyWMgN3U49felvIfFHN4UJLFRbh52jhWD9tjRyyFhB8FQ8RjDjtFQZcwh4cFesKZyn4MkG0jAFu-WsoShFGZAsude80ZkoRdhXCHKoyjamTiY8Djhy8ZYmC-Ao1Oco8if37OcBDOcu8OfG0ja48jWsTic8XpPuXZWFX-Ao1OWF8S1ZTjhmDO1FUiABkZWF3jAF8OcFzdP37O1FUiABkZWF3jAF8ShFGZAsude80ZkoRdhXCjAFu-WsoShFGZAsude80ZkoRdhXCjAFu-WsoShFGZAsude80Zko0ZWbCjWw0dA9Cja48jWsTic8XpPuXZWFX-Ao1OWF8S1ZTjhmDO1FUiABkZWF3jAF8OcFzdPURScmyie8h-WsoiemDie80ZaF8S1ZTjhmDOcFzdPUCdhFydeyzSabCSaBujW48SagyjhmDjhy8ZYmC-Ao1OcFzdPUaiaS0jAFu-WsoShFGZAsude80ZkoRdhXCiaiaOcBRiA8XpWFR-emqiAUTdcS0dcmXOYiaikoySkolZPUaiaS0ja48jWsTic8XpPuXZWFX-Ao1OWF8S1ZTjhmDO1FUiABkZWF3jAF8OcFzdPUaiaS0ja48jWsTic8XpWs8SasTdciDZW4h-AF8SkoRdhXCiaiaO1FUiABkZWF3jAF8ShFGZAsude80ZkoRdhXK2YgkdayTdAIldcNhjPJ4Z1mXiW4yOWgXH6qJn3IbMg6IJMJ7f6K3UMIbMg6BJMJ7fbKOMsMMeMS6MKG4fOMgIMMj2KMfH6qJRMIbMg6sJMJ7fbKgmsMgeMS6MKG4fJ4mIMIjIPMfqMeVxj2ugb.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-c1c86395ba3414af8be5f-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-c1c86395ba3414af8be5f-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-b91bd194c00e2fea10548-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-b91bd194c00e2fea10548-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/performance-e8f0bc9aa26cee507ecf9-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-performance');</script>
<script crossorigin="anonymous" defer="" src="//assets.squarespace.com/universal/scripts-compressed/performance-e8f0bc9aa26cee507ecf9-min.en-US.js"></script><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8be49b73ad531d31edbc8-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-5d4498a49e5091bde11f6-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-50e1ca5f8968d0c020ee8-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-3274f168731adc99d3461-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-cbd71cfafe08916ef8001-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-d159e1836ef1682cf4dcc-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-b47c21169b04c244a504e-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-cb055ee486e9b3c94512b-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-00e249abc78aa93f3231f-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-7b24235161bd3a653d994-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-787fcc4151829901d878a-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-b22a563dcc59db7a491ee-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-7af3468bac8e7e7f1abf5-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-db05f9d8574f6098496bb-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-5732346962a1f0dc3ded0-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-c3fa6604995e6f64096ad-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-f60014f42e12cc8607c57-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-8a8dcf9cee65dbef63e76-min.en-US.js"}},"pageType":1,"website":{"id":"58b8a7ae59cc6856f3bf4dbf","identifier":"creativity-testing-services","websiteType":1,"contentModifiedOn":1608821476609,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"America/Los_Angeles","machineTimeZoneOffset":-28800000,"timeZoneOffset":-28800000,"timeZoneAbbr":"PST","siteTitle":"Creativity Testing Services","fullSiteTitle":"Creativity Testing Services","siteDescription":"<p>Creativity testing services</p>","logoImageId":"58c0b128e6f2e14660be493c","shareButtonOptions":{"3":true,"4":true,"6":true,"1":true,"2":true,"8":true,"7":true},"logoImageUrl":"//static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1608821476609/","authenticUrl":"https://www.creativitytestingservices.com","internalUrl":"https://creativity-testing-services.squarespace.com","baseUrl":"https://www.creativitytestingservices.com","primaryDomain":"www.creativitytestingservices.com","sslSetting":3,"typekitId":"","statsMigrated":false,"imageMetadataProcessingEnabled":false,"screenshotId":"f7cba469fe1f2bba9b3d49184116b3631b3a9b8fd9f3fe19a50626364c70cb1d","showOwnerLogin":false},"websiteSettings":{"id":"58b8a7ae59cc6856f3bf4dc2","websiteId":"58b8a7ae59cc6856f3bf4dbf","subjects":[],"country":"US","state":"CA","simpleLikingEnabled":true,"mobileInfoBarSettings":{"isContactEmailEnabled":false,"isContactPhoneNumberEnabled":false,"isLocationEnabled":false,"isBusinessHoursEnabled":false},"commentLikesAllowed":true,"commentAnonAllowed":true,"commentThreaded":true,"commentApprovalRequired":false,"commentAvatarsOn":true,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":true,"commentEnableByDefault":true,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"storeSettings":{"returnPolicy":null,"termsOfService":null,"privacyPolicy":null,"expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"US","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":true,"isBillingPhoneRequired":false,"currenciesSupported":["CHF","HKD","MXN","EUR","DKK","USD","CAD","MYR","NOK","THB","AUD","SGD","ILS","PLN","GBP","CZK","SEK","NZD","PHP","RUB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":false,"sameAsRetailLocation":false,"merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":false,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"disabled"},"ampEnabled":false},"cookieSettings":{"isCookieBannerEnabled":false,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"collection":{"title":"Home","id":"58bef64459cc6866d22d178d","fullUrl":"/","type":1,"permissionType":1},"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"aspect-ratio":"Auto","contentBgColor":"rgba(255,255,255,1)","first-index-image-fullscreen":"true","fixed-header":"false","footerBgColor":"rgba(27,36,66,0.90)","gallery-arrow-style":"Circular","gallery-auto-crop":"true","gallery-autoplay":"true","gallery-design":"Slideshow","gallery-info-overlay":"Hide","gallery-loop":"true","gallery-navigation":"None","gallery-show-arrows":"true","galleryArrowBackground":"rgba(255,255,255,1)","galleryArrowColor":"rgba(51,51,51,1)","galleryAutoplaySpeed":"4","galleryCircleColor":"rgba(255,255,255,1)","galleryInfoBackground":"rgba(0, 0, 0, .7)","galleryThumbnailSize":"100px","gridSize":"350px","gridSpacing":"20px","headerBgColor":"hsla(217, 37%, 54%, 0.98)","imageOverlayColor":"hsla(218, 55%, 27%, 0.53)","index-image-height":"Fullscreen","parallax-scrolling":"false","product-gallery-auto-crop":"true","product-image-auto-crop":"true","title--description-alignment":"Center","title--description-position":"Over Image","titleBgColor":"rgba(151,166,180,1)","transparent-header":"true","tweak-v1-related-products-title-spacing":"50px"},"templateId":"515c7bd0e4b054dae3fcf003","templateVersion":"7","pageFeatures":[1,2,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/515c7b5ae4b0875140c3d94a/2779/scripts/","betaFeatureFlags":["domains_transfer_flow_improvements","donations_customer_accounts","commerce_minimum_order_amount","animations_august_2020_new_preset","events_panel_70","commerce_tax_panel_v2","commerce_subscription_order_delay","commerce_afterpay_toggle","commerce_category_id_discounts_enabled","commerce_reduce_cart_calculations","commerce_restock_notifications","commerce_setup_wizard","commerce-recaptcha-enterprise","seven-one-content-preview-section-api","commerce_pdp_edit_mode","seven_one_frontend_render_page_section","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","seven-one-menu-overlay-theme-switcher","seven_one_frontend_render_gallery_section","dg_downloads_from_fastly","domain_locking_via_registrar_service","domains_transfer_flow_hide_preface","member_areas_ga","domains_universal_search","commerce_instagram_product_checkout_links","ORDERS-SERVICE-check-digital-good-access-with-service","ORDERS-SERVICE-reset-digital-goods-access-with-service","seven_one_header_editor_update","omit_tweakengine_tweakvalues","list_sent_to_groups","campaigns_new_sender_profile_page","commerce_afterpay","gallery_captions_71","crm_campaigns_sending","seven-one-main-content-preview-api","campaigns_single_opt_in","nested_categories_migration_enabled","commerce_add_to_cart_rate_limiting","customer_notifications_panel_v2","commerce_demo_products_modal","seven_one_portfolio_hover_layouts","campaigns_user_templates_in_sidebar","generic_iframe_loader_for_campaigns","ORDER_SERVICE-submit-subscription-order-through-service","seven_one_image_overlay_opacity","commerce_activation_experiment_add_payment_processor_card","seven_one_image_effects","domain_deletion_via_registrar_service","newsletter_block_captcha","campaigns_email_reuse_template_flow","domains_allow_async_gsuite","commerce_pdp_survey_modal","local_listings","domain_info_via_registrar_service","domains_use_new_domain_connect_strategy","page_interactions_improvements"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"demoCollections":[{"collectionId":"5433e637e4b08c8867785af5","deleted":false},{"collectionId":"5433e662e4b08c1f73bdf00c","deleted":false},{"collectionId":"5433e670e4b08c1f73bdf01a","deleted":false},{"collectionId":"5436e476e4b0629a266a38fa","deleted":false}],"tzData":{"zones":[[-480,"US","P%sT",null]],"rules":{"US":[[1967,2006,null,"Oct","lastSun","2:00","0","S"],[1987,2006,null,"Apr","Sun>=1","2:00","1:00","D"],[2007,"max",null,"Mar","Sun>=8","2:00","1:00","D"],[2007,"max",null,"Nov","Sun>=1","2:00","0","S"]]}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><link href="https://www.creativitytestingservices.com/home-index?format=rss" rel="alternate" title="RSS Feed" type="application/rss+xml"/>
<script type="application/ld+json">{"url":"https://www.creativitytestingservices.com","name":"Creativity Testing Services","description":"<p>Creativity testing services</p>","image":"//static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1608821476609/","@context":"http://schema.org","@type":"WebSite"}</script><link href="//static1.squarespace.com/static/sitecss/58b8a7ae59cc6856f3bf4dbf/44/515c7bd0e4b054dae3fcf003/58bef5845016e1ddd0445bcd/2779-05142015/1587560184893/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<!-- End of Squarespace Headers -->
<script>
      Y.Squarespace.FollowButtonUtils = {};
      Y.Squarespace.FollowButtonUtils.renderAll = function(){};
    </script>
</head>
<body class="event-excerpts event-list-date event-list-time event-list-address event-icalgcal-links events-layout-columns events-width-full gallery-design-slideshow aspect-ratio-auto lightbox-style-dark gallery-navigation-none gallery-info-overlay-hide gallery-arrow-style-circular gallery-show-arrows gallery-auto-crop gallery-autoplay gallery-loop transparent-header hide-parallax-nav first-index-image-fullscreen index-image-height-fullscreen image-overlay hide-page-title title--description-alignment-center title--description-position-over-image extra-description-formatting description-button-style-solid description-button-corner-style-square blog-layout-simple blog-width-full hide-author hide-back-to-top-link show-category-navigation product-list-titles-under product-list-alignment-center product-item-size-11-square product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark opentable-style-light small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-center image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-text-alignment-left button-style-solid button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-type-index collection-layout-default collection-58bef64459cc6866d22d178d homepage view-list mobile-style-available has-banner-image " id="collection-58bef64459cc6866d22d178d">
<div data-content-field="navigation-mobileNav" id="mobileNav">
<nav class="main-nav">
<div class="nav-wrapper">
<ul class="cf">
<li class="index-collection active-link folder">
<a href="/"><span>Home</span></a>
</li>
<li class="page-collection">
<a href="/about"><span>About</span></a>
</li>
<li class="page-collection">
<a href="/products"><span>Products</span></a>
</li>
<li class="page-collection">
<a href="/personnel"><span>Personnel</span></a>
</li>
<li class="blog-collection">
<a href="/blog"><span>Blog</span></a>
</li>
<li class="page-collection">
<a href="/contact"><span>Contact</span></a>
</li>
</ul>
</div>
</nav>
</div>
<div class="site-wrapper">
<div class="site-inner-wrapper">
<header class="sqs-announcement-bar-dropzone" id="header">
<div class="sqs-cart-dropzone"></div>
<div class="title-nav-wrapper">
<h1 class="site-title" data-content-field="site-title">
<a href="/" id="top">
<img alt="Creativity Testing Services" src="//static1.squarespace.com/static/58b8a7ae59cc6856f3bf4dbf/t/58c0b128e6f2e14660be493c/1608821476609/?format=1500w"/>
</a>
</h1>
<a class="icon-menu" id="mobileMenu"></a>
<!--MAIN NAVIGATION-->
<div data-annotation-alignment="bottom left" data-content-field="navigation-mainNav" id="desktopNav">
<nav class="main-nav">
<div class="nav-wrapper">
<ul class="cf">
<li class="index-collection active-link folder">
<a href="/"><span>Home</span></a>
</li>
<li class="page-collection">
<a href="/about"><span>About</span></a>
</li>
<li class="page-collection">
<a href="/products"><span>Products</span></a>
</li>
<li class="page-collection">
<a href="/personnel"><span>Personnel</span></a>
</li>
<li class="blog-collection">
<a href="/blog"><span>Blog</span></a>
</li>
<li class="page-collection">
<a href="/contact"><span>Contact</span></a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</header>
<section data-content-field="main-content" id="content-wrapper">
<!-- <div class="sqs-state-message error">There are no pages in this Index.</div> -->
<div id="parallax-images">
<div class="image-container content-fill first" data-content-field="main-image" data-url-id="home">
<img alt="creativity-optim.jpg" class="loading" data-image="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1488908289770-PRXKKHRCNH4JOERP71DR/ke17ZwdGBToddI8pDm48kC74qCv3EladMXjzQvmYVDwUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcqpBeoaxdN84IPN0D4kFbkzhZOosEnu_gDDT57d5GPRAtSY-pMavEQArwjbuCI-7p/creativity-optim.jpg" data-image-dimensions="1220x976" data-image-focal-point="0.5,0.5" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1488908289770-PRXKKHRCNH4JOERP71DR/ke17ZwdGBToddI8pDm48kC74qCv3EladMXjzQvmYVDwUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcqpBeoaxdN84IPN0D4kFbkzhZOosEnu_gDDT57d5GPRAtSY-pMavEQArwjbuCI-7p/creativity-optim.jpg"/>
<div class="image-overlay-wrapper"></div>
</div>
</div>
<div class="parallax-item" data-url-id="home">
<div class="title-desc-wrapper over-image has-main-image first" data-color-suggested="#201f31">
<div class="title-desc-image content-fill" data-content-field="main-image">
<img alt="creativity-optim.jpg" data-image="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1488908289770-PRXKKHRCNH4JOERP71DR/ke17ZwdGBToddI8pDm48kC74qCv3EladMXjzQvmYVDwUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcqpBeoaxdN84IPN0D4kFbkzhZOosEnu_gDDT57d5GPRAtSY-pMavEQArwjbuCI-7p/creativity-optim.jpg" data-image-dimensions="1220x976" data-image-focal-point="0.5,0.5" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1488908289770-PRXKKHRCNH4JOERP71DR/ke17ZwdGBToddI8pDm48kC74qCv3EladMXjzQvmYVDwUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcqpBeoaxdN84IPN0D4kFbkzhZOosEnu_gDDT57d5GPRAtSY-pMavEQArwjbuCI-7p/creativity-optim.jpg"/>
<div class="image-overlay-wrapper"></div>
</div>
<div class="title-desc-inner" data-collection-id="58beefe51e5b6c65a77e7a51" data-edit-main-image="Background">
<div class="page-title-wrapper"><h1 class="page-title" data-content-field="title">Creativity Testing Services Assessment Creative Research</h1></div>
<br/> <!-- this keeps the inline-blocks from collapsing -->
<div class="page-desc" data-content-field="description"><p><strong>Creativity Testing Services</strong></p><p>Creativity Testing Services is a leading creativity assessment company. To help people fulfill their creative potential, CTS has developed a comprehensive battery of tests for measuring creativity. The Runco Creativity Assessment Battery, known as the rCAB, represents more than 35 years of creativity research.</p></div>
</div>
<div class="scroll-arrow"><span>SCROLL DOWN</span></div>
</div>
<div class="content has-main-image">
<div class="content-inner" data-content-field="main-content">
<div class="title-desc-wrapper">
<div class="title-desc-inner" data-collection-id="58beefe51e5b6c65a77e7a51">
<div class="page-title-wrapper"><h1 class="page-title">Creativity Testing Services Assessment Creative Research</h1></div>
<br/> <!-- this keeps the inline-blocks from collapsing -->
<div class="page-desc" data-content-field="description"><p><strong>Creativity Testing Services</strong></p><p>Creativity Testing Services is a leading creativity assessment company. To help people fulfill their creative potential, CTS has developed a comprehensive battery of tests for measuring creativity. The Runco Creativity Assessment Battery, known as the rCAB, represents more than 35 years of creativity research.</p></div>
</div>
</div>
</div>
</div>
</div>
<div class="back-to-top"></div>
</section>
<!--FOOTER WITH OPEN BLOCK FIELD-->
<footer id="footer">
<div class="back-to-top-link"><a href="#top"><span class="arrow"></span>Top</a></div>
<div class="footer-wrapper">
<div data-content-field="navigation-secondaryNav" id="secondaryNav">
</div>
<div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Footer Content" data-type="block-field" data-updated-on="1608748399654" id="footerBlocks"><div class="row sqs-row"><div class="col sqs-col-12 span-12"><div class="row sqs-row"><div class="col sqs-col-3 span-3"><div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-yui_3_17_2_1_1608732384015_6019"><div class="sqs-block-content">
<div class=" image-block-outer-wrapper layout-caption-hidden design-layout-inline combination-animation-reveal individual-animation-none individual-text-animation-none " data-test="image-block-inline-outer-wrapper">
<figure class=" sqs-block-image-figure intrinsic " style="max-width:100%;">
<button class=" sqs-block-image-button lightbox " data-description="" data-lightbox-theme="dark">
<span class="v6-visually-hidden">View fullsize</span>
<div class=" image-block-wrapper has-aspect-ratio " data-animation-override="" data-animation-role="image" style="padding-bottom:100.0%;">
<noscript><img alt="AoC-logo.png" src="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1608732420071-8NC49GII6QVWDJOHK8VS/ke17ZwdGBToddI8pDm48kBtpJ0h6oTA_T7DonTC8zFdZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIBqmMCQ1OP129tpEIJko8bi_9nfbnt8dRsNvtvnXdL8M/AoC-logo.png"/></noscript><img alt="AoC-logo.png" class="thumb-image" data-image="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1608732420071-8NC49GII6QVWDJOHK8VS/ke17ZwdGBToddI8pDm48kBtpJ0h6oTA_T7DonTC8zFdZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIBqmMCQ1OP129tpEIJko8bi_9nfbnt8dRsNvtvnXdL8M/AoC-logo.png" data-image-dimensions="960x960" data-image-focal-point="0.5,0.5" data-image-id="5fe34f032140ed673505c78c" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/58b8a7ae59cc6856f3bf4dbf/1608732420071-8NC49GII6QVWDJOHK8VS/ke17ZwdGBToddI8pDm48kBtpJ0h6oTA_T7DonTC8zFdZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIBqmMCQ1OP129tpEIJko8bi_9nfbnt8dRsNvtvnXdL8M/AoC-logo.png" data-type="image"/>
</div>
</button>
</figure>
</div>
</div></div></div><div class="col sqs-col-6 span-6"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_1_1608732384015_7505"><div class="sqs-block-content"><p class="" style="white-space:pre-wrap;">CTS is accredited and certified by the <strong>Academy of Creativity</strong> and, as of December of 2020, is jointly operating with the Academy of Creativity</p></div></div></div><div class="col sqs-col-3 span-3"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-yui_3_17_2_1_1608732636179_9017"><div class="sqs-block-content"> </div></div></div></div><div class="row sqs-row"><div class="col sqs-col-9 span-9"><div class="sqs-block horizontalrule-block sqs-block-horizontalrule" data-block-type="47" id="block-yui_3_17_2_62_1488907888149_5973"><div class="sqs-block-content"><hr/></div></div></div><div class="col sqs-col-3 span-3"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-yui_3_17_2_1_1608732502986_7645"><div class="sqs-block-content"> </div></div></div></div><div class="row sqs-row"><div class="col sqs-col-8 span-8"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_62_1488907888149_3989"><div class="sqs-block-content"><p class="" style="white-space:pre-wrap;"><em>The content and opinions expressed on this Website do not necessarily reflect the views of any University or partner of CTS personnel. </em></p></div></div></div><div class="col sqs-col-4 span-4"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-yui_3_17_2_6_1491571265899_7174"><div class="sqs-block-content"> </div></div></div></div><div class="sqs-block code-block sqs-block-code" data-block-type="23" id="block-yui_3_17_2_5_1491571265899_5942"><div class="sqs-block-content"><p class="copyright">Designed and developed by <a href="http://sciencesites.org">ScienceSites: Websites for Scientists</a> <span class="delimiter">|</span> © <script>document.write((new Date()).getFullYear())</script> <a href="http://markrunco.com">Mark Runco</a> </p>
</div></div><div class="sqs-block code-block sqs-block-code" data-block-type="23" id="block-yui_3_17_2_58_1488907888149_4003"><div class="sqs-block-content"><!-- Five template social icons html:
Configurable from the Style Editor GUI: colors, size, style. Change social URL's here and/or add/remove each social service (just copy/paste <a> to </a> and follow naming convention of Twitter and Facebook for LinkedIn, YouTube, etc.) for clients who "just want a link" out to their social accounts --->
<div class="social-links sqs-svg-icon--list" data-content-field="connected-accounts" id="sqs-social">
<a class="sqs-svg-icon--wrapper twitter" href="https://twitter.com/ProfMeza" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--background" xlink:href="/universal/svg/social-accounts.svg#background" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
<use class="sqs-use--icon" xlink:href="/universal/svg/social-accounts.svg#twitter-icon" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
<use class="sqs-use--mask" xlink:href="/universal/svg/social-accounts.svg#twitter-mask" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
</svg>
</div>
</a>
<a class="sqs-svg-icon--wrapper linkedin" href="https://www.linkedin.com/in/jcmeza/" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--background" xlink:href="/universal/svg/social-accounts.svg#background" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
<use class="sqs-use--icon" xlink:href="/universal/svg/social-accounts.svg#linkedin-icon" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
<use class="sqs-use--mask" xlink:href="/universal/svg/social-accounts.svg#linkedin-mask" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
</svg>
</div>
</a>
</div></div></div></div></div></div>
</div>
</footer>
</div>
</div>
<script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script>
<script src="https://static1.squarespace.com/static/ta/515c7b5ae4b0875140c3d94a/2779/scripts/site-bundle.js" type="text/javascript"></script>
</body>
</html>
