<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-63619247-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63619247-1');
</script>
<title>Asian Media Group</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<link href="https://www.amg.biz/css/reset.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.amg.biz/css/slicknav.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.amg.biz/css/slick.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.amg.biz/css/responsive.css" media="screen" rel="stylesheet" type="text/css"/>
<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
<!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
<!--==============================header================================= -->
<header>
<div class="fixer2">
<h1 class="logo2"><a href="https://www.amg.biz/index.php"><img alt="" src="https://www.amg.biz/images/logo.png"/></a></h1>
<nav>
<ul class="menu">
<li class="active"><a href="https://www.amg.biz/index.php">Home</a></li>
<li class="dummy"><a>      </a></li>
<li><a href="https://www.amg.biz/about.php">Who we are</a></li>
<li class="dummy"><a>      </a></li>
<li><a href="https://www.amg.biz/brands.php">Brands</a>
<ul class="sub">
<li><a href="https://www.amg.biz/garavigujrat.php">Garavi Gujarat</a></li>
<li><a href="https://www.amg.biz/easterneye.php">Eastern Eye</a></li>
<li><a href="https://www.amg.biz/pharmacybusiness.php">Pharmacy Business</a></li>
<li><a href="https://www.amg.biz/asiantrader.php">Asian Trader</a></li>
<li><a href="https://www.amg.biz/asianhospitality.php">Asian Hospitality</a></li>
<li><a href="https://www.amg.biz/asianrichlist.php">Asian Richlist</a></li>
<li><a href="https://www.amg.biz/powerlist.php">Powerlist</a></li>
</ul>
</li>
<li class="dummy"><a>      </a></li>
<li class="logo"><a href="https://www.amg.biz/index.php"><img alt="" src="https://www.amg.biz/images/logo.png"/></a></li>
<li>
<a href="events.php">Events</a>
<ul class="sub">
<li><a href="http://www.theasianbusinessawards.co.uk" target="_blank">Asian Business Conference and Awards</a></li>
<li><a href=" https://www.asiantrader.biz/at-live/" target="_blank">Asian Trader Conference and Awards</a></li>
<li><a href="http://www.actas.co.uk/" target="_blank">Eastern Eye ACTAs</a></li>
<li><a href="https://www.gg2.net/Events" target="_blank">GG2 Diversity Conference and Leadership Awards</a></li>
<li><a href="https://www.pharmacy.biz/events" target="_blank">Pharmacy Business Conference and Awards</a></li>
</ul>
</li>
<li>
<a href="https://www.amg.biz/services.php">services</a>
<!-- <ul class="sub" style="display:none;">
                        	<li><a href="brands.php">Call Centre Services</a></li>
                        	<li><a href="brands.php">Website Services</a></li>
                        	<li><a href="brands.php">Publisher Services</a></li>
                        	<li><a href="brands.php">Awards Services</a></li>
                        </ul> -->
</li>
<li><a href="https://www.amg.biz/competitions.php">Competitions</a></li>
<li><a href="https://www.amg.biz/contactus.php">Contact US</a></li>
</ul>
</nav>
</div>
</header>
<div class="popup"><i></i>
<h3>Enquiry</h3>
<form>
<input name="name" placeholder="Your name" type="text"/>
<input name="email" placeholder="Your E-mail" type="text"/>
<input name="phone" placeholder="Your Phone" type="text"/>
<textarea name="message" placeholder="Your Message"></textarea>
<p><input name="" type="checkbox" value=""/> By using this form you agree with the storage and handling of your data by this website.</p>
<input class="submit" type="submit" value="Submit"/>
</form>
</div>
<!--==============================content================================-->
<div class="banner">
<ul>
<li>
<aside>
<h1>We are Britain’s biggest Asian publishing house.</h1>
<a href="#">Explore</a>
</aside>
<img alt="amg" src="https://www.amg.biz/images/bg1.jpg"/>
</li>
<li>
<aside>
<h1>We publish a stable of market-leading consumer and business titles serving the Asian community in the UK and US. </h1>
<a href="#">Explore</a>
</aside>
<video autoplay="" id="video" loop="" poster="https://www.amg.biz/images/banner2018a.jpg">
<source src="https://www.amg.biz/videos/publish.mp4">
                    Your browser does not support the video tag.
                </source></video>
</li>
<li>
<aside>
<h1>We host leading events celebrating success, leadership and diversity, from black-tie award dinners to seminars, conferences and round table discussions.</h1>
<a href="#">Explore</a>
</aside>
<video autoplay="" id="video" loop="" muted="" poster="https://www.amg.biz/images/banner2018a.jpg">
<source src="https://www.amg.biz/videos/award.mp4">
                    Your browser does not support the video tag.
                </source></video>
</li>
</ul>
<div class="vmenu">
<a class="a1" href="#"></a><a class="a2" href="https://www.facebook.com/AsianMediaPublishers/" target="_blank"></a><a class="a3" href="https://twitter.com/amgbiz" target="_blank"></a><a class="a4" href="https://www.linkedin.com/company/asian-media-&amp;-marketing-group/" target="_blank"></a><a class="a5" href="https://in.pinterest.com/amgbiz/" target="_blank"></a>
</div>
</div>
<section class="s1">
<div class="fixer">
<h2>We are Asian Media Group</h2>
<p>Our business began on 1st April 1968 with the launch of our flagship title, Garavi Gujarat. Today, AMG is a diversified media house, publishing some of the country’s most iconic and trusted Asian media brands and hosting some of the UK’s leading events. </p>
<p>Operating in the UK, US and India, our reach extends across the globe. Our websites attract over a million unique visitors every month  and our business and consumer publications reach over 1.3 million readers each month. </p>
<a class="more" href="#">Read more</a>
</div>
</section>
<section class="s2">
<div class="fixer">
<h2>Publications</h2>
<div class="publications">
<a href="https://www.amg.biz/garavigujarat.php"><img alt="AMG Publications" src="images/brands/b1.png"/></a>
<a href="https://www.amg.biz/easterneye.php"><img alt="AMG Publications" src="images/brands/b2.png"/></a>
<a href="https://www.amg.biz/asiantrader.php"><img alt="AMG Publications" src="images/brands/b3.png"/></a>
<a href="https://www.amg.biz/pharmacybusiness.php"><img alt="AMG Publications" src="images/brands/b4.png"/></a>
<a href="https://www.amg.biz/asianhospitality.php"><img alt="AMG Publications" src="images/brands/b5.png"/></a>
<a href="https://www.amg.biz/asianrichlist.php"><img alt="AMG Publications" src="images/brands/b6.png"/></a>
<a href="https://www.amg.biz/powerlist.php"><img alt="AMG Publications" src="images/brands/b7.png"/></a>
</div>
</div>
</section>
<section class="s3">
<div class="fixer">
<h2>Events</h2>
<div class="events">
<a><img alt="AMG Events" src="images/events/e0.png"/></a>
<a><img alt="AMG Events" src="images/events/e1.png"/></a>
<a><img alt="AMG Events" src="images/events/e2.png"/></a>
<a><img alt="AMG Events" src="images/events/e3.png"/></a>
<a><img alt="AMG Events" src="images/events/e4.png"/></a>
<a><img alt="AMG Events" src="images/events/e5.png"/></a>
<a><img alt="AMG Events" src="images/events/e6.png"/></a>
<a><img alt="AMG Events" src="images/events/e7.png"/></a>
<a><img alt="AMG Events" src="images/events/e8.png"/></a>
<a><img alt="AMG Events" src="images/events/e9.png"/></a>
<a><img alt="AMG Events" src="images/events/e10.png"/></a>
<a><img alt="AMG Events" src="images/events/e11.png"/></a>
</div>
</div>
</section>
<!--==============================footer=================================-->
<footer>
<div class="fixer">
<ul>
<li><h4>Online Media</h4></li>
<li><a href="http://www.gg2.net" target="_blank">gg2.net</a></li>
<li><a href="http://www.asiantrader.biz" target="_blank">asiantrader.biz</a></li>
<li><a href="http://www.asianhospitality.com" target="_blank">asianhospitality.com </a></li>
<li><a href="http://www.pharmacy.biz" target="_blank">pharmacy.biz </a></li>
<li><a href="http://www.easterneye.biz" target="_blank">easterneye.biz </a></li>
<li><a href="http://www.asiantimes.biz" target="_blank">asiantimes.biz</a></li>
<li><a href="http://www.srilankaweekly.co.uk" target="_blank">srilankaweekly.co.uk</a></li>
<li><a href="http://www.bangladeshweekly.com" target="_blank">bangladeshweekly.com</a></li>
<li><a href="http://www.pakistanweekly.co.uk" target="_blank">pakistanweekly.co.uk</a></li>
<li><a href="http://www.gujarat.co.uk" target="_blank">gujarat.co.uk</a></li>
<li><a href="http://www.indiaweekly.co.uk" target="_blank">indiaweekly.co.uk</a></li>
<li><a href="http://www.jobsbuster.com" target="_blank">jobsbuster.com</a></li>
</ul>
<ul>
<li><h4>Publications</h4></li>
<li><a href="https://www.amg.biz/garavigujarat.php" target="_blank">Garavi Gujarat </a></li>
<li><a href="https://www.amg.biz/easterneye.php" target="_blank">Eastern Eye</a></li>
<li><a href="https://www.amg.biz/asiantrader.php" target="_blank">Asian Trader</a></li>
<li><a href="https://www.amg.biz/pharmacybusiness.php" target="_blank">Pharmacy Business</a></li>
<li><a href="https://www.amg.biz/asianhospitality.php" target="_blank">Asian Hospitality</a></li>
<li><a href="https://www.amg.biz/asianrichlist.php" target="_blank">Asian Rich List</a></li>
</ul>
<ul>
<li><h4>Our Awards</h4></li>
<li><a href="https://www.gg2leadershipawards.co.uk/" target="_blank">GG2 Awards</a></li>
<li><a href="http://www.pharmacybusinessawards.co.uk/" target="_blank">Pharmacy Business Awards</a></li>
<li><a href="http://www.asiantraderawards.co.uk/" target="_blank">Asian Trader Awards</a></li>
<li><a href="https://www.theasianbusinessawards.co.uk/" target="_blank">Asian Business Awards</a></li>
<li><a href="http://www.asiantraderconference.co.uk/" target="_blank">Asian Trader Conference</a></li>
<li><a href="http://www." target="_blank">London Retail Show</a></li>
<!--
            <li><h4>Services</h4></li>
            <li><a href="http://www." target="_blank">Call&nbsp;Centre&nbsp;Services</a></li>
            <li><a href="http://www." target="_blank">Website&nbsp;Services</a></li>
            <li><a href="http://www." target="_blank">Awards&nbsp;Events</a></li>
-->
</ul>
<aside>
<p><a href="#">Privacy Policy</a> <a href="#">Terms &amp; Conditions</a> <a href="#">Disclaimer</a> <a href="#">Accessibility</a></p>
<p>© 2009 Garavi Gujarat Publications Ltd &amp; Garavi Gujarat Publications USA Inc</p>
</aside>
</div>
</footer>
</body>
<script src="https://www.amg.biz/js/jquery-1.7.min.js"></script>
<script src="https://www.amg.biz/js/jquery.easing.1.3.js"></script>
<script src="https://www.amg.biz/js/slick.js"></script>
<script src="https://www.amg.biz/js/jquery.slicknav.min.js"></script>
<script src="https://www.amg.biz/js/commonscript.js"></script>
<script>
 $('.banner ul').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  autoplay:true,
  autoplaySpeed:6000,
  arrows:false,
  pauseOnHover: false,
  pauseOnFocus: false,
  fade:true
});

</script>
</html>