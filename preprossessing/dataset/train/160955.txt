<!DOCTYPE html>
<html>
<head>
<title>
    
      Home | Asmodee USA
    
  </title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- <meta name="google-site-verification" content="3v9vpFva4SPwQnAg3Vek0A-bd1AOyFuUEBnvC09whmQ" />
   <meta property="fb:pages" content="110432647142" /> -->
<link href="/static/images/asmodee_north_america_logo_icon.8827d3282e86.png" rel="shortcut icon" sizes="32x32" type="image/png"/>
<link href="/static/css/approval.89fc8db25fe6.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/main.97f564f2bcc8.css" rel="stylesheet" type="text/css"/>
<script src="/static/js/underscorejs_1.7.0/underscore.min.137af05d496f.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js" type="text/javascript"></script>
<script src="/jsi18n/" type="text/javascript"></script>
<script src="/static/js/wotan.25a9193c78a4.js" type="text/javascript"></script>
<script src="/static/js/store.73e0789d2218.js" type="text/javascript"></script>
<script src="/static/js/ffg_core.07da20169d14.js" type="text/javascript"></script>
<script src="/static/js/angular_app.48e458f22b7e.js" type="text/javascript"></script>
<script src="/static/js/jquery_cookie_1.4.1/jquery.cookie.d5528dde0006.js" type="text/javascript"></script>
</head>
<body>
<nav class="grid-container">
<div class="desktop-nav-container">
<div class="top-bar">
<div class="top-bar-left">
<ul class="dropdown menu" data-dropdown-menu="">
<li class="logo-wrapper">
<a href="/">
<img alt="Asmodee USA" src="/static/images/asmodee_usa_desktop_logo.ff3ed556922e.svg"/>
</a>
</li>
<li class=" sibling ">
<a href="/en/retailers/">Retailers</a>
</li>
<li class=" sibling ">
<a href="/en/catalogs/">Products</a>
</li>
<li class=" sibling ">
<a href="/en/press/">Press</a>
</li>
<li class=" sibling ">
<a href="/en/contact/">Contact</a>
</li>
<li class=" sibling ">
<a href="/en/careers/">Careers</a>
</li>
</ul>
</div>
<div class="top-bar-right">
<a class="login-btn" href="https://retailers.asmodeena.com" target="_blank">Retailer Account Sign In</a>
</div>
</div>
</div>
<div class="mobile-nav-container">
<div class="top-bar">
<div class="top-bar-left">
<a href="/">
<img alt="Asmodee USA" src="/static/images/asmodee_north_america_mobile_logo.c862675fe438.svg"/>
</a>
</div>
<div class="top-bar-right">
<div class="hamburger-button">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</div>
</div>
<ul class="mobile-nav vertical menu">
<li class="sibling">
<a href="/en/retailers/">Retailers</a>
</li>
<li class="sibling">
<a href="/en/catalogs/">Products</a>
</li>
<li class="sibling">
<a href="/en/press/">Press</a>
</li>
<li class="sibling">
<a href="/en/contact/">Contact</a>
</li>
<li class="sibling">
<a href="/en/careers/">Careers</a>
</li>
</ul>
</div>
</nav>
<main class="grid-container full">
<section class="grid-container">
<div class="grid-x grid-margin-x">
<div class="cell">
<div style="display: none">
<svg height="500" id="iceberg" width="635">
<path d="M0.000, 0.000 L537.000 0.000 L635.000 260.000 L0.000 500.000 L0.000 0.000 Z" fill="currentColor" fill-rule="evenodd"></path>
</svg>
</div>
<div class="mod-layered-banner-image overflow-hidden">
<div class="mod-content">
<div class="image-wrapper">
<figure class="mobile-background crop-photo" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/7f/7c/7f7cfd4c-63b8-4b00-b3c1-99f28a6fad43/hobby_next_mockup.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190711Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=6a0806a345dd21fad82854725ff2d9075b58247a107b86e2901f7899f82042be');">
<img src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
<figure class="desktop-background crop-photo" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/7f/7c/7f7cfd4c-63b8-4b00-b3c1-99f28a6fad43/hobby_next_mockup.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190711Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=6a0806a345dd21fad82854725ff2d9075b58247a107b86e2901f7899f82042be');">
<img src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
<div class="iceberg">
<svg height="500" width="635">
<use xlink:href="#iceberg"></use>
</svg>
</div>
<div class="text-wrapper">
<h2>Ask about Hobby Next!</h2>
<p></p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="grid-container">
<div class="grid-x grid-margin-x">
<div class="cell">
<div class="mod-callout">
<div class="mod-content">
<a href="/specialty-retailer-application/">
<ul class="list">
<li class="callout-left show-for-large">
<h2>Let Us Be a Part of Your Business</h2>
</li>
<li class="callout-right">
<h2>Open a Retail Account Today <i class="fa fa-long-arrow-right"></i> </h2>
<p>Browse Our Catalog of Over 1,500 Products</p>
</li>
</ul>
</a>
</div>
</div>
</div>
</div>
</section>
<section class="grid-container full">
<div class="grid-x grid-margin-x">
<div class="cell">
<div class="mod-announcement mod-styled-text">
<div class="mod-content">
<a href="https://retail.us.asmodee.com/">
<div class="text-wrapper">
<h3>Asmodee Retailer Web Store</h3>
<p>
          See new releases, special offers, and more!
          
            <span>
            Visit our retailer store now
            <i class="fa fa-long-arrow-right"></i>
</span>
</p>
</div>
</a>
</div>
</div><div class="mod-announcement mod-styled-text">
<div class="mod-content">
<a href="https://drive.google.com/drive/folders/1TwQYw7C7SUjCYahthnJPIWK28_qgSUbw?usp=sharing">
<div class="text-wrapper">
<h3>January 2021 Releases</h3>
<p>
          Including a release calendar, promotions, and image assets.
          
            <span>
            Browse here
            <i class="fa fa-long-arrow-right"></i>
</span>
</p>
</div>
</a>
</div>
</div><div class="mod-styled-text" style="">
<h2>COVID-19 UPDATE</h2>
<p>Asmodee USA Distribution continues to ensure the safety of our employees. Our sales team remains available to fulfill reorders as best they can, but please be aware there will be delays in shipments. At this time, we have announced our releases for June. We wish you all good health and safety.</p>
</div>
</div>
</div>
</section>
<section class="grid-container">
<div class="grid-x grid-margin-x">
<div class="cell medium-6">
<div class="article-list-mbibg mod-article-list">
<header class="mod-header">
<h3>Distribution News</h3>
</header>
<div class="mod-content">
<ul class="list articles">
<li>
<a href="/en/news/2020/4/6/asmodee-offers-free-print-play-downloadable-board-games/">
<div class="headline-wrapper">
<time datetime="2020-04-06T16:06:59.230554">06 Apr 2020</time>
<h4>Asmodee Offers Free “Print &amp; Play” Downloadable Board Games</h4>
<p></p><p><span style="line-height: 113%;">For Families To Enjoy While Staying at Home</span></p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/b2/c3/b2c3b199-05b1-49b8-83f8-65369dc53826/print-play-1110x350.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=3067daf73117ccf4b5d1aa422a0ebee2f82f3fcb18c8e1497b2203c88e7b6811')">
<img alt="Asmodee Offers Free “Print &amp; Play” Downloadable Board Games" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2020/3/17/asmodee-usa-direct-distribution/">
<div class="headline-wrapper">
<time datetime="2020-03-17T16:19:38.419604">17 Mar 2020</time>
<h4>Asmodee USA direct distribution</h4>
<p>Asmodee USA Distribution will begin selling direct to all retail channels starting July 1, 2020</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/27/7f/277fb634-d530-441e-aa36-4be3b4652dc9/ana_pressrelease_banner_1110x350_blu.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=de1942b028edf18e63d7c6bedf69b87eda090317868bb976f2873c97832c6e34')">
<img alt="Asmodee USA direct distribution" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2020/3/17/asmodee-usa-and-mantic-games/">
<div class="headline-wrapper">
<time datetime="2020-03-17T16:14:15.105243">17 Mar 2020</time>
<h4>Asmodee USA and Mantic Games</h4>
<p>Exclusive distributor for Hellboy: The Board Game from Mantic Games</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/e5/ca/e5ca74b7-acfa-49d8-960a-471e75d0af42/ana_pressrelease_banner_1110x350_ylw.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=0a974d95c700f09939f77026445228077d79ac3f9ecb7e378095bf01872929db')">
<img alt="Asmodee USA and Mantic Games" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2020/3/17/asmodee-usa-distribution-begins-distributing-exploding-kittens-inc-games/">
<div class="headline-wrapper">
<time datetime="2020-03-17T16:08:58">17 Mar 2020</time>
<h4>Asmodee USA and Exploding Kittens</h4>
<p>Asmodee USA Distribution is exploding with excitement for Exploding Kittens, Inc.</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/56/91/5691fb2c-28b5-454d-9e2b-d57cbf8f2a22/ana_pressrelease_banner_1110x350_pnk.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=87f6c99a06e655b0988f54338c9138d19a1b50ef71deca740388b7db68978e70')">
<img alt="Asmodee USA and Exploding Kittens" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2020/3/16/asmodee-usa-announces-price-increases/">
<div class="headline-wrapper">
<time datetime="2020-03-16T22:07:25.229164">16 Mar 2020</time>
<h4>Asmodee USA announces price increases</h4>
<p></p><p>Effective May 1, 2020, 27 titles in the Asmodee USA distribution library will receive an MSRP price increase.</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/27/7f/277fb634-d530-441e-aa36-4be3b4652dc9/ana_pressrelease_banner_1110x350_blu.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=de1942b028edf18e63d7c6bedf69b87eda090317868bb976f2873c97832c6e34')">
<img alt="Asmodee USA announces price increases" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2020/3/16/asmodee-usa-discontinues-parts-replacement-services/">
<div class="headline-wrapper">
<time datetime="2020-03-16T21:40:13.135064">16 Mar 2020</time>
<h4>Asmodee USA Discontinues Parts Replacement Services</h4>
<p></p><p>Effective as of February 18, 2020, Asmodee USA Distribution no longer offers parts replacements services. Asmodee USA remains committed to ensuring customers receive a full, complete game experience and will do so by providing credit to retail partners and consumers. </p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/e5/ca/e5ca74b7-acfa-49d8-960a-471e75d0af42/ana_pressrelease_banner_1110x350_ylw.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=0a974d95c700f09939f77026445228077d79ac3f9ecb7e378095bf01872929db')">
<img alt="Asmodee USA Discontinues Parts Replacement Services" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2019/10/31/best-seller-s-program-update-nov-2019/">
<div class="headline-wrapper">
<time datetime="2019-10-31T19:45:27.517518">31 Oct 2019</time>
<h4>Best Seller's Program Update (Nov 2019)</h4>
<p></p><p>A new list of products will go into effect November 1st, 2019</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/82/21/822131b2-0882-4184-92e9-6630b44c4e3b/bestsellers-1110x350.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=e77fe2a39dbfa2830904d64b54fc18f62bc59f691a6ae381fb4e0880713ac9ca')">
<img alt="Best Seller's Program Update (Nov 2019)" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2019/8/23/asmodee-usa-to-distribute-wavelength/">
<div class="headline-wrapper">
<time datetime="2019-08-23T07:00:00">23 Aug 2019</time>
<h4>Asmodee USA to distribute Wavelength</h4>
<p></p><p>Bringing Palm Court’s new mind-bending party game, Wavelength, to the United States.</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/9c/42/9c42d909-6189-4706-ba23-c9f8fbf7d8d6/wav01-1110x350.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=38e24972e157e5007c8edbbd4b485c619f0ce21e4aed7b17b51b2bb7fcfe90a2')">
<img alt="Asmodee USA to distribute Wavelength" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2019/8/16/asmodee-usa-to-distribute-helvetiq-games/">
<div class="headline-wrapper">
<time datetime="2019-08-16T14:50:23">16 Aug 2019</time>
<h4>Asmodee USA to distribute Helvetiq games</h4>
<p></p><p>Exclusive distributor for games in the United States</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/a0/84/a08489aa-1c00-4834-a844-f4bd78dc4380/misty-1110x350.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=1b2d8de294c20419e801dda7967191090ed05bddc4c388603698430797190d95')">
<img alt="Asmodee USA to distribute Helvetiq games" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2019/5/30/Distribution-Agreement-Update/">
<div class="headline-wrapper">
<time datetime="2019-05-30T14:48:08.028241">30 May 2019</time>
<h4>Distribution Agreement Update</h4>
<p></p><p>Asmodee North America and Alliance Game Distributors announce next phase of distribution agreement</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/56/91/5691fb2c-28b5-454d-9e2b-d57cbf8f2a22/ana_pressrelease_banner_1110x350_pnk.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=87f6c99a06e655b0988f54338c9138d19a1b50ef71deca740388b7db68978e70')">
<img alt="Distribution Agreement Update" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2018/7/30/christian-t-peterson-to-step-down-as-ceo-of-asmodee-north-america/">
<div class="headline-wrapper">
<time datetime="2018-07-30T00:00:00">30 Jul 2018</time>
<h4>Christian T. Petersen to Step Down as CEO of Asmodee North America</h4>
<p>Christian T. Petersen to Step Down as CEO of Asmodee North America</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/27/7f/277fb634-d530-441e-aa36-4be3b4652dc9/ana_pressrelease_banner_1110x350_blu.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=de1942b028edf18e63d7c6bedf69b87eda090317868bb976f2873c97832c6e34')">
<img alt="Christian T. Petersen to Step Down as CEO of Asmodee North America" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
<li>
<a href="/en/news/2018/6/8/asmodee-north-america-introduces-best-sellers-program/">
<div class="headline-wrapper">
<time datetime="2018-06-08T00:00:00">08 Jun 2018</time>
<h4>Asmodee North America Introduces Best Sellers Program</h4>
<p></p><p>Asmodee North America Introduces Best Sellers Program</p>
</div>
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://b2b-media-production-ana.s3.amazonaws.com/filer_public/e5/ca/e5ca74b7-acfa-49d8-960a-471e75d0af42/ana_pressrelease_banner_1110x350_ylw.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=0a974d95c700f09939f77026445228077d79ac3f9ecb7e378095bf01872929db')">
<img alt="Asmodee North America Introduces Best Sellers Program" src="/static/images/placeholder-16x9.6a31cb23a316.jpg"/>
</figure>
</div>
</a>
</li>
</ul>
</div>
<footer class="mod-footer">
<a class="btn" href="/en/news/">
            More Stories <i class="fa fa-long-arrow-right"></i>
</a>
</footer>
</div>
</div>
<div class="cell medium-6">
<div class="studio mod-article-list">
<header class="mod-header">
<h3>News From Our Publishers</h3>
</header>
<div class="mod-content">
<ul class="list articles">
<li>
<a href="https://www.fantasyflightgames.com/en/news/2021/1/7/a-mutation-in-the-rules/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png')">
<img alt="A Mutation in the Rules" src="https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>A Mutation in the Rules</h4>
<p>Fantasy Flight Games</p>
</div>
<div class="date-wrapper">
<time datetime="2021-01-07T17:00:00.344988">01/07</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.fantasyflightgames.com/en/news/2021/1/6/support-protocols/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png')">
<img alt="Support Protocols" src="https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Support Protocols</h4>
<p>Fantasy Flight Games</p>
</div>
<div class="date-wrapper">
<time datetime="2021-01-06T22:00:00.340569">01/06</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.fantasyflightgames.com/en/news/2021/1/6/standing-out/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png')">
<img alt="Standing Out" src="https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Standing Out</h4>
<p>Fantasy Flight Games</p>
</div>
<div class="date-wrapper">
<time datetime="2021-01-06T21:59:59.340604">01/06</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.fantasyflightgames.com/en/news/2020/12/30/a-worthy-opponent/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png')">
<img alt="A Worthy Opponent" src="https://www.asmodeena.com/static/images/studio_news_logo-fantasy_flight_games.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>A Worthy Opponent</h4>
<p>Fantasy Flight Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-30T17:00:00.301340">12/30</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.zmangames.com/en/news/2020/12/19/gift-strategy/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-zman.png')">
<img alt="Holiday Gift Guide: Strategy Games" src="https://www.asmodeena.com/static/images/studio_news_logo-zman.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Holiday Gift Guide: Strategy Games</h4>
<p>Z-Man Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-19T14:00:09.246228">12/19</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.zmangames.com/en/news/2020/12/16/gift-history-games/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-zman.png')">
<img alt="Holiday Gift Guide: Historical Games" src="https://www.asmodeena.com/static/images/studio_news_logo-zman.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Holiday Gift Guide: Historical Games</h4>
<p>Z-Man Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-16T14:00:07.947287">12/16</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.zmangames.com/en/news/2020/12/13/gift-cooperative/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-zman.png')">
<img alt="Holiday Gift Guide: Co-op Games" src="https://www.asmodeena.com/static/images/studio_news_logo-zman.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Holiday Gift Guide: Co-op Games</h4>
<p>Z-Man Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-13T14:00:06.646751">12/13</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.zmangames.com/en/news/2020/12/7/gift-two-player/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-zman.png')">
<img alt="Holiday Gift Guide: Perfect Games for Two" src="https://www.asmodeena.com/static/images/studio_news_logo-zman.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Holiday Gift Guide: Perfect Games for Two</h4>
<p>Z-Man Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-07T14:00:06.429182">12/07</time>
</div>
</div>
</a>
</li>
<li>
<a href="https://www.zmangames.com/en/news/2020/12/3/gift-kids-family/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-zman.png')">
<img alt="Holiday Gift Guide: Games for the Kids and Family" src="https://www.asmodeena.com/static/images/studio_news_logo-zman.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Holiday Gift Guide: Games for the Kids and Family</h4>
<p>Z-Man Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-12-03T14:00:05.272484">12/03</time>
</div>
</div>
</a>
</li>
<li>
<a href="http://www.plaidhatgames.com/news/2020/06/25/forgotten-waters-cocktails/" target="_blank">
<div class="image-wrapper">
<figure class="crop-photo image" style="background-image: url('https://www.asmodeena.com/static/images/studio_news_logo-plaidhat.png')">
<img alt="Forgotten Waters Cocktails" src="https://www.asmodeena.com/static/images/studio_news_logo-plaidhat.png"/>
</figure>
</div>
<div class="headline-wrapper">
<div class="text-wrapper">
<h4>Forgotten Waters Cocktails</h4>
<p>Plaid Hat Games</p>
</div>
<div class="date-wrapper">
<time datetime="2020-07-09T19:00:01.176394">07/09</time>
</div>
</div>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="grid-x grid-margin-x">
<div class="cell small-9 center-cell">
<!DOCTYPE html>

<style>
div.studio-logos {
display: none !important;
}
</style>
<div class="studio-logos mod-media-grid">
<div class="mod-content">
<ul class="list media medium-block-3 large-block-4 centered-list">
<li>
<a href="https://www.fantasyflightgames.com/" target="_blank">
<img alt="Fantasy Flight Games" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/84/16/84166275-3c9f-4085-bf4f-4378f65961c4/studio_logo-fantasy_flight_games.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=e490b9581437a48d0a46b4401ee3e38082c2637b02629c577aaaa5e7733dc34f"/>
</a>
</li>
<li>
<a href="https://www.spacecowboys.fr/our-board-games" target="_blank">
<img alt="Space Cowboys" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/d6/1b/d61be4ef-045c-461a-9533-bf5ac006f586/studio_logo-space_cowboys.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=866941099ee9f20fdc2361daea9be30278c57bdce6aa3e6448deff06dfd51bdc"/>
</a>
</li>
<li>
<a href="https://www.catanstudio.com/" target="_blank">
<img alt="Catan Studio" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/92/8b/928b5427-21a9-43dd-bdda-8053f653bafa/studio_logo-catan.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=3c23560ee6ca4368c35827f3907d1af1d4e6bc8ef5716cfc50ca0d3a4dbb0d2b"/>
</a>
</li>
<li>
<a href="https://www.daysofwonder.com/" target="_blank">
<img alt="Days of Wonder" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/b1/3a/b13ae5fe-3d8b-4b9b-9cfb-d632d016ecd0/studio_logo-days_of_wonder.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=cf4e013bd9f12441a9a96cff8c5da183604c38e3a5971d53d9d1a625874243b6"/>
</a>
</li>
<li>
<a href="https://www.gamegenic.com/" target="_blank">
<img alt="Gamegenic" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/35/78/3578add4-0053-40b5-91c6-088ce3f0563f/studio-logo-gamegenic.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=23cf585e5503ca0809715e2bdced1c36468304d8b9ba20854083b75dc7082a21"/>
</a>
</li>
<li>
<a href="https://www.zmangames.com/" target="_blank">
<img alt="Z-Man Games" src="https://b2b-media-production-ana.s3.amazonaws.com/filer_public/72/dd/72dd392c-3a60-4104-aab5-342ac01c9ff5/studio_logo-zman.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Expires=3600&amp;X-Amz-Date=20210114T190712Z&amp;X-Amz-SignedHeaders=host&amp;X-Amz-Security-Token=FwoGZXIvYXdzEDwaDA5lQY7PCrWLl9n%2FcSLKAQ4lA7yXgga2mSgWomnBbbYB6B9KxBsj%2FkzDvt6eMEFqqjixXkHjv6CLxvLD6ic9uCQhFWlUGPU7HG3%2BGpBVFVDj0fidf9sqC7UTwv7I4hBaeTjxXRbGf7VUPnorSa3Wc0WJ3i4iO9f%2Bt%2FOlLxkPtkzItmjv59zII7J6bZMq3MAHxy9ToMadCTTb3%2BHyabgD4jryAh9SCLb%2F7NPQ3LuYdwGfOWk%2Bt%2BWFDoBd2X%2B7OreNkPMelJSpbS2TQfG4u43woUlY9uunbz4QQgIogqmCgAYyLdEjUbgRvfx0BZvT9bLPBNF79mKKvnvlCe5oCaSSwMKdx3ES11AlRCm%2FUfoprA%3D%3D&amp;X-Amz-Credential=ASIAZZHALS6O3QPY65AY%2F20210114%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Signature=95a80066be59eeaa4e0c8691df7ae51c81d1f419d511cd00a30162866e5fe7d3"/>
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<footer class="footer grid-container full">
<div class="grid-x grid-padding-x">
<div class="cell medium-6">
<div>
<a href="/en/terms-use/">Terms of Use</a>
<span> | </span>
<a href="/en/privacy-policy/">Privacy Policy</a>
<span> | </span>
<a href="/en/vendor-code-conduct/">Vendor Code of Conduct</a>
</div>
</div>
<div class="cell medium-6">
<div>
<span>Questions?</span>
<a href="/en/contact/">Contact Us</a>
</div>
</div>
</div>
<div class="grid-x grid-padding-x">
<div class="cell medium-6">
<a href="/select-region/" target="_blank">
<small>Change Region</small>
<img alt="United States" class="change-region-flag" src="/static/images/us_small.ba10c0fa27a9.png"/>
</a>
</div>
<div class="cell medium-6">
<a href="https://corporate.asmodee.com/" target="_blank">
<small>Corporate</small>
</a>
</div>
</div>
</footer>
</main>
<script src="/static/js/scripts.a0229085d8c0.js" type="text/javascript"></script>
</body>
</html>