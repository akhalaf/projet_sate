<!DOCTYPE html>
<html>
<head>
<title>
Schreibe kostenlos eigene Satire oder Scherzartikel
</title>
<meta charset="utf-8"/>
<meta content="Teile deine eigene Satire oder Scherzartikel in Sekunden bei WhatsApp, Facebook und Twitter. Nimm Freunde &amp; Familie auf den Arm und hab Spaß dabei." name="description"/>
<meta content="fake news, zeitung, fake generator, artikel, medienkompetenz, fun, spaß, satire, hoax, zeitungsente, tatarenmeldung, scherzartikel" name="keywords"/>
<meta content="Schreibe kostenlos eigene Satire oder Scherzartikel" property="og:title"/>
<meta content="Paul Newsman" property="og:site_name"/>
<meta content="https://paulnewsman.com/?ref=magazine&amp;utm_campaign=magazine-root-page-www.blitz-kurier.net&amp;utm_medium=magazine-root-page&amp;utm_source=magazine" property="og:url"/>
<meta content="Teile deine eigene Satire oder Scherzartikel in Sekunden bei WhatsApp, Facebook und Twitter. Nimm Freunde &amp; Familie auf den Arm und hab Spaß dabei." property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://paulnewsman.com/assets/logo_social-d5537aec5366c2cf0f7d820c322904ba13b81834cae035008c1ec83d72c4b046.png" property="og:image"/>
<meta content="1543150622657436" property="fb:app_id"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="d74320d74b2c52c98a909135c383a914" name="p:domain_verify"/>
<link data-turbolinks-track="true" href="/assets/application-e687e78f2d472ccee5914d89aa82e607e2593a76461b13822c100140afe56896.css" media="all" rel="stylesheet"/>
<script data-turbolinks-track="true" src="/assets/application-4ba550ea8d4bc21445d2261265867bd0ac7a2f5f6c5807f483e9316f7251ccfa.js"></script>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="4+55by8OT8Bgo58uaIz2xD1kE0N/4775jluMCBAMT6JL/cMtOunQWNm8ozk0+j8CgJJDBv5M/5GlB93XMXcrOw==" name="csrf-token"/>
<link href="/favicons/paulnewsman/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicons/paulnewsman/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicons/paulnewsman/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicons/paulnewsman/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/favicons/paulnewsman/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#ffffff" name="theme-color"/>
</head>
<body class="pages index" data-fb-app-id="1543150622657436">
<div id="outer_wrapper">
<header id="navigation">
<div class="signup-or-login">
<a class="no-margin" href="/registrieren">Kostenlos registrieren</a>
oder
<a href="/einloggen">Einloggen</a>
</div>
</header>
<div id="inner_wrapper">
<div class="reference_message">
<article>
<div class="message">
<h2>
Heiliger Moses!
</h2>
<p>
Du wurdest ganz schön an der Nase herumgeführt! Diese Zeitung gibt es gar nicht.
Richtig schön verschaukelt, Keule.
</p>
<p>
Wir hätten es dir nicht so leicht machen müssen. Und andere machen es dir auch nicht so leicht. Denk mal drüber nach.
</p>
<p>
Und jetzt ran an die Tasten, schreib deinen eigenen Artikel.
</p>
<p>
<a href="https://www.facebook.com/paulnewsmancom" target="_blank">Folge uns bei Facebook</a>
um eine Auswahl der besten Artikel zu erhalten.
</p>
</div>
<div class="gif">
<img alt="Hahaha" src="/assets/ui/hahaha-ff35e379525d24ed9017c5196a7ad9905eb5543b8be396dfca7b277ee999858f.gif"/>
</div>
</article>
</div>
<section class="header">
<a href="/"><img src="/assets/logo-401ae4e14ffa1c349e7715ee10e6990378cb4635b2e4d4868cfc9ee3b22e488c.png"/>
<div id="eyeleft">
<div id="pupilleft"></div>
</div>
<div id="eyeright">
<div id="pupilright"></div>
</div>
</a><h1>
Schreibe kostenlos eigene
<br/>
Satire oder Scherzartikel
</h1>
</section>
<div id="content">
<section class="corona">
<strong>
<a href="/corona">Wichtiges Corona Update: Jetzt lesen!</a>
</strong>
</section>
<section class="bg">
<div class="intro-text">
und teile diese erfundenen Artikel mit Freunden, Familie, Kollegen oder Sportkameraden :)
</div>
<a class="main-cta" href="/artikel/neu" onclick="ga('send', 'event', 'Internal Links', 'Start - Artikel schreiben');return;">Jetzt
<br/>
Artikel
<br/>
schreiben
</a><div class="textbox">
<p>
Hast du schon immer davon geträumt deine investigativen Recherchen in einer großen deutschen Zeitung zu veröffentlichen? Das wird wohl auf ewig ein Traum bleiben
<i class="fa fa-smile-o"></i>
</p>
<p>
Es sei denn du hast einen Heidenspaß daran deine Freunde und Kollegen zu veräppeln. Paul Newsman, dein Sprungbrett in das kalte Haifischbecken der Medienlandschaft.
</p>
<p>
 
<br/>
Paul Newsman bringt dich groß raus. Veröffentliche deinen vermeintlich seriösen Artikel im Kölner Abendblatt oder befriedige die voyeuristische Gier des Boulevards mit exklusivem Regenbogenjournalismus im Blitz-Kurier.
</p>
</div>
<div class="trumpet"></div>
<section class="magazine-logos">
<div class="magazine">
<img alt="Logo front" src="/assets/magazines/koelner-abendblatt/logo_front-10f21d1dd05a0d04f8a31ffdcdf69a22d8c20648738092853c6ec269cd08d87e.png"/>
<a href="/artikel/neu?magazine=koelner-abendblatt" onclick="ga('send', 'event', 'Internal Links', 'Start - Artikel schreiben - Kölner Abendblatt');return;">Jetzt Artikel schreiben</a>
</div>
<div class="magazine">
<img alt="Logo front" src="/assets/magazines/blitz-kurier/logo_front-9343900a9ae6c5ffd72231f2b34bb95fe9dc7da643d53713f2d4d32c10933216.png"/>
<a href="/artikel/neu?magazine=blitz-kurier" onclick="ga('send', 'event', 'Internal Links', 'Start - Artikel schreiben - Blitz Kurier');return;">Jetzt Artikel schreiben</a>
</div>
<div class="magazine">
<img alt="Logo front" src="/assets/magazines/britta/logo_front-34cfcec519be4bb7833b53c4cee4f3ba514b4e27a336f131f02abf55a289a5de.png"/>
<a href="/artikel/neu?magazine=britta" onclick="ga('send', 'event', 'Internal Links', 'Start - Artikel schreiben - Britta');return;">Jetzt Artikel schreiben</a>
</div>
<div class="magazine">
<img alt="Logo front" src="/assets/magazines/graetsche/logo_front-a8534b4127994cbb8280f47ce3289c8ee6ca9340feedb196276fe9a62e38d4f8.png"/>
<a href="/artikel/neu?magazine=graetsche" onclick="ga('send', 'event', 'Internal Links', 'Start - Artikel schreiben - Grätsche');return;">Jetzt Artikel schreiben</a>
</div>
</section>
<section class="demo-articles">
<article>
<div class="header">
<h2>
Beispiel
<img alt="Glasses" src="/assets/ui/glasses-d2f41c2a24aa414b295d197a1b5d6a97efc15324db513c8b0494fbfd0c7c828f.png"/>
Artikel
</h2>
</div>
<div class="table">
<div class="article">
<a href="https://www.koelner-abendblatt.de/artikel/justiz/usa/donald-trump-begnadigt-den-tiger-king-joe-exotic-21295756.html" onclick="ga('send', 'event', 'Article Links', 'Frontpage Article 1');return;" target="_blank"></a>
<header style="background-image: url(https://dm1fis1lh7pbl.cloudfront.net/articles/21295756/card.jpg?1590745063)"></header>
<div class="title">
Donald Trump begnadigt den "Tiger King" Joe Exotic
</div>
<div class="read">
» Artikel lesen
<span class="magazine">
/ Kölner Abendblatt
</span>
</div>
</div>
<div class="article">
<a href="https://www.koelner-abendblatt.de/artikel/politik/chemtrails/afd/bundesregierung-bestaetigt-chemtrailprojekte-49238492.html" onclick="ga('send', 'event', 'Article Links', 'Frontpage Article 2');return;" target="_blank"></a>
<header style="background-image: url(https://dm1fis1lh7pbl.cloudfront.net/articles/49238492/card.jpg?1501340261)"></header>
<div class="title">
Bundesregierung bestätigt Chemtrailprojekte
</div>
<div class="read">
» Artikel lesen
<span class="magazine">
/ Kölner Abendblatt
</span>
</div>
</div>
<div class="article">
<a href="https://www.koelner-abendblatt.de/artikel/politik/interview/hass-first-lebensqualitaet-second-das-schicksal-einer-afd-waehlerin-93416773.html" onclick="ga('send', 'event', 'Article Links', 'Frontpage Article 3');return;" target="_blank"></a>
<header style="background-image: url(https://dm1fis1lh7pbl.cloudfront.net/articles/93416773/card.jpg?1506198060)"></header>
<div class="title">
Hass First. Lebensqualität Second. Das Schicksal einer AfD-Wählerin!
</div>
<div class="read">
» Artikel lesen
<span class="magazine">
/ Kölner Abendblatt
</span>
</div>
</div>
</div>
<div class="more-button">
<a href="/archiv" onclick="ga('send', 'event', 'Internal Links', 'Start - Weitere Artikel');return;">Weitere Artikel</a>
</div>
</article>
</section>
</section>
<section class="press">
<article>
<h2>Bekannt aus:</h2>
<div class="logos">
<div class="logo">
<a href="http://www.allgemeine-zeitung.de/politik/rheinland-pfalz/das-internet-portal-paul-newsman-aus-koblenz-will-mit-fake-news-entlarvend-wirken_18086951.htm" onclick="ga('send', 'event', 'External Links - Press', 'Allgemeine Zeitung', 'http://www.allgemeine-zeitung.de/politik/rheinland-pfalz/das-internet-portal-paul-newsman-aus-koblenz-will-mit-fake-news-entlarvend-wirken_18086951.htm');return;" target="_blank"><img alt="Allgemeine zeitung" src="/assets/press/allgemeine_zeitung-e9de72ccd145351390d06956772d8daf33934c0dd9684c8fe63b2b122f64b86b.png"/>
</a></div>
<div class="logo">
<a href="https://www.n-joy.de/multimedia/Fakes-im-Netz-So-erkennt-ihr-falsche-Bilder-Videos-und-Nachrichten,fakesimnetz100.html" onclick="ga('send', 'event', 'External Links - Press', 'NDR', 'http://www.n-joy.de/multimedia/Fakes-im-Netz-So-erkennt-ihr-falsche-Bilder-Videos-und-Nachrichten,fakesimnetz100.html');return;" target="_blank"><img alt="Ndr" src="/assets/press/ndr-8613d9f0f3898ffa6879d6da79405b76ba02db097cadfe0078ad2ffcff14ed7b.svg"/>
</a></div>
<div class="logo">
<a href="https://www1.wdr.de/mediathek/video/sendungen/westpol/video-westpol-354.html" onclick="ga('send', 'event', 'External Links - Press', 'WDR', 'http://www1.wdr.de/mediathek/video/sendungen/westpol/video-westpol-354.html');return;" target="_blank"><img alt="Wdr" src="/assets/press/wdr-2dbb8545622e34a37096767f6bb09e22adc213e62b109dfa09148e409bb4df57.svg"/>
</a></div>
<div class="logo">
<a href="http://www.rhein-zeitung.de/region/lokales/koblenz_artikel,-enftagung-in-koblenz-pegida-faellt-auf-satire-ueber-rheinmoselhalle-rein-update-_arid,1596839.html" onclick="ga('send', 'event', 'External Links - Press', 'Rhein-Zeitung', 'https://www.rhein-zeitung.de/region/lokales/koblenz_artikel,-enftagung-in-koblenz-pegida-faellt-auf-satire-ueber-rheinmoselhalle-rein-update-_arid,1596839.html');return;" target="_blank"><img alt="Rhein zeitung" src="/assets/press/rhein_zeitung-9aab2a07c2dfc3bc0a21bb7fb3053678db263feba9f8323b443679a5b7efded4.svg"/>
</a></div>
<div class="logo">
<a href="https://www.buzzfeed.com/karstenschmehl/satire-und-luegen-sind-nicht-das-gleiche?utm_term=.mbdBNaEGY#.qxKWxnYvz" onclick="ga('send', 'event', 'External Links - Press', 'Buzzfeed', 'https://www.buzzfeed.com/karstenschmehl/satire-und-luegen-sind-nicht-das-gleiche?utm_term=.xnEX4Lp0q#.dgb5W6l2K');return;" target="_blank"><img alt="Buzzfeed" src="/assets/press/buzzfeed-33b99587f6e16e34cf86248423f4af009c07b5090f74bcb3eb832f210467329e.png"/>
</a></div>
<div class="logo">
<a href="http://www.mimikama.at/?s=paul+newsman" onclick="ga('send', 'event', 'External Links - Press', 'Mimikama', 'http://www.mimikama.at/?s=paul+newsman');return;" target="_blank"><img alt="Mimikama" src="/assets/press/mimikama-6de33ee35b652053d4a3a27bff71a20d7eccef721ce747177c6ad8b02faee798.png"/>
</a></div>
<div class="logo">
<a href="https://correctiv.org/echtjetzt/artikel/2017/08/17/satire-oder-fake-24aktuelles-paul-newsman-koelner-abendblatt-chemtrails-kondensstreifen/" onclick="ga('send', 'event', 'External Links - Press', 'Correctiv', 'https://correctiv.org/echtjetzt/artikel/2017/08/17/satire-oder-fake-24aktuelles-paul-newsman-koelner-abendblatt-chemtrails-kondensstreifen/');return;" target="_blank"><img alt="Correctiv" src="/assets/press/correctiv-fd54479c7aeea631887760711fbc4d53176989c93399cad298c0a1396f6ee35f.png"/>
</a></div>
<div class="logo">
<a href="http://www.sueddeutsche.de/digital/viel-news-um-wenig-fake-welche-rolle-falschmeldungen-im-wahlkampf-spielten-1.3678429" onclick="ga('send', 'event', 'External Links - Press', 'Süddeutsche Zeitung', 'http://www.sueddeutsche.de/digital/viel-news-um-wenig-fake-welche-rolle-falschmeldungen-im-wahlkampf-spielten-1.3678429');return;" target="_blank"><img alt="Sz" src="/assets/press/sz-f1b9f75d875fc8ac179bb64d43b20dcf15785ded21961c1f278fdd3bed0bbb22.png"/>
</a></div>
<div class="logo">
<a href="https://de.sputniknews.com/panorama/20170805316877321-bundesregierung-bestaetigt-chemtrialprojekte-reaktion/" onclick="ga('send', 'event', 'External Links - Press', 'Sputnik', 'https://de.sputniknews.com/panorama/20170805316877321-bundesregierung-bestaetigt-chemtrialprojekte-reaktion/');return;" target="_blank"><img alt="Sputnik" src="/assets/press/sputnik-047946759ea91db30845dedcb1ba5528df677c62192b642792c00f728881e5ce.png"/>
</a></div>
<div class="logo">
<a href="http://www.bild.de/bild-plus/politik/inland/fake-news/kommen-die-fakenews-noch-52879398.bild.html" onclick="ga('send', 'event', 'External Links - Press', 'Bild', 'http://www.bild.de/bild-plus/politik/inland/fake-news/kommen-die-fakenews-noch-52879398.bild.html');return;" target="_blank"><img alt="Bild" src="/assets/press/bild-7eb5c60b0723e7bc0c31553adf89ee040c2d88cd6e0f02fac4bb9783c6db907c.svg"/>
</a></div>
<div class="logo">
<a href="http://www.haz.de/Hannover/Aus-der-Stadt/Uebersicht/Trump-kauft-das-Ihme-Zentrum-nicht" onclick="ga('send', 'event', 'External Links - Press', 'Hannoversche Allgemeine', 'http://www.haz.de/Hannover/Aus-der-Stadt/Uebersicht/Trump-kauft-das-Ihme-Zentrum-nicht');return;" target="_blank"><img alt="Haz" src="/assets/press/haz-e1dc0c18f77a02925645d4e34e7bda77c7d614ff63c513274325528f0be0272d.svg"/>
</a></div>
<div class="logo">
<a href="https://paulnewsman.s3.amazonaws.com/press/20170928-rockland-annette-radueg.mp3" onclick="ga('send', 'event', 'External Links - Press', 'Rockland - Annette Radüg', 'https://paulnewsman.s3.amazonaws.com/press/20170928-rockland-annette-radueg.mp3');return;" target="_blank"><img alt="Rockland" src="/assets/press/rockland-ac173e0c1f82c58a5e51f9df63dbdbd99ef19a56dd2fd556f64a5d22730b9d5d.jpg"/>
</a></div>
<div class="logo">
<a href="http://www.mz-web.de/dessau-rosslau/petry-wechselt-in-die-cdu--afd-politiker-faellt-auf-fake-news-herein-28498126" onclick="ga('send', 'event', 'External Links - Press', 'Mitteldeutsche Zeitung', 'http://www.mz-web.de/dessau-rosslau/petry-wechselt-in-die-cdu--afd-politiker-faellt-auf-fake-news-herein-28498126');return;" target="_blank"><img alt="Mitteldeutsche zeitung" src="/assets/press/mitteldeutsche_zeitung-915fd0fd5a679c6be935522421019031a4543c04681ea789e95fbed71fc868bb.png"/>
</a></div>
</div>
</article>
</section>
<section class="faq">
<article>
<h2>
Häufig gestellte <i>?</i> Fragen
</h2>
<div class="questions">
<div class="column">
<div class="question">
<header>
Warum funktioniert Paul Newsman?
</header>
<div class="answer">
<ul>
<li>
All unsere Magazine haben ihre eigene Domain und die Magazine könnten wirklich existieren
</li>
<li>
Alle Magazine haben ein passendes und hochwertiges Design der entsprechenden Branche
</li>
<li>
Das erste, was man als Empfänger sieht, ist der Artikel-Link. Dieser sieht genauso aus wie bei realen Online Zeitungen
</li>
<li>
Jeder Artikel wird schnell geladen und ist für Desktop, Notebook, Tablet und Smartphone optimiert
</li>
</ul>
Paul Newsman funktioniert in den allermeisten Fällen, wenigstens für 3 Sekunden.
</div>
</div>
<div class="question">
<header>
Erfährt der Leser meines Artikels, dass es sich nicht um einen seriösen Artikel handelt?
</header>
<div class="answer">
Um Missbrauch vorzubeugen wird dem Leser unter jedem Artikel eine Aufklärung angezeigt.
Dort steht also, dass dein Artikel mit Paul Newsman erstellt wurde und nicht als seriöse Quellenangabe angesehen werden darf.
<br/>
<br/>
Der Witz besteht vor allem in den ersten Schocksekunden. Jeder kritische Leser soll daher die Möglichkeit haben die Ente schnell aufdecken zu können.
</div>
</div>
</div>
<div class="column">
<div class="question">
<header>
Kostet Paul Newsman etwas?
</header>
<div class="answer">
Nein. Du kannst kostenlos so viele Artikel schreiben wie du möchtest. Als Gast (ohne Registrierung)
sind deine Artikel jedoch nur 24 Stunden online und können höchstens 10 mal gelesen werden. Nach einer
schmerzlosen und schnellen Registrierung gilt diese Einschränkung nicht mehr.
</div>
</div>
<div class="question">
<header>
Sehe ich Statistiken zu meinen Artikeln?
</header>
<div class="answer">
Ja. Du siehst in den Artikeldetails wie oft dein Artikel gelesen und bei Facebook geteilt, kommentiert und geliked wurde.
</div>
</div>
<div class="question">
<header>
Wie darf ich Paul Newsman verwenden?
</header>
<div class="answer">
Bitte lies unbedingt unsere <a href="/nutzungsbedingungen">Nutzungsbedingungen</a>.
Dies ist wichtig, da wir nicht jeden Inhalt akzeptieren, z.B. Artikel, die nur die Absicht haben, Menschen zu
mobben. Ja, wir löschen wirklich Artikel, die dagegen verstoßen!
</div>
</div>
<div class="question">
<header>
Wie kam es zu Paul Newsman?
</header>
<div class="answer">
Eigentlich wollte nur jemand einen Kollegen verarschen.
Heraus kam dieses grandiose Tool um die Medienkompetenz deiner Freunde zu testen!
<a href="https://medium.com/@paulnewsman/paul-newsman-geht-online-und-fragt-wie-medienkritisch-und-kompetent-ist-eigentlich-deutschland-c2d83d05dbc0" onclick="ga('send', 'event', 'External Links', 'Medium', 'https://medium.com/@paulnewsman/paul-newsman-geht-online-und-fragt-wie-medienkritisch-und-kompetent-ist-eigentlich-deutschland-c2d83d05dbc0');return;" target="_blank">Lies die ganze Story hier.</a>
</div>
</div>
</div>
</div>
</article>
</section>
</div>
<footer id="footer">
<div class="logo">
<a href="/"><img alt="Paul footer" src="/assets/paul_footer-d05b140d0622dda97ed8a93bdd9356878199c4a975c66e44e3baa553a8dc5ab7.png"/>
</a></div>
<div class="text">
<ul>
<li class="blog">
<a href="/archiv">Archiv</a>
</li>
<li class="blog">
<a href="https://medium.com/@paulnewsman" target="_blank">Blog</a>
</li>
<li>
<a href="/presse">Presse</a>
</li>
<li>
<a href="/nutzungsbedingungen">Nutzungsbedingungen</a>
</li>
<li>
<a href="/datenschutz">Datenschutz</a>
</li>
<li>
<a href="/impressum">Impressum</a>
</li>
</ul>
</div>
</footer>
<div id="social_footer">
<a href="https://www.facebook.com/paulnewsmancom" rel="noreferrer" target="_blank"><i class="fa fa-facebook"></i>
</a><a href="https://twitter.com/paulnewsmancom" rel="noreferrer" target="_blank"><i class="fa fa-twitter"></i>
</a><a href="https://medium.com/@paulnewsman" rel="noreferrer" target="_blank"><i class="fa fa-medium"></i>
</a><a href="https://www.instagram.com/paulnewsman/" rel="noreferrer" target="_blank"><i class="fa fa-instagram"></i>
</a><a href="https://de.pinterest.com/paulnewsman/" rel="noreferrer" target="_blank"><i class="fa fa-pinterest"></i>
</a></div>
</div>
</div>
</body>
<script>

    if (!window.ga) {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-87432253-3', 'auto');
      ga('set', 'anonymizeIp', true);
      ga('set', 'transport', 'beacon');
    }

      ga('set', 'userId', '');


      ga('send', { hitType: 'pageview', location: location.href.split('#')[0] });


  </script>
</html>
