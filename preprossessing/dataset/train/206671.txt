<!DOCTYPE html>
<html lang="en-US" xmlns:og="http://ogp.me/ns#">
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-9533848-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-9533848-1');
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<meta content="Voice Compare: X-Men - Sasquatch - Behind The Voice Actors" name="title"/>
<meta content="Listen to sound clips and see images of all the different voice over actors who have been the voice of Sasquatch in X-Men" name="description"/>
<meta content="voice of Sasquatch, Sasquatch voice actors, Sasquatch sound clips, Sasquatch sounds, X-Men, Sasquatch, " name="keywords"/>
<meta content="Voice Compare - Sasquatch" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.behindthevoiceactors.com/voice-compare/X-Men/Sasquatch/" property="og:url"/>
<meta content="https://www.behindthevoiceactors.com/_img/voice_compare/thumbs/1648_thumb.jpg" property="og:image"/>
<meta content="Behind The Voice Actors" property="og:site_name"/>
<meta content="Listen to sound clips and see images of all the different voice over actors who have been the voice of Sasquatch in X-Men" property="og:description"/>
<meta content="bzyEawXDJqy0LACQhtEg4xex7pChYa18XBI2GAg6kJg" name="google-site-verification"/>
<title>Voice Compare: X-Men - Sasquatch - Behind The Voice Actors</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="https://www.behindthevoiceactors.com/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.behindthevoiceactors.com/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.behindthevoiceactors.com/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.behindthevoiceactors.com/site.webmanifest" rel="manifest"/>
<link href="https://www.behindthevoiceactors.com/favicon.ico?v=3" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.behindthevoiceactors.com/_css/master1v18.css" rel="stylesheet" type="text/css"/>
<link href="https://www.behindthevoiceactors.com/_css/mobile1v18.css" rel="stylesheet" type="text/css"/> <!--AdThrive Head Tag -->
<script>
	(function(w, d) {
	w.adthrive = w.adthrive || {};
	w.adthrive.cmd = w.adthrive.cmd || [];
	w.adthrive.plugin = 'adthrive-ads-1.0.41';
	w.adthrive.host = 'ads.adthrive.com';
	var s = d.createElement('script');
	s.async = true;
	s.referrerpolicy='no-referrer-when-downgrade';
	s.src = 'https://' + w.adthrive.host + '/sites/5b914efa377d57533c0af714/ads.min.js?referrer=' + w.encodeURIComponent(w.location.href);
	var n = d.getElementsByTagName('script')[0];
	n.parentNode.insertBefore(s, n);
	})(window, document);
	</script>
<!--End AdThrive Head Tag -->
</head>
<body class="layout_voicecompare">
<div class="vs-context-menu"></div>
<div class="popup popup_login">
<div class="bg_bucket_title" style="padding-top: 6px; height: 24px;">LOGIN</div>
<div class="login_content">
<form action="https://www.behindthevoiceactors.com/forums/login.php" method="post" onsubmit="md5hash(vb_login_password,vb_login_md5password,vb_login_md5password_utf)">
<script src="https://staticf.behindthevoiceactors.com/behindthevoiceactors/forums/clientscript/vbulletin_md5.js" type="text/javascript"></script>
<b>USERNAME</b>:<br/>
<input class="login_field" id="navbar_username" name="vb_login_username" onfocus="if (this.value == 'User Name') this.value = '';" style="margin-bottom: 10px;" type="text"/>
<b>PASSWORD</b>:<br/>
<input class="login_field" name="vb_login_password" type="password"/>
<a href="https://www.behindthevoiceactors.com/forums/login.php?do=lostpw" style="font-size: 10px;">Forgot password?</a>
<div style="text-align: center;">
<label for="cb_cookieuser_navbar"><input checked="checked" id="cb_cookieuser_navbar" name="cookieuser" type="checkbox" value="1"/><span style="font-size: 10px;">Remember Me?<br/></span></label><input class="button" style="margin: 5px 0 10px 0;" type="submit" value=" LOGIN "/>
<br/>
<span style="color: #777;">Don't have an account?</span> <a href="https://www.behindthevoiceactors.com/forums/register.php?s=">Join BTVA</a>
</div>
<input name="s" type="hidden" value="/"/>
<input name="do" type="hidden" value="login"/>
<input name="vb_login_md5password" type="hidden"/>
<input name="vb_login_md5password_utf" type="hidden"/>
</form> </div>
</div>
<div class="popup" id="popup_comments"></div>
<div class="popup" id="popup_shoutout_who">
<div class="bg_bucket_title bg_bucket_title_html_text">Members Who Shout This Out!<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div id="shoutout_who_container" style="overflow-y: scroll;"></div>
</div>
<div class="popup" id="popup_confirmed_credit">
<div class="bg_bucket_title bg_bucket_title_html_text">Confirmed Credits<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div style="height: 360px; overflow-y: scroll;"><img alt="" src=""/></div>
</div>
<div id="header">
<a href="javascript:void(0);" id="nav_mobile_menu_burger" onclick="menu_burger_expand('navmob')"><span>MENU</span><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/" id="logo" title="Behind The Voice Actors"></a>
<div id="login_logout">
<div class="top_links">
<a href="javascript:;" id="search_button"></a>
<div>
<a class="login_link" href="javascript:;">LOGIN</a> | 
				<a href="https://www.behindthevoiceactors.com/forums/register.php?s=">JOIN BTVA</a> <div class="accounts_alert"></div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
<a href="#top" id="back_to_top" title="Back to Top"><svg height="50" id="back_to_top_arrow" width="50">
<circle cx="25" cy="24" fill="#666666" r="20" stroke="white" stroke-width="1"></circle>
<text dominant-baseline="middle" fill="white" text-anchor="middle" x="50%" y="50%">➜</text>
</svg></a>
<div id="search_container">
<form action="https://www.behindthevoiceactors.com/search/" class="search_form" method="GET">
<select id="search_type" name="search_type">
<option value="all">All</option><option value="va">Voice Actors</option><option value="med">Titles</option><option value="cg">Characters (Reprisals)</option><option value="char">Characters (Single)</option><option value="vc">Voice Compares</option> </select>
<input id="search_keyword" maxlength="150" name="search_keyword" placeholder="Search ..." type="text" value=""/>
<input class="orange" id="search_submit" name="search_submit" type="submit" value=""/>
<!--<input id="search_sort" name="search_sort" type="hidden" value="" />-->
	 
	<div class="clear"></div>
</form></div>
<div id="container">
<div id="navmob">
<a class="navmob_top" href="https://www.behindthevoiceactors.com/voice-actors/">▪  Voice Actors</a>
<a class="navmob_top" href="https://www.behindthevoiceactors.com/characters/">▪  Characters</a>
<div class="clear"></div>
<a href="https://www.behindthevoiceactors.com/tv-shows/">▪  TV Shows</a>
<a href="https://www.behindthevoiceactors.com/movies/">▪  Movies</a>
<a href="https://www.behindthevoiceactors.com/video-games/">▪  Video Games</a>
<a href="https://www.behindthevoiceactors.com/shorts/">▪  Shorts</a>
<a href="https://www.behindthevoiceactors.com/rides-attractions/">▪  Attractions</a>
<a href="https://www.behindthevoiceactors.com/commercials/">▪  Commercials</a>
<div class="divider navmob_divider"></div>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/team-ups/">•  Team Ups</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/voice-compare/">•  Voice Compare</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/voice-directors/">•  Voice Directors</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/franchises/">•  Franchises</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/news/">•  News</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/top-listings/?type=pop_va_weekly">•  Top Listings</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/coming-soon/">•  Coming Soon</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/quotes/voice-actors/">•  VA Quotes</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/casting-call/">•  Casting Call</a>
<a class="navmob_sub" href="https://www.behindthevoiceactors.com/forums/">•  Forums</a>
</div>
<div class="navbar" id="nav">
<a href="javascript:void(0);" id="nav_menu_burger" onclick="menu_burger_expand('nav')"><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/voice-actors/">Voice Actors</a>
<a href="https://www.behindthevoiceactors.com/characters/">Characters</a>
<a href="https://www.behindthevoiceactors.com/tv-shows/">TV Shows</a>
<a href="https://www.behindthevoiceactors.com/movies/">Movies</a>
<a href="https://www.behindthevoiceactors.com/video-games/">Video Games</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/shorts/">Shorts</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/rides-attractions/">Attractions</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/commercials/">Commercials</a>
</div>
<div class="navbar" id="subnav">
<a href="javascript:void(0);" id="subnav_menu_burger" onclick="menu_burger_expand('subnav')"><div></div><div></div><div></div></a>
<a href="https://www.behindthevoiceactors.com/team-ups/">Team Ups</a>
<a href="https://www.behindthevoiceactors.com/voice-compare/">Voice Compare</a>
<a href="https://www.behindthevoiceactors.com/voice-directors/">Voice Directors</a>
<a href="https://www.behindthevoiceactors.com/franchises/">Franchises</a>
<a href="https://www.behindthevoiceactors.com/news/">News</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/top-listings/?type=pop_va_weekly">Top Listings</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/coming-soon/">Coming Soon</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/quotes/voice-actors/">VA Quotes</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/casting-call/">Casting Call</a>
<a class="nav_mob" href="https://www.behindthevoiceactors.com/forums/">Forums</a>
</div>
<script type="text/javascript">
	/*$(function() {
		if (document.body.scrollWidth > 660) {
			document.cookie = 'my_browser_width = desktop';
		}
		else {
			document.cookie = 'my_browser_width = mobile';
		}
	});*/
	</script>
<div class="feature_banner_small_info_wrap vc_page" id="content_full">
<ol class="breadcrumbs_padding" id="breadcrumbs" itemscope="" itemtype="https://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="https://www.behindthevoiceactors.com/" itemprop="item"><span itemprop="name">Home</span></a><meta content="1" itemprop="position"/>
</li><b>›</b><li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="https://www.behindthevoiceactors.com/voice-compare/" itemprop="item"><span itemprop="name">Voice Compare</span></a><meta content="2" itemprop="position"/>
</li><b>›</b><b class="breadcrumb_back">‹</b><li class="breadcrumb_back" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="https://www.behindthevoiceactors.com/voice-compare/X-Men/" itemprop="item"><span itemprop="name">X-Men</span></a><meta content="3" itemprop="position"/>
</li><b>›</b><li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><span itemprop="name">Sasquatch</span><meta content="4" itemprop="position"/>
</li></ol> <div class="polaroid_img">
<img alt="Sasquatch" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/voice_compare/1648.jpg"/>
</div>
<div class="vc_banner" id="feature_banner_small">
<img alt="X-Men" id="feature_banner2_small" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/franchises/3.jpg"/>
<img alt="X-Men" id="feature_banner1_small" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/franchises/3.jpg"/>
<img alt="Voice Compare" id="banner_title" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/banner_title_voice_compare.png" style="width: 500px;"/>
</div>
<h1 id="medium_heading">Sasquatch</h1>
<h6 class="orange">Comparison of the voice actors who have been the voice of Sasquatch with sound clips and images.</h6>
<p id="vc_desc"><b class="label_colon">Number of Comparisons:</b> 4<br/><b class="label_colon">Franchise:</b> <a class="label_colon" href="https://www.behindthevoiceactors.com/franchises/X-Men/">X-Men</a><br/><br/>
<br/></p>
<div class="clear"></div>
</div>
<div id="content_wide_left">
<div class="main" style="margin-top: -10px;">
<div class="divider"></div><div style="padding: 5px; background: #EEE; margin: -7px 0;">
			Created on Oct 15 2011<select id="sort_dropdown" style="margin: 0;"><option value="otn">Oldest Titles</option><option value="nto">Latest Titles</option><option value="vot">Most Votes</option><option value="pop">Most Popular VA</option><option value="rec">Most Recent Clip</option></select><div class="clear"></div></div><div class="divider"></div><div class="vc_comparison_container"><div class="vc_comparison_title"><a href="https://www.behindthevoiceactors.com/tv-shows/X-Men/">X-Men</a> <span class="title_year">(1992)</span></div><a class="gray_border_img" href="https://www.behindthevoiceactors.com/tv-shows/X-Men/Sasquatch/" title="X-Men - Sasquatch"><img alt="Sasquatch" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/sasquatch-x-men-83.2_thumb.jpg"/></a><a class="gray_border_img" href="https://www.behindthevoiceactors.com/Harvey-Atkin/" style=" width: 130px; margin-left: -2px;" title="Harvey Atkin"><img alt="Harvey Atkin" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/harvey-atkin-52_thumb.jpg" style="width: 130px; height: 148px;"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/Harvey-Atkin/">Harvey Atkin</a><a class="play_sound_clip" href="javascript:;" rel="https://www.behindthevoiceactors.com/_audio/voice_compare/5285.mp3|270" title="Play Sound Clip"></a></div><div class="vc_comparison_container"><div class="vc_comparison_title"><a href="https://www.behindthevoiceactors.com/tv-shows/The-Incredible-Hulk-1996/">The Incredible Hulk (1996)</a> <span class="title_year">(1996)</span></div><a class="gray_border_img" href="https://www.behindthevoiceactors.com/tv-shows/The-Incredible-Hulk-1996/Sasquatch/" title="The Incredible Hulk (1996) - Sasquatch"><img alt="Sasquatch" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/sasquatch-the-incredible-hulk-1996-0.85_thumb.jpg"/></a><a class="gold_border_img" href="https://www.behindthevoiceactors.com/Clancy-Brown/" style=" width: 130px; margin-left: -2px;" title="Clancy Brown"><img alt="Clancy Brown" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/clancy-brown-42.9_thumb.jpg" style="width: 130px; height: 148px;"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/Clancy-Brown/">Clancy Brown</a><a class="play_sound_clip" href="javascript:;" rel="https://www.behindthevoiceactors.com/_audio/voice_compare/5286.mp3|270" title="Play Sound Clip"></a></div><div class="vc_comparison_container"><div class="vc_comparison_title"><a href="https://www.behindthevoiceactors.com/video-games/Marvel-Super-Hero-Squad-Online/">Marvel Super Hero Squad Online</a> <span class="title_year">(2011)</span></div><a class="gray_border_img" href="https://www.behindthevoiceactors.com/video-games/Marvel-Super-Hero-Squad-Online/Sasquatch/" title="Marvel Super Hero Squad Online - Sasquatch"><img alt="Sasquatch" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/sasquatch-marvel-super-hero-squad-online-35.1_thumb.jpg"/></a><a class="gray_border_img" href="https://www.behindthevoiceactors.com/Charlie-Adler/" style=" width: 130px; margin-left: -2px;" title="Charlie Adler"><img alt="Charlie Adler" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/charlie-adler-3.87_thumb.jpg" style="width: 130px; height: 148px;"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/Charlie-Adler/">Charlie Adler</a><a class="play_sound_clip" href="javascript:;" rel="https://www.behindthevoiceactors.com/_audio/voice_compare/7552.mp3|270" title="Play Sound Clip"></a></div><div class="vc_comparison_container"><div class="vc_comparison_title"><a href="https://www.behindthevoiceactors.com/tv-shows/Wolverine-versus-Sabretooth/">Wolverine versus Sabretooth</a> <span class="title_year">(2014)</span></div><a class="gray_border_img" href="https://www.behindthevoiceactors.com/tv-shows/Wolverine-versus-Sabretooth/Sasquatch/" title="Wolverine versus Sabretooth - Sasquatch"><img alt="Sasquatch" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/chars/thumbs/sasquatch-wolverine-versus-sabretooth-88.7_thumb.jpg"/></a><a class="gray_border_img" href="https://www.behindthevoiceactors.com/Trevor-Devall/" style=" width: 130px; margin-left: -2px;" title="Trevor Devall"><img alt="Trevor Devall" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/actors/thumbs/trevor-devall-98_thumb.jpg" style="width: 130px; height: 148px;"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/Trevor-Devall/">Trevor Devall</a><a class="play_sound_clip" href="javascript:;" rel="https://www.behindthevoiceactors.com/_audio/voice_compare/9581.mp3|270" title="Play Sound Clip"></a></div>
<div class="clear"></div>
<div class="divider"></div>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.js" type="text/javascript"></script><script type="text/javascript">
var hash = '';

$(document).ready(function() {
	var comment_note = $('#comment_note').val();
	var default_comment_position = 0;
	
	if (window.location.href.indexOf("#comment") > -1) {
		default_comment_position = -1;
		hash = window.location.hash.substr(1);
	}
	$('#add_new_reply').hide();
	
	$('#comments_load').click(function() {
		$(this).remove();
		$('#add_new_reply').show();
		render_the_comments(0);
		//render_the_comments(default_comment_position);
		render_reply_box($('#reply_to'), 0, 0, '','first_load',comment_note);
	});
    
     $('#add_new_reply').click(function() {
		var comment_note = $('#comment_note').val();
        $('.comments_reply_container').html('').hide();
        render_reply_box($('#reply_to'), 0, 0, '', '',comment_note);
    });
});
function render_reply_box(comments_reply_container, parent_id, reply_id, username, first_load, comment_note) {
    $.post('https://www.behindthevoiceactors.com/comments_reply_box.php', {
        parent_id : parent_id, reply_id : reply_id, username : username, comment_note : comment_note, id_sub : 'submit_comment', id_comment : 'comment', id_parent_comment_id : 'parent_comment_id', id_reply_user_id : 'reply_user_id'
    }, function(response) {
        var returnVal = jQuery.trim(response);
        comments_reply_container.html(returnVal).show();
		
		if (first_load != '') {
			$('.post_comment_login_link').click(function() {
				$('.popup_login').jqm({overlay:70,toTop:true}).jqmShow();
			});
			$('.post_comment_validate_link').click(function() {
				$('.popup_validate').jqm({overlay:70,toTop:true}).jqmShow();
			});
		}			
		
		$('#submit_comment').unbind('click').bind('click', function() {
			var button = $(this);
			button.hide().next().show();
			var comment = $('#comment').val();
			var parent_comment_id = $('#parent_comment_id').val();
			var reply_user_id = $('#reply_user_id').val();
			var medium = $('#medium').val();
			var medium_id = $('#medium_id').val();
			
			if (comment != '') {
				if (comment.length < 2000) {
					$.post('https://www.behindthevoiceactors.com/add_comment.php', {
						comment : comment
						, parent_comment_id : parent_comment_id
						, reply_user_id : reply_user_id
						, medium : medium
						, medium_id : medium_id
					}, function(response){
						var returnVal = jQuery.trim(unescape(response));
						if (returnVal != '') {
							var member = $('#my_username').html();
							var member_friendly = $('#my_username').attr('href');
							var avatar = $('#my_avatar img').attr('src');
							vals = returnVal.split('|',2);
							var new_id = vals[0];
							var date = vals[1];
							var css = (parent_comment_id == 0) ? 'comments_container' : 'comments_child_container';
							var comment_formatted = nl2br(comment,true);
							
							var html = $('<div class="'+css+'"><a name="comment'+new_id+'" href="https://www.behindthevoiceactors.com/members/'+member_friendly+'/" class="avatar_thumb"><img src="'+avatar+'" alt="'+member+'"></a><b class="orange"><a href="https://www.behindthevoiceactors.com/members/'+member_friendly+'/">'+member+'</a></b><span class="comments_date"><br />said at '+date+'</span><div class="comments_description comments_description_child">'+comment_formatted+'</div><a rel="'+new_id+'|'+parent_comment_id+'|'+member+'" href="javascript:;" class="reply reply_link reply_child">Reply</a></div></div>').fadeIn('slow');
							$('#reply_to_'+parent_id).html('').hide();
							if (parent_comment_id == 0)
								$('#reply_to').after(html);
							else
								$('#reply_to_'+parent_id).parent().append(html);
							render_reply_box($('#reply_to'), 0, '', '','');
						}
						else
							window.location.reload();
					});
				}
				else {
					alert('Please keep your comments less than 2000 characters.');
					button.show().next().hide();
				}
			}
			else {
				$('#error_fill_in').show();
				button.show().next().hide();
			}
		});
		$('.cancel_reply_button').unbind('click').bind('click', function() {
			var comment_note = $('#comment_note').val();
			$('.comments_reply_container').html('').hide();
			render_reply_box($('#reply_to'), 0, '', '',comment_note);
		});
    });
}
function render_the_comments(current_position) {
    $.post('https://www.behindthevoiceactors.com/comments_get.php', {
        comments_medium : 'voicecompare', comments_medium_id_label : 'VoiceCompareID', comments_medium_id : '1648', current_position : current_position
    }, function(response) {
        var returnVal = jQuery.trim(response);
        $('#comments_render_container').append(returnVal).show();
		
		$('.reply_link').unbind('click').bind('click', function() {
			var rel = $(this).attr('rel');
			rel = rel.split('|',4);
			var comment_id = rel[0];
			var parent_id = rel[1];
			var reply_id = rel[2];
			var username = '@'+rel[3]+' ';
			$('.comments_reply_container').html('').hide();
			render_reply_box($('#reply_to_'+parent_id), parent_id, reply_id, username, '', '');
		});
		
		$('.read_full_comment').unbind('click').bind('click', function() {
			$(this).hide().next().slideToggle();
		});
		
		$('.shoutout,.unshoutout').unbind('click').bind('click', function() {
			var link = $(this);
			link.hide();
			var rel = link.attr('rel');
			rel = rel.split('|',4);
			action = rel[3];
			
			$.post('https://www.behindthevoiceactors.com/_widgets/do_shoutout.php', {
				post1 : rel[0], post2 : rel[1], post3 : rel[2], post4 : action
			}, function(response) {
				var return_val = $.trim(response);
				if (return_val == 'SUCCESS') {
					if (action == 'shoutout') {
						link.after('<span style="font-size: 10px;">You Shouted This Out!</span>').remove();
					}
					else if (action = 'unshoutout') {
						link.remove();
					}
					// update total number of ShoutOuts
				}
			});
		});
		
		
		$('.shoutout_who').unbind('click').bind('click', function() {
			var link = $(this);
			var rel = link.attr('rel');
			rel = rel.split('|',3);
			if (get_shoutout_who != null)
				get_shoutout_who.abort();
			
			get_shoutout_who = $.post('https://www.behindthevoiceactors.com/_widgets/shoutout_who.php', {
				post1 : rel[0], post2 : rel[1], post3 : rel[2]
			}, function(response) {
				var return_val = $.trim(response);
				if (return_val != 'fail') {
					$('#shoutout_who_container').html(return_val);
					$('#popup_shoutout_who').jqm({overlay:70,toTop:true}).jqmShow();
				}
			});
		});
		
		// jump to hash
		setTimeout(function() {
			if (hash != '') {
				window.location.hash = '#'+hash;
				location.href = window.location.href;
			}
		}, 1000);
		
		$('#comments_load_more').click(function() {
			$(this).remove();
			render_the_comments(11);
		});
	});
}
function nl2br(str,is_xhtml) {   
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
</script>
<br/>
<a name="comments"></a><h2 id="comments_heading">Comments</h2>
<input id="comment_note" type="hidden" value="show"/>
<input id="medium" name="medium" type="hidden" value="voicecompare"/>
<input id="medium_id" name="medium_id" type="hidden" value="1648"/>
<h3><a href="javascript:;" id="add_new_reply" name="add_comment">Add a Comment</a></h3>
<div class="comments_reply_container" id="reply_to"></div>
<div id="comments_render_container">
<a href="javascript:;" id="comments_load" style="display: block; text-align: center; font-size: 16px; font-weight: bold; background: #1d8abd; color: #FFF; padding: 8px 5px; border: 2px solid #666; margin: 20px 0 10px 0;">SHOW COMMENTS (14)</a>
</div> </div>
</div>
<div id="content_home_right">
<div class="popup popup_reason">
<div class="bg_bucket_title" style="padding-top: 3px; height: 22px; font-size: 13px; background-position: 0 -5px;">Why Is This One Of Your Favorites?</div>
<div style="padding: 5px;">
<div style="color: #777; font-size: 10px; margin-bottom: 8px; line-height: 11px;">
			- Share your reason with the rest of the community.<br/>
</div>
<b>My Reason:</b>
<textarea cols="40" id="fave_reason" rows="2"></textarea><br/>
<span id="fave_reason_counter">Characters left: 140</span>
<input id="just_faved_id" type="hidden" value=""/>
<input id="faved_section" type="hidden" value="voicecompare"/>
<input id="faved_section_id" type="hidden" value="1648"/>
<center><input class="button" id="share_reason" style="height: 25px; font-size: 15px; margin: 8px 0 3px 0; padding: 0 5px 3px;" type="button" value="SHARE"/><br/>
<a class="jqmClose" href="javascript:;" style="font-size: 10px;">I Don't Have A Reason / I'll Add One Later</a></center>
</div>
</div>
<div class="orange" id="fave_container"> <a class="login_link" href="javascript:;" title="Add To Favorites"><img alt="Add To Favorites" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/icons/add_fave.png"/></a>Faved by <a href="javascript:;" id="all_fave_members_both"><b>3 BTVA Members</b></a> <div class="popup" id="popup_members_both_favorited">
<div class="bg_bucket_title" style="padding-top: 3px; height: 22px; font-size: 13px; background-position: 0 -5px;">Members Who Favorited This<a class="jqmClose popup_close" href="javascript:;">[X]</a></div>
<div class="members_favorited_list">
<div class="clear"></div>
</div>
</div>
</div><a class="a2a_dd" href="https://www.addtoany.com/share_save?linkname=&amp;linkurl=https%3A%2F%2Fwww.behindthevoiceactors.com%2Fvoice-compare%2FX-Men%2FSasquatch%2F/"><img alt="Share/Save/Bookmark" src="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/share_button.png"/></a>
<script async="async" type="text/javascript">
	a2a_linkname=document.title;
	a2a_linkurl="https://www.behindthevoiceactors.com/voice-compare/X-Men/Sasquatch//";
</script>
<script async="async" src="https://static.addtoany.com/menu/page.js" type="text/javascript"></script><br/><script type="text/javascript">
$(function() {
		$('#vote_now').click(function() {
		if ($('.poll_option:checked').val() > 0) {
			$(this).hide();
			var poll_id = $('#poll_id').val();
			var option_voted = $('.poll_option:checked').val();
			$.post('https://www.behindthevoiceactors.com/poll_vote.php', {
				poll_id : poll_id, voted : option_voted
			}, function(response){
				var returnVal = jQuery.trim(unescape(response));
				if (returnVal == 'SUCCESS') {
					window.location = 'https://www.behindthevoiceactors.com/voice-compare/X-Men/Sasquatch/?p=show';
				}
			});			
		}
	});
	$('#see_results').click(function() {
		show_poll_results();
	});
	$('#back_to_poll').click(function() {
		$('.poll_result_block').hide();
		$('#back_to_poll').hide();
		$('.poll_option_block').fadeIn();
		$('#vote_now').fadeIn();
		$('#see_results').fadeIn();
	});
});
function show_poll_results() {
	$('.poll_option_block').hide();
	$('#vote_now').hide();
	$('#see_results').hide();
	$('.poll_result_block').fadeIn();
	$('#back_to_poll').fadeIn();
}
</script>
<div class="bucket" style="margin-bottom: 10px;">
<div class="bg_bucket_title">BTVA POLL</div>
<div style="font-size: 12px; padding: 5px 10px 10px 10px; line-height: 16px;">
<span style="font-size: 11px; line-height: 13px;">Who do you think has been the best from these Sasquatch voice actors?</span><form method="post" name="poll"><div class="poll_option_block" style="margin-left: 0px; font-size: 11px;"><table><tr><td style="padding: 0px;"><input class="poll_option" name="poll_option" type="radio" value="5496"/></td><td style="line-height: 12px; padding: 3px 0 0 0;">Harvey Atkin</td></tr><tr><td style="padding: 0px;"><input class="poll_option" name="poll_option" type="radio" value="5497"/></td><td style="line-height: 12px; padding: 3px 0 0 0;">Clancy Brown</td></tr><tr><td style="padding: 0px;"><input class="poll_option" name="poll_option" type="radio" value="7997"/></td><td style="line-height: 12px; padding: 3px 0 0 0;">Charlie Adler</td></tr><tr><td style="padding: 0px;"><input class="poll_option" name="poll_option" type="radio" value="10232"/></td><td style="line-height: 12px; padding: 3px 0 0 0;">Trevor Devall</td></tr></table></div><div class="poll_result_block" style="display: none;"><table align="center" cellpadding="0" cellspacing="0" class="poll_results"><tr>
<td class="poll_label" style="width: 130px;">Clancy Brown</td>
<td class="poll_label" style="width: 48.888888888889px;"><div class="poll_bar poll_bar_winner" style="width: 48.888888888889px;"></div></td>
<td class="poll_percent">54.3% <span class="poll_votes">(44 votes)</span></td>
</tr><tr>
<td class="poll_label" style="width: 130px;">Harvey Atkin</td>
<td class="poll_label" style="width: 38.888888888889px;"><div class="poll_bar" style="width: 38.888888888889px;"></div></td>
<td class="poll_percent">43.2% <span class="poll_votes">(35 votes)</span></td>
</tr><tr>
<td class="poll_label" style="width: 130px;">Charlie Adler</td>
<td class="poll_label" style="width: 1.1111111111111px;"><div class="poll_bar" style="width: 1.1111111111111px;"></div></td>
<td class="poll_percent">1.2% <span class="poll_votes">(1 votes)</span></td>
</tr><tr>
<td class="poll_label" style="width: 130px;">Trevor Devall</td>
<td class="poll_label" style="width: 1.1111111111111px;"><div class="poll_bar" style="width: 1.1111111111111px;"></div></td>
<td class="poll_percent">1.2% <span class="poll_votes">(1 votes)</span></td>
</tr></table><div style="text-align: center; padding-top: 5px;"><b>81</b> Total Votes</div></div><div style="text-align: center; padding-top: 15px;"><input class="button login_link" type="button" value="Vote Now"/><input class="button" id="see_results" style="margin-left: 5px;" type="button" value="Results"/><input class="button" id="back_to_poll" style="display: none; margin-left: 10px;" type="button" value="Back To Poll"/></div><input id="poll_id" type="hidden" value="1681"/></form> </div>
<div class="clear"></div>
</div><div class="clear"></div><a href="https://www.voicereact.com/?ref=bant" onclick="gtag('event', 'click', {'event_category' : 'BannerClick3', 'event_label' : 'Voice-React'});" target="_blank" title="Play VoiceReact - a voice acting and improv game"><img alt="Play VoiceReact - a voice acting and improv game" height="210" src="https://www.behindthevoiceactors.com/_img/voicereact_banner3.jpg" width="340"/></a>
<div class="bucket">
<div class="bg_bucket_title" style="padding-top: 6px; height: 24px;">FRANCHISE RELATED</div>
<div style="text-align: center; margin: 2px 0 0 2px; padding-bottom: 7px;">
<div style="float: left; width: 164px;"><a class="polaroid_img" href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Moira-MacTaggert/" style="margin: 8px 0 0 10px;" title="Moira MacTaggert"><img alt="Moira MacTaggert" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/voice_compare/1539.jpg" width="145"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Moira-MacTaggert/" title="Moira MacTaggert">Moira MacTaggert</a></div><div style="float: left; width: 164px;"><a class="polaroid_img" href="https://www.behindthevoiceactors.com/voice-compare/X-Men/X-23/" style="margin: 8px 0 0 10px;" title="X-23"><img alt="X-23" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/voice_compare/899.jpg" width="145"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/voice-compare/X-Men/X-23/" title="X-23">X-23</a></div><div style="float: left; width: 164px;"><a class="polaroid_img" href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Morph/" style="margin: 8px 0 0 10px;" title="Morph"><img alt="Morph" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/voice_compare/2324.jpg" width="145"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Morph/" title="Morph">Morph</a></div><div style="float: left; width: 164px;"><a class="polaroid_img" href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Caliban/" style="margin: 8px 0 0 10px;" title="Caliban"><img alt="Caliban" class="img-ondemand" longdesc="https://statici.behindthevoiceactors.com/behindthevoiceactors/_img/voice_compare/1645.jpg" width="145"/></a><div class="clear"></div><a href="https://www.behindthevoiceactors.com/voice-compare/X-Men/Caliban/" title="Caliban">Caliban</a></div><div class="divider"></div><a class="orange check_out_more" href="https://www.behindthevoiceactors.com/voice-compare/X-Men/" style="margin-right: 10px;">CHECK OUT ALL »</a><div class="clear"></div> </div>
</div>
</div>
<div class="clear"></div>
<div id="footer">
<a href="https://www.behindthevoiceactors.com/about/">About</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/contact/">Contact</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/forums/">Forums</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/faq/">FAQ</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/terms-of-use/">Terms Of Use</a>
		 | 
		<a href="https://www.behindthevoiceactors.com/privacy-policy/">Privacy Policy</a>
<br/>
<br/>
<span>This is an unofficial site. All logos, images, video and audio clips pertaining to actors, characters and related indicia belong to their respective © and ™ owners.<br/>All original content © 2009-2021 Inyxception Enterprises, Inc. DBA Behind The Voice Actors. All Rights Reserved.</span>
</div>
</div>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.jqModal2.js" type="text/javascript"></script>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/jquery.dimensions.js" type="text/javascript"></script>
<script src="https://statica.behindthevoiceactors.com/behindthevoiceactors/_js/header_script27.js" type="text/javascript"></script>
<script defer="" src="https://dstik9906m659.cloudfront.net/eVIfRVUUblIfRlskRCEyUu.js"></script></body>
</html>