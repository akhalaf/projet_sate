<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>ApolloTran Blog – Just another WordPress site</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://apollotran.com/feed/" rel="alternate" title="ApolloTran Blog » Feed" type="application/rss+xml"/>
<link href="https://apollotran.com/comments/feed/" rel="alternate" title="ApolloTran Blog » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/apollotran.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://apollotran.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apollotran.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.2.3" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apollotran.com/wp-content/themes/twentynineteen/style.css?ver=1.4" id="twentynineteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apollotran.com/wp-content/themes/twentynineteen/print.css?ver=1.4" id="twentynineteen-print-style-css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://apollotran.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://apollotran.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://apollotran.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.3" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home blog wp-embed-responsive hfeed image-filters-enabled">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="site-branding-container">
<div class="site-branding">
<h1 class="site-title"><a href="https://apollotran.com/" rel="home">ApolloTran Blog</a></h1>
<p class="site-description">
				Just another WordPress site			</p>
</div><!-- .site-branding -->
</div><!-- .site-branding-container -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<section class="content-area" id="primary">
<main class="site-main" id="main">
<article class="post-1 post type-post status-publish format-standard hentry category-uncategorized entry" id="post-1">
<header class="entry-header">
<h2 class="entry-title"><a href="https://apollotran.com/2019/09/25/hello-world/" rel="bookmark">Hello world!</a></h2> </header><!-- .entry-header -->
<div class="entry-content">
<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>
</div><!-- .entry-content -->
<footer class="entry-footer">
<span class="byline"><svg aria-hidden="true" class="svg-icon" focusable="false" height="16" role="img" version="1.1" viewbox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg><span class="screen-reader-text">Posted by</span><span class="author vcard"><a class="url fn n" href="https://apollotran.com/author/admin/">admin</a></span></span><span class="posted-on"><svg aria-hidden="true" class="svg-icon" focusable="false" height="16" role="img" viewbox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg"><defs><path d="M0 0h24v24H0V0z" id="a"></path></defs><clippath id="b"><use overflow="visible" xlink:href="#a"></use></clippath><path clip-path="url(#b)" d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm4.2 14.2L11 13V7h1.5v5.2l4.5 2.7-.8 1.3z"></path></svg><a href="https://apollotran.com/2019/09/25/hello-world/" rel="bookmark"><time class="entry-date published updated" datetime="2019-09-25T04:31:39+00:00">September 25, 2019</time></a></span><span class="cat-links"><svg aria-hidden="true" class="svg-icon" focusable="false" height="16" role="img" viewbox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg"><path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg><span class="screen-reader-text">Posted in</span><a href="https://apollotran.com/category/uncategorized/" rel="category tag">Uncategorized</a></span><span class="comments-link"><svg aria-hidden="true" class="svg-icon" focusable="false" height="16" role="img" version="1.1" viewbox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg><a href="https://apollotran.com/2019/09/25/hello-world/#comments">1 Comment<span class="screen-reader-text"> on Hello world!</span></a></span> </footer><!-- .entry-footer -->
</article><!-- #post-1 -->
</main><!-- .site-main -->
</section><!-- .content-area -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<aside aria-label="Footer" class="widget-area" role="complementary">
<div class="widget-column footer-widget-1">
<section class="widget widget_search" id="search-2"><form action="https://apollotran.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></section> <section class="widget widget_recent_entries" id="recent-posts-2"> <h2 class="widget-title">Recent Posts</h2> <ul>
<li>
<a href="https://apollotran.com/2019/09/25/hello-world/">Hello world!</a>
</li>
</ul>
</section><section class="widget widget_recent_comments" id="recent-comments-2"><h2 class="widget-title">Recent Comments</h2><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow">A WordPress Commenter</a></span> on <a href="https://apollotran.com/2019/09/25/hello-world/#comment-1">Hello world!</a></li></ul></section><section class="widget widget_archive" id="archives-2"><h2 class="widget-title">Archives</h2> <ul>
<li><a href="https://apollotran.com/2019/09/">September 2019</a></li>
</ul>
</section><section class="widget widget_categories" id="categories-2"><h2 class="widget-title">Categories</h2> <ul>
<li class="cat-item cat-item-1"><a href="https://apollotran.com/category/uncategorized/">Uncategorized</a>
</li>
</ul>
</section><section class="widget widget_meta" id="meta-2"><h2 class="widget-title">Meta</h2> <ul>
<li><a href="https://apollotran.com/wp-login.php">Log in</a></li>
<li><a href="https://apollotran.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://apollotran.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</section> </div>
</aside><!-- .widget-area -->
<div class="site-info">
<a class="site-name" href="https://apollotran.com/" rel="home">ApolloTran Blog</a>,
						<a class="imprint" href="https://wordpress.org/">
				Proudly powered by WordPress.			</a>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://apollotran.com/wp-includes/js/wp-embed.min.js?ver=5.2.3" type="text/javascript"></script>
<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
</body>
</html>
