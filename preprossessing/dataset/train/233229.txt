<!DOCTYPE html>
<html><head>
<meta charset="utf-8"/>
<title>India's Online Business Directory | Biz15.co.in</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Biz15 India's Online Business Directory" name="description"/>
<meta content="India business directory, business directory India, India Free Business Directory, Busi" name="keywords"/>
<meta content="India's Online Business Directory" name="author"/>
<link href="https://www.biz15.co.in/themes/default/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/selectize.bootstrap3.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/intlTelInput.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/famfamfam-flags.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/social-buttons.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/themes/default/css/animate.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/jquery.fancybox.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/common.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/general.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/themes/default/css/global.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/assets/css/star-rating.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/themes/default/css/_home.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.biz15.co.in/themes/default/css/home.css" media="screen" rel="stylesheet" type="text/css"/>
<script async="" type="text/javascript">
    /*----------------------------------------------------*/
	/*	Google Analytics Code
	/*----------------------------------------------------*/
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-34416340-1', 'auto');
	ga('send', 'pageview');
	</script>
<link href="https://www.biz15.co.in/assets/images/favicon.ico" rel="shortcut icon"/>
<link crossorigin="" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" rel="stylesheet"/>
<script async="" crossorigin="" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
<!-- Pixel Code for https://myweb.biz15.com/notify/ -->
<script async="" src="https://myweb.biz15.com/notify/pixel/c2c908c88b3ea712f75d83e5084355d4"></script>
<!-- END Pixel Code -->
</head>
<body><div class="navbar navbar-expand-md p-0 text-black gc__nav-default">
<ul class="list-inline d-block d-sm-none m-0 p-0">
<li class="list-inline-item"></li> <li class="list-inline-item">
<a href="https://www.biz15.co.in/login" title="My Account">My Account</a></li> </ul>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler mr-3 mt-1 mb-1" data-target="#topBar" data-toggle="collapse" type="button">
<i class="fas fa-bars fa-lg"></i>
</button>
<div class="collapse navbar-collapse navbar-header" id="topBar">
<ul class="navbar-nav mb-0">
<li class="nav-item"><a class="nav-link" href="https://www.biz15.co.in/"><strong>Home</strong></a></li>
<li class="navbar-item"><a class="nav-link" href="https://www.biz15.co.in/pages/about-us" target="_self" title="About us">About us</a>
</li><li class="navbar-item"><a class="nav-link" href="https://www.biz15.co.in/all-categories.html" target="_blank" title="All Categories">All Categories</a>
</li><li class="navbar-item"><a class="nav-link" href="http://bit.ly/2WLVFlr" target="_blank" title="Create a Website">Create a Website</a>
</li><li class="navbar-item"><a class="nav-link" href="http://bit.ly/2WLnydd" target="_blank" title="Advertise Online">Advertise Online</a>
</li> <li class="phones nav-item"></li> <li class="nav-item d-none d-sm-block"></li> </ul>
<ul class="navbar-nav mb-0 ml-auto">
<li class="nav-item"></li> <li class="user nav-item d-none d-sm-block">
<a class="nav-link" href="https://www.biz15.co.in/login" title="My Account"><i class="fa fa-user fa-fr"></i>My Account</a></li> </ul>
</div>
</div>
<nav class="navbar navbar-expand-md navbar-light navbar-default">
<div class="navbar-brand" id="brand">
<h1><a href="https://www.biz15.co.in/" title="India business directory, business directory India, India Free Business Directory, Busi">India's Online Business Directory</a></h1>
</div>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler btn-theme p-2" data-target="#searchNav" data-toggle="collapse" type="button">
<i class="fas fa-search"></i>
</button>
<div class="collapse navbar-collapse" id="searchNav">
<div class="col-md-8 col-lg-8 col-xl-9 col-sm-8 col-xs-12">
<form action="https://www.biz15.co.in/search" class="navbar-form form mt-2" id="search-form" method="get" name="search-form" role="search">
<div class="form-row">
<div class="form-group col-md-3">
<input autocomplete="off" class="form-control typeahead-location search-input form-control-lg rounded-0 w-100" name="l" placeholder="Town, City or Postcode" type="text" value="Chennai"/>
<input id="tl" name="tl" type="hidden" value="1"/> </div>
<div class="form-group col-md-9">
<div class="input-group input-group-btn">
<input autocomplete="off" class="typeahead form-control typeahead-search search-input w-100 rounded-0 form-control-lg" name="q" placeholder="Search by keyword..." type="text" value=""/>
<div class="input-group-append">
<button class="btn btn-info btn-flip" name="search" type="submit">
<i class="fa fa-search"></i>
</button>
</div>
</div> </div>
</div>
</form>
<div class="mt-3">
<ul class="unstyled list-inline menu-links text-center">
<li class="d-none d-lg-inline bottom1 list-inline-item"><a class="btn btn-sm btn-theme" href="https://www.biz15.co.in/classifieds" title="Classifieds"><i class="fa fa-file fa-fr"></i>Classifieds</a></li>
<li class="d-lg-none d-md-inline bottom2 list-inline-item"><a class="btn btn-sm btn-theme" data-original-title="Classifieds" data-tooltip="tooltip" href="https://www.biz15.co.in/classifieds" title="Classifieds"><i class="fa fa-file"></i></a></li>
<li class="d-none d-lg-inline bottom1 list-inline-item"><a class="btn btn-sm btn-theme" href="https://www.biz15.co.in/events" title="Events"><i class="fa fa-calendar fa-fr"></i>Events</a></li>
<li class="d-lg-none d-md-inline bottom2 list-inline-item"><a class="btn btn-sm btn-theme" data-original-title="Events" data-tooltip="tooltip" href="https://www.biz15.co.in/events" title="Events"><i class="fa fa-calendar"></i></a></li>
<li class="d-none d-lg-inline bottom1 list-inline-item"><a class="btn btn-sm btn-theme" href="https://www.biz15.co.in/properties" title="Properties"><i class="fa fa-th fa-fr"></i>Properties</a></li>
<li class="d-lg-none d-md-inline bottom2 list-inline-item"><a class="btn btn-sm btn-theme" data-original-title="Properties" data-tooltip="tooltip" href="https://www.biz15.co.in/properties" title="Properties"><i class="fa fa-th"></i></a></li>
<li class="d-none d-lg-inline bottom1 list-inline-item"><a class="btn btn-sm btn-theme" href="https://www.biz15.co.in/jobs" title="Jobs"><i class="fa fa-folder fa-fr"></i>Jobs</a></li>
<li class="d-lg-none d-md-inline bottom2 list-inline-item"><a class="btn btn-sm btn-theme" data-original-title="Jobs" data-tooltip="tooltip" href="https://www.biz15.co.in/jobs" title="Jobs"><i class="fa fa-folder"></i></a></li>
<li class="d-md-none bottom2 list-inline-item">
<a class="btn btn-sm btn-theme" data-original-title="Add Your Business" data-tooltip="tooltip" href="https://www.biz15.co.in/members/add_business">
<i class="fa fa-plus"></i>
</a>
</li>
</ul> </div>
</div>
<ul class="list-unstyled d-none d-sm-none d-md-block text-center m-auto pl-3">
<li>
<a class="btn btn-info btn-flip" href="https://www.biz15.co.in/members/add_business">
            	Add Your Business<i class="fa fa-chevron-right fa-fl"></i>
</a>
</li>
</ul>
</div>
</nav>
<div class="container-fluid pt-4"><div class="row">
<div class="col-lg-12">
<h1 style="text-align:center; color:#0988c5;; padding-top:35px; padding-bottom:35px;">POPULAR CATEGORIES</h1>
<section class="h1">
<div class="container">
<div class="row">
<div class="col">
<a href="https://www.biz15.co.in/category/Automotive-11">
<i aria-hidden="true" class="iconbtn fa fa fa-car" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/Automotive-11"><h5 class="cr">Automotive</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/building-construction-3">
<i aria-hidden="true" class="iconbtn fa fa fa-building" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/building-construction-3"><h5 class="cr">Building &amp; Construction</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/business-professionals-services-2">
<i aria-hidden="true" class="iconbtn fas fa-user-tie" style="font-size:32px;"></i>
</a><a href="https://www.biz15.co.in/category/business-professionals-services-2"><h5 class="cr">Business, Professionals &amp; Services</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/computers-software-electronics-12">
<i aria-hidden="true" class="iconbtn fa fa-desktop" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/computers-software-electronics-12"><h5 class="cr">Computers, Software &amp; Electronics</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/education-training-13">
<i class="iconbtn fa fa-book" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/education-training-13"><h5 class="cr">

Education &amp; Training</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/health-medicine-16">
<i class="iconbtn fa fa-ambulance"></i></a>
<a href="https://www.biz15.co.in/category/health-medicine-16"><h5 class="cr">
Health &amp; <br/>Medicine</h5></a>
</div>
</div>
</div>
</section>
<section class="h1">
<div class="container">
<div class="row">
<div class="col">
<a href="https://www.biz15.co.in/category/interior-exterior-627">
<i aria-hidden="true" class="iconbtn fas fa-couch" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/furniture-interior-exterior-627"><h5 class="cr">
Furniture, Interior &amp; Exterior</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/Real-Estate-74">
<i aria-hidden="true" class="iconbtn fa fa-home" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/Real-Estate-74"><h5 class="cr">Real Estate</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/technology-and-accessories-422">
<i aria-hidden="true" class="iconbtn fa fa-mobile" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/technology-and-accessories-422"><h5 class="cr">
Technology &amp; Accessories</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/travel-tourism-19">
<i aria-hidden="true" class="iconbtn fa fa-plane" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/travel-tourism-19"><h5 class="cr">
Travel &amp; Tourism</h5></a>
</div>
<div class="col">
<a href="https://www.biz15.co.in/category/wedding-event-planning-20">
<i aria-hidden="true" class="iconbtn fab fa-weixin" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/category/wedding-event-planning-20"><h5 class="cr">
Wedding &amp; Event Planning</h5></a>
</div>
<div class="col">
<a alt="No More" href="https://www.biz15.co.in/all-categories.html">
<i alt="No More" aria-hidden="true" class="iconbtn fas fa-angle-double-right" style="font-size:32px;"></i></a>
<a href="https://www.biz15.co.in/all-categories.html"><h5 class="cr">
More</h5></a>
</div>
</div>
</div>
</section>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/Automotive-11">Automotive</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/auto-parts-734" title="Auto Parts">
<img class="img-fluid" src="images/automotive/auto-parts.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/auto-parts-734">Auto Parts</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Car-Dealers-745" title="Car Dealers">
<img class="img-fluid" src="images/automotive/car-dealer.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Car-Dealers-745">Car Dealers</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Car-Service-Centers-591" title="Car Service Centers">
<img class="img-fluid" src="images/automotive/car-service-center.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Car-Service-Centers-591">Car Service Centers</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/building-construction-3">Building &amp; Construction</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Cement-883" title="Cement">
<img class="img-fluid" src="images/building/cement.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Cement-883">Cement</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Plywoods-2163" title="Plywoods">
<img class="img-fluid" src="images/building/plywood.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Plywoods-2163">Plywoods</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/hardware-and-tools-580" title="Hardware &amp; Tools">
<img class="img-fluid" src="images/building/hardware-tools.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/hardware-and-tools-580">Hardware &amp; Tools</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/pipes-and-tubes-587" title="Pipes &amp; Tubes">
<img class="img-fluid" src="images/building/pipes-tubes.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/pipes-and-tubes-587">Pipes &amp; Tubes</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/bricks-stones-2482" title="Bricks Stones">
<img class="img-fluid" src="images/building/brick-stone.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/bricks-stones-2482">Bricks Stones</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/business-professionals-services-2">Business, Professionals &amp; Services</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/courier-service-697" title="Courier Service">
<img class="img-fluid" src="images/business/courier.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/courier-service-697">Courier Service</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/virtual-office-945" title="Virtual Office">
<img class="img-fluid" src="images/business/virtual-office.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/virtual-office-945">Virtual Office</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/web-design-and-development-746" title="Web Design &amp; Development">
<img class="img-fluid" src="images/business/web-design.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/web-design-and-development-746">Web Design And Development</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/office-equipment-supplies-403" title="Office Equipment Supplies">
<img class="img-fluid" src="images/business/office-equip.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/office-equipment-supplies-403">Office Equipment Supplies</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Business-Services-566" title="Business Services">
<img class="img-fluid" src="images/business/business-service.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Business-Services-566">Business Services</a></h5></div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/computers-software-electronics-12">Computers, Software &amp; Electronics</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Computers-628" title="Computers">
<img class="img-fluid" src="images/computer/computer.png"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Computers-628">Computers</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Electronics-690" title="Electronics">
<img class="img-fluid" src="images/computer/electronics.png"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Electronics-690">Electronics</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Computer-Service-Center-947" title="Computer Service Center">
<img class="img-fluid" src="images/computer/computer-service.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Computer-Service-Center-947">Computer Service Center</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/education-training-13">Education &amp; Training</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/schools-584" title="Schools">
<img class="img-fluid" src="images/education/schools.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/schools-584">Schools</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Books-555" title="Books">
<img class="img-fluid" src="images/education/books.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Books-555">Books</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Colleges-And-University-632" title="Colleges And University">
<img class="img-fluid" src="images/education/colleges-university.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Colleges-And-University-632">Colleges And University</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/coaching-centers-2084" title="Coaching Centers">
<img class="img-fluid" src="images/coaching.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/coaching-centers-2084">Coaching Centers</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/educational-institutes-578" title="Educational Institutes">
<img class="img-fluid" src="images/educational.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/educational-institutes-578">Educational Institutes</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/electrical-products-services-98">Electrical Products &amp; Services</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/pumps-motors-576" title="Pumps &amp; Motors">
<img class="img-fluid" src="images/electrical/pump.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/pumps-motors-576">Pumps &amp; Motors</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/electrical-goods-equipment-supplies-590" title="Electrical Goods, Equipment &amp; Supplies">
<img class="img-fluid" src="images/electrical/elegoods.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/electrical-goods-equipment-supplies-590">Electrical Goods, Equipment &amp; Supplies</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/electricals-1998" title="Electricals">
<img class="img-fluid" src="images/electrical/electrical.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/electricals-1998">Electricals</a></h5></div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/fashion-accessories-446">Fashion Accessories</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/bags-and-footwears-579" title="Bags And Footwears">
<img class="img-fluid" src="images/fashion/bag1.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/bags-and-footwears-579">Bags And Footwears</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/gems-and-jewellery-618" title="Gems And Jewellery">
<img class="img-fluid" src="images/fashion/gems.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/gems-and-jewellery-618">Gems And Jewellery</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/watches-clocks-1555" title="Watches And Clocks">
<img class="img-fluid" src="images/watch.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/watches-clocks-1555">Watches And Clocks</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/food-beverage-industry-15">Food &amp; Beverage Industry</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Restaurants-684" title="Restaurants">
<img class="img-fluid" src="images/food/restaraunt.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Restaurants-684">Restaurants</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/food-suppliers-2495" title="Food Suppliers">
<img class="img-fluid" src="images/food/suppliers.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/food-suppliers-2495">Food Suppliers</a></h5></div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/furniture-interior-exterior-627">Furniture, Interior &amp; Exterior</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/furniture-572" title="Furniture">
<img class="img-fluid" src="images/interior/furniture.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/furniture-572">Furniture</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/office-interior-decorator-944" title="Office Interior Decorator">
<img class="img-fluid" src="images/interior/office-interior.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/office-interior-decorator-944">Office Interior Decorator</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/interior-decorators-769" title="Interior Decorators">
<img class="img-fluid" src="images/interior/interior-decorator.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/interior-decorators-769">Interior Decorators</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/health-medicine-16">Health &amp; Medicine</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Dental-Clinics-363" title="Dental Clinics">
<img class="img-fluid" src="images/heath/dental.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Dental-Clinics-363">Dental Clinics</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/hospitals-364" title="Hospitals">
<img class="img-fluid" src="images/heath/hospital.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/hospitals-364">Hospitals</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Doctors-379" title="Doctors">
<img class="img-fluid" src="images/heath/doctor.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Doctors-379">Doctors</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/opticals-624" title="Opticals">
<img class="img-fluid" src="images/heath/opti.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/opticals-624">Opticals</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/home-appliances-679">Home Appliances</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/air-conditioner-dealers-593" title="Air Conditioner Dealers">
<img class="img-fluid" src="images/home/split1.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/air-conditioner-dealers-593">Air Conditioner Dealers</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/air-conditioners-multi-range-2439" title="Air Conditioners Multi Range">
<img class="img-fluid" src="images/home/wall1.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/air-conditioners-multi-range-2439">Air Conditioners Multi Range</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#007bff; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/technology-and-accessories-422">Technology &amp; Accessories</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/mobile-phone-service-centers-819" title="Mobile Phone Service Centers">
<img class="img-fluid" src="images/technology/mobile-service.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/mobile-phone-service-centers-819">Mobile Phone Service Centers</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/gps-1134" title="Gps">
<img class="img-fluid" src="images/technology/gps.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/gps-1134">Gps</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/video-conferencing-1547" title="Video Conferencing">
<img class="img-fluid" src="images/technology/video-conferencing.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/video-conferencing-1547">Video Conferencing</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/wireless-communication-1567" title="Wireless Communication">
<img class="img-fluid" src="images/technology/wireless.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/wireless-communication-1567">Wireless Communication</a></h5>
</div>
</div>
</div>
<div class="container">
<h1 style="text-align:center; color:#0a82bb; padding-top:35px; padding-bottom:35px;"><a href="https://www.biz15.co.in/category/Transportation-10">Transportation</a></h1>
<div class="row">
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/Cargo-Services-570" title="Cargo Services">
<img class="img-fluid" src="images/transportation/cargo.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/Cargo-Services-570">Cargo Services</a></h5>
</div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/international-road-cargo-services-765" title="International Road Cargo Services">
<img class="img-fluid" src="images/transportation/road.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/international-road-cargo-services-765">International Road Cargo Services</a></h5></div>
<div class="col-sm-2point4">
<a class="img-box img-thumbnail" href="https://www.biz15.co.in/category/domestic-air-cargo-services-763" title="Domestic Air Cargo Services">
<img class="img-fluid" src="images/transportation/aircar.jpg"/>
</a>
<h5><a href="https://www.biz15.co.in/category/domestic-air-cargo-services-763">Domestic Air Cargo Services</a></h5>
</div>
</div>
</div></div>
</div>
</div>
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-sm-4">
<h5>About Us</h5>
<p class="text-justify">Biz15 is a popular online business directory of India, which promotes your business in emerging markets of the digital world, helping to showcase your products and services, and to get the most potential new customers in your business in technology.
</p>
</div>
<!-- end col-sm-4 -->
<div class="col-sm-2">
<h5>Useful Links</h5>
<ul class="list-unstyled footer-menu">
<li><a href="https://www.biz15.co.in/classifieds" title="Classifieds"><i class="fa fa-angle-double-right fa-fr"></i>Classifieds</a></li>
<li><a href="https://www.biz15.co.in/events" title="Events"><i class="fa fa-angle-double-right fa-fr"></i>Events</a></li>
<li><a href="https://www.biz15.co.in/properties" title="Properties"><i class="fa fa-angle-double-right fa-fr"></i>Properties</a></li>
<li><a href="https://www.biz15.co.in/jobs" title="Jobs"><i class="fa fa-angle-double-right fa-fr"></i>Jobs</a></li>
</ul> </div>
<div class="col-sm-2">
<h6><strong>Navigation</strong></h6>
<ul class="list-unstyled">
<li><a href="https://www.biz15.co.in/pages/about-us" target="_self" title="About us"><i class="fa fa-fr fa-angle-double-right fa-fr"></i>About us</a>
<ul>
</ul>
</li><li><a href="https://www.biz15.co.in/pages/contact" target="_self" title="Contact Us"><i class="fa fa-fr fa-angle-double-right fa-fr"></i>Contact Us</a>
<ul>
</ul>
</li><li><a href="https://www.biz15.co.in/pages/privacy-policy" target="_self" title="Privacy Policy"><i class="fa fa-fr fa-angle-double-right fa-fr"></i>Privacy Policy</a>
<ul>
</ul>
</li><li><a href="https://www.biz15.co.in/pages/terms-conditions" target="_self" title="Terms &amp; Conditions"><i class="fa fa-fr fa-angle-double-right fa-fr"></i>Terms &amp; Conditions</a>
<ul>
</ul>
</li> </ul>
</div>
<!-- end col-sm-2 navigation -->
<div class="col-sm-2">
<h5>Quick Links</h5>
<ul class="list-unstyled footer-menu">
<li><a href="https://www.biz15.co.in/categories" title="Browse by Category">Browse by Category</a></li>
<li><a href="https://www.biz15.co.in/locations" title="Browse by Location">Browse by Location</a></li>
<li><a href="https://www.biz15.co.in/requirements" title="Post Your Requirements">Post Your Requirements</a></li>
</ul> <h6><strong>Follow Us</strong></h6>
<ul class="social nav-link">
<li class="facebook">
<a data-placement="bottom" data-toggle="tooltip" href="https://facebook.com/biz15" target="_blank" title="Facebook URL">
<i class="fab fa-facebook"></i>
</a>
</li>
<li class="twitter">
<a data-placement="bottom" data-toggle="tooltip" href="https://twitter.com/biz15digital" target="_blank" title="Twitter URL">
<i class="fab fa-twitter"></i>
</a>
</li>
<li class="gplus">
<a data-placement="bottom" data-toggle="tooltip" href="https://plus.google.com/+biz15services" target="_blank" title="Google Plus URL">
<i class="fab fa-google-plus"></i>
</a>
</li>
</ul>
</div>
<!-- end col-sm-2 -->
<div class="col-sm-2">
</div>
</div>
<div class="row mt-2">
<div class="col-sm-8">
<span><strong>Popular Searches</strong>
<a href="https://www.biz15.co.in/category/industries-machinery-supplies-equipment-7" title="Industries, Machinery, Supplies &amp; Equipment">Industries, Machinery, Supplies &amp; Equipment</a>, <a href="https://www.biz15.co.in/category/business-professionals-services-2" title="Business, Professionals &amp; Services">Business, Professionals &amp; Services</a>, <a href="https://www.biz15.co.in/category/health-medicine-16" title="Health &amp; Medicine">Health &amp; Medicine</a>, <a href="https://www.biz15.co.in/category/building-construction-3" title="Building &amp; Construction">Building &amp; Construction</a>, <a href="https://www.biz15.co.in/category/packaging-printing-service-2037" title="Packaging &amp; Printing Service">Packaging &amp; Printing Service</a> </span>
</div>
</div>
<div class="row">
<div class="col-sm-8">
<span><strong>Popular Cities</strong>
<a href="https://www.biz15.co.in/location/chennai-34" title="Chennai">Chennai</a>, <a href="https://www.biz15.co.in/location/madurai-48" title="Madurai">Madurai</a>, <a href="https://www.biz15.co.in/location/delhi-137" title="Delhi">Delhi</a>, <a href="https://www.biz15.co.in/location/jaipur-404" title="Jaipur">Jaipur</a>, <a href="https://www.biz15.co.in/location/vellore-63" title="Vellore">Vellore</a> </span>
</div>
</div>
<h5 class="mt-2">Copyright © 2021 Biz15.co.in</h5>
</div><!-- end container-fluid -->
</footer><!-- end of footer -->
<input id="error_lang" name="error_lang" type="hidden" value="Please fill below given details"/><input id="pre_location" name="pre_location" type="hidden" value="Chennai"/><input defcount="in" id="site_url" lang="en" name="site_url" type="hidden" value="https://www.biz15.co.in/"/>
<input block="3" id="search-location" lang="remaining" name="search-location" type="hidden" value="2"/>
<div id="debug"></div>
<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script>window.jQuery || document.write('<script src="https://www.biz15.co.in/assets/js/jquery-latest.min.js"><\/script>')</script>
<script>var gcLangString = {"login":"login","register":"register"};
</script>
<!-- This would be a good place to use a CDN version of jQueryUI if needed -->
<script src="https://www.biz15.co.in/themes/default/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/selectize.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/theme-switcher.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/metisMenu.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/validator.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/jquery.cookie.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/intlTelInput.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/jquery.sharrre.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/jquery.lazyload.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/jquery.fancybox.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/themes/default/js/wow.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/star-rating.min.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/themes/default/js/_home.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/themes/default/js/home.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/gc.js" type="text/javascript"></script>
<script src="https://www.biz15.co.in/assets/js/general.js" type="text/javascript"></script>
<script>
/*----------------------------------------------------*/
/*	Theme Switcher
/*----------------------------------------------------*/
jQuery(document).ready(function() {
    ThemeSwitcher.initThemeSwitcher();
    // activate wow.js for Home Effects
    new WOW().init();
});
</script>
</body>
</html>