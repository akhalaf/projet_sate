<!DOCTYPE html>
<html lang="ru">
<head>
<link href="https://amssoft.ru" rel="canonical"/>
<title>Лучшие программы AMS Software для фото, видео, полиграфии и дизайна</title>
<meta content="AMS Software - разработчик программ для фото, видео, полиграфии и дизайна. Мы делаем качественные и простые программы на русском языке, которые сможет освоить каждый!" name="description"/>
<!-- Custom Browsers Color Start -->
<meta content="#15151a" name="theme-color"/>
<!-- Custom Browsers Color End -->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="/css/main.css" rel="stylesheet"/>
</head>
<body>
<div class="wrap is-home">
<header class="main-head">
<div class="container">
<a class="logo" href="/"><img alt="AMS" src="/img/logo.png"/></a>
<div class="toggle-menu">
<a class="hamburger hamburger--slider">
<span class="hamburger-box"><span class="hamburger-inner"></span></span>
</a>
</div>
<div class="mobile-nav"></div>
<nav class="main-nav">
<ul class="main-nav__list">
<li class="main-nav__item main-nav__item-toggle main-nav__item-toggle--1"><a class="main-nav__link" href="javascript:void(0)">Программы <svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></a>
<div class="main-nav__submenu main-nav__submenu--1">
<div class="container">
<div class="main-nav__submenu-part">
<div>
<div class="main-nav__submenu-part-title">Работа с видео<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/videomontage/">ВидеоМОНТАЖ</a></li>
<li><a href="/videomaster/">ВидеоМАСТЕР</a></li>
<li><a href="/screencam/">Экранная Камера</a></li>
<li><a href="/videoshow/">ВидеоШОУ</a></li>
</ul>
</div>
</div>
<div class="main-nav__submenu-part">
<div>
<div class="main-nav__submenu-part-title">Обработка фото<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/fotomaster/">ФотоМАСТЕР</a></li>
<li><a href="/homephotostudio/">Домашняя Фотостудия</a></li>
<li><a href="/passportphoto/">Фото на документы</a></li>
<li><a href="/collagemaker/">ФотоКОЛЛАЖ</a></li>
</ul>
</div>
</div>
<div class="main-nav__submenu-part">
<div>
<div class="main-nav__submenu-part-title">Полезный софт<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/office-metrika/">ОфисМЕТРИКА</a></li>
<li><a href="/fenix/">ФЕНИКС</a></li>
<li><a href="/fotodoctor/">ФотоДОКТОР</a></li>
<li><a href="/audiomaster/">АудиоМАСТЕР</a></li>
</ul>
</div>
</div>
<div class="main-nav__submenu-part">
<div>
<div class="main-nav__submenu-part-title">Графика и дизайн<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/calendarcreator/">Дизайн Календарей</a></li>
<li><a href="/businesscard/">Мастер Визиток</a></li>
<li><a href="/envelop/">Почтовые Конверты</a></li>
<li><a href="/interior/">Дизайн Интерьера 3D</a></li>
</ul>
</div>
</div>
</div>
<div class="txt-center"><a class="all-apps" href="/programm.php"><span>Смотреть </span>все программы <svg class="icon icon-right-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a></div>
</div>
</li>
<li class="main-nav__item main-nav__item-toggle"><a class="main-nav__link" href="javascript:void(0)">Обучение <svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></a>
<div class="main-nav__submenu main-nav__submenu--2 main-nav__submenu--2-2">
<ul>
<li><a href="/photo/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Обработка фото</a></li>
<li><a href="/video/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Работа с видео</a></li>
<li><a href="/dizajn/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Дизайн и полиграфия</a></li>
<li><a href="/audio/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Обработка звука</a></li>
<li><a href="/vosstanovlenie/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Восстановление файлов</a></li>
<li><a href="/repair/"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg> Дизайн интерьера</a></li>
<li><a href="/article.php"><svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg>Полезные статьи</a></li>
</ul>
</div>
</li>
<li class="main-nav__item main-nav__item-toggle"><a class="main-nav__link" href="javascript:void(0)">Компания <svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></a>
<div class="main-nav__submenu main-nav__submenu--2">
<ul>
<li><a href="/blog/">Наш блог</a></li>
<li><a href="/vacation.php">Вакансии</a></li>
<li><a href="/about.php">История компании</a></li>
</ul>
</div>
</li>
<li class="main-nav__item"><a class="main-nav__link" href="/support.php">Поддержка</a></li>
<li class="main-nav__item"><a class="main-nav__link" href="/blog/">Блог</a></li>
</ul>
</nav>
</div>
</header>
<div class="index-banner zoom-eff">
<div class="index-banner__app">
<div class="index-banner__app-bg" style="background-image: url(img/fotoshowpro-bg.png)"></div>
<div class="index-banner__app-info">
<div class="index-banner__app-logo"><img alt="Логотип" src="img/fotoshowpro-logo.png"/></div>
<div class="index-banner__app-title"><b>ФотоШОУ</b><span class="light">PRO</span></div>
<div class="index-banner__app-description">Создавайте яркие слайд-шоу из фото, видео и музыки</div>
</div>
<a class="know-more" href="fotoshow-pro">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
<div class="index-banner__app">
<div class="index-banner__app-bg" style="background-image: url(img/videomontash-bg.png)"></div>
<div class="index-banner__app-info">
<div class="index-banner__app-logo"><img alt="Логотип" src="img/videomontash-logo.png"/></div>
<div class="index-banner__app-title">Видео<b>МОНТАЖ</b></div>
<div class="index-banner__app-description">Редактор видео, доступный каждому</div>
</div>
<a class="know-more" href="videomontage">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
<div class="index-banner__app">
<div class="index-banner__app-bg" style="background-image: url(img/fotomaster-bg.png)"></div>
<div class="index-banner__app-info">
<div class="index-banner__app-logo"><img alt="Логотип" src="img/fotomaster-logo.png"/></div>
<div class="index-banner__app-title">Фото<b>МАСТЕР</b></div>
<div class="index-banner__app-description">Мощная программа для обработки фото</div>
</div>
<a class="know-more" href="fotomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
<div class="index-banner__app">
<div class="index-banner__app-bg" style="background-image: url(img/videomaster-bg.png)"></div>
<div class="index-banner__app-info">
<div class="index-banner__app-logo"><img alt="Логотип" src="img/videomaster-logo.png"/></div>
<div class="index-banner__app-title">Видео<b>МАСТЕР</b></div>
<div class="index-banner__app-description">Универсальный видео конвертер</div>
</div>
<a class="know-more" href="videomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
<section class="s-download">
<div class="container">
<h2 class="sect-header">Доступные программы для фото и видео</h2>
<div class="sect-descr">AMS Software c 2003 года разрабатывает простые, но мощные программы, которыми с легкостью может пользоватся каждый</div>
<div class="txt-center"><a class="btn" href="programm.php">Смотреть все программы <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a></div>
</div>
</section>
<section class="s-tags">
<div class="container">
<h2 class="sect-header">Что вас интересует?</h2>
<div class="sect-descr">Выберите, что вы хотите сделать и мы найдем вам нужную программу</div>
<div class="tags">
<div class="tags-group">
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Обработка фотографий</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/photo.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Обработка фотографий</div>
<p>Домашняя Фотостудия — качественная ретушь и цветокоррекция, устранение дефектов, рамки и маски для фото.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/HomeStudioAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="homephotostudio">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Создание слайд-шоу</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/photo.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Создание слайд-шоу</div>
<p>ФотоШОУ PRO - создание красочных слайд-шоу из фотографий с красивыми эффектами, анимацией и музыкой.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/FotoShowAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="fotoshow-pro">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Создание коллажей</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/photo.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Создание коллажей</div>
<p>ФотоКОЛЛАЖ — 100+ дизайнерских шаблонов, рамки, маски и яркий клипарт для коллажей из фото.

                  </p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNwYXJrLnJ1L0ZvdG9Db2xsYWdlLmV4ZQ==" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="collagemaker">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Дизайн интерьера</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/design.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Дизайн интерьера</div>
<p>Дизайн Интерьера 3D — точные макеты помещений, отделка, расстановка мебели и техники из каталога.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNmaWxlcy5ydS9JbnRlcmlvcjNELmV4ZQ==" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="interior">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Обработка аудио</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Обработка аудио</div>
<p>АудиоМАСТЕР — работа с аудиофайлами всех форматов, редактирование, запись звука, обрезка, наложение эффектов. </p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/AudioMasterAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="audiomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Запись дисков</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/other.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Запись дисков</div>
<p>Студия Дисков — запись на DVD, Blu-Ray и CD. Очистка, перезапись дисков, резервное копирование файлов.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNmaWxlcy5ydS9EaXNrU3R1ZGlvLmV4ZQ==" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="diskstudio">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Запись видео с экрана</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Запись видео с экрана</div>
<p>Экранная Камера — съёмка и обработка обучающих роликов, игр, онлайн трансляций. </p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/ScreenCameraAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="screencam">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Создание фото на документы</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/photo.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Создание фото на документы</div>
<p>Фото на документы — подготовка и печать фото профессионального качества на все виды документов. </p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNwYXJrLnJ1L1Bob3RvRG9jUHJvLmV4ZQ==" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="passportphoto">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Программы для бизнеса</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/other.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Программы для бизнеса</div>
<p>ОфисМЕТРИКА - учёт рабочего времени сотрудников, контроль используемых программ и посещения сайтов.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cDovL29mZmljZS1tZXRyaWthLnJ1L2Rvd25sb2FkLnBocA==" data-target="jsdownload" href="#" target="_blank">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="office-metrika">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
</div>
<div class="tags-group">
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Конвертирование видео</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Конвертирование видео</div>
<p>ВидеоМАСТЕР — конвертация роликов в любой формат, подготовка к просмотру на мобильных устройствах.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/VideoMasterAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="videomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Дизайн календарей</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/design.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Дизайн календарей</div>
<p>Дизайн Календарей — сотни готовых шаблонов, удобные инструменты для создания календарей с нуля.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNmaWxlcy5ydS9DYWxlbmRhci5leGU=" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="calendarcreator">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Создание визиток</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/design.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Создание визиток</div>
<p>Мастер Визиток — 150+ готовых вариантов дизайна, удобные инструменты для оформления визиток.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNwYXJrLnJ1L01hc3RlclZpeml0b2suZXhl" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="businesscard">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Редактирование видео</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Редактирование видео</div>
<p>ВидеоМОНТАЖ — улучшение качества, обрезка, ускорение и замедление, спецэффекты и замена звука.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/VideoEditorAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="videomontage">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Восстановление данных</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/other.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Восстановление данных</div>
<p>ФЕНИКС — безопасное восстановление документов Word, фотографий, видео, музыки с любых устройств.</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" data-href="aHR0cHM6Ly9hbXNwYXJrLnJ1L0Ftc0Zlbml4LmV4ZQ==" data-target="jsdownload" href="#">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="fenix">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Запись с веб-камеры</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/design.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Запись с веб-камеры</div>
<p>ВидеоМОНТАЖ — быстрая запись роликов с помощью веб-камеры и их дальнейшая обработка.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/VideoEditorAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="videomontage">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Замена фона в видео</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Замена фона в видео</div>
<p>ВидеоШОУ — хромакей для быстрой замены фона, редактирование видео, оригинальная анимация.</p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/VideoShowAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="videoshow">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Скачивание видео из сети</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/video.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Скачивание видео из сети</div>
<p>ВидеоМАСТЕР — быстрая безопасная загрузка любых видеороликов по ссылке из брузера</p>
</div>
<div class="app-popup__btns">
<a class="js-noindex app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/VideoMasterAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="videomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
<div class="tag">
<a class="btn-tag" href="javascript:void(0)">Замена фона на фото</a>
<div class="app-popup">
<div class="app-popup__pic" style="background-image: url('img/apps-pics/photo.jpg');"></div>
<div class="app-popup__info">
<div class="app-popup__title">Замена фона на фото</div>
<p>ФотоМАСТЕР — быстрая замена фона без обводки контура. Качественная обработка снимков, ретушь портретов. </p>
</div>
<div class="app-popup__btns">
<a class="app-popup__btn app-popup__btn--load" href="https://amssoft.ru/downloads/PhotoMasterAMS.exe">Скачать <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
<a class="app-popup__btn app-popup__btn--know-more" href="fotomaster">Узнать больше <svg class="icon icon-right-arrow">
<use xlink:href="img/svg/symbol/sprite.svg#right-arrow"></use>
</svg></a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="s-news">
<div class="container">
<h2 class="sect-header">Новости софта</h2>
<div class="sect-descr">Познакомьтесь с новыми версиями наших самых популярных программ для Windows</div>
<div class="news">
<a class="new" href="fotoshow-pro">
<span class="new__label">Слайд-шоу</span>
<div class="new__pic"><img alt="Alt" src="img/news-pics/design.jpg"/></div>
<div class="new__info">
<div class="new__title">ФотоШОУ PRO 18.0</div>
<ul class="new__list">
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>12 новых шаблонов для слайд-шоу</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Удобный конструктор слайд-шоу</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Дополнительные опции для слоев-масок</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Добавлены новые переходы в каталог</li>
</ul>
</div>
</a>
<a class="new" href="fotomaster">
<span class="new__label">Фото</span>
<div class="new__pic"><img alt="Alt" src="img/news-pics/photo.jpg"/></div>
<div class="new__info">
<div class="new__title">ФотоМАСТЕР 9.15</div>
<ul class="new__list">
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Эффект HDR для пейзажных снимков</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Сохранение шаблонов текста и стикеров</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Улучшение формы тела за секунду</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Увеличение объема любых частей тела</li>
</ul>
</div>
</a>
<a class="new" href="videomontage">
<span class="new__label">Видео</span>
<div class="new__pic"><img alt="Alt" src="img/news-pics/video.jpg"/></div>
<div class="new__info">
<div class="new__title">ВидеоМОНТАЖ 9.31</div>
<ul class="new__list">
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Система отмены/повтора действий</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Ускорена конвертация готового видео</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Добавлены новогодние видеофоны</li>
<li><svg class="icon icon-checked">
<use xlink:href="img/svg/symbol/sprite.svg#checked"></use>
</svg>Улучшена функция ускорения видео</li>
</ul>
</div>
</a>
</div>
</div>
</section>
<footer class="main-foot">
<div class="foot-nav">
<nav class="foot-menu">
<div class="container">
<div class="foot-menu__part">
<div>
<div class="foot-menu__part-title">Работа с видео<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/videomontage/">ВидеоМОНТАЖ</a></li>
<li><a href="/videomaster/">ВидеоМАСТЕР</a></li>
<li><a href="/screencam/">Экранная камера</a></li>
<li><a href="/videoshow/">ВидеоШОУ</a></li>
</ul>
</div>
</div>
<div class="foot-menu__part">
<div>
<div class="foot-menu__part-title">Обработка фото<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/fotomaster/">ФотоМАСТЕР</a></li>
<li><a href="/homephotostudio/">Домашняя Фотостудия</a></li>
<li><a href="/passportphoto/">Фото на документы</a></li>
<li><a href="/collagemaker/">ФотоКОЛЛАЖ</a></li>
</ul>
</div>
</div>
<div class="foot-menu__part">
<div>
<div class="foot-menu__part-title">Полезный софт<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/interior/">Дизайн Интерьера 3D</a></li>
<li><a href="/fenix/">Восстановление файлов</a></li>
<li><a href="/audiomaster/">АудиоМАСТЕР</a></li>
<li><a href="https://amssoft.ru/neobkhodimye-programmy-obzory.php">Рейтинги лучших программ 2021</a></li>
</ul>
</div>
</div>
<div class="foot-menu__part">
<div>
<div class="foot-menu__part-title">Компания<svg class="icon icon-down-arrow">
<use xlink:href="/img/svg/symbol/sprite.svg#down-arrow"></use>
</svg></div>
<ul>
<li><a href="/about.php">Об AMS Software</a></li>
<li><a class="js-noindex" data-href="aHR0cDovL2Ftcy1wYXJ0bmVyLnJ1" data-target="" href="#" rel="noopener" target="_blank">Партнёрская программа</a></li>
<li><a href="/vacation.php">Вакансии</a></li>
<li><a href="/contacs.php">Контакты</a></li>
</ul>
</div>
</div>
</div>
</nav>
<nav class="bottom-menu">
<div class="container">
<ul class="bottom-menu__list">
<li><a href="/order.php">Оформление заказа</a></li>
<li><a href="/payment.php">Оплата и доставка</a></li>
<li><a href="/refund.php">Возврат</a></li>
<li><a href="/oferta.php">EULA</a></li>
<li><a href="/privacy-policy.php">Политика конфиденциальности</a></li>
<li><a href="/otzyvy.php">Отзывы</a></li>
<li><a href="/contacs.php">Контакты</a></li>
</ul>
</div>
</nav>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    const links = document.querySelectorAll('.js-noindex[data-href]');
    let metrikaCounter = null;
    for (let i = 0; i < links.length; i++)
      links[i].addEventListener('click', doRedirect);
    waitForYm(0, function (ymCounter) { metrikaCounter = ymCounter; });

    function doRedirect(e) {
      e.preventDefault();
      if (metrikaCounter && e.target.dataset.target)
        metrikaCounter.reachGoal(e.target.dataset.target);
      if (e.target.target === '_blank')
        return window.open(atob(e.target.dataset.href, '_blank')).focus();
      const method = e.target.classList.contains('nohistory') ? 'replace' : 'assign';
      window.location[method](atob(e.target.dataset.href));
    }

    function waitForYm(ymCounterNum, callback) {
      if (!callback) return;
      if (!ymCounterNum) {
        let metrikaObj  = (window.Ya && (window.Ya.Metrika || window.Ya.Metrika2)) || null;
        ymCounterNum = (metrikaObj && metrikaObj.counters && (metrikaObj.counters() || [0])[0].id) || 0;
      }
      let ymCounterObj = window['yaCounter' + ymCounterNum] || null;
      if (ymCounterObj) return callback(ymCounterObj);
      setTimeout(function () { waitForYm(ymCounterNum, callback); }, 250);
    }
  });
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(51938684, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/51938684" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="copyrights">
    © 2007-2021 AMS Software. Все права защищены.
  </div>
</footer>
</div>
<script src="/js/scripts.min.js"></script>
<script src="/js/yu.js"></script>
<script src="/js/main.js"></script>
</body>
</html>