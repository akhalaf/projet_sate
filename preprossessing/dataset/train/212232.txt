<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-type"/>
<title>503 メンテナンス中</title>
<style media="screen" type="text/css">
			body {
				font-family: Helvetica, Arial, Sans-Serif;
				font-size: 16px;
				padding-top: 150px;
				color: #333;
				line-height: 1.5;
			}

			div {
				border: 1px solid #ccc;
				background: #D2EFFF;
				padding: 20px;
				width: 350px;
				margin: auto;
				text-align: center;
			}

			div p {
				margin-top: 0;
				margin-bottom: 10px;
			}
		</style>
</head>
<body>
<div>
<p><strong>メンテナンス中です</strong></p>
<p>現在ベストケンコーは以下の期間、パフォーマンス向上のためのアップデートを実施中です。<br/><br/>
ご迷惑をおかけしますが、完了まで少しの間お待ちください。<br/><br/>2018年3月20日（火）午前7時〜午後12時頃</p>
</div>
</body>
</html>