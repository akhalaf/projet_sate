<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "202",
      cRay: "610b04030c54e7c5",
      cHash: "c6eeaa4d04545df",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuMmNoLm5ldC8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "n4/qiVsRuBajRwhDqkBnfykf/b13cXfZ00d76hCPTkYVajgOVRby3eoaIXjRI+4gAtdkh5R/vQtlMDzKehPY0Lm+qsssZPULic0SDckovIIt6GREV4Ite92c4+ig2/bTZMe8+E6zD6ig15ZuOyZfHu13tU2eCIcS6aq0qn4QdEFQPK7YDIrMZKDThgS0Y5AYNkViTFv8oh+Nc+OTRrdkO0ulzkjT7Zo8LoxyBgXfyofxt+Up8uk7lukpdpdHTHeAjPK/cVrkkS3xZGXvpxvw1xQHsXiBieWlYMTXUmxoGLpLn/e40Bd+n0uTnokR4ReLQBV6jkx9hRMN8exTKLm9qedrhQ8ZuFWBKvSza8WOfTMtQp3EHTk4BwoLINlu2+1M68Gvkj1mOUg+fcNpQK0FYPTILcVL7OYNyz78suLp5zuElL2fIAHnuW98PLPoRBN5tdu21GbWUrPMqrV/c/EoLElIIYB4l+XhflmFkLB1nT2YaqqNg/kXDc+XHK/903UOqUnBj+Ae61KTVlQnteZyIZCWJcBWYNQ8UYH0vh+Da2vK9eKUh0mRzNb+aQp6tFziCX+8prkALgmif26AQ8ShWw/otDEHbYGfGZTQOG7dre6513axxXAVOHXLi8x1GQha3/GoPf/eKNNEjHcdGrbAyDxzzCedZdw8o4Ofo2WnP02bKoQlgZ/J4/jZ+kPJ0kwgC3UNakqkbYyXIxeTCGtQC7Czh0wDh4U82btAUMj9MDDtLJJc79KUQyF9RVnhzv2A",
        t: "MTYxMDQ5NzkwMC4wMzEwMDA=",
        m: "XYX6H3emJRYP1mJtWbjyE86Gv9bR8OjymxfTrCy8OPY=",
        i1: "4SGvwmSYRSU/hYwBn7Toqg==",
        i2: "GvsqXDpGRh+Oj1p9wH3cjA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "O5Al4GlQ8uMgv016DHYySx2xg+K0d46h0gOtN4yKMc4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.2ch.net</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=53105d6d3d21ebe4a7c5d9a3729ea0e7950bf706-1610497900-0-AelEQSf-mO6pHPna_QHrwZKcB3GeTArSRHDio5KjI2tNucU2aAR86LlfAyDiAiDQDREXS343HhSSYQvrilNbMvRHQ6rxdkQ7-cG8jB-dXwCXiSd2uePG3wvW2O-1_TcdwV9GI3wdVCQHbtw-sGtEbZxULOFQHEN9S3Hg7sHmrpwHCdPi3x2xZ7IVWvCOeJsjtFiCyyQn9EVKr8IKEBtKl71rNt3IKE6lKXR4Wdtv2IGW5cKg6hbYoiPh0x5rnFIPMNPkq380t-z1f_nyU6u1I--U22pQT2aZ8VIY0NypB1HWMoau_jWnu6g_OZE4IaN1zZJ5UVsYwpLgNMJTKrGHFf253uR7XmNrGnNxnkMEYZONu4KZeonpPPrP72vbcap7VRj6mst6HB6ADeGilIxoKs_LXkFElwzzWK7DG4FynH10Rhd4lTqY9JlXm7YrlUqkNrwGyV-WMINE7s9iYy_cl0vBXrKSsYvaQKxi65pRV4O-RaxL9NEt3AZNKgWlMTOKUV4678HWtrG8GKb9qdIdOw0" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="b86d5df2415d38b1bad8255fa8d546fcbb03ad51-1610497900-0-AfgcllsAzLULi0mzYsocWDtbGQpQ45iZBk0ZH8W6r5p8gYw8Z82+/h+jolwwxFnmYetAC4fGkUxpsc24wEw1miDBg5y3IBZqAkXt5clU9YSmmLrQTnx8CSXFyAg1pA0IwOy85wpMQeiIanbEv9sO+FJkJrvq1u9yavb7qgF1V+32M+tqpLnMIoGOEmFw4zvBmzb9zofJGyrDDY7xKEwos1hn3KcY7R+Y9a7mQXrq/zQX18dw257XoWzQo8ka2y++RbG8rCrAJnX0EiQzIOwQeEVFDSJt4Rf2XftUvv365EMKzwK8hFpGjkrLcwYI3Vbed5AMeRIcr8mBVB5HSCEorxG35ngEUoOwIwl+rx7M67OJxsM3Q6QOExUlqXsAV9LfiAwOJVc4AimwAsVMHDIUJHRZ9DPmd3Z7dFjGDlC+I61SwMwaIXJ2Cs/R7MYZTbyzThRpHJ24+rEF78Mb6NZ7N8H/wik8cUg1bjevDOaZidD4DOr83liszqBMd0n2nclnZQGkwcryC0O6py3HXHS6pRJL3GHPo9a0j+/q9d3yKHBUA0vG95WwaX06JtDPibT8P8IqtC4gtDg68wHZq9n+cOOhrmteBc1rBRDUgPFVPpM4ICumqODGX+WmJf3F/GoSRjb0mGT4ORvLoeoaWCC5fpfnM43tGC5n2B+s50nTYzKjve1PY7wnw7sz+5fz7nGcVSbIjiGuj1z9CknQCbqiK0pQkkbxt7bSl8SghknaN3PMbqoUcflhaebUgfgGiZIsEpQ85Yf8f86EgzE6zoELfBfZEdTsq8Ieh8LmmybskYNfIMXcNStAGqSLut1+g80aXPV0z7/qLapY/8W7uMRUAvptWhxpzexODCqBBwZLg0fO6R0zFKGzgXvoh37o5jREBuSAtQdsusmaFpcmjA6my4QotxJPgYVOAB+wkw3qTdsrCs1oUGT5ba3cTZ+oNZme/wLTThQkNkm2gCT3Snv12X5VZJSK/1Ot7eHcfGOPCW1GLIe4p0EdtDN6mcNIYa3W2RbxCOAmcg1laNHpbOhvM95mYoKkVfo19L37k0rCT0ee6zaz/m6XkL52y0qzqrTh/4lpQVayniUA/Lh2E7V6msFYihmK3UvrsOOOP6DXTPb3GYE9mcigbr8GWzW4+vlq+0M9Zf6ohkDRY+YBPIJdGj1d5o1Rg+P3aWa2pUYBsQ9Y1o9NGAnNYCWFVRhvIYvmD2yt9+kiYozusLm0Fo4DYH2z4jHAsRRtm0Fs/hdzbvdqJY3zfkOyfJU7AHuNljy5NfQf2Xat3MCluSEXOwnJs7E="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="7139ba37b60f01a959620f6bfecbf0c6"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610b04030c54e7c5')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610b04030c54e7c5</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
