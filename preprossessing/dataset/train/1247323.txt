<!DOCTYPE html>
<html dir="rtl" lang="en">
<head>
<!-- Required Meta Tags -->
<meta content="ar" name="language"/>
<meta charset="utf-8" content="text/html" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " name="description"/>
<meta content="مؤسسة رسوخ, مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية, التمكين العلمي, مداد رسوخ" name="keywords"/>
<title> مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية </title>
<!-- Open Graph Meta Tags -->
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " property="og:title"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " property="og:description"/>
<meta content="http://domain.com/page-url/" property="og:url"/>
<meta content="img/logo.png" property="og:image"/>
<!-- Twitter Card Meta Tags -->
<meta content="summary_large_image" name="twitter:card"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " name="twitter:title"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " name="twitter:description"/>
<meta content="img/logo.png" name="twitter:image"/>
<!-- Facebook Card Meta Tags -->
<meta content="img/logo.png" name="facebook:image"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " name="facebook:title"/>
<meta content=" مؤسسة رسوخ للاستشارات والدراسات التربوية والتعليمية " name="facebook:description"/>
<!-- Other Meta Tags -->
<meta content="index, follow" name="robots"/>
<meta content="رسوخ" name="copyright"/>
<link href="https://www.rosokh.com/public/front/img/logo.png" rel="shortcut icon" type="image/png"/> <!-- Required CSS Files -->
<link href="https://www.rosokh.com/public/front/css/tornado-rtl.min.css" rel="stylesheet"/>
</head>
<body>
<!-- Top Navigation -->
<div class="top-navigation wow">
<div class="container">
<span>الثلاثاء 29 جمادى الأولى 1442 </span>
<span>هـ</span>
<span> الموافق</span>
<span>12  يناير  2021 </span>
<span>م</span>
<div class="social float-end">
<a class="ti-facebook" href="https://www.facebook.com/rusukh.tamkin.9" target="_blank"></a> <a class="ti-twitter" href="https://twitter.com/rusoukh_tamkin?lang=ar"></a> <!-- -->
</div>
</div>
</div>
<!-- // Top Navigation        <!-- Header -->
<header class="tornado-header main-header wow" data-sticky="">
<div class="container">
<!-- Logo -->
<a class="logo" href="https://www.rosokh.com"><img alt="" src="https://www.rosokh.com/public/front/img/logo.png"/></a>
<!-- Main Menu -->
<div class="navigation-menu" data-id="main-menu" data-logo="https://www.rosokh.com/public/front/img/logo.png">
<ul>
<li><a href="https://www.rosokh.com/about">من نحن</a></li>
<li><a href="https://www.rosokh.com/articles/1">التمكين العلمي</a>
<ul>
<li><a href="https://www.rosokh.com/articles/3">نبذة تعريفية</a></li>
<li><a href="https://www.rosokh.com/articles/24">المهارت الفقهية</a></li>
<li><a href="https://www.rosokh.com/articles/23">المهارات الأصولية</a></li>
<li><a href="https://www.rosokh.com/articles/22">المهارات القضائية</a></li>
<li><a href="https://www.rosokh.com/articles/15">المهارات القانونية</a></li>
<li><a href="https://www.rosokh.com/articles/20">المهارات المالية</a></li>
<!-- <li><a href="https://www.facebook.com/rusukh.tamkin.9" target="_blank">رابط البرنامج الإلكتروني</a></li> -->
</ul>
</li>
<li><a href="https://www.rosokh.com/books">الإصدارات</a>
<!-- <ul>
                                <li><a href="https://www.rosokh.com/books/1">كتب الشيخ</a></li>
                                <li><a href="https://www.rosokh.com/books/2">كتب مختارة</a></li>
                            </ul> -->
</li>
<!--<li><a href="https://www.rosokh.com/media">الوسائط</a>-->
<!-- -->
<!--<ul>-->
<!-- -->
<!--<li><a href="https://www.rosokh.com/media/1">وسائط مرئية</a></li>-->
<!-- -->
<!--<li><a href="https://www.rosokh.com/media/2">وسائط صوتية</a></li>-->
<!-- -->
<!--</ul>-->
<!-- -->
<!--</li>-->
<li><a href="https://www.rosokh.com/activities">الفعاليات والأنشطة</a></li>
<li><a href="https://www.rosokh.com/articles/2">مداد رسوخ</a>
<ul>
<li><a href="https://www.rosokh.com/articles/6">مختارات من إصدارات رسوخ</a></li>
<li><a href="https://www.rosokh.com/articles/7">نصوص ملهمة</a></li>
<li><a href="https://www.rosokh.com/articles/8">تحرير القلم</a></li>
</ul>
</li>
<li><a href="https://www.rosokh.com/contact">تواصل معنا</a></li>
</ul>
</div>
<!-- Action Buttons -->
<div class="action-btns">
<!-- Cart Button -->
<div class="dropdown">
<!-- <a href="#" class="icon-btn ti-cart dropdown-btn"></a> -->
<ul class="dropdown-list cart-list" id="cart-content">
<!-- small cart -->
<!-- <li class="cart-item">
                                <a href="#" class="image"><img src="https://www.rosokh.com/public/front/img/cart-item.png" alt=""></a>
                                <div class="info">
                                    <a href="#"><h3>اسم او عنوان الكتاب</h3></a>
                                    <h5 class="price">السعر : 325$</h5>
                                </div>
                                <a href="#" class="ti-close remove-item"></a>
                            </li> -->
<!-- info item -->
<!-- <li class="info-item">الاجمالي : 352$</li> -->
<!-- info item -->
<!-- <li class="info-item">الاجمالي النهائى : 375$</li> -->
<!-- buttons item -->
<!-- <li class="btns-item">
                                <a href="Checkout.html" class="btn small primary">انهاء الشراء</a>
                                <a href="Cart_Page.html" class="btn small secondary">سله التسوق</a>
                            </li> -->
</ul>
</div>
<!-- Main Menu Button -->
<a class="menu-btn ti-menu-round icon-btn" data-id="main-menu" href="#"></a>
<!-- User Button -->
<!--<div class="dropdown">-->
<!--    <a href="#" class="icon-btn ti-businessman dropdown-btn"></a>-->
<!--    <ul class="dropdown-list">-->
<!-- -->
<!--            <li><a href="https://www.rosokh.com/login">تسجيل الدخول</a></li>-->
<!--            <li><a href="https://www.rosokh.com/register">إنشاء حساب جديد</a></li>-->
<!-- -->
<!--    </ul>-->
<!--</div>-->
<!-- Search Button -->
<a class="icon-btn ti-search" data-modal="search-box" href="#"></a>
<a class="icon-btn lang-icon" href="https://www.rosokh.com/language/en?redirect=https://www.rosokh.com/img/en/pp/%09%0A">en</a>
<!-- Logo 2 -->
<img alt="" class="logo2030" src="https://www.rosokh.com/public/front/img/2030-logo.png"/>
<img alt="" class="logo2030" src="https://www.rosokh.com/public/front/img/new-logo.jpeg"/>
</div>
</div>
</header>
<!-- // Header -->
<!-- Search Box -->
<div class="modal-box cta-modal" id="search-box">
<div class="modal-content">
<span class="ti-close close-modal"></span>
<form action="https://www.rosokh.com/search" class="form-ui modal-body" method="post">
<input name="_token" type="hidden" value=""/>
<label>كلمات البحث</label>
<input name="search" placeholder="كلمات البحث" required="required" type="search"/>
<label>اختيار القسم</label>
<select class="chevron-select" name="type" required="">
<option value="">-- اختيار القسم --</option>
<option value="Article">المقالات</option>
<option value="Book">الكتب</option>
<option value="Media">الوسائط المتعدده</option>
<option value="Fatwa">الفتاوي</option>
</select>
<input class="btn primary block-lvl" type="submit" value="بدأ البحث"/>
</form>
</div>
</div>
<!-- // Search Box -->
<!-- Page Head -->
<div class="page-head wow fadeInUp">
<div class="container">
<div class="breadcrumb">
<a href="https://www.rosokh.com">مؤسسة رسوخ</a>
<a href="javascript:void(0)">خطأ</a>
</div>
</div>
</div>
<!-- // Page Head -->
<!-- Page Content -->
<div class="container page-content">
<!-- Message Box -->
<div class="row thanks">
<div class="col-12 col-m-5">
<div class="boxing-style">
<img alt="" src="https://www.rosokh.com/public/front/img/error.png"/>
</div>
</div>
<div class="col-12 col-m-7">
<div class="boxing-style">
<h2><span>خطأ 404 الصفحة غير موجودة</span></h2>
<h4>لقد حدث خطأ الصفحة الذي تبحث عنها غير موجودة للتبليغ والاتصال بنا انقر هنا</h4>
<a class="btn primary pro" href="https://www.rosokh.com">الصفحة الرئيسيه</a>
</div>
</div>
</div>
<!-- // Message Box -->
</div>
<!-- // Page Content -->
<!-- Copyrights -->
<div class="copyrights wow">
<div class="container">
<p></p><p>جميع الحقوق محفوظة لــ مؤسسه رسوخ للاستشارات والدراسات التربوية والتعليمية</p>
</div>
</div>
<!-- Required JS Files -->
<script src="https://www.rosokh.com/public/front/js/jquery-3.3.1.min.js"></script>
<script src="https://www.rosokh.com/public/front/js/tornado.min.js"></script>
<script>
        $("body").on('click','.add-cart',function() {
            var bookId   = $(this).attr('data-id');
            var postData = {'id':bookId};
            $.ajax({
                url: 'https://www.rosokh.com/add',
                type: 'GET',
                data: postData
            }).done(function(t) {
                "غير متاح" == t ? $(".add-cart").html("غير متاح") : $("#item-added").addClass('active')
                ("updated" == t ? $(".add-cart").html(t) : $(".add-cart").html(t), a())
            }).fail(function() {
                console.log('Failed');
            });
        });
        $("body").on("click", ".remove-button", function() {
            var t = $(this).data("id");
            var postData = {id:t};
            $.ajax({
                url: "https://www.rosokh.com/remove",
                type: "get",
                data: postData,
                crossDomain: !0
            }).done(function(t) {
                a();
            }).fail(function() {
                console.log("Failed");
            })
        });
        function a() {
            $.getJSON("https://www.rosokh.com/cart", function(t) {
                $(".cart-counter").html(t.count), $("#cart-content").empty(), $("#cart-content").html(t.cart), 
                $(".cart-coupon").empty(), $(".cart-coupon").html(t.coupon),
                $(".cart-total").empty(), $(".cart-total").html(t.total)
            })
        }
        a();
        </script>
</body>
</html>