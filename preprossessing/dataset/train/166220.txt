<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "32703",
      cRay: "610f1584aaec32d7",
      cHash: "279f661c6607721",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXRrLW1hdHVyZWFuZGhhaXJ5LmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "MoPNHFP2Ep86zncDSH4KJ78GFSXYr4mriaO9IDuj3FWsXU5TnspYdb4CNbzP+YfD65GMmsP1u4ebF+9DXDjJYRB1Yx/QtdEL0ysHuo7AtEPCcZYARwGsc/f9HmelgqJAWSMt0EMU2MToL9MLB9ruZOXOOodZXWcegBqaMZUvAs1/dEFUFQhila9ftWu0squxW7Db8QQ+Cgg8/wjPLH+6anASza/cXbn/UEuxb86bH+hAr6yoLGfi9CkyFJlQXRqF4tQclyEOxM11Go2bQervIvNARCl7mwdYAgZyLGFZEM3P3M6NUSJNAwcXYxp3Ajd9XAG2GUqEPRmYr/TM0o1YbDcCjDTC3Ok7IRFA8OsLM5FFLKghV0hr52ngYJd84kRDv4v5UDf+QMWGr+a7eOB8894VQ9HWO3UreZFBS/qAgMApEYtvhudSjDq+4rjcPmeLzrH4UPsOBc0DAVtpgKE+Kwq+jl214Mx2FnrqGfh9Rj4C9DGwpTJmx3h9FzVn9Rph0aAgW5NmLTbf3V/ODhueKl3DTciSu5lR3C9sqkojBI/+2kP2v2e0Swmv/40inOCtfTz+YSVR0FrPNG1kerGZti7bv/g+i9tbdbrDU+KW9GuvMXXhWiCppybwxbwkcE0KquisuYCRwlnFGxvaVncildyQBoNb1wfNdnPFrF/6OG7ARU3ANrSt98ZhDdkHg74njejBPPM1quMdJjgtXx69JbKZIwigCpeKR+6mctJ6c7yI69E2lFZcGfHVdix7/dWt",
        t: "MTYxMDU0MDU2MC4xMTQwMDA=",
        m: "WOCLCDTSSvdUe4D4r51fsB3LXdz1jM1llnQVjfEajNM=",
        i1: "vuQslSR06ad+FTBUiTPskw==",
        i2: "Ib3Hbw0MlkaUyPTLS852oQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "zCHRxiHXSZvqAx2mardkszLVzjLVZv7Yrsnh4JaN8x0=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.atk-matureandhairy.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=24dfbf9722a0c1c15c9eb99a77513032c521b9ca-1610540560-0-AdG-Pz--QTe2FUf47rXaz8pQKaOfE1_hHau7GlS6qLhdyI0oitqycdmkuZED6QfWz2iaaILHRxtIvN236htmHZBc_TMPrqmua1vhYp2Cwsa9UavGDCiHeDVZJWnsV7d0W47mGxEOYRtYJRLB3w7ABnIMf-fFAcqEiIoXi3Wo6NrIPp70wcgHyWYsfaNZ7r4jc45xBYGla03wBth16EwPdDjn_Rsjb9cEymgAVMahvmelQeOhZsygXZ8ULkQI5Ku8YVUHAEr_ty9odYWMqdG7U6Lks0xuSmFQ0v8cqfsVuGHM56xb4QCP6NBCPP9zPI1JY8ZCNryrPpEe-dPmE8aXFHakNRR-CGLiY9vFCA0tQcwp6N3Rxec8KALb5tMzMnQ3V9GyxXKOtPvK4ptRvjuufTXnnJogfxyRtxw7i_p22ojSgZJ8NcbOKrnoeBtH65-lwJXXsh4UndnAaKOQvQg5Bx3aGTCz2LQ_X5hpGuIwfeDTM6BNdwouqUtlBH5B5SkDG5F5TK0ji7ZSOtIxCl--EjI" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="db80801584ed63f2a58ab457444601abbef65514-1610540560-0-AW8IasK62h1eSeXEaLEyH1EsWy2t0s/3CsgZIxT0FpQB1op5IkLt+LVA2Y6cgXdp8odecfvK1Rj0KkT+zBbovyPHEECHSbx6G4qc5DKlJZG1m9kTZeO98USNp/xj+8nEZwy5GTpd6+lXFUAsFrzMXJCKR2HnJGMGrlo4KUPaA6A8l5z0xBdhpHNiQ4gW+FitHcmL6N789f8rbNPH6FI7oWTT4tHI67iPiutEISR+X/5WOR54Ql4mVBU4W7xHz9iMdwPttwMFKTLNqI1jkCEWEM5wdVsiWnJaewN0w4q3+sJzv6pUfch657H1C3lxF3u8+9lz4ggU6tarDE/jTgbRqkTiMd/JanvfWsS5NePZtKvpqGHXBrDrk3/DeyshC1MeUalZNh1yf4oL8FWezGQC8JvLHY4MdsfUVt854JvpiQKyn5THgWkVKINXAFgj/hosRO7pHiGh1MeP3sgT/SCeLjlPLq+lZ8I2Gatnx3S2QbGgZhfDZv12JrOOgmT0xa0T+A6ryl0QFHjHMzkgC3FJsmGsAX5JIL/JD+gBAqnsNLbourf8tZvDC/9dQHQTyuepM6weH85/t+WGxBd/+Qp7QbN4OvrBtRU76e6RIMXbSw9eoEhgv3uIS0+dJp19BLRqFl4G9h0gQVt5XtKaOyjUu5cu40mVwM9aIIosqUkM8pgfT7y+6MwuwCGuSXFoe12CA+++Labt6xCxpWByy3QJg7qRmURcAU/W30hy03Tiz4k2Acr0hm1+NrjpUwov+YDCwyetXkbUwTYaBxts2iBzZM65RQBH61GTKP025FGlW7YMn/2Wvhb23z1Od9BCLk0X7r9KZA3Egf81M/dGJ5X7iS3g1Yv68VhYNlx4xzUipxmsi10VZjK0I1Ex4uyvMZwUBVpdUI4AGs90eoZEsD4pEp6lIE/o0i3eY5DxVFwgM/58bIX7rGFfdudxGdtR+SoekZmYaC6Thm2/gnJpCyfg/xZvjctm7sbx5mOq3itVXd/nKdvuWbgFaLk5ZuH05OlKHyKvey9XmlVVuOynZJSJMTzQ5/fs3eFDuDKOamUnFWK/2v+HheIOuoHLSdmYBSpQBySd8aHqZQ9Gd4Ue5KLiEg7SxsTwqEDBXrpxZaVUr1deFCwD3mZNFp0e7G5H3iUiNKf55wCEeIHBhVV2Bn4ETBRsEQIliAyHYd3tkrhzcP0K3j3bYouv3dB59aMwUJ4XFT2Itdib9bVvj37mjWp7oNLuwwBmr8r//nBZh1fuKSGmCWUZZePCMy0cBX95VXVZg6EtSS6mDAoqAWrfVSyfV6krT5K1Q0qb7tTC0OmRu4tVteddV1uDo5cQ7YJJWdrRZN4v7D7cTnh1ehz9h+bPx60="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="9e746b509eaab2b4613a1c0dc10db143"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610f1584aaec32d7')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://tempestsw.com/nightingalesguilt.php?c=0">table</a></div>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610f1584aaec32d7</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
