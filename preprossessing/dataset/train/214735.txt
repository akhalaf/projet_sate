<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "33022",
      cRay: "6110dba64b541995",
      cHash: "29421d2884f7ea3",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly9iZXlvdW5nLmNvbS5ici8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "OX2zM+gJnr3hS/4VfxBT7ASeqDrbvgyQsJ3hc4A8kk9jH9yQ678b0lXNxWGTYwdP/HCOre6p547du0Ayk9ilk9Hrp1zT6VYz6Zkt0+FiicGWDNS4aR5FQhuFrXFYuyuU3FRmYD1sXIYHv6MYu98dhCM84fb3gHuDF4hHgDNP4qOllzc+7xW7QabSKSL9RkOlSdyAJVIBEXYZspgVcBkAuPCN7dqu5Bmo6bML6uMM7gsYuNHyhV3VCm8dOx0GMgDiy/BTucwugb1gSKy0dgCPr/pltZaM4e4N/4IIh8HA2r56o/Azsx++HMmfO65Mok3Az2p5U7Fdhjr+U9qQmdZoxzt1T44jvkfSGD7YhGceMp62MPnEHtwPhvbu+IU9moRQplWFTXOCLvVJEyHkVwWnIvNGWoE4tFhPeViXzme0R9D3Nyvi7Gl06davDrPGaS8OxmmYrdMwT9Dk86FXNjwJfr9CKP353U0Qgc2u1EyPSJahUmhFPbVBkqdOjTOj8tLuG7IGQM/jlmjlnrVdtpQVc2BmHSkXqfL50KRsIGonJe1CiwnOu7Xu501+nbCn29O4HtXeB/4wextwMYPOdrPEjJMvsb0HmPhGtli8Ygq5D3h3CnL2aW3rFE4X+A3LmYP8vSZ1GPbp1AFwfZNJJArQX5gwqjOQ62UZEuqdPshpzhJ6eirCMiWmTBddh6eP2dMiNLX9mDaWYnC0jaJHNt52giyWIsR8Dig8DbHKiUQngW42Drfp8XIs6WBk3xguCIoU",
        t: "MTYxMDU1OTE2MS4zMjYwMDA=",
        m: "kp/Ohm9EngpEsUlqGi0vmXTOcwKgy7CqIc0egePxpJE=",
        i1: "UmClWhm1I7Ra0RqwBzdSyw==",
        i2: "QzbNNjybO/pM5w2k8sQ0gw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "FvcSWGaRy1bs5B16O5smQc46jFHpe6++RjHaeZGDn+o=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> beyoung.com.br</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=bafdd8212d746c35735f366dcbd453dda58f432b-1610559161-0-AenyKkZmHtrFbg-ZBgnLyKb5VciBsb354p_J-niiuZwHTzXk2BiK_a-mYjLvFAUVf1PsxtXUkFGfLSvV4WZ3jwiKJlA2GXKFvNb0dZn6o34qUHDeRd5FZu9H-kMhzS__qT9ZNk1hDmM-H0ro2QZU08mds9dsm89ovg-PFIhAzNvT_vt0Stkk-CqhNUrHFs9Jv8w3LDYAeHS9GKIE_DS7KUqTM5fo26V7Ikhnp64hlg9iDPbimTR4SoH596trG9H4BNPeOE9XlqKvlD_xhGQ9unvfd3OUqO5DnpmrvoOCLsARp1Z_L1PDly1CRjDzb78nhB-iMgv7_XWqgdUrE8_OZVEEf0foR_Cu4ghlRWC4YiIKHA3a56tgaqcFTrKe7BRihLtUSV6Izp-0wCFc47kC90VO6g5egv8PVzS07CeusK5h00IlpkP1-7qa0kYA5j_xsb531CyJVV4PcF3-H_pYSLQJCPQYdIXtCbXgBxfRhPa7eQ_oxXnTIh24GQnPCkFOI5Etjdk-PLjoiNNJAaTYmuA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="af5b79fe1c57a4dd537c2ece99d36623083138ad-1610559161-0-AfW3n/nxfKwgV5l9Pn9DVl2lGAHiOcWspd06uBItxue1USPN5npA6mdo0B+57BmkWLvfTNg/jWPDjFIhDoetghKVIBJl/7sFOYufAbUvcKOnSpJpbbG3vJUUt0yuNX+AAzIyDqoxf4r4yKOuSbNwYwTN2VA6P7Wzd0r6hrhvb5guRVrfoW8fDQngqje1qhRmgdvT4e86KuKC5sL20yJe8gMHgGmpet2TbX0Ezu/W0/Ym4VjeVUmICf+EPPWsuHbhdwQ/D5e2OcjMBg4xHjZ0lLosIXCHG8RdqIp5v9F4kgMgAX8uq1Xa0xn/fuKYIT1RmGR9BcIHvS5shboQku6aPBI2hdkscHw43reozNcQP19O+PHRI3NFKJTOQoDZ4G9F0xZTXGu5JiKq7VyUnhxgflAK0EmjUb1B3UkrFcHBEM9sJCwrgQ055fd6yyLifJEYo3QvmI1Eysu8c6OJ6hRu7DEdQt0avEMhce1snafWgpzffFS596gYs8N/siXdH8SIx9Wp1wFGdRwsVXYqTNwU7L/QPFSyO+lu/cSCRSh/pgui27D+v3rK4AmH9l9GX3vZG5k7MdDwXXnValnGnoB99xuJTzUgAKMySeK9pVyivzZnmptUG6w1oinsPk9OPhhIyGAto8IFz220bjnDq3l9uHVHmqzNqr5JlZ8zEPCoDuW1DvQPFAu861pomN6cv9OjLsx1od0ZsoklctkF6io3u1lC9as3EkogpwoBygy+9d7d+scztlfdZ/RmcX+bQ106nXrtoZt532AFlmwmcksanXG21D5kcdhDlmV8ybaIAO/PlDz8CZMjfvpYb9RDksE2GmZT3emP60jBBBu8DwdoHkVba1WShSDCeHOSnMNVlCp4SLEx6tGuFqoNaIcOBzVz2J76IiZjt+FxZWKO1bmT2rBzX2m5mdGF5bNxReLKIRsCIC7K5WU6SDmxncXaM1oqSYSmhp6Pp325m/55kwii0V531zpd3g9S/MY0ZrFv2NbZ8B9BRN2u+yuvcp5/x3xaxLKOnK2BCHfeSov1seAFFkLEm/R8fCCv87/aqD/Rrg997asSo2txhZZ2b2RHbtdinSKReZhsc+xxC+7/Sq3IwvQ1RDbSPMkbg5uVIcOigBw/5CS63NPxOMVSvv0ye8+kJId3x/ePnMaj3q6CHhB44mx2YCwS5TQTy6U6BS4q19FDBN1pA0P7IPjlFEfCUrzVstYyWfkoW5Gjzpy7AeaBaSvrtm7qHcgBRpkyGqg+E7mDDM9kZNLMyBXs0es43PbxrfL741jzEEiECm5rMtK2ZaYPmXRDVhUH8s4g02uSqWSespruMliugJE4hWuAY/tlbFkfknqeyZ6CMBK8DH4OJSM="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="265210e53b11ebd2aca737d64716a768"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6110dba64b541995')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6110dba64b541995</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
