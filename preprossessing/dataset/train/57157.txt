<!DOCTYPE html>
<html lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title></title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.abcmaxi.com/themes/warehouse/assets/cache/theme-7f397e47.css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://use.fontawesome.com/f26ca72beb.js"></script>
</head>
<body class="maintenance-page maintenance-page-layout-1" id="maintenance-page">
<div>
<section class="maintenance-flexbox-container" id="main">
<section class="page-content page-maintenance " id="content">
<div class="logo"><img alt="logo" class="img-fluid" src="/img/shopadmin-logo-1536953202.jpg"/></div>
<h1>Nous serons bientôt de retour.</h1>
<div data-days="Days" data-hours="Hours" data-minutes="Minutes" data-seconds="Seconds" data-time="2017-08-31 00:00:00" id="maintenance-countdown">
</div>
<p>We are currently updating our shop and will be back really soon. Thanks for your patience.</p>
<h2>Stay in touch</h2>
<ul class="social-links ">
<li class="facebook"><a href="https://www.facebook.com/abcmaxishop/" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li> <li class="twitter"><a href="https://twitter.com/abcmaxishop" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li> <li class="google"><a href="https://www.google.com/search?ludocid=17562284210082264742&amp;q=ABCmaxi%207%20Quai%20Am%C3%A9d%C3%A9e%20Couesnon%2002400%20Ch%C3%A2teau-Thierry&amp;authuser=1" target="_blank"><i aria-hidden="true" class="fa fa-google-plus"></i></a></li> </ul>
</section>
</section>
</div>
<script crossorigin="anonymous" integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc=" src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="/modules/iqitthemeeditor/views/js/maintenance.js" type="text/javascript"></script>
</body>
</html>