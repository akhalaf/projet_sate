<!DOCTYPE html>
<html>
<head>
<title>Сайт временно недоступен</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://index.from.sh/fonts.css?10" rel="stylesheet" type="text/css"/>
<link href="https://index.from.sh/index.css?10" rel="stylesheet" type="text/css"/>
<link href="https://index.from.sh/stub.css?10" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="page-content" id="content-wrapper">
<div class="message-content fl-n-xs" id="message-wrapper">
<div class="message-inner" id="message-inner">
<h1>Сайт временно недоступен</h1>
<p>В настоящий момент с сайтом производятся технические работы.<br/>Пожалуйста, попробуйте зайти через некоторое время.</p>
<p>По всем вопросам вы можете связаться со <a href="https://sprinthost.ru/support/">службой поддержки</a>.</p> <span id="error-code">Код ответа #5031</span>
<img class="sprinthost-logo m-t-100" src="https://index.from.sh/img/sellers/sprinthost/logo.svg" style="height: 60px; width: 128px;" title="Хостинг от Спринтхост"/>
</div>
</div>
<div class="hidden-xs" id="game-cat-link" onclick="CatLink.start();">
<div class="game-cat-link-arrow-bottom push-me-animation"></div>
<div class="game-cat-link-animal animation-cat"></div>
</div>
<div id="game-cat-wrapper" style="display: none;">
<div id="game-cat-content">
<div id="game-cat-link-end" onclick="CatLink.end();">
<div class="game-cat-link-arrow-top push-me-animation"></div>
<div class="game-cat-link-animal animation-cat"></div>
</div>
<canvas height="282" width="935"></canvas>
<div id="start" onclick="game.initStart(); this.style.visibility = 'hidden';"></div>
<div id="skip" onclick="game.startNewGame(); this.style.visibility = 'hidden';"></div>
<div id="replay" onclick="game.replay(); this.style.visibility = 'hidden';"></div>
<div id="replay-end" onclick="game.replay(); this.style.visibility = 'hidden';"></div>
<div id="sound" onclick="game.soundToggle();"></div>
<div id="games-sph-cat-link" style="display: none;">
<a href="//games.sprinthost.ru" target="_blank">хотите сыграть в другую игру?</a>
</div>
</div>
</div>
<div class="game-content hidden-xs" id="game-wrapper" style="display: none;">
<div id="game-svg"></div>
<div class="game-sound sound-on" id="game-sound" onclick="Points.sound();"></div>
<div class="game-link-arrow-left" id="game-exit" onclick="Points.exit();"></div>
<div id="game-points">
<div id="game-unsupported">
<h4 class="h4">Не поддерживается на вашем устройстве</h4>
</div>
<div id="game-init" style="display: none;">
<h3 class="h3">Поиграем?</h3>
<h4 class="h4">Соедините точки<span class="arrow-bottom"></span></h4>
</div>
<div id="game-timer" style="display: none;"></div>
<div id="game-start-overlay" style="display: none;">
<h3 class="h3 countdown"></h3>
<div class="cat-paws">
<div class="cat-paw"></div>
<div class="cat-paw"></div>
<div class="cat-paw"></div>
</div>
</div>
<div id="game-next" onclick="Points.next();" style="display: none;">
<h4>Отлично!</h4>
<div class="arrow-right"></div>
</div>
<div id="game-replay" onclick="Points.replay();" style="display: none;">
<div class="arrow-replay" title="Повторить"></div>
</div>
<div id="game-finish-overlay" style="display: none;">
<div class="finish-cats">
<div class="finish-cat finish-cat-1"></div>
<div class="finish-cat finish-cat-2" style="display: none;"></div>
<div class="finish-cat finish-cat-3" style="display: none;"></div>
</div>
<h4 class="h4">А вы умеете шевелить мышкой!</h4>
</div>
<div id="game-again-overlay" onclick="Points.again();" style="display: none;">
<div class="arrow-replay" title="Повторить"></div>
<h4 class="h4">Еще разок?</h4>
</div>
</div>
<div style="text-align: center; position: relative; z-index: 1000; display: none;">
<a href="//games.sprinthost.ru" style="font-size: 10px; z-index: 50;" target="_blank">хотите сыграть в другую игру?</a>
</div>
<div class="scripts">
<script src="https://index.from.sh/js/raphael.js"></script>
<script src="https://index.from.sh/js/points.js"></script>
<script src="https://index.from.sh/js/cat.js?20170516_10"></script>
<script type="text/javascript">
                function toggleVisible(el) {
                    if (el.style.visibility == 'hidden') {
                        el.style.visibility = 'visible';
                    } else {
                        el.style.visibility = 'hidden';
                    }
                }

                function toggleDisplay(el) {
                    if (el.style.display == 'none') {
                        el.style.display = 'block';
                    } else {
                        el.style.display = 'none';
                    }
                }

                window.onload = function() {
                    window.staticUrl = 'https://index.from.sh';

                    var $elMessage = document.getElementById('message-wrapper');
                    var isVisibleMode = $elMessage['clientHeight'] < window.document.documentElement['clientHeight'];

                    var errorHelp = document.getElementById('error-help');
                    if (errorHelp) {
                        var errorDiv = document.getElementById('error-div');
                        if (errorDiv) {
                            if (!isVisibleMode) {
                                errorDiv.style.display = 'none';
                                errorDiv.style.visibility = 'visible';
                            }

                            errorHelp.onclick = function() {
                                if (isVisibleMode) {
                                    toggleVisible(errorDiv);
                                } else {
                                    toggleDisplay(errorDiv);
                                }
                            }
                        }
                    }

                    var errorDomain = document.getElementById('error-domain');
                    if (errorDomain) {
                        errorDomain.innerHTML = document.location.hostname;
                    }

                                            window.game = new Game('game-cat-content');

                        var minHeight = 482,
                            minWidth = 480;
                        var height = window.document.documentElement.clientHeight,
                            width = window.document.documentElement.clientWidth;

                        if (height >= minHeight && width >= minWidth) {
                            document.getElementById('game-cat-link').style.display = 'block';
                            game.render();
                        }
                    
                    Points.init();
                };
            </script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
                (function(d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter43792989 = new Ya.Metrika({
                                id:43792989,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true,
                                trackHash:true
                            });
                        } catch(e) {}
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/43792989" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</div>
</div>
<div class="clear"></div>
</div>
</body>
</html>