<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "61698",
      cRay: "610ed03e289b3291",
      cHash: "f3214af43a9a243",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXNobGVpZ2htb25leXNhdmVyLmNvLnVrLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "3xf2xl47EVeQTpvj+1GUpU+9IOI3NEVmDC8J7ZSpjf6iT6DzqS7fuOLoXStWLcvYA+9aGANAEkjopyiyGCvf3mtLrF/AIaH62mFe8IbtDryuLKxQXwBA0l7fzUXdOx8fMWB405pBQU/LkEIw53OxXqbtY4fdq9oGMPHTS724MMtbc0dxmYLdvxgkCOwWj/fxGeDmtEVf4bjPJEr3ykkWYxANCe9Xpp9XX9MkEjf9NAErwVjQuEeWZ3JYXL51xdR5ngPTGzySiymMp02H9sgfYDujdnr8SzDG1n+22rkv1RpcshYhL7ckHfDgeBTW2r83aErkGDo6KVSuibSNg4eGSIMkhENuy0OkEnFKdoihQIZFV/KaQIR6O6kvkgJeXWIYyl4ks7bT5Tsmzg9j43a+X/FhZhDD/cs+hCsUJoDqszD368T6nRZr3dYQn/dZIoCsrXYZ1jNlp3HiDBX3F6hRvVNy4eqFM3UckOfMxz5eMvOetKjURntK5qJC6w3mcAHKm37wsKGTkSZa7c4uwUo49a4U67cljfzL+Hyk3/q4bPqyjGgS3Bd85uLmHFDo6XxZAnddrnIPN9oHYtlzJcANpCIwJkQkrEcJYOLsrWNXse1bSOkvkUrTN9dHD6bzda8eaVjnBs0h9lss/kUowze9cX/7Nf/lqJQBL/DmVTg791aDxgZXoH0bcDDQaohYLNCo6BZBFPhXjjD0UIuzcF5V8gKgzS6Qa8jP0fv30FdrBuO1KOW4p6TxqUhdNmLpsUux",
        t: "MTYxMDUzNzcyMi41OTcwMDA=",
        m: "oFse1R+BVbIFn4JGQ/8jV5E20aip+iK6fuwCnvGuNrI=",
        i1: "b1SJTw6N/JrMe9UGrrQodQ==",
        i2: "r7OMnmVdCslZBCd8xHzh4g==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "jhe7RKnhQHfPC9CaoyIs4lY9rdU9PLv9iTSxMLJIv4Y=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.ashleighmoneysaver.co.uk</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=8baab000726e233a65b73594ec5bc0f297278018-1610537722-0-Ae3fDXUj-7QQDQyhUiGr0MyA6ngBvWCQaiDcOs196Y_dzTRidsWaB96xUIliw5lR8dpn6EEMYjf22sdiQtKGfIOw1eJc2fMYfjGq9VFjaGE3yR0hX7STtgbvJCTArPgR5cqTohoeqeo9wvxZ8W4W5vYABsSJqsEu8MEDUWDxQmugFKU7Y3l1w6fK0Y1P0U5rxDGGcSJRPqUqFi0a52oHwgao4n8qfRuoLxldpb5xAl7Prx_cWEO34Gh8N1z2PlbKtDs4Cc4Sujr6qieBZgGAixmorlItLqEvmIDfkcxV_qsF-bYZ-qYgKqCVNJQ2dkCshxHttUMiUBTV46ZEz3yMfuBWULDQSwvHASQHN9NstxQo3xYJM6dEet00zKfspLTa7XPHBcQraeL3Z2qV_p-4myK6GHMnFFPXbqZw8-2MRcKXKe4dpwJ5CDYYz8Uc7Vrhzgy4EE1RIyV5T5B3Zh_58Vh6i8jTdhndYafWO3wc5vNVwJ6lzQoXEsuJXbjXtiorS1rNylHW7CDqA7bVAXqL-v8" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="8110b9ceb64fddff30c81b809cfd412a8c86e2b0-1610537722-0-ASXagE9rEyYctEK11bpu2U4nLa7PVYeaTLnhFHtJiqil/lufD8KOfwZz9gl9YDhQQjyaRCw+AT80Zb0jy94PCZ7bK+ckGjyrWlchnWBsBcsPfjOPxUCW8j1G9mztl8HadAUwwfK6rfghmnFLMauTGzBj8BudAHUvCuL8hL/Y9Y4ODaBWYFfcJOAuDwBzAR1EbBoT88QScAexqd6LOO+X6Pm8eM4w0EAGezWtULCD3dl9nwT/oNU03RUmjgNvMyr1S40TtIXltn4yUMd9IO4jXUjmZfBtekUf+BPKpL3jKkoQMlMqPTftp5PWQNrHUKUh47ihAmtpaJxRBTnegn5FjoS+UtwIOhCBvoO6e9YG05sAwqkfBkgGf/lMo7tYzw58j8edj7/0o7mNIMMAbnmyLb8BNoWP2U/ICeuF/7EKJevCMwaycuLtiJYviDJiEpBdX3xS65WGEptxpY/rYEWp4wTAmIVpZa8wTZ2B7nAp5DVYvcDTjUHM89CBZ018FqW0jXHZnZ+WkokQNdv5lklQNF1btO6/b2cae9xM5zwhEFa1vqvrSi4bMQsAJneq6d0iqiotzZ6qmpFqVNFkCLNJRDbQOOG0F1rpab1FUF4rmNwlMZldyAOi63NL5TejCJrQPqqK4RIh4oWqZNcn78zdCD1TwfgZeMzAqX0iZI0fRERjPSu32oMD/v+Mmp01dGizswY2Ks7T91+pDSKnMzIyvN7BiafCgQhNDu+5GJXceA54f5zljJ1T4WrQIPO6l053ADtyK6UyozoBlXFt+O2ehHH3rFfrbsreUctJE/hl4wGvoK+pri0SE7TkKm8ULFMkjhDDMwI8rf3ca+4MrUBr+YjYvDAOQZpISv6on2PFTKO5CgYoJKD1X8HpKHvPA+3/V76c/B3lV5DuiLoMLxI2esyZB09d4pua6PLK+uu0nrEjNOlnTXrrRfVTgE3yO0kPHQxHv5qb3227agJUPXgpTkBM8YcStMmoXCI5uy9WRjVuDY0XOvKD6LxuCE9n4PneeYYWGYgKXgkmRWF6xbrCAvwrGTRxI8T4lNsX5a2/g4/NGLDJLFYboxo3jo3dqmPltAj7yHwAWddrqUS8RBWLBTysz0yTStiZIsA1y0/Gg7igqbZBIzB3ikmSzfpaPQfB2Koupbt3IumjyxK3JDlFFx+mtcZOzADKj4Iv4cBCfuZsV3FP1uh9NdzlNClyC/cI/o3D3Wv9FgX6IpK64PKXdZawworfdAUzIPPEVPETAKn6ynUxv1NGhJDu64UBpIwJIrZfSna5n1gc0pStFlDfWYkpPiyqyTC/lWqPo8WlnZKm5SrHKwCPm9wKT1xTWUUMnoBINXgta7CXDEMQolGG6Jt6p3mh+r48o2R+SY/Vpc8+"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="5ce38ee9e7e4e7249a4f2dbdd5aa7327"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ed03e289b3291')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ed03e289b3291</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
