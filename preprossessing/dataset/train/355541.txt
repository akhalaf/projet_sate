<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Consorzio S.A.V.O. Autoparco Veneto Orientale, parcheggio mezzi pesanti, stazione di servizio carburanti, servizi per autotrasportatori, area di sosta video sorvegliata</title>
<!-- meta tag -->
<meta content="it" name="language"/>
<meta content="Consorzio SAVO - Autoparco Veneto Orientale, area di sosta per mezzi pesanti, parcheggio per camion, servizi per gli autotrasportatori" lang="it" name="description"/>
<meta content="autoparco,consorzio veneto orientale, savo, parcheggio camion, parcheggio mezzi pesanti, distributore carburanti, stazione di servizio" lang="it" name="keywords"/>
<meta content="public" name="distribution"/>
<meta content="swstudio.it" name="author"/>
<meta content="Copyright © by Software Studio" name="copyright"/>
<!-- Robots -->
<meta content="index, follow" name="robots"/>
<meta content="14 days" name="revisit-after"/>
<meta content="all" name="googlebot"/>
<meta content="all" name="msnbot"/>
<link href="css/master.css" rel="stylesheet"/>
<!-- SWITCHER -->
<!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<link href="/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/favicon/manifest.json" rel="manifest"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
</head>
<body data-scrolling-animations="true">
<script data-starcookie="statistics" type="text/plain">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74307025-1', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);

</script>
<div class="sp-body">
<header id="this-is-top">
<div class="container-fluid">
<div class="topmenu row">
<nav class="col-sm-offset-3 col-md-offset-4 col-lg-offset-4 col-sm-5 col-md-4 col-lg-4">
<a href="/CNZ_18_SOC/">AREA SOCI</a>
<a content="nofollow" href="CNZ_16_GES/index.php">AREA ADMIN</a>
<a href="index.php"><img src="/media/flags/italy_640.png"/></a>
<a href="eng/index.php"><img src="/media/flags/england_640.png"/></a>
</nav>
<nav class="text-right col-sm-3 col-md-3 col-lg-3">
<a href="https://www.facebook.com/Autoparco.ConsorzioSAVO/" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/ConsorzioSAVO" target="_blank"><i class="fa fa-twitter"></i></a>
<a href="https://www.youtube.com/channel/UCNdicw0eStuAnmLqkcfWGvA" target="_blank"><i class="fa fa-youtube"></i></a>
</nav>
</div>
<div class="row header">
<div class="col-sm-3 col-md-3 col-lg-3">
<a href="index.php" id="logo"></a>
</div>
<div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-sm-8 col-md-8 col-lg-8">
<div class="text-right header-padding">
<div class="h-block"><span>CONTATTACI</span>0421.276795<br/>0421.271091</div>
<div class="h-block"><span>EMAIL</span>ufficio@consorziosavo.it<br/>info@pec.consorziosavo.it</div>
<div class="h-block"><span>ORARI UFFICIO</span>Lun-Ven: dalle 08.30 alle 12.30 e dalle 14.30 alle 18.30<br/>Sabato 08.30 - 12.30</div>
<div class="h-block"><span>COME RAGGIUNGERCI</span><a href="contatti.php">Vedi la mappa</a></div>
</div>
</div>
</div>
<div id="main-menu-bg"></div>
<a href="#" id="menu-open"><i class="fa fa-bars"></i></a>
<nav class="main-menu navbar-main-slide">
<ul class="nav navbar-nav navbar-main">
<li><a href="index.php">HOME</a></li>
<li><a href="associazione.php">CONSORZIO</a></li>
<li><a href="servizi.php">SERVIZI AI CONSORZIATI</a></li>
<li><a href="associarsi.php">COME ASSOCIARSI</a></li>
<li><a href="accesso.php">ACCESSO PARCHEGGIO</a></li>
<li><a href="distributore.php">DISTRIBUTORE</a></li>
<li><a href="contatti.php">CONTATTI</a></li>
</ul>
</nav>
<a href="#" id="menu-close"><i class="fa fa-times"></i></a>
</div>
</header>
<div class="owl-carousel enable-owl-carousel" data-auto-play="true" data-main-slider="true" data-pagination="false" data-single-item="true" data-stop-on-hover="true" id="owl-main-slider">
<div class="item">
<img alt="Autoparco Consorzio SAVO a Portogruaro" src="media/main-slider/autoparco_savo_vista.jpg"/>
<div class="container-fluid">
<div class="slider-content col-md-6 col-lg-6" style="background-color:rgba(0, 0, 0, 0.6); padding:10px;">
<div style="display:table;">
<div>
<h1>AUTOPARCO CONSORZIO S.A.V.O</h1>
</div>
</div>
<div>
<p>Area di sosta videosorvegliata a Portogruaro, servizi per autotrasportatori che transitano in A4 Venezia-Trieste.<br/>Distributore aperto al pubblico, convenienza e qualità garantite.<br/><a class="btn btn-success" href="associazione.php">Scopri di più</a></p>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Distributore Autoparco SAVO" src="media/main-slider/distributore_autoparco_savo.jpg"/>
<div class="container-fluid">
<div class="slider-content col-md-6 col-lg-6" style="background-color:rgba(0, 0, 0, 0.6); padding:10px;">
<div style="display:table;">
<div>
<h1>DISTRIBUTORE CARBURANTI</h1>
</div>
</div>
<div>
<p>
															 Efficaci sistemi di filtraggio per un'elevata qualità del carburante erogato: NO particelle contaminanti, NO acqua.<br/>
															 Scegli il nostro distributore e proteggi il tuo veicolo.<br/><a class="btn btn-success" href="distributore.php">Scopri di più</a></p>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Stazione di servizio Autoparco SAVO" src="media/main-slider/consorziosavo_distributore.jpg"/>
<div class="container-fluid">
<div class="slider-content col-md-6 col-lg-6" style="background-color:rgba(0, 0, 0, 0.6); padding:10px;">
<div style="display:table;">
<div>
<h1>PERCHE' FERMARSI ALLA NOSTRA STAZIONE DI SERVIZIO?</h1>
</div>
</div>
<div>
<p>Perchè da noi trovi solo carburanti di qualità a prezzi convenienti, 24 ore su 24. Un vantaggio sicuro per tutti, privati e camionisti.<br/><a class="btn btn-success" href="distributore.php">Scopri di più</a></p>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Img" src="media/main-slider/autoparco_savo.jpg"/>
<div class="container-fluid">
<div class="slider-content col-md-6 col-lg-6" style="background-color:rgba(0, 0, 0, 0.6); padding:10px;">
<div style="display:table;">
<div>
<h1>PERCHÉ SCEGLIERCI</h1>
</div>
</div>
<div>
<p>Area di sosta attrezzata. Videosorveglianza e controllo dei mezzi in entrata ed uscita. Servizi per gli autotrasportatori. Autoparco aperto 24 ore su 24, 365 giorni all'anno.<br/><a class="btn btn-success" href="accesso.php">Scopri di più</a></p>
</div>
</div>
</div>
</div>
<div class="item">
<img alt="Img" src="media/main-slider/autoparco_savo_autolavaggio.jpg"/>
<div class="container-fluid">
<div class="slider-content col-md-6 col-lg-6" style="background-color:rgba(0, 0, 0, 0.6); padding:10px;">
<div style="display:table;">
<div>
<h1>TANTI SERVIZI PER AZIENDE ED AUTOTRASPORTATORI</h1>
</div>
</div>
<div>
<p>Autolavaggio, officina, elettrauto, carrozzeria, gommista, convenzioni con le maggiori compagnie di assicurazione.<br/><a class="btn btn-success" href="servizi.php">Scopri i servizi per i Consorziati</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="block-content inner-offset">
<div class="info-texts wow fadeIn" data-wow-delay="0.3s">
<div class="container-fluid">
<div class="row">
<div class="col-sm-4 col-md-4 col-lg-4">
<p><strong class="box">CARBURANTI</strong><br/>
                                                                Stazione di servizio al pubblico, gasolio per autotrazione, bar e ristorante.<br/>Qualità e convenienza.</p>
</div>
<div class="col-sm-4 col-md-4 col-lg-4">
<p><strong class="box">PARCHEGGI</strong><br/>Sorvegliati, area di sosta per mezzi pesanti e trasporti eccezionali.</p>
</div>
<div class="col-sm-4 col-md-4 col-lg-4">
<p><strong class="box">AUTOLAVAGGIO</strong><br/>Servizio automatizzato con operatore o self service, per ogni tipo di automezzo.</p>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid">
<div class="row column-info">
<div class="col-sm-6 col-md-6 col-lg-6 wow fadeInLeft" data-wow-delay="0.3s">
<img alt="La sede del Consorzio S.A.V.O." src="media/3-column-info/sede_consorziosavo.jpg"/>
</div>
<div class="col-sm-6 col-md-6 col-lg-6 wow fadeInUp" data-wow-delay="0.3s">
Il Consorzio Gestione Servizi Autoparco Veneto Orientale, detto S.A.V.O., nasce dalla volontà di un gruppo di autotrasportatori del mandamento di Portogruaro con lo scopo principale di promuovere acquisti collettivi e fornire ai propri consorziati servizi a prezzi molto competitivi.<br/><br/>
Promosso dalle associazioni di categoria del Veneto Orientale e appoggiato dalla Regione Veneto, il Consorzio S.A.V.O. è stato formalmente costituito nel 1987 e l'area di sosta e la sede sono state realizzate all’interno dell’area P.I.P Noiari, a Summaga di Portogruaro, posizione strategica in prossimità del “Corridoio 5” collegato dalla nuova tangenziale. <br/><br/>
Attualmente il Consorzio S.A.V.O. è formato da 200 imprese consorziate, Italiane ed appartenenti all’Unione Europea, con un parco veicolare complessivo di oltre 700 automezzi.
					</div>
</div>
</div>
<div class="clearfix" style="margin: 2% 0;"></div>
<div class="container-fluid">
<div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s">
<div class="text-left" style="margin-right:40px;">
<h2>PERCHÈ FARE CARBURANTE DA NOI?</h2>
<p>Perchè eroghiamo benzina e gasolio per autotrazione di altissima qualità a prezzi più che convenienti, <br/>
                                    nel rispetto dell'ambiente e garantendo l'efficienza della tua auto e del tuo camion. Prossimamente anche distributore GNL.</p>
</div>
<div><a class="btn btn-success" href="distributore.php" style="border: 1px solid white;">SCOPRI</a></div>
</div>
</div>
<div class="clearfix" style="margin: 1% 0;"></div>
<div class="block-content inner-offset">
<div class="info-texts wow fadeIn" data-wow-delay="0.3s">
<div class="container-fluid">
<div class="row" id="prezzi">
<div class="col-sm-12 col-md-4 col-lg-4">
<p style="font-size: 1.1em;"><strong>GASOLIO ADDITIVATO</strong><br/><br/>
<span class="digital">
        1.289    </span></p>
</div>
<div class="col-sm-12 col-md-4 col-lg-4">
<p style="font-size: 1.1em;">
<strong>PREZZI AGGIORNATI AL 13-01-2021</strong><br/>
                                                        Prossimamente disponibile anche ADBLUE<br/>
<br/>Scopri dove risparmiare scaricando l'app<br/>
<a href="http://www2.prezzibenzina.it/" target="_blank"><img alt="prezzi benzina" src="media/partners/prezzi-benzina-logo.png"/></a>
</p>
</div>
<div class="col-sm-12 col-md-4 col-lg-4">
<p style="font-size: 1.1em;"><strong>BENZINA</strong><br/><br/>
<span class="digital">
        1.409    </span>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid block-content">
<div class="hgroup text-center wow zoomInUp" data-wow-delay="0.3s">
<h1>I SERVIZI OFFERTI DALL'AUTOPARCO S.A.V.O.</h1>
<h2>Ecco i principali servizi offerti dal Consorzio che ti permettono di avere tanti vantaggi a livello di risparmio, qualità e sicurezza</h2>
</div>
<div class="row our-services styled">
<div class="col-sm-6 wow zoomInLeft" data-wow-delay="0.3s">
<a href="servizi.php">
<span><img src="glyphicons/png/distributore_benzina.png"/></span>
<h4>STAZIONE DI SERVIZIO</h4>
<p>Distributore aperto al pubblico 24h su 24h, prezzi molto competitivi, convenzioni con tutte le carte carburante.<br/>Ampia manovrabilità per gli autotreni. <br/>Attivo il servizio bar e ristorante.</p>
</a>
</div>
<div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
<a href="servizi.php">
<span><img src="glyphicons/png/autolavaggio.png"/></span>
<h4>AUTOLAVAGGIO</h4>
<p>Autolavaggio automatizzato con operatore e self-service, adatto al lavaggio di ogni tipo di automezzo industriale.<br/>Dotato di pista assistita per camion fino a 18 metri di larghezza massima 4,95 metri.</p>
</a>
</div>
<div class="col-sm-6 wow zoomInLeft" data-wow-delay="0.3s">
<a href="servizi.php">
<span><img src="glyphicons/png/officina.png"/></span>
<h4>OFFICINA</h4>
<p>Officina camion e automezzi, elettrauto, gommista, carrozzeria industriale.<br/>
                            Convenzioni per l'acquisto di pneumatici, batterie, lubrificanti, accessori e ricambi a prezzi convenienti.</p>
</a>
</div>
<div class="col-sm-6 wow zoomInRight" data-wow-delay="0.3s">
<a href="servizi.php">
<span><img src="glyphicons/png/gommista.png"/></span>
<h4>PARCHEGGIO</h4>
<p>Accesso all'area di sosta con badge e sbarra, parcheggi sorvegliati dal personale e videocamere 24 ore su 24 per mezzi pesanti e trasporti eccezionali, stalli attrezzati per mezzi refrigerati.</p>
</a>
</div>
</div>
</div>
<div class="big-hr color-1 wow zoomInUp" data-wow-delay="0.3s">
<div class="text-left" style="margin-right:40px;">
<h2>SEI ISCRITTO ALL'ALBO DEGLI AUTOTRASPORTATORI?</h2>
<p>Entra anche tu nel Consorzio ed avrai sconti sull'acquisto del carburante e tanti servizi a prezzi agevolati.</p>
</div>
<div><a class="btn btn-success" href="/CNZ_18_SOC/" style="border: 1px solid white;">DIVENTA SOCIO</a></div>
</div>
<div class="clearfix" style="margin: 1% 0;"></div>
<div class="block-content blog-section">
<div class="container-fluid">
<div class="wow fadeInLeft" data-wow-delay="0.3s">
</div>
</div>
</div>
<div class="darken-block inner-offset">
<div class="container-fluid">
<div class="container-fluid partners block-content">
<div class="title-space wow fadeInLeft" data-wow-delay="0.3s">
<div class="hgroup">
<h1>I NOSTRI PARTNER</h1>
</div>
</div>
<div class="owl-carousel enable-owl-carousel" data-min450="2" data-min600="2" data-min768="4" data-navigation="true" data-pagination="false" id="partners">
<!-- <div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img src="media/partners/pneus-market.jpg" alt="Img"></a></div> -->
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/fai-service.jpg"/></a></div>
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/sanmarco-petroli.jpg"/></a></div>
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/vanello-carburanti.jpg"/></a></div>
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/diesel24.jpg"/></a></div>
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/AGENZIA_MENEGHINI.JPG"/></a></div>
<div class="wow rotateIn" data-wow-delay="0.3s"><a href="#"><img alt="Img" src="media/partners/yokohama.jpg"/></a></div>
</div>
</div>
</div>
</div>
<footer>
<div class="color-part2"></div>
<div class="color-part"></div>
<div class="container-fluid">
<div class="row block-content" style="margin-bottom: 0px;">
<div class="col-xs-12 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
<a class="logo-footer" href="#"></a>
<p class="riferimenti">
<a href="tel:0421276795">tel.: (+39) 0421.276795</a><br/>
<a href="tel:+390421276795">tel.: (+39) 0421.271091</a><br/>
<a href="fax:+390421390140">fax: (+39) 0421.390140</a><br/>
                                ufficio@consorziosavo.it<br/>
                                info@pec.consorziosavo.it
                            </p>
<div class="footer-icons riferimenti">
<a href="https://www.facebook.com/Autoparco.ConsorzioSAVO/"><i class="fa fa-facebook-square fa-2x"></i></a>
<a href="https://twitter.com/ConsorzioSAVO"><i class="fa fa-twitter-square fa-2x"></i></a>
<a href="https://www.youtube.com/channel/UCNdicw0eStuAnmLqkcfWGvA" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a>
</div>
</div>
<div class="col-xs-6 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
<h4>L'ASSOCIAZIONE</h4>
<nav>
<a href="associazione.php">Consiglio di amministrazione</a>
<a href="associazione.php">Collegio Sindacale</a>
<a href="statuto_consorzio_savo_291114.pdf" target="_blank">Lo Statuto</a>
<a href="2020_regolamento_Consorzio_SAVO.pdf" target="_blank">Regolamento Interno</a>
</nav>
</div>
<div class="col-xs-6 col-sm-2 wow zoomIn" data-wow-delay="0.3s">
<h4>I SERVIZI</h4>
<nav>
<a href="distributore.php"><span class="servizi">Distributore carburanti</span></a>
<a href="servizi.php"><span class="servizi">Servizio di autolavaggio</span></a>
<a href="servizi.php"><span class="servizi">Officina Elettrauto Carrozzeria</span></a>
<a href="servizi.php"><span class="servizi">Gommista pneumatici</span></a>
<a href="servizi.php"><span class="servizi">Convenzione acquisti</span></a>
</nav>
</div>
<div class="col-xs-12 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
<h4>ORARIO DI APERTURA UFFICIO</h4>
<div class="contact-info">
<span><strong>DA LUNEDÌ A VENERDÌ</strong><br/>dalle 08.30 alle 12.30 e dalle 14.30 alle 18.30</span>
<span><strong>IL SABATO</strong><br/>dalle 08.30 alle 12.30</span>
<span>Il distributore è in funzione 24h su 24h, in grado di erogare fino a 180 litri al minuto</span>
<span>L'accesso all'autoparco è consentito 24h su 24h con Pass o Badge.</span>
</div>
</div>
</div>
<div class="copy text-right">
<a href="#this-is-top" id="to-top">
<i class="fa fa-chevron-up"></i>
</a>© 
		2021 Consorzio Gestione Servizi S.A.V.O. Autoparco Veneto Orientale - tutti i diritti riservati - P.IVA 02261650275 - 
		<a href="privacy.php">Privacy</a> - 
		<a href="termini_uso.php">Termini d'uso</a> - 
		<a href="cookies-policy.php">Cookies Policy</a>
</div>
</div>
</footer> </div>
<!--Main-->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!-- Loader -->
<script src="assets/loader/js/classie.js"></script>
<script src="assets/loader/js/pathLoader.js"></script>
<script src="assets/loader/js/main.js"></script>
<script src="js/classie.js"></script>
<!--Switcher-->
<script src="assets/switcher/js/switcher.js"></script>
<!--Owl Carousel-->
<script src="assets/owl-carousel/owl.carousel.min.js"></script>
<!--Contact form-->
<script src="assets/contact/jqBootstrapValidation.js"></script>
<script src="assets/contact/contact_me.js"></script>
<!-- SCRIPTS -->
<script src="assets/isotope/jquery.isotope.min.js" type="text/javascript"></script>
<!--Theme-->
<script src="js/jquery.smooth-scroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/theme.js"></script>
<script src="js/cookies.js"></script>
</body>
</html>