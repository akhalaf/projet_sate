<!DOCTYPE html>
<html lang="es">
<head>
<title>DAMARQCHILE</title>
<meta charset="utf-8"/>
<meta content="DAMARQCHILE somos la solución en repuestos para cosecheras CASE IH en Chile" name="description"/>
<meta content="DAMARQCHILE, repuestos CASE IH chile, repuestos, axial flow, case ih 2188, case ih 2388, case ih 1660, acople rotor, cadena estera, buje zarandon, correa picador, polea tensora" name="keywords"/>
<meta content="index,follow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="humans.txt" rel="author" type="text/plain"/>
<link href="sitemap.xml" rel="sitemap" title="Sitemap" type="application/xml"/>
<link href="css/flexslider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/estiloindex.css" rel="stylesheet" type="text/css"/>
<link href="css/tinycarousel.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<!--<script src="https://apis.google.com/js/platform.js"></script>-->
<script>
			!window.jquery && document.write("<script src='js/jquery.min.js'><\/script>");
		</script>
<script src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
			$(window).load(function() {
				$(".flexslider").flexslider();
		});
		</script>
<script src="js/jquery.tinycarousel.js"></script>
<script type="text/javascript">
		$(document).ready(function()
		{
			$('#slider1').tinycarousel();
		});
		</script>
<script type="text/javascript">
			$(window).load(function() {
			$("#noticias").load('news.html');
		});
		</script>
<!--[if lt IE 9]>
			<script> src="//html5shiv.googlecode.com/svn/trunk/html5.js"</script>
		<![endif]-->
<script src="https://apis.google.com/js/platform.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-67198100-1"></script>
<script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-67198100-1');
        </script>
</head>
<body>
<!--Carga del js de facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
<header id="cabecera">
<section id="logo">
<figure id="logo">
<a href="index.html">
<img alt="DAMARQCHILE somos parte del ciclo" class="fade" src="img/logo3.png"/>
</a>
<figcaption>Somos parte del ciclo<figcaption></figcaption>
</figcaption></figure>
<p>Venta de Repuestos para CASE IH y Servicio de trillas en zona centro-sur del país</p><br/><br/>
</section>
<section id="slide">
<div class="flexslider">
<ul class="slides">
<li>
<a href="contacto.html" target="_blank"><img alt="Semi ejes de torsion, torsion, remolques, carros de arrastre, colosos, frenos electricos" src="img/semiejes.jpg"/></a>
<!--<p class="flex-caption">Consulte por semi ejes aquÃ­</p>-->
</li>
<li>
<img alt="DAMARQCHILE repuestos para CASE IH en CHILE" src="img/slide-1.jpg"/>
<!--<p class="flex-caption">Repuestos para CASE IH en Chile</p>-->
</li>
<!--<li>
							<a href="http://www.tractordrive.es/" target="_blank"><img src="img/slide-tractordrive.jpg" alt="tractordrive"/></a>
						</li>-->
<li>
<a href="repus/tienda.html"><img alt="DAMARQCHILE Servicio de Trilla, repuestos case ih" src="img/slide-2.jpg"/></a>
</li>
</ul>
</div>
</section>
</header>
<nav id="barra">
<ul>
<li><a href="index.html">INICIO</a></li>
<li><a href="repus/tienda.html">TIENDA</a></li>
<li><a href="trilla.html">TRILLA</a></li>
<li><a href="nosotros.html">NOSOTROS</a></li>
<li><a href="contacto.html">CONTACTO</a></li>
</ul>
</nav>
<section id="matriz">
<section id="principal">
<br/>
<section id="noticias">
</section>
<br/>
<!--<div id="slider1">
					<a class="buttons prev" href="#">&#60;</a>
					<div class="viewport">
						<ul class="overview">
							<li><a href="repus/tienda.html"><img src="img/repuestos/Buje zarandon grande.jpg" alt="Buje zarandon grande"/></a></li>
							<li><a href="repus/tienda.html"><img src="img/repuestos/Buje zarandon mediano.jpg" alt="Buje zarandon mediano"/></a></li>
							<li><a href="repus/tienda.html"><img src="img/repuestos/Buje zarandon chico.jpg" alt="Buje zarandon chico"/></a></li>
							<li><a href="repus/tienda.html"><img src="img/repuestos/Maza variador de ventilador.jpg" alt="Maza variador de ventilador"/></a></li>
							<li><a href="repus/tienda.html"><img src="img/repuestos/Engranaje para sin fin.jpg" alt="Engranaje para sin fin"/></a></li>
							<li><a href="repus/tienda.html"><img src="img/repuestos/Acople para rotor.jpg" alt="Acople para rotor"/></a></li>
						</ul>
					</div>
					<a class="buttons next" href="#">&#62;</a>
				</div>-->
</section>
<aside id="costado">
<br/>
<article id="WEBPAY">
<p>ACCESO AL PORTAL DE PAGOS</p>
<figure>
<a href="http://www.webpay.cl/" target="_blank"><img alt="Portal de pagos, WEBPAY, TC, Tarjeta de CrÃ©dito, Redbank, Transbank" src="img/webpay.jpg" width="80%"/></a>
</figure>
</article><br/><hr/><br/>
<article id="caseih">
<figure>
<img alt="Repuestos CASE IH, CHILE" src="img/Case IH logo.jpg" width="80%"/>
</figure><br/>
</article><hr/><br/>
<!--<article id="tractordrive">
					<figure>
						<a href="http://www.tractordrive.es/" target="_blank"><img src="img/tractordrive.png" alt="tractordrive"/></a>
						<figcaption>Banderillero Satelital</figcaption>
					</figure>	
				</article><hr>-->
<article id="oferta">
<figure id="oferta">
<a href="http://www.poluzzi-track.it/it/" target="_blank"><img alt="Orugas Poluzzi" src="img/Logo Poluzzi.png"/></a>
<figcaption>Orugas Poluzzi</figcaption>
</figure>
</article><hr/><br/>
<article id="face">
<p>NUESTRAS REDES SOCIALES</p>
<figure><img src="img/face.jpg"/></figure>
<div class="fb-like" data-action="like" data-href="http://facebook.com/damarqchile/" data-layout="button_count" data-share="true" data-show-faces="true" data-size="large" data-width="20"></div>
</article>
<article id="video">
<br/><div class="g-ytsubscribe" data-channel="alsparatano" data-count="default" data-layout="full" data-theme="dark"></div><br/>
</article><br/>
</aside>
</section>
<footer id="pie">
<br/><p>| <a href="index.html">INICIO</a> | <a href="repus/tienda.html">TIENDA</a> | <a href="trilla.html">TRILLA</a> | <a href="nosotros.html"> NOSOTROS </a> | <a href="contacto.html"> CONTACTO </a>| </p>
<br/><p>© 2016 Damarqchile. Todos los derechos reservados.</p><br/>
</footer>
</body>
</html>
