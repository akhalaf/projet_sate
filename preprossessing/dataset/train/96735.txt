<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Антикварные, букинистические и просто старые книги. Букинист. Поиск и продажа книг." name="description"/>
<meta content="книг, магазин, антиквар, антикварные, старые, редкие, букинист,щящтюкг, букинистическая, БСЭ, ЖЗЛ, литература, фантастика, подписные издания, научная, техническая, учебная, скупка" name="keywords"/>
<meta content="index,all" name="robots"/>
<link href="/style.css" media="screen" rel="stylesheet" title="Default" type="text/css"/>
<title>Alib.ru - букинистические книги. Продажа, покупка, поиск. Букинист. Антиквариат. Редкие издания.</title>
</head>
<body>
<a name="Begin"> </a>
<table width="100%">
<tr><td align="left" colspan="2" valign="top">
<h1>Alib.ru - букинистические книги.</h1>
<font color="#888888" face="Arial Cyr" size="1">Место встречи покупателей и продавцов любых книг.</font>
</td>
<td align="right" rowspan="2" valign="bottom" width="35%">
<small> «<b>Почта России</b>» запустила услугу доставки посылки на дом - за дополнительные 100 руб. почтальон доставит пакеты весом до 2 кг.<br/>
	Cроки хранения отправлений продлены до 60 дней. 
     </small>
</td>
</tr>
<tr><td align="left" valign="bottom" width="35%">
<!--  Антиквариум  -->
<hr/>
<a href="clicks.phtml?uid=75" rel="nofollow">
<img alt="Аукцион редких книг и графики." border="1" height="60" src="_bookshelf2.gif" width="468"/></a>
</td>
<td align="center" valign="bottom" width="30%">
</td>
</tr>
</table>
<hr/>
<table width="100%"><tbody>
<tr><td align="left" valign="top" width="40%">
<h2>Продажа книг</h2>
<h3><a href="kat.phtml">Все книги</a> (3977993) </h3>
</td><td align="center" valign="top" width="20%">
<!--  auctionica   -->
</td><td align="right" valign="top" width="40%">
<h2>Поиск книг</h2>
<form action="find3.php4">
<input name="tfind" size="56" type="text"/> <input type="submit" value="Найти!"/><br/><small>Пример: <a href="aboutfind.phtml">как найти</a>                              <a href="findp.php4">Расширенный поиск</a></small>
</form>
</td></tr>
</tbody></table>
<hr/>
<!--noindex-->
<table width="100%">
<tr><td width="33%">
<table>
<tr><td align="left" valign="top">
<b>Последние поступления за:</b>
<br/>
<ul>
<li><a href="razdeltema.php4?new=1" rel="nofollow">сегодня</a> (3068)</li>
<li><a href="razdeltema.php4?new=2" rel="nofollow"><b>2 дня</b></a> (5075)</li>
<li><a href="razdeltema.php4?new=3" rel="nofollow">3 дня</a> (6940)</li>
<li><a href="razdeltema.php4?new=7" rel="nofollow">неделю</a> (13855)</li>
<li><a href="razdelskid.php4" rel="nofollow">2 года назад</a></li>
</ul>
<br/>
</td></tr>
</table>
</td><td align="center" valign="MIDDLE" width="33%">
<table>
<tr><td align="left">
<ul>
<li><a href="akcii.phtml">Акции</a> </li>
<li><a href="zakazano.php4">Последние заказы 
	</a> (481) 23 ч.   </li>
<li><a href="razdeltemazakaz.php4?new=2" rel="nofollow">Заказано за 2 дня</a>
	(2611)   </li>
<li><a href="bbnew.phtml"><b>Как купить книги</b></a></li>
<li><a href="sellers.phtml">Как продать книги</a></li>
</ul>
</td></tr>
</table>
</td><td>
<!--/noindex-->
</td><td align="center" rowspan="2" valign="MIDDLE" width="33%">
<h3>Букинистические магазины:</h3>
<p><a href="bukmoscow.phtml">
   Москвы</a>,
   <a href="bukpiter.phtml">
   Петербурга</a><br/>
<a href="bukrussia.phtml">
   и других городов</a>...
   <br/><br/>
</p>
</td>
</tr>
<tr><td colspan="3">
</td></tr>
</table>
<table width="100%">
<tr>
<td align="left">
<a href="clicks.phtml?uid=55" rel="nofollow">
<img alt="Аукционный дом 12-й стул" border="0" height="120" src="_12stul2.gif" width="468"/></a>
</td>
<td align="center">
</td>
<td align="right">
<a href="clicks.phtml?uid=1" rel="nofollow">
<img alt="Аукцион В Никитском" border="0" height="120" src="_vnikit2.gif" width="468"/></a>
</td>
</tr>
</table>
<hr/>
<!--noindex-->
<table width="100%">
<tr><td align="center" valign="bottom" width="37%">
<h3><a href="bsbest.phtml">Все продавцы</a></h3>
<br/>
<br/>
<br/>
</td><td align="center" valign="top" width="37%">
<b>Новые продавцы:</b>
<table border="2" cellspacing="3"><tr bgcolor="#dddddd"><td> Продавец </td><td> Книг выставлено</td></tr>
<tr><td><a href="bs.php4?bs=yulrudolf">yulrudolf</a></td><td align="right">20</td></tr>
<tr><td><a href="bs.php4?bs=Kristallograf">Kristallograf</a></td><td align="right">43</td></tr>
<tr><td><a href="bs.php4?bs=Pskovbook">Pskovbook</a></td><td align="right">258</td></tr>
<tr><td><a href="bs.php4?bs=Baikalo4ka">Baikalo4ka</a></td><td align="right">239</td></tr>
<tr><td><a href="bs.php4?bs=Archeo">Archeo</a></td><td align="right">13</td></tr>
<tr><td><a href="bs.php4?bs=Melid">Melid</a></td><td align="right">43</td></tr>
<tr><td><a href="bs.php4?bs=SergeySvr">SergeySvr</a></td><td align="right">121</td></tr>
<tr><td><a href="bs.php4?bs=SergeyK">SergeyK</a></td><td align="right">77</td></tr>
</table>
</td><td align="center" bgcolor="#FFFFFF" valign="MIDDLE" width="26%">
<table><tr><td align="right">
<b>Ищу книгу! Готов заплатить:</b><br/>...<br/>
<small>
  Оллстон Аарон. Воины Адумара. Серия Звездные войны. 200&amp;nbspруб.
<br/>Д.Ролинг.Гарри Поттер и узник Азкабана Росмэн. сост.отл.только Москва 500&amp;nbspруб.
<br/>Ассман, Ян. Египет: теология и благочестие ранней цивилизации.  1000&amp;nbspруб.
<br/>...<br/> • <a href="classified.php4?new=1" rel="nofollow">сегодня</a> (9)  • <a href="classified.php4?new=2" rel="nofollow">2 дня</a> (38)  • <a href="classified.php4">Весь список</a> (38277)•</small> </td></tr></table>
<br/>
<!-- Баннер -->
</td></tr>
</table>
<hr/>
<!--/noindex-->
<table width="100%">
<tr>
<td align="left" valign="top" width="33%">
<script src="//vk.com/js/api/openapi.js?117" type="text/javascript"></script>
<!-- VK Widget -->
<div id="vk_groups"></div>
<script type="text/javascript">
 VK.Widgets.Group("vk_groups", {mode: 0, width: "234", height: "300", color1: 'FFFFEE', color2: '2B587A', color3: '5B7FA6'}, 30195556);
 </script>
</td>
<td align="left" valign="top" width="33%">
<h3><a href="forum.phtml">Форум</a> (36226+0 )  
  </h3>
</td>
<td align="center" valign="top" width="33%">
<table>
<tr>
<td>
<p><b>Количество книг по годам издания:</b><br/>
<!--noindex-->
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1830-40гг (1687)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1840-50гг (2204)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1850-60гг (3275)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1860-70гг (5175)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1870-80гг (7085)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1880-90гг (11135)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1890-1900гг (22595)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1900-10гг (52675)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1910-20гг (64444)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1920-30гг (61170)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1930-40гг (79205)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1940-50гг (60558)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1950-60гг (206000)<br/>
&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp•&amp;nbsp&amp;nbsp&amp;nbsp
1960-70гг (366362)<br/>
<!--/noindex-->
</p>
</td>
</tr></table>
</td>
</tr>
<tr>
<td align="left" valign="bottom">
</td>
<td>
</td>
<td>
</td>
</tr>
</table>
<hr/>
<table>
<tr><td align="left" valign="MIDDLE" width="67%">
<table>
<tr><td align="left" valign="MIDDLE">
<h3>Букинистические новости</h3>
<ul>
<li>Журнал «Знание — сила» навсегда попрощался со своим зданием и раздал москвичам коллекционную библиотеку </li>
<li>Тверские букинисты начали издавать собственную газету</li>
<li>Редкие библиографические ценности вновь будут представлены на торги Аукционного Дома «Книжная полка» </li>
<li>Независимый книжный магазин выпустил духи с запахом старинных фолиантов</li>
<li></li>
</ul>
<br/><br/>
<a href="news.phtml">Все новости подробно...</a><br/>
<br/>
<hr/>
<p>
<a href="osaite.phtml">O cайте</a> – • – 
  <a href="onas.phtml">O нac</a> – • – 
  <a href="banner.phtml" rel="nofollow">Наши баннеры</a>
</p>
<hr/>
<p>C 2003г заказано <b> 6712433 книг</b><br/> <a href="statistik.phtml" rel="nofollow">Другая cтатистика</a>
</p>
</td></tr></table>
</td><td>
</td><td>
<table>
<tr><td align="left" valign="MIDDLE" width="33%">
<a href="links.phtml">
<b>Букинистические страницы Сети:</b></a><br/><br/>
	• <a href="clicks.phtml?uid=8" target="_blank">"Антикварная Книга"</a> (учебник)<br/>
	• <a href="clicks.phtml?uid=9" target="_blank">Biblionne: Ценные старые книги</a><br/>
	• <a href="clicks.phtml?uid=13" target="_blank">Русская книга в Америке. Продажа редких, антикварных книг</a>. Много книг из библиотеки А.Смирдина<br/>
	• <a href="https://www.libourge.ru" target="_blank">Бутик-мастерская Libourge</a>. Коллекционные подарочные книги. Переплет фамильных библиотек.<br/>
<br/>
	• <a href="clicks.phtml?uid=71" target="_blank">Скупка книг</a><br/>
<br/>
	• <a href="links.phtml" rel="nofollow">Все сcылки...</a><br/>
</td>
</tr><tr><td>
<br/>
<a href="antikat.phtml">Каталог-прейскурант антикварных книг от 1961г.</a><br/>
<br/>
</td></tr><tr><td>
</td></tr>
</table>
</td></tr></table>
<hr/>
<p align="center"> <font face="Arial Cyr" size="1">
<a href="map.phtml">КАРТА сайта</a>
<b>·</b> <a href="law.phtml">Авторам и правообладателям</a>
<b>·</b> <a href="politconf.phtml">Политика конфиденциальности</a>
<b>·</b> <a href="clicks.phtml?uid=15">Указатель серий</a>
<b>·</b> <a href="clicks.phtml?uid=16">Alib в Українi</a>
<b>·</b> <a href="clicks.phtml?uid=17" rel="nofollow">Пластинки</a>
<b>·</b> <a href="clicks.phtml?uid=19" rel="nofollow">Марки</a>
<b>·</b> <a href="reklama.phtml">Реклама на сайте</a>
</font>
</p>
<hr/>
<table align="center" width="100%">
<tr><td align="center" valign="top" width="25%">
<!-- Put this div tag to the place, where the Like block will be -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "button"});
</script>
</td><td valign="top" width="30%">
</td><td valign="top" width="20%">
</td><td valign="top" width="15%">
</td></tr>
</table>
<hr/>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td align="center">
</td>
<td align="right">
</td>
</tr>
</table>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td align="center" valign="MIDDLE">
</td>
</tr>
</table>
<hr/>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td align="center" valign="MIDDLE">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t43.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border=0 width=31 height=31><\/a>")//--></script><!--/LiveInternet-->
<!-- Yandex.Metrika informer -->
<a href="//metrika.yandex.ru/stat/?id=134151&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" onclick="try{Ya.Metrika.informer({i:this,id:134151,type:0,lang:'ru'});return false}catch(e){}" src="//bs.yandex.ru/informer/134151/3_0_E0E0E0FF_C0C0C0FF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter134151 = new Ya.Metrika({id:134151, enableAll: true, webvisor:true});
        } catch(e) {}
    });
    
    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/134151" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</td>
</tr>
<tr>
<td align="center" valign="top">
<!--/noindex-->
<hr/>
<p>Alib.ru - крупнейший букинистический сайт России. 
Сайт позволяет эффективно продавать, покупать и разыскивать любые книги.<br/>
</p>
</td>
</tr>
</table>
</body>
</html>