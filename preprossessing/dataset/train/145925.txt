<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="yes" name="mobile-web-app-capable"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="#B30202" name="theme-color"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="#B30202" name="msapplication-TileColor"/>
<meta content="#B30202" name="msapplication-navbutton-color"/>
<link href="http://ararunaonline.com/css/estilo.css?1.1.4" rel="stylesheet"/>
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" rel="stylesheet"/>
<link href="http://ararunaonline.com/css/navmenu-reveal.css?1.1.4" rel="stylesheet"/>
<link href="http://ararunaonline.com/sitemap.xml" rel="sitemap" title="Sitemap" type="application/xml"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!-- [if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif] -->
<title>Araruna Online </title>
<meta content="Araruna Online " property="og:title"/>
<meta content="Araruna Online " property="og:description"/>
<meta content="http://ararunaonline.com/imagens/marcasite.png" property="og:image"/>
<meta content="website" property="og:type"/>
<meta content="http://www.ararunaonline.com" property="og:url"/>
<meta content="pt_BR" property="og:locale"/>
<meta content="1" property="fb:app_id"/>
<meta content="https://www.facebook.com/https://pt-br.facebook.com/portalararunaonline/" property="article:publisher"/>
<meta content="summary_large_image" property="twitter:card"/>
<meta content="Araruna Online  @https://twitter.com/ararunaonline" property="twitter:site"/>
<meta content="Araruna Online " property="twitter:title"/>
<meta content="Araruna Online " property="twitter:description"/>
<meta content="http://ararunaonline.com/imagens/marcasite.png" property="twitter:image"/>
<meta content="http://www.ararunaonline.com" property="twitter:url"/>
<meta content="ararunaonline.com" itemprop="name"/>
<meta content="http://www.ararunaonline.com" itemprop="url"/>
<meta content="http://ararunaonline.com/imagens/marcasite.png" itemprop="image"/>
<meta content="Últimas notícias de Araruna e todo o Estado da Paraíba. " itemprop="description"/>
<meta content="ararunaonline.com" name="application-name"/>
<meta content="noodp" name="googlebot"/>
<meta content="index, follow" name="robots"/>
<meta content="Últimas notícias de Araruna e todo o Estado da Paraíba. " name="description"/>
<meta content="portal de notícias da paraíba, mais pb, polemica paraiba, pbagora, maispb, berg lima, fabiano gomes, paraiba ja, noticias paraiba, noticias da paraiba, noticias araruna, noticias de araruna, vital costa, Google, pb, nordeste, , pbmais, paraiba online, agorapb, política, Brasil, mundo, webtv, pedra da boca, passa e fica, noticias do rio grande do norte, noticias do flamengo, noticias do corinthians, credibilidade, melhor, maior, João Pessoa, fortaleza, ceará, campina grande, Araruna, prefeitura de araruna, prefeito vital costa, vital costa, iran motos, araruna moto fest, são joão na serra, são joão em araruna, natal, rio grande do norte, rn, recife, pernambuco, pe, portal, pedra da boca, eventos, mural, recados, festas, eventos, mulher, região, brejo, curimataú, sertão, grátis, morte, jovem, festividades, paraíba, são paulo, mundo, rio de janeiro, internacional, vendas, publicidade, conexão, o, melhor, mundo, sitesaj, noticias, new, grátis, fgts, mega-sena, ganhei, leão, inss, gta, policial, bbb, briga, juventude, politica, eleicoes, brasil, 2012, 2013, 2014, 2015, 2016, 2017, 2018, tv, televisao, fama, famoso, fofoca, novelas, noticias, últimas, conexão, noticias, fique, ligado, portal, noticias, noticia, news, new, google, Yahoo, Bing, meta, orkut, Facebook, Twitter, Instagram, sexo, festa, noticias, seriedade, verdade, política, prefeita, vereador, senador, governador, presidente, ministro, geral, fofoca, agora, dona inês, festa, not, noticiário, verdade, serio, 1,2,3, verdade, morte, jovem, festa, brasil, guarabira, são joão em araruna, araruna moto fest, China, mg, rs, santa catarina, sc, brasil, olinda, conexão, noticias, Brasil, mentira, cia, policial, carros, seca, esporte, jornais, revistas, biblioteca, classificados, compras, computador, corpo, saúde, moda, carros, cinema, crianças, diversão, arte, economia, educação, internet, jogos, novelas, rádio, tv, tempo, mapas, trânsito, últimas notícias, viagem, jornalismo, informação, notícia, cultura, entretenimento, lazer, opinião, análise, internet, televisão, fotografia, imagem, som, áudio, vídeo, fotos, tecnologia, gay, vestibular, empregos, humor, música, rádio serrana de araruna, talismã fm de belém, jornais da paraíba, cacimba de dentro, tacima, picuí, cuité, damião, barra de santa rosa, passa e fica, serra de são bento, nova cruz, santa cruz, turismo" name="keywords"/>
<link href="http://www.ararunaonline.com" rel="canonical"/>
<meta content="SFnbfninnVgwGV9P33Eg-Ffghqhinv_vwtIX-IbYF2E" name="google-site-verification"/>
<meta content="" name="alexaVerifyID"/>
<meta content="Araruna Online " name="author"/>
<meta content="no-cache, no-store" http-equiv="Cache-Control"/>
<meta content="no-cache, no-store" http-equiv="Pragma"/>
<meta content="Mon, 06 Jan 1990 00:00:01 GMT" http-equiv="expires"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="http://ararunaonline.com/imagens/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="http://ararunaonline.com/imagens/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="http://ararunaonline.com/imagens/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="http://ararunaonline.com/imagens/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="http://ararunaonline.com/imagens/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="http://ararunaonline.com/imagens/favicon/manifest.json" rel="manifest"/>
<meta content="http://ararunaonline.com/imagens/favicon/ms-icon-144x144.png" name="msapplication-TileImage"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-34722011-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34722011-1');
</script>
<script async="" data-ad-client="ca-pub-1275688678175984" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- advertising EG9sfARVeUvkHdfOdlD7XEgagGlF8Oqn0HzMEurQnsI42BCzJs1b13jdsm5asHRFcDIKPXsWcOKmftT66osesw==-->
<script data-cfasync="false" id="clevernt" type="text/javascript">
							 (function (document, window) {
                var c = document.createElement("script");
                c.type = "text/javascript"; c.async = !0; c.id = "CleverNTLoader44156";  c.setAttribute("data-target",window.name); c.setAttribute("data-callback","put-your-callback-macro-here");
                c.src = "//clevernt.com/scripts/1caeb424132994c52ade1453099d01db.min.js?20200608=" + Math.floor((new Date).getTime());
                var a = !1;
                try {
                    a = parent.document.getElementsByTagName("script")[0] || document.getElementsByTagName("script")[0];
                } catch (e) {
                    a = !1;
                }
                a || ( a = document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]);
                a.parentNode.insertBefore(c, a);
            })(document, window);
                                </script>
<!-- end advertising --> </head>
<body>
<div id="mobileblack"></div>
<div id="main">
<nav>
<div class="navigation">
<div class="container">
<a href="http://ararunaonline.com/" title="Araruna Online "><div class="logomarca disab"></div></a>
<ul>
<li><a href="http://ararunaonline.com/noticias/"><i aria-hidden="true" class="fa fa-newspaper-o"></i> Notícias <b class="caret desibset"></b></a>
<ul class="dropdown">
<div class="col-mx-5 lesfnave">
<li><a href="http://www.ararunaonline.com/noticias/araruna/">Araruna</a></li>
<li><a href="http://www.ararunaonline.com/noticias/brasil/">Brasil</a></li>
<li><a href="http://www.ararunaonline.com/noticias/cidades/">Cidades</a></li>
<li><a href="http://www.ararunaonline.com/noticias/concursos/">Concursos</a></li>
<li><a href="http://www.ararunaonline.com/noticias/economia/">Economia</a></li>
<li><a href="http://www.ararunaonline.com/noticias/educacao/">Educação</a></li>
<li><a href="http://www.ararunaonline.com/noticias/entretenimento/">Entretenimento</a></li>
<li><a href="http://www.ararunaonline.com/noticias/esporte/">Esporte</a></li>
<li><a href="http://www.ararunaonline.com/noticias/geral/">Geral</a></li>
<li><a href="http://www.ararunaonline.com/noticias/mundo/">Mundo</a></li>
<li><a href="http://www.ararunaonline.com/noticias/opiniao/">Opinião</a></li>
<li><a href="http://www.ararunaonline.com/noticias/paraiba/">Paraíba </a></li>
<li><a href="http://www.ararunaonline.com/noticias/policial/">Policial</a></li>
<li><a href="http://www.ararunaonline.com/noticias/politica/">Política</a></li>
<li><a href="http://www.ararunaonline.com/noticias/religiao/">Religião</a></li>
<li><a href="http://www.ararunaonline.com/noticias/saude/">Saúde</a></li>
<li><a href="http://www.ararunaonline.com/noticias/tecnologia/">Tecnologia</a></li>
<li><a href="http://www.ararunaonline.com/noticias/turismo-araruna/">Turismo Araruna</a></li>
</div> </ul> </li>
<li><a href="http://ararunaonline.com/videos/"><i aria-hidden="true" class="fa fa-video-camera"></i> Vídeos</a></li>
<li><a href="http://ararunaonline.com/hospedagem/"><i aria-hidden="true" class="fa fa-bed"></i> <span class="dis"> Hospedagem</span></a></li>
<li><a href="http://ararunaonline.com/galerias/"><i aria-hidden="true" class="fa fa-camera"></i> <span class="dis">Galerias</span></a></li>
<li><a href="http://ararunaonline.com/contato/" title="Contato"><i aria-hidden="true" class="fa fa-commenting-o"></i> <span class="dis "> Contato</span></a></li>
<span class="lupa ">
<li><a class="addClass disab" href="#"><i aria-hidden="true" class="fa fa-search"></i></a></li>
</span>
</ul>
</div>
</div>
<div class="nav_bg">
<div class="nav_bar"> <a href="#" id="nav-toggle"><span></span></a></div>
<a href="http://ararunaonline.com/"><div class="logomarca"></div></a>
<span class="lupa1">
<a class="addClass" href="#"><i aria-hidden="true" class="fa fa-search"></i></a>
</span>
</div>
</nav>
<div class="sincronia">
<div class="propaganda">
<div class="container">
<div class="banner"><center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-1275688678175984" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="4031901864" style="display:block; text-align:center;"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script> </center></div>
</div>
</div>
<div class="container">
<div class="col-md-6 col-sm-12 ">
<div class="post post-spacer">
<div class="entry-header signle_iteamd">
<a href="http://ararunaonline.com/noticias/araruna/araruna-recebe-projeto-nossa-energia.html"><img alt="Araruna recebe projeto Nossa Energia" class="img-responsive imgs" src="http://ararunaonline.com/static/noticias/araruna-recebe-projeto-nossa-energia1610406544.png" titulo="Araruna recebe projeto Nossa Energia"/></a>
<div class="post-content">
<div class="entry-title">
<span class="label catnews">Araruna</span>
<span class="title_news"><a class="fontnews" href="http://ararunaonline.com/noticias/araruna/araruna-recebe-projeto-nossa-energia.html">Araruna recebe projeto Nossa Energia</a></span>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-sm-12 mar30-top">
<div class="row">
<div class="col-md-12 col-sm-12 ">
<a class="fonteg1" href="http://ararunaonline.com/noticias/paraiba/paraiba-confirma-523-novos-casos-de-covid-19-e-16-obitos-nesta-segunda-total-de-mortos-chega-a-3824-e-173299-infectados.html">
<span class="label catnews">Pandemia</span>
<div class="line">Paraíba confirma 523 novos casos de Covid-19 e 16 óbitos nesta segunda; total de mortos chega a 3.824​ e 173.299​ infectados </div>
</a>
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6 mar15-bot">
<div class="post post-spacer">
<div class="entry-header signle_iteamd">
<a href="http://ararunaonline.com/noticias/araruna/araruna-divulga-novo-boletim-epidemiologico-da-covid-19-confira.html"><img alt="Araruna divulga novo boletim epidemiológico da COVID-19; confira" class="img-responsive imga" src="http://ararunaonline.com/static/noticias/araruna-divulga-novo-boletim-epidemiologico-da-covid-19-confira1610496536.png" title="Araruna divulga novo boletim epidemiológico da COVID-19; confira"/></a>
<div class="post-content">
<div class="entry-title">
<span class="label catnews">Pandemia</span>
<div class="title_news"><a class="fontnews_2" href="http://ararunaonline.com/noticias/araruna/araruna-divulga-novo-boletim-epidemiologico-da-covid-19-confira.html">Araruna divulga novo boletim epidemiológico da COVID-19; confira </a></div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-sm-6 mar10-bot">
<div class="post post-spacer">
<div class="entry-header signle_iteamd">
<a href="http://ararunaonline.com/noticias/politica/morre-deputado-estadual-paraibano-joao-henrique-vitima-da-covid-19.html"><img alt="Morre deputado estadual paraibano João Henrique, vítima da Covid-19" class="img-responsive imga" src="http://ararunaonline.com/static/noticias/morre-deputado-estadual-paraibano-joao-henrique-vitima-da-covid-191610478033.jpg" title="Morre deputado estadual paraibano João Henrique, vítima da Covid-19"/></a>
<div class="post-content">
<div class="entry-title">
<span class="label catnews">Luto</span>
<div class="title_news"><a class="fontnews_2" href="http://ararunaonline.com/noticias/politica/morre-deputado-estadual-paraibano-joao-henrique-vitima-da-covid-19.html">Morre deputado estadual paraibano João Henrique, vítima da Covid-19 </a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="propaganda">
<div class="container">
<div class="row">
<div class="col-sm-6 bannes2">
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Rodape -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1275688678175984" data-ad-format="auto" data-ad-slot="2839586197" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> </center>
</div>
<div class="col-sm-6 bannes2">
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Rodape -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1275688678175984" data-ad-format="auto" data-ad-slot="2839586197" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> </center>
</div>
</div>
</div>
</div>
<div class="container">
<div class="col-md-8 col-sm-7 mar15-top ">
<div class="row">
<div class="col-md-12 col-sm-12 ">
<a class="fonteg" href="http://ararunaonline.com/noticias/esporte/pelo-brasileirao-ceara-vence-no-maraca-e-aumenta-pressao-sobre-o-flamengo.html">
<span class="label catnews">Futebol</span>
<div class="line">Pelo Brasileirão Ceará vence no Maraca e aumenta pressão sobre o Flamengo </div>
</a>
</div>
</div>
<div class="row">
<div class="col-md-6 col-sm-6">
<a class="fontem" href="http://ararunaonline.com/noticias/cidades/prefeitura-de-barra-de-santa-rosa-fecha-escolas-e-recomenda-suspensao-de-academias.html">
<img class="img-responsive img-rounded imgdestm2" src="http://ararunaonline.com/static/noticias/prefeitura-de-barra-de-santa-rosa-fecha-escolas-e-recomenda-suspensao-de-academias1609980569.jpg" width="100%"/>
<span class="label catnewsp">Pandemia</span>
<div class="line">Prefeitura de Barra de Santa Rosa fecha escolas e recomenda suspensão de academias </div>
</a>
</div>
<div class="col-md-6 col-sm-6">
<a class="fontem" href="http://ararunaonline.com/noticias/cidades/bairros-com-o-metro-quadrado-mais-barato-de-sp.html">
<img class="img-responsive img-rounded imgdestm2" src="http://ararunaonline.com/static/noticias/bairros-com-o-metro-quadrado-mais-barato-de-sp1609862300.jpg" width="100%"/>
<span class="label catnewsp">Cidades</span>
<div class="line">Bairros com o metro quadrado mais barato de SP </div>
</a>
</div>
</div>
</div>
<div class="col-md-4 col-sm-4 ">
<div class="fbpage img-responsive">
<div class="fb-page" data-adapt-container-width="false" data-hide-cover="false" data-href="https://www.facebook.com/portalararunaonline/" data-show-facepile="true" data-small-header="false"><blockquote cite="https://www.facebook.com/portalararunaonline/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/portalararunaonline/">Portal Araruna Online</a></blockquote></div>
</div>
<div class="fbpage">
<a href="http://ararunaonline.com/widgets"> <img class="img-responsive" src="http://ararunaonline.com/imagens/widgets.png"/> </a>
</div>
</div>
</div>
</div>
<div class="espacobranco1">
<div class="container"> </div>
</div>
<div class="propaganda">
<div class="container">
<div class="banner">
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Rodape -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1275688678175984" data-ad-format="auto" data-ad-slot="2839586197" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> </center>
</div>
</div>
</div>
<div class="cor">
<div class="container">
<div class="col-md-4 col-sm-4">
<p class="fonteg3">VÍDEOS</p>
<ul class="list-unstyled video-list-thumbs row">
<li class="col-lg-12">
<a href="http://ararunaonline.com/videos/nde-tudo-acontece-vereadores-eleitos-odon-macedo-junior-costa.html" title="NDE TUDO ACONTECE | VEREADORES ELEITOS | ODON MACÊDO | JÚNIOR COSTA">
<span class="fa fa-play-circle-o"></span>
<img alt="Barca" class="imgsd" height="130px" src="https://i.ytimg.com/vi/kVGarPNt0ew/0.jpg"/>
<div class="line2 videotitul">NDE TUDO ACONTECE | VEREADORES ELEITOS | ODON MACÊDO | JÚNIOR COSTA</div>
</a>
</li>
<li class="col-lg-12 ">
<div class="line2 fontem01"><a class="conexao" href="http://ararunaonline.com/videos/araruna-webtv-entrevista-com-edvaldo-costa-secretario-de-turismo-de-araruna.html">Araruna WebTV: Entrevista com Edvaldo Costa - Secretário de Turismo de Araruna</a></div>
</li>
</ul>
</div>
<div class="col-md-4 col-sm-4 cor2 quadrado">
<p class="fonteg3">FOTOS</p>
<div class="signle_iteam slick-slide slick-cloned slick-active" index="-1">
<img alt="website template image" height="400px" src="http://ararunaonline.com/static/galerias/posse-do-prefeito-vital-costa-e-vice-availdo-azevedo1609596846.jpg"/>
<div class="sing_commentbox slider_comntbox">
<p><i class="fa fa-calendar"></i> 02/01/2021</p>
<a href="http://ararunaonline.com/galerias/posse-do-prefeito-vital-costa-e-vice-availdo-azevedo.html"><i aria-hidden="true" class="fa fa-camera"></i> FOTOS</a>
</div>
<a class="slider_tittle" href="http://ararunaonline.com/galerias/posse-do-prefeito-vital-costa-e-vice-availdo-azevedo.html">Posse do Prefeito Vital Costa e vice Availdo Azevedo</a>
</div>
<div class="signle_iteam slick-slide slick-cloned slick-active" index="-1">
<img alt="website template image" height="400px" src="http://ararunaonline.com/static/galerias/a-volta-da-feira-livre-de-araruna1598107310.jpg"/>
<div class="sing_commentbox slider_comntbox">
<p><i class="fa fa-calendar"></i> 22/08/2020</p>
<a href="http://ararunaonline.com/galerias/a-volta-da-feira-livre-de-araruna.html"><i aria-hidden="true" class="fa fa-camera"></i> FOTOS</a>
</div>
<a class="slider_tittle" href="http://ararunaonline.com/galerias/a-volta-da-feira-livre-de-araruna.html">A volta da Feira Livre de Araruna</a>
</div>
</div>
<div class="col-md-4 col-sm-4">
<p class="fonteg3">TOP</p>
<span class="label catnewsp">Música Gospel</span>
<div class="line"><a class="conexao" href="http://ararunaonline.com/noticias/entretenimento/louvores-e-adoracao-2020-2021-as-melhores-musicas-gospel-top-hinos-gospel.html">Louvores e Adoração 2020/2021 - As Melhores Músicas Gospel - top hinos gospel </a></div>
<span class="label catnewsp">Pandemia</span>
<div class="line"><a class="conexao" href="http://ararunaonline.com/noticias/araruna/araruna-registra-01-novo-caso-de-covid-19-e-confirma-mais-um-obito.html">Araruna registra 01 novo caso de Covid-19 e confirma mais um óbito </a></div>
<span class="label catnewsp">Pandemia</span>
<div class="line"><a class="conexao" href="http://ararunaonline.com/noticias/araruna/confira-o-novo-boletim-epidemiologico-da-covid-19-no-municipio-de-araruna.html">Confira o novo boletim epidemiológico da COVID-19 no Município de Araruna </a></div>
<span class="label catnewsp">Pandemia</span>
<div class="line"><a class="conexao" href="http://ararunaonline.com/noticias/araruna/araruna-divulga-novo-boletim-epidemiologico-da-covid-19-confira.html">Araruna divulga novo boletim epidemiológico da COVID-19; confira </a></div>
</div>
</div>
</div>
<div class="propaganda">
<div class="container">
<div class="banner">
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-1275688678175984" data-ad-format="autorelaxed" data-ad-slot="9466184197" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script> </center>
</div>
</div>
</div>
<div class="final">
<div class="container">
<div class="col-md-4 col-sm-4">
<a class="fontem" href="http://ararunaonline.com/noticias/araruna/araruna-nao-registra-novos-casos-da-covid-19-e-se-mantem-com-473-casos.html">
<img class="img-responsive img-rounded imgdestm21" src="http://ararunaonline.com/static/noticias/araruna-nao-registra-novos-casos-da-covid-19-e-se-mantem-com-473-casos1610405327.png" width="100%"/>
<span class="label catnewsp">Pandemia</span>
<div class="line">Araruna não registra novos casos da COVID-19 e se mantém com 473 casos</div>
</a>
</div>
<div class="col-md-8 col-sm-8 embaixo">
<a class="fonteg" href="http://ararunaonline.com/noticias/paraiba/pelo-menos-tres-agencias-do-banco-do-brasil-devem-fechar-na-paraiba.html">
<span class="label catnews">Reestruturação</span>
<div class="line">Pelo menos três agências do Banco do Brasil devem fechar na Paraíba </div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a class="fontem" href="http://ararunaonline.com/noticias/paraiba/joao-azevedo-anuncia-abertura-das-pre-matriculas-digitais-para-alunos-da-rede-estadual-de-ensino.html">
<span class="label catnewsp">Paraíba </span>
<div class="line">João Azevêdo anuncia abertura das pré-matrículas digitais para alunos da Rede Estadual de Ensino </div>
</a>
</div>
<div class="col-md-4 col-sm-4">
<a class="fontem" href="http://ararunaonline.com/noticias/paraiba/paraiba-confirma-515-novos-casos-de-covid-19-e-08-obitos-neste-domingo.html">
<span class="label catnewsp">Pandemia</span>
<div class="line">Paraíba confirma 515 novos casos de Covid-19 e 08 óbitos neste domingo </div>
</a>
</div>
</div>
</div>
<div class="prerodape">
<div class="container">
<div class="col-md-3 col-sm-3">
<div>
<ul class="margeroda"><a href="http://www.ararunaonline.com/noticias/" id="fonteg10">NOTÍCIAS</a></ul>
</div>
<div class="fontem012">
<ul class="list">
<li><a href="http://www.ararunaonline.com/noticias/araruna/">Araruna</a></li>
<li><a href="http://www.ararunaonline.com/noticias/brasil/">Brasil</a></li>
<li><a href="http://www.ararunaonline.com/noticias/cidades/">Cidades</a></li>
<li><a href="http://www.ararunaonline.com/noticias/concursos/">Concursos</a></li>
<li><a href="http://www.ararunaonline.com/noticias/economia/">Economia</a></li>
<li><a href="http://www.ararunaonline.com/noticias/educacao/">Educação</a></li>
<li><a href="http://www.ararunaonline.com/noticias/entretenimento/">Entretenimento</a></li>
<li><a href="http://www.ararunaonline.com/noticias/esporte/">Esporte</a></li>
<li><a href="http://www.ararunaonline.com/noticias/geral/">Geral</a></li>
</ul>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div>
<ul class="margeroda"><a href="#" id="fonteg10">ARARUNA ONLINE</a></ul>
</div>
<div class="fontem012">
<ul class="list">
<li><a href="http://www.ararunaonline.com">Home</a></li>
<li><a href="http://www.ararunaonline.com/noticias/">Notícias</a></li>
<li><a href="http://www.ararunaonline.com/videos/">Vídeos</a></li>
<li><a href="http://www.ararunaonline.com/hospedagem/">Hospedagem</a></li>
<li><a href="http://www.ararunaonline.com/galerias/">Fotos</a></li>
<li><a href="http://www.ararunaonline.com/enquetes/">Enquetes</a></li>
<li><a href="http://www.ararunaonline.com/contato/">Contato</a></li>
<li><a href="http://www.ararunaonline.com/rss/">Rss</a></li>
<li><a href="http://www.ararunaonline.com/politica-de-privacidade/">Política de Privacidade</a></li>
<li><a href="http://www.ararunaonline.com/mobile/">Acesso mobile</a></li>
<li><a href="http://www.ararunaonline.com/widgets/">Widgets</a></li>
</ul>
</div>
</div>
<div class="col-md-5 col-sm-5">
<div class="col-md-12 marpers-top CSD">
<div class="fonteg3">NA REDE </div>
<div class="face"><a href="https://pt-br.facebook.com/portalararunaonline/" target="_blank"><i aria-hidden="true" class="fa fa-facebook-square fa-3x"></i></a></div>
<div class="inst"><a href="#" target="_blank"> <i aria-hidden="true" class="fa fa-instagram fa-3x"></i> </a></div>
<div class="tuit"><a href="https://twitter.com/ararunaonline" target="_blank"><i aria-hidden="true" class="fa fa-tumblr-square fa-3x"></i></a></div>
<div class="goog"><a href="https://www.youtube.com/ararunawebtv" target="_blank"><i aria-hidden="true" class="fa fa-youtube-square fa-3x"></i></a></div>
<div class="goog"><a href="https://br.linkedin.com/company/portal-araruna-online?trk=extra_biz_viewers_viewed" target="_blank"><i aria-hidden="true" class="fa fa-linkedin-square fa-3x"></i></a></div>
</div>
<div class="col-md-12 mar10-top fonteroda">
<div class="fonteg3"> Contato </div>
              Entre em contato com a Redação
              ararunapb@gmail.com
            </div>
</div>
</div>
</div>
<footer id="rodape">
<div class="container"> <div class="col-md-8 col-sm-12"><p>&amp;copyCopyright 2007-2017 Todos os direitos reservados</p> ArarunaOnline.com </div>
<div class="col-md-4 col-sm-4 fontfooter">
<a href="http://www.agenciasincronia.com.br" target="_blank" title="AGÊNCIA SINCRONIA - COMUNICAÇÃO DIGITAL"><div class="sincroniadigital">Desenvolvido por </div></a>
</div>
</div>
</footer>
<div class="off" id="qnimate">
<div class="open" id="search">
<button class="close" data-widget="remove" id="removeClass" type="button">×</button>
<form action="http://ararunaonline.com/resultado" autocomplete="off" method="">
<input id="term" name="search" placeholder="Buscar No Araruna Online" type="text" value=""/>
<button class="btn btn-lg btn-site" type="submit"><i aria-hidden="true" class="fa fa-search"></i> Buscar </button>
</form>
</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script>
$(document).ready(function(){
	  	$('.nav_bar').click(function(){
			$('.navigation').toggleClass('visible');
			$('#mobileblack').toggleClass('opacity');
        $('#nav-toggle').toggleClass('active');

      $('body').toggleClass('scoll');
      });

      $('#mobileblack').click(function(){
      $('.navigation').toggleClass('visible',false);
      $('#mobileblack').toggleClass('opacity',false);
      $('body').toggleClass('scoll',false);
      $('#button').toggleClass('close');
     $('#nav-toggle').toggleClass('active');

    });

    $(".addClass").click(function () {
    $('#qnimate').addClass('popup-box-on');
    });

    $("#removeClass").click(function () {
    $('#qnimate').removeClass('popup-box-on');
    });

	});

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="http://ararunaonline.com/js/bootstrap.js"></script>
</div></body>
</html>
