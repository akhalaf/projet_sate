<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "30931",
      cRay: "610b0407fdf67399",
      cHash: "cd040b0a176af45",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuMmNoLnBtLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "SFYR4uoPjJFuc1YWJ6WZBwX9F8+fFOExalB6abQlqRDBlB9MgDECMjESmUlgOKlEpHilHtN+VSptauIZvkpwdd/1JuO3Kk2lhyViXq602o6J/dYCQkicmQKZUKgH4RHMgJwQSH1pqzBzgZxLdXXAFO2kDEKg365ZKPnf+sAuJm28qLvWABFAXECBdxMLMq3ixrbPuY5GIavs5r+ITdy9JyDFvKlOkCtSezNARLLa41P9eeG14RKIHcc1WyuOpSLQAd/ajErxIej0xkAZawbdDPJvh90soCldyiWFiVJ8TIqlJgDqEWAQFsNjbf35+xmAUbvUj6cghMpCHtYV+Hd725uE5fREEOtNzlbOKCv62BqEM2DZ9YS3KVd08XrJHYcmKNxVB8g1UgQlz79RRscu1+xkU+PqzuF3h7WeCDdXFJlSFfqoiyMomlqiaLgncEPvrrRknOOh3a21szyRGlRCIwn97iSLupehAatNMK9WM0daveaTJ01g0CR9vctaQjKSFeegJbZ84aKG2mcXiT/040cFzCrLDWM3QwK01p2IfCPDgXrYgQywRsn+/MLFmreIB7UL5Z7HEJsa4PrdLWZUKCpII2PxBCNxybR5WzpruNwQdQQ7sH8duh9AbjJVrPhgGFp0QnFtZYPgcKB6CXsFbS+o14SBdI8ajg1yNaZBPCC/zVPsZJ/ByqjChtZhlkbZBm2di416R2tVkSaJ3/Py/ztM1jdROt5rmGoQyjfsHWGzruP3r8UhZFyO9RD2M9oe",
        t: "MTYxMDQ5NzkwMC44MDgwMDA=",
        m: "cXzCAVCbMPxnoMOkfT2Eyqr3vqg965jZ3bMG2+vwYHs=",
        i1: "oN3OyfYPwNOtEewroN8T0Q==",
        i2: "MYTGrVXksWk1htgtrt+QRQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "P61CYISXoMbqjg2tpTb8wPwDFbXqNLT5ykITjlJcIpA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.2ch.pm</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=b30c76cf113eac4bff315662f92c0044da452cc7-1610497900-0-ARj_cVTpcHwHTptwZK0pjfYG2coxZD7BCljhaEW8mr_wETIoLvNw8MIrhr4K6wv5wRcNbSBbgs6VpOcrvpKht_5HP1jQrQP4lutAqUBJ07S9USTt7qMfRs_UEUy16WXGOgJeMjx0ZiVMWAibE5xgUpof94ZlzH3-RkkEn3NjmYFh4b3jn2MVtxRxs5XUdxieYQ9iIM1xKsfjiQDsA-CnGHPcedfoasSj7aUXMWoEiFnO2Al7iomXuAPcvU3btMN6xk2DetIIy8_9Zwc5Sy1V1BkUQrxmczvZRGHdsu7jrcbfR-crceAaoXdYgWGhHC6gR5qPAV1Da6ePPffik5w_ur_2_JHa7D55R8FG3tBNZm4zdc2Nnb8IUIG7Z9r7mUPYCzHPg5Q61samVYzgm-YTVmKPCqlOTlhNpd9o-GN2tsd_NkFtYIXbzcycVQeUZ6fvFAdXBwYjuDCJUZALb8OTI3RIK8O_bD9FSoQaYhOqOUvbrkKW0kVrIZR8C_ynvd3ffF2k_97IZT55sbOflHOPrns" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="f36d5ca9cba206921231c23548ab3a79edb581fc-1610497900-0-AW1B8E1jkYwUpDF/+G6VDxKnWKssYIIfb3JORwi223LCVKE7lf4ZIGD7aDTGdisBdmoupok6c3eX5gHrLDqh+ACHmmBGqr6OAXNFLnuu2bBJad6p/NgEChHfsVcMbn9I1bLxW9sjhrZwVL6v9cHWCd7eC7+/mbGth1ntdcryZfZW83GLjY86TOgzYCWIS/pGrox4LvPWlUx4OfzWaWsDK8ALuR6VE5np6njs0QXXsDRV3XF+9xP11xyTbjDb1ima4oY275R0KE1hPY3Bm5zI/TjVZ2kN7GiwZY5xC7I5kX4wdTFb7qmikNNYJ0RdMxTNsW904zsMyr5yfX42NdJx1+1yKu/77BXkpmUGvZ5vPTepOdmLs4NTAWtWQnDgJ7CvyEE4fc4pjTtoyrSJ3tc1wScS4jLVtNIvtDTaLac6N3xA82OVqFuAUmFLWWaxj+Iz5UhJ5oytnBsV5kNM2bVzTM1i6jqLt8M3R4iMIf1qyqpmjgD++FfEXmSZvpZNOE/g9c1Kdrbo27SzOesGZFFfSiyDZagqVb3eKx1fYZYgZkEMV1JQyPu5CH6mM8cNyAXikxCrmpteJ5jlVXQ/La7H7ZFKlEDUMuwjA2ta8+fe7Qgwj18iYTh1ZQf4Dq0AkIszGjEylxOJVGouAeVC1y1HfXVaoF1do1egi5pevfu3jsMmpEz2RJM8fTMfRCCdlAYx/oGCHagj6SLXTIFYymr4vD7EaxpLoOuFZns+aJz6kzI2wgZ8eBMjCImjqVK287i1H81qIaWYdbICrcP2VKjEcvV1R32DZDprBPgK8OteD78gwXDWbuYPDO0yFmWMSMRmQqDdOZTbdKttmv8yf54qhlVS0B81E9UwoJPnWtJ1ak4YMr5r0kOM7XW6Ca7ZrpwQlHuRJSfqxDmMgPUV5Grk28WoyFuu61pm60vMwBup7nLrGcB824ldE9KQoGMrMqs3WVfQdYaQ1XQapLkd2SJVUTuFJVow+CekR65ZCby2Dpjgi7Q32dBkFlfgL3iLIvz1uxJQUlPWfLuttGzLlERFqIfWyq8DJP7m2+2/Tc0avarU3/v89TTJ/Lu9BiVSro5RDN3YMGq4lHktPpvsLFgIgqUspHQj7OEWFFz3aSW5hW2jAcNL3gVQeriLoEo4eaH+xfMfNdex48vPk9EVjpHf0OxMjJjxPo7pLj93pD8tdQ4MRylFwoe+CM244+3DVYkUunDBJD0BKN6Cudj+HJdHDLtHWl+4ReZJhj2m5qhpgdLd6LP3ay9W8yhxzrjn9jLQIuH+FQa9zfg/ZQ090puvSXgEvG5M3V8lzQ0h4eOjeVQm"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="0751fca4b5d83f5f62af600a795faed7"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610b0407fdf67399')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610b0407fdf67399</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
