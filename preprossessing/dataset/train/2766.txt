<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html class="not-ie" lang="en">
<!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="101HR,人力资源,人力资源管理,人力资源系统,人力资源服务" name="keywords"/>
<meta content="101HR是51社保网的人力资源管理系统,旨在为企业提供高效的人力资源管理服务.101HR可帮助HR实现一键发offer,社保公积金管理,员工福利管理等功能, 101HR力求成为专业的人力资源服务平台." name="description"/>
<meta content="True" name="MSSmartTagsPreventParsing"/>
<meta content="本页版权归51社保网所有" name="Copyright"/>
<link href="//www.101hr.com/2020120301/static/images/favicons/favicon.ico" rel="shortcut icon"/>
<!-- base.css 基本样式-->
<link href="//www.101hr.com/2020120301/static/css/themes/base.css" rel="stylesheet" type="text/css"/>
<!-- module.css 模块-->
<link href="//www.101hr.com/2020120301/static/css/home/module.css?v=20170101" rel="stylesheet" type="text/css"/>
<!--[if lt IE 7]>
		<link type="text/css" rel="stylesheet" href="//www.101hr.com/2020120301/static/css/ie6.css" />
		<![endif]-->
<title>【101HR】－企业社保服务_人力资源管理平台</title>
<script src="//www.101hr.com/2020120301/static/lib/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="//www.101hr.com/2020120301/static/lib/layer/innerlayer.min.js" type="text/javascript"></script>
<link href="//www.101hr.com/2020120301/static/scripts/libs/validator/0.7.3/jquery.validator.css" media="screen" rel="stylesheet"/>
<link href="//www.101hr.com/2020120301/static/css/jquery-ui.css" rel="stylesheet"/>
<link href="//www.101hr.com/2020120301/static/lib/jquery/autocomplete/autocomplete.min.css" rel="stylesheet"/>
<link href="//apps.bdimg.com/libs/select2/3.4.8/select2.css" rel="stylesheet"/>
<script src="//www.101hr.com/2020120301/static/scripts/require.js"></script>
<script>
var ssc_hostname = "http://api.members.shebao.net?service=";
try{
    require.config({
        baseUrl: "//www.101hr.com/2020120301/static/scripts/libs",
        paths: ((function(file_module, file_path){
            var script_paths = {
                jquery : '//www.101hr.com/2020120301/static/lib/jquery/dist/jquery.min',
                layer  : '//www.101hr.com/2020120301/static/lib/layer/innerlayer.min', //.min /static/lib/layer/innerlayer.min.js
                //layer  : '//www.101hr.com/2020120301/static/lib/layer/layer', //.min
                layer2  : '//www.101hr.com/2020120301/static/lib/layer-v1.9.3/layer',
                'socket.io' : '//www.101hr.com/2020120301/static/lib/socket.io/1.2.1/socket.io',
                'jquery.ui.widget': '//www.101hr.com/2020120301/static/lib/jquery_file_upload/js/vendor/jquery.ui.widget',
                'jquery.iframe-transport': '//www.101hr.com/2020120301/static/lib/jquery_file_upload/js/jquery.iframe-transport',
                fileupload: '//www.101hr.com/2020120301/static/lib/jquery_file_upload/js/jquery.fileupload',
                'jquery.fileupload-process': '//www.101hr.com/2020120301/static/lib/jquery_file_upload/js/jquery.fileupload-process',
                cropper: '//www.101hr.com/2020120301/static/lib/cropper/dist/cropper',
                mustache:'//www.101hr.com/2020120301/static/lib/mustache/mustache.min',
                datePickerSelect: '//www.101hr.com/2020120301/static/lib/date_picker/src/select.min',
                tagEditor: '//www.101hr.com/2020120301/static/lib/tagEditor/jquery.tag-editor.min',
                commonScript: '//www.101hr.com/2020120301/static/js/common',
                jplaceholder: '//www.101hr.com/2020120301/static/js/jquery.jplaceholder',
                'jquery.ui':'//www.101hr.com/2020120301/static/lib/jquery/jquery-ui',
                'jquery.nicescroll':'//www.101hr.com/2020120301/static/js/jquery.nicescroll',
                 'autocomplete':'//www.101hr.com/2020120301/static/lib/jquery/autocomplete/autocomplete.min',
                 'index_echarts':'//www.101hr.com/2020120301/static/js/index/echarts.min',
                 Vue: '//www.101hr.com/2020120301/static/lib/vue/vue/1.0.14/vue.min'
            };

            if(file_path){
                (typeof script_paths[file_module] === "undefined") && (script_paths[file_module] = '../' + file_path);
            }
            return script_paths;
        })("home_index", "home/index")),
        waitSeconds: 0,
        shim: {
            'layer': {
                deps: ['jquery'],
                exports: 'layer'
            },
            'layer2': {
                deps: ['jquery'],
                exports: 'layer2'
            },
            Validator: {
                deps: ['Vue']
            },
            'socket.io' : {
                exports: "io"
            },
            'jquery.ui.widget': {
                deps: ['jquery']
            },
            'jquery.iframe-transport': {
                deps: ['jquery']
            },
            fileupload: {
                deps: ['jquery', 'jquery.ui.widget', 'jquery.iframe-transport']
            },
            cropper: {
                deps: ['jquery']
            },
            'jquery.fileupload-process': {
                deps: ['fileupload']
            },
            datePickerSelect: {
                deps: ['jquery']
            },
            tagEditor: {
                deps: ['jquery']
            },
            commonScript: {
                deps: ['jquery']
            },
            jplaceholder: {
                deps: ['jquery']
            }
        }
    });
}catch (e){
    console.log(e);
}
requirejs.onError = function (err) {
    console.log('type:' + err.requireType + 'modules: ' + err.requireModules);
};
try{
require(['jquery','layer', 'commonScript', 'Vue'], function($,layer, common, Vue){
    function getUrlParam(name){
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1) ? window.location.search.substr(1).match(reg) : null;
        if (r != null) return unescape(r[2]);
        return null;
    }
    if(getUrlParam('is_change_company')){

        $.layer({
                    title:'温馨提示',
                    type: 1,
                    shade: [0],
                    btn:['刷新'],
                    btns:1,
                    border: [0],
                    area: ['460px', 'auto'],
                    page:{
                        html:'<div style="text-align:center; height:120px;">您的登录企业已发生变化，请刷新页面</div>'
                    },
                    yes:function(index){
                        layer.close(index);
                        window.location.href = '/finance/cash/summary';

                    }
                })
    }
    $.ajaxSetup({
        headers: {
            encryptCid: $('#encryptCid').val() || ''
        },
        complete:function(xhr, status){
            if(xhr.responseJSON.code == 22005){
                // layer.open({
                //     content: '<div>您的登录企业已发生变化，请刷新页面</div>',
                //     title: '温馨提示',
                //     btn: ['刷新'],
                //     area: ['460px', '202px'],
                //     closeBtn:0,
                //     success: function () {
                        
                //     },
                //     yes: function (index) { 
                //         layer.close(index);
                //         window.location.reload();
                //     }
                // })
                $.layer({
                    title:'温馨提示',
                    type: 1,
                    shade: [0],
                    btn:['刷新'],
                    btns:1,
                    border: [0],
                    area: ['460px', 'auto'],
                    page:{
                        html:'<div style="text-align:center; height:120px;">您的登录企业已发生变化，请刷新页面</div>'
                    },
                    yes:function(index){
                        layer.close(index);
                        window.location.reload();

                    }
                })
            }
        }
    });
    
});
}catch (e){}

</script>
<script async="true" defer="defer" src="//www.101hr.com/2020120301/static/scripts/home/index.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="//www.101hr.com/2020120301/static/js/html5.js"></script>
<script>
    require && require(['jplaceholder'], function(jplaceholder){
        JPlaceHolder.init();
    });
</script>
<![endif]-->
<style>
[v-cloak] { 
	display: none;
}
</style>
</head>
<body class="aaa">
<div class="wrapper clearfix" style="overflow: hidden;zoom:1;min-width: 1200px;">
<div id="tip"><span></span></div>
<div class="tip_block"></div>
<script>
    (function(a, h, c, b, f, g) {
        var has_token = "";
        if(!has_token){
          a["qimoClientId"] = Math.random()+'_';
        }else{
            a["qimoClientId"]={
            customField:{
                商户ID: "",
                商户名称: "",
                操作人ID: ""
            },
            userId: ""+"_"
            }
        }
      g = h.createElement('script');
      g.src = b;
      h.getElementsByTagName('body')[0].appendChild(g);
    })(window, document, "script", "https://webchat.7moor.com/javascripts/7moorInit.js?accessId=910920a0-1a5c-11eb-a51a-1be58fb800cd&autoShow=true&language=ZHCN");
</script>
<script src="//www.101hr.com/2020120301/static/js/index/jquery.flexslider-min.js" type="text/javascript"></script>
<div class="index_header">
<h1 class="logo_banner">
<a href="/" target="_blank">
<img alt="101HR-最高效的企业EHR人力资源管理平台" src="//www.101hr.com/2020120301/static/images/ucenter/logo.png"/>
</a>
</h1>
<div class="head_a" id="head_a">
<a class="head_signup" href="https://home.101hr.com/register?ttlurl=https://www.101hr.com/loginregist&amp;reback=https://www.101hr.com/index" target="_blank">企业注册</a>
<a class="head_login" href="https://home.101hr.com/login?ttlurl=https://www.101hr.com/loginregist&amp;reback=https://www.101hr.com/index">登录</a>
</div>
</div>
<div class="flexslider banner ">
<ul class="slides">
<li class="slides_one" style="background:url(//www.101hr.com/2020120301/static/images/home/banner_2.png?id=17051601) no-repeat center;">
<a href="https://home.101hr.com/register?ttlurl=https://www.101hr.com/loginregist&amp;reback=https://www.101hr.com/index">
<!-- <h3 class="title1">101HR<span>永久免费</span></h3>
				<h3 class="title2">高效便捷的一站式人力资源共享服务云平台</h3>
				<p>招聘+人事+合同+保险+薪酬+福利</p>
				<a href="/users/signup" target="_blank">立即注册</a> -->
</a></li>
</ul>
</div>
<script type="text/javascript">
	// $(function(){
 //        $('.flexslider').flexslider({
 //            directionNav: true,
 //            pauseOnAction: false
 //        });
 //        $(".flex-control-nav a").text("");
 //    });
    function state_ajax(){
        $.ajax({
            url:"/api/user/login_status",
            type:"post",
            dataType:"json",
            async:false,
            success:function(datas){
               if(datas.code==22000){
             
                    $('#head_a .head_login').html('进入首页').attr("href","/index");
               }else{
                    $('#head_a .head_login').html('登录').attr("href","https://home.101hr.com/login?ttlurl=https://www.101hr.com/loginregist&reback=https://www.101hr.com/index");
               }
            }
        });

    }
    state_ajax();
    $('#head_a .head_login').on("click",function(){
        state_ajax();
    });
    
</script>
<div class="service">
<div class="service_box">
<ul class="service_tab tabs_con">
<li class="tab tab_current">独创的4S模式</li>
<li class="tab">社保服务</li>
<li class="tab">薪酬服务</li>
</ul>
<div class="cons_group">
<dl class="con clearfix" style="display:block;">
<dt>
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_1_1.png"/>
</dt>
<dd style="margin-top:34px;">
<h4>Solution（方案）</h4>
<p>我司推出的“薪税保” 为企业提供薪酬、个税、社保综合解决方案</p>
</dd>
<dd>
<h4>System（系统）</h4>
<p>高效便捷的一站式人力资源共享服务平台，轻松做人力</p>
</dd>
<dd>
<h4>Shared Service（共享服务）</h4>
<p>秉承“真服务，直营好”理念，专业客户成功团队，提供贴心的共享服务</p>
</dd>
</dl>
<dl class="con clearfix" style="display:none;">
<dt>
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_1_2.png"/>
</dt>
<dd style="margin-top:84px;">
<h4>覆盖全国的直营服务网络，统一的标准化服务体系</h4>
</dd>
<dd>
<h4>全程透明、可信赖的优质服务，为您省时省力省成本！</h4>
</dd>
<dd>
<h4>社保+公积金+补充医疗 一键搞定！</h4>
</dd>
</dl>
<dl class="con clearfix" style="display:none;">
<dt>
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_1_3.png"/>
</dt>
<dd style="margin-top:84px;">
<h4>社保专家经过人力成本精算，降低企业用工成本，老板舒心 </h4>
</dd>
<dd>
<h4>优化后员工收入显著提升，员工开心</h4>
</dd>
<dd>
<h4>专业设计可以实现企业财税合规，全方位规避风险</h4>
</dd>
</dl>
</div>
</div>
</div>
<div class="serve_function">
<h2>我们有哪些功能</h2>
<ul class="pt0">
<li class="clearfix">
<div class="fl">
<h3><img alt="招聘" src="//www.101hr.com/2020120301/static/images/home/icon_2_1.png"/>招聘</h3>
<h4>智能简历筛选 快速招到合适人才 </h4>
<p>多渠道海量简历，帮你收取帮你筛，快速找人才<br/>
	一键发布职位，多渠道可选，让招聘更容易</p>
</div>
<img alt="招聘" class="funt_img fr" src="//www.101hr.com/2020120301/static/images/home/zhaoping.gif"/>
</li>
</ul>
<ul class="bg_color">
<li class="clearfix">
<img alt="人事" class="funt_img fl" src="//www.101hr.com/2020120301/static/images/home/renshi.png"/>
<div class="fr">
<h3><img alt="人事" src="//www.101hr.com/2020120301/static/images/home/icon_2_2.png"/>人事</h3>
<h4>一站式人事管理  轻松管人事</h4>
<p>员工入、转、调、离一站式管理，人事流程灵活设置<br/>
	离职自动触发保险减员，管理更贴心</p>
</div>
</li>
</ul>
<ul>
<li class="clearfix">
<div class="fl">
<h3><img alt="合同" src="//www.101hr.com/2020120301/static/images/home/icon_2_3.png"/>合同</h3>
<h4>在线合同想签就签 权威认证合法有效  </h4>
<p>在线签署，无需快递。结构化管理，检索更容易<br/>
	移动端一键签署，适用于多种签章场景，规避用工风险</p>
</div>
<img alt="合同" class="funt_img fr" src="//www.101hr.com/2020120301/static/images/home/hetong.gif"/>
</li>
</ul>
<ul class="bg_color">
<li class="clearfix">
<img alt="保险" class="funt_img fl" src="//www.101hr.com/2020120301/static/images/home/baoxian.png"/>
<div class="fr">
<h3><img alt="保险" src="//www.101hr.com/2020120301/static/images/home/icon_2_4.png"/>保险</h3>
<h4>五险一金、商业保险集中管</h4>
<p>一键上保险，操作简易，进度随时查<br/>
	缴纳方式灵活，HR省心，员工放心</p>
</div>
</li>
</ul>
<ul>
<li class="clearfix">
<div class="fl">
<h3><img alt="薪酬" src="//www.101hr.com/2020120301/static/images/home/icon_2_5.png"/>薪酬</h3>
<h4>企业发薪灵活，员工随时查看</h4>
<p>在线发薪，灵活易操作，薪资报表随时查看<br/>
	增加员工工资条，沟通反馈更及时</p>
</div>
<img alt="薪酬" class="funt_img fr" src="//www.101hr.com/2020120301/static/images/home/xinchou.gif"/>
</li>
</ul>
<ul class="bg_color">
<li class="clearfix">
<img alt="福利" class="funt_img fl" src="//www.101hr.com/2020120301/static/images/home/fuli.png"/>
<div class="fr">
<h3><img alt="福利" src="//www.101hr.com/2020120301/static/images/home/icon_2_6.png"/>福利</h3>
<h4>有福利，没烦恼，给员工更多关爱</h4>
<p>补充医疗，全国适用。企业投入低，员工更满意<br/>
	薪酬优化，专家精算，提升收入，留住人才</p>
</div>
</li>
</ul>
</div>
<!-- <div class="industrial_report">
	<h2>行业报告下载</h2>
	<p class="ind_report_p">本公司连续多年发布《中国企业社保白皮书》，报告坚持第三方视角，对数万中国HR社保从业人员进行深度调查和专业分析</p>
	<ul class="ind_report_ul clearfix">
		<li>
			<p>2016《中国企业社保白皮书》</p>
			<a href="javascript:void(0);" rel="nofollow">付费下载</a>
		</li>
		<li>
			<p>2015《中国企业社保白皮书》</p>
			<a href="javascript:void(0);" rel="nofollow">付费下载</a>
		</li>
		<li>
			<p>2014《中国企业社保白皮书》</p>
			<a href="javascript:void(0);" rel="nofollow">付费下载</a>
		</li>
		<li>
			<p>2013《中国企业社保白皮书》</p>
			<a href="javascript:void(0);" rel="nofollow">付费下载</a>
		</li>
	</ul>
</div> -->
<div class="advantage bg_white">
<div class="advantage_box">
<h3 class="adv_title">我们的优势</h3>
<ul class="tabs_groups">
<li class="tab1 tab_current">贴心的服务</li>
<li class="tab1">银行级的安全保障</li>
</ul>
<div class="cons_group3">
<ul class="con1 clearfix" style="display:block;">
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_1.png"/>
<div>
<h4>专业+创新</h4>
<p>由社保领域权威专家余清泉带领的顶级专业团队，创新研发各种产品方案，快速解决您的所需</p>
</div>
</li>
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_2.png"/>
<div>
<h4>强大的客户成功团队，全天候贴心服务</h4>
<p>您的需求就是我们的追求，强大的服务团队，高效全面提供即时服务，为您的企业保驾护航</p>
</div>
</li>
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_3.png"/>
<div>
<h4>通知及时到达</h4>
<p>社保不能断缴，时限不能错过，多渠道给您及时的回复和通知，降低您利益受损的可能性</p>
</div>
</li>
</ul>
<ul class="con1 clearfix" style="display:none;">
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_4.png"/>
<div>
<h4>双云端服务</h4>
<p>依托先进的阿里云端服务+AWS云服务，数据可靠性不低于99.999%。</p>
</div>
</li>
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_5.png"/>
<div>
<h4>全流程风险监控</h4>
<p>采用自动宕机迁移、数据备份和回滚等机制。自动故障检测与恢复</p>
</div>
</li>
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_6.png"/>
<div>
<h4>恶意访问监测</h4>
<p>LVS SYNPROXY技术防攻击能力，结合云盾提供防DDoS攻击</p>
</div>
</li>
<li class="clearfix">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/icon_3_7.png"/>
<div>
<h4>信息独立管理</h4>
<p>多用户资源完全隔离，防密码破解</p>
</div>
</li>
</ul>
</div>
</div>
</div>
<section class="client overflow">
<div class="client_box">
<div class="intitle posRel">
<h3>我们的合作商户 <a alt="更多商户&gt;&gt;" href="/about/client" style="display:none;" target="_blank">MORE&gt;&gt;</a></h3>
</div>
<ul class="clientList clearfix">
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/1.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/2.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/3.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/4.jpg"/>
</a>
</li>
<li class="last">
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/5.jpg"/>
</a>
</li>
<li class="last">
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/6.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/7.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/8.jpg"/>
</a>
</li>
<li>
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/9.jpg"/>
</a>
</li>
<li class="">
<a rel="nofollow">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/partner/10.jpg"/>
</a>
</li>
</ul>
</div>
</section>
<section class="index_signup_box">
<h2 class="signup_box_title">让我们一起，开启体验吧</h2>
<a class="index_signup_btn" href="https://home.101hr.com/register?ttlurl=https://www.101hr.com/loginregist&amp;reback=https://www.101hr.com/index" target="_blank">立即注册</a>
</section>
<script id="home_affirm" type="text/x-template">
<div class="pop_body home_affirm" >
    <p>该报告为本公司独家发布，售价为19.9元/份<br>
点击按钮即可扫码支付</p>
</div>
</script>
<script id="change_company_layer" type="text/x-template">
<div class="pop_body home_payment" >
    <img src="//www.101hr.com/2020120301/static/images/home/home_erweima.png" alt="支付二维码">
	<p>微信扫码支付</p>
</div>
</script>
</div>
<div class="bottom_service">
<div class="ser_list clearfix">
<div class="ser_list_left">
<dl>
<dt>关于</dt>
<dd><a href="/issue/about" target="_blank">关于我们</a></dd>
<dd><a href="/issue/aptitude" target="_blank">资质认证</a></dd>
<dd><a href="/issue/contact" target="_blank">联系我们</a></dd>
<dd><a href="/issue/business" target="_blank">商务合作</a></dd>
</dl>
<dl>
<dt>产品</dt>
<dd><a href="/issue/introduce" target="_blank">产品介绍</a></dd>
<dd><a href="/issue/safety" target="_blank">安全与隐私</a></dd>
<dd><a href="/issue/client" target="_blank">合作商户</a></dd>
</dl>
<dl>
<dt>使用</dt>
<!-- <dd style="display:none;"><a href="/issue/video" target="_blank">视频教程</a></dd> -->
<dd><a href="/issue/protocol" target="_blank">使用协议</a></dd>
<dd><a href="http://crm2.qq.com/page/portalpage/wpa.php?uin=4006651751&amp;aty=0&amp;a=0&amp;curl=&amp;ty=1" target="_blank">在线咨询</a></dd>
</dl>
</div>
<div class="ser_list_right">
<ul>
<li><i class="sicon "></i>客服电话：400-668-5151</li>
<li><i class="sicon sicon2"></i>问题投诉：tousu@51shebao.com</li>
<li><i class="sicon sicon3"></i>产品意见反馈：fankui@101hr.com</li>
</ul>
<div class="erweima">
<img alt="" src="//www.101hr.com/2020120301/static/images/home/footer_erma.png"/>
<p>扫一扫<br/>
立刻关注我们</p>
</div>
</div>
</div>
<div class="home_hot">
<label>51社保旗下网站:</label>
<a href="http://www.51shebao.com/" target="_blank">51社保®</a>
<a href="http://www.51shebao.org/" target="_blank">51社保论坛</a>
<a class="bor_none" href="/" target="_blank">101HR</a>
<a href="https://www.hrsay.com/" target="_blank">HRSAY</a>
</div>
</div>
<footer class="footer clearfix">
<div class="footer_box clearfix">
<div class="footer_left fl">
<!-- <a class="safety_cx bradius4" id='___szfw_logo___' href='https://search.szfw.org/cert/l/CX20150624010868010610' target='_blank'></a> -->
<!--可信网站图片LOGO安装开始-->
<span id="kx_verify"></span>
<script type="text/javascript">(function (){ var _kxs = document.createElement('script');_kxs.id = 'kx_script';_kxs.async = true;_kxs.setAttribute('cid', 'kx_verify');_kxs.src = ('https:' == document.location.protocol ? 'https://ss.knet.cn' : 'http://rr.knet.cn')+'/static/js/icon3.js?sn=e15070211010659304trgu000000&tp=icon3';_kxs.setAttribute('size', 4);var _kx = document.getElementById('kx_verify');_kx.parentNode.insertBefore(_kxs, _kx); })();</script>
<!--可信网站图片LOGO安装结束-->
<!-- <a  class="safety_aqlm" key ="55890cc2efbfb03ac5bfb1d5" logo_size="124x47" logo_type="business" href="http://www.anquan.org" ><script src="//static.anquan.org/static/outer/js/aq_auth.js"></script></a> -->
<!-- <a class="safety_360 bradius4" href="http://webscan.360.cn/index/checkwebsite/url/www.101hr.com" target="_blank"></a> -->
<a class="safety_qxrz bradius4 fl" href="http://ec.eqixin.com/?sn=QX6000070412171467857640" target="_blank"></a>
</div>
<div class="footer_right fr">
<!-- <div class="copy">©&nbsp;2015-2017 &nbsp;&nbsp;<a herf="http://www.101hr.com" target="_blank">www.101hr.com</a>&nbsp;&nbsp;
			<a herf="http://www.miibeian.gov.cn/" target="_blank">京ICP备13001493号-10</a></div>
			<p class="copy">地址：北京市朝阳区水碓子北里8号楼4层东405</p> -->
<div class="copy">© 2015-2020   <a href="http://www.101hr.com" target="_blank">www.101hr.com</a>  
			<a href="http://beian.miit.gov.cn/" rel="nofollow" target="_blank">京ICP备13001493号-10</a></div>
<p class="copy">地址：北京市朝阳区建外SOHO 西区17号楼18层</p>
</div>
</div>
</footer>
<script>

var _hmt = _hmt || [];
  (function() {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?6d8f4e948527b8b0863fafddafe72097";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
  })();
</script>
<!--[if lt IE 9]>
	<div class="browser" id="browser">
		<label>为了您更高效，更好的体验，推荐您使用这些浏览器的最新版本：</label>
		<ul class="ser_list">
			<li><a href="http://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=2&tn=baiduhome_pg&wd=%E8%B0%B7%E6%AD%8C%E6%B5%8F%E8%A7%88%E5%99%A8&rsv_spt=1&rsv_pq=d22a27890000b739&rsv_t=7b52EH3pqYXq4rc7A%2BXtj9GgvepLpa2NJnxFCGhAeJjp1DLG9YMwOXOxVRi3stqiYd2g&rsv_enter=0&rsv_sug3=1&rsv_sug4=42&rsv_sug1=1&rsv_sug=1" target="_blank" rel="nofollow"><em class="icon icon1"></em>谷歌浏览器</a></li>
			<li><a href="http://www.firefox.com.cn/download/" target="_blank" rel="nofollow"><em class="icon icon2"></em>火狐浏览器</a></li>
			<li><a href="http://se.360.cn/" target="_blank" rel="nofollow"><em class="icon icon3"></em>360浏览器</a></li>
			<li><a href="http://www.liebao.cn/download.html" target="_blank" rel="nofollow"><em class="icon icon4"></em>猎豹浏览器</a></li>
		</ul>
		<span class="aclose" onclick="closeBrowserDialog()"></span>
		<script>
        function closeBrowserDialog(){
            var node = document.getElementById('browser');
            node && (node.style.display = 'none');
        }
		</script>
	</div>
<![endif]-->
</body>
</html>