<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-130100796-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130100796-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-538BH6P');</script>
<!-- End Google Tag Manager -->
<title>De Mûelenaere Oncology &gt; Home</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="cancersa, cancer, treatment, radiation, chemotherapy, Oncology, radiotherapy,De Mûelenaere Oncology, DMO" name="keywords"/>
<meta content="Cancer treatment available in Gauteng, South Africa. De Mûelenaere Oncology (DMO) is a group of radiation oncology practices that offer cancer patients the total spectrum of radiotherapy services." name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<link href="//fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css"/>
<link href="/includes/css/swiper.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/dark.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/font-icons.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/style6959.css" id="style" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider369f.css?ver=6.6.4" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/contact-form-7/includes/css/styles20fd.css?ver=4.9.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/cf7-conditional-fields/stylec412.css?ver=1.3.4" id="cf7cf-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-listing/assets/css/listing6959.css?ver=4.9.3" id="qode_listing_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-listing/assets/css/listing-responsive.min6959.css?ver=4.9.3" id="qode_listing_style_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-membership/assets/css/qode-membership.min6959.css?ver=4.9.3" id="qode_membership_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-membership/assets/css/qode-membership-responsive.min6959.css?ver=4.9.3" id="qode_membership_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-quick-links/assets/css/qode-quick-links.min6959.css?ver=4.9.3" id="qode_quick_links_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-restaurant/assets/css/qode-restaurant.min6959.css?ver=4.9.3" id="qode_restaurant_script-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-restaurant/assets/css/qode-restaurant-responsive.min6959.css?ver=4.9.3" id="qode_restaurant_responsive_script-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/svg-vector-icon-plugin/admin/css/wordpress-svg-icon-plugin-style.min6959.css?ver=4.9.3" id="default-icon-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/superfish6959.css?ver=4.9.3" id="timetable_sf_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/style6959.css?ver=4.9.3" id="timetable_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/event_template6959.css?ver=4.9.3" id="timetable_event_template-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/responsive6959.css?ver=4.9.3" id="timetable_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato%3A400%2C700&amp;ver=4.9.3" id="timetable_font_lato-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/wp-job-manager/assets/css/chosenf488.css?ver=1.1.0" id="chosen-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/wp-job-manager/assets/css/frontend2866.css?ver=1.29.2" id="wp-job-manager-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/style6959.css?ver=4.9.3" id="default_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/font-awesome/css/font-awesome.min6959.css?ver=4.9.3" id="qode_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/elegant-icons/style.min6959.css?ver=4.9.3" id="qode_font_elegant-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/linea-icons/style6959.css?ver=4.9.3" id="qode_linea_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/dripicons/dripicons6959.css?ver=4.9.3" id="qode_dripicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/stylesheet.min6959.css?ver=4.9.3" id="stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/print6959.css?ver=4.9.3" id="qode_print-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/timetable-schedule.min6959.css?ver=4.9.3" id="qode_timetable-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/timetable-schedule-responsive.min6959.css?ver=4.9.3" id="qode_timetable_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-news/assets/css/news-map.min6959.css?ver=4.9.3" id="qode_news_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/responsive.min6959.css?ver=4.9.3" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-news/assets/css/news-map-responsive.min6959.css?ver=4.9.3" id="qode_news_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/style_dynamic3650.css?ver=1517570090" id="style_dynamic-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/style_dynamic_responsive3650.css?ver=1517570090" id="style_dynamic_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/js_composer/assets/css/js_composer.min5243.css?ver=5.4.5" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/custom_cssd11a.css?ver=1517570086" id="custom_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/master-slider/public/assets/css/masterslider.main5589.css?ver=3.4.1" id="msl-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/uploads/master-slider/custom7ef2.css?ver=1.5" id="msl-custom-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/cache/nextend/web/n2-ss-2/n2-ss-2b6d0.css?1517574399" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic|Oxygen:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic|Quicksand:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- External JavaScripts
	============================================= -->
<script src="/includes/js/jquery.js" type="text/javascript"></script>
<script src="/includes/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/includes/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/includes/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
		var isMobi = false		//var isMobi = 'true';
	</script>
<!-- Footer Scripts
	============================================= -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
</head>
<body class="stretched no-transition">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-538BH6P" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="clearfix" id="wrapper">
<header class="full-header" id="header" style="position:fixed;">
<div id="header-wrap">
<section style="">
<div class="container clearfix" style="text-align:Center;"><div class="visible-lg-block visible-md-block " id="logo" style="text-align:Center;">
<a class="standard-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/" style="text-align:Center;"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="margin-right: auto;margin-left: auto;"/></a>
<a class="retina-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/" style="text-align:Center;"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="margin-right: auto;margin-left: auto;"/></a>
</div>
<div class="visible-xs-block visible-sm-block" id="logo">
<a class="standard-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="height:70px !important"/></a>
<a class="retina-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="height:70px !important; padding-top:20px;"/></a>
</div></div>
<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
<div class="visible-lg-block " style="position:relative;float:right;color:#fff;right: 1%;z-index: 9999999;top:20px;">
<!-- <div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/in.png" alt=""></a></div>
	        	<div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/yt.png" alt=""></a></div>
	        	<div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/ig.png" alt=""></a></div> 
	        	<div style="display:inline-block;width:40px;"><a href="https://www.facebook.com/SandtonOncologyCentre" target="_blank"><img alt="Sandton Oncology Facebook" style="width:30px" src="/includes/images/fb.png" alt=""></a></div>-->
</div>
<nav class="dark" id="primary-menu"><ul><li class=" "><a href="/home">Home</a></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="about">About <b class="caret"></b> </a><ul style="0"><li class=" "><a href="vision">What We Stand For</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="specialists">Specialists <b class="caret"></b> </a><ul style="0"><li class=" "><a href="robert">Dr Robert Alexander Georges (Robbie) de Mûelenaere</a></li><li class=" "><a href="kobie">Dr Kobie Pio</a></li><li class=" "><a href="ingo">Dr Ingo de Mûelenaere</a></li><li class=" "><a href="bernard">Prof Bernard Donde</a></li><li class=" "><a href="sudeshen">Dr Sudeshen M Naidoo</a></li><li class=" "><a href="marlene">Dr Marlene Soares</a></li><li class=" "><a href="nirasha">Dr Nirasha Chiranjan</a></li><li class=" "><a href="maryke">Dr Maryke Etsebeth</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="">Services <b class="caret"></b> </a><ul style="0"><li class=" "><a href="beam">External Beam Radiation Therapy</a></li><li class=" "><a href="stereo">Stereotactic Radiosurgery</a></li><li class=" "><a href="intensity">Intensity-Modulated Radiation Therapy</a></li><li class=" "><a href="brach">Brachytherapy</a></li><li class=" "><a href="ortho">Orthovoltage Radiation Therapy</a></li><li class=" "><a href="forums">Multidisciplinary Forums</a></li><li class=" "><a href="social">Oncology Social Workers for Psychosocial Care</a></li><li class=" "><a href="ct">CT Scanners For Treatment Planning</a></li><li class=" "><a href="survivor">Survivor Run Support Groups</a></li><li class=" "><a href="shuttle">Shuttle Services</a></li><li class=" "><a href="medical">Medical Oncologists</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="patient">Patient Info <b class="caret"></b> </a><ul style="0"><li class=" "><a href="treatment">Treatment Information</a></li><li class=" "><a href="faq">FAQS</a></li><li class=" "><a href="support">Charities, Useful Links and Networks</a></li><li class=" "><a href="testimonial">Testimonials</a></li><li class=" "><a href="covid19">Covid-19<br/>What you need to know</a></li><li class=" "><a href="visitor">Covid-19 Visitor Policy</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="">A DMO center near you <b class="caret"></b> </a><ul style="0"><li class=" "><a href="ahmed">Ahmed Kathrada Cancer Institute</a></li><li class=" "><a href="sandton">Sandton Oncology</a></li><li class=" "><a href="westrand">West Rand Oncology Centre</a></li><li class=" "><a href="muelmed">Muelmed Radiation Oncology</a></li><li class=" "><a href="groenkloof">Groenkloof Radiation Oncology</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="blog">Blog</a></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="request">Request an Appointment <b class="caret"></b> </a><ul style="0"><li class=" "><a href="request#prepare">How to prepare for your appoint ment</a></li></ul></li></ul></nav>
</section>
</div>
</header>
<script type="text/javascript">
 $(document).ready(function() {
	 
	 ////$("#abtlikebox").hover(
		//	 function() {
	//			 $(this).stop().animate({right: "0"}, "medium");
	//			 }, 
	//		function() {
	//			$(this).stop().animate({right: "-250"}, "medium");
	//			}, 500);
		});
 </script>
<style type="text/css">
.abtlikebox {
	background: url("includes/images/fb1-right-red.png") no-repeat scroll
		left center transparent !important;
	float: right;
	height: 270px;
	padding: 0 5px 0 34px;
	z-index: 99999;
	position: fixed;
	right: -250px;
	top: 10%;
	z-index: 99;
}

.abtlikebox div {
	padding: 0;
	margin-right: -8px;
	border: 4px solid #3b5998;
	background: #fafafa;
}

.abtlikebox span {
	bottom: 4px;
	font: 8px "lucida grande", tahoma, verdana, arial, sans-serif;
	position: absolute;
	right: 6px;
	text-align: right;
	z-index: 99999;
}

.abtlikebox span a {
	color: gray;
	text-decoration: none;
}

.abtlikebox span a:hover {
	text-decoration: underline;
}

.sm {
	position: absolute;
	right: 0px;
	z-index: 99999;
	width: 50px;
	top: 20%;
	margin-right: -3px;
	position: fixed;
}
.swiper-slide p{
color:#fff !important;
font-size:14px;
font-style:italic;
margin:0px;
}
</style>
<div class="content_inner home " id="site-content">
<div class="title_outer title_without_animation" data-height="307">
<div class="title title_size_small position_left " style="height: 307px;">
<div class="image not_responsive"></div>
<div class="title_holder" style="padding-top: 207px; height: 100px;">
<div class="container">
<div class="container_inner clearfix">
<div class="title_subtitle_holder">
<h1>
<span>Home New</span>
</h1>
<span class="separator small left"></span>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="full_width">
<div class="full_width_inner">
<div class="covidContainer" style="text-align:Center;font-size:20px;color:#fff;margin-top:207px;height:300px;background-image: url('/includes/images/covid_header.jpg');background-position: center center;background-size: cover;background-repeat: no-repeat;padding-top: 20px;">
<div style="padding:20px 0px;">Learn about our preparedness for COVID-19 and our updated<br/>
					policies to protect our patients and employees.</div>
<div style="padding:20px 0px;"><a href="/covid19" style="color:#fff;border-radius:5px;padding:15px;background:#225ca5;border:2px solid #35aae0 ">What you need to know</a></div>
<div style="padding:20px 0px;">For more information on Covid-19 visit the official South African<br/>
Covid-19 portal at <a href="https://www.sacoronavirus.co.za" style="color:#fff;" target="_blank">www.sacoronavirus.co.za</a></div>
</div>
<section class="slider-parallax swiper_wrapper clearfix" id="slider" style="/* margin-top: 207px; */ background: #fff">
<div class="" style="height: 100%">
<div class="slider-parallax-inner" style="position: relative; height: 100%;">
<div class="swiper-container swiper-parent">
<div class="swiper-wrapper">
<div class="swiper-slide dark" style="background-image: url('/includes/images/headers/slide-1-1.jpg');">
<div class="container clearfix slider-text-background">
<div class="col-md-12">
<div class="slider-caption slider-caption-center" style="vertical-align: top;">
<div style="margin-bottom: 15px;">
<div style="text-align: center; display: inline-block; width: 100%; margin-top: 1%;">
<span class="slider-text-span" style="font-family: 'Oxygen', sans-serif; font-weight: 600; color: #fff; font-size: 1.5em;"> Your Story Our Journey </span>
</div>
</div>
<div>
<div style="margin-left: auto; margin-right: auto; width: 200px; border-top: 1px solid #fff"></div>
</div>
<div style="font-family: 'Oxygen', sans-serif;; margin-top: 15px; color: #fff">
													PATIENT FOCUSED CARE</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide dark" style="background-image: url('/includes/images/headers/slide-2-3.jpg');">
<div class="container clearfix slider-text-background">
<div class="col-md-12">
<div class="slider-caption slider-caption-center" style="vertical-align: top;">
<div style="margin-bottom: 15px;">
<div style="text-align: center; display: inline-block; width: 100%; margin-top: 1%;">
<span class="slider-text-span" style="font-family: 'Oxygen', sans-serif; font-weight: 600; color: #fff; font-size: 1.5em;"> Your Story Our Journey </span>
</div>
</div>
<div>
<div style="margin-left: auto; margin-right: auto; width: 200px; border-top: 1px solid #fff"></div>
</div>
<div style="font-family: 'Oxygen', sans-serif;; margin-top: 15px; color: #fff">
													PATIENT FOCUSED CARE</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide dark" style="background-image: url('/includes/images/headers/slide-3.jpg');">
<div class="container clearfix slider-text-background">
<div class="col-md-12">
<div class="slider-caption slider-caption-center" style="vertical-align: top;">
<div style="margin-bottom: 15px;">
<div style="text-align: center; display: inline-block; width: 100%; margin-top: 1%;">
<span class="slider-text-span" style="font-family: 'Oxygen', sans-serif; font-weight: 600; color: #fff; font-size: 1.5em;"> Your Story Our Journey </span>
</div>
</div>
<div>
<div style="margin-left: auto; margin-right: auto; width: 200px; border-top: 1px solid #fff"></div>
</div>
<div style="font-family: 'Oxygen', sans-serif;; margin-top: 15px; color: #fff">
													PATIENT FOCUSED CARE</div>
</div>
</div>
</div>
</div>
</div>
<div id="slider-arrow-left">
<i class="icon-angle-left"></i>
</div>
<div id="slider-arrow-right">
<i class="icon-angle-right"></i>
</div>
<!-- <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div> -->
</div>
</div>
</div>
</section>
<div class="vc_row wpb_row section vc_row-fluid columns-home" style="padding-top: 45px !important; padding-bottom: 75px; text-align: left;">
<div class="container">
<div class="" style="margin-bottom:40px !important;text-align:center;">
<!-- De M&#251;elenaere Oncology is a group of radiation oncology centres, founded by Dr Georges de M&#251;elenaere, and we have been providing cancer treatment since 1976. -->
							De Mûelenaere Oncology is a group of radiation oncology centres that have been providing cancer treatment since 1976.  
							<br/><br/>
							DMO offers cancer patients the total spectrum of radiotherapy services. We believe that this focus on one treatment modality - radiation oncology - enables us to provide a high level of care in this area. Although we specialise in radiotherapy treatments, we do work with teams of Medical Oncologists at all of our centres to ensure that we can assist our patients with a holistic range of services related to the cancer care journey.
							<br/><br/>
							At DMO, we do more than provide treatment, we walk a life journey with our patients.  Our goal is to treat patients with state of the art equipment and technologies. Our team provides patient focused cancer care and services, which cover numerous aspects of the patient's experience from diagnosis to treatment, follow-up care and psycho social support - to enhance their lives throughout the cancer journey.
						</div>
</div>
<div class=" full_section_inner clearfix" style="padding: 0% 20%/20%%">
<div class="wpb_column vc_column_container vc_col-sm-4">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3 style="text-align: center;">Request An Appointment</h3>
<p> </p>
<p style="text-align: center;">
<a class="qbutton large default" href="/request" target="_self">Request An<br/> Appointment
											</a>
</p>
<p> </p>
<p style="text-align: center;">Get in touch by sending us a
											message</p>
</div>
</div>
</div>
</div>
</div>
<div class="wpb_column vc_column_container vc_col-sm-4">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3>Find A Centre</h3>
<p style="text-align: center;">
<a href="/ahmed">Ahmed Kathrada Cancer Institute</a><br/> <a href="/sandton">Sandton Oncology</a><br/> <a href="/westrand">West Rand Oncology Centre</a><br/> <a href="/muelmed">Muelmed Radiation Oncology</a><br/> <a href="/groenkloof">Groenkloof Radiation Oncology</a>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="wpb_column vc_column_container vc_col-sm-4">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h3>Services</h3>
<p style="text-align: center;">
<a href="/beam">External Beam Radiation Therapy</a>
</p>
<p style="text-align: center;">
<a href="/sterio">Stereotactic Radiosurgery</a>
</p>
<p style="text-align: center;">
<a href="/intensity">Intensity-Modulated Radiation Therapy
												(IMRT)</a>
</p>
<p style="text-align: center;">
<a href="/brach">Brachytherapy</a>
</p>
<p style="text-align: center;">
<a href="/forums">Multidisciplinary Forums (MDTs)</a>
</p>
<p style="text-align: center;">
<a href="/social">Oncology Social Workers for Psychosocial
												Care</a>
</p>
<p style="text-align: center;">
<a href="/ct">CT Scanners For Treatment Planning</a>
</p>
<p style="text-align: center;">
<a href="/survivor">Survivor Run Support Groups</a>
</p>
<p style="text-align: center;">
<a href="/shuttle">Shuttle Services</a>
</p>
<p style="text-align: center;">
<a href="/medical">Medical Oncologists</a>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="vc_row wpb_row section vc_row-fluid testimonail-container full_screen_section" style="background-image: url(/includes/wp-content/uploads/2018/01/testimonial-bg.jpg); border-bottom: 1px solid #ffffff; text-align: center;">
<div class=" full_section_inner clearfix">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner ">
<div class="wpb_wrapper">
<div class="slider-parallax-inner" style="position: relative; height: 100%;">
<div class="testimonials_c_holder clearfix dark">
<div class="testimonials_c testimonials_c_carousel" data-animation-speed="" data-auto-rotate-slides="3" data-number-per-slide="1" data-show-navigation="yes">
<div class="swiper-container swiper-parent">
<div class="swiper-wrapper">
<div class="swiper-slide dark testimonial_content" style="">
<div class="container testimonial_content_inner clearfix slider-text-background">
<div class="col-md-12">
<div class="slider-caption slider-caption-center" style="vertical-align: top;">
<div style="margin-bottom: 15px;">
<div style="text-align: center; display: inline-block; width: 100%; margin-top: 1%;">
<span class="" style="font-size: 15px; color: #ffffff;font-style:italic;font-weight:bold;">Looking back, I am amazed by the amount of support I received all round. My family, friends, a wonderful boss and colleagues – all of them were such a fantastic source of support. Having my Doctors and care team extend that support made the world of difference to a very difficult journey. Many hands really do make a task much lighter.
‘’I can’t emphasize this point enough… the Sandton Oncology care team is AWESOME and I thank them deeply.’’... <a href="/testimonial" style="color:#fff;">Read More</a> </span>
</div>
</div>
<div>
<div style=""><p>Mrs Sybel Shiba</p></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="slider-arrow-left">
<i class="icon-angle-left"></i>
</div>
<div id="slider-arrow-right">
<i class="icon-angle-right"></i>
</div>
<!-- <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div> -->
</div>
</div></div>
</div>
<!-- <div class='testimonials_c_holder clearfix dark'>
									<div class="testimonials_c testimonials_c_carousel"
										data-show-navigation="yes" data-animation-speed=""
										data-auto-rotate-slides="3" data-number-per-slide="1">
										<ul class="slides">
											<li id="testimonials-c-1048" class="testimonial_content"><div
													class="testimonial_content_inner">
													<div class="testimonial_text_holder">
														<div class="testimonial_text_inner"
															style="color: #ffffff;">
															<p style='font-size: 20pxpx; color: #ffffff;'>
																Looking back, I am amazed by the amount of support I
																received all round. My family, friends, a wonderful boss
																and colleagues - all of them were such a fantastic
																source of support. Having my Doctors and care team
																extend that support made the world of difference to a
																very difficult journey. Many hands really do make a task
																much lighter. <br /> "I can't emphasize this point
																enough... the Sandton Oncology care team is AWESOME and
																I thank them deeply." - <a href="/testimonial"
																	alt="Testimonials">Read More</a>
															</p>
															<p class="testimonial_author" style="color: #ffffff;">Mrs
																Sybel Shiba</p>
														</div>
													</div>
												</div></li>
												
										</ul>
									</div>
								</div>-->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="det_popup"></div>
<script type="text/javascript">

$(document).mousemove( function(e) {	
	//console.log(e)
	//elementMouseIsOver = document.elementFromPoint(e.clientX, e.clientY);
	//console.log($(elementMouseIsOver))
	//var mouseX = $(elementMouseIsOver).offset().left;
    //var mouseY = $(elementMouseIsOver).offset().top +40;
    var mouseX = e.pageX +40;
    var mouseY = e.pageY ;
    if(!isMobi)
    {
    	var ItPos = e.clientY;
    	var winHalfSize = $(window).height() / 2;
    	
    	if(ItPos > winHalfSize)
        	mouseY -= 300;
    	//console.log(mouseY + ' ' + mouseX);
    	$('.tooltip').css({'top':mouseY,'left':mouseX+20}).fadeIn('slow', function() {
        	
		
    	  });
    	//
    }


    


   
     /* $('.image').mouseover(function(){
	    //$(this).css('background','rgba(0,0,0,0.5)');
	    
	    $(this).css('opacity','.2');
	    $(this).parent().next('.middle').show();
	    $(this).parent().next('.middle').css('background','rgba(0,0,0,0.2)');
	}).mouseout(function(){
	    $(this).css('opacity','1');
	    $(this).parent().next('.middle').hide(); 
	});  */
}); 
	$(document).ready(function() {
		var mySwiper = new Swiper('.swiper-container', {
			  
			  speed:5000,
			  autoplay: 5000,
			  fadeEffect: {
				    crossFade: true
				  },
			loop:true,  
			});
		
		$('.pimage').parent().next('.middle').mouseout(function(){
        	$(".pimage").each(function( ) {
        		 $(this).css('opacity','1');
     	    	$(this).parent().next('.middle').hide(); 
        });
	   
	});
		
    $(".pimage").hover(function(){
    	 $(this).css('opacity','.2');
	 	    $(this).parent().next('.middle').show();
	 	    $(this).parent().next('.middle').css('background','rgba(0,0,0,0.2)');
	    },function(){
	    	
	    });
		$(".tooltip-bot, .tooltip-bot a, .nav-social-links a").tooltip({
		    placement: "right"
		});
		
		
			

	   
	});
	function resize(maxWidth,maxHeight) {
	     var image =  $('img'),
	        imgWidth = image.width(),
	        imgHeight = image.height(),
	        newWidth=0,
	        newHeight=0;

	    if (imgWidth/maxWidth>imgHeight/maxHeight) {
	        newWidth = maxWidth;
	    } else {
	        newHeight = maxHeight;
	    }
	    image.mapster('resize',newWidth,newHeight,resizeTime);   
	}

	// Track window resizing events, but only actually call the map resize when the
	// window isn't being resized any more

	function onWindowResize() {
	    
	   /* var curWidth = $(window).width(),
	        curHeight = $(window).height(),
	        checking=false;
	    if (checking) {
	        return;
	            }
	    checking = true;
	    window.setTimeout(function() {
	        var newWidth = $(window).width(),
	           newHeight = $(window).height();
	        if (newWidth === curWidth &&
	            newHeight === curHeight) {
	            resize(newWidth,newHeight); 
	        }
	        checking=false;
	    },resizeDelay );*/
	}

	$(window).bind('resize',onWindowResize);	

	
</script>
<div class="footer_inner footer-stick clearfix" id="site-footer">
<div class="footer_top_holder">
<div class="footer_top">
<div class="container">
<div class="container_inner">
<div class="three_columns clearfix">
<div class="column1 footer_col1">
<div class="column_inner">
<div class="widget_text widget widget_custom_html" id="custom_html-3"><div class="textwidget custom-html-widget"><img alt="DMO Logo" src="/includes/wp-content/uploads/2014/12/DMO-logo.png"/>
<br/>
<ul>
<!-- <li><a target="_blank" href="mailto:Marketing@cancersa.co.za"><i class="wp-svg-envelop envelop"></i></a></li> -->
<li></li>
<li><a href="https://www.facebook.com/DMOCancerSpecialists/" target="_blank"><i class="wp-svg-facebook-2 facebook-2"></i></a></li>
<!-- <li><a target="_blank" href="https://plus.google.com/u/0/101069306117158681068"><i class="wp-svg-google-plus-3 google-plus-3"></i></a></li> -->
<li><a href="https://twitter.com/DMOcancersa" target="_blank"><i class="wp-svg-twitter-2 twitter-2"></i></a></li>
</ul></div></div> </div>
</div>
<div class="column2 footer_col2">
<div class="column_inner">
<div class="widget_text widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget"><h1>
	A DMO Centre Near You
</h1>
<table>
<td><strong>Ahmed Kathrada Cancer Institute</strong><br/>Ahmed Kathrada Private Hospital<br/>K43 Highway<br/>Lenasia Extension 8<br/>Johannesburg, Gauteng<br/>+27 (10) 900 3199<br/><a href="/ahmed">View Map</a></td>
<td><strong>Sandton Oncology</strong><br/>200 Rivonia Medical Centre<br/>200 Rivonia Road<br/>Morningside, Sandton<br/>+27 (11) 883 0900<br/><a href="/sandton">View Map</a></td>
<td><strong>West Rand Oncology Centre</strong><br/>Flora Clinic<br/>Cnr William Nicol Road &amp; Joseph Lister Avenue<br/>Floracliffe, Roodepoort<br/>+27 (11) 991 3500<br/><a href="/westrand">View Map</a></td>
<td><strong>Muelmed Radiation Oncology</strong><br/>Muelmed Hospital<br/>577 Pretorius Street<br/>Arcadia, Pretoria<br/>+27 (12) 440 8089<br/><a href="/muelmed">View Map</a></td>
<td><strong>Groenkloof Radiation Oncology</strong><br/>Life Groenkloof Hospital<br/>50 George Storrar Dr<br/>Groenkloof, Pretoria<br/>+27 (12) 460 4749<br/><a href="/groenkloof">View Map</a></td>
</table>
<br/>
<p class="" style="text-align: center;">© <a href="/">De Mûelenaere Oncology</a> 2018. </p>
</div></div> </div>
</div>
<div class="column3 footer_col3">
<div class="column_inner">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Go To Top
	============================================= -->
<div class="icon-angle-up" id="gotoTop"></div>
<!-- External JavaScripts
	============================================= -->
<script src="/includes/js/plugins.js" type="text/javascript"></script>
<!-- Footer Scripts
	============================================= -->
<script src="/includes/js/functions.js" type="text/javascript"></script>
</div></body>
</html>