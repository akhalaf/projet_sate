<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Page not found | Technomax</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.technomax.com.my/wp-content/themes/locus-t/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.technomax.com.my/xmlrpc.php" rel="pingback"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script charset="utf-8" src="https://www.technomax.com.my/wp-content/themes/locus-t/js/custom.js" type="text/javascript"></script>
<!-- start: google fonts -->
<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css"/>
<!-- end: google fonts -->
<link href="https://www.technomax.com.my/feed/" rel="alternate" title="Technomax » Feed" type="application/rss+xml"/>
<link href="https://www.technomax.com.my/comments/feed/" rel="alternate" title="Technomax » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/www.technomax.com.my\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.23"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.technomax.com.my/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.1.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.technomax.com.my/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.5.95&amp;ver=4.5.23" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link href="https://www.technomax.com.my/wp-content/plugins/wpclef/assets/dist/css/main.min.css?ver=2.2.9.3" id="wpclef-main-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.technomax.com.my/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.technomax.com.my/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.technomax.com.my/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?rev=4.5.95&amp;ver=4.5.23" type="text/javascript"></script>
<script src="https://www.technomax.com.my/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.5.95&amp;ver=4.5.23" type="text/javascript"></script>
<link href="https://www.technomax.com.my/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.technomax.com.my/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.technomax.com.my/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.5.23" name="generator"/>
</head>
<body class="error404">
<div id="wrapper">
<div id="header">
<div class="shadow main-header" id="container">
<div class="content">
<div class="left">
<a href="https://www.technomax.com.my"><img src="https://www.technomax.com.my/wp-content/themes/locus-t/images/logo.png"/></a>
</div>
<div class="right">
<div class="slogan">
<li>We do more</li>
</div>
</div>
</div>
</div>
<div class="menu-bg" id="container">
<div class="content">
<div><div class="menu-header"><ul class="menu" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-329" id="menu-item-329"><a href="https://www.technomax.com.my/">home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9" id="menu-item-9"><a href="https://www.technomax.com.my/about/">company</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-82" id="menu-item-82"><a href="#">products</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-21" id="menu-item-21"><a href="https://www.technomax.com.my/micro-super-watering-machine/">micro Super watering machine</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107" id="menu-item-107"><a href="https://www.technomax.com.my/car-washes/">car washes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106" id="menu-item-106"><a href="https://www.technomax.com.my/hotels-fb-beverage/">hotels &amp; F&amp;B beverage</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-105" id="menu-item-105"><a href="https://www.technomax.com.my/hospitals-and-healthcare-facilities/">Hospitals and healthcare facilities</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-104" id="menu-item-104"><a href="https://www.technomax.com.my/farm-livestock-and-agriculture/">farms (livestock and agriculture)</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20"><a href="https://www.technomax.com.my/research-development/">research &amp; development</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://www.technomax.com.my/customer-care/">customer care</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18" id="menu-item-18"><a href="https://www.technomax.com.my/contact-us/">contact us</a></li>
</ul></div></div>
</div>
</div>
</div><!-- #header -->
</div>
<div id="main">
<div class="full-float" id="container">
<div class="content">
<div class="content-area full-float">
<div class="fullwidth-block">
<div class="breadcrumbs">
</div>
<div class="page-title remove-bg">
<h1 class="entry-title">Error 404</h1>
</div>
<div class="entry-content">
<div class="full-float">
<p>Please click on link below and go back to our homepage.</p>
<a href="https://www.technomax.com.my/">&gt; Back to homepage</a>
<div class="custom-pagenavi">
</div>
</div><!-- #product -->
</div>
</div><!-- .page-right -->
</div><!-- .content-area -->
</div><!-- .content -->
</div><!-- #container -->
<div class="full-float" id="footer">
<div class="footer-wrapper">
<div class="content">
<p>All Rights Reserved by Technomax International Sdn.Bhd. 2013 </p>
</div>
</div><!-- .footer-wrapper -->
</div><!-- #footer -->
</div><!-- #main -->
<!-- #wrapper -->
<script src="https://www.technomax.com.my/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/www.technomax.com.my\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://www.technomax.com.my/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.1.1" type="text/javascript"></script>
<script src="https://www.technomax.com.my/wp-includes/js/wp-embed.min.js?ver=4.5.23" type="text/javascript"></script>
</body>
</html>
