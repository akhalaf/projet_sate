<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<link href="https://www.radioshahabat.net/xmlrpc.php" rel="pingback"/>
<title>RADIO SHAHABAT MUSLIM – JALIN UKHUWAH DI ATAS SUNNAH</title>
<script src="https://www.radioshahabat.net/wp-includes/js/comment-reply.min.js?ver=4.8.15" type="text/javascript"></script>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.radioshahabat.net/feed/" rel="alternate" title="RADIO SHAHABAT MUSLIM » Feed" type="application/rss+xml"/>
<link href="https://www.radioshahabat.net/comments/feed/" rel="alternate" title="RADIO SHAHABAT MUSLIM » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.radioshahabat.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.15"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/reset.css?ver=4.8.15" id="inkthemes_reset_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/960_24_col_responsive.css?ver=4.8.15" id="inkthemes_responsive_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/style.css?ver=4.8.15" id="inkthemes_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/superfish.css?ver=4.8.15" id="inkthemes_superfish-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/media.css?ver=4.8.15" id="inkthemes-media-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/animate.css?ver=4.8.15" id="inkthemes-animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.radioshahabat.net/wp-content/plugins/wp-downloadmanager/download-css.css?ver=1.68.2" id="wp-downloadmanager-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.radioshahabat.net/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/superfish.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/jquery.flexslider.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/jquery.tipsy.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/menu/jquery.meanmenu.2.0.min.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/menu/jquery.meanmenu.options.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/modernizr.custom.79639.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/custom.js?ver=4.8.15" type="text/javascript"></script>
<link href="https://www.radioshahabat.net/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.radioshahabat.net/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.radioshahabat.net/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.8.15" name="generator"/>
<link href="https://www.radioshahabat.net/" rel="canonical"/>
<link href="https://www.radioshahabat.net/" rel="shortlink"/>
<link href="https://www.radioshahabat.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.radioshahabat.net%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.radioshahabat.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.radioshahabat.net%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="http://www.radioshahabat.net/download/rss/" rel="alternate" title="RADIO SHAHABAT MUSLIM Downloads RSS Feed" type="application/rss+xml"/>
<link href="http://www.radioshahabat.net/wp-content/uploads/2017/09/483869_509608435751610_882883632_n-e1505370631831.jpg" rel="shortcut icon"/>
<link href="https://www.radioshahabat.net/wp-content/themes/colorway/css/green.css" media="all" rel="stylesheet" type="text/css"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-image: url("https://www.radioshahabat.net/wp-content/themes/colorway/images/body-bg.png"); background-position: left top; background-size: auto; background-repeat: repeat; background-attachment: scroll; }
</style>
<link href="https://www.radioshahabat.net/wp-content/uploads/2017/08/250274_790362911009493_4707797859623144076_n-1-150x150.jpg" rel="icon" sizes="32x32"/>
<link href="https://www.radioshahabat.net/wp-content/uploads/2017/08/250274_790362911009493_4707797859623144076_n-1-250x250.jpg" rel="icon" sizes="192x192"/>
<link href="https://www.radioshahabat.net/wp-content/uploads/2017/08/250274_790362911009493_4707797859623144076_n-1-250x250.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.radioshahabat.net/wp-content/uploads/2017/08/250274_790362911009493_4707797859623144076_n-1.jpg" name="msapplication-TileImage"/>
</head>
<body background="" class="home page-template page-template-template-home page-template-template-home-php page page-id-502 custom-background"> <!--Start Container Div-->
<div class="container_24 container">
<!--Start Header Grid-->
<div class="grid_24 header">
<div class="logo">
<a href="https://www.radioshahabat.net"><img alt="RADIO SHAHABAT MUSLIM logo" src="http://www.radioshahabat.net/wp-content/uploads/2017/09/483869_509608435751610_882883632_n-e1505371351500.jpg"/></a>
</div>
<!--Start MenuBar-->
<div class="menu-bar">
<div class="menu-menu-1-container" id="menu"><ul class="sf-menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-502 current_page_item menu-item-2798" id="menu-item-2798"><a href="https://www.radioshahabat.net/">Halaman Depan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2961" id="menu-item-2961"><a href="https://www.radioshahabat.net/artikel/">Artikel</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2800" id="menu-item-2800"><a href="https://www.radioshahabat.net/jadwal-radio/">Jadwal Radio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3241" id="menu-item-3241"><a href="https://www.radioshahabat.net/donwload/">Donwload</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2795" id="menu-item-2795"><a href="https://www.radioshahabat.net/about-radio-shahabat/">Tentang Radio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903" id="menu-item-2903"><a href="https://www.radioshahabat.net/donasi/">Donasi</a></li>
</ul></div>
<div class="clearfix"></div>
</div>
<!--End MenuBar-->
</div>
<div class="clear"></div>
<!--End Header Grid-->
<!--Start Slider-->
<div class="sl-slider-wrapper" id="slider">
<div class="sl-slider">
<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice1-scale="2" data-slice2-rotation="-25" data-slice2-scale="2">
<div class="sl-slide-inner">
<div class="salesdetails">
<h1><a href="http://www.radioshahabat.net">Gedung radio shahabat</a></h1>
<p>Alhamdulillah kini gedung radio Shahabat Muslim sudah menjadi hak milik
Semoga dengan menjadi hak milik tersebut memudah kan operasional radio Shahabat Muslim dalam rangka mengudarakan dakwah sunah di kota Tegal ini</p>
</div>
</div>
<div class="bg-img bg-img-1">
<a href="http://www.radioshahabat.net">
<img alt="Gedung radio shahabat" src="http://www.radioshahabat.net/wp-content/uploads/2017/09/Gedung-Radio.png"/>
</a>
</div>
</div>
<div class="sl-slide slide2" data-orientation="vertical" data-slice1-rotation="10" data-slice1-scale="1.5" data-slice2-rotation="-15" data-slice2-scale="1.5">
<div class="sl-slide-inner">
<div class="salesdetails">
<h1><a href="http://www.radioshahabat.net">Kapolsek Dukuhturi Polres Tegal</a></h1>
<p>kapolsek saat sedang mensosialisasikan himbauan Ramadhan di studio shahabat muslim 107.7 fm</p>
</div>
</div>
<div class="bg-img bg-img-2">
<a href="http://www.radioshahabat.net">
<img alt="Kapolsek Dukuhturi Polres Tegal" src="http://www.radioshahabat.net/wp-content/uploads/2017/09/kapoplosek-dukuhturi.png"/></a>
</div>
</div>
</div><!-- /sl-slider -->
<nav class="nav-arrows" id="nav-arrows">
<span class="nav-arrow-prev">Previous</span>
<span class="nav-arrow-next">Next</span>
</nav>
<nav class="nav-dots" id="nav-dots">
<span class="nav-dot-current"></span>
<span class="slide2"></span>
</nav>
</div>
<div class="clear"></div>
<!--End Slider-->
<!--Start Content Grid-->
<div class="grid_24 content">
<div class="content-wrapper">
<div class="content-info home">
<center>
<h2>
                                        RADIO SHAHABAT MUSLIM
jalin ukhuwah di atas sunah                                    </h2>
</center>
</div>
<div class="clear"></div>
<div id="content">
<div class="columns">
<div class="one_fourth animated" style="-webkit-animation-delay: .4s; -moz-animation-delay: .4s; -o-animation-delay: .4s; -ms-animation-delay: .4s;">
<a class="bigthumbs" href="http://www.radioshahabat.net">
<div class="img_thumb_feature"><span></span>
<img src="http://www.radioshahabat.net/wp-content/uploads/2017/09/Logo-Radio.png"/>
</div>
</a>
<h2><a href="http://www.radioshahabat.net">Radio Shahabat</a></h2>
<p>Radio da'wah ahlu sunnah waljamaah
selain sebagai tempat mengudaranya kajian kajian islam, di studio juga ada kajian rutin</p>
</div>
<div class="one_fourth middle animated" style="-webkit-animation-delay: .8s; -moz-animation-delay: .8s; -o-animation-delay: .8s; -ms-animation-delay: .8s;">
<a class="bigthumbs" href="http://www.radioshahabat.net/category/kajian-rutin/">
<div class="img_thumb_feature"><span></span>
<img src="http://www.radioshahabat.net/wp-content/uploads/2017/09/drawing.png"/>
</div>
</a>
<h2><a href="http://www.radioshahabat.net/category/kajian-rutin/">Kajian Rutin Radio</a></h2>
<p>Kajian Rutin bersama Ust Junaedi Abdillah hafizahullahuta'la
pemabahasan kitab aadaa waadawaa
dan sahih Bukhari
</p>
</div>
<div class="one_fourth animated" style="-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; -o-animation-delay: 1.2s; -ms-animation-delay: 1.2s;">
<a class="bigthumbs" href="http://www.radioshahabat.net/category/tablig-akbar">
<div class="img_thumb_feature"><span></span>
<img src="http://www.radioshahabat.net/wp-content/uploads/2017/09/Subhan-Bawasir.png"/>
</div>
</a>
<h2><a href="http://www.radioshahabat.net/category/tablig-akbar">Subhan Bawasier</a></h2>
<p>Tabligh Akbar
Kajian rtuin yang di selenggarakan yang di dukung radio shahabat dalam kelancaran pengajian tersebut</p>
</div>
<div class="one_fourth last animated" style="-webkit-animation-delay: 1.6s; -moz-animation-delay: 1.6s; -o-animation-delay: 1.6s; -ms-animation-delay: 1.6s;">
<a class="bigthumbs" href="#">
<div class="img_thumb_feature"><span></span>
<img src="http://www.radioshahabat.net/wp-content/uploads/2017/09/kajian-Abdullah-Zaen.png"/>
</div>
</a>
<h2><a href="#">Abdullah Zaen</a></h2>
<p>Kajian sesi ke dua di masjid al irsyad saat acara safari da'wah ust Abdullah Zaen</p>
</div>
</div>
</div>
<div class="clear"></div>
<div class="clear"></div>
<div class="testimonial_item_container">
<div class="testimonial_heading_container animated fading">
<h2>salam sapa shahabat radio</h2>
<p>saling sapa saling kenal saling mendoakan</p>
</div>
<div class="testimonial_item_content">
<div class="testimonial_item animated fading" style="-webkit-animation-delay: .4s; -moz-animation-delay: .4s; -o-animation-delay: .4s; -ms-animation-delay: .4s;">
<p>Kumpul-kumpul dan ngobrol seputar Islam dan kaum muslimin</p>
<div class="testimonial_item_inner">
<img src="https://www.radioshahabat.net/wp-content/themes/colorway/images/testimonial.jpg"/>
<div class="testimonial_name_wrapper">
<span>Ragil Putra Handoyo</span>
</div>
</div>
</div>
<div class="testimonial_item animated fading" style="-webkit-animation-delay: .8s; -moz-animation-delay: .8s; -o-animation-delay: .8s; -ms-animation-delay: .8s;">
<p>Radio Ahlus Sunnah Wal Jama'ah
semoga tetap eksis mengudara</p>
<div class="testimonial_item_inner">
<img src="https://www.radioshahabat.net/wp-content/themes/colorway/images/testimonial.jpg"/>
<div class="testimonial_name_wrapper">
<span>Nur Arifin</span>
</div>
</div>
</div>
<div class="testimonial_item animated fading" style="-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; -o-animation-delay: 1.2s; -ms-animation-delay: 1.2s;">
<p>Semoga radio shahabat bisa memberi kebutuhan ilmu syari</p>
<div class="testimonial_item_inner">
<img src="https://www.radioshahabat.net/wp-content/themes/colorway/images/testimonial.jpg"/>
<div class="testimonial_name_wrapper">
<span>Ahmad Sholeh</span>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<!--End Content Grid-->
</div>
<!--End Container Div-->
<!--Start Footer container-->
<div class="container_24 footer-container">
<div class="grid_24 footer">
</div>
<div class="clear"></div>
</div>
<!--End footer container-->
<!--Start footer navigation-->
<div class="container_24 footer-navi">
<div class="grid_24">
<div class="grid_10 alpha">
<div class="navigation">
<ul class="footer_des">
<li><a href="https://www.radioshahabat.net">RADIO SHAHABAT MUSLIM -
                            JALIN UKHUWAH DI ATAS SUNNAH                        </a></li>
</ul>
</div>
</div>
<div class="grid_4">
<div class="social-icons">
<a href="https://twitter.com/shahabatmuslim"><img alt="twitter" src="https://www.radioshahabat.net/wp-content/themes/colorway/images/twitter-icon.png" title="Twitter"/></a>
<a href="https://www.facebook.com/RadioShahabat/"><img alt="facebook" src="https://www.radioshahabat.net/wp-content/themes/colorway/images/facebook-icon.png" title="facebook"/></a>
</div>
</div>
<div class="grid_10 omega">
<div class="right-navi">
<p><a href="http://www.inkthemes.com" rel="nofollow">Colorway Wordpress Theme</a> by InkThemes.com</p>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<!--End Footer navigation-->
<div class="footer_space"></div>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/jquery.ba-cond.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/jquery.slitslider.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-content/themes/colorway/js/slider-init.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.radioshahabat.net/wp-includes/js/wp-embed.min.js?ver=4.8.15" type="text/javascript"></script>
</body></html>