<!DOCTYPE html>
<html lang="sr-Latn">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>SUBOTICA.com - Stranica nije nađena</title>
<link href="https://www.subotica.com/images/favicons/apple-touch-icon.png?v=1.01" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.subotica.com/images/favicons/favicon-32x32.png?v=1.01" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.subotica.com/images/favicons/favicon-16x16.png?v=1.01" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.subotica.com/images/favicons/site.webmanifest?v=1.01" rel="manifest"/>
<link color="#50b6fe" href="https://www.subotica.com/images/favicons/safari-pinned-tab.svg?v=1.01" rel="mask-icon"/>
<link href="https://www.subotica.com/images/favicons/favicon.ico?v=1.01" rel="shortcut icon"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="https://www.subotica.com/images/favicons/browserconfig.xml?v=1.01" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<!-- Fonts -->
<link href="//fonts.gstatic.com" rel="dns-prefetch"/>
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"/>
<!-- Styles -->
<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }

            .flex-column {
                align-items: center;
                display: flex;
                justify-content: center;
                flex-direction: column;
            }
            .site-image {
                padding: 0px 15px;
            }
            .site-image img {
                max-width: 100%;
            }
        </style>
</head>
<body>
<div class="flex-column position-ref full-height">
<div class="site-image">
<img alt="SUBOTICA.com" src="https://www.subotica.com/images/sucom_logo.png"/>
</div>
<div class="error-details flex-center">
<div class="code">
                    404                </div>
<div class="message" style="padding: 10px;">
                    Stranica nije nađena                </div>
</div>
</div>
<!-- Google analytics - START -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1760981-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-1760981-1');
</script>
<!-- Google analytics - END -->
</body>
</html>
