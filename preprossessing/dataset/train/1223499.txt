<!DOCTYPE html>
<!--[if lt IE 7 ]>      <html lang="en" class="default" class="no-js ie6"> <![endif]--><!--[if IE 7 ]>         <html lang="en" class="default" class="no-js ie7"> <![endif]--><!--[if IE 8 ]>         <html lang="en" class="default" class="no-js ie8"> <![endif]--><!--[if IE 9 ]>         <html lang="en" class="default" class="no-js ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html class="no-js default" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://rehab.org/wp-content/themes/overit-master/css/reset.css?v=?v=20190614-1655" rel="stylesheet"/>
<link href="https://rehab.org/wp-content/themes/overit-master/css/style.css?v=?v=20190614-1655" rel="stylesheet"/>
<link href="https://rehab.org/wp-content/themes/overit-master/css/nivo-slider.css?v=?v=20190614-1655" rel="stylesheet"/>
<!--[if IE 7 ]>  
	<link rel="stylesheet" href="https://rehab.org/wp-content/themes/overit-master/css/ie7.css?v=?v=20190614-1655">
	<![endif]-->
<!--[if lt IE 9 ]> 
	<link rel="stylesheet" href="https://rehab.org/wp-content/themes/overit-master/css/ie.css">
	<![endif]-->
<script src="https://rehab.org/wp-content/themes/overit-master/js/libs/modernizr-2.0.6.min.js"></script>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found - Rehabilitation Support Services</title>
<meta content="noindex, follow" name="robots"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://rehab.org/#website","url":"https://rehab.org/","name":"Rehabilitation Support Services","description":"","potentialAction":[{"@type":"SearchAction","target":"https://rehab.org/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.14.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.14.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-31187471-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-31187471-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/rehab.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://rehab.org/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rehab.org/wp-content/plugins/theme-my-login/assets/styles/theme-my-login.min.css?ver=7.1.2" id="theme-my-login-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rehab.org/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.min.css?ver=7.14.0" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/rehab.org","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://rehab.org/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.14.0" type="text/javascript"></script>
<script id="jquery-core-js" src="https://rehab.org/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://rehab.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<link href="https://rehab.org/wp-json/" rel="https://api.w.org/"/><link href="https://rehab.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://rehab.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://rehab.org/wp-content/plugins/wp-page-numbers/classic/wp-page-numbers.css" media="screen" rel="stylesheet" type="text/css"/>
</head>
<body id="default">
<div id="outer">
<div id="inner">
<div id="inner2"></div>
</div>
</div>
<div class="clearfix" id="container">
<div id="backgroundImage">
<!-- HERADER -->
<header id="header">
<div id="headerContent">
<div id="siteTitle"><a class="ir" href="https://rehab.org" title="Rehabilitation Support Services">Rehabilitation Support Services (RSS)</a></div>
<nav id="mainNav">
<ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://rehab.org/rehabilitation-services/" title="Rehabilitation Services">Rehabilitation Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34" id="menu-item-34"><a href="https://rehab.org/counties-we-serve/" title="Counties We Serve">Counties We Serve</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1302" id="menu-item-1302"><a href="https://rehab.org/about/careers/">Career Opportunities</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36" id="menu-item-36"><a href="https://rehab.org/contact/" rel="nofollow">Contact</a></li>
</ul> </nav>
<form action="https://rehab.org" method="get" onsubmit="return s.value!=''">
<div id="topSearch">
<span class="inputContainer labelGroup"><label for="searchInput">Search...</label><input id="searchInput" name="s" type="text" value=""/></span><input class="ir searchSubmit" type="submit" value="search"/>
</div>
</form>
</div>
</header>
<!-- CENTER AREA -->
<div id="centralArea">
<!-- Slideshow overlay -->
<a id="homeSlideShowOverlay"></a>
<!-- Coutines we serve button -->
<a class="ir countiesButton" href="https://rehab.org/counties-we-serve/">Counties We Serve</a>
<div id="countyMapContainer">
<img id="countyMapBg" src="https://rehab.org/wp-content/themes/overit-master/images/bg-map-container.png" style="z-index:0; position:absolute"/>
<div class="countyMap"> <!-- static map background -->
<div class="counties"> <!-- container with sprite background-->
<img alt="" border="0" height="181" id="mapImage" src="https://rehab.org/wp-content/themes/overit-master/images/map-image-map-ref.png" style="display:block" usemap="#countyMap" width="206"/> <!-- transparent image for map -->
<map name="countyMap">
<area alt="Tioga" class="county_m county1" coords="20,62,2,60,2,50,2,45,9,43,15,43,15,36,24,46" href="https://rehab.org/county/tioga-county/" rel="nofollow" shape="poly" title="Tioga"/>
<area alt="Tioga" class="county_m county1" coords="0,38,37,88" href="https://rehab.org/county/tioga-county/" rel="nofollow" shape="rect" title="Tioga"/>
<area alt="Chenango" class="county_m county2" coords="74,67,48,62,43,22,81,20,81,27,79,35,74,38,77,46,75,58" href="https://rehab.org/county/chenango-county/" rel="nofollow" shape="poly" title="Chenango"/>
<area alt="Otsego" class="county_m county3" coords="80,52,82,48,78,41,82,36,85,24,88,14,94,9,106,17,112,9,121,14,122,23,119,37,108,39,100,43,94,50" href="https://rehab.org/county/otsego-county/" rel="nofollow" shape="poly" title="Otsego"/>
<area alt="Schoharie" class="county_m county4" coords="122,38,136,50,147,49,150,39,150,31,155,24,144,22,142,19,128,13,129,23,125,33" href="https://rehab.org/county/schoharie/" rel="nofollow" shape="poly" title="Schoharie"/>
<area alt="Schenectady" class="county_m county5" coords="151,21,148,12,161,2,171,3,177,13,157,18" href="https://rehab.org/county/schenectady/" rel="nofollow" shape="poly" title="Schenectady"/>
<area alt="Albany" class="county_m county6" coords="152,46,152,39,153,30,157,18,188,13,180,42" href="https://rehab.org/county/albany-county/" rel="nofollow" shape="poly" title="Albany"/>
<area alt="Delaware" class="county_m county7" coords="95,96,87,89,79,80,75,74,76,59,88,54,98,47,111,41,122,42,132,51,140,54,134,69" href="https://rehab.org/county/delaware-county/" rel="nofollow" shape="poly" title="Delaware"/>
<area alt="Sullivan" class="county_m county8" coords="96,97,115,85,139,97,133,102,141,114,137,126,124,126,120,126,118,135,105,127,99,119,101,111" href="https://rehab.org/county/sullivan-county/" rel="nofollow" shape="poly" title="Sullivan"/>
<area alt="Ulster" class="county_m county9" coords="119,81,136,70,145,74,164,75,164,68,173,71,170,83,172,104,171,118,160,118,151,115,142,116,132,104,138,96" href="https://rehab.org/county/ulster-county/" rel="nofollow" shape="poly" title="Ulster"/>
<area alt="Orange" class="county_m county10" coords="120,138,118,127,134,127,144,117,151,115,159,117,166,118,171,120,168,128,172,135,154,156" href="https://rehab.org/county/orange-county/" rel="nofollow" shape="poly" title="Orange"/>
<area alt="Dutchess" class="county_m county11" coords="170,130,174,116,172,96,171,77,184,81,200,85,199,122" href="https://rehab.org/county/dutchess-county/" rel="nofollow" shape="poly" title="Dutchess"/>
<area alt="Westchester" class="county_m county12" coords="187,180,177,176,177,165,177,156,174,149,172,142,198,139,205,150,187,160,195,168" href="https://rehab.org/county/westchester-county/" rel="nofollow" shape="poly" title="Westchester"/>
<area alt="Image Map" class="county_m county13" coords="204,179,206,181" href="https://www.image-maps.com/index.php?aff=mapped_users_6201201161529245" rel="nofollow" shape="rect" title="Image Map"/>
<area alt="Rensselaer" class="county_m county14" coords="185,5,189,2,201,2,211,2,210,17,206,38,181,42,188,18" href="https://rehab.org/county/rensselaer-county/" rel="nofollow" shape="poly" title="Rensselaer"/>
</map>
</div>
</div>
</div>
<div class="scrollable" id="homeSlideShow">
<img alt="Rehabilitation Support Services" src="https://rehab.org/wp-content/uploads/2012/04/1_0005_Layer-6.jpg"/> </div>
</div>
<div class="clearfix" id="main" role="main">
<div class="clearfix" id="wrapper">
<div id="content">
<article class="entryContent">
<header class="entryHeader">
<h1 id="category">
<span class="ir">404 Error</span>
</h1>
</header>
<section>
<h2>Not Found</h2>
<p>Sorry, we couldn't find what you were looking for.</p>
<form action="https://rehab.org" method="get" onsubmit="return s.value!=''">
<div>
<label>Try Searching:</label><input name="s" type="text" value=""/><input class="searchSubmit" type="submit" value="Search"/>
</div>
</form>
</section>
</article>
</div>
<aside id="sidebar">
<div class="address">
<a href="http://maps.google.com/maps?q=5172+Western+Turnpike+Altamont%2C+NY+12009" target="_blank" title="See a Map">
<img class="map" src="http://maps.googleapis.com/maps/api/staticmap?size=200x143&amp;zoom=15&amp;markers=icon:http://dev.overit.com/RSS/marker/marker_small.png%7C5172+Western+Turnpike+Altamont%2C+NY+12009&amp;sensor=false"/>
</a>
<h3>Rehabilitation Support Services, Inc.</h3><p>5172 Western Turnpike</p><p>Altamont, NY 12009</p><p>Phone: (518) 464-1511 </p><p>Fax: (518) 464-9198 (Fax)</p> </div>
<a class="ir" href="https://rehab.org/contact/" id="envelope" rel="nofollow" title="Contact Us">Need help? Contact us now.</a>
</aside>
</div>
</div>
<footer id="footer">
<div id="footerContent">
<a class="footerLink" href="https://rehab.org/employee-login/" id="employeeLoginLink" rel="nofollow" title="Employee Login">Employee Login</a>
<a class="footerLink" href="https://rehab.org/about/" id="aboutRSSLink" rel="nofollow" title="About RSS">About RSS</a>
<a class="footerLink" href="https://www.facebook.com/pages/Rehabilitation-Support-Services-Inc/188795451154740" id="facebookLink" rel="nofollow" target="_blank" title="Connect with us on Facebook!">Facebook</a>
<a class="footerLink" href="https://twitter.com/#!/rss_" id="twitterLink" rel="nofollow" target="_blank" title="Connect with us on Twitter!">Twitter</a>
<p id="addressBottom"><span class="name">Rehabilitation Support Services, Inc.</span><span class="address">5172 Western Turnpike | Altamont, NY 12009</span></p>
<dl id="contactInfoBottom">
<!-- <dd><strong>P:</strong> 518.496.1233</dd> <dd><strong>F:</strong> 518.496.1233</dd> -->
<dd><a href="https://rehab.org/contact/" rel="nofollow">Contact</a></dd> <dd><a href="https://rehab.org/sitemap/">Sitemap</a></dd>
</dl>
<span id="copyright">© 2021 RSS - <a href="https://rehab.org" title="Rehabilitation Support Services">Rehabilitation Support Services</a></span>
</div>
</footer>
</div>
</div> <!--! end of #container -->
<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script>
<script>!window.jQuery && document.write('<script src="https://rehab.org/wp-content/themes/overit-master/js/libs/jquery-1.6.2.min.js"><\/script>')</script>
<script src="https://rehab.org/wp-content/themes/overit-master/js/libs/modernizr-2.0.6.min.js?v=?v=20190614-1655"></script>
<script src="https://rehab.org/wp-content/themes/overit-master/js/script.js?v=?v=20190614-1655"></script>
<script src="https://rehab.org/wp-content/themes/overit-master/js/plugins.js?v=?v=20190614-1655"></script>
<script src="https://rehab.org/wp-content/themes/overit-master/js/jquery.hoverintent.js?v=?v=20190614-1655"></script>
<script src="https://rehab.org/wp-content/themes/overit-master/js/nivo-slider/jquery.nivo.slider.pack.js?v=?v=20190614-1655"></script>
<!--[if lt IE 7 ]>
  <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
<script type="text/javascript">
    $(function(){
       initMaps(false);//initMaps(true);
    });
  </script>
<script id="theme-my-login-js-extra" type="text/javascript">
/* <![CDATA[ */
var themeMyLogin = {"action":"","errors":[]};
/* ]]> */
</script>
<script id="theme-my-login-js" src="https://rehab.org/wp-content/plugins/theme-my-login/assets/scripts/theme-my-login.min.js?ver=7.1.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://rehab.org/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>
