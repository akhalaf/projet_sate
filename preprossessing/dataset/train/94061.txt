<!DOCTYPE html>
<html><head><meta charset="utf-8"/><title>Albion Online 2D Database — Meta, Market Prices, Craft Calculator</title><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/><meta content="Albion Online 2D Database — Current Meta, Items, Mobs, Destiny Calculator, Craft Calculator, Fame Calculator and more" name="description"/><meta content="Lizard Brain UG" name="author"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script src="/public/minton-dark/js/jquery.min.js"></script><link href="/public/minton/images/favicon.ico" rel="shortcut icon"/><link href="/public/minton-dark/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/css/icons.css" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/plugins/ion-rangeslider/ion.rangeSlider.css" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/plugins/ion-rangeslider/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/css/style.css" rel="stylesheet" type="text/css"/><link href="/public/vendor/bootstrap-treeview/dist/bootstrap-treeview.min.css" rel="stylesheet" type="text/css"/><link href="/public/css/style-dark.css?v=edd99bc257714ae5ffd7d208be99f382cd0de45c" rel="stylesheet" type="text/css"/><link href="/public/minton-dark/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-81229459-7"></script><script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-81229459-7');</script><script>LANG = 'en';
</script></head><body ng-app="ao2d"><header id="topnav"><div class="topbar-main"><script>(function () {
    var cx = '001538945727927768667:guehywbj40c';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();

window.onload = function () {
    document.getElementById('gsc-i-id1').placeholder = 'Search for items, mobs, expeditions and more';
};
</script><gcse:search></gcse:search></div><nav class="navbar-custom"><div class="container-fluid"><div class="menu-extras topbar-custom"><ul class="list-inline float-right mb-0"><li class="menu-item list-inline-item"><a class="navbar-toggle nav-link"><div class="lines"><span></span><span></span><span></span></div></a></li><li class="list-inline-item dropdown notification-list"><a aria-expanded="false" aria-haspopup="false" class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"><br/></a></li></ul></div><div id="navigation"><ul class="navigation-menu"><li><a href="/en"><i class="ti-home"></i></a></li><li><a href="/en/meta">Meta</a></li><li class="has-submenu"><a href="#">Database</a><ul class="submenu"><li><a href="/en/item">Items</a></li><li><a href="/en/mob">Mobs</a></li><li><a href="/en/building">Buildings</a></li><li><a href="/en/labourer">Labourers</a></li><li><a href="/en/island">Islands</a></li><li><a href="/en/expedition">Expeditions</a></li><li><a href="/en/equipment/mainhand">Unlocked Equipment</a></li><li><a href="/en/spells/mainhand">Spells</a></li></ul></li><li class="has-submenu"><a href="#">Tools</a><ul class="submenu"><li><a href="/en/destinycalculator">Destiny Calculator</a></li><li><a href="/en/craftcalculator">Craft Calculator</a></li><li><a href="/en/tools/farming-profit-calculator">Farming Profit Calculator</a></li><li class="li has-submenu"><a href="#">Refining Profit Calculator</a><ul class="submenu"><li><a href="/en/tools/refining-profit-calculator/hide">Hide</a></li><li><a href="/en/tools/refining-profit-calculator/ore">Ore</a></li><li><a href="/en/tools/refining-profit-calculator/wood">Wood</a></li><li><a href="/en/tools/refining-profit-calculator/fiber">Fiber</a></li><li><a href="/en/tools/refining-profit-calculator/rock">Rock</a></li></ul></li><li><a href="/en/devtracker">Dev Tracker</a></li><li><a href="/en/discordbot">Discord Killbot</a></li></ul></li><li class="has-submenu"><a href="#">Guides</a><ul class="submenu"><li><a href="/en/guides/food-buffs">Food Buffs Table</a></li><li><a href="/en/guides/fish-buffs">Fish Buffs Table</a></li><li><a href="/en/guides/potion-buffs">Potion Buffs Table</a></li><li><a href="/en/guides/fastest-mounts">Mounts Speed Table</a></li><li><a href="/en/guides/breeding">Animal Breeding Table</a></li><li><a href="/en/guides/farming">Crops Farming and Harvest Table</a></li><li><a href="/en/guides/focusfire">Focus Fire Protection Table</a></li><li><a href="/en/guides/hints">Game Hints</a></li><li><a href="/en/guides/achievements">Achievements</a></li><li><a href="/en/guides/partyfinder">Party Finder</a></li></ul></li><li class="has-submenu"><a href="#">Scoreboard</a><ul class="submenu"><li><a href="/en/scoreboard/players">Players</a></li><li><a href="/en/scoreboard/gvg">GvG</a></li><li><a href="/en/scoreboard/battles">Battles</a></li></ul></li><li><a href="/en/map">Map</a></li><li><a href="http://exitl.ag/albiononline2d" style="color: #ff5722;" target="_blank">Reduce ping (Sponsored)</a></li></ul></div></div></nav></header><div class="wrapper"><div class="container-fluid pb-5"><div class="row"><div class="col-xl-12"><div class="page-title-box"></div></div></div><div class="row"><div class="col-xl-12"><h1 class="text-center">Albion Online 2D — Database and Tools</h1><br/></div></div><div class="row"><div class="col-xl-10"><h4>Albion Online on Twitch</h4><div class="carousel slide" data-ride="carousel" id="twitch-videos-carousel"><div id="_index_twitch"><div class="spinningloader loader"><span></span><span></span><span></span></div><script>(function () {
  var name = "_index_twitch";
  $.get('/' + LANG + '/_partials/' + name, function (response) {
      $('#' + name).replaceWith(response);
  });
})();
</script></div></div><div class="row justify-content-center"><div class="col-lg-auto"><div class="card m-b-20"><div class="card-header text-center mmm">Affiliate Partner</div><div class="card-body mmm"><a href="http://exitl.ag/albiononline2d" target="_blank"><img class="img-fluid" src="https://albiononline2d.ams3.cdn.digitaloceanspaces.com/_/exitlag_728x90.png"/></a></div></div></div></div><div class="row"><div class="col-xl-6"><div class="card m-b-20"><a href="/en/scoreboard/players"><h6 class="card-header text-center">Top Killers, last 7 Days</h6></a><div class="card-body"><table class="table table-striped"><thead><tr><th>#</th><th>Name</th><th>Kills</th></tr></thead><tbody><tr><td>1</td><td><a href="/en/scoreboard/players/EnRoi8RMQj-dztn7yfiw8A">NinShika</a></td><td>117</td></tr><tr><td>2</td><td><a href="/en/scoreboard/players/ae9ZL9kvRZyrXAsHrfdBEw">lIlIlIII</a></td><td>63</td></tr><tr><td>3</td><td><a href="/en/scoreboard/players/uo1kCNF-Rlm5bJ6CyIIkPg">Melethril</a></td><td>51</td></tr><tr><td>4</td><td><a href="/en/scoreboard/players/5KJ6LbSsSDeqpDsdmp3pVQ">Drogmor</a></td><td>47</td></tr><tr><td>5</td><td><a href="/en/scoreboard/players/oyTZ_lUEQiiWyqEAEZeKxg">Vibrator</a></td><td>46</td></tr></tbody></table></div></div></div><div class="col-xl-6"><div class="card m-b-20"><a href="/en/scoreboard/players"><h6 class="card-header text-center">Top Victims, last 7 Days</h6></a><div class="card-body"><table class="table table-striped"><thead><tr><th>#</th><th>Name</th><th>Deaths</th></tr></thead><tbody><tr><td>1</td><td><a href="/en/scoreboard/players/NpIkLAurTu6m5Eo4jHK8Qw">BiggyX</a></td><td>15</td></tr><tr><td>2</td><td><a href="/en/scoreboard/players/bvAN-WuiTWyO99HCstfA2A">FrigoEnPanne</a></td><td>14</td></tr><tr><td>3</td><td><a href="/en/scoreboard/players/rJV67KcUSjaftCFfTpqqbQ">Matuca</a></td><td>14</td></tr><tr><td>4</td><td><a href="/en/scoreboard/players/EnRoi8RMQj-dztn7yfiw8A">NinShika</a></td><td>13</td></tr><tr><td>5</td><td><a href="/en/scoreboard/players/iZHi_8gaScKxRFgqXuUU4w">rRellikr</a></td><td>13</td></tr></tbody></table></div></div></div></div><div class="row justify-content-center"><div class="col-lg-auto"><div class="card m-b-20"><div class="card-header text-center mmm">Advertisement</div><div class="card-body mmm"><div class="mmm-container-728"><ins data-a4g-block="" data-a4g-blockcampaign="" data-a4g-zone="67152"></ins><script type="text/javascript">(function (cdnPath, charset) {var el = document.createElement('SCRIPT'),body = document.body,asyncAjsSrc = cdnPath + '/async-ajs.min.js',isAsyncPresent = (function (scripts, asyncAjsSrc) {for (var i = 0; i < scripts.length; i++) {if (scripts[i].src === asyncAjsSrc) {return true;}}return false;} (document.getElementsByTagName('SCRIPT'), asyncAjsSrc));if (!isAsyncPresent) {el.type = 'text/javascript';el.async = true;el.src = asyncAjsSrc;if (charset) {el.setAttribute('data-a4g-charset', charset);}body.appendChild(el);}} (location.protocol === 'https:' ? 'https://cdn.ad4game.com' : 'http://cdn.ad4game.com', ''));</script></div></div></div></div></div><div class="row"><div class="col-xl-12"><div class="card m-b-20"><a href="/en/scoreboard/gvg"><h6 class="card-header text-center">Upcoming GvG Matches</h6></a><div class="card-body"><div id="_index_upcoming-gvg"><div class="spinningloader loader"><span></span><span></span><span></span></div><script>(function () {
  var name = "_index_upcoming-gvg";
  $.get('/' + LANG + '/_partials/' + name, function (response) {
      $('#' + name).replaceWith(response);
  });
})();
</script></div></div></div></div></div><div class="row"><div class="col-xl-6"><div class="card m-b-20"><h6 class="card-header text-center">Players in PvP, last 7 days, by day*</h6><div class="card-body"><canvas height="200" id="recent-online-day"></canvas></div><div class="text-right" style="margin-right: 25px;"><p class="small"><em>* Based on the recent PvP activities only. Doesn't include PvE, crafting, gathering etc.</em></p></div></div></div><div class="col-xl-6"><div class="card m-b-20"><h6 class="card-header text-center">Players in PvP, last 24 hours, by hour*</h6><div class="card-body"><canvas height="200" id="recent-online-hour"></canvas></div><div class="text-right" style="margin-right: 25px;"><p class="small"><em>* Based on the recent PvP activities only. Doesn't include PvE, crafting, gathering etc.</em></p></div></div></div></div><div class="row justify-content-center"><div class="col-lg-auto"><div class="card m-b-20"><div class="card-header text-center mmm">Advertisement</div><div class="card-body mmm"><div class="mmm-container-728"><ins data-a4g-block="" data-a4g-blockcampaign="" data-a4g-zone="67165"></ins><script type="text/javascript">(function (cdnPath, charset) {var el = document.createElement('SCRIPT'),body = document.body,asyncAjsSrc = cdnPath + '/async-ajs.min.js',isAsyncPresent = (function (scripts, asyncAjsSrc) {for (var i = 0; i < scripts.length; i++) {if (scripts[i].src === asyncAjsSrc) {return true;}}return false;} (document.getElementsByTagName('SCRIPT'), asyncAjsSrc));if (!isAsyncPresent) {el.type = 'text/javascript';el.async = true;el.src = asyncAjsSrc;if (charset) {el.setAttribute('data-a4g-charset', charset);}body.appendChild(el);}} (location.protocol === 'https:' ? 'https://cdn.ad4game.com' : 'http://cdn.ad4game.com', ''));</script></div></div></div></div></div><div class="row"><div class="col-xl-12"><div class="card m-b-20"><h6 class="card-header text-center">Players in PvP, last 7 days, by hour*</h6><div class="card-body"><canvas height="200" id="recent-online-week"></canvas></div><div class="text-right" style="margin-right: 25px;"><p class="small"><em>* Based on the recent PvP activities only. Doesn't include PvE, crafting, gathering etc.</em></p></div></div></div></div></div><div class="col-xl-2"><div class="card m-b-20"><a href="/en/devtracker"><h6 class="card-header text-center">Dev Tracker</h6></a><div class="card-body"><div id="_index_posts"><div class="spinningloader loader"><span></span><span></span><span></span></div><script>(function () {
  var name = "_index_posts";
  $.get('/' + LANG + '/_partials/' + name, function (response) {
      $('#' + name).replaceWith(response);
  });
})();
</script></div></div></div></div></div></div></div><div class="div pb-4 d-block d-sm-none"></div><footer class="footer"><div class="col-md-12"><div class="container"><div class="row"><div class="col-xl-12"><ul class="list-inline d-flex flex-row flex-wrap justify-content-center"><a class="text-dark" href="https://discord.gg/albiononline" target="_blank">Official Albion Online Discord</a>  </ul></div></div><div class="row"><div class="col-xl-12"><ul class="list-inline d-flex flex-row flex-wrap justify-content-center"><li><a class="text-dark" href="/en">English</a>  </li><li><a class="text-dark" href="/de">Deutsch</a>  </li><li><a class="text-dark" href="/pl">Polski</a>  </li><li><a class="text-dark" href="/fr">Français</a>  </li><li><a class="text-dark" href="/ru">Русский</a>  </li><li></li><a class="text-dark" href="/pt">Português</a>  <li><a class="text-dark" href="/es">Español</a></li></ul></div></div><div class="row"><div class="col-12 text-center"><small>Current data version: <a href="https://albiononline.com/en/changelog/RISE%20OF%20AVALON%20PATCH%2011" target="_blank">Rise of Avalon Patch 11 - Ver. 1.17.406 / REV 181307 - 10 December 2020</a> | Server time, UTC: <span id="server-time"></span></small></div></div></div></div></footer><script src="//cdn.jsdelivr.net/npm/vanilla-lazyload@11.0.3/dist/lazyload.min.js"></script><script src="/public/minton-dark/js/popper.min.js"></script><script src="/public/minton-dark/js/bootstrap.min.js"></script><script src="/public/minton-dark/js/waves.js"></script><script src="/public/minton-dark/js/jquery.slimscroll.js"></script><script src="/public/minton-dark/js/jquery.scrollTo.min.js"></script><script src="/public/minton-dark/plugins/waypoints/lib/jquery.waypoints.min.js"></script><script src="/public/minton-dark/plugins/ion-rangeslider/ion.rangeSlider.min.js"></script><script src="/public/minton-dark/plugins/raphael/raphael-min.js"></script><script src="/public/minton-dark/plugins/select2/js/select2.min.js"></script><script src="/public/minton-dark/js/jquery.core.js"></script><script src="/public/minton-dark/js/jquery.app.js"></script><script src="/public/vendor/bootstrap-treeview/dist/bootstrap-treeview.min.js"></script><script src="/public/vendor/lodash/dist/lodash.min.js"></script><script src="/public/minton-dark/js/modernizr.min.js"></script><script src="/public/vendor/angular/angular.min.js"></script><script src="/public/vendor/moment.min.js"></script><script src="/public/minton-dark/plugins/datatables/jquery.dataTables.min.js"></script><script src="/public/minton-dark/plugins/datatables/dataTables.bootstrap4.min.js"></script><script type="text/javascript">$('.select2').select2({
    minimumInputLength: 2
});
new LazyLoad();

var app = angular.module('ao2d', []);
app.filter('formatNumber', function () {
    return function (input) {
        return Intl.NumberFormat().format(input);
    }
});

setInterval(function () {
    $('#server-time').html(moment.utc().format('DD.MM.YYYY HH:mm:ss'));
}, 1000);
</script><script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script><script>function drawOnlineChart(period) {
    $.get('/dashboard/api/recent-online/' + period, function (response) {
        var labels = _.map(response, 'date');
        var data = _.map(response, 'count');
        var ctx = document.getElementById('recent-online-' + period).getContext('2d');
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    backgroundColor: 'rgba(3, 156, 253, 0.1)',
                    borderColor: '#039cfd',
                    data: data
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        display: false
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            beginAtZero: true
                        },
                        display: true
                    }]
                },
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                }
            }
        });
    });
}

drawOnlineChart('hour');
drawOnlineChart('day');
drawOnlineChart('week');</script></body></html>