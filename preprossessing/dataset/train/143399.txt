<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="nositelinkssearchbox" name="google"/>
<meta content="/images/fbrfg/browserconfig.xml" name="msapplication-config"/>
<meta content="#2d89ef" name="msapplication-tilecolor"/>
<meta content="/images/fbrfg/mstile-144x144.png" name="msapplication-tileimage"/>
<meta content="noindex" name="robots"/>
<meta content="#ffffff" name="theme-color"/>
<title>
Google Apps セットアップ&amp;サポート</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="//www.appsupport.jp" rel="dns-prefetch"/>
<link href="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" rel="dns-prefetch"/>
<link href="//www.google-analytics.com" rel="dns-prefetch"/>
<link href="https://www.appsupport.jp/feed/" rel="alternate" title="Google Apps セットアップ&amp;サポート RSS Feed" type="application/rss+xml"/>
<link href="https://www.appsupport.jp/xmlrpc.php" rel="pingback"/>
<link href="https://www.appsupport.jp/googleapps/docs/%09/" rel="canonical"/>
<link href="https://www.appsupport.jp/wp-json/" rel="https://api.w.org/"/>
<link href="/images/fbrfg/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/images/fbrfg/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/images/fbrfg/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/images/fbrfg/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/images/fbrfg/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/images/fbrfg/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/images/fbrfg/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/images/fbrfg/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/images/fbrfg/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/images/fbrfg/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/fbrfg/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/images/fbrfg/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/images/fbrfg/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/images/fbrfg/manifest.json" rel="manifest"/>
<link href="/images/fbrfg/favicon.ico" rel="shortcut icon"/>
<link href="https://www.appsupport.jp/form/commons/mailform.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.appsupport.jp/wp-content/themes/appsupport/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.appsupport.jp/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appsupport.jp/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.appsupport.jp/wp-content/plugins/meteor-slides/css/meteor-slides.css?ver=1.0" id="meteor-slides-css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">/*<![CDATA[ */
img.wp-smiley,
img.emoji {
display: inline !important;
border: none !important;
box-shadow: none !important;
height: 1em !important;
width: 1em !important;
margin: 0 .07em !important;
vertical-align: -0.1em !important;
background: none !important;
padding: 0 !important;
}
.broken_link, a.broken_link {
text-decoration: line-through;
}
/* ]]>*/</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://www.appsupport.jp/js/qaTab.js" type="text/javascript"></script>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<script src="https://www.appsupport.jp/wp-content/plugins/head-cleaner/includes/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.appsupport.jp/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="jquery-cycle-js" src="https://www.appsupport.jp/wp-content/plugins/meteor-slides/js/jquery.cycle.all.js?ver=5.6" type="text/javascript"></script>
<script id="jquery-metadata-js" src="https://www.appsupport.jp/wp-content/plugins/meteor-slides/js/jquery.metadata.v2.js?ver=5.6" type="text/javascript"></script>
<script id="jquery-touchwipe-js" src="https://www.appsupport.jp/wp-content/plugins/meteor-slides/js/jquery.touchwipe.1.1.1.js?ver=5.6" type="text/javascript"></script>
<script id="meteorslides-script-js" src="https://www.appsupport.jp/wp-content/plugins/meteor-slides/js/slideshow.js?ver=5.6" type="text/javascript"></script>
<script type="text/javascript">//<![CDATA[
{"@context":"https:\/\/schema.org","@graph":[{"@type":"WebSite","@id":"https:\/\/www.appsupport.jp\/#website","url":"https:\/\/www.appsupport.jp\/","name":"Google Apps \u30bb\u30c3\u30c8\u30a2\u30c3\u30d7&\u30b5\u30dd\u30fc\u30c8","description":"Google Apps \u306e\u5c0e\u5165\u7a93\u53e3\u3002\u5404\u7a2e\u30b5\u30dd\u30fc\u30c8\u3084\u30aa\u30f3\u30e9\u30a4\u30f3\u30de\u30cb\u30e5\u30a2\u30eb\u3082\u5145\u5b9f\u3002","publisher":{"@id":"https:\/\/www.appsupport.jp\/#organization"}},{"@type":"Organization","@id":"https:\/\/www.appsupport.jp\/#organization","name":"Google Apps \u30bb\u30c3\u30c8\u30a2\u30c3\u30d7&\u30b5\u30dd\u30fc\u30c8","url":"https:\/\/www.appsupport.jp\/"},{"@type":"BreadcrumbList","@id":"https:\/\/www.appsupport.jp\/googleapps\/docs\/%09%0A\/#breadcrumblist","itemListElement":[{"@type":"ListItem","@id":"https:\/\/www.appsupport.jp\/#listItem","position":1,"item":{"@type":"WebPage","@id":"https:\/\/www.appsupport.jp\/#item","name":"\u30db\u30fc\u30e0","description":"Google Apps\u306e\u304a\u7533\u3057\u8fbc\u307f\u7a93\u53e3\u3002Google Apps\u306e\u30bb\u30c3\u30c8\u30a2\u30c3\u30d7\u3084\u904b\u7528\u3092\u7121\u6599\u3067\u30b5\u30dd\u30fc\u30c8\u3002Google Apps \u3092\u30d3\u30b8\u30cd\u30b9\u3067100%\u6d3b\u7528\u3067\u304d\u308b\u3088\u3046\u306b\u304a\u624b\u4f1d\u3044\u3044\u305f\u3057\u307e\u3059\u3002\u9280\u884c\u632f\u8fbc\u306b\u3088\u308b\u304a\u652f\u6255\u3044\u3084\u8acb\u6c42\u66f8\u30fb\u9818\u53ce\u66f8\u767a\u884c\u306b\u3082\u5bfe\u5fdc\u3057\u3066\u304a\u308a\u307e\u3059\u3002","url":"https:\/\/www.appsupport.jp\/"},"nextItem":"https:\/\/www.appsupport.jp\/googleapps\/docs\/%09%0A\/#listItem"},{"@type":"ListItem","@id":"https:\/\/www.appsupport.jp\/googleapps\/docs\/%09%0A\/#listItem","position":2,"item":{"@id":"https:\/\/www.appsupport.jp\/googleapps\/docs\/%09%0A\/#item","url":"https:\/\/www.appsupport.jp\/googleapps\/docs\/%09%0A\/"},"previousItem":"https:\/\/www.appsupport.jp\/#listItem"}]}]}
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', "UA-11365912-1", 'auto');
ga('send', 'pageview');
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.appsupport.jp\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
var meteorslidessettings = {"meteorslideshowspeed":"2000","meteorslideshowduration":"5000","meteorslideshowheight":"390","meteorslideshowwidth":"580","meteorslideshowtransition":"scrollLeft"};
//]]></script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js"></script>
<script>
jQuery(function(){
var addr_de = function(){
AjaxZip3.zip2addr(this,'','pref','address-1');
};
$('[name=zip]').change(addr_de).keyup(addr_de);
});
</script>
</head>
<body data-rsssl="1" style="-webkit-text-size-adjust:none">
<div id="container">
<div id="header">
<h1>Google Apps™ のお申し込み窓口、導入から運用までしっかりサポート。</h1>
<h2><a href="https://www.appsupport.jp"><img alt="Google Apps セットアップ&amp;サポート" src="https://www.appsupport.jp/images/appsupport_logo.png" title="Google Apps セットアップ&amp;サポート"/></a></h2>
</div>
<ul id="globalNavi">
<li class="lefttab"><a href="https://www.appsupport.jp/">　</a></li>
<li><a href="https://www.appsupport.jp/service/">サービス内容・料金</a></li>
<li><a href="https://www.appsupport.jp/googleapps/">Google Apps とは</a></li>
<li><a href="https://www.appsupport.jp/option/">拡張オプション</a></li>
<li><a href="https://www.appsupport.jp/category/">使い方</a></li>
<li class="righttab"><a href="https://www.appsupport.jp/faq/">よくある質問</a></li>
</ul>
<div id="topcontent">
<div id="content">
<h2 class="center">Google Apps セットアップ＆サポート ページエラー</h2>
    ページが見つかりません。 </div>
<!-- begin sidebar -->
<div id="sidebar">
<div id="infobox1">
<p>500万社以上がご利用の Google Apps を無料でサポート。現メールシステムとの併用も可能。</p>
<a href="https://www.appsupport.jp/trial/" title="GoogleApps30日間の無料試用"><img alt="GoogleApps30日間の無料試用" class="size-full wp-image-105 aligncenter" height="60" onmouseout="this.src='https://www.appsupport.jp/images/orderbar.png'" onmouseover="this.src='https://www.appsupport.jp/images/orderbar_on.png';" src="https://www.appsupport.jp/images/orderbar.png" style="margin-top: 5px; margin-bottom: 5px;" width="243"/></a>
<p style="margin-top: 10px;">Google Apps の導入前のご相談はこちらから。</p>
<a href="https://www.appsupport.jp/contact/" title="GoogleApps導入前のご相談"><img alt="Google Apps導入前のご相談" class="size-full wp-image-105 aligncenter" height="60" onmouseout="this.src='https://www.appsupport.jp/images/contactbar.png'" onmouseover="this.src='https://www.appsupport.jp/images/contactbar_on.png';" src="https://www.appsupport.jp/images/contactbar.png" style="margin-top: 5px; margin-bottom: 10px;" width="243"/></a></div>
<ul id="imgmenu">
<li><a href="https://www.appsupport.jp/service/request/" title="GoogleAppsの資料を請求"><img alt="GoogleAppsの資料ダウンロードはコチラ" class="size-full wp-image-113 aligncenter" height="75" onmouseout="this.src='https://www.appsupport.jp/images/request_top.png'" onmouseover="this.src='https://www.appsupport.jp/images/request_top_on.png';" src="https://www.appsupport.jp/images/request_top.png" title="GoogleAppsの資料ダウンロードはコチラ" width="275"/></a></li>
<li><a href="https://www.appsupport.jp/service/secure/" title="GoogleAppsのセキュリティ"><img alt="GoogleAppsのセキュリティ" class="alignnone size-full wp-image-895" height="75" onmouseout="this.src='https://www.appsupport.jp/images/secure.png'" onmouseover="this.src='https://www.appsupport.jp/images/secure_on.png';" src="https://www.appsupport.jp/images/secure.png" title="GoogleAppsのセキュリティ" width="275"/></a></li>
<li><a href="https://www.appsupport.jp/disaster-control/" title="災害対策としてのGoogleApps"><img alt="災害対策としてのGoogleApps" class="size-full wp-image-113 aligncenter" height="75" onmouseout="this.src='https://www.appsupport.jp/images/disaster.png'" onmouseover="this.src='https://www.appsupport.jp/images/disaster_on.png';" src="https://www.appsupport.jp/images/disaster.png" title="災害対策としてのGoogleApps" width="275"/></a></li>
<li><a href="https://www.appsupport.jp/service/costdown/" title="ITコストを削減"><img alt="ITコストを削減" class="size-full wp-image-113 aligncenter" height="75" onmouseout="this.src='https://www.appsupport.jp/images/cost_top.png'" onmouseover="this.src='https://www.appsupport.jp/images/cost_top_on.png';" src="https://www.appsupport.jp/images/cost_top.png" title="ITコストを削減" width="275"/></a></li>
<li><a href="https://www.appsupport.jp/service/mobile/" title="GoogleAppsとモバイルについて"><img alt="GoogleAppsとモバイル" class="alignnone size-full wp-image-1220" height="75" onmouseout="this.src='https://www.appsupport.jp/images/mobile.png'" onmouseover="this.src='https://www.appsupport.jp/images/mobile_on.png';" src="https://www.appsupport.jp/images/mobile.png" title="GoogleAppsとモバイル" width="275"/></a></li>
</ul>
</div>
<!-- end sidebar -->
</div>
</div>
<div id="footer">
<div id="footer_box">
<div id="box1">
<div class="footermenutitle">サービス案内</div>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/" title="ホーム">ホーム</a></li>
<li><a href="https://www.appsupport.jp/service/" title="サービス内容">サービス内容・料金</a></li>
<li><a href="https://www.appsupport.jp/service/secure/" title="GoogleAppsのセキュリティ">Google Apps のセキュリティ</a></li>
<li><a href="https://www.appsupport.jp/service/costdown/" title="ITコストを削減">IT コストを削減</a></li>
<li><a href="https://www.appsupport.jp/disaster-control/" title="災害時などの事業継続対策として">災害時などの事業継続対策として</a></li>
<li><a href="https://www.appsupport.jp/service/mobile/" title="GoogleAppsとモバイル">Google Apps とモバイル</a></li>
<li><a href="https://www.appsupport.jp/service/comparison_googleapps-and-gmailcom/" title="GoogleAppsと個人向けGoogleサービスとの違い">個人向けサービスとの違い</a></li>
<li><a href="https://www.appsupport.jp/faq/" title="よくある質問">よくある質問</a></li>
</ul>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/option/" title="GoogleApps拡張オプション">Google Apps 拡張オプション</a></li>
</ul>
</div>
<div id="box2">
<div class="footermenutitle">サービス概要</div>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/googleapps/" title="GoogleAppsとは">Google Apps</a></li>
<li><a href="https://www.appsupport.jp/googleapps/gmail/" title="Gmailとは">Gmail</a></li>
<li><a href="https://www.appsupport.jp/googleapps/calendar/" title="Googleカレンダーとは">Google カレンダー</a></li>
<li><a href="https://www.appsupport.jp/googleapps/drive/" title="Googleドライブとは">Google ドライブ</a></li>
<li><a href="https://www.appsupport.jp/googleapps/docs/" title="Googleドキュメントとは">Google ドキュメント</a></li>
<li><a href="https://www.appsupport.jp/googleapps/hangouts/" title="ハングアウトとは">ハングアウト</a></li>
<li><a href="https://www.appsupport.jp/googleapps/sites/" title="Googleサイトとは">Google サイト</a></li>
<li><a href="https://www.appsupport.jp/googleapps/administrator/" title="管理コンソール">管理コンソール</a></li>
</ul>
</div>
<div id="box3">
<div class="footermenutitle">Google Apps の使い方</div>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/category/" title="GoogleAppsの使い方">Google Apps の使い方</a></li>
<li><a href="https://www.appsupport.jp/category/gmail/" title="Gmailの使い方">Gmail の使い方</a></li>
<li><a href="https://www.appsupport.jp/category/calendar/" title="Googleカレンダーの使い方">Google カレンダーの使い方</a></li>
<li><a href="https://www.appsupport.jp/category/drive/" title="Googleドライブの使い方">Google ドライブの使い方</a></li>
<li><a href="https://www.appsupport.jp/category/docs/" title="Googleドキュメントの使い方">Google ドキュメントの使い方</a></li>
<li><a href="https://www.appsupport.jp/category/hanuouts/" title="ハングアウトの使い方">ハングアウトの使い方</a></li>
<li><a href="https://www.appsupport.jp/category/sites/" title="Googleサイトの使い方">Google サイトの使い方</a></li>
<li><a href="https://www.appsupport.jp/category/administrator/" title="管理コンソールの使い方">管理コンソールの使い方</a></li>
</ul>
</div>
<div id="box4">
<div class="footermenutitle">Google Apps を導入する</div>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/trial/" title="GoogleApps30日間の無料試用">30日間の無料試用お申し込み</a></li>
<li><a href="https://www.appsupport.jp/service/about-transfer/" title="移管お申し込み">移管お申し込み</a></li>
<li><a href="https://www.appsupport.jp/contact/" title="GoogleApps導入前お問い合わせ">導入前お問い合わせ</a></li>
<li><a href="https://www.appsupport.jp/service/request/" title="GoogleAppsの資料請求">資料請求</a></li>
</ul>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/information/" title="企業情報">企業情報</a></li>
<li><a href="https://www.appsupport.jp/privacypolicy/" title="プライバシーポリシー">プライバシーポリシー</a></li>
</ul>
<ul class="footermenu">
<li><a href="https://www.appsupport.jp/sitemap/" title="サイトマップ">サイトマップ</a></li>
</ul>
</div>
</div>
<div id="aioseo-admin"></div><script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.appsupport.jp\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.appsupport.jp/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="jquery-sonar-js" src="https://www.appsupport.jp/wp-content/plugins/lazy-load/js/jquery.sonar.min.js?ver=0.6.1" type="text/javascript"></script>
<script id="wpcom-lazy-load-images-js" src="https://www.appsupport.jp/wp-content/plugins/lazy-load/js/lazy-load.js?ver=0.6.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.appsupport.jp/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<div id="copy"> Google™ ならびに Google Apps™ は、Google Inc. の商標です。<br/>
    当ウェブサイトでの Google Apps™ とは特に記載のない限り、全て Google Apps™ for Work を表します。<br/>
    Copyright © 2021 <a href="https://www.appsupport.jp/information/" title="Crucial Works Inc.">Google Apps for Work Partner Crucial Works Inc.</a> All Rights Reserved. </div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11365912-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body></html>