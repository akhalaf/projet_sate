<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>New Version of 'Scarface' Being Developed at Universal</title>
<meta content="The upcoming third version of the gangster film reportedly will not be set as a remake or a sequel, but will still involve the basic plot of the first two movies. " name="description"/>
<meta content="" name="keywords"/>
<meta content="" name="news_keywords"/>
<meta content="" name="author"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<meta content="Wed, 17 Aug 2016 05:00:00 GMT" http-equiv="Expires"/>
<meta content="en-us, en-gb, en-au, en-ca" http-equiv="content-language"/>
<meta content="320088948016615" property="fb:app_id"/>
<meta content="New Version of 'Scarface' Being Developed at Universal" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://www.aceshowbiz.com/news/view/00043855.html" property="og:url"/>
<meta content="https://www.aceshowbiz.com/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg" property="og:image"/>
<meta content="AceShowbiz" property="og:site_name"/>
<meta content="The upcoming third version of the gangster film reportedly will not be set as a remake or a sequel, but will still involve the basic plot of the first two movies." property="og:description"/>
<meta content="New Version of 'Scarface' Being Developed at Universal" property="og:image:alt"/>
<meta content="New Version of 'Scarface' Being Developed at Universal" name="twitter:title"/>
<meta content="The upcoming third version of the gangster film reportedly will not be set as a remake or a sequel, but will still involve the basic plot of the first two movies." name="twitter:description"/>
<meta content="https://www.aceshowbiz.com/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg" name="twitter:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@aceshowbiz" name="twitter:site"/>
<meta content="@aceshowbiz" name="twitter:creator"/>
<meta content="aceshowbiz.com" name="twitter:domain"/>
<meta content="index, follow" name="robots"/>
<meta content="index, follow" name="Googlebot-news"/>
<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "NewsArticle",
			"mainEntityOfPage": 
			{
				"@type": "WebPage",
				"@id": "https://www.aceshowbiz.com/news/view/00043855.html"
			},
			"headline": "New Version of 'Scarface' Being Developed at Universal",
			"description": "The upcoming third version of the gangster film reportedly will not be set as a remake or a sequel, but will still involve the basic plot of the first two movies.",			
			"image": {
				"@type": "ImageObject",
				"url": "https://www.aceshowbiz.com/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg",
				"height": 150,
				"width": 100			},			
			"datePublished": "2011-09-22 08:47:42",
			"dateModified": "2011-09-22 08:47:42",
						"author": {
				"@type": "Person",
				"name": ""
			},
			"publisher": {
				"@type": "Organization",
				"name": "AceShowbiz.com",
				"logo": {
					"@type": "ImageObject",
					"url": "https://www.aceshowbiz.com/assets/img/png/logo225x30.png",
					"width": 225,
					"height": 30
				}
			}
		}
		</script>
<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "WebPage",
		  "headline": "New Version of 'Scarface' Being Developed at Universal",
		  "url": "https://www.aceshowbiz.com/news/view/00043855.html",
		  "datePublished": "2011-09-22 08:47:42",
		  "image": "https://www.aceshowbiz.com/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg",
		  "thumbnailUrl" : "https://www.aceshowbiz.com/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg"
		}
		</script>
<link href="https://m.aceshowbiz.com/news/view/00043855.html" media="only screen and (max-width: 600px)" rel="alternate"/>
<link href="https://www.aceshowbiz.com/amp/news/view/00043855.html" rel="amphtml"/>
<link href="/assets/css/normalize.css" rel="stylesheet"/>
<link href="/assets/css/main.css" rel="stylesheet"/>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/assets/css/meanmenu.min.css" rel="stylesheet"/>
<link href="/assets/css/style.css?v=1.1" rel="stylesheet"/>
<link href="/assets/css/ie-only.css" rel="stylesheet" type="text/css"/>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<meta content="#5F256F" name="theme-color"/>
<link href="/manifest.json" rel="manifest"/>
<link href="/assets/img/gif/favicon.gif" rel="shortcut icon" type="image/x-icon"/>
<link href="/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon167.png" rel="apple-touch-icon" sizes="167x167"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon192.png" rel="icon" sizes="192x192"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="https://www.aceshowbiz.com/assets/img/png/icon128.png" rel="icon" sizes="128x128"/>
<link href="//whizzco.com" rel="dns-prefetch"/>
<link href="//facebook.net" rel="dns-prefetch"/>
<link href="//sharethis.com" rel="dns-prefetch"/>
<link href="//mgid.com" rel="dns-prefetch"/>
<link href="//googlesyndication.com" rel="dns-prefetch"/>
<link href="//google-analytics.com" rel="dns-prefetch"/>
<link href="https://certify-js.alexametrics.com" rel="dns-prefetch"/>
<link href="https://tpc.googlesyndication.com" rel="dns-prefetch"/>
<link href="https://pagead2.googlesyndication.com" rel="dns-prefetch"/>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-4375969-1', 'aceshowbiz.com');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
<script src="/assets/js/modernizr-2.8.3.min.js"></script>
<script src="/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script async="" src="/assets/js/plugins.js " type="text/javascript"></script>
<script async="" src="/assets/js/bootstrap.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.meanmenu.min.js " type="text/javascript"></script>
<script src="/assets/js/jquery.scrollUp.min.js " type="text/javascript"></script>
<script async="" src="/assets/js/jquery.magnific-popup.min.js"></script>
<script defer="" src="/assets/js/main.js?v=10" type="text/javascript"></script>
<style>
			.videoWrapper {
				position: relative;
				padding-bottom: 56.25%; /* 16:9 */
				padding-top: 25px;
				height: 0;
			}
			.videoWrapper iframe {
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
			}
			.videoWrapper object {
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
			}			
		</style>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an 
        <strong>outdated</strong> browser. Please 
        <a href="https://www.google.com/chrome/">upgrade your browser</a> to improve your experience.
    </p>
    <![endif]-->
<div class="wrapper" id="wrapper">
<header>
<div class="header-style1" id="header-layout1">
<div class="main-menu-area bg-primarytextcolor header-menu-fixed" id="sticker">
<div class="container">
<div class="row no-gutters d-flex align-items-center">
<div class="col-lg-2 d-none d-lg-block">
<div class="logo-area">
<a href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
</div>
</div>
<div class="col-xl-8 col-lg-7 position-static min-height-none">
<div class="ne-main-menu">
<nav id="dropdown">
<ul>
<li>
<a href="/" title="Home">
<strong>Home</strong>
</a>
</li>
<li>
<a href="/news/" title="News">
<strong>News</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/news/celebrity/" title="Celebrity News">
Celebrity News
</a>
</li>
<li>
<a href="/news/movie/" title="Movie News">
Movie News
</a>
</li>
<li>
<a href="/news/tv/" title="TV News">
TV News
</a>
</li>
<li>
<a href="/news/music/" title="Music News">
Music News
</a>
</li>
</ul>
</li>
<li>
<a href="/celebrity/" title="Celebrity">
<strong>Celebrity</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/celebrity/buzz/" title="The Buzz">
The Buzz
</a>
</li>
<li>
<a href="/celebrity/legends/" title="The Legend">
The Legends
</a>
</li>
<li>
<a href="/celebrity/teenage/" title="Young Celeb">
Young Celeb
</a>
</li>
</ul>
</li>
<li>
<a href="/movie/" title="Movie">
<strong>Movie</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/movie/chart/" title="U.S. Box Office">
U.S. Box Office
</a>
</li>
<li>
<a href="/movie/now_playing/" title="Now Playing">
Now Playing
</a>
</li>
<li>
<a href="/movie/coming_soon/" title="Coming Soon">
Coming Soon
</a>
</li>
<li>
<a href="/movie/trailer/" title="Trailers">
Trailers
</a>
</li>
<li>
<a href="/movie/stills/" title="Pictures">
Pictures
</a>
</li>
<li>
<a href="/movie/review/" title="Reviews">
Reviews
</a>
</li>
<li>
<a href="/movie/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/tv/" title="TV">
<strong>TV</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/tv/trailer/" title="TV Clip / Previews">
TV Clip / Previews
</a>
</li>
<li>
<a href="/tv/dvd/" title="TV on DVD">
TV on DVD
</a>
</li>
<li>
<a href="/tv/soundtrack/" title="Soundtrack">
Soundtrack
</a>
</li>
</ul>
</li>
<li>
<a href="/music/" title="Music">
<strong>Music</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/artist/" title="Artist of The Week">
Artist of The Week
</a>
</li>
<li>
<a href="/music/chart/" title="ASB Music Chart">
ASB Music Chart
</a>
</li>
<li>
<a href="/music/new_release/" title="New Release">
New Release
</a>
</li>
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
</ul>
</li>
<li>
<a href="/gallery/" title="Photo">
<strong>Photo</strong>
</a>
</li>
<li>
<a href="/video/" title="Video">
<strong>Video</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/music/video/" title="Music Video">
Music Video
</a>
</li>
<li>
<a href="/movie/trailer/" title="Movie Trailer">
Movie Trailer
</a>
</li>
<li>
<a href="/tv/trailer/" title="TV Clip">
TV Clip
</a>
</li>
</ul>
</li>
<li>
<a href="#" title="Other">
<strong>Other</strong>
</a>
<ul class="ne-dropdown-menu">
<li>
<a href="/interviews/" title="Interviews">
<strong>Interviews</strong>
</a>
</li>
<li>
<a href="/movie/dvd/" title="DVD">
<strong>DVD</strong>
</a>
</li>
<li>
<a href="/contest/" title="Contest">
<strong>Contest</strong>
</a>
</li>
<li>
<a href="/index_old.php" title="Old Homepage">
<strong>Old Homepage</strong>
</a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-12 text-right position-static">
<div class="header-action-item">
<ul>
<li>
<form class="header-search-light" id="top-search-form">
<input class="search-input" placeholder="Search...." required="" style="display: none;" type="text"/>
<button class="search-button">
<i aria-hidden="true" class="fa fa-search"></i>
</button>
  
</form>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
<div class="container"> </div>
<div class="mt-30"> </div>
<style>
			#RightFloatAds
			{
			right: 0px;
			position: fixed;
			top: 84px;
			z-index:2;
			}
		</style>
<section class="breadcrumbs-area" style="background-image: url('/assets/img/banner/breadcrumbs-banner.jpg');">
<div class="container">
<div class="breadcrumbs-content">
<h1>New Version of 'Scarface' Being Developed at Universal</h1>
<ul>
<li><a href="/">Home</a> -</li>
<li><a href="/news/">News</a></li>
</ul>
</div>
</div>
</section>
<section class="bg-body">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mt-30 mb-30 text-center">
<div class="OUTBRAIN" data-src="DROP_PERMALINK_HERE" data-widget-id="AR_1"></div> <script async="async" src="//widgets.outbrain.com/outbrain.js" type="text/javascript"></script>
</div>
</div>
</div>
</div>
</section>
<section class="bg-body section-space-less30">
<div class="container">
<div class="row">
<div class="col-lg-8 col-md-12 mb-30">
<div class="news-details-layout1">
<div class="position-relative mb-30">
<figure>
<img alt="New Version of 'Scarface' Being Developed at Universal" class="img-fluid width-100" src="/images/news-thumb/2011/09/new-version-scarface-developed-universal.jpg"/>
</figure>
<div class="topic-box-top-sm">
<div class="topic-box-sm color-cinnabar mb-20">Movie</div>
</div>
</div>
<h2 class="title-semibold-dark size-c30">The upcoming third version of the gangster film reportedly will not be set as a remake or a sequel, but will still involve the basic plot of the first two movies.</h2>
<section class="bg-body">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mb-20 text-center">
</div>
</div>
</div>
</div>
</section>
<ul class="post-info-dark mb-30">
<li>
<i aria-hidden="true" class="fa fa-calendar"></i> Sep 22, 2011 </li>
</ul>
<p><strong>AceShowbiz</strong> - A new version of "<strong><a href="/movie/scarface/">Scarface</a></strong>" is now being developed at Universal Pictures. Deadline reported that the studio already had a meeting with some scribes to pen the script for the third version of the gangster movie. </p><p>
The first "Scarface" was released in 1932, while the second one was debuted in 1983 with <strong><a href="/celebrity/al_pacino/">Al Pacino</a></strong> playing the notorious gangster leader Tony Montana. Meanwhile, the upcoming project will reportedly be produced by Marc Shmuger and Martin Bregman, who also produced the Pacino version. </p><p>
</p><div class="author-info p-10-r see-also mb-30 border-all">
<div class="see-also-title">  See also...</div>
<ul class="fa-ul see-also-ul">
<li class="see-also-li">
<i class="fa-li top03em fa fa-arrow-right"></i>
<a href="/news/view/00165063.html" target="_blank">Spike Lee's Daughter and Son Make History as 2021 Golden Globe Ambassadors </a>
</li>
<li class="see-also-li">
<i class="fa-li top03em fa fa-arrow-right"></i>
<a href="/news/view/00165058.html" target="_blank">Nick Jonas In Talks to Play Frankie Valli in 'Jersey Boys' Streaming Event</a>
</li>
<li class="see-also-li">
<i class="fa-li top03em fa fa-arrow-right"></i>
<a href="/news/view/00165055.html" target="_blank">Jessica Chastain Ensures Her Co-Stars Share Ownership and Profits of New Movie</a>
</li>
<li class="see-also-li">
<i class="fa-li top03em fa fa-arrow-right"></i>
<a href="/news/view/00165054.html" target="_blank">Paul McCartney's Daughter Mary Tapped to Direct Abbey Road Studios Documentary </a>
</li>
</ul>
</div>
<p>
<script async="" data-cfasync="false" type="text/javascript">
		var truvidScript = document.createElement('script'); 
		truvidScript.setAttribute('data-cfasync','false'); 
		truvidScript.async = true; 
		truvidScript.type = 'text/javascript'; 
		truvidScript.src = '//stg.truvidplayer.com/index.php?sub_user_id=641&widget_id=2757&m=a&cb=' + (Math.random() * 10000000000000000); 
		var currentScript = document.currentScript || document.scripts[document.scripts.length - 1]; 
		currentScript.parentNode.insertBefore(truvidScript, currentScript.nextSibling);
	</script>
</p><p>
<script>
	var unruly = window.unruly || {};
	unruly.native = unruly.native || {};
	unruly.native.siteId = 1079155
	</script>
<script src="//video.unrulymedia.com/native/native-loader.js"></script>
</p><p>
According to the report, the new "Scarface" movie is not planned as a remake or a sequel. Nonetheless, it will reportedly still involve the basic plot of the first two films, an immigrant who commits a crime and becomes a mafia kingpin in pursuit of a twisted American dream. </p><p>
It is not clear where the Tony character, who has a signature line "Say hello to my little friend", will come from in the new project. In the first version, <strong>Paul Muni</strong>'s Tony was an Italian who took over Chicago, while in the second one, Pacino's Tony was a Cuban refugee who rules the cocaine trade in Miami. </p><p></p>
<div class="mb-30">
<section class="bg-body">
<div class="container">
<div class="row">
<div class="col-12">
<div class="ne-banner-layout1 mt-20 text-center">
<div id="ads_container_104124" website_id="104" widget_id="124"></div>
<script type="text/javascript">var uniquekey = "104124"; </script>
<script src="https://cdn.whizzco.com/scripts/widget/widget_t.js" type="text/javascript"></script>
</div>
</div>
</div>
</div>
</section>
</div>
<div class="post-share-area mb-40 item-shadow-1">
<p>You can share this post!</p>
<div class="sharethis-inline-share-buttons"></div>
</div>
<div class="row no-gutters divider blog-post-slider">
<div class="col-lg-6 col-md-6 col-sm-6 col-6">
<a class="prev-article" href="/news/view/00043854.html">
<i aria-hidden="true" class="fa fa-angle-left"></i>Previous article</a>
<a href="/news/view/00043854.html"><h3 class="title-medium-dark pr-50">R.E.M. Announce Retirement, to Release Greatest Hits Album in November</h3></a>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-6 text-right">
<a class="next-article" href="/news/view/00043856.html">Next article
<i aria-hidden="true" class="fa fa-angle-right"></i>
</a>
<a href="/news/view/00043856.html"><h3 class="title-medium-dark pl-50">Eva Mendes Forgets Sensible Shoes When Going on a Hike With Ryan Gosling</h3></a>
</div>
</div>
<section>
<div class="row">
<div class="col-12">
<div class="topic-border color-cod-gray mb-30 width-100">
<div class="topic-box-lg color-cod-gray">Related Posts</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-6">
<div class="media mb-30">
<a class="width38-lg width40-md img-opacity-hover" href="/news/view/00160899.html">
<img alt="Scarface Begs for Kidney Donation After COVID-19 Damaged His" class="img-fluid" data-qazy="true" src="/display/images/90x60/2020/10/08/00160899.jpg"/>
</a>
<div class="media-body">
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00160899.html" target="_blank">Scarface Begs for Kidney Donation After COVID-19 Damaged His</a>
</h3>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-6">
<div class="media mb-30">
<a class="width38-lg width40-md img-opacity-hover" href="/news/view/00153860.html">
<img alt="'Scarface' Remake Gets Director" class="img-fluid" data-qazy="true" src="/display/images/90x60/2020/05/15/00153860.jpg"/>
</a>
<div class="media-body">
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00153860.html" target="_blank">'Scarface' Remake Gets Director</a>
</h3>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-6">
<div class="media mb-30">
<a class="width38-lg width40-md img-opacity-hover" href="/news/view/00152903.html">
<img alt="Scarface Relies on Dialysis Treatment After Near-Fatal Coronavirus Complications" class="img-fluid" data-qazy="true" src="/display/images/90x60/2020/04/27/00152903.jpg"/>
</a>
<div class="media-body">
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00152903.html" target="_blank">Scarface Relies on Dialysis Treatment After Near-Fatal Coronavirus Complications</a>
</h3>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-6">
<div class="media mb-30">
<a class="width38-lg width40-md img-opacity-hover" href="/news/view/00151272.html">
<img alt="Scarface on Contracting Coronavirus: I've Been to the Point Where I Felt I Was Going to Die" class="img-fluid" data-qazy="true" src="/display/images/90x60/2020/03/27/00151272.jpg"/>
</a>
<div class="media-body">
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00151272.html" target="_blank">Scarface on Contracting Coronavirus: I've Been to the Point Where I Felt I Was Going to Die</a>
</h3>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
<div class="ne-sidebar sidebar-break-md col-lg-4 col-md-12">
<div class="sidebar-box">
<ul class="stay-connected overflow-hidden">
<li class="facebook">
<a href="https://www.facebook.com/aceshowbizdotcom/">
<i aria-hidden="true" class="fa fa-facebook"></i>
</a>
</li>
<li class="twitter">
<a href="https://twitter.com/aceshowbiz/">
<i aria-hidden="true" class="fa fa-twitter"></i>
</a>
</li>
<li class="google_plus">
<a href="https://plus.google.com/117276967966258729454">
<i aria-hidden="true" class="fa fa-google-plus"></i>
</a>
</li>
<li class="rss">
<a href="/site/rss.php">
<i aria-hidden="true" class="fa fa-rss"></i>
</a>
</li>
</ul>
</div>
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
<div class="OUTBRAIN" data-src="DROP_PERMALINK_HERE" data-widget-id="AR_2"></div> <script async="async" src="//widgets.outbrain.com/outbrain.js" type="text/javascript"></script>
</div>
</div>
<div class="sidebar-box">
<div class="topic-border color-scampi mb-5">
<div class="topic-box-lg color-scampi">Most Read</div>
</div>
<div class="row">
<div class="col-12">
<div class="img-overlay-70 img-scale-animate mb-30">
<a href="/news/view/00164950.html" target="_blank"><img alt="Laverne Cox Quits Sex Industry Documentary Amid Backlash" class="img-fluid width-100" data-qazy="true" src="/display/images/300x400/2021/01/10/00164950.jpg"/></a>
<div class="topic-box-top-lg">
<div class="topic-box-sm color-cod-gray mb-20">Movie</div>
</div>
<div class="mask-content-lg">
<div class="post-date-light">
<ul>
<li>
<span>
<i aria-hidden="true" class="fa fa-calendar"></i>
</span></li>
</ul>
</div>
<h2 class="title-medium-light size-lg">
<a href="/news/view/00164950.html" target="_blank">Laverne Cox Quits Sex Industry Documentary Amid Backlash</a>
</h2>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164995.html" target="_blank">
<img alt="Jack Quaid Defends His Character for Killing Amandla Stenberg's Rue in 'The Hunger Games'" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164995.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164995.html" target="_blank">Jack Quaid Defends His Character for Killing Amandla Stenberg's Rue in 'The Hunger Games'</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164952.html" target="_blank">
<img alt="'Star Wars: Rogue Squadron' Hires 'Doctor Strange 2' Screenwriter" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/10/00164952.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164952.html" target="_blank">'Star Wars: Rogue Squadron' Hires 'Doctor Strange 2' Screenwriter</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164946.html" target="_blank">
<img alt="Carey Mulligan's 'Promising Young Woman' Wins Big at Columbus Awards" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/10/00164946.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164946.html" target="_blank">Carey Mulligan's 'Promising Young Woman' Wins Big at Columbus Awards</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164972.html" target="_blank">
<img alt="'Home Alone 2' Fans Demand Removal of Trump's Cameo Following Capitol Riot" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164972.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164972.html" target="_blank">'Home Alone 2' Fans Demand Removal of Trump's Cameo Following Capitol Riot</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00164958.html" target="_blank">
<img alt="'Nomadland' Wins Best Picture at National Society of Film Critics Awards" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/11/00164958.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00164958.html" target="_blank">'Nomadland' Wins Best Picture at National Society of Film Critics Awards</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165008.html" target="_blank">
<img alt="'Black Panther 2' to Explore Other Characters and Different Subcultures " class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165008.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165008.html" target="_blank">'Black Panther 2' to Explore Other Characters and Different Subcultures </a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165037.html" target="_blank">
<img alt="2021 Gotham Awards Sees 'Nomadland' Taking Top Honor" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165037.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165037.html" target="_blank">2021 Gotham Awards Sees 'Nomadland' Taking Top Honor</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165040.html" target="_blank">
<img alt="Ryan Reynolds Jokes About Manipulating Disney Into Agreeing to 'Deadpool 3' R-Rating" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165040.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165040.html" target="_blank">Ryan Reynolds Jokes About Manipulating Disney Into Agreeing to 'Deadpool 3' R-Rating</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165004.html" target="_blank">
<img alt="'Bridgerton' Star Rege-Jean Page Reacts to James Bond Rumors" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165004.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165004.html" target="_blank">'Bridgerton' Star Rege-Jean Page Reacts to James Bond Rumors</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165054.html" target="_blank">
<img alt="Paul McCartney's Daughter Mary Tapped to Direct Abbey Road Studios Documentary " class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/13/00165054.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165054.html" target="_blank">Paul McCartney's Daughter Mary Tapped to Direct Abbey Road Studios Documentary </a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165028.html" target="_blank">
<img alt="'Morbius' Delayed Again as Theatergoing Continues to Struggle Amid COVID-19 Crisis" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/12/00165028.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165028.html" target="_blank">'Morbius' Delayed Again as Theatergoing Continues to Struggle Amid COVID-19 Crisis</a>
</h3>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-4 col-6">
<div class="mt-25">
<a class="img-opacity-hover" href="/news/view/00165055.html" target="_blank">
<img alt="Jessica Chastain Ensures Her Co-Stars Share Ownership and Profits of New Movie" class="img-fluid mb-10 width-100" data-qazy="true" src="/display/images/160x117/2021/01/13/00165055.jpg"/>
</a>
<h3 class="title-medium-dark size-md mb-none">
<a href="/news/view/00165055.html" target="_blank">Jessica Chastain Ensures Her Co-Stars Share Ownership and Profits of New Movie</a>
</h3>
</div>
</div>
</div>
</div>
<div class="sidebar-box">
<div class="ne-banner-layout1 text-center">
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="footer-area-bottom">
<div class="container">
<div class="row">
<div class="col-12 text-center">
<a class="footer-logo img-fluid" href="/">
<img alt="AceShowbiz logo" class="/assets/img-fluid" src="/assets/img/logo.png"/>
</a>
<h2 class="title-bold-light">
<a href="/site/about.php" title="About">About</a> |
<a href="/site/faq.php" title="Authors">FAQ</a> |
<a href="/site/term.php" title="Term of Use">Term of Use</a> |
<a href="/site/sitemap.php" title="Sitemap">Sitemap</a> |
<a href="/site/contact.php" title="Contact Us">Contact Us</a>
</h2>
<h3 class="title-medium-light size-md mb-10">© 2005-2021 <a href="https://www.aceshowbiz.com" target="_blank" title="AceShowbiz">AceShowbiz</a>. All Rights Reserved.</h3>
</div>
</div>
</div>
</div>
</footer>
</div>
<script>
			if ('serviceWorker' in navigator) {
				navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
					// Registration was successful
					console.log('ServiceWorker registration successful with scope: ', registration.scope);
				}).catch(function(err) {
					// registration failed :(
					console.log('ServiceWorker registration failed: ', err);
				});
			}
			
			self.addEventListener('fetch', function(event) {
			console.log(event.request.url);
			event.respondWith(
			caches.match(event.request).then(function(response) {
			return response || fetch(event.request);
			})
			);
			});			
		</script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5989b2b57c544f0011665e85&amp;product=inline-share-buttons" type="text/javascript"></script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'610b7a432c7cd93e',m:'5c9ad8c693f0be8bdb41b47a795ae39dc5d4493e-1610502744-1800-AQESDjTNKJn3GIBqZYVJonG8nzZ0tx54KfcHxW0A78/tghm938HzkCA9zyMqQu2T1sIEG9GDFVFcIibtUDOpSiTSmW32PJe0/7jY0iXGdwzXysZd+E56KEwrAuIt2lHjPnTO7fggyS0R/aurlzz9f1g=',s:[0xf30c856774,0xe259f989ee],}})();</script></body>
</html>
