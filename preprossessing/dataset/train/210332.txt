<!DOCTYPE html>
<html lang="de-DE">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.berlinleuchtet.com/xmlrpc.php" rel="pingback"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- PWA -->
<meta content="yes" name="mobile-web-app-capable"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/manifest.json" rel="manifest"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/img/icons/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<!-- PWA -->
<base href="https://www.berlinleuchtet.com/"/>
<!--noptimize-->
<script type="text/javascript">

            // 
            // Get screen dimensions, device pixel ration and set in a cookie.
            // 
            
                            var screen_width = screen.width;
            
            var devicePixelRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;

            document.cookie = 'resolution=' + screen_width + ',' + devicePixelRatio + '; path=/';

        </script>
<!--/noptimize--> <title>Startseite - Berlin Leuchtet</title>
<!-- This site is optimized with the Yoast SEO plugin v13.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://www.berlinleuchtet.com/" rel="canonical"/>
<meta content="de_DE" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Startseite - Berlin Leuchtet" property="og:title"/>
<meta content="https://www.berlinleuchtet.com/" property="og:url"/>
<meta content="Berlin Leuchtet" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Startseite - Berlin Leuchtet" name="twitter:title"/>
<meta content="@berlin_leuchtet" name="twitter:site"/>
<meta content="@berlin_leuchtet" name="twitter:creator"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.berlinleuchtet.com/#organization","name":"Berlin Leuchtet","url":"https://www.berlinleuchtet.com/","sameAs":["https://www.facebook.com/BerlinLeuchtet/","https://www.instagram.com/berlinleuchtet/","https://www.youtube.com/channel/UC7HDpdN7g8YWcdiDDq6gLIg","https://twitter.com/berlin_leuchtet"],"logo":{"@type":"ImageObject","@id":"https://www.berlinleuchtet.com/#logo","inLanguage":"de-DE","url":"https://www.berlinleuchtet.com/wp-content/uploads/2019/09/csm_berlin_leuchtet_rgb_0d15a77884.png","width":1104,"height":1080,"caption":"Berlin Leuchtet"},"image":{"@id":"https://www.berlinleuchtet.com/#logo"}},{"@type":"WebSite","@id":"https://www.berlinleuchtet.com/#website","url":"https://www.berlinleuchtet.com/","name":"Berlin Leuchtet","inLanguage":"de-DE","publisher":{"@id":"https://www.berlinleuchtet.com/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://www.berlinleuchtet.com/?s={search_term_string}","query-input":"required name=search_term_string"}]},{"@type":"WebPage","@id":"https://www.berlinleuchtet.com/#webpage","url":"https://www.berlinleuchtet.com/","name":"Startseite - Berlin Leuchtet","isPartOf":{"@id":"https://www.berlinleuchtet.com/#website"},"inLanguage":"de-DE","about":{"@id":"https://www.berlinleuchtet.com/#organization"},"datePublished":"2019-08-19T15:35:45+00:00","dateModified":"2020-10-09T08:37:05+00:00","potentialAction":[{"@type":"ReadAction","target":["https://www.berlinleuchtet.com/"]}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.4';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-163340043-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-163340043-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview');
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/css/bootstrap.css?v=050857&amp;ver=050857" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/css/index.css?v=050857&amp;ver=050857" id="index-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/countdown-wpdevart-extended/includes/admin/css/font-awesome.min.css?ver=5.4" id="FontAwesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/countdown-wpdevart-extended/includes/frontend/css/front_end.css?ver=5.4" id="wpdevart_countdown_extend_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/wd-google-maps/css/bootstrap.css?ver=5.4" id="bootstrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/wd-google-maps/css/frontend_main.css?ver=5.4" id="frontend_main-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/countdown-wpdevart-extended/includes/admin/gutenberg/style.css?ver=5.4" id="wpda_countdown_extended_gutenberg_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/captcha/css/front_end_style.css?ver=4.4.5" id="cptch_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-includes/css/dashicons.min.css?ver=5.4" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/captcha/css/desktop_style.css?ver=4.4.5" id="cptch_desktop_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.4" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/easy-video-player/lib/skin/skin.css?ver=5.4" id="flowplayer-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/weglot/dist/css/front-css.css?ver=3.1.6" id="weglot-css-css" media="" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/style.css?ver=5.4" id="lnmedia-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://www.berlinleuchtet.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.5' type='text/css' media='screen' />
<![endif]-->
<link href="https://www.berlinleuchtet.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.5" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/all-in-one-video-gallery/public/assets/css/backward-compatibility.css?ver=2.3.0" id="all-in-one-video-gallery-backward-compatibility-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/all-in-one-video-gallery/public/assets/css/public.css?ver=2.3.0" id="all-in-one-video-gallery-public-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.berlinleuchtet.com/wp-content/plugins/youtube-embed-plus/styles/ytprefs.min.css?ver=13.2.3" id="__EPYT__style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="__EPYT__style-inline-css" type="text/css">

                .epyt-gallery-thumb {
                        width: 33.333%;
                }
                
</style>
<!--n2css--><script src="https://www.berlinleuchtet.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/countdown-wpdevart-extended/includes/frontend/js/front_timer.js?ver=5.4" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places%2Cgeometry&amp;v=3.exp&amp;key=AIzaSyBxiaSJPIRfQWID9j4hCrX3t7z-9IOOjis&amp;ver=5.4" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/wd-google-maps/js/init_map.js?ver=5.4" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/wd-google-maps/js/frontend_main.js?ver=5.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/www.berlinleuchtet.com","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxUrl":"https:\/\/www.berlinleuchtet.com\/wp-admin\/admin-ajax.php","nonce":"b173d67d1a","hideEffect":"fade","position":"bottom","onScroll":"0","onScrollOffset":"100","onClick":"0","cookieName":"cookie_notice_accepted","cookieTime":"2592000","cookieTimeRejected":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"0","cache":"0","refuse":"0","revokeCookies":"0","revokeCookiesOpt":"automatic","secure":"1","coronabarActive":"0"};
/* ]]> */
</script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.1" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/easy-video-player/lib/flowplayer.min.js?ver=5.4" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/uploads/wpservefile_files/wpswmanager_sw-registrar.js?ver=5.4" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/weglot/dist/front-js.js?ver=3.1.6" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _EPYT_ = {"ajaxurl":"https:\/\/www.berlinleuchtet.com\/wp-admin\/admin-ajax.php","security":"b06b7f8133","gallery_scrolloffset":"20","eppathtoscripts":"https:\/\/www.berlinleuchtet.com\/wp-content\/plugins\/youtube-embed-plus\/scripts\/","eppath":"https:\/\/www.berlinleuchtet.com\/wp-content\/plugins\/youtube-embed-plus\/","epresponsiveselector":"[\"iframe.__youtube_prefs__\",\"iframe[src*='youtube.com']\",\"iframe[src*='youtube-nocookie.com']\",\"iframe[data-ep-src*='youtube.com']\",\"iframe[data-ep-src*='youtube-nocookie.com']\",\"iframe[data-ep-gallerysrc*='youtube.com']\"]","epdovol":"1","version":"13.2.3","evselector":"iframe.__youtube_prefs__[src], iframe[src*=\"youtube.com\/embed\/\"], iframe[src*=\"youtube-nocookie.com\/embed\/\"]","ajax_compat":"","ytapi_load":"light","pause_others":"","stopMobileBuffer":"1","vi_active":"","vi_js_posttypes":[]};
/* ]]> */
</script>
<script defer="" src="https://www.berlinleuchtet.com/wp-content/plugins/youtube-embed-plus/scripts/ytprefs.min.js?ver=13.2.3" type="text/javascript"></script>
<link href="https://www.berlinleuchtet.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.berlinleuchtet.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4" name="generator"/>
<link href="https://www.berlinleuchtet.com/" rel="shortlink"/>
<link href="https://www.berlinleuchtet.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.berlinleuchtet.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.berlinleuchtet.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.berlinleuchtet.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<!-- This content is generated with the Easy Video Player plugin v1.1.8 - http://noorsplugin.com/wordpress-video-plugin/ --><script>flowplayer.conf.embed = false;flowplayer.conf.keyboard = false;</script><!-- Easy Video Player plugin -->
<link href="https://www.berlinleuchtet.com/" hreflang="de" rel="alternate"/>
<link href="https://www.berlinleuchtet.com/en/" hreflang="en" rel="alternate"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<style type="text/css">.broken_link, a.broken_link {
	text-decoration: line-through;
}</style> <style type="text/css">
					.site-title a,
			.site-description {
				color: #ffffff;
			}
				</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #000000; background-image: url("https://www.berlinleuchtet.com/wp-content/uploads/2020/04/bl-back.001.jpeg"); background-position: center center; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="home page-template-default page page-id-10 page-parent custom-background wp-site cookies-not-set wpb-js-composer js-comp-ver-6.0.5 vc_responsive">
<main id="wrap">
<div class="main-nav-wp"> <div class="main-nav-wp-contain"> <a class="main-nav-wp-brand" href="https://www.berlinleuchtet.com"></a><ul class="menu" id="menu-hauptmenue"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-10 current_page_item menu-item-13" id="menu-item-13"><a aria-current="page" href="https://www.berlinleuchtet.com/">Startseite</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2161" id="menu-item-2161"><a href="https://www.berlinleuchtet.com/media-2/">Presse</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-171" id="menu-item-171"><a href="https://www.berlinleuchtet.com/startseite/verein/">Verein</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272" id="menu-item-272"><a href="https://www.berlinleuchtet.com/startseite/kontakt/">Kontakt</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1785" id="menu-item-1785"><a href="https://www.berlinleuchtet.com/covid-19/">Covid 19</a></li>
<li class="weglot-lang menu-item-weglot weglot-language weglot-flags flag-1 weglot-de de menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-weglot-1137-de" id="menu-item-weglot-1137-de"><a data-wg-notranslate="true" href="https://www.berlinleuchtet.com/" title="Deutsch"></a></li>
<li class="weglot-lang menu-item-weglot weglot-language weglot-flags flag-1 weglot-en en menu-item menu-item-type-custom menu-item-object-custom menu-item-weglot-1137-en" id="menu-item-weglot-1137-en"><a data-wg-notranslate="true" href="https://www.berlinleuchtet.com/en/" title="English"></a></li>
</ul> <div class="nav-wp-trigger"><span></span><span></span><span></span></div> </div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="alert alert-warning text-center">Aktuell keine Events Verfügbar</div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<h4></h4>
<h1 style="text-align: center;"><span style="color: #ffffff;">Welcome to Berlin Leuchtet</span></h1>
<p> </p>
<h4 style="text-align: center;"></h4>
<h4 style="text-align: center;"><span style="color: #ffffff;"><strong>Der Verein Berlin leuchtet</strong></span></h4>
<h4 style="text-align: center;"></h4>
<h4 style="text-align: center;"></h4>
<h4 style="text-align: center;"><span style="color: #ffffff;"><strong>Der Verein BERLIN leuchtet wurde im Januar 2013 gegründet und steht in der Tradition des vor 10 Jahren von der City Stiftung Berlin initiierten und inzwischen bekannten Lichtfests.</strong></span></h4>
<h4 style="text-align: center;"><span style="color: #ffffff;"><strong>Der Verein BERLIN leuchtet wird mit aktiver Unterstützung der von seinen Mitgliedern repräsentierten ca. 600 Berliner und überregionalen Unternehmen, Institutionen und Einrichtungen sowie weiterer engagierter Partner, Künstler oder Sponsoren im Herbst ein Event veranstalten. Dabei werden mittels der Symbolkraft des Lichts sowohl Sehenswürdigkeiten bzw. hervorgehobene Gebäude in besonderem Glanz erstrahlen sowie kreative und innovative, international herausragende Entwicklungen und Leistungen der Stadt und ihrer Bürger zum „Leuchten“ gebracht werden. Die Hauptstadt wird sich in einem anderen, ganz besonderen Licht präsentieren.</strong></span></h4>
<h4 style="text-align: center;"></h4>
<h4 style="text-align: center;"><span style="color: #ffffff;"><strong>Der große gemeinsame Event des Vereins BERLIN leuchtet, das “Lichterfest”, fand erstmals im Herbst 2013 statt und seitdem jedes Jahr in den ersten beiden Oktoberwochen.</strong></span></h4>
<h4 style="text-align: center;"></h4>
</div>
</div>
<div class="btn-group btn-group-center"> <a alt="Mehr erfahren »" class="btn " href="http://www.berlinleuchtet.com/verein/" title="Mehr erfahren »">Mehr erfahren »   </a></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
</div>
</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div>
<div class="footer-wp"> <div class="footer-wp-contain"> <div class="footer-wp-info"> <div class="textwidget"><p><strong>Berlin Leuchtet e. V.</strong><br/>
Geschäftsstelle:<br/>
Hauptstraße 30<br/>
10827 Berlin<br/>
<strong>Tel:</strong> +49 (0)30 263 939 26<br/>
<strong>Fax:</strong> +49 (0)30 263 939 27<br/>
<strong>E-Mail:</strong> <a href="mailto:info@berlin-leuchtet.com">info@berlin-leuchtet.com</a></p>
</div>
</div> <div class="footer-wp-newsletter"> <b>Newsletter abonnieren!</b><form action="" class="input-group" enctype="multipart/form-data" id="post-form" method="post" name="post-form"> <input class="form-control" name="emailNewsletter" placeholder="E-Mail" type="email"/> <button class="btn" type="submit">abonnieren</button></form> </div> </div></div>
</main>
<script src="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/js/bootstrap.min.js?ver=1" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/themes/berlinleuchtet-child/js/index.min.js?v=050857&amp;ver=050857" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/pb-mailcrypt-antispam-email-encryption/mailcrypt.js?ver=1.0.1" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/themes/lnmedia/js/navigation.js?ver=20151215" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/themes/lnmedia/js/skip-link-focus-fix.js?ver=20151215" type="text/javascript"></script>
<script defer="" src="https://www.berlinleuchtet.com/wp-content/plugins/youtube-embed-plus/scripts/fitvids.min.js?ver=13.2.3" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-includes/js/wp-embed.min.js?ver=5.4" type="text/javascript"></script>
<script src="https://www.berlinleuchtet.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.5" type="text/javascript"></script>
<!-- Cookie Notice plugin v1.3.1 by Digital Factory https://dfactory.eu/ -->
<div aria-label="Cookie Notice" class="cookie-notice-hidden cookie-revoke-hidden cn-position-bottom" id="cookie-notice" role="banner" style="background-color: rgba(0,0,0,1);"><div class="cookie-notice-container" style="color: #fff;"><span class="cn-text-container" id="cn-notice-text">Diese Website benutzt Cookies. Wenn du die Website weiter nutzt, gehen wir von deinem Einverständnis aus.</span><span class="cn-buttons-container" id="cn-notice-buttons"><a class="cn-set-cookie cn-button bootstrap button" data-cookie-set="accept" href="#" id="cn-accept-cookie">OK</a><a class="cn-more-info cn-button bootstrap button" href="https://www.berlinleuchtet.com/datenschutz/" id="cn-more-info" target="_blank">Datenschutzerklärung</a></span><a class="cn-close-icon" data-cookie-set="accept" href="javascript:void(0);" id="cn-close-notice"></a></div>
</div>
<!-- / Cookie Notice plugin -->
</body>
</html>
