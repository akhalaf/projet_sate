<!DOCTYPE html>
<html lang="en">
<head>
<meta content="width=device-width" id="p7HMY" name="viewport"/>
<meta charset="utf-8"/>
<title>Free art lessons, design lessons and art appreciation for all.</title>
<meta content="Artyfactory offers Free Art Lessons, Design Lessons and Art Appreciation for art teachers, art students and artists of all ages." name="Description"/>
<meta content="41CD05CACA1A62452515701094B956B9" name="msvalidate.01"/>
<meta content="yAfI33wGSKlQa4oKkFSOHw9_dljItmBuvaAI2lbSxwM" name="google-site-verification"/>
<meta content="108dbe618b135376dae8d90c960ef36f" name="p:domain_verify"/>
<meta content="14a3ce573ec17cf25f6d48667f8e5194" name="p:domain_verify"/>
<meta content="f953f5aaa7e83ff1620c6bcefdcb7435" name="p:domain_verify"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1714600-1', 'artyfactory.com');
  ga('send', 'pageview');
</script>
<link href="sitebody/sitebody_img/logos/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<!-- Begin Cookie Consent plugin -->
<script async="" data-cbid="e7076441-0630-4d04-9e07-bf213b1298e8" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script>
<script async="" id="CookieDeclaration" src="https://consent.cookiebot.com/e7076441-0630-4d04-9e07-bf213b1298e8/cd.js" type="text/javascript"></script>
<!-- End Cookie Consent plugin -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-5875116297643912",
          enable_page_level_ads: true
     });
</script>
<link href="p7amm/p7AMM-01.css" media="all" rel="stylesheet" type="text/css"/>
<script src="p7amm/p7AMMscripts.js" type="text/javascript"></script>
<link href="p7hmy/p7HMY-01.css" media="all" rel="stylesheet" type="text/css"/>
<script src="p7hmy/p7HMYscripts.js" type="text/javascript"></script>
<link href="p7typecrafters/p7Typecrafters-01.css" rel="stylesheet"/>
<style></style>
<link href="p7fgm/p7FGM-01.css" media="all" rel="stylesheet" type="text/css"/>
<script src="p7fgm/p7FGMscripts.js" type="text/javascript"></script>
<link href="p7ifx/p7IFX-01.css" media="all" rel="stylesheet" type="text/css"/>
<script src="p7ifx/p7IFXscripts.js" type="text/javascript"></script>
<link href="p7stt/p7STT-01.css" media="all" rel="stylesheet" type="text/css"/>
<script src="p7stt/p7STTscripts.js" type="text/javascript"></script>
<link href="p7tlk/p7TKL-01.css" media="all" rel="stylesheet" type="text/css"/>
<script async="" data-ad-client="ca-pub-5875116297643912" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<div class="p7STT right dark" id="p7STT_1"> <a class="p7STT-anchor" href="#" id="p7STTa_1"><i></i><span>Scroll To Top</span></a>
<script type="text/javascript">P7_STTop('p7STT_1',200,1,450)</script>
</div>
<div class="content">
<div class="p7HMY hmy-border-root-all hmy-noscript" data-hmy-max-width="100,%" id="p7HMY_1">
<div class="hmy-section" data-hmy-basis="25,%,1">
<div class="hmy-content-wrapper hmy-color-content-black logo">
<div class="hmy-content hmy-no-pad hmy-left"><a href="index.html"><img alt="Artyfactory Logo" class="no-border" height="116" src="sitebody/sitebody_img/logos/artyfactory-logo.png" width="306"/></a></div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="50,%,1">
<div class="hmy-content-wrapper hmy-vertical-center hmy-color-content-black">
<div class="hmy-content hmy-no-pad hmy-right"><div class="p7AMM amm-noscript amm-responsive amm-root-black amm-root-border amm-rootlinks-gray amm-rootlink-borders amm-rootlinks-rounded amm-subs-gray amm-right" data-amm="360,1,900,5,-10,0,1,1,0,5,1,0,1,1,0" data-amm-label="More..." id="p7AMM_1">
<div class="amm-toolbar closed" id="p7AMMtb_1"><a href="#" title="Hide/Show Menu">≡</a></div>
<ul aria-label="Navigation Menu" class="closed">
<li data-amm-priority="0"><a href="art-lessons.html">ART LESSONS</a></li>
<li data-amm-priority="0"><a href="art-appreciation.html">ART APPRECIATION</a></li>
<li data-amm-priority="0"><a href="design-lessons.html">DESIGN LESSONS</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="p7HMY hmy-border-root-left-right hmy-noscript" data-hmy-max-width="1300,px" id="p7HMY_3">
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-border-content-top-bottom hmy-color-content-gray">
<div class="hmy-content hmy-left">
<h1>Free Art Lessons</h1>
<p>www.artyfactory.com offers free art and design lessons for all. Learn how to draw, paint and design by following our illustrated step by step instructions. We are building a growing resource of free art tutorials that are designed to improve your artistic skills and to increase your enjoyment in creating artworks.</p>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="google-ads-header">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5875116297643912" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="8243205735" style="display:block; text-align:center;"></ins>
<script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-border-content-top-bottom hmy-color-content-gray">
<div class="hmy-content hmy-left">
<h2><a class="p7TKL TKL tkl-BTN tkl-LC-black tkl-HC-white tkl-VC-black tkl-BGL-white tkl-BGH-black tkl-BGV-white tkl-BRD-dk tkl-rnd tkl-ani" href="art-lessons.html">Art Lessons</a></h2>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="p7FGM fgm-white fgm-borders fgm-rounded fgm-noscript" data-fgm="p7FGM_1,250,px,0" id="p7FGM_1">
<div class="fgm-section">
<div class="fgm-img"><a href="aboriginal-art/aboriginal-art-lessons.html"><img alt="Aboriginal Art Lessons" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/aboriginal-art-buttons/aboriginal-art.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="aboriginal-art/aboriginal-art-lessons.html">Aboriginal Art Lessons</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="africanmasks/index.htm"><img alt="African Masks" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/african_masks/teke-mask.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="africanmasks/index.htm">African Masks</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="egyptian_art/egyptian_art_lessons.htm"><img alt="Ancient Egyptian Art Lessons" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/ancient_egyptian_art/ancient-egyptian-art.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="egyptian_art/egyptian_art_lessons.htm">Ancient Egyptian Art Lessons</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/pencil-portraits/drawing-portraits.html"><img alt="Pencil Portraits" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/pencil_portraits/eye-drawing.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/pencil-portraits/drawing-portraits.html">Pencil Portraits</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/color-pencil-portraits/drawing-color-pencil-portraits.html"><img alt="Color Pencil Portraits" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/color-pencil-portraits/color-pencil-eyes.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/color-pencil-portraits/drawing-color-pencil-portraits.html">Color Pencil Portraits</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/charcoal-portraits/charcoal-portraits.html"><img alt="Charcoal Portraits" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/charcoal-pencil-portraits/charcoal-portrait-eyes.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/charcoal-portraits/charcoal-portraits.html">Charcoal Portraits</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/acrylic-portrait-painting/acrylic-portrait-painting-1.html"><img alt="Acrylic Portrait Painting" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/portrait_painting/acrylic-portrait-painting.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/acrylic-portrait-painting/acrylic-portrait-painting-1.html">Acrylic Portrait Painting</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="drawing_animals/drawing_animals.htm"><img alt="How to Draw Animals" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/drawing_animals/tiger.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="drawing_animals/drawing_animals.htm">How to Draw Animals</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="still-life/still-life-lessons.html"><img alt="Still Life Lessons" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/still_life_lessons/still-life-painting.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="still-life/still-life-lessons.html">Still Life Lessons</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/pop-art-portraits/pop-art-portrait-lessons.html"><img alt="Pop Art Portraits" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/pop_art_portraits/mona_lisa.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/pop-art-portraits/pop-art-portrait-lessons.html">Pop Art Portraits</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="portraits/chuck-close-project/chuck-close-art-lesson.html"><img alt="Chuck Close Art Project" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/chuck-close-portraits/chuck-close-lesson.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="portraits/chuck-close-project/chuck-close-art-lesson.html">Chuck Close Art Project</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="perspective_drawing/perspective_index.html"><img alt="Perspective Drawing" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/perspective_drawing/perspective-drawing.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="perspective_drawing/perspective_index.html">Perspective Drawing</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="aerial-perspective/aerial-perspective.html"><img alt="Aerial Perspective" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/aerial-perspective/aerial-perspective-1.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="aerial-perspective/aerial-perspective.html">Aerial Perspective</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="pen_and_ink_drawing/pen_and_ink_drawing.htm"><img alt="Pen and Ink Drawing" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/pen_and_ink_drawing/ink_drawing_1.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="pen_and_ink_drawing/pen_and_ink_drawing.htm">Pen and Ink Drawing</a></div>
</div>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-left">
<p><span class="dropcap">A</span>rtyfactory's free lessons share the knowledge, understanding and experience of art and design to improve your artistic skills and to increase your enjoyment in creating artworks. Our lessons explore a variety of drawing, painting and design techniques across a range of subjects and styles. In each lesson we lead you through an illustrated step by step development of the artwork from its initial stages to the finished example. </p>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="google-ads">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5875116297643912" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="8243205735" style="display:block; text-align:center;"></ins>
<script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-border-content-top-bottom hmy-color-content-gray">
<div class="hmy-content hmy-left">
<h2><a class="p7TKL TKL tkl-BTN tkl-LC-black tkl-HC-white tkl-VC-black tkl-BGL-white tkl-BGH-black tkl-BGV-white tkl-BRD-dk tkl-rnd tkl-ani" href="art-appreciation.html">Art Appreciation</a></h2>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="p7FGM fgm-white fgm-borders fgm-rounded fgm-noscript" data-fgm="p7FGM_2,250,px,0" id="p7FGM_2">
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/visual-elements/visual-elements.html"><img alt="The Visual Elements of Art" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/visual-elements/visual-elements.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/visual-elements/visual-elements.html">The Visual Elements of Art</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/art_movements.htm"><img alt="Art Movements" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/pop.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/art_movements.htm">Art Movements</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/italian-renaissance/italian-renaissance-art.html"><img alt="Italian Renaissance Art" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/renaissance-fresco-painting.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/italian-renaissance/italian-renaissance-art.html">Italian Renaissance Art</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/impressionism.htm"><img alt="Impressionism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/impressionism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/impressionism.htm">Impressionism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/post_impressionism.htm"><img alt="Post Impressionism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/post-impressionism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/post_impressionism.htm">Post Impressionism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/cubism.htm"><img alt="Cubism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/cubism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/cubism.htm">Cubism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/fauvism.htm"><img alt="Fauvism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/fauvism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/fauvism.htm">Fauvism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/expressionism.htm"><img alt="Expressionism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/expressionism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/expressionism.htm">Expressionism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/dadaism.htm"><img alt="Dadaism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/dadaism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/dadaism.htm">Dadaism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/surrealism.htm"><img alt="Surrealism" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/surrealism.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/surrealism.htm">Surrealism</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art_movements/pop_art.htm"><img alt="Pop Art" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-movements/roy-lichtenstein.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art_movements/pop_art.htm">Pop Art</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/timelines/art_history_timelines.htm"><img alt="Art History Timelines" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-timelines/art-timelines.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/timelines/art_history_timelines.htm">Art History Timelines</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/art-history-slideshows/art-history-slideshow.html"><img alt="Art History Slideshows" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/art-history-slideshow/art-history-slideshows.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/art-history-slideshows/art-history-slideshow.html">Art History Slide Shows</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/great-artists/great-artists.html"><img alt="Great Artists" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/great-artists/rembrandt.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/great-artists/great-artists.html">Great Artists</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/still_life/still_life.htm"><img alt="Still Life Artists" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/still-life/harmen-steenwyck.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/still_life/still_life.htm">Still Life  Artists</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="art_appreciation/animals_in_art/animals_in_art.htm"><img alt="Animals in Art" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/animals-in-art/franz_marc.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="art_appreciation/animals_in_art/animals_in_art.htm">Animals in Art</a></div>
</div>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-left">
<p><span class="dropcap">A</span>rt appreciation is the knowledge and understanding of the universal and timeless qualities that identify all great art. The more you appreciate and understand the art of different eras, movements, styles and techniques, the better you can develop, evaluate and improve your own artwork. </p>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="google-ads">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5875116297643912" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="8243205735" style="display:block; text-align:center;"></ins>
<script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-border-content-top-bottom hmy-color-content-gray">
<div class="hmy-content hmy-left">
<h2><a class="p7TKL TKL tkl-BTN tkl-LC-black tkl-HC-white tkl-VC-black tkl-BGL-white tkl-BGH-black tkl-BGV-white tkl-BRD-dk tkl-rnd tkl-ani" href="design-lessons.html">Design Lessons</a></h2>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="p7FGM fgm-white fgm-borders fgm-rounded fgm-noscript" data-fgm="p7FGM_3,250,px,0" id="p7FGM_3">
<div class="fgm-section">
<div class="fgm-img"><a href="repeat-patterns/repeat-patterns.html"><img alt="Repeat Patterns" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/repeat-patterns/repeat-pattern-2.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="repeat-patterns/repeat-patterns.html">Repeat Patterns</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="color_theory/color_theory.htm"><img alt="Color Theory" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/color_theory/color-theory.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="color_theory/color_theory.htm">Color Theory</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/graphic-design-workout.html"><img alt="A Graphic Design Workout" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/graphic_design/question-mark-3.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/graphic-design-workout.html">A Graphic Design Workout</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/graphic_design_evaluation.html"><img alt="Evaluating Graphic Designs" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/graphic_design/graphic-design.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/graphic_design_evaluation.html">Evaluating Graphic Designs</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/the_art_of_typography.html"><img alt="The Art of Typography" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/graphic_design/typography.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/the_art_of_typography.html">The Art of Typography</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/designing_a_logotype.html"><img alt="Design a Logotype" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/graphic_design/logotypes.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/designing_a_logotype.html">Design a Logotype</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/logo_design_history.html"><img alt="Logo Design History" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/graphic_design/logo-design-history.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/logo_design_history.html">Logo Design History</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="isometric-drawing/isometric-drawing.html"><img alt="Isometric Design" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/isometric-design/isometric-poster-design.gif" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="isometric-drawing/isometric-drawing.html">Isometric Design</a></div>
</div>
<div class="fgm-section">
<div class="fgm-img"><a href="graphic_design/graphic_designers/graphic_designers.htm"><img alt="Famous Graphic Designers" class="p7IFX" data-ifx="8,0.8,1,4,0.5,0,1,1" height="250" src="sitebody/sitebody_img/buttons/art_appreciation/graphic-designers/cassandre.jpg" width="250"/></a></div>
<div class="fgm-content fgm-cnt-bottom fgm-center"><a href="graphic_design/graphic_designers/graphic_designers.htm">Famous Graphic Designers</a></div>
</div>
</div>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-left">
<p><span class="dropcap">O</span>ur design lessons explore the basic elements of design such as imagery, color, pattern, composition, layout and typography and how to evaluate their effectiveness in the creative process.</p>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-white">
<div class="hmy-content hmy-center">
<div class="google-ads-footer">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5875116297643912" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="8243205735" style="display:block; text-align:center;"></ins>
<script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer">
<div class="p7HMY hmy-noscript" data-hmy-max-width="100,%" id="p7HMY_2">
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-black social">
<div class="hmy-content hmy-center">
<ul class="social-icons">
<li><a href="https://www.facebook.com/artyfactory.freeartlessons" target="_blank"><img alt="Facebook" class="no-border" height="48" src="p7typecrafters/img/social/facebook.png" width="48"/></a></li>
<li><a href="https://www.instagram.com/artyfactory_art_lessons" target="_blank"><img alt="Instagram" class="no-border" height="48" src="p7typecrafters/img/social/instagram.png" width="48"/></a></li>
<li><a href="https://uk.pinterest.com/johnmactaggart9/" target="_blank"><img alt="Instagram" class="no-border" height="48" src="p7typecrafters/img/social/pinterest.png" width="48"/></a></li>
<li><a href="https://twitter.com/artyfactory1" target="_blank"><img alt="Twitter" class="no-border" height="48" src="p7typecrafters/img/social/twitter.png" width="48"/></a></li>
</ul>
</div>
</div>
</div>
<div class="hmy-section" data-hmy-basis="100,%,1">
<div class="hmy-content-wrapper hmy-color-content-black">
<div class="hmy-content hmy-no-pad hmy-center">
<p><a class="p7TKL TKL tkl-BTN tkl-LC-gray tkl-HC-green tkl-VC-gray" href="sitebody/privacy-contact.html" target="blank">Â© www.artyfactory.com</a></p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
