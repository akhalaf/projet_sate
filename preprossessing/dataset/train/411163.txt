<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<base href="Dgaccountingtaxation.com.au"/>
<meta content="DG Accounting &amp; Taxation is a Chartered Accounting firm specializing in business consulting, Self Managed Superannuation, Corporate Secretarial, GST, Accounting, advice, and taxation. Our personalized approach to our valued clients is by working closely with you as a member of your team to achieve your goals." name="Description"/>
<meta content="accounting, tax, accounts, taxation, Danielle, George, Toowong, Brisbane, Chartered account, DG, firm, consulting, GST, advice" name="Keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title>DG Accounting &amp; Taxation - Welcome</title>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/collection.js" type="text/javascript"></script>
<script src="js/jquery-1.2.3.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.min.js" type="text/javascript"></script>
<script src="js/jquery.lavalamp.min.js" type="text/javascript"></script>
<script src="js/cta-javascript.js" type="text/javascript"></script>
<script type="text/javascript">
        $(function() {
            $("#1, #2, #3").lavaLamp({
                fx: "backout",
                speed: 700,
                click: function(event, menuItem) {
                    return true;
                }
            });
        });
    </script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23975517-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 6]>
<script src="js/belatedPNG.js"></script>
<script>
	DD_belatedPNG.fix('*');
</script>
<![endif]-->
</head>
<body>
<div id="wrap">
<nav id="mainnav">
<h1 id="textlogo"> <a href="index.html">DG <span>ACCOUNTING &amp; TAXATION</span></a> </h1>
<ul>
<ul class="primary_nav" id="3">
<li class="current"><a href="index.html">Home</a></li>
<li><a href="services.html">Services</a></li>
<li><a href="clients.html">Our clients</a></li>
<li><a href="links.html">Links</a></li>
<li><a href="contact.html">Contact</a></li>
</ul>
<!--<li class="active"><a href="index.html">Home</a></li>
    <li><a href="services.html">Services</a></li>
    <li><a href="products.html">Products</a></li>
    <li><a href="about.html">About</a></li>
    <li><a href="#">Contact</a></li>-->
</ul>
</nav>
<section id="content">
<header id="homeheader">
<h2>
<span>Welcome</span>
DG Accounting &amp; Taxation is a chartered accounting firm offering a full range of business advisory and consulting services accross a wide range of industries and business growith phases.<br/>
<br/>
</h2><p class="cta-button"><a href="contact.html">Sign Up</a></p>
<img alt="header image" class="headerimg" height="218" src="images/hero.png" width="255"/> </header>
<section id="page">
<!--<header class="mainheading">
<h2 class="introhead">Every client is important to us and we understand that, a personal hand on service is of utmost significance. </h2>-->
<header class="mainheading">
<h2 class="introhead">Every client is important to us and we understand that, a personal hand on service is of utmost significance.</h2>
</header>
<section id="introcol">
<div class="leftcol">
<h3><img alt="What we do icon" height="48" src="images/icon_2.png" width="48"/><a href="services.html">What we do ?</a></h3>
<p>We are a personal boutique firm with a personalised approach to small businesses and tax planning.  </p> <p><a href="services.html">More</a></p>
</div>
<div class="rightcol">
<h3><img alt="Why choose us icon" height="48" src="images/icon_3.png" width="48"/><a href="contact.html">Why Choose Us ?</a></h3>
<p>We take the time to understand your business needs so that we can give you the most accurate advice possible. </p><p><a href="clients.html">More</a></p>
</div>
<div class="midcol">
<h3><img alt="Our process icon" height="48" src="images/icon_1.png" width="48"/><a href="services.html">Our Process</a></h3>
<p>We work closely with you as a member of your team giving certainty as to your financial positions before the tax year end and planning with you prior to so that your affairs are in order.</p><p><a href="services.html">More</a></p>
</div>
<div class="clear"></div>
</section>
<section id="fourcols">
<div class="clear"></div>
<div class="col">
<h3>World Class Services</h3>
<p>We fostor close working relationships with our clients and work closely with them to achieve their goals.Your business is our business. We partner with leading law, finance, insurance brokers and financial planners to ensure you have access and protection for your business and family - Everything that matters.<a href="services.html">Our Services</a></p>
</div>
<div class="col">
<h3>Affordable Prices</h3>
<p>Our fees are very reasonable. They depend entirely upon size, complexity, number of transactions, and your record keeping.We have streamlined our systems and procedures which allow us to offer competitive rates.<a href="contact.html">Contact Us</a></p>
</div>
<div class="col">
<h3>Great Support</h3>
<p>We are in regular contact with our clients and provide a bespoke service.You will be updated on changes we feel are of interest to you from the regular tax updates and seminars we attend.<a href="clients.html">Our Clients</a></p>
</div>
<div class="col">
<h3>Satisfied Customers</h3>
<p><i>"All of the above taxation activities are done in a professional and understanding mannerI have no hesitation recommending this accounting firm"</i><b>Andrew Fox</b>(Director/Owner - Westwind Roofing)<a href="clients.html">Testimonials</a></p>
</div>
<div class="clear"></div>
<div class="divide">
<p class="ca-button"><a href="http://www.charteredaccountants.com.au/">Chartered Accountants</a></p></div>
</section>
</section>
</section>
</div>
<footer>
<div id="bottom">
<a href="#">Home</a> | <a href="services.html">Services</a> | <a href="clients.html">Our clients</a> |<a href="links.html"> Links</a> | <a href="contact.html">Contact</a>
<div class="clear"></div>
</div>
<div id="credits">
2011 © All Rights Reserved.  </div>
</footer>
</body>
</html>
