<!DOCTYPE HTML>
<!--
	Verti by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
--><html>
<head>
<title>LYBE/BEG METAR and TAF weather</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!--[if lte IE 8]><script src="/assets/js/ie/html5shiv.js"></script><![endif]-->
<link href="/assets/css/main.css?r=4" rel="stylesheet"/>
<!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/ie8.css" /><![endif]-->
<meta content="LYBE/BEG METAR and TAF weather" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="https://www.aviatorjoe.net/go/wx/" property="og:url"/>
<meta content="aviatorjoe.net" property="og:site_name"/>
<meta content="LYBE/BEG METAR and TAF weather" property="og:description"/>
<meta content="190988147627931" property="fb:page_id"/>
<!--<script src="https://aviatorjoe-69898.firebaseapp.com/js/jquery.min.js"></script>-->
<!--<script src="/assets/js/jquery.min.js"></script>-->
<script crossorigin="anonymous" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" src="https://code.jquery.com/jquery-1.12.4.min.js">
</script>
<script src="/assets/js/aviator.js" type="text/javascript"></script>
<script src="/js/jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="/js/jquery.spinbox.js" type="text/javascript"></script>
<script src="/js/jquery.tools.min.js" type="text/javascript"></script>
<meta content="HmqnYjJ9tMAJ9tf28J-5dyN1bhz1EzxDoXQ2GUUt_rE" name="google-site-verification"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
		     (adsbygoogle = window.adsbygoogle || []).push({
		          google_ad_client: "ca-pub-5893471395394463",
		          enable_page_level_ads: false
		     });
		</script>
<script src="//cdn.thisiswaldo.com/static/js/4353.js" type="text/javascript"></script>
<style>
			@media (max-width:736px) {
				  .optionalstuff {
				    display: none;
				  }
				}
		</style>
</head>
<body class="right-sidebar">
<div id="page-wrapper">
<!-- Header -->
<div id="header-wrapper">
<header class="container" id="header">
<!-- Logo -->
<div id="logo">
<img src="https://aviatorjoe-69898.firebaseapp.com/gfx/v2.left.png"/>
</div>
<!-- Nav -->
<nav id="nav">
<ul>
<li class="current"><a href="/go/wx/">METAR/TAF</a></li>
<li><a href="/go/compare/">Aircraft comparison</a></li>
<li><a href="/go/calc/">AviatorCalculator</a></li>
<li>
<a href="/go/airbus/">Aviation codes</a>
<ul>
<li><a href="/go/airbus/">Airbus aircraft models</a></li>
<li><a href="/go/boeing-codes/">Boeing customer codes</a></li>
<!--<li ><a href="/go/airport-codes/">Airport codes</a></li>-->
<li><a href="/go/icao-nation-codes/">ICAO codes classification</a></li>
<li><a href="/go/icao-registration-prefixes/">ICAO registration prefixes</a></li>
<!--<li ><a href="/go/aircraft-types/">Aircraft type designators</a></li>-->
</ul>
</li>
<li><a href="/app/">WXfeed</a></li>
</ul>
</nav>
</header>
<div id="navc"></div>
</div>
<!-- Main -->
<div id="main-wrapper">
<div class="container">
<div class="row 200%">
<div class="8u 12u$(medium) ">
<div id="content">
<!-- Content -->
<article>
<script type="text/javascript">
	function wx_check()	{
		var filter = /[A-Za-z0-9]{4}$/;
		var txt = document.getElementById('frm_apt').value;
		if(!filter.test(txt)) return false;
	}
	function wx_clear()	{
		document.getElementById('frm_apt').value = "";
	}
	$(document).ready(function(){
		$("span[title]").tooltip({offset: [75, 60]});
		var frm_apt = document.getElementById("frm_apt");
	    frm_apt.focus();
	    frm_apt.value = "LYBE";
	});
</script>
<style>
/* tooltip styling. by default the element to be styled is .tooltip  */
.tooltip {
	display:none;
	font-size:13px;
	height:60px;
	width:200px;
	padding:2px;
	color:black;
	background-color:#f5f5f5;
	border:2px solid #0099FF;
	border-top:2px solid #0099FF;
	padding:4px;
	line-height:15px;
}
p.metar {
	margin:0;
	padding:0.35em;
	text-align:left;
	line-height:20pt;
	font-weight:600;
}
</style>
<h2>LYBE/BEG METAR and TAF weather</h2>
<form action="/go/wx/" method="post" onsubmit="return wx_check();">
<table>
<tr>
<td style="vertical-align:middle;"><span style="font-weight:600">Enter ICAO airport designator</span></td>
<td style="vertical-align:top" width="20%"><input id="frm_apt" maxlength="4" name="frm_apt" onfocus="wx_clear()" type="text" value="LYBE"/></td>
<td width="5%"></td>
<td style="vertical-align:top" width="20%"><input name="wx_submit" style="padding-top:0;padding-bottom:0" type="submit" value="Submit"/></td>
</tr>
</table>
</form>
<table align="center" class="compareTabela">
<tr>
<th width="20%">Station</th>
<th width="20%">Type</th>
<th width="20%">Date</th>
<th width="20%">Time</th>
<th width="20%">Source</th>
</tr>
<tr>
<td>LYBE</td>
<td><span title="&lt;b&gt;MET&lt;/b&gt;eorological &lt;b&gt;A&lt;/b&gt;erodrome &lt;b&gt;R&lt;/b&gt;eport">METAR</span></td>
<td><span>2021/01/13</span></td>
<td><span title="&lt;b&gt;U&lt;/b&gt;niversal &lt;b&gt;T&lt;/b&gt;ime &lt;b&gt;C&lt;/b&gt;oordinated">13:00 UTC</span></td>
<td><span title="&lt;b&gt;N&lt;/b&gt;ational &lt;b&gt;O&lt;/b&gt;ceanic and&lt;br /&gt;&lt;b&gt;A&lt;/b&gt;tmospheric &lt;b&gt;A&lt;/b&gt;dministration">NOAA</span></td>
</tr>
<tr>
<td align="left" colspan="5"><p class="metar">LYBE 131300Z 27013<span title="Nautical miles per hour">KT</span> 7000 <span title="No Significant Cloud">NSC</span> 01/M01 Q1014 R12/0///95 <span title="No Significant Change">NOSIG</span>
</p></td>
</tr>
</table>
<!--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>-->
<!-- Aviatorjoe - responsive - weather -->
<!--<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-5893471395394463"
     data-ad-slot="7486250167"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script><br />-->
<div style="margin-left:auto;margin-right:auto;width:728px"><div id="waldo-tag-4360"></div></div><br/>
<table align="center" class="compareTabela">
<tr>
<th width="20%">Station</th>
<th width="20%">Type</th>
<th width="20%">Date</th>
<th width="20%">Time</th>
<th width="20%">Source</th>
</tr>
<tr>
<td>LYBE</td>
<td><span title="Decoded &lt;b&gt;MET&lt;/b&gt;eorological &lt;b&gt;A&lt;/b&gt;erodrome &lt;b&gt;R&lt;/b&gt;eport">Decoded METAR</span></td>
<td><span>2021/01/13</span></td>
<td><span title="&lt;b&gt;U&lt;/b&gt;niversal &lt;b&gt;T&lt;/b&gt;ime &lt;b&gt;C&lt;/b&gt;oordinated">13:00 UTC</span></td>
<td><span title="&lt;b&gt;N&lt;/b&gt;ational &lt;b&gt;O&lt;/b&gt;ceanic and&lt;br /&gt;&lt;b&gt;A&lt;/b&gt;tmospheric &lt;b&gt;A&lt;/b&gt;dministration">NOAA</span></td>
</tr>
<tr>
<td align="left" colspan="5"><p class="metar">Beograd / Surcin, Serbia and Montenegro (LYBE) 44-49N 020-17E 99M<br/>
Jan 13, 2021 - 08:00 AM EST / 2021.01.13 1300 UTC<br/>
Wind: from the W (270 degrees) at 15 MPH (13 KT):0<br/>
Visibility: 4 mile(s):0<br/>
Temperature: 33 F (1 C)<br/>
Windchill: 23 F (-5 C):1<br/>
Dew Point: 30 F (-1 C)<br/>
Relative Humidity: 86%<br/>
Pressure (altimeter): 29.94 in. Hg (1014 hPa)<br/>
ob: LYBE 131300Z 27013KT 7000 NSC 01/M01 Q1014 R12/0///95 NOSIG<br/>
cycle: 13<br/>
</p></td>
</tr>
</table>
<table align="center" class="compareTabela" style="margin-bottom: 8px">
<tr>
<th width="20%">Station</th>
<th width="20%">Type</th>
<th width="20%">Date</th>
<th width="20%">Time</th>
<th width="20%">Source</th>
</tr>
<tr>
<td>LYBE</td>
<td><span title="&lt;b&gt;T&lt;/b&gt;erminal &lt;b&gt;A&lt;/b&gt;erodrome &lt;b&gt;F&lt;/b&gt;orecast">TAF</span></td>
<td><span>2021/01/13</span></td>
<td><span title="&lt;b&gt;U&lt;/b&gt;niversal &lt;b&gt;T&lt;/b&gt;ime &lt;b&gt;C&lt;/b&gt;oordinated">12:21 UTC</span></td>
<td><span title="&lt;b&gt;N&lt;/b&gt;ational &lt;b&gt;O&lt;/b&gt;ceanic and&lt;br /&gt;&lt;b&gt;A&lt;/b&gt;tmospheric &lt;b&gt;A&lt;/b&gt;dministration">NOAA</span></td>
</tr>
<tr>
<td align="left" colspan="5"><p class="metar">TAF LYBE 131100Z 1312/1412 28010<span title="Nautical miles per hour">KT</span> 4000 <span title="Mist (&gt;=5/8SM)">BR</span> <span title="Scattered (3/8-4/8 cloud coverage)">SCT</span>030 TX01/1313Z TNM06/1405Z <br/>
<span title="Becoming">BECMG</span> 1312/1314 6000 <span title="No significant Weather">NSW</span> <br/>
<span title="Becoming">BECMG</span> 1317/1319 22006<span title="Nautical miles per hour">KT</span> <br/>
<span title="Temporary fluctuation">TEMPO</span> 1408/1412 4000 <span title="Light precipation">-</span><span title="Snow">SN</span><br/>
</p></td>
</tr>
</table>
<span style="display:block;font-size:12px;line-height: 17px">Example: see <a href="/go/airport-list/" title="more">airport list</a></span> </article>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5893471395394463" data-ad-format="autorelaxed" data-ad-slot="4333033813" style="display:block"></ins>
<script>
										     (adsbygoogle = window.adsbygoogle || []).push({});
										</script>
</div>
</div>
<div class="4u 12u$(medium) important(medium)">
<div class="optionalstuff" id="sidebar">
<!--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>-->
<!-- AviatorJoe - responsive -->
<!--<ins class="adsbygoogle"
										     style="display:block"
										     data-ad-client="ca-pub-5893471395394463"
										     data-ad-slot="5974226168"
										     data-ad-format="auto"></ins>
										<script>
										(adsbygoogle = window.adsbygoogle || []).push({});
										</script>-->
<div id="waldo-tag-4354"></div>
</div>
</div>
</div>
</div>
</div>
<!-- Footer -->
<div id="footer-wrapper">
<footer class="container" id="footer">
<div class="row">
<div class="12u">
<div id="copyright">
<ul class="menu">
<li><a href="/go/contact/">Contact</a></li>
<li><a href="/go/privacy/">Privacy policy</a></li>
<li>Copyright © 2021 aviatorjoe.net</li>
<li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
</ul>
</div>
</div>
</div>
</footer>
</div>
</div>
<!-- Scripts -->
<!--<script src="https://www.gstatic.com/firebasejs/5.5.6/firebase.js"></script>
		<script>
		  // Initialize Firebase
		  var config = {
		    apiKey: "AIzaSyBgSZyyQO2mDfX8REakMdD2sVRwkPopDo8",
		    authDomain: "aviatorjoe-69898.firebaseapp.com",
		    databaseURL: "https://aviatorjoe-69898.firebaseio.com",
		    projectId: "aviatorjoe-69898",
		    storageBucket: "aviatorjoe-69898.appspot.com",
		    messagingSenderId: "540089748342"
		  };
		  firebase.initializeApp(config);
		</script>-->
<script src="/assets/js/jquery.dropotron.min.js"></script>
<script src="/assets/js/skel.min.js"></script>
<script src="/assets/js/util.js"></script>
<!--[if lte IE 8]><script src="/assets/js/ie/respond.min.js"></script><![endif]-->
<script src="/assets/js/main.js"></script>
<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-213524-4', 'aviatorjoe.net');
			ga('require', 'displayfeatures');
			ga('send', 'pageview');
		</script>
</body>
</html>
