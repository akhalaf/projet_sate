<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- This is Squarespace. --><!-- beacon-school -->
<base href=""/>
<meta charset="utf-8"/>
<title>The Beacon School</title>
<link href="https://images.squarespace-cdn.com/content/v1/5823bcb6ff7c501a0fc02603/1584690745482-A04MIE24KKWQ51O8E5ME/ke17ZwdGBToddI8pDm48kPiDXGvsflYyBJyQo1azq4bQ-nt006wJsyyMCxWr4wDHV29j-wAjJSV-OGgQy2_vsdGRJgyZlreXt8p7VdWvZI4WBmahRsLRkAFbY47uPRueGgggKDATri5cmwk9KQI4Lg/favicon.ico?format=50w" rel="shortcut icon" type="image/x-icon"/>
<meta content="The Beacon School" property="og:site_name"/>
<meta content="The Beacon School" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="http://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w" property="og:image"/>
<meta content="The Beacon School" itemprop="name"/>
<meta content="http://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w" itemprop="thumbnailUrl"/>
<link href="http://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w" rel="image_src"/>
<meta content="http://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w" itemprop="image"/>
<meta content="The Beacon School" name="twitter:title"/>
<meta content="http://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="" name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/0q3BJR-XOHkAlTE2rsJTZmyYF63SCoQb8oi8xCLmHsJfeG92fFHN4UJLFRbh52jhWD9twRFKjhJhZcjajD93wAI3Zcw3wRj3wU7FMPG0jcmyjhN0OWFR-eNzdfoDSWmyScmDSeBRZPoRdhXCjcmyjhN0OWFR-eNzdfoDSWmyScmDSeBRZPoRdhXCjcmyjhN0ShFGdhNCO1gGOeUzjhBC-eNDifUaiaS0jcmyjhN0OWFR-eNzdfoDSWmyScmDSeBRZPoRdhXCiaiaOc48jAFzd1FR-eNzdfo3-fJwSY4zpe8ljPu0daZyJ6TyZemCde9lihmKJ6Z8iW4zSeI7fbKnMsMMeMI6MKG4f5J7IMMjMkMfH6qJK3IbMg6YJMJ7fbRRHyMMeMX6MKG4fJ3gIMMjIPMfH6qJDR9bMs6IJMJ7fbRbFsMgeMj6MKG4fFIVIMIjgkMfH6qJDD9bMs65JMJ7fbRP2UMgegI6MKG4fH8oIMJjMkMfH6qJ71qbMy6IJMJ7fbKGpsMfeMS6MKGHf55eMsMfeMX6MKGHf5AeMsMfegI6MTMg_V-nv39.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"></script><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8b244fce99594deac3684-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-9d33505677455a9b22add-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-82b361c64e6e75913711e-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-1b8d1adb275f098d8dc6d-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-9fd3c3147f23827502474-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-f51d938650dc82e600503-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-415312b37ef6978cf711b-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-da71321e53a08371a214c-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-9fe4eb33891804b4464fe-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-0149a748bc121034a13df-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-ce2b51182ccd8ac6accc6-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-5426bac76cb8bd5a92e8b-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-ac207ca39a69cb84f84f0-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-638ba2bf8ec524b820947-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-32cc08f2dd7137611cfc4-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-146a4d691830282d9ce5a-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-ee4869629f6a684b35f94-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-9e5a6a309dfd7e877bf6f-min.en-US.js"}},"pageType":100,"website":{"id":"5823bcb6ff7c501a0fc02603","identifier":"beacon-school","websiteType":1,"contentModifiedOn":1605776820563,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"Asia/Manila","machineTimeZoneOffset":28800000,"timeZoneOffset":28800000,"timeZoneAbbr":"PST","siteTitle":"The Beacon School","fullSiteTitle":"The Beacon School","siteTagLine":"A FIlipino Community IB World School","siteDescription":"<p>The Beacon School. A Filipino Community IB World School</p>","location":{"mapLat":14.5284744,"mapLng":121.02490369999998,"addressTitle":"The Beacon International School","addressLine1":"2332 Chino Roces Avenue Extension","addressLine2":"Taguig, NCR, ","addressCountry":"Philippines"},"logoImageId":"582abe81440243c12b30316e","shareButtonOptions":{"8":true,"2":true,"7":true,"6":true,"3":true,"1":true,"4":true},"logoImageUrl":"//static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/","authenticUrl":"https://www.beaconschool.ph","internalUrl":"https://beacon-school.squarespace.com","baseUrl":"https://www.beaconschool.ph","primaryDomain":"www.beaconschool.ph","sslSetting":3,"isHstsEnabled":false,"typekitId":"","statsMigrated":false,"imageMetadataProcessingEnabled":false,"screenshotId":"08aa676a257b5d8d9a18e546fbe42fea94035722c8a68969a434316080a89d60","captchaSettings":{"siteKey":"6Ldo8fwSAAAAAP2v8ESWSFeQ1xvjsJBNSbgJCKHw","enabledForDonations":false},"showOwnerLogin":false},"websiteSettings":{"id":"5823bcb6ff7c501a0fc02605","websiteId":"5823bcb6ff7c501a0fc02603","type":"Non-Profit","subjects":[],"country":"PH","state":"00","simpleLikingEnabled":true,"mobileInfoBarSettings":{"isContactEmailEnabled":false,"isContactPhoneNumberEnabled":false,"isLocationEnabled":false,"isBusinessHoursEnabled":false},"announcementBarSettings":{"style":2,"text":"<p><a href=\"/new-page-1\">CONTACT</a>  |  <a href=\"https://beaconschool.managebac.com/\" target=\"_blank\">MANAGEBAC</a>  |  <a href=\"/community\" target=\"_blank\">COMMUNITY</a>  |  <a href=\"/calendar\">CALENDAR</a></p>","clickthroughUrl":{"url":"/community","newWindow":true}},"commentLikesAllowed":true,"commentAnonAllowed":true,"commentThreaded":true,"commentApprovalRequired":false,"commentAvatarsOn":true,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":true,"commentEnableByDefault":true,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"contactPhoneNumber":"632 8840-5040","businessHours":{"monday":{"text":"8am to 4pm","ranges":[{"from":480,"to":960}]},"tuesday":{"text":"8am to 4pm","ranges":[{"from":480,"to":960}]},"wednesday":{"text":"8am to 4pm","ranges":[{"from":480,"to":960}]},"thursday":{"text":"8am to 4pm","ranges":[{"from":480,"to":960}]},"friday":{"text":"8am to 4pm","ranges":[{"from":480,"to":960}]},"saturday":{"text":"","ranges":[{}]},"sunday":{"text":"","ranges":[{}]}},"storeSettings":{"returnPolicy":null,"termsOfService":null,"privacyPolicy":null,"expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"PH","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":true,"isBillingPhoneRequired":false,"currenciesSupported":["CHF","HKD","MXN","EUR","DKK","USD","CAD","MYR","NOK","THB","AUD","SGD","ILS","PLN","GBP","CZK","SEK","NZD","PHP","RUB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":false,"contactLocation":{"addressLine1":"2332 Chino Roces Avenue Extension  ","addressLine2":"Taguig, NCR, ","addressCountry":"Philippines"},"businessName":"Beacon School International School Foundation, Inc.","sameAsRetailLocation":false,"businessId":"","merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":false,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"disabled"},"ampEnabled":false},"cookieSettings":{"isCookieBannerEnabled":true,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"aspect-ratio":"Auto","bannerImageHeight":"443px","gallery-arrow-style":"No Background","gallery-aspect-ratio":"3:2 Standard","gallery-auto-crop":"true","gallery-autoplay":"true","gallery-design":"Slideshow","gallery-info-overlay":"Show on Hover","gallery-loop":"true","gallery-navigation":"Circles","gallery-show-arrows":"false","gallery-transitions":"Scroll","galleryArrowBackground":"rgba(34,34,34,1)","galleryArrowColor":"rgba(255,255,255,1)","galleryAutoplaySpeed":"3","galleryCircleColor":"rgba(232,232,232,1)","galleryInfoBackground":"rgba(0, 0, 0, .7)","galleryThumbnailSize":"100px","gridSize":"350px","gridSpacing":"10px","product-gallery-auto-crop":"true","product-image-auto-crop":"true","tweak-v1-related-products-title-spacing":"50px"},"templateId":"5093f261e4b0979eac7cb299","templateVersion":"7","pageFeatures":[1,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/5093f258e4b0979eac7cb197/3315/scripts/","betaFeatureFlags":["nested_categories_migration_enabled","ORDERS-SERVICE-reset-digital-goods-access-with-service","crm_campaigns_sending","commerce_setup_wizard","commerce_tax_panel_v2","seven_one_header_editor_update","commerce_pdp_survey_modal","ORDERS-SERVICE-check-digital-good-access-with-service","commerce_activation_experiment_add_payment_processor_card","generic_iframe_loader_for_campaigns","commerce_minimum_order_amount","seven-one-main-content-preview-api","customer_notifications_panel_v2","local_listings","commerce_instagram_product_checkout_links","domain_deletion_via_registrar_service","events_panel_70","dg_downloads_from_fastly","domains_transfer_flow_hide_preface","domains_transfer_flow_improvements","campaigns_single_opt_in","member_areas_ga","domain_locking_via_registrar_service","domains_use_new_domain_connect_strategy","seven_one_image_effects","domains_universal_search","commerce_afterpay","seven_one_portfolio_hover_layouts","product_composer_feedback_form_on_save","page_interactions_improvements","commerce_demo_products_modal","seven_one_image_overlay_opacity","campaigns_new_sender_profile_page","list_sent_to_groups","commerce_reduce_cart_calculations","domain_info_via_registrar_service","commerce_subscription_order_delay","omit_tweakengine_tweakvalues","domains_allow_async_gsuite","seven_one_frontend_render_page_section","commerce_category_id_discounts_enabled","seven-one-content-preview-section-api","gallery_captions_71","ORDER_SERVICE-submit-subscription-order-through-service","commerce_afterpay_toggle","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","donations_customer_accounts","seven-one-menu-overlay-theme-switcher","animations_august_2020_new_preset","commerce_pdp_edit_mode","seven_one_frontend_render_gallery_section","campaigns_user_templates_in_sidebar","commerce_restock_notifications","commerce_add_to_cart_rate_limiting","newsletter_block_captcha","campaigns_email_reuse_template_flow","commerce-recaptcha-enterprise"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"tzData":{"zones":[[480,"Phil","+08/+09",null]],"rules":{"Phil":[]}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><script type="application/ld+json">{"url":"https://www.beaconschool.ph","name":"The Beacon School","description":"<p>The Beacon School. A Filipino Community IB World School</p>","image":"//static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/","@context":"http://schema.org","@type":"WebSite"}</script><script type="application/ld+json">{"legalName":"Beacon School International School Foundation, Inc.","address":"2332 Chino Roces Avenue Extension\nTaguig, NCR, \nPhilippines","email":"info@beaconschool.ph","telephone":"632 8840-5040","sameAs":[],"@context":"http://schema.org","@type":"Organization"}</script><script type="application/ld+json">{"address":"2332 Chino Roces Avenue Extension  \nTaguig, NCR, \nPhilippines","image":"https://static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/","name":"Beacon School International School Foundation, Inc.","openingHours":"Mo 08:00-16:00, Tu 08:00-16:00, We 08:00-16:00, Th 08:00-16:00, Fr 08:00-16:00, , ","@context":"http://schema.org","@type":"LocalBusiness"}</script><link href="//static1.squarespace.com/static/sitecss/5823bcb6ff7c501a0fc02603/356/5093f261e4b0979eac7cb299/5823c2151b631b02ca6adf31/3315-05142015/1594711563350/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-81407308-2', 'auto');ga('send', 'pageview');</script><!-- End of Squarespace Headers -->
<script src="https://static1.squarespace.com/static/ta/5093f258e4b0979eac7cb197/3315/scripts/site-bundle.js" type="text/javascript"></script>
</head>
<body class="header-icons header-dropdown-contact-or-location-none always-show-nav header--nav-borders title-decoration-none blog-text-width-narrow show-category-navigation event-show-past-events event-thumbnails event-thumbnail-size-32-standard event-date-label event-list-show-cats event-list-date event-list-time event-list-address event-icalgcal-links event-excerpts gallery-design-slideshow aspect-ratio-auto lightbox-style-light gallery-navigation-circles gallery-info-overlay-show-on-hover gallery-aspect-ratio-32-standard gallery-arrow-style-no-background gallery-transitions-scroll gallery-auto-crop gallery-autoplay gallery-loop product-list-titles-under product-list-alignment-center product-item-size-11-square product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark hide-opentable-icons opentable-style-dark small-button-style-outline small-button-shape-rounded medium-button-style-outline medium-button-shape-rounded large-button-style-outline large-button-shape-rounded image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-left image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-outline button-corner-style-rounded tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd not-found-page mobile-style-available logo-image has-location has-description has-phone-number has-email" id="not-found">
<div class="contact-wrapper">
<div class="contact-inner-wrapper">
<div class="sqs-layout sqs-grid-12 columns-12 sqs-locked-layout" data-layout-label="Header Contact Info Block" data-type="block-field" data-updated-on="1413387585375" id="contactBlock"><div class="row sqs-row"><div class="col sqs-col-6 span-6"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-59fffaa48d3b263d8f7e"><div class="sqs-block-content"><h3>Contact Us</h3><p>Use the form on the right to contact us.</p><p>You can edit the text in this area, and change where the contact form on the right submits to, by entering edit mode using the modes on the bottom right. </p></div></div></div><div class="col sqs-col-6 span-6"><div class="sqs-block form-block sqs-block-form" data-block-type="9" id="block-8f2c192e88cef9774dbf"><div class="sqs-block-content">
<div class="form-wrapper">
<div class="form-inner-wrapper">
<form action="https://beacon-school.squarespace.com" autocomplete="on" data-form-id="5823c2151b631b02ca6adf32" data-success-redirect="" method="POST" onsubmit="return (function (form) {
  Y.use('squarespace-form-submit', 'node', function usingFormSubmit(Y) {
    (new Y.Squarespace.FormSubmit(form)).submit({
      formId: '5823c2151b631b02ca6adf32',
      collectionId: '',
      objectName: 'contactBlock'
    });
  });
  return false;
})(this);">
<div class="field-list clear">
<div class="form-item field email required" id="email-yui_3_17_2_1_1413387338948_8123">
<label class="title" for="email-yui_3_17_2_1_1413387338948_8123-field">
              Email Address
              
                <span aria-hidden="true" class="required">*</span>
</label>
<input aria-required="true" autocomplete="email" class="field-element" id="email-yui_3_17_2_1_1413387338948_8123-field" name="email" spellcheck="false" type="email"/>
</div>
<div class="form-item field textarea required" id="textarea-yui_3_17_2_1_1413387338948_8943">
<label class="title" for="textarea-yui_3_17_2_1_1413387338948_8943-field">
              Message
              
                <span aria-hidden="true" class="required">*</span>
</label>
<textarea aria-required="true" class="field-element " id="textarea-yui_3_17_2_1_1413387338948_8943-field"></textarea>
</div>
</div>
<div class=" form-button-wrapper form-button-wrapper--align-left " data-animation-role="button">
<input class="button sqs-system-button sqs-editable-button" type="submit" value="Submit"/>
</div>
<div class="hidden form-submission-text">Thank you!</div>
<div class="hidden form-submission-html" data-submission-html=""></div>
</form>
</div>
</div>
</div></div></div></div></div>
</div>
</div>
<div id="contactInfo">
<div class="contact-inner-wrapper contact-info-inner-wrapper">
<div class="map-image"><a data-latitude="14.5284744" data-longitude="121.02490369999998" data-zoom="" href="//maps.google.com/maps?q=14.5284744,121.02490369999998" target="_blank" title="View map in new window"></a></div>
<div class="address-phone-email">
<p class="site-location">
<span>2332 Chino Roces Avenue Extension</span><br/>
<span>Taguig, NCR, </span><br/>
<span>Philippines</span>
</p>
<p class="phone">632 8840-5040</p>
<p class="email"><a href="mailto:info@beaconschool.ph">info@beaconschool.ph</a></p>
<div class="site-description" data-content-field="site-description"><p>The Beacon School. A Filipino Community IB World School</p></div>
</div>
</div>
</div>
<nav class="mobile-nav" id="mobile-navigation">
<div class="nav-wrapper" data-content-field="navigation-mobileNav">
<ul>
<li class=""><a href="/home">
      Home
    </a></li>
<li class=""><a href="/vision-mission">
      About Us
    </a></li>
<li class=""><a href="/admissions-overview">
      Admissions
    </a></li>
<li class=""><a href="/pyp">
      Programmes and Services
    </a></li>
<li class=""><a href="/overview-co-curricular">
      Co-Curricular Programmes
    </a></li>
<li class=""><a href="/highlights">
      The Beacon Experience
    </a></li>
<li class=""><a href="/virtual-learning">
      Virtual Learning
    </a></li>
</ul>
</div>
</nav>
<div class="opacity-overlay"></div>
<div class="outer-wrapper">
<div class="wrapper cf">
<div id="mobile-header">
<a class="icon-menu" id="mobileMenu"></a><!-- comment the space between elements because science
          --><div class="site-title-wrapper">
<h1 class="site-title" data-content-field="site-title">
<a class="home-link" href="/">The Beacon School</a>
</h1>
</div><!-- comment the space between elements because science
          --><div class="info-email-wrapper">
<a class="icon-info" id="info-mobile"><span class="icon-text">Info</span></a>
<div id="email-mobile">
<a class="icon-email"><span class="icon-text">Email</span></a>
</div>
<div id="search-mobile">
<a class="icon-search" data-source="template" href="/search"><span class="icon-text">Search</span></a>
</div>
</div>
</div>
<header id="header">
<!--MAIN NAVIGATION-->
<nav class="main-nav" id="main-navigation">
<div class="info-title-email-wrapper">
<a class="icon-menu" id="desktopMenu"><span class="icon-text">Menu</span></a>
<div class="site-title-wrapper">
<h1 class="site-title" data-content-field="site-title">
<div class="logo-wrapper"><a class="home-link" href="/"><img alt="The Beacon School" class="logo" src="//static1.squarespace.com/static/5823bcb6ff7c501a0fc02603/t/582abe81440243c12b30316e/1605776820563/?format=1500w"/></a></div>
<div class="logo-image-title"><a class="home-link" href="/">The Beacon School</a></div>
</h1>
</div>
<a class="icon-info" id="info"><span class="icon-text">Info</span></a>
<div id="email">
<a class="icon-email"><span class="icon-text">Email</span></a>
</div>
<div id="headerSearch">
<a class="icon-search" data-source="template" href="/search"><span class="icon-text">Search</span></a>
</div>
</div>
<div class="nav-wrapper" data-content-field="navigation-mainNav">
<ul class="cf">
<li class=""><a href="/home">
      Home
    </a></li>
<li class=""><a href="/vision-mission">
      About Us
    </a></li>
<li class=""><a href="/admissions-overview">
      Admissions
    </a></li>
<li class=""><a href="/pyp">
      Programmes and Services
    </a></li>
<li class=""><a href="/overview-co-curricular">
      Co-Curricular Programmes
    </a></li>
<li class=""><a href="/highlights">
      The Beacon Experience
    </a></li>
<li class=""><a href="/virtual-learning">
      Virtual Learning
    </a></li>
</ul>
</div>
</nav>
</header>
<!--CONTENT INJECTION POINT-->
<section id="content">
<div class="content-inner-wrapper">
<div class="main-content-wrapper" data-collection-id="" data-content-field="main-content" data-edit-main-image="Banner">
<p>We couldn't find the page you were looking for. This is either because:</p>
<ul>
<li>There is an error in the URL entered into your web browser. Please check the URL and try again.</li>
<li>The page you are looking for has been moved or deleted.</li>
</ul>
<p>
  You can return to our homepage by <a href="/">clicking here</a>, or you can try searching for the
  content you are seeking by <a href="/search">clicking here</a>.
</p>
</div>
</div>
</section>
</div> <!-- end .wrapper -->
<div class="footer-nav">
<nav class="" data-content-field="navigation-footer-navigation" id="footer-navigation">
<div class="nav">
<ul class="cf">
<li class=""><a href="/site-map-1">Site Map</a></li>
<li class=""><a href="/new-page-1">Contact</a></li>
<li class=""><a href="https://beaconschool.managebac.com/login" target="_blank">
                Managebac
              </a></li>
<li class=""><a href="/community">
                Community
              </a></li>
<li class=""><a href="/calendar">
                Calendar
              </a></li>
<li class=""><a href="/teaching-at-beacon">
                CAREERS
              </a></li>
<li class=""><a href="/virtual-learning">
                Virtual Learning
              </a></li>
</ul>
</div>
</nav>
</div>
<script>
    Y.use('node', function() {
      Y.on('domready', function() {
        if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        var move = false;
        Y.all('.folder-child a').each(function(a) {
          a.on('touchmove', function()
          { move = true; }
          );
          a.on('touchend', function() {
            if (move === false)
            { window.location = a.getAttribute('href'); }
          });
        });
        }
      });
    });
  </script>
<footer class="cf" id="footer">
<div class="sqs-layout sqs-grid-1 columns-1" data-layout-label="Left Footer Content" data-type="block-field" data-updated-on="1559194685454" id="footerBlockLeft"><div class="row sqs-row"><div class="col sqs-col-1 span-1"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_2_1482238520807_14291"><div class="sqs-block-content"><p class="" style="white-space:pre-wrap;">The Beacon International School Foundation<br/>PCPD Bldg., 2332 Chino Roces Ext.,<br/>Taguig City, Philippines, 1603</p><p class="" style="white-space:pre-wrap;"><a href="https://www.ibo.org/school/002224/" target="_blank">IB School Code: 002224</a></p></div></div></div></div></div>
<div class="sqs-layout sqs-grid-1 columns-1" data-layout-label="Right Footer Content" data-type="block-field" data-updated-on="1535585134867" id="footerBlockRight"><div class="row sqs-row"><div class="col sqs-col-1 span-1"><div class="sqs-block image-block sqs-block-image" data-block-type="5" id="block-yui_3_17_2_26_1483932518168_24736"><div class="sqs-block-content">
<div class=" image-block-outer-wrapper layout-caption-hidden design-layout-inline combination-animation-none individual-animation-none individual-text-animation-none " data-test="image-block-inline-outer-wrapper">
<figure class=" sqs-block-image-figure intrinsic " style="max-width:80.0px;">
<a class=" sqs-block-image-link " href="https://www.ibo.org/school/002224/" target="_blank">
<div class=" image-block-wrapper has-aspect-ratio " data-animation-role="image" style="padding-bottom:98.75%;">
<noscript><img alt="ib-world-school-logo-white-solid-rev.png" src="https://images.squarespace-cdn.com/content/v1/5823bcb6ff7c501a0fc02603/1483937528076-4661SJR6M444OXLVLMZL/ke17ZwdGBToddI8pDm48kLai1oJEYDWs1nvc_9h2GzmoCXeSvxnTEQmG4uwOsdIceAoHiyRoc52GMN5_2H8Wp2rbJSMB-zdgTc-GJyYc6p59XcpTcjChRgOBIJZsxj74Xy9Y99TUvrgdStHMozAKMA/ib-world-school-logo-white-solid-rev.png"/></noscript><img alt="ib-world-school-logo-white-solid-rev.png" class="thumb-image" data-image="https://images.squarespace-cdn.com/content/v1/5823bcb6ff7c501a0fc02603/1483937528076-4661SJR6M444OXLVLMZL/ke17ZwdGBToddI8pDm48kLai1oJEYDWs1nvc_9h2GzmoCXeSvxnTEQmG4uwOsdIceAoHiyRoc52GMN5_2H8Wp2rbJSMB-zdgTc-GJyYc6p59XcpTcjChRgOBIJZsxj74Xy9Y99TUvrgdStHMozAKMA/ib-world-school-logo-white-solid-rev.png" data-image-dimensions="80x79" data-image-focal-point="0.5,0.5" data-image-id="587316f7e58c62c2b15d7ec0" data-load="false" data-src="https://images.squarespace-cdn.com/content/v1/5823bcb6ff7c501a0fc02603/1483937528076-4661SJR6M444OXLVLMZL/ke17ZwdGBToddI8pDm48kLai1oJEYDWs1nvc_9h2GzmoCXeSvxnTEQmG4uwOsdIceAoHiyRoc52GMN5_2H8Wp2rbJSMB-zdgTc-GJyYc6p59XcpTcjChRgOBIJZsxj74Xy9Y99TUvrgdStHMozAKMA/ib-world-school-logo-white-solid-rev.png" data-type="image"/>
</div>
</a>
</figure>
</div>
</div></div></div></div></div>
</footer>
</div> <!-- end .outer-wrapper -->
<!--INJECTION POINT FOR TRACKING SCRIPTS AND USER CONTENT FROM THE CODE INJECTION TAB-->
<script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script>
</body>
</html>
