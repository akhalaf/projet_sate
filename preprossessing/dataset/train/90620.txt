<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="fi">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="fi">
<![endif]--><!--[if !(IE 7) & !(IE 8)]><!--><html lang="fi">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://ak-isannointi.fi/xmlrpc.php" rel="pingback"/>
<title>Sivua ei löydy – AK-isännöinti Oy – Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://ak-isannointi.fi/feed/" rel="alternate" title="AK-isännöinti Oy - Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla » syöte" type="application/rss+xml"/>
<link href="https://ak-isannointi.fi/comments/feed/" rel="alternate" title="AK-isännöinti Oy - Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla » kommenttien syöte" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ak-isannointi.fi\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://ak-isannointi.fi/wp-content/plugins/formidable/css/formidableforms.css?ver=10141349" id="formidable-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ak-isannointi.fi/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ak-isannointi.fi/wp-content/themes/spacious/style.css?ver=5.6" id="chld_thm_cfg_parent-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ak-isannointi.fi/wp-content/themes/spacious-child/style.css?ver=5.6" id="spacious_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ak-isannointi.fi/wp-content/themes/spacious/genericons/genericons.css?ver=3.3.1" id="spacious-genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato&amp;ver=5.6" id="google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script id="jquery-core-js" src="https://ak-isannointi.fi/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://ak-isannointi.fi/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="spacious-custom-js" src="https://ak-isannointi.fi/wp-content/themes/spacious/js/spacious-custom.js?ver=5.6" type="text/javascript"></script>
<link href="https://ak-isannointi.fi/wp-json/" rel="https://api.w.org/"/><link href="https://ak-isannointi.fi/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://ak-isannointi.fi/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
</head>
<body class="error404 left-sidebar " data-rsssl="1">
<div class="hfeed site" id="page">
<header class="site-header clearfix" id="masthead">
<div id="header-text-nav-container">
<div class="inner-wrap">
<div class="clearfix" id="header-text-nav-wrap">
<div id="header-left-section">
<div id="header-logo-image">
<a href="https://ak-isannointi.fi/" rel="home" title="AK-isännöinti Oy – Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla"><img alt="AK-isännöinti Oy – Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla" src="https://ak-isannointi.fi/wp/wp-content/uploads/logoperus_ak-isannointi-2013-300x71.jpg"/></a>
</div><!-- #header-logo-image -->
</div><!-- #header-left-section -->
<div id="header-right-section">
<div class="clearfix" id="header-right-sidebar">
<aside class="widget widget_search" id="search-2"><form action="https://ak-isannointi.fi/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Etsi" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform --></aside> </div>
<nav class="main-navigation" id="site-navigation" role="navigation">
<h3 class="menu-toggle">Valikko</h3>
<div class="menu-ylavalikko-container"><ul class="menu" id="menu-ylavalikko"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-91" id="menu-item-91"><a href="https://ak-isannointi.fi/">Etusivu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124" id="menu-item-124"><a href="https://ak-isannointi.fi/palvelut/">Palvelut</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10" id="menu-item-10"><a href="https://ak-isannointi.fi/yhteystiedot/">Yhteystiedot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191" id="menu-item-191"><a href="https://ak-isannointi.fi/henkilosto/">Henkilöstö</a></li>
</ul></div><div class="menu-mobiilisivut-container"><ul class="menu" id="menu-mobiilisivut"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-177" id="menu-item-177"><a href="https://ak-isannointi.fi/">Etusivu</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-178" id="menu-item-178"><a href="https://ak-isannointi.fi/palvelut/">Palvelut</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-179" id="menu-item-179"><a href="https://ak-isannointi.fi/yhteystiedot/">Yhteystiedot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-180" id="menu-item-180"><a href="https://ak-isannointi.fi/vesimittarin-luenta/">Vesimittarin lukema</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181" id="menu-item-181"><a href="https://ak-isannointi.fi/muutto-saapumisilmoitus/">Muuttoilmoitus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-182" id="menu-item-182"><a href="https://ak-isannointi.fi/vikailmoitus/">Vikailmoitus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-183" id="menu-item-183"><a href="https://ak-isannointi.fi/muutostyoilmoitus/">Muutostyöilmoitus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184" id="menu-item-184"><a href="https://ak-isannointi.fi/vuokrahakemus/">Vuokra-asuntohakemus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-185" id="menu-item-185"><a href="https://ak-isannointi.fi/isannoitsijantodistus/">Isännöitsijäntodistus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-186" id="menu-item-186"><a href="https://ak-isannointi.fi/talonkirjaote/">Talonkirjaote</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187" id="menu-item-187"><a href="https://ak-isannointi.fi/muuttajan-muistilista/">Muuttajan muistilista</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189" id="menu-item-189"><a href="https://ak-isannointi.fi/henkilosto/">Henkilöstö</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-225" id="menu-item-225"><a href="https://ak-isannointi.fi/yhteydenottolomake/">Yhteydenottolomake</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-253" id="menu-item-253"><a href="https://ak-isannointi.fi/kva/">Karstulan Vuokra-asunnot Oy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-399" id="menu-item-399"><a href="https://ak-isannointi.fi/kva/vuokra-asunnot/">Karstulan vuokra-asuntojen huoneistot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-402" id="menu-item-402"><a href="https://ak-isannointi.fi/kva/vapaat-huoneistot2/">Karstulan vapaat huoneistot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432" id="menu-item-432"><a href="https://ak-isannointi.fi/taloyhtion-vastuunjakotaulukko/">Taloyhtiön vastuunjakotaulukko</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-549" id="menu-item-549"><a href="https://ak-isannointi.fi/kva/vapaat-huoneistot/">Karstulan vapaat huoneistot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-606" id="menu-item-606"><a href="https://ak-isannointi.fi/hairioilmoitus/">Häiriöilmoitus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-671" id="menu-item-671"><a href="https://ak-isannointi.fi/vok-vesimittarin-lukema/">Vesiosuuskunnan vesimittarin lukema</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1296" id="menu-item-1296"><a href="https://ak-isannointi.fi/tahko-lomaosake/">Tahkolla vuokrattavat ja myynnissä olevat lomaosakkeet</a></li>
</ul></div> </nav>
</div><!-- #header-right-section -->
</div><!-- #header-text-nav-wrap -->
</div><!-- .inner-wrap -->
</div><!-- #header-text-nav-container -->
<div class="header-post-title-container clearfix" style="background-image:url('https://ak-isannointi.fi/wp-content/uploads/olohuone-1024x363.jpg'); background-size:cover; background-position:center center;">
<div class="inner-wrap">
<div class="post-title-wrapper">
<h1 class="header-post-title-class">Sivua ei löydy</h1>
</div>
</div>
</div>
</header>
<div class="clearfix" id="main">
<div class="inner-wrap">
<div id="primary">
<div class="clearfix" id="content">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h1 class="page-title">Hups! Kyseistä sivua ei löydy.</h1>
</header>
<p>Voi harmi! Sivua ei löytynyt. Palaa takaisin tai kokeile hakua.</p>
<form action="https://ak-isannointi.fi/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Etsi" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform -->
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div><!-- #content -->
</div><!-- #primary -->
<div id="secondary">
<aside class="widget widget_pages" id="pages-2"><h3 class="widget-title"><span>Sivut</span></h3>
<ul>
<li class="page_item page-item-252 page_item_has_children"><a href="https://ak-isannointi.fi/kva/">Karstulan Vuokra-asunnot Oy</a>
<ul class="children">
<li class="page_item page-item-397"><a href="https://ak-isannointi.fi/kva/vuokra-asunnot/">Karstulan vuokra-asuntojen huoneistot</a></li>
<li class="page_item page-item-548"><a href="https://ak-isannointi.fi/kva/vapaat-huoneistot/">Karstulan vapaat huoneistot</a></li>
<li class="page_item page-item-507"><a href="https://ak-isannointi.fi/kva/asuntohakemus/">Karstulan vuokra-asuntojen asuntohakemus</a></li>
<li class="page_item page-item-936"><a href="https://ak-isannointi.fi/kva/vuokrasuhteen-paattyessa-ohje/">Vuokrasuhteesi päättyy -ohje</a></li>
<li class="page_item page-item-859"><a href="https://ak-isannointi.fi/kva/varastot/">Varastot</a></li>
</ul>
</li>
<li class="page_item page-item-1295"><a href="https://ak-isannointi.fi/tahko-lomaosake/">Tahkolla vuokrattavat ja myynnissä olevat lomaosakkeet</a></li>
<li class="page_item page-item-431"><a href="https://ak-isannointi.fi/taloyhtion-vastuunjakotaulukko/">Taloyhtiön vastuunjakotaulukko</a></li>
<li class="page_item page-item-224"><a href="https://ak-isannointi.fi/yhteydenottolomake/">Yhteydenottolomake</a></li>
<li class="page_item page-item-78"><a href="https://ak-isannointi.fi/vesimittarin-luenta/">Vesimittarin lukema</a></li>
<li class="page_item page-item-670"><a href="https://ak-isannointi.fi/vok-vesimittarin-lukema/">Vesiosuuskunnan vesimittarin lukema</a></li>
<li class="page_item page-item-76"><a href="https://ak-isannointi.fi/muutto-saapumisilmoitus/">Muuttoilmoitus</a></li>
<li class="page_item page-item-80"><a href="https://ak-isannointi.fi/vikailmoitus/">Vikailmoitus</a></li>
<li class="page_item page-item-82"><a href="https://ak-isannointi.fi/muutostyoilmoitus/">Muutostyöilmoitus</a></li>
<li class="page_item page-item-605"><a href="https://ak-isannointi.fi/hairioilmoitus/">Häiriöilmoitus</a></li>
<li class="page_item page-item-84"><a href="https://ak-isannointi.fi/vuokrahakemus/">Vuokra-asuntohakemus</a></li>
<li class="page_item page-item-86"><a href="https://ak-isannointi.fi/isannoitsijantodistus/">Isännöitsijäntodistus</a></li>
<li class="page_item page-item-88"><a href="https://ak-isannointi.fi/talonkirjaote/">Talonkirjaote</a></li>
<li class="page_item page-item-90"><a href="https://ak-isannointi.fi/muuttajan-muistilista/">Muuttajan muistilista</a></li>
</ul>
</aside>
<aside class="widget widget_recent_entries" id="recent-posts-2">
<h3 class="widget-title"><span>Ajankohtaista</span></h3>
<ul>
<li>
<a href="https://ak-isannointi.fi/keski-suomen-taloyhtiopaiva-2018/">Keski-Suomen Taloyhtiöpäivä 2018</a>
<span class="post-date">8.10.2018</span>
</li>
<li>
<a href="https://ak-isannointi.fi/hyvaa-uutta-vuotta-2018/">Hyvää Uutta Vuotta 2018</a>
<span class="post-date">29.12.2017</span>
</li>
<li>
<a href="https://ak-isannointi.fi/karstulan-palvelupiste-on-suljettuna-29-12-2017/">Karstulan palvelupiste on suljettuna 29.12.2017</a>
<span class="post-date">29.12.2017</span>
</li>
<li>
<a href="https://ak-isannointi.fi/toivotamme-kaikille-asiakkaillemme-hyvaa-joulua/">Toivotamme kaikille asiakkaillemme hyvää joulua.</a>
<span class="post-date">22.12.2017</span>
</li>
<li>
<a href="https://ak-isannointi.fi/poikkeuksia-karstulan-palvelupisteen-aukioloajoissa/">Poikkeuksia Karstulan palvelupisteen aukioloajoissa</a>
<span class="post-date">13.12.2017</span>
</li>
</ul>
</aside> </div>
</div><!-- .inner-wrap -->
</div><!-- #main -->
<footer class="clearfix" id="colophon">
<div class="footer-widgets-wrapper">
<div class="inner-wrap">
<div class="footer-widgets-area clearfix">
<div class="tg-one-fourth tg-column-1">
<aside class="widget widget_text" id="text-5"><h3 class="widget-title"><span>Yhteystiedot</span></h3> <div class="textwidget"><p>Isännöitsijä Atro Kallio<br/>
puh 050 528 7768<br/>
sähköposti atro.kallio(at)ak-isannointi.fi</p>
<p>Kiinteistösihteeri, kirjanpitäjä<br/>
Riitta Levänen puh 040 5808658<br/>
sähköposti Riitta Levänen(at)ak-isannointi.fi</p>
</div>
</aside> </div>
<div class="tg-one-fourth tg-column-2">
<aside class="widget widget_text" id="text-4"><h3 class="widget-title"><span>Toimisto</span></h3> <div class="textwidget"><p><strong>Saarijärvellä:</strong><br/>
Kauppakatu 5 G II-kerros<br/>
43100 Saarijärvi</p>
<p><strong>Palvelupiste Karstulassa:</strong><br/>
Yritystalo Kuhila<br/>
Keskustie 8, 43500 Karstula</p>
<p>toimisto puh. 040 5808658<br/>
toimisto(at)ak-isannointi.fi</p>
</div>
</aside> </div>
<div class="tg-one-fourth tg-after-two-blocks-clearfix tg-column-3">
<aside class="widget widget_text" id="text-2"><h3 class="widget-title"><span>Postiosoite</span></h3> <div class="textwidget"><p>PL 41, Kauppakatu 5 G II-kerros<br/>
43101 Saarijärvi</p>
<p>Toimistomme sijaitsee Saarijärven keskustassa osoitteessa Kauppakatu 5 (Säästökeskus) II-kerros,<br/>
toimistoomme on oma ovi Kauppakadulta kauppakäytävän ovista 7 metriä vasemmalle.</p>
</div>
</aside> </div>
<div class="tg-one-fourth tg-one-fourth-last tg-column-4">
<aside class="widget widget_text" id="text-3"><h3 class="widget-title"><span>Aukioloajat</span></h3> <div class="textwidget"><p>Saarijärven Toimistomme on avoinna arkisin klo 9-16.</p>
<p>Karstulan palvelupiste on avoinna arkipäivisin vähintään 4 tunnin ajan.</p>
<p>Satunnaisesti toimistomme/palvelupisteemme saattaa olla kiinni lomien tai sairastapausten vuoksi. Tällöin meidät kuitenkin tavoittaa puhelimitse.</p>
</div>
</aside> </div>
</div>
</div>
</div>
<div class="footer-socket-wrapper clearfix">
<div class="inner-wrap">
<div class="footer-socket-area">
<div class="copyright">Copyright © 2015 AK-isännöinti Oy – Asiakaslähtöistä ammatti-isännöintiä Saarijärven seutukunnalla.</div>
<nav class="small-menu clearfix">
</nav>
</div>
</div>
</div>
</footer>
<a href="#masthead" id="scroll-up"></a>
</div><!-- #page -->
<script id="spacious-navigation-js" src="https://ak-isannointi.fi/wp-content/themes/spacious/js/navigation.js?ver=5.6" type="text/javascript"></script>
<script id="wp-embed-js" src="https://ak-isannointi.fi/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>