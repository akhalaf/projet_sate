<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Client Area - Socialpire</title>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet"/>
<link href="/templates/swiftmodders/css/swiftmodders.css?v=835f5f" rel="stylesheet"/>
<script type="text/javascript">
    var csrfToken = '6dbf3d9a5ce3e48338658e3dc12d7f9d1f089893',
        markdownGuide = 'Markdown Guide',
        locale = 'en',
        saved = 'saved',
        saving = 'autosaving',
		whmcsBaseUrl = "",
        requiredText = 'Required',
        recaptchaSiteKey = "6LcPXxIUAAAAAPGBEpRxin6mO3gDEfe8ZF2370xp";
</script>
<script src="/templates/swiftmodders/js/swiftmodders.min.js?v=dc43e3"></script>
<!-- Dynamic Template Compatibility -->
<!-- Please update your theme to include or have a comment on the following to negate dynamic inclusion -->
<link href="/assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css"/>
</head>
<body class="login" data-phone-cc-input="1">
<div class="sm-loading-container">
<div class="sm-loading">
<div class="loader">
<svg class="circular" viewbox="25 25 50 50">
<circle class="path" cx="50" cy="50" fill="none" r="20" stroke-miterlimit="10" stroke-width="2"></circle>
</svg>
</div>
</div>
</div>
<div class="sm-content-container">
<div class="sm-content main-content">
<div class="sm-login-box">
<div class="logo"><a href="https://billing.socialpire.com"><img alt="Socialpire" class="img-responsive" src="https://i.imgur.com/k8rmFSIr.png"/></a></div>
<div class="panel panel-default">
<div class="panel-body"><div class="sm-page-heading">
<h1>Login</h1>
<small>This page is restricted</small></div> <div class="providerLinkingFeedback"></div>
<form action="https://billing.socialpire.com/dologin.php" method="post" role="form">
<input name="token" type="hidden" value="6dbf3d9a5ce3e48338658e3dc12d7f9d1f089893"/>
<div class="form-group">
<div class="input-group">
<div class="input-group-addon"><i aria-hidden="true" class="fa fa-envelope"></i></div>
<label class="hidden" for="inputEmail">Email Address</label>
<input autofocus="" class="form-control" id="inputEmail" name="username" placeholder="Email Address" type="email"/>
</div>
</div>
<div class="form-group">
<div class="input-group">
<div class="input-group-addon"><i aria-hidden="true" class="fa fa-key"></i></div>
<label class="hidden" for="inputPassword">Password</label>
<input autocomplete="off" class="form-control" id="inputPassword" name="password" placeholder="Password" type="password"/>
</div>
</div>
<div class="checkbox">
<label>
<input name="rememberme" type="checkbox"/>
                  Remember Me </label>
</div>
<div class="text-center margin-bottom"> </div>
<hr/>
<div class="actions">
<input class="btn btn-primary" id="login" type="submit" value="Login"/>
<a class="btn btn-default" href="/register.php"><i class="fas fa-key"></i> Register</a>
<br/>
<a class="small-text" href="/index.php?rp=/password/reset/begin">Forgot Password?</a></div>
</form>
</div>
</div>
<div class="copyright">2021 © Socialpire.</div>
<br/>
<div class="text-center"><a class="btn btn-primary" data-target="#languageChooserModal" data-toggle="modal"><i class="far fa-language"></i> <span class="text">English</span></a></div> </div>
<div class="bg-overlay"></div>
</div>
</div>
<div aria-hidden="true" class="modal fade" id="languageChooserModal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content panel-primary">
<div class="modal-header panel-heading">
<button class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">×</span> <span class="sr-only">Close</span> </button>
<h4 class="modal-title"><i class="far fa-language"></i> Choose language</h4>
</div>
<div class="modal-body panel-body">
<div class="row"> <div class="col-md-4"><a href="/clientarea.php?language=arabic">العربية</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=azerbaijani">Azerbaijani</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=catalan">Català</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=chinese">中文</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=croatian">Hrvatski</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=czech">Čeština</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=danish">Dansk</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=dutch">Nederlands</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=english">English</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=estonian">Estonian</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=farsi">Persian</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=french">Français</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=german">Deutsch</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=hebrew">עברית</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=hungarian">Magyar</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=italian">Italiano</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=macedonian">Macedonian</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=norwegian">Norwegian</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=portuguese-br">Português</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=portuguese-pt">Português</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=romanian">Română</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=russian">Русский</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=spanish">Español</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=swedish">Svenska</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=turkish">Türkçe</a></div>
<div class="col-md-4"><a href="/clientarea.php?language=ukranian">Українська</a></div>
</div>
</div>
<div class="modal-footer panel-footer">
<button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
</div>
</div>
</div>
</div>
<div class="hidden" id="fullpage-overlay">
<div class="outer-wrapper">
<div class="inner-wrapper">
<img src="/assets/img/overlay-spinner.svg"/>
<br/>
<span class="msg"></span>
</div>
</div>
</div>
<div aria-hidden="true" class="modal system-modal fade" id="modalAjax" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content panel-primary">
<div class="modal-header panel-heading">
<button class="close" data-dismiss="modal" type="button"> <span aria-hidden="true">×</span> <span class="sr-only">Close</span> </button>
<h4 class="modal-title">Title</h4>
</div>
<div class="modal-body panel-body"> Loading... </div>
<div class="modal-footer panel-footer">
<div class="pull-left loader"> <i class="fas fa-circle-notch fa-spin"></i> Loading... </div>
<button class="btn btn-default" data-dismiss="modal" type="button"> Close </button>
<button class="btn btn-primary modal-submit" type="button"> Submit </button>
</div>
</div>
</div>
</div>
<form action="#" class="form-horizontal" id="frmGeneratePassword">
<div class="modal fade" id="modalGeneratePassword">
<div class="modal-dialog">
<div class="modal-content panel-primary">
<div class="modal-header panel-heading">
<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
<h4 class="modal-title">
                        Generate Password
                    </h4>
</div>
<div class="modal-body">
<div class="alert alert-danger hidden" id="generatePwLengthError">
                        Please enter a number between 8 and 64 for the password length
                    </div>
<div class="form-group">
<label class="col-sm-4 control-label" for="generatePwLength">Password Length</label>
<div class="col-sm-8">
<input class="form-control input-inline input-inline-100" id="inputGeneratePasswordLength" max="64" min="8" step="1" type="number" value="12"/>
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="generatePwOutput">Generated Password</label>
<div class="col-sm-8">
<input class="form-control" id="inputGeneratePasswordOutput" type="text"/>
</div>
</div>
<div class="row">
<div class="col-sm-8 col-sm-offset-4">
<button class="btn btn-default btn-sm" type="submit">
<i class="fas fa-plus fa-fw"></i>
                                Generate new password
                            </button>
<button class="btn btn-default btn-sm copy-to-clipboard" data-clipboard-target="#inputGeneratePasswordOutput" type="button">
<img alt="Copy to clipboard" src="/assets/img/clippy.svg" width="15"/>
</button>
</div>
</div>
</div>
<div class="modal-footer">
<button class="btn btn-default" data-dismiss="modal" type="button">
                        Close
                    </button>
<button class="btn btn-primary" data-clipboard-target="#inputGeneratePasswordOutput" id="btnGeneratePasswordInsert" type="button">
                        Copy to clipboard &amp; Insert
                    </button>
</div>
</div>
</div>
</div>
</form>
<a class="sm-top" href="#top" title="Back to Top"><i class="far fa-angle-up"></i></a>
</body>
</html>