<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="LPe7guYdhzEvals4RjQqrqWcSbctqEyF60nNDGIMStelEb6+X2AisY18/0ebioy30eQhNg+KVDr0yvrVfT99Pg==" name="csrf-token"/>
<link href="https://www.dbattenuation.co.uk/404" rel="canonical"/>
<title>We could not find that page</title>
<meta content="" name="description"/>
<meta content="RZcDQrbr3GMUEtLSG-ItLp6vuVFgLMbIfb-saNbESpI" name="google-site-verification"/>
<meta content="We could not find that page" property="og:title"/>
<meta content="" property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="/packs/media/images/facebook_cover_image-ef1c107adc8d49a596608c3c17104651.png" property="og:image"/>
<link href="/packs/media/images/favicon-510ae91ff3dbe569ceaefb4a6cc08dd8.png" rel="shortcut icon" type="image/png"/>
<meta content="noindex" name="robots"/>
<link data-turbolinks-track="true" href="/packs/css/application-3ce88f6f.css" media="all" rel="stylesheet"/>
<script data-turbolinks-track="reload" defer="defer" src="/packs/js/application-a4c3d1c98e11a5d1e440.js"></script>
<script src="/packs/js/vue_app-c711df0ecc04eb44cdcc.js"></script>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&amp;display=swap" rel="stylesheet"/>
<script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "dB Attenuation Ltd",
        "url": "https://www.dbattenuation.co.uk/",
        "sameAs": ["https://www.facebook.com/pages/dB-Attenuation-Ltd/1418598895022722", "https://www.linkedin.com/company/dbattenuation/"]
      }
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-102836793-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-102836793-1');
    </script>
</head>
<body class="font-sans error">
<div class="sticky overflow-hidden top-0 left-0 w-full z-50 border-b shadow-lg" data-action="scroll@window-&gt;header#scroll" data-controller="header" x-data="dropdown()">
<header :class="{ 'app-header--open': show != '' }" class="bg-db-gray app-header" data-scroll-header="" data-target="header.header">
<div class="container h-full">
<div class="flex items-center h-full justify-between">
<div :class="{ 'app-header__logo--open': show != '' }" class="app-header__logo">
<a href="/"><img alt="dB Attenuation Ltd logo" src="/packs/media/images/logo-49df30055fbe28496196a1c3f1e03ac0.svg"/></a>
</div>
<button class="flex lg:hidden items-center justify-center text-gray-700">
<svg class="w-8 h-8" fill="none" stroke="currentColor" viewbox="0 0 24 24" x-show="show != 'mobile'"><path d="M4 6h16M4 12h16m-7 6h7" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path></svg>
<svg class="w-8 h-8" fill="none" stroke="currentColor" viewbox="0 0 24 24" x-show="open('mobile')"><path d="M6 18L18 6M6 6l12 12" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path></svg>
</button>
<nav :class="{ 'app-nav--open': show != '' }" class="hidden lg:block relative h-full flex-1 ml-6 app-nav" role="navigation">
<ul class="flex h-full items-center justify-end">
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/">Home</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/about">About</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/solutions">Solutions</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/case-studies">Case Studies</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/project-map">Project Map</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/insights">Insights</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center uppercase h-full text-primary app-nav__link " href="/contact">Contact</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center relative h-full ml-6 text-primary font-semibold focus:outline-none app-nav__link app-nav__border" href="tel:+44 (0) 1206 386800">+44 (0) 1206 386800</a>
</li>
<li class="ml-6 h-full">
<a class="flex items-center relative h-full ml-6 text-primary font-semibold focus:outline-none app-nav__link app-nav__border" href="mailto:info@dbattenuation.co.uk">info@dbattenuation.co.uk</a>
</li>
</ul>
</nav>
</div>
</div>
</header>
<div class="w-full py-8 md:py-12 overflow-y-scroll bg-white text-primary border-t shadow-lg" x-show="open('mobile')" x-transition:enter="transition ease-out duration-200" x-transition:enter-end="opacity-100 transform scale-100" x-transition:enter-start="opacity-0 transform scale-90" x-transition:leave="transition ease-in duration-200" x-transition:leave-end="opacity-0 transform scale-90" x-transition:leave-start="opacity-100 transform scale-100">
<div class="container">
<nav class="flex flex-wrap -mx-4">
<ul class="w-1/2 px-4">
<li class="mb-4">
<a class="text-xl font-semibold" href="/solutions">Solutions</a>
</li>
<ul class="-mt-1">
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/enclosures">Enclosures</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/screens-barriers">Screens &amp; Barriers</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/interior-acoustics">Interior Acoustics </a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/acoustic-doors">Acoustic Doors</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/louvres-vents">Louvres &amp; Vents</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/ventilation">Ventilation</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/flooring-steps-and-access">Flooring, Steps and Access</a>
</li>
<li class="mt-1 mb-2">
<a class="font-semibold" href="/solutions/fire-protection">Fire Protection</a>
</li>
</ul>
</ul>
<ul class="w-1/2 px-4">
<li class="mb-4">
<a class="text-xl font-semibold" href="/">Home</a>
</li>
<li class="mb-4">
<a class="text-xl font-semibold" href="/about">About</a>
</li>
<li class="mb-4">
<a class="text-xl font-semibold" href="/case-studies">Case Studies</a>
</li>
<li class="mb-4">
<a class="text-xl font-semibold" href="/project-map">Project Map</a>
</li>
<li class="mb-4">
<a class="text-xl font-semibold" href="/insights">Insights</a>
</li>
<li class="mb-4">
<a class="text-xl font-semibold" href="/contact">Contact</a>
</li>
</ul>
</nav>
</div>
</div>
<div class="w-full py-16 overflow-y-scroll bg-db-gray text-primary border-t shadow-lg" x-show="open('About')" x-transition:enter="transition ease-out duration-200" x-transition:enter-end="opacity-100 transform scale-100" x-transition:enter-start="opacity-0 transform scale-90" x-transition:leave="transition ease-in duration-200" x-transition:leave-end="opacity-0 transform scale-90" x-transition:leave-start="opacity-100 transform scale-100">
<div class="container">
<div class="flex items-center -mx-8">
<div class="w-2/5 px-8">
<div class="w-1/2">
<h2 class="leading-tight mb-8 text-4xl font-semibold">
                  About
                </h2>
<a class="leading-none pb-2 border-b-2 border-secondary font-semibold uppercase" href="/about">View more</a>
</div>
</div>
<div class="w-3/5 px-8">
<nav class="flex flex-wrap justify-end -mx-4">
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/about/meet-the-team">
<div class="relative w-full h-40 bg-gray-100 flex items-center justify-center">
<img class="h-24 group-hover:opacity-25" src="/packs/media/images/logo-49df30055fbe28496196a1c3f1e03ac0.svg"/>
</div>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Meet the Team</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/about/literature">
<img alt="38" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBY0E9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d75af6680aeca71eec0ef5336104cb1148b7fd97/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/38.png"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Literature </p>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>
<div class="w-full py-16 overflow-y-scroll bg-db-gray text-primary border-t shadow-lg" x-show="open('Solutions')" x-transition:enter="transition ease-out duration-200" x-transition:enter-end="opacity-100 transform scale-100" x-transition:enter-start="opacity-0 transform scale-90" x-transition:leave="transition ease-in duration-200" x-transition:leave-end="opacity-0 transform scale-90" x-transition:leave-start="opacity-100 transform scale-100">
<div class="container">
<div class="flex items-center -mx-8">
<div class="w-2/5 px-8">
<div class="w-1/2">
<h2 class="leading-tight mb-8 text-4xl font-semibold">
                  Solutions
                </h2>
<a class="leading-none pb-2 border-b-2 border-secondary font-semibold uppercase" href="/solutions">View more</a>
</div>
</div>
<div class="w-3/5 px-8">
<nav class="flex flex-wrap justify-end -mx-4">
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/enclosures">
<img alt="11" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBZkU9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ca187fa1068be0cb2e38947f85c840f33835be8d/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/11.JPG"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Enclosures</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/screens-barriers">
<img alt="S1" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBOUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e8dcd89308eb3a062720cbb084b5614d3da3ee74/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/S1.jpg"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Screens &amp; Barriers</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/interior-acoustics">
<img alt="Img 3634" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBWjA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--b2477c6f432bcb4f9599edb3b453c47141cb138e/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/IMG_3634.jpg"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Interior Acoustics </p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/acoustic-doors">
<img alt="34" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBMdz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--0d2ffba8ee06faf8d0c402208fcc5d498c954b59/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/34.jpg"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Acoustic Doors</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/louvres-vents">
<img alt="20" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBXdz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--309f951330d71e6fed975017ac38704de0fe0a35/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/20.JPG"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Louvres &amp; Vents</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/ventilation">
<img alt="Cfd" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBYZz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--404d4119253efaf513b9c5009c2dbaf90a974543/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/CFD.jpg"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Ventilation</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/flooring-steps-and-access">
<img alt="Img 2282" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBYjA9IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--8801ee46d29ca4062c4b9ea2c9cc4ad953d841b8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/IMG_2282.JPG"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Flooring, Steps and Access</p>
</div>
</div>
<div class="w-1/3 px-4">
<div class="group">
<a class="block relative w-full h-40" href="/solutions/fire-protection">
<img alt="Untitled" class="absolute inset-0 w-full object-cover h-full group-hover:opacity-25" src="https://www.dbattenuation.co.uk/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBcWdCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e349e66548c8ac4dc6170b213b48d87f27c3f196/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lPTWpVMk1IZ3hORFF3QmpvR1JWUT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--d77c4e29f121ca6f36b79713f8226f0c23ea6091/Untitled.png"/>
</a> <p class="text-2xl mt-2 font-semibold group-hover:opacity-25">Fire Protection</p>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>
</div>
<script>
  function dropdown() {
    return {
      show: '',
      close() { this.show = '' },
      toggle(name) { this.show = (this.show != name ? name : '') },
      open(name) { return this.show == name }
    }
  }
</script>
<main class="overflow-hidden" id="main-content" role="main">
<section class="bg-white text-primary">
<div class="container h-full py-12 lg:py-24">
<div class="flex flex-wrap items-center h-full -mx-8">
<div class="w-full md:w-3/5 px-8">
<h1 class="leading-tight mb-8 text-6xl font-semibold">
          Sorry
          <span class="block font-light">could not find that page</span>
</h1>
<div class="w-full md:w-2/3 mb-8">
<p>
            The team at dB Attenuation Ltd will look into this.
          </p>
</div>
<a class="flex items-center" href="/">
<span class="inline-block mr-4 font-semibold uppercase">Return home</span>
<svg class="w-8 h-8 md:w-10 md:h-10" height="50" viewbox="0 0 50 50" width="50" xmlns="http://www.w3.org/2000/svg">
<g>
<g fill="none" stroke="currentColor" transform="rotate(90 25 25)">
<circle cx="25" cy="25" r="25" stroke="none"></circle>
<circle cx="25" cy="25" fill="none" r="24.5"></circle>
</g>
<g fill="none" stroke="currentColor" stroke-width="2" transform="rotate(-90 25 25)">
<path d="M31.063 26.417l-6.111 6.11-6.111-6.11"></path>
<path d="M24.952 32.527V16.978"></path>
</g>
</g>
</svg>
</a> </div>
</div>
</div>
</section>
</main>
<footer class="overflow-hidden app-footer">
<div class="container">
<div class="flex flex-wrap items-center">
<div class="w-full lg:w-1/5 py-8 lg:py-0">
<img class="w-24 md:w-32 lg:w-40 mx-auto" src="/packs/media/images/logo-49df30055fbe28496196a1c3f1e03ac0.svg"/>
</div>
<div class="w-full lg:w-4/5 p-8 md:p-16 relative bg-primary text-white app-footer__section">
<div class="flex flex-wrap items-end md:-mx-8">
<div class="w-full md:w-3/5 md:px-8 relative z-10">
<h2 class="leading-tight text-2xl md:text-3xl">
              Have a project coming up?
              <br class="hidden md:block"/>
<span class="font-light">Enquire now for expert advice</span>
</h2>
<ul class="mt-8 mb-8">
<li class="mb-1">
<a class="text-xl font-semibold transition duration-100 hover:opacity-75" href="tel:+44 (0) 1206 386800">+44 (0) 1206 386800</a>
</li>
<li>
<a class="text-xl font-semibold transition duration-100 hover:opacity-75" href="mailto:info@dbattenuation.co.uk">info@dbattenuation.co.uk</a>
</li>
</ul>
<ul class="flex flex-col md:flex-row md:-ml-8">
<li class="md:pl-8 md:mt-0">
<a class="leading-none pb-2 border-b-2 border-secondary uppercase text-white transition duration-100 hover:opacity-75" href="https://www.facebook.com/pages/dB-Attenuation-Ltd/1418598895022722" target="blank">Facebook</a>
</li>
<li class="mt-6 md:pl-8 md:mt-0">
<a class="leading-none pb-2 border-b-2 border-secondary uppercase text-white transition duration-100 hover:opacity-75" href="https://www.linkedin.com/company/dbattenuation/" target="blank">LinkedIn</a>
</li>
</ul>
</div>
<div class="hidden md:block w-2/5 md:px-8 relative z-10">
<nav class="flex flex-wrap -mx-8">
<ul class="w-1/2 px-8 -mb-2">
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/">Home</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/about">About</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/solutions">Solutions</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/case-studies">Case Studies</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/project-map">Project Map</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/insights">Insights</a>
</li>
<li class="mb-2">
<a class="text-lg transition duration-100 hover:opacity-75" href="/contact">Contact</a>
</li>
</ul>
</nav>
</div>
<div class="w-full mt-12 grid grid-cols-2 md:grid-cols-5 gap-4 relative z-10">
<a class="inline-flex items-center justify-center p-2" href="https://www.ioa.org.uk/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/ioa-3b39331d0afefd78704c1782b5889bbe.png"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="https://www.theiet.org/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/iet-6f8f9fd2616bdb7a9f51efae3ebeee74.jpg"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="https://www.constructionline.co.uk/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/contructionline-8d1771d8ca0a453f8bd88792e4299013.svg"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="https://smasltd.com/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/smas-c66c4d649ffab5079257c83f020460f5.jpg"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="https://www.dbc-ltd.co.uk/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/dbc-cfd4142daca58d94e898a1ac6b4e4123.svg"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="https://www.achilles.com/" target="blank">
<img class="w-auto h-10 multiply" src="/packs/media/images/achilles-9fcae2d45074d5d501e85bba0db0343a.png"/>
</a> <a class="inline-flex items-center bg-white justify-center p-2" href="http://hfbond.co.uk/" target="blank">
<img class="w-auto h-10" src="/packs/media/images/hf-bond-e107c4d30180d93b92a05482d09a9428.jpg"/>
</a> </div>
</div>
</div>
</div>
</div>
<div class="bg-white">
<div class="container">
<div class="flex flex-wrap items-center py-8 md:-mx-8 relative z-10 md:px-8">
<div class="w-full md:w-1/2 flex flex-wrap justify-between items-center md:-mx-4">
<div class="px-4 flex-1">
<img alt="QMS ISO 9001" src="/packs/media/images/qms011-a0c36c0dbda3dfb77998dc290cb2f378.jpg"/>
</div>
<div class="px-4 flex-1">
<img alt="QMS ISO 14001" src="/packs/media/images/qms021-1ed2fc87133627510d3b54cd50b56fdc.jpg"/>
</div>
<div class="px-4 flex-1">
<img alt="QMS ISO 45001" src="/packs/media/images/qms031-f548ab35895caa8b96fd2f59f9624f9c.jpg"/>
</div>
</div>
<div class="w-full md:w-1/2 flex flex-col md:flex-row items-center md:justify-end md:px-8 text-primary mt-6 md:mt-0">
<p class="mb-4 md:mb-0">© 2021 dB Attenuation Ltd</p>
</div>
</div>
</div>
</div>
</footer>
</body>
</html>
