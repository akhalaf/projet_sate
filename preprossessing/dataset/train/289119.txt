<!DOCTYPE html>
<html dir="ltr" lang="fr-fr">
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta charset="utf-8"/>
<base href="https://www.carprotect-parking.com/index.php"/>
<meta content="carprotect,parking, gestion,places,parking,protection,chocs,ouverture,portières.
Panneaux de stationnement,signalisation,interactive,sonore,lumineuse" name="keywords"/>
<meta content="multimédia sandoz trading 2020" name="rights"/>
<meta content="index, follow" name="robots"/>
<meta content="carprotect parking concu pour la gestion des places de parking et protection des chocs à l'ouverture des portières.
Nouveauté : Panneaux de stationnement avec signalisation interactive sonore et lumineuse" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Carprotect Réservation de place et protection des portières</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet"/>
<link href="/plugins/content/jw_sig/jw_sig/tmpl/Classic/css/template.css?v=4.1.0" rel="stylesheet"/>
<link href="/plugins/content/jw_allvideos/jw_allvideos/tmpl/Responsive/css/template.css?v=6.1.0" rel="stylesheet"/>
<link href="/templates/system/css/offline.css?561ba731faf01609164459a02f02a6fc" rel="stylesheet"/>
<link href="/templates/system/css/general.css?561ba731faf01609164459a02f02a6fc" rel="stylesheet"/>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"6e84dd3a46b44faad081f335c74f8fcd","system.paths":{"root":"","base":""}}</script>
<script src="/media/system/js/mootools-core.js?561ba731faf01609164459a02f02a6fc"></script>
<script src="/media/system/js/core.js?561ba731faf01609164459a02f02a6fc"></script>
<script src="/media/jui/js/jquery.min.js?561ba731faf01609164459a02f02a6fc"></script>
<script src="/media/jui/js/jquery-noconflict.js?561ba731faf01609164459a02f02a6fc"></script>
<script src="/media/jui/js/jquery-migrate.min.js?561ba731faf01609164459a02f02a6fc"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="/plugins/content/jw_allvideos/jw_allvideos/includes/js/behaviour.js?v=6.1.0"></script>
<!--[if lt IE 9]><script src="/media/jui/js/html5.js?561ba731faf01609164459a02f02a6fc"></script><![endif]-->
<script src="/media/jui/js/bootstrap.min.js?561ba731faf01609164459a02f02a6fc"></script>
<script>

        (function($) {
            $(document).ready(function() {
                $.fancybox.defaults.i18n.en = {
                    CLOSE: 'Close',
                    NEXT: 'Next',
                    PREV: 'Previous',
                    ERROR: 'The requested content cannot be loaded.<br/>Please try again later.',
                    PLAY_START: 'Start slideshow',
                    PLAY_STOP: 'Pause slideshow',
                    FULL_SCREEN: 'Full screen',
                    THUMBS: 'Thumbnails',
                    DOWNLOAD: 'Download',
                    SHARE: 'Share',
                    ZOOM: 'Zoom'
                };
                $.fancybox.defaults.lang = 'en';
                $('a.fancybox-gallery').fancybox({
                    buttons: [
                        'slideShow',
                        'fullScreen',
                        'thumbs',
                        'share',
                        'download',
                        //'zoom',
                        'close'
                    ],
                    beforeShow: function(instance, current) {
                        if (current.type === 'image') {
                            var title = current.opts.$orig.attr('title');
                            current.opts.caption = (title.length ? '<b class="fancyboxCounter">Image ' + (current.index + 1) + ' of ' + instance.group.length + '</b>' + ' | ' + title : '');
                        }
                    }
                });
            });
        })(jQuery);
    
	</script>
</head>
<body>
<div id="system-message-container">
</div>
<div class="outline" id="frame">
<h1>
			CARPROTECT PARKING		</h1>
<p>
			Site en cours de maintenance.
Merci de votre compréhension.

L'équipe Carprotect		</p>
<form action="/" id="form-login" method="post">
<fieldset class="input">
<p id="form-login-username">
<label for="username">Identifiant</label>
<input alt="Identifiant" autocapitalize="none" autocomplete="off" class="inputbox" id="username" name="username" type="text"/>
</p>
<p id="form-login-password">
<label for="passwd">Mot de passe</label>
<input alt="Mot de passe" class="inputbox" id="passwd" name="password" type="password"/>
</p>
<p id="submit-buton">
<input class="button login" name="Submit" type="submit" value="Connexion"/>
</p>
<input name="option" type="hidden" value="com_users"/>
<input name="task" type="hidden" value="user.login"/>
<input name="return" type="hidden" value="aHR0cHM6Ly93d3cuY2FycHJvdGVjdC1wYXJraW5nLmNvbS8="/>
<input name="6e84dd3a46b44faad081f335c74f8fcd" type="hidden" value="1"/> </fieldset>
</form>
</div>
</body>
</html>
