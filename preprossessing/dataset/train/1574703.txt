<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "60139",
      cRay: "61096c94af7ae247",
      cHash: "34a6ecf5a3089dc",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cud2VkZGluZ3NlZ3lwdC5jb20vYmluL2RvY3MlMDklMEE=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "FqouWrENbRgSvy5x4pq9k8QYfjsbpp0ltLM2+4ouUjDuIe+0DvGVGzEkQplqVtW7vy9SU1vWJIrZ301Tau3D+GtfFCQ31q5K2fD9FN2/vhQZRI5ifLKW981seGRhYsH4TyCaVUJN2j8w8rwHTvFR/kyftyVwhL4jwog9G7FHzOxJ//xHZCbzvPHlbzukLQhsVgm5n0nU82Guc9JqVips9CNjnA5UsRe3EJxH/UoUTlgDr7tq3y1+1SOv3OwWDSPWjTZCHs2Kj0i0cbJ2aW7REXS2Xt92S19ZbiU8WPhX2Rog8yNoxJiqWNSgLAScjE2JZdUvfIIKosojV79NxE4kDgb2qtZ+WYBbIM9PeTO2MwRvT2awjxnf40vYVgu5XkW9dFfVH0rj2KoS61Tl4ghgBG3jcryxP7f2GhnoskiCuZfX7ITlzzkAL96xtmbVfpcmcyZxXZDFBitdpQu2D5ckSuy11yck4Tv42PYEVG2oNmlfPRETSjnsTukOG0Db2/PDit95s7Wf9wcJDhgrb3biK3gcd4SLV6nsmBVWpizZcaXPfl9FHnz+wljexXw5j8hkFYGM/SS8sUFyGg3zkGrKfUBWBK2E2BcxG62wj/L5yzIgrLKeh/NkuogE6WwnGlGeli8jMKDhPZY7hHnWuPnwp3TcRF0QCCjtC9MMEv/Ia6dA3o0I0dm6qF9+RfuTot9YwzYhOFdagLpH8uw+ZeREa2L+vXOtGBDV3skHOQn+bzEEhb6vmHW2pGpbTvo9Pj2Eud4U5ezROdl1OxB3fJtrzw==",
        t: "MTYxMDQ4MTIxMS42MjgwMDA=",
        m: "5H3RXJ7WPiOyEKLCLAz0Q6UkdxA2lFy0CSIs5tPJTrA=",
        i1: "Y4rY86HqekScDwbvxrUiTg==",
        i2: "dlxo0fzbqO/9avQJOr7avQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "cywdJ85s7v1q+Gglqgjbgg8LrXth9xPYyVqOm+1lIj0=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.weddingsegypt.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/bin/docs%09%0A?__cf_chl_captcha_tk__=456a9b1924fdf24e635728c4e4a7daa480f4e144-1610481211-0-ASy_EiZxBNgHgQAEl4AednbA7czaM6YoLDIDsn-nCmtwlDMMxsjpcmJLQaBlHsOll9e-l1duavocHTwJWuD9yFsKkesheL21dAPPpUhI4KIxLe6qwd4HufZ9jloXLiC9aSMorBw7HZHCUAp3XXcXUlIxZJ_02RJixqwI6380T1MuUO3VjiC9sZaY_4VOmJyAEWROyzQ9Frs849ync7taHrPeukkHrJPZDKQ-JbzH4h1L4Umgi2YLLKOveqkHaftxIXJu6yIzPRF4tOXDFv6AENU-93a-rgIa3k1iy3T0L5T1XcwuJdfj8--vI-ghUbuGM-jWfeUd8EIJSlM6-tu1QKxvtcAWaq05XLt0yVDLs7BHbULN9zlrISIKBAxQwQ3NBZYFDMSzfXsfIbRh9g07P4HHh3dw4Ghxj6kC8S5zaiNYhXP0MC0uyZ4euMZMcsdTLpftZASvHkHWu1fAkRsfBgYTssL_BGNmuJ7aZ06AncKfUQf53FDAjB2XFHLE8Btyldw_7sNCjphxaSQAvBvFMLsiVxmytmvjIr1kxhC3hJ_zvZsBNscstX9NVJ2d3xEpqTHej9KkKGr2ka7LGDBVqizMLAal_LLvz-RwhB54AJDs" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="b683b26ee5467ead5d5c9a3a887f019cc5481e28-1610481211-0-Ae9I9CL2dNlKNdOKbCe5lmnm8iQc8DlFBnCfmgzzVfFoew/g8h7pY7cA9C85rgDWqUn3dn55AtpkD3NWmPIhuJGtIt8CBQb4+S4mPl28cGRkkp4GymR7D2FocQF+f2U0kUhroBRMaxWN2mgRZlJVvhjPa6+Hd271eeBCJBmPnCBxEWSfNZ54E/sPQu+/txxXGeZ+vX4ZbCxkAKD2ArsRe1Xhs6CE6rq3M5nvHlPWsBjgvehmlokQeWnvZD0mFQgW5wIY4GLDAg3Y8zS3HVQH87LIQpt9oes4UHW8JzCSMGDDQpqYdW4/vWjjpjXTXXvMtNCC4sqk6GkjYwuiXx55sNuDof7cxmboZw56osgOoPXm8kN+CKr8eU43AU58x9yzchKlLSOpVGvfGAiMZtDoedCBjYK4DF53Cq+eplPWzXfaO8YFUiFbYKjCyJhLswAe3T0cxnUra05xDHeGei/0PZesfUHznoTXlwtuGTvDmFYVk+0gLY8C9TsRCQ4gmPOdU81oz9WxRE7Wh6mfZsf/ggzI7on5pNEgyaejJ22Jnxe89z5hhNptQ+UhguFH5M/lTFBKyjo6IfJlSEz/RyBfDhqFv12FEX2ctlds9JzyzD1Eu4YFSKH6QD+yfgRAjvviDbrgX9a2ktpLrQ1aZ7K0jhrKnzPNqqqjP65U+vkKKkkCJ7KnNQz2mIlHfGDyPdF9/GfuhQDYRFFvrFZKvXhnCME/8YO81YanMwOsm7I5TZe55xZygZA9FxM+Mx72SQoo3VcOaJW6IZviJ6cwjl9nuwnmTjVzIeMXBqqcjVocn9p7KeKPtbtBfux6ekKyZ8T/FdvywgrdLnEElGhUNZIMvhiZx9P0nMd18XULolJ6E+kWWsSn7GaQ1XbuysV6aEVoRQkOinHaA/MO0mrSvZ1IBGMhj4hrHdiy8R70gquZotgqfk1rDPJB6QqmVviTVRrzuDZz11hqBwbxSHC5tseixS98Nv8WIqD3JTnaU8UoRIcNe0b44GHpz4lofAoR5IceevCdDwgaXRhYoedsZBQWi6GECyULY/gmRlTHFqj4CAbGNfoeIdn4xstVRT7fsTrE6r0C28WkJnW+0Jdl6+jBOH/c7VhZqMHPM3LshmKSI/rDddR06FUflUuF67bAMssan75ma8b0cVCwFya7u4HoMPKjVLqORywCqTUp57jncThRMbTfcrskwLGtziinO4gxSI7U84srDkoHBkJ7MGAnOSHgrvnYQOKVlMukiRDsdX3ydVrCejqawPmy5tTNYNl4rM9TW+FPnTTcTHZe4a4OwJL+FrC3DDJ3SZs+ME7oi1gf48g48B7KyPDdSYyTG7LsHFPu1GeGdTvw7sjd8vl74VU="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="bce5b81b91a18d414bd8e6eb94615b26"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=61096c94af7ae247')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<a href="https://derchris.net/fungoidintensity.php?page=2"><span style="display: none;">table</span></a>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">61096c94af7ae247</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
