<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/build/images/favicon.cbc59eff.ico" rel="shortcut icon"/>
<title>Bahnhof</title>
<link href="/build/app.a74b1f10.css" rel="stylesheet"/>
</head>
<body>
<header id="header">
<div>
<div class="container top-menu_container hide-on-mob d-flex">
<nav class="top-left_nav">
<a href="http://bahnhof.se/">bahnhof.se</a>
</nav>
<nav class="top-right-nav">
<ul>
<li><a href="/aboutbahnhof">About Bahnhof</a></li>
<li><a href="/datacenters">Data centers</a></li>
<li><a href="/contacts">Contact</a></li>
</ul>
</nav>
</div>
<div class="header-line hide-on-mob"></div>
<div class="container bottom-menu_container">
<nav class="bottom-left-nav">
<ul>
<li>
<a class="logo-mob-margin" href="/">
<img alt="logo" height="52" src="/build/images/bahnhof-logo.1116df85.png" width="103"/>
</a>
</li>
<li class="hide-on-mob">
<a href="/kubernetes">Kubernetes</a>
</li>
<li class="hide-on-mob">
<a href="/accelerated-compute">Accelerated Compute</a>
</li>
<li class="hide-on-mob">
<a href="/openstack">OpenStack</a>
</li>
<li class="hide-on-mob">
<a href="/vps">Virtual Private Server (VPS)</a>
</li>
<li class="hide-on-mob">
<a href="/vdc">Virtual Data Center (VDC)</a>
</li>
</ul>
</nav>
<nav class="bottom-right_nav">
<a class="login-mob" href="/login">
<img alt="account" src="/build/images/account-icon.2c2f7225.png"/>
</a>
<div class="hamburger hide-on-desktop">
<input id="menu__toggle" type="checkbox"/>
<label class="menu__btn" for="menu__toggle">
<span></span>
</label>
<ul class="menu__box">
<li class="menu-underscore-item">
<a class="menu__item" href="/kubernetes">Kubernetes</a>
</li>
<li class="menu-underscore-item">
<a class="menu__item" href="/accelerated-compute">Accelerated Compute</a>
</li>
<li class="menu-underscore-item">
<a class="menu__item" href="/openstack">OpenStack</a>
</li>
<li class="menu-underscore-item">
<a class="menu__item" href="/vps">Virtual Private Server (VPS)</a>
</li>
<li class="menu-underscore-item">
<a class="menu__item" href="/vdc">Virtual Data Center (VDC)</a>
</li>
<li class="menu-secondary-item">
<a class="menu__item" href="/aboutbahnhof">About Bahnhof</a>
</li>
<li class="menu-secondary-item">
<a class="menu__item" href="/datacenters">Data centers</a>
</li>
<li class="menu-secondary-item">
<a class="menu__item" href="/contacts">Contact</a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</header>
<main>
<section class="main-info_section" style="height:700px;">
<div class="container">
<div class="main-info_wrapper">
<h1>Your cloud and your data - safe in Sweden</h1>
<p>Build your cloud with us - within Swedish borders and completely under Swedish jurisdiction, we give you protection against Cloud Act.</p>
<div class="main-info_products-row_wrapper">
<div>
<img alt="kubernetes" src="/build/images/kubernetes-logo.007bcccc.png"/>
<span>Leading open source platform for managment of container based programs and associated network and storage solutions.</span>
</div>
<div>
<img alt="openstack" src="/build/images/openstack-logo.3ac06d90.png"/>
<span>Rapidly growing open source tools for administration of cloud platforms and virtual server environments.</span>
</div>
</div>
<div class="main-info_products-row_wrapper mg-top_20">
<div>
<img src="/build/images/wmware-logo.4d30ea80.png"/>
<span>Leading enivironment for cloud based infrastructure and virtualization of cloud server- and IT environments.</span>
</div>
<div>
<img src="/build/images/onapplogo.7829175f.png"/>
<span>Complete cloud infrastructure and virtualization platform.</span>
</div>
</div>
</div>
</div>
</section>
<section class="our-services_section">
<div class="container">
<h1>Our services</h1>
<div class="d-flex space-between adaptive-flex-column">
<div>
<img src="/build/images/bahnhof-kubernetes-cloud.61a619e1.jpg"/>
<h2>Kubernetes</h2>
<p>
                        Container based computing environment customized to your needs.
                    </p>
<a href="/kubernetes">
<button>Read more</button>
</a>
</div>
<div>
<img src="/build/images/bahnhof-openstack.c57292cc.jpg"/>
<h2>OpenStack</h2>
<p>
                        Create your complete IT infrastructure in our OpenStack cloud.
                    </p>
<a href="/openstack">
<button>Read more</button>
</a>
</div>
<div>
<img src="/build/images/bahnhof-vpsbasic.c691f77c.jpg"/>
<h2>Virtual Private Server (VPS)</h2>
<p>
                        Get your own virtual machine up and running in minutes.
                    </p>
<a href="/vps">
<button>Read more</button>
</a>
</div>
<div>
<img src="/build/images/bahnhof-virtual-data-center.97502458.jpg"/>
<h2>Virtual Data Center (VDC)</h2>
<p>
                       Complete cloud environments on the platform of your choice.
                    </p>
<a href="/vdc">
<button>Read more</button>
</a>
</div>
</div>
</div>
</section>
<section class="advanteges_section">
<div class="container">
<div class="d-flex space-between adaptive-flex-column">
<div>
<img src="/build/images/green-checkmark.52a07745.svg"/>
<div class="advantage-title">
<h2>Your data: controlled and owned by you</h2>
</div>
<p>
                        Your data is safe and secure in our state of the art data centers. Retain full control over your environment and your data. We do not interfere with or hand out data to third parties.
                    </p>
</div>
<div>
<img src="/build/images/green-checkmark.52a07745.svg"/>
<div class="advantage-title">
<h2>Under Swedish Jurisdiction</h2>
</div>
<p>With all infrastructure in Sweden, our cloud services run under the country's strong free speech and data protection laws. Your information is protected against foreign surveillance.</p>
</div>
<div>
<img src="/build/images/green-checkmark.52a07745.svg"/>
<div class="advantage-title">
<h2>Highest security</h2>
</div>
<p>Our cloud services are run in top modern data centers built to high security and safety specs. Several leading companies and organizations trust Bahnhof as their cloud and data center provider.</p>
</div>
<div>
<img src="/build/images/green-checkmark.52a07745.svg"/>
<div class="advantage-title">
<h2>Simple pricing</h2>
</div>
<p>Avoid unpleasant surprises. Our services are priced clearly and you will always know how much you will pay as well as what is. and what isn't, included in the price.</p>
</div>
</div>
</div>
</section>
<section class="contact-form_section">
<div class="container">
<h1>Contact us for general enquiries and price quotes</h1>
<p>Send us your info and we will contact you shortly. <br/>Or contact our cloud sales team directly on +46-(0)10-510 00 00 or cloudsales@bahnhof.se</p>
<div data-api-endpoints='{"sendForm":"\/ajax\/form\/user"}' id="mainForm"></div>
</div>
</section>
</main>
<footer>
<div class="container">
<div class="footer-info-wrapper">
<p>+46-(0)10-510 00 00    <a class="hide-on-mob" href="mailto:cloudsales@bahnhof.net"><span class="info_orange">cloudsales@bahnhof.net</span></a></p>
<p class="hide-on-desktop"><a href="mailto:cloudsales@bahnhof.net"><span class="info_orange">cloudsales@bahnhof.net</span></a></p>
<p>Tunnelgatan 2, 111 37 Stockholm, Sweden</p>
<p><a href="https://www.bahnhof.se/filestorage/userfiles/file/PrivacyPolicy_Bahnhof.pdf" target="_blank"><span class="info_orange">Privacy Policy</span></a> - <a href="/build/files/20200720-BahnhofAB_terms-and-conditions_cloud-TOS.f6831643.pdf" target="_blank"><span class="info_orange">Terms and conditions</span></a></p>
<p>Visit our Swedish site <a href="http://bahnhof.se/"> <span class="info_orange">bahnhof.se</span></a></p>
<p class="footer-info_copyright">© Copyright 2020 Bahnhof AB</p>
</div>
</div>
</footer>
<script src="/build/runtime.420770e4.js"></script><script src="/build/0.cadbe4fd.js"></script><script src="/build/app.9cd7bb63.js"></script>
</body>
</html>
