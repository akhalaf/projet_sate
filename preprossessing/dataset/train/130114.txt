<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-5739459317466268",
          enable_page_level_ads: true
     });
</script>
<link href="/_19/2019.css" rel="stylesheet"/>
<meta content="initial-scale=1" name="viewport"/>
<meta content="antiMUSIC.com news - Blessed By A Broken Heart Set New Album Release" name="Description"/>
<title>Blessed By A Broken Heart Set New Album Release (a top story)::Blessed By A Broken Heart News ::antiMusic.com</title>
<link href="http://antimusic.com/as.xml" rel="search" title="antiMusic.com" type="application/opensearchdescription+xml"/>
<link href="http://www.antimusic.com/news/11/nov/ts04Blessed_By_A_Broken_Heart_Set_New_Album_Release.shtml" rel="canonical"/>
</head>
<body alink="#990000" link="#990000" text="#000000" vlink="#666666">
<div class="container" onclick="">
<div class="antimenu">
<div class="logo">
<a href="/"><img alt="antiMusic Logo" src="/_res/s-logo.gif"/></a><br/></div>
<div class="navbox">
<nav>
<label for="toggle-mobile-menu">☰ </label>
<input id="toggle-mobile-menu" type="checkbox"/>
<ul>
<li><a href="/news/">News</a></li>
<li><a href="/reviews/">Reviews</a></li>
<li><a href="/dayinrock/">Day in Rock</a></li>
<li><a href="/rocknews/">RockNewsWire</a></li>
<li><a href="/rss.shtml">Feeds</a></li>
<li><form action="/cgi-bin/search.cgi" method="post" target="_top"><input maxlength="199" name="searchstring" size="10"/><input alt="Go" name="Search" src="/_lo/search16.gif" type="image"/></form></li>
</ul>
</nav>
</div>
</div>
</div>
<div class="mbar"><br/><br/></div>
<div align="center" class="topad">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- res leaderboard -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5739459317466268" data-ad-slot="4824882041" style="display:inline-block;width:728px;height:90px">
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</ins></div>
<div><br/><br/>.</div>
<div class="content" onclick="">
<div class="article">
<!-- content start -->
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="99%">
<tr>
<td><!-- text content start -->
<center><font face="Arial,Helvetica"><font color="red"><h1>Blessed By A Broken Heart Set New Album Release (A Top Story)</h1></font></font></center>
<center><table border="0" cellpadding="0" cellspacing="0" width="90%">
<tr>
<td bgcolor="#000000" nowrap="">
<table border="0" cellpadding="0" cellspacing="0" cols="2" height="1" width="1">
<tr>
<td></td>
<td></td>
</tr>
</table>
</td>
</tr>
</table></center>
<center><font face="Arial,Helvetica"><font size="-2"></font></font>
<br/><font face="Arial,Helvetica"><font color="#FFFFFF">.</font></font></center>
<font face="Arial,Helvetica">On Friday Blessed By A Broken Heart Set New Album Release was a top story. Here is the recap: (BLPR) Tooth and Nail Records have set a January 24, 2012 release date for Blessed By A Broken Heart's new album Feel the Power. <p>"The whole band is still obsessed with the '80s, and that's bled into our lifestyles. It's not just when we're on tour: We all kind of dress '80s; we still have mullets. It's become part of our lives," explains singer Tony Gambino.</p><p>"On the last album, we used more of the cheesy side of the '80s, and this album we used a little more of the mature side. It's a little less fluff, a lot less cheese, a little more serious. We've kind of gone a little more classic metal and heavy rock 'n' roll. It's real organic how it all came together," he adds. "We're not in this to follow the trendswe're in it to do what we love."  - <i><a href="http://www.antimusic.com/news/11/nov/04Blessed_By_A_Broken_Heart_Set_New_Album_Release.shtml">more on this story</a></i>
</p><p>
</p><p></p><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr valign="TOP"><td>
<b><font face="Arial,Helvetica">
<p><a href="Blessed+By+A+Broken+Heart" target="_blank">Blessed By A Broken Heart CDs, DVDs and MP3s</a>
</p><p><a href="shirt+poster+Blessed+By+A+Broken+Heart" target="_blank">Blessed By A Broken Heart T-shirts and Posters</a>
</p></font></b>
<p></p><center>
<p>
</p><div class="centerhl"><p>Share this article</p></div><div class="center">
<a href="https://www.facebook.com/sharer/sharer.php" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&amp;t=' + encodeURIComponent(document.URL)); return false;" target="_blank" title="Share on Facebook"><img alt="Share on Facebook" src="/_img/sm/f.png"/></a>
<a href="https://twitter.com/intent/tweet?source=&amp;text=:%20" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;" target="_blank" title="Tweet"><img alt="Share on Twitter" src="/_img/sm/t.png"/></a>
<a href="https://plus.google.com/share?url=" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;" target="_blank" title="Share on Google+"><img alt="Share on Google+" src="/_img/sm/g.png"/></a>
<a href="http://pinterest.com/pin/create/button/?url=&amp;description=" onclick="window.open('http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(document.URL) + '&amp;description=' +  encodeURIComponent(document.title)); return false;" target="_blank" title="Pin it"><img alt="Pin it" src="/_img/sm/p.png"/></a>
<a href="http://www.reddit.com/submit?url=&amp;title=" onclick="window.open('http://www.reddit.com/submit?url=' + encodeURIComponent(document.URL) + '&amp;title=' +  encodeURIComponent(document.title)); return false;" target="_blank" title="Share on Reddit"><img alt="Share on Reddit" src="/_img/sm/s.png"/></a>
<a href="mailto:?subject=&amp;body=:%20" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&amp;body=' +  encodeURIComponent(document.URL)); return false;" target="_blank" title="Send email"><img alt="email this article" src="/_img/sm/e.png"/></a>
</div>
<p></p></center>
</td></tr></table>
<p></p><p></p></font><center><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-5739459317466268" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="2542312409" style="display:block; text-align:center;"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></center> </td> </tr> </table><!-- content end --> <br/></div>
<div class="sidemenu"><p>
</p><div class="adhead">advertisement</div><br/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- d 336 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5739459317466268" data-ad-slot="2068246792" style="display:inline-block;width:336px;height:280px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="reviews">
<br/><br/><center><b>
<div class="dirh">
Day In Rock
</div></b></center>
<a href="http://www.antimusic.com/dayinrock/">Scott Weiland's Son 'Let Go' From Suspect208 Supergroup Over Drug Use- Dave Mustaine Teases New Megadeth Album- Rush's Geddy Lee To Perform With Orchestra- more</a>
</div>
<div class="reviews">
<br/><br/><center><b>
<div class="dirh">
Reviews
</div></b></center>
<a href="https://www.antimusic.com/reviews/21/Paul_McCartney_-_McCartney_III.shtml">Paul McCartney - McCartney III</a><p>
<a href="https://www.antimusic.com/reviews/21/Joe_Bonamassa_-_Guitar_Man.shtml">Quick Flicks: Joe Bonamassa - Guitar Man</a></p><p>
<a href="https://www.antimusic.com/reviews/21/Travevel.shtml">Travel News, Trips and Tips: Travevel</a></p><p>
<a href="https://www.antimusic.com/morley/21/MariaVidal.shtml">MorleyView Maria Vidal (Desmond Child and Rouge)</a></p><p>
<a href="https://www.antimusic.com/reviews/20/Aussie_Edition.shtml">RockPile: Aussie Edition</a></p><p>
</p></div>
<center><b><font face="Arial,Helvetica"><font size="-1">advertisement</font></font></b><br/></center>
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- d tower new -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5739459317466268" data-ad-slot="3534259759" style="display:inline-block;width:300px;height:600px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!-- DO NOT MODIFY -->
</center>
<div class="reviews">
<br/><br/><center><b>
<div class="dirh">
Latest News
</div></b></center>
<a href="https://www.antimusic.com/news/21/01/12Scott_Weilands_Son_Let_Go_From_Suspect208_Supergroup_Over_Drug_Use.shtml">Scott Weiland's Son 'Let Go' From Suspect208 Supergroup Over Drug Use</a><p>
<a href="https://www.antimusic.com/news/21/01/12Dave_Mustaine_Teases_New_Megadeth_Album.shtml">Dave Mustaine Teases New Megadeth Album</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Marko_Hietala_Announces_Departure_From_Nightwish.shtml">Marko Hietala Announces Departure From Nightwish</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Rushs_Geddy_Lee_To_Perform_With_Vancouver_Symphony_Orchestra.shtml">Rush's Geddy Lee To Perform With Vancouver Symphony Orchestra</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Jack_White_Streaming_Song_From_New_Live_Package.shtml">Jack White Streaming Song From New Live Package</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Concrete_Blondes_Joey_Given_A_Country_Makeover_By_Jeremy_Pinnell.shtml">Concrete Blonde's 'Joey' Given A Country Makeover By Jeremy Pinnell</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Gatecreeper_Surprise_Releasing_New_Album_An_Unexpected_Reality.shtml">Gatecreeper Surprise Releasing New Album 'An Unexpected Reality'</a></p><p>
<a href="https://www.antimusic.com/news/21/01/12Demon_Head_Release_The_Feline_Smile_Video.shtml">Demon Head Release 'The Feline Smile' Video</a></p><p>
</p></div>
</div>
</div>
<div class="foot">
<div class="center">
<br/><br/><b>Follow Us:</b> <a href="https://www.facebook.com/antiMusicOfficial/"><img alt="Facebook" src="/_res/facebook.png"/></a> <a href="https://twitter.com/dayinrock"><img alt="Twitter" src="/_res/twitter.png"/></a>
<a href="/rss.shtml"><img alt="Feeds" src="/_res/rss.png"/></a>
<p><a href="http://www.ieginc.com/contacts.html">Contact Us</a> - <a href="http://www.ieginc.com/privacy.html">Privacy</a> - <a href="http://www.antimusic.com/amemail.shtml">antiMusic Email</a> - <a href="http://www.antimusic.com/why.shtml">Why we are antiMusic</a>
</p><p>Copyright© 1998 - 2021 <a href="http://www.ieginc.com">Iconoclast Entertainment Group</a> All rights reserved.
</p><p>Please <a href="http://www.ieginc.com/tos.html">click here </a>for legal restrictions and terms of use applicable to this site. Use of this site signifies your agreement to the terms of use.<br/></p></div></div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-9902148-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-9902148-1', { 'anonymize_ip': true });
</script>
<script type="text/javascript"> var infolinks_pid = 3244164; var infolinks_wsid = 0; </script> <script src="//resources.infolinks.com/js/infolinks_main.js" type="text/javascript"></script>
</body> </html>
