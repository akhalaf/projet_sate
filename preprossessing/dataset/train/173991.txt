<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>
</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://automobilesdenislefebvre.com/feed/" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="http://automobilesdenislefebvre.com/xmlrpc.php" rel="pingback"/>
<link href="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/library/css/print.css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/library/css/slimbox.css" rel="stylesheet" type="text/css"/>
<script src="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/library/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/library/js/taber.js" type="text/javascript"></script>
<link href="https://automobilesdenislefebvre.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://automobilesdenislefebvre.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 3.5.1" name="generator"/>
<link href="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/skins/3-orange.css" rel="stylesheet" type="text/css"/>
<script>
(function(d, script) {
    script = d.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.onload = function(){
        // remote script has loaded
    };
    script.src = 'https://autodenislefebvre.nuageclient.com/livesms/';
    d.getElementsByTagName('head')[0].appendChild(script);
}(document));

</script>
</head>
<body class="error404">
<div id="outer">
<div class="clearfix" id="wrapper">
<!-- page nav #end -->
<div class="clearfix" id="header">
<div class="logo">
<a href="http://automobilesdenislefebvre.com/">
<img alt="Automobiles Denis Lefebvre – Véhicules Usagées à Trois-Rivières" src="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/skins/3-orange/logo.png"/></a>
<p class="blog-description">
</p>
</div>
<div class="header_banner">
               
         </div>
<div class="callnow">
                  
                
            </div>
<div class="widget"> <div class="textwidget"><span style=" position: absolute; TOP: 3px; left: -8px; visibility : visible;z-index : 1"><a href="http://automobilesdenislefebvre.com" internallink="false" target="_self"><img border="0" src="http://automobilesdenislefebvre.com/logo-jcf.jpg"/></a></span></div>
</div>
</div> <!-- header #end -->
<div id="categories_strip">
<div class="widget"> <div class="textwidget"><span style=" position: absolute; TOP: -141px; left: 466px; visibility : visible;z-index : 1"><img border="0" src="http://automobilesdenislefebvre.com/top01.png"/></span></div>
</div><div class="widget"><div class="menu-menu1-container"><ul class="menu" id="menu-menu1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-410" id="menu-item-410"><a href="http://automobilesdenislefebvre.com">Accueil</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-411" id="menu-item-411"><a href="http://automobilesdenislefebvre.com/?s=search&amp;srchmake=&amp;srchmodels=&amp;srchminprice=&amp;srchmaxprice=&amp;srchtype=&amp;search=Search">Inventaire</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-472" id="menu-item-472"><a href="https://automobilesdenislefebvre.com/financement/">Financement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-473" id="menu-item-473"><a href="https://automobilesdenislefebvre.com/a-props-de-nous/">À propos de nous</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-414" id="menu-item-414"><a href="https://automobilesdenislefebvre.com/nous-joindre/">Nous joindre</a></li>
</ul></div></div>
</div>
<div class="sidebar" id="sidebar_l">
<div class="widget"> <div class="textwidget"><a href="http://automobilesdenislefebvre.com/page/2/?s=search&amp;srchmake&amp;srchmodels&amp;srchminprice&amp;srchmaxprice=5000 "><img alt="Voir les 5000$ et moins" border="0" src="http://automobilesdenislefebvre.com/banner-gauche.jpg"/></a></div>
</div><div class="widget"><h3><span>Marques disponible</span></h3> <ul>
<li class="cat-item cat-item-58"><a href="https://automobilesdenislefebvre.com/make/chevrolet/" title="Voir tous les articles classés dans Chevrolet">Chevrolet</a>
</li>
<li class="cat-item cat-item-59"><a href="https://automobilesdenislefebvre.com/make/dodge/" title="Voir tous les articles classés dans Dodge">Dodge</a>
</li>
<li class="cat-item cat-item-57"><a href="https://automobilesdenislefebvre.com/make/ford/" title="Voir tous les articles classés dans Ford">Ford</a>
</li>
<li class="cat-item cat-item-61"><a href="https://automobilesdenislefebvre.com/make/honda/" title="Voir tous les articles classés dans Honda">Honda</a>
</li>
<li class="cat-item cat-item-69"><a href="https://automobilesdenislefebvre.com/make/hyundai/" title="Voir tous les articles classés dans Hyundai">Hyundai</a>
</li>
<li class="cat-item cat-item-72"><a href="https://automobilesdenislefebvre.com/make/kia/" title="Voir tous les articles classés dans KIA">KIA</a>
</li>
<li class="cat-item cat-item-70"><a href="https://automobilesdenislefebvre.com/make/mazda/" title="Voir tous les articles classés dans Mazda">Mazda</a>
</li>
<li class="cat-item cat-item-74"><a href="https://automobilesdenislefebvre.com/make/mercedes/" title="Voir tous les articles classés dans Mercedes">Mercedes</a>
</li>
<li class="cat-item cat-item-85"><a href="https://automobilesdenislefebvre.com/make/nissan/" title="Voir tous les articles classés dans NISSAN">NISSAN</a>
</li>
<li class="cat-item cat-item-44"><a href="https://automobilesdenislefebvre.com/make/subaru/" title="Voir tous les articles classés dans Subaru">Subaru</a>
</li>
<li class="cat-item cat-item-65"><a href="https://automobilesdenislefebvre.com/make/toyota/" title="Voir tous les articles classés dans Toyota">Toyota</a>
</li>
<li class="cat-item cat-item-66"><a href="https://automobilesdenislefebvre.com/make/volkswagen/" title="Voir tous les articles classés dans Volkswagen">Volkswagen</a>
</li>
<li class="cat-item cat-item-64"><a href="https://automobilesdenislefebvre.com/make/volvo/" title="Voir tous les articles classés dans Volvo">Volvo</a>
</li>
</ul>
</div>
</div><!--sidebar_l #end -->
<div class="common_wrap">
<div id="content">
<h1 class="cat_head"></h1>
<h1> 404 Error !</h1>
<div class="cat-spot"><p></p></div>
</div> <!-- content  #end -->
<div class="sidebar" id="sidebar_r">
<div class="widget"> <div class="textwidget"><img src="http://automobilesdenislefebvre.com/banner-droite.jpg"/></div>
</div></div> <!-- sidebar right--> <!--sidebar_r #end -->
</div> <!--common_wrap #end -->
</div><!-- wrapper #end -->
<div class="clearfix" id="footer">
<p class="copy">©  Automobiles Denis Lefebvre – Véhicules Usagées à Trois-Rivières Tous Droits Réservés </p>
</div><!-- footer #end -->
</div> <!-- outer #end -->
<script src="https://automobilesdenislefebvre.com/wp-content/themes/Automobile/library/js/jquery.helper.js" type="text/javascript"></script>
</body></html>