<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "19171",
      cRay: "610ec0eb7a5add12",
      cHash: "99da6a199ec2d18",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJ0c3Bvb2wtZS1sZWFybmluZy5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "o1GLhzrfuTnhuOUHuedmmVrdrSd2h1J6Sxj9+QX02XsublaDGqHKkRuoW+JO+lSnVDfF7r+zFDRRT5uY/ajzmx8MSMV+lZStPtSeeDg6bXw5zf34gkJzqLLW1zB3o805ZDS0b+F+8LndM0D8oAhjJcjJ0yH3oStBrMVyvqaUSvKwkuaPwrxiDZ8oYhWjMQlp+803iywWl6UQjz+VFNQOf5hx3jGHhEUoHVJvLuFjwQbvCgAr0R2DUkN6HwmMGzcPP6LxH0iG0Art5Snv3ZdJuIhWbTGbWW2/xN3JQildUvhgt762pc2ezXntbmuE9HO0YZAaW7heXqtsKsx90PYoMvOeFBjAq7RSWxPeLytSouT8hXPgmT6jMGuDKBHJA+psPach+w0/3PHsqwVAUq3iuhdyBA+HgpkZ9FTPd7zWp3iivBj6AjrEBa6uypAMS7k9OXVdxjBU/jlC/+TelY3mdIGqRV+gRfYcuXhkWam2nK+HL2ROtm0Kjk1Eh7I0gLojyziRWE43DHtXasS8vpDt3yRlqFVhDCZBwZgeZF6QFv43E8UQEHqCsWiCGKJH4mqt4/oL7PyzMlgEy6FuxMCQfEJaYE+1QqnREoUH9JHe++8y8G8GJ6xbVSyO7ZyLrB3Hm4c99qPmmuGZjazsfGuOOD7k/oL8igKiwPBnQ7/CegNZOOZqXOG+zLptRVOS4/GXbtguf3A1BVcbBeg7IsFnAxRvFjArTpEKDsI/mZT4W1GzyOpDwGqFvZOqczU+Ki+D",
        t: "MTYxMDUzNzA5NC45NjMwMDA=",
        m: "BVilVbVMovKtBzMpQux88/Dd1i3E0xtshl/WcDsRxPc=",
        i1: "gzj01nM8yi1YqBcSmGux9Q==",
        i2: "zHupRyEnuX4LMdqEvMvKEg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "Z+aQ27a4g2noqP64RJT13nAIf9mq3WKkkO0OC4NUAek=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610ec0eb7a5add12");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> artspool-e-learning.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=1d34840e5d44dfa0e8b985d33689beb53ec51a49-1610537094-0-AW6NslqSDZSb2yTPvMiVu-JSXNjmh5d-FVwr4SVY3rj7NTuc7HCqtDkR9Kx6f58jh6WOIR7aFaAEa0iSLn_jt1DXLbDvHNq_ZELDWBYJvPEorvTxJ8kGFyA0E_8vdQ61LPL66ffQKZ-Va85-Nn3MiCELFtLEvxPFQKKtSbWIcJ_rJ_JCSDgATSs2N7jegQauVy2wBJvbNEmP6o7g2_A8FzKaF5foA54uY-RhqDNy__yffipycwx-xBhkRBS8wSKXXkkJkCFdinbM_YtDsOFztdIMkX-KUtlsLve10n0ZkyC8LSt-5NtXY8iNYMYWGZxpzg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="678e71ddf4872ff621a9e92e59efc93258ac4503-1610537094-0-AUvA6fGRzGdeRVo2IVJGkTeY4XruHmp519T1SLpf0VQ/u23lFcQm3LwjzrE0wj0msCxUWUB9R0+QnqDMP4H+I8S0JBx7w+CE52GaHk5nN+xxUk8ufmqXMet3rEAmZ6/C3zMmdtk5nVMs9qX0atLNTMUtD0wvo54RRcXzarjMsq4SeJlu23FE//1UkVwQBaT/omt4nlMKigU7mXvX/w31LcTgVZ5DrYqG56b5Qqqjm/RgZ/D+B6KvrTUt3t85Q10ikJk+fIVEZ+3dFzDjdmA7k0DYwp09ZjMacJnC2Xd+GIgHw/PVJEEu+IrZZpr6m7RemIIagahgPvoEilyA10WlQI/BFYNEpU1hA46sdcGPc1FTbl0riyUhN8m0ZRz6iENGungShtkSVzDC/pYwrpody8tyjQ4Yt4+h1FzlZS5QlxAlQLOC5oFnBvcEWfFQh8AOdOShXZJSzo/u2SjKzs2JWdmxfgDYm6ulv2w8GJQNwIv+7RwLszIe5UiFpEKxQJXcw8uPJvc7O/SGTAEE1qObs6jFUmH2zBekfrMXOCU/82eSNobJhNm/4CEDQfjnTMx/X4l9NZowP8V+m1z2LCwvDVYpR8/cmP0LtL0zlYzTV9EFYYa26dBWpGP8eIn76vQYcbdP5/44dCEJ89bvZNcHlH9jBKZyTD4LD7AR5ByksgQWHCzvHKqYsZ9GIhd1l/Xl+uKc04+2qPCOO+bbJb4LUeYnckPoFBdxU9vUxR4eUvcp5LXPDWKEURFAt8NFlCNJuhVJcA7LgOOohk+wG35LhlUOV6BgannGFQ6rxGBgO/KWkSoXWceDqtBgHVbp7x77rWSdG4PzTb68vu0daby3urewbV7FEyVZ7adnVHhV7qreIg/Zi/g00Bw9VQ3/J1X+JUDO1m4HR9dNX7xy1LJeGNiIi2P99+1IrRwQh44n1XN744CCvZd/XvFd2/X/RJumEcwBDkUhSY9zpYSWpSWj2QpjnhnIEy6YxHgYizSgE/AOWlM9AhiLafcno7vl6d8GBq57cjEtXtnEpPqRoqn2z7IEQ+iCkEco88UVcU4pDx8qEW/3rNPXYCoZ++wL4g+gKnrjweK/L7UxAhFbX6ZFLo63056uJKpg2hF9fHCEOAPIq7doSfqbGrUJGkkD/ENEzHmqkuKPJ8MA6riGuXMZMy3FG5sssgWN8EZ7VFmCnvzAUdCzgc/Pfxk59kjeEY9yCHNB6U/nOh2GFhgpTirvdYwpYyo92rWDn0Z0LHKT2Zfnq3wP+DOK6BfI8PQMtRCikR2VqEmPohJbNvmZ0I+lO7Y/HxxKvOm6Xd1aUYd+YWJvwM1ktZkWYRhxDAgURTv/ew=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="d6375720dae6dee2be6516db311c78b8"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610537098.963-XV4nDR+QIO"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610ec0eb7a5add12')"> </div>
</div>
<a href="https://sensationalstickers.com/unprepared.php?more=208" style="display: none;">table</a>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610ec0eb7a5add12</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
