<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/sites/all/themes/atltheme/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="Les Colocs lyrics: 'Juste Une P'tite Nuite', 'Tassez-vous De D'l', 'Belz' etc" name="description"/>
<meta content="283249275118803" property="fb:app_id"/>
<title>Les Colocs lyrics: 16 song lyrics</title>
<meta content="width=device-width" name="viewport"/>
<link href="http://www.allthelyrics.com/lyrics/les_colocs" hreflang="en" rel="alternate"/>
<link href="http://www.allthelyrics.com/fr/lyrics/les_colocs" hreflang="fr" rel="alternate"/>
<link href="http://www.allthelyrics.com/de/lyrics/les_colocs" hreflang="de" rel="alternate"/>
<link href="http://www.allthelyrics.com/it/lyrics/les_colocs" hreflang="it" rel="alternate"/>
<link href="http://www.allthelyrics.com/es/lyrics/les_colocs" hreflang="es" rel="alternate"/>
<link href="/sites/default/files/css/css_7958ffeb95bb688d2456f734a58bba69.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/default/files/css/css_70145eb6949315cdd0246d4fc26c2a3d.css" media="print" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css"></style>
<!--[if lte IE 8]>
  <link type="text/css" rel="stylesheet" href="/sites/all/themes/atltheme/styles/ie8.css" media="all" />
  <![endif]-->
<!--[if lte IE 7]>
  <link type="text/css" rel="stylesheet" href="/sites/all/themes/atltheme/styles/ie7.css" media="all" />
  <![endif]-->
<script src="/sites/default/files/js/js_573f0c79eb488d569ead5ae1164286a7.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "my_module": { "atl_artistname": [ "Les Colocs", "Les Colocs" ] } });
//--><!]]>
</script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&amp;light&amp;v1&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
<!--BEGIN FIRSTIMPRESSION TAG -->
<script data-cfasync="false" type="text/javascript">
	window.apd_options = { 'websiteId': 6189, 'runFromFrame': false };
	(function() {
		var w = window.apd_options.runFromFrame ? window.top : window;
		if(window.apd_options.runFromFrame && w!=window.parent) w=window.parent;
		if (w.location.hash.indexOf('apdAdmin') != -1){if(typeof(Storage) !== 'undefined') {w.localStorage.apdAdmin = 1;}}
		var adminMode = ((typeof(Storage) == 'undefined') || (w.localStorage.apdAdmin == 1));
		w.apd_options=window.apd_options;
		var apd = w.document.createElement('script'); apd.type = 'text/javascript'; apd.async = true;
		apd.src = '//' + (adminMode ? 'cdn' : 'ecdn') + '.firstimpression.io/' + (adminMode ? 'fi.js?id=' + window.apd_options.websiteId : 'fi_client.js') ;
		var s = w.document.getElementsByTagName('head')[0]; s.appendChild(apd);
	})();
</script>
<!-- END FIRSTIMPRESSION TAG -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-9394998891234553",
          enable_page_level_ads: true
     });
</script>
</head>
<body class="not-front not-logged-in page-node node-type-contentartist i18n-en one-sidebar">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        
  ga('create', 'UA-57480429-1', 'auto');
  ga('send', 'pageview');
</script>
<div id="header-wrapper">
<!-- header-wrapper-top -->
<!-- end header-wrapper-top -->
<!-- header -->
<div class="clear-block header-pos" id="header">
<div class="header-pos" id="headerleft"></div>
<div id="header-line">
<div id="header-logo"><a href="/" rel="home" title="Home"><img alt="Home" src="/sites/all/themes/atltheme/images/logo.png"/></a></div>
<div id="header-search">
<div class="searchformhead searchformhead-active" id="searchform1">
<div class="searchformhead-text"> </div>
<script>
  (function() {
    var cx = 'partner-pub-9394998891234553:4879285622';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchbox-only queryparametername="query" resultsurl="https://www.allthelyrics.com/google-search"></gcse:searchbox-only>
</div>
</div>
</div>
</div>
</div>
<div id="main-columns-wrapper">
<div id="main-menu">
<div id="buttonjoinus"><a href="/forum/register.php" title="Join us">Join us</a></div>
<div id="buttonsubmitlyrics"><a href="/atl/add/lyrics/1136877" title="Submit Lyrics">Add new song</a></div> <ul id="mainmenuul">
<li class="mainmenuli mainmenuli-open mainmenuli-active" id="mainmenuli"><a href="/">Lyrics</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/">Forum</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/activity.php">What's New?</a></li>
<li class="mainmenulili"><a href="/index">A-Z Artists</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Identify it</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=11">All</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=124">Non-English Songs</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=137">Music from TV</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=53">Store Musics Identify</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=136">Scat Lyrics Identify</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Lyrics translations</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=46">All</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=44">Arabic</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=48">French</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=50">German</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=32">Greek</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=113">Hebrew</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=47">Italian</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=65">Persian</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=33">Spanish</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=129">Turkish</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">Lyrics request</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=10">Lyrics request</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/showthread.php?t=13574">Please read before posting</a></li>
</ul>
</li>
<li class="mainmenuli nomobile" id="mainmenuli"><a href="/">More</a> <ul class="mainmenuulul">
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=13">Lyrics Review</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=67">Poetry</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=31">Tabs</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=30">Songs for Special Moments</a></li>
<li class="mainmenulili "><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=15">Music Events</a></li>
<li class="mainmenulili nomobile"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=17">Music instruments</a></li>
<li class="mainmenulili"><a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=20">Look for</a></li>
</ul>
</li>
</ul>
</div>
<div class="clear"></div>
<div id="header-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-9394998891234553",
enable_page_level_ads: true
});
</script>
<ins class="adsbygoogle ads-center-top" data-ad-client="ca-pub-9394998891234553" data-ad-slot="7664738822" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="clear"></div>
<div id="main-columns">
<div id="breadcrumb"><div class="breadcrumb"><a href="/">Home</a> » <a href="/index">Artists</a> » <a href="http://www.allthelyrics.com/lyrics/les_colocs">Les Colocs</a></div></div>
<div id="main-columns-inner">
<div class="clear-block " id="sidebar-first">
<div class="region region-sidebar-first sticky" id="sticky-sidebar" style="top: 10px;">
<div class="block block-atl" id="block-atl-atl-banner-left">
<div class="inner">
<div class="content">
<style type="text/css">
@media screen and (min-width: 701px) { .ads-left-top { width:300px; height:250px; } }
@media screen and (max-width: 700px) { .ads-left-top { width:120px; height:600px; } }
</style>
<div class="node-inside-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle ads-left-top" data-ad-client="ca-pub-9394998891234553" data-ad-slot="6638249221" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> </div>
</div>
</div>
<div class="block block-atl" id="block-atl-atl-banner-left-bottom">
<div class="inner">
<div class="content">
<div class="node-inside-banner">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle ads-left-bottom" data-ad-client="ca-pub-9394998891234553" data-ad-slot="4711272423" style="display:inline-block;"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> </div>
</div>
</div>
<img class="pixel" src="/sites/all/themes/atltheme/images/pixel.png"/>
</div>
</div> <!-- /#sidebar-first -->
<div id="main-wrapper">
<div id="main">
<div class="clear-block" id="page">
<h1 class="page-title">Les Colocs lyrics</h1>
<div class="region region-content">
<div class="node nodetype-artist " id="node-1136877">
<script>
/* BIT - Allthelyics.com - Flex */
cf_page_artist = "Les Colocs";
cf_page_song = "";
cf_adunit_id = "39381075";
cf_flex = true;
</script>
<script src="//srv.clickfuse.com/showads/showad.js"></script>
<div class="node-center">
<div class="content">
<div class="nodetype-artist-content-top">
<div class="nodetype-artist-content-top">
<div class="content-top-lyricslist">Les Colocs lyrics: 'Juste Une P'tite Nuite', 'Tassez-vous De D'l', 'Belz', 'La Rue Principale', 'La Maladresse' </div>
<div class="content-top-lyricspopular"><div class="artist-lyrics-list artist-lyrics-list-popular"><h2 class="artist-lyrics-list-h2">Top 5 songs</h2><ol><li class="lyrics-list-item lyrics-list-item-1585886"><a href="/lyrics/les_colocs/juste_une_ptite_nuite-lyrics-1080057.html">Juste Une P'tite Nuite</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253780"><a href="/lyrics/les_colocs/tassezvous_de_dl-lyrics-118392.html">Tassez-vous De D'l</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1255426"><a href="/lyrics/les_colocs/belz-lyrics-120594.html">Belz</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253774"><a href="/lyrics/les_colocs/la_rue_principale-lyrics-118383.html">La Rue Principale</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253773"><a href="/lyrics/les_colocs/la_maladresse-lyrics-118382.html">La Maladresse</a> <span></span></li></ol></div></div>
</div>
</div>
<div class="clear"></div>
<div class="nodetype-artist-content-lyrics-all"><div class="artist-lyrics-list artist-lyrics-list-all"><h2 class="artist-lyrics-list-h2">Les Colocs song lyrics</h2><div class="lyricslist-output" id="lyricslist-left"><ul><li class="lyrics-list-item lyrics-list-item-1255426"><a href="/lyrics/les_colocs/belz-lyrics-120594.html">Belz</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1255427"><a href="/lyrics/les_colocs/d-lyrics-120595.html">D</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1255428"><a href="/lyrics/les_colocs/dehors_novembre-lyrics-120596.html">Dehors Novembre</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1255429"><a href="/lyrics/les_colocs/je_chante_comme_une_casserole-lyrics-120597.html">Je Chante Comme Une Casserole</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253772"><a href="/lyrics/les_colocs/julie-lyrics-118380.html">Julie</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1585886"><a href="/lyrics/les_colocs/juste_une_ptite_nuite-lyrics-1080057.html">Juste Une P'tite Nuite</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253773"><a href="/lyrics/les_colocs/la_maladresse-lyrics-118382.html">La Maladresse</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253774"><a href="/lyrics/les_colocs/la_rue_principale-lyrics-118383.html">La Rue Principale</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253775"><a href="/lyrics/les_colocs/la_travers-lyrics-118384.html">La Travers</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253776"><a href="/lyrics/les_colocs/le_r-lyrics-118385.html">Le R</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1585887"><a href="/lyrics/les_colocs/maudit_qule_monde_est_beau-lyrics-1080058.html">Maudit Qu'le Monde Est Beau</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253777"><a href="/lyrics/les_colocs/pis_si_au_moins-lyrics-118389.html">Pis Si Au Moins</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253779"><a href="/lyrics/les_colocs/s_boogie-lyrics-118391.html">S Boogie</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253780"><a href="/lyrics/les_colocs/tassezvous_de_dl-lyrics-118392.html">Tassez-vous De D'l</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253781"><a href="/lyrics/les_colocs/tellement_longtemps-lyrics-118393.html">Tellement Longtemps</a> <span></span></li><li class="lyrics-list-item lyrics-list-item-1253782"><a href="/lyrics/les_colocs/tout_seul-lyrics-118394.html">Tout Seul</a> <span></span></li></ul></div></div></div> <div class="clear"></div>
<div class="nodetype-artist-content-lyrics-submit"><a class="atladdlyrics" href="/atl/add/lyrics/1136877">Submit new song</a></div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div> <!-- /#page -->
</div> <!-- /#main -->
</div> <!-- /#main-wrapper -->
</div>
</div> <!-- /#main-columns -->
<div id="footer">
<div id="footer-menu">
<p>
<span class="footer-logo-text"><a href="/">AllTheLyrics.com</a></span>
<a href="/index">A-Z Artists</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=46">Lyrics translations</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=11">Identify</a>   |  
        <a href="https://www.allthelyrics.com/forum/forumdisplay.php?f=10">Lyrics request</a>   
        </p>
</div>
<div id="footer-languages">
<div class="footer-languages">
<p>Translate interface: English
 | <a href="http://www.allthelyrics.com/fr/lyrics/les_colocs">Français</a>
 | <a href="http://www.allthelyrics.com/de/lyrics/les_colocs">Deutsch</a>
 | <a href="http://www.allthelyrics.com/it/lyrics/les_colocs">Italiano</a>
 | <a href="http://www.allthelyrics.com/es/lyrics/les_colocs">Español</a>
</p>
<p><a href="/privacy.htm">Privacy Policy</a>  |  <a href="/dmca-policy.htm">DMCA Policy</a>  |  <a href="/contact">Contact us</a></p>
<p>All lyrics are property and copyright of their owners.<br/>All lyrics provided for educational purposes only.</p>
</div>
</div>
<div class="clear"></div>
</div>
</div> <!-- /#main-columns-wrapper -->
</body>
</html>
