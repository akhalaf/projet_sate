<!DOCTYPE html>
<html class="no-js no-svg" lang="es" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Página no encontrada - Tekton Technologies</title>
<script>
                            /* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
                            if ( typeof WebFontConfig === "undefined" ) {
                                WebFontConfig = new Object();
                            }
                            WebFontConfig['google'] = {families: ['Roboto:400', 'Rajdhani:700', 'Poppins:400,500&amp;subset=latin']};

                            (function() {
                                var wf = document.createElement( 'script' );
                                wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js';
                                wf.type = 'text/javascript';
                                wf.async = 'true';
                                var s = document.getElementsByTagName( 'script' )[0];
                                s.parentNode.insertBefore( wf, s );
                            })();
                        </script>
<!-- This site is optimized with the Yoast SEO plugin v13.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="es_ES" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Página no encontrada - Tekton Technologies" property="og:title"/>
<meta content="Tekton Technologies" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Página no encontrada - Tekton Technologies" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.tektontech.com/#website","url":"https://www.tektontech.com/","name":"Tekton Technologies","inLanguage":"es","description":"Somos una empresa colombiana l\u00edder en el desarrollo de soluciones tecnologicas en e-commerce","potentialAction":[{"@type":"SearchAction","target":"https://www.tektontech.com/?s={search_term_string}","query-input":"required name=search_term_string"}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.tektontech.com/feed/" rel="alternate" title="Tekton Technologies » Feed" type="application/rss+xml"/>
<link href="https://www.tektontech.com/comments/feed/" rel="alternate" title="Tekton Technologies » Feed de los comentarios" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.14.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.14.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-73448557-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-73448557-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('require', 'linkid', 'linkid.js');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Función desactivada __gaTracker(' + arguments[0] + " ....) porque no estás siendo rastreado. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.tektontech.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.tektontech.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.3.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.0.5" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://www.tektontech.com/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.min.css?ver=7.14.0" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.3" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/themes/pixzlo/assets/css/theme.min.css?ver=1.0" id="pixzlo-min-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/themes/pixzlo/style.css?ver=1.0" id="pixzlo-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/themes/pixzlo/assets/css/shortcode.css?ver=1.0" id="pixzlo-shortcode-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tektontech.com/wp-content/uploads/pixzlo/theme_1.css?ver=1.0" id="pixzlo-theme-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://www.tektontech.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.3' type='text/css' media='screen' />
<![endif]-->
<link href="https://www.tektontech.com/wp-content/themes/pixzlo-child/style.css" id="pixzlo-child-theme-style-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/www.tektontech.com","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.tektontech.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.14.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/plugins/revslider/public/assets/js/revolution.tools.min.js?ver=6.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.0.5" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/html5.js?ver=3.7.3'></script>
<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var ajax_var = {"url":"https:\/\/www.tektontech.com\/wp-admin\/admin-ajax.php","nonce":"cbed34c7ab","pageCurrent":null};
/* ]]> */
</script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo-child/js/site.js?version=20190703&amp;ver=5.3.6" type="text/javascript"></script>
<link href="https://www.tektontech.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.tektontech.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.tektontech.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<meta content="Powered by Slider Revolution 6.0.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<link href="https://www.tektontech.com/wp-content/uploads/2019/09/favicon-80x80.png" rel="icon" sizes="32x32"/>
<link href="https://www.tektontech.com/wp-content/uploads/2019/09/favicon.png" rel="icon" sizes="192x192"/>
<link href="https://www.tektontech.com/wp-content/uploads/2019/09/favicon.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.tektontech.com/wp-content/uploads/2019/09/favicon.png" name="msapplication-TileImage"/>
<script type="text/javascript">function setREVStartSize(a){try{var b,c=document.getElementById(a.c).parentNode.offsetWidth;if(c=0===c||isNaN(c)?window.innerWidth:c,a.tabw=void 0===a.tabw?0:parseInt(a.tabw),a.thumbw=void 0===a.thumbw?0:parseInt(a.thumbw),a.tabh=void 0===a.tabh?0:parseInt(a.tabh),a.thumbh=void 0===a.thumbh?0:parseInt(a.thumbh),a.tabhide=void 0===a.tabhide?0:parseInt(a.tabhide),a.thumbhide=void 0===a.thumbhide?0:parseInt(a.thumbhide),a.mh=void 0===a.mh||""==a.mh?0:a.mh,"fullscreen"===a.layout||"fullscreen"===a.l)b=Math.max(a.mh,window.innerHeight);else{for(var d in a.gw=Array.isArray(a.gw)?a.gw:[a.gw],a.rl)(void 0===a.gw[d]||0===a.gw[d])&&(a.gw[d]=a.gw[d-1]);for(var d in a.gh=void 0===a.el||""===a.el||Array.isArray(a.el)&&0==a.el.length?a.gh:a.el,a.gh=Array.isArray(a.gh)?a.gh:[a.gh],a.rl)(void 0===a.gh[d]||0===a.gh[d])&&(a.gh[d]=a.gh[d-1]);var e,f=Array(a.rl.length),g=0;for(var d in a.tabw=a.tabhide>=c?0:a.tabw,a.thumbw=a.thumbhide>=c?0:a.thumbw,a.tabh=a.tabhide>=c?0:a.tabh,a.thumbh=a.thumbhide>=c?0:a.thumbh,a.rl)f[d]=a.rl[d]<window.innerWidth?0:a.rl[d];for(var d in e=f[0],f)e>f[d]&&0<f[d]&&(e=f[d],g=d);var h=c>a.gw[g]+a.tabw+a.thumbw?1:(c-(a.tabw+a.thumbw))/a.gw[g];b=a.gh[g]*h+(a.tabh+a.thumbh)}void 0===window.rs_init_css&&(window.rs_init_css=document.head.appendChild(document.createElement("style"))),document.getElementById(a.c).height=b,window.rs_init_css.innerHTML+="#"+a.c+"_wrapper { height: "+b+"px }"}catch(a){console.log("Failure at Presize of Slider:"+a)}};</script>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 wp-embed-responsive wpb-js-composer js-comp-ver-6.0.3 vc_responsive" data-rsssl="1" data-scroll-distance="" data-scroll-time="">
<div class="mobile-header">
<div class="mobile-header-inner hidden-lg-up hidden-lg-land-up">
<div class="container">
<ul class="mobile-header-items nav pull-left">
<li class="nav-item">
<div class="nav-item-inner">
<div class="mobile-logo"><a href="https://www.tektontech.com/" title="Tekton Technologies"><img alt="Tekton Technologies" class="img-responsive" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton.png" title="Tekton Technologies"/></a></div> </div>
</li>
</ul>
<ul class="mobile-header-items nav pull-right">
<li class="nav-item">
<div class="nav-item-inner">
<a class="mobile-bar-toggle" href="#"><i class="fa fa-bars"></i></a> </div>
</li>
</ul>
</div><!-- container -->
</div>
</div>
<div class="mobile-bar animate-from-top">
<a class="mobile-bar-toggle close" href="#"></a>
<div class="mobile-bar-inner">
<div class="container">
<ul class="mobile-bar-items nav flex-column mobile-bar-middle">
<li class="nav-item">
<div class="nav-item-inner">
<div class="mobile-logo"><a href="https://www.tektontech.com/" title="Tekton Technologies"><img alt="Tekton Technologies" class="img-responsive" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton.png" title="Tekton Technologies"/></a></div> </div>
</li>
<li class="nav-item">
<div class="nav-item-inner">
<div class="pixzlo-mobile-main-menu"></div> </div>
</li>
<li class="nav-item">
<div class="nav-item-inner">
</div>
</li>
</ul>
</div><!-- container -->
</div>
</div>
<div class="page-loader"></div>
<div class="pixzlo-wrapper" id="page">
<header class="pixzlo-header header-absolute">
<div class="header-inner hidden-md-down hidden-md-land-down">
<div class="sticky-outer"> <div class="sticky-head"> <nav class="navbar clearfix">
<div class="custom-container navbar-inner">
<ul class="navbar-items nav pull-left">
<li class="nav-item">
<div class="nav-item-inner">
<div class="main-logo">
<a href="https://www.tektontech.com/" title="Tekton Technologies"><img alt="Tekton Technologies" class="custom-logo img-responsive" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton-white.png" title="Tekton Technologies"/></a>
</div><div class="sticky-logo"><a href="https://www.tektontech.com/" title="Tekton Technologies"><img alt="Tekton Technologies" class="img-responsive" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton.png" title="Tekton Technologies"/></a></div> </div>
</li>
<li class="nav-item">
<div class="nav-item-inner">
</div>
</li>
</ul>
<ul class="navbar-items nav pull-right">
<li class="nav-item">
<div class="nav-item-inner">
<ul class="nav pixzlo-main-menu" id="pixzlo-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home nav-item menu-item-4148" id="menu-item-4148"><a class="nav-link" href="https://www.tektontech.com/">Inicio</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children nav-item menu-item-4221 dropdown" id="menu-item-4221"><a class="nav-link dropdown-toggle" href="#">Tecnología</a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4339" id="menu-item-4339"><a class="nav-link" href="https://www.tektontech.com/servicios/web-ecommerce/">Web &amp; eCommerce</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4338" id="menu-item-4338"><a class="nav-link" href="https://www.tektontech.com/servicios/bi-where2buy/">BI – Where2buy®</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4488" id="menu-item-4488"><a class="nav-link" href="https://www.tektontech.com/servicios/cyber-eventos-pav/">Cyber Eventos – PAV®</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4337" id="menu-item-4337"><a class="nav-link" href="https://www.tektontech.com/servicios/inteligencia-artificial/">Inteligencia artificial</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-5665" id="menu-item-5665"><a class="nav-link" href="https://www.tektontech.com/servicios/integraciones/">Integraciones</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4489" id="menu-item-4489"><a class="nav-link" href="https://www.tektontech.com/servicios/fabrica-de-software/">Fábrica de Software</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4493" id="menu-item-4493"><a class="nav-link" href="https://www.tektontech.com/servicios/otras-soluciones/">Otras soluciones</a></li>
</ul></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-5589" id="menu-item-5589"><a class="nav-link" href="https://www.tektontech.com/agencia/">Agencia Digital</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children nav-item menu-item-5117 dropdown" id="menu-item-5117"><a class="nav-link dropdown-toggle" href="#">Partners</a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-5118" id="menu-item-5118"><a class="nav-link" href="https://www.tektontech.com/partners/iu/">IU</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-5645" id="menu-item-5645"><a class="nav-link" href="https://www.tektontech.com/servicios/vtex/">VTEX</a></li>
</ul></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4149" id="menu-item-4149"><a class="nav-link" href="https://www.tektontech.com/cultura-tekton/">Cultura Tekton</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4526" id="menu-item-4526"><a class="nav-link" href="https://www.tektontech.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-4225" id="menu-item-4225"><a class="nav-link" href="https://www.tektontech.com/contactanos/">Contáctanos</a></li>
</ul> </div>
</li>
<li class="nav-item">
<div class="nav-item-inner">
<ul class="nav"><li class="menu-item dropdown mini-cart-items"></li></ul> </div>
</li>
<li class="nav-item">
<div class="nav-item-inner">
<div class="search-toggle-wrap"><div class="bottom-search-wrap">
<form action="https://www.tektontech.com/" class="search-form" method="get">
<div class="input-group">
<input class="form-control" name="s" placeholder="Search for..." type="text" value=""/>
<span class="input-group-btn">
<button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
</span>
</div>
</form></div>
<a class="bottom-search-toggle" href="#"><i class="fa fa-search"></i></a></div> </div>
</li>
</ul>
</div>
</nav>
</div><!--stikcy outer-->
</div><!-- sticky-head or sticky-scroll --> </div>
</header>
<div class="pixzlo-content-wrapper"><div class="wrap pixzlo-page">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<section class="error-404 not-found text-center">
<div class="container">
<header class="page-header">
<div class="404-image-wrap">
<img src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/images/404.jpg"/>
</div>
<div class="relative mb-2">
<h3 class="page-title">Page Not Found</h3>
</div>
<p class="error-description">
								Go Back to 								<a href="https://www.tektontech.com/">
									Home Page								</a>
</p>
</header><!-- .page-header -->
</div><!-- .container -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .wrap -->
</div><!-- .pixzlo-content-wrapper -->
<footer class="site-footer">
<div class="footer-middle-wrap">
<div class="container">
<div class="row">
<div class="col-lg-3">
<div class="footer-middle-sidebar">
<section class="widget widget_media_image" id="media_image-1"><img alt="" class="image wp-image-5379 attachment-full size-full" height="464" sizes="(max-width: 1000px) 100vw, 1000px" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton.png" srcset="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton.png 1000w, https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton-768x356.png 768w" style="max-width: 100%; height: auto;" width="1000"/></section><section class="widget widget_text" id="text-4"> <div class="textwidget"><p>Arquitectos de Ecommerce. Creemos en la innovación tecnológica y el poder de la información para optimizar procesos y aumentar resultados.</p>
</div>
</section><section class="widget zozo_social_widget" id="zozo_social_widget-1">
<ul class="nav social-icons social-widget widget-content social-circled social-theme social-h-white social-bg-transparent social-hbg-black">
<li><a class="social-fb" href="https://www.facebook.com/Tektontech" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a class="social-twitter" href="https://twitter.com/TektonTech" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a class="social-linkedin" href="https://www.linkedin.com/company/tekton-technologies-de-colombia" target="_blank"><i class="fa fa-linkedin"></i></a></li>
</ul>
</section> </div>
</div>
<div class="col-lg-3">
<div class="footer-middle-sidebar">
<section class="widget widget_nav_menu" id="nav_menu-5"><h3 class="widget-title">Servicios</h3><div class="menu-menu-servicios-container"><ul class="menu" id="menu-menu-servicios"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4775" id="menu-item-4775"><a href="https://www.tektontech.com/servicios/web-ecommerce/">Web &amp; eCommerce</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4774" id="menu-item-4774"><a href="https://www.tektontech.com/servicios/bi-where2buy/">BI – Where2buy®</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4773" id="menu-item-4773"><a href="https://www.tektontech.com/servicios/inteligencia-artificial/">Inteligencia artificial</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4776" id="menu-item-4776"><a href="https://www.tektontech.com/servicios/cyber-eventos-pav/">Cyber Eventos – PAV®</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4777" id="menu-item-4777"><a href="https://www.tektontech.com/servicios/fabrica-de-software/">Fábrica de Software</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4778" id="menu-item-4778"><a href="https://www.tektontech.com/servicios/otras-soluciones/">Otras soluciones</a></li>
</ul></div></section> </div>
</div>
<div class="col-lg-3">
<div class="footer-middle-sidebar">
<section class="widget widget_nav_menu" id="nav_menu-6"><h3 class="widget-title">Legales</h3><div class="menu-menu-footer-container"><ul class="menu" id="menu-menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4987" id="menu-item-4987"><a href="https://www.tektontech.com/trabaja-con-nosotros/">Trabaja con nosotros</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5200" id="menu-item-5200"><a href="https://www.tektontech.com/wp-content/uploads/2019/09/POLÍTICAS-DE-MANEJO-DE-DATOS-PERSONALES.pdf" rel="noopener noreferrer" target="_blank">Política de manejo de datos personales</a></li>
</ul></div></section> </div>
</div>
<div class="col-lg-3">
<div class="footer-middle-sidebar">
<div class="widget widget_text" id="text-2"><h3 class="widget-title">Ubicación</h3> <div class="textwidget"><h4>Colombia</h4>
<p>Bogotá<br/>
<span class="dir">Calle 124 # 45 – 15 Oficina 401</span><br/>
<span class="tel">+(57) 1 7039015 – 1 6221683</span></p>
</div>
</div><div class="widget widget_text" id="text-3"> <div class="textwidget"><h4>United States</h4>
<p>Mid Cape Corporate Center<br/>
<span class="dir">125 SW 3rd Place, Suite 201 Cape Coral, Florida 33991, US</span></p>
</div>
</div><div class="widget_text widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget"><div class="floater"><a href="https://www.tektontech.com/contactanos/"></a></div></div></div> </div>
</div> </div>
</div>
</div>
<div class="footer-bottom">
<div class="footer-bottom-inner container">
<div class="row">
<div class="col-md-12">
<ul class="footer-bottom-items nav pull-left">
<li class="nav-item">
<div class="nav-item-inner">
					2019 © Todos los derechos reservados 						</div>
</li>
</ul>
<ul class="footer-bottom-items nav pull-right">
<li class="nav-item">
<div class="nav-item-inner">
<div class="footer-bottom-widget">
<section class="widget_text widget widget_custom_html" id="custom_html-3"><div class="textwidget custom-html-widget">Diseñado y desarrollado por Tekton Technologies de Colombia S.A <img alt="Tekton Technologies" class="custom-logo img-responsive" src="https://www.tektontech.com/wp-content/uploads/2019/09/logo-tekton-white.png" title="Tekton Technologies" width="100px"/></div></section> </div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
<a class="back-to-top" href="#" id="back-to-top"><i class="fa fa-angle-up"></i></a>
</footer><!-- #colophon -->
</div><!-- #page -->
<script async="" src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/6c56b37c-a4ad-4255-9087-3c27747db321-loader.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.tektontech.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.tektontech.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LdnR-kUAAAAAOILgBJFl5xZ6luFgVrtwoLCUuXG&amp;ver=3.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/popper.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/bootstrap.min.js?ver=4.1.1" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/smart-resize.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/owl.carousel.min.js?ver=2.2.1" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=6.0.3" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/infinite-scroll.pkgd.min.js?ver=2.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.stellar.min.js?ver=0.6.2" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/sticky-kit.min.js?ver=1.1.3" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.mb.YTPlayer.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.magnific.popup.min.js?ver=1.1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.easy.ticker.min.js?ver=2.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.easing.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.countdown.min.js?ver=2.2.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.circle.progress.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/jquery.appear.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/smoothscroll.min.js?ver=1.20.2" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var pixzlo_ajax_var = {"admin_ajax_url":"https:\/\/www.tektontech.com\/wp-admin\/admin-ajax.php","like_nonce":"c16529e435","fav_nonce":"c5221ef89b","infinite_loader":"https:\/\/www.tektontech.com\/wp-content\/uploads\/2018\/11\/page-loader.gif","load_posts":"Loading next set of posts.","no_posts":"No more posts to load.","cmt_nonce":"6b678df774","mc_nounce":"043b6681ba","wait":"Wait..","must_fill":"Must Fill Required Details.","valid_email":"Enter Valid Email ID.","cart_update_pbm":"Cart Update Problem."};
/* ]]> */
</script>
<script src="https://www.tektontech.com/wp-content/themes/pixzlo/assets/js/theme.js?ver=1.0" type="text/javascript"></script>
<script src="https://www.tektontech.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script type="text/javascript">
( function( grecaptcha, sitekey, actions ) {

	var wpcf7recaptcha = {

		execute: function( action ) {
			grecaptcha.execute(
				sitekey,
				{ action: action }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		},

		executeOnHomepage: function() {
			wpcf7recaptcha.execute( actions[ 'homepage' ] );
		},

		executeOnContactform: function() {
			wpcf7recaptcha.execute( actions[ 'contactform' ] );
		},

	};

	grecaptcha.ready(
		wpcf7recaptcha.executeOnHomepage
	);

	document.addEventListener( 'change',
		wpcf7recaptcha.executeOnContactform, false
	);

	document.addEventListener( 'wpcf7submit',
		wpcf7recaptcha.executeOnHomepage, false
	);

} )(
	grecaptcha,
	'6LdnR-kUAAAAAOILgBJFl5xZ6luFgVrtwoLCUuXG',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
</body>
</html>