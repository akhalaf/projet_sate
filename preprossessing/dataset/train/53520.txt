<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>A-Z Rental Center - Equipment Rentals in Eden Prairie MN</title>
<meta content="A-Z Rental Center - We specialize in equipment and tool rentals in Eden Prairie Minnesota, Bloomington, Minnetonka, Chanhassen, Chaska, Edina, Richfield, Shakopee, Plymouth MN" name="description"/>
<meta content="A-Z Rental Center - Eden Prairie MN, Equipment Rentals, construction rentals, generator, welding, compressor, lifts, power tools, contractor, used equipment, heavy equipment, forklift" name="keywords"/>
<link href="porcore.css?v=2.84" rel="stylesheet" type="text/css"/>
<link href="porstyle.css?v=2.84" rel="stylesheet" type="text/css"/>
<link href="css/icons.min.css?v=2.84" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
<script defer="defer" src="js/jssor.js" type="text/javascript"></script>
<script defer="defer" src="js/jssor.slider.min.js" type="text/javascript"></script>
<script defer="defer" src="js/slideshow.js" type="text/javascript"></script>
<script src="js/stellar.js" type="text/javascript"></script>
<meta content="width=1024" name="viewport"/>
<link href="favicon.png" rel="shortcut icon"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Cinzel:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<script src="js/doubletaptogo.min.js" type="text/javascript"></script>
<script src="js/ios6fix.js" type="text/javascript"></script>
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 7]> <script type="text/javascript" src="js/unitpngfix.js"></script> <![endif]--><script type="text/javascript">
//  A-Z Rental Eden Prairie MN   www.a-zrental.com from store   avfarci_c and avfarci_g
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7780020-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7794757-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</head>
<body>
<div class="wrapper">
<a id="top"></a><div class="go-to-top"><a href="#top"><i class="fa fa-arrow-up"></i></a></div>
<div class="cf" id="porheader-wrap">
<div class="header cf">
<div class="logo"><a href="index.asp"><img alt="A-Z Rental Center - Equipment Rentals in Eden Prairie MN" src="images/logo.png?v=2.0"/></a></div>
<div class="tagline">
<div style="float: left; margin-left: 2%;"><span></span></div>
</div>
<div class="header-right-pane">
<div class="locations" id="header-links">
<ul style="margin: 0px;">
<li style="margin-right: 20px;"><span>Call A-Z Rental Center</span> <a href="tel://9529448040">952-944-8040</a></li>
<li><a class="facebook" href="https://www.facebook.com/A-to-Z-Rental-2248919338766235/" target="_blank" title="A-Z Rental Center on Facebook"><i class="fa fa-facebook"></i></a></li>
</ul>
</div>
<div id="search-wrap">
<div id="search">
<form action="search.asp" class="search" id="search-form-header" method="get">
<input id="search-box" name="search" placeholder="Search our products..." type="text"/>
<a class="btn-search" href="javascript:void(0);" onclick="$('form#search-form-header').submit(); return false;" title="Search our products..."><i class="fa fa-search"></i></a>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-wrap">
<div class="navbar">
<ul>
<li class="sticky"><a class="sticky-logo first" href="index.asp" title="Home of A-Z Rental Center"><img alt="Home" src="images/logo.png"/></a></li>
<li class="sticky"><a class="first" href="tel://9529448040"><span class="phone">952-944-8040</span></a></li>
<li><a class="first" href="equipment-rentals.asp" title="Equipment rentals in the Twin Cities Metro area">Equipment &amp; Tools</a></li>
<li class="divider">-</li>
<li><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank" title="Stihl equipment for sale in the Twin Cities Metro area">Stihl</a></li>
<li class="divider">-</li>
<li><a href="supplies.asp" title="Supplies for sale in the Twin Cities Metro area">Supplies</a></li>
<li class="divider">-</li>
<li><a href="services.asp" title="Equipment services in the Twin Cities Metro area">Services</a></li>
<li class="divider">-</li>
<li><a href="about-us.asp" title="About Us">About Us</a></li>
<li class="divider">-</li>
<li><a href="resources.asp">Resources</a></li>
<li class="divider">-</li>
<li><a href="contact-us.asp" title="Contact Us">Contact</a></li>
<li>
</li><li class="sticky dropdown left">
<a class="sticky-search icon" href="javascript:void(0);" title="Search our inventory"><i class="fa fa-search"></i></a>
<ul class="toggle-search">
<li>
<form action="search.asp" class="search" id="search-form" method="get">
<input id="sticky-search-box" name="search" placeholder="Search our products..." type="text"/>
<a class="btn-search" href="#" onclick="$('form#search-form').submit(); return false;" title="Search our products..."><i class="fa fa-search"></i></a>
</form>
</li>
</ul>
</li>
</ul>
</div>
</div> <div id="slideshow-wrap">
<div class="slideshow-dimensions" id="slider1_container">
<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
<div style="position: absolute; display: block; background: url(images/loading.gif) no-repeat center center; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
</div>
<div class="slides slideshow-dimensions" data-u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; overflow: hidden;">
<div><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl Equipment sales in the Twin Cities Metro area" data-u="image" height="400" src="slideshow/slide-1.jpg" width="1600"/></a></div>
<div><a href="equipment-rentals.asp"><img alt="Tool rentals in the Twin Cities Metro area" data-u="image" height="400" src="slideshow/slide-2.jpg" width="1600"/></a></div>
<div><a href="equipment-rentals.asp"><img alt="Equipment rentals in the Twin Cities Metro area" data-u="image" height="400" src="slideshow/slide-3.jpg" width="1600"/></a></div>
</div>
<div class="jssorb21" data-u="navigator">
<div data-u="prototype" style="position: absolute; width: 19px; height: 19px; text-align: center; line-height: 19px; color: #FFF; font-size: 12px;"></div>
</div>
<span class="jssora21l" data-u="arrowleft"></span>
<span class="jssora21r" data-u="arrowright"></span>
</div>
</div>
<h1 class="index">Specializing in Equipment Rentals &amp; Tool Rentals in Eden Prairie MN and the Twin Cities Metro area since 1983</h1>
<div class="poricons-wrap bg-light-gray border-bottom cf">
<ul>
<li>
<a href="equipment-rentals.asp"><img alt="Equipment rentals at A-Z Rental Center serving Eden Prairie Minnesota, Bloomington, Minnetonka, Chanhassen, Chaska, Edina, Richfield, Shakopee, Plymouth MN" src="images/icon-equipment-rentals.jpg" style="border-color: #CCC;"/></a>
<div class="icons-content">
<h2>Equipment &amp; Tool Rentals</h2>
<p>A-Z Rental Center is a full-time rental center, providing a wide range of quality rental equipment for homeowners and contractors. </p>
</div>
</li>
<li>
<a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl equipment sales at A-Z Rental Center serving Eden Prairie Minnesota, Bloomington, Minnetonka, Chanhassen, Chaska, Edina, Richfield, Shakopee, Plymouth MN" src="images/icon-stihl-sales.jpg" style="border-color: #CCC;"/></a>
<div class="icons-content">
<h2>Stihl Sales</h2>
<p>We are an authorized Stihl Dealer and Repair Shop. Stop in and see the extensive line of Stihl equipment we service and sell! </p>
</div>
</li>
<li>
<a href="supplies.asp"><img alt="Contractor Supplies at A-Z Rental Center serving Eden Prairie Minnesota, Bloomington, Minnetonka, Chanhassen, Chaska, Edina, Richfield, Shakopee, Plymouth MN" src="images/icon-supplies.jpg"/></a>
<div class="icons-content">
<h2>Supplies</h2>
<p>We offer a full line of supplies to support our rental equipment - blades, safety items, plus propane refills and moving supplies.</p>
</div>
</li>
</ul>
<ul style="margin-top: 15px;">
<li><div class="readmore button"><a href="equipment-rentals.asp">Equipment  &amp; Tool Rentals</a></div></li>
<li><div class="readmore button"><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank">Stihl Sales</a></div></li>
<li><div class="readmore button"><a href="supplies.asp">Supplies</a></div></li>
</ul>
</div> <div class="featured-wrap">
<h2 class="featured">Your Local Authorized Stihl Dealer in Eden Prairie</h2>
<ul>
<li><h2><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl chainsaw sales in the Twin Cities Metro area" src="images/featured-item-1.jpg?v=2.84"/><span>MSA 120 C-BQ</span></a></h2></li>
<li><h2><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl pressure washer sales in the Twin Cities Metro area" src="images/featured-item-2.jpg?v=2.84"/><span>RB 400 DIRT BOSS®</span></a></h2></li>
<li><h2><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl blower sales in the Twin Cities Metro area" src="images/featured-item-3.jpg?v=2.84"/><span>BR 700</span></a></h2></li>
<li class="last"><h2><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank"><img alt="Stihl KombiSystem sales in the Twin Cities Metro area" src="images/featured-item-4.jpg?v=2.84"/><span>KM 91 R</span></a></h2></li>
</ul>
</div> <div class="footer-wrap">
<div class="footer">
<ul class="footer-categories first">
<li><h3>A-Z Rental Center</h3></li>
<li>
<ul>
<li><a href="index.asp">Home</a></li>
<li><a href="equipment-rentals.asp">Tool Rentals</a></li>
<li><a href="https://atozequipmentedenprairie.stihldealer.net/" target="_blank">Stihl Sales</a></li>
<li><a href="supplies.asp">Supplies</a></li>
<li><a href="services.asp">Services</a></li>
</ul>
<ul class="last">
<li><a href="about-us.asp">About Us</a></li>
<li><a href="contact-us.asp">Location &amp; Hours</a></li>
<li><a href="feedback.asp">Feedback</a></li>
<li><a href="resources.asp">Resources</a></li>
</ul>
</li>
</ul>
<ul class="footer-categories middle" style="border-left: 1px solid #EEE; padding-left: 35px;">
<li><h3>Popular Categories</h3></li>
<li>
<ul>
<li><a href="equipment.asp?action=category&amp;category=22">Compressors</a></li>
<li><a href="equipment.asp?action=category&amp;category=42">Carpentry</a></li>
<li><a href="equipment.asp?action=category&amp;category=43">Concrete Tools</a></li>
<li><a href="equipment.asp?action=category&amp;category=23">Excavation</a></li>
<li><a href="equipment.asp?action=category&amp;category=49">Generators</a></li>
</ul>
<ul>
<li><a href="equipment.asp?action=category&amp;category=26">Ladder &amp; Scaffold</a></li>
<li><a href="equipment.asp?action=category&amp;category=4">Lawn &amp; Garden</a></li>
<li><a href="equipment.asp?action=category&amp;category=21">Lifts</a></li>
<li><a href="equipment.asp?action=category&amp;category=45">Moving</a></li>
<li><a href="equipment.asp?action=category&amp;category=51">Painting</a></li>
</ul>
<ul class="last">
<li><a href="equipment.asp?action=category&amp;category=60">Plumbing</a></li>
<li><a href="equipment.asp?action=category&amp;category=53">Pressure Washers</a></li>
<li><a href="equipment.asp?action=category&amp;category=52">Pumps</a></li>
<li><a href="equipment.asp?action=category&amp;category=36">Trailers</a></li>
<li><a href="equipment.asp?action=category&amp;category=9">Trucks</a></li>
</ul>
</li>
</ul>
<div style="clear: both; height: 15px;"> </div>
<hr class="soft"/>
<p style="margin-top: 10px; font-size: 14px; text-align: center;">Address: 12450 Plaza Drive, Eden Prairie, MN 55344 |  For information call: <a href="tel://9529448040"> <span class="phone">952-944-8040</span></a> | <a href="feedback.asp">E-mail  <img alt="Send us feedback" height="16" src="images/icon-email.png" width="26"/></a></p>
<hr class="soft"/>
<div class="serving">Serving the cities of Eden Prairie, Minnetonka, Chanhassen, Chaska, Bloomington, Edina, Plymouth, St. Louis Park, Shakopee, Victoria, Minneapolis and the Twin Cities Metropolitan area with all your equipment rental and Stihl equipment needs!</div>
<div class="copyright">Copyright © 2021 RentalHosting.com</div>
<div class="last-update">
          
           Powered by Point-of-Rental - Last Update: 12/17/2020 2:01:06 AM
        </div>
</div>
</div>
<script>
      // Sticky search menu
      $(document).ready(function() {
        // Sticky menu
        var stickyOffset = $('.navbar-wrap').offset().top;
        $(window).scroll(function() {
          var sticky = $('.navbar-wrap'), scroll = $(window).scrollTop();
          if (scroll >= stickyOffset) {
            sticky.addClass('fixed-navbar');
            $('.navbar-wrap .navbar li.sticky').show();
            $('.go-to-top').fadeIn();
          } else {
            sticky.removeClass('fixed-navbar');
            $('.navbar-wrap .navbar li.sticky').hide();
            $('.go-to-top').fadeOut();
          }
          // Check the location of each desired element
          $('.hideme').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
              $(this).delay(100).animate({'opacity':'1'},500);
            }
          });
        });
        $("ul.toggle-search").hide();
        $(".sticky-search").click(function() {
          $("ul.toggle-search").toggle("fast");
          $("#sticky-search-box").focus();
          return false;
        });
      });
      // Scroll to top
      $("a[href='#top']").click(function() {
        $('.navbar-wrap .navbar li.sticky').hide();
        $("ul.toggle-search").hide();
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
      });
      // Scroll to top on orientaion change
      $(window).on("orientationchange", function(event) {
        $(".navbar-wrap").removeClass("sticky");
        $("html, body").animate({scrollTop : 0}, 200);
          return false;
      });
      // FAQs
      $('.hide-faq').hide();
      $('.faq h3').click(function() {
        var plusSign = "+";
        var minusSign = "-";
        $(this).toggleClass('black');
        if($(this).find('span').text()==plusSign) {
          $(this).find('span').text(minusSign);
        } else {
          $(this).find('span').text(plusSign);
        }
        $(this).nextAll('.hide-faq').toggle("fast");
        return false;
       });
        // Locations
        $('.hide-location').hide();
        $(".show-location").click(function(e) {
          e.preventDefault();
          $('.store-locations-wrap .store-location').hide();
          $('html, body').animate({scrollTop:$('#scroll-to-location').position().top});
          $('#' + $(this).data('rel')).fadeIn('slow');
        });
        // Tileshow
        $(".tileshow ul li").click(function() {		       
          $(".panel", this).slideToggle("fast");    
        });
        // Check for mobile devices
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if( !isMobile.any() ){
          // Parallax
          $(function(){
            $.stellar({
              horizontalScrolling: false,
              responsive: true
            });
          });
          // Hide then show elements
          $('.hideme').css({opacity:'.1'});
        } else {
          // Disable Hide/Show elements
          $('.hideme').css({opacity:'1'});
        }
    </script>
</div>
</body>
</html>