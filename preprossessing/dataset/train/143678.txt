<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<title>Ejercicios de espaÃ±ol: gramÃ¡tica, vocabulario, lecturas...</title>
<meta content="Ejercicios de espaÃ±ol: gramÃ¡tica, vocabulario, lecturas. Ejercicios de espaÃ±ol para todos los niveles ele. Spanish exercises." name="Description"/>
<meta content="gramÃ¡tica, ejercicios, espaÃ±ol, castellano, niÃ±os, extranjera, vocabulario, verbos, videos, cuentos, lecturas, canciones, actividades, ELE, spanish, exercises, primaria, principiantes" name="Keywords"/>
<meta content="global" name="distribution"/>
<meta content="index, follow" name="robots"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="inde.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&amp;display=swap" rel="stylesheet"/>
<link href="https://aprenderespanol.org/imaxes/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style type="text/css"></style>
<!-- Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-53022008-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-53022008-1');
</script>
<!-- End Google Analytics -->
<script type="text/javascript"><!--
function enlaceBlank() {
	for(var i = 0; document.links[i]; i++) {
		document.links[i].target = '_blank';
	}
}
//--></script>
</head>
<body>
<div id="wrapper">
<div id="head">
<div id="head-left"><a href="https://aprenderespanol.org/"><img alt="Ejercicios de espaÃ±ol" src="https://aprenderespanol.org/imaxes/aprenderespanol-logo.gif" title="aprenderespanol.org"/></a></div>
<div id="head-right">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aprenderespanol-728x90-top -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0114706156691227" data-ad-slot="1320214787" style="display:inline-block;width:728px;height:90px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
<div id="content">
<div id="left">
<div class="side-top"> Recursos Ãºtiles</div>
<div class="top-subtext" id="left-side"> <a class="sidelink" href="https://agendaweb.org/" rel="nofollow"><img alt="" height="10" src="https://aprenderespanol.org/imaxes/arrow-2.gif" width="13"/><img align="absmiddle" alt="" height="17" src="imaxes/flag.gif" width="30"/>  <u>English exercises</u></a> </div>
<div class="sideadd"><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aprender espaÃ±ol indice 336x280 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0114706156691227" data-ad-slot="6931905178" style="display:inline-block;width:336px;height:280px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script> </div>
<div class="sideadd2">
<form action="https://www.thefreedictionary.com/_/partner.aspx" id="dictionary" method="get" style="display:inline;margin:0">
<table cellpadding="1" cellspacing="0" class="box-white" id="dictionarybox" style="border:1px #0036cc solid;width:335px;color:#0036cc" width="335">
<tr>
<td align="right" class="small-text" height="30" width="144">Diccionario:</td>
<td align="center" class="box-white" width="180"><input class="small-text" name="Word" size="15" value=""/></td>
</tr>
<tr id="dictionarybox_source">
<td align="right" class="small-text" height="48">Idioma:</td>
<td class="box-white" id="boxsource_td" style="font-size:8pt;text-align:left"><p class="indice">
<input checked="checked" name="Set" type="radio" value="es"/>
<span class="small-text"><a class="texts" href="https://es.thefreedictionary.com">EspaÃ±ol</a> </span></p>
<p class="indice"> <span class="small-text">
<input name="Set" type="radio" value="www"/>
<a class="texts" href="https://www.thefreedictionary.com">English</a></span><br/>
</p></td>
</tr>
<tr>
<td align="center" colspan="2"><select class="small-text" name="mode">
<option value="w">Palabra</option>
</select>
<input class="small-text" name="submit" type="submit" value="Buscar"/></td>
</tr>
</table>
</form>
</div>
<div id="left-side-3">
<p><a class="sidelink" href="https://dle.rae.es/diccionario"><span class="notas"><img alt="" height="10" src="https://aprenderespanol.org/imaxes/arrow-2.gif" width="13"/></span> Diccionario de la lengua espaÃ±ola</a><a class="sidelink" href="https://translate.google.com/"><span class="notas"><img alt="" height="10" src="https://aprenderespanol.org/imaxes/arrow-2.gif" width="13"/></span> Traductor: palabras, texto, audio</a><a class="sidelink" href="https://www.lenguaje.com/CGI-BIN/che_revisor.exe"><span class="notas"><img alt="" height="10" src="https://aprenderespanol.org/imaxes/arrow-2.gif" width="13"/></span> Corrector ortogrÃ¡fico</a><a class="sidelink" href="https://www.wordreference.com/conj/EsVerbs.aspx?v=ser"><span class="notas"><img alt="" height="10" src="https://aprenderespanol.org/imaxes/arrow-2.gif" width="13"/></span> Conjugador de verbos</a> </p>
</div>
<div class="side-text"></div>
</div>
<div id="top-mobil">
<div class="navigator">
<ul class="nav">
<li class="nav_tab"><a href="https://aprenderespanol.org/gramatica-ejercicios">
<p>GramÃ¡tica - ejercicios</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/verbos-ejercicios">
<p>Verbos -  ejercicios</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/vocabulario-ejercicios">
<p>Vocabulario - ejercicios</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/lecturas-ejercicios">
<p>Lecturas - ejercicios</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/canciones-cuentos">
<p>Cuentos - audiciones</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/videos-ejercicios">
<p>VÃ­deos: cursos, series</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/canciones-cuentos">
<p>Canciones: vÃ­deo y letra</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org/audiciones-podcast">
<p>Audiciones - ejercicios</p>
</a></li>
<li class="nav_tab"><a href="https://aprenderespanol.org">
<p>PÃ¡gina principal</p>
</a></li>
</ul>
</div>
<div class="menu-icon">
<div class="line_one"></div>
<div class="line_two"></div>
<div class="line_three"></div>
</div>
<p class="menu-text">MenÃº</p>
</div>
<div id="principal">
<h1>Ejercicios de espaÃ±ol</h1>
<h2>Ejercicios y recursos ele</h2>
<ul>
<li><a class="text-index" href="https://aprenderespanol.org/gramatica-ejercicios"><u>GramÃ¡tica</u></a>: <span class="sensitive-ads">artÃ­culos, adjetivos, adverbios, pronombres, preposiciones, acentuaciÃ³n, comparaciones, alfabeto, ortografÃ­a ...</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/verbos-ejercicios"><u>Verbos</u></a>:<span class="sensitive-ads"> regulares e irregulares, presente, pasado, futuro, condicional, ser y estar ...</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/vocabulario-ejercicios"><u>Vocabulario</u></a>:<span class="sensitive-ads"> colores, comida, nÃºmeros, actividades diarias, cuerpo humano, dÃ­as, familia, ropa, el tiempo, la hora ...</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/lecturas-ejercicios"><u>Lecturas</u></a><u>:</u> <span class="sensitive-ads">con ejercicios de comprensiÃ³n para todos los niveles: infantil, elemental, intermedio y avanzado.</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/cuentos-fabulas"><u>Cuentos</u></a>: <span class="sensitive-ads">cuentos, fÃ¡bulas y relatos breves para leer o escuchar.</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/audiciones/dictados-audiciones.html"><u>Dictados</u></a>: <span class="sensitive-ads">dictados interactivos y fichas imprimibles.</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/audiciones-podcast"><u>Audiciones</u></a>: <span class="sensitive-ads">con ejercicios de comprensiÃ³n.</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/videos-ejercicios"><u>VÃ­deos</u></a>: <span class="sensitive-ads">cursos en espaÃ±ol, lecciones y series subtituladas.</span></li>
<li><a class="text-index" href="https://aprenderespanol.org/canciones-cuentos"><u>Canciones</u></a>:<span class="sensitive-ads"> canciones infantiles y las mejores canciones pop en espaÃ±ol con vÃ­deo y letra.</span></li>
</ul>
</div>
<div id="right">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aprenderespanol-300x600 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0114706156691227" data-ad-slot="5939981008" style="display:inline-block;width:300px;height:600px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
</div>
<div id="bottom"><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- aprenderespanol-fondo -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0114706156691227" data-ad-format="auto" data-ad-slot="5435323886" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<div id="bottom-mobil"><script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- espanol-mobil-fondo -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0114706156691227" data-ad-slot="7705951973" style="display:inline-block;width:336px;height:280px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
<div id="bottom-2">
<div class="foot6a2"><span class="text-foot">Buscar en este sitio</span> <script async="" src="https://cse.google.com/cse.js?cx=partner-pub-0114706156691227:3uow67c3y1e"></script>
<div class="gcse-searchbox-only"></div>
</div>
</div>
<div id="foot6"><div class="foot6c"><a class="text-foot" href="https://aprenderespanol.org/"> Ãndice</a></div>
<div class="foot6c"> <a class="text-foot" href="https://aprenderespanol.org/mensaje/contacto.php">Contacto</a></div>
<div class="foot6e"><a class="text-foot" href="https://aprenderespanol.org/politica-privacidad.html">Privacidad</a></div>
</div>
</div>
<p><!-- You should make sure that the file cookiechoices.js is available
and accessible from the root directory of your site.  -->
<script src="https://aprenderespanol.org/cookiechoices.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Este sitio utiliza cookies propias y de terceros para ofrecerle una mejor experiencia y servicio.',
      'cerrar mensaje','mÃ¡s informaciÃ³n', 'https://aprenderespanol.org/politica-privacidad.html');
  });
</script>
<!---Non async para que cargue en mobil-->
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="menuindex.js"></script>
<script async="" type="text/javascript">
			<!--

			enlaceBlank();

			//-->
		</script>
</p>
</body>
</html>
