<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- Drupal 6 code: <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="" lang="">
--><html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kings, Queens, and Courtiers: Art in Early Renaissance France | The Art Institute of Chicago</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script src="js/roll.js" type="text/javascript"></script>
<link href="css/roll.css" media="all" rel="stylesheet" type="text/css"/>
<link href="images/aicexhibitionstheme_favicon.png" rel="shortcut icon" type="image/x-icon"/>
<style media="all" type="text/css">@import "css/aicportal.css";</style>
<style media="all" type="text/css">@import "css/node.css";</style>
<style media="all" type="text/css">@import "css/user.css";</style>
<style media="all" type="text/css">@import "css/content.css";</style>
<style media="all" type="text/css">@import "css/gallery.css";</style>
<style media="all" type="text/css">@import "css/colorpicker.css";</style>
<style media="all" type="text/css">@import "css/exhibition-12365.css";</style>
<style media="all" type="text/css">@import "css/fieldgroup.css";</style>
<style media="all" type="text/css">@import "css/devel.css";</style>
<style media="all" type="text/css">@import "css/style.css";</style>
<style media="all" type="text/css">@import "css/reset-fonts-grids.css";</style>
<style media="all" type="text/css">@import "css/system-menus.css";</style>
<style media="all" type="text/css">@import "css/system.css";</style>
<style media="all" type="text/css">@import "css/main.css";</style>
<style media="all" type="text/css">@import "css/collage-managed.css";</style>
<style media="all" type="text/css">@import "css/citi-exhib-front.css";</style>
<style media="all" type="text/css">@import "css/citi-artwork.css";</style>
<style media="all" type="text/css">@import "css/citi-artwork-illustrated-list.css";</style>
<style media="print" type="text/css">@import "css/print.css";</style>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/drupal.js" type="text/javascript"></script>
<script src="js/jquery.compat-1.0.js" type="text/javascript"></script>
<script src="js/extlink.js" type="text/javascript"></script>
<script src="js/devel.js" type="text/javascript"></script>
<script src="js/jqModal.js" type="text/javascript"></script>
<script src="js/jquery.lazyload.js" type="text/javascript"></script>
<script src="js/scripts.js" type="text/javascript"></script>
<script src="js/flv.js" type="text/javascript"></script>
<script type="text/javascript">Drupal.extend({ settings: { "extlink": { "extTarget": "_blank", "extClass": 0, "extSubdomains": 1 } } });</script>
<!--[if IE 6]>
<style type="text/css" media="all">@import "/aic/collections/sites/default/themes/aicexhibitionstheme/css/ie6-styles.css";</style>
<![endif]-->
<style type="text/css">
      #nav a#exhibitnav {
      color: #fff;
      background: #666666;
    }
  </style>
<script type="text/javascript">
  $(document).ready(function(){
    $("img.lazyLoad").lazyload({
      threshold : 200,
      placeholder : "/aic/collections/sites/default/themes/aicexhibitionstheme/images/blank.gif",
    });
  });
 </script>
<!--urchin webstats utm-->
<script src="js/__utm.js" type="text/javascript"></script>
<!--/var/www/drupal-collections/sites/default/themes/aicexhibitionstheme/page-node.tpl.php-->
</head>
<body class="not-front not-logged-in ntype-exhibitionpage sidebar-right">
<div id="header">
<div id="header-region">
<div id="logo-title">
<a href="http://www.artic.edu/aic/" id="logo" rel="home" title="Home">
<img alt="The Art Institute of Chicago" height="15" src="images/KQC_AIC.gif" title="" width="325"/></a>
<div id="name-and-slogan">
<h1 id="site-name"> <a href="http://www.artic.edu/aic/collections/" rel="home" title="Home"><span>The Art Institute of Chicago</span></a> </h1>
</div>
<!-- /name-and-slogan -->
</div>
<!-- /logo-title -->
<div id="exhib-banner"><img alt="Kings, Queens, and Courtiers" height="131" src="images/KQC_hdr.gif" title="" width="735"/></div>
</div><!--/header-region-->
</div>
<!-- /header -->
<div id="pagewrapper">
<div id="page">
<div id="column_container">
<div class="clear-block" id="container">
<!--<div id="sidebar-left" class="column sidebar"> </div>-->
<!-- /sidebar-left -->
<div class="column" id="main">
<div id="main-squeeze">
<div id="navigation">
<div id="breadcrumb">
<div class="breadcrumb"><div class="breadcrumb"><a href="http://www.artic.edu/aic/">Home</a> &gt; <a href="http://www.artic.edu/aic/exhibitions/">Exhibitions</a> &gt; <span class="sel">Kings, Queens, and Courtiers: Art in Early Renaissance France</span></div></div>
</div>
<div id="page-title-wrapper">
<h1 class="title">Kings, Queens, and Courtiers: Art in Early Renaissance France</h1>
</div>
</div>
<!-- /navigation -->
<div id="content">
<div class="clear-block" id="content-content"> <div class="node clear-block" id="node-12366">
<div class="meta">
</div>
<div class="content">
<h3>February 27–May 30, 2011</h3>
<div class="image"><a href="artwork/208045"><img alt="images/KQC_ss.jpg" class="inline" height="300" src="images/KQC_ss.jpg" title="images/KQC_ss.jpg" width="640"/></a></div>
<p>Discover splendid tapestries, painted portraits, altarpieces, stained glass, exquisite illuminated manuscripts, and monumental sculpture made to express the power and prestige of the kings of France and their court. <em>Kings, Queens, and Courtiers: Art in Early Renaissance France</em> highlights a fascinating period in French art in the years around 1500, when late Gothic exuberance encountered the energy and harmony of the Italian Renaissance in an elegant fusion of artistic styles. The Art Institute of Chicago invites you to enter an enchanting world of chivalry, royal symbolism, and the rediscovered glamour of antiquity in this stunning exhibition’s only North American venue.</p>
<p><!--Caption--></p>
<p></p><hr/>
<p><sub><em>Vessel for the Heart of Anne of Brittany, with Its Crown</em> (detail). Probably Loire Valley or Paris, January 9–March 19, 1514. Musée Dobrée, Nantes, D. 886-1-1.</sub></p>
<p> <!--<br />
Start of DoubleClick Spotlight Tag: Please do not remove<br />
Activity name of this tag: Kings, Queens, and Courtiers<br />
URL of the webpage where the tag is expected to be placed: <a href="http://www.artic.edu/aic/collections/exhibitions/Kings-Queens/index" title="http://www.artic.edu/aic/collections/exhibitions/Kings-Queens/index">http://www.artic.edu/aic/collections/exhibitions/Kings-Queens/index</a><br />
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.<br />
Creation Date: 03/07/2011<br />
--></p>
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<img src="http://ad.doubleclick.net/activity;src=2676259;type=aiccy424;cat=kings086;ord=' + a + '?" width="1" height="1" alt=""/>');
</script><p></p><noscript><br/>
<img alt="" height="1" src="http://ad.doubleclick.net/activity;src=2676259;type=aiccy424;cat=kings086;ord=1?" width="1"/><br/>
</noscript><br/>
<!-- End of DoubleClick Spotlight Tag: Please do not remove -->
</div>
</div>
</div>
<!-- /content-content -->
</div>
<!-- /content -->
</div>
<div id="footer-wrapper">
<div id="footer">
<!--Collage footer include-->
<div id="footer_nav">
<ul>
<li class="first footer_link"><a href="http://www.artic.edu/about">About</a> </li>
<li class="footer_link"><a href="http://www.artic.edu/contact-us">Contact</a> </li>
<li class="footer_link"><a href="http://www.artinstituteimages.org">Image Licensing </a> </li>
<li class="footer_link"><a href="http://www.artic.edu/copyright">Terms of Use </a> </li>
<li class="footer_link"><a href="http://www.saic.edu/">SAIC</a> </li>
<li class="footer_link"><a href="http://www.artic.edu/employment">Employment</a> </li>
<li class="footer_link"><a href="http://www.artic.edu/e-news">E-news</a> </li>
</ul>
</div>
<p>© 2013 The Art Institute of Chicago, 111 South Michigan Avenue, Chicago, Illinois, 60603-6404<br/>
<a href="http://www.artic.edu/copyright">Terms and Conditions</a> | All text and images on this site are protected by U.S. and international copyright laws. Unauthorized use is prohibited.</p>
<!--/Collage footer include-->
</div>
<!-- /footer -->
</div>
<!-- /footer-wrapper -->
</div>
<!-- /main-squeeze /main -->
<div class="column sidebar" id="sidebar-right"> <div class="block block-menu_trim" id="block-menu_trim-475">
<div class="blockinner">
<h2 class="title"> Kings, Queens, and Courtiers: Art in Early Renaissance France </h2>
<div class="content">
<ul class="menu">
<li class="leaf first"><a href="overview">Overview</a></li>
<li class="leaf"><a href="http://www.artic.edu/aic/visitor_info/geninfo.html">Visitor Information</a></li>
<li class="collapsed"><a href="themes">Exhibition Themes</a></li>
<li class="leaf"><a href="artwork">Selected Works</a></li>
<li class="collapsed"><a href="Timeline">Additional Resources</a></li>
<li class="leaf"><a href="http://twitter.com/CourtierRobert#">Royal Twitterer</a></li>
<li class="leaf last"><a href="http://www.artic.edu/aic/visitor_info/selfguide/mar11_guide.pdf">Related Self-Guide</a></li>
</ul>
</div>
</div>
</div>
<div id="hoverify-target"></div></div>
<!-- /sidebar-right -->
</div>
<!-- /container --></div>
</div>
<!-- /page -->
</div><!--/pagewrapper--><!--collage managed!-->
</body>
</html>
