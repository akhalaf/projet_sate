<!DOCTYPE html>
<html dir="ltr" lang="sr-yu">
<head>
<meta charset="utf-8"/>
<title>404 - Kategorija nije pronađena</title>
<link href="/templates/system/css/error.css" rel="stylesheet"/>
<!--[if lt IE 9]><script src="/media/jui/js/html5.js"></script><![endif]-->
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - Kategorija nije pronađena</div>
<div id="errorboxbody">
<p><strong>Niste u mogućnosti da vidite ovu stranu zbog:</strong></p>
<ol>
<li><strong>out-of-date bookmark/favourite</strong></li>
<li>Sistem za pretraživanje koji ima <strong>listanje po datumu za ovaj sajt</strong></li>
<li><strong>pogrešna adresa</strong></li>
<li><strong>nemate pristup</strong> ovoj strani</li>
<li>Traženi resurs nije pronađen</li>
<li>Došlo je do greške prilikom obrade vašeg zahteva</li>
</ol>
<p><strong>Molim probajte jednu od sledećih strana:</strong></p>
<ul>
<li><a href="/index.php" title="Povratak na početnu stranu">Početna strana</a></li>
</ul>
<p>Ako i dalje postoji problem, kontaktirajte sistem administratora ovog sajta</p>
<div id="techinfo">
<p>
				Kategorija nije pronađena							</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
