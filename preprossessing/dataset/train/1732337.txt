<!DOCTYPE html>
<html lang="pl" mozdisallowselectionprint="" moznomarginboxes="">
<head>
<title>Zoo Warszawa</title>
<meta charset="utf-8"/>
<meta content="Oficjalna strona internetowa Miejskiego Ogrodu Zoologicznego w Warszawie." name="description"/>
<link href="/css/all_gzip.css?v=83" rel="stylesheet" type="text/css"/>
<script src="/js/all_gzip.js?v=83"></script>
<meta content="default-src 'none' ;
font-src 'self' https://fonts.googleapis.com
https://fonts.gstatic.com ;
media-src 'self';
img-src 'self' 'unsafe-inline' https://www.googletagmanager.com https://www.google-analytics.com https://ssl.google-analytics.com;
style-src 'self' 'unsafe-inline'
https://fonts.googleapis.com https://www.google-analytics.com;
script-src 'self' 'unsafe-eval' 'unsafe-inline' https://www.googletagmanager.com https://www.google-analytics.com;
connect-src 'self' https://www.google-analytics.com;
child-src 'none' ;
object-src 'none' ;" http-equiv="Content-Security-Policy"/>
<link href="/grafika/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/grafika/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700&amp;subset=latin-ext" rel="stylesheet"/>
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<meta content="width=device-width" id="viewport" name="viewport"/>
<meta content="all" name="robots"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=GA_TRACKING_ID"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-125551081-1');
</script>
</head>
<body class="body page11111 " data-next="&lt;span&gt;Następny&lt;/span&gt;" data-prev="&lt;span&gt;Poprzedni&lt;/span&gt;">
<div class="search-box">
<form action="" method="GET">
<div class="search-title">Czego szukasz ?</div>
<div class="input">
<input name="q" placeholder="Wyszukaj ..." title="Wpisz szukaną frazę" type="text" value=""/>
<button type="Wyszukaj">Szukaj</button>
</div>
</form>
<div class="i-close">×</div>
</div>
<div class="wrapper">
<header class="has-slider">
<div class="info">
<div class="container">
<div class="hours">
ZOO jest czynne w codziennie, w każdą niedzielę i święta od godz. 9.00.
</div>
<div class="socials">
<a href="https://www.facebook.com/WarszawskieZOO/" rel="nofollow" target="_blank" title="Facebook W nowym oknie">
<img alt="Facebook" src="/grafika/i-facebook.png"/>
</a>
<a href="https://www.youtube.com/channel/UCnQxP9rBJU7voXNKCYEOLww" rel="nofollow" target="_blank" title="Youtube W nowym oknie">
<img alt="Youtube" src="/grafika/i-youtube.png"/>
</a>
<a href="https://www.instagram.com/zoo_warszawa/?hl=pl" rel="nofollow" target="_blank" title="Instagram W nowym oknie">
<img alt="Instagram" src="/grafika/i-instagram.png"/>
</a>
<a href="https://pl.tripadvisor.com/Attraction_Review-g274856-d588540-Reviews-Warszawskie_Zoo-Warsaw_Mazovia_Province_Central_Poland.html" rel="nofollow" target="_blank" title="TripAdvisor W nowym oknie">
<img alt="TripAdvisor" src="/grafika/i-tripadvisor.png"/>
</a>
</div>
</div>
</div>
<div class="heading">
<div class="container">
<div class="logo logo-is-zoo">
<a href="/">
<img alt="Zoo Warszawa" src="/grafika/logo.png"/>
</a>
</div>
<div class="welcome">Witamy w Warszawskim<br/>
Ogrodzie zoologicznym</div>
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Pokaż menu</span>
<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
</button>
<div class="links">
<div>
<a class="bip" href="/bip" title="Biuletyn Informacji Publicznej">
<img alt="Biuletyn Informacji Publicznej" src="/grafika/bip-full.svg"/>
</a>
<a class="irbis" href="/edukacja/konkursy/irbis-warszawski-konkurs-zoologiczny,p49325755" title="Irbis">
<img alt="Irbis" src="/grafika/irbis.png"/>
</a>
<a class="zoo" href="/" title="ZOO Warszawa">ZOO Warszawa</a>
</div>
</div>
<div class="i-search"></div>
<div class="font-languages">
<div class="font">
<span class="a">A</span>
<span class="aa">A <sup>+</sup></span>
<span id="contrast-change">A</span>
</div>
<div class="languages">
<div class="lang-title">Wybierz język</div>
<div class="chose-lang">
<a href="/" title="Język Polski">
<img alt="Flaga polska" src="/grafika/flag-pl.png"/>
</a>
<a href="/en" title="Język angielski">
<img alt="Flaga Wielkiej Brytanii" src="/grafika/flag-en.png"/>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="menu">
<div class="container">
<div class="collapse navbar-collapse" role="navigation">
<ul class="nav navbar-nav nav-header"><li class="mdropdown"><a href="/o-nas">O nas</a>
<ul class="dropdown-menu"><li><a href="/o-nas/misja-zoo">Misja ZOO</a></li><li><a href="/o-nas/historia-zoo">Historia ZOO</a></li><li><a href="/o-nas/gatunki-zwierzat">Gatunki zwierząt</a></li><li><a href="/o-nas/zoomaniacy-w-pracy">ZOOmaniacy w pracy</a></li><li><a href="/bip/rekrutacja">Rekrutacja</a></li><li><a href="/o-nas/wolontariat">Wolontariat</a></li><li><a href="/o-nas/praktyki">Praktyki</a></li><li><a href="/o-nas/jak-pomoc-zwierzetom">Jak pomóc zwierzętom?</a></li></ul></li><li class="mdropdown"><a href="/wizyta-w-zoo">Wizyta w Zoo</a>
<ul class="dropdown-menu"><li><a href="/wizyta-w-zoo/bilety">Bilety</a></li><li><a href="https://www.kupbilet.pl/index.php?url=sport/rekreacja/re0024">Kup bilet on-line</a></li><li><a href="/wizyta-w-zoo/godziny-otwarcia">Godziny otwarcia</a></li><li><a href="/wizyta-w-zoo/dojazdy">Dojazdy</a></li><li><a href="/wizyta-w-zoo/parkingi">Parkingi</a></li><li><a href="/wizyta-w-zoo/mieszkancy-zoo">Mieszkańcy ZOO</a></li><li><a href="/wizyta-w-zoo/godziny-karmienia">Godziny karmienia</a></li><li><a href="/aktualnosci/najblizsze-wydarzenia/weekendowe-spacery-z-wolontariuszami,p1217632819">Spacery</a></li><li><a href="/wizyta-w-zoo/inne-atrakcje">Inne atrakcje</a></li><li><a href="/wizyta-w-zoo/gastronomia">Gastronomia</a></li><li><a href="/wizyta-w-zoo/mapa-zoo">Mapa ZOO</a></li><li><a href="/wizyta-w-zoo/1325923089">Mapa ZOO - Zimowa</a></li><li><a href="/wizyta-w-zoo/wozek-inwalidzki-uzyczenie">Wózek inwalidzki – użyczenie </a></li><li><a href="/wizyta-w-zoo/sciezka-edukacyjna">Ścieżka edukacyjna</a></li><li><a href="/wizyta-w-zoo/przed-wizyta-w-zoo-przeczytaj">Przed wizytą w ZOO przeczytaj</a></li><li><a href="/wizyta-w-zoo/regulamin">Regulamin</a></li><li><a href="/wizyta-w-zoo/po-wizycie">Po wizycie</a></li></ul></li><li class="mdropdown"><a href="/willa-zabinskich">Willa Żabińskich</a>
<ul class="dropdown-menu"><li><a href="/willa-zabinskich/historia">Historia</a></li><li><a href="/willa-zabinskich/zwiedzanie">Zwiedzanie</a></li><li><a href="http://zoo.waw.pl/willa/wirtualna-wycieczka.html" target="_blank" title="Wirtualna wycieczka W nowym oknie">Wirtualna wycieczka</a></li></ul></li><li class="mdropdown"><a href="/edukacja">Edukacja</a>
<ul class="dropdown-menu"><li><a href="/edukacja/oferta-edukacyjna">Oferta edukacyjna</a></li><li><a href="/edukacja/edu-prezentacje">Edu-Prezentacje</a></li><li><a href="/edukacja/kto-uczy-w-zoo">Kto uczy w ZOO?</a></li><li><a href="/edukacja/zapisz-sie-na-zajecia">Zapisz się na zajęcia</a></li><li><a href="/edukacja/na-ratunek-przyrodzie">Na ratunek przyrodzie</a></li><li><a href="/edukacja/ciekawostki-i-artykuly-popularnonaukowe">Ciekawostki i artykuły popularnonaukowe</a></li><li><a href="/edukacja/nauka">Nauka</a></li><li><a href="/edukacja/konkursy">Konkursy</a></li><li><a href="/edukacja/konkursy/irbis-warszawski-konkurs-zoologiczny,p49325755">Konkurs Zoologiczny IRBIS</a></li><li><a href="/edukacja/edukacja-dla-najmlodszych">Edukacja dla najmłodszych</a></li><li><a href="/edukacja/quizydm">Quizy dla najmłodszych</a></li><li><a href="/edukacja/zostaw-nie-dokarmiaj">ZOOstaw, nie dokarmiaj</a></li><li><a href="/edukacja/akademia-mlodego-ornitologa">Akademia Młodego Ornitologa</a></li></ul></li><li class=""><a href="/rola-ogrodow-zoologicznych">Rola ogrodów zoologicznych</a></li><li class=""><a href="/dla-mediow">Dla mediów</a></li><li class="mdropdown"><a href="/aktualnosci">Aktualności</a>
<ul class="dropdown-menu"><li><a href="/aktualnosci/najblizsze-wydarzenia">Wydarzenia</a></li><li><a href="/bip/zamowienia-publiczne">Zamówienia publiczne</a></li><li><a href="/aktualnosci/archiwalne">Archiwalne</a></li></ul></li><li class=""><a href="/kontakt">Kontakt</a></li><li class=""><a href="/dzierzawa-najem-sprzedaz">Dzierżawa, najem, sprzedaż</a></li><li class=""><a href="https://panda.zoo.waw.pl/">Fundacja PANDA</a></li><li class=""><a href="/blog">BLOG</a></li><li class=""><a href="/kalendarz">KALENDARZ</a></li></ul>
</div>
</div>
</div>
</header>
<div class="container">
<div class="owl-carousel" id="carousel">
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="/aktualnosci/najblizsze-wydarzenia/maseczki-obowiazkowe-w-calym-zoo,p780027977">
<img alt="nowe obostrzenia" src="/cache/files/11111/corono1---wo-1200-ho-550.png"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<img alt="Pozory mylą" src="/cache/files/11111/banner-1200-x-550---wo-1200-ho-550.png"/>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="/edukacja/zostaw-nie-dokarmiaj ">
<img alt="Buba" src="/cache/files/11111/1200x550-px-zoo-03---wo-1200-ho-550.jpg"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="/edukacja/zostaw-nie-dokarmiaj ">
<img alt="Pogodka" src="/cache/files/11111/5521200x550-px-zoo-04---wo-1200-ho-550.jpg"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<img alt="zostaw nie dokarmiaj kapucynka" src="/cache/files/11111/1200x550-px-zoo-02---wo-1200-ho-550.jpg"/>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="https://www.facebook.com/events/2054048261571024/">
<img alt="zostaw nie dokarmiaj" src="/cache/files/11111/1200x550-px-zoo-01---wo-1200-ho-550.jpg"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<div class="item-image-description">
<div class="background">
<img alt="Hipopotam" src="/cache/files/11111/bghipopotam---wo-1200-ho-550.png"/>
<div class="image">
<img alt="Hover" class="fade-image" src="/cache/files/11111/hipopotam2_1---w-2000-h-550.png"/>
<img alt="Zwierzak" src="/cache/files/11111/hipopotam_1---w-2000-h-550.png"/>
</div>
</div>
<div class="description">
<div>
<div class="title">
Hipopotam
</div>
<div class="description-body">
Skóra hipopotama ma około 4 cm grubości i stanowi 25% masy ciała zwierzęcia. Mogą ważyć nawet 3 tony! <br/> <br/>
Wydziela substancję stanowiącą filtr przeciwsłoneczny, chroniący zwierzę przed szkodliwymi promieniami UV
<br/> <br/>
Mogą być bardzo niebezpieczne dla ludzi z uwagi na terytorializm i porywczy temperament potrafią atakować intruzów
</div>
</div>
</div>
</div>
<!-- Samo zdjęcie -->
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="http://ncf-india.org/projects/hornbill-nest-adoption-program">
<img alt="dzioborożec" src="/cache/files/11111/banerdzioborozec---wo-1200-ho-550.jpg"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<div class="item-image-description">
<div class="background">
<img alt="Ara hiacyntowa" src="/cache/files/11111/bgara---wo-1200-ho-550.png"/>
<div class="image">
<img alt="Hover" class="fade-image" src="/cache/files/11111/ara2---w-2000-h-550.png"/>
<img alt="Zwierzak" src="/cache/files/11111/ara---w-2000-h-550.png"/>
</div>
</div>
<div class="description">
<div>
<div class="title">
Ara hiacyntowa
</div>
<div class="description-body">
Masywny dziób działa jak dziadek do orzechów podczas rozłupywania twardych łupin i skorup orzechów <br/><br/>
Największa z latających papug, której rozpiętość skrzydeł osiąga ponad 120 cm! <br/><br/>
W odróżnieniu od większości ptaków papugi potrafią ruszać zarówno dolną jak i górną połówką dzioba
</div>
</div>
</div>
</div>
<!-- Samo zdjęcie -->
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<div class="item-image-description">
<div class="background">
<img alt="Niedźwiedź polarny " src="/cache/files/11111/bgniedzwiedz---wo-1200-ho-550.png"/>
<div class="image">
<img alt="Hover" class="fade-image" src="/cache/files/11111/niedzwiedz2_2---w-2000-h-550.png"/>
<img alt="Zwierzak" src="/cache/files/11111/niedzwiedz_2---w-2000-h-550.png"/>
</div>
</div>
<div class="description">
<div>
<div class="title">
Niedźwiedź polarny
</div>
<div class="description-body">
Nasze niedźwiedzie polarne to Aleut i Gregor - bracia bliźniacy <br/><br/>
Jesteśmy jedynym ogrodem zoologicznym w Polsce,
który ma ma ten gatunek <br/><br/>
Futro niedźwiedzi nie jest białe tylko niemal przezroczyste, tak by łatwiej przenikały przez nie promienie słońca
</div>
</div>
</div>
</div>
<!-- Samo zdjęcie -->
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<div class="item-image-description">
<div class="background">
<img alt="Leniwiec" src="/cache/files/11111/bgleniwiec---wo-1200-ho-550.png"/>
<div class="image">
<img alt="Hover" class="fade-image" src="/cache/files/11111/leniwiec2_1---w-2000-h-550.png"/>
<img alt="Zwierzak" src="/cache/files/11111/leniwiec_1---w-2000-h-550.png"/>
</div>
</div>
<div class="description">
<div>
<div class="title">
Leniwiec
</div>
<div class="description-body">
Nasz leniwiec Pavoo mieszka razem ze swą o kilka lat starszą towarzyszką Amy w jednej z wolier w budynku ptaszarni <br/><br/>
Zazwyczaj wisi „do góry nogami” wysoko nad ziemią wygrzewając się pod wydzielającymi ciepło promiennikami <br/><br/>
Leniwce mają w swym futrze całą masę pasażerów na gapę jak kilka gatunków chrząszczy i ciem oraz glony, które nadają jego sierści zielonkawy kolor
</div>
</div>
</div>
</div>
<!-- Samo zdjęcie -->
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<div class="item-image-description">
<div class="background">
<img alt="Słoń" src="/cache/files/11111/background_6---wo-1200-ho-550.jpg"/>
<div class="image">
<img alt="Hover" class="fade-image" src="/cache/files/11111/slon2_1---w-2000-h-550.png"/>
<img alt="Zwierzak" src="/cache/files/11111/484slon---w-2000-h-550.png"/>
</div>
</div>
<div class="description">
<div>
<div class="title">
Słoń
</div>
<div class="description-body">
Słonie afrykańskie są największymi lądowymi zwierzętami na świecie. Samiec może ważyć nawet 6 ton! <br/><br/>
Wielkie uszy służą do chłodzenia dzięki licznym przebiegającym w tym miejscu naczyniom krwionośnym <br/><br/>
Pofałdowana skóra zwiększa powierzchnię chłodzenia organizmu, dzięki czemu słoniom nie straszne afrykańskie upały
</div>
</div>
</div>
</div>
<!-- Samo zdjęcie -->
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="/wizyta-w-zoo/godziny-karmienia">
<img alt="Godziny karmienia pokazowego" src="/cache/files/11111/bkarmienie---wo-1200-ho-550.png"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<img alt="Poznaj i pokoloruj mieszkańców ZOO" src="/cache/files/11111/www.zoo-baner-dla-wz_1---wo-1200-ho-550.jpg"/>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
<div class="item">
<!-- Zdjęcia z punktami -->
<!-- Zdjęcie i opis -->
<!-- Samo zdjęcie -->
<div class="item-image">
<a href="https://www.mpay.pl/uslugi/bilety-do-warszawskiego-zoo/">
<img alt="Bilety do Warszawskiego ZOO" src="/cache/files/11111/baner-glowny-zoo1200x550---wo-1200-ho-550.png"/>
</a>
</div>
<!-- Animacja zdjęcia -->
<!-- Animacja description + tytuł + zdjęcie -->
</div>
</div>
<div class="content">
<div class="row">
<div class="col-sm-12" id="cmsContent">
<div class="section"><div class="row"><div class="col-xxs-12 col-xs-12 col-sm-12"><div class="box_info div-link" style="background: url('/cache/files/11111/background---w-1200.jpg') no-repeat top center;">
<div class="description">
<div class="description-body">
<h2 class="title">
<a href="/rola-ogrodow-zoologicznych">
Rola ogrodu zoologicznego
</a>
</h2>
<p>Miejski Ogród Zoologiczny w Warszawie to wielka, zielona wyspa w środku miasta, miejsce magiczne, pełne ciekawych przygód. Tu można przyjść po wiedzę, wypocząć pośród pięknej przyrody i obserwować wspaniałe zwierzęta. Mamy ponad 12 000 zwierząt reprezentujących około 500 gatunków z czego blisko 50 jest objętych programem ochrony EEP (Europejskim Programem Ochrony Zwierząt). Misją Ogrodu jest zintegrowana ochrona przyrody dążąca do zapewnienia trwałości gatunków. Realizację naszej misji opieramy na 5 filarach: Hodowli, Edukacji, Nauce, Rehabilitacji i Rekreacji. Wizyta u nas to wspaniała atrakcja dla dzieci i dorosłych, dlatego zapraszam wszystkich do ciekawego spędzenia czasu na łonie przyrody i w towarzystwie zwierzaków z naszego ZOO.</p> <p><em>dr Andrzej Grzegorz Kruszewicz - dyrektor Zoo</em></p>
</div>
<div class="image">
<img alt="Rola ogrodu zoologicznego" class="a-link" src="/cache/files/11111/dr---wo-230-ho-230.jpg"/>
</div>
</div>
</div>
</div></div></div>
<div class="section"><div class="row"><div class="col-xxs-12 col-xs-12 col-sm-4"><div class="box_offer div-link" style="background: #ffa042;">
<div class="description">
<a href="https://www.kupbilet.pl/index.php?url=sport/rekreacja/re0024" title="Kup bilet on-line">
<img alt="Kup bilet&lt;br&gt; on-line" class="a-link" src="/cache/files/11111/i1---w-150.png"/>
</a>
<h3 class="a-link">Kup bilet<br/> on-line</h3>
<span class="more" style="background: #ffa042;">
</span>
</div>
<div class="image">
<img alt="Kup bilet&lt;br&gt; on-line" class="a-link" src="/cache/files/11111/b1---wo-150-ho-285.jpg"/>
</div>
</div>
</div><div class="col-xxs-12 col-xs-12 col-sm-4"><div class="box_offer div-link" style="background: #8dbd6e;">
<div class="description">
<a href="/wizyta-w-zoo/bilety" title="Sprawdź cennik">
<img alt="Sprawdź cennik" class="a-link" src="/cache/files/11111/i2---w-150.png"/>
</a>
<h3 class="a-link">Sprawdź cennik</h3>
<span class="more" style="background: #8dbd6e;">
</span>
</div>
<div class="image">
<img alt="Sprawdź cennik" class="a-link" src="/cache/files/11111/b2---wo-150-ho-285.jpg"/>
</div>
</div>
</div><div class="col-xxs-12 col-xs-12 col-sm-4"><div class="box_offer div-link" style="background: #9a9a9a;">
<div class="description">
<a href="/wizyta-w-zoo/godziny-otwarcia" title="Godziny otwarcia">
<img alt="Godziny otwarcia" class="a-link" src="/cache/files/11111/i3---w-150.png"/>
</a>
<h3 class="a-link">Godziny otwarcia</h3>
<span class="more" style="background: #9a9a9a;">
</span>
</div>
<div class="image">
<img alt="Godziny otwarcia" class="a-link" src="/cache/files/11111/b3---wo-150-ho-285.jpg"/>
</div>
</div>
</div></div></div>
<div class="section"><div class="row"><div class="col-xxs-12 col-xs-12 col-sm-12">
<div class="ukl ukl_lt">
<div class="ukl-body">
<div></div>
</div>
</div>
</div></div></div>
<div class="row">
</div>
</div>
</div>
</div>
<div class="owl-carousel" id="slider-animals">
<div class="item div-link">
<div class="image">
<img alt="" src="/cache/files/11111/surokatki5_1---wo-600-ho-400.jpg"/>
<div class="loop-image">
<img alt="" src="/cache/files/11111/9112---wo-235-ho-235.jpg"/>
</div>
</div>
<div class="description">
<strong class="nagl">
<a href="/pl/wizyta-w-zoo/mieszkancy-zoo/rodzina-surykatek,p196989171" title="Poznaj bliżej zwierzaki&lt;br&gt; w naszym ZOO">Poznaj bliżej zwierzaki<br/> w naszym ZOO</a>
</strong>
<div class="description-body">
RODZINA SURYKATEK<br/>
Surykatki mieszkają w tym samym obiekcie co żyrafy i antylopy bongo, czyli zupełnie jak w naturze mają kontakt ze swymi afrykańskimi sąsiadami.
</div>
<span class="a-link more">Poznajmy się</span>
</div>
</div>
<div class="item div-link">
<div class="image">
<img alt="" src="/cache/files/11111/hugo---wo-600-ho-400.jpg"/>
<div class="loop-image">
<img alt="" src="/cache/files/11111/347hugo_1---wo-235-ho-235.jpg"/>
</div>
</div>
<div class="description">
<strong class="nagl">
<a href="/wizyta-w-zoo/mieszkancy-zoo" title="Poznaj bliżej zwierzaki&lt;br&gt; w naszym ZOO">Poznaj bliżej zwierzaki<br/> w naszym ZOO</a>
</strong>
<div class="description-body">
HIPOPOTAM NILOWY Hugo<br/>
Niezwykle okazały i sympatyczny blisko 10 letni hipopotam. W ciepłe dni można go oglądać, jak pływa w przestronnym basenie zewnętrznym.
</div>
<span class="a-link more">Poznajmy się</span>
</div>
</div>
<div class="item div-link">
<div class="image">
<img alt="" src="/cache/files/11111/5---wo-600-ho-400.jpg"/>
<div class="loop-image">
<img alt="" src="/cache/files/11111/nosorozec---wo-235-ho-235.jpg"/>
</div>
</div>
<div class="description">
<strong class="nagl">
<a href="/pl/wizyta-w-zoo/mieszkancy-zoo/shikara-i-maly-nosorozec,p397415851" title="Poznaj bliżej zwierzaki&lt;br&gt; w naszym ZOO">Poznaj bliżej zwierzaki<br/> w naszym ZOO</a>
</strong>
<div class="description-body">
NOSOROŻCE PANCERNE<br/>
Mały nosorożec przyszedł na świat 16 marca 2018 r. Ciąża jego mamy Shikari trwała 472 dni, cztery godziny po porodzie maluch zjadł swój pierwszy posiłek: przyssał się do mamy na całe sześć minut.
</div>
<span class="a-link more">Poznajmy się</span>
</div>
</div>
</div>
</div>
<!-- FOOTER -->
<div class="footer">
<div class="animal-left" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-duration="4000"></div>
<div class="animal-right" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-duration="2000"></div>
<div class="container">
<div class="columns">
<div class="shortcuts">
<div class="title-h3">Na skróty</div>
<ul>
<li><a href="/kontakt">Kontakt</a></li>
<li><a href="/wizyta-w-zoo/bilety">Bilety do ZOO</a></li>
<li><a href="/wizyta-w-zoo/godziny-otwarcia">Godz. otwarcia</a></li>
<li><a href="/wizyta-w-zoo/przed-wizyta-w-zoo-przeczytaj">Regulamin ZOO</a></li>
<li><a href="/wizyta-w-zoo/dojazdy">Dojazdy</a></li>
<li><a href="/wizyta-w-zoo/parkingi">Parkingi</a></li>
<li><a href="/wizyta-w-zoo/mapa-zoo">Główne atrakcje </a></li>
</ul>
</div>
<div class="public-life">
<div class="title-h3">Życie publiczne</div>
<ul>
<li><a href="/dla-mediow">Dla mediów</a></li>
<li><a href="/bip/zamowienia-publiczne/do-30000-euro">Zamówienia publiczne</a></li>
<li><a href="/dzierzawa-najem-sprzedaz">Dzierżawa, najem, sprzedaż</a></li>
<li><a href="/bip">Biuletyn informacji publicznej</a></li>
<li><a href="/adopcja-zwierzat">Adopcja zwierząt</a></li>
<li><a href="/klub-milosnikow-zoo">Klub miłośników ZOO</a></li>
<li><a href="/partnerzy">Partnerzy</a></li>
</ul>
</div>
<div class="contact">
<div class="title-h3">Dane adresowe</div>
<p>ul. Ratuszowa 1/3<br/>
03-461 Warszawa, Polska<br/>
tel.: <a href="tel:+48226194041">+48 (22) 619 40 41</a><br/>
faks: <a href="tel:+48226195898">+48 (22) 619 58 98</a><br/>
e-mail: <a class="mailer">zoo|zoo.waw.pl| |zoo|zoo.waw.pl</a></p>
<p>ochrona: <a href="tel:+48226194041">+48 (22) 619 40 41</a></p>
</div>
<div class="client-service">
<div class="title-h3">Punkt Obsługi Klienta</div>
<p>ul. Ratuszowa 1/3 <br/>
tel.  <a href="tel:+48226194041">+48 (22) 619 40 41</a> w. 6<br/>
e-mail: <a class="mailer">pok|zoo.waw.pl | |pok|zoo.waw.pl </a></p>
<p>W punkcie wyrobisz kartę roczną, zakupisz <br/>
jednorazowe bilety wstępu, otrzymasz fakturę,  <br/>
dostaniesz ogólne informacje o Ogrodzie</p>
</div>
</div>
</div>
<div class="container">
<div class="nav-bottom">
<div class="socials-bottom">
<a class="facebook" href="https://www.facebook.com/WarszawskieZOO/" rel="nofollow" target="_blank" title="Facebook W nowym oknie">
<img alt="Facebook" src="/grafika/i-footer-facebook.png"/>
</a>
<a class="youtube" href="https://www.youtube.com/channel/UCnQxP9rBJU7voXNKCYEOLww" rel="nofollow" target="_blank" title="Youtube W nowym oknie">
<img alt="Youtube" src="/grafika/i-footer-youtube.png"/>
</a>
<a class="instagram" href="https://www.instagram.com/zoo_warszawa/?hl=pl" rel="nofollow" target="_blank" title="Instagram W nowym oknie">
<img alt="Instagram" src="/grafika/i-footer-instagram.png"/>
</a>
<a class="tripadvisor" href="https://pl.tripadvisor.com/Attraction_Review-g274856-d588540-Reviews-Warszawskie_Zoo-Warsaw_Mazovia_Province_Central_Poland.html" rel="nofollow" target="_blank" title="TripAdvisor W nowym oknie">
<img alt="Youtube" src="/grafika/i-footer-tripadvisor.png"/>
</a>
</div>
<a href="/o-nas">O nas</a>
<a href="/wizyta-w-zoo">Wizyta w ZOO</a>
<a href="/willa-zabinskich">Willa Żabińskich </a>
<a href="/edukacja">Edukcja</a>
<a href="/dla-mediow">Dla mediów</a>
<a href="/aktualnosci">Aktualności</a>
<a href="/kontakt">Kontakt</a>
<a href="/informacje-rodo">Informacje RODO</a>
<a href="/57071458"></a>
<a href="/files/958233671/file/Deklaracja.html">Deklaracja dostępności</a>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="copyright copyright-1111">
© 2018 Miejski Ogród Zoologiczny w Warszawie | Wszelkie prawa zastrzeżone<br/>
Projekt &amp; <a href="http://www.dms-cms.pl">cms</a>: <a href="https://www.zstudio.pl" title="Projektowanie stron WWW Warszawa">www.zstudio.pl</a>
</div>
<a class="warszawa-link" href="http://www.um.warszawa.pl/" target="_blank" title="Miasto Stołeczne Warszawa W nowym oknie">
<img alt="Miasto Stołeczne Warszawa" src="/grafika/logo-warszawa.png"/>
</a>
</div>
</footer>
</div>
</body>
</html>
