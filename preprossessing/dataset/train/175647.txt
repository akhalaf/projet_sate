<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Web3 -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="qRaZkHvZbakAwsSAN0lZ1jI57VCH2O_J3PtvPTT7i98" name="google-site-verification"/>
<title>SoftCity – PC Utility Software for Consumers and Businesses</title>
<meta content="software,avanquest,utility software,anti-virus software,pc protection,pc maintenance,speed up pc,computer games,kids software,fun learning,driving test software,greeting card software,cad software,turbocad,interior design software,pdf conversion,pc..." name="keywords"/>
<meta content="Software to help Small Businesses grow, utility software to speed up and optimise your PC, Creativity software to express yourself, Learning software to help you develop, Computer Games, Kids fun..." name="description"/>
<meta content="home" name="aq-type"/>
<link href="/template/new/css/global.css" rel="stylesheet" type="text/css"/>
<meta content="0A975B32C484072C212B2E7D8CF3719E" name="msvalidate.01"/>
<!--[if lt IE 8]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<link rel="stylesheet" href="/template/new/css/ie.css" type="text/css" />
<![endif]-->
<script src="/template/new/js/jquery-3.4.1.min.js"></script>
<link href="/template/new/images/logos/sc-favicon.ico" rel="shortcut icon"/>
<script type="text/javascript">
var dataLayer = window.dataLayer || [];
dataLayer.push({'page': {
'environment': 'production',
'type': 'homepage',
'culture':'en'
}});

(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NMQXB9D');</script>
<link href="/template/new/css/responsive.css" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="iDL2uE1dxS2IMfg8tZ8Zg29cIj026DMbxvFCREYKNec" name="google-site-verification"/>
</head>
<body class=" site-id-75 home-page ">
<div id="header">
<div class="container header-top">
<a class="logo" href="/"><img alt="SoftCity" class="logo-img" height="64" src="/template/new/images/logos/softcity.png" title="SoftCity" width="190"/></a>
<div class="header-top-right">
<div class="currency_drop">
<form action="/?tr3=aqredir" id="curFrm" method="get" name="curFrm">
<label>Currency:</label>
<select id="forceCurrency" name="forceCurrency" onchange="document.curFrm.submit();">
<option value="USD">USD</option>
<option value="EUR">EUR</option>
<option value="GBP">GBP</option>
</select>
</form>
</div> <div class="search-forrm" id="new_search_wrapper" rel="75">
<script>
		var searchText;
		var searchUrl = '/search.html';
		  switch ('75') {
			case '51':
				//UK
				searchText = "Search for"
			break;
			case '49':
				//USA
				searchText = "Search for"
			break;
			case '50':
				//FRANCE
				searchText = "Rechercher"
			break;
			case '53':
				//SPAIN
				searchText = "Buscar"
			break;
			case '54':
				//ITALLY
				searchText = "Cerca"
			break;
			case '52':
				//Germany
				searchText = "Suche nach"
			break;
			default:
				searchText = "Search for"
			break;
		  }
		$(document).ready(function(){
		  $('#inputString').attr('placeholder', searchText);
		});
	</script>
<style>
/* BASIC RESET */
a img { border:0 }

a span img { border:0 }

/* COMMON CLASSES */
.break { clear:both; }

/* SEARCH FORM */
#searchform { position:absolute; top:0px; right:47px; z-index:90; }
#searchform input {
	border:0 none;
	float:right;
	height:19px;
	margin-left:2px;
	padding:0;
	position:relative;
	width:275px;
}
#suggestions{display:none; left:0; position:absolute; top:20px; width:327px; padding-bottom:7px; background:url(//cdn1.avanquest.com/template/auto_comp/images/auto_comp_bg.png) no-repeat left bottom;
}
/* SEARCHRESULTS */
#searchresults { border-top:1px solid #838181; margin-left:2px; width:320px; background-color:#fff; font-size:12px; line-height:14px; margin-top:0px;}
#searchresults a { 
	background-color:#FFFFFF;
	border-bottom:1px solid #EFF2F3;
	clear:left;
	display:block;
	padding:6px 0;
	text-decoration:none; 
	}
#searchresults a.text_search { display:block; background-color:#fff; clear:left; height:30px; text-decoration:none; }
#searchresults a:hover { background-color:#eff3f7; color:#006EA9; }
#searchresults a span img { float:left; margin-left:10px; margin-top:5px; width:50px;}
#searchresults span.ac_img { float:left; height:60px; width:70px;}
#searchresults a span.searchheading { display:block; padding-top:0px; color:#555; margin-left:10px;}
#searchresults a.text_search span.searchheading { display:block; font-weight:bold; margin-left:20px; padding-top:0px;line-height:30px; height:30px; color:#555; text-decoration:underline; }
#searchresults a:hover span.searchheading { color:#006EA9; }
#searchresults a span { color:#555555; }
#searchresults a:hover span { color:#006EA9; }
#searchresults span.category { font-size:12px; display:block; color:#555; background:#eaebeb; padding:5px 0px;}
#searchresults span.category .category_inner { margin-left:10px;}
#searchresults span.seperator { float:right; padding-right:15px; margin-right:5px;background-image:url(//cdn3.avanquest.com/template/images/shortcuts_arrow.gif); background-repeat:no-repeat; background-position:right; }
#searchresults span.seperator a { background-color:transparent; display:block; margin:5px; height:auto; color:#ffffff; }
.itemhover {  background-color:#eff3f7 !important; color:#006EA9 !important;  }
.itemhover span {  background-color:#eff3f7 !important; color:#006EA9 !important;  }
div.search_forrm_btn{
position:absolute;
right:-40px;
top:0px;
}
.breadcrunbs_links{
	color:#4A5054;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-style:italic;
	font-weight:bold;
	height:27px;
	line-height:28px;
	margin-left:10px;
	text-decoration:none;
	text-transform:none;
}
</style>
<form action="/search.html" id="searchform" method="get" name="searchform">
<script>
					function gsc(item) {if (item=="Search"){document.searchform.search.value='';}}
				</script>
<input alt="Search" autocomplete="off" id="inputString" name="gsearch" onfocus="gsc(this.value)" size="30" type="text" value=""/>
<div id="suggestions">
<div id="searchresults">
</div>
</div>
<div class="search_forrm_btn">
<a href="javascript://" name="&amp;lid=bt_go&amp;lpos=Search_Bar" onclick="document.searchform.submit();"> </a>
</div>
</form></div> <div class="header-top-links">
<a class="top-link" href="https://avanquest.zendesk.com/hc/en-us"><span class="icon help"></span> Support</a>
<a class="top-link " href="https://shop.avanquest.com/members/?company_rs=75"><span class="icon account"></span> Your Account</a>
<a class="top-link top-link-cart" href="/checkout.html?"><span class="icon cart"></span> Your Basket </a>
</div>
</div>
<div class="menu-btns">
<div class="fa search-btn"><span></span></div>
<a class="fa menu-btn" href="javascript:void(0)"><span></span></a>
</div>
</div>
<div class="mob-search"></div>
<div class="container header-btm">
<nav>
<div id="menu">
<div align="center" id="globalheader">
<!--googleoff: all-->
<ul id="globalnav_home">
<li id="gn-home"><a href="/International" style="background-position: 0 -32px;">Avanquest International</a></li>
</ul>
<ul id="globalnav">
<li class="menu_tab_item menu_has_children pcutilities menu-tab-item" id="menuItem1" rel="1"><a href="/pc-utilities">PC Utilities</a>
<ul class="sub-menu"> <li class="sub-menu-item menu-item-has-children">
<a href="/pc-utilities/security">Security<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/pc-utilities/security/anti-virus-anti-spyware">Anti-virus &amp; Anti-spyware</a></li>
<li><a href="/pc-utilities/security/internet-security">Internet Security</a></li>
</ul> </li><li class="sub-menu-item">
<a href="/pc-utilities/drivers">Drivers</a>
</li><li class="sub-menu-item menu-item-has-children">
<a href="/pc-utilities/optimisation">Optimisation<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/pc-utilities/optimisation/registry">Registry</a></li>
<li><a href="/pc-utilities/optimisation/maintenance">Maintenance</a></li>
<li><a href="/pc-utilities/optimisation/cleaning">Cleaning</a></li>
</ul> </li><li class="sub-menu-item menu-item-has-children">
<a href="/pc-utilities/recovery">Backup &amp; Recovery<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/pc-utilities/recovery/disk-backup-recovery">Disk Backup &amp; Recovery</a></li>
<li><a href="/pc-utilities/recovery/data-repair">Data Repair</a></li>
</ul> </li>
</ul>
</li>
<li class="menu_tab_item menu_has_children office menu-tab-item" id="menuItem2" rel="2"><a href="/office">Office</a>
<ul class="sub-menu"> <li class="sub-menu-item menu-item-has-children">
<a href="/office/office-suites">Office Suites<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/office/office-suites/pdf">PDF</a></li>
<li><a href="/office/office-suites/ocr">OCR</a></li>
</ul> </li><li class="sub-menu-item">
<a href="/office/file-management">File Manager</a>
</li><li class="sub-menu-item">
<a href="/office/design">Design</a>
</li>
</ul>
</li>
<li class="menu_tab_item menu_has_children photodesign menu-tab-item" id="menuItem3" rel="3"><a href="/photo-design">Photo &amp; Design</a>
<ul class="sub-menu"> <li class="sub-menu-item menu-item-has-children">
<a href="/photo-design/photo-editing">Photo Editing<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/photo-design/photo-editing/portrait-retouching">Portrait Retouching</a></li>
<li><a href="/photo-design/photo-editing/noise-reduction">Noise Reduction</a></li>
<li><a href="/photo-design/photo-editing/photo-enhancement">Photo Enhancement</a></li>
<li><a href="/photo-design/photo-editing/image-cutout">Image Cutout</a></li>
</ul> </li><li class="sub-menu-item menu-item-has-children">
<a href="/photo-design/creative">Creative Software<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/photo-design/creative/greeting-cards">Greeting Cards</a></li>
<li><a href="/photo-design/creative/website-design">Website Design</a></li>
<li><a href="/photo-design/creative/animation">Animation</a></li>
</ul> </li><li class="sub-menu-item menu-item-has-children">
<a href="/photo-design/home-design">Home Design<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/photo-design/home-design/architect-3d">Architect 3D</a></li>
<li><a href="/photo-design/home-design/turbofloorplan">TurboFloorPlan</a></li>
</ul> </li>
</ul>
</li>
<li class="menu_tab_item menu_has_children multimedia menu-tab-item" id="menuItem4" rel="4"><a href="/multimedia">Multimedia</a>
<ul class="sub-menu"> <li class="sub-menu-item menu-item-has-children">
<a href="/multimedia/video">Video &amp; Audio<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/multimedia/video/streaming-recording">Streaming &amp; Recording</a></li>
<li><a href="/multimedia/video/conversion">Conversion</a></li>
<li><a href="/multimedia/video/creation">Editing</a></li>
</ul> </li><li class="sub-menu-item menu-item-has-children">
<a href="/multimedia/learning">Learning<span class="fa sub-menu"></span></a>
<ul class="sub-sub-menu"> <li><a href="/multimedia/learning/pc-training">PC Training</a></li>
<li><a href="/multimedia/learning/language">Language Learning</a></li>
<li><a href="/multimedia/learning/translation">Translation</a></li>
</ul> </li><li class="sub-menu-item">
<a href="/multimedia/smartphone">Smartphone</a>
</li><li class="sub-menu-item">
<a href="/multimedia/genealogy">Genealogy</a>
</li>
</ul>
</li>
<li class="menu_tab_item mac menu-tab-item" id="menuItem5" rel="5"><a href="/mac">Mac</a>
<ul class="sub-menu"> <li class="sub-menu-item">
<a href="/mac/utilities">Utilities</a>
</li><li class="sub-menu-item">
<a href="/mac/photo-design">Photo &amp; Design</a>
</li><li class="sub-menu-item">
<a href="/mac/multimedia">Multimedia</a>
</li><li class="sub-menu-item">
<a href="/mac/office">Office</a>
</li>
</ul>
</li>
</ul>
<div class="mob-extra-links"></div>
</div>
</div>
</nav>
</div>
</div><div class="container" id="page-container">
<div class="full_page_home">
<!-- start Banner 1 --> <div class="positions_3x2" id="home_banner0" style="float:left">
<a href="/software/inpixio-photo-clip-8-506071?rs2=SC_EN_BAN_PCLIP8"><img class="mega_pclip8_v2_en" src="//cdn1.avanquest.com/jimages/UK/homePagesBanners/mega_pclip8_v2_en.png"/></a> </div>
<!-- END BANNER -->
<!-- start Banner 2 --> <div class="positions_1 banner_right" id="home_banner1" style="float:left">
<div class="prodChart_wrap"><div class="prodChart_head"></div><div class="prodChart_item" id="prodChart_item0"><a href="/software/powerdesk-pro-9-506017?meta=office&amp;cat=file-management">PowerDesk Pro 9</a></div><div class="prodChart_item" id="prodChart_item1"><a href="/software/movavi-video-converter-18-506195?meta=multimedia&amp;cat=video&amp;sub=conversion">Movavi Video Converter 18</a></div><div class="prodChart_item" id="prodChart_item2"><a href="/software/movavi-video-converter-8-for-mac-506196?meta=mac&amp;cat=multimedia">Movavi Video Converter 8 for Mac</a></div><div class="prodChart_item" id="prodChart_item3"><a href="/software/movavi-video-converter-18-premium-506255?meta=multimedia&amp;cat=video&amp;sub=conversion">Movavi Video Converter 18 Premium</a></div><div class="prodChart_item" id="prodChart_item4"><a href="/software/movavi-video-converter-8-premium-for-mac-506256?meta=mac&amp;cat=multimedia" style="line-height:1em; margin-top:2px;">Movavi Video Converter 8 Premium for Mac</a></div><div class="prodChart_item" id="prodChart_item5"><a href="/software/greeting-card-factory-deluxe-9-506018?meta=photo-design&amp;cat=creative&amp;sub=greeting-cards">Greeting Card Factory Deluxe 9</a></div><div class="prodChart_item" id="prodChart_item6"><a href="/software/driver-genius-19-professional-506632?meta=pc-utilities&amp;cat=drivers">Driver Genius 19 Professional</a></div></div> </div>
<!-- END BANNER -->
<!-- start Banner 3 --> <div class="positions_1 banner_right" id="home_banner2" style="float:left">
<div class="find_it_fast_wrap_1"><div class="find_it_fast_head"></div><div class="find_it_fast_item"><a href="/software/inpixio-photo-clip-8-506071?meta=photo-design&amp;cat=photo-editing&amp;sub=image-cutout">InPixio Photo Clip</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="search.html?gsearch=expert+pdf">PDF Editing Software</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="/software/hallmark-card-studio-deluxe-2018-506152?meta=photo-design&amp;cat=creative&amp;sub=greeting-cards">Greeting Card Software</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="/software/website-x5-start-14-506150?meta=photo-design&amp;cat=creative&amp;sub=website-design">Web Design Software</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="/software/filmora-506231?meta=multimedia&amp;cat=video&amp;sub=creation">Filmora</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="/aq-you/register-your-product/">Product Registration</a></div><div class="find_it_fast_spacer"></div><div class="find_it_fast_item"><a href="http://support-uk.avanquest.com/en/support/home">Technical Support FAQs</a></div></div> </div>
<!-- END BANNER -->
<div class="clear"></div>
<div class="new-prods-banner">
<h6>NEW IN</h6>
<div class="new-prods-inner">
<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<!-- start Banner 4 --> <div class="positions_1" id="home_banner3" style="float:left">
<a href="/reseller/"><img class="program reseller" src="//cdn1.avanquest.com/jimages/program reseller.gif"/></a> </div>
<!-- END BANNER -->
<!-- start Banner 5 --> <div class="positions_1 banner_right" id="home_banner4" style="float:left">
<a href="/productindex.html?os=Windows+10&amp;rs2=SC_EN_BAN_WIN10"><img class="carre_win10_235x220_uk" src="//cdn3.avanquest.com/jimages/UK/homePagesBanners/235x220/carre_win10_235x220_uk.png"/></a> </div>
<!-- END BANNER -->
<!-- start Banner 6 --> <div class="positions_1 banner_right" id="home_banner5" style="float:left">
<a href="/search.html?gsearch=driver+genius&amp;rs2=SC_EN_BAN_DG18"><img class="dg18 235x220" src="//cdn.avanquest.com/jimages/UK/homePagesBanners/235x220/dg18 235x220.jpg"/></a> </div>
<!-- END BANNER -->
</div> <div class="clear"></div>
<div action="" class="newsletter-bar" method="get">
<form class="newsletter-bar-form" id="newsletter-bar-form">
<strong class="newsletter-bar-title">Newsletter</strong>
<input class="input text newsletter_bar_email" name="subscribe" placeholder="Your email adress" type="text" value=""/>
<input class="newsletter_bar_rs2" name="rs2" type="hidden" value=""/>
<input class="submit" type="submit" value="Submit"/>
</form>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div id="footer">
<div class="container footer-menu-wrap">
<div class="footer-block footer-block-1">
<header>
			 Shop with Confidence		</header>
<nav>
<ul>
<li><a href="/aq-you/shop-with-confidence/index.html">Guaranteed Security</a></li>
<li><a href="/aq-you/shop-with-confidence/Respecting_Your_Privacy.html">Privacy Policy</a></li>
<li><a href="/aq-you/shop-with-confidence/delivery_options.html">Your Delivery Options</a></li>
<li><a href="/aq-you/shop-with-confidence/easy_returns.html">Return Policy</a></li>
<li><a href="/aq-you/support/index.html">Customer Support</a></li>
</ul>
</nav>
</div>
<div class="footer-block footer-block-2">
<header>
			About Us		</header>
<nav>
<ul>
<li><a href="/discover-avanquest/">Discover Avanquest</a></li>
<li><a href="/group/legal/index.html">Terms of use for this site</a></li>
<li><a href="/group/legal/Terms_and_Conditions.html">Terms and Conditions</a></li>
<li><a href="/group/legal/Privacy_Policy.html">How we Protect your Privacy</a></li>
<li><a href="/group/legal/copyright_trademarks.html">About Trademarks and Logos</a></li>
<li><a href="http://files.avanquest.com/">Articles &amp; Knowledgebase</a></li>
<li><a href="/site_map.html">Site Map</a></li>
</ul>
</nav>
</div>
<div class="footer-block footer-block-3">
<header>
			OUR PARTNERS		</header>
<nav>
<ul>
<li><a href="http://www.avanquest-group.com/en/become_a_reseller.html">Trade Distributors</a></li>
<li><a href="http://www.avanquest-group.com/en/become_an_affiliate.html">Become an Affiliate</a></li>
</ul>
</nav>
</div>
<div class="clear"></div>
</div>
<div class="container footer-links-wrap">
<div class="footer-links">
<ul>
<li><b>SoftCity Network Websites:</b></li>
<li><a href="http://www.architecte3d.com/" target="_blank">Architecte3D.com</a> | </li>
<li><a href="http://www.expert-pdf.com/" target="_blank">ExpertPDF.com</a> | </li>
<li><a href="http://www.outlookpstrecovery.com/how-to-repair-a-corrupt-pst-file/" target="_blank">Repair PST File</a> | </li>
<li><a href="https://datarepairandrecovery.com/win-data-recovery-software/" target="_blank">Recover Data</a> | </li>
<li><a href="https://www.inpixio.com/" target="_blank">InPixio Photo Software</a> | </li>
<li><a href="http://www.onesafesoftware.com/" target="_blank">OneSafe PC Cleaner</a> | </li>
<li><a href="https://downloadsafer.com/" target="_blank">DownloadSafer</a></li>
</ul>
</div>
<div class="footer-copyright">
<div class="footer-social">
<a class="optanon-show-settings">Cookie Parameters</a>
<a class="foot-social" href="http://www.facebook.com/Avanquest"><span class="fb"></span></a> <a class="foot-social" href="http://twitter.com/Avanquest_Soft"><span class="tw"></span></a> </div>
			Copyright © 2021 Avanquest Software		</div>
<div class="clear"></div>
</div>
</div>
<div id="lightbox-overlay"></div>
<div id="lightbox-popup"><div class="inner"></div><div class="close"></div></div>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Roboto:400,500,700" rel="stylesheet" type="text/css"/>
<script src="/template/new/js/common-new.js"></script>
<script src="/include/javascript/AC_RunActiveContent.js"></script>
<link href="/template/new/css/fa.css" rel="stylesheet" type="text/css"/>
<link href="/template/new/css/style.75.css" rel="stylesheet" type="text/css"/>
<div class="prod-countdown"></div>
</body>
</html>
