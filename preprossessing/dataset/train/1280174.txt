<!DOCTYPE html>
<html class="error-page" dir="rtl" lang="ar-aa" xml:lang="ar-aa" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.sdcltd.com.sa/index.php/ar/log/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>404 - خطأ: 404</title>
<link href="/images/fav.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/components/com_k2/css/k2.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_helix3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_helix3/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/shaper_helix3/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/media/widgetkit/wk-styles-2332b0a3.css" id="wk-styles-css" rel="stylesheet" type="text/css"/>
<script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="/media/system/js/core.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/components/com_k2/js/k2.js?v2.6.9&amp;sitepath=/" type="text/javascript"></script>
<script src="/media/widgetkit/uikit2-20e549e5.js" type="text/javascript"></script>
<script src="/media/widgetkit/wk-scripts-4f8b11c1.js" type="text/javascript"></script>
</head>
<body>
<div class="error-page-inner">
<div>
<div class="container">
<p><i class="fa fa-exclamation-triangle"></i></p>
<h1 class="error-code">404</h1>
<p class="error-message">Page not found</p>
<a class="btn btn-primary btn-lg" href="/" title="HOME"><i class="fa fa-chevron-left"></i> Go Back Home</a>
</div>
</div>
</div>
</body>
</html>