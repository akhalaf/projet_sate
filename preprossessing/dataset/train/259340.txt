<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.brilltech.co.in/"/>
<title>404 Page not Found</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="Brilltech Engineers Pvt. Ltd." name="author"/>
<meta content="global" name="distribution"/>
<meta content="English" name="language"/>
<meta content="general" name="rating"/>
<meta content="index, follow" name="ROBOTS"/>
<meta content="Daily" name="revisit-after"/>
<meta content="index, follow" name="googlebot"/>
<meta content="index, follow" name="bingbot"/>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script>
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/bootstrap.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/bootstrap-theme.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/fonts/themify-icons/themify-icons.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/style.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/media-query.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/menu.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/color.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/external-style.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/widget.css\" \/>");
document.write("<link rel=\"stylesheet\" href=\"brilltech/assets/css/responsive.css\" \/>");
</script>
<script>
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/jquery.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/modernizr.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/bootstrap.min.js\"><\/script>");
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-61830056-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61830056-1');
</script></head><body>
<div class="wrapper">
<header class="modern " id="header">
<div class="top-bar">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<ul class="top-nav nav-left">
<li>
<p><span class="ti-location-pin"></span> Plot No. 58, Ecotech-12, Noida Extn. -201310, NCR - Delhi, India</p>
</li>
</ul>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="bu-social-media">
<ul>
<li><a href="/">Home</a></li><span>|</span>
<li><a href="company-profile.htm" title="Company Profile">Company Profile</a></li><span>|</span>
<li><a href="complaint.htm" title="Complaint">Complaint</a></li><span>|</span>
<li><a href="sitemap.htm" title="Sitemap">Sitemap</a></li><span>|</span>
<li><a href="contact-us.htm" title="Contact Us">Contact Us</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="main-header">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<div class="bu-logo bu-logo-dark">
<div class="bu-media">
<figure>
<a href="/" title="Brilltech Engineers Pvt. Ltd."><img alt="Brilltech Engineers Pvt. Ltd." src="images/brilltech-engineers-pvt-ltd-logo.png" title="Brilltech Engineers Pvt. Ltd."/></a>
</figure>
</div>
</div>
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<div class="bu-main-nav">
<div class="nav-top-section">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="img-holder">
<figure><span class="ti-mobile"></span></figure>
</div>
<div class="text-holder">
<p>+91-9818292266</p>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="img-holder">
<figure><span class="ti-email"></span></figure>
</div>
<div class="text-holder">
<a class="text-white" href="mailto:nitin@brilltech.co.in" title="nitin@brilltech.co.in">
<p>nitin@brilltech.co.in</p></a>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="img-holder">
<figure><span class="ti-time"></span></figure>
</div>
<div class="text-holder">
<p>Mon - Sat 8.00 AM - 18.00 PM</p>
</div>
</div>
</div>
</div>
<nav class="main-navigation">
<ul>
<li class="menu-item-has-children"> <a href="panels.htm" title="Panel">Panel</a>
<ul class="child-product height-child">
<li><a href="lt-distribution-panel.htm" title="LT Distribution Panel">LT Distribution Panel</a></li>
<li><a href="load-management-panel.htm" title="Load Management Panel">Load Management Panel</a></li>
<li><a href="outdoor-power-panel.htm" title="Outdoor Power Panel">Outdoor Power Panel</a></li>
<li><a href="dg-synchronization-panel.htm" title="DG Synchronization Panel">DG Synchronization Panel</a></li>
<li><a href="33-kv-vcb-panel.htm" title="33 KV VCB Panel">33 KV VCB Panel</a></li>
<li><a href="marshalling-panel.htm" title="Marshalling Panel">Marshalling Panel</a></li>
<li><a href="main-lt-panels.htm" title="Main LT Panel">Main LT Panel</a></li>
<li><a href="electrical-panel.htm" title="Electrical Panel">Electrical Panel</a></li>
<li><a href="low-tension-panels.htm" title="Low Tension Panel">Low Tension Panel</a></li>
<li><a href="power-control-panel.htm" title="Power Control Panel">Power Control Panel</a></li>
<li><a href="process-control-panels.htm" title="Process Control Panel">Process Control Panel</a></li>
<li><a href="control-panel-boards.htm" title="Control Panel Board">Control Panel Board</a></li>
<li><a href="power-distribution-panels.htm" title="Power Distribution Panel">Power Distribution Panel</a></li>
<li><a href="fire-panel.htm" title="Fire Panel">Fire Panel</a></li>
<li><a href="acb-distribution-panel.htm" title="ACB Distribution Panel">ACB Distribution Panel</a></li>
<li><a href="capacitor-panel.htm" title="Capacitor Panel">Capacitor Panel</a></li>
<li><a href="medium-and-low-voltage-panels.htm" title="Medium And Low Voltage Panel">Medium And Low Voltage Panel</a></li>
<li><a href="power-control-center-panel.htm" title="Power Control Center Panel">Power Control Center Panel</a></li>
<li><a href="motor-control-center-panel.htm" title="Motor Control Center Panel">Motor Control Center Panel</a></li>
<li><a href="mimic-panel.htm" title="Mimic Panel">Mimic Panel</a></li>
<li><a href="plc-control-panel.htm" title="PLC Control Panel">PLC Control Panel</a></li>
<li><a href="instrumentation-panel.htm" title="Instrumentation Panel">Instrumentation Panel</a></li>
<li><a href="vfd-panel.htm" title="VFD Panel">VFD Panel</a></li>
<li><a href="soft-starter-panel.htm" title="Soft Starter Panel">Soft Starter Panel</a></li>
<li><a href="amf-control-panel.htm" title="AMF Control Panel">AMF Control Panel</a></li>
<li><a href="synchronizing-panel.htm" title="Synchronizing Panel">Synchronizing Panel</a></li>
<li><a href="11-kv-vcb-panel.htm" title="11 KV VCB Panel">11 KV VCB Panel</a></li>
<li><a href="ht-panel.htm" title="HT Panel">HT Panel</a></li>
<li><a href="feeder-pillar-panel.htm" title="Feeder Pillar Panel">Feeder Pillar Panel</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="electrical-cables.htm" title="Electrical Cable">Electrical Cable</a>
<ul class="child-product height-child">
<li><a href="cables.htm" title="Cable">Cable</a></li>
<li><a href="xlpe-cables.htm" title="XLPE Cable">XLPE Cable</a></li>
<li><a href="ht-cables.htm" title="HT Cable">HT Cable</a></li>
<li><a href="lt-cables.htm" title="LT Cable">LT Cable</a></li>
<li><a href="frls-wires.htm" title="FRLS Wire">FRLS Wire</a></li>
<li><a href="telephone-cable.htm" title="Telephone Cable">Telephone Cable</a></li>
<li><a href="house-wire.htm" title="House Wire">House Wire</a></li>
<li><a href="instrumentation-cable.htm" title="Instrumentation Cable">Instrumentation Cable</a></li>
<li><a href="submersible-cable.htm" title="Submersible Cable">Submersible Cable</a></li>
<li><a href="thermocouple-cable.htm" title="Thermocouple Cable">Thermocouple Cable</a></li>
<li><a href="control-cable.htm" title="Control Cable">Control Cable</a></li>
<li><a href="copper-wires.htm" title="Copper Wire">Copper Wire</a></li>
<li><a href="electric-cables.htm" title="Electric Cable">Electric Cable</a></li>
<li><a href="lt-control-cables.htm" title="LT Control Cable">LT Control Cable</a></li>
<li><a href="mining-cables.htm" title="Mining Cable">Mining Cable</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="chemical-earthing.htm" title="Chemical Earthing">Chemical Earthing</a>
<ul class="child-product height-child">
<li><a href="gi-earthing-strip.htm" title="GI Earthing Strip">GI Earthing Strip</a></li>
<li><a href="copper-earthing-strip.htm" title="Copper Earthing Strip">Copper Earthing Strip</a></li>
<li><a href="conventional-earthing.htm" title="Conventional Earthing">Conventional Earthing</a></li>
<li><a href="earthing-solutions.htm" title="Earthing Solutions">Earthing Solutions</a></li>
<li><a href="chemical-earthing-electrode.htm" title="Chemical Earthing Electrode">Chemical Earthing Electrode</a></li>
<li><a href="lightning-arrester.htm" title="Lightning Arrester">Lightning Arrester</a></li>
<li><a href="lightning-protection.htm" title="Lightning Protection">Lightning Protection</a></li>
<li><a href="rising-mains.htm" title="Rising Mains">Rising Mains</a></li>
<li><a href="meter-board.htm" title="Meter Board">Meter Board</a></li>
<li><a href="sub-stations.htm" title="Sub Stations">Sub Stations</a></li>
<li><a href="package-substation.htm" title="Package Substation">Package Substation</a></li>
<li><a href="unitized-substations.htm" title="Unitized Substations">Unitized Substations</a></li>
<li><a href="switchyards-substation.htm" title="Switchyards Substation">Switchyards Substation</a></li>
<li><a href="solar-power-plants.htm" title="Solar Power Plants">Solar Power Plants</a></li>
<li><a href="11-kv-pss.htm" title="11 KV PSS">11 KV PSS</a></li>
<li><a href="22-kv-pss.htm" title="22 KV PSS">22 KV PSS</a></li>
<li><a href="33-kv-pss.htm" title="33 KV PSS">33 KV PSS</a></li>
<li><a href="mobile-substation.htm" title="Mobile Substation">Mobile Substation</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="transformers.htm" title="Transformer">Transformer</a>
<ul class="child-product height-child">
<li><a href="electrical-transformers.htm" title="Electrical Transformer">Electrical Transformer</a></li>
<li><a href="oil-filled-transformer.htm" title="Oil Filled Transformer">Oil Filled Transformer</a></li>
<li><a href="dry-type-transformers.htm" title="Dry Type Transformer">Dry Type Transformer</a></li>
<li><a href="distribution-transformer.htm" title="Distribution Transformer">Distribution Transformer</a></li>
<li><a href="power-transformer.htm" title="Power Transformer">Power Transformer</a></li>
<li><a href="isolation-transformer.htm" title="Isolation Transformer">Isolation Transformer</a></li>
<li><a href="amorphous-transformer.htm" title="Amorphous Transformer">Amorphous Transformer</a></li>
<li><a href="converter-transformer.htm" title="Converter Transformer">Converter Transformer</a></li>
<li><a href="furnace-transformer.htm" title="Furnace Transformer">Furnace Transformer</a></li>
<li><a href="cast-resin-transformers.htm" title="Cast Resin Transformer">Cast Resin Transformer</a></li>
<li><a href="current-transformers.htm" title="Current Transformer">Current Transformer</a></li>
<li><a href="electrical-power-transformers.htm" title="Electrical Power Transformer">Electrical Power Transformer</a></li>
<li><a href="power-distribution-transformers.htm" title="Power Distribution Transformer">Power Distribution Transformer</a></li>
<li><a href="potential-transformer.htm" title="Potential Transformer">Potential Transformer</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="generator-set.htm" title="Generator Set">Generator Set</a>
<ul class="child-product ">
<li><a href="diesel-generator-set.htm" title="Diesel Generator set">Diesel Generator set</a></li>
<li><a href="silent-diesel-generator.htm" title="Silent Diesel Generator">Silent Diesel Generator</a></li>
<li><a href="gas-generator-set.htm" title="Gas Generator Set">Gas Generator Set</a></li>
<li><a href="mobile-generator.htm" title="Mobile Generator">Mobile Generator</a></li>
<li><a href="power-generator.htm" title="Power Generator">Power Generator</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="bus-duct.htm" title="Bus Duct">Bus Duct</a>
<ul class="child-product ">
<li><a href="plug-in-bus-duct.htm" title="Plug in Bus Duct">Plug in Bus Duct</a></li>
<li><a href="segregated-bus-duct.htm" title="Segregated Bus Duct">Segregated Bus Duct</a></li>
<li><a href="isolated-phase-bus-duct.htm" title="Isolated Phase Bus Duct">Isolated Phase Bus Duct</a></li>
<li><a href="cable-bus-duct.htm" title="Cable Bus Duct">Cable Bus Duct</a></li>
<li><a href="sandwich-bus-duct.htm" title="Sandwich Bus Duct">Sandwich Bus Duct</a></li>
<li><a href="11-kv-bus-duct.htm" title="11 KV Bus Duct">11 KV Bus Duct</a></li>
<li><a href="33-kv-bus-ducts.htm" title="33 KV Bus Duct">33 KV Bus Duct</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="cable-trays.htm" title="Cable Tray">Cable Tray</a>
<ul class=" ">
<li><a href="galvanized-cable-tray.htm" title="Galvanized Cable Tray">Galvanized Cable Tray</a></li>
<li><a href="ladder-cable-tray.htm" title="Ladder Cable Tray">Ladder Cable Tray</a></li>
<li><a href="perforated-cable-tray.htm" title="Perforated Cable Tray">Perforated Cable Tray</a></li>
<li><a href="race-ways.htm" title="Race Way">Race Way</a></li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</header><div class="page-section">
<div class="header-banner">
<div class="common-banner">
<div class="container">
<div class="manage-braedcumb">
<h1>404 Page</h1>
<ul>
<li><a href="/" title="Home">Home</a></li>
<li>/</li>
<li>404 Page</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<section class="sitemap">
<div class="container">
<div class="row">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 table-type">
<br/>
<br/>
<h3>You can still browse Our Panels – <a href="panels.htm" style="color:#ff8a00;">View Panels</a></h3>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-side">
<div class="form-contact-color" id="inner-form">
<p>Need A Query</p>
<img alt="Brilltech Engineers Pvt. Ltd." src="brilltech/assets/images/under-form.png" title="Brilltech Engineers Pvt. Ltd."/>
<div class="under-form">
<div id="innersuccessmsg"></div>
<form action="enquiries/add" method="post">
<input name="enquiry_for" type="hidden" value="Enquiry For Contact Us"/>
<input name="page_url" type="hidden" value="https://www.brilltech.co.in/docu/fonts/%09%0A"/>
<div class="query-form-field">
<input name="name" placeholder="Name*" required="" type="text"/>
</div>
<div class="query-form-field">
<input name="email" placeholder="Email*" required="" type="email"/>
</div>
<div class="query-form-field">
<input maxlength="15" name="mobile" onkeypress="return event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57 || event.charCode == 43" placeholder="Phone*" required="" type="text"/>
</div>
<div class="query-form-field">
<input name="address" placeholder="Address" type="text"/>
</div>
<div class="query-form-field">
<textarea name="message" placeholder="Message"></textarea>
</div>
<div class="query-form-field-btn">
<input name="submit" type="submit" value="submit"/>
</div>
</form>
</div>
</div>
<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script>
$(function() {
	$("#inner-form form").on('submit', function(event) {
		
		var $form = $(this);		
		$.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize(),
			success: function() {
 			$('#innersuccessmsg').html('Thank you for your enquiry with us, <br> Our representative will get in touch with you soon.');
		 	$('#innersuccessmsg').addClass(' alert alert-success');
						 			 
			 $("#innersuccessmsg").show();
			 setTimeout(function() { $("#innersuccessmsg").hide(); }, 5000);
			 $('#inner-form form')[0].reset();		 
 			
			}
		});
		event.preventDefault();		 
	});
});
</script> </div>
</div>
</div>
</section><footer id="footer">
<div class="bu-footer-widgets">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="widget widget-text">
<div class="widget-section-title">
<p class="widget-heading-1 foot-par">Get in Touch</p></div>
<ul>
<li> <span class="ti-mobile"></span>
<a class="hk-hide" href="tel:+91-9818292266" title="+91-9818292266"><p class="bu-color widget-font-size-1">+91-9818292266</p></a>
</li>
<li> <span class="ti-location-pin"></span>
<p>Plot No. 58, Ecotech-12, Noida Extn. -201310, NCR - Delhi, India</p>
</li>
<li> <span class="ti-email"></span>
<p><a href="mailto:nitin@brilltech.co.in" title="nitin@brilltech.co.in">nitin@brilltech.co.in</a></p>
</li>
<li> <span class="ti-world"></span>
<p><a href="https://www.brilltech.co.in" target="_blank">www.brilltech.co.in</a></p>
</li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="widget widget-categores">
<div class="widget-section-title">
<p class="widget-heading-1 foot-par">Quick Links</p></div>
<ul>
<li><a href="/" title="Home">Home</a></li>
<li><a href="company-profile.htm" title="Company Profile">Company Profile</a></li>
<li><a href="our-presence.htm" title="Our Presence">Our Presence</a></li>
<li><a href="contact-us.htm" title="Contact Us">Contact Us</a></li>
<li><a href="sitemap.htm" title="Sitemap">Sitemap</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="widget widget-useful-links">
<div class="widget-section-title">
<p class="widget-heading-1 foot-par">Our Products</p></div>
<ul>
<li><a href="panels.htm" title="Panel">Panel</a></li>
<li><a href="electrical-cables.htm" title="Electrical Cable">Electrical Cable</a></li>
<li><a href="chemical-earthing.htm" title="Chemical Earthing">Chemical Earthing</a></li>
<li><a href="transformers.htm" title="Transformer">Transformer</a></li>
<li><a href="generator-set.htm" title="Generator Set">Generator Set</a></li>
<li><a href="bus-duct.htm" title="Bus Duct">Bus Duct</a></li>
<li><a href="cable-trays.htm" title="Cable Tray">Cable Tray</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="widget widget-news-letter">
<div class="widget-section-title">
<p class="widget-heading-1 foot-par">About Us</p></div>
<p style="text-align:justify;">In 2007, The Cornerstone Of An Innovative Value-Driven Company - Brilltech Engineers Pvt. Ltd. , Was Laid Under The Supervision Of The Honorable Chairman Mr. Nitin Pratap Singh. With Customers Centric Approach, The Company Caters To Its Clients With A Huge Assortment Of Products That Includes Electrical Panels, Package Substation...</p>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="bu-footer-logo-holder center">
<div class="bu-footer-nav">
<div class="footer-nav"> Copyright © 2019 Brilltech | All Rights Reserved . Website Designed &amp; SEO By Webclick Digital Pvt. Ltd. <strong><a href="http://www.webclickindia.com" target="_blank" title="Website Designing Company">Website Designing Company</a></strong></div>
</div>
</div>
</div>
</div>
</div>
</footer>
</div>
<script>
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/menu.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/slick.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/parallax/parallax.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/counter.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/jquery.fitvids.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/ekko-lightbox.js\"><\/script>");
document.write("<script type=\"text/javascript\" src=\"brilltech/assets/scripts/functions.js\"><\/script>");
</script>
</body>
</html>
