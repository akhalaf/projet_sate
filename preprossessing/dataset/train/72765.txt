<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "52341",
      cRay: "610bac85ae1a1923",
      cHash: "66b1db0ebbf6da8",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWN1ZmYubWUv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "2UMD2lqm4eklzHTA5z2uRB8S2xXz5rHXi4b6NpVh+gJ5xbKipG8lkow9zxoIJv9m0+ClyxpSFb+NmGo6OD0IF0pTxW0m7wbneOUneO7Pyi1bkhnR7VyMTL+hGrbl23CHgmvjYk4b2TfHwk+b5bBVKYCtqsLAFKcSHsLKBsVHYbJPU5JV1UjxalM8Tn6Vlt1wW4lv5bHT4RIb7PlwSdBXQlz7z4f44vZEgPYU/wyepmZvVUdkbOYu8ZoDotT4l3Re4QGSzzY0BVFkHR838cd5VlKs4Vv117gxAz35SCdVHEWUY+r3AXRwwak0UZxT+Cl2jQjgMeFAwaacNdFpqDYq+xVH5DSoMPnXZ3vjBD3BL8IHHB9LiJ+GAhCtFngUOyk8869dtPVIClB/fQmErlwC9n3xdIG0W5okJzW/I0Dhp1tYMCxtASTl4GtmrMFXxey2GwbClxFMMoLfYUgioQA9b7wlTqhwefU63xTqslbeN4T9L56gBFPUSDaykuA5VZ6ErumP8WiZMz2s8l7km+0Ur7+Zwq4BiQyPPVf/FyrpUZYvvIrAIAXsDUiDISwqvd8ephsiJAWAU68VHyCfp7dj0O1JwWkLxZiswt7KlE19l9Klb560ngTRriXmGMRkWLRWgo2VK/b/0mfbt0wgDEcX+QekrFTVsP8HqAEYC7nY1aEz81oNWoH/1aKuRBTjSlM9sJgbn+FV3ZmccYMX7Z5vMonts5ADNPYV2V2Xzjc7TBTgAvBwoVQYU0iq2uHvjX7S",
        t: "MTYxMDUwNDgwMi4xOTcwMDA=",
        m: "yB1USG+dsHio3il8LrZV6b9GHkKgk7lAIAJBcUDfcV8=",
        i1: "HbyppwveAjx4ootk1YPHFw==",
        i2: "B9I8BdjHuBPFo+Q3Z2YICQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "HX7CpmxEJCE/y/c2mjz1gfFHw9YOFivM1Mdg03qZn9o=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.acuff.me</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=0fd05487d89c61555490162fbd309d42c1a80c77-1610504802-0-ARBX22kMkHwHqC0dZk7sPy-d-1A9YV79RERK9YkOlN_uWUHd82ShAOFvTZA2aBwYhA9yQmR0tNDji73PlrXvJxUQ7hTmTJbmvU-MAbEcmBv-GEBHKukHnQn1cOUyyObdxt0rPgKxRt5o4Xcd3XPMFkGFga-8CYLWY43Fy4Pf3zVoePGjdb-7bC0v9vqkE2Jn2LW56G1M_fke_TSLZMKnsx47Se4xhTRqRDDG1Ig6jpNE7RdC1ltJ5q8wJIhCeNB4iWGFL-h6ciNkIxI3Hg5lsu-1_MSLjAfgdGMBQavGoCCcRTEcEnFtEbeHbHDsDSwPXnb6v_wexce99mfTGNFtIVTnXKzhhqkv2mCQEUN7ym3UOyCtRYDv7Tnk8NIuSjXjljN4BlYnLkcS3HSqC0Pn2tMRmetM9y6z1V2tF_VwMJHIv7-KNq_2_T4b8v_Bs8wwpRvIqY1y1olGcRu_K4ZxQy98Lp2zwCJ_eo-alivGWTzdT67asut56P6kFDzILDF3u6XXi1fl8hqhUBLrWfYiMVs" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="575dd130daaf8a8dcfb273a609f378ad057082b4-1610504802-0-AQ1Iv27LR4B559k0tVGBG99FYWs18Y8vmsnjgtvf1/Ad8pOP5HX0jjyhl7mrkhCnoZ7nyxin87hD1OFO4uUPetqvG8CMMGG0HsxjxuBLRgmPjNM/OJsb+/zT5qyfdDeBLpqYHGRho2mr8+MdzaT5sXWlzXhuro7yWBOwC2t3jsRL2RKGesaXzMXW3oEwidiyhFLGKsBNOF9Ce7w5ZDcx6pzCF5PQIn/4sq5hfB7vpDg5QQdBjkHpVtSGoL9RYbQDdUmFx9xyraBSo6EHVhIIPQ7Jc3uJgeOxEy9wpwAVgpH/B7pA6azMOGY63mL6PExMd1sbKq1K1oH9K2356mTyzrJEJILxWRflYLJJUhJw0/Lb4K9CilvAASoNRyL3DXm3vadG+MLiy1uJsRImivAqc6cgj/WdwoIrXnrhaSBUntcTUYplikScUJ+bQ3/1v3iSPEpcqmAdbFKlAU21W1NB07aeJ2SJo0QkfTLgTzPZ8hMWci/6RRSmWsR8P5ESblMKK2VrKwcqbvp27WyAY3XRIMLz50GphdzTv7e5MfL7XJLWfbIfZIWVActUHGRPdqGYAJ21u11a8M79m7YLpZFueENSPuywgoGlvQZe9H6dgdY7sEUjEz45IG0zpCoHwn7qUkTpqepm4zSdAlyTc4Fkc3XAungLOTcKpWY8Zf94Bj1dRORPEJT84il5RZwWlp23qpcOgKLPOGs/4piUVazfwCyZ7F1hXNP7cde8IFn/3UZ1nyHhDoC1PHF//GxcRUOkVNf8fHBksZQUBBAMnufXtcDOx7TDgIhBVCbR8b5YqAob0rHLy4NKOFCNOyPjtd8Iw5vpgurOc2zzPXGGJoeBpP/UHpeS+OL4FfKhtJcUNI1oJfJZp43XXKKWRLzG34PK+ioXPqM4snihMiaJKvpThPQ1HexD94fGDTfa+0GKOrcgTy0r476GmnERJXv9gcWI1nz2Cb2Serf7lKpHr5ArGs7eaRtkUwRqdynhuy07TQxIeoV81cF0s7DfdTu09NpPyfwB6qX/4GVeRmyXGnE7FZggD5oGHpVNOMy9clL2njh5rRp2X+Xbp2AxaId93rFLKZk02mJjj+7kGx27sL8hD1p8pSildLu5uUJnfkEm03xm/9uua1RotHiQsezu6YVm8rqCd8LYAqzPePo96Ew3q1FEZgU2vnkNgt4Y3AK48Df0m0dd2c2Ab55kZKttY1kbkwpRGpFb2bEUsixLv5sY0mN58MOsqQEYo+9SEk9NHU+IWTYRPKxug1bwLPJ73x9uRZgUWGEcs7pzzA6mB0vVEuQ="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="f81166ad60f4c39a4a1750ee03440356"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610bac85ae1a1923')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610bac85ae1a1923</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
