<!DOCTYPE html>
<html lang="zh">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>肯朋网 - 肯德基、麦当劳、必胜客等优惠券及促销活动分享</title>
<meta content="肯朋网分享肯德基优惠券、麦当劳优惠券等品牌优惠券获取使用方法，以及KFC、麦当劳、必胜客、汉堡王等菜单及新玩具和促销活动信息。" name="description"/>
<style type="text/css">
html,body{padding:0;margin:0;font-size:16px;font-family:Arial,"PingFang SC","Microsoft YaHei";}
ul,li{list-style-type:none;margin:0;padding:0;}
a{color:#666;text-decoration:none;}a:hover{color:#4aacdc;text-decoration:underline;}
h1,h2,h3,h4{margin:0;padding:0;}h1{font-size:2em;}h3{font-size:1.25em;}
.wrap{width:1080px;min-height:1em;text-align:left;margin:0 auto;clear:both;padding:0 0.75em;}
.header,.main,.footer,.tips{text-align:center;min-width:1128px;clear:both;}
.header{height:65px;border-bottom:1px solid #eee;}
#logo{display:block;float:left;border:0;}#logo img{height:60px;width:109px;border:0;}
.head_nav{float:left;padding-left:4em;}.head_nav li{display:inline-block;line-height:60px;}.head_nav li a{padding:0 1.5em;color:#333;font-weight:bold;text-decoration:none;border-right:1px solid #f3f3f3;}.head_nav li a:hover{color:#4aacdc;}
.tips{padding:1em 0;background-color:#f1feff;border-bottom:1px solid #eee;min-height:3em;}.tips em{display:block;float:left;margin-top:0.8em;padding:0 1.5em 0 1em;border-right:2px solid #60c3fc;font-weight:bold;}.tips span{display:block;float:left;padding-left:2em;font-size:0.875em;color:#666;line-height:1.7em;}
.icon_wrap{float:right;padding-top:0.5em;font-size:1.8em;}.icon_wrap li{float:left;margin-left:0.7em;}
.icon{width:0.9em;height:0.9em;vertical-align:middle;overflow:hidden;}.contact{margin-top:0.1em;width:0.8em;height:0.8em;}
.main{padding-top:1.5em;}.dsimg{display:block;margin:0 auto;width:220px;height:128px;background-size:220px 176px;}
.sug_wrap{padding-top:0.8em;}.sug_wrap li{display:block;float:left;width:25%;}.sug_wrap li a{display:block;font-size:0.875em;border:1px solid #f3f3f3;text-align:center;padding:0.75em 0;}.sug_wrap li a:hover{background-color:#fdfdfd;}
.dstxt{display:block;width:220px;line-height:1.8em;height:3.6em;overflow:hidden;margin:0 auto;text-align:left;padding-top:0.5em;}
.last{padding-top:1.25em;}.last ul{font-size:0.875em;padding-top:0.3em;}.last ul li{width:50%;display:block;float:left;line-height:3em;height:2.8em;overflow:hidden;border-bottom:1px solid #eee;}
.bd{color:#000;}
.mnu,.cps{padding-top:2em;}.mnu_col1,.mnu_col2{float:left;}.mnu_col1{width:25%;}.mnu_col2{width:75%;font-size:0.875em;}.mnu_col1 ul{border-left:3px solid #ccc;padding:0.5em 1em;min-height:300px;}.mnu_col1 li{padding-bottom:2em;}.mnu_col2 img{width:120px;height:93px;border:1px solid #f3f3f3;margin-right:0.5em;vertical-align:middle;}.mnu_col2 ul li{width:33.3%;display:block;float:left;height:95px;overflow:hidden;margin-bottom:1.5em;}.mnu_col2 a:hover img{border-color:#f90;}
.tps{margin-right:305px;font-size:0.875em;}.tps li{width:250px;float:left;margin-bottom:2em;}.tps a{display:block;clear:both;width:180px;line-height:1.8em;border-bottom:1px dashed #eee;}.tps img{width:180px;height:112px;}
.ctxt{height:3.8em;overflow:hidden;}
.mre{background-color:#f9f9f9;padding:1.5em 0;}
.mre h3{padding-left:1.3em;}.tags{overflow:hidden;padding-top:1em;}.tags li{display:block;float:left;margin:0.5em 0.3em 0.5em 1.3em;border:1px solid #eee;background-color:#fcfcfc;font-size:0.875em;}.tags a{display:block;padding:0.7em 1.5em;line-height:1em;}
.footer,.footer a{color:#999;font-size:0.8em;}.footer{margin-top:1.5em;padding:1.5em 0;text-align:center;line-height:2em;border-top:1px solid #eee;}.footer a{text-decoration:underline;}
.bb{font-weight:bold;color:#000;}.g300{float:right;}
</style>
</head>
<body>
<div class="header"><div class="wrap">
<a href="/" id="logo"><img alt="肯朋网" src="/static/logo/logo_0156.png"/></a>
<ul class="head_nav">
<li><a href="/">首页</a></li>
<li><a href="/discount/">优惠信息</a></li>
<li><a href="/menu/">菜单</a></li>
<li><a href="#" style="color:#999;">热门</a></li>
<li><a href="/contact.php">联系</a></li>
</ul>
<ul class="icon_wrap">
<li id="wechat"></li>
<li id="fav"></li>
<li id="contact"></li>
</ul>
</div></div>
<div class="tips"><div class="wrap">
<em>提示</em>
<span>肯德基、麦当劳、必胜客、汉堡王等优惠券目前均采用会员领取的方式，本站只分享各品牌官方优惠券获取方式和优惠内容，<br/>以及新品菜单、最新玩具、优惠促销等信息。</span>
</div></div>
<div class="main">
<div class="wrap">
<h3>推荐活动</h3>
<ul class="sug_wrap">
<li><a href="//www.5ikfc.com/discount/kfc/19-wucan-taocan/"><span class="dsimg" style="background-image:url(//d3.5ikfc.com/static/discount/kfc/20/v/kfc-5ikfc-1118_141.jpg);"></span><span class="dstxt">肯德基工作日硬核午餐超值套餐19元起</span></a></li>
<li><a href="//www.5ikfc.com/discount/tb/buwsb/"><span class="dsimg" style="background-image:url(//d3.5ikfc.com/static/discount/eg/20/v/tb-5ikfc-1021_81.jpg);"></span><span class="dstxt">淘宝天猫2020双11超级红包，无门槛可叠加</span></a></li>
<li><a href="//www.5ikfc.com/discount/tb/2pceg/"><span class="dsimg" style="background-image:url(//d3.5ikfc.com/static/discount/eg/20/v/ele-5ikfc-41_171.jpg);"></span><span class="dstxt">饿了么红包天天领，每天一次先领券后点餐</span></a></li>
<li><a href="//www.5ikfc.com/discount/kfc/201808-thursday-9-9/"><span class="dsimg" style="background-image:url(//d3.5ikfc.com/static/discount/kfc/20/v/kfc-5ikfc-1116_190.jpg);"></span><span class="dstxt">肯德基疯狂星期四活动，11月9.9元15块鸡翅尖</span></a></li>
</ul>
</div>
<div class="wrap last">
<h3>最新优惠信息</h3>
<ul class="last_wrap">
<li>[<a class="bd" href="/kfc/">肯德基</a>] <a href="//www.5ikfc.com/kfc/shouji/5141102/">肯德基11月优惠券卡券，双人餐33元起 圣代优惠价9.5元</a></li>
<li>[<a class="bd" href="/kfc/">肯德基</a>] <a href="//www.5ikfc.com/kfc/shouji/7241102/">葡式蛋挞（经典）1只+黄金鸡块5块 2020年11月凭肯德基优惠券15.5元</a></li>
<li>[<a class="bd" href="/kfc/">肯德基</a>] <a href="//www.5ikfc.com/kfc/shouji/6241102/">卷堡双人套餐 2020年11月凭肯德基优惠券58元</a></li>
<li>[<a class="bd" href="/mdl/">麦当劳</a>] <a href="//www.5ikfc.com/mdl/coupon202023740">麦当劳早餐+1元升级紫米风味优品豆浆</a></li>
<li>[<a class="bd" href="/kfc/">麦当劳</a>] <a href="//www.5ikfc.com/mdl/coupon202023751">麦当劳阿华田口味华夫筒甜品站第二份半价，原价10元/个</a></li>
<li>[<a class="bd" href="/hbw/">汉堡王</a>] <a href="//www.5ikfc.com/hbw/coupon202023767">汉堡王11月优惠券，套餐优惠价25元起</a></li>
<li>[<a class="bd" href="/kfc/">肯德基</a>] <a href="//www.5ikfc.com/kfc/shouji/9141102/">薯条（中）1份 2020年11月凭肯德基优惠券9.5元</a></li>
<li>[<a class="bd" href="/hbw/">汉堡王</a>] <a href="//www.5ikfc.com/hbw/coupon202023773">乌市汉堡王 霸王桶（盐酥鸡版） 2020年11月12月2021年1月凭券优惠价48元</a></li>
</ul>
</div>
<div class="wrap cps">
<div class="g300">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- vblk -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6457330432628840" data-ad-slot="5319842204" style="display:inline-block;width:300px;height:600px"></ins>
<script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
</div>
<div class="tps">
<ul>
<li><a href="//www.5ikfc.com/kfc/shouji/1241102/"><img alt="原味圣代（北美蓝莓酱/冲绳黑糖珍珠酱）1个 2020年11月凭肯德基优惠券9.5元" src="//data.5ikfc.com/static/coupons/kfcm/2020/thumb/kfc-5ikfc-111_109.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/kfc/shouji/1241102/">原味圣代（北美蓝莓酱/冲绳黑糖珍珠酱）1个 2020年11月凭肯德基优惠券9.5元</a></li>
<li><a href="//www.5ikfc.com/kfc/shouji/6241102/"><img alt="卷堡双人套餐 2020年11月凭肯德基优惠券58元" src="//data.5ikfc.com/static/coupons/kfcm/2020/thumb/kfc-5ikfc-111_104.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/kfc/shouji/6241102/">卷堡双人套餐 2020年11月凭肯德基优惠券58元</a></li>
<li><a href="//www.5ikfc.com/kfc/shouji/8241102/"><img alt="新奥尔良烤翅2块+薯条（小） 2020年11月凭肯德基优惠券17.5元" src="//data.5ikfc.com/static/coupons/kfcm/2020/thumb/kfc-5ikfc-111_101.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/kfc/shouji/8241102/">新奥尔良烤翅2块+薯条（小） 2020年11月凭肯德基优惠券17.5元</a></li>
<li><a href="//www.5ikfc.com/mdl/coupon202023610"><img alt="麦当劳周优惠券领取，免费麦辣鸡翅 0元新麦旋风" src="//data.5ikfc.com/homecoupons/mdl/2020/mdl-5ikfc-1027_111_1.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/mdl/coupon202023610">麦当劳周优惠券领取，免费麦辣鸡翅 0元新麦旋风</a></li>
<li><a href="//www.5ikfc.com/mdl/coupon202023751"><img alt="麦当劳阿华田口味华夫筒甜品站第二份半价，原价10元/个" src="//data.5ikfc.com/homecoupons/mdl/2020/mdl-5ikfc-1029_51.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/mdl/coupon202023751">麦当劳阿华田口味华夫筒甜品站第二份半价，原价10元/个</a></li>
<li><a href="//www.5ikfc.com/mdl/coupon202023744"><img alt="麦当劳炸鸡麦麦脆汁鸡限时9.9元特惠" src="//data.5ikfc.com/homecoupons/mdl/2020/mdl-5ikfc-109_221.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/mdl/coupon202023744">麦当劳炸鸡麦麦脆汁鸡限时9.9元特惠</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/202010-di5renge/"><img alt="肯德基万圣节冰淇淋得《第五人格》限量贴纸" src="//d3.5ikfc.com/static/discount/kfc/20/v/kfc-5ikfc-1026_171.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/discount/kfc/202010-di5renge/">肯德基万圣节冰淇淋得《第五人格》限量贴纸</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/20201031-toys-doraemon/"><img alt="肯德基2020万圣节哆啦A梦玩具，尖叫开跑的蓝胖子" src="//d3.5ikfc.com/static/discount/kfc/20/v/kfc-5ikfc-1024_211.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/discount/kfc/20201031-toys-doraemon/">肯德基2020万圣节哆啦A梦玩具，尖叫开跑的蓝胖子</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/202010-doulu-nancha/"><img alt="肯德基新品豆乳奶茶，玄米与豆乳的初见" src="//d3.5ikfc.com/static/discount/kfc/20/v/kfc-5ikfc-1022_212.jpg"/></a><a class="ctxt" href="//www.5ikfc.com/discount/kfc/202010-doulu-nancha/">肯德基新品豆乳奶茶，玄米与豆乳的初见</a></li>
</ul>
</div>
</div>
<div class="wrap mnu">
<div class="mnu_col1">
<ul>
<li><a href="/kfc/menu/" style="color:#f90;">肯德基菜单</a></li>
<li><a href="/mdl/menu/">麦当劳菜单</a></li>
<li><a href="/bsk/menu/">必胜客菜单</a></li>
</ul>
</div>
<div class="mnu_col2">
<ul>
<li><a href="//www.5ikfc.com/kfc/menu/Bucket/chitong"><img alt="肯德基 翅桶" src="//cache.5ikfc.com/imgs/kfc/menu/bucket/preview/kfc-5ikfc-19325_04.jpg"/>翅桶（10翅1桶）</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Meal/neniuwufang"><img alt="肯德基 嫩牛五方" src="//cache.5ikfc.com/imgs/kfc/menu/meal/preview/kfc-5ikfc-19325_01.jpg"/>嫩牛五方</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Meal/n5zas"><img alt="香辣鸡枞菌菌菇鸡腿双层堡" src="//cache.5ikfc.com/imgs/kfc/menu/meal/preview/kfc-5ikfc-2242_126.jpg"/>香辣鸡枞菌菌菇鸡腿双层堡</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/DessertAndDrink/taozilianwulongcha"><img alt="桃之恋乌龙茶" src="//cache.5ikfc.com/imgs/kfc/menu/dessertanddrink/preview/kfc-5ikfc-19325_07.jpg"/>桃之恋乌龙茶</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/panini/dlgrilledchopspanini"><img alt="双层香烤鸡排帕尼尼" src="//cache.5ikfc.com/imgs/kfc/menu/panini/preview/kfc-5ikfc-1993_853.jpg"/>双层香烤鸡排帕尼尼</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/kjifantong/paojiaojikuaifan"><img alt="川辣泡椒鸡块饭" src="//cache.5ikfc.com/imgs/kfc/menu/kjifantong/preview/kfc-5ikfc-94_133.jpg"/>川辣泡椒鸡块饭</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Breakfast/bbbqs"><img alt="生滚牛肉粥" src="//cache.5ikfc.com/imgs/kfc/menu/breakfast/preview/kfc-5ikfc-19113_93.jpg"/>生滚牛肉粥</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Breakfast/7628d"><img alt="酱拌金枪鱼肉酥饭团" src="//cache.5ikfc.com/imgs/kfc/menu/breakfast/preview/kfc-5ikfc-1993_851.jpg"/>酱拌金枪鱼肉酥饭团</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Breakfast/youjituizhongshican"><img alt="有鸡腿中式全餐" src="//cache.5ikfc.com/imgs/kfc/menu/breakfast/preview/kfc-5ikfc-1993_856.jpg"/>有鸡腿中式全餐</a></li>
</ul>
</div>
</div>
<div class="wrap mre">
<h3>更多标签</h3>
<div class="tags_wrap">
<ul class="tags">
<li><a class="bb" href="/kfc/">肯德基</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/201808-thursday-9-9/">疯狂星期四</a></li>
<li><a href="//www.5ikfc.com/kfc/menu/Bucket/chitong">39元翅桶</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/20201031-toys-doraemon/">万圣节哆啦A梦玩具</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/6yuan-zaocan/">6元起早餐</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/kaifengcai-luoshifeng/">螺蛳粉</a></li>
<li><a href="//www.5ikfc.com/discount/kfc/202010-kaifengcai-jitang/">鸡汤</a></li>
<li><a class="bb" href="/mdl/">麦当劳</a></li>
<li><a href="//www.5ikfc.com/mdl/coupon202023744">9.9元炸鸡</a></li>
<li><a href="//www.5ikfc.com/mdl/coupon202023610">每周会员日超值优惠</a></li>
<li><a class="bb" href="/bsk/">必胜客</a></li>
<li><a class="bb" href="/hbw/">汉堡王</a></li>
<li><a class="bb" href="/b/dks/">德克士</a></li>
</ul>
</div>
</div>
</div>
<div class="footer">
    © 肯朋网 <a href="/">5ikfc.com</a><br/>[ <a href="http://beian.miit.gov.cn/" rel="nofollow" target="_blank">浙ICP备2020038318号-1</a> ]
</div>
<script type="text/javascript">
    var $ = function(id){return(document.getElementById(id));};
    var wch = $("wechat"), fav = $("fav"), contact = $("contact");
    if(typeof SVGRect != 'undefined'){
    wch.innerHTML = '<a href="/t/weixin-5ikfc.html"><svg class="icon" id="wechat" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M392.5 136.5C200.5 136.5 42.6 264.5 42.6 418.1c4.2 89.6 51.2 174.9 132.2 217.6l-38.4 110.9L256 674.1c42.6 17.0 89.6 21.3 136.5 21.3-12.8-29.8-17.0-59.7-17.0-93.8 0-153.6 145.0-281.6 328.5-281.6h25.6c-51.2-102.4-179.2-183.4-337.0-183.4M256 251.7c25.6 0 46.9 21.3 46.9 46.9S281.6 345.6 256 345.6 209.0 328.5 209.0 298.6c0-25.6 17.0-46.9 46.9-46.9m234.6 0c25.6 0 46.9 21.3 46.9 46.9s-21.3 46.9-46.9 46.9c-25.6 0-46.9-21.3-46.9-46.9s17.0-46.9 46.9-46.9m209.0 119.4c-153.6 0-281.6 106.6-281.6 234.6s128 234.6 281.6 234.6c29.8 0 59.7-4.2 89.6-12.8l98.1 59.7-29.8-89.6c72.5-38.4 119.4-110.9 123.7-192 0-128-128-234.6-281.6-234.6m-93.8 115.2c25.6 0 46.9 21.3 46.9 46.9s-21.3 46.9-46.9 46.9c-25.6 0-46.9-21.3-46.9-46.9 0-25.6 21.3-46.9 46.9-46.9m187.7 0c25.6 0 46.9 21.3 46.9 46.9s-21.3 46.9-46.9 46.9-46.9-21.3-46.9-46.9c0-25.6 21.3-46.9 46.9-46.9z" fill="#666"></path></svg></a>';
    fav.innerHTML = '<a href="#"><svg class="icon" id="fav" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M335.0 916.6c-35.9 22.3-82.8 10.7-104.6-25.5a77.3 77.3 0 0 1-8.9-57.4l46.4-198.2a13.1 13.1 0 0 0-4.0-12.8l-152.1-132.5c-31.6-27.5-35.2-75.6-8.2-107.7a75.6 75.6 0 0 1 51.7-26.7L354.8 339.2c4.3-0.3 8.2-3.2 10.0-7.5l76.9-188.1c16.0-39.2 60.6-57.9 99.5-41.4a76.3 76.3 0 0 1 40.8 41.4l76.9 188.1c1.7 4.3 5.6 7.2 10.0 7.6l199.7 16.2c41.8 3.4 72.8 40.4 69.5 82.5a76.9 76.9 0 0 1-26.0 51.9L760.1 622.5c-3.5 3.0-5.1 8.0-4.0 12.8l46.4 198.2c9.6 41.0-15.3 82.3-56.1 92.2a75.2 75.2 0 0 1-57.5-9.2L517.9 810.4a11.2 11.2 0 0 0-12.0 0L334.9 916.6z m216.7-160.5l170.9 106.2c2.6 1.6 5.7 2.1 8.6 1.4 6.4-1.5 10.5-8.4 8.9-15.4l-46.4-198.2a77.1 77.1 0 0 1 24.2-75.7l152.1-132.5c2.4-2.1 4.0-5.3 4.3-8.7 0.5-7.1-4.4-13.1-10.9-13.6l-199.7-16.2a75.9 75.9 0 0 1-64.0-47.1l-76.9-188.1a12.3 12.3 0 0 0-6.5-6.7c-5.8-2.4-12.7 0.3-15.3 6.7l-76.9 188.1a75.9 75.9 0 0 1-64.0 47.1l-199.7 16.2a11.6 11.6 0 0 0-7.9 4.1 13.2 13.2 0 0 0 1.3 18.2l152.1 132.5a77.1 77.1 0 0 1 24.2 75.7l-46.4 198.2a13.3 13.3 0 0 0 1.5 9.8c3.4 5.7 10.5 7.5 16.0 4.1l170.9-106.2a75.2 75.2 0 0 1 79.5 0z" fill="#666"></path></svg></a>';
    contact.innerHTML = '<a href="/contact.php"><svg class="icon contact" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M1024.0 220.6v574.3c0 5.7-0.7 11.4-1.8 17.0v0.0l-0.2 0.9A84.3 84.3 0 0 1 939.2 880H101.0a102.7 102.7 0 0 1-82.5-40.8 95.9 95.9 0 0 1-18.5-57.2V236.2a74.0 74.0 0 0 1 52.0-70.7l0.7-0.2 0.2-0.0A127.6 127.6 0 0 1 89.6 160h883.4c1.3 0.5 2.6 1.2 4.0 1.4 25.5 4.3 40.5 19.1 45.3 44.4v0.0c0.8 4.8 1.5 9.7 1.5 14.6zM512.0 591.2l451.1-379.4c-66.2-3.8-888.4-1.7-897.8 2.3L512.0 591.2v0.0z m133.8-45.1l-114.1 96.1c-13.7 11.5-25.6 11.6-39.2 0.2l-48.2-40.7-63.8-53.9L81.0 825.2l1.0 2.3h873.3L645.7 546.0h0.0z m-304.8-31.6L51.7 270.0v512.4l289.2-268.0h-0.0z m344.3-1.6l287.0 260.9V270.9L685.3 512.7z" fill="#666"></path></svg></a>';
    }else{
        wch.innerHTML = '<a href="/t/weixin-5ikfc.html">W</a>';
        fav.innerHTML = '<a href="#">F</a>';
        contact.innerHTML = '<a href="/contact.php">C</a>';
    }
</script>
</body>
</html>