<!DOCTYPE html>
<html>
<head>
<title>Webcam Berliner Schloss - Südfassade | Hi.Res.Cam</title>
<meta charset="utf-8"/>
<meta content="Webcam, Stadtschloss, Berliner Schloss, Hi.Res.Cam" name="keywords"/>
<meta content="Webcam Berliner Schloss" name="description"/>
<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=no;" name="viewport"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/dhtmlx/skins/dhtmlxmenu_dhx_black.css" rel="stylesheet" type="text/css"/>
<link href="/css/ui-darkness/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css"/>
<link href="/css/rlightbox/lightbox.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/hirescam.css" rel="stylesheet" type="text/css"/>
<style type="text/css">#logo_l {
    background-repeat: no-repeat !important;
    background-image: url(/sbsh.png) !important;
    background-position:center !important;
    background: rgba(255, 255, 255, 1);
    border-bottom: 1px solid white !important;
    border-right: 1px solid white !important;
    height: 95px !important;
    top: 23px !important;
    left: 0 !important;
    position: absolute !important;
    width: 250px !important;
    z-index: 1 !important;
}

.nanoGalleryViewer .toolbar .label {
                font-family: "Droid Sans",sans-serif !important;
}
.nanoGalleryViewer .toolbar .pageCounter {
                font-family: "Droid Sans",sans-serif !important;
}
</style>
<script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
<script src="/js/jquery.fullscreen.js" type="text/javascript"></script>
<script src="/dhtmlx/dhtmlxcommon.js" type="text/javascript"></script>
<script src="/dhtmlx/dhtmlxmenu.js" type="text/javascript"></script>
<script src="/dhtmlx/ext/dhtmlxmenu_ext.js" type="text/javascript"></script>
<script src="/js/jquery.ui.rlightbox.min.js" type="text/javascript"></script>
<link href="/nanogallery/css/nanogallery.min.css" rel="stylesheet" type="text/css"/>
<script src="nanogallery/jquery.nanogallery.min.js" type="text/javascript"></script>
<script type="text/javascript">var webcam = "sbsh03";
var filename = "sbsh03";
var resolution = "320";
var piwik_id = "21";
var minsize = "100";
var nextprevious = "1";
var showdate = "1";
var download = "1";
var fitborders = "0";
var dzf = "0.9";
var logo_l = "sbsh.png";</script>
<script src="/js/seadragon-min.js" type="text/javascript"></script>
<script src="/js/aws_processor.js" type="text/javascript"></script>
</head>
<body>
<div id="wrapper">
<div id="navBar"></div>
<div id="container">
<noscript>
<h2>Sie benÃ¶tigen Javascript, um diese Seite zu betrachten</h2>
Bitte aktivieren Sie Javascript in den Einstellungen Ihres Browsers.<br/><br/>
Wenn Sie Microsoft Internet Explorer 6 oder hÃ¶her verwenden, aktivieren Sie Javascript indem Sie im MenÃ¼ "Extras" den Eintrag "Internetoptionen" aufrufen. Hier wÃ¤hlen Sie die Reiterkarte "Sicherheit". Unter "Stufe Anpassen" aktivieren Sie das KontrollkÃ¤stchen bei "Scripting" / "Active Scripting".<br/><br/>
Wenn Sie Firefox verwenden, aktivieren Sie Javascript indem Sie im MenÃ¼ "Extras" den Eintrag "Einstellungen" aufrufen. Hier wÃ¤hlen Sie "Inhalt" und aktivieren das KontrollkÃ¤stchen "Javascript aktivieren". <br/><br/>
</noscript>
</div>
</div>
</body>
</html>
