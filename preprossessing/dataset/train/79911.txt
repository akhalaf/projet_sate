<!DOCTYPE html>
<html>
<head>
<base href="https://advodka.com/"/>
<meta charset="utf-8"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<title>advODKA.com</title>
<link href="static/front.less.1563255782.css" rel="stylesheet" type="text/css"/>
<meta content="1610642602.5AQfkeob7XXabtpo0gzG" name="csrf-token"/>
<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<div class="main-pause-overlay">
<div class="main-pause-overlay__row">
<img class="main-pause-overlay__image" src="static/images/logo_black.png"/>
<div class="main-pause-overlay__title">Работа сервиса приостановлена.</div>
<div class="main-pause-overlay__text">Спасибо, что были с нами!</div>
</div>
</div>
</body>
</html>
