<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="_csrf" name="csrf-param"/>
<meta content="48hfzGXHY1hIsPTuFnrejh6mZ_jj9OjOE6lYmLZqZl3WkAeYUaJWCDjivax7C-bqVMsxza-Vj6Ng3SLT5w5VPA==" name="csrf-token"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="always" name="referrer"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="https://yiifrontend.p3k.hu/favicons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://yiifrontend.p3k.hu/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://yiifrontend.p3k.hu/favicons/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="https://yiifrontend.p3k.hu/favicons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="https://yiifrontend.p3k.hu/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://yiifrontend.p3k.hu/favicons/manifest.json" rel="manifest"/>
<link color="#406d97" href="https://yiifrontend.p3k.hu/favicons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css"/>
<title>Startlap - Belépés</title>
<link href="https://yiifrontend.p3k.hu/css/auth.css?1601552541" rel="stylesheet"/>
<script src="https://startlogin.hu/auth/js/keycloak.js"></script> <script src="https://cts.p24.hu/service/js/startlap/useraccount/" type="text/javascript"></script>
<script type="text/javascript">
        var tagging = null;
        try {
            tagging = new Tagging();
            tagging.render('head');
        } catch (err) {
            if(tagging) tagging.log(err);
        }
    </script>
</head>
<body>
<div class="check-login"></div>
<script src="https://yiifrontend.p3k.hu/js/sso_check_login.js?1601552541" type="text/javascript"></script>
<img class="hsts-img" src="https://www.startlap.hu/img/hsts.gif"/>
<script type="text/javascript">
    try {
        tagging.render('body-start');
    } catch (err) {
        if(tagging) tagging.log(err);
    }
</script>
<script async="async" id="facebook-jssdk" src="//connect.facebook.net/hu_HU/sdk.js" type="text/javascript"></script>
<div class="blur-bg"></div>
<div class="content login">
<div class="container">
<div class="row first">
<div class="col-sm-12">
<a class="logo center-block" href="/"></a>
</div>
</div>
<div class="row second">
<div class="col-sm-12">
<div class="box center-block clearfix">
<div class="title">Belépés <strong>Startlap</strong> felhasználói adatokkal</div>
<form method="post">
<div class="container-fluid">
<div class="row">
<div class="form-group col-sm-12 text-input-container">
<input class="form-control text-input email" name="LoginForm[email]" placeholder="E-mail cím" type="text" value=""/>
<span class="fa fa-at form-control-feedback"></span>
<span class="help-block"></span>
</div>
</div>
<div class="row">
<div class="form-group col-sm-12 text-input-container">
<input class="form-control text-input" name="LoginForm[password]" placeholder="Jelszó" type="password" value=""/>
<span class="fa fa-key form-control-feedback"></span>
<span class="help-block"></span>
</div>
</div>
<div class="row">
<div class="form-group col-sm-6">
</div>
<div class="form-group col-sm-6">
<input name="_csrf" type="hidden" value="48hfzGXHY1hIsPTuFnrejh6mZ_jj9OjOE6lYmLZqZl3WkAeYUaJWCDjivax7C-bqVMsxza-Vj6Ng3SLT5w5VPA=="/>
<input class="btn submit-btn pull-right" type="submit" value="Belépés"/>
</div>
</div>
<div class="row">
<div class="form-group col-sm-6 col-sm-offset-6">
<a class="pw-reset-link pull-right" href="#">Elfelejtettem a jelszavam!</a>
</div>
</div>
</div>
</form>
</div>
</div>
<a class="fb-login" data-csrf="48hfzGXHY1hIsPTuFnrejh6mZ_jj9OjOE6lYmLZqZl3WkAeYUaJWCDjivax7C-bqVMsxza-Vj6Ng3SLT5w5VPA==" href="#"><span class="fa fa-facebook-square"></span><img class="preloader" src="https://yiifrontend.p3k.hu/img/preloader.gif?1"/><span class="text">Belépnél a Facebook fiókoddal?</span></a>
</div>
</div>
</div>
<div class="modal fade" id="jelszoemlekezteto" role="dialog">
<div class="modal-dialog">
<div class="modal-content clearfix">
<div class="modal-header">
<button class="close" data-dismiss="modal" type="button">×</button>
<h4 class="modal-title" id="modal-title">JELSZÓ EMLÉKEZTETŐ</h4>
</div>
<div class="modal-body">
<div class="text-left" role="dialog">
<p class="text-center">A megadott e-mail címre elküldtük a jelszavad módosításához szükséges teendőket.</p>
</div>
<div class="text-center" id="feedback" style="display: none;">
<h4 class="modal-title">Kérésedet rögzítettük, melyről e-mailben megerősítést küldtünk. <br/><br/>Köszönjük!</h4>
</div>
</div>
</div>
</div>
</div>
<div class="modal-backdrop fade"></div><script src="https://yiifrontend.p3k.hu/js/helpers.min.js?1601552542"></script>
<script src="https://yiifrontend.p3k.hu/js/auth.min.js?1601552542"></script>
<script async="async" src="https://yiifrontend.p3k.hu/js/facebook.min.js?1601552542"></script><script type="text/javascript">
    try {
        tagging.render('body-end-adv');
    } catch (err) {
        if(tagging) tagging.log(err);
    }
    try {
        tagging.render('body-end');
    } catch (err) {
        if(tagging) tagging.log(err);
    }
</script>
</body>
</html>