<!--//
https://youtu.be/cU8rjBmjqW4
//--><!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]--><!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]--><!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<meta content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" name="viewport"/>
<title>Work — Alaa Mendili</title>
<meta content="Creative Direction, Experiences and Product Design." name="description"/>
<meta content="Alaa Mendili" name="author"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@almendili" name="twitter:site"/>
<meta content="Alaa Mendili" name="twitter:title"/>
<meta content="Alaa Mendili" name="twitter:description"/>
<meta content="http://www.a-l-a-a.com/images/social.png" name="twitter:image:src"/>
<meta content="Alaa Mendili" property="og:title"/>
<meta content="Creative Direction, Experiences and Product Design." property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="Alaa Mendili" property="og:site_name"/>
<meta content="856080462" property="fb:admins"/>
<meta content="http://a-l-a-a.com" property="og:url"/>
<meta content="http://www.a-l-a-a.com/images/social.png" property="og:image"/>
<link href="/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/icons/manifest.json" rel="manifest"/>
<link color="#2acdc1" href="/icons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/icons/favicon.ico" rel="shortcut icon"/>
<meta content="/icons/browserconfig.xml" name="msapplication-config"/>
<meta content="#2acdc1" name="theme-color"/>
<meta content="60cdb550f9db7a722b5e508ff1761454" name="p:domain_verify"/>
<link href="/css/style-e5603372.css" rel="stylesheet"/>
<link href="/css/tablet-large-77d0dc3c.css" media="(max-width:1300px)" rel="stylesheet"/>
<link href="/css/tablet-small-f5fe8f48.css" media="(max-width:980px)" rel="stylesheet"/>
<link href="/css/mobile-large-83cb4519.css" media="(max-width:740px)" rel="stylesheet"/>
<link href="/css/mobile-a858c74a.css" media="(max-width:500px)" rel="stylesheet"/>
<script src="/js/libs/modernizr-2.6.2.min.js"></script>
</head>
<body id="work">
<div id="container">
<header>
<div id="header-left">
<a href="/">
<div id="logo"><img src="/images/logo-small.png"/></div>
<div id="logo-smiley"><img src="/images/logo-smiley.png"/></div>
</a>
</div>
<div id="header-mobile">
<div id="nav-icon">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</div>
<div id="header-right">
<div class="line-holder">
<div class="center-content">
<div class="center-content-inner">
<hr/>
</div>
</div>
</div>
<div class="menu-holder">
<div class="menu-item">
<a class="nav-link " href="/profile">
<div class="center-content">
<div class="center-content-inner">
<div class="copy">
                                        Profile
                                        <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a class="nav-link selected" href="/projects">
<div class="center-content">
<div class="center-content-inner">
<div class="copy">
                                        Projects
                                        <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a class="nav-link " href="/talks">
<div class="center-content">
<div class="center-content-inner">
<div class="copy">
                                        Talks
                                        <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a class="nav-link " href="/amateur-hour">
<div class="center-content">
<div class="center-content-inner">
<div class="copy">
                                        Amateur Hour
                                        <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a href="#" id="menu-contact">
<div class="center-content">
<div class="center-content-inner">
<div class="copy">
                                        Contact
                                        <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</div>
</header>
<div class="clearfix" id="content">
<section class="content-section content-section-margin clearfix">
<div class="content-section-grid clearfix" id="work-content">
<div class="item">
<a class="nav-link" href="/projects/stanley-piano">
<img alt="Stanley Piano" src="/images/projects/stanley-piano/stanley-thumb.jpg" title="Stanley Piano"/>
<div class="covervid-wrapper">
<video autoplay="" class="covervid-video" loop="" muted="" poster="/images/projects/stanley-piano/stanley-thumb.jpg">
<source src="/images/projects/stanley-piano/stanley_loop.mp4" type="video/mp4">
<source src="/images/projects/stanley-piano/stanley_loop.webm" type="video/webm">
<source src="/images/projects/stanley-piano/stanley_loop.ogv" type="video/ogv">
<img alt="Stanley Piano" src="/images/projects/stanley-piano/stanley-thumb.jpg" title="Stanley Piano"/>
</source></source></source></video>
</div>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Stanley Piano</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-double-third">
<a class="nav-link" href="/projects/field-project">
<img alt="Field Project" src="/images/projects/field-project/field-thumb.gif" title="Field Project"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Field Project</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-third">
<a class="nav-link" href="/projects/bling-vr">
<img alt="Bling VR" src="/images/projects/blingvr/blingvr-thumb.jpg" title="Bling VR"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Bling VR</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/protobooth">
<img alt="Protobooth" src="/images/projects/protobooth/protobooth-social.jpg" title="Protobooth"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Protobooth</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/goodbye-friends">
<img alt="Goodbye Friends" src="/images/projects/goodbye-friends/goodbye-thumb.jpg" title="Goodbye Friends"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Goodbye Friends</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item">
<a class="nav-link" href="/projects/pingpongfm">
<img alt="PingPong.FM" src="/images/projects/pingpongfm/pingpongfm-thumb.jpg" title="PingPong.FM"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>PingPong.FM</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-third">
<a class="nav-link" href="/projects/tinytron">
<img alt="Tinytron" src="/images/projects/tinytron/tinytron-thumb.jpg" title="Tinytron"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Tinytron</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-third">
<a class="nav-link" href="/projects/enigmatic">
<img alt="The Enigmatic" src="/images/projects/enigmatic/enigmatic-thumb.jpg" title="The Enigmatic"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>The Enigmatic</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-third">
<a class="nav-link" href="/projects/mimo">
<img alt="Mimo" src="/images/projects/mimo/mimo-howitworks.jpg" title="Mimo"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Mimo</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-third">
<a class="nav-link" href="/projects/earthling-month">
<img alt="Earthling Month" src="/images/projects/earthling-month/earthlingmonth-thumb.jpg" title="Earthling Month"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Earthling Month</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-double-third">
<a class="nav-link" href="/projects/dodge-journey">
<img alt="Dodge Journey" src="/images/projects/dodge-journey/dodgejourney-thumb.jpg" title="Dodge Journey"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Dodge Journey</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/internet-explorer">
<img alt="Internet Explorer" src="/images/projects/internet-explorer/internetexplorer-thumb.jpg" title="Internet Explorer"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Internet Explorer</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/in-kibera">
<img alt="In Kibera" src="/images/projects/inkibera/inkibera-thumb.jpg" title="In Kibera"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>In Kibera</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/htc-vive">
<img alt="HTC Vive" src="/images/projects/htcvive/htcvive-thumb.jpg" title="HTC Vive"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>HTC Vive</h1>
</div>
<div class="item-border"></div>
</a>
</div>
<div class="item item-half">
<a class="nav-link" href="/projects/brooks-running">
<img alt="Brooks Running" src="/images/projects/brooks-running/brookssnap.gif" title="Brooks Running"/>
<div class="item-overlay"></div>
<div class="item-content">
<h1>Brooks Running</h1>
</div>
<div class="item-border"></div>
</a>
</div>
</div>
</section>
<section class="content-section content-section-margin clearfix" id="work-archive">
<div class="content-section-grid clearfix">
<div class="item item-double-third">
<h1>Archive</h1>
<p>
				I have spent the past 20 years making sh*t on the internet. I’m pretty pretty prettty proud of the things I’ve made.  I will keep digging through the old hard drives and hopefully find some <a href="http://media.gifshop.tv/30/E565H6QD.gif" target="_blank">treasures</a>.
    		</p>
</div>
</div>
<div class="content-section-grid clearfix">
<nav class="list" id="work-archive-list">
<ul>
<li>
<a href="https://www.laviniavago.com/" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Lavinia Vago</div>
<div class="item item-double-third list-subtitle">Branding / Web</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2017</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/195768959" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Emoji Center</div>
<div class="item item-double-third list-subtitle">Experience Design / Event</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2017</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://damntheweather.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Damn The Weather</div>
<div class="item item-double-third list-subtitle">Branding / Web</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2016</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://elmcoffeeroasters.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Elm Coffee Roasters</div>
<div class="item item-double-third list-subtitle">Branding / Web</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2015</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://biancoartists.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Bianco Artists</div>
<div class="item item-double-third list-subtitle">Web</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2015</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://tc15.tableau.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Tableau Conference 2015</div>
<div class="item item-double-third list-subtitle">Branding / Web / Event</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2015</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vizable.tableau.com/" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Vizable</div>
<div class="item item-double-third list-subtitle">Branding / Web</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2015</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://rockpaperscissors.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Rock Paper Scissors</div>
<div class="item item-double-third list-subtitle">Web</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2015</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://tc14.tableau.com" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Tableau Conference 2014</div>
<div class="item item-double-third list-subtitle">Branding / Web / Event</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2014</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/70798916" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Telemundo La Voz Kids</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2013</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/134635877" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Sierra Nevada Brewing Company</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2012</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://davidmikula.com/projects/xprsso/" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">XprssO</div>
<div class="item item-double-third list-subtitle">Digital Product</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2011</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/32229131" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Ask Away</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Digital Kitchen</div>
<div class="item item-sixth list-date">2011</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/19883724" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Robyn - We Dance to the Beat</div>
<div class="item item-double-third list-subtitle">Interactive Music Video</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2010</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/20730957" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Yoopa</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Sid Lee</div>
<div class="item item-sixth list-date">2010</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/32105066" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Google Nexus Contraptions</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2010</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/146019371" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">56 Sage Street</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2009</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/15662133" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Asylum 626</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2009</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://www.youtube.com/watch?v=IxRnHXIjI_I" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Axe 100 Girls</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2009</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/11211073" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Drop Everything for Love</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2009</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/14930480" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Nokia Somebody Else's Phone</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2008</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/10509398" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Magnum Manor</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2008</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/10506998" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Tastebuddy Land</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">B-Reel</div>
<div class="item item-sixth list-date">2008</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/8987772" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Rated Rookies</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2008</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://vimeo.com/10510318" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">MGM City Center</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Sid Lee</div>
<div class="item item-sixth list-date">2007</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="http://www.alaa.se/offline/akufen/" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Akufen</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2007</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://thefwa.com/cases/imbroglio" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Imbroglio</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2006</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
<li>
<a href="https://thefwa.com/cases/furax" target="_blank">
<div class="content-section-grid clearfix">
<div class="item list-title">Furax</div>
<div class="item item-double-third list-subtitle">Experience Design</div>
<div class="item item-sixth list-subtitle">Independent</div>
<div class="item item-sixth list-date">2005</div>
</div>
<div class="border-bottom"></div>
</a>
</li>
</ul>
</nav>
</div>
</section>
</div>
<div id="footer-holder">
<section id="contact">
<div class="center-content" id="contact-content">
<div class="center-content-inner">
<div class="content-section content-section-margin">
<div class="content-section-grid clearfix">
<div id="contact-holder">
<h1>
<span class="highlight-blue">
                                            Get In Touch
                                        </span>
</h1>
<h2>
<span class="highlight-blue">
                                            Let's be friends and talk about Curb Your Enthusiam.                                        </span>
</h2>
<a class="button button-dark" href="mailto:me@a-l-a-a.com?subject=Let's%20Talk%20About%20the%20Internet">
<div class="bottom"></div>
<div class="top">
<div class="label">Hit Me Up!</div>
<div class="button-border button-border-left"></div>
<div class="button-border button-border-top"></div>
<div class="button-border button-border-right"></div>
<div class="button-border button-border-bottom"></div>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div id="footer-left">
<div class="line-holder">
<div class="center-content">
<div class="center-content-inner">
<hr/>
</div>
</div>
</div>
<div class="menu-holder">
<div class="menu-item">
<a href="http://www.twitter.com/almendili" target="_blank">
<div class="center-content">
<div class="center-content-inner">
<div class="icon">
<div class="bottom"><img src="/images/twitter.png"/></div>
<div class="top"><img src="/images/twitter.png"/></div>
</div>
<div class="copy">
                                            Twitter
                                            <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a href="http://www.instagram.com/almendili" target="_blank">
<div class="center-content">
<div class="center-content-inner">
<div class="icon">
<div class="bottom"><img src="/images/instagram.png"/></div>
<div class="top"><img src="/images/instagram.png"/></div>
</div>
<div class="copy">
                                            Instagram
                                            <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<div class="menu-item">
<a href="http://www.linkedin.com/in/almendili" target="_blank">
<div class="center-content">
<div class="center-content-inner">
<div class="icon">
<div class="bottom"><img src="/images/linkedin.png"/></div>
<div class="top"><img src="/images/linkedin.png"/></div>
</div>
<div class="copy">
                                            LinkedIn
                                            <div class="underline"></div>
</div>
</div>
</div>
</a>
</div>
<!--div class="menu-item">
                            <a href="http://www.facebook.com/almendili" target="_blank">
                                <div class="center-content">
                                    <div class="center-content-inner">
                                        <div class="icon">
                                            <div class="bottom"><img src="/images/facebook.png" /></div>
                                            <div class="top"><img src="/images/facebook.png" /></div>
                                        </div>
                                        <div class="copy">
                                            Facebook
                                            <div class="underline"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div-->
</div>
</div>
<div id="footer-right">
<a href="#" id="footer-back">
<div id="footer-back-icon">
<img alt="Back to Top" src="/images/arrow-top.png" title="Back to Top"/>
</div>
</a>
</div>
</footer>
</div>
<div id="overlay"></div>
</div>
<!--[if lte IE 8]>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
    <![endif]-->
<!--[if (!IE)|(gt IE 8)]><!-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/libs/jquery-2.0.0.min.js"><\/script>')</script>
<!--<![endif]-->
<script src="/js/lib-e3289615.js"></script>
<script src="/js/main-06ef2ffe.js"></script>
<script src="/js/work-ce6e2361.js"></script>
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-82828643-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
<!--//
you've made this far. here's a little something for you. xo
https://open.spotify.com/user/alaam/playlist/1TKMzT9PUBWXf4Fpjc4GVC
//-->