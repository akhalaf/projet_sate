<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ASAP Sports Transcripts - Golf - 2011 - CHILDREN'S MIRACLE NETWORK HOSPITALS CLASSIC - October 22 - Tom Pernice, Jr.</title>
<meta content="slbHbc0lYYv6O/pD/jlxKm5goi5FliNVMyoNlrlLIgU=" name="verify-v1"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="asapsports, sports interviews, sports transcripts, sports interview, sports transcript, us open golf, us open tennis, wimbledon, masters golf, PGA, LPGA, NBA, NFL, WNBA, MLB, Nascar, indy racing, all star game, NCAA, cart racing, world series, super bowl, NHL, tennis, golf, USTA" name="keywords"/>
<meta content="ASAPSports FastScripts, a system using state-of-the-art technology created to produce verbatim FastScripts of press conferences and player/team interviews at sporting events around the globe." name="description"/>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<script src="functions.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="stm31.js" type="text/javascript"></script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1135978-1";
urchinTracker();
</script>
<script src="crawler.js" type="text/javascript">
/*
Text and/or Image Crawler Script �2009 John Davenport Scheuer
as first seen in http://www.dynamicdrive.com/forums/ username: jscheuer1
This Notice Must Remain for Legal Use
*/
</script>
<script type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="780">
<tr>
<td> <script async="" data-ad-client="ca-pub-1510223520953796" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<table border="0" cellpadding="0" cellspacing="0" width="780">
<tr>
<td background="images/header_int_secondary_bg.jpg">
<table align="right" border="0" cellpadding="3" cellspacing="2" style="margin-right: 10px;">
<tr>
<td><a href="http://www.asapsports.com">home</a></td>
<td><a href="jobs_new.php">jobs</a></td>
<td><a href="contact.php">contact us</a></td>
</tr>
</table> </td>
</tr>
<tr height="145">
<td background="images/header_int_bg.jpg">
<table border="0" cellpadding="0" cellspacing="0" width="780">
<tr>
<td rowspan="3" style="padding-left: 2em;" width="168"><a href="http://www.asapsports.com"><img border="0" height="90" src="images/logo_interior.png" width="125"/></a></td>
<td width="566">
<div id="buttonscontainer">
<ul>
<li><a href="captioning.php">products</a></li>
<li><a href="recent.php">recent interviews</a></li>
<li><a href="show_event.php">FastScripts archive</a></li>
<li><a href="about.php">about ASAP Sports</a></li>
</ul>
</div>
</td>
</tr>
<tr>
<td> <div align="right" style="margin: 32px 5px 5px 0;">
<div align="center"></div>
</div></td>
</tr>
</table>
</td>
</tr>
<tr class="searchbg" height="49">
<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding-left: 1em; font-size: 12px;" width="10%">Our Clients:</td>
<td width="53%">
<!--    <div class="marquee" id="mycrawler">
Those confounded friars dully buzz that faltering jay. An appraising tongue acutely causes our courageous hogs. Their fitting submarines deftly break your approving improvisations. Her downcast taxonomies actually box up those disgusted turtles.
</div>

<script type="text/javascript">
marqueeInit({
	uniqueid: 'mycrawler',
	style: {
		'padding': '5px',
		'width': '415px',
		'background': '#d4dee0',
		'border': 'none'
	},
	inc: 8, //speed - pixel increment for each iteration of this marquee's movement
	mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
	moveatleast: 4,
	neutral: 150,
	savedirection: true
});
</script>
-->
<div class="marquee" id="mycrawler2" style="margin-left: .5em;">
<img src="images/NBA_logo.gif"/> <img src="images/MLB_logo.gif"/> <img src="images/ATP_logo.gif"/> <img src="images/NHL_logo.gif"/> <img src="images/PGA_logo.gif"/> <img src="images/NCAA_logo.gif"/> <img src="images/NFL_logo.gif"/><img src="images/CBS_logo.gif"/><img src="images/NHRA_logo.gif"/><img src="images/Indy_logo.gif"/><img src="images/Nascar_logo.gif"/><img src="images/MLS_logo.gif"/><img src="images/WNBA_logo.gif"/><img src="images/Wimbledon_logo.gif"/><img src="images/PGA_logo.gif"/><img src="images/EuroTour_logo.gif"/><img src="images/LPGA_logo.gif"/><img src="images/USGA_logo.gif"/><img src="images/USTA_logo.gif"/>
</div>
<script type="text/javascript">
marqueeInit({
	uniqueid: 'mycrawler2',
	style: {
		'padding': '1px',
		'width': '430px',
		'height': '40px'
	},
	inc: 5, //speed - pixel increment for each iteration of this marquee's movement
	mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
	moveatleast: 2,
	neutral: 150,
	addDelay: 40,
	savedirection: true
});
</script>
</td>
<td width="30%">
<script>
  (function() {
    var cx = '006965992139505998230:fxp4nfq_bmq';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="780">
<tr>
<td bgcolor="#dadada" width="1"><img alt="" height="1" src="images/spacer.gif" width="1"/></td>
<td bgcolor="#fafafa" valign="top" width="190"><table bgcolor="#fafafa" border="0" cellpadding="0" cellspacing="0" style="background:url(images/leftside_bg.png) no-repeat top;" width="100%">
<tr>
<td align="center" class="subtitlelarge">Browse by Sport</td>
</tr>
<tr>
<td align="center"><script language="JavaScript1.2" src="fastscripts-sports.js" type="text/javascript"></script></td>
</tr>
<tr>
<td><img height="30" src="images/spacer.gif" width="1"/></td>
</tr>
<tr bgcolor="#efefef">
<td style="margin: 1em 0;"><table align="center" border="0" cellpadding="2" cellspacing="4">
<tr>
<td class="subtitlemed">Find us on</td>
<td><a href="http://www.facebook.com/pages/ASAP-Sports/122368829031" target="_blank"><img alt="ASAP sports on Facebook" border="0" height="24" src="images/fb_icon.gif" width="25"/></a></td>
<td><a href="http://twitter.com/asapsports" target="_blank"><img alt="ASAP sports on Twitter" border="0" height="25" src="images/twitter_icon.png" width="25"/></a></td>
</tr>
</table></td>
</tr>
<tr>
<td style="margin: 1em 0;"><img height="20" src="images/spacer.gif" width="1"/></td>
</tr>
<tr bgcolor="#efefef">
<td><table align="center" border="0" cellpadding="2" cellspacing="4">
<tr>
<td><a href="rss12.php"><img alt="ASAP Sports RSS" border="0" height="24" src="images/rss_icon.png" width="24"/></a></td>
<td class="subtitlemed"><a href="rss12.php">Subscribe to RSS</a></td>
</tr>
</table></td>
</tr>
<tr>
<td><img height="20" src="images/spacer.gif" width="1"/></td>
</tr>
<tr bgcolor="#efefef">
<td><table align="center" border="0" cellpadding="2" cellspacing="4">
<tr>
<td class="subtitlemed"><a href="http://www.asaptext.com">Click to go to<br/>
          Asaptext.com</a></td>
<td><a href="http://www.asaptext.com"><img alt="ASAPtext.com" border="0" height="32" src="images/arrow_icon.gif" width="32"/></a></td>
</tr>
</table></td>
</tr>
<tr>
<td><img height="20" src="images/spacer.gif" width="1"/></td>
</tr>
<tr bgcolor="#efefef">
<td><table align="center" border="0" cellpadding="2" cellspacing="4">
<tr>
<td><img alt="ASAP Sports e-Brochure" border="0" height="27" onclick="MM_openBrWindow('slide2.html','','width=800,height=680')" src="images/asap_logo_small.gif" style="cursor:hand" width="38"/></td>
<td class="subtitlemed">
<a href="#"><span onclick="MM_openBrWindow('slide2.html','','width=800,height=680')" style="cursor:hand">View our<br/>
          e-Brochure</span></a>
</td>
</tr>
</table></td>
</tr>
<tr>
<td><img height="50" src="images/spacer.gif" width="1"/></td>
</tr>
</table>
</td>
<td bgcolor="#dadada" width="1"><img alt="" height="1" src="images/spacer.gif" width="1"/></td>
<td style="padding: 10px;" valign="top">
<h1><a href="http://www.asapsports.com/show_events.php?category=4&amp;year=2011&amp;title=CHILDREN%27S+MIRACLE+NETWORK+HOSPITALS+CLASSIC">CHILDREN'S MIRACLE NETWORK HOSPITALS CLASSIC</a></h1>
<br/>
<h2>October 22, 2011</h2>
<br/>
<h3><a href="http://www.asapsports.com/show_player.php?id=11403">Tom Pernice, Jr.</a></h3><br/>
<i>LAKE BUENA VISTA, FLORIDA</i><br/><br/>
<b> Q. Tom, you got it going out there and a little tough finish, but a nice way to finish it, especially with that lengthy birdie putt at 18?</b><br/>
 TOM PERNICE, JR.: That was a big surprise. I pushed my 5-iron over there and I was trying to stay right of the pin a little bit, but I pushed it and wanted to putt back up the hill and had a 60-footer and it happened to go in. <br/>
 Yeah, I was playing good. Some of the shorter holes there I had a stretch where I took advantage of them and had some good opportunities for them, and did. And just hit a few loose tee shots coming in really. I mean the loose tee shot on 17 obviously was the big one. Just didn't trust a little bit and came out of it.<br/>
<br/>
<b> Q. How was it out there playing with Luke and Webb as they're battling for No. 1 on the Money List?</b><br/>
 TOM PERNICE, JR.: You know, I thought it was great. They're both pretty relaxed and they're both playing well, and it's always nice to be out there and be somewhat in the mix a little bit. I enjoyed it, Webb's a close friend of mine and just couldn't be happier for a guy that's had a year as great as his. <br/>
 So it was nice. I don't know Luke real well, but just a little bit, but it was very enjoyable, and they're playing well, and I think they're enjoying the challenge. <br/>
<br/>
<b> Q. I know how competitive you are. You're playing a lot of golf on the Champions Tour. You're trying to play out here on the regular tour as much as you can. How much would it mean for you to go ahead and have a good finish tomorrow and get yourself exempt on the regular TOUR, too?</b><br/>
 TOM PERNICE, JR.: Well, you know, that's what I've been trying to do here in the fall. If I could just play good enough and have some good weeks, you know, that might happen. I'm not stressed about it. I'd like to put myself there to have a good week and I've got it there, so we'll go out tomorrow and try to stay relaxed and see what happens.<br/>
<br/>
<b> Q. Well played so far. Good luck tomorrow.</b><br/>
 TOM PERNICE, JR.: Thanks. Appreciate it. <br/>
<strong></strong><br/>
<strong> FastScripts Transcript by ASAP Sports</strong><span style="font-weight:normal; "> </span><br/>
<br/>
<br/>
<br/>
<br/>
</td>
<td bgcolor="#dadada" width="1"><img alt="" height="1" src="images/spacer.gif" width="1"/></td>
</tr>
<tr>
<td bgcolor="#dadada" colspan="5"><img alt="" height="1" src="images/spacer.gif" width="1"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="68">
<td align="center" style="background: url('images/footer_links_bg.jpg'); background-repeat: no-repeat; background-position:center;">
<span class="darkgraybold">
<a href="about.php">About ASAP Sports</a> • 
        <a href="show_event.php">FastScripts Archive</a> • 
        <a href="recent.php">Recent Interviews</a> • 
        <a href="captioning.php">Captioning</a> • 
		<a href="upcoming.php">Upcoming Events</a> • 
        <a href="contact.php">Contact Us</a>
</span><br/>
<span class="darkgray">
<a href="about-fast-scripts.php">FastScripts</a> | 
        <a href="about-events-covered.php">Events Covered</a> | 
        <a href="about-our-clients.php">Our Clients</a> | 
        <a href="about-other-services.php">Other Services</a> | 
        <a href="about-news.php">ASAP in the News</a> | 
        <a href="sitemap.php">Site Map</a> | 
        <a href="jobs.php">Job Opportunities</a> | 
        <a href="links.php">Links</a>
</span>
</td>
</tr>
<tr height="20">
<td align="right" style="background: url('images/footer_copyright_bg.jpg'); background-repeat: no-repeat; background-position:center;">
<div class="whitesmall"><b>ASAP Sports, Inc.</b> | T: 1.212 385 0297 </div>
</td>
</tr>
</table></td>
</tr>
</table>
<!-- This page was generated in 0.008324146270752, used 3 MySQL queries-->
</body>
</html>
