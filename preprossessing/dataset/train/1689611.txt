<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>XhtmlJunction Sitemap - Links of All XhtmlJunction Pages</title>
<meta content="XhtmlJunction sitemap: Navigate through the whole site. Find all links of our services and other important pages at a single place." name="description"/>
<link href="common/images/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="common/images/favicon.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="common/css/style.css" rel="stylesheet"/>
<link href="common/css/responsive.css" rel="stylesheet"/>
<link href="common/css/jquery.mmenu.all.css" rel="stylesheet"/>
<link href="common/css/font-awesome.css" rel="stylesheet"/>
<link href="https://www.xhtmljunction.com/sitemap.html" rel="canonical"/>
<meta content="index, follow" name="robots"/>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new 
Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

   ga('create', 'UA-8679159-5', 'auto');
   ga('send', 'pageview');

</script>
<!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!--wrapper -->
<section id="wrapper">
<!--header part-->
<!--header part-->
<header id="header-part">
<div class="top_row">
Limited time offer  Get 30% OFF, Discount code: <strong>SAVE30</strong>
</div>
<a class="mobilemenu" href="#menu"><span></span></a>
<a class="close" href="#"></a>
<span itemscope="" itemtype="https://schema.org/Organization"> <a class="logo deskl" href="https://www.xhtmljunction.com/" itemprop="url"><img alt="PSD to HTML - XhtmlJunction" itemprop="logo" src="https://www.xhtmljunction.com/common/images/logo.png"/></a> <a class="logo mobl" href="https://www.xhtmljunction.com/" itemprop="url"><img alt="PSD to HTML - XhtmlJunction" itemprop="logo" src="https://www.xhtmljunction.com/common/images/logo.png"/></a> </span> <span class="call_number"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:8189660495">818-966-0495</a></span>
<!--<div class="diwali-banner">
	<a href="pricing.html"><img src="common/images/diwali_wi.png" alt="" /></a>
</div> -->
<a class="support_btn" href="https://www.xhtmljunction.com/pricing.html" onclick="ga('send', 'event', 'Order Header Button', 'Clicked on Order Icon', 'Land on Order Page');">Order Now</a>
<!--nav bar-->
<div class="nav-bar">
<nav id="menu">
<ul>
<li class="serv_mob"><span>Services <i class="fa fa-angle-down"></i></span>
<ul>
<li><a href="https://www.xhtmljunction.com/html5-banners.html">Html5 Banners  </a></li>
<li><a href="https://www.xhtmljunction.com/hire-dedicated-developers.html">Hire Dedicated Developers </a></li>
<li><a href="https://www.xhtmljunction.com/sketch-to-html.html">Sketch To Html</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-html.html">HTML5/CSS3 Coding</a></li>
<li><a href="https://www.xhtmljunction.com/test-automation.html">Test Automation</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-bootstrap.html">Bootstrap Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-newsletter.html">Email Coding</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-angular.html">Psd To Angular</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-wordpress.html">Wordpress Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-magento.html">Magento Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-shopify-theme-development.html">Shopify Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-mobile.html">Mobile Theming</a></li>
<li><a href="https://www.xhtmljunction.com/responsive.html">Existing Site To Responsive</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-mobile-apps.html">Mobile Apps</a></li>
</ul>
</li>
<li><a href="https://www.xhtmljunction.com/feature-index.html">Features</a></li>
<li><a href="https://www.xhtmljunction.com/portfolio.html">portfolio </a></li>
<li><a href="https://www.xhtmljunction.com/about.html">about us </a></li>
<li><a href="https://www.xhtmljunction.com/contact.html">contact us </a></li>
</ul>
</nav>
</div>
<!--nav bar-->
</header>
<!--header part--> <!--header part-->
<!--content part-->
<section id="content-part">
<div class="heading-bar">
<div class="wrapper">
<h1>Sitemap</h1>
<small>List of all the Pages</small>
<ul class="bradcamp">
<li><a href="#">Home</a><cite>/</cite></li>
<li><a href="#">Pages</a><cite>/</cite></li>
<li>FAQs</li>
</ul>
</div>
</div>
<!--sitemap bar-->
<article class="sitemap-bar">
<div class="wrapper">
<div class="col-4">
<h2>Pages</h2>
<ul>
<li> <a><i class="fa fa-angle-right"></i> Services</a>
<ul>
<li><a href="https://www.xhtmljunction.com/html5-banners.html">Html5 Banners </a></li>
<li><a href="https://www.xhtmljunction.com/hire-dedicated-developers.html">Hire Dedicated Developers </a></li>
<li><a href="https://www.xhtmljunction.com/sketch-to-html.html">Sketch To Html</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-angular.html">PSD To Angular</a></li>
<li><a href="https://www.xhtmljunction.com/test-automation.html">Test Automation</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-html.html">HTML5/CSS3 Coding</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-bootstrap.html">Bootstrap Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-newsletter.html">Email Coding</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-wordpress.html">Wordpress Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-magento.html">Magento Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-shopify-theme-development.html">Shopify Theming</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-mobile.html">Mobile Theming</a></li>
<li><a href="https://www.xhtmljunction.com/responsive.html">Existing Site To Responsive</a></li>
<li><a href="https://www.xhtmljunction.com/psd-to-mobile-apps.html">Mobile Apps</a></li>
</ul>
</li>
<li><a href="feature-index.html"><i class="fa fa-angle-right"></i> Features</a></li>
<li><a href="portfolio.html"><i class="fa fa-angle-right"></i> portfolio </a></li>
<li><a href="about.html"><i class="fa fa-angle-right"></i> about us </a></li>
<li><a href="contact.html"><i class="fa fa-angle-right"></i> contact us </a></li>
<li> <a href="pricing.html"><i class="fa fa-angle-right"></i> Order Now</a>
<ul>
<li><a href="https://www.xhtmljunction.com/order-banner.html">HTML5 Banners Order </a></li>
<li><a href="https://www.xhtmljunction.com/order-html5.html">HTML5/CSS3 Coding Order </a></li>
<li><a href="https://www.xhtmljunction.com/order-bootstrap.html">Bootstrap Theming Order</a></li>
<li><a href="https://www.xhtmljunction.com/order-email.html">Email Coding Order</a></li>
<li><a href="https://www.xhtmljunction.com/order-wp.html">Wordpress Theming Order</a></li>
<li><a href="https://www.xhtmljunction.com/order-magento.html">Magento Theming Order</a></li>
<li><a href="https://www.xhtmljunction.com/order-shopify.html">Shopify Theming Order</a></li>
<li><a href="https://www.xhtmljunction.com/order-mobile-theme.html">Mobile Theming Order</a></li>
<li><a href="https://www.xhtmljunction.com/get-a-quote.html">Mobile Apps Order</a></li>
<li><a href="https://www.xhtmljunction.com/get-a-quote.html">Existing Site To Responsive Order</a></li>
</ul>
</li>
<li><a href="https://www.xhtmljunction.com/blog"><i class="fa fa-angle-right"></i> Blog</a></li>
<li><a href="faq.html"><i class="fa fa-angle-right"></i> FAQ</a></li>
<li><a href="privacy-policy.html"><i class="fa fa-angle-right"></i> Privacy</a></li>
<li><a href="nda.html"><i class="fa fa-angle-right"></i> NDA</a></li>
<li><a href="terms-of-services.html"><i class="fa fa-angle-right"></i> Terms</a></li>
<li><a href="sitemap.html"><i class="fa fa-angle-right"></i> Sitemap</a></li>
</ul>
</div>
<div class="col-4" style="display:none">
<h2>Resources</h2>
</div>
<div class="clear"></div>
</div>
</article>
<!--sitemap bar-->
</section>
<!--content part-->
<!--footer part-->
<!--footer part-->
<footer id="footer-part">
<!--promo bar-->
<div class="promo-bar">
<div class="wrapper">
<div class="left">
<h3>Call us today at <a href="tel:8189660495">818-966-0495</a> or Email us at <a href="mailto:sales@xhtmljunction.com" onclick="ga('send', 'event', 'Support Email ID', 'Clicked on Support Email ID', 'Sent a Mail');">sales@xhtmljunction.com</a></h3>
<span>If you have a project in mind or if you are looking to find out more, then get in touch.</span> </div>
<a class="btn" href="https://www.xhtmljunction.com/get-a-quote.html" onclick="ga('send', 'event', 'Start a Project Link', 'Clicked on Start a Project Link', 'Land on Contact Page');">Get a Quote</a> </div>
</div>
<!--promo bar-->
<!--footer bar-->
<article class="footer-bar">
<div class="wrapper">
<div class="row">
<div class="col1">
<h2>XhtmlJunction</h2>
<p>We are a web development company in Oak Park, specializing in responsive front end coding and in custom theme development.</p>
<p class="center" itemscope=""><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-full"></i>   Rated 4.7 out of 5.0 for XHTMLJunction services by 10040+ Customers.</p>
<address>
<strong>Headquarters:</strong><br/>
          Oak Park, California, 91377<br/>
<br/>
<strong><u>USA:</u></strong> <a href="tel:8189660495">818-966-0495</a><br/>
<strong><u>Phone:</u></strong> <a href="tel:+919718199560" rel="nofollow">(+91) 971-819-9560</a><br/>
<strong><u>Email:</u></strong> <a class="mail" href="mailto:support@xhtmljunction.com" onclick="ga('send', 'event', 'Support Email ID', 'Clicked on Support Email ID', 'Sent a Mail');" rel="nofollow"><img alt="Email Us" src="common/images/email_img2.jpg"/></a>
</address>
</div>
<div class="col2">
<h5>Links</h5>
<ul class="list">
<li><a href="https://www.xhtmljunction.com/blog/">Blog</a></li>
<li><a href="https://www.xhtmljunction.com/faq.html">FAQ</a></li>
<li><a href="https://www.xhtmljunction.com/privacy-policy.html">Privacy</a></li>
<li><a href="https://www.xhtmljunction.com/nda.html">NDA</a></li>
<li><a href="https://www.xhtmljunction.com/terms-of-services.html">Terms</a></li>
<li><a href="https://www.xhtmljunction.com/sitemap.html">Sitemap</a></li>
</ul>
</div>
<div class="col3">
<h5>RECENT POSTS</h5>
<ul class="post" id="blog_list"></ul>
</div>
<div class="col4">
<div class="counts">
<div class="downloads"> <span class="num ">1,25,000</span> <span>PAGES CODED</span> </div>
<div class="downloads"> <span class="num ">10,648</span> <span>CLIENTS</span> </div>
</div>
<p><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</p>
<!--<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">-->
<div id="mc_embed_signup">
<!-- Current /// xhtmljunction.us13.list-manage.com/subscribe/post?u=1f5ec1224e549f7c6d455e893&amp;id=b548466b36 -->
<!-- XhtmlJunction.us14.list-manage.com/subscribe/post?u=985712fdda4387df8ad5162b3&amp;id=8e18241bd3 -->
<form action="//XhtmlJunction.us14.list-manage.com/subscribe/post?u=985712fdda4387df8ad5162b3&amp;id=db6fa2b600" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group">
<input class="required email" id="mce-EMAIL" name="EMAIL" type="email" value=""/>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div aria-hidden="true" style="position: absolute; left: -5000px;">
<input name="b_5e3bf99037ba1a11c3663936e_f81be5db30" tabindex="-1" type="text" value=""/>
</div>
<div class="clear">
<input class="button" id="mc-embedded-subscribe" name="subscribe" onclick="ga('send', 'event', 'Footer Subscription Section', 'Clicked on Go', 'Subscribed XJ')" type="submit" value="Subscribe"/>
</div>
</div>
</form>
</div>
<script src="https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js" type="text/javascript"></script>
<script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
<ul class="social">
<li><a href="https://www.facebook.com/XHTMLJunctionFans" target="_blank"> <i class="fa fa-facebook-f"></i> <span><strong>Like Us</strong>on Facebook</span> </a></li>
<li><a href="https://www.xhtmljunction.com/blog/feed/" target="_blank"> <i class="fa fa-rss"></i> <span><strong>Subscribe</strong>to RSS Feeds</span> </a></li>
</ul>
</div>
</div>
</div>
</article>
<!--footer bar-->
<!--bottom bar-->
<article class="bottom-bar">
<div class="wrapper">
<div class="left">
<p>Copyrights © 2021 All Rights Reserved. 
        <br/>
         Powered by XHTMLJunction (Division of IPraxa Software &amp; Services INC) </p>
<p><a href="https://www.xhtmljunction.com/terms-of-services.html">Terms of Use</a> / <a href="https://www.xhtmljunction.com/privacy-policy.html">Privacy Policy</a></p>
<div class="icons"> <a class="dmca-badge" href="https://www.dmca.com/Protection/Status.aspx?ID=0e4e5575-56f2-4749-831c-12d864b5ef9d&amp;refurl=https://www.xhtmljunction.com/" rel="nofollow" target="_blank" title="DMCA.com Protection Status"> <img alt="DMCA.com Protection Status" src="common/images/DMCA_logo-std-btn160w.png"/></a> <img alt="iso" class="iso" src="common/images/iso.png"/> </div>
</div>
<div class="right">
<div>
<ul class="social" itemscope="" itemtype="https://schema.org/Organization">
<li><a href="https://www.facebook.com/XHTMLJunctionFans" itemprop="sameAs" rel="nofollow" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
<li><a href="https://twitter.com/xhtml_junction" itemprop="sameAs" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i></a></li>
<!--<li><i class="fa fa-google-plus"></i></li>-->
<!--<li><a rel="nofollow"  target="_blank" itemprop="sameAs" href="http://www.linkedin.com/groups/Xhtmljunction-PSD-HTML-Conversion-6588517"><i class="fa fa-linkedin"></i></a></li>-->
<li><a href="https://www.youtube.com/user/XHTMLJunctionCompany" itemprop="sameAs" rel="nofollow" target="_blank"><i class="fa fa-youtube"></i></a></li>
</ul>
</div>
<div class="clear"></div>
<ul class="contact">
<li><a href="mailto:support@xhtmljunction.com" onclick="ga('send', 'event', 'Support Email ID', 'Clicked on Support Email ID', 'Sent a Mail');" rel="nofollow"><img alt="Email Us" src="common/images/email_img3.jpg"/></a></li>
<li><a href="tel:8189660495" rel="nofollow"><i class="fa fa-headphones"></i>818-966-0495</a></li>
<li><a href="skype:XhtmljunctionOnSkype?call" rel="nofollow"><i class="fa fa-skype"></i>XhtmljunctionOnSkype</a></li>
</ul>
</div>
</div>
</article>
<!--bottom bar-->
<a href="#" id="back-to-top"><i class="fa fa-caret-up"></i></a> </footer>
<!--footer part-->
<!--<script type="text/javascript">
var __lc = {}; __lc.license = 3177132; (function() { 	
    var lc = document.createElement('script'); lc.type = 'text/javascript'; 
    lc.async = true; 	lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js'; 	
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); })(); </script>
<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0082/1078.js" async="async"></script> -->
<script src="common/js/1.12.2.min.js"></script>
<!-- File upload -->
<!-- Styles -->
<link href="s3imageup/css/jquery.filer.css" rel="stylesheet"/>
<!-- Jvascript -->
<script src="s3imageup/js/jquery.filer.min.js" type="text/javascript"></script>
<script src="s3imageup/js/jquery.cookie.js" type="text/javascript"></script>
<script src="s3imageup/js/custom.js" type="text/javascript"></script>
<!-- End File upload -->
<script>
  $(function(){
    $.ajax({url: "blog/api/blog.php", success: function(result){
      $("#blog_list").html(result);
  }});
  })
  </script>
<!--footer part-->
</section>
<!--wrapper -->
<script src="common/js/1.12.2.min.js"></script>
<script src="common/js/jquery.mmenu.min.all.js"></script>
<script src="common/js/custom.js"></script>
<script>
  $(function(){
    $.ajax({url: "blog/api/blog.php", success: function(result){
      console.log(result);
      $("#blog_list").html(result);
  }});
  })
  </script></body>
</html>
