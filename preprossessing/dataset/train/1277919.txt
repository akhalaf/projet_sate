<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>New Orleans Gold | Welcome | Southern Coins &amp; Precious Metals</title>
<meta content="If you're in New Orleans, gold scraps and other precious metals can be sold at Southern Coins &amp; Precious Metals. Visit our website to learn about our spot pricing." name="description"/>
<meta content="buy, gold, currency, coins, rare" name="keywords"/>
<meta content="Southern Coins &amp; Precious Metals, Inc., 2011" name="copyright"/>
<meta content="NMD, Inc." name="author"/>
<meta content="Global" name="distribution"/>
<meta content="9cg25ijqz1ut8uy9bc7t0wqs7ui6zxfdwz2rv8icrrc2yzqvrg9zxy3b6x3oh6583bgi7ui05q8znyjqokge4s6hzza6tz1a298r0t-rik-ihloz83fiob3r8-ytwyd9" name="norton-safeweb-site-verification"/>
<base href="https://scpm.com/"/>
<meta ..="" content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link ..="" charset="utf-8" href="./templatefiles/styles.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="./css/bootstrap.css" rel="stylesheet"/>
<link href="./css/main.css" rel="stylesheet"/>
<link ..="" href="favicon.ico" rel="shortcut icon"/>
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet" type="text/css"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="./templatefiles/javascript.js" type="text/javascript"></script>
</head>
<script async="" src="//web-2-tel.com/sdk?identifier=13d5289a15bf4da587e1ab9eb418ef7a" type="text/javascript"></script>
<!-- Mobile Navigation -->
<body><nav class="navbar navbar-default navbar-fixed-top visible-xs visible-sm" role="navigation">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="tel:5048870000" style="color: #2d2613;"><strong>504.887.0000</strong></a><a class="navbar-brand" href="tel:18005359704" style="color: #2d2613;"><strong>800.535.9704</strong></a>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav navbar-right">
<li>
<a href="https://scpm.com/bullion.php"><strong>BULLION</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=8"><strong>U.S. CERTIFIED INVENTORY</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=10"><strong>CURRENCY</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=155"><strong>ANCIENT COINS CERTIFIED</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=158"><strong>WORLD CERTIFIED INVENTORY</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=159"><strong>ESTATE JEWELRY</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=checkout/cart"><strong><span class="glyphicon glyphicon-shopping-cart" style="margin-right:7px;">Cart</span></strong></a>
</li>
<li>
<a href="https://scpm.com/scpm.com/shop/index.php?route=account/login"><strong>Account</strong></a>
</li>
<li>
<a href="https://scpm.com/shop/index.php?route=account/login"><strong>Login</strong></a>
</li>
</ul>
</div><!-- /.navbar-collapse -->
</nav>
<!-- END Mobile Navigation -->
<!-- Full Size Header with Full Size Navigation -->
<!-- Topmost Navigation Bar -->
<div class="hidden-sm hidden-xs" style="background-color: #cf962c; max-height:50px;">
<div class="container">
<div class="row">
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="col-md-7" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li style="padding-top:15px; padding-right:11px;"><strong><span style="color:#4f3503;">504.887.0000  |  CALL TOLL FREE: 800.535.9704</span></strong> </li>
<li class="active"><a href="https://scpm.com/shop/index.php?route=checkout/cart"><strong><span class="glyphicon glyphicon-shopping-cart" style="margin-right:7px;"></span>Cart</strong></a></li>
<li><a href="https://scpm.com/shop/index.php?route=account/login"><strong>Account</strong></a></li>
<li><a href="https://scpm.com/shop/index.php?route=account/login"><strong>Login</strong></a></li>
</ul>
</div>
<div class="col-md-5">
<form class="navbar-form navbar-right" role="search">
<div class="form-group"> <a href="https://www.google.com/maps/place/4513+Zenith+St,+Metairie,+LA+70001/@29.99533,-90.1834109,17z/data=!3m1!4b1!4m5!3m4!1s0x8620b06710900c5f:0x7fd770641cd053c4!8m2!3d29.99533!4d-90.1812222?hl=en" target="_blank"><img alt="View map to SCPM" height="18" src="https://scpm.com/img/map.png" width="12"/> <span style="color: #ecce40;">View Map</span></a>   <a href="https://www.facebook.com/southerncoins/" target="_blank"><img alt="SCPM on Facebook" height="18" src="https://scpm.com/img/fb.png" width="9"/></a>   <a href="https://twitter.com/scpm_gold" target="_blank"><img alt="SCPM on Twitter" height="16" src="https://scpm.com/img/tw.png" width="18"/></a>  
		<input class="form-control" placeholder="Search" style="margin-top:-3px;" type="text"/>
</div>
<button class="searchbtn" style="border:0px; margin-left:2px;" type="submit"><span class="glyphicon glyphicon-search"></span>  <strong>Search</strong></button>
</form>
</div>
</div><!-- /.row -->
</div><!-- /.container -->
</div>
<!-- END Topmost Navigation Bar -->
<!-- Header Content -->
<div class="headerbg hidden-sm hidden-xs">
<div class="container">
<div class="row">
<div class="col-md-5"><a href="./index.php"><img alt="Southern Coins &amp; Precious Metals" src="https://scpm.com/img/scpm_logo.png" style="max-width:282px; margin-bottom:20px; margin-top:12px;" width="100%"/></a></div>
<div align="center" class="col-md-2" style="margin-top:25px;">
<span style="color:#ffffff; font-size:18px;"><strong>Prices updated<br/>
          every 5 minutes*</strong></span><br/>
<br/>
<span id="update_time_two" style="color:#cf962c;"><em>Last Updated:<br/>

1/12/2021 11:45:02am CST</em></span></div>
<div align="center" class="col-md-5" style="padding-top:8px;">
<div class="spotbg" id="spot_price_one">
<table border="0" cellpadding="2" cellspacing="0" width="328">
<tr>
<td align="center" width="88"><span style="color:#ffffff;"><strong>Spot 

                        Gold:</strong></span></td>
<td style="padding-left:5px;" width="30"><span style="color:#f5d956;"><strong>$1839.43</strong></span></td>
<td align="center" width="88"><span style="color:#ffffff;"><strong>Spot 

                        Silver:</strong></span></td>
<td style="padding-left:5px;" width="30"><span style="color:#f5d956;"><strong>$25.31</strong></span></td>
</tr>
<tr>
<td><span style="color:#ffffff;"><strong>Spot 

                        Platinum:</strong></span></td>
<td style="padding-left:5px;"><span style="color:#f5d956;"><strong>$<strong>1066.07</strong></strong></span></td>
<td><span style="color:#ffffff;"><strong>Spot 

                        Palladium:</strong></span></td>
<td style="padding-left:5px;"><span style="color:#f5d956;"><strong>$2369.31</strong></span></td>
</tr>
<tr>
<td align="center" colspan="4" style="padding-top:30px; padding-bottom:15px;"><a class="spotbtn" href="https://scpm.com/bullion.php/bullion.php">LIVE SPOT PRICING <span class="glyphicon glyphicon-chevron-right" style="margin-left:55px;"></span></a></td>
</tr>
</table>
</div></div>
</div>
</div>
</div>
<div class="headerbg hidden-lg hidden-md">
<div class="container">
<div class="row">
<div align="center" class="col-md-12">
<img alt="Southern Coins &amp; Precious Metals" src="./img/scpm_logo.png" style="max-width:282px; margin-bottom:20px; margin-top:12px;" width="100%"/>
<br/>
<a href="https://www.google.com/maps/place/4513+Zenith+St,+Metairie,+LA+70001/@29.99533,-90.1834109,17z/data=!3m1!4b1!4m5!3m4!1s0x8620b06710900c5f:0x7fd770641cd053c4!8m2!3d29.99533!4d-90.1812222?hl=en" target="_blank"><span style="color: #ecce40; font-size:18px;"><strong>4513 Zenith St. Metairie, LA 70001</strong></span><br/>
<img alt="View map to SCPM" height="18" src="https://scpm.com/img/map.png" width="12"/> <span style="color: #ecce40;">Click Here to View Map</span></a><br/><br/>
<a href="https://www.facebook.com/southerncoins/" target="_blank"><img alt="SCPM on Facebook" height="18" src="https://scpm.com/img/fb.png" width="9"/></a>    <a href="https://twitter.com/scpm_gold" target="_blank"><img alt="SCPM on Twitter" height="16" src="https://scpm.com/img/tw.png" width="18"/></a><br/>
<br/>
<span style="color:#ffffff; font-size:18px;"><strong>Prices updated<br/>
          every 5 minutes*</strong></span><br/>
<br/>
<span id="update_time_one" style="color:#cf962c;"><em>Last Updated:<br/>

1/12/2021 11:45:02am CST</em></span><br/>
<br/>
<div align="center" class="spotbg" id="spot_price_two" style="padding-top:25px;">
<div class="spot_price_two_content">
<span style="color:#ffffff;"><strong>Spot Gold:</strong></span>  <span style="color:#f5d956;"><strong>$1839.43</strong></span><br/>
<span style="color:#ffffff;"><strong>Spot Silver:</strong></span>  <span style="color:#f5d956;"><strong>$25.31</strong></span><br/>
<span style="color:#ffffff;"><strong>Spot Platinum:</strong></span>  <span style="color:#f5d956;"><strong>$1066.07</strong></span><br/>
<span style="color:#ffffff;"><strong>Spot Palladium:</strong></span>  <span style="color:#f5d956;"><strong>$2369.31</strong></span><br/>
<br/>
<a class="spotbtn" href="https://scpm.com/bullion.php">LIVE SPOT PRICING <span class="glyphicon glyphicon-chevron-right" style="margin-left:10px;"></span></a><br/><br/>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END Header Content -->
<!-- Fullsize Navigation -->
<div class="navbg hidden-sm hidden-xs">
<div class="container">
<div align="center" class="row">
<div align="center" class="col-md-12">
<!-- Desktop nav starts -->
<div class="desktop-nav">
<a href="https://scpm.com/bullion.php" style="margin-right:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>BULLION</strong></a>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=8" style="margin-left:10px; margin-right:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>U.S. CERTIFIED INVENTORY</strong></a>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=10" style="margin-left:10px; margin-right:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>CURRENCY</strong></a>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=155" style="margin-left:10px; margin-right:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>ANCIENT COINS CERTIFIED</strong></a>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=158" style="margin-left:10px; margin-right:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>WORLD CERTIFIED INVENTORY</strong></a>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=159" style="margin-left:10px; letter-spacing: .5px; font-size:14px; color: #2d2613;"><strong>ESTATE JEWELRY</strong></a>
</div>
<!-- Desktop nav ends -->
</div>
</div>
</div>
</div>
<br/><br/>
<!-- END Fullsize Navigation -->
<!-- END Full Size Header with Full Size Navigation -->
<div class="container" style="padding-right:22px; padding-left:22px;">
<div class="row">
<div align="center" class="col-md-12" style="padding-right:10px; padding-left:10px;"><h1>New Orleans Gold <span style="color:#cf962c;">&amp;</span> Rare Coins Dealer</h1>
<h2>Serving the Greater New Orleans Area</h2>
<br/>
Southern Coins &amp; Precious Metals (SCPM) has been a leader in the numismatic community since 1977. <br/>
Proudly servicing a nationwide clientele, SCPM offers easy and secure online ordering, live spot pricing &amp; the latest in numismatic news.<br/>
<br/>
<br/>
</div>
</div>
<div class="row">
<div align="center" class="col-md-3" style="padding-bottom:15px;"><img alt="Bullion" src="img/bullion.png" style="max-width:409px; padding-bottom:10px;" width="100%"/><br/>
<span class="blogtitle">Bullion</span><br/><br/>
<a class="scpmbtn" href="bullion.php">View All</a></div>
<div align="center" class="col-md-3" style="padding-bottom:15px;"><img alt="U.S. Certified Inventory" src="img/certified.png" style="max-width:409px; padding-bottom:10px;" width="100%"/><br/>
<span class="blogtitle">U.S. Certified Inventory</span><br/><br/>
<a class="scpmbtn" href="shop/index.php?route=product/category&amp;path=8">View All</a></div>
<div align="center" class="col-md-3" style="padding-bottom:15px;"><img alt="Raw U.S. Gold" src="img/raw.png" style="max-width:409px; padding-bottom:10px;" width="100%"/><br/>
<span class="blogtitle">Raw U.S. Gold</span><br/><br/>
<a class="scpmbtn" href="raw.php">View All</a></div>
<div align="center" class="col-md-3" style="padding-bottom:15px;"><img alt="U.S. Silver Eagles" src="img/silver.png" style="max-width:409px; padding-bottom:10px;" width="100%"/><br/>
<span class="blogtitle">U.S. Silver Eagles</span><br/><br/>
<a class="scpmbtn" href="shop/index.php?route=product/category&amp;path=138">View All</a></div>
</div>
<div style="padding-top:20px; padding-bottom:20px;"><hr/></div>
<div class="row">
<div class="col-md-6" style="padding-bottom:22px;"><div align="center" style="padding-bottom:20px;"><span class="sectiontitle">SCPM Consumer Protection</span></div>
<div style="text-align:justify; padding-bottom:20px;">Through the mid-1980's, rare coin prices were determined by individual dealers using their own subjective criteria to judge the quality or condition of a coin. Unfortunately, this led to conflicts of interest and many abuses in our industry. Investors now enjoy consistent, independent third-party grading of coins by firms whose sole function is to authenticate and grade coins. In the major grading services coins are graded not by an individual grader, but by the consensus of a panel of experts. To protect the integrity of the final, assigned grade, the coins are sealed in tamper-resistant plastic holders that are sonically sealed with an individual certification number and the grade permanently displayed...<a href="consumer.php"><strong>Learn More</strong></a></div></div>
<div class="col-md-6" style="padding-bottom:22px;"><div align="center" style="padding-bottom:20px;"><span class="sectiontitle">Sell Your Gold to SCPM</span></div>
<div style="text-align:justify;  padding-bottom:20px;">If you're interested in selling precious metals, please get in touch with Southern Coins &amp; Precious Metals today. A leader in the numismatic community, our company was created to appraise fine metals, and to buy and sell rare coins and precious metals. New Orleans locals have trusted and depended on us for this service for 40 years. We look forward to working with you when you would like to invest in or sell precious metals, coins or currency. You are welcome to come in or contact us with any questions regarding the process. Come visit us and see for yourself why we have been trusted by our clients for the past four decades. You won’t be sorry!...<a href="selling.php"><strong>Learn More</strong></a></div></div>
</div>
</div>
<div style="background-color: #f6f7f7; padding-top:35px; padding-bottom:15px; padding-right:15px; padding-left:15px;">
<div class="container">
<div class="row">
<div class="col-md-4">
<div align="center" style="padding-bottom:20px;"><span class="sectiontitle">What Our Customers Say</span></div>
<div style="text-align:justify;  padding-bottom:20px;">"...I noticed you have some superb NGC coins. 
      <br/>
      My compliments on a beautiful inventory and website."<br/>
<span style="color: #8a6c12; font-weight:bold;">- Mark Salzberg,<br/>
Numismatic Guaranty Corporation (NGC)</span>
<hr/>
"I have received the coin today and I am immensely pleased with it. I love the toning and I am sure the coin has finally found itself a safe home for many years. It has been a pleasure to do business with you and, even though I can afford to buy only 3-4 coins a year, I will certainly purchase more coins from you in the future. Thank you one more time." <span style="color: #8a6c12; font-weight:bold;">- M.G.</span></div>
</div>
<div align="center" class="col-md-4" style="padding-bottom:20px;"><img alt="" src="img/ad_pic.png" style="max-width:324px;" width="100%"/><br/>
<br/>
<span class="sectiontitle">Our Latest Ads</span>
<br/><br/>
<a class="scpmbtn" href="ads.php">View All</a>
</div>
<div class="col-md-4"><div align="center" style="padding-bottom:20px;"><span class="sectiontitle">Our Latest Blog</span></div>
<div align="center" class="blogtitle" style="padding-bottom:15px;">4 Reasons You Should Invest In Gold &amp; Silver</div>
<div style="text-align:justify; padding-bottom:20px;">
<p>
<span style="font-size:16px;"><span style="color:#333333;">J.P Morgan once said, "Gold is money, everything else is credit." Over time, this statement has proven to be nothing short of the truth; while paper currency has continued to yo-yo between stability and evascence, natural resources like gold and silver have remained constant sources of insurance in times of market turmoil.</span></span></p>
<p>
<span style="font-size:16px;"><strong><span style="color:#000000;">Here are 4 reasons you should invest in gold &amp; silver, the world's bullet-proof insurance: </span></strong></span></p>
<a href="/shop/index.php?route=phpblog/blog&amp;id=14"><strong>Read More</strong></a></div></div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div align="center" class="col-md-12" style="padding-top:22px; padding-right:25px; padding-left:25px; padding-bottom:40px;"><h1>Proud Memberships <span style="color:#cf962c;">&amp;</span> Affiliations</h1>
<br/>
<span style="margin-right:20px; padding-bottom:40px;"><img alt="" src="img/png.jpg" style="max-width:175px;" width="100%"/></span><span style="margin-right:20px; padding-bottom:40px;"><img alt="" src="img/pngapmd.jpg" style="max-width:260px;" width="100%"/></span><span style="margin-right:20px; padding-bottom:40px;"><img alt="" src="img/anniversary.jpg" style="max-width:188px;" width="100%"/></span><span style="margin-right:20px; padding-bottom:40px;"><img alt="" src="img/ngc.jpg" style="max-width:203px;" width="100%"/></span><span style="padding-bottom:40px;"><img alt="" src="img/pcgs.jpg" style="max-width:176px;" width="100%"/></span></div>
</div>
</div>
<div class="footerbg">
<div class="container">
<div class="row">
<div class="col-md-2" style="padding-top: 15px; padding-bottom:15px;"><span class="footertitle">About SCPM</span><br/>
<br/>
<a href="https://scpm.com/history.php">· Company History</a><br/>
<a href="https://scpm.com/affiliations.php">· Affiliations</a><br/>
<a href="https://scpm.com/selling.php">· Selling to Us</a><br/>
<a href="https://scpm.com/rare_coin.php">· Rare Coin Valuation</a><br/>
<a href="https://scpm.com/consumer.php">· Consumer Protection</a><br/>
<a href="https://scpm.com/advantage.php">· Having the Advantage</a></div>
<div class="col-md-2" style="padding-top: 15px; padding-bottom:15px;"><span class="footertitle">Customer Service</span><br/>
<br/>
<a href="https://scpm.com/shop/index.php?route=information/contact">· Inquiries</a><br/>
<a href="https://scpm.com/privacy.php">· Privacy</a><br/>
<a href="https://scpm.com/join.php">· Join our Email List</a><br/>
<a href="https://scpm.com/shop/index.php?route=checkout/checkout">· My Shopping Cart</a><br/>
<a href="https://scpm.com/sitemap.php">· Site Map</a></div>
<div class="col-md-2" style="padding-top: 15px; padding-bottom:15px;"><span class="footertitle">What We Offer You!</span><br/>
<br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=8">· Current Cert. Currency</a><br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=2">· Certified US Gold</a><br/>
<a href="https://scpm.com/raw.php">· Raw US Gold</a><br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=9">· Cert. Morgan Dollar Sets</a><br/>
<a href="https://scpm.com/bullion.php">· Gold &amp; Silver Bullion</a><br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=10">· Currency</a><br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=4">· Vault Specials</a><br/>
<a href="https://scpm.com/shop/index.php?route=product/category&amp;path=87">· Finest Rarities</a></div>
<div align="center" class="col-md-6" style="padding-top: 22px; padding-bottom:22px;"><a href="https://scpm.com/index.php"><img alt="Southern Coins &amp; Precious Metals" src="https://scpm.com/img/scpm_logo.png" style="max-width:230px;" width="100%"/></a><br/>
<br/>
<span class="footertitle">Our Location</span><br/>
<span style="color:#cf962c;">4513 Zenith St. Metairie, LA 70001 -</span> <a href="http://maps.google.com/maps?q=4513+zenith+st,+metairie,+la&amp;hl=en&amp;sll=37.0625,-95.677068&amp;sspn=51.708931,73.740234&amp;vpsrc=0&amp;t=m&amp;z=17" style="font-weight:bold; color: #f5d956; text-decoration:none;" target="_blank">View Map</a><br/>
<!--<span style="color:#cf962c;">5525 S Sherwood Forest Blvd. Baton Rouge, LA 70816 -</span> <a href="http://maps.google.com/maps?q=5525+Sherwood+Forest+Boulevard,+Baton+Rouge,+LA&hl=en&sll=30.458283,-91.14032&sspn=0.443915,0.586395&vpsrc=0&t=m&z=17" target="_blank" style="font-weight:bold; color: #f5d956; text-decoration:none;">View Map</a><br>-->
<span style="font-weight:bold; color: #f5d956;">504.887.0000    |    CALL TOLL FREE: 800.535.9704<br/>
FAX: 504.887.0071    |    <a href="mailto:sales@scpm.com" style="font-weight:bold; color: #f5d956; text-decoration:none;" target="_blank">Email Us</a></span><br/><br/>
<span style="color:#ffffff; font-size:11px; line-height:13px;">*All prices shown are intended to be reference points and are not intended to be contractual agreements. 
<br/>
Please call 1-800-535-9704 to have your price locked in. All bullion payments must be by wire or overnight check. No exceptions.</span></div>
</div>
<script>jQuery('a[href*="/shop/index.php?route=product/category&path=7"]').attr('href','/bullion.php')</script>
</div>
</div>
<script>
	var update_header= function(){
		$('#spot_price_one').html('')
		$('#spot_price_one').load('/index.php #spot_price_one table')
		$('#spot_price_one').html('')
		$('#spot_price_two').load('/index.php #spot_price_two .spot_price_two_content')
		$('#update_time_one').html('');
		$('#update_time_one').load('/index.php #update_time_one')
		$('#update_time_two').html('');
		$('#update_time_two').load('/index.php #update_time_two')
	}
	
	setInterval(function(){update_header()},150000)
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42098066-1', 'auto');
  ga('send', 'pageview');

</script>
<script async="" src="https://i.simpli.fi/dpx.js?cid=54792&amp;action=100&amp;segment=SouthernCoin_May2016_Site&amp;m=1&amp;sifi_tuid=29505"></script>
<script async="" src="https://i.simpli.fi/dpx.js?cid=54792&amp;conversion=10&amp;campaign_id=0&amp;m=1&amp;c=SouthernCoin_May2016_Conversion&amp;sifi_tuid=29507"></script>
<script async="" src="https://tag.simpli.fi/sifitag/5e909940-9077-0137-5818-06659b33d47c"></script>
</body></html>