<!DOCTYPE html>
<html id="html" style="width:100%;height:100%">
<head>
<title>zvg.com</title>
<link href="images/suche.png" rel="icon"/>
<script>
			var version=535;
			var startSuche="";
			var id_b=0;
			var id_g=0;
			var id_t=0;
			var id_oa=0;
			var delSuche="";
			var impressum=false;
			var infomail="";
		</script>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" name="viewport"/>
<meta content="no-store" http-equiv="Cache-Control"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="VerÃ¶ffentlich von Zwangsversteigerungen auf zvg.com" name="DC.Description"/>
<meta content="Zwangsversteigerungen auf www.zvg.com" name="DC.Page-topic"/>
<meta content="Zwangsversteigerung, Versteigerungen, Zwangsversteigerungen, zvg, Portal, Immobilien, Amtsgericht, Hamburg, Berlin, Stuttgart, Einfamilienhaus, Wohnung, GrundstÃ¼ck" name="keywords"/>
<meta content="Zwangsversteigerung, Versteigerungen, Immobilien, Zwangsversteigerungen, zvg, Portal, Justiz, Adressen" name="Audience"/>
<meta content="zvg.com" name="DC.Author"/>
<meta content="deutsch, de" name="LANGUAGE"/>
<meta content="follow,index,noimageindex,notranslate" name="ROBOTS"/>
<meta content="1 days" name="revisit-after"/>
<title>Zwangsversteigerungen auf zvg.com</title>
<link href="dhtmlx/codebase/fonts/font_roboto/roboto.css" rel="stylesheet" type="text/css"/>
<script src="dhtmlx/codebase/tdb_lib.js" type="text/javascript"></script>
<script src="dhtmlx/codebase/dhtmlxscheduler.js" type="text/javascript"></script>
<script charset="utf-8" src="dhtmlx/codebase/sources/locale/locale_de.js"></script>
<link href="dhtmlx/codebase/dhtmlxscheduler.css" rel="stylesheet" type="text/css"/>
<link href="dhtmlx/codebase/dhtmlx.css" rel="stylesheet" type="text/css"/>
<link href="inbounds/css/main.css" rel="stylesheet" type="text/css"/>
<link href="inbounds/css/card.css" rel="stylesheet" type="text/css"/>
<link href="appl/css/zvg.css" rel="stylesheet" type="text/css"/>
<script>
	 (function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga'); ga('create', 'UA-67877536-1', 'auto'); 
	 </script>
<script src="appl/js/zvg.js"></script>
</head>
<body id="body" onload="doOnLoad();" onresize="resizeBody();" style="width:100%;height:100%">
<div style="display:none">
<h1>zvg.vom publiziert Zwangsversteigerungen im Auftrag der nachfolgenden Amtsgerichte</h1><ol><li>Strausberg</li><li>Bad Liebenwerda</li><li>Pasewalk Zweigstelle Anklam</li><li>Ludwigslust Zweigstelle Parchim</li><li>SchÃ¶nebeck</li><li>MÃ¼hlhausen</li><li>Senftenberg</li><li>Neuruppin</li><li>Potsdam</li><li>Gotha</li></ol><h1>Ãbersicht Ã¼ber die Amtsgerichte in einem Bundesland</h1><ol><li><a href="https://www.zvg.com/brandenburg/index.html">Zwangsversteigerungen in Brandenburg</a></li><li><a href="https://www.zvg.com/mecklenburg-vorpommern/index.html">Zwangsversteigerungen in Mecklenburg-Vorpommern</a></li><li><a href="https://www.zvg.com/sachsen-anhalt/index.html">Zwangsversteigerungen in Sachsen-Anhalt</a></li><li><a href="https://www.zvg.com/thueringen/index.html">Zwangsversteigerungen in ThÃ¼ringen</a></li><li><a href="https://www.zvg.com/baden-wuerttemberg/index.html">Zwangsversteigerungen in Baden-WÃ¼rttemberg</a></li><li><a href="https://www.zvg.com/schleswig-holstein/index.html">Zwangsversteigerungen in Schleswig-Holstein</a></li><li><a href="https://www.zvg.com/hamburg/index.html">Zwangsversteigerungen in Hamburg</a></li><li><a href="https://www.zvg.com/rheinland-pfalz/index.html">Zwangsversteigerungen in Rheinland-Pfalz</a></li><li><a href="https://www.zvg.com/niedersachsen/index.html">Zwangsversteigerungen in Niedersachsen</a></li><li><a href="https://www.zvg.com/berlin/index.html">Zwangsversteigerungen in Berlin</a></li></ol><h1>Die neuesten Objekte</h1><div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 3 K 26-15</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2021-10-05T09:00" itemprop="startDate">
Di, den 05.10.2021, 09:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Strausberg 
KlosterstraÃe 13, 15344 Strausberg
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
1
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Einfamilienhaus</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Kirschgarten 28, 29</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 16321</span>
<span itemprop="addressLocality"> Bernau OT Ladeburg</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  22,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=5&amp;gericht=41&amp;id_t=193916" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=5&amp;gericht=41&amp;id_t=193916">
<img itemprop="image" src="../bilder/straus/v_3k26-15.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 15 K 8-16</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-21T13:00" itemprop="startDate">
Di, den 21.04.2020, 13:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Bad Liebenwerda 
Burgplatz 4, 04924 Bad Liebenwerda
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
1, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="18600.00" itemprop="price">
18.600,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Gewerbeobjekt</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">JeÃnigk 47</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 04916</span>
<span itemprop="addressLocality"> SchÃ¶newalde OT JeÃnigk</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Es gibt ein kostenfreies Gutachten zum Download.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=5&amp;gericht=154&amp;id_t=195025" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=5&amp;gericht=154&amp;id_t=195025">
<img itemprop="image" src="../bilder/badlieb/v_15k8-16.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 513 K 26-19</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-16T09:00" itemprop="startDate">
Do, den 16.04.2020, 09:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Pasewalk Zweigstelle Anklam 
BaustraÃe 9, 17389 Anklam
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
124, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="19500.00" itemprop="price">
19.500,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Wohnen/Gewerbe</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Maxim-Gorki-StraÃe 5a</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 17321</span>
<span itemprop="addressLocality"> LÃ¶cknitz</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  26,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=6&amp;gericht=144&amp;id_t=195138" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=6&amp;gericht=144&amp;id_t=195138"><img itemprop="image" src="https://www.zvg.com/bilder/placeholder.jpg"/></a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 513 K 27-19</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-16T13:30" itemprop="startDate">
Do, den 16.04.2020, 13:30
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Pasewalk Zweigstelle Anklam 
BaustraÃe 9, 17389 Anklam
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
124, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="140000.00" itemprop="price">
140.000,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Einfamilienhaus</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Platz 7a</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 17375</span>
<span itemprop="addressLocality"> Ahlbeck OT Seegrund</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  17,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=6&amp;gericht=144&amp;id_t=195139" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=6&amp;gericht=144&amp;id_t=195139"><img itemprop="image" src="https://www.zvg.com/bilder/placeholder.jpg"/></a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 15 K 106-17</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-06-09T09:30" itemprop="startDate">
Di, den 09.06.2020, 09:30
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Ludwigslust Zweigstelle Parchim 
Amtsgericht Ludwigslust Zweigstelle Parchim, Moltkeplatz 2, 19370 Parchim
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
247, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="1.00" itemprop="price">
1,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
3- bis 4,5-Zimmer-Wohnung</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Grevener StraÃe 51</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 19386</span>
<span itemprop="addressLocality"> Werder</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  25,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=6&amp;gericht=97&amp;id_t=195144" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=6&amp;gericht=97&amp;id_t=195144">
<img itemprop="image" src="../bilder/ludwigslust/v_15k106-17.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 15 K 6-18</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-05-05T10:00" itemprop="startDate">
Di, den 05.05.2020, 10:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Ludwigslust Zweigstelle Parchim 
Amtsgericht Ludwigslust Zweigstelle Parchim, Moltkeplatz 2, 19370 Parchim
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
247, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="42000.00" itemprop="price">
42.000,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Einfamilienhaus</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">BahnhofstraÃe 2</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 19376</span>
<span itemprop="addressLocality"> Suckow</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  23,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=6&amp;gericht=97&amp;id_t=195166" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=6&amp;gericht=97&amp;id_t=195166">
<img itemprop="image" src="../bilder/ludwigslust/v_15k6-18.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 15 K 51-18</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-28T13:00" itemprop="startDate">
Di, den 28.04.2020, 13:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Bad Liebenwerda 
Burgplatz 4, 04924 Bad Liebenwerda
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
1, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Land-/Forstwirtschaft</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"></span>
<span itemprop="postalCode"> 04895</span>
<span itemprop="addressLocality"> Falkenberg</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Es gibt ein kostenfreies Gutachten zum Download.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=5&amp;gericht=154&amp;id_t=195178" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=5&amp;gericht=154&amp;id_t=195178">
<img itemprop="image" src="../bilder/badlieb/v_15k51-18.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 6 K 9-19</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-05-27T10:00" itemprop="startDate">
Mi, den 27.05.2020, 10:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht MÃ¼hlhausen 
GerichtsgebÃ¤ude Untermarkt 17, 99974 MÃ¼hlhausen
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
VI, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Land-/Forstwirtschaft</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"></span>
<span itemprop="postalCode"> 37318</span>
<span itemprop="addressLocality"> Rustenfelde</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  19,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=12&amp;gericht=127&amp;id_t=195204" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=12&amp;gericht=127&amp;id_t=195204">
<img itemprop="image" src="../bilder/muehlhausen/v_6k9-19.jpg"/>
</a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 3 K 30-16</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-21T10:00" itemprop="startDate">
Di, den 21.04.2020, 10:00
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht SchÃ¶nebeck 
FriedrichstraÃe 96, 39218 SchÃ¶nebeck
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
308
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="194700.00" itemprop="price">
194.700,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
Land-/Forstwirtschaft</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"></span>
<span itemprop="postalCode"> 39249</span>
<span itemprop="addressLocality"> Barby (Elbe)</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  31,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=10&amp;gericht=143&amp;id_t=195211" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=10&amp;gericht=143&amp;id_t=195211"><img itemprop="image" src="https://www.zvg.com/bilder/placeholder.jpg"/></a>
</div>
<div style="display:inline-block;width:650px">
<div itemscope="" itemtype="http://schema.org/Event">
<span style="display:inline-block;width:150px">Aktenzeichen:</span>
<span itemprop="name">Az: 42 K 26-18</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungstermin:</span>
<span content="2020-04-29T09:30" itemprop="startDate">
Mi, den 29.04.2020, 09:30
</span>
<br/>
<span style="display:inline-block;width:150px">Versteigerungsort:</span>
<span itemprop="performer">Amtsgericht Senftenberg 
Steindamm 8, 01968 Senftenberg
</span>
<br/>
<span style="display:inline-block;width:150px">Saal:</span>
E 01, Sitzungssaal
<br/>
<span style="display:inline-block;width:150px">Verkehrswert:</span>
<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
<span content="20000.00" itemprop="price">
20.000,00</span> EUR
</span>
<br/>
<span style="display:inline-block;width:150px">Objektart:</span><span itemprop="description">
GrundstÃ¼ck</span>
<br/>
<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
<span itemprop="name" style="display:inline-block;width:150px">Objektanschrift:</span>
<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Markt 7-8</span>,<br/>
<span itemprop="name" style="display:inline-block;width:150px"></span>
<span itemprop="postalCode"> 03226</span>
<span itemprop="addressLocality"> Vetschau</span>
</span>
</span>
<br/>
<span style="display:inline-block;width:150px">Gutachten:</span>
Das Gutachten kann kostenpflichtig zum Preis von  14,00 angefordert werden.
<span style="display:inline-block;width:150px">Details zum Objekt:</span>
<a href="../start.prg?land=5&amp;gericht=34&amp;id_t=195231" itemprop="url">
Weiter zur Detailansicht ...</a>
</div>
</div>
<div style="display:inline-block;width:140px">
<a href="../start.prg?land=5&amp;gericht=34&amp;id_t=195231">
<img itemprop="image" src="../bilder/senft/v_42k26-18.jpg"/>
</a>
</div>
</div>
<script src="dhtmlx/codebase/dhtmlx.js"></script>
</body>
</html>