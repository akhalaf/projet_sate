<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#"><head><meta charset="utf-8"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><link href="https://raredesign.ca/xmlrpc.php" rel="pingback"/> <script type="text/javascript">document.documentElement.className = 'js';</script> <script>var et_site_url='https://raredesign.ca';var et_post_id='1527';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}</script><link href="https://raredesign.ca/wp-content/cache/autoptimize/css/autoptimize_48c37a124a76b1e1ae3aa1e9f47fc581.css" media="all" rel="stylesheet"/><title>Page Not Found | Rare Design Group</title><link href="//fonts.googleapis.com" rel="dns-prefetch"/><link href="//use.fontawesome.com" rel="dns-prefetch"/><link href="//s.w.org" rel="dns-prefetch"/><link href="https://cdn.shortpixel.ai" rel="preconnect"/><link href="https://raredesign.ca/feed/" rel="alternate" title="Rare Design Group » Feed" type="application/rss+xml"/><link href="https://raredesign.ca/comments/feed/" rel="alternate" title="Rare Design Group » Comments Feed" type="application/rss+xml"/> <script type="text/javascript">window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/raredesign.ca\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);</script> <meta content="Rare Design Group Custom Theme v.1.0" name="generator"/><link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext&amp;display=swap" id="divi-fonts-css" media="all" rel="stylesheet" type="text/css"/><link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" id="font-awesome-official-css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" media="all" rel="stylesheet" type="text/css"/><link href="https://raredesign.ca/wp-includes/css/dashicons.min.css?ver=5.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/><link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.13.0/css/v4-shims.css" id="font-awesome-official-v4shim-css" integrity="sha384-/7iOrVBege33/9vHFYEtviVcxjUsNCqyeMnlW/Ms+PH8uRdFkKFmqf9CbVAN0Qef" media="all" rel="stylesheet" type="text/css"/> <script id="jquery-core-js" src="https://raredesign.ca/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script> <link href="https://raredesign.ca/wp-json/" rel="https://api.w.org/"/><link href="https://raredesign.ca/wp-json/wp/v2/pages/1527" rel="alternate" type="application/json"/><link href="https://raredesign.ca/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/><link href="https://raredesign.ca/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/><meta content="WordPress 5.6" name="generator"/><link href="https://raredesign.ca/error/" rel="canonical"/><link href="https://raredesign.ca/?p=1527" rel="shortlink"/><link href="https://raredesign.ca/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fraredesign.ca%2Ferror%2F" rel="alternate" type="application/json+oembed"/><link href="https://raredesign.ca/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fraredesign.ca%2Ferror%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/> <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-131037248-1"></script> <script>window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131037248-1');</script><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link as="font" crossorigin="anonymous" href="https://raredesign.ca/wp-content/themes/Divi/core/admin/fonts/modules.ttf" rel="preload"/><link href="https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_32,h_32/https://raredesign.ca/wp-content/uploads/2020/05/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/><link href="https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_192,h_192/https://raredesign.ca/wp-content/uploads/2020/05/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/><link href="https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_180,h_180/https://raredesign.ca/wp-content/uploads/2020/05/cropped-favicon-180x180.png" rel="apple-touch-icon"/><meta content="https://raredesign.ca/wp-content/uploads/2020/05/cropped-favicon-270x270.png" name="msapplication-TileImage"/> <script>jQuery(document).ready(function($) {

    var ResponsiveMenu = {
        trigger: '#responsive-menu-button',
        animationSpeed:500,
        breakpoint:980,
        pushButton: 'off',
        animationType: 'slide',
        animationSide: 'left',
        pageWrapper: '',
        isOpen: false,
        triggerTypes: 'click',
        activeClass: 'is-active',
        container: '#responsive-menu-container',
        openClass: 'responsive-menu-open',
        accordion: 'off',
        activeArrow: '▲',
        inactiveArrow: '▼',
        wrapper: '#responsive-menu-wrapper',
        closeOnBodyClick: 'off',
        closeOnLinkClick: 'off',
        itemTriggerSubMenu: 'off',
        linkElement: '.responsive-menu-item-link',
        subMenuTransitionTime:200,
        openMenu: function() {
            $(this.trigger).addClass(this.activeClass);
            $('html').addClass(this.openClass);
            $('.responsive-menu-button-icon-active').hide();
            $('.responsive-menu-button-icon-inactive').show();
            this.setButtonTextOpen();
            this.setWrapperTranslate();
            this.isOpen = true;
        },
        closeMenu: function() {
            $(this.trigger).removeClass(this.activeClass);
            $('html').removeClass(this.openClass);
            $('.responsive-menu-button-icon-inactive').hide();
            $('.responsive-menu-button-icon-active').show();
            this.setButtonText();
            this.clearWrapperTranslate();
            this.isOpen = false;
        },
        setButtonText: function() {
            if($('.responsive-menu-button-text-open').length > 0 && $('.responsive-menu-button-text').length > 0) {
                $('.responsive-menu-button-text-open').hide();
                $('.responsive-menu-button-text').show();
            }
        },
        setButtonTextOpen: function() {
            if($('.responsive-menu-button-text').length > 0 && $('.responsive-menu-button-text-open').length > 0) {
                $('.responsive-menu-button-text').hide();
                $('.responsive-menu-button-text-open').show();
            }
        },
        triggerMenu: function() {
            this.isOpen ? this.closeMenu() : this.openMenu();
        },
        triggerSubArrow: function(subarrow) {
            var sub_menu = $(subarrow).parent().siblings('.responsive-menu-submenu');
            var self = this;
            if(this.accordion == 'on') {
                /* Get Top Most Parent and the siblings */
                var top_siblings = sub_menu.parents('.responsive-menu-item-has-children').last().siblings('.responsive-menu-item-has-children');
                var first_siblings = sub_menu.parents('.responsive-menu-item-has-children').first().siblings('.responsive-menu-item-has-children');
                /* Close up just the top level parents to key the rest as it was */
                top_siblings.children('.responsive-menu-submenu').slideUp(self.subMenuTransitionTime, 'linear').removeClass('responsive-menu-submenu-open');
                /* Set each parent arrow to inactive */
                top_siblings.each(function() {
                    $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
                    $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
                });
                /* Now Repeat for the current item siblings */
                first_siblings.children('.responsive-menu-submenu').slideUp(self.subMenuTransitionTime, 'linear').removeClass('responsive-menu-submenu-open');
                first_siblings.each(function() {
                    $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
                    $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
                });
            }
            if(sub_menu.hasClass('responsive-menu-submenu-open')) {
                sub_menu.slideUp(self.subMenuTransitionTime, 'linear').removeClass('responsive-menu-submenu-open');
                $(subarrow).html(this.inactiveArrow);
                $(subarrow).removeClass('responsive-menu-subarrow-active');
            } else {
                sub_menu.slideDown(self.subMenuTransitionTime, 'linear').addClass('responsive-menu-submenu-open');
                $(subarrow).html(this.activeArrow);
                $(subarrow).addClass('responsive-menu-subarrow-active');
            }
        },
        menuHeight: function() {
            return $(this.container).height();
        },
        menuWidth: function() {
            return $(this.container).width();
        },
        wrapperHeight: function() {
            return $(this.wrapper).height();
        },
        setWrapperTranslate: function() {
            switch(this.animationSide) {
                case 'left':
                    translate = 'translateX(' + this.menuWidth() + 'px)'; break;
                case 'right':
                    translate = 'translateX(-' + this.menuWidth() + 'px)'; break;
                case 'top':
                    translate = 'translateY(' + this.wrapperHeight() + 'px)'; break;
                case 'bottom':
                    translate = 'translateY(-' + this.menuHeight() + 'px)'; break;
            }
            if(this.animationType == 'push') {
                $(this.pageWrapper).css({'transform':translate});
                $('html, body').css('overflow-x', 'hidden');
            }
            if(this.pushButton == 'on') {
                $('#responsive-menu-button').css({'transform':translate});
            }
        },
        clearWrapperTranslate: function() {
            var self = this;
            if(this.animationType == 'push') {
                $(this.pageWrapper).css({'transform':''});
                setTimeout(function() {
                    $('html, body').css('overflow-x', '');
                }, self.animationSpeed);
            }
            if(this.pushButton == 'on') {
                $('#responsive-menu-button').css({'transform':''});
            }
        },
        init: function() {
            var self = this;
            $(this.trigger).on(this.triggerTypes, function(e){
                e.stopPropagation();
                self.triggerMenu();
            });
            $(this.trigger).mouseup(function(){
                $(self.trigger).blur();
            });
            $('.responsive-menu-subarrow').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                self.triggerSubArrow(this);
            });
            $(window).resize(function() {
                if($(window).width() > self.breakpoint) {
                    if(self.isOpen){
                        self.closeMenu();
                    }
                } else {
                    if($('.responsive-menu-open').length>0){
                        self.setWrapperTranslate();
                    }
                }
            });
            if(this.closeOnLinkClick == 'on') {
                $(this.linkElement).on('click', function(e) {
                    e.preventDefault();
                    /* Fix for when close menu on parent clicks is on */
                    if(self.itemTriggerSubMenu == 'on' && $(this).is('.responsive-menu-item-has-children > ' + self.linkElement)) {
                        return;
                    }
                    old_href = $(this).attr('href');
                    old_target = typeof $(this).attr('target') == 'undefined' ? '_self' : $(this).attr('target');
                    if(self.isOpen) {
                        if($(e.target).closest('.responsive-menu-subarrow').length) {
                            return;
                        }
                        self.closeMenu();
                        setTimeout(function() {
                            window.open(old_href, old_target);
                        }, self.animationSpeed);
                    }
                });
            }
            if(this.closeOnBodyClick == 'on') {
                $(document).on('click', 'body', function(e) {
                    if(self.isOpen) {
                        if($(e.target).closest('#responsive-menu-container').length || $(e.target).closest('#responsive-menu-button').length) {
                            return;
                        }
                    }
                    self.closeMenu();
                });
            }
            if(this.itemTriggerSubMenu == 'on') {
                $('.responsive-menu-item-has-children > ' + this.linkElement).on('click', function(e) {
                    e.preventDefault();
                    self.triggerSubArrow($(this).children('.responsive-menu-subarrow').first());
                });
            }            if (jQuery('#responsive-menu-button').css('display') != 'none') {
                $('#responsive-menu-button,#responsive-menu a.responsive-menu-item-link, #responsive-menu-wrapper input').focus( function() {
                    $(this).addClass('is-active');
                    $('html').addClass('responsive-menu-open');
                    $('#responsive-menu li').css({"opacity": "1", "margin-left": "0"});
                });

                $('#responsive-menu-button, a.responsive-menu-item-link,#responsive-menu-wrapper input').focusout( function() {
                    if ( $(this).last('#responsive-menu-button a.responsive-menu-item-link') ) {
                        $(this).removeClass('is-active');
                        $('html').removeClass('responsive-menu-open');
                    }
                });
            }            $('#responsive-menu a.responsive-menu-item-link').keydown(function(event) {
                console.log( event.keyCode );
                if ( [13,27,32,35,36,37,38,39,40].indexOf( event.keyCode) == -1) {
                    return;
                }
                var link = $(this);
                switch(event.keyCode) {
                    case 13:                        link.click();
                        break;
                    case 27:                        var dropdown = link.parent('li').parents('.responsive-menu-submenu');
                        if ( dropdown.length > 0 ) {
                            dropdown.hide();
                            dropdown.prev().focus();
                        }
                        break;

                    case 32:                        var dropdown = link.parent('li').find('.responsive-menu-submenu');
                        if ( dropdown.length > 0 ) {
                            dropdown.show();
                            dropdown.find('a, input, button, textarea').first().focus();
                        }
                        break;

                    case 35:                        var dropdown = link.parent('li').find('.responsive-menu-submenu');
                        if ( dropdown.length > 0 ) {
                            dropdown.hide();
                        }
                        $(this).parents('#responsive-menu').find('a.responsive-menu-item-link').filter(':visible').last().focus();
                        break;
                    case 36:                        var dropdown = link.parent('li').find('.responsive-menu-submenu');
                        if( dropdown.length > 0 ) {
                            dropdown.hide();
                        }
                        $(this).parents('#responsive-menu').find('a.responsive-menu-item-link').filter(':visible').first().focus();
                        break;
                    case 37:
                    case 38:
                        event.preventDefault();
                        event.stopPropagation();                        if ( link.parent('li').prevAll('li').filter(':visible').first().length == 0) {
                            link.parent('li').nextAll('li').filter(':visible').last().find('a').first().focus();
                        } else {
                            link.parent('li').prevAll('li').filter(':visible').first().find('a').first().focus();
                        }
                        break;
                    case 39:
                    case 40:
                        event.preventDefault();
                        event.stopPropagation();                        if( link.parent('li').nextAll('li').filter(':visible').first().length == 0) {
                            link.parent('li').prevAll('li').filter(':visible').last().find('a').first().focus();
                        } else {
                            link.parent('li').nextAll('li').filter(':visible').first().find('a').first().focus();
                        }
                        break;
                }
            });
        }
    };
    ResponsiveMenu.init();
});</script></head><body class="page-template-default page page-id-1527 et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns1 et_cover_background et_pb_gutter et_pb_gutters2 et_pb_pagebuilder_layout et_no_sidebar et_divi_theme et-db et_minified_js et_minified_css responsive-menu-slide-left error404 pp404-std"><div id="page-container"><header data-height-onload="66" id="main-header"><div class="container clearfix et_menu_container"><div class="logo_container"> <span class="logo_helper"></span> <a href="https://raredesign.ca/"> <noscript><img alt="Rare Design Group" data-height-percentage="54" src="https://cdn.shortpixel.ai/client/q_glossy,ret_img/https://raredesign.ca/wp-content/uploads/2020/05/rare-design-group-logo_1@3x.png"/></noscript><img alt="Rare Design Group" class="lazyload" data-height-percentage="54" data-src="https://cdn.shortpixel.ai/client/q_glossy,ret_img/https://raredesign.ca/wp-content/uploads/2020/05/rare-design-group-logo_1@3x.png" id="logo" src="https://cdn.shortpixel.ai/client/q_lqip,ret_wait/https://raredesign.ca/wp-content/uploads/2020/05/rare-design-group-logo_1@3x.png"/> </a></div><div data-fixed-height="40" data-height="66" id="et-top-navigation"><nav id="top-menu-nav"><ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a href="https://raredesign.ca/work/">Work</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1502" id="menu-item-1502"><a href="https://raredesign.ca/ideas/">Ideas</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://raredesign.ca/about/">About</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://raredesign.ca/contact/">Contact</a></li></ul></nav><div id="et_mobile_nav_menu"><div class="mobile_nav closed"> <span class="select_page">Select Page</span> <span class="mobile_menu_bar mobile_menu_bar_toggle"></span></div></div></div></div><div class="et_search_outer"><div class="container et_search_form_container"><form action="https://raredesign.ca/" class="et-search-form" method="get" role="search"> <input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/></form> <span class="et_close_search_field"></span></div></div></header><div id="et-main-area"><div id="main-content"><article class="post-1527 page type-page status-publish hentry" id="post-1527"><div class="entry-content"><div class="et-boc" id="et-boc"><div class="et-l et-l--post"><div class="et_builder_inner_content et_pb_gutters2"><div class="et_pb_section et_pb_section_0 et_section_regular"><div class="et_pb_row et_pb_row_0"><div class="et_pb_column et_pb_column_4_4 et_pb_column_0 et_pb_css_mix_blend_mode_passthrough et-last-child"><div class="et_pb_module et_pb_text et_pb_text_0 et_pb_text_align_left et_pb_bg_layout_light"><div class="et_pb_text_inner"><h1>404 Error</h1></div></div><div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div><div class="et_pb_module et_pb_text et_pb_text_1 et_pb_text_align_left et_pb_bg_layout_light"><div class="et_pb_text_inner">This link is broken or the page has been removed. Try these pages instead:<p><a href="/" title="Home">Home</a><br/> <a href="/work/" title="Work">Work</a><br/> <a href="/ideas/" title="Ideas">Ideas</a><br/> <a href="/about/" title="About">About</a><br/> <a href="/contact/" title="Contact">Contact</a></p><p> </p></div></div></div></div></div></div></div></div></div></article></div><footer id="main-footer"><div class="container"><div class="clearfix" id="footer-widgets"><div class="footer-widget"><div class="widget_text fwidget et_pb_widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget">RARE DESIGN GROUP <a class="raretel underline" href="tel:+6045054100">604.505.4100</a><a class="underline" href="mailto:hello@raredesign.ca">hello@raredesign.ca</a><br/><a class="underline" href="https://goo.gl/maps/5dttRAvKfzkKdYGT7">200–3702 West 10th Avenue, Vancouver BC   V6R 2G4</a> <br/> <a href="https://www.linkedin.com/company/rare-design-group/" rel="noopener" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a> <a href="https://twitter.com/eyeofthesquid" rel="noopener" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a> <a href="https://www.instagram.com/raredesigngroup/" rel="noopener" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></div></div></div></div></div><div id="footer-bottom"><div class="container clearfix"></div></div></footer></div></div><div id="aioseo-admin"></div><noscript><style>.lazyload{display:none;}</style></noscript><script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script><script async="" data-noptimize="1" src="https://raredesign.ca/wp-content/plugins/autoptimize/classes/external/js/lazysizes.min.js?ao_version=2.8.1"></script><script data-noptimize="1">function c_img(a,b){src="avif"==b?"data:image/avif;base64,AAAAIGZ0eXBhdmlmAAAAAGF2aWZtaWYxbWlhZk1BMUIAAADybWV0YQAAAAAAAAAoaGRscgAAAAAAAAAAcGljdAAAAAAAAAAAAAAAAGxpYmF2aWYAAAAADnBpdG0AAAAAAAEAAAAeaWxvYwAAAABEAAABAAEAAAABAAABGgAAABoAAAAoaWluZgAAAAAAAQAAABppbmZlAgAAAAABAABhdjAxQ29sb3IAAAAAamlwcnAAAABLaXBjbwAAABRpc3BlAAAAAAAAAAEAAAABAAAAEHBpeGkAAAAAAwgICAAAAAxhdjFDgQ0MAAAAABNjb2xybmNseAACAAIAAYAAAAAXaXBtYQAAAAAAAAABAAEEAQKDBAAAACJtZGF0EgAKCBgADsgQEAwgMgwf8AAAWAAAAACvJ+o=":"data:image/webp;base64,UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==";var c=new Image;c.onload=function(){var d=0<c.width&&0<c.height;a(d,b)},c.onerror=function(){a(!1,b)},c.src=src}function s_img(a,b){w=window,"avif"==b?!1==a?c_img(s_img,"webp"):w.ngImg="avif":!1==a?w.ngImg=!1:w.ngImg="webp"}c_img(s_img,"avif");document.addEventListener("lazybeforeunveil",function({target:a}){window.ngImg&&["data-src","data-srcset"].forEach(function(b){attr=a.getAttribute(b),null!==attr&&-1==attr.indexOf("/client/to_")&&a.setAttribute(b,attr.replace(/\/client\//,"/client/to_"+window.ngImg+","))})});</script><button aria-label="Menu" class="responsive-menu-button responsive-menu-boring responsive-menu-accessible" id="responsive-menu-button" tabindex="1" type="button"> <span class="responsive-menu-box"><span class="responsive-menu-inner"></span> </span></button><div class="slide-left" id="responsive-menu-container"><div aria-label="mobile-menu" id="responsive-menu-wrapper" role="navigation"><div id="responsive-menu-additional-content"><br/> <br/></div><ul aria-label="mobile-menu" id="responsive-menu" role="menubar"><li class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-home responsive-menu-item" id="responsive-menu-item-227" role="none"><a class="responsive-menu-item-link" href="https://raredesign.ca/" role="menuitem" tabindex="1">Home</a></li><li class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item" id="responsive-menu-item-230" role="none"><a class="responsive-menu-item-link" href="https://raredesign.ca/work/" role="menuitem" tabindex="1">Work</a></li><li class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item" id="responsive-menu-item-1501" role="none"><a class="responsive-menu-item-link" href="https://raredesign.ca/ideas/" role="menuitem" tabindex="1">Ideas</a></li><li class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item" id="responsive-menu-item-228" role="none"><a class="responsive-menu-item-link" href="https://raredesign.ca/about/" role="menuitem" tabindex="1">About</a></li><li class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item" id="responsive-menu-item-229" role="none"><a class="responsive-menu-item-link" href="https://raredesign.ca/contact/" role="menuitem" tabindex="1">Contact</a></li></ul></div></div> <script id="divi-custom-script-js-extra" type="text/javascript">var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/raredesign.ca\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/raredesign.ca\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/raredesign.ca\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"4c198dd481","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"d446b74792","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"1527","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"yes","is_shortcode_tracking":"","tinymce_uri":""}; var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"}; var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];</script> <script defer="" src="https://raredesign.ca/wp-content/cache/autoptimize/js/autoptimize_c0c556c6276d1496f84c139a0ef0dc1e.js"></script></body></html>