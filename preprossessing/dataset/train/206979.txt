<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>beIN SPORTS - World's leading Live Sports TV network - Local Editions</title>
<meta content="beIN SPORTS - World's leading Live Sports TV network. Sports News, Videos Highlights, Live Matches of your favorite sports. Find your country edition." name="description"/>
<meta content="GHPoYfCx_pYFnJKxLp3pKR2rdaq331wKdp6AARiWoxY" name="google-site-verification"/> <meta content="543fa33758cc95e2" name="yandex-verification"/> <meta content="0711B319FAEA585CE706A5FFC4DFFEC1" name="msvalidate.01"/> <meta content="unique_verification_code_provided_by_dailymotion" name="dailymotion-domain-verification"/> <script type="application/ld+json"> { "@context": "http://schema.org", "@type": "WebSite", "url": "https://www.beinsports.com/site-locator", "name": ["bein sports","bein sport"], "image": "https://assets.beinsports.com/beIN_SPORTS.png", "headline" : "beIN SPORTS - World's leading Live Sports TV network - Local Editions", "publisher": { "@type": "Organization", "name": "beIN SPORTS" }, "keywords": "bein sport,bein sports", "mainEntityOfPage": { "@type": "WebPage", "@id": "https://www.beinsports.com/site-locator"}, "inLanguage": [{"@type": "Language", "name": "english", "alternateName": "en"},"en"] } </script>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="//assets.beinsports.com/3.2.8/images/favicon.ico" rel="shortcut icon"/>
<link href="//assets.beinsports.com/3.2.8/styles/bein.css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans|Open+Sans:600" rel="stylesheet" type="text/css"/>
<link href="https://www.beinsports.com/site-locator" rel="canonical"/>
<link href="https://www.beinsports.com/site-locator" hreflang="x-default" rel="alternate"/> <link href="https://www.beinsports.com/ar/" hreflang="ar" rel="alternate"/> <link href="https://www.beinsports.com/en/" hreflang="en" rel="alternate"/> <link href="https://www.beinsports.com/us/" hreflang="en-us" rel="alternate"/> <link href="https://www.beinsports.com/au/" hreflang="en-au" rel="alternate"/> <link href="https://www.beinsports.com/au/" hreflang="en-nz" rel="alternate"/> <link href="https://www.beinsports.com/us-es/" hreflang="es" rel="alternate"/> <link href="https://www.beinsports.com/france/" hreflang="fr" rel="alternate"/> <link href="https://www.beinsports.com/id/" hreflang="id" rel="alternate"/> <link href="https://www.beinsports.com/id-en/" hreflang="en-id" rel="alternate"/> <link href="https://www.beinsports.com/th-en/" hreflang="en-th" rel="alternate"/> <link href="https://www.beinsports.com/th/" hreflang="th" rel="alternate"/> <link href="https://www.beinsports.com/hk/" hreflang="en-hk" rel="alternate"/> <link href="https://www.beinsports.com/ph/" hreflang="en-ph" rel="alternate"/> <link href="https://www.beinsports.com/my/" hreflang="en-my" rel="alternate"/> <link href="https://tr.beinsports.com" hreflang="tr" rel="alternate"/>
<script src="//assets.beinsports.com/3.2.8/locate/js/vendor/modernizr-2.6.2.min.js"></script>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements--><!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style>
        .mejs-controls {display: none !important;}
        .custom-video-controls {
            z-index: 2147483647;
        }
        video::-webkit-media-controls {
            display:none !important;
        }
    </style>
<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-86984118-3', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body class="locator">
<div class="container">
<img class="logo-locator" src="//assets.beinsports.com/3.2.8/images/logo-locator.png"/>
<p>beIN SPORTS isn't yet available for your location.<br/> Please visit one of our local editions:</p>
<ul>
<li><a href="/ar/">Middle East and Africa</a></li>
<li><a href="https://tr.beinsports.com">Turkey</a></li>
<li><a href="/france/">France </a></li>
<li><a href="/id/">Indonesia</a></li>
<li><a href="https://es.beinsports.com/">Spain</a></li>
<li><a href="/th/">Thailand </a></li>
<li><a href="/us/">Canada </a></li>
<li><a href="/hk/">Hong Kong </a></li>
<li><a href="/us/">United States </a></li>
<li><a href="/ph/">Philippines </a></li>
<li><a href="/au/">Australia</a></li>
<li><a href="/my/">Malaysia and Singapore</a></li>
<li><a href="/au/">New Zealand</a></li>
</ul>
<!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>
        <![endif]-->
</div>
<!-- <div class="overlay"></div>-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="//assets.beinsports.com/3.2.8/locate/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="//assets.beinsports.com/3.2.8/locate/js/plugins.js"></script>
<script src="//assets.beinsports.com/3.2.8/locate/js/main.js"></script>
</body>
</html>
