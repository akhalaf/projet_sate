<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet"/>
<title>Between Digital</title>
<style>
    @font-face {
        font-family: "Gilroyextrabold";
        src: url("Gilroyextrabold.ttf");
    }
    @font-face {
        font-family: "Lato-Regular";
        src: url("Lato-Regular.ttf");
    }
    html,body {
    height: 100%;
    margin: 0;
    padding: 0;
    overflow-y: hidden;
    overflow: hidden;
    }
    body {
    background-image: url(dec.svg);
    background-repeat: no-repeat;
    background-size: 100%;
    }
    div {
        display: block;
    }
    .main {
        max-width: 560px;
        width: 100%;
        top: 160px;
        box-sizing: border-box;
        margin: 0 auto;
        overflow: hidden;
        position: relative;
        padding: 0px;
        overflow-y: hidden;
    }
    .betweenXlogo {
        display: flex;
        justify-content: center;
    }
    .mainText h2 {
        font-family: "Gilroyextrabold";
        font-size: 31px;
        line-height: 39px;
        display: flex;
        align-items: center;
        text-align: center;
        margin-block-start: 30px;
        margin-block-end: 50px;
        color: #29344B;
    }
    .group {
        display: flex;
        justify-content: center;
        top: 160px;
        box-sizing: border-box;
        margin: 0 auto;
        overflow: hidden;
        position: relative;
        padding: 0px;
    }
    .gr {
        width: 33.33%;
        float: left;
    }
    .vihub {
        background: url("gr_vihub.png");
        width: 93.89px;
        height: 32px;
        background-size: 93.89px 32px;
        background-repeat: no-repeat;
    }
    .rtb {
        background: url("gr_rtb.png");
        width: 89.86px;
        height: 32px;
        background-size: 89.86px 32px;
        margin-left: 102px;
        background-repeat: no-repeat;
    }
    .intency {
        background: url("gr_intency.png");
        width: 113.33px;
        height: 32px;
        background-size: 113.33px 26px;
        margin-left: 101.14px;
        background-repeat: no-repeat;
    }
    .last {
        top: 210px;
        display: flex;
        justify-content: center;
        box-sizing: border-box;
        margin: 0 auto;
        overflow: hidden;
        position: relative;
        padding: 0px;
    }
    .last h3 {
        font-family: "Lato-Regular";
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 22px;
        display: flex;
        align-items: center;
        text-align: center;
        margin-block-start: 0em;
        margin-block-end: 0em;
        color: #29344B;
    }
    a {
        font-family: "Lato-Regular";
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 22px;
        display: flex;
        align-items: center;
        text-align: center;
        margin-block-start: 0em;
        margin-block-end: 0em;
        color: #29344B; 
    }
</style>
<script type="text/javascript">
            function timer(){
             var obj=document.getElementById('timer_inp');
             obj.innerHTML--;
             if(obj.innerHTML==0){setTimeout(window.location.replace("https://betweenx.com/"));setTimeout(function(){},1000);}
             else{setTimeout(timer,1000);}
            }
            setTimeout(timer,1500);
        </script>
</head>
<body>
<div class="main">
<div class="betweenXlogo"> <img src="BetweenDigital.svg"/></div>
<div class="mainText">
<h2>Ð¢ÐµÐ¿ÐµÑÑ Ð¼Ñ â ÑÐ°ÑÑÑ Ð³ÑÑÐ¿Ð¿Ñ ÐºÐ¾Ð¼Ð¿Ð°Ð½Ð¸Ð¹ Between Exchange</h2>
</div>
</div>
<div class="group">
<div class="gr vihub"></div>
<div class="gr rtb"></div>
<div class="gr intency"></div>
</div>
<div class="last">
<h3>ÐÑ Ð±ÑÐ´ÐµÑÐµ Ð¿ÐµÑÐµÐ°Ð´ÑÐµÑÐ¾Ð²Ð°Ð½Ñ Ð½Ð° <a href="https://betweenx.com/">ÑÐ°Ð¹Ñ</a> ÑÐµÑÐµÐ· <a id="timer_inp">15</a> ÑÐµÐºÑÐ½Ð´</h3>
</div>
</body>
</html>