<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="" name="description"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- Title -->
<title>Punjab Tiles-Manufacturer of Mosaic Tiles &amp; Pavers</title>
<!-- Favicon -->
<link href="img/core-img/favicon.png" rel="icon"/>
<!-- Stylesheet -->
<link href="style.css" rel="stylesheet"/>
</head>
<body>
<!-- Preloader -->
<div class="preloader d-flex align-items-center justify-content-center">
<div class="lds-ellipsis">
<div></div>
<div></div>
<div></div>
<div></div>
</div>
</div>
<!-- ##### Header Area Start ##### -->
<header class="header-area">
<!-- Top Header Area -->
<div class="top-header-area">
<div class="container h-100">
<div class="row h-100 align-items-center">
<div class="col-12 d-flex justify-content-between">
<!-- Logo Area -->
<div class="logo">
<a href="index.html"><img alt="" src="img/core-img/logo-credit.png"/></a>
</div>
<!-- Top Contact Info -->
<div class="top-contact-info d-flex align-items-center">
<a data-placement="bottom" data-toggle="tooltip" href="#" title="43-Tipu Block (LDA), New Garden Town, Lahore."><img alt="" src="img/core-img/placeholder.png"/> <span>43-Tipu Block (LDA), New Garden Town, Lahore.</span></a>
<a data-placement="bottom" data-toggle="tooltip" href="#" title="info@punjabtiles.com"><img alt="" src="img/core-img/message.png"/> <span>info@punjabtiles.com</span></a>
</div>
</div>
</div>
</div>
</div>
<!-- Navbar Area -->
<div class="credit-main-menu" id="sticker">
<div class="classy-nav-container breakpoint-off">
<div class="container">
<!-- Menu -->
<nav class="classy-navbar justify-content-between" id="creditNav">
<!-- Navbar Toggler -->
<div class="classy-navbar-toggler">
<span class="navbarToggler"><span></span><span></span><span></span></span>
</div>
<!-- Menu -->
<div class="classy-menu">
<!-- Close Button -->
<div class="classycloseIcon">
<div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
</div>
<!-- Nav Start -->
<div class="classynav">
<ul>
<li><a href="index.html">Home</a></li>
<li><a href="about.html">About Us</a></li>
<li><a href="#">Products</a>
<ul class="dropdown">
<li><a href="terrazzo-tiles.html">Terrazzo Tiles</a></li>
<li><a href="paving-stones.html">Paving Stones</a></li>
<li><a href="kerb-stones.html">Kerb Stones</a></li>
<li><a href="hollow-blocks.html">Hollow Blocks</a></li>
</ul>
</li>
<li><a href="contact.html">Contact</a></li>
</ul>
</div>
<!-- Nav End -->
</div>
<!-- Contact -->
<div class="contact">
<a href="#"><img alt="" src="img/core-img/call2.png"/> +92 42 35841053</a>
</div>
</nav>
</div>
</div>
</div>
</header>
<!-- ##### Header Area End ##### -->
<!-- ##### Hero Area Start ##### -->
<div class="hero-area">
<div class="hero-slideshow owl-carousel">
<!-- Single Slide -->
<div class="single-slide bg-img">
<!-- Background Image-->
<div class="slide-bg-img bg-img bg-overlay" style="background-image: url(img/bg-img/Slider-1.jpg);"></div>
<!-- Welcome Text -->
<div class="container h-100">
<div class="row h-100 align-items-center justify-content-center">
<div class="col-12 col-lg-9">
<div class="welcome-text text-center">
<h2 data-animation="fadeInUp" data-delay="300ms">Over <span>25 Years</span> of Experience</h2>
<p data-animation="fadeInUp" data-delay="500ms">We will supply all the Terrazzo essentials to help you complete every job on time on bugdet and on the mark.</p>
<a class="btn credit-btn mt-50" data-animation="fadeInUp" data-delay="700ms" href="#">Discover</a>
</div>
</div>
</div>
</div>
<!-- Slide Duration Indicator -->
<div class="slide-du-indicator"></div>
</div>
<!-- Single Slide -->
<div class="single-slide bg-img">
<!-- Background Image-->
<div class="slide-bg-img bg-img bg-overlay" style="background-image: url(img/bg-img/Slider-2.jpg);"></div>
<!-- Welcome Text -->
<div class="container h-100">
<div class="row h-100 align-items-center justify-content-center">
<div class="col-12 col-lg-9">
<div class="welcome-text text-center">
<h2 data-animation="fadeInDown" data-delay="300ms">Aesthetically <span>Advanced</span> Flooring Solution</h2>
<p data-animation="fadeInDown" data-delay="500ms">Manufacturing of our products is done using state of art technology and break through automated machinery.</p>
<a class="btn credit-btn mt-50" data-animation="fadeInDown" data-delay="700ms" href="#">Discover</a>
</div>
</div>
</div>
</div>
<!-- Slide Duration Indicator -->
<div class="slide-du-indicator"></div>
</div>
<!-- Single Slide -->
<div class="single-slide bg-img">
<!-- Background Image-->
<div class="slide-bg-img bg-img bg-overlay" style="background-image: url(img/bg-img/Slider-3.jpg);"></div>
<!-- Welcome Text -->
<div class="container h-100">
<div class="row h-100 align-items-center justify-content-center">
<div class="col-12 col-lg-9">
<div class="welcome-text text-center">
<h2 data-animation="fadeInUp" data-delay="300ms">Numerous <span>Customization</span> Options</h2>
<p data-animation="fadeInUp" data-delay="500ms">Create the perfect Designs. Choose a sample and build your own.</p>
<a class="btn credit-btn mt-50" data-animation="fadeInUp" data-delay="700ms" href="#">Discover</a>
</div>
</div>
</div>
</div>
<!-- Slide Duration Indicator -->
<div class="slide-du-indicator"></div>
</div>
<!-- Single Slide -->
<div class="single-slide bg-img">
<!-- Background Image-->
<div class="slide-bg-img bg-img bg-overlay" style="background-image: url(img/bg-img/Slider-4.jpg);"></div>
<!-- Welcome Text -->
<div class="container h-100">
<div class="row h-100 align-items-center justify-content-center">
<div class="col-12 col-lg-9">
<div class="welcome-text text-center">
<h2 data-animation="fadeInDown" data-delay="300ms">Terrazzo <span>Stairsteps</span> Available</h2>
<p data-animation="fadeInDown" data-delay="500ms">We offers various traditional as well as modern sizes and shapes. Our experienced representative values the opportunity to be included in discussions, regarding designing and other specifications</p>
<a class="btn credit-btn mt-50" data-animation="fadeInDown" data-delay="700ms" href="#">Discover</a>
</div>
</div>
</div>
</div>
<!-- Slide Duration Indicator -->
<div class="slide-du-indicator"></div>
</div>
</div>
</div>
<!-- ##### Hero Area End ##### -->
<!-- ##### Features Area Start ###### -->
<section class="features-area section-padding-100-0">
<div class="container">
<div class="row align-items-end">
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-features-area mb-100 wow fadeInUp" data-wow-delay="100ms">
<!-- Section Heading -->
<div class="section-heading">
<div class="line"></div>
<p>Take look at our</p>
<h2>Our Products</h2>
</div>
<h6>Our products use the highest amount of recycled and low VOC materials into their composition as possible. Many of our products feature post-industrial recycled content.</h6>
<a class="btn credit-btn mt-50" href="#">Discover</a>
</div>
</div>
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-features-area mb-100 wow fadeInUp" data-wow-delay="300ms">
<img alt="" src="img/bg-img/lahore-metro-recent.jpg"/>
<h5>Terrazzo Tiles)</h5>
</div>
</div>
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-features-area mb-100 wow fadeInUp" data-wow-delay="500ms">
<img alt="" src="img/bg-img/paving-stones.jpg"/>
<h5>Paving Stones</h5>
</div>
</div>
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-features-area mb-100 wow fadeInUp" data-wow-delay="700ms">
<img alt="" src="img/bg-img/kerb-stones.jpg"/>
<h5>Kerb Stones</h5>
</div>
</div>
</div>
</div>
</section>
<!-- ##### Features Area End ###### -->
<!-- ##### Call To Action Start ###### -->
<section class="cta-area d-flex flex-wrap">
<!-- Cta Thumbnail -->
<div class="cta-thumbnail bg-img jarallax" style="background-image: url(img/bg-img/terrazzo-front.jpg);"></div>
<!-- Cta Content -->
<div class="cta-content">
<!-- Section Heading -->
<div class="section-heading white">
<div class="line"></div>
<p>Italian Technology</p>
<h2>Aesthetically Pleasing &amp; Advanced</h2>
</div>
<h6>We make it possible for architects and designers to design a fully customized terrazzo finish without the need to search for materials. We supply all the epoxy, aggregates, and precast to give clients full quality control to design  terrazzo for institutional, commercial and industrial projects.</h6>
<div class="d-flex flex-wrap mt-50">
<!-- Single Skills Area -->
<div class="single-skils-area mb-70 mr-5">
<div class="circle" data-value="0.90" id="circle">
<div class="skills-text">
<span>5000psi</span>
</div>
</div>
<p>Strength</p>
</div>
<!-- Single Skills Area -->
<div class="single-skils-area mb-70 mr-5">
<div class="circle" data-value="0.75" id="circle2">
<div class="skills-text">
<span>75%</span>
</div>
</div>
<p>Power</p>
</div>
<!-- Single Skills Area -->
<div class="single-skils-area mb-70">
<div class="circle" data-value="0.97" id="circle3">
<div class="skills-text">
<span>97%</span>
</div>
</div>
<p>Resource</p>
</div>
</div>
<a class="btn credit-btn box-shadow btn-2" href="#">Read More</a>
</div>
</section>
<!-- ##### Call To Action End ###### -->
<!-- ##### Call To Action Start ###### -->
<section class="cta-2-area wow fadeInUp" data-wow-delay="100ms">
<div class="container">
<div class="row">
<div class="col-12">
<!-- Cta Content -->
<div class="cta-content d-flex flex-wrap align-items-center justify-content-between">
<div class="cta-text">
<h4>Are you looking for a reliable partner for your next construction project?</h4>
</div>
<div class="cta-btn">
<a class="btn credit-btn box-shadow" href="#">Read More</a>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- ##### Call To Action End ###### -->
<!-- ##### Services Area Start ###### -->
<section class="services-area section-padding-100-0">
<div class="container">
<div class="row">
<div class="col-12">
<!-- Section Heading -->
<div class="section-heading text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
<div class="line"></div>
<h2>Why Choose Us?</h2>
</div>
</div>
</div>
<div class="row">
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="200ms">
<div class="icon">
<i class="icon-profits"></i>
</div>
<div class="text">
<h5>Quality Service</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="300ms">
<div class="icon">
<i class="icon-money-1"></i>
</div>
<div class="text">
<h5>Timely Delivery</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="400ms">
<div class="icon">
<i class="icon-coin"></i>
</div>
<div class="text">
<h5>Competitive Pricing</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="500ms">
<div class="icon">
<i class="icon-smartphone-1"></i>
</div>
<div class="text">
<h5>3 Decades of Experience</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="600ms">
<div class="icon">
<i class="icon-diamond"></i>
</div>
<div class="text">
<h5>Large Client Base</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
<!-- Single Service Area -->
<div class="col-12 col-md-6 col-lg-4">
<div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="700ms">
<div class="icon">
<i class="icon-piggy-bank"></i>
</div>
<div class="text">
<h5>Trained Staff</h5>
<p>Morbi ut dapibus dui. Sed ut iaculis elit, quis varius mauris. Integer ut ultricies orci, lobortis egestas sem.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- ##### Services Area End ###### -->
<!-- ##### Footer Area Start ##### -->
<footer class="footer-area section-padding-100-0">
<div class="container">
<div class="row">
<!-- Single Footer Widget -->
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-footer-widget mb-100">
<h5 class="widget-title">About Us</h5>
<!-- Nav -->
<nav>
<p> Punjab Tiles has been a pioneer in the flooring industry right from its establishment over 3 decades ago. In a modern environment, Punjab tiles Pavers feature in pedestrian schemes, urban renewal, dockland redevelopment and Petrol station forecourts.
                            </p>
</nav>
</div>
</div>
<!-- Single Footer <Wi></Wi>dget -->
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-footer-widget mb-100">
<h5 class="widget-title">Products</h5>
<!-- Nav -->
<nav>
<ul>
<li><a href="terrazzo-tiles.html">Terrazzo Tiles</a></li>
<li><a href="paving-stones.html">Paving Stones</a></li>
<li><a href="kerb-stones.html">Kerb Stones</a></li>
<li><a href="hollow-blocks.html">Hollow Blocks</a></li>
</ul>
</nav>
</div>
</div>
<!-- Single Footer Widget -->
<div class="col-12 col-sm-6 col-lg-3">
<div class="single-footer-widget mb-100">
<h5 class="widget-title">Latest News</h5>
<!-- Single News Area -->
<div class="single-latest-news-area d-flex align-items-center">
<div class="news-thumbnail">
<img alt="" src="../../Pictures/Saved Pictures/61632737_427311194517696_8185263772391128430_n.jpg"/>
</div>
<div class="news-content">
<a href="#">How to intall terrazzo tiles?</a>
<div class="news-meta">
<a class="post-author" href="#"><img alt="" src="img/core-img/pencil.png"/> Ali Ghaffar</a>
<a class="post-date" href="#"><img alt="" src="img/core-img/calendar.png"/> April 26</a>
</div>
</div>
</div>
<!-- Single News Area -->
<div class="single-latest-news-area d-flex align-items-center">
<div class="news-thumbnail"> <img alt="" src="../../Pictures/Saved Pictures/60086808_425121451552861_5954296928267352090_n.jpg"/>
</div>
<div class="news-content">
<a href="#">Maintenance of Terrazzo Tiles</a>
<div class="news-meta">
<a class="post-author" href="#"><img alt="" src="img/core-img/pencil.png"/> Ali Ghaffar </a>
<a class="post-date" href="#"><img alt="" src="img/core-img/calendar.png"/> April 26</a>
</div>
</div>
</div>
<!-- Single News Area -->
<div class="single-latest-news-area d-flex align-items-center">
<div class="news-thumbnail"> <img alt="" src="../../Pictures/Saved Pictures/69621605_156813925387488_6455062064724051896_n.jpg"/>
</div>
<div class="news-content">
<a href="#">How durable are terrazzo tiles?</a>
<div class="news-meta">
<a class="post-author" href="#"><img alt="" src="img/core-img/pencil.png"/> Ali Ghaffar </a>
<a class="post-date" href="#"><img alt="" src="img/core-img/calendar.png"/> April 26</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Copywrite Area -->
<div class="copywrite-area">
<div class="container">
<div class="row">
<div class="col-12">
<div class="copywrite-content d-flex flex-wrap justify-content-between align-items-center">
<!-- Footer Logo -->
<a class="footer-logo" href="index.html"><img alt="" src="img/core-img/logo.png"/></a>
<!-- Copywrite Text -->
<p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright ©<script>document.write(new Date().getFullYear());</script> All rights reserved | </a><a href="https://www.facebook.com/Punjabtiles/" target="_blank">Punjab Tiles</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
</div>
</div>
</div>
</div>
</div>
</footer>
<!-- ##### Footer Area End ##### -->
<!-- ##### All Javascript Script ##### -->
<!-- jQuery-2.2.4 js -->
<script src="js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/bootstrap/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="js/plugins/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>
</body>
</html>