<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]--><!--[if IE 9]> <html lang="en" class="ie9"> <![endif]--><!--[if !IE]><!--><html lang="zh-CN"> <!--<![endif]-->
<head>
<title>L-肾上腺素|L-Adrenaline|51-43-4|参数，分子结构式，图谱信息 - 物竞化学品数据库-专业、全面的化学品基础数据库</title>
<!-- Meta -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="29692071f029af5f" property="wb:webmaster"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="物竞化学品数据库是由上海物竞化工科技有限公司开发的一个化学品专用数据库。其中涵盖了数万种化学品，其内容包括化学品的中英文名称、别名、CAS号、用途、物性、结构式、分子式、分子量以及表征图谱等方面的信息" name="description"/>
<meta content="('L-肾上腺素', 'L-Adrenaline', '51-43-4'),化学品,物竞数据库,物竞,化学品数据库" name="keywords"/>
<meta content="http://blog.lizhigang.net" name="author"/>
<meta content="app-id=518583101" name="apple-itunes-app"/>
<meta content="11452437376213530556727" property="qc:admins"/>
<!-- Favicon -->
<link href="http://img2.basechem.org/frontend/favicon.ico" rel="shortcut icon"/>
<!-- CSS Global Compulsory -->
<link href="http://img2.basechem.org/frontend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="http://img2.basechem.org/frontend/css/style.css" rel="stylesheet"/>
<!-- CSS Header and Footer -->
<link href="http://img2.basechem.org/frontend/css/headers/header-v4.css" rel="stylesheet"/>
<link href="http://img2.basechem.org/frontend/css/footers/footer-v1.css" rel="stylesheet"/>
<!-- CSS Implementing Plugins -->
<link href="http://img2.basechem.org/frontend/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<!-- CSS Customization -->
<link href="http://img2.basechem.org/frontend/css/custom.css" rel="stylesheet"/>
<link href="http://img2.basechem.org/frontend/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
<!--=== Header v4 ===-->
<div class="header-v4">
<!-- Topbar -->
<div class="topbar-v1">
<div class="container">
<div class="row">
<div class="col-md-6">
<ul class="list-inline top-v1-contacts">
<li>
<i class="fa fa-envelope"></i> 邮箱: <a href="mailto:wingch@basechem.org">wingch@basechem.org</a>
</li>
<li>
<i class="fa fa-phone"></i> 客服: 400-700-1514
                        </li>
</ul>
</div>
<div class="col-md-6">
<ul class="list-inline top-v1-data">
<li><a href="/"><i class="fa fa-home"></i></a></li>
<li><a href="/account/">我的物竞</a></li>
<li><a href="/help/">帮助中心</a></li>
</ul>
</div>
</div>
</div>
</div>
<!-- End Topbar -->
<!-- Navbar -->
<div class="navbar navbar-default mega-menu" role="navigation">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<div class="row">
<div class="col-md-4">
<a class="navbar-brand" href="/">
<img alt="Logo" src="http://img2.basechem.org/frontend/img/logo-wingch.png"/>
</a>
</div>
<div class="col-md-8 visible-md-block visible-lg-block">
<div class="row">
<div class="col-md-11">
<form action="/search" class="search-block input-group" method="GET">
<input class="form-control input-lg" name="q" placeholder="请输入化学品名称或 CAS No." type="text" value=""/>
<span class="input-group-btn">
<button class="btn-u btn-u-lg" type="submit"><i class="fa fa-search"></i></button>
</span>
<ul class="search-choices">
</ul>
</form>
</div>
<div class="col-md-1">
<a class="search-top" href="/search/adv/">高级搜索</a>
</div>
</div>
<div class="row">
<div class="col-md-12">
<ul class="search-hot list-inline">
<li>热门关键词：</li>
<li><a href="/search?q=N-异丙基丙烯酰胺">N-异丙基丙烯酰胺</a></li>
<li><a href="/search?q=缩水甘油">缩水甘油</a></li>
<li><a href="/search?q=甲基丙烯酸羟乙酯">甲基丙烯酸羟乙酯</a></li>
<li><a href="/search?q=巯基乙酸">巯基乙酸</a></li>
</ul>
</div>
</div>
</div>
</div>
<button class="navbar-toggle" data-target=".navbar-responsive-collapse" data-toggle="collapse" type="button">
<span class="full-width-menu">菜单</span>
<span class="icon-toggle">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</span>
</button>
</div>
</div>
<div class="clearfix"></div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-responsive-collapse">
<div class="container">
<ul class="nav navbar-nav">
<li id="nav-home"><a href="/">首页</a></li>
<li class="active" id="nav-chemical"><a href="/search">化学品</a></li>
<li id="nav-news"><a href="/news/">资讯</a></li>
<li id="nav-mobile"><a href="/mobile/download/">手机应用</a></li>
<li id="nav-special"><a href="/special/">专题</a></li>
<li id="nav-my"><a href="/account/">我的物竞</a></li>
</ul>
</div><!--/end container-->
</div><!--/navbar-collapse-->
</div>
<!-- End Navbar -->
</div>
<!--=== End Header v4 ===-->
<div class="breadcrumbs">
<div class="container">
<span class="pull-left"><h1>L-肾上腺素 <small>L-Adrenaline</small></h1></span>
<ul class="pull-right breadcrumb">
<li><a href="/">首页</a></li>
<li><a href="/search">化学品</a></li>
<li class="active">L-肾上腺素</li>
</ul>
</div>
</div>
<div class="container content">
<div class="row">
<!-- -->
<div class="col-md-9">
<div class="row">
<div class="col-sm-4">
<div class="carousel slide carousel-v1 margin-bottom-10" id="chemical-struct">
<div class="carousel-inner">
<div class="item active">
<div class="thumbnail">
<img alt="L-肾上腺素结构式" src="http://staticv5.basechem.org/img/tupu/old/8e029e99512242fbb0371b77b89a7038.gif"/>
</div>
<div class="carousel-caption">
<p>结构式</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-8">
<table class="summary-table table table-striped table-bordered">
<col width="90"/>
<tbody>
<tr>
<th scope="row">物竞编号</th>
<td>014U</td>
</tr>
<tr>
<th scope="row">分子式</th>
<td>C9H13NO3</td>
</tr>
<tr>
<th scope="row">分子量</th>
<td>183.2</td>
</tr>
<tr>
<th scope="row">标签</th>
<td>
                      
                        (R)-肾上腺素,
                      
                        1-(3,4-二羟基苯基)-2-甲氨基乙醇,
                      
                        3,4-二羟基-α-(甲氨基甲基)苄醇L-肾上腺素,
                      
                        L(-)-肾上腺素,
                      
                        肾副碱,
                      
                        (R)-(−)-3,4-Dihydroxy-α-(methylaminomethyl)benzyl alcohol,
                      
                        Epinephrine,
                      
                        Adnephrin,
                      
                        Epifrina,
                      
                        Suprarenine,
                      
                        Sus-Phrine,
                      
                        Tonogen,
                      
                        Vaponefrin,
                      
                        酶·蛋白质·肽
                      
                  </td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="height:31px">
<div class="tab-v1" id="catlog">
<ul class="nav nav-tabs">
<li class="active"><a href="#bh">编号系统</a></li>
<li><a href="#bz">表征图谱</a></li>
<li><a href="#wx">物性数据</a></li>
<li><a href="#dl">毒理学数据</a></li>
<li><a href="#st">生态学数据</a></li>
<li><a href="#fz">分子结构数据</a></li>
<li><a href="#js">计算化学数据</a></li>
<li class="dropdown pull-right">
<a class="dropdown-toggle" data-toggle="dropdown" href="">更多 <span class="caret"></span></a>
<ul class="dropdown-menu" role="menu">
<li><a href="#xz">性质与稳定性</a></li>
<li><a href="#zc">贮存方法</a></li>
<li><a href="#hc">合成方法</a></li>
<li><a href="#yt">用途</a></li>
<li><a href="#aq">安全信息</a></li>
<li><a href="#document">文献</a></li>
<li><a href="#remark">备注</a></li>
</ul>
</li>
</ul>
</div>
</div>
<div class="c-content">
<h2 id="bh">编号系统</h2>
<p><b>CAS号：</b>51-43-4</p>
<p><b>MDL号：</b>MFCD00002204</p>
<p><b>EINECS号：</b>200-098-7</p>
<p><b>RTECS号：</b>DO2625000</p>
<p><b>BRN号：</b>2368277</p>
<p><b>PubChem号：</b>24894545</p>
<h2 id="wx">物性数据</h2>
<p>
</p><p>1.       性状：白色结晶性粉末。</p><p>2.       密度（g/mL,25/4℃）： 未确定</p><p>3.       相对蒸汽密度（g/mL,空气=1）：未确定</p><p>4.       熔点（ºC）：211-212</p><p>5.       沸点（ºC,常压）：未确定</p><p>6.       沸点（ºC,5.2kPa）：未确定</p><p>7.       折射率：未确定</p><p>8.       闪点（ºC）：未确定 </p><p>9.       比旋光度（º，C=2，0.6mol /L盐酸中）：-50～-53.5 </p><p>10.    自燃点或引燃温度（ºC）：未确定 </p><p>11.    蒸气压（kPa,25ºC）：未确定</p><p>12.    饱和蒸气压（kPa,60ºC）：未确定 </p><p>13.    燃烧热（KJ/mol）：未确定</p><p>14.    临界温度（ºC）：未确定 </p><p>15.    临界压力（KPa）：未确定  </p><p>16.    油水（辛醇/水）分配系数的对数值：未确定  </p><p>17.    爆炸上限（%,V/V）：未确定 </p><p>18.    爆炸下限（%,V/V）：未确定  </p><p>19.    溶解性：易溶于矿酸及氢氧化碱溶液，微溶于水，不溶于醇、醚、丙酮、氯仿。无气味，味苦。露置空气及见光色渐深。</p>
<h2 id="dl">毒理学数据</h2>
<p>
    		暂无
    	</p>
<h2 id="st">生态学数据</h2>
<p>
    		暂无
    	</p>
<h2 id="fz">分子结构数据</h2>
<p>
</p><p>1、  摩尔折射率：49.33</p><p>2、  摩尔体积（cm<sup>3</sup>/mol）：142.6</p><p>3、  等张比容（90.2K）：397.0</p><p>4、  表面张力（dyne/cm）：59.9</p><p>5、  极化率（10-24cm3）：19.55</p>
<h2 id="js">计算化学数据</h2>
<p>
</p><p>1.疏水参数计算参考值（XlogP）:无</p><p>2.氢键供体数量:4</p><p>3.氢键受体数量:4</p><p>4.可旋转化学键数量:3</p><p>5.互变异构体数量:10</p><p>6.拓扑分子极性表面积72.7</p><p>7.重原子数量:13</p><p>8.表面电荷:0</p><p>9.复杂度:154</p><p>10.同位素原子数量:0</p><p>11.确定原子立构中心数量:1</p><p>12.不确定原子立构中心数量:0</p><p>13.确定化学键立构中心数量:0</p><p>14.不确定化学键立构中心数量:0</p><p>15.共价键单元数量:1</p>
<h2 id="xz">性质与稳定性</h2>
<p>
    		暂无
    	</p>
<h2 id="zc">贮存方法</h2>
<p>
</p><p>充氮密封阴凉避光保存。</p>
<h2 id="hc">合成方法</h2>
<p>
</p><p>1.3,4-二羟基-α-甲氨基苯乙酮（即肾上原酮）配制成盐酸盐，在钯类催化下通氢反应。加氢温度为30-35α，压力0.05-0.1MPa，得到的消旋肾上腺素用酒石酸分拆、氢氧化铵中和，得左旋肾上腺素。</p><p>2.<span style="font-family:Arial;">家畜肾上腺髓质中提取、分离、纯化。</span></p><p>3.<span style="font-family:Arial;">人工合成</span></p><p><img alt="" src="http://images.basechem.org/internal/day_101124/201011241644108396.gif"/></p><p> </p>
<h2 id="yt">用途</h2>
<p>
</p><p>1.非甾体激素类药，也用作止血药安络血的中间体。主要用于过敏性休克、支气管哮喘及心搏骤停的抢救</p><p>2.<span style="font-family:Arial;">肾上腺素受体激动药。用于解除支气管哮喘、抢救过敏性休克、与局部麻醉药配伍和局部止血、荨麻疹、枯草热、血清反应等过敏性疾病、青光眼等。</span></p>
<h2 id="aq">安全信息</h2>
<p>危险运输编码：UN2811 6.1/PG 2</p>
<p>危险品标志：<img border="0" src="http://img2.basechem.org/frontend/img/safe/youdu.png" title="T"/>有毒 </p>
<p>安全标识：<a href="/products/safeinfo-1.html" target="_blank">S45</a> <a href="/products/safeinfo-1.html" target="_blank">S36/S37/S39</a> </p>
<p>危险标识：<a href="/products/danger-1.html" target="_blank">R23/24/25</a> </p>
<h2 id="document">文献</h2>
<p>
            暂无
        </p>
<h2 id="remark">备注</h2>
<p>
            暂无
        </p>
<h2 id="bz">表征图谱</h2>
<div class="carousel slide carousel-v1 margin-bottom-40" id="biaozhengtupu">
<div class="carousel-inner">
<div class="item active">
<a class="fancybox" data-rel="bztp" href="http://staticv5.basechem.org/img/tupu/old/f4bce6449fd6451fb117fa99570faf52.gif?x-oss-process=style/0x0" title="质谱图 (1/3)">
<img src="http://staticv5.basechem.org/img/tupu/old/f4bce6449fd6451fb117fa99570faf52.gif?x-oss-process=style/0x0"/>
</a>
<div class="carousel-caption">
<p> 质谱图 (1/3) </p>
</div>
</div>
<div class="item">
<a class="fancybox" data-rel="bztp" href="http://staticv5.basechem.org/img/tupu/old/41e4db9015ab4239b0ca2b8d4e56f231.gif?x-oss-process=style/0x0" title="红外图谱ir1 (2/3)">
<img src="http://staticv5.basechem.org/img/tupu/old/41e4db9015ab4239b0ca2b8d4e56f231.gif?x-oss-process=style/0x0"/>
</a>
<div class="carousel-caption">
<p> 红外图谱ir1 (2/3) </p>
</div>
</div>
<div class="item">
<a class="fancybox" data-rel="bztp" href="http://staticv5.basechem.org/img/tupu/old/f52d0eb26f114309b95707e8775c2d43.gif?x-oss-process=style/0x0" title="红外图谱ir2 (3/3)">
<img src="http://staticv5.basechem.org/img/tupu/old/f52d0eb26f114309b95707e8775c2d43.gif?x-oss-process=style/0x0"/>
</a>
<div class="carousel-caption">
<p> 红外图谱ir2 (3/3) </p>
</div>
</div>
</div>
<div class="carousel-arrow">
<a class="left carousel-control" data-slide="prev" href="#biaozhengtupu">
<i class="fa fa-angle-left"></i>
</a>
<a class="right carousel-control" data-slide="next" href="#biaozhengtupu">
<i class="fa fa-angle-right"></i>
</a>
</div>
</div>
</div>
</div>
<!-- Begin Sidebar -->
<div class="col-md-3" id="sidebar">
<div class="margin-bottom-20">
<div class="headline"><h3>统计数据</h3></div>
<p style="font-size:17px">共收录化学品数据<br/>
<span class="text-right" style="display: block;margin: 15px;"><span class="counter" style="font-size:37px">147579</span> 条</span>
</p>
</div>
<div class="margin-bottom-20">
<div class="headline"><h3>公告</h3><a class="more" href="/news/">更多&gt;</a></div>
<ul class="list-unstyled ul-li-margin-bottom-4">
<li><a href="/news/6/">关于复制粘贴</a></li>
<li><a href="/news/5/">微信升级增加自定义菜单</a></li>
<li><a href="/news/4/">物竞搜索算法更新，更准，更快！</a></li>
<li><a href="/news/3/">物竞数据库升级V5版本</a></li>
<li><a href="/news/130/">物竞搜索算法更新，更准，更快！</a></li>
<li><a href="/news/2/">接近遗传密码符号扩增的生物合成方法：非自然DNA碱基对的分子设计</a></li>
<li><a href="/news/129/">手机端首屏轮播位试运行免费推广！！</a></li>
<li><a href="/news/128/">手机客户端强力更新-离线收藏数据可以自动增加啦！</a></li>
<li><a href="/news/127/">志贺氏菌显色培养基</a></li>
<li><a href="/news/119/">Z-D-丙氨酸</a></li>
</ul>
</div>
<div class="margin-bottom-20">
<div class="headline"><h3>热门化学品</h3><a class="more" href="/hot/">更多&gt;</a></div>
<ul class="list-unstyled ul-li-margin-bottom-4">
<li><a href="/chemical/1">甲醛</a></li>
<li><a href="/chemical/2">巯基乙酸</a></li>
<li><a href="/chemical/3">N,N-二甲基甲酰胺</a></li>
<li><a href="/chemical/4">盐酸胍</a></li>
<li><a href="/chemical/5">地塞米松</a></li>
<li><a href="/chemical/6">醋酸氢化可的松</a></li>
<li><a href="/chemical/7">乙酸可的松</a></li>
<li><a href="/chemical/8">丝裂霉素C</a></li>
<li><a href="/chemical/9">奥芬澳胺</a></li>
<li><a href="/chemical/10">麦角骨化醇</a></li>
</ul>
</div>
<div class="margin-bottom-20">
<div class="headline"><h3>最受关注化学品</h3><a class="more" href="/popular/">更多&gt;</a></div>
<ul class="list-unstyled ul-li-margin-bottom-4">
<li><a href="/chemical/12">乳酸</a></li>
<li><a href="/chemical/13">皮质酮</a></li>
<li><a href="/chemical/14">炔诺酮</a></li>
<li><a href="/chemical/15">氢化可的松</a></li>
<li><a href="/chemical/16">维生素A</a></li>
<li><a href="/chemical/17">泼尼松龙</a></li>
<li><a href="/chemical/18">对甲苯硫酰苯胺</a></li>
<li><a href="/chemical/19">雌三醇</a></li>
<li><a href="/chemical/20">磺胺嘧啶</a></li>
<li><a href="/chemical/21">1,4-双(三氯甲基)苯</a></li>
</ul>
</div>
<div class="margin-bottom-20">
<div class="headline"><h3>最新录入化学品</h3><a class="more" href="/new/">更多&gt;</a></div>
<ul class="list-unstyled ul-li-margin-bottom-4">
<li><a href="/chemical/22">β-雌二醇</a></li>
<li><a href="/chemical/23">D-环丝氨酸</a></li>
<li><a href="/chemical/24">次黄嘌呤</a></li>
<li><a href="/chemical/25">N-乙酰－L-脯氨酸</a></li>
<li><a href="/chemical/26">17α-羟孕酮</a></li>
<li><a href="/chemical/27">米帕林二盐酸盐</a></li>
<li><a href="/chemical/28">滴滴涕</a></li>
<li><a href="/chemical/29">氯普马嗪盐酸</a></li>
<li><a href="/chemical/30">氟奋乃静</a></li>
<li><a href="/chemical/31">2,6-二氯苯甲酸</a></li>
</ul>
</div>
<dl class="dl-horizontal">
<dt><a href="#"><img alt="" src="http://img2.basechem.org/frontend/img/weixin.jpg"/></a></dt>
<dd>
<p><br/>微信扫一扫<br/>关注：物竞化学品数据库</p>
</dd>
</dl>
<div class="devider devider-dotted margin-bottom-20"></div>
<dl class="dl-horizontal">
<dt><a href="#"><img alt="" src="http://img2.basechem.org/frontend/img/weixin.jpg"/></a></dt>
<dd>
<p><br/>物竟数据库 手机版<br/>国内首款化工类专业手机应用</p>
</dd>
</dl>
<div class="devider devider-dotted margin-bottom-20"></div>
<dl class="dl-horizontal">
<dt><a href="#"><img alt="" src="http://img2.basechem.org/frontend/img/weixin.jpg"/></a></dt>
<dd>
<p><br/>微博账号<br/>wjhxp</p>
</dd>
</dl>
<div class="btn-group btn-group-justified">
<a class="col-xs-6 btn-u btn-u-blue" href="mailto:wingch@basechem.org" type="button"><i class="fa fa-envelope-square fa-lg"></i> 我要投稿</a>
<a class="col-xs-6 btn-u btn-success" href="mailto:wingch@basechem.org" type="button"><i class="fa fa-times fa-lg"></i> 我要纠错</a>
</div>
</div>
<!-- End Sidebar -->
</div>
</div><!--/container-->
<!--=== Footer Version 1 ===-->
<div class="footer-v1">
<div class="footer">
<div class="container">
<div class="headline"><h2>快速导航</h2></div>
<div class="quick-nav">
<p>化学品:
                    
                        <a href="/chemical/nav/a/" title="化学品：A">A</a>
<a href="/chemical/nav/b/" title="化学品：B">B</a>
<a href="/chemical/nav/c/" title="化学品：C">C</a>
<a href="/chemical/nav/d/" title="化学品：D">D</a>
<a href="/chemical/nav/e/" title="化学品：E">E</a>
<a href="/chemical/nav/f/" title="化学品：F">F</a>
<a href="/chemical/nav/g/" title="化学品：G">G</a>
<a href="/chemical/nav/h/" title="化学品：H">H</a>
<a href="/chemical/nav/i/" title="化学品：I">I</a>
<a href="/chemical/nav/j/" title="化学品：J">J</a>
<a href="/chemical/nav/k/" title="化学品：K">K</a>
<a href="/chemical/nav/l/" title="化学品：L">L</a>
<a href="/chemical/nav/m/" title="化学品：M">M</a>
<a href="/chemical/nav/n/" title="化学品：N">N</a>
<a href="/chemical/nav/o/" title="化学品：O">O</a>
<a href="/chemical/nav/p/" title="化学品：P">P</a>
<a href="/chemical/nav/q/" title="化学品：Q">Q</a>
<a href="/chemical/nav/r/" title="化学品：R">R</a>
<a href="/chemical/nav/s/" title="化学品：S">S</a>
<a href="/chemical/nav/t/" title="化学品：T">T</a>
<a href="/chemical/nav/u/" title="化学品：U">U</a>
<a href="/chemical/nav/v/" title="化学品：V">V</a>
<a href="/chemical/nav/w/" title="化学品：W">W</a>
<a href="/chemical/nav/x/" title="化学品：X">X</a>
<a href="/chemical/nav/y/" title="化学品：Y">Y</a>
<a href="/chemical/nav/z/" title="化学品：Z">Z</a>
<a href="/chemical/nav/0/" title="化学品：0">0</a>
<a href="/chemical/nav/1/" title="化学品：1">1</a>
<a href="/chemical/nav/2/" title="化学品：2">2</a>
<a href="/chemical/nav/3/" title="化学品：3">3</a>
<a href="/chemical/nav/4/" title="化学品：4">4</a>
<a href="/chemical/nav/5/" title="化学品：5">5</a>
<a href="/chemical/nav/6/" title="化学品：6">6</a>
<a href="/chemical/nav/7/" title="化学品：7">7</a>
<a href="/chemical/nav/8/" title="化学品：8">8</a>
<a href="/chemical/nav/9/" title="化学品：9">9</a>
</p>
<p>CAS号:
                    
                        <a href="/chemical/cas/1/" title="CAS号：1">1</a>
<a href="/chemical/cas/2/" title="CAS号：2">2</a>
<a href="/chemical/cas/3/" title="CAS号：3">3</a>
<a href="/chemical/cas/4/" title="CAS号：4">4</a>
<a href="/chemical/cas/5/" title="CAS号：5">5</a>
<a href="/chemical/cas/6/" title="CAS号：6">6</a>
<a href="/chemical/cas/7/" title="CAS号：7">7</a>
<a href="/chemical/cas/8/" title="CAS号：8">8</a>
<a href="/chemical/cas/9/" title="CAS号：9">9</a>
</p>
</div>
<div class="row">
<!-- About -->
<div class="col-md-4 md-margin-bottom-40">
<div class="headline"><h2>关于物竞</h2></div>
<p class="margin-bottom-25 md-margin-bottom-40">物竞数据库是一个全面、专业、专注，并且<strong>免费</strong>的中文化学品信息库，为学生、学者、化学品研究机构、检测机构、化学品工作者提供专业的化学品平台进行交流。<br/>数据库采用全中文化服务，完全突破了中英文在化学物质命名、化学品俗名、学名等方面的差异，所提供的数据全部中文化，更方便国内从事化学、化工、材料、生物、环境等化学相关行业的工作人员查询使用。</p>
</div>
<!-- End About -->
<div class="col-md-4 md-margin-bottom-40">
<!-- Recent Blogs -->
<div class="posts">
<div class="headline"><h2>关注我们</h2></div>
<dl class="dl-horizontal">
<dt><a href="#"><img alt="" src="http://img2.basechem.org/frontend/img/weixin.jpg"/></a></dt>
<dd>
<p><br/><br/>微信账号：<strong>物竞化学品数据库</strong></p>
</dd>
</dl>
<dl class="dl-horizontal">
<dt><a href="#"><img alt="" src="http://img2.basechem.org/frontend/img/weibo.png"/></a></dt>
<dd>
<p><br/><br/>微博账号：<strong>wjhxp</strong></p>
</dd>
</dl>
</div>
<!-- End Recent Blogs -->
</div><!--/col-md-4-->
<div class="col-md-4">
<!-- Contact Us -->
<div class="headline"><h2>联系我们</h2></div>
<p class="md-margin-bottom-40">
                        上海市延长路149号上海大学科技园412室 <br/>
                        公司总机: 021-56389801 <br/>
                        订购电话: 4007001514 <br/>
                        传真电话: 021-56389802 <br/>
                        客服电话: 021-56332350 <br/>
                        电子邮件: <a class="" href="mailto:wingch@basechem.org">wingch@basechem.org</a>
</p>
<!-- End Contact Us -->
</div><!--/col-md-4-->
</div>
</div>
</div><!--/footer-->
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-md-10">
<p>
                        Copyright © 物竞数据库 2009-2021.All Rights Reserved.
                        <a href="/info/basechem/" rel="nofollow">关于数据库</a> | <a href="/info/wingch/" rel="nofollow">关于物竞</a> | <a href="/info/law/" rel="nofollow">法律声明</a> | <a href="/chemical/keywords">热门关键字</a>
</p>
<p>
<a class="text-muted" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=31010602001115" rel="nofollow" target="_blank"><img src="http://img2.basechem.org/frontend/img/ba1.png"/> 沪公网安备 31010602001115号</a>
<a class="text-muted" href="http://www.beian.miit.gov.cn" rel="nofollow" target="_blank">沪ICP备08115995号</a>
</p>
</div>
<!-- Social Links -->
<div class="col-md-2 text-right hidden-xs hidden-sm">
<div class="margin-bottom-25"></div>
<a href="/">
<img alt="Logo" id="logo-footer" src="http://img2.basechem.org/frontend/img/logo-wingch-white.png"/>
</a>
</div>
<!-- End Social Links -->
</div>
</div>
</div><!--/copyright-->
</div>
<!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->
<!-- JS Global Compulsory -->
<script src="http://img2.basechem.org/frontend/plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="http://img2.basechem.org/frontend/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- JS Implementing Plugins -->
<script src="http://img2.basechem.org/frontend/plugins/back-to-top.js" type="text/javascript"></script>
<!-- JS Customization -->
<script src="http://img2.basechem.org/frontend/js/custom.js" type="text/javascript"></script>
<!-- JS Page Level -->
<script src="http://img2.basechem.org/frontend/js/app.js" type="text/javascript"></script>
<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
		});
	</script>
<!--[if lt IE 9]>
	<script src="http://img2.basechem.org/frontend/plugins/respond.js"></script>
	<script src="http://img2.basechem.org/frontend/plugins/html5shiv.js"></script>
	<script src="http://img2.basechem.org/frontend/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->
<script>
		var _hmt = _hmt || [];
		(function() {
		  var hm = document.createElement("script");
		  hm.src = "https://hm.baidu.com/hm.js?f803af401ba54d5798d6c8726535fa70";
		  var s = document.getElementsByTagName("script")[0];
		  s.parentNode.insertBefore(hm, s);
		})();
	</script>
<script src="http://img2.basechem.org/frontend/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="http://img2.basechem.org/frontend/js/plugins/fancy-box.js" type="text/javascript"></script>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"1","bdPos":"right","bdTop":"250"}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
</body>
</html>