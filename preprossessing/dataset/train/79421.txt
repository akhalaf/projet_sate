<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<base href="/"/>
<meta charset="utf-8"/>
<meta content="" name="description"/>
<meta content="width=device-width initial-scale=1.0 maximum-scale=1.0, user-scalable=0" name="viewport"/>
<title>AdventistGiving</title>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,700|Roboto:400,400i,700,700i" rel="stylesheet"/>
<link href="" rel="stylesheet"/>
<base href="/"/>
<meta content="yes" name="mobile-web-app-capable"/><meta content="#fff" name="theme-color"/><meta content="AdventistGiving" name="application-name"/><link href="assets/favicons/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/><link href="assets/favicons/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/><link href="assets/favicons/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/><link href="assets/favicons/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/><link href="assets/favicons/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/><link href="assets/favicons/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/><link href="assets/favicons/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/><link href="assets/favicons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/><link href="assets/favicons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/><meta content="yes" name="apple-mobile-web-app-capable"/><meta content="black-translucent" name="apple-mobile-web-app-status-bar-style"/><meta content="AdventistGiving" name="apple-mobile-web-app-title"/><link href="assets/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/><link href="assets/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/><link href="assets/favicons/favicon.ico" rel="shortcut icon"/><link href="assets/favicons/apple-touch-startup-image-320x460.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-640x920.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-640x1096.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-750x1294.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-1182x2208.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-1242x2148.png" media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-748x1024.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-768x1004.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-1496x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image"/><link href="assets/favicons/apple-touch-startup-image-1536x2008.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image"/><link href="./styles/vendor.2dac6fa13c0dee9cef37.css" rel="stylesheet"/></head>
<body>
<!--if !webpackConfig.metadata.WIREFRAME_MODE
      script(src="#{webpackConfig.apiConfig.baseUrl}v1/client_login_helper.js")-->
<!-- Add your site or application content here-->
<ag-app>
<div class="loading-site-message">Loading…</div>
</ag-app>
<noscript class="no-js-message">
<p>
        If the site does not load you may not have javascript enabled.<br/>
        Unfortunately, AdventistGiving needs javascript :(
      </p>
</noscript>
<script type="text/javascript">!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});</script>
<script type="text/javascript">window.Beacon('init', 'b2bc667c-ef85-47f0-aba6-ca6ab867a3cd');window.Beacon('config', {display: {style: "manual"}});</script>
<script src="polyfills.bundle.d75925b5d3d2a5db5f1c.js" type="text/javascript"></script><script src="vendor.bundle.2dac6fa13c0dee9cef37.js" type="text/javascript"></script><script src="main.chunk.83e879967949136663c9.js" type="text/javascript"></script><script src="styles.chunk.01995aeb873a30e0a9f1.js" type="text/javascript"></script></body>
</html>
