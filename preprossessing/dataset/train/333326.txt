<!DOCTYPE html>
<html class="static common fluid-layout fixed-nav " lang="es-ES">
<head>
<meta charset="utf-8"/>
<title>No encontrado</title>
<link href="/Modules/Contrib.CookieCuttr/Styles/cookiecuttr.min.css" rel="stylesheet" type="text/css"/>
<link href="/Themes/Dayvo.Bootstrap/Styles/site-default.min.css" rel="stylesheet" type="text/css"/>
<link href="/Themes/Dayvo.Bootstrap/Styles/site.css" rel="stylesheet" type="text/css"/>
<script src="/Modules/Orchard.Resources/scripts/jquery.min.js" type="text/javascript"></script>
<script src="/Modules/Orchard.Resources/scripts/jquery.cookie.min.js" type="text/javascript"></script>
<script src="/Modules/Contrib.CookieCuttr/scripts/jquery.cookiecuttr.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="/Core/Shapes/Scripts/html5.js" type="text/javascript"></script>
<![endif]-->
<meta content="Orchard" name="generator"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<link href="/Media/clinicatortosamontalvo/ico/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/Media/clinicatortosamontalvo/ico/favicon.ico" rel="apple-touch-icon"/>
<script>
(function(){let preferences = cookie('cookiePreferences') || '';
if(cookie('cc_cookie_decline') || (!preferences.includes('sta') && !cookie('cc_cookie_accept'))){
window['ga-disable-UA-103566815-1']=true;}})();
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-103566815-1', 'auto');
ga('send', 'pageview');
</script>
<script type="text/javascript">
        (function (d) { d.className = "dyn" + d.className.substring(6, d.className.length); })(document.documentElement);
        window.applicationBaseUrl = "/";
    </script>
<link href="/Media/clinicatortosamontalvo/css/estilo.min.css" rel="stylesheet" type="text/css"/>
<style></style>
</head>
<body class="theme-Dayvo.Bootstrap" id="dayvocms-menu">
<div class="container-fluid">
<div id="layout-wrapper">
<header class="group" id="layout-header">
<div id="header">
<div class="zone zone-header">
<article class="widget-header widget-html-widget widget">
<div>
<div class="row datos-cabecera">
<div class=" col-sm-12">
<p style="color: white; text-align: right;"><i aria-hidden="true" class="fa fa-envelope"></i><span style="color: #ffffff;"> <strong><a class="datos-arriba" href="mailto:recepcion@clinicatortosamontalvo.com" style="color: #ffffff;">recepcion@clinicatortosamontalvo.com</a>  </strong><i aria-hidden="true" class="fa fa-phone"></i> <a class="datos-arriba" href="tel:956 54 05 51" style="color: #ffffff;"><strong>956 54 05 51</strong></a> <a class="enlaces-sociales" style="color: #ffffff;"><strong> <a class="soc_ico_fb ico-soc" href="https://www.facebook.com/Cl%C3%ADnica-Dental-Tortosa-Montalvo-179695122196707/" id="soc_fb" target="blank" title="Facebook"></a>
<a class="soc_ico_twitter ico-soc" href="https://twitter.com/TortosaMontalvo" id="soc_twitter" target="blank" title="Twitter"></a>
<a class="soc_ico_you ico-soc" href="https://www.youtube.com/channel/UCi8gfh958p5DpABr-Eue7qQ" id="soc_you" target="blank" title="Youtube"></a>
<a class="soc_ico_whats ico-soc" href="https://wa.me/34672394207" id="soc_whats" target="blank" title="WhatsApp"></a>
</strong></a></span></p>
</div></div>
</div>
<div>
<div class="row imagencabecera">
<div class=" col-sm-4"></div>
<div class=" col-sm-4">
<p><a href="/"><img alt="" height="223" src="/Media/clinicatortosamontalvo/dayvo/clinica%20tortosamontalvo-1.png" style="display: block; margin-left: auto; margin-right: auto;" width="223"/></a></p>
</div>
<div class=" col-sm-4">
<div class="col-md-12 text-center" style="text-align: center;"><a class="btn btn-success cabecera" href="tel:956 54 05 51"><i aria-hidden="true" class="fa fa-phone"></i> Pide cita</a> </div>
</div></div>
</div>
</article></div>
</div>
</header>
<div class="navbar-wrapper">
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="group" id="layout-navigation">
<div class="zone zone-navigation">
<article class="widget-navigation widget-menu-widget widget">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#main-menu" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse" id="main-menu">
<ul class="nav navbar-nav menu menu-main-menu">
<li class="first"><a class="brand" href="/"><i class="glyphicon glyphicon-home"></i></a></li>
<li class="dropdown first"><a class="dropdown-toggle" data-toggle="dropdown" href="/">Quienes somos <i class="fa fa-angle-down"></i></a> <ul class="dropdown-menu">
<li>
<a href="/dr-francisco-tortosa">Dr. Francisco Tortosa</a>
</li>
<li>
<a href="/dra-myriam-montalvo-dentista-puerto-santa-maria">Dra. Myriam Montalvo</a>
</li>
<li>
<a href="/equipo-profesional-odontologos-cadiz">Equipo profesional</a>
</li>
</ul>
</li>
<li>
<a href="/tratamientos-bucodentales">Tratamientos</a>
</li>
<li>
<a href="/equipamiento-e-instalaciones-clinica-dental">Equipamiento e Instalaciones</a>
</li>
<li>
<a href="/noticias">Casos Clínicos</a>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Galeria <i class="fa fa-angle-down"></i></a> <ul class="dropdown-menu">
<li>
<a href="/galerias/album">Instalaciones</a>
</li>
<li>
<a href="/galerias/equipamiento">Equipamiento</a>
</li>
<li>
<a href="/galerias/personal">Personal</a>
</li>
</ul>
</li>
<li>
<a href="/blog">Blog</a>
</li>
<li class="last">
<a href="/contacto">Contacto</a>
</li>
<li class="menuUserName">
<a href="/Users/Account/LogOn?ReturnUrl=%2Fmenu%2F" rel="nofollow">Iniciar Sesión</a>
</li>
</ul>
</div>
</article></div>
</div>
</div>
</div>
<div class="group" id="layout-featured">
<div class="zone zone-featured">
<article class="widget-featured widget-html-widget widget">
<div>
<div class="row">
<div class=" col-sm-12">
<p class="boton-fijo"><a href="https://api.whatsapp.com/send?phone=34672394207&amp;text=&amp;source"> <img alt="" height="71" src="/Media/clinicatortosamontalvo/dayvo/whatsapp-icon.png" style="float: right;" width="71"/> </a></p>
</div></div>
</div>
</article></div>
</div>
<div id="layout-main-container">
<div class="group" id="layout-main">
<div class="group" id="layout-content">
<div class="group" id="content">
<div class="zone zone-content">
<h1>No encontrado</h1>
<p>La página que esta buscando no existe.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="group" id="layout-footer">
<footer id="footer">
<div class="group" id="footer-sig">
<div class="zone zone-footer">
<article class="widget-footer widget-html-widget widget">
<hr/>
<div style="margin-top: 30px;">
<div class="row">
<div class=" col-sm-6">
<h4 style="text-align: center; font-family: 'Comfortaa', cursive; font-size: 30px; color: #074892;">Contacta con nosotros</h4>
<p style="text-align: center;"><i aria-hidden="true" class="fa fa-phone"></i> <a href="tel:956 54 05 51">956 54 05 51</a><br/><i aria-hidden="true" class="fa fa-envelope"></i> <a href="mailto:recepcion@clinicatortosamontalvo.com">recepcion@clinicatortosamontalvo.com</a><br/><i aria-hidden="true" class="fa fa-map-marker"></i> C/ Virgen de Los Milagros, 26<br/>El Puerto de Sta María<br/>11500 - Cádiz</p>
</div>
<div class=" col-sm-6">
<h4 style="text-align: center; font-family: 'Comfortaa', cursive; font-size: 30px; color: #074892;">¡Síguenos!</h4>
<p style="text-align: center;"><a href="https://www.facebook.com/Cl%C3%ADnica-Dental-Tortosa-Montalvo-179695122196707/" rel="noopener" target="_blank"><img alt="" height="30" src="/Media/clinicatortosamontalvo/dayvo/if_facebook_245987.png" width="30"/></a>  <a href="https://www.youtube.com/channel/UCi8gfh958p5DpABr-Eue7qQ" rel="noopener" target="_blank"><img alt="" height="30" src="/Media/clinicatortosamontalvo/dayvo/if_social-youtube_299074.png" width="30"/></a> <a href="https://twitter.com/TortosaMontalvo"> <img alt="" height="30" src="/Media/clinicatortosamontalvo/dayvo/if_twitter_circle_color_107170.png" width="30"/> </a><a href="https://wa.me/34672394207 " rel="noopener" target="_blank"><img alt="" height="31" src="/Media/clinicatortosamontalvo/dayvo/whatsapp-icon.png" width="31"/></a></p>
<p style="text-align: center;"><strong>(Atención por WhatsApp de lunes a viernes)</strong></p>
</div></div>
<div class="row">
<div class=" col-sm-12">
<h3 class="datos-footer" style="text-align: center;"><a href="tel:956 54 05 51">956 54 05 51</a> - <a href="mailto:recepcion@clinicatortosamontalvo.com">recepcion@clinicatortosamontalvo.com</a></h3>
</div></div>
</div>
</article>
<article class="widget-footer widget-layout-widget widget">
<div>
<div class="row">
<div class=" col-sm-12">
<p style="text-align: center;"><strong><span style="color: #074892;"><a href="/blanqueamiento-dental-cadiz" style="color: #074892;">Blanqueamiento dental Cádiz</a> | <a href="/tratamientos-de-periodoncia-cadiz" style="color: #074892;">Periodoncia Cádiz</a> | <a href="/implantes-dentales-cadiz" style="color: #074892;">Implantes dentales Cádiz</a> | <a href="https://clinicatortosamontalvo.com/clinica-dental-cadiz" style="color: #074892;">Clinica dental Cádiz</a> | <a href="/ortodoncia-invisible-cadiz" style="color: #074892;">Ortodoncia invisible Cádiz</a> | <a href="/dentistas-en-el-puerto-de-santa-maria" style="color: #074892;">Dentistas en el Puerto de Santa María</a> | <a href="https://clinicatortosamontalvo.com/invisalign-cadiz" style="color: #074892;">Invisalign Cádiz</a></span></strong></p>
</div></div>
</div>
</article>
<article class="widget-Cookies widget-footer widget-cookiecuttr-widget widget">
<div aria-hidden="true" aria-labelledby="cookieConfig" class="modal fade" id="cookie-config" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<a class="close" data-dismiss="modal">&amp;times</a>
<h3>Para que propósito se utiliza mi información y quién la utiliza</h3>
</div>
<div class="modal-body">
<div>
<p>
                        Éste sitio utiliza cookies propias y de otras entidades para acceder y utilizar su información para los propósitos descritos abajo. Si no está de acuerdo con ninguno de estos propósitos, puede personalizarlas mas abajo.
                    </p>
</div>
<div style="margin-bottom:25px">
<span>Permite el uso de cookies para lo siguiente</span>
</div>
<div class="container-fluid">
<div class="row">
<div class="col-sm-8 cookie-info">
<label class="cookie-title">Necesarias<span class="fa fa-chevron-circle-down cookie-icon" style="margin-bottom:15px"></span></label>
<div class="cookie-description">
<span>
                                        Estas cookies son esenciales para poder navegar en el sitio y utilizar sus características, como acceder a zonas seguras del sitio. Cookies que permiten que tiendas web mantengan productos en el carrito mientras hace las compras son un ejemplo de cookies necesarias. Estas cookies en general se originan en el mismo sitio.
                                    </span>
</div>
</div>
<div class="col-sm-4">
</div>
</div>
<div class="row">
<div class="col-sm-8 cookie-info">
<label class="cookie-title">Preferencias<span class="fa fa-chevron-circle-down cookie-icon" style=""></span></label>
<div class="cookie-description">
<span>
                                        Estas cookies permiten al sitio recordar las elecciones que ha hecho en el pasado, como el idioma de preferencia, para que región le gustaría obtener el reporte del clima, o su nombre de usuario y contraseña para ingresar automáticamente.
                                    </span>
</div>
</div>
<div class="col-sm-4">
<input class="accept-radio custom-radio" id="accept-2" name="radio-2" type="radio"/>
<label class="radio-label accept-cookie" data-value="pre" for="accept-2">Aceptar</label>
<input class="decline-radio custom-radio" id="decline-2" name="radio-2" type="radio"/>
<label class="radio-label decline-cookie" data-value="pre" for="decline-2">Rechazar</label>
</div>
</div>
<div class="row">
<div class="col-sm-8 cookie-info">
<label class="cookie-title">Estadísticas<span class="fa fa-chevron-circle-down cookie-icon" style=""></span></label>
<div class="cookie-description">
<span>
                                        Estas cookies recolectan información de como se usa el sitio, como las páginas que visita y cuales enlaces se acceden. Esta información no puede ser usada para identificarlo. Todos los datos son agregados y, por lo tanto, anónimos. Su único propósito es mejorar la funcionalidad del sitio. Estas incluyen cookies de servicios de analíticas de terceros.
                                    </span>
</div>
</div>
<div class="col-sm-4">
<input class="accept-radio custom-radio" id="accept-3" name="radio-3" type="radio"/>
<label class="radio-label accept-cookie" data-value="sta" for="accept-3">Aceptar</label>
<input class="decline-radio custom-radio" id="decline-3" name="radio-3" type="radio"/>
<label class="radio-label decline-cookie" data-value="sta" for="decline-3">Rechazar</label>
</div>
</div>
<div class="row">
<div class="col-sm-8 cookie-info">
<label class="cookie-title">Mercadeo<span class="fa fa-chevron-circle-down cookie-icon" style=""></span></label>
<div class="cookie-description">
<span>
                                        Estas cookies hacen seguimiento de su actividad en internet para ayudar a los anunciantes entregar publicidad más relevante o para limitar cuantas veces ve una publicidad. Estas cookies pueden compartir información con otras organizaciones o anunciantes. Estas cookies son persistentes y casi siempre provienen de terceros.
                                    </span>
</div>
</div>
<div class="col-sm-4">
<input class="accept-radio custom-radio" id="accept-4" name="radio-4" type="radio"/>
<label class="radio-label accept-cookie" data-value="mar" for="accept-4">Aceptar</label>
<input class="decline-radio custom-radio" id="decline-4" name="radio-4" type="radio"/>
<label class="radio-label decline-cookie" data-value="mar" for="decline-4">Rechazar</label>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="normal-buttons">
<button id="decline-all" type="button">Rechazar todos</button>
<button id="accept-all" type="button">Aceptar todos</button>
</div>
<div class="save-section hidden">
<label id="save-warning">Establezca todas sus preferencias antes de guardar</label>
<button disabled="" id="save-config" type="button">Guardar</button>
</div>
</div>
</div>
</div>
</div>
</article>
<div class="credits">
    MYRIAM TRINIDAD MONTALVO FERNANDEZ
    <span class="copyright"> 2021</span> |
    <span>
<a href="https://clinicatortosamontalvo.com/condiciones-de-la-lopd">Aviso Legal y Política de Privacidad</a> |
        <a href="https://clinicatortosamontalvo.com/politica-de-cookies">Política de cookies</a>
</span>
</div>
</div>
</div>
</footer>
</div>
<div id="toTop">
<div style="font-size: xx-large;"><i class="fa fa-chevron-up"></i></div>
</div>
</div>
<script src="/Themes/Dayvo.Bootstrap/scripts/bootstrap-3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/Themes/Dayvo.Bootstrap/scripts/hover-dropdown.js" type="text/javascript"></script>
<script src="/Themes/Dayvo.Bootstrap/scripts/validationEngine/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="/Themes/Dayvo.Bootstrap/scripts/validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
<script src="/Themes/Dayvo.Bootstrap/scripts/custom.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function () {
            var cookiesettings = {};
            cookiesettings.cookieAnalytics = true;
            if (cookiesettings.cookieAnalytics) {
                if ('Las cookies de este sitio web se usan para personalizar el contenido y los anuncios, ofrecer funciones de redes sociales y analizar el tr&#225;fico. Adem&#225;s, compartimos informaci&#243;n sobre el uso que haga del sitio web con nuestros partners de redes sociales, publicidad y an&#225;lisis web, quienes pueden combinarla con otra informaci&#243;n que les haya proporcionado o que hayan recopilado a partir del uso que haya hecho de sus servicios.'.length > 0)
                    cookiesettings.cookieAnalyticsMessage = 'Las cookies de este sitio web se usan para personalizar el contenido y los anuncios, ofrecer funciones de redes sociales y analizar el tr&#225;fico. Adem&#225;s, compartimos informaci&#243;n sobre el uso que haga del sitio web con nuestros partners de redes sociales, publicidad y an&#225;lisis web, quienes pueden combinarla con otra informaci&#243;n que les haya proporcionado o que hayan recopilado a partir del uso que haya hecho de sus servicios.';
                if ('Pol&#237;tica de cookies.'.length > 0)
                    cookiesettings.cookieWhatAreLinkText = 'Pol&#237;tica de cookies.';
                if ('/politica-de-cookies'.length > 0)
                    cookiesettings.cookieWhatAreTheyLink = 'https://clinicatortosamontalvo.com'+''+'/politica-de-cookies';
            }
            else {
                if ('We use cookies on this website, you can &lt;a href=&quot;{{cookiePolicyLink}}&quot; title=&quot;read about our cookies&quot;&gt;read about them here&lt;/a&gt;. To use the website as intended please...'.length > 0)
                    cookiesettings.cookieMessage = 'We use cookies on this website, you can <a href="{{cookiePolicyLink}}" title="read about our cookies">read about them here</a>. To use the website as intended please...';
                if (''.length > 0)
                    cookiesettings.cookiePolicyLink = '';
            }
            // positioning
            cookiesettings.cookieNotificationLocationBottom = true;
            cookiesettings.cookieDiscreetReset = false;
            if ('topleft'.length > 0)
                cookiesettings.cookieDiscreetPosition = 'topleft';

            // buttons
            cookiesettings.cookieAcceptButton = true;
            if ('ACEPTAR COOKIES'.length > 0)
                cookiesettings.cookieAcceptButtonText = 'ACEPTAR COOKIES';

            cookiesettings.cookieDeclineButton = true;
            if ('RECHAZAR COOKIES'.length > 0)
                cookiesettings.cookieDeclineButtonText = 'RECHAZAR COOKIES';

            cookiesettings.cookieResetButton = false;
            if ('RESET COOKIES FOR THIS WEBSITE'.length > 0)
                cookiesettings.cookieResetButtonText = 'RESET COOKIES FOR THIS WEBSITE';

            cookiesettings.cookieConfigButton = true;
            if ('CONFIGURAR'.length > 0)
                cookiesettings.cookieConfigButtonText = 'CONFIGURAR';

            $.cookieCuttr(cookiesettings);

            let titles = document.querySelectorAll('.cookie-title');
            for (let i = 0; i < titles.length; i++) {
                titles[i].addEventListener('click', toggleVisible);
            }

            //let chevrons = document.querySelectorAll('.cookie-title span');
            //for (let i = 0; i < chevrons.length; i++) {
            //    chevrons[i].addEventListener('click', function (e) {
            //        e.stopPropagation();
            //    });
            //}
        });

        function toggleVisible(e) {
            if (e.target.nextElementSibling) {
                e.target.nextElementSibling.classList.toggle('active');
            }
            else {
                e.target.parentElement.nextElementSibling.classList.toggle('active');
            }

            if (e.target.tagName != 'SPAN') {
                e.target.firstElementChild.classList.toggle('fa-chevron-circle-down');
                e.target.firstElementChild.classList.toggle('fa-chevron-circle-up');
            }
            else if (e.target.tagName == 'SPAN') {
                e.target.classList.toggle('fa-chevron-circle-down');
                e.target.classList.toggle('fa-chevron-circle-up');
            }
            e.stopPropagation();
        }
    </script>
<script type="text/javascript">
//<![CDATA[

//]]>
</script>
</body>
</html>
