<!DOCTYPE html>
<html>
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="/resources/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script>
      window.eb = {
        multiLanguage: 'S' == 'S',
        languages: JSON.parse('["PT","EN"]'),
       cmsEnabled: 'true' == 'true',
      };
      window.defaultLanguage = 'PT';
    </script>
<script src="/resources/js/jquery.min.js" type="text/javascript"></script>
<script charset="UTF-8" src="/resources/js/moment-with-locales.min.js" type="text/javascript"></script>
<script src="/resources/js/highcharts.js" type="text/javascript"></script>
<script src="/resources/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/resources/js/vex.combined.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/resources/js/dragula.min.js" type="text/javascript"></script>
<script charset="UTF-8" src="/resources/js/fullcalendar.min.js" type="text/javascript"></script>
<script src="/resources/js/en-gb.js" type="text/javascript"></script>
<script charset="UTF-8" src="/resources/js/pt.js" type="text/javascript"></script>
<script src="/resources/js/autoNumeric.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery.mask.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/resources/js/url-polyfill.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery.pwstrength.js" type="text/javascript"></script>
<script src="/resources/js/formUtils/formUtils.js" type="text/javascript"></script>
<script src="/resources/js/formUtils/twoSteps.js" type="text/javascript"></script>
<script src="/resources/js/formUtils/confirmationKey.js" type="text/javascript"></script>
<script src="/resources/js/formUtils/otpHandler.js" type="text/javascript"></script>
<script src="/resources/js/formUtils/reports.js" type="text/javascript"></script>
<link href="/resources/css/vendor.css" rel="stylesheet"/>
<link href="/resources/css/ie.css" rel="stylesheet"/>
<link href="/resources/css/screen.css" rel="stylesheet"/>
</head>
<body class="tundra">
<div id="top"></div>
<input id="language-locale" type="hidden" value="pt"/>
<div class="full-layout-area">
<nav id="header"></nav>
<div class="page-body">
<script>
  eb.error = '' == 'true';

  eb.login = {
    toggles: {
      username: 'true' == 'true',
      password: 'false' == 'true',
    },
    multiLanguage: 'S' == 'S',
    languages: window.eb.languages,
    username: {
      text: '',
      defState: {
        class: true ? 'eb-icon-eye' : 'eb-icon-hide-eye',
        input: true ? 'text' : 'password',
      },
      hasVirtualKeyboard: 1 !== 0,
      onlyVirtualKeyboard: 1 === 2,
    },
    isPrivate: 'E' === 'P',
  };

  eb.topNav = {
    locationUrl: '[PROTOCOL]://?',
    contactUsUrl: '[PROTOCOL]://?',
  };

  eb.loginDialogShow = false;
  eb.origin = 'E';
  eb.virtualKeyboard = 1;

  eb.labels = {
    login: {
      invalidMessagePass: 'Insira uma password',
      invalidMessageUser: 'Insira um utilizador',
      errorLogin: 'Neste momento esta operação não se encontra disponível',
    },

    validation: {
      minlength: 'Por favor, introduza pelo menos {0} caracteres.',
      maxlength: 'Por favor, não introduza mais do que {0} caracteres.',
      invalidChars: 'O valor inserido possui caracteres inválidos'
    }
  };
</script>
<div id="login-view"></div>
<div class="errorSystemEBPLus application-error" id="errorSystemEBPLus" style="display:none;"></div>
<script src="/resources/js/login.js" type="text/javascript"></script>
</div>
<div class="row-full page-footer" id="page-footer" style="display: none;">
</div>
<script>
        var dUrl = 'https://www.bancobic.ao/';

        $(document).ready(function(){
          $('.full-layout-area').addClass('login');
        });
        var lang = $('#language-locale').val();
        moment.locale(lang);
      </script>
</div>
</body>
</html>
