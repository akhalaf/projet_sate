<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Von Erta box" name="description"/>
<meta content="mladi psi, kužki, pasji mladiči, mladiči, boxer, bokser, psi, mladiči, nemški bokser, german boxer, Deutsch Boxer, " name="keywords"/>
<meta content="Copyright by PLAN e d.o.o." name="copyright"/>
<meta content="10" name="revisit-after"/>
<meta content="Index,Follow" name="robot"/>
<meta content="sl_SI" name="language"/>
<title>Von Erta Box - Domov</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<style>
.clear:after { visibility : hidden; display : block; font-size : 0; content : " "; clear : both; height : 0; }
#cookieWrapper {width: 620px; padding: 10px; display : block; position: fixed; bottom: 0; left:50%; line-height: 21px; font-family: ebrima,Arial,Helvetica,sans-serif; text-align: left; font-size: 11px; color: #fff; margin-left: -345px; background: #000; filter:alpha(opacity=85); opacity:0.85;
                border-radius: 5px 5px 0 0; moz-border-radius: 5px 5px 0 0; -webkit-border-radius: 5px 5px 0 0; z-index: 3333;
                -webkit-transition: opacity 0.5s linear;	-moz-transition: opacity 0.5s linear;	-ms-transition: opacity 0.5s linear;	-o-transition: opacity 0.5s linear; transition: opacity 0.5s linear;
}
#cookieWrapper:hover {filter:alpha(opacity=98); opacity:0.98;}
#cookieWrapper a:hover {text-decoration: none;}
#cookieWrapper #firstText {width: 490px; float: left; }
#cookieWrapper #firstText a,
#cookieWrapper #firstText a:active,
#cookieWrapper #firstText a:visited {color: #3D9ACF; text-decoration: underline;}
#cookieWrapper #firstText a:hover {text-decoration: none;}
#cookieWrapper #links {width: 105px;float: right;}
#cookieWrapper #links a {float: left; margin-right: 15px; transition: none;}
#cookieWrapper a#agree {width: 42px; height: 42px; background: url('/site/class/cookiejar/images/agree.png') no-repeat 0 0; display: block; text-indent: -123456px; }
#cookieWrapper a#disagree {width: 42px; height: 42px; margin-right: 0; background: url('/site/class/cookiejar/images/disagree.png') no-repeat 0 0; display: block; text-indent: -123456px; }
#cookieWrapper a#agree:hover,#cookieWrapper a#disagree:hover {background-position: left bottom;}

#cookieWrapper #secondText {display: none; margin-top: 20px;}
#cookieWrapper ul {margin-top: 20px; list-style-type: decimal; margin-left: 20px;}
</style>
</head><body><div id="cookieWrapper" style="display: none;">
<div class="clear">
<div id="firstText">
            Naša spletna stran zaradi zagotavljanja boljše uporabniške izkušnje uporablja piškotke. Več o posameznih piškotkih si lahko preberete v            <a href="#" id="statement">izjavi o zasebnosti</a>.
            Ali se strinjate z uporabo piškotkov?     
        </div>
<div id="links">
<a href="#" id="agree" onclick="jQuery.ajax({type: 'GET', url: '/index.php?class=cookiejar&amp;cookieAllow=1', dataType: 'script'}); return false;" title="Se strinjam">Se strinjam</a>
<a href="#" id="disagree" onclick="jQuery.ajax({type: 'GET', url: '/index.php?class=cookiejar&amp;cookieAllow=0', dataType: 'script'}); return false;" title="Se ne strinjam">Se ne strinjam</a>
</div>
</div>
<div id="secondText">
        Naša spletna stran uporablja piškotke. Piškotki so majhne datoteke, ki jih naložimo na vaš računalnik. Tako vas lahko prepoznamo, ko se vrnete na našo stran.   
        Uporabljamo naslednje piškotke:
        <ul>
<li>Piškotek orodja Google Analytics za štetje obiskovalcev strani in optimizacijo vsebine strani. Z njim raziskujemo, kako se obiskovalci premikajo po strani, da vidimo, kam postaviti vsebino.</li>
</ul>
<br/>
        
        Podatkov, zbranih s pomočjo piškotkov ne bomo posredovali tretjim osebam.    </div>
</div>
<script>
jQuery(document).ready(function() {
    jQuery("#cookieWrapper").css("display", "");
    
    jQuery('#cookieWrapper #statement').click(function() {
      jQuery('#cookieWrapper #secondText').toggle('slow', function() {
      });
    });
     
    jQuery('#cookieWrapper #agree, #cookieWrapper #disagree').click(function() {
      jQuery('#cookieWrapper').toggle('slow', function() {
      });
    });
});
</script>
<!-- SITE default [head include]  ----- ## -->
<script src="/js/baseDir.js.php" type="text/javascript"></script>
<script src="/js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="/js/ajax.js" type="text/javascript"></script>
<script src="/js/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery/ui.min.js" type="text/javascript"></script>
<script src="/js/jquery/_limit/jquery.limit-1.2.source.js" type="text/javascript"></script>
<script src="/js/jquery/_equalHeights/equalHeights.js" type="text/javascript"></script>
<script src="/js/jquery/_datepicker/datepicker.js" type="text/javascript"></script>
<script src="/js/jquery/_datepicker/ui.datepicker-sl.js" type="text/javascript"></script>
<script src="/js/jquery/_timepicker/timepicker.js" type="text/javascript"></script>
<script src="/js/jquery/_timepicker/timepicker.init.js" type="text/javascript"></script>
<script src="/js/jquery/_timepicker/ui.timepicker-sl.js" type="text/javascript"></script>
<link href="/js/jquery/_timepicker/timepicker.css_cache.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript">
        $(function(){
      		datepicker.init();
            timepicker.init();
    	});        
        </script>
<script src="/js/dialog.js" type="text/javascript"></script>
<script src="/js/easySlider1.5.js" type="text/javascript"></script>
<script src="/js/jquery.jcarousel.min.js" type="text/javascript"></script>
<script src="/js/functions.js" type="text/javascript"></script>
<script src="/site/template/page/functions.js" type="text/javascript"></script>
<link href="/site/default/css/!Set.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/site/template/page/default/css/!Set.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/site/template/page/design/css/!Set.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/cms/tiny_mce/css/tiny.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery/_slideshow/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<link href="/js/jquery/_slideshow/default.css" rel="stylesheet" type="text/css"/>
<link href="/js/jquery/_slideshow/nivo-slider.css" rel="stylesheet" type="text/css"/>
<!-- SITE default [end]  ----- ## -->
<!-- MODULES [head include]  ----- ## -->
<script src="/modules/eventCalendar/js/functions.js" type="text/javascript"></script>
<link href="/modules/eventCalendar/template/default/css/style.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/modules/mailSend/template/default/css/style.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/modules/user/css/style.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/modules/news/template/default/css/style.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/modules/gallery/template/default/album/css/style.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/modules/gallery/template/default/css/gallery.css_cache.css" rel="stylesheet" type="text/css"/>
<!--
        <link rel="stylesheet" type="text/css" href="/modules/home/template/default/css/style.css_cache.css" />
       	<link rel="stylesheet" type="text/css" href="" />
        <link rel="stylesheet" type="text/css" href="/modules/survey/template/default/css/style.css_cache.css" />-->
<!-- MODULES [end]  ----- ## -->
<!-- jQuery  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ## -->
<!-- JS -->
<script src="/js/jquery/_imgcorners/div.corners.min.js" type="text/javascript"></script>
<script src="/js/jquery/_tip/html/source.js" type="text/javascript"></script>
<script src="/js/jquery/_uploadify/swfobject.js" type="text/javascript"></script>
<script src="/js/jquery/_uploadify/jquery.uploadify.v2.1.4.min.js" type="text/javascript"></script>
<script src="/js/jquery/_lightbox/jquery.lightbox.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
            $(function(){
            		$(".lightbox").lightbox();
            	});
            </script>
<!-- CSS -->
<link href="/js/jquery/_lightbox/css/jquery.lightbox.css" rel="stylesheet" type="text/css"/>
<link href="/js/jquery/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="/js/jquery/_imgcorners/divcorners.css" rel="stylesheet" type="text/css"/>
<link href="/js/jquery/_tip/html/style.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript">

			$(document).keypress(function(e){
			openAdmin(e);
			
		});
		
	$(document).load(function(){
														
	});
	
	$(document).ready(function(){
		dcAjaxResize()
		controlAdminObjects(); ajaxUnlock();
		dct1CreateCorner(".shadow");
		
		var c_1 = "#f6eee8";
		var c_2 = "#d5aa8d";
		objOverlay([ ".overlay1", c_1, ".overlay2", c_2 ]);
		
        $(".htmlTip").htmltip({
			header: "",
			content: "",
			footer: "",
			alignment: "bottom",
			xOff: 20,
			yOff: 10,
			formFocus: false,
			effect: "",
			effectOptions: {},
			effectSpeed: 500,
			spacing: 0
		});
        										
	});
		
	
	
	/*
	$(document).mousedown(function(e){
		
		
	});
	$(window).scroll(function(){
		
		
	});
	*/
	
</script>
<style type="text/css">
</style>
<!-- CMS [head include]  ----- ## -->
<script src="/cms/template/plane/js/adminMenuNav.js" type="text/javascript"></script>
<script src="/cms/template/plane/js/functions.js" type="text/javascript"></script>
<link href="/cms/default/css/!Set.css_cache.css" rel="stylesheet" type="text/css"/>
<link href="/cms/template/plane/css/!Set.css_cache.css" rel="stylesheet" type="text/css"/>
<!-- CMS [end]  ----- ## -->
<div class="container" style="background: url(/site/template/page/design/images/2_Body/boxerBottom.png) no-repeat bottom right;">
<div class="adminWindow" id="admWinLogin" style="visibility: hidden; ">
<div class="adminWindowTopLine">
<p class="awtlTitle" id="awtlTitle"></p>
<div id="closeSubGalleryBtn" onclick="zapriPomoznoOkno();" onmouseout="this.style.backgroundImage='url(/cms/template/plane/images/btns/closeBlue.png)'; this.style.color='#004383';" onmouseover="this.style.cursor='pointer'; this.style.backgroundImage='url(/cms/template/plane/images/btns/closeOrange.png)'; this.style.color='#f26522';" style="margin-top: 2px; background-image: url(/cms/template/plane/images/btns/closeBlue.png);"></div>
</div>
<div class="clear"></div>
<div class="adminWindowContext">
<div class="adminWindowContextWraper" id="pomoznoOkno">
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="adminWindowBottomLine"></div>
<div class="clear"></div>
</div>
<div id="wrapper">
<!-- HEAD -->
<div id="head">
<ul id="language">
<li><a class="sel" href="/lang=slo">slo</a>   | </li>
<li><a class="" href="/lang=eng">eng</a>   | </li>
<li><a class="" href="/lang=nem">nem</a></li>
</ul>
<div class="clear">
<a href="/" id="logo"></a>
<ul id="menu">
<li>
<a class="sel" href="/">
                        Domov                    </a>
</li>
<li>
<a href="/o-nas">
                        O nas                    </a>
</li>
<li>
<a href="/nasi-psi">
                        Naši psi                    </a>
</li>
<li>
<a href="/legla">
                        Legla                    </a>
</li>
<li>
<a href="/gallery">
                        Galerija                    </a>
</li>
<li>
<a href="/o-boxerju">
                        O boxerju                    </a>
</li>
<li>
<a href="/contact">
                        Kontakt                    </a>
</li>
<li>
<a href="https://www.vonertabox.si/news">
                        Aktualno / News                    </a>
</li>
</ul>
<div class="clear"></div>
</div>
</div>
<!-- BODY -->
<div id="body">
<!-- HOLDER [start] -->
<div id="ajaxC0nt3nt">
<div id="slider">
<script src="/modules/slideshow/template/default/sliderengine/amazingslider.js"></script>
<script src="/modules/slideshow/template/default/sliderengine/initslider-1.js"></script>
<style>
    .amazingslider-text-wrapper-0 {margin: 0 !important; bottom: auto !important; top: 100px; }
    .amazingslider-text-bg-0 {background: #000; color: #fff; opacity: 0.8; }
    .amazingslider-title-0 {color: #fff;}
    .amazingslider-watermark-0, .amazingslider-bottom-shadow-0 {display: none!important;}
    .amazingslider-nav-0 {bottom: -70px; }
    .amazingslider-slider-0 {border: none !important; margin: 0 !important;}
</style>
<div id="amazingslider-1" style="display:block;position:relative; margin-bottom: 110px; height: 350px !important;">
<ul class="amazingslider-slides" style="display:none;">
<li>
<img alt=" " src="/modules/slideshow/images/1_JPEG_1f51af5fc2863c6c095c0266d7a1bf40.jpg"/>
<video preload="none" src="#"></video>
</li>
</ul>
<ul class="amazingslider-thumbnails" style="display:none;">
<li><img src="/modules/slideshow/images/1_JPEG_1f51af5fc2863c6c095c0266d7a1bf40.jpg"/></li>
</ul>
</div>
</div>
<div class="clear">
<img class="shadowImageHome" src="/site/template/page/design/images/2_Body/boxer.png" style="float: left; margin-bottom: 30px;"/>
<div class="L">
<h1 style="font-size: 25px;text-align: center;">Dobrodošli</h1>
<p style="text-align: center; font-size: 19px; line-height: 25px;">Pozdravljeni na naši spletni strani. Smo veliki ljubitelji psov pasme
        nemški bokser in registrirana psarna nemških bokserjev <br/><br/><strong>VON ERTA BOX.</strong></p>
<p style="text-align: center; font-size: 19px; line-height: 25px;">Naš cilj je vzrediti zdrave, temperamentne in delovne bokserje, ki
        bodo v ponos svoji pasmi in svojim lastnikom.
        <br/><br/>
<strong>Še enkrat dobrodošli!</strong></p> </div>
</div>
<div class="clear">
<div style="width:  700px; height: auto; float: left">
<div id="homeNews">
<h1>Aktualno</h1>
<a href="/news" id="archive" title="Arhiv novic">[Arhiv]</a>
<ul>
<li class="homeNewsContent homeNewsContentLast ">
<img alt="New puppies in Kennel Von Erta Box" class="homeNewsImg shadowImage" src="/modules/news/images/Montessa &amp; Ares von Manitoy  - new puppies 1 JPEG.jpg_6364d35a92d63cd4fdeceaaffcd029a0.jpg"/>
<a class="homeNewsLink" href="/news/show/newsID=50" title="New puppies in Kennel Von Erta Box">
<span class="homeNewsTitle">New puppies in Kennel Von Erta Box</span>
<span>Puppies are born 24.12.2014... Več</span>
</a>
</li>
<li class="homeNewsContent ">
<img alt="New puppies in Kennel Von Erta Box" class="homeNewsImg shadowImage" src="/modules/news/images/iDSC_0703 - 1.jpg_aba89dc83c3b94e8d22572bd31a94b86.jpg"/>
<a class="homeNewsLink" href="/news/show/newsID=53" title="New puppies in Kennel Von Erta Box">
<span class="homeNewsTitle">New puppies in Kennel Von Erta Box</span>
<span>New puppies in kennel VON ERTA BOX... Več</span>
</a>
</li>
</ul>
</div>
</div>
<div style="width: 200px; float: right;">
<a class="buttonHome" href="/contact#contact">Vprašajte nas</a>
</div>
</div>
</div>
<!-- HOLDER [end] -->
</div>
</div>
<!-- FOOT -->
<div id="foot" style="background: #e6ded2; border-top: 1px solid #d3c6b2; padding: 20px 0;">
<div id="wrapper"><div class="footText">
<div style="float:  left;">© von Erta box</div>
<div style="float:  right;">
<a href="/index.php?class=cookiejar&amp;cookieAllow=1&amp;refresh=1">Omogoči piškotke</a> |
        Izdelava strani: <a href="http://www.plan-e.si">PLAN e d.o.o.</a>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<script type="text/javascript">
        	var t = setTimeout("dcAjaxResize()", 100);
        </script>
</div>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('.allCarousel').jcarousel({
    	wrap: 'circular'
    });
});

</script>
</body></html>
