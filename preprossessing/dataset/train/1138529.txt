<!DOCTYPE html>
<html lang="en">
<head>
<base href="https://pharma-chem.net/"/>
<meta content="width=device-width ,initial-scale=1.0, user-scalable=0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="noindex" name="robots"/>
<meta content="Error 404 - At the present moment the requested page is not available - Pharmaceutical Chemistry" name="keywords"/>
<meta content="Error 404 - At the present moment the requested page is not available - Pharmaceutical Chemistry" name="title"/>
<meta content="Error 404 - At the present moment the requested page is not available - Pharmaceutical Chemistry" name="description"/>
<title>Error 404 - At the present moment the requested page is not available - Pharmaceutical Chemistry</title>
<link href="assets/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="assets/template.css" rel="stylesheet" type="text/css"/>
<link href="assets/bootstrap.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<main>
<section id="area">
<div class="container">
<div class="row">
<div class="col-sm-9 col-md-9">
<div class="logo">
<a href="/" title="Pharmaceutical Chemistry"><img alt="Pharmaceutical Chemistry" src="assets/logo.gif" title="Pharmaceutical Chemistry"/>Pharmaceutical Chemistry Co., Ltd</a>
</div>
</div>
<div class="col-sm-3 col-md-3">
<div class="flags">
<a href="/"><img alt="English" src="assets/en.gif" title="English"/></a>
<a href="https://ru.pharma-chem.net/" target="_blank"><img alt="Russian (Русский)" src="assets/ru.gif" title="Russian (Русский)"/></a>
</div>
</div>
</div>
</div>
</section>
<section id="content">
<div class="container">
<div class="row">
<div class="col-sm-4 col-md-3">
<div class="sidebar">
<div class="module">
<h3>Contact Us</h3>
<div class="content">
<p><img alt="" src="assets/con_info.png"/><strong>Contact name:</strong> Alex</p>
<p><img alt="" src="assets/email.jpg"/><strong>E-mail:</strong> <a class="__cf_email__" data-cfemail="7704161b120437071f16051a165a141f121a1e145914181a" href="/cdn-cgi/l/email-protection">[email protected]</a></p>
<p><img alt="" src="assets/skype.jpg"/><strong>Skype:</strong> <a class="__cf_email__" data-cfemail="05766469607645756d6477686428666d60686c662b666a68" href="/cdn-cgi/l/email-protection">[email protected]</a></p>
<p><img alt="" src="assets/jabber_icon.png"/><strong>Jabber:</strong> <a class="__cf_email__" data-cfemail="4c3c242d3e212d612f242921252f0c34213c3c62263c" href="/cdn-cgi/l/email-protection">[email protected]</a></p>
<p><img alt="" src="assets/weblink.png"/><strong>Site:</strong> pharma-chemic.com</p>
<p><img alt="" src="assets/con_address.png"/><strong>Address:</strong> № 20, Sanxiang Long, Suzhou City, Jiangsu Province, China</p>
</div>
</div>
</div>
</div>
<div class="col-sm-8 col-md-9">
<div class="info">
<h3>Error 404</h3>
<div class="content">
<p><img alt="Error 404 - At the present moment the requested page is not available" src="assets/404.jpg" title="Error 404 - At the present moment the requested page is not available"/></p>
<h4>Error 404 - At the present moment the requested page is not available</h4>
<p>Dear Clients! Our site has moved to the new domain pharma-chemic.com. Our website: <a href="https://pharma-chemic.com/">pharma-chemic.com</a>. If you have any questions, write to us: <a href="/cdn-cgi/l/email-protection#f48795989187b4849c95869995d9979c91999d97da979b99"><span class="__cf_email__" data-cfemail="196a78757c6a596971786b7478347a717c74707a377a7674">[email protected]</span></a> or <a href="/cdn-cgi/l/email-protection#721f131c131517005c021a13001f135f111a171f32060706131c1d06135c111d1f"><span class="__cf_email__" data-cfemail="7f121e111e181a0d510f171e0d121e521c171a123f0b0a0b1e11100b1e511c1012">[email protected]</span></a>. Sorry for any inconvenience!</p>
</div>
</div>
</div>
</div>
</div>
</section>
</main>
<footer>
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-12">
<p class="power_by">Copyright © 2010-2019, <a href="/">Pharmaceutical Chemistry Co., Ltd</a>. All rights reserved.<br/>Reprinting of the information is liable only with the permission of the Administration and an available active reference to the source.</p>
<p class="power_by">The products are delivered from China. The company forwards the goods at the sender’s discretion. The delivery term of the goods depends on the remoteness of the buyer.<br/>On our site you can purchase pharmaceutical chemistry intended only for laboratory and chemical research.</p>
<p class="power_by">Please, note that before you make an order, you should verify the legality of the products in your country.<br/>Our company is not responsible for ordering the substances prohibited in your country. All the responsibility and any possible consequences rest on the Customer.</p>
</div>
</div>
</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script></body>
</html>