<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Law Offices of Danielle V. Behesnilian" name="description"/>
<meta content="law, criminal law" name="keywords"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://dvblawoffices.com/xmlrpc.php" rel="pingback"/>
<title>Page not found | Daniel V. Behesnilian, Attorney at Law</title>
<!--[if lt IE 9]>
<script src="https://dvblawoffices.com/wp-content/themes/unite/inc/js/html5shiv.min.js"></script>
<script src="https://dvblawoffices.com/wp-content/themes/unite/inc/js/respond.min.js"></script>
<![endif]-->
<link href="https://dvblawoffices.com/feed/" rel="alternate" title="Daniel V. Behesnilian, Attorney at Law » Feed" type="application/rss+xml"/>
<link href="https://dvblawoffices.com/comments/feed/" rel="alternate" title="Daniel V. Behesnilian, Attorney at Law » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/dvblawoffices.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.29"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://dvblawoffices.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.2.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://dvblawoffices.com/wp-content/plugins/pdf-print/css/style.css?ver=4.2.29" id="pdfprnt_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://dvblawoffices.com/wp-content/themes/unite/inc/css/bootstrap.min.css?ver=4.2.29" id="unite-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://dvblawoffices.com/wp-content/themes/unite/inc/css/font-awesome.min.css?ver=4.2.29" id="unite-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://dvblawoffices.com/wp-content/themes/unite/style.css?ver=4.2.29" id="unite-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://dvblawoffices.com/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.9" id="A2A_SHARE_SAVE-css" media="all" rel="stylesheet" type="text/css"/>
<style id="A2A_SHARE_SAVE-inline-css" type="text/css">
@media screen and (max-width:768px){
.a2a_floating_style.a2a_vertical_style{display:none;}
}
</style>
<script src="https://dvblawoffices.com/wp-includes/js/jquery/jquery.js?ver=1.11.2" type="text/javascript"></script>
<script src="https://dvblawoffices.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<script src="https://dvblawoffices.com/wp-content/plugins/wp-testimonial-widget/js/jquery.cycle.all.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://dvblawoffices.com/wp-content/themes/unite/inc/js/bootstrap.min.js?ver=4.2.29" type="text/javascript"></script>
<script src="https://dvblawoffices.com/wp-content/themes/unite/inc/js/main.min.js?ver=4.2.29" type="text/javascript"></script>
<link href="https://dvblawoffices.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://dvblawoffices.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.2.29" name="generator"/>
<script type="text/javascript"><!--
var a2a_config=a2a_config||{},wpa2a={done:false,html_done:false,script_ready:false,script_load:function(){var a=document.createElement('script'),s=document.getElementsByTagName('script')[0];a.type='text/javascript';a.async=true;a.src='https://static.addtoany.com/menu/page.js';s.parentNode.insertBefore(a,s);wpa2a.script_load=function(){};},script_onready:function(){if(a2a.type=='page'){wpa2a.script_ready=true;if(wpa2a.html_done)wpa2a.init();}},init:function(){for(var i=0,el,target,targets=wpa2a.targets,length=targets.length;i<length;i++){el=document.getElementById('wpa2a_'+(i+1));target=targets[i];a2a_config.linkname=target.title;a2a_config.linkurl=target.url;if(el){a2a.init('page',{target:el});el.id='';}wpa2a.done=true;}wpa2a.targets=[];}};a2a_config.callbacks=a2a_config.callbacks||[];a2a_config.callbacks.push({ready:wpa2a.script_onready});
a2a_config.onclick=1;
//--></script>
<style type="text/css">a, #infinite-handle span {color:#999999}a:hover {color: #ffffff;}a:active {color: #999999;}.btn-primary, .label-primary, .carousel-caption h4 {background-color: #ffffff; border-color: #ffffff;} hr.section-divider:after, .entry-meta .fa { color: #ffffff}.btn-primary:hover, .label-primary[href]:hover, .label-primary[href]:focus, #infinite-handle span:hover, .btn.btn-primary.read-more:hover, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .site-main [class*="navigation"] a:hover, .more-link:hover, #image-navigation .nav-previous a:hover, #image-navigation .nav-next a:hover  { background-color: #ffffff; border-color: #ffffff; }.navbar.navbar-default {background-color: #ffffff;}.navbar-default .navbar-nav > li > a, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus { color: #999999;}.dropdown-menu, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {background-color: #ffffff;}.navbar-default .navbar-nav .open .dropdown-menu > li > a { color: #ffffff;}.entry-content {font-family: arial; font-size:14px; font-weight: normal; color:#1a2930;}</style>
</head>
<body class="error404">
<div class="hfeed site" id="page">
<div class="container header-area">
<header class="site-header col-sm-12" id="masthead" role="banner">
<div class="social" id="social"><a class="social-profile facebook" href="https://www.facebook.com/lawofficesofdanielbehesnilian?fref=ts" target="_blank" title="Facebook"><span class="social_icon fa fa-facebook"></span></a><a class="social-profile twitter" href="https://twitter.com/dvbhome" target="_blank" title="Twitter"><span class="social_icon fa fa-twitter"></span></a><a class="social-profile instagram" href="http://instagram.com/dvb.law" target="_blank" title="Instagram"><span class="social_icon fa fa-instagram"></span></a><a class="social-profile Yelp1" href="http://www.yelp.com/biz/law-offices-of-daniel-v-behesnilian-beverly-hills" target="_blank" title="Yelp1"><span class="social_icon fa fa-Yelp1"></span></a></div> <div class="site-branding col-md-12">
<div id="logo">
<a href="https://dvblawoffices.com/"><img alt="Daniel V. Behesnilian, Attorney at Law" height="198" src="https://dvblawoffices.com/wp-content/uploads/2015/07/logo.png" width="400"/></a>
</div><!-- end of #logo -->
</div>
</header><!-- #masthead -->
</div>
<nav class="navbar navbar-default" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
		                Menu
		            </button>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse"><ul class="nav navbar-nav" id="menu-main-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-69 dropdown" id="menu-item-69"><a class="dropdown-toggle" data-toggle="dropdown" href="#" title="ATTORNEY PROFILES">ATTORNEY PROFILES <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://dvblawoffices.com/attorney-profiles/" title="DANIEL V. BEHESNILIAN">DANIEL V. BEHESNILIAN</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24 dropdown" id="menu-item-24"><a class="dropdown-toggle" data-toggle="dropdown" href="#" title="TEAM">TEAM <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-86" id="menu-item-86"><a href="https://dvblawoffices.com/marine-sakaian/" title="MARINE SAKAIAN">MARINE SAKAIAN</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85" id="menu-item-85"><a href="https://dvblawoffices.com/rosie-carillo/" title="ROSIE CARILLO">ROSIE CARILLO</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="https://dvblawoffices.com/practice-areas/" title="PRACTICE AREAS">PRACTICE AREAS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" id="menu-item-22"><a href="https://dvblawoffices.com/free-consultation/" title="FREE CONSULTATION">FREE CONSULTATION</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-21" id="menu-item-21"><a href="https://dvblawoffices.com/what-to-do/" title="WHAT TO DO">WHAT TO DO</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20" id="menu-item-20"><a href="https://dvblawoffices.com/contact/" title="CONTACT">CONTACT</a></li>
</ul></div> </div>
</nav><!-- .site-navigation -->
<div id="content">
<div class="content-area col-sm-12 col-md-8 pull-left" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://dvblawoffices.com/" class="search-form form-inline" method="get" role="search">
<label class="sr-only">Search for:</label>
<div class="input-group">
<input class="search-field form-control" name="s" placeholder="Search..." type="search" value=""/>
<span class="input-group-btn">
<button class="search-submit btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
</span>
</div>
</form>
<div class="widget widget_recent_entries"> <h2 class="widgettitle">Recent Posts</h2> <ul>
<li>
<a href="https://dvblawoffices.com/what-is-bail-and-how-is-it-set/">What Is Bail And How Is It Set</a>
</li>
<li>
<a href="https://dvblawoffices.com/what-to-do-if-you-are-stopped-by-the-police/">What To Do If You Are Stopped By The Police</a>
</li>
<li>
<a href="https://dvblawoffices.com/what-to-do-if-you-are-arrested/">What To Do If You Are Arrested</a>
</li>
<li>
<a href="https://dvblawoffices.com/what-to-do-when-you-are-involved-in-a-car-accident/">What To Do When You Are Involved In A Car Accident:</a>
</li>
<li>
<a href="https://dvblawoffices.com/home3/">home3</a>
</li>
</ul>
</div>
<div class="widget widget_categories">
<h2 class="widgettitle">Most Used Categories</h2>
<ul>
<li class="cat-item cat-item-3"><a href="https://dvblawoffices.com/category/what-to-do/">WHAT TO DO</a> (4)
</li>
<li class="cat-item cat-item-1"><a href="https://dvblawoffices.com/category/uncategorized/">Uncategorized</a> (3)
</li>
</ul>
</div><!-- .widget -->
<div class="widget widget_archive"><h2 class="widgettitle">Archives</h2><p>Try looking in the monthly archives. <img alt=":)" class="wp-smiley" src="https://dvblawoffices.com/wp-includes/images/smilies/simple-smile.png" style="height: 1em; max-height: 1em;"/></p> <label class="screen-reader-text" for="archives-dropdown--1">Archives</label>
<select id="archives-dropdown--1" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
<option value="">Select Month</option>
<option value="https://dvblawoffices.com/2015/07/"> July 2015 </option>
</select>
</div>
<div class="widget widget_tag_cloud"><h2 class="widgettitle">Tags</h2><div class="tagcloud"></div>
</div>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<div class="footersocial" style="display:none"><div class="social" id="social"><a class="social-profile facebook" href="https://www.facebook.com/lawofficesofdanielbehesnilian?fref=ts" target="_blank" title="Facebook"><span class="social_icon fa fa-facebook"></span></a><a class="social-profile twitter" href="https://twitter.com/dvbhome" target="_blank" title="Twitter"><span class="social_icon fa fa-twitter"></span></a><a class="social-profile instagram" href="http://instagram.com/dvb.law" target="_blank" title="Instagram"><span class="social_icon fa fa-instagram"></span></a><a class="social-profile Yelp1" href="http://www.yelp.com/biz/law-offices-of-daniel-v-behesnilian-beverly-hills" target="_blank" title="Yelp1"><span class="social_icon fa fa-Yelp1"></span></a></div></div>
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info container">
<div class="row">
<nav class="col-md-12" role="navigation">
</nav>
<div class="copyright col-md-12">
										© Copyright 2019 Law Offices of Daniel V. Behesnilian. All Rights Reserved.					
				</div>
</div>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<div class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_vertical_style a2a_target" id="wpa2a_1" style="right:0px;top:100px;"><a class="a2a_dd addtoany_share_save" href="https://www.addtoany.com/share_save"></a>
<script type="text/javascript"><!--
wpa2a.script_load();
//--></script>
</div>
<script type="text/javascript"><!--
wpa2a.targets=[
{title:document.title,url:location.href}];
wpa2a.html_done=true;if(wpa2a.script_ready&&!wpa2a.done)wpa2a.init();wpa2a.script_load();
//--></script>
<script src="https://dvblawoffices.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/dvblawoffices.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://dvblawoffices.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.2.1" type="text/javascript"></script>
</body>
</html>