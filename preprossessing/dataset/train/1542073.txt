<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/layout.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Welcome to Vikrant Engineers</title>
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css"/>
<link href="lightbox.css" rel="stylesheet" type="text/css"/>
<script src="js/prototype.js" type="text/javascript"></script>
<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="js/lightbox.js" type="text/javascript"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<a id="top" name="top"></a>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="1004">
<tr>
<td width="766"><table border="0" cellpadding="0" cellspacing="0" width="766">
<tr>
<td width="10"><img height="68" src="images/top_nav_left_img.jpg" width="10"/></td>
<td class="Top_Navigation" valign="bottom" width="590"><table align="center" border="0" cellpadding="0" cellspacing="0">
<tr>
<td align="center" height="28" width="67"><a href="index.html">Home</a></td>
<td align="center" width="1">|</td>
<td align="center" width="82"><a href="about_us.html">About Us</a></td>
<td align="center" width="1">|</td>
<td align="center" width="104"><a href="our_products.html">Our Products</a></td>
<td align="center" width="1">|</td>
<td align="center" width="79"><a href="services.html">Services</a></td>
<td align="center" width="1">|</td>
<td align="center" width="70"><a href="inquiry.html">Inquiry</a></td>
<td align="center" width="1">|</td>
<td align="center" width="76"><a href="dealers.html">Our Clients</a></td>
<td align="center" width="1">|</td>
<td align="center" width="94"><a href="contact_us.html">Contact Us</a></td>
</tr>
</table></td>
<td width="166"><img height="68" src="images/top_nav_rgt_img.jpg" width="166"/></td>
</tr>
</table></td>
<td width="238"><img height="68" src="images/rgt_img_prt_1.jpg" width="238"/></td>
</tr>
<tr>
<td background="images/tp_tri_line.jpg"><img height="8" src="images/tp_tri_line.jpg" width="1"/></td>
<td><img height="8" src="images/rgt_img_prt_2.jpg" width="238"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="766">
<tr>
<td><img height="176" src="images/ban_img.jpg" width="766"/></td>
</tr>
<tr>
<td background="images/btm_tiws_line.jpg"><img height="4" src="images/btm_tiws_line.jpg" width="1"/></td>
</tr>
</table>
<!-- InstanceBeginEditable name="Main" -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="96%">
<tr>
<td class="Main_Text"> </td>
</tr>
<tr>
<td class="Main_Text"><img alt="" class="Float_Right" height="158" src="images/unit.jpg" style="margin-left:20px; margin-bottom:20px; margin-top:25px;" width="254"/><span class="Main_Title">Welcome</span><br/>
<br/>
            Vikrant equipments, specializes in design, manufacturing,supply &amp; installation of cranes &amp; hoists in India. Established in 1983 founded by late Mr. Arvind Patel, with farsighted vision to manufacture high quality cranes/hoista and its components in India. Having over two decades of experince, the crane and material handling specialties of Vikrant have achieved a strong customer base with our in house manufacturing unit, proven computer aided equipment designs and extensive customer's services programes and sales and parts support networks.<br/>
<br/>
            We insist in good quality reliable products as well as quality control. Our products can save your time, man - hours and lift up productivity with safety of human beings.<br/>
<br/>
<a class="Read_More" href="#">read more...</a></td>
</tr>
<tr>
<td> </td>
</tr>
<tr>
<td class="Quality_Content"><table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
<tr>
<td height="10"></td>
</tr>
<tr>
<td class="Main_Text"><b>Quality Policy</b> <br/>
<br/>
<img class="Float_Right" height="132" src="images/iso.jpg" width="250"/> Vikrant Equipment is commited to provide products meeting customer on requirements time to achive more and more customer satisfaction.<br/>
<br/>
                  We will strive for on going improvements in all our activities by enhancing skills of all our employees through training and motivation.<br/>
<br/>
                  We also commit to implement, maintain and continuously improve quality managment system as per ISO - 9001 : 2000 standrad and also commit to fulfill all applicable statutory and regulatory requirements.</td>
</tr>
<tr>
<td height="10"></td>
</tr>
</table></td>
</tr>
</table>
<!-- InstanceEndEditable --></td>
<td align="center" bgcolor="#FFFFFF" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" valign="top"><img height="152" src="images/rgt_img_prt_3.jpg" width="238"/></td>
</tr>
<tr>
<td align="left" class="Right_Blue_Box" valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="17"> </td>
<td><table align="left" border="0" cellpadding="0" cellspacing="0" width="190">
<tr>
<td height="20"> </td>
</tr>
<tr>
<td><b><a href="our_products.html">Our Products</a></b></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="electric_wirerope_hoist.html">Electric Wire Rope Hoist</a></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="jib_cranes.html">JIB Cranes</a></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="product_eot_cranes.html">EOT Cranes</a></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="product_girntry_cranes.html">Gantry Cranes</a></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="spares.html">Spares</a></td>
</tr>
<tr>
<td height="20"> </td>
</tr>
<tr>
<td> ¬ <a href="winch.html">Winch</a></td>
</tr>
<tr>
<td> </td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td> </td>
</tr>
<tr>
<td align="left"><a href="#"><img border="0" height="64" src="images/mntnc_icn.jpg" width="221"/></a></td>
</tr>
<tr>
<td> </td>
</tr>
<tr>
<td align="left"><a href="images/iso_certi_big.jpg" rel="lightbox[]"><img border="0" src="images/iso_certi.jpg"/></a></td>
</tr>
</table></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" height="12"></td>
<td bgcolor="#FFFFFF"></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="CopyrightÂ©" height="35"><table align="center" border="0" cellpadding="0" cellspacing="0" width="96%">
<tr>
<td align="left" width="50%">Copyright Â© 2009 Vikrant Equipments. All rights reserved.</td>
<td align="right" width="50%"> </td>
</tr>
</table></td>
<td align="center" bgcolor="#FFFFFF" class="CopyrightÂ©"><table align="left" border="0" cellpadding="0" cellspacing="0" width="221">
<tr>
<td align="right">Powered By: <a href="http://www.web4india.com" target="_blank">Web4India</a></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<!-- InstanceEnd --></html>
