<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ro" class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if (IE 7)&!(IEMobile)]><html lang="ro" class="no-js lt-ie9 lt-ie8"><![endif]--><!--[if (IE 8)&!(IEMobile)]><html lang="ro" class="no-js lt-ie9"><![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="ro"><!--<![endif]-->
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-40940726-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-40940726-17');
</script>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<title>Bere Ciucas - Povestea din Poiana</title>
<meta content="Bine ai venit în Poiana Ciucaș, locul în care magia te înconjoară, iar relaxarea e la ea acasă." itemprop="description" name="description"/>
<meta content="" itemprop="keywords" name="keywords"/>
<link href="https://www.bereciucas.ro/static/img/favicon.png" rel="shortcut icon" type="image/png"/>
<meta content="never" name="expires"/>
<meta content="1 Days" name="revisit-after"/>
<meta content="index, follow" name="robots"/>
<meta content="website" property="og:type"/>
<meta content="https://www.bereciucas.ro/" property="og:url"/>
<meta content="Bere Ciucas - Povestea din Poiana" property="og:title"/>
<meta content="Povestea din Poiana" property="og:description"/>
<meta content="https://www.bereciucas.ro/static/img/home_share.jpg" property="og:image"/>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.bereciucas.ro/static/css/style_premii.css?v=113012021" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    var $_base = 'https://www.bereciucas.ro/';
</script>
<script data-cbid="86ed66e6-4eb3-429a-850c-2f896d12c7e8" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script></head>
<body class=" ">
<div class="age-overlay">
<div class="age-container">
<div class="age-inner">
<div class="container">
<div class="row">
<div class="col-12">
<div id="logo"><img src="https://www.bereciucas.ro//static/img/logo.png"/></div>
<p class="d-none">Bine ai venit în Poiana Ciucaș, locul în care magia te înconjoară, iar relaxarea e la ea acasă.</p>
<p>Introdu anul de naștere:</p>
<div id="age">
<form data-month="0" data-year="0" id="ageCheckForm">
<input class="year_1 year-char" maxlength="1" name="year_1" pattern="[0-9]*" placeholder="_" type="number"/>
<input class="year_2 year-char" maxlength="1" name="year_2" pattern="[0-9]*" placeholder="_" type="number"/>
<input class="year_3 year-char" maxlength="1" name="year_3" pattern="[0-9]*" placeholder="_" type="number"/>
<input class="year_4 year-char" maxlength="1" name="year_4" pattern="[0-9]*" placeholder="_" type="number"/>
<div class="month-select">
<input class="small month" maxlength="2" name="month" pattern="[0-9]*" placeholder="_ _" type="number"/>
</div>
<div class="day-select">
<input class="small day" maxlength="2" name="day" pattern="[0-9]*" placeholder="_ _" type="number"/>
</div>
</form>
</div>
<div id="age_error"></div>
<div class="under-age"></div>
<!-- <div class="age-text">Trebuie sa ai cel putin 18 ani pentru a accesa continutul acestui website.</div> -->
</div>
</div>
</div>
</div>
</div>
</div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://www.bereciucas.ro/static/js/plugins.js?v=13012021"></script>
<script src="https://www.bereciucas.ro/static/js/age-gate.js?v=13012021"></script>
</body>
</html>
