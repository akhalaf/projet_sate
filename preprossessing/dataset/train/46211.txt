<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "84171",
      cRay: "610b41c578841aa4",
      cHash: "a6dcd26da1bd56c",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuNzc3c2NvcmUuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "ZCTAgWE3wf2SQaIKl2AXQM/O7jG3/NWL21TG8CBTwjDg5y6Tx2YRYTeP0JvFEeND7znIREHEDM/PwW5DrHAPRaBLkuESF29cX2gPoi8YnaF1BYq1gqkUWgwm/gHblqD4IJafN6FJLEJR/zaKmNT35eAHzgu5ECz7bxV9bMGeYUIZeSLtUtGdqOJOA6rLN9kAlzXqnJgGyX4q4mUQyQzUV8mCUlWYHos2m2dryTGhmQORN96fmL8lwlFUr7SS7loDKJBV0tiQgSrNdZ83Z0b71RcByP/RO7ZskCn+sLfPwbRuG7frV2bkfsaHCV2Pk7scUqBdnwuqiBT+VDcucP4X6NpwIaR9cAj1hc9iHQfzrFiLFICxUpTeSooVr2tzABdWsIo4XX3MA3CF+9V41W2zRtQ+AxZS4eL3pl1DGkiwGjRR09N+nalKLyh/we541L7welGBWeIZS4DA3xtC68am+9EaGklJFNZN90S/Y12bww2RHz4q8wYGVbWi1TEiF7gWBFS2nKyIvgDwhFCUfIicHuEINnFlDuJ1AfFurZXGOuIrGHMGvXbzXNQFYP0Z8QPpL3hiGkfZzUFs9pqXRfNeT5V7VtEDpPxQ5U5Zxw9AxvzatA7V1troSMNjD8/dmUybuSjgXOVmWEzSOQWg5YKAkP4UBmzZgTqrkLL4SsHLtzNn4UmWxViIfOrLkXvXB+ttepWCaqRzwUyK+WAM8zZV3ToJWl0tQEs8gbVJ3m9Ve8LBHbWNxVI9Io82NvrdiI0uvzgeNcqJKvvSOvkWkf+mJw==",
        t: "MTYxMDUwMDQyOS42ODEwMDA=",
        m: "5vwhWCO/MQ9rrIweTT2qFF1BOVLcWigDutoLgOkfjQQ=",
        i1: "vVgv7XLSfqD7KbzjWf6hkg==",
        i2: "+Hbd2uhxPkBqz96qFhSehQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "4uciTILWk4QK/ZplO8iY6W9lEUFxfzRlb8vjVQ5HOrY=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.777score.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<a href="https://efnetwrestling.com/reptilelaborer.php?more=1" style="position: absolute; top: -250px; left: -250px;"></a>
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=b982784a6e40aca958c96d5c4efe402700a561a9-1610500429-0-AYz5s1jNO88OpA0MIM-XPjp6ezEuODem6yoLkAdjRFZPpClL16GuZDF6dC-BO_Hr0xA0eLz8pkK1uWGAoaeUYpX_IxBYCpArXxBOpWmTGHuI1T9ISZ-q15_e1pGasXMftZCn3xYn6PzuPXT3o0IxkVomD86KszB3iafFFfQGkSkrRhopUYhPfAXio-AfN46EQiPoNt5CzmnRmr4dJBO72p3wQFLADj93wP9YY1elWxL2Gm0WeSum3me8um9stHzEr9iGeJtuldZf0hsBX4Fj81M1TnZLHCPd6PVpLWc3F6wKBlzezTmVG8fGIjSJyE2WJn-dHtIdPpVzvvJCeMQE9ev_zf2gT3GbDJgR75Y7aAcvF1JebR4_yTsNCWtO_PMVv40NbJRxcnSUDQR49L4zrleoRuOJ6-iVhLfb_Pej1_FMem0euBjMUmdsbJgUH8yP6RZj8FX_ES9zYq5JDqy5fyojXvGYCPIVPDjiD-Uu0Rj73LySUQuX6PyUM9S0JhEZTbI-af0l3hsgxek1L9k3ianMeFOuJ59Ng8FzecIWa-DrfemLCAn-B8fmIc0000qNfqONEuVAHFxNLWtRCpHwaRM" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="1becaa107627377ebd4ad3cb163d6c0d2330c613-1610500429-0-AVEaUOk7W9tczQoJwC7dQBiMW6U/iN2bct23riY7iCRFDUGqgn8BQEgkg8ChJ/Eojax1Ch4Cfya8NXzqJvJnGrN6amoUl7l3QnAhhFzQMD0ChhxRd3Q6Wxgfgr8zGgkx7nQPbssf8gR5ZCtf9U2RXOURmXgNB+ms4kdDTkG9Oi0QROk5/f1ZIp4FKn7vRA6sLcXsDM36CbkSDeofNgc7bp48JpANIvMGmCkk8/QvsCSVpCKW4QUpVSzuVFYTaX8VwquDc8uCJrKgF39zON67klXd4VX+Ude7ViHfMgXZX8RRyWLt2POC+E2hHrRKqO0wczH7OgNuEVm1GhgqFfgc+fP5M20lMJD+U8wr3VJmmoALOEbMpGj1nqPPE3YExvaKASBZVbeqClMIuNpqXzOTSrezIKUUj9+h4bhI313MB5FF6hmGGLcbEl/fqt2l3BhBCGOhb9L1yWWcDcXpyDTFvjbg0r2GNROJA1qemGMIXF9YZ+YQ5LXgFSObcYvpMPULHWNGvra6HlFN4SKiNUKFbQi8q1KH/p5Ph4dwD11u8nX9NZsX3K7SuyCAo3HsM0+7MfZdH4bPZYTnEsxETcEJhqB98CyL5a7F2dX/GfgiklC6JKQQNbcOwl6kGVJIyQ7odlZDpig/DQAuMk64bSrwoPPSeBRPEnVw5cVL0CC1SEv3kHUqLuLowdqhocMRf7iXILhwMcqhk3z3VroHSXGw1P1ZkGzGPOaGVY+wj1BtiHYSBNTWufyVxYhMYAGqwQZq1g8u1OH0wtfgsBfirSZ6lQFgjENWCn5/XJz2iVObOkkfQuv8MRTSsqdLh5/LskBHiHrjRt19vrruyX9lyR98nZVEUifAlFqAWrCPa0cAliaoVrKPtOyzkNIpzGaCDByNkH9NKKLkfw774OrsSOVYDkYvju7p4hIueehNRnNle2KM3R7J0iExeT82zBvjUN0QTM3PC10P803pXZH3I8nsKAX1g4YOIKoSdpU49q4fQm9ylg99zKcKFKlEB069kfmGiKYR7n8dlz4WHJMUOdE052o4E5kIsg3qxDUP24FmzTssXpRLCxOrUqiOvNNn0sJADJ+WwySuuSRnIl9qUI7kX+9YbTACemdP/T1r1BNlQXosK7gXm/EOQUs4MzF7WRYsBixMn+rwuto/phQMIp6RBcktJfhEDW6r6BgIDWq7SKhufCW3Egiepqne5D1xCFokkdMgiLBNHctSv3eGu7BUB7NiFJhxNDIq3XN7apPjHjZQS/mS7/J3DyqA8TEL/tsxcwHPqBhHlsGdJx+5syCaPVzZ83LoykH2xZF18t7lPCys"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="0b7963a014694acad431d88243726cf4"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610b41c578841aa4')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610b41c578841aa4</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
