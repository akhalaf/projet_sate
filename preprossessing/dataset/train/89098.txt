<html>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED.  -->
<!-- DO NOT COPY THIS CODE WITHOUT PERMISSION.     -->
<!-- DO NOT MAKE DERIVATIVE WORKS.                 -->
<!-- If you are thinking of copying this code      -->
<!-- email contact@airnav.com for authorization.   -->
<head>
<title>AirNav: Allen County Regional Airport Authority at Lima Allen County Airport</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="nofollow" name="robots"/>
<link href="//img.airnav.com/css/css2.css?v=0" rel="StyleSheet" type="text/css"/>
<base target="_top"/>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" width="100%">
<tr valign="bottom">
<td align="left" height="60" valign="top">
<a href="/"><img alt="AirNav" border="0" height="60" src="//img.airnav.com/logo/header.gif?v=HTWA8Q" width="280"/></a>
</td>
<td> </td>
<td align="right" height="60" width="468">
<a href="/ad/click/SYWlyd2lzY29uc2luMj.xOS0x" onclick="_gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-1', 'Click']);" target="_new"><img alt="Pilot Hours Calculator" border="0" height="60" src="//img.airnav.com/banners/airwisconsin2019-1.gif?v=QHE2CI" width="468"/></a>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td width="20"> </td><td align="left">
<table border="0" cellpadding="0" cellspacing="0"><tr><td>
<div id="mainmenu">
<ul>
<li><a href="/airports/" id="airports">Airports</a>
</li>
<li><a href="/navaids/" id="navaids">Navaids</a>
</li>
<li><a href="/airspace/fix/" id="fix">Airspace Fixes</a>
</li>
<li><a href="/fuel/" id="fuel">Aviation Fuel</a>
</li>
<li><a href="/hotels/" id="hotels">Hotels</a>
</li>
<li><a href="/airboss/" id="airboss">AIRBOSS</a>
</li>
<li><a href="/iphoneapp/" id="iphoneapp">iPhone App</a>
</li>
<li><a href="https://www.airnav.com/members/login?return=//my.airnav.com/my" id="my">My AirNav</a>
</li>
</ul>
</div>
</td></tr></table>
</td></tr>
<tr><td bgcolor="#9999FF" width="20"> </td><td align="right" bgcolor="#9999FF" width="100%"><font size="-1">1723 users online
 <a href="/members/login"><img alt="" border="0" height="10" src="//img.airnav.com/btn/mini/login.gif?v=JZBHPB" width="45"/></a></font> </td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="30" width="100%">
<tr><td colspan="2">
<h1>Allen County Regional Airport Authority</h1>
<h2>at <a href="/airport/KAOH">Lima Allen County Airport</a></h2>
</td></tr>
<tr valign="top">
<td valign="top" width="50%">
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Services</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<ul>
<li>Airport management</li>
<li>Aviation fuel</li>
<li>Aircraft ground handling</li>
<li>Oxygen service</li>
<li>Aircraft parking (ramp or tiedown)</li>
<li>Hangars</li>
<li>Hangar leasing / sales</li>
<li>GPU / Power cart</li>
<li>Passenger terminal and lounge</li>
<li>Flight training</li>
<li>...</li>
</ul>
</td></tr>
</table>
</td>
<td valign="top" width="50%">
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Aviation fuel services</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="middle"><td align="right" valign="middle">Brand: </td><td align="left" valign="middle">EPIC </td></tr>
<tr valign="top"><td align="right" nowrap="" valign="top">Fueling hours: </td><td align="left" valign="top">during regular FBO business hours, plus after hours call-out service (with prior arrangement and/or a callout fee); call FBO for details</td></tr>
</table>
 <br/>
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="bottom">
<th align="left" colspan="3" nowrap="">Fuel prices as last reported on 14-Jan-2021</th>
</tr>
<tr valign="top">
<td nowrap="">
100LL Avgas <a href="/popup/service-explain.html?K=FS" onclick="javascript:window.open('/popup/service-explain.html?K=FS','airnavpopup','width=350,height=500,scrollbars=yes');return false;" target="_blank">Full service</a></td>
<td>  </td>
<td nowrap="">$4.30</td>
<td><font size="-1">Reported by the FBO</font></td>
</tr>
<tr valign="top">
<td nowrap="">
Jet A <a href="/popup/service-explain.html?K=FS" onclick="javascript:window.open('/popup/service-explain.html?K=FS','airnavpopup','width=350,height=500,scrollbars=yes');return false;" target="_blank">Full service</a></td>
<td>  </td>
<td nowrap="">$3.75</td>
<td><font size="-1">Reported by the FBO</font></td>
</tr>
</table>
 <br/>
<table border="0" cellpadding="0" cellspacing="0"><tr valign="top"><td valign="top">Discounts: </td><td align="left" valign="top">Oil company branded card<br/>Volume / quantity / bulk discounts<br/>Angel Flight / medical flights<br/>Pilots n Paws flights<br/>Local-based aircraft<br/>Contact us to negotiate discounts<br/>Select special events<br/>Certain fees waived or discounted with minimum fuel purchase</td></tr></table>
Prices include all taxes. Prices <a href="/fuel/guarantee.html">not guaranteed</a>.<br/>
 <br/> <br/>
<div align="left"><a href="/airport/KAOH/A1/update-fuel"><img alt="Update Prices Now" border="0" height="20" src="//img.airnav.com/btn/update-prices.gif?v=HTW9TQ" width="125"/></a></div>
</td></tr>
</table>
</td></tr>
<tr valign="top">
<td><table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Contact information</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="3">
<tr valign="top"><td align="right" valign="top">Address:</td><td valign="top">700 Airport Drive<br/>
Lima, OH 45804

<br/>
United States of America
</td></tr>
<tr valign="top"><td align="right" valign="top">Telephone:</td><td valign="top">
419-227-3225
</td></tr>
<tr valign="top"><td align="right">Email:</td><td><a href="mailto:manager@allencountyairport.com?subject=Message from AirNav.com user to Allen County Regional Airport Authority (KAOH)" onclick="_gaq.push(['_trackEvent', 'EmailListing', 'KAOH-A1', 'Click']);">manager@allencountyairport.com</a></td></tr>
<tr valign="top"><td align="right">Web site:</td><td><a href="/airport/KAOH/A1/link" target="_blank">www.allencountyairport.com</a></td></tr>
</table>
</td></tr>
</table>
</td>
<td><table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Hotels Nearby</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<style>
.hotel_row:hover {
          background-color: #eeeeff;
        }
</style>
<table border="0" cellspacing="2">
<tr class="hotel_row" onclick="window.location='/hotel/COURTYARD_BY_MARRIOTT_LIMA?near=KAOH-A1';" valign="center">
<td>COURTYARD BY MARRIOTT LIMA</td>
<td align="right" nowrap="">4.3 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/HAMPTON_INN_LIMA?near=KAOH-A1';" valign="center">
<td>HAMPTON INN LIMA</td>
<td align="right" nowrap="">4.3 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/HOWARD_JOHNSON_BY_WYNDHAM_LIMA?near=KAOH-A1';" valign="center">
<td>HOWARD JOHNSON BY WYNDHAM LIMA</td>
<td align="right" nowrap="">4.4 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/MOTEL_6_LIMA?near=KAOH-A1';" valign="center">
<td>MOTEL 6 LIMA</td>
<td align="right" nowrap="">4.5 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/TRAVELODGE_BY_WYNDHAM_LIMA_OH?near=KAOH-A1';" valign="center">
<td>TRAVELODGE BY WYNDHAM, LIMA OH</td>
<td align="right" nowrap="">4.6 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/METRO_INNS_FALKIRK?near=KAOH-A1';" valign="center">
<td>METRO INNS FALKIRK</td>
<td align="right" nowrap="">4.6 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/HOLIDAY_INN_LIMA?near=KAOH-A1';" valign="center">
<td>HOLIDAY INN &amp; SUITES LIMA</td>
<td align="right" nowrap="">4.7 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/COUNTRY_INN_AND_SUITES_BY_RADISSON_LIMA_OH?near=KAOH-A1';" valign="center">
<td>COUNTRY INN &amp; SUITES BY RADISSON, LIMA, OH</td>
<td align="right" nowrap="">4.7 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/TOWNEPLACE_SUITES_BY_MARRIOTT_LIMA?near=KAOH-A1';" valign="center">
<td>TOWNEPLACE SUITES BY MARRIOTT LIMA</td>
<td align="right" nowrap="">5.0 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/WINGATE_BY_WYNDHAM_LIMA_DOWNTOWN?near=KAOH-A1';" valign="center">
<td>WINGATE BY WYNDHAM LIMA DOWNTOWN</td>
<td align="right" nowrap="">6.5 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/ECONO_LODGE_LIMA?near=KAOH-A1';" valign="center">
<td>ECONO LODGE LIMA</td>
<td align="right" nowrap="">6.9 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/RED_CARPET_INN_SUITES_LIMA?near=KAOH-A1';" valign="center">
<td>RED CARPET INN  &amp; SUITES  LIMA</td>
<td align="right" nowrap="">6.9 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/COMFORT_INN_LIMA?near=KAOH-A1';" valign="center">
<td>COMFORT INN LIMA</td>
<td align="right" nowrap="">7.0 mi</td>
</tr>
<tr class="hotel_row" onclick="window.location='/hotel/RODEWAY_INN_LIMA?near=KAOH-A1';" valign="center">
<td>RODEWAY INN LIMA</td>
<td align="right" nowrap="">7.1 mi</td>
</tr>
</table>
<p>You can book these or other hotels...<br/><a href="/hotels/selecthotel?airport=KAOH&amp;fbo=A1"><img alt="" border="0" height="20" src="//img.airnav.com/btn/book-a-hotel.gif?v=OXYUT1" width="125"/></a></p>
</td></tr>
</table>
</td>
</tr>
<tr>
<td colspan="2">
<a name="c"></a>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Comments from AirNav users</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<font size="-1"><i>Comments are submitted by their authors and do not reflect the opinion of AirNav, LLC. All comments must adhere to <a href="/info/comments/policy.html">AirNav's Policy on Comments</a>.<br/>
<table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left" colspan="3">
From PJ Ickes
on 01-Oct-2019</th></tr>
<tr valign="top">
<td align="center" width="60">
<br/> 
 </td>
<td> </td>
<td>
<img alt="" border="0" height="12" src="//img.airnav.com/rating/5.gif?v=L3K7L3" width="60"/>
Called ahead requesting GPU. Flight of two from Eastern PA. Line guy made sure it was available &amp; working. Great FBO, Long runway! Good fuel price. Easy in/out.
<br/> </td></tr>
<tr><th align="left" colspan="3">
From Ian Riley
on 20-Jul-2019</th></tr>
<tr valign="top">
<td align="center" width="60">
<br/> 
 </td>
<td> </td>
<td>
<img alt="" border="0" height="12" src="//img.airnav.com/rating/5.gif?v=L3K7L3" width="60"/>
Stopped after hours and Josh was there to take cafe of another plane. Josh got us fuel and oil and after finding our planned destination had no hotels available he got us a nice crew car for the whole night and a discount on a hotel. Great service and a nice little airport.
<br/> </td></tr>
<tr><th align="left" colspan="3">
From A. Travnicek
on 23-Oct-2018</th></tr>
<tr valign="top">
<td align="center" width="60">
<br/> 
 </td>
<td> </td>
<td>
<img alt="" border="0" height="12" src="//img.airnav.com/rating/5.gif?v=L3K7L3" width="60"/>
Great stop! Very reasonable hangar rates, decent courtesy car, and they can fuel you anywhere on the airport! Josh and Derek took great care of us. Hotels and restaurants in town (Try The Met restaurant on Main St. for great brews and food) is a short drive away. We'll be back!
<br/> </td></tr>
</table>
 <br/>
<a href="/airport/KAOH/A1/comment"><img alt="Add your own comment" border="0" height="20" src="//img.airnav.com/btn/add-your-comment.gif?v=HTW9TQ" width="156"/></a>
</i></font></td></tr>
</table>
</td>
</tr>
</table>
<hr/>
<h3>Where would you like to go next?</h3>
<ul>
<li><a href="/airport/KAOH">Lima Allen County Airport</a> information</li>
<li><a href="./update">Update this listing</a>
</li><li><a href="/">AirNav</a> home page</li>
</ul>
<br/> <br/>
<table bgcolor="#CCCCFF" border="0" cellpadding="0" cellspacing="5" width="100%">
<tr><td bgcolor="#333399" colspan="2"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/></td></tr>
<tr valign="bottom">
<td><font color="#404040" size="-2">
Copyright © AirNav, LLC. All rights reserved.
</font>
</td>
<td align="right"><font size="-2"><a href="/info/privacy.html">Privacy Policy</a> 
<a href="/info/contact.html">Contact</a></font></td>
</tr>
</table>
<script type="text/javascript">
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-467723-1']); 
 _gaq.push(['_trackPageview']);
 _gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-1', 'Impression']);
 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
</script>
</body>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED. -->
</html>
