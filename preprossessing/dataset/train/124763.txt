<!DOCTYPE html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# video: http://ogp.me/">
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://animepahe.com" hreflang="en-us" rel="alternate"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<meta content="#373a3c" name="theme-color"/><!-- Chrome, Firefox OS, Opera and Vivaldi -->
<meta content="#373a3c" name="msapplication-navbutton-color"/><!-- Windows Phone -->
<meta content="#373a3c" name="apple-mobile-web-app-status-bar-style"/><!-- iOS Safari -->
<meta content="on" http-equiv="x-dns-prefetch-control"/>
<link href="//i.animepahe.com" rel="dns-prefetch"/><link href="//i.animepahe.org" rel="dns-prefetch"/><link href="//ajax.cloudflare.com" rel="dns-prefetch"/><link href="//cdn.jsdelivr.net" rel="dns-prefetch"/><link href="//kwik.cx" rel="dns-prefetch"/><link href="//i.kwik.cx" rel="dns-prefetch"/><link href="//files-eu-a1.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-a2.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-a3.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-a4.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-b1.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-b2.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-c1.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-c2.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-d1.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-d2.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-d3.nextstream.org" rel="dns-prefetch"/><link href="//files-eu-d4.nextstream.org" rel="dns-prefetch"/><link href="//files-na-a1.nextstream.org" rel="dns-prefetch"/><link href="//files-na-a2.nextstream.org" rel="dns-prefetch"/><link href="//files-na-a3.nextstream.org" rel="dns-prefetch"/><link href="//files-na-b1.nextstream.org" rel="dns-prefetch"/><link href="//files-na-b2.nextstream.org" rel="dns-prefetch"/><link href="//files-na-c1.nextstream.org" rel="dns-prefetch"/><link href="//files-na-c2.nextstream.org" rel="dns-prefetch"/><link href="//files-na-d1.nextstream.org" rel="dns-prefetch"/><link href="//files-na-d2.nextstream.org" rel="dns-prefetch"/><link href="//files-na-d3.nextstream.org" rel="dns-prefetch"/><link href="//files-na-d4.nextstream.org" rel="dns-prefetch"/>
<link as="style" href="//cdn.jsdelivr.net/combine/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css,npm/magnific-popup@1.1.0/dist/magnific-popup.min.css" rel="preload"/>
<link as="style" href="//cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" rel="preload"/>
<link as="script" href="//cdn.jsdelivr.net/combine/npm/jquery@3.3.1,npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js,npm/lazysizes@latest,npm/jquery.cookie@1.4.1,npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js" rel="preload"/>
<meta content="https://animepahe.com/animepahe-270x270.png" name="msapplication-TileImage"/>
<title>animepahe :: okay-ish anime website</title>
<meta content="Watch or download anime shows in HD 720p/1080p." name="description"/>
<meta content="Anime,Pahe,Mini,720p,HD,mp4,English,Subtitle,Hardsub" name="keywords"/>
<meta content="qLRcw8VKEK7qw4Ye--g1lPNpkehTl6RHzwmHXe79KOQ" name="google-site-verification"/>
<meta content="animepahe" property="og:site_name"/>
<meta content="en_US" property="og:locale"/>
<meta content="https://animepahe.com/animepahe-270x270.png" property="og:image"/>
<meta content="https://animepahe.com" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="cloud anime encoding" property="og:title"/>
<meta content="Watch or download anime shows in HD 720p/1080p." property="og:description"/>
<meta content="animepahe" name="author"/>
<meta content="d66c436435b68407387b90b07727bc0c" name="propeller"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon-precomposed" type="image/png"/>
<link href="/apple-touch-icon.png" rel="shortcut icon"/>
<link href="/pikacon-32x32.png" rel="shortcut icon" type="image/png"/>
<link href="/pikacon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://animepahe.com/feed" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="//fonts.googleapis.com/css?family=Sansita:400" rel="stylesheet"/>
<link href="//cdn.jsdelivr.net/combine/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css,npm/magnific-popup@1.1.0/dist/magnific-popup.min.css" rel="stylesheet"/>
<link href="//cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/app/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header class="main-header">
<nav class="navbar navbar-expand-lg">
<a class="navbar-brand" href="/" title="animepahe"><img loading="lazy" onerror="this.src='/app/images/apdoesnthavelogotheysaidapistooplaintheysaid.png'" src="/app/images/apdoesnthavelogotheysaidapistooplaintheysaid.svg"/></a>
<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarNavDropdown" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNavDropdown">
<ul class="navbar-nav mr-auto main-nav">
<li class="nav-item">
<a class="nav-link" href="/" title="#stayhome">#stayhome</a>
</li>
<!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Anime
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="/anime">Index</a>
              <a class="dropdown-item" href="#">Blu-ray</a>
              <a class="dropdown-item" href="#">Seasons</a>
              <a class="dropdown-item" href="#">Genres</a>
              <a class="dropdown-item" href="#">Schedule</a>
            </div>
          </li> -->
<li class="nav-item">
<a class="nav-link" href="/anime" title="anime">anime</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/queue" title="queue">queue</a>
</li>
</ul>
<form class="form-inline nav-search" onkeypress="return event.keyCode != 13;">
<input autocomplete="off" class="input-search" name="q" placeholder="Search" required="" type="text"/>
<div class="search-results-wrap"></div>
</form>
</div>
</nav>
</header>
<section class="main">
<article>
<div class="content-wrapper">
<div class="latest-release">
<h2>Latest Releases</h2>
<div class="notification-release"></div>
<div class="episode-list-wrapper"><div class="episode-list"></div></div>
</div>
</div>
</article>
</section>
<div id="encode-status" style="z-index:2">
<div class="encode-status" style="display:none"><strong class="anime-name"></strong><br/>
<div class="row">
<div class="col-xs-4"><span class="hidden-xs-down">Progress: </span><span class="current-duration"></span> (<strong class="encode-progress"></strong>)</div>
<div class="col-xs-4 text-xs-center"><span class="hidden-xs-down">Size: </span><strong class="encode-size"></strong></div>
<div class="col-xs-4 text-xs-right"><span class="hidden-xs-down">Anime duration: </span><span class="anime-duration"></span></div>
</div>
<div class="progress">
<div aria-valuemax="100" aria-valuemin="0" class="progress-bar progress-bar-success" role="progressbar"></div>
</div>
</div>
</div>
<div aria-hidden="true" class="modal fade" id="settings" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title"><strong>Settings</strong></h5>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<div class="container-fluid">
<div class="row">
<div class="col-12">
<div class="form-group">
<label class="form-control-label" for="resolution-group"><strong>Video Resolution:</strong></label>
<select aria-describedby="resolutionHelp" class="form-control" id="setting-resolution">
<option value="low">Low</option>
<option selected="" value="medium">Medium</option>
<option value="high">High</option>
</select>
</div>
</div>
<div class="col-12">
<div class="form-group">
<label class="form-control-label" for="filehost-group"><strong>Video Provider:</strong></label>
<select aria-describedby="filehostHelp" class="form-control" id="setting-filehost">
<option value="nodefiles">NodeFiles</option>
</select>
</div>
</div>
<div class="col-12">
<div class="form-group">
<label class="form-control-label" for="fansub-group"><strong>Fansub:</strong></label>
<select aria-describedby="fansubHelp" class="form-control" id="setting-fansub">
<option value="HorribleSubs">HorribleSubs</option>
<option value="NotHorribleSubs">Not HorribleSubs (e.g. FFF, DameDesuYo, etc)</option>
</select>
</div>
</div>
<div class="col-12">
<div class="form-group">
<label class="form-control-label" for="list-group"><strong>Listing:</strong></label>
<select aria-describedby="listHelp" class="form-control" id="setting-list">
<option value="grid">Grid</option>
<option value="list">List</option>
</select>
</div>
</div>
<div class="col-12">
<div class="form-group">
<label class="custom-control custom-checkbox">
<input class="custom-control-input" type="checkbox"/>
<span class="custom-control-indicator"></span>
<strong class="custom-control-description">Enable low spec</strong>
</label>
</div>
</div>
<div class="col-12">
<div class="alert alert-warning" role="alert">
<small><strong>Note:</strong> Settings will be saved to your browser, make sure cookies is enabled.</small>
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
<button class="btn btn-primary" type="button">Save changes</button>
</div>
</div>
</div>
</div>
<script src="//cdn.jsdelivr.net/combine/npm/jquery@3.3.1,npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js,npm/lazysizes@latest,npm/jquery.cookie@1.4.1,npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
<script src="/app/js/core.js"></script>
<script>
  $(function() {
    var title = "animepahe :: okay-ish anime website";

    function push(urlPath){
      // document.getElementById("content").innerHTML = response.html;
      // document.title = response.pageTitle;
      window.history.pushState("","", "https://www.animepahe.com/?page=" + urlPath);
    }

    window.onpopstate = function(e) { getLatestRelease(window.location.href.replace("https://www.animepahe.com", "").replace("/?page=", "")); };

    getLatestRelease(1);

    var isPaused = false;

    var currentID = 0;

    // var check = setInterval(function() {
    //   if(isPaused == false || currentID == 0) {
    //     checkNewRelease(currentID);
    //   }
    // }, 60000);

    $('.notification-release').on('click', '.refetch', function(e) {
      isPaused = true;
      $(this).addClass('disabled');
      getLatestRelease();
      e.preventDefault();
    });

    $('.episode-list-wrapper').on('click', 'ul.pagination li a', function(e) {
      getLatestRelease($(this).data('page'));
      push($(this).data('page'));
      e.preventDefault();
    });

    $(document).on('keyup', function(e) {
      if (!$('.nav-search').hasClass('active')) {
        if (e.keyCode == 37) {
          if (!$('.latest-release .prev-page').parent().hasClass('disabled')) $('.prev-page').trigger('click');
        } else if (e.keyCode == 39) {
          if (!$('.latest-release .next-page').parent().hasClass('disabled')) $('.next-page').trigger('click');
        }
      }
    });

    function checkNewRelease(id) {
      $.getJSON('/api?m=new&id=' + id, function(data) {
        if (data.count > 0) {
          var release = (data.count > 1) ? "releases" : "release";
          document.title = '(' + data.count + ') ' + title;
          $('.notification-release').html('<button class="btn btn-warning btn-block refetch">Show ' + data.count + ' new ' + release + '</button>');
        }
      });
    }

    function getLatestRelease(page = 1) {
      $('ul.pagination li').addClass('disabled');
      $('.notification-release .refetch').addClass('disabled');

      var spinner = $('<div class="loading"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>').hide();
      // $('body').append(spinner);
      // spinner.fadeIn(150);

      $.getJSON('/api?m=airing&l=12&page=' + page, function(data) {
        var output = '<div class="episode-list row">';
        var paging = '<nav aria-label="Page navigation"><ul class="pagination justify-content-center">';

        if (page == 1 && data.total != 0) {
          $.cookie("latest", data.data[0].id);
          currentID = data.data[0].id;
        }

        if (data.total == 0) {
          output += '<div class="no-results">no results found</div>';
          adv = '';
          paging = '';
        } else {
          for (var i = 0; i < data.data.length; i++) {
            var release = data.data[i];
            var now = Date.now() - (3 * 24 * 60 * 60 * 1000);
            var timestamp = + new Date(release.created_at);

            output += '<div class="episode-wrap col-12 col-sm-4" data-id="' + release.id + '">';
            output += '<div class="episode">';
            output += '<div class="episode-snapshot">';
            output += '';
            // output += (release.snapshot.length === 0) ? '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="">' : '<img data-src="' + release.snapshot.replace('https://i.animepahe.com', 'https://i-animepahe-com.cdn.ampproject.org/i/s/i.animepahe.com') + '" loading="lazy" class="lazyload" alt="">';
            // output += (release.snapshot.length === 0) ? '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="">' : '<img data-src="' + release.snapshot + '" loading="lazy" class="lazyload" alt="">';
            output += (release.snapshot.length === 0) ? '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="">' : '<img src="' + release.snapshot + '" alt="">';
            output += '<svg class="play-button" viewBox="0 0 200 200" alt="Play Video">';
            output += '<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>';
            output += '<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>';
            output += '</svg>';
            if (release.disc.length !== 0) {
            output += '<span class="episode-disc ' + release.disc + '">' + release.disc + '</span>';
            }
            output += '<a href="/play/' + release.anime_session + '/' + release.session + '" class="play">Watch ' + release.anime_title + ' - ' + release.episode + ' Online</a>';
            output += '</div>';
            output += '<div class="episode-label-wrap">';
            output += '<div class="episode-label">';
            output += '<div class="episode-title-wrap">';
            output += '<span class="episode-title"><a href="/anime/' + release.anime_session + '" title="' + release.anime_title + '"><span class="hidden-sm-down">[' + release.fansub + '] </span>' + release.anime_title + '</a></span>';
            output += '</div>';
            output += '<div class="episode-number-wrap">';
            if (release.completed == 1) {
            output += '<div class="episode-number text-success"><i class="fa fa-check" aria-hidden="true"></i></div>';
            } else {
            output += (release.episode.length === 0) ? '' : '<div class="episode-number"><span class="text-hide">' + release.anime_title + ' Episode </span>' + release.episode + " " + release.edition + '</div>';
            }
            output += '</div>';
            output += '</div>';
            output += '</div>';
            output += '</div>';
            output += '</div>';
          }

          if (data.last_page != 1) {
            if (data.current_page == 1) {
              var prevDisable = firstDisable = " disabled";
              var prevPage = data.current_page;
            } else {
              var prevDisable = firstDisable = "";
              var prevPage = data.current_page - 1;
            }

            if (data.current_page == data.last_page) {
              var nextDisable = lastDisable = " disabled";
              var nextPage = data.current_page;
            } else {
              var nextDisable = lastDisable = "";
              var nextPage = data.current_page + 1;
            }

            paging += '<li class="page-item' + firstDisable + '">';
            paging += '<a class="page-link" data-page="1" title="Go to the First Page"><span aria-hidden="true">&laquo;</span><span class="sr-only">First</span></a>';
            paging += '</li>';
            paging += '<li class="page-item' + prevDisable + '">';
            paging += '<a class="page-link prev-page" data-page="' + prevPage + '" title="Go to Page ' + prevPage + '"><span aria-hidden="true">&lsaquo;</span><span class="sr-only">Prev</span></a>';
            paging += '</li>';
            paging += '<li class="page-item active">';
            paging += '<span class="page-link" title="We\'re on Page ' + data.current_page + ' of ' + data.last_page + '">' + data.current_page + '<span class="sr-only"> (current)</span></span>';
            paging += '</li>';
            paging += '<li class="page-item' + nextDisable + '">';
            paging += '<a class="page-link next-page" data-page="' + nextPage + '" title="Go to Page ' + nextPage + '"><span class="sr-only">Next</span><span aria-hidden="true">&rsaquo;</span></a>';
            paging += '</li>';
            paging += '<li class="page-item' + lastDisable + '">';
            paging += '<a class="page-link" data-page="' + data.last_page + '" title="Go to the Last Page"><span class="sr-only">Last</span><span aria-hidden="true">&raquo;</span></a>';
            paging += '</li>';
          } else {
            paging = "";
          }
        }

        $('.notification-release').html('');
        document.title = title;
        isPaused = false;
        // $('.loading').fadeOut(150, function() { $(this).remove(); });
        // Export output
        $('.episode-list-wrapper').html(output).append(paging);
      });
    }

    $(".close-ann-hs").on('click', function(e) {
      $.cookie('ann-hs', 0, { expires: 15});
    });
  });
</script>
<!-- Matomo -->
<script type="text/javascript">
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://anal.pahe.win/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
<!-- End Matomo Code -->
</body>
</html>
