<!DOCTYPE html>
<html lang="en">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport"/>
<script src="js/modernizer.js" type="text/javascript"></script>
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2542/jquery.scrollie.min_1.js" type="text/javascript"></script>
<script crossorigin="anonymous" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/animate.css" rel="stylesheet"/>
<link href="fonts/font.css" rel="stylesheet"/>
<link href="css/hover.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/responsive-style.css" rel="stylesheet"/>
<link href="css/bootstrap.offcanvas.min.css" rel="stylesheet"/>
<link href="css/normalize.css" rel="stylesheet" type="text/css"/>
<link href="css/component.css" rel="stylesheet" type="text/css"/>
<link href="css/content.css" rel="stylesheet" type="text/css"/>
<script src="js/modernizr.custom.js"></script>
<link href="images/fevi.png" rel="icon" type="image/png"/>
<script>
jQuery(function() {
    var header = jQuery("#header-2");
    jQuery(window).scroll(function() {    
        var scroll = jQuery(window).scrollTop();
    
        if (scroll >= 100) {
            header.removeClass('noaffix').addClass("posi");
        } else {
            header.removeClass("posi").addClass('noaffix');
        }
    });
});
</script>
<title>We Acquire Technology For You</title>
<style>
</style>
</head>
<body>
<header class="wow slideInDown clearfix" id="header-2">
<div class="primary-menu col-lg-12 col-md-12 col-xs-12">
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<a class="navbar-brand" href="/index.html"> <img alt="" class="img-fluid" src="images/site-logo.png"/> </a>
<button class="navbar-toggle offcanvas-toggle pull-right " data-target="#js-bootstrap-offcanvas" data-toggle="offcanvas" style="float:left;" type="button">
<span class="sr-only">Toggle navigation</span>
<span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</span>
</button>
</div>
<div class="navbar-offcanvas navbar-offcanvas-touch collapse navbar-collapse" id="js-bootstrap-offcanvas">
<ul class="nav navbar-nav">
<li class="wow fadeIn" data-wow-delay=".2s"><a href="/about-us.html">ABOUT US</a></li>
<li class="wow fadeIn" data-wow-delay=".3s"><a href="/services.html">SERVICES</a></li>
<li class="wow fadeIn" data-wow-delay=".4s"><a href="/freebies.html">FREEBIES</a></li>
<li class="wow fadeIn" data-wow-delay=".5s"><a href="/portfolio.html">PORTFOLIO</a></li>
<li class="wow fadeIn" data-wow-delay=".6s"><a href="#">BLOG</a></li>
<li class="wow fadeIn" data-wow-delay=".7s"><a href="/contact.html">CONTACT US</a></li>
<li class="wow fadeIn" data-wow-delay=".8s"><a class="menu-telephone visible-xs" href="tel:+918871131188">Call Us</a></li>
<li class="wow fadeIn" data-wow-delay=".8s"><a class="menu-telephone hidden-xs" href="tel:+918871131188">+91 88-7-11-3-11-88</a></li>
</ul>
<ul class="menu-social visible-sm visible-xs wow fadeIn" data-wow-delay=".9s">
<li><a href="https://www.instagram.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
<li><a href="https://www.facebook.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
<li><a href="https://www.linkedin.com/company/acquire-infotech" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
</nav>
</div>
</header>
<div class="main-content">
<section class="main-banner both-side home-slider"><!--Main Banner section Starts-->
<div class="social-media">
<div class="media-extends">
<ul>
<li><a href="https://www.instagram.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
<li><a href="https://www.facebook.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
<li><a href="https://www.linkedin.com/company/acquire-infotech" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<div class="col-lg-5 col-xs-12 banner-text" id="slidaf">
<h1>Want to learn how to make your Website a 
Success?</h1>
<div class="small-seprater"></div>
<p>Is your website a primary driver of sales in your business? 
Is it getting a high enough level of traffic? 
Are your leads are converting?</p>
</div>
<div class="col-lg-4 banner-iphone wow fadeInDown" data-wow-delay=".5s">
<div class="iphone-container">
<div class="iphone">
<div class="iphone-content stony-iphon-slider" id="iphone-animated-duplicate"></div>
</div>
</div>
</div>
<div class="row button-row home-btn">
<div class="col-lg-12">
<div class="home-color-btn">
<a class="" data-popup-open="popup-1" href="#">Download Free Guide</a>
<a class="free-audit" data-popup-open="popup-2" href="#">Request Free Audit</a>
</div>
<div class="popup" data-popup="popup-1">
<div class="popup-inner">
<div class="content-style-form content-style-form-1">
<h2>Free Guide</h2>
<form action="mailer.php" id="ajax-contact" method="post" name="contact">
<div class="field">
<label for="name">Name:</label>
<input id="name" name="name" required="" type="text"/>
</div>
<div class="field">
<label for="email">Email:</label>
<input id="email" name="email" required="" type="email"/>
</div>
<div class="field text-center">
<button type="submit">Submit</button>
</div>
</form>
<div id="form-messages"></div>
<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
</div>
</div>
</div>
<div class="popup" data-popup="popup-2">
<div class="popup-inner">
<div class="content-style-form content-style-form-1">
<h2>Free Audit</h2>
<form action="mailer2.php" id="audit-contact" method="post" name="audit">
<div class="field">
<label for="name">Name:</label>
<input id="name" name="name" required="" type="text"/>
</div>
<div class="field">
<label for="email">Email:</label>
<input id="email" name="email" required="" type="email"/>
</div>
<div class="field">
<label for="Website">Website:</label>
<input id="website" name="website" required="" type="text"/>
</div>
<div class="field text-center">
<button type="submit">Submit</button>
</div>
</form>
<div id="audit-messages"></div>
<a class="popup-close" data-popup-close="popup-2" href="#">x</a>
</div>
</div>
</div>
</div>
</div>
<a href="#down"><div class="icon-scroll"></div></a>
</section><!--Main Banner Section End-->
<div class="expert-space" id="down"></div>
<section class="expertise both-side slide slide-one home-serv" data-background="#25adff" id="one"><!--Service Section Starts-->
<div class="servic-heading cstum-heading">
<h2>AREAS</h2>
<span>of</span>
<h1>EXPERTISE</h1>
</div>
</section><!--Service Section Ends-->
<section class="game-plan both-side slide slide-two wow fadeInUp" data-background="#25adff" data-wow-duration=".3s" id="two">
<div class="expert-space"></div>
<h2>MULTIDISCIPLNED GAMEPLAN</h2>
<p>More than an impressive list of offerings, we care about how those offerings work together to build something remarkable.</p>
<div class="expert-space"></div>
<div class="disciplned row">
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Strategy</h3>
<ul>
<li>Campaign strategy</li>
<li>Brand strategy</li>
<li>Marketing strategy</li>
<li>Digital strategy</li>
<li>Content strategy</li>
<li>Social strategy</li>
</ul>
</aside>
</div>
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Creative</h3>
<ul>
<li>Content creation</li>
<li>Art direction</li>
<li>Photography</li>
<li>Video production</li>
<li>Branding</li>
<li>Graphic design</li>
</ul>
</aside>
</div>
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Design &amp; UX</h3>
<ul>
<li>UX/UI design</li>
<li>Web and mobile design</li>
<li>Application design</li>
</ul>
</aside>
</div>
</div>
<div class="disciplned row">
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Development</h3>
<ul>
<li>Website development</li>
<li>Mobile app development</li>
<li>CMS implentation</li>
<li>Mobile/responsive development</li>
<li>Ecommerce development</li>
<li>Bespoke development</li>
</ul>
</aside>
</div>
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Marketing</h3>
<ul>
<li>Google adwords</li>
<li>Search engine optimisation (SEO)</li>
<li>Search engine marketing (SEM)</li>
<li>Social media marketing</li>
<li>Email marketing</li>
</ul>
</aside>
</div>
<div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-duration=".3s">
<aside class="plan">
<h3>Insight</h3>
<ul>
<li>Reporting and analysis</li>
<li>Conversion optimisation</li>
<li>Campaign optimisation</li>
<li>User testing</li>
<li>Analytics &amp; metrics</li>
</ul>
</aside>
</div>
</div>
</section>
<div class="expert-space"></div>
<section class="email-part both-side slide slide-three" data-background="#57d781" id="three">
<div class="col-md-6 col-xs-12 wow fadeInUp" data-wow-duration=".3s"> <img alt="" class="img-responsive hidden-xs" src="images/book.png"/></div>
<div class="col-md-6">
<h2>Download the first ever transparent, cut-throat, no bullshit guide on how to actually get results with digital marketing.</h2>
<img alt="" class="img-responsive visible-xs" src="images/book.png"/>
<form action="marketing-mail.php" id="marketing-contact" method="post" name="audit">
<div class="form-group">
<input id="email" name="email" placeholder="Your email" required="" type="email"/>
<button type="submit">Download</button>
</div>
</form>
<div id="marketing-messages"></div>
</div>
</section>
<section class="featured-projects slide slide-four" data-background="#57d781" id="four">
<div class="my-heading">
<h2>Featured Projects</h2>
</div>
<div class="expert-space"></div>
<div class="box both-side">
<div class="feat-img">
<div class="col-md-3">
<div class="iphone-container onecenter-iphone wow fadeInUp" data-wow-duration=".8s">
<div class="iphone">
<div class="iphone-content " id="iphone-animated"></div>
</div>
</div>
</div>
</div>
<div class="feat-text">
<div class="my-text col-md-9 home-1center-port">
<div class="expert-space"></div>
<div class="expert-space"></div>
<h2 class="">1Centre</h2>
<h3 class="digitalexp">Creating an engaging digital experience for NZâs first digital trade application platform.</h3>
<p class="port-mobile-hide">1Centre is a business platform that digitisies the way suppliers and consumers connect. With a focusing on showcasing their product and usablilty, we designed and built a website that brings their brand to life.</p>
<a class="read-m" href="#">Find out more about how we helped 1Center launch with their platform</a>
<div class="new-spoint">
<p><span>01</span> Responsive Website Design</p>
<p><span>02</span> UI/UX Design</p>
<p><span>03</span> Website Development</p>
</div>
</div>
</div>
</div>
<div class="expert-space"></div>
<div class="box2 both-side slide slide-four" data-background="#fba6c9">
<div class="box2-feat-text">
<div class="box2-my-text col-md-6 home-vine-yard">
<h2 class="">Stonyridge Vineyard</h2>
<h3 class="">Designing a modern &amp; responsive website that showcases their beautiful wines, culture &amp; history.</h3>
<a class="" href="#">Learn more about how we helped them generate leads</a>
<div class="new-spoint">
<p><span>01</span> Responsive Website Design</p>
<p><span>02</span> UI/UX Design</p>
<p><span>03</span> Website Development</p>
</div>
</div>
</div>
<div class="our-web">
<div class="col-md-6">
<div class="mac-container wow fadeInUp" data-wow-duration=".8s">
<div class="mac stony-ridge-mac">
<div class="mac-content " id="mac-animated"></div>
</div>
</div>
</div>
</div>
</div>
<div class="expert-space"></div>
<div class="expert-space"></div>
</section>
<section class="home-get get-together slide-five wow fadeInUp" data-background="#e67e22" data-wow-duration=".3s" id="five">
<div class="expert-space"></div>
<h1 class="wow fadeInUp" data-wow-duration=".3s">Letâs get together and throw some ideas around.</h1>
<a class="wow fadeInUp" data-wow-duration=".3s" href="/contact.html">Contact Us</a>
<div class="expert-space"></div>
</section>
<section class="digital-market both-side slide-six" id="seven">
<div class="expert-space"></div>
<div class="expert-space"></div>
<h2 class="wow fadeInUp" data-wow-duration=".3s">Digital marketing results.</h2>
<p class="wow fadeInUp" data-wow-duration=".3s">We believe in helping brands create through strategy, digital products and integrated experiences on web and mobile. Our team becomes an extension of your business, helping you achieve results.</p>
<a class="wow fadeInUp" data-wow-duration=".3s" href="/portfolio.html">Our Work</a><a class="wow fadeInUp" data-popup-open="popup-2" data-wow-duration=".3s" href="#">Free Audit</a>
<div class="expert-space"></div>
<div class="expert-space"></div>
</section>
<section class="get-map" id="eight" style="display: inline-block;height: 400px;width: 100%;"><!-- Map Section Starts-->
<div class="la">
<h2 style="display:none;">test</h2>
</div>
<!--  <div class="overlay" onClick="style.pointerEvents='none'"></div>-->
<div style="overflow:hidden;width:100%;height:400px;resize:none;max-width:100%;">
<div id="my-map-canvas" style="height:100%; width:100%;max-width:100%;">
<div id="map"></div>
<script src="js/map.js"></script>
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwokew-eyHQ1WFj4Sgvpch7odLZOeNtSk&amp;callback=initMap">
</script>
</div>
</div>
</section><!-- Map Section Ends-->
</div>
<footer class="both-side">
<div class="expert-space"></div>
<aside class="footer-item hidden-xs">
<div class="col-md-4">
<h3>Weâre a forward thinking digital agency</h3>
<p>Based in India &amp; U.K. we offer web design, web &amp; mobile development, e-commerce, branding, SEO services, Google AdWords and social media marketing.</p>
<a href="#"><img alt="" src="images/site-logo.png"/></a>
</div>
</aside>
<aside class="footer-item">
<div class="col-md-4 contact-us mobile-footer">
<h3>Contact us</h3>
<p>103, Royal Ratan Building</p>
<p>7, M.G. Road Indore (M.P.)</p>
<p>India</p>
<a class="mf-btn" href="tel:08871131188">+91 8871131188</a><br/>
<a class="mf-btn" href="mailto:info@acquireinfotech.co">info@acquireinfotech.co</a>
<div class="fa-icon mobile-footer-social"> <a href="https://www.instagram.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a><a href="https://www.linkedin.com/company/acquire-infotech" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a> <a href="https://www.facebook.com/acquireinfotech/" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></div>
</div>
</aside>
<aside class="footer-item hidden-xs">
<div class="col-md-4">
<h3>Services</h3>
<ul>
<li><a href="/website-design.html">Web design</a></li>
<li><a href="/website-development.html">Web Developement</a></li>
<li><a href="/seo-services.html">SEO Services</a></li>
<li><a href="/adwords.html">Google AdWords</a></li>
<li><a href="/ecommerce-development.html">Ecommerce Development</a></li>
</ul>
</div>
</aside>
<div class="expert-space"></div>
</footer>
<script>
$( window ).ready(function() {

  var wHeight = $(window).height();

  $('.slide')
    .height(wHeight)
    .scrollie({
      scrollOffset : -50,
      scrollingInView : function(elem) {

        var bgColor = elem.data('background');

        $('body').css('background-color', bgColor);

      }
    });

});
</script>
<script type="text/javascript">
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
      
    }
  });
});
</script>
<script>
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        $('body').addClass('mmodal');
        e.preventDefault();
    });
 
    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        $('body').removeClass('mmodal');
 
        e.preventDefault();
    });
});

</script>
<script src="js/app.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoot-scroll.js"></script>
<script src="js/cstume.js"></script>
<script src="js/bootstrap.offcanvas.js"></script>
<script src="js/classie.js"></script>
<script>new WOW().init();</script>
</body>
</html>
