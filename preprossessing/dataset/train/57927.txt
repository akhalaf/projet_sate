<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="ABES, Engenharia, SanitÃ¡ria, Ambiental, AssociaÃ§Ã£o" name="keywords"/>
<meta content="AssociaÃ§Ã£o Brasileira de Engenharia SanitÃ¡ria e Ambiental" name="description"/>
<meta content="Pedro Pires Gazzana" name="author"/>
<meta content="ABES RS - AssociaÃ§Ã£o Brasileira de Engenharia SanitÃ¡ria e Ambiental" property="og:title"/>
<meta content="https://www.abes-rs.org.br/" property="og:url"/>
<meta content="AssociaÃ§Ã£o Brasileira de Engenharia SanitÃ¡ria e Ambiental" property="og:description"/>
<meta content="http://www.abes-rs.org.br/site/img/icone_abesrs.png" property="og:image"/>
<link href="inc/bootstrap/css/bootstrap.min.css?check=1610603522" rel="stylesheet"/>
<link href="inc/fontawesome/css/all.css?check=1610603522" rel="stylesheet"/>
<link href="inc/owlcarousel/owl.carousel.min.css?check=1610603522" rel="stylesheet"/>
<link href="inc/owlcarousel/owl.theme.default.min.css?check=1610603522" rel="stylesheet"/>
<link href="inc/bootstrap-datepicker/css/bootstrap-datepicker.standalone.css?check=1610603522" rel="stylesheet"/>
<link href="css/geral.css?check=1610603522" rel="stylesheet"/>
<link href="css/custom.css?check=1610603522" rel="stylesheet"/>
<link href="inc/fancybox-master/jquery.fancybox.min.css?check=1610603522" rel="stylesheet"/>
<link href="img/favicon.ico?check=1610603522" rel="shortcut icon" type="image/x-icon"/>
<link href="img/favicon.ico?check=1610603522" rel="icon" type="image/x-icon"/>
<script src="inc/jquery/jquery-3.3.1.min.js?check=1610603522"></script>
<script src="inc/popper/popper.min.js?check=1610603522"></script>
<script src="inc/bootstrap/js/bootstrap.min.js?check=1610603522"></script>
<script src="inc/owlcarousel/owl.carousel.min.js?check=1610603522"></script>
<script src="inc/bootstrap-datepicker/js/bootstrap-datepicker.min.js?check=1610603522"></script>
<script src="inc/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js?check=1610603522"></script>
<script src="inc/fancybox-master/jquery.fancybox.min.js?check=1610603522"></script>
<script src="js/funcao.js?check=1610603522"></script>
<title>ABES RS - AssociaÃ§Ã£o Brasileira de Engenharia SanitÃ¡ria e Ambiental</title>
</head>
<body>
<div class="container mt-2 cabecalho-bloco">
<div class="row">
<div class="col-6 offset-6 text-right">
<a href="https://www.facebook.com/abesrs.oficial" target="_blank"><i class="fab fa-facebook-square fa-2x mr-2"></i></a>
<a href="https://twitter.com/Abes_RS" target="_blank"><i class="fab fa-twitter-square fa-2x"></i></a>
</div>
</div>
<div class="row d-flex align-items-center my-3">
<div class="col-6">
<a href="index.php">
<img alt="Logo ABES RS" class="img-fluid" src="img/logo_abesrs.png"/>
</a>
</div>
<div class="col-6 text-right">
<a class="btn btn-light btn-lg text-uppercase btn-b px-4" href="http://abes-dn.org.br/?page_id=6438" target="_blank">
<i class="fas fa-star"></i>
            Seja sÃ³cio
        </a>
</div>
</div> </div>
<div class="container-fluid fundo-cor-1 menu-principal-bloco">
<div class="container">
<div class="row mb-1">
<div class="col-12">
<nav class="navbar navbar-expand-lg menu navbar-light p-0 py-2">
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler text-white" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<i class="fas fa-bars"></i>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav w-100 d-flex justify-content-between">
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link" href="index.php" id="navbarIndex" role="button">
<i class="fas fa-home"></i>
                                PÃ¡gina inicial
                            </a>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarInstitucional" role="button">
                                Institucional
                            </a>
<div aria-labelledby="navbarInstitucional" class="dropdown-menu">
<a class="dropdown-item" href="conheca.php">ConheÃ§a a Abes-RS</a>
<a class="dropdown-item" href="diretoria.php">Diretoria SeÃ§Ã£o RGS</a>
<a class="dropdown-item" href="representacao.php">RepresentaÃ§Ãµes RGS</a>
<a class="dropdown-item" href="https://www.livrariaabes.com.br/" target="_blank">Livraria virtual</a>
<a class="dropdown-item" href="http://abes-dn.org.br/" target="_blank">ABES Nacional</a>
<a class="dropdown-item" href="http://abes-dn.org.br/?page_id=6438" target="_blank">Torne-se SÃ³cio</a>
</div>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarEventos" role="button">
                                Eventos e Cursos
                            </a>
<div aria-labelledby="navbarEventos" class="dropdown-menu">
<a class="dropdown-item" href="evento.php?f=1" target="">Eventos e cursos ativos</a>
<a class="dropdown-item" href="evento.php?f=2" target="">SeminÃ¡rios e SimpÃ³sios</a>
<a class="dropdown-item" href="evento.php?f=3" target="">Ciclos de Palestras</a>
<a class="dropdown-item" href="evento.php?f=4" target="">Ciclos de Debates</a>
<a class="dropdown-item" href="evento.php?f=5" target="">Ciclos de SaÃºde Ambiental</a>
<a class="dropdown-item" href="evento.php?f=6" target="">ReuniÃµes AlmoÃ§o</a>
<a class="dropdown-item" href="http://www.premiojornalismoambiental.com.br/?f=7" target="_blank">PrÃªmio Lutzenberger</a>
<a class="dropdown-item" href="evento.php?f=8" target="">Eventos Anteriores</a>
<a class="dropdown-item" href="evento.php?f=9" target="">Cursos</a>
<a class="dropdown-item" href="evento.php?f=10" target="">Cursos Anteriores</a>
</div>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarCamaras" role="button">
                                CÃ¢maras tÃ©cnicas
                            </a>
<div aria-labelledby="navbarCamaras" class="dropdown-menu">
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=1" target="_self">CÃ¢mara TÃ©cnica de Recursos HÃ­dricos</a>
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=2" target="_self">CÃ¢mara TÃ©cnica de ResÃ­duos SÃ³lidos</a>
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=3" target="_self">CÃ¢mara TÃ©cnica de GestÃ£o de Perdas de Ãgua</a>
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=4" target="_self">CÃ¢mara TÃ©cnica de  Saneamento</a>
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=6" target="_self">CÃ¢mara TÃ©cnica de Indicadores de Desempenho para o Saneamento Ambiental</a>
<a class="dropdown-item" href="camaraTecnica.php?idcamaraTecnica=7" target="_self">GT Qualidade da Ãgua</a>
</div>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link" href="jps.php" id="navbarJps" role="button">
                                JPS
                            </a>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link" href="artigo.php" id="navbarFiquePorDentro" role="button">
                                Fique por dentro
                            </a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</div> </div>
<div class="main-bloco">
<div class="container-fluid bannerHome py-2">
<div class="row">
<div class="col-12">
<div class="carousel slide" data-ride="carousel" id="carouselHome">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carouselHome"></li>
<li class="" data-slide-to="1" data-target="#carouselHome"></li>
<li class="" data-slide-to="2" data-target="#carouselHome"></li>
<li class="" data-slide-to="3" data-target="#carouselHome"></li>
<li class="" data-slide-to="4" data-target="#carouselHome"></li>
<li class="" data-slide-to="5" data-target="#carouselHome"></li>
</ol>
<div class="carousel-inner">
<div class="carousel-item active">
<img alt="Boas Festas!" class="d-block img-fluid imagem-banner-home" src="upload/banner/5fe22793dc2fc.png" title="Boas Festas!"/>
</div>
<div class="carousel-item ">
<a href="https://www.abes-rs.org.br/site/eventoDetalhe.php?eventoid=136" target="_self">
<img alt="Semana da Ã¡gua 2020" class="d-block img-fluid" src="upload/banner/5f64fce33d811.jpg" title="Semana da Ã¡gua 2020"/>
</a>
</div>
<div class="carousel-item ">
<img alt="Comunicado ABES - RS - Devido a pandemia de CoronavÃ­rus, a sede da ABES -RS estarÃ¡ fechada por tempo" class="d-block img-fluid imagem-banner-home" src="upload/banner/5f04aa718feb6.jpg" title="Comunicado ABES - RS - Devido a pandemia de CoronavÃ­rus, a sede da ABES -RS estarÃ¡ fechada por tempo"/>
</div>
<div class="carousel-item ">
<a href="https://www.abes-rs.org.br/site/upload/ckeditor/1031479997.pdf" target="_blank">
<img alt="Revista Afluente" class="d-block img-fluid" src="upload/banner/5f3ebe9a7a414.png" title="Revista Afluente"/>
</a>
</div>
<div class="carousel-item ">
<a href="https://mbappp.com/" target="_blank">
<img alt="Curso FSPSP" class="d-block img-fluid" src="upload/banner/5f68a7904df27.jpeg" title="Curso FSPSP"/>
</a>
</div>
<div class="carousel-item ">
<a href="https://mbasaneamento.com/" target="_blank">
<img alt="curso FSPSP" class="d-block img-fluid" src="upload/banner/5f68a7dd103cc.jpeg" title="curso FSPSP"/>
</a>
</div>
</div>
<a class="carousel-control-prev" data-slide="prev" href="#carouselHome" role="button">
<span aria-hidden="true">
<i class="fas fa-arrow-circle-left fa-3x"></i>
</span>
</a>
<a class="carousel-control-next" data-slide="next" href="#carouselHome" role="button">
<span aria-hidden="true"></span>
<span aria-hidden="true">
<i class="fas fa-chevron-circle-right fa-3x"></i>
</span>
</a>
</div>
</div>
</div>
</div>
<div class="container mt-4 fique-por-dentro py-1">
<div class="row d-flex align-items-center">
<div class="col-12 col-md-7">
<h1>
<img src="img/icones/fiquepordentro.png"/>
<span>Fique</span> por dentro
            </h1>
</div>
<div class="col-12 col-md-5 text-right">
<a class="btn btn-outline-info" href="artigo.php">
<i class="fas fa-info-circle mr-2"></i>
                Veja mais informaÃ§Ãµes
            </a>
</div>
</div>
<div class="row align-self-stretch mt-2 d-flex justify-content-center">
<div class="col-12 col-md-6 col-lg-4">
<div class="card h-100 border-0">
<div class="card-body text-center">
<div class="text-left mb-2">
<small class="text-muted">
<i class="fas fa-calendar-day mr-2"></i>
                            26/11/2020
                        </small>
</div>
<h5 class="card-title text-center">PremiaÃ§Ã£o PNQS.</h5>
<p class="card-text text-justify">
</p><p style="text-align:justify"> </p>
<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="background-color:white"><span style="color:#2a2a2a">A Comissão do CNQA – Comitê Nacional da Qualidade ABES divulgou nesta quarta, 25 de novembro, as empresas reconhecidas nas Categorias AMEGSA (As Melhores em Gestão no Saneamento Ambiental) e SQFSA do Prêmio Nacional de Qualidade em Saneamento – PNQS Ciclo 2020. </span></span></span></span></span></p>
<p style="text-align:justify"> </p>
<p style="text-align:justify"> </p>
</div>
<div class="card-footer bg-transparent">
<a class="btn btn-primary btn-block text-uppercase btn-a stretched-link" href="artigoDetalhe.php?idartigo=261" title="PremiaÃ§Ã£o PNQS.">Saiba mais</a>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-4">
<div class="card h-100 border-0">
<div class="card-body text-center">
<div class="text-left mb-2">
<small class="text-muted">
<i class="fas fa-calendar-day mr-2"></i>
                            28/10/2020
                        </small>
</div>
<h5 class="card-title text-center">Parceria Abes-RS x Sindienergia-RS</h5>
<p class="card-text text-justify">
</p><p style="text-align:justify"><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">No dia 23 de outubro reuniram-se, a Abes-RS representada pelo Ricardo Roüver Machado e Maria Lucia Coelho e Silva e o Sindienergia (Sindicato das Indústrias de Energia Renováveis do Rio Grande do Sul) representado pela Diretora Daniela Cardeal e o Coordenador  Alexandre Bugin, a fim de, firmar convênio de ações de convergências sobre energias renováveis no saneamento. </span></span></p>
<p> </p>
</div>
<div class="card-footer bg-transparent">
<a class="btn btn-primary btn-block text-uppercase btn-a stretched-link" href="artigoDetalhe.php?idartigo=260" title="Parceria Abes-RS x Sindienergia-RS">Saiba mais</a>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-4">
<div class="card h-100 border-0">
<div class="card-body text-center">
<div class="text-left mb-2">
<small class="text-muted">
<i class="fas fa-calendar-day mr-2"></i>
                            21/09/2020
                        </small>
</div>
<h5 class="card-title text-center">Parceria Abes-RS x FSPSP</h5>
<p class="card-text text-justify">
<img alt="" class="img-fluid w-50 float-left pr-2 fique-por-dentro-imagem" src="upload/artigo/5f68ed8bd8590.png" title=""/>
</p><p style="text-align:justify"><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">No dia 8 de setembro de 2020, a Diretoria da Abes seção RS, reuniu-se com  Carlos Alexandre Nascimento,<em> </em></span></span><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><span style="background-color:white">Coo<span style="color:#202124">ordenador Geral do MBA PPP e Concessões e Diretor de Programas da LSE Custom Programmes e o  </span></span><strong>Rafael Castilho<em>, </em></strong><span style="background-color:white">Coordenador Administrativo do MBA Saneamento Ambiental e do MBA PPP e Concessões, para tratar de parceria Abes-RS x SFPS. Estão sendo tratados descontos aos sócios na realização dos  cursos MBA Saneamento Ambiental e MBA PPP e Concessões.</span></span></span></p>
</div>
<div class="card-footer bg-transparent">
<a class="btn btn-primary btn-block text-uppercase btn-a stretched-link" href="artigoDetalhe.php?idartigo=259" title="Parceria Abes-RS x FSPSP">Saiba mais</a>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid fundo-icones py-1 my-3">
<div class="container my-5 icones">
<div class="row my-4">
<div class="col-12">
<h1 class="text-white">
<a href="#"><img src="img/icones/conheca.png"/></a>
                    ConheÃ§a <span>a ABES</span>
</h1>
</div>
</div>
<div class="row my-3 icones-atalho">
<div class="col-12 col-sm-6 col-md-3 text-center">
<a class="stretched-link" href="http://abes-dn.org.br/?page_id=6438" target="_blank">
<img alt="SEJA SÃCIO" class="figure-img img-fluid" src="upload/iconeAtalho/5ebc9bea24290.png" title="SEJA SÃCIO"/>
</a>
<h2>SEJA SÃCIO</h2>
</div>
<div class="col-12 col-sm-6 col-md-3 text-center">
<a class="stretched-link" href="diretoria.php" target="_self">
<img alt="CONHEÃA A DIRETORIA" class="figure-img img-fluid" src="upload/iconeAtalho/5ebca4524200b.png" title="CONHEÃA A DIRETORIA"/>
</a>
<h2>CONHEÃA A DIRETORIA</h2>
</div>
<div class="col-12 col-sm-6 col-md-3 text-center">
<a class="stretched-link" href="evento.php" target="_self">
<img alt="EVENTOS E CURSOS" class="figure-img img-fluid" src="upload/iconeAtalho/5ebca46aef3c5.png" title="EVENTOS E CURSOS"/>
</a>
<h2>EVENTOS E CURSOS</h2>
</div>
<div class="col-12 col-sm-6 col-md-3 text-center">
<a class="stretched-link" href="camaraTecnica.php" target="_self">
<img alt="CÃMARAS TÃCNICAS" class="figure-img img-fluid" src="upload/iconeAtalho/5ebca48289b36.png" title="CÃMARAS TÃCNICAS"/>
</a>
<h2>CÃMARAS TÃCNICAS</h2>
</div>
</div>
</div>
</div>
<div class="container py-1 parceiros">
<div class="row my-4">
<div class="col-12">
<h1>
<img src="img/icones/parceiros.png"/>
                Nossos <span>Parceiros</span>
</h1>
</div>
</div>
<div class="row">
<div class="owl-carousel owl-theme owl-carousel-parceiro">
<div class="item">
<a href="https://www.corsan.com.br/" target="_blank">
<img alt="Corsan" class="d-block" src="upload/parceiro/5f0a0fd1d5b7d.png" title="Corsan"/>
</a>
</div>
<!--
            <div class="item">
                <a href="https://www.corsan.com.br/" target="_blank">
                    <img src="upload/parceiro/corsan.png" class="d-block" alt="" title="">
                </a>
            </div>
            -->
</div>
</div>
<div class="row my-2">
<div class="col-12 text-center mt-4">
<a class="btn btn-b" data-target="#modalApoiadores" data-toggle="modal" href="#">
<i class="fas fa-hand-point-right"></i>
                ConheÃ§a os nossos apoiadores</a>
</div>
</div>
</div>
<div aria-hidden="true" aria-labelledby="modalApoiadoresTitulo" class="modal fade" id="modalApoiadores" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="modalApoiadoresTitulo">ConheÃ§a nossos apoiadores</h5>
</div>
<div class="modal-body py-3">
<div class="row">
<div class="col-10 offset-1 text-center">
</div>
</div>
</div>
<div class="modal-footer">
<button class="btn btn-secondary" data-dismiss="modal" type="button">Fechar</button>
</div>
</div>
</div>
</div>
<script>
    $(document).ready(function() {
        var quantidadeParceiro = $('.owl-carousel-parceiro .item').length;
        var resoluca0 = 1;
        var resoluca1 = (quantidadeParceiro >= 3 ? 3 : quantidadeParceiro);
        var resoluca2 = (quantidadeParceiro >= 5 ? 5 : quantidadeParceiro);
        $('.owl-carousel-parceiro').owlCarousel({
            loop:true,
            nav: false,
            dots: false,
            margin:0,
            responsiveClass:true,
            autoplayTimeout: 2500,
            autoplay: true,
            autoWidth: false,
            responsive:{
                0:{
                    items: resoluca0
                },
                600:{
                    items: resoluca1
                },
                1000:{
                    items: resoluca2
                }
            }
        })
    });
</script> </div>
<div class="container-fluid rodape py-4 mt-3 rodape-bloco">
<div class="container">
<div class="row">
<div class="col-12 col-md-8">
<div class="row">
<div class="col-12 col-md-6 text-center text-md-left">
<h3 class="mb-4">Institucional</h3>
<p><a href="conheca.php">ConheÃ§a a Abes-RS</a></p>
<p><a href="diretoria.php">Diretoria SeÃ§Ã£o RGS</a></p>
<p><a href="representacao.php">RepresentaÃ§Ãµes RGS</a></p>
<p><a href="https://www.livrariaabes.com.br/" target="_blank">Livraria virtual</a></p>
<p><a href="http://abes-dn.org.br/" target="_blank">ABES Nacional</a></p>
<p><a href="http://abes-dn.org.br/?page_id=6438" target="_blank">Torne-se SÃ³cio</a></p>
</div>
<div class="col-12 col-md-6 text-center text-md-left">
<h3 class="mb-4">Eventos e cursos</h3>
<p>
<a href="evento.php?f=1" target="" title="Eventos e cursos ativos">Eventos e cursos ativos</a>
</p>
<p>
<a href="evento.php?f=2" target="" title="SeminÃ¡rios e SimpÃ³sios">SeminÃ¡rios e SimpÃ³sios</a>
</p>
<p>
<a href="evento.php?f=3" target="" title="Ciclos de Palestras">Ciclos de Palestras</a>
</p>
<p>
<a href="evento.php?f=4" target="" title="Ciclos de Debates">Ciclos de Debates</a>
</p>
<p>
<a href="evento.php?f=5" target="" title="Ciclos de SaÃºde Ambiental">Ciclos de SaÃºde Ambiental</a>
</p>
<p>
<a href="evento.php?f=6" target="" title="ReuniÃµes AlmoÃ§o">ReuniÃµes AlmoÃ§o</a>
</p>
<p>
<a href="http://www.premiojornalismoambiental.com.br/?f=7" target="_blank" title="PrÃªmio Lutzenberger">PrÃªmio Lutzenberger</a>
</p>
<p>
<a href="evento.php?f=8" target="" title="Eventos Anteriores">Eventos Anteriores</a>
</p>
<p>
<a href="evento.php?f=9" target="" title="Cursos">Cursos</a>
</p>
<p>
<a href="evento.php?f=10" target="" title="Cursos Anteriores">Cursos Anteriores</a>
</p>
</div>
<!--
                <div class="col-12 col-md-4 text-center text-md-left">
                    <h3 class="mb-4">Cursos</h3>
                    <p><a href="#">Recursos HÃ­dricos</a></p>
                    <p><a href="#">Residuos SÃ³lidos</a></p>
                    <p><a href="#">GestÃ£o de Perdas de Ãgua</a></p>
                    <p><a href="#">Esgotamento SanitÃ¡rio</a></p>
                    <p><a href="#">Ger. de Ãreas Contaminadas</a></p>
                    <p><a href="#">Indicadores de Desempenho</a></p>
                </div>
                -->
</div>
</div>
<div class="col-12 col-md-4 border-left px-4 text-center mt-3 mt-md-0">
<img alt="Logo ABES RS" class="img-fluid mb-4" src="img/logo_abesrs_branco.png" title="Logo ABES RS"/>
<p>Av. JÃºlio de Castilhos, 440, sala 131 (13Âº andar) - Centro - Porto Alegre - RS CEP: 90030-130</p>
<p>Telefone: (51) 3212-1375<br/>E-mail: abes-rs@abes-rs.org.br</p>
<p class="font-weight-bold">HORÃRIO DE FUNCIONAMENTO</p>
<p><small>Segunda a Sexta das 9:30 Ã s 12:00 e 14:00 Ã s 17:30</small></p>
</div>
</div>
</div> </div>
</body>
</html>