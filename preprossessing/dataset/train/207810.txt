<!DOCTYPE html>
<!--[if IE]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]--><html><head>
<title>BellaSkin Plus</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="css/form.css" rel="stylesheet"/>
<link href="./css/style.css" rel="stylesheet" type="text/css"/>
<meta content="skin whitening, proven results, age spots, sun damage, revitalize, moisturize, skin clarity, natural ingredients" name="keywords"/>
<meta content="BellaVei is a fast acting skin whitening cream that creates a luminous glow,  revitalizes skin, optimizes skin clarity and lightens the skin colour." name="description"/>
<link href="images/favicon.ico" rel="icon"/>
</head>
<body class="ng-scope ng-animate index-new" oncontextmenu="return false" ondragstart="return false;" ondrop="return false;">
<div class="outer-container ng-scope">
<header class="ng-scope">
<div class="container">
<figure class="logo">PURE REJUVENATING SKIN CARE</figure>
<figure class="bottle"></figure>
<figure class="model"><span class="before">Before</span><span class="after">After</span></figure>
<div class="headline">
<span class="text1">WHITEN</span>
<span class="text2">MOISTURIZE</span>
<span class="text3">PROTECT</span>
</div>
<ul class="list">
<li>Revitalizes skin</li>
<li>Optimizes skin clarity</li>
<li>Made for daily use</li>
</ul>
<p class="txt1">BellaSkin Plus is a fast-acting skin whitening cream that creates a luminous glow and helps diminish the appearance of visible spots, freckles and dullness.</p>
<a class="btn btn-primary btn-send" href="step2.php" onclick="submitForm(event)"><span class="text">SEND MY PACKAGE!</span></a>
<div><figure class="badge-icon"></figure></div>
</div>
</header>
<div class="ng-scope" id="main">
<section class="sec1">
<div class="container">
<figure class="badge_expert"></figure>
<figure class="seen-on">As seen in:</figure>
<h2 class="title1">FAST WHITENING<br/>ACTION</h2>
<div class="text">
<p class="txt1"><strong>BellaSkin Plus Skin Whitening cream goes to work from day 1.</strong>Just apply the product evenly throughout the desired area, and in just a matter of days you will start to see a natural fade in your skin tone, whitening your outward appearance!</p>
<p class="txt2">Many other skin whiteners you may have tried leave your skin feeling irritated. But thanks to BellaSkin Plus's proprietary blend of botanicals, your skin will be left feeling amazing without any harsh stinging or irritation.</p>
</div>
<h1 class="title2">DRAMATIC, CLINICALLY PROVEN RESULTS<small> BellaSkin Plus Skin Whitening System is Proven in Clinical Studies Worldwide!</small></h1>
<figure class="whitener"></figure>
</div>
</section>
<section class="sec2">
<div class="container">
<div class="left">
<h2 class="title1">How Skin Whitening Works</h2>
<p class="txt1"><strong>Watch your skin progressively lighten,</strong>making age spots and sun damage disappear. All this while repairing your skin's natural elasticity, leaving you with that bright, youthful look that we all desire!</p>
<figure class="skin-img">
<span class="part1">Skin Whitening Cream</span>
<span class="part2">Age Spot</span>
<span class="part3">Excess<br/>Melanin</span>
<span class="part4">Epidermisla</span>
<span class="part5">Dermis</span>
<span class="part6">An excess amount of melanin, the pigment that determines hair and skin color, causes age and sun spots to appear.</span>
<span class="part6">Apply BellaSkin Plusâs Face Whitening cream directly to your skinâs troubled areas, gently massaging it in until completely absorbed.</span>
<span class="part6">The cream will begin to break down particles of melanin into smaller particles leaving the age or sun spot looking speckled.</span>
<span class="part6">The small particles of melanin will rise to the skinâs surface and begin to flake away as part of the bodyâs natural waste process.</span>
<span class="part6">Remaining particles flake away from the skinâs surface, leaving the skin smooth and clear.</span>
</figure>
</div>
<div class="right">
<h2 class="title2">Natural Ingredients</h2>
<p class="txt2"><strong>BellaSkin Plus's Skin Whitening System is the perfect blend</strong> of powerful ingredients to produce dramatic results, while also providing your skin with the protection and nourishment it needs to really glow!</p>
<figure class="ingredients">
<span class="item1"><strong>Curcumin</strong>Helps regulate the synthesis of melanin</span>
<span class="item1"><strong>Aloe Vera</strong>Improves moisturization and pigment suppression</span>
<span class="item1"><strong>X50 PURE WHITE</strong>A special complex of ingredients that promotes skin elasticity and fights inflammation</span>
<span class="item1"><strong>Vitamin E</strong>Helps reduce the depth of microwrinkles</span>
</figure>
</div>
</div>
</section>
<section class="sec3">
<div class="container">
<div class="left">
<h4 class="title1">
<small>Fast-Acting Visible PIGMENT<span>WHITENING &amp; PROTECTION</span></small>
Before <span> &amp; </span> After
</h4>
<figure class="benefits">
<span class="ben1">Easy to apply.</span>
<span class="ben1">Cover exposed areas.</span>
<span class="ben1">Fast acting results!</span>
</figure>
</div>
<div class="right">
<figure class="logo">PURE REJUVENATING SKIN CARE</figure>
<h4 class="title2">LEARN THE SECRET THAT CELEBRITIES ARE USING FOR BRIGHTER, YOUTHFUL LOOKING SKIN</h4>
<p class="txt1">Celebrities throughout Asia are in on the secret to whiter, more youthful looking skin: The BellaSkin Plus Skin Whitening System. Some of China's biggest stars such as Phoemela Baranda and Zhang Ziyi utilize the power of the BellaSkin Plus Skin Whitening System to keep their youthful glow, with their much desired lightened skin tone.</p>
<h5 class="title3"><strong>Phoemela Baranda,</strong>Actress</h5>
<p class="txt2">See the amazing results that only BellaSkin Plus can deliver. Make it your celebrity secret!</p>
</div>
</div>
</section>
<section class="sec5">
<div class="container">
<div class="section-content two">
<div class="left">
<section class="title">
<p class="heading-title">WE'VE GOT YOU COVERED</p>
<p class="heading-subtitle">with BellaSkin Plus' Skin Whitening System!</p>
</section>
<figure class="img-bottle"></figure>
<section class="logo-holder">
<figure class="logo"></figure>
<p class="subtitle">PURE REJUVENATING SKIN CARE</p>
</section>
</div>
<div class="right">
<div class="arrow">
<div class="arrow-inner">
<p class="title">Promote Radiant Skin</p>
<p class="subtitle">while fighting the aging process</p>
</div>
</div>
<p class="text full">BellaSkin Plus' Skin Whitening System gently softens skin tones, bringing out its beautiful, natural color without the harsh discoloration brought on by extended sun exposure.</p>
<p class="text normal">BellaSkin Plus's Skin Whitening System is the perfect blend of powerful ingredients to produce dramatic results, while also providing your skin with the protection and nourishment it needs to really glow! Watch your skin progressively lighten, making age spots and sun damage disappear. All this while repairing your skin's natural elasticity, leaving you with that bright, youthful look that we all desire.</p>
<div class="footer-heading">
<p class="text blue01">NO NEEDLES, NO PAIN,</p>
<p class="text blue02">JUST BEAUTIFUL SKIN IN</p>
<p class="text blue03">LESS THAN A MONTH!</p>
</div>
</div>
</div>
<div class="section-content three">
</div>
</div>
</section>
<section class="sec4">
<div class="container">
<figure class="logo">PURE REJUVENATING SKIN CARE</figure>
<figure class="bottles"></figure>
<figure class="seen-on">As seen in:</figure>
<figure class="badge_satisfaction"></figure>
<figure class="badge_expert"></figure>
<div class="headline">
<span class="text1">WHITEN</span>
<span class="text2">MOISTURIZE</span>
<span class="text3">PROTECT</span>
</div>
<ul class="list">
<li>Revitalizes skin</li>
<li>Optimizes skin clarity</li>
<li>Made for daily use</li>
</ul>
<a class="btn btn-primary btn-send" href="step2.php" onclick="submitForm(event)"><span class="text">SEND MY PACKAGE!</span></a>
</div>
</section>
</div>
</div>
<form action="step2.php" id="orderform" method="post" name="form-index">
<input id="shipping_fname" name="shipping_fname" type="hidden" value="NULL"/>
<input id="shipping_email" name="shipping_email" type="hidden" value="NULL"/>
</form>
<footer>
<div class="container">
<div class="left">
<section class="logo-holder">
<figure class="logo"></figure>
<p class="subtitle">PURE REJUVENATING SKIN CARE</p>
</section>
<p class="copyright ng-binding">Copyright Â© 2016 BellaSkin Plus Inc.</p>
<figure class="badge-icon"></figure>
</div>
<p class="footer-links">
<a href="javascript: void(0)" onclick="window.open('privacy.php', 'windowname2', 'width=670, height=700, scrollbars=1'); return false;">Privacy Policy</a> |
                <a href="javascript: void(0)" onclick="window.open('terms.php', 'windowname2', 'width=670, height=700, scrollbars=1'); return false;">Terms &amp; Conditions</a> |
                <a href="javascript: void(0)" onclick="window.open('contacts.php', 'windowname2', 'width=670, height=700, scrollbars=1'); return false;">Contact Us</a>
</p>
</div>
</footer>
<script>
    var submitForm = function(e) {
        e.preventDefault();
        document.getElementById('orderform').submit();
    };
    </script>
</body>
</html>
