<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>快递单号查询-爱快递</title>
<link href="https://cdn.aikuaidi.cn/css/ngloba0310.css" rel="stylesheet" type="text/css"/> <link href="https://cdn.aikuaidi.cn/new/css/index.css" rel="stylesheet" type="text/css"/>
<meta content="gKUON1qqf5" name="baidu-site-verification"/>
<meta content="快递查询,快递单号查询,快递网点查询,爱快递" name="Keywords"/>
<meta content="爱快递网整合国内100多家快递单号查询服务,可查询包括圆通快递查询单号、顺丰快递查询、EMS快递、申通、天天快递、京东快递单号查询、韵达快递查询单号查询等国内知名快递公司快递单号跟踪查询。以及圆通速递查询、圆通查询服务于一体的网站！" name="Description"/><link href="//m.aikuaidi.cn" media="only screen and(max-width: 640px)" rel="alternate"/><meta content="format=html5;url=//m.aikuaidi.cn" name="mobile-agent"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/></head>
<script src="https://cdn.aikuaidi.cn/js/exp.js" type="text/javascript"></script>
<script src="https://apps.bdimg.com/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdn.aikuaidi.cn/js/jquery.cookie.js" type="text/javascript"></script>
<script src="https://cdn.aikuaidi.cn/layer.w/layer.js" type="text/javascript"></script>
<script src="https://cdn.aikuaidi.cn/js/index.js" type="text/javascript"></script>
<body>
<div class="ikd-hand">
<div class="hand-top ">
<div class="top1 box-shadow">
<div class="wd">
<a class="top1-logo" href="//www.aikuaidi.cn" target="_blank" title="爱快递"></a>
<div class="top1-menu clearfix">
<ul class="menu-left">
<li><a href="//www.aikuaidi.cn" style="color:#128bed" target="_blank">首页</a></li>
<li class="dropdown"><a href="//www.aikuaidi.cn/express/" target="_blank">查快递<i class="ddlist"></i></a>
<div class="menu-more menu-list">
<dl>
<dd>
<a href="//www.aikuaidi.cn/auto.html">自动识别</a>
</dd>
<dd>
<a href="//www.aikuaidi.cn/express/">快递大全</a>
</dd>
<dl>
</dl>
</dl>
</div> </li>
<li class="dropdown"><a href="//www.aikuaidi.cn/outlets/" target="_blank" title="快递网点">找网点<i class="ddlist"></i></a><div class="menu-more menu-list" style="display: none;"><dl><dd><a href="//www.aikuaidi.cn/search/">搜网点</a></dd><dd><a href="//www.aikuaidi.cn/outlets/">网点列表</a></dd></dl></div></li>
<li><a href="//www.aikuaidi.cn/tools/" target="_blank">查运费</a></li>
</ul> </div>
</div>
</div>
<div class="top2">
<h2>高效的快递查询服务</h2>
<div class="top2-tab">
<div class="tab-tit" id="tab-tit">
<div class="item active" data-id="item-ord">
<div class="top"><span>查单号</span></div>
</div>
<div class="item" data-id="item-out">
<div class="top"><span>搜网点</span></div>
</div>
</div>
</div>
<div class="top2-cont">
<div class="input-group home-group" id="item-ord"><form autocomplete="off">
<div class="search-content">
<input class="input -hg" id="no" maxlength="50" placeholder="请输快递、物流单号追踪信息 " type="search"/>
<div class="ext-content more-border" id="no-more">
<div class="more-lf">
<span>智能识别</span>
<ul id="more-auto">
<li class="history-none"><img src="https://cdn.aikuaidi.cn/images/h-none.png"/><br/><br/> 暂无数据</li>
</ul>
</div>
<div class="more-rg">
<span>查询历史</span>
<ul id="more-history">
<li class="history-none"><img src="https://cdn.aikuaidi.cn/images/h-none.png"/><br/><br/> 暂无数据</li>
</ul>
</div>
</div>
<div class="ext-result" id="result">
<div class="result-tit">
<ul>
<li><img src="https://cdn.aikuaidi.cn/images/gwyy.png"/><span class="rt-name">.</span><i>|</i></li>
<li><img src="https://cdn.aikuaidi.cn/images/gw.png"/><a class="rt-site" href="#" target="_blank">官网地址</a><i>|</i></li>
<li><img src="https://cdn.aikuaidi.cn/images/gwdh.png"/><span id="rt-tel">客服电话 : </span></li>
</ul>
<em class="delete-btn-r" id="del-btn" title="关闭">×</em>
</div>
<div class="result-data">
<p class="basetip result-no" id="result-no">
<img alt="暂无信息" src="https://cdn.aikuaidi.cn/images/nnn.png"/><br/><br/>
<span>爱快递 , 查无此 [ <label class="rt-name">.</label> ] 单 [ <label id="rt-no">.</label> ] 记录<br/>点击进入<a class="rt-site" href="#" target="_blank">官网查询</a>。</span>
</p>
<table border="0" cellpadding="0" cellspacing="0" class="result-info" id="data-table" style="display:table"></table>
</div>
<a class="result-close" href="javascript:layer.closeAll();">关闭结果</a>
</div>
</div></form><div class="input-btn btn -hg" id="input-btn"><span>自动识别</span></div></div>
<div class="input-group home-group" id="item-out">
<form autocomplete="off">
<div class="search-content">
<input class="input -hg" id="q" maxlength="50" placeholder="输入快递网点名称以及乡镇、街道名称、负责人、电话等搜索" type="search"/>
</div>
</form>
<div class="input-btn btn -hg" id="input-btn-q"><span>网点搜索</span></div>
</div>
</div>
<div class="hotsearch">
<div class="title">热门 :</div>
<ul><li><a href="//www.aikuaidi.cn/express/yuantong.html">圆通</a></li>
<li><a href="//www.aikuaidi.cn/express/shentong.html">申通</a></li>
<li><a href="//www.aikuaidi.cn/express/yunda.html">韵达</a></li>
<li><a href="//www.aikuaidi.cn/express/zhongtong.html">中通</a></li>
<li><a href="//www.aikuaidi.cn/express/debang.html">德邦</a></li>
<li><a href="//www.aikuaidi.cn/express/shunfeng.html">顺丰</a></li>
<li class="toprg"><a href="//www.aikuaidi.cn/express/ems.html">EMS</a></li>
<li class="toprg"><a href="//www.aikuaidi.cn/express/">更多 &gt;</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="ikd-outs wd">
<div class="outs-tit"><h2>快递网点展示！</h2><label>热门快递网点直达。</label></div>
<div class="outs-cont">
<div class="containerc">
<a href="//www.aikuaidi.cn/outlets/shunfeng.html"> <strong>顺丰快递</strong> <small>3000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/shentong.html"> <strong>申通快递</strong> <small>15000+<br/>更新于2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/yunda.html"> <strong>韵达快递</strong> <small>20000+<br/>更新于2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/zhongtong.html"> <strong>中通快递</strong> <small>5000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/ems.html"><strong>EMS</strong> <small>3000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/yuantong.html"> <strong>圆通快递</strong> <small>8000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/zjs.html"> <strong>宅急送</strong> <small>3000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/tiantian.html"> <strong>天天快递</strong> <small>3000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/guotong.html"> <strong>国通快递</strong> <small>5000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/suer.html"> <strong>速尔快递</strong> <small>2000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/jingdong.html"> <strong>京东快递</strong> <small>7000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/yousu.html"> <strong>优速快递</strong> <small>8000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/suning.html"> <strong>苏宁快递</strong> <small>3000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/huitong.html"> <strong>百世快递</strong> <small>6000+<br/>更新于：2019-01-20</small> </a>
<a href="//www.aikuaidi.cn/outlets/debang.html"> <strong>德邦物流</strong> <small>12000+<br/>更新于：2019-01-20</small> </a>
</div>
</div>
</div>
<div class="ikd-notes">
<div class="outs-tit">
<h2>功能服务展示</h2><br/>
<label>支持超百家快递物流单号跟踪。</label>
</div>
<div class="wd">
<p>
<img src="https://cdn.aikuaidi.cn/images/fw-kd.png"/><br/>
<strong>300+ 快递物流</strong><br/>支持多种查询，无需任何注册、登录，完全免费</p>
<p><img src="https://cdn.aikuaidi.cn/images/fw-wd.png"/><br/>
<strong>20w+ 快递网点</strong><br/>高频率更新网点信息，为网点信息的实时性提供保障。</p>
<p><img src="https://cdn.aikuaidi.cn/images/fw-sb.png"/><br/>
<strong>快递单号识别</strong><br/>根据快递单号大数据分析单号规则，为您自动识别所属快递</p>
<p><img src="https://cdn.aikuaidi.cn/images/fw-jg.png"/><br/>
<strong>价格查询</strong><br/>整合各快递价格查询服务，为您价格对比提供参考。</p>
</div>
</div>
<div class="ikd-footer">
<p> 友情链接:  <a href="//www.supfree.net/" style="color:#999" target="_blank">在线查询网</a>  <a href="http://tool.cncn.com/" style="color:#999" target="_blank">欣欣百宝箱</a><br/>Copyright © 2019  爱快递旗下网站  版权所有 <a href="http://beian.miit.gov.cn/" target="_blank">沪ICP备12017298号-3</a></p>
</div>
</body>
<script src="https://ssl.captcha.qq.com/TCaptcha.js"></script>
</html>
