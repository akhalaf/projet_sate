<!DOCTYPE html>
<html lang="en">
<head lang="en">
<meta charset="utf-8"/>
<meta content="initial-scale=1.0" name="viewport"/>
<title>4ONO - Sample papers, Result, Cutoff, Answer key, Job, Recruitment, news</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.4ono.com/xmlrpc.php" rel="pingback"/>
<!-- All in One SEO Pack 2.7.3 by Michael Torbert of Semper Fi Web Design[131,220] -->
<meta content="Get the latest Sample papers, Job, Recruitment alerts, Download Answer key, Admit Card, Result, Cutoff. Current Affairs News And Download General Knowledge(GK) PDF Files" name="description"/>
<meta content="Answer key, Result, Cutoff, Jobs, Recruitment, news, Current Affairs, General Knowledge, Government Jobs" name="keywords"/>
<link href="https://www.4ono.com/page/2/" rel="next"/>
<link href="https://www.4ono.com/" rel="canonical"/>
<meta content="4ono.com India's leading educational web portal!" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.4ono.com/" property="og:url"/>
<meta content="https://www.4ono.com/wp-content/uploads/2016/01/screenshot.jpg" property="og:image"/>
<meta content="4ONO" property="og:site_name"/>
<meta content="167581003304812" property="fb:admins"/>
<meta content="Get latest sample, previous and practice papers, Educational News, General Knowledge PDF, Answer Keys, Cutoff Marks, Online Registration, Syllabus, Tips and Tricks" property="og:description"/>
<meta content="summary" name="twitter:card"/>
<meta content="@TeamSqeets" name="twitter:site"/>
<meta content="4ono" name="twitter:domain"/>
<meta content="4ono.com India's leading educational web portal!" name="twitter:title"/>
<meta content="Get latest sample, previous and practice papers, Educational News, General Knowledge PDF, Answer Keys, Cutoff Marks, Online Registration, Syllabus, Tips and Tricks" name="twitter:description"/>
<meta content="https://www.4ono.com/wp-content/uploads/2016/01/screenshot.jpg" name="twitter:image"/>
<meta content="https://www.4ono.com/wp-content/uploads/2016/01/screenshot.jpg" itemprop="image"/>
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Rewire Media",
  "url" : "https://www.4ono.com",
  "sameAs" : ["https://plus.google.com/u/1/111857237386625569744","https://www.facebook.com/rojgar99/","https://twitter.com/TeamSqeets"]
}
</script>
<!-- /all in one seo pack -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.4ono.com/feed/" rel="alternate" title="4ONO » Feed" type="application/rss+xml"/>
<link href="https://www.4ono.com/comments/feed/" rel="alternate" title="4ONO » Comments Feed" type="application/rss+xml"/>
<link href="https://www.4ono.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.4ono.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.8.2" name="generator"/>
<link href="https://www.4ono.com/wp-content/uploads/2017/09/favico.png" rel="icon" type="image/x-icon"/>
<link href="https://www.4ono.com/wp-content/themes/ono/style.css?1.2.10" media="all" rel="stylesheet" type="text/css"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-153068113-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-153068113-1');
</script>
<script async="" src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
<script>
  window.OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "8296ab58-c309-41ae-84f6-df2898e5787f",
    });
  });
</script>
</head>
<body>
<div class="header">
<div>
<div class="box top_ono">
<a class="logo" href="https://www.4ono.com/" title="Jobs, Recruitment, Result, Answer Key, Admit Card, News">
<img alt="Jobs, Recruitment, Result, Answer Key, Admit Card, News" aria-label="4ono Home" height="29" src="https://www.4ono.com/wp-content/themes/ono/static/img/logo.png"/>
</a>
<div class="search">
<form action="https://www.google.co.in/search" method="get" name="_sf" role="search">
<input class="sbox" name="q" placeholder="e.g. past year board papers" spellcheck="false" type="text"/>
<input name="sitesearch" type="hidden" value="www.4ono.com"/>
<span><input type="submit"/></span>
</form>
</div>
<div class="menu_top CLR">
<ul role="menu">
<li class="i_gk" role="menuitem"><a href="https://www.4ono.com/gk/">General Knowledge</a></li>
<li class="i_cf" role="menuitem"><a href="https://www.4ono.com/salary/">Salary</a></li>
<li class="i_qp" role="menuitem"><a href="https://www.4ono.com/sample-papers/">Sample Papers</a></li>
<li class="i_qp" role="menuitem"><a href="https://www.4ono.com/exam-strategies/">Exam Tips</a></li>
</ul>
</div>
<section class="social CLR">
<div role="presentation">
<div>
<span><span>5</span></span>
</div>
</div>
<div role="presentation">
<a href="https://www.4ono.com/store/">BUY Papers</a>
</div>
</section>
</div>
</div>
<div>
<div class="box">
<nav class="menu CLR" role="navigation">
<div class="ono_menu">
</div>
<ul>
<li class="i_ac"><a href="https://www.4ono.com/admit-card/">Admit Card</a></li>
<li class="i_ak"><a href="https://www.4ono.com/category/answer-key/">Answer Key</a></li>
<li class="i_cfm"><a href="https://www.4ono.com/merit-list/">Cutoff/Merit List</a></li>
<li class="i_ru"><a href="https://www.4ono.com/recruitment/">Recruitment</a></li>
<li class="i_re"><a href="https://www.4ono.com/result/">Results</a></li>
<li class="i_sy"><a href="https://www.4ono.com/syllabus/">Syllabus</a></li>
<li class="i_ds"><a href="https://www.4ono.com/date-sheet/">Date Sheet</a></li>
<!--<li class="i_ex"><a href="https://www.4ono.com/choose-exam/">Exams</a></li>-->
</ul>
<a href="https://www.4ono.com/online-test/">Online Test</a>
</nav>
</div>
</div>
</div>
<div class="spacer"></div>
<!--ARTICLE SCHEMA-->
<div class="box article_page">
<div class="o_center other_page">
<div class="home_pg CLR">
<div class="top_ad">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7816056402050814" data-ad-format="auto" data-ad-slot="8276301572" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="kitkat CLR">
<h1>NDA, CBSE 10th &amp; 12th, Indian Army, SSC Sample papers and previous year papers</h1>
<p>4ono brings the best study material, practice papers, sample papers and notifications of major exams like NDA, CBSE 10th, CBSE 12th, Indian Army and SSC. Choose an exam below to start.</p>
<ul>
<li>
<a href="https://www.4ono.com/indian-army-jco-or/">
<img alt="Indian army" src="https://www.4ono.com/wp-content/uploads/2017/09/army.jpg"/>
<h2>INDIAN ARMY</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/nda-national-defence-academy/">
<img alt="NDA" src="https://www.4ono.com/wp-content/uploads/2017/09/nda.jpg"/>
<h2>NDA</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/cbse/">
<img alt="CBSE board" src="https://www.4ono.com/wp-content/uploads/2017/09/CBSE.jpg"/>
<h2>CBSE BOARD</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/isc-icse/">
<img alt="ICSE Board" src="https://www.4ono.com/wp-content/uploads/2017/09/icse.jpg"/>
<h2>ICSE / ISC BOARD</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/up-board/">
<img alt="UP Board" src="https://www.4ono.com/wp-content/uploads/2017/09/up-board.jpg"/>
<h2>UP State BOARD</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/bsf-border-security-forces/">
<img alt="BSF" src="https://www.4ono.com/wp-content/uploads/2017/09/bsf_home_image.jpg"/>
<h2>BSF</h2>
</a>
</li>
<li>
<a href="https://www.4ono.com/ssc-staff-selection-commission/">
<img alt="SSC logo" src="https://www.4ono.com/wp-content/uploads/2019/01/ssc-logo.jpg"/>
<h2>SSC</h2>
</a>
</li>
</ul>
</div>
<aside>
<div>
<h3>Recruitments</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-tour-of-duty/">Indian Army Tour Of Duty – Eligibility Criteria, Salary, Joining Process</a></li>
<li><a href="https://www.4ono.com/indian-army-online-registration/">Join Indian Army Registration 2021 – Indian Army Login Form</a></li>
<li><a href="https://www.4ono.com/ssc-constable-gd-eligibility/">SSC Constable GD Eligibility 2021 – Age, Race, Height, Physical &amp; Medical Qualification</a></li>
<li><a href="https://www.4ono.com/indian-army-female-bharti-recruitment/">Indian Army Female Bharti 2021 – Indian Army Recruitment For Female (Soldier GD)</a></li>
<li><a href="https://www.4ono.com/crpf-recruitment/">CRPF Recruitment 2021 – Apply Online at crpf.gov.in</a></li>
</ul>
<a href="http://www.4ono.com/category/recruitment/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Admit Card's</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-admit-card/">Join Indian Army Admit Card 2021 | Download Admit Card For Bharti Rally</a></li>
<li><a href="https://www.4ono.com/itbp-admit-card/">Download ITBP Admit card 2021 – Head Constable, Tradesman, Telecom</a></li>
<li><a href="https://www.4ono.com/jee-main-admit-20181920/">Download JEE Main Admit Card/Hall Ticket 2021</a></li>
<li><a href="https://www.4ono.com/ssc-cpo-admit-card/">SSC CPO Admit Card 2021 – Tier 1, 2 And 3</a></li>
<li><a href="https://www.4ono.com/bsf-admit-card/">BSF Admit Card 2021 For SI, HC, ASI, Constable, Veterinary</a></li>
</ul>
<a href="http://www.4ono.com/category/admit-card/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Syllabus - Check</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-female-gd-sample-papers/">Indian Army Female GD Sample Papers 2020 – 21 Women Military Police</a></li>
<li><a href="https://www.4ono.com/cbse-class-11-syllabus/">CBSE Class 11 Syllabus 2021 – All Subjects (Science, Arts, &amp; Commerce)</a></li>
<li><a href="https://www.4ono.com/cbse-class-10th-syllabus/">CBSE Class 10 Syllabus 2021 PDF For Maths, Science, English, Social Science (All Subjects Blueprint)</a></li>
<li><a href="https://www.4ono.com/10th-cbse-blueprint-20161718/">CBSE Class 10th Blueprint 2021 – All Subjects Marking Scheme PDF</a></li>
<li><a href="https://www.4ono.com/cbse-class-12th-syllabus/">2021 CBSE Syllabus For Class 12 PDF Download – Physics, Chemistry, Maths (All Subjects)</a></li>
</ul>
<a href="https://www.4ono.com/category/syllabus/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Date Sheet</h3>
<ul>
<li><a href="https://www.4ono.com/cbse-board-exam-date-sheet/">CBSE Board Exam Date Sheet 2021 10th 12th Declared cbse.nic.in</a></li>
</ul>
<a href="https://www.4ono.com/category/date-sheet/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Upcoming Exams</h3>
<ul>
<li><a href="https://www.4ono.com/neet-2019-top-government-and-private-colleges/">NEET  2021 Top Government and Private Colleges</a></li>
<li><a href="https://www.4ono.com/study-plan-for-droppers-to-crack-neet/">Study plan for droppers to crack NEET</a></li>
<li><a href="https://www.4ono.com/top-30-colleges-in-india-after-cracking-neet/">Top 30 Colleges in India after Cracking NEET Exam</a></li>
<li><a href="https://www.4ono.com/up-board-compartment-form-result/">UP Board Compartment Form, Result 2020 For Class 12th and Class 10th</a></li>
<li><a href="https://www.4ono.com/most-important-topics-of-chemistry-for-jee-main-2018-jee-main-2019/">JEE Main Chemistry Exam Most Important Topics</a></li>
</ul>
<a href="https://www.4ono.com/category/exams/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Educational NEWS</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-rank-wise-salary/">Indian Army Rank Wise Salary 2021, 7th CPC Grade Pay And Total Salary Chart</a></li>
<li><a href="https://www.4ono.com/bsf-rank-wise-salary/">BSF Rank Wise Salary, Check BSF Payslip &amp; App – bsf.gov.in</a></li>
<li><a href="https://www.4ono.com/promotions-after-joining-ssc-chsl-work-responsibility-in-chsl/">Promotions after Joining SSC CHSL – Work &amp; Responsibility in CHSL</a></li>
<li><a href="https://www.4ono.com/ssc-cpo-salary/">SSC CPO Salary In Hand For SI/ASI In CAPF, CISF &amp; BSF</a></li>
<li><a href="https://www.4ono.com/ssc-cgl-salary/">SSC CGL In Hand Salary Structure, Pay Scale – Full Chart</a></li>
</ul>
<a href="https://www.4ono.com/category/news/" rel="category tag">SEE ALL</a>
</div>
</aside>
<aside>
<div>
<h3>Answer Key's</h3>
<ul>
<li><a href="https://www.4ono.com/ssc-constable-gd-answer-key-04-oct-2015-with-paper-solution/">SSC Constable GD Answer Key With Paper Solution, Feb-March 2021</a></li>
<li><a href="https://www.4ono.com/ssc-cpo-answer-key/">SSC CPO Answer Key 2021 For Paper I And Paper II</a></li>
<li><a href="https://www.4ono.com/jee-main-answer-key-18192/">JEE Main Answer Key 2021 – Paper 1 &amp; Paper 2</a></li>
<li><a href="https://www.4ono.com/ssc-cgl-answer-key/">Original SSC CGL Answer Key 2021 – Tier I All Shifts</a></li>
<li><a href="https://www.4ono.com/bsf-answer-key/">BSF Answer Key 2021 – SI, ASI, HC, Constable, Steno</a></li>
</ul>
<a href="https://www.4ono.com/category/answer-key/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Results</h3>
<ul>
<li><a href="https://www.4ono.com/up-board-result-12th-10th/">UP Board Result 2020 For 10th &amp; 12th Exam – Roll No, Name, School Wise</a></li>
<li><a href="https://www.4ono.com/indian-army-result/">Join Indian Army Result 2021 Written Exam | GD, Clerk, Technical Result</a></li>
<li><a href="https://www.4ono.com/cbse-board-result-10th-12th/">CBSE Board Result Class 12th And Class 10th 2020 – cbseresults.nic.in</a></li>
<li><a href="https://www.4ono.com/icse-isc-result-10th-12th/">ICSE 10th And ISC 12th Result 2020 – cisce.org</a></li>
<li><a href="https://www.4ono.com/nda-result-merit-list/">NDA Result 2021 And Final Merit List – For NDA I, II</a></li>
</ul>
<a href="https://www.4ono.com/category/results/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>General Knowledge</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-soldier-technical-online-test/">Indian Army Soldier Technical Online Test</a></li>
<li><a href="https://www.4ono.com/ssc-cgl-general-awareness-online-test/">SSC CGL General Awareness online test – For Tier I</a></li>
<li><a href="https://www.4ono.com/ssc-cgl-english-online-test/">Practice SSC CGL English – Online Test</a></li>
<li><a href="https://www.4ono.com/soldier-clerk-english-online-test/">Soldier Clerk/SKT English Online Test</a></li>
<li><a href="https://www.4ono.com/computer-science-online-test/">Army Computer Science Online Test – Level 2</a></li>
</ul>
<a href="http://www.4ono.com/category/g-k/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Current Affairs</h3>
<ul>
<li><a href="https://www.4ono.com/cbse-sample-papers/">CBSE Sample Papers</a></li>
<li><a href="https://www.4ono.com/icse-isc-sample-papers/">ICSE/ISC Sample Papers</a></li>
<li><a href="https://www.4ono.com/current-affairs-gk-pdf-2015-2016/">Download Current Affairs GK PDF 2015 Wise</a></li>
</ul>
<a href="https://www.4ono.com/category/current-affairs/" rel="category tag">SEE ALL</a>
</div>
<div>
<h3>Sample Papers</h3>
<ul>
<li><a href="https://www.4ono.com/indian-army-female-gd-sample-papers/">Indian Army Female GD Sample Papers 2020 – 21 Women Military Police</a></li>
<li><a href="https://www.4ono.com/ssc-gd-constable-sample-papers/">Download SSC GD Constable Sample Papers 2021 PDF</a></li>
<li><a href="https://www.4ono.com/cbse-class-12th-guess-papers/">CBSE Class 12th Guess Papers 2020 Important Questions</a></li>
<li><a href="https://www.4ono.com/up-10th-sample-papers-pdf-2016171819/">Up Board Class 10th Sample Papers PDF 2021 – All Subjects</a></li>
<li><a href="https://www.4ono.com/indian-army-written-exam-important-questions-previous-year-papers/">Indian Army Written Exam Important Questions Answers &amp; Sample Papers</a></li>
</ul>
<a href="https://www.4ono.com/category/previous-years-sample-model-papers/" rel="category tag">SEE ALL</a>
</div>
<div>
<h5 class="widget-title">Indian Jobs</h5> <ul>
<li>
<a href="https://www.4ono.com/up-scholarship-status/">Check UP Scholarship Status 2020-2021 Online</a>
</li>
<li>
<a href="https://www.4ono.com/indian-army-female-gd-sample-papers/">Indian Army Female GD Sample Papers 2020 – 21 Women Military Police</a>
</li>
<li>
<a href="https://www.4ono.com/indian-army-tour-of-duty/">Indian Army Tour Of Duty – Eligibility Criteria, Salary, Joining Process</a>
</li>
<li>
<a href="https://www.4ono.com/ssc-gd-constable-sample-papers/">Download SSC GD Constable Sample Papers 2021 PDF</a>
</li>
<li>
<a href="https://www.4ono.com/cbse-class-11-syllabus/">CBSE Class 11 Syllabus 2021 – All Subjects (Science, Arts, &amp; Commerce)</a>
</li>
<li>
<a href="https://www.4ono.com/up-board-result-12th-10th/">UP Board Result 2020 For 10th &amp; 12th Exam – Roll No, Name, School Wise</a>
</li>
<li>
<a href="https://www.4ono.com/score-more-than-90-percent-marks-board-exams-2016-17/">11 Strategies To Score More Than 90% Marks In 12th 10th Board Exam 2020-21</a>
</li>
<li>
<a href="https://www.4ono.com/indian-army-result/">Join Indian Army Result 2021 Written Exam | GD, Clerk, Technical Result</a>
</li>
<li>
<a href="https://www.4ono.com/cbse-improvement-re-evaluation/">CBSE Re-Evaluation Process 2021, Improvement Form, &amp; Verification Of Class 10th &amp;12th Board Exam</a>
</li>
<li>
<a href="https://www.4ono.com/indian-army-online-registration/">Join Indian Army Registration 2021 – Indian Army Login Form</a>
</li>
<li>
<a href="https://www.4ono.com/ssc-constable-gd-eligibility/">SSC Constable GD Eligibility 2021 – Age, Race, Height, Physical &amp; Medical Qualification</a>
</li>
<li>
<a href="https://www.4ono.com/cbse-board-result-10th-12th/">CBSE Board Result Class 12th And Class 10th 2020 – cbseresults.nic.in</a>
</li>
</ul>
</div>
</aside>
</div>
<div class="ono_share CLR">
<div><fb:like href="https://www.4ono.com/" layout="button_count" show_faces="false" width="200"></fb:like></div>
<div><g:plusone href="http://www.4ono.com/" size="medium"></g:plusone></div>
</div>
</div>
<aside class="o_right" role="complementary">
<section class="top_post">
<h5 class="imr">4ono Android App</h5><div class="textwidget custom-html-widget"><div><a href="https://play.google.com/store/apps/details?id=com.team4ono.a4ono"><img src="https://drive.google.com/uc?id=1xYuC5-YgBIepjaLGejpdORanrc7bpLVQ" style="width:100%"/></a></div></div><h5 class="imr">Recent Articles</h5><div class="textwidget custom-html-widget"><ul style="margin-bottom:15px">
<li><a href="https://www.4ono.com/cbse-12th-science-solved-previous-year-papers/">Buy CBSE 12th (Physics, Chemistry, Maths, Biology, English) solved last 10 year papers</a></li>
<li><a href="https://www.4ono.com/indian-army-admit-card/">Indian Army Admit Card 2020</a></li>
<li><a href="https://www.4ono.com/indian-army-result/">Join Indian Army Result</a></li>
<li><a href="https://www.4ono.com/indian-army-soldier-gd-sample-papers-pdf/">Indian Army Sample Papers/Model Papers</a></li>
<li><a href="https://www.4ono.com/indian-army-merit-list/">Indian Army Merit List</a></li>
<li><a href="https://www.4ono.com/up-board-result-12th-10th/">UP Board Exam Result 2020</a></li>
<li><a href="https://www.4ono.com/cbse-board-result-10th-12th/">CBSE Board Exam Result 2020</a></li>
<li><a href="https://www.4ono.com/icse-isc-result-10th-12th/">ISC 12th ICSE 10th Board Exam Result 2020</a></li>
</ul></div><div class="textwidget custom-html-widget"><!-- thumb -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7816056402050814" data-ad-slot="8131111285" style="display:inline-block;width:336px;height:280px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div> </section>
<div class="ono_float">
<section class="downloads">
<h5>DOWNLOAD ZONE</h5>
<div class="CLR">
<a href="https://www.4ono.com/indian-army-gd/">Army GD</a>
<a href="https://www.4ono.com/indian-army-clerk-skt/">Army Clerk</a>
<a href="https://www.4ono.com/indian-army-technical/">Army Technical</a>
<a href="https://www.4ono.com/indian-army-nursing-assistant/">Nursing Assistant</a>
<a href="https://www.4ono.com/indian-army-tradesman/">Army Tradesman</a>
<a href="https://www.4ono.com/online-test/">Online test</a>
</div>
<h5 class="ck_now">CHECK NOW</h5>
<div class="CLR ck_i">
<a href="https://www.4ono.com/cbse/">CBSE</a>
<a href="https://www.4ono.com/isc-icse/">ISC/ICSE</a>
<a href="https://www.4ono.com/up-board/">UP Board</a>
<a href="https://www.4ono.com/nda-national-defence-academy/">NDA</a>
</div>
</section>
<!-- thumb -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7816056402050814" data-ad-slot="8131111285" style="display:inline-block;width:336px;height:280px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</aside></div>
<div class="footer">
<div>
<div class="box CLR ft_menu">
<div class="o_about" role="note">
<span>Stay tuned with everything</span>
<p>What are you preparing for? 4ONO helps you to Find and Get latest educational NEWS.</p>
<div>Contact us: <a href="mailto:team4ono@gmail.com">team4ono@gmail.com</a></div>
<div class="o_like_me CLR">
<a class="i_fb" href="https://www.facebook.com/4onostore/" target="_blank"></a>
<a class="i_tw" href="https://twitter.com/4onoCom" target="_blank"> </a>
</div>
</div>
<div class="ev_articles CLR">
<h5>#Evergreen Jobs Area</h5>
<ul role="listbox">
<li><a href="https://www.4ono.com/buy-army-gd-books/">Buy Army Books</a></li>
<li><a href="https://www.4ono.com/army-gd-solved-papers/">Buy Army Papers</a></li>
<li><a href="https://www.youtube.com/watch?v=FYaA7RpG8A0">How to Buy</a></li>
<li><a href="https://www.4ono.com/indian-army-gd/">Army GD</a></li>
<li><a href="https://www.4ono.com/indian-army-clerk-skt/">Army clerk</a></li>
<li><a href="https://www.4ono.com/indian-army-technical/">Army technical</a></li>
<li><a href="https://www.4ono.com/indian-army-nursing-assistant/">Nursing assistant</a></li>
<li><a href="https://www.4ono.com/indian-army-tradesman/">Tradesman</a></li>
</ul>
</div>
<div class="thought">
<label>Today's Thought</label>
<div class="textwidget"><p>Education is the most powerful weapon which you can use to change the world.</p>
</div>
<a href="https://www.4ono.com/inspirational-motivational-educational-quotes-20171819/" title="motivational thoughts">Read more thoughts</a>
</div>
</div>
</div>
<div>
<div class="box CLR foot_base">
<div><a href="https://www.4ono.com/about/">About</a><a href="/about/#help">Help</a><a href="/about/#contact_us">Contact</a><a href="/about/#policies">Terms and Privacy</a><a href="/about/#ad_policies">Advertise</a></div>
<div><span role="note">4ONO.com© - All Rights Reserved |</span><a href="https://www.4ono.com/sitemap.xml">Sitemap</a></div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script async="" src="https://www.4ono.com/wp-content/themes/ono/main.js?ver=1.0" type="text/javascript"></script>
<script async="" src="https://apis.google.com/js/plusone.js" type="text/javascript"></script>
<script async="" src="https://connect.facebook.net/en_US/all.js#xfbml=1"></script>
<script>
var nt = '<ul role="listbox"><li><p>Latest Jobs/Recruitments</p></li>'+
                                                             '<li><a href="https://www.4ono.com/indian-army-tour-of-duty/">Indian Army Tour Of Duty &#8211; Eligibility Criteria, Salary, Joining Process</a></li>' +
                                                             '<li><a href="https://www.4ono.com/indian-army-online-registration/">Join Indian Army Registration 2021 &#8211; Indian Army Login Form</a></li>' +
                                                             '<li><a href="https://www.4ono.com/ssc-constable-gd-eligibility/">SSC Constable GD Eligibility 2021 &#8211; Age, Race, Height, Physical &#038; Medical Qualification</a></li>' +
                                                             '<li><a href="https://www.4ono.com/indian-army-female-bharti-recruitment/">Indian Army Female Bharti 2021 &#8211; Indian Army Recruitment For Female (Soldier GD)</a></li>' +
                                                             '<li><a href="https://www.4ono.com/crpf-recruitment/">CRPF Recruitment 2021 – Apply Online at crpf.gov.in</a></li>' +
                             '<li><a href="https://www.4ono.com/category/recruitment/">SEE MORE</a></li></ul>';
</script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7816056402050814",
          enable_page_level_ads: true
     });
</script>
</body>
</html>
<!-- Dynamic page generated in 0.074 seconds. -->
