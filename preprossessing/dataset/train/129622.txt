<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:gr="http://gree.jp/ns" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:og="http://ogp.me/ns#">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta content="uYsfLqrfC9C2f7BP5JdHBREq1yOH8sv-FgvKeuEEKz0" name="google-site-verification"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="noodp,noydir" name="robots"/>
<title>フリーソフトラボ.com</title>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<link href="https://freesoftlab.com/wp-content/themes/wp4/style.css" media="screen,tv,print" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!--[if lte IE 8 ]>
<link rel="stylesheet" href="https://freesoftlab.com/wp-content/themes/wp4/ie.css" type="text/css" />
<![endif]-->
<link href="https://freesoftlab.com/feed/" rel="alternate" title="フリーソフトラボ.com RSS Feed" type="application/rss+xml"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/>
<link href="http://www.hatena.ne.jp/freesoftlab/" rel="author"/>
<link href="https://freesoftlab.com/" rel="canonical"/>
<!--[if IE 6]>
<script type="text/javascript" src="https://freesoftlab.com/wp-content/themes/wp4/js/DD_belatedPNG.js"></script>
<script type="text/javascript" src="https://freesoftlab.com/wp-content/themes/wp4/js/png.js"></script>
<![endif]-->
<meta content="このサイトはWindows用フリーソフトを多数紹介しているフリーソフト・無料ソフトの総合情報ポータルサイトです。ソフトウェアの概要から機能・使い方まで詳しく紹介しています。各ソフトウェアはカテゴリー別に細かく分類されていますので、トップページからリンクをたどっていけば目的のソフトがすぐに見つかるようになっています。" name="description"/>
<meta content="554494414930097" property="fb:app_id"/>
<meta content="ja_JP" property="og:locale"/>
<meta content="フリーソフトラボ.com" property="og:site_name"/>
<meta content="https://freesoftlab.com/" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="フリーソフトラボ.com" property="og:title"/>
<meta content="このサイトはWindows用フリーソフトを多数紹介しているフリーソフト・無料ソフトの総合情報ポータルサイトです。ソフトウェアの概要から機能・使い方まで詳しく紹介しています。各ソフトウェアはカテゴリー別に細かく分類されていますので、トップページからリンクをたどっていけば目的のソフトがすぐに見つかるようになっています。" property="og:description"/>
<meta content="https://freesoftlab.com/wp-content/themes/wp4/images/common/ogp-logo.png" property="og:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="@freesoftlab" name="twitter:site"/>
<link href="https://freesoftlab.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css" type="text/css">
.has-text-align-justify{text-align:justify;}
</style>
<link href="https://freesoftlab.com/wp-json/" rel="https://api.w.org/"/><style type="text/css">img#wpstats{display:none}</style><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40022302-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body class="home blog">
<!--[if gt IE 6]><!--><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;js.async = true;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script><!--<![endif]-->
<!--[if lte IE 8 ]>
<script>(function(w,d){w.___gcfg={lang:"ja"};var s,e=d.getElementsByTagName("script")[0],a=function(u,i){if(!d.getElementById(i)){s=d.createElement("script");s.src=u;if(i){s.id=i}e.parentNode.insertBefore(s,e)}};a("//b.st-hatena.com/js/bookmark_button_wo_al.js")})(this,document);</script>
<![endif]-->
<!-- container Start -->
<div id="container">
<!-- /container Start -->
<!--main-->
<div id="main">
<!--main-in-->
<div id="main-in">
<!--▽メインコンテンツ-->
<div id="main-contents">
<!--▼パン屑ナビ-->
<!--▲パン屑ナビ-->
<!-- google_ad_section_start -->
<div class="heading-line" style="margin-bottom:10px;"><div class="heading-main"><h2><span style="">Windows用ソフトウェア</span></h2></div></div>
<table class="contents-menu" style="margin-bottom:40px;">
<tr>
<td class="lefted">
<table class="defaulted">
<tr>
<td class="contents-menu1" valign="top">
<div class="parent-category utility">
<img alt="ユーティリティソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Utility.png" width="64"/>　
<a href="https://freesoftlab.com/windows/utility/">ユーティリティソフト</a>
</div>
<a href="https://freesoftlab.com/windows/utility/recovery/">ファイル復元</a> <a href="https://freesoftlab.com/windows/utility/delete/">ファイル完全削除・データ消去</a> <a href="https://freesoftlab.com/windows/utility/force-delete/">強制削除</a> <a href="https://freesoftlab.com/windows/utility/recyclebin/">ごみ箱拡張</a> <a href="https://freesoftlab.com/windows/utility/rename/">リネーム</a> <a href="https://freesoftlab.com/windows/utility/mouse/">マウス拡張</a> <a href="https://freesoftlab.com/windows/utility/keyboard/">キーボード拡張</a> <a href="https://freesoftlab.com/windows/utility/launcher/">ランチャー</a> <a href="https://freesoftlab.com/windows/utility/display-off/">モニター電源オフ</a> <a href="https://freesoftlab.com/windows/utility/brightness/">ディスプレイ輝度調整</a> <a href="https://freesoftlab.com/windows/utility/no-sleep/">スリープ防止</a>
</td>
<td class="contents-menu2" valign="top">
<div class="parent-category security">
<img alt="セキュリティソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Security.png" width="64"/>　
<a href="https://freesoftlab.com/windows/security/">セキュリティソフト</a>
</div>
<a href="">総合セキュリティソフト</a> <a href="https://freesoftlab.com/windows/security/antivirus/">ウイルス対策</a> <a href="">スパイウェア対策</a> <a href="">ファイアウォール</a> <a href="">オンラインスキャン</a> <a href="">脆弱性対策</a> <a href="https://freesoftlab.com/windows/security/encryption/">暗号化</a> <a href="">個人情報保護</a> <a href="">パスワード管理</a> <a href="https://freesoftlab.com/windows/security/password-generator/">パスワード生成</a>
</td>
</tr>
<tr>
<td class="contents-menu1" valign="top">
<div class="parent-category websoft">
<img alt="ウェブ関連ソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Web.png" width="64"/>
　<a href="https://freesoftlab.com/windows/web/">ウェブ関連ソフト</a>
</div>
<a href="https://freesoftlab.com/windows/web/web-browser/">ウェブブラウザ</a> <a href="">ダウンローダー・ダウンロード支援ソフト</a> <a href="">メール</a> <a href="">オンラインストレージ</a> <a href="">ファイル転送ソフト</a>
</td>
<td class="contents-menu2" valign="top">
<div class="parent-category systemsoft">
<img alt="Windowsシステム関連ソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/System.png" width="64"/>　
<a href="https://freesoftlab.com/windows/system/">Windowsシステム関連ソフト</a>
</div>
<a href="https://freesoftlab.com/windows/system/speed-up/">Windows高速化</a> <a href="https://freesoftlab.com/windows/system/formatter/">フォーマット</a> <a href="https://freesoftlab.com/windows/system/cleaner/">システムクリーナー</a> <a href="https://freesoftlab.com/windows/system/memory/">メモリ解放</a> <a href="">不要ファイル削除</a> <a href="https://freesoftlab.com/windows/system/uninstall/">アンインストーラー</a> <a href="https://freesoftlab.com/windows/system/product-key/">プロダクトキー解析</a> <a href="https://freesoftlab.com/windows/system/shutdown/">PC自動終了</a>
</td>
</tr>
<tr>
<td class="contents-menu1" valign="top">
<div class="parent-category mediasoft">
<img alt="音楽 ・動画ソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Music.png" width="64"/>　
<a href="https://freesoftlab.com/windows/media/">音楽 ・動画ソフト</a>
</div>
<a href="https://freesoftlab.com/windows/media/media-player/">メディアプレイヤー</a> <a href="">音楽プレイヤー</a> <a href="">CD/DVD/Blu-rayライティング</a> <a href="">リッピング</a> <a href="https://freesoftlab.com/windows/media/video-converter/">動画変換</a> <a href="https://freesoftlab.com/windows/media/audio-converter/">音楽変換</a> <a href="">録音ソフト</a>
</td>
<td class="contents-menu2" valign="top">
<div class="parent-category documentsoft">
<img alt="画像・文書ソフト" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Document.png" width="64"/>　
<a href="https://freesoftlab.com/windows/document/">画像・文書ソフト</a>
</div>
<a href="https://freesoftlab.com/windows/document/text-editor/">テキストエディタ</a> <a href="">PDFビューア</a> <a href="">オフィス総合</a> <a href="">画像編集・フォトレタッチ</a> <a href="">画像変換</a> <a href="">スクリーンキャプチャ</a> <a href="https://freesoftlab.com/windows/document/icon/">アイコン作成</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="heading-line" style="margin-bottom:10px;"><div class="heading-main"><h2>ウェブブラウザ拡張機能</h2></div></div>
<table class="contents-menu">
<tr>
<td class="lefted">
<table class="defaulted">
<tr>
<td class="contents-menu1" valign="top">
<div class="parent-category firefox">
<img alt="Firefoxアドオン" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Firefox.png" width="64"/>　
<a href="https://freesoftlab.com/browser-extension/firefox/">Firefoxアドオン</a>
</div>
<a href="https://freesoftlab.com/browser-extension/firefox/firefox-mouse-gesture/">マウスジェスチャー</a> <a href="https://freesoftlab.com/browser-extension/firefox/firefox-speed-up/">Firefox高速化</a> <a href="">テーマ・スキン</a> <a href="https://freesoftlab.com/browser-extension/firefox/firefox-downloader/">ダウンローダー</a> <a href="">仕事効率化</a> <a href="">インターフェース改善</a> <a href="">ブックマーク管理</a> <a href="https://freesoftlab.com/browser-extension/firefox/firefox-security/">セキュリティ</a>
</td>
<td class="contents-menu2" valign="top">
<div class="parent-category chrome">
<img alt="Google Chrome拡張機能" border="0" class="takasasoroe png_bg" height="64" src="https://freesoftlab.com/wp-content/themes/wp4/images/Chrome.png" width="64"/>　
<a href="https://freesoftlab.com/browser-extension/chrome/">Google Chrome拡張機能</a>
</div>
<a href="https://freesoftlab.com/browser-extension/chrome/chrome-mouse-gesture/">マウスジェスチャー</a> <a href="https://freesoftlab.com/browser-extension/chrome/chrome-speed-up/">Chrome高速化</a> <a href="https://freesoftlab.com/browser-extension/chrome/chrome-downloader/">ダウンローダー</a> <a href="https://freesoftlab.com/browser-extension/chrome/chrome-context-menu/">右クリック拡張</a> <a href="">仕事効率化</a> <a href="">インターフェース改善</a> <a href="https://freesoftlab.com/browser-extension/chrome/chrome-security/">セキュリティ</a> <a href="https://freesoftlab.com/browser-extension/chrome/chrome-web-development/">ウェブ制作</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/新着ブログエリア-->
<div class="index-new-blog" style="margin-top:100px;">
<div class="index-new-blog-title">ブログ - 新着の記事 <a class="index-open-archives-button" href="https://freesoftlab.com/blog/">ブログ記事の一覧をみる</a></div>
<div class="index-new-blog-area" style="height:320px;overflow:auto;">
<div class="index-new-blog-contents">
<div class="index-new-blog-contents-image-block">
<div class="index-new-blog-contents-image-margin">
<div class="index-new-blog-image-frame">
<img alt="JVCケンウッドの木製素材の高級イヤホン「HA-FX1100」レビュー！" class="index-new-blog-image wp-post-image" height="150" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/ha-fx1100-image-150x150.jpg" width="150"/></div>
</div>
</div>
<div class="index-new-blog-contents-text-block">
<div class="index-new-blog-contents-text-margin">
<a href="https://freesoftlab.com/blog/ha-fx1100/">JVCケンウッドの木製素材の高級イヤホン「HA-FX1100」レビュー！</a>
<div style="margin-top:5px;">
<span style="display:inline-block;background:#428DE5;border-radius:4px;color:#fff;font-size:12px;padding:1px 7px;">買ったものレビュー (イヤホン・ヘッドホン)</span>
</div>
<div style="font-size:13px;margin:10px 0 5px 0;">今回レビューするのはJVCケンウッド高級カナル型イヤホン「HA-FX1100」です。発売当初の価格は5万5000円ほどで、かつては同社のWO...</div>
<div style="margin-top:5px;">
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-clock-o" style="margin-right:5px;"></i>約4分で読めます</span>
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-calendar" style="margin-right:5px;"></i>投稿 2020/01/04</span>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="index-new-blog-contents">
<div class="index-new-blog-contents-image-block">
<div class="index-new-blog-contents-image-margin">
<div class="index-new-blog-image-frame">
<img alt="澪ホンで有名なAKGのオープンエアー型ヘッドホン「K701」レビュー！" class="index-new-blog-image wp-post-image" height="150" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2019/12/k701-image-150x150.jpg" width="150"/></div>
</div>
</div>
<div class="index-new-blog-contents-text-block">
<div class="index-new-blog-contents-text-margin">
<a href="https://freesoftlab.com/blog/k701/">澪ホンで有名なAKGのオープンエアー型ヘッドホン「K701」レビュー！</a>
<div style="margin-top:5px;">
<span style="display:inline-block;background:#428DE5;border-radius:4px;color:#fff;font-size:12px;padding:1px 7px;">買ったものレビュー (イヤホン・ヘッドホン)</span>
</div>
<div style="font-size:13px;margin:10px 0 5px 0;">今回レビューするのは、オーストリアの音響メーカーAKGの、かつてフラグシップだったオープンエアー型ヘッドホン「K701」です。2005年の発...</div>
<div style="margin-top:5px;">
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-clock-o" style="margin-right:5px;"></i>約7分で読めます</span>
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-calendar" style="margin-right:5px;"></i>投稿 2019/12/30</span>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="index-new-blog-contents">
<div class="index-new-blog-contents-image-block">
<div class="index-new-blog-contents-image-margin">
<div class="index-new-blog-image-frame">
<img alt="レトロな伝説のゲーム機ファミコンカラーの「ゲームボーイミクロ」レビュー！" class="index-new-blog-image wp-post-image" height="150" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2019/12/gameboy-micro-image-150x150.jpg" width="150"/></div>
</div>
</div>
<div class="index-new-blog-contents-text-block">
<div class="index-new-blog-contents-text-margin">
<a href="https://freesoftlab.com/blog/gameboy-micro/">レトロな伝説のゲーム機ファミコンカラーの「ゲームボーイミクロ」レビュー！</a>
<div style="margin-top:5px;">
<span style="display:inline-block;background:#428DE5;border-radius:4px;color:#fff;font-size:12px;padding:1px 7px;">買ったものレビュー (ガジェット)</span>
</div>
<div style="font-size:13px;margin:10px 0 5px 0;">今回は、今となっては伝説的なゲーム機になった「ゲームボーイミクロ」を買ってみました。発売は2005年なので今から15年ほど前。一昔前のゲーム...</div>
<div style="margin-top:5px;">
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-clock-o" style="margin-right:5px;"></i>約3分で読めます</span>
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-calendar" style="margin-right:5px;"></i>投稿 2019/12/30</span>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="index-new-blog-contents">
<div class="index-new-blog-contents-image-block">
<div class="index-new-blog-contents-image-margin">
<div class="index-new-blog-image-frame">
<img alt="Switchでも使えるBUFFALOの有線LANアダプター「LUA4-U3-AGTE-BK」レビュー！" class="index-new-blog-image wp-post-image" height="150" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2019/12/lua4-u3-agte-bk-image-150x150.jpg" width="150"/></div>
</div>
</div>
<div class="index-new-blog-contents-text-block">
<div class="index-new-blog-contents-text-margin">
<a href="https://freesoftlab.com/blog/lua4-u3-agte-bk/">Switchでも使えるBUFFALOの有線LANアダプター「LUA4-U3-AGTE-BK」レビュー！</a>
<div style="margin-top:5px;">
<span style="display:inline-block;background:#428DE5;border-radius:4px;color:#fff;font-size:12px;padding:1px 7px;">買ったものレビュー (ハードウェア・PC周辺機器)</span>
</div>
<div style="font-size:13px;margin:10px 0 5px 0;">今回は、BUFFALOのUSB有線LANアダプター「LUA4-U3-AGTE-BK」を買ってみました。もともとNintendo Switch...</div>
<div style="margin-top:5px;">
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-clock-o" style="margin-right:5px;"></i>約2分で読めます</span>
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-calendar" style="margin-right:5px;"></i>投稿 2019/12/29</span>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
<div class="index-new-blog-contents">
<div class="index-new-blog-contents-image-block">
<div class="index-new-blog-contents-image-margin">
<div class="index-new-blog-image-frame">
<img alt="前世代から大幅にスペックアップした第9世代「Fire HD 10」レビュー！" class="index-new-blog-image wp-post-image" height="150" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2019/12/fire-hd-10-2019-image-150x150.jpg" width="150"/></div>
</div>
</div>
<div class="index-new-blog-contents-text-block">
<div class="index-new-blog-contents-text-margin">
<a href="https://freesoftlab.com/blog/fire-hd-10-2019/">前世代から大幅にスペックアップした第9世代「Fire HD 10」レビュー！</a>
<div style="margin-top:5px;">
<span style="display:inline-block;background:#428DE5;border-radius:4px;color:#fff;font-size:12px;padding:1px 7px;">買ったものレビュー (ガジェット)</span>
</div>
<div style="font-size:13px;margin:10px 0 5px 0;">今回は、2019年に発売されたAmazonのタブレット「Fire HD 10」（第9世代）のレビューです。Amazonのタブレットの中では一...</div>
<div style="margin-top:5px;">
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-clock-o" style="margin-right:5px;"></i>約3分で読めます</span>
<span style="display:inline-block;margin-left:10px;font-size:13px;padding:2px 2px;"><i aria-hidden="true" class="fa fa-calendar" style="margin-right:5px;"></i>投稿 2019/12/29</span>
</div>
</div>
</div>
<div style="clear:both;"></div>
</div>
</div>
</div>
<!--/新着ブログエリア-->
<h3 class="update-title"><i aria-hidden="true" class="fa fa-minus" style="font-size:22px;margin-right:7px;vertical-align:-10%;"></i>更新履歴<span>Updates</span></h3>
<div id="update-table">
<table>
<tr>
<td class="update-date" rowspan="2" valign="top">お知らせ</td>
<td>更新について</td>
</tr>
<tr>
<td class="update-content">現在、サイトシステム構築・修正や過去記事の見直しに時間を割いているためソフトウェア新規掲載は休止中です。</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2019/10/15</td>
<td>システム更新 ( Ver. 0.210 )</td>
</tr>
<tr>
<td class="update-content">サイドバーのデザインを変更しました。</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/16</td>
<td><a href="https://freesoftlab.com/detail/easeus-data-recovery-wizard-free/">[ソフト掲載] EaseUS Data Recovery Wizard Free</a></td>
</tr>
<tr>
<td class="update-content">「EaseUS Data Recovery Wizard Free」は、ゴミ箱やHDDから誤って削除したファイルを復元できるソフトです。検出...</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/16</td>
<td><a href="https://freesoftlab.com/detail/easeus-photo-recovery/">[ソフト掲載] EaseUS Photo Recovery</a></td>
</tr>
<tr>
<td class="update-content">「EaseUS Photo Recovery」は、SDカードから削除したデジカメの写真を復元できるソフトです。JPG、BMP、GIF、PNG...</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/07</td>
<td><a href="https://freesoftlab.com/detail/data-fukkyu-r-for-ntfs/">[ソフト掲載] データ復旧R for NTFS</a></td>
</tr>
<tr>
<td class="update-content">ゴミ箱から削除してたファイルやフォーマットで読み取れなくなったファイルを復元するソフトです。スキャンに時間がかかりますが復元性能が高いのが特...</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/07</td>
<td><a href="https://freesoftlab.com/detail/datarecovery/">[ソフト掲載] DataRecovery</a></td>
</tr>
<tr>
<td class="update-content">ゴミ箱やUSBメモリから削除してしまったファイルを復元できる超シンプルなインターフェースが特徴のファイル復元ソフトです。ファイル名での絞り込...</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/06</td>
<td><a href="https://freesoftlab.com/detail/kickass-undelete/">[ソフト掲載] Kickass Undelete</a></td>
</tr>
<tr>
<td class="update-content">ゴミ箱やUSBメモリから削除したファイルを復元できるフリーソフトです。大切なファイルを間違って消してしまったとき、削除した直後なら、本ソフト...</td>
</tr>
<tr>
<td class="update-date" rowspan="2" valign="top">2020/02/06</td>
<td><a href="https://freesoftlab.com/detail/finalseeker-card-data-recovery/">[ソフト掲載] FinalSeeker Card Data Recovery</a></td>
</tr>
<tr>
<td class="update-content">SDカードやUSBメモリから削除したファイルを復元できるソフトです。誤って削除したSDカード内のデジカメの写真を復元したいときなどに便利です...</td>
</tr>
</table>
</div>
<!-- google_ad_section_end -->
<div style="margin-top:60px;">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- ソフトラボ記事外下部 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2444845910812172" data-ad-format="auto" data-ad-slot="4353868298" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
<!--▲メインコンテンツ-->
<!--▽:sidebar.phpを編集、またはWP管理画面のウィジェットで編集-->
<div id="sidebar">
<!-- google_ad_section_start(weight=ignore) -->
<div id="sidebar-in">
<!--広告エリア-->
<p><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 300x250, 作成済み 09/03/08 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2444845910812172" data-ad-slot="3642288254" style="display:inline-block;width:300px;height:250px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></p>
<!--広告エリア-->
<!--広告エリア-->
<div style="margin-bottom:10px;">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- ソフトラボ右カラムリンク -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2444845910812172" data-ad-format="link" data-ad-slot="6754698626" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<!--広告エリア-->
<!--広告エリア-->
<!--広告エリア-->
<!--新着ソフトエリア-->
<div class="new-soft">
<div class="new-title">新着ソフト <a class="open-archives-button" href="https://freesoftlab.com/archives/">一覧をみる</a></div>
<div class="new-area">
<div class="new-contents-2"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/sleep-preventer-icon-32x32.png" width="32"/><a href="https://freesoftlab.com/detail/sleep-preventer/" title="パソコンがスリープ(休止状態)になるのを防止するソフト「Sleep Preventer」">Sleep Preventer</a>
</div>
<div class="new-soft-text">パソコンがスリープ(休止状態)になるのを防止するソフト</div>
</div>
<div class="new-contents"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/no-sleep-icon-32x32.png" width="32"/><a href="https://freesoftlab.com/detail/no-sleep/" title="PCがスリープ状態になるのを防止するソフト「No Sleep」">No Sleep</a>
</div>
<div class="new-soft-text">PCがスリープ状態になるのを防止するソフト</div>
</div>
<div class="new-contents-2"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/caffeine-icon-32x32.png" width="32"/><a href="https://freesoftlab.com/detail/caffeine/" title="省電力モードやスクリーンセーバーを一時的に無効にするソフト「Caffeine」">Caffeine</a>
</div>
<div class="new-soft-text">省電力モードやスクリーンセーバーを一時的に無効にするソフト</div>
</div>
<div class="new-contents"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/pc-sleeper-icon-150x150.png" width="32"/><a href="https://freesoftlab.com/detail/pc-sleeper/" title="指定した時刻/指定した時間経過でPCのシャットダウン等を行うソフト「PC Sleeper」">PC Sleeper</a>
</div>
<div class="new-soft-text">指定した時刻/指定した時間経過でPCのシャットダウン等を行うソフト</div>
</div>
<div class="new-contents-2"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/shutdown-express-icon-32x32.png" width="32"/><a href="https://freesoftlab.com/detail/shutdown-express/" title="タスクトレイから素早くシャットダウン/再起動/スリープなどを行うソフト「Shutdown Express」">Shutdown Express</a>
</div>
<div class="new-soft-text">タスクトレイから素早くシャットダウン/再起動/スリープなどを行うソフト</div>
</div>
<div class="new-contents"><div class="new-soft-icon">
<img alt="" class="iconsoroe png_bg wp-post-image" height="32" loading="lazy" src="https://freesoftlab.com/wp-content/uploads/2020/01/wise-auto-shutdown-icon-32x32.png" width="32"/><a href="https://freesoftlab.com/detail/wise-auto-shutdown/" title="指定した時間にシャットダウンやスリープができるソフト「Wise Auto Shutdown」">Wise Auto Shutdown</a>
</div>
<div class="new-soft-text">指定した時間にシャットダウンやスリープができるソフト</div>
</div>
</div>
</div>
<!--/新着ソフトエリア-->
<div id="sidefooter">
<!--[if gt IE 6]><!-->
<div class="sidebookmark cf">
<div><a class="twitter-share-button" data-count="vertical" data-text="フリーソフトラボ.com" data-url="https://freesoftlab.com/" data-via="freesoftlab" href="https://twitter.com/share">tweet</a><script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>
<div><div class="fb-like" data-href="https://freesoftlab.com/" data-layout="box_count" data-send="false" data-show-faces="false" data-width="70"></div></div>
<div><a class="hatena-bookmark-button" data-hatena-bookmark-layout="vertical" data-hatena-bookmark-title="フリーソフトラボ.com" href="http://b.hatena.ne.jp/entry/https://freesoftlab.com/" title="このエントリーをはてなブックマークに追加"><img alt="このエントリーをはてなブックマークに追加" height="20" src="https://b.st-hatena.com/images/entry-button/button-only.gif" style="border: none;" width="20"/></a><script async="async" charset="utf-8" src="https://b.st-hatena.com/js/bookmark_button.js" type="text/javascript"></script></div>
</div>
<!--<![endif]-->
</div>
</div>
<!-- google_ad_section_end -->
</div><!--△:sidebar.phpを編集、またはWP管理画面のウィジェットで編集-->
</div>
<!--/main-in-->
<!--▼ページの先頭へ戻る-->
<div id="page-top">
<div id="footer-breadcrumbs">
<div id="breadcrumbs2">
<a href="https://freesoftlab.com">フリーソフトラボ.com</a></div>
</div>
<div id="page-top-button"><a href="#container" id="PageTop"><img alt="このページの先頭へ" class="over png_bg" height="34" src="https://freesoftlab.com/wp-content/themes/wp4/images/common/btn-pagetop_off.png" width="162"/></a></div>
</div>
<!--▲ページの先頭へ戻る-->
<!--▽フッター:footer.phpを編集-->
<div id="footer">
<div id="footer-line-bg">
<div id="footer-line">
<!-- google_ad_section_start(weight=ignore) -->
<div id="footer-in">
<!--アドレスエリア-->
<div id="area01">
<div id="footer-text">
<p><img alt="フリーソフトラボ.com" height="26" src="https://freesoftlab.com/wp-content/themes/wp4/images/common/logos.png" width="160"/></p>
<p style="padding-top:15px;color: #666;">このサイトはソフトウェアの専門家がWindows用フリーソフトを多数紹介しているフリーソフト・無料ソフトの総合情報ポータルサイトです。各ソフトウェアはカテゴリー別に細かく分類されていますので、トップページからリンクをたどっていけば目的のソフトがすぐに見つかるようになっています。各ソフトの詳細ページは、概要から機能・使い方まで詳しく記しています。海外製ソフトの場合は日本語化パッチの配布サイトなども紹介しており、シェアウェアの場合は体験版があれば明記しています。</p>
<!--/アドレスエリア-->
</div>
</div>
<!--フッターメニュー-->
<div id="area02">
<!--カテゴリーエリア-->
<h3>Facebookページ</h3>
<div style="background: #ffffff; padding-right: 9px; padding-right: 10px; padding-bottom: 50px;">
<!--[if gt IE 6]><!--><div class="fb-like-box" data-header="true" data-height="200" data-href="http://www.facebook.com/freesoftlab" data-show-faces="true" data-stream="false" data-width="617"></div><!--<![endif]-->
</div>
<!--/カテゴリーエリア-->
</div>
<!--/フッターメニュー-->
</div>
<!--フッターバナー-->
<div id="footer-banner">
<ul>
<li><a href="http://www.facebook.com/freesoftlab"><img alt="Twitter" class="png_bg overimg" height="38" src="https://freesoftlab.com/wp-content/themes/wp4/images/facebook.png" width="134"/></a></li>
<li><a href="http://twitter.com/freesoftlab"><img alt="Facebook" class="png_bg overimg" height="38" src="https://freesoftlab.com/wp-content/themes/wp4/images/twitter.png" width="114"/></a></li>
<li><a href="https://freesoftlab.com/feed/" rel="nofollow"><img alt="RSS" class="png_bg overimg" height="38" src="https://freesoftlab.com/wp-content/themes/wp4/images/rss.png" width="88"/></a></li>
<li style="margin-left:80px;margin-top:8px;"><a href="https://freesoftlab.com/about/"><i aria-hidden="true" class="fa fa-info-circle"></i> このサイトについて</a> | <a href="https://freesoftlab.com/old/"><i aria-hidden="true" class="fa fa-folder-open-o"></i> 旧コンテンツ</a> | <a href="https://freesoftlab.com/link/"><i aria-hidden="true" class="fa fa-link"></i> リンク集</a> | <a href="https://freesoftlab.com/about/privacy/"><i aria-hidden="true" class="fa fa-lock"></i> プライバシーポリシー</a></li>
</ul>
</div>
<!--/フッターバナー-->
<!--コピーライト-->
<p class="copyright"><small>Copyright (C) 2008 - 2021 <span itemprop="reviewer">フリーソフトラボ.com</span> All Rights Reserved.</small></p>
<!--/コピーライト-->
<!-- google_ad_section_end -->
</div>
</div>
</div><!--△フッター:footer.phpを編集-->
</div>
<!--/main-->
<!--▼TOPメニュー:topmenu.phpを編集-->
<div id="top">
<div id="top-in">
<ul id="top-menu">
<!-- <li><a href="https://freesoftlab.com/?sitemap">サイトマップ</a></li> -->
<li><a href="https://freesoftlab.com/word/">コンピューター用語一覧</a></li>
<li><a href="https://freesoftlab.com/history/">閲覧履歴</a></li>
</ul>
</div>
</div><!--▲TOPメニュー:topmenu.phpを編集-->
<!--▼ヘッダー-->
<div id="header">
<div id="header-in">
<div id="header-logo">
<p id="logo-text"><a href="https://freesoftlab.com/"><img alt="フリーソフトラボ.com" height="53" src="https://freesoftlab.com/wp-content/themes/wp4/images/common/top_logo.png" srcset="https://freesoftlab.com/wp-content/themes/wp4/images/common/top_logo.png 1x,https://freesoftlab.com/wp-content/themes/wp4/images/common/top_logo@2x.png 2x" width="350"/></a></p>
</div>
<div id="header-text">
<div class="search_block" style="padding-top:30px;margin-left:auto;float:right;width:400px;">
<script>
  (function() {
    var cx = 'partner-pub-2444845910812172:8301585803';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchbox-only></gcse:searchbox-only>
</div>
</div>
</div>
<div id="nav">
<ul class="clearfix">
<li class="nav-home"><a class="home-on-mouse" href="https://freesoftlab.com" title="トップページ">トップページ</a></li>
<li class="nav-windows"><a href="https://freesoftlab.com/windows/" title="Windows用ソフトウェア">Windows用ソフトウェア</a></li>
<li class="nav-browser"><a href="https://freesoftlab.com/browser-extension/" title="ブラウザ拡張機能">ブラウザ拡張機能</a></li>
</ul>
</div><!-- /グローバルナビ -->
<div id="header-pankuzu-area">
<div id="header-pankuzu-area-inner">
<a href="https://freesoftlab.com">フリーソフトラボ.com</a></div>
</div><!-- /ぱんくずリスト -->
<div style="width:1000px;margin:20px auto 0;">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- ソフトラボヘッダー下 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2444845910812172" data-ad-format="horizontal" data-ad-slot="2097924856" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div><!--▲ヘッダー-->
</div>
<script id="wp-embed-js" src="https://freesoftlab.com/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:9.2',blog:'25457015',post:'0',tz:'9',srv:'freesoftlab.com'} ]);
	_stq.push([ 'clickTrackerInit', '25457015', '0' ]);
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function(){$(".over").hover(function(){var a=$(this).attr("src").replace("_off","_on");$(this).attr("src",a)},function(){var a=$(this).attr("src").replace("_on","_off");$(this).attr("src",a)})});</script>
<script type="text/javascript">$(function(){$("a#PageTop").click(function(){$("html,body").animate({scrollTop:$($(this).attr("href")).offset().top},"normal","swing");return false})});</script>
<!--[if gt IE 6]><!--><script type="text/javascript">(function($){$(document).ready(function(){var d=$('#main-contents');var e=$('#sidebar');var f=$('#sidefooter');var w=$(window);var g=f.outerHeight();var h=f.offset().top;var i=e.offset().left;var j={top:e.css('margin-top')?e.css('margin-top'):0,right:e.css('margin-right')?e.css('margin-right'):0,bottom:e.css('margin-bottom')?e.css('margin-bottom'):0,left:e.css('margin-left')?e.css('margin-left'):0};var k;var l;var m=function(){sideHeight=e.outerHeight();mainHeight=d.outerHeight();mainAbs=d.offset().top+mainHeight;var a=w.scrollTop();k=w.scrollLeft();var b=w.height();var c=(a>h)&&(mainHeight>sideHeight)?true:false;l=!c?'static':(a+g)>mainAbs?'absolute':'fixed';if(l==='fixed'){e.css({position:l,top:'',bottom:b-g,left:i-k,margin:0})}else if(l==='absolute'){e.css({position:l,top:mainAbs-sideHeight,bottom:'',left:i,margin:0})}else{e.css({position:l,marginTop:j.top,marginRight:j.right,marginBottom:j.bottom,marginLeft:j.left})}};var n=function(){e.css({position:'static',marginTop:j.top,marginRight:j.right,marginBottom:j.bottom,marginLeft:j.left});i=e.offset().left;k=w.scrollLeft();if(l==='fixed'){e.css({position:l,left:i-k,margin:0})}else if(l==='absolute'){e.css({position:l,left:i,margin:0})}};w.on('load',m);w.on('scroll',m);w.on('resize',n)})})(jQuery);</script><!--<![endif]-->
<script type="text/JavaScript">$(function(){var a=$('.overimg');a.hover(function(){$(this).fadeTo(100,0.5)},function(){$(this).fadeTo(100,1)})});</script>
<script src="https://freesoftlab.com/wp-content/themes/wp4/js/jquery.biggerlink.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
	$('.new-area .new-contents').biggerlink();
});
</script>
<script src="https://freesoftlab.com/wp-content/themes/wp4/js/jquery.biggerlink.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){
	$('#main-contents .parent-category').biggerlink();
});
</script>
<script defer="" src="//freesoftlab.xsrv.jp/thk/script.php" type="text/javascript"></script><noscript><img alt="" height="1" src="//freesoftlab.xsrv.jp/thk/track.php" width="1"/></noscript>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement":
  [
    {
      "@type": "ListItem",
      "position": 1,
      "item":
      {
        "@id": "https://freesoftlab.com",
        "name": "フリーソフトラボ.com"
      }
    }
  ]
}
</script>
</body>
</html>
