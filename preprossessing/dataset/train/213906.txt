<!DOCTYPE html>
<html dir="ltr" lang="es-es" vocab="http://schema.org/">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/images/logos/favicon.png" rel="shortcut icon"/>
<link href="/images/logos/apple_touch_icon.png" rel="apple-touch-icon"/>
<meta charset="utf-8"/>
<base href="https://www.betsubarabakery.es/"/>
<meta content="León, tartas, repostería, creativa,cupcakes, bodas, mesas, dulces, pasteles, comunión, bautizo, cumpleaños" name="keywords"/>
<meta content="Betsubara" name="rights"/>
<meta content="Super User" name="author"/>
<meta content="Betsubara Bakery" name="description"/>
<title>Betsubara Bakery - Bienvenidos</title>
<link href="/plugins/system/jce/css/content.css?ece9ab7fbaa16f90a1af6c271aa44aeb" rel="stylesheet"/>
<link href="/templates/yootheme/css/theme.9.css?1610024906" rel="stylesheet"/>
<script src="/templates/yootheme/vendor/assets/uikit/dist/js/uikit.min.js?2.3.25"></script>
<script src="/templates/yootheme/vendor/assets/uikit/dist/js/uikit-icons.min.js?2.3.25"></script>
<script src="/templates/yootheme/js/theme.js?2.3.25"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
            Array.prototype.slice.call(document.querySelectorAll('a span[id^="cloak"]')).forEach(function(span) {
                span.innerText = span.textContent;
            });
        });
	</script>
<script>var $theme = {};</script>
</head>
<body class="">
<div class="tm-page">
<div class="tm-header-mobile uk-hidden@m">
<div class="uk-navbar-container">
<nav uk-navbar="">
<div class="uk-navbar-center">
<a class="uk-navbar-item uk-logo" href="https://www.betsubarabakery.es/">
<img alt="Betsubara" src="/images/logos/betsubarabakery-logo-02.svg"/></a>
</div>
<div class="uk-navbar-right">
<a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle="">
<div uk-navbar-toggle-icon=""></div>
</a>
</div>
</nav>
</div>
<div class="uk-modal-full" id="tm-mobile" uk-modal="">
<div class="uk-modal-dialog uk-modal-body uk-text-center uk-flex uk-height-viewport">
<button class="uk-modal-close-full" type="button" uk-close=""></button>
<div class="uk-margin-auto-vertical uk-width-1-1">
<div class="uk-child-width-1-1" uk-grid=""> <div>
<div class="uk-panel" id="module-menu-mobile">
<ul class="uk-nav uk-nav-primary uk-nav-center">
<li class="uk-active"><a href="/">Bienvenidos</a></li>
<li><a href="/nuestros-productos">Nuestras tartas</a></li>
<li><a href="/cupcakes">Cupcakes</a></li>
<li><a href="/nuestros-sabores">Mesas dulces</a></li>
<li><a href="/detalles-para-eventos">Detalles para eventos</a></li>
<li><a href="/contacto">Contacto</a></li>
<li><a href="/galería">Galería</a></li></ul>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="tm-header uk-visible@m" uk-header="">
<div class="tm-headerbar-top">
<div class="uk-container uk-container-expand">
<div class="uk-text-center">
<a class="uk-logo" href="https://www.betsubarabakery.es/">
<img alt="Betsubara" src="/images/logos/betsubarabakery-logo-01.svg"/><img alt="Betsubara" class="uk-logo-inverse" src="/images/logos/betsubarabakery-logo-03.svg"/></a>
</div>
</div>
</div>
<div cls-active="uk-navbar-sticky" media="@m" sel-target=".uk-navbar-container" uk-sticky="">
<div class="uk-navbar-container">
<div class="uk-container uk-container-expand">
<nav class="uk-navbar" uk-navbar='{"align":"center","boundary":"!.uk-navbar-container","boundary-align":true}'>
<div class="uk-navbar-center">
<ul class="uk-navbar-nav">
<li class="uk-active"><a href="/">Bienvenidos</a></li>
<li><a href="/nuestros-productos">Nuestras tartas</a></li>
<li><a href="/cupcakes">Cupcakes</a></li>
<li><a href="/nuestros-sabores">Mesas dulces</a></li>
<li><a href="/detalles-para-eventos">Detalles para eventos</a></li>
<li><a href="/contacto">Contacto</a></li>
<li><a href="/galería">Galería</a></li></ul>
</div>
</nav>
</div>
</div>
</div>
</div>
<div data-messages="[]" id="system-message-container">
</div>
<!-- Builder #page -->
<div class="uk-section-secondary" tm-header-transparent="dark" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-top; delay: false;">
<div class="uk-background-norepeat uk-background-cover uk-background-top-center uk-background-fixed uk-section uk-section-large" data-sizes="(max-aspect-ratio: 4160/3120) 133vh" data-src="/templates/yootheme/cache/DSC_2027-b41ebd5e.jpeg" data-srcset="/templates/yootheme/cache/DSC_2027-b6be3b8a.jpeg 768w, /templates/yootheme/cache/DSC_2027-06736fd1.jpeg 1024w, /templates/yootheme/cache/DSC_2027-c4c4fc38.jpeg 1366w, /templates/yootheme/cache/DSC_2027-d30e8107.jpeg 1600w, /templates/yootheme/cache/DSC_2027-04ae3f41.jpeg 1920w, /templates/yootheme/cache/DSC_2027-b41ebd5e.jpeg 4160w" style="background-color: #1e3040;" uk-height-viewport="offset-top: true; offset-bottom: 20;" uk-img="">
<div class="uk-container">
<div class="tm-header-placeholder uk-margin-remove-adjacent"></div>
<div class="tm-grid-expand uk-child-width-1-1 uk-grid-margin" uk-grid="">
<div class="uk-width-1-1@m">
</div>
</div>
</div>
</div>
</div>
<div class="uk-section-default uk-section-overlap uk-section uk-section-large" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-top-medium; delay: false;">
<div class="uk-container uk-container-expand">
<div class="uk-grid-margin uk-container uk-container-expand"><div class="tm-grid-expand uk-child-width-1-1" uk-grid="">
<div class="uk-width-1-1@m">
<h1 class="uk-text-center" uk-scrollspy-class="">        Bienvenidos    </h1>
<div class="uk-margin-small uk-text-left@m uk-text-left">
<div class="uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-2@m uk-child-width-1-2@l uk-child-width-1-2@xl uk-grid-match" uk-grid=""> <div>
<div class="el-item uk-panel uk-margin-remove-first-child" uk-scrollspy-class="">
<img alt="" class="el-image" data-height="666" data-sizes="(min-width: 1000px) 1000px" data-src="/templates/yootheme/cache/IMG-20171130-WA0002-a0286b15.jpeg" data-srcset="/templates/yootheme/cache/IMG-20171130-WA0002-d97789ae.jpeg 768w, /templates/yootheme/cache/IMG-20171130-WA0002-a0286b15.jpeg 1000w" data-width="1000" uk-img=""/>
<h3 class="el-title uk-margin-top uk-margin-remove-bottom">                        Quien soy                    </h3>
<div class="el-content uk-panel uk-margin-top">Soy Adela, como mi abuela, y tengo dos pasiones en la vida: la repostería y el motociclismo. Bueno, y la música. Y los libros. Y los tatus. Y el gim. Y la cocina. Y las artes marciales. Vale.. Me apasiona vivir. Creo que no merece la pena que pase un día sin disfrutar al menos un poquito. Aunque sea un trozo de <strong>chocolate</strong> (o una tableta entera).<br/><br/>
Siempre esperé mi carta de Hogwarts, pero no llegaba y acabé en Matrix. Números, orden, y ojeras hasta los pies. Estuvo bien, pero no era lo mío. Busqué y busqué una salida.. y cuando ya no sabía por que camino ir apareció un conejo blanco. La lógica me decía que no lo siguiera, pero siempre he sido un poco cabeza loca así que me fuí detrás.  No he tomado mejor decisión en mi vida. Directa a Wonderland. Betsubara Bakery es un sueño, mi dulce aportación a este mundo a veces tan capullo y tan feo. Y quiero que soñeis conmigo. ¿Me acompañáis?</div>
</div></div>
<div>
<div class="el-item uk-panel uk-margin-remove-first-child" uk-scrollspy-class="">
<img alt="CrazyCake" class="el-image" data-height="982" data-sizes="(min-width: 1490px) 1490px" data-src="/templates/yootheme/cache/recorte2-8c1932a9.jpeg" data-srcset="/templates/yootheme/cache/recorte2-5cbaaff6.jpeg 768w, /templates/yootheme/cache/recorte2-0c9ed91f.jpeg 1024w, /templates/yootheme/cache/recorte2-7d2dbee1.jpeg 1366w, /templates/yootheme/cache/recorte2-8c1932a9.jpeg 1490w" data-width="1490" uk-img=""/>
<h3 class="el-title uk-margin-top uk-margin-remove-bottom">                        Betsubara Bakery                    </h3>
<div class="el-content uk-panel uk-margin-top">No tenemos sangre, es chocolate fundido. Como cerebro, un esponjoso bizcocho de canela. Vemos a través de ojos de caramelo y nos trenzamos el cabello de ángel. ¿El corazón? Un enorme cupcake coronado con suave y cremosa buttercream y un montón de sprinkles de colores. 
Si estamos tristes, horneamos (y comemos) . Si estamos felices, horneamos más (y repartimos). Por eso existe Betsubara Bakery. Queremos formar parte de vuestra vida con lo que nos la da a nosotros: el dulce. En cualquier momento y ocasión. Te casas y no-sé-cómo-lo-ves-pero-quiero-mesa-dulce. Es el cumple de tu amigo enganchado a los programas de tartas que habla de Alma Obregón como si fuera su prima. Quieres tener un detalle con tu madre por todo lo que te ha aguantado (y lo que le queda). O simplemente quieres darte un homenaje porque te lo mereces (o no, pero a quién le importa).  Queremos encargarnos de todo. Solo necesitamos conocerte, hablar contigo...y dejar que la magia fluya.</div>
</div></div>
</div>
</div>
</div>
</div></div>
</div>
</div>
<div class="uk-section-secondary" tm-header-transparent="dark" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-top; delay: false;">
<div class="uk-background-norepeat uk-background-cover uk-background-bottom-center uk-background-fixed uk-section uk-section-large uk-flex uk-flex-middle" data-sizes="(max-aspect-ratio: 3288/2000) 164vh" data-src="/templates/yootheme/cache/10-75ef42eb.jpeg" data-srcset="/templates/yootheme/cache/10-c1b6cd74.jpeg 768w, /templates/yootheme/cache/10-73ee508a.jpeg 1024w, /templates/yootheme/cache/10-bb40f67d.jpeg 1366w, /templates/yootheme/cache/10-6df56b2a.jpeg 1600w, /templates/yootheme/cache/10-75365e90.jpeg 1920w, /templates/yootheme/cache/10-75ef42eb.jpeg 3288w" style="background-color: #1e3040;" uk-height-viewport="offset-top: true; offset-bottom: 20;" uk-img="">
<div class="uk-width-1-1">
<div class="uk-container">
<div class="tm-header-placeholder uk-margin-remove-adjacent"></div>
<div class="tm-grid-expand uk-child-width-1-1 uk-grid-margin" uk-grid="">
<div class="uk-width-1-1@m">
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Builder #footer -->
<div class="tm-footer uk-section-default uk-section" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-bottom-medium; delay: 200;">
<div class="uk-container">
<div class="tm-grid-expand uk-child-width-1-1 uk-grid-margin" uk-grid="">
<div class="uk-grid-item-match uk-flex-middle uk-width-1-1@m">
<div class="uk-panel uk-width-1-1">
<div class="uk-margin uk-text-center@m uk-text-center" uk-scrollspy-class="">
<a class="el-link" href="/index.php"><img alt="" class="el-image" data-src="/images/logos/betsubarabakery-logo-02.svg" uk-img="" width="150"/></a>
</div>
<div class="uk-panel uk-text-meta uk-margin uk-text-center" uk-scrollspy-class="">Diseño: <a href="http://www.creatico.es" targewt="_blank">Creático Comunicación</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
