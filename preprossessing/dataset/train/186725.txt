<html>
<head>
<title>Jarry Park</title>
<style type="text/css">
<!--
  A:link {color: #00FF00; text-decoration: none}
  A:visited {color: #00C000; text-decoration: none}
  A:active {color: #00A000; text-decoration: none}
  A:hover {color: #00A000; text-decoration: none}
-->
</style>
<meta content="Facts, figures and photos about the former home of the Montreal Expos" name="description"/>
<meta content="Montreal,Quebec,Expos,Jarry,Park,ballparks,stadiums,fields,baseball,tickets" name="keywords"/>
</head>
<body alink="#00A000" background="jarrypbg.gif" bgcolor="#006000" bgproperties="fixed" link="#00FF00" text="#FFFFFF" vlink="#00C000">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="490"><tr><td>
<font face="arial,helvetica" size="2">
</font><center>
<p></p><table border="3" cellpadding="0" cellspacing="0"><tr><td>
<a href="http://affiliates.art.com/get.art?T=15038844&amp;A=926552&amp;L=7&amp;P=1834&amp;S=1&amp;Y=194" onmouseout="window.status=''; return true" onmouseover="window.status='Click here to visit Art.com';return true" target="_top"><img alt="Visit Art.com" border="0" height="60" src="https://secureimg.art.com/images/aff/banners/2804_10000138.gif" width="468"/></a></td></tr></table>
<h2>Jarry Park</h2>
<p></p><table border="3" cellpadding="0" cellspacing="0"><tr><td><img alt="Aerial view of Jarry Park" height="240" src="jarryp01.jpg" width="400"/></td></tr></table>
<h3>Montreal, Quebec</h3>
</center>
<p><b>Tenant: </b>Montreal Expos (NL)<br/>
<b>Opened: </b>n/a<br/>
<b>First Expos game: </b>April 14, 1969<br/>
<b>Last Expos game: </b>September 26, 1976<br/>
<b>Current status: </b>Tennis stadium<br/>
<b>Capacity: </b>3,000 (1968); 28,456 (1969)<br/>
<b>Surface: </b>Grass</p>
<p><b>Washington Nationals tickets:</b></p>
<ul>
<li><a href="http://www.viewpointtickets.com" target="_top"><b>Viewpoint Tickets</b></a> - Best prices on
<a href="http://www.viewpointtickets.com/ResultsGeneral.aspx?stype=0&amp;kwds=Washington Nationals" target="_top">Nationals tickets</a>,
<a href="http://www.viewpointtickets.com/MLB.aspx" target="_top">MLB tickets</a> and
<a href="http://www.viewpointtickets.com/ResultsGeneral.aspx?stype=0&amp;kwds=MLB All Star" target="_top">MLB All Star tickets</a>.</li>
</ul>
<p><b>Location:</b> About 7 kilometers (4.2 miles) from downtown Montreal.  First base, Rue Faillon; right field, Boulevard St. Laurent and public swimming pool; left field, Rue Jarry; third base, Canadian Pacific Railroad tracks.</p>
<p><b>Dimensions:</b> Left field: 340 ft.; left-center: 368 ft.; center field: 415 ft. (1969), 417 ft. (1971), 420 ft. (1974); right-center: 368 ft.; right field: 340 ft.; backstop: 62 ft.</p>
<p><b>Fences:</b> 8 ft. (1969); 5 ft. (1970); 8 ft. (1976)</p>
<center><p></p><table border="0" cellpadding="0" cellspacing="0"><tr><td><table border="3" cellpadding="0" cellspacing="0"><tr><td><img alt="Jarry Park and the scoreboard" border="0" height="138" src="jarryp02.jpg" width="230"/></td></tr></table></td><td width="12">&amp;nbsp</td><td><table border="3" cellpadding="0" cellspacing="0"><tr><td><img alt="Jarry Park grandstand" border="0" height="138" src="jarryp03.jpg" width="230"/></td></tr></table></td></tr></table></center>
<p><b>M</b>ontreal was granted Canada's first Major League Baseball franchise in 1968 with the proviso that the team find a suitable place to play, pending the eventual construction of a promised domed stadium.  Jarry Park (Parc Jarry) Stadium was a 3,000 seat municipally owned ballpark located in a corner of Jarry Park.  The city expanded the seating capacity to nearly 28,500 in time for the Expos' 1969 inaugural season and the team played there until they moved into Olympic Stadium in 1977.  Jarry Park is still standing and is used regularly for social and civic events, professional tennis and other large outdoor gatherings.</p>
<p><b>Recommended Reading</b> (<a href="../general/biblio.htm">bibliography</a>)<b>:</b></p>
<p></p><ul>
<li><i>Take Me Out to the Ballpark: An Illustrated Tour of Baseball Parks Past and Present</i> by Josh Leventhal and Jessica Macmurray.</li>
<li><i>The Ballpark Book: A Journey Through the Fields of Baseball Magic (Revised Edition)</i> by Ron Smith and Kevin Belford.</li>
<li><i>City Baseball Magic: Plain Talk and Uncommon Sense about Cities and Baseball Parks</i> by Philip Bess.</li>
<li><i>Diamonds: The Evolution of the Ballpark</i> by Michael Gershman.</li>
<li><i>Green Cathedrals: The Ultimate Celebration of All 273 Major League and Negro League Ballparks</i> by Philip J. Lowry.</li>
<li><i>Lost Ballparks: A Celebration of Baseball's Legendary Fields</i> by Lawrence S. Ritter.</li>
<li><i>Roadside Baseball: A Guide to Baseball Shrines Across America</i> by Chris Epting.</li>
<li><i>The Story of America's Classic Ballparks</i> (VHS).</li>
</ul>
<center>
<p></p><table border="3" cellpadding="0" cellspacing="0"><tr><td bgcolor="#FFFFFF"><table border="0" cellpadding="11" cellspacing="0"><tr><td><a href="olympi.htm" onmouseout="window.status=''; return true" onmouseover="window.status='The Expos played at Olympic Stadium from 1977-2004';return true"><img alt="Olympic Stadium" border="0" height="60" src="../menu/olympiic.jpg" width="100"/></a></td><td><a href="../national/hibith.htm" onmouseout="window.status=''; return true" onmouseover="window.status='The Expos played part of their schedule at Hiram Bithorn Stadium in 2003 and 2004';return true"><img alt="Hiram Bithorn Stadium" border="0" height="60" src="../menu/hibithic.jpg" width="100"/></a></td><td><a href="../american/rfksta.htm" onmouseout="window.status=''; return true" onmouseover="window.status='The Nationals (Expos) relocated to RFK Stadium in Washington, DC in 2005';return true"><img alt="RFK Stadium" border="0" height="60" src="../menu/rfkstaic.jpg" width="100"/></a></td><td><a href="../national/wasbpk.htm" onmouseout="window.status=''; return true" onmouseover="window.status='The Nationals (Expos) will play in Nationals Park in 2008';return true"><img alt="Nationals Park" border="0" height="60" src="../menu/wasbpkic.jpg" width="100"/></a></td></tr></table><table border="0" cellpadding="10" cellspacing="0"><tr><td>
<script type="text/javascript"><!--
google_ad_client = "pub-3797364905133207";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="";
google_page_url = document.location;
//--></script>
<script src="https://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</td></tr></table></td></tr></table>
<p>Help us provide a better web site by completing our <a href="../general/feedback.htm" onmouseout="window.status=''; return true" onmouseover="window.status='Send us your comments and suggestions on our feedback form';return true"><b>feedback</b></a> form</p>
</center>
<font size="1">
<p>PHOTOGRAPHS:</p>
<p>Aerial view of Jarry Park courtesy of Dennis Goldstein.<br/>
View of right field scoreboard courtesy of the Franklin Digital Collection.<br/>
View of grandstand by Munsey &amp; Suppes.</p>
<p>Updated November 2004</p>
<p>Tickets to <a href="https://ballparks.com/tickets/ncaabasketball/ncaa_basketball_tournament.htm" target="_top">NCAA Basketball Tournament</a>, <a href="https://ballparks.com/tickets/ncaafootball/ncaa_football_bowl_tickets.htm" target="_top">College Football Bowl</a>, <a href="https://ballparks.com/tickets/ncaafootball/ncaa_football_tickets.htm" target="_top">NCAA Football</a>, <a href="https://ballparks.com/tickets/paul_mccartney/paul_mccartney_tickets.htm" target="_top">Paul McCartney</a> and <a href="https://ballparks.com/tickets/washington_nationals_tickets.htm" target="_top">Washington Nationals</a> provided by Ticket Triangle.
</p>
<p><a href="../index.htm" onmouseout="window.status=''; return true" onmouseover="window.status='Click here to go to the Ballparks main page';return true" target="_top">BALLPARKS</a> © 1996-2014 by Munsey &amp; Suppes.</p>
<p> </p>
</font>
</td></tr></table>
</center>
</body>
</html>