<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>          
            Eric Copeman Imaging | Home 
          
</title>
<!-- Facebook Open Graph -->
<meta content="https://www.ericcopeman.com/" property="og:url"/>
<meta content="" property="og:image"/>
<meta content="website" property="og:type"/>
<meta content="Eric Copeman Imaging" property="og:title"/>
<meta content="Eric Copeman Imaging" property="og:site_name"/>
<meta content="Eric Copeman Imaging, Landscape, Atmospheric, Cosmetic, Food" property="og:description"/>
<meta content="Eric Copeman Imaging" property="og:image:alt"/>
<meta content="" property="og:image:width"/>
<meta content="" property="og:image:height"/>
<meta content="summary_large_image" name="twitter:card"/>
<!-- End of Facebook Open Graph -->
<!-- Meta Tags -->
<meta content="Home" name="title"/>
<meta content="Eric Copeman Imaging, Landscape, Atmospheric, Cosmetic, Food" name="keywords"/>
<meta content="Eric Copeman Imaging, Landscape, Atmospheric, Cosmetic, Food" name="description"/>
<link href="https://www.ericcopeman.com/" rel="canonical"/>
<meta content="index, follow" name="robots"/>
<meta content="telephone=no" name="format-detection"/>
<!-- End of Site Meta Tags -->
<meta content="MTYxMDYwMTc0NTA3ZTcxMWE3MjA2ZDNjYmNhMmJiYzJhY2NjYmUyYzEwNWRiN2Q0MTEyd0p5VXVLWHhjT1RmRGhyMlFRV3NFYUYzUEdJMXEzMA==" name="csrf-token"/>
<link href="https://assets.foliowebsites.com/f76a89f0cb91bc419542ce9fa43902dc/web-643/7ed455de6a8b8903.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://assets.foliowebsites.com" rel="dns-prefetch"/>
<!--  Base CSS Styles -->
<!-- <link rel="stylesheet" href="/public/css/bootstrap.css"> -->
<link href="/public/css/themes/common/bootstrap.css" rel="stylesheet"/>
<link href="/public/fonts/folio/folio.css" rel="stylesheet"/>
<link href="/public/fonts/folio-icon/folio-icon.css" rel="stylesheet"/>
<link href="/public/gulp_dest/common/common.min.css" rel="stylesheet"/>
<link href="/public/css/themes/common/bundle/theme-plugin.css" rel="stylesheet"/>
<link href="/public/css/themes/minimalist_theme/bundle/css/minimalist-theme.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Abhaya+Libre:400,600,700|Arvo:400,700|Dancing+Script:400,700|Droid+Serif:400,700|Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Oswald:300,400,600,700|Playfair+Display:400,400i,700,700i|Roboto:300,300i,400,400i,700,700i|Tangerine:400,700|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet"/>
<script type="text/javascript">
    window.mapElement = [];

    window.folioUrl = 'https://foliowebsites.com';

    function generateRandomHeight() {
        return Math.floor((Math.random() * 200) + 300);
    };

    
    
    </script>
<!-- Custom Head Scripts -->
<!-- Custom Page Header Scripts -->
<!-- End of Custom Page Header Scripts -->
<style>
        span[class^="f_color_"], span[class*=" f_color_"] {
            color: initial !important;
        }

        
        
        #minimalist_theme { background-color: rgba(0, 0, 0, 1); }

        #minimalist_theme .number-input .inc-btn,
        #minimalist_theme .number-input .dec-btn { background-color: rgba(0, 0, 0, 1); }

        #minimalist_theme h1,
        #minimalist_theme h2,
        #minimalist_theme h3,
        #minimalist_theme h4,
        #minimalist_theme h5,
        #minimalist_theme h6,
        #minimalist_theme .section-layout-wrapper h1 a,
        #minimalist_theme .section-layout-wrapper h2 a,
        #minimalist_theme .section-layout-wrapper h3 a,
        #minimalist_theme .section-layout-wrapper h4 a,
        #minimalist_theme .section-layout-wrapper h5 a,
        #minimalist_theme .section-layout-wrapper h6 a {
            font-family: 'Raleway', sans-serif;
            line-height: 1.5;
        }
        
        #minimalist_theme h1,
        #minimalist_theme h2,
        #minimalist_theme h3,
        #minimalist_theme h4,
        #minimalist_theme h5,
        #minimalist_theme h6 {
            color: #303030;
        }

        #minimalist_theme .section-layout-wrapper p,
        #minimalist_theme .section-layout-wrapper li,
        #minimalist_theme .section-layout-wrapper p span, 
        #minimalist_theme .section-layout-wrapper label,
        #minimalist_theme .section-layout .copyright-subtitle span {
            color: #303030;
        }
        
        #minimalist_theme .section-layout-wrapper a:not(.theme-btn),
        #minimalist_theme .section-layout-wrapper a:not(.theme-btn) span {
            color: #303030;
        }

        #minimalist_theme .section-layout-wrapper a:not(.theme-btn):hover {
            color: #c3c4c7;
        }
        
        #minimalist_theme .section-layout-wrapper .site-title,
        #minimalist_theme .section-layout-wrapper .site-title a,
        #minimalist_theme .section-layout-wrapper .site-title a:hover {
            color: #303030;
        }

        #minimalist_theme h1,
        #minimalist_theme h1 a {
            font-size: 45px;
        }

        #minimalist_theme h2,
        #minimalist_theme h2 a {
            font-size: 30px;
        }

        #minimalist_theme h3,
        #minimalist_theme h3 a {
            font-size: 24px;
        }

        #minimalist_theme h4,
        #minimalist_theme h4 a {
            font-size: 20px;
        }

        #minimalist_theme h5,
        #minimalist_theme h5 a{
            font-size: 15px;
        }

        #minimalist_theme h6,
        #minimalist_theme h6 a {
            font-size: 14px;
        }   

        #minimalist_theme p,
        #minimalist_theme a:not(.theme-btn),
        #minimalist_theme input,
        #minimalist_theme label,
        #minimalist_theme select,
        #minimalist_theme option,
        #minimalist_theme li,
        #minimalist_theme textarea,
        #minimalist_theme .contact-form.form-1 .form-group .select .select-styled,
        #minimalist_theme .contact-form.form-2 .form-group .select .select-styled,
        #minimalist_theme .contact-form.form-3 .form-group .select .select-styled,
        #minimalist_theme .alert span { font-family: 'Playfair Display', serif; }
        

        #minimalist_theme .f_xl {
            font-size: 24px;
            line-height: 1.5;
        }

        #minimalist_theme .f_lg {
            font-size: 18px;
            line-height: 1.5;
        }

        #minimalist_theme .f_md {
            font-size: 15px;
            line-height: 1.5;
        }

        #minimalist_theme .f_sm {
            font-size: 13px;
            line-height: 1.5;
        }

        #minimalist_theme .f_xs {
            font-size: 11px;
            line-height: 1.5;
        }

        #minimalist_theme a:not(.theme-btn), 
        #minimalist_theme a:not(.theme-btn):active, 
        #minimalist_theme a:not(.theme-btn):focus { 
            color: #303030;
        }

        #minimalist_theme a:not(.theme-btn):hover {
            color:  #c3c4c7;
        }

    

        #minimalist_theme .s-link-text-white .site-title,
        #minimalist_theme .s-link-text-white .site-title a,
        #minimalist_theme .s-link-text-white .site-title a:hover,
        #minimalist_theme .s-link-text-white .site-title span,
        #minimalist_theme .s-link-text-white .icon-header-3 {
            color: #fff;
        }
        
        #minimalist_theme .s-link-text-white a:not(.m-item):not(.theme-btn),
        #minimalist_theme .s-link-text-white a:not(.m-item):not(.theme-btn):hover {
            color: #fff;
        }
        
        #minimalist_theme .s-link-text-white .layout-header_1 .menu-horizontal a:before,
        #minimalist_theme .s-link-text-white .layout-header_1 .menu-horizontal a.active:before {
            background-color: #fff;
            color: #fff;
        }


        #minimalist_theme .s-content-text-white,
        #minimalist_theme .s-content-text-white p,
        #minimalist_theme .s-content-text-white li,
        #minimalist_theme .s-content-text-white label,
        #minimalist_theme .s-content-text-white span:not(.mce-text):not(.m-item):not(.mce-txt):not(.logo-link):not([class^="f_color_"]):not(.form-error):not(.form-send-message)  {
            color: #fff;
        }
        
        #minimalist_theme .s-title-text-white h1,
        #minimalist_theme .s-title-text-white h2,
        #minimalist_theme .s-title-text-white h3,
        #minimalist_theme .s-title-text-white h4,
        #minimalist_theme .s-title-text-white h5,
        #minimalist_theme .s-title-text-white h6,
        #minimalist_theme .s-title-text-white h1 a,
        #minimalist_theme .s-title-text-white h2 a,
        #minimalist_theme .s-title-text-white h3 a,
        #minimalist_theme .s-title-text-white h4 a,
        #minimalist_theme .s-title-text-white h5 a,
        #minimalist_theme .s-title-text-white h6 a,
        #minimalist_theme .s-title-text-white h1 a:hover,
        #minimalist_theme .s-title-text-white h2 a:hover,
        #minimalist_theme .s-title-text-white h3 a:hover,
        #minimalist_theme .s-title-text-white h4 a:hover,
        #minimalist_theme .s-title-text-white h5 a:hover,
        #minimalist_theme .s-title-text-white h6 a:hover {
            color: #fff;
        }
        
        #minimalist_theme .s-link-text-black .site-title,
        #minimalist_theme .s-link-text-black .site-title a,
        #minimalist_theme .s-link-text-black .site-title a:hover,
        #minimalist_theme .s-link-text-black .site-title span,
        #minimalist_theme .s-link-text-black .site-tagline {
            color: #000;
        }

        #minimalist_theme .s-link-text-black a:not(.m-item):not(.theme-btn),
        #minimalist_theme .s-link-text-black a:not(.m-item):not(.theme-btn):hover {
            color: #000;
        }

       
        #minimalist_theme .s-content-text-black p,
        #minimalist_theme .s-content-text-black li,
        #minimalist_theme .s-content-text-black label,
        #minimalist_theme .s-content-text-black span:not(.mce-text):not(.m-item):not(.mce-txt):not(.logo-link):not([class^="f_color_"]):not(.form-error):not(.form-send-message)  {
            color: #000;
        }
        
        #minimalist_theme .s-title-text-black h1,
        #minimalist_theme .s-title-text-black h2,
        #minimalist_theme .s-title-text-black h3,
        #minimalist_theme .s-title-text-black h4,
        #minimalist_theme .s-title-text-black h5,
        #minimalist_theme .s-title-text-black h6,
        #minimalist_theme .s-title-text-black h1 a,
        #minimalist_theme .s-title-text-black h2 a,
        #minimalist_theme .s-title-text-black h3 a,
        #minimalist_theme .s-title-text-black h4 a,
        #minimalist_theme .s-title-text-black h5 a,
        #minimalist_theme .s-title-text-black h6 a,
        #minimalist_theme .s-title-text-black h1 a:hover,
        #minimalist_theme .s-title-text-black h2 a:hover,
        #minimalist_theme .s-title-text-black h3 a:hover,
        #minimalist_theme .s-title-text-black h4 a:hover,
        #minimalist_theme .s-title-text-black h5 a:hover,
        #minimalist_theme .s-title-text-black h6 a:hover {
            color: #000;
        }

        /*placeholder*/

        #minimalist_theme .s-link-text-white .no-element,
        #minimalist_theme .s-link-text-white p.no-element,
        #minimalist_theme .s-link-text-white .html-element-placeholder {
            border: 1px dashed rgba(255,255,255,1) !important;
            opacity: 1;
            color: #fff !important;
        }

        #minimalist_theme .s-link-text-black .no-element,
        #minimalist_theme .s-link-text-black p.no-element,
        #minimalist_theme .s-link-text-black .html-element-placeholder {
            border: 1px dashed rgba(0,0,0,1) !important;
            opacity: 1;
            color: #000 !important;
        }

        #minimalist_theme .s-content-text-black .html-element-placeholder { border-color: #000; }

        /*input placeholder*/
        #minimalist_theme ::-webkit-input-placeholder     { font-family: 'Playfair Display', serif; }
        #minimalist_theme ::-moz-placeholder              { font-family: 'Playfair Display', serif; }
        #minimalist_theme :-ms-input-placeholder          { font-family: 'Playfair Display', serif; }
        #minimalist_theme :-moz-placeholder               { font-family: 'Playfair Display', serif; }

        /*gallery style 2*/
        #minimalist_theme .section_photogrid .s-content-text-white .layout-photogrid_2 p,
        #minimalist_theme .section_photogrid .s-content-text-white .layout-photogrid_2 h6 { color: #fff;  }

        #minimalist_theme .section_photogrid .s-content-text-black .layout-photogrid_2 p,
        #minimalist_theme .section_photogrid .s-content-text-black .layout-photogrid_2 h6 { color: #000; }

        #minimalist_theme .s-link-text-black #header-menu-wrap .sub-menu a,
        #minimalist_theme .s-link-text-white #header-menu-wrap .sub-menu a,
        #minimalist_theme .s-content-text-white #header-menu-wrap .sub-menu a, 
        #minimalist_theme .s-content-text-black #header-menu-wrap .sub-menu a { color: #202020 !important} 

                    #minimalist_theme .f_color_0, 
            #minimalist_theme .f_color_0 span,
            #minimalist_theme .f_color_0 p,
            #minimalist_theme p.f_color_0,
            #minimalist_theme span.f_color_0,
            #minimalist_theme h1 span.f_color_0,
            #minimalist_theme h2 span.f_color_0,
            #minimalist_theme h3 span.f_color_0,
            #minimalist_theme h4 span.f_color_0,
            #minimalist_theme h5 span.f_color_0,
            #minimalist_theme h6 span.f_color_0, 
            #minimalist_theme .text-element h1 span.f_color_0 a,
            #minimalist_theme .text-element h2 span.f_color_0 a,
            #minimalist_theme .text-element h3 span.f_color_0 a,
            #minimalist_theme .text-element h4 span.f_color_0 a,
            #minimalist_theme .text-element h5 span.f_color_0 a,
            #minimalist_theme .text-element h6 span.f_color_0 a
            { color: #ffffff !important; }
                    #minimalist_theme .f_color_1, 
            #minimalist_theme .f_color_1 span,
            #minimalist_theme .f_color_1 p,
            #minimalist_theme p.f_color_1,
            #minimalist_theme span.f_color_1,
            #minimalist_theme h1 span.f_color_1,
            #minimalist_theme h2 span.f_color_1,
            #minimalist_theme h3 span.f_color_1,
            #minimalist_theme h4 span.f_color_1,
            #minimalist_theme h5 span.f_color_1,
            #minimalist_theme h6 span.f_color_1, 
            #minimalist_theme .text-element h1 span.f_color_1 a,
            #minimalist_theme .text-element h2 span.f_color_1 a,
            #minimalist_theme .text-element h3 span.f_color_1 a,
            #minimalist_theme .text-element h4 span.f_color_1 a,
            #minimalist_theme .text-element h5 span.f_color_1 a,
            #minimalist_theme .text-element h6 span.f_color_1 a
            { color: #000000 !important; }
                    #minimalist_theme .f_color_2, 
            #minimalist_theme .f_color_2 span,
            #minimalist_theme .f_color_2 p,
            #minimalist_theme p.f_color_2,
            #minimalist_theme span.f_color_2,
            #minimalist_theme h1 span.f_color_2,
            #minimalist_theme h2 span.f_color_2,
            #minimalist_theme h3 span.f_color_2,
            #minimalist_theme h4 span.f_color_2,
            #minimalist_theme h5 span.f_color_2,
            #minimalist_theme h6 span.f_color_2, 
            #minimalist_theme .text-element h1 span.f_color_2 a,
            #minimalist_theme .text-element h2 span.f_color_2 a,
            #minimalist_theme .text-element h3 span.f_color_2 a,
            #minimalist_theme .text-element h4 span.f_color_2 a,
            #minimalist_theme .text-element h5 span.f_color_2 a,
            #minimalist_theme .text-element h6 span.f_color_2 a
            { color: #303030 !important; }
                    #minimalist_theme .f_color_3, 
            #minimalist_theme .f_color_3 span,
            #minimalist_theme .f_color_3 p,
            #minimalist_theme p.f_color_3,
            #minimalist_theme span.f_color_3,
            #minimalist_theme h1 span.f_color_3,
            #minimalist_theme h2 span.f_color_3,
            #minimalist_theme h3 span.f_color_3,
            #minimalist_theme h4 span.f_color_3,
            #minimalist_theme h5 span.f_color_3,
            #minimalist_theme h6 span.f_color_3, 
            #minimalist_theme .text-element h1 span.f_color_3 a,
            #minimalist_theme .text-element h2 span.f_color_3 a,
            #minimalist_theme .text-element h3 span.f_color_3 a,
            #minimalist_theme .text-element h4 span.f_color_3 a,
            #minimalist_theme .text-element h5 span.f_color_3 a,
            #minimalist_theme .text-element h6 span.f_color_3 a
            { color: #606060 !important; }
                
                    #minimalist_theme .theme-btn.style1 {
                background-color : rgba(0, 0, 0, 1);
                border-color : transparent;
                
                
                color: #ffffff;
                font-size: 12px;
                font-weight: 400;  
                font-family: 'Lato', sans-serif;
                letter-spacing: 2px;
                line-height: 1.3;
                padding: 10px 30px;
                text-transform: uppercase;
            }

            #minimalist_theme .theme-btn.style1 p {
                color: #ffffff;
                font-family: 'Lato', sans-serif;
                font-size: 12px;
                font-weight: 400 ;  
                letter-spacing: 2px;
                line-height: 1.3;
                text-transform: uppercase;
            }
                    #minimalist_theme .theme-btn.style2 {
                border-color : rgba(0, 0, 0, 1);
                background-color : transparent;
                border-width: 1px;
                
                color: #000000;
                font-size: 12px;
                font-weight: 400;  
                font-family: 'Lato', sans-serif;
                letter-spacing: 2px;
                line-height: 1.5;
                padding: 25px 30px;
                text-transform: uppercase;
            }

            #minimalist_theme .theme-btn.style2 p {
                color: #000000;
                font-family: 'Lato', sans-serif;
                font-size: 12px;
                font-weight: 400 ;  
                letter-spacing: 2px;
                line-height: 1.5;
                text-transform: uppercase;
            }
                    #minimalist_theme .theme-btn.style3 {
                background-color : rgba(0, 0, 0, 1);
                border-color : transparent;
                
                
                color: #ffffff;
                font-size: 12px;
                font-weight: 400;  
                font-family: 'Lato', sans-serif;
                letter-spacing: 2px;
                line-height: 1.3;
                padding: 18px 24px;
                text-transform: uppercase;
            }

            #minimalist_theme .theme-btn.style3 p {
                color: #ffffff;
                font-family: 'Lato', sans-serif;
                font-size: 12px;
                font-weight: 400 ;  
                letter-spacing: 2px;
                line-height: 1.3;
                text-transform: uppercase;
            }
        
        /*SweetAlert*/
        #minimalist_theme .swal2-container {
            font-family: 'Playfair Display', serif;
        } 
        
        #minimalist_theme #email-validate-form label { color: rgba(0,0,0,1) !important; }
        
        #minimalist_theme #email-validate-form input {
            border: 1px solid rgba(0,0,0,0.2) !important;
            color: #202020 !important;
        }
        
        #minimalist_theme #email-validate-form input:hover,
        #minimalist_theme #email-validate-form input:focus,
        #minimalist_theme #email-validate-form input:hover {
            border: 1px solid rgba(0,0,0,1) !important;
        }
        
    </style>
</head>
<body class="theme-width-none folio-live-site " data-attr_currpage="page-5448" id="minimalist_theme">
<div id="page-5448">
<header class="folio-section">
<!-- pre-header -->
<!-- header -->
<section class="folio-section section-horizontal section-header" id="section-15588">
<!-- Desktop Styles -->
<!-- End of Desktop Styles -->
<!-- Tablet Styles -->
<!-- End of Tablet Styles -->
<!-- Mobile Styles -->
<!-- End of Mobile Styles --> <div class="section-layout-wrapper s-content-text-white s-title-text-white s-link-text-white dark-background container-fluid-1200 logo-text-white" style="padding-bottom:0px;padding-top:0px">
<div class="section-layout layout-header layout-header_1 d-flex vertical-align layout-more-header content-fixed">
<div class="clearfix d-flex vertical-align full-width ">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
<div class="pos-relative clearfix">
<div class="website-logo d-flex no-max-height ">
<a class="full-height" href="/" style="width:72%;">
<img alt="Eric Copeman Imaging
" class="img-responsive" src="https://assets.foliowebsites.com/f76a89f0cb91bc419542ce9fa43902dc/web-643/5a3ab378c05eb27b.png"/>
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-xs-3 text-right visible-sm visible-xs no-pad-right">
<span class="icon lg-icon icon-header-3 toggle-nav menu menu-icon dgray-hover"></span>
</div>
<div class="col-lg-6 col-md-6 hidden-sm hidden-xs no-pad-right">
<div class="d-flex full-width header-element">
<nav class="menu-element full-width" id="header-menu-wrap">
<ul class="no-margin menu-horizontal text-right custom-scrollbar-2">
<li class="pos-relative menu-items">
<a class="active fw-regular" href="/">Home - 2020</a>
</li>
<li class="pos-relative menu-items">
<a class=" fw-regular">Portfolios</a>
<span class="icon xs-icon f-icon-s_add_xs sub-menu-more-indicator"></span>
<div class="sub-menu scrollbar">
<ul class="nav nav-pills nav-stacked inner-menu-level custom-scrollbar-2">
<li>
<div class="row no-margin">
<a class="fw-regular text-center" href="/landscape-1">Landscape</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="fw-regular text-center" href="/cloudscape-1">Atmospheric</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="fw-regular text-center" href="/cloudscape-2">Cloud Clusters</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="fw-regular text-center" href="/stilllife">Still Life</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="fw-regular text-center" href="/people">People +</a>
</div>
</li>
</ul>
</div>
</li>
<li class="pos-relative menu-items">
<a class=" fw-regular" href="/contact">Contact</a>
</li>
<li class="pos-relative more-menu hidden">
<a class="fw-regular" href="">More</a>
<div class="sub-menu-more add-top-15 custom-scrollbar-2">
<ul class="no-margin menu-horizontal more-menu-horizontal custom-scrollbar-2 text-right">
</ul>
</div>
</li>
</ul>
</nav>
</div>
</div>
<nav class="nav-wrapper pad-top-5 pad-bottom-5 menu-popup menu-popup-s2 pos-fixed full-width full-height hidden-md hidden-lg scrollbar">
<div class=" clearfix menu text-right clickable pad-right-10 pad-left-3">
<span class="icon lg-icon f-icon-s_close_xs"></span>
</div>
<div class="container-fluid pos-relative menu-overflow">
<ul class="add-top-3">
<li>
<div class="d-flex flex-content-center vertical-align">
<a class="active" href="/">Home - 2020</a>
</div>
</li>
<li class="show-submenu">
<div class="d-flex flex-content-center vertical-align">
<a>Portfolios</a>
<span class="icon xs-icon f-icon-s_add_s dgray-hover add-left show-s-menu submenu-icon"></span>
</div>
<div class="sub-menu">
<ul class="nav nav-pills nav-stacked inner-menu-level">
<li>
<div class="row no-margin">
<a class="" href="/landscape-1">Landscape</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="" href="/cloudscape-1">Atmospheric</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="" href="/cloudscape-2">Cloud Clusters</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="" href="/stilllife">Still Life</a>
</div>
</li>
<li>
<div class="row no-margin">
<a class="" href="/people">People +</a>
</div>
</li>
</ul>
</div>
</li>
<li>
<div class="d-flex flex-content-center vertical-align">
<a class="" href="/contact">Contact</a>
</div>
</li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</section>
</header>
<section class="folio-section section-horizontal main-section main-body">
<section class="folio-section section-horizontal section_featured section-slider" id="section-321566">
<!-- Desktop Styles -->
<!-- End of Desktop Styles -->
<!-- Tablet Styles -->
<!-- End of Tablet Styles -->
<!-- Mobile Styles -->
<!-- End of Mobile Styles --> <div class="section-layout-wrapper clearfix s-content-text-white s-title-text-white s-link-text-white dark-background container-fluid container-fluid" style="padding-bottom:0px;padding-top:0px">
<div class="section-layout layout-featured layout-featured_4 content-full slider-photo">
<div class="folio-slider slick one-photo no-title">
<ul class="slides list-unstyled horizontal-thumbnails">
<li class="mySlides img_1">
<div class="full-height full-width d-flex vertical-align flex-content-center">
<a class="no-rightclick img-fancy full-height d-flex vertical-align" data-caption="" data-fancybox="image-5f947" data-src="https://assets.foliowebsites.com/f76a89f0cb91bc419542ce9fa43902dc/web-643/e22ba52999debadf.jpeg" href="javascript:;" rel="slider-5f947">
<img alt="180-PINECONES-960x640H-.jpg" src="https://assets.foliowebsites.com/f76a89f0cb91bc419542ce9fa43902dc/web-643/e22ba52999debadf.jpeg"/></a>
</div>
<div class="featured-textwrap hidden">
</div>
</li>
</ul>
</div>
</div>
</div>
</section>
</section>
<footer class="folio-section">
<!-- pre-footer -->
<!-- footer -->
<section class="folio-section section-horizontal section-footer" id="section-15590">
<!-- Desktop Styles -->
<!-- End of Desktop Styles -->
<!-- Tablet Styles -->
<!-- End of Tablet Styles -->
<!-- Mobile Styles -->
<!-- End of Mobile Styles --> <div class="section-layout-wrapper s-content-text-white s-title-text-white s-link-text-white dark-background container-fluid-1200 logo-text-white" style="padding-bottom:0px; padding-top:0px">
<div class="section-layout layout-footer layout-footer_2 d-flex vertical-align content-fixed">
<div class="clearfix full-width">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-div d-flex vertical-align d-flex vertical-align flex-content-start text-left">
<p class="fs-11 text-uppercase regular letterspacing-2em copyright-title">
                                    © 2021 ERIC COPEMAN IMAGING 
                                                                            / <a class="fs-11 text-uppercase fw-regular letterspacing-2em"></a>
</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-div text-right d-flex vertical-align flex-content-end">
<div class="clearfix full-width">
<nav class="footer-menu full-width">
<ul class="social-media-menu text-right">
<li>
<a class="no-margin icon sm-icon f-icon-facebook_m" href="https://facebook.com/eric.copeman" rel="nofollow" target="_blank">
</a>
</li>
<li>
<a class="no-margin icon sm-icon f-icon-linkedin_m" href="https://linkedin.com/in/profile/viewid61163474trknav_responsive_tab_profile" rel="nofollow" target="_blank">
</a>
</li>
<li>
<a class="no-margin icon sm-icon f-icon-twitter_m" href="https://twitter.com/Eric_Copeman" rel="nofollow" target="_blank">
</a>
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</section>
</footer>
<!-- gallery style 1 underscore template -->
<script id="gallery-tpl-style-1" type="text/template">
                    <% for(var key in photos) { %>
                        <div class="photo-item">
                            <% if(gallery_type == 'photo') { %>
                                <a  class="no-rightclick" href="javascript:;" data-src="<%- photos[key].url %>" data-fancybox="image-<%- element_id %>" rel="photogrid-group">
                            <% } else { %>
                                <a href="<%- photos[key].url %>">
                            <% } %>

                            <div class="photo-info d-flex vertical-align flex-content-center">
                                <% if(show_title || show_desc || show_button) { %>
                                    <div class="clearfix">
                                        <% if(show_title) { %>
                                        <h6 class="photo-title bold uppercase t-white">
                                            <% if(photos[key].title) { %>
                                            <%- photos[key].title.truncPlain(20) %>
                                            <% } %>
                                        </h6>
                                        <% } %>

                                        <% if(show_desc) { %>
                                        <p class="photo-description f_md add-bottom-25">
                                            <% if(photos[key].description) { %>
                                            <%- photos[key].description.truncPlain(50) %>
                                            <% } %>
                                        </p>

    									<% } %>

    									<% if(show_button) { %>
                                        <button class="theme-btn style1 add-top-2">View <%- gallery_type %></button>
                                    	<% } %>
    								</div>

                                <% } %>
                            </div>
                            <img src="<%- photos[key].photo_url %>" alt="<%- photos[key].alt_text %>" />
                            </a>
                        </div>
                    <% } %>
                    </script>
<!-- gallery style 2 underscore template -->
<script id="gallery-tpl-style-2" type="text/template">
                    <% for(var key in photos) { %>
                        <% if(show_title || show_desc || show_button) { %>
                            <%
                            var elemClass = '';
                            if(!show_title) {
                            elemClass += ' no-title';
                            }
                            if(!show_desc) {
                            elemClass += ' no-desc';
                            }
                            if(!show_button) {
                            elemClass += ' no-button';
                            }
                        %>
                            <div class="photo-item <%- elemClass %>">
                                <% if(gallery_type == 'photo') { %>
                                    <a  class="no-rightclick" href="javascript:;" data-src="<%- photos[key].url %>" data-fancybox="image-<%- element_id %>" rel="photogrid-group">
                                <% } else { %>
                                    <a href="<%- photos[key].url %>">
                                <% } %>

                                <div class="img-wrap " style="background-image: url('<%- photos[key].photo_url %>');"></div>

                                <% if(show_title || show_desc) { %>
                                    <div class="photo-info add-bottom-2">
                                        <% if(show_title) { %>
                                        <h6 class="photo-title uppercase">
                                            <% if(photos[key].title) { %>
                                                <%- photos[key].title %>
                                            <% } %>
                                        </h6>
                                        <% } %>

                                        <% if(show_desc) { %>
                                        <p class="photo-description f_md">
                                            <% if(photos[key].description) { %>
                                                <%- photos[key].description %>
                                            <% } %>
                                        </p>
                                        <% } %>
                                    </div>
                                <% } %>
                                </div>
                                </a>
                            </div>
                         <% } %>
                    <% } %>
                    </script>
<!-- gallery style 3 underscore template -->
<script id="gallery-tpl-style-3" type="text/template">
                    <% for(var key in photos) { %>
                        <div class="photo-item">
                            <% if(gallery_type == 'photo') { %>
                                <a  class="no-rightclick" href="javascript:;" data-src="<%- photos[key].url %>" data-fancybox="image-<%- element_id %>" rel="photogrid-group">
                            <% } else { %>
                                <a href="<%- photos[key].url %>">
                            <% } %>

                            <div class="img-wrap">
                                <img src="<%- photos[key].photo_url %>" alt="<%- photos[key].alt_text %>" />
                            </div>

                            <% if(show_title || show_desc || show_button) { %>
                                <div class="photo-info d-flex vertical-align flex-content-center">
                                    <div class="clearfix">
                                    <% if(show_title) { %>
                                        <h6 class="photo-title bold uppercase t-white">
                                            <% if(photos[key].title) { %>
                                                <%- photos[key].title.truncPlain(20) %>
                                            <% } %>
                                        </h6>
                                    <% } %>

                                    <% if(show_desc) { %>
                                        <p class="photo-description f_md add-bottom-25">
                                            <% if(photos[key].description) { %>
                                                <%- photos[key].description.truncPlain(20) %>
                                            <% } %>
                                        </p>
                                    <% } %>

                                    <% if(show_button) { %>
                                        <button class="theme-btn style1 add-top-2">View <%- gallery_type %></button>
                                    <% } %>
                                    </div>
                                </div>
                            <% } %>
                            </a>
                        </div>
                    <% } %>
                    </script>
<div class="clearfix d-flex vertical-align flex-content-center email-loader">
<div class="spinner lg-spinner blue"></div>
</div>
<div class="md-overlay"></div>
</div>
<script id="jqueryMain" src="/public/js/vendor/jquery-1.11.3.min.js"></script>
<script src="/public/js/config/requireConf.js"></script>
<script src="/public/js/vendor/require/require.js"></script>
<script src="/public/js/themes/minimalist_theme/config.js"></script>
<script>
window.map_api_key = 'AIzaSyCOD3KHGTOH4W2Ff1HA7Db68oqKY9VkGlQ';
require(['commonV2'], function(commonV2) { require(['main']); });
</script>
<!-- Custom Footer Scripts -->
<!-- Custom Page Footer Scripts -->
<!-- End of Custom Page Footer Scripts -->
</body>
</html>
