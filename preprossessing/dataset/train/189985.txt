<!DOCTYPE html>
<html lang="pl">
<head>
<!--[if gte IE 8]>
    <meta http-equiv='X-UA-Compatible' content='IE=9' />
    <![endif]-->
<meta charset="utf-8"/>
<meta content="width = device-width, initial-scale = 1.0, maximum-scale=1.0" name="viewport"/>
<title>BPS | Strona główna</title>
<link href="https://www.bankbps.pl/__data/assets/image/0010/1243/favicon.png?v=0.0.2" rel="shortcut icon" type="image/png"/>
<link href="https://www.bankbps.pl/__data/assets/file/0009/1242/favicon.ico?v=0.0.3" rel="icon" type="image/x-icon"/>
<!-- Metadata -->
<!-- Page Config //-->
<!-- Basic SEO //-->
<meta content="Strona główna" name="Description"/>
<meta content="Bank BPS, Bank BPS SA, Grupa BPS, Banki Spółdzielcze, Banki Polskiej Spółdzielczości, Zrzeszenie BPS, BPS" name="Keywords"/>
<meta content="Bank BPS" name="Author"/>
<meta content="index, follow" name="robots"/>
<!-- Popup Config //-->
<meta content="" name="show.popup"/>
<meta content="" name="content.popup"/>
<!-- /Metadata -->
<!-- CSS -->
<link href="https://www.bankbps.pl/__data/assets/file/0008/107999/style.css?v=0.2.61" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bankbps.pl/__data/assets/file/0017/1466/print.css?v=0.1.16" media="print" rel="stylesheet" type="text/css"/>
<link href="https://www.bankbps.pl/_global_designs/css/custom.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lte IE 8]>
    															<link rel="stylesheet" type="text/css" href="https://www.bankbps.pl/__data/assets/file/0011/1271/ie.css?v=0.1.10" media="all" />
											    <![endif]-->
<!--[if lte IE 6]>
    			<link rel="stylesheet" type="text/css" href="https://www.bankbps.pl/__data/assets/file/0016/120841/ie6.css?v=0.1.4" media="all" />
					    <![endif]-->
<link href="https://www.bankbps.pl/__data/assets/file/0009/107991/thirdparty.css?v=0.2.2" media="all" rel="stylesheet" type="text/css"/>
<!-- /CSS -->
<!-- Header JS -->
<!--[if lt IE 9]><script src="https://www.bankbps.pl/__data/assets/js_file/0015/1275/html5shiv.js?v=0.1.1"></script><![endif]--><!-- html5Shiv -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0014/1274/jquery-1.9.1.min.js?v=0.1.1" type="text/javascript"></script> <!-- jQuery -->
<!--<script src="https://www.bankbps.pl/__data/assets/js_file/0020/8246/main.js?v=0.1.39" type="text/javascript"></script>--> <!-- calculator.js -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0015/100950/kalkulatory.js?v=0.1.130" type="text/javascript"></script> <!-- kalkulatory.js -->
<script type="text/javascript">
var settings = {
        site_URL: 'https://www.bankbps.pl',
        site_ID: '77',
        page_URL: 'https://www.bankbps.pl/strona-glowna',
        page_ID: '78',
        update_departments_in_select_by_city_URL: 'https://www.bankbps.pl/_resources/rests/search',
        update_departments_in_select_by_state_URL: 'https://www.bankbps.pl/_resources/rests/search_by_state/_nocache',
        bps_currency_rates_URL: 'https://www.bankbps.pl/_services/solr-kursy-walut/bps-current-exchange-rate-62417',
        box_news_from_bps: '',
        box_news_URL: 'https://www.bankbps.pl/_resources/listings/news-list',
        group_page: 'https://www.bankbps.pl/o-grupie-bps'
    };
</script> <!-- /Header JS -->
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-11694409-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
<meta content="63678543ebff98825158b050c7b4fee7" name="certum-domain-verification"/>
<meta content="7571A4xoHQwT-NljBBA3JqmD7gir3n2LhEnrvoEAEDs" name="google-site-verification"/>
<!--
  Running Squiz Matrix
  Developed by Squiz - http://www.squiz.net
  Squiz, Squiz Matrix, MySource, MySource Matrix and Squiz.net are registered Trademarks of Squiz Pty Ltd
  Page generated: 13 January 2021 15:59:54
-->
</head>
<body class="home green_buttons">
<!--noindex-->
<div id="wrapper">
<!-- Header start -->
<div id="header">
<!-- Top links start -->
<div id="top_line">
<div id="ie_warning"><span>Szanowni Państwo, uprzejmie informujemy że w związku z ryzykiem nieprawidłowego wyświetlania się strony Banku BPS w przeglądarce Internet Explorer wersja 8 polecamy wyłączenie trybu zgodności lub skorzystanie z innej przeglądarki internetowej, np. Mozilla Firefox.<br/>Wyłączenie trybu zgodności można wykonać poprzez wciśnięcie klawisza F12 i zmianę na opcję bez „ - widok zgodności”.<br/>Przepraszamy za zaistniałe trudności. Pracujemy nad optymalizacją tego zagadnienia.</span><a href="#" id="ie_warning_off">[Zamknij]</a></div> <ul id="options">
<li class="flag_PL current">
<a href="https://www.bankbps.pl" title="Polska wersja językowa"><span class="hide">Polska wersja językowa</span></a>
</li>
<li class="flag_EN">
<a href="https://www.bankbps.pl/english" title="English language version"><span class="hide">English language version</span></a>
</li>
<!--<li class="flag_DE">
    <a href="" title="Deutsch Sprache Version"><span class="hide">Deutsch Sprache Version</span></a>
  </li> -->
<li class="font_minus">
<a href="#font_minus" id="font_minus" title="Zmniejsz rozmiar czcionki"><span class="hide">Zmniejsz rozmiar czcionki</span></a>
</li>
<li class="font_plus">
<a href="#font_plus" id="font_plus" title="Zwiększ rozmiar czcionki"><span class="hide">Zwiększ rozmiar czcionki</span></a>
</li>
<li class="highcontrast">
<a href="#highcontrast_switch" id="highcontrast_switch" title="Wersja kontrastowa"><span class="hide">Wersja kontrastowa</span></a>
</li>
</ul>
<!-- Pole 1 default-->
<!-- /Pole 1 default-->
<!-- Pole 1 -->
<div class="left shadow0" id="top_links_left"><a class="first" href="https://www.bankbps.pl/polityka-prywatnosci" title="Polityka prywatności">Polityka prywatności</a><a href="https://www.bankbps.pl/rodo" title="RODO">RODO</a><a href="https://www.bankbps.pl/kontakt" title="Kontakt">Kontakt</a><a href="https://www.bankbps.pl/mapa-serwisu" title="Mapa serwisu">Mapa serwisu</a></div><div class="right" id="search">
<ul id="options_right">
<li class="flag_PL current">
<a href="https://www.bankbps.pl" title="Polska wersja językowa"><span class="hide">Polska wersja językowa</span></a>
</li>
<li class="flag_EN">
<a href="https://www.bankbps.pl/english" title="English language version"><span class="hide">English language version</span></a>
</li>
<!--<li class="flag_DE">
    <a href="" title="Deutsch Sprache Version"><span class="hide">Deutsch Sprache Version</span></a>
  </li> -->
</ul>
<form action="https://www.bankbps.pl/szukaj" method="GET">
<input name="mode" type="hidden" value="results"/>
<input class="shadow1inset" name="queries_query_query" placeholder="szukana fraza..." title="szukana fraza..." type="text" value=""/>
<button class="button-search" title="Szukaj" type="submit"><span class="hide">Szukaj</span></button>
</form>
</div> <!-- /Pole 1 -->
<div class="clear"><!-- --></div>
</div><!-- Top links end -->
<!-- Logos start -->
<div id="logos">
<h1 class="left">
<!-- Pole 3 default-->
<!-- /Pole 3 default-->
<!-- Pole 3 -->
<a href="https://www.bankbps.pl" title="Bank BPS"><img alt="Bank BPS logo" src="https://www.bankbps.pl/__data/assets/image/0015/1482/bps_logo.png"/><span class="hide">Bank BPS</span></a> <!-- /Pole 3 -->
</h1>
<div id="login">
<!-- Pole 2 default-->
<!-- /Pole 2 default-->
<!-- Pole 2 -->
<a class="shadow1 button_green" id="login_button" title="Logowanie">Zaloguj się</a>
<ul class="shadow1 hide" id="login_options">
<li class="close_x"><a class="close_x">X</a></li>
<li>
<a class="shadow1" href="https://www.e25.pl/" rel="" title="">e25</a>
</li>
</ul>
<div class="hide" id="cover"></div> <!-- /Pole 2 -->
</div>
<!-- EKOBANK -->
<a href="https://mojbank.pl/nasze-spolecznosci/koronawirus-wsparcie-dla-klientow" id="eko">
<div class="shadow1" id="eko_button">
                    KORONAWIRUS SERWIS WWW
                </div>
</a>
<!-- /EKOBANK -->
<div class="clear">
<!-- -->
</div>
</div><!-- Logos end -->
<!-- Menu start -->
<div id="menu">
<!-- Pole 4 default-->
<!-- /Pole 4 default-->
<!-- Pole 4 -->
<ul id="main_menu"><li><a class="" href="https://www.bankbps.pl/o-banku" target="_self" title="O banku">O banku</a></li><li><a class="" href="https://www.bankbps.pl/o-grupie-bps" target="_self" title="O Grupie">O Grupie</a></li><li><a class="" href="http://www.talentowisko.pl" target="_blank" title="Talentowisko">Talentowisko</a></li><li><a class="" href="https://www.bankbps.pl/placowki-i-bankomaty" target="_self" title="Placówki i bankomaty">Placówki i bankomaty</a></li><li><a class="" href="https://www.bankbps.pl/psd2" target="_self" title="PSD2">PSD2</a></li><li><a class="" href="https://www.bpsleasing.pl/oferta/zapytanie-o-oferte" target="_self" title="Zapytaj o Leasing">Zapytaj o Leasing</a></li><li><a class="" href="https://www.bankbps.pl/informacje-dla-klientow-w-zw.-z-epidemia" target="_self" title="Koronawirus - informacje">Koronawirus - informacje</a></li></ul> <!-- /Pole 4 -->
<!-- Pole 5 default-->
<!-- /Pole 5 default-->
<!-- Pole 5 -->
<!-- /Pole 5 -->
<!-- Pole 6 default-->
<!-- /Pole 6 default-->
<!-- Pole 6 -->
<ul class="box_6 boxes_4" id="submenu"><li>
<a class="button shadow0 only_text" href="https://www.bankbps.pl/klienci-indywidualni" title="Klienci indywidualni">
                    Klienci indywidualni
            </a>
</li><li>
<a class="button shadow0 only_text" href="https://www.bankbps.pl/mikrofirmy" title="Mikrofirmy">
                    Mikrofirmy
            </a>
</li><li>
<a class="button shadow0 only_text" href="https://www.bankbps.pl/msp" title="Małe i Średnie Przedsiębiorstwa">
                    Małe i Średnie Przedsiębiorstwa
            </a>
</li><li>
<a class="button shadow0 only_text" href="https://www.bankbps.pl/rolnicy" title="Rolnicy">
                    Rolnicy
            </a>
</li></ul> <!-- /Pole 6 -->
<!-- Pole 6b default-->
<!-- /Pole 6b default-->
<!-- Pole 6b -->
<!-- /Pole 6b -->
<div class="clear"><!-- --></div>
</div><!-- Menu end -->
<div class="clear"><!-- --></div>
</div><!-- Header end -->
<!--endnoindex-->
<!-- Content start -->
<div id="content">
<!-- Pole 7 default-->
<!-- /Pole 7 default-->
<!--noindex-->
<!-- Pole 7 -->
<div class="box_4" id="slider">
<div class="shadow2" id="slider_img"><div><a data-time="3" href="https://www.bankbps.pl/klienci-indywidualni/kredyty/kredyty-gotowkowe/kredyt-ekologiczny" rel="" title="Kredyt ekologiczny"><img alt="image" src="https://www.bankbps.pl/__data/assets/image/0008/382571/BPS_2020_12_Eko_Prad_banner_677x277_new20201222_5.png"/></a>
<p class="sliderTitle">BPS_2020_12_Eko_Prad_banner_677x277_new20201222_5.png</p></div><div><a data-time="3" href="https://www.bankbps.pl/klienci-indywidualni/kredyty/kredyty-hipoteczne/kredyt-mieszkaniowy-moj-dom" rel="" title="Promocja kredytu"><img alt="KH3" src="https://www.bankbps.pl/__data/assets/image/0004/382909/BPS_Kredyt-Hipoteczny-Intensywnie-Zielony_baner_667x277_mc.png"/></a>
<p class="sliderTitle">BPS_Kredyt Hipoteczny Intensywnie Zielony_baner_667x277_mc.png</p></div><div><a data-time="3" href="https://www.bankbps.pl/o-banku/aktualnosci/gorace-lato-z-ubezpieczeniami-komunikacyjnymi." rel="" title="Ubezpieczenia komunikacyjne"><img alt="image" src="https://www.bankbps.pl/__data/assets/image/0006/378888/BPS_20-08-27__ubezpieczenia-komunikacyjne__3-667x277.jpg"/></a>
<p class="sliderTitle">BPS_20-08-27__ubezpieczenia-komunikacyjne__3-667x277.jpg</p></div></div>
<div id="slider_ctrl"></div>
</div> <!-- /Pole 7 -->
<!-- Pole 8 show_menu_by_default-->
<!-- /Pole 8 -->
<!-- Pole 8 is_menu-->
<!-- /Pole 8 -->
<!-- Pole 8 default-->
<!-- /Pole 8 default-->
<!-- Side menu start -->
<!-- Side menu end -->
<!-- Pole 8 -->
<div class="box_2a" id="bps_map"><a class="shadow3" href="https://www.bankbps.pl/o-grupie-bps" target="_self" title="O Grupie"><img alt="" src="https://www.bankbps.pl/__data/assets/image/0006/382605/Mapka-325-31.12.2020.jpg" title="Mapka-325-31.12.2020.jpg"/></a></div> <!-- /Pole 8 -->
<div class="clear"><!-- --></div>
<!-- Bottom info start -->
<div id="bottom_line">
<!-- Pole 9 default-->
<!-- /Pole 9 default-->
<!-- Pole 9 -->
<div class="boxes_3">
<!-- News start -->
<div class="shadow0 left box_3">
<div class="sub">
<div class="news-header">
<h3>Aktualności</h3>
<a class="rss-link" href="/rss" title="RSS">Aktualności RSS</a>
<div class="clear"></div>
</div>
<div id="box-news-container"></div>
<a class="all_news" href="https://www.bankbps.pl/o-banku/aktualnosci" title="Zobacz wszystkie aktualności">zobacz wszystkie</a>
<div class="clear"><!-- --></div>
</div>
</div>
<div class="shadow0 right box_2b"><div class="sub exhange currency kursy_bps">
<h3>Kursy walut</h3>
<table class="solr"><thead><tr><th>Kursy walut</th><th class="bps_currency_date to_right">2021-01-13 obowiązuje od godziny: 14:00</th></tr></thead><tbody><tr><td class="ex_ta_left">1 EUR</td><td class="ta_right">4.3908</td></tr><tr><td class="ex_ta_left">1 USD</td><td class="ta_right">3.6068</td></tr><tr><td class="ex_ta_left">1 CHF</td><td class="ta_right">4.0344</td></tr><tr><td class="ex_ta_left">1 GBP</td><td class="ta_right">4.9173</td></tr></tbody>
<tfoot><tr><td class="ta_right" colspan="2"><a href="https://www.bankbps.pl/kurs-walut?type=bps" title="Zobacz więcej kursów">więcej</a></td></tr></tfoot></table></div></div>
<!-- BPS -->
<div class="shadow0 right box_2b">
<div class="sub exhange"><h3>Stawki referencyjne</h3>
<table>
<tbody>
<tr>
<td>WIBOR 1M - średnia</td>
<td>0,20</td>
</tr>
<tr>
<td>WIBOR 3M - średnia</td>
<td>0,21</td>
</tr>
</tbody>
<tfoot>
<tr>
<td class="ta_right" colspan="2"><a href="https://www.bankbps.pl/stawki-referencyjne" title="Zobacz więcej stawek">więcej</a></td>
</tr>
</tfoot>
</table>
</div>
</div>
</div> <!-- /Pole 9 -->
<div class="clear"><!-- --></div>
</div><!-- Bottom info end -->
</div><!-- Content end -->
<!-- Footer start -->
<div id="footer">
<!-- Footer menu start -->
<!-- Pole 10 default-->
<!-- /Pole 10 default-->
<!-- Pole 10 -->
<div class="boxes_5"><div class="box_1 shadow0 left">
<ul class="sub">
<li>
<a href="https://www.bankbps.pl/bankowosc-internetowa" rel="" title="Bankowość internetowa">
    Bankowość internetowa
  </a>
</li><li>
<a href="https://www.bankbps.pl/serwis-transakcyjny/e25/zasady-bezpiecznego-korzystania-z-systemu" rel="" title="Zasady bezpiecznego korzystania z bankowości internetowej ">
    Zasady bezpiecznego korzystania z bankowości internetowej 
  </a>
</li><li>
<a href="https://www.bankbps.pl/platnosci" rel="" title="Płatności">
    Płatności
  </a>
</li><li>
<a href="https://www.bankbps.pl/godziny-graniczne-realizacji-przelewow" rel="" title="Godziny graniczne realizacji przelewów">
    Godziny graniczne realizacji przelewów
  </a>
</li><li>
<a href="https://www.bankbps.pl/__data/assets/pdf_file/0005/344633/20180809_Lista-korespondentow-Banku-BPS.pdf" rel="external" title="Wykaz banków Korespondentów">
    Wykaz banków Korespondentów
  </a>
</li><li>
<a href="https://www.bankbps.pl/planet-plus" rel="" title="Planet Plus ">
    Planet Plus 
  </a>
</li><li>
<a href="https://www.bankbps.pl/__data/assets/pdf_file/0010/288613/20170816_Zasady-dobrych-praktyk-Banku-BPS.pdf" rel="" title="Zasady Dobrych Praktyk Banku Polskiej Spółdzielczości S.A. w Zakresie świadczenia usług związanych z kredytem hipotecznym">
    Zasady Dobrych Praktyk Banku Polskiej Spółdzielczości S.A. w Zakresie świadczenia usług związanych z kredytem hipotecznym
  </a>
</li>
</ul>
</div><div class="box_1 shadow0 left">
<ul class="sub">
<li>
<a href="https://www.bankbps.pl/regulaminy" rel="" title="Regulaminy">
    Regulaminy
  </a>
</li><li>
<a href="https://www.bankbps.pl/oprocentowanie" rel="" title="Oprocentowanie">
    Oprocentowanie
  </a>
</li><li>
<a href="https://www.bankbps.pl/oplaty-i-prowizje" rel="" title="Opłaty i prowizje">
    Opłaty i prowizje
  </a>
</li><li>
<a href="https://www.bankbps.pl/oplata-interchange" rel="" title="Opłata interchange">
    Opłata interchange
  </a>
</li><li>
<a href="http://www.visa.pl/o-nas/" rel="" title="Organizacja płatnicza Visa">
    Organizacja płatnicza Visa
  </a>
</li><li>
<a href="http://www.mastercard.pl/o-mastercard.html" rel="" title="Organizacja płatnicza MasterCard">
    Organizacja płatnicza MasterCard
  </a>
</li>
</ul>
</div><div class="box_1 shadow0 left">
<ul class="sub">
<li>
<a href="https://www.bankbps.pl/bfg" rel="" title="BFG">
    BFG
  </a>
</li><li>
<a href="https://www.bankbps.pl/mifid" rel="" title="MiFID">
    MiFID
  </a>
</li><li>
<a href="https://www.bankbps.pl/fatca" rel="" title="FATCA">
    FATCA
  </a>
</li><li>
<a href="https://www.bankbps.pl/euro-fatca" rel="" title="EURO-FATCA">
    EURO-FATCA
  </a>
</li><li>
<a href="https://www.bankbps.pl/biuro-informacji-kredytowej" rel="" title="Biuro Informacji Kredytowej">
    Biuro Informacji Kredytowej
  </a>
</li><li>
<a href="https://www.bankbps.pl/zastrzeganie-dokumentow-tozsamosci" rel="" title="Zastrzeganie dokumentów tożsamości">
    Zastrzeganie dokumentów tożsamości
  </a>
</li><li>
<a href="https://www.bankbps.pl/informacja-o-trybie-zglaszania-reklamacji" rel="" title="Informacja o trybie zgłaszania reklamacji">
    Informacja o trybie zgłaszania reklamacji
  </a>
</li><li>
<a href="https://www.bankbps.pl/o-grupie-bps/system-ochrony-zrzeszenia-bps" rel="" title="System Ochrony Zrzeszenia BPS">
    System Ochrony Zrzeszenia BPS
  </a>
</li><li>
<a href="https://www.bankbps.pl/psd2" rel="" title="PSD2">
    PSD2
  </a>
</li><li>
<a href="https://www.bankbps.pl/soc" rel="" title="SOC">
    SOC
  </a>
</li>
</ul>
</div><div class="box_1 shadow0 left">
<ul class="sub">
<li>
<a href="https://www.bankbps.pl/o-banku" rel="" title="O Banku">
    O Banku
  </a>
</li><li>
<a href="https://www.bankbps.pl/kariera" rel="" title="Kariera">
    Kariera
  </a>
</li><li>
<a href="https://www.bankbps.pl/informacja-makroekonomiczna" rel="" title="Informacja makroekonomiczna">
    Informacja makroekonomiczna
  </a>
</li><li>
<a href="https://bankbps.prowly.com/" rel="external" title="Biuro prasowe">
    Biuro prasowe
  </a>
</li><li>
<a href="https://www.bankbps.pl/informacja-dla-akcjonariuszy" rel="" title="Informacje dla akcjonariuszy">
    Informacje dla akcjonariuszy
  </a>
</li><li>
<a href="https://www.bankbps.pl/__data/assets/pdf_file/0007/370915/22.07.2020-Informacja-o-przedsiebiorcach.pdf" rel="external" title="Informacja o przedsiębiorcach">
    Informacja o przedsiębiorcach
  </a>
</li><li>
<a href="https://www.bankbps.pl/pozostale-informacje" rel="" title="Pozostałe informacje">
    Pozostałe informacje
  </a>
</li><li>
<a href="https://www.bankbps.pl/nieruchomosci-i-ruchomosci-na-sprzedaz" rel="" title="Nieruchomości i ruchomości na sprzedaż">
    Nieruchomości i ruchomości na sprzedaż
  </a>
</li><li>
<a href="https://www.bankbps.pl/lad-korporacyjny" rel="" title="Ład Korporacyjny">
    Ład Korporacyjny
  </a>
</li><li>
<a href="https://www.bankbps.pl/art.-111-prawo-bankowe" rel="" title="Art. 111 Prawa bankowego">
    Art. 111 Prawa bankowego
  </a>
</li><li>
<a href="https://www.bankbps.pl/__data/assets/pdf_file/0005/381434/Polityka-infomacyjna-Banku-BPS-SA-w-zakresie-kontaktow-z-inwestorami_-klientami-i-mediami.pdf" rel="" title="Polityka informacyjna">
    Polityka informacyjna
  </a>
</li><li>
<a href="https://www.bankbps.pl/__data/assets/pdf_file/0003/303627/Zasady-Systemu-Kontroli-Wewnetrznej-w-Banku-BPS-SA.pdf" rel="" title="Zasady systemu kontroli wewnętrznej">
    Zasady systemu kontroli wewnętrznej
  </a>
</li>
</ul>
</div><div class="box_1 shadow0 left">
<ul class="sub">
<li>
<a href="http://www.bpsfaktor.pl" rel="" title="BPS Faktor">
    BPS Faktor
  </a>
</li><li>
<a href="http://www.bpsnieruchomosci.pl/" rel="external" title="BPS Nieruchomości">
    BPS Nieruchomości
  </a>
</li><li>
<a href="http://www.bpsleasing.pl/" rel="external" title="BPS Leasing">
    BPS Leasing
  </a>
</li><li>
<a href="http://www.bpstfi.pl/" rel="external" title="BPS TFI">
    BPS TFI
  </a>
</li><li>
<a href="http://dmbps.pl/" rel="external" title="Dom Maklerski Banku BPS">
    Dom Maklerski Banku BPS
  </a>
</li><li>
<a href="http://it-bps.pl/" rel="external" title="CRUZ">
    CRUZ
  </a>
</li><li>
<a href="http://frbs.org.pl/" rel="external" title="Fundacja Rozwoju Bankowości Spółdzielczej">
    Fundacja Rozwoju Bankowości Spółdzielczej
  </a>
</li>
</ul>
</div></div> <!-- /Pole 10 -->
<!-- Footer menu end -->
<div class="clear">
<!-- -->
</div>
<!-- Pole 11 default-->
<!-- /Pole 11 default-->
<!-- Pole 11 -->
<!-- Partners start -->
<div class="shadow0 box_6" id="partners">
<ul class="sub">
<li><a href="http://kir.com.pl" rel="external" title="Krajowa Izba Rozliczeniowa"><img alt="Krajowa Izba Rozliczeniowa" src="https://www.bankbps.pl/__data/assets/image/0018/1818/logo_KIR_RBG.jpg"/></a></li><li><a href="http://bfg.pl" rel="external" title="Bankowy Fundusz Gwarancyjny"><img alt="Bankowy Fundusz Gwarancyjny" src="https://www.bankbps.pl/__data/assets/image/0018/1827/bfg_logo.png"/></a></li><li><a href="http://zbp.pl" rel="external" title="ZBP"><img alt="ZBP" src="https://www.bankbps.pl/__data/assets/image/0008/108098/Logo_ZBP_01.jpg"/></a></li><li><a href="http://www.kzbs.pl" rel="" title="KZBS"><img alt="KZBS" src="https://www.bankbps.pl/__data/assets/image/0008/238895/logo_kzbs_napis.jpg"/></a></li><li><a href="https://www.bankbps.pl/o-grupie-bps/system-ochrony-zrzeszenia-bps" rel="" title="SOZ BPS"><img alt="SOZ BPS" src="https://www.bankbps.pl/__data/assets/image/0011/273449/SOZ-BPS.png"/></a></li>
</ul>
</div>
<!-- Partners end --> <!-- /Pole 11 -->
<!-- Pole 12 default-->
<!-- /Pole 12 default-->
<!-- Pole 12 -->
<div class="box_6 shadow0 wrapper" id="footer_line"><ul class="left" id="social"><li><a class="youtube external" href="https://www.youtube.com/channel/UCxisDMFlWlGtZWgoFgkZgGg" rel="external" title="YouTube"><span class="hide">YouTube</span></a></li><li><a class="facebook external" href="https://www.facebook.com/JestesUSiebieBPS" rel="external" title="Facebook"><span class="hide">Facebook</span></a></li><li><a class="linkedin " href="https://pl.linkedin.com/company/bank-bps-sa" rel="external" title="Linkedin"><span class="hide">Linkedin</span></a></li></ul><span class="sub left"><a class="footer-img" href="https://www.bankbps.pl/o-grupie-bps" rel="" title="Grupa BPS">
<img alt="Grupa BPS" height="20" src="https://www.bankbps.pl/__data/assets/image/0009/63693/logo_small.png" title="logo_small" width="80"/>
</a>
<a class="footer-line" href="https://www.bankbps.pl/kontakt" rel="" title="Kontakt">
Kontakt
</a>
</span><div class="sub right">Copyright © Grupa BPS | 2021</div></div> <!-- /Pole 12 -->
<!-- Pole popup -->
<div class="hide">
</div>
<!-- /Pole popup -->
</div><!-- Footer end -->
</div><!-- Wrapper end -->
<!-- Cookie info start -->
<div id="cookie_info">
<div class="sub">
<p>Serwis bankbps.pl używa plików cookie. Brak zmian ustawień przeglądarki oznacza zgodę na ich użycie.
<a href="#" id="cookie_switch" title="Zamknij informację o plikach cookie"><span class="hide">Zamknij informację o plikach cookie</span></a></p>
</div>
</div>
<!-- Cookie info end -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0013/108013/thirdparty.js?v=0.2.12" type="text/javascript"></script> <!-- thirdparty.js -->
<link href="https://www.bankbps.pl/__data/assets/css_file/0008/319319/slick.css?v=0.1.1" rel="stylesheet" type="text/css"/>
<script src="https://www.bankbps.pl/__data/assets/js_file/0006/319317/slick.js?v=0.1.1" type="text/javascript"></script> <!-- slickslider.js -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0016/1276/page.js?v=0.4.21" type="text/javascript"></script> <!-- page.js -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0020/190217/chosen.jquery.min.js?v=0.1.1" type="text/javascript"></script> <!-- chosen.js -->
<script src="https://www.bankbps.pl/__data/assets/js_file/0007/264364/Currecy_exchange_archive-solr.js?v=0.1.36" type="text/javascript"></script> <!-- currency_exchange_archive.js -->
<!-- (c) 2008 Gemius SA / gemiusHeatMap(GHM+XY) / http://www.bankbps.pl -->
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var ghmxy_align = 'center';
var ghmxy_type = 'percent';
var ghmxy_identifier = '.ReacWtzvybxWK12Pf2Ny3a4XfXZiyOkFOo3xRdj6xD.Y7';
//--><!]]>
</script>
<script src="//ghmpl.hit.gemius.pl/hmapxy.js" type="text/javascript"></script> <!--endnoindex-->
</body>
</html>
