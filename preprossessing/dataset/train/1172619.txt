<!DOCTYPE html>
<html>
<head>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" rel="stylesheet"/>
<style>
        .errors {
            color: black;
            text-align: center;
        }

        html,
        body {
            height: 100%;
        }
        .tint {
            width: 100vw;
            background-color: #00000040;
            overflow: hidden;
        }

        h1 {
            font-size: 100px
        }
    </style>
</head>
<body>
<div class="tint h-100">
<div class="row h-100 justify-content-center align-items-center">
<div class="errors">
<h1>404</h1>
<p>Page not found</p>
<button class="btn btn-primary ml-auto" onclick="location.href='/'">Return to Home</button>
</div>
</div>
</div>
</body>
</html>