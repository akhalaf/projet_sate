<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link href="http://www.bishop-accountability.org/includes/style.css" rel="stylesheet" type="text/css"/>
<title>Priest Charged with Sex Crimes, by Carolyn Moreau and John Springer, Hartford Courant [Connecticut], February 9, 2000</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
</head>
<body>
<table align="center" border="0" cellpadding="2" cellspacing="0" width="100%">
<tr>
<td class="header" colspan="3" height="50" valign="middle"><a class="header" href="/">BishopAccountability.org</a>
<hr color="#000000"/></td>
</tr>
<tr><td colspan="3"> </td></tr>
<tr>
<td width="7%"> </td>
<td>
<strong>Priest Charged with Sex Crimes</strong><br/><br/>
By Carolyn Moreau and John Springer<br/>
Hartford Courant [Connecticut]<br/>
February 9, 2000<br/><br/>
Southington — A Catholic priest known in several states for flying a hot air balloon to promote a church-sponsored program was arrested Tuesday and charged with sexually assaulting a minor.<br/><br/>
Robert P. Pelkington, 58, also known as Robert Leo Pelkington, turned himself in to town police early Tuesday. He was charged with third-degree sexual assault and risk of injury to a minor in connection with an incident in April 1999 and risk of injury to a minor and disorderly conduct for an incident that police say occurred sometime between December 1998 and March 1999. <br/><br/>
Pelkington, a priest of the Dominican Order, was initially suspected of fondling a boy under the age of 16, police said. During the investigation, a second boy came forward to make a complaint. Police declined to comment further, and the warrants for the arrest were sealed by the court.<br/><br/>
Pelkington was ordained as a priest in Hartford in 1968, according to church records. He was never a priest in Southington, but spent time in town because he had friends there, said Norman Haddad of the Dominican Provincial Office in New York City, the order's eastern headquarters.<br/><br/>
"He is a very talented person who is very generous," Haddad said. "He turned himself in and he intends to cooperate. Hopefully this will come to a resolution."<br/><br/>
Since late last year, Pelkington has lived in St. Luke Institute, a Maryland psychiatric facility for Catholic clergy. His most recent assignment as a priest was in Valhallah, N.Y., at the Holy Name of Jesus Church from 1995 to 1999. During that time, Pelkington also helped run Marriage Encounters weekends in New York, Connecticut and other states, where he is known as Father Leo. Marriage Encounters is an ecumenical program that provides getaways for couples seeking to improve their marriages. It is sponsored by various church denominations.<br/><br/>
Pelkington became a hot air balloon pilot in 1998 with the idea of towing a large Marriage Encounter banner to publicize the program. He has flown the balloon at several weekend retreats in Connecticut, according to The Catholic Commentator, a newspaper of the Diocese of Baton Rouge, La.<br/><br/>
On Tuesday, Bristol Superior Court Judge Christina Dunnell ordered Pelkington to have no contact with the alleged victims.<br/><br/>
Noting that Pelkington returned from Maryland voluntarily after learning about the warrants, Dunnell did not order him to remain in Connecticut while the charges are pending.<br/><br/>
Pelkington's attorney, Hubert Santos of Hartford, refused comment after the arraignment. After signing the conditions of his release on bonds totalling $25,000, Pelkington was allowed by a special deputy sheriff to elude reporters by leaving the courthouse down a secured stairwell.<br/><br/>
If convicted of the three felony charges brought against him Tuesday, Pelkington could face up to 25 years in prison. He is due back in court April 10.
<br/><br/>
</td>
<td width="7%"> </td>
</tr>
<tr><td colspan="3"> </td></tr>
<tr>
<td class="sm" colspan="3"><hr color="#000000"/>
Any original material on these pages is copyright © BishopAccountability.org
2004. Reproduce freely with attribution. </td>
</tr>
<tr>
<td> </td>
<td> </td>
<td> </td>
</tr>
</table>
</body>
</html>
