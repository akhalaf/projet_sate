<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "38016",
      cRay: "610a2912590101dd",
      cHash: "f3a9d4f85c6f324",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2VkYXNweS5jb20uYnIvd3AtYWRtaW4vLS9kM2ZjYjdhMDk2ZmZjNWU2OTliMWZhNDgwMTk1Njg0ZSUwOSUwQQ==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "W900gZg9bLvdq5Vh8mRbhEG/IS5uM1RvH4M3jwizZg5ieTax1KTP+e59yyqJqHPHif1XrEc41B+13cXYubPidy0w8KiIGaZ6UNu1xur8n+moRj7WYmg4YwkI97R/xqrdPPP3kEcsIs1+jMl4suGeGpZb15cuMcP6P86gJklB1dnQM6L0dcT3n/uW+WwG8s92pjQ3IShJX4HTe9d8PIKPBYvskfswiyB9aRWYyNBIkvizOpI9OCuIpEyXfWkdBfyv1qMJIvWCYcyvu4XVqZ8lqADBhxFtraybAQ7bkV4eP3h1HFzfznDGYIJOpCr1kw+MN7dGMgrlZkjMrbotz1lWjXcN74durxExJ9xlSMvWMYx98Jp5DPFAfRfxp5HZEcTV2U8RjM9nL02l6FNvaIOD3OwRZTgkK3iX/cwxKg8J2vF/9zK0bFT3xO+mW+WX+KhWb2m7OA8vCg4sP0yuFZXiwp8sDTUTiunaqUiuDEKPaMjF1HKOpVz8v+Wcsc21un9EghQbm57KPk44uxdcyGjfrAkiYKkQgShTwBabn6JDv0WDD0iLGOmkVMn1WjiSNuXAwk5/KMy+DOSkfpbU2m9Vu8HRcFH2Kko+GglYPpvHkun+znfjhYxq+qL4GIcVxSHr+u2hGv1KnQZwx44XaKfEAwc4uyg2A0bLNeNGppGCZE+Gqm1M2r0j3tuM2nh22uz9xLbBFcAyQz7QJhJnh3UNTQJiqB32hXkGdYiweqR8hbEHpx3fsEoWaP8wK2C4FTr+",
        t: "MTYxMDQ4ODkzMi4yMTYwMDA=",
        m: "X1K5Oc66YVUjm9WoBa6mu5SzrVi3T5GO+n5exT1jing=",
        i1: "I9LBVclnEcBbgfPR3QSuXw==",
        i2: "rFQoLWMzG7NtMhmWkoeRYA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "HkXh7i1rtsnA0g2/I3SBEL61bT2NsJNojiXRskRgcSo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.cedaspy.com.br</h2>
</div><!-- /.header -->
<a href="https://derfueller.com/cautiouslongitudina.php?e=27"><!-- table --></a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/wp-admin/-/d3fcb7a096ffc5e699b1fa480195684e%09%0A?__cf_chl_captcha_tk__=661439e8983c9a73f22c18957d31be257ea0c57b-1610488932-0-AY2f9PcFXcAbHToE1u_xYhFjU2PJk_vhogb4x-FCEqQcxPrZDYcO1VnHnzwcaGtq1BgyRmKGrS6gDI631Kc9rKM21fRyKZjNZCsDlTlnBsUNBj9QPRdC1v5ocLARUJLQc1UB38vc3JnzD6Tw9qsmGh0lf5nYqST7m_rGWR9oqK-9WNTrIg0zeXWFzyPpT-ivGj_87tloDp61jJanRIPvLRdS4lyykNQrZtcqiDyhPr-d7M0RfNcvWDHfmF_wrY0872ix5Gmui2NzWPcqObGwf-LSmeyNgty8MwtM1a5urb0xhV0kvh_ntuv-vuOUzhoD3b1nBWyKCQRPOHhO_RYM610oB4yjNiDYOBAhGMZoqUjn63FUTLjatn2D8edqk4Wo2oK3Rzwzv6S-Igf80jjGsfNJwP5DsCFC2r1aiFz9qoF1HsFoG-mZ6JiJH8b1_vlfYoCWPWjik98avLOujSfdO-7DyKfeVcTkKceIEX39Pc2YMwB3olKRaPxyjZT7CYNtoxKbURdsPQq5BuQEy-4tMf_mAmSIwTQT5yTtqBEbbHqsQu3psyu8sWQp9hjyOg6nbgKZ5kFkYVxw0OqYG9zZAsY" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="e7955c0e2d161228f2016f1114f7cb95d092e17d-1610488932-0-ATDoC0g5R7LLKL9/adl3nGN2GiRnexeajL6L4CB3s/s7TLaF6NngJi4p96eRLh2K1Yo3/GJLtsu7321kjogYcfCG8z2fwGbHSJJispyQRH9w0t9zDkgqHFPi2yr2uhXOVex+A2BNDy9gYPWwgKkLLZrPvTmzsBw1yt/OgIracN1LBSaK3i7krkmCAjNhCymp4vNglQAJw6in5SVZEBGCCRnRJITqSOYD2pFR8xQo6RHG91yPabvG1mmMPm1CHW5loWCUm4AriL1dugdoIg0L1QTfH4290YZnc6lSGZSG4cs7Xxeg2QK6VDWatKAOKfEdszD1eXyJN5p5tLEWj5caDW9s2LBAbT1keY/BLPGB+dbSl7qpAVtoE5lfGV6szKufNt3n0yspuXEA1gtvcAlVoq1A8n44Q8LvF2YndjdxH7Te2i4VgsHfqBJ0h7u0RQ2FgbY6W5TWZrrqWS245d50lIoShja8JVE2VvqebtvX5qdKg+NipjnuK6MVWdtmHNAZzsXj6Go+QjSqr67vxfVW4tpUq53+6shTob1WwqLIH7h5L1yjY48GWC9KeF1+stuD8Lgk/Y/uS+HaT80y5xbtY5oBE7SkxEMc1iS3lKGCAgos+WJXFJQFkExW2kmxLu4zRMYgs5CLw4s5/uJNhGiz2OEKLiZALNvaN2QbNozYUHVI+RVykJ1RF02ciV90zlaY7oiLGkIN92mT35fZMTnADrdEld8+NOr5nhSEcHnHag+XkLa38P29NK7loNRhqULJnJuKvSiCkymW+S205oCEyiqWRC4d1KNpMJHkYf1MotwnePnpdOYi66rj7v4ib0ecA9A2N8ZVQFMxOcaBsBvReemM5oOW5RemD0/Gi2O7/vsNfq9K/CHqFMFt5yr/qkAsjjjPXkM6Rw5Q+wK1z85VP1FW2l12cSRT7+AVAlm09Oe2KM0WaXZoAJzhdFdaXYChDqC+af6dSNE9TOKNNCN8cystT0qRP8VEZmxqk69k6tXE3fiN9EY3a+zwNPzCiZ7J3y3BM4nM3eiE0Ovz8nV1roYPkFGF7TpoQXXTxkZt1Ct9V57oKi3IP/ojOmtInyKokRzL4gqcyBOYy9PN902Z1IjcpNFiupVB9HdW2SrYaLJfUPNgoAwHKXeBwoK3nszeixQdyenECsKva2qPUI27YJoaT6YDinQYsyOlE270wMstUP5EFcG1sVs5hXEyFdMEU2C/lpE1lPFQyFus/TPTx9FpWqTtNMHwwu8naWZoEw1k38VRDkKwNBjPe5mR+KmTFLNNeLuut8H1IpX2renpCFZsK+1R0fgw3E2izegu54H11/rYmQCRmwDoJxiI0j3znA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="3b515b588c91d46f8eaf7418cd12760d"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610a2912590101dd')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610a2912590101dd</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
