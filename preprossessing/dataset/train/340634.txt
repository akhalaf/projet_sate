<!DOCTYPE html>
<html dir="rtl" lang="ar" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="YHZ97bcvh6wZlzMVTdwcH_spcoGnyZuFUc7zvUVux3o" name="google-site-verification"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.coderoute-ma.com/xmlrpc.php" rel="pingback"/>
<title>الصفحة غير موجودة. – Code route Maroc</title>
<link href="//platform-api.sharethis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.coderoute-ma.com/feed" rel="alternate" title="Code route Maroc « الخلاصة" type="application/rss+xml"/>
<link href="https://www.coderoute-ma.com/comments/feed" rel="alternate" title="Code route Maroc « خلاصة التعليقات" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.coderoute-ma.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.coderoute-ma.com/wp-includes/css/dist/block-library/style-rtl.min.css" id="wp-block-library-rtl-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.coderoute-ma.com/wp-content/plugins/sharethis-share-buttons/css/mu-style.css" id="share-this-share-buttons-sticky-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/style.css" id="tie-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/css/ilightbox/dark-skin/skin.css" id="tie-ilightbox-skin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/earlyaccess/droidarabickufi" id="droidarabickufi-css" media="all" rel="stylesheet" type="text/css"/>
<script id="share-this-share-buttons-mu-js" src="//platform-api.sharethis.com/js/sharethis.js#property=5f784fe43517dc0012b7a154&amp;product=inline-buttons" type="text/javascript"></script>
<script id="jquery-core-js" src="https://www.coderoute-ma.com/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<link href="https://www.coderoute-ma.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.coderoute-ma.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.coderoute-ma.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/rtl.css" media="screen" rel="stylesheet" type="text/css"/><meta content="WordPress 5.5.3" name="generator"/>
<link href="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" rel="shortcut icon" title="Favicon"/>
<!--[if IE]>
<script type="text/javascript">jQuery(document).ready(function (){ jQuery(".menu-item").has("ul").children("a").attr("aria-haspopup", "true");});</script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/js/html5.js"></script>
<script src="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/js/selectivizr-min.js"></script>
<![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" media="all" href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/css/ie9.css" />
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" media="all" href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/css/ie8.css" />
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" media="all" href="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/css/ie7.css" />
<![endif]-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" rel="apple-touch-icon-precomposed" sizes="120x120"/>
<link href="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" rel="apple-touch-icon-precomposed"/>
<style media="screen" type="text/css">

body{
	font-family: 'droid arabic kufi';
	font-size : 16px;
}

#main-nav, #main-nav ul li a{
	font-family: 'droid arabic kufi';
	color :#ffffff;
	font-size : 18px;
	font-style: normal;
}

#main-nav,
.cat-box-content,
#sidebar .widget-container,
.post-listing,
#commentform {
	border-bottom-color: #37b8eb;
}

.search-block .search-button,
#topcontrol,
#main-nav ul li.current-menu-item a,
#main-nav ul li.current-menu-item a:hover,
#main-nav ul li.current_page_parent a,
#main-nav ul li.current_page_parent a:hover,
#main-nav ul li.current-menu-parent a,
#main-nav ul li.current-menu-parent a:hover,
#main-nav ul li.current-page-ancestor a,
#main-nav ul li.current-page-ancestor a:hover,
.pagination span.current,
.share-post span.share-text,
.flex-control-paging li a.flex-active,
.ei-slider-thumbs li.ei-slider-element,
.review-percentage .review-item span span,
.review-final-score,
.button,
a.button,
a.more-link,
#main-content input[type="submit"],
.form-submit #submit,
#login-form .login-button,
.widget-feedburner .feedburner-subscribe,
input[type="submit"],
#buddypress button,
#buddypress a.button,
#buddypress input[type=submit],
#buddypress input[type=reset],
#buddypress ul.button-nav li a,
#buddypress div.generic-button a,
#buddypress .comment-reply-link,
#buddypress div.item-list-tabs ul li a span,
#buddypress div.item-list-tabs ul li.selected a,
#buddypress div.item-list-tabs ul li.current a,
#buddypress #members-directory-form div.item-list-tabs ul li.selected span,
#members-list-options a.selected,
#groups-list-options a.selected,
body.dark-skin #buddypress div.item-list-tabs ul li a span,
body.dark-skin #buddypress div.item-list-tabs ul li.selected a,
body.dark-skin #buddypress div.item-list-tabs ul li.current a,
body.dark-skin #members-list-options a.selected,
body.dark-skin #groups-list-options a.selected,
.search-block-large .search-button,
#featured-posts .flex-next:hover,
#featured-posts .flex-prev:hover,
a.tie-cart span.shooping-count,
.woocommerce span.onsale,
.woocommerce-page span.onsale ,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle,
#check-also-close,
a.post-slideshow-next,
a.post-slideshow-prev,
.widget_price_filter .ui-slider .ui-slider-handle,
.quantity .minus:hover,
.quantity .plus:hover,
.mejs-container .mejs-controls .mejs-time-rail .mejs-time-current,
#reading-position-indicator  {
	background-color:#37b8eb;
}

::-webkit-scrollbar-thumb{
	background-color:#37b8eb !important;
}

#theme-footer,
#theme-header,
.top-nav ul li.current-menu-item:before,
#main-nav .menu-sub-content ,
#main-nav ul ul,
#check-also-box {
	border-top-color: #37b8eb;
}

.search-block:after {
	border-right-color:#37b8eb;
}

body.rtl .search-block:after {
	border-left-color:#37b8eb;
}

#main-nav ul > li.menu-item-has-children:hover > a:after,
#main-nav ul > li.mega-menu:hover > a:after {
	border-color:transparent transparent #37b8eb;
}

.widget.timeline-posts li a:hover,
.widget.timeline-posts li a:hover span.tie-date {
	color: #37b8eb;
}

.widget.timeline-posts li a:hover span.tie-date:before {
	background: #37b8eb;
	border-color: #37b8eb;
}

#order_review,
#order_review_heading {
	border-color: #37b8eb;
}


</style>
<script type="text/javascript">
			/* <![CDATA[ */
				var sf_position = '0';
				var sf_templates = "<a href=\"{search_url_escaped}\">\u0639\u0631\u0636 \u0643\u0644 \u0627\u0644\u0646\u062a\u0627\u0626\u062c<\/a>";
				var sf_input = '.search-live';
				jQuery(document).ready(function(){
					jQuery(sf_input).ajaxyLiveSearch({"expand":false,"searchUrl":"https:\/\/www.coderoute-ma.com\/?s=%s","text":"Search","delay":500,"iwidth":180,"width":315,"ajaxUrl":"https:\/\/www.coderoute-ma.com\/wp-admin\/admin-ajax.php","rtl":0});
					jQuery(".live-search_ajaxy-selective-input").keyup(function() {
						var width = jQuery(this).val().length * 8;
						if(width < 50) {
							width = 50;
						}
						jQuery(this).width(width);
					});
					jQuery(".live-search_ajaxy-selective-search").click(function() {
						jQuery(this).find(".live-search_ajaxy-selective-input").focus();
					});
					jQuery(".live-search_ajaxy-selective-close").click(function() {
						jQuery(this).parent().remove();
					});
				});
			/* ]]> */
		</script>
</head>
<body class="rtl error404 lazy-enabled" id="top">
<div class="wrapper-outer">
<div class="background-cover"></div>
<aside id="slide-out">
<div class="search-mobile">
<form action="https://www.coderoute-ma.com/" id="searchform-mobile" method="get">
<button class="search-button" type="submit" value="بحث"><i class="fa fa-search"></i></button>
<input id="s-mobile" name="s" onblur="if (this.value == '') {this.value = 'بحث';}" onfocus="if (this.value == 'بحث') {this.value = '';}" title="بحث" type="text" value="بحث"/>
</form>
</div><!-- .search-mobile /-->
<div class="social-icons">
<a class="ttip-none" href="https://www.coderoute-ma.com/feed" target="_blank" title="Rss"><i class="fa fa-rss"></i></a>
</div>
<div id="mobile-menu"></div>
</aside><!-- #slide-out /-->
<div class="boxed" id="wrapper">
<div class="inner-wrapper">
<header class="theme-header" id="theme-header">
<div class="top-nav" id="top-nav">
<div class="container">
<div class="top-menu"><ul class="menu" id="menu-top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145" id="menu-item-145"><a href="https://www.coderoute-ma.com/%d9%85%d9%86-%d9%86%d8%ad%d9%86">من نحن</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147" id="menu-item-147"><a href="https://www.coderoute-ma.com/%d8%a7%d9%84%d8%b3%d9%8a%d8%a7%d8%b3%d8%a9-%d8%a7%d9%84%d8%ae%d8%b5%d9%88%d8%b5%d9%8a%d8%a9">السياسة الخصوصية</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209" id="menu-item-209"><a href="https://www.coderoute-ma.com/terms">شروط الإستخدام</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143" id="menu-item-143"><a href="https://www.coderoute-ma.com/%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7">اتصل بنا</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-210" id="menu-item-210"><a href="/">الرئسية</a></li>
</ul></div>
<div class="search-block">
<form action="https://www.coderoute-ma.com/" id="searchform-header" method="get">
<button class="search-button" type="submit" value="بحث"><i class="fa fa-search"></i></button>
<input class="search-live" id="s-header" name="s" onblur="if (this.value == '') {this.value = 'بحث';}" onfocus="if (this.value == 'بحث') {this.value = '';}" title="بحث" type="text" value="بحث"/>
</form>
</div><!-- .search-block /-->
<div class="social-icons">
<a class="ttip-none" href="https://www.coderoute-ma.com/feed" target="_blank" title="Rss"><i class="fa fa-rss"></i></a>
</div>
</div><!-- .container /-->
</div><!-- .top-menu /-->
<div class="header-content">
<a class="slide-out-open" href="#" id="slide-out-open"><span></span></a>
<div class="logo" style=" margin-top:15px; margin-bottom:15px;">
<h2> <a href="https://www.coderoute-ma.com/" title="Code route Maroc">
<img alt="Code route Maroc" height="180" src="https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg" width="180"/><strong>Code route Maroc Code permis maroc test code de la route maroc</strong>
</a>
</h2> </div><!-- .logo /-->
<script type="text/javascript">
jQuery(document).ready(function($) {
	var retina = window.devicePixelRatio > 1 ? true : false;
	if(retina) {
       	jQuery('#theme-header .logo img').attr('src',		'https://www.coderoute-ma.com/wp-content/uploads/2020/08/crm.jpg');
       	jQuery('#theme-header .logo img').attr('width',		'180');
       	jQuery('#theme-header .logo img').attr('height',	'180');
	}
});
</script>
<div class="e3lan e3lan-top"> <script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- res1 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-1082990280989504" data-ad-format="auto" data-ad-slot="7817797886" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script> </div> <div class="clear"></div>
</div>
<nav class="fixed-enabled" id="main-nav">
<div class="container">
<div class="main-menu"><ul class="menu" id="menu-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-168" id="menu-item-168"><a href="/"><i class="fa fa-home"></i>الصفحة الرئسية</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-82" id="menu-item-82"><a href="https://www.coderoute-ma.com/article/category/%d9%82%d8%a7%d9%86%d9%88%d9%86-%d8%a7%d9%84%d8%b3%d9%8a%d8%b1">قانون السير</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-169" id="menu-item-169"><a href="https://www.coderoute-ma.com/article/category/%d8%b9%d8%a7%d9%84%d9%85-%d8%a7%d9%84%d8%b3%d9%8a%d8%a7%d8%b1%d8%a7%d8%aa">عالم السيارات</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-83" id="menu-item-83"><a href="https://www.coderoute-ma.com/article/category/%d8%a7%d9%84%d8%b3%d9%84%d8%a7%d9%85%d8%a9-%d8%a7%d9%84%d8%b7%d8%b1%d9%82%d9%8a%d8%a9-2">السلامة الطرقية</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-170" id="menu-item-170"><a href="https://www.coderoute-ma.com/article/category/%d9%82%d9%8a%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%b3%d9%8a%d8%a7%d8%b1%d8%a7%d8%aa">قيادة السيارات</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185" id="menu-item-185"><a href="https://www.coderoute-tn.com/%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7/">تواصل معنا</a></li>
</ul></div> <a class="random-article ttip" href="https://www.coderoute-ma.com/?tierand=1" title="مقال عشوائي"><i class="fa fa-random"></i></a>
</div>
</nav><!-- .main-nav /-->
</header><!-- #header /-->
<div class="container full-width" id="main-content">
<div class="content">
<div class="post error404">
<div class="post-inner">
<div class="title-404">404 :(</div>
<h2 class="post-title">غير موجود !</h2>
<div class="clear"></div>
<div class="entry">
<p>المعذرة ولكن الصفحة المطلوبة غير موجودة .. حاول إستخدام محرك البحث .</p>
<div class="search-block-large">
<form action="https://www.coderoute-ma.com/" method="get">
<button class="search-button" type="submit" value="بحث"><i class="fa fa-search"></i></button>
<input id="s" name="s" onblur="if (this.value == '') {this.value = 'بحث';}" onfocus="if (this.value == 'بحث') {this.value = '';}" type="text" value="بحث"/>
</form>
</div><!-- .search-block /-->
</div><!-- .entry /-->
<section id="related_posts">
<div class="block-head">
<h3>شاهد أيضاً</h3><div class="stripe-line"></div>
</div>
<div class="post-listing">
<div class="related-item">
<div class="post-thumbnail">
<a href="https://www.coderoute-ma.com/article/190.html" rel="bookmark">
<img alt="" class="attachment-tie-medium size-tie-medium wp-post-image" height="165" loading="lazy" src="https://www.coderoute-ma.com/wp-content/uploads/2020/05/2020-05-10_030528-310x165.jpg" width="310"/> <span class="fa overlay-icon"></span>
</a>
</div><!-- post-thumbnail /-->
<h3><a href="https://www.coderoute-ma.com/article/190.html" rel="bookmark">هواوي تتحكم في سيارة porsche panamera عبر الذكاء الاصطناعي</a></h3>
<p class="post-meta"></p>
</div>
<div class="related-item">
<div class="post-thumbnail">
<a href="https://www.coderoute-ma.com/article/189.html" rel="bookmark">
<img alt="ما الفرق بين سيارات الديزل وسيارات البنزين؟" class="attachment-tie-medium size-tie-medium wp-post-image" height="165" loading="lazy" src="https://www.coderoute-ma.com/wp-content/uploads/2020/05/2020-05-10_030133-310x165.jpg" width="310"/> <span class="fa overlay-icon"></span>
</a>
</div><!-- post-thumbnail /-->
<h3><a href="https://www.coderoute-ma.com/article/189.html" rel="bookmark">ما الفرق بين سيارات الديزل وسيارات البنزين؟</a></h3>
<p class="post-meta"></p>
</div>
<div class="related-item">
<div class="post-thumbnail">
<a href="https://www.coderoute-ma.com/article/188.html" rel="bookmark">
<img alt="كيف تختار سيارتك" class="attachment-tie-medium size-tie-medium wp-post-image" height="165" loading="lazy" src="https://www.coderoute-ma.com/wp-content/uploads/2020/05/2020-05-10_025649-310x165.jpg" width="310"/> <span class="fa overlay-icon"></span>
</a>
</div><!-- post-thumbnail /-->
<h3><a href="https://www.coderoute-ma.com/article/188.html" rel="bookmark">كيف تختار سيارتك</a></h3>
<p class="post-meta"></p>
</div>
<div class="related-item">
<div class="post-thumbnail">
<a href="https://www.coderoute-ma.com/article/39.html" rel="bookmark">
<img alt="كيف تحافظ على سيارتك" class="attachment-tie-medium size-tie-medium wp-post-image" height="165" loading="lazy" src="https://www.coderoute-ma.com/wp-content/uploads/2020/05/2020-05-10_025122-310x165.jpg" width="310"/> <span class="fa overlay-icon"></span>
</a>
</div><!-- post-thumbnail /-->
<h3><a href="https://www.coderoute-ma.com/article/39.html" rel="bookmark">كيف تحافظ على سيارتك</a></h3>
<p class="post-meta"></p>
</div>
<div class="clear"></div>
</div>
</section>
</div><!-- .post-inner -->
</div><!-- .post-listing -->
</div>
<div class="clear"></div>
</div><!-- .container /-->
<footer id="theme-footer">
<div class="footer-3c" id="footer-widget-area">
<div class="footer-widgets-box" id="footer-second">
<div class="footer-widget widget_rss" id="rss-2"><div class="footer-widget-top"><h4><a class="rsswidget" href="https://cars-post.com/feed"><img alt="RSS" class="rss-widget-icon" height="14" src="https://www.coderoute-ma.com/wp-includes/images/rss.png" style="border:0" width="14"/></a> <a class="rsswidget" href="https://cars-post.com/">كار  بوست</a></h4></div>
<div class="footer-widget-container"><ul><li><a class="rsswidget" href="https://cars-post.com/article/71.html">نصائح عملية لاختيار كاميرا الرؤية الخلفية لسيارتك (Caméra de recul)</a></li><li><a class="rsswidget" href="https://cars-post.com/article/68.html">يا ترى أي سيارة أختار في عام 2020 ؟</a></li><li><a class="rsswidget" href="https://cars-post.com/article/65.html">إياك والاستمرار في سياقة سيارتك ذات الخزان شبه الفارغ من الوقود !</a></li><li><a class="rsswidget" href="https://cars-post.com/article/62.html">تجربة السياقة وحيدا في السيارة لأول مرة</a></li><li><a class="rsswidget" href="https://cars-post.com/article/59.html">كيفية ملء محضر المعاينة الودية أو الحبية (constat amiable)؟</a></li><li><a class="rsswidget" href="https://cars-post.com/article/56.html">مدونة السير : أخطاء احرص على تجنبها خلال إجابتك على سلسلة الأسئلة</a></li><li><a class="rsswidget" href="https://cars-post.com/article/53.html">اجتزت اختبار الحصول على رخصة السياقة . . . معلومات عليك معرفتها في هذه المرحلة</a></li><li><a class="rsswidget" href="https://cars-post.com/article/49.html">مراحل امتحان الحصول على رخصة السياقة بالمغرب</a></li><li><a class="rsswidget" href="https://cars-post.com/article/46.html">شروط الحصول على رخصة السياقة بالمغرب</a></li><li><a class="rsswidget" href="https://cars-post.com/article/43.html">إذا لم توفق في الاختبار الأول (النظري ثم التطبيقي) .. فماذا تفعل؟</a></li></ul></div></div><!-- .widget /--> </div><!-- #second .widget-area -->
</div><!-- #footer-widget-area -->
<div class="clear"></div>
</footer><!-- .Footer /-->
<div class="clear"></div>
<div class="footer-bottom">
<div class="container">
<div class="alignright">
			Powered by <a href="http://wordpress.org">WordPress</a> | Designed by <a href="http://tielabs.com/">TieLabs</a> </div>
<div class="social-icons">
<a class="ttip-none" href="https://www.coderoute-ma.com/feed" target="_blank" title="Rss"><i class="fa fa-rss"></i></a>
</div>
<div class="alignleft">
			© Copyright 2021, All Rights Reserved		</div>
<div class="clear"></div>
</div><!-- .Container -->
</div><!-- .Footer bottom -->
</div><!-- .inner-Wrapper -->
</div><!-- #Wrapper -->
</div><!-- .Wrapper-outer -->
<div class="fa fa-angle-up" id="topcontrol" title="إلى الأعلى"></div>
<div id="fb-root"></div>
<script id="tie-scripts-js-extra" type="text/javascript">
/* <![CDATA[ */
var tie = {"mobile_menu_active":"true","mobile_menu_top":"","lightbox_all":"true","lightbox_gallery":"true","woocommerce_lightbox":"","lightbox_skin":"dark","lightbox_thumb":"vertical","lightbox_arrows":"","sticky_sidebar":"1","is_singular":"","reading_indicator":"","lang_no_results":"\u0644\u0627 \u064a\u0648\u062c\u062f \u0646\u062a\u0627\u0626\u062c","lang_results_found":"\u0646\u062a\u0627\u0626\u062c \u062a\u0645 \u0627\u0644\u0639\u062b\u0648\u0631 \u0639\u0644\u064a\u0647\u0627"};
/* ]]> */
</script>
<script id="tie-scripts-js" src="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/js/tie-scripts.js" type="text/javascript"></script>
<script id="tie-ilightbox-js" src="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/js/ilightbox.packed.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.coderoute-ma.com/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script id="tie-search-js" src="https://www.coderoute-ma.com/wp-content/themes/sahifa2020/js/search.js" type="text/javascript"></script>
</body>
</html>