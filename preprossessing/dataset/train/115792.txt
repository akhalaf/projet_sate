<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Page Not Found</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="nofollow, noarchive, noindex" name="robots"/>
<link href="/themes/custom/muse3/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;display=swap" rel="stylesheet"/>
<style>
    * {
      line-height: 1.2;
      margin: 0;
    }
    html {
      color: #081F2C;
      font-family: 'Roboto', sans-serif;
      height: 100%;
      text-align: center;
      width: 100%;
    }
    body {
      box-sizing: border-box;
      line-height: 1.428571429;
      font-size: 16px;
      padding: 0 1em;
    }
    a {
      color: #222;
      font-weight: bold;
      text-decoration: underline;
    }
    ol {
      padding-left: 20px;
    }
    div.header {
      margin: 1em 0 2em;
    }
    div > header {
      font-weight: bold;
      padding: 1em 0;
    }
    h1 {
      font-size: 1.5em;
      letter-spacing: -1px;
      text-rendering: optimizeLegibility;
      font-weight: 500;
      line-height: 1.1;
    }
    div.message div.links {
      text-align: left;
      width: 95%;
      margin: 0 auto;
    }
    div.card {
      padding: 1em;
      background-color: #EFEFEF;
      margin: 1em auto;
    }
    div.card img {
      width: 100%;
    }
    div.card p.caption {
      font-size: small;
      color: #555;
      text-align: left;
    }
    @media (min-width: 576px) {
      h1 {
        font-size: 2.5em;
      }
      div.card, div.message div.links {
        width: 85%;
      }
    }
    @media (min-width: 768px) {
      h1 {
        font-size: 3em;
      }
      div.card, div.message div.links {
        width: 75%;
      }
      div > header {
        font-size: larger;
      }
    }
    @media (min-width: 992px) {
      h1 {
        font-size: 4em;
      }
      div.card, div.message div.links {
        width: 65%;
      }
    }
  </style>
</head>
<body>
<div class="header">
<a class="logo" href="/" rel="home" title="Home">
<img alt="Home" src="https://americanart.si.edu/themes/custom/muse3/images/branding/logo.png"/>
</a>
</div>
<div class="message">
<h1>404 - Oops, sorry, that page doesn't exist.</h1>
<div class="links">
<header>May we suggest:</header>
<ol>
<li><a href="/search">Searching for the page</a> you're looking for.</li>
<li><a href="/art">Exploring the art + artists page</a> or checking our lists of current, past, and upcoming <a href="/exhibitions">exhibitions</a></li>
<li>View upcoming events in our <a href="/events">calendar of events</a></li>
<li>Or, if you have a little time and want to try something new, try browsing SAAM’s collection by <a href="/art/colors">colors</a> or <a href="/art/emoji">emoji</a></li>
</ol>
</div>
</div>
<div class="card">
<a href="/artwork/search-12882">
<img alt="The Search" src="https://s3.amazonaws.com/assets.saam.media/files/styles/max_650x650/s3/files/images/1979/SAAM-1979.80.9_1.jpg"/>
</a>
<p class="caption">Jacob Kainen, <a href="/artwork/search-12882">The Search</a>, 1952, oil on canvas, Smithsonian American Art Museum, Gift of the artist, 1979.80.9</p>
</div>
</body>
</html>
