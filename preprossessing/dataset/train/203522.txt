<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "80109",
      cRay: "611080438f7819d1",
      cHash: "f0bd5897375c679",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2FsaWZvcm5pYWJlYWNoZXMuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "/GMZp5ZsAHiQcqi3MJKv9XPwqLbZ0kf/jW0qkEPi9OpJT3eL/P2/kCOcTviHihLNtT1gkvYDxyHqjqrMOT6IHbh4aoWDSF356qyynLPQCrZdWlRoo2uNwb794lKmZWSL7M1iJzetw6hB0zjo4yXMZyhk06NDf43EILsLtTjdYyzaXOK3m03AelKBAKvTaqB71t2tD4MGC+10MJnojw2W3bvD0Soj46r8Go7NdtjbuT9Q8lVkcymrzlEAQG0T8E/ozPdqCUot3UEu09HtPomUiplPOZjl8ocxAmmbXoAcCgnK0Hflblksr//pu1679QVzKu5c0saTL8mUU5HDVTFzttc8ftvpdaQH+cQcCQMcKiy4+KLGQP1f0EEVCIQiYa3qDXa9e/vSUHGXAPDY7Uac+ufNMtoZCBocICHKvROt4GEF2kY5ag5hT/FVP7eK4HJqofQgu0H3XBskUDFpvoO+8UTVTnKRCljUv9ZiyP4EUhn5iBIqehzsVTFXuVhXNPsrvLudguYRiLfrgmO2HlASqKLd32LC0VI2jBKw2T52f9Vq2sKhuSFb57OdejlAe08lZuerE0lTNf+SnF/rLNMXJmCj7kaTFgtGyYWoeHz0uIW6bfFOnzZ+redJP4l9lNSf0vjDZHJjiBz0FB1BvpBDR9EY0WcLrqIAKGNHlF7w7vuBAeWL8gQbLjJHKkapsdQl3VO2DRBdHgqadO61UwE964zOEvot6hTb/BeLLNBtj5WE3EmAU23PRMGQrk2P64PD",
        t: "MTYxMDU1NTQxOC4xNzAwMDA=",
        m: "zXtKcgRlPGsLupcWAiWbbgtqHyUChUhDhM1BiyPChfI=",
        i1: "Po7bqFf3CvVhMwpdZIS7cg==",
        i2: "YiO7YeQlA28BBINgJ/HcUQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "MO0dV88updyJRe38MLNudPbYbUmrnR2k1jGkljkVG24=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=611080438f7819d1");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> californiabeaches.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=9ab2bb70bc806306f1a6eda127486ab539b359eb-1610555418-0-AWyJPzm-aVXqaFzgBEidJuSLsofM5hZy0dz1YGER9xo5xrfj_So5YeiPwM7nmCc0d9h5tz35507FJBKO-uHPs_wd5qdD6KlOw7JMuXu2kZ3kX_cCbMjv3VhUOCADo5yotr6Z1y7evYFAFGFWODNTVprzmSwAHg2oPSsKpHTt1YFfVm9KnwumO6s9heRPEKQzFnguZvflihwAqN_dEOfiToYGpPKqawWAtTm4GfP3Rvof_K9Ij9NTAQIjkqb7AwNu4pjcet4h7YXhimmE8LhEn5durcUHXvneT2tbVJ8-pjDCY-ChAgnTVeEeGiWhQKGfhA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="9dca63b07401fce2cb98686d0a348d8ce06e1762-1610555418-0-AZOXyWX6Ge9lFAt4AYijbhsGoCeLP8U7EkStGTnpieGXtOWwsJaYDSMSUkPQS3ORfJA3EOvt19YpiuunqvWBz++EaU/z085Oen+NsjUNCnVlRgPfLkUN0YwhPiXcjJ4gVmQ7KUjc2GLHSMU11rhWOGr9BgGIxbTZoUrl8f+u0BbtUyLW4ZVos6meRnMeGoknkGCWfF66IiD8AbRcYrKICqCGGYeRbcS3wx7dDtF8g4RGFMscplIsClmboqeJv0qUZ+PUAAyUuc9C1gRHznmjtelSniWtlu+fC9myYTNYD/t0OItFuuDqTYnxNrzLQ6/UHxgLAGL4d3Z/ENFMo3c5wbcBFRicTaD93QZb1jc947JXi/mCGwc/7+TD3pbGuHTb+pcU6mKDrkRnLLZgitu1bsgPQeD2bQuw+RXIzggPR9dhi3h3u8koqsDOSf8xotq0DZrSag3fHM4kGZQnaYDnthqqzCj6Cs2p1vgI2xYt0a4n87pDacPuR/b1rCffzmuSicR0Kv+nhf9Nybdz0o6cPAubBXifs+CKu84yQ4yJbiGFZKex+FQ64QW7TI2k0QI9y+2v8UgbYxYn/O4paS/5QNc1TbRogo2yZbzQbblGMflguLgMtLP9iy/cfVYCga5+25VusDVsYvElJyUyS8zhsWHpnr/doHXrPLdalxsMlI23nLQlaHo5jH0Oq4tGSFtebmuuTXnuNMTiidvZllOCnNeECD5w6PDyP2dNjJCZTGmUpRHxjFS8e2+4OzK2RlQewUPkpC8AVCblMZriiAgTfu40UoYoj/C1ok22/TKnYXQbpmJx5ixPWpUiMIhABwp2THpkOkjUXgkau7MjnSGIigF14xqSFVa+gqJEGceoUxvLt5y9g57lOdaSNapEB4UWBLZ2p9dU3BBLXIMkg97t87hTyaqF3LNllKIg+Pifp0jxmkKuMSYV4DZdYwz7K8BDT9setAEBgQSCUXQU3sspWC+dC2IkyhmMZveAfjzxaEGMXGVyf7XeTcccPHxHLbXgCsKh92Fzb7e7eWGiK/iUBSXyt0mTPYJs95KlGt6nojN3DtUshsWoa/vjdkWjUG65An3QyWsVK4NfQrS1Y1vLg9c/aDAkCqmAXBrFpoa307iWG9gX0kvrGLpXIr3+x/pcIttjGuHuTyeNci+jXM2SX/YhCK+JgVvDvt8N8YhLrQDHo5q71XtzxW9Vab1nKru1AI7q4xbttKR19KsjfJAJXB8X0bT+gFGfVVmh4uUzMbsQdvi/vMnSfDcxxm3eC5xXdoP3IlPfOPQtr1GKfYmcy/zAbReoDrrnUd3w/f1AMAH5+LyQYb8v8VKuX3/PFhRJpfEkzqNXa83d+6KMUnBnxwI="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="4d41ce043ee242d2292ebbb457eff495"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610555422.17-5c+q5/8Bn3"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=611080438f7819d1')"> </div>
</div>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://tornado-networks.com/lupineabbey.php?goto=465">table</a></div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>611080438f7819d1</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
