<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Tim &amp; Josh</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="Rvglobalsoft" name="generator"/>
<!-- Version: 6.4.41 PRO; Project name: TimandJosh; Project id: 5c618fcca25639b54430e271eac43390; Template Name: 23-827638-1_darkgreen_DiyPicture_2; 
Published date: April 7, 2020, 5:33 33 (GMT -05:00) -->
<link href="http://timandjosh.com/style.css" rel="stylesheet" type="text/css"/>
<link href="http://timandjosh.com/Arial.css" rel="stylesheet" type="text/css"/>
<link href="http://timandjosh.com/Navigator.css" rel="stylesheet" type="text/css"/>
<link href="http://timandjosh.com/pathway.css" rel="stylesheet" type="text/css"/>
<script async="" defer="" src="//www.google.com/recaptcha/api.js"></script>
<link href="http://timandjosh.com/js/jquery-ui/themes/base/ui.all.css" id="linkcssglobal" media="screen" rel="stylesheet" type="text/css"/>
<script src="http://timandjosh.com/js/jquery-ui/jquery.min.js" type="text/javascript"></script>
<script src="http://timandjosh.com/js/jquery-ui/ui/minified/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://timandjosh.com/rvsincludefile/rvsheadpage.js" type="text/javascript"></script>
<script src="http://timandjosh.com/js/jquery.url.js" type="text/javascript"></script>
<!-- new navigation style 2013 -->
<link href="http://timandjosh.com/js/publishNavigator/ddsmoothmenu.css" rel="stylesheet" type="text/css"/>
<link href="http://timandjosh.com/js/publishNavigator/ddsmoothmenu-v.css" rel="stylesheet" type="text/css"/>
<script defer="defer" type="text/javascript">

(function ($) {
$(document).ready(function() {
    var CurrentUrl = window.location.href;
    if (CurrentUrl.match(/(rvsindex.php)|(blogweb\/index)|(faqweb\/)/)) {
        page = (urlRefer != undefined) ? urlRefer : '';
    } else {
        rPath = CurrentUrl.match(/.*?(\/).*?/img);
        for (i=0;i< rPath.length;i++) {
            page = CurrentUrl.replace(/(.*?)\//,'');
            CurrentUrl = page;
        }
    }

    if ($.browser.webkit) {
	    window.addEventListener('load', function(){
	    	$(':-webkit-autofill').each(function(){
            	var text = $(this).val();
            	var name = $(this).attr('name');
            	$(this).after(this.outerHTML).remove();
            	$('input[name=' + name + ']').val(text);
            });
	    }, false);
	}


    var poiont = $('.rvnavigator a[href="' + page + '"]').parents("li").length;
    if (poiont > 0) {
        for (i=0;i<=poiont;i++) {
            if (page != '') {
            	$('.rvnavigator a[href="' + page + '"]').parents("li").eq(i).find('a:first').attr('class', 'current');
            }
        }
    }

    $('#jumpmenu').find('option').each(function() {
		if($(this).val() == window.location.href) {
			$(this).attr('selected', 'selected');
		}
	})
    
});
})($);

</script>
<script src="http://timandjosh.com/rvsincludefile/rvscustomopenwindow.js" type="text/javascript"></script>
</head>
<body class="diybackground">
<table align="center" cellpadding="0" cellspacing="0" id="rv_top_adjust_width_0" width="780">
<tr>
<td align="left" valign="top">
<!-- START LOGO -->
<div style="position: absolute;">
<div id="Layer1" style="position:relative; left:19px; top:32px; width:120; height:60; text-align:center; z-index:1; overflow:visible; white-space:nowrap;"> <a href="http://timandjosh.com/Home.php">
<img alt="Logo" border="0" height="104" src="http://timandjosh.com/LOGO.JPG?a114c2d394e335c9fe2c3776175345ad" width="120"/>
</a></div>
</div>
<div style="position: absolute;">
<div class="company" id="Layer2" style="position:relative; left:362px; top:53px; width:auto; height:auto; text-align:left; z-index:2; overflow:visible; white-space:nowrap;"><div>Tim &amp; Josh Company</div></div>
</div>
<div style="position: absolute;">
<div class="slogan" id="Layer3" style="position:relative; left:362px; top:85px; width:auto; height:auto; text-align:left; z-index:3; overflow:visible; white-space:nowrap;"><div>Products to improve your daily life. </div></div>
</div>
<!-- END LOGO -->
<!-- START CUSTOM LAYER --><!-- END CUSTOM LAYER -->
<div id="fb-root"></div>
<!-- {literal} -->
<script id="scriptBuildFacebook" type="text/javascript">

//<![CDATA[

  (function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=362039017157260";
	  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

//]]>

function getFrameFacebook() {

	if($('.fb-like-box').find('iframe').get(0) == undefined)
	{
		recursiveTimeout = setTimeout(function(){getFrameFacebook();},100);
		//console.log('undefined')
	} else {
		//console.log('sucess'+$('.fb-like-box').find('iframe').width()+'='+ fbWidth )
		if ($('.fb-like-box').find('iframe').width() != 0) {
			$('.fb-like-box').find('iframe').attr('width',fbWidth+'px').css('width',fbWidth);
			recursiveTimeout = setTimeout(function(){getFrameFacebook();},100);
			$('.fb-like-box').find('iframe').load(function(){
			clearTimeout(recursiveTimeout); })
		} else {
			$('.fb-like-box').find('iframe').attr('width',fbWidth+'px').css('width',fbWidth);
		       }
	}
}

$(document).ready(function(){
     var recursiveDetectWidth = setTimeout(function(){
			detectWidthFacebook();
	},500);

     function detectWidthFacebook(){
    	  if($('div.fbFanPageBlock').find('iframe').prop('tagName') == undefined){
    	   recursiveDetectWidth = setTimeout(function(){
				detectWidthFacebook();
			},500);
    	}else{
    	   var fbWidth = $('div.fbFanPageBlock').find('div[data-width]').attr('data-width');
    	   var fBody = $('div.fbFanPageBlock').find('iframe');

    	   fBody.load(function(){
				$(this).css('width',fbWidth);
    	   })
    	}
    	}
});

</script>
<!-- {/literal} -->
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left" class="bgheader" height="128"><img alt="" src="images/bannerImg.jpg?cache=f07fd45e8fd908cc07336eee21ccf725"/></td>
</tr>
<tr><td class="header_line3"></td></tr>
<tr><td class="header_line4"><img alt="" height="2" src="images/spacer.gif" width="1"/></td></tr>
<tr>
<td align="left" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left" class="bgnavigator" height="32" valign="top"><div id="rvnavigator"><ul class="rvnavigator">
<li>
<a href="http://timandjosh.com/Home.php" target="_self"><span file_name="Home.php" is_draft="0" is_homepage="1" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="9addd7f7f29adb23629311906071a2df">Home</span></a></li>
<li>
<a href="http://timandjosh.com/Products.php" target="_self"><span file_name="Products.php" is_draft="0" is_homepage="0" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="25fb8e5e764d8b88e8e6181de44a9f8a">Products</span></a></li>
<li>
<a href="http://timandjosh.com/Testimonials.php" target="_self"><span file_name="Testimonials.php" is_draft="0" is_homepage="0" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="d3fb5cdeffba57813ffaa6ef0d612128">Testimonials</span></a></li>
<li>
<a href="http://timandjosh.com/FAQ.php" target="_self"><span file_name="FAQ.php" is_draft="0" is_homepage="0" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="c188e7e34697aa09bbb68c7d44f3a592">FAQ</span></a></li>
<li>
<a href="http://timandjosh.com/Order.php" target="_self"><span file_name="Order.php" is_draft="0" is_homepage="0" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="a4175f3d926caf51b5a63f28a55b38a1">Order</span></a></li>
<li>
<a href="http://timandjosh.com/Contact-Us.php" target="_self"><span file_name="Contact-Us.php" is_draft="0" is_homepage="0" meta_description="" meta_keyword="" other_embed="" page_css="" page_title="" pageid="cdf50ab606f448c8f96d035a1986a51f">Contact Us</span></a></li>
</ul></div>
<div class="clear"></div>
</td>
</tr>
<tr><td class="bgtshadow"><img alt="" height="12" src="images/spacer.gif" width="1"/></td></tr>
<tr>
<td align="left" valign="top">
<table cellpadding="0" cellspacing="0" class="bgbody" width="100%">
<tr><td align="center" class="content_margin_top"></td></tr>
<!-- Begin PATHWAY and ICON -->
<tr>
<td class="magin">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<!-- Begin PATHWAY -->
<td align="left" width="99%"><table cellpadding="0" cellspacing="0" iscomponentdb="0" width="100%"><tr style="float:left;" valign="top"> <td nowrap="nowrap">   <a href="http://timandjosh.com/index.php" pageid="9addd7f7f29adb23629311906071a2df" target="_top"><b>Home</b></a></td><td nowrap="nowrap" width="99%"></td></tr></table></td>
<!-- End PATHWAY -->
<!-- Begin ICON -->
<td align="right"><!--Cannot get icon--></td>
<!-- End ICON -->
</tr>
</table>
</td>
</tr>
<!-- End PATHWAY and ICON -->
<tr>
<td align="left" valign="top">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left" class="magin" id="rv_adjust_width_0" valign="top" width="780"><div class="content_margin_left_right">
<table cellpadding="0" cellspacing="0" width="100%"> <tr> <td align="left" id="layout_zone1" style="" valign="top" width="100%"><table cellpadding="1" cellspacing="1" class="rvFrame" style="width: 875px; height: 1011px" width="875">
<tbody>
<tr>
<td align="left" class="tdSolid" colspan="2" valign="top">
<div class="dashView">
<table cellpadding="8" cellspacing="0" width="100%">
<tbody>
<tr>
<td valign="top"><br/>
</td>
<td valign="top" width="99%">
<h1 align="center"><font style="font-size: medium"><u><font style="font-size: 24px">Welcome to the Tim &amp; Josh Company<font style="font-size: 24px">!</font></font></u></font><font style="font-size: medium"> </font></h1>
<div><font style="font-size: medium"> </font></div>
<div align="justify"><font style="font-size: medium">Our company is named after our first two boys, Timothy and Joshua. We are a family run company, and seek out new, unique, and high quality products to offer to the world. We only offer products that are well proven and can help people in their daily lives. </font></div>
<div align="justify"><font style="font-size: medium"></font> </div>
<div align="justify"><font style="font-size: medium">We are pleased to introduce our initial product line: Tim &amp; Josh Eczema Cream. This all natural cream is highly effective for people (including infants and children) who struggle with various skin problems due to eczema. Hand made by the doctor who created the unique combination, this cream has previously only been available to a limited audience. The Tim &amp; Josh Company has made arrangements to offer this cream to the public.</font></div>
<div align="justify"> </div>
<div align="justify"><font size="3">Please stay tuned for more products in the near future!</font> </div>
<div> <font style="font-size: medium">  </font></div>
<ul>
<li><font size="3">Visit our <a href="Products.php" title="">Products</a> page for more information on the Eczema Cream.<br/>
<br/></font></li>
<li><font size="3">Read through our <a href="Testimonials.php" title="">Testimonials</a> for real stories from customers.<br/>
</font><font style="font-size: medium"> </font></li>
<li><font style="font-size: medium">Check our <a href="FAQ.php" title="">FAQ</a> page for answers to common questions.<br/>
</font><font style="font-size: medium"> </font></li><font style="font-size: medium">
<li><font style="font-size: medium">Visit our <a href="Order.php" title="">Order</a> page for pricing and information on ordering.<br/>
</font> </li>
<li>Go to the <a href="Contact-Us.php" title="">Contact Us</a> page to submit an inquiry or to get our contact information.</li></font></ul><font style="font-size: medium">
<div> </div></font>
<div><font style="font-size: medium"> </font></div>
<div align="justify"><font style="font-size: medium">Thank you for your interest in our products! We welcome your comments and feedback. </font></div>
<div> </div>
<div> </div></td></tr>
<tr>
<td align="left" colspan="2" style="padding: 5px" valign="top"><br/>
</td></tr></tbody></table></div></td></tr>
<tr>
<td align="left" class="tdSolid" valign="top">
<div class="dashView">
<table cellpadding="5" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="left" colspan="2">
<p align="center"><span class="style1" style="padding: 5px"><img alt="" height="269" src="http://timandjosh.com/images/IMG_2777b.jpg" style="width: 404px; height: 269px" title="" width="404"/></span></p></td></tr>
<tr>
<td colspan="2">
<p align="center"> </p><br/>
</td></tr></tbody></table></div></td></tr></tbody></table></td> </tr></table>
</div></td>
</tr>
</table>
</td>
</tr>
<!-- Begin FOOTER -->
<tr>
<td align="center" class="magin"></td>
</tr>
<!-- End FOOTER -->
<tr><td align="center" class="content_margin_bottom"></td></tr>
<tr>
<td align="center" valign="bottom">
<table cellpadding="0" cellspacing="0">
<tr>
<td align="center" class="marginpw" valign="bottom"></td>
<td width="8"></td>
<td align="center" class="marginpw" valign="bottom"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="bgbshadow"><img alt="" height="18" src="images/spacer.gif" width="1"/></td>
</tr>
<tr>
<td class="bgfooter"><img alt="" height="30" src="images/spacer.gif" width="1"/></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
