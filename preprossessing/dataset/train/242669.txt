<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="John Mayer" name="Author"/>
<meta content="Mozilla/4.8 [en] (Win98; U) [Netscape]" name="GENERATOR"/>
<meta content="Give Peace A Chance, The Beatles, Lennon &amp; McCartney Song Lyrics" name="Description"/>
<meta content="Give Peace A Chance, Yesterday, Beatles, John Lennon &amp; Paul McCartney, Song Lyrics" name="KeyWords"/>
<title>Give Peace A Chance Lyrics, Beatles</title>
</head>
<body><b><font face="Arial Narrow"><font color=""><font size="+0"> </font></font></font></b><table align="center" border="0" cellpadding="0" cellspacing="0" div="" width="800">
<td bgcolor="#8CACEC" valign="top" width="136">

   <b><font size="+1"><a href="http://www.bluesforpeace.com"><img alt="Blues for Peace" border="0" height="75" src="/bfplink.gif" width="61"/></a>
<p><b>   <a href="http://www.bluesforpeace.com">Home Page</a>
<br/>   <a href="/warning.htm">Warning!</a>
<br/>   <a href="/guitar-licks.htm">Guitar Licks</a>
</b></p><p>   <a href="/music.htm">Blues Music</a>
<br/>   <a href="/blues-mp3.htm">Blues Mp3</a>
<br/>   <a href="/eric-clapton.htm">Blues Video</a>
</p><p>   <a href="/guitar-hero.htm">Guitar Hero</a>
<br/>   <a href="/blues-harp.htm">Blues Harp</a>
<br/>  <a href="/unsung-heroes.htm">Blues Heroes</a>
</p><p>   <a href="/guitar-tab.htm">Guitar TAB</a></p></font></b>
<br/>   <a href="/music-books.htm">Music Books</a>
<br/>   <p></p><center>
<p>
</p></center>
<p>   <a href="/search.htm">Search Site</a>
<br/>   <a href="mailto:info@bluesforpeace.com">Contact Us</a>
</p><p>
</p><center>
<b><a href="/killer-guitar.htm"><img alt="Killer Guitar Player" border="0" height="211" src="/images/clapton-blues.jpg" width="120"/><br/>Killer Guitar</a></b>
<p>
<script type="text/javascript"><!--
google_ad_client = "pub-9837599300929880";
/* 160x600, created 7/1/09 */
google_ad_slot = "4501863543";
google_ad_width = 160;
google_ad_height = 600;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</p><p>
</p><center><b>
<b></b></b><center><font size="-1"><b><b><b>Bluesmen<br/>
<a href="http://www.bluesforpeace.com/eric-clapton.htm">Eric Capton</a>
<br/><a href="http://www.bluesforpeace.com/zz-top.htm">ZZ TOP Band</a>
<br/><a href="http://www.bluesforpeace.com/acollins.htm">Albert Collins</a>
<br/><a href="http://www.bluesforpeace.com/hendrix.htm">Jimi Hendrix</a>
<br/><a href="http://www.bluesforpeace.com/joe-bonamassa.htm">Bonamassa</a>
<br/><a href="http://www.bluesforpeace.com/lynyrd-skynyrd.htm">Lynyrd Skynyrd</a><br/><a href="http://www.bluesforpeace.com/carlos-santana.htm">Santana</a>
<br/><a href="http://www.bluesforpeace.com/chuck-berry.htm">Chuck Berry</a>
<br/><a href="http://www.bluesforpeace.com/beatles.htm">The Beatles</a>
<br/><a href="http://www.bluesforpeace.com/rory-gallagher.htm">R. Gallagher</a>
<br/><a href="http://www.bluesforpeace.com/freddie-king.htm">Freddie King</a>
<br/><a href="http://www.bluesforpeace.com/bruce-springsteen.htm">B. Springsteen</a>
<br/><a href="http://www.bluesforpeace.com/rolling-stones.htm">Rolling Stones</a>
<br/><a href="http://www.bluesforpeace.com/johnny-wnter.htm">Johnny winter</a>
<br/><a href="http://www.bluesforpeace.com/buddy-guy.htm">Buddy Guy</a>
<br/><a href="http://www.bluesforpeace.com/blues-guitar-guru.htm">Your Band!</a>
</b></b></b><p>
</p></font></center></center></center></td>
<td bgcolor="#FFFFFF" valign="top" width="495">
<center>
<script type="text/javascript"><!--
google_ad_client = "pub-9837599300929880";
/* 468x15, created 10/4/08 */
google_ad_slot = "3227981281";
google_ad_width = 468;
google_ad_height = 15;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</center>
<h2>
<center>
<a href="http://www.bluesforpeace.com"><img alt="Blues For Peace" border="0" height="54" src="newbluelogo.JPG" width="356"/></a>
<br/><a href="/lyrics.htm">Blues Lyrics</a> | <a href="/jazz-song-lyrics.htm">Jazz</a> | <a href="/pop-song-lyrics.htm">Rock</a> | <a href="/pop-song-lyrics.htm">R&amp;B</a></center></h2>
<table bgcolor="#000000" border="0" cellpadding="0" cellspacing="1" width="468">
<tr height="35"><td>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" bgcolor="#000000" height="35"><a href="http://www.sheetmusicplus.com/a/logo.html?id=78010"><img border="0" height="35" src="http://gfxb.smpgfx.com/gfx/smp_logo_130_black.gif" width="130"/></a></td>
<td align="center" bgcolor="#6699FF"><font color="#FFFFFF" face="Verdana, Arial" size="2"><b>The World's largest selection - Shop now!</b></font></td>
</tr>
</table>
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="101" valign="middle">
<td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5833170"><img alt="Eric Clapton - Acoustic Classics (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxc.smpgfx.com/060x080/5833170.gif" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5833170">Eric Clapton - Acoustic C...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5833170"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td> <td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5039498"><img alt="Blues Rock Guitar Soloing (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxb.smpgfx.com/060x080/HL-00320395.GIF" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5039498">Blues Rock Guitar Soloing...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5039498"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td> <td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260"><img alt="The Best of the Allman Brothers Band (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxc.smpgfx.com/060x080/HL-00320230.GIF" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260">The Best of the Allman Br...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td>
</tr>
<tr height="101" valign="middle">
<td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4553677"><img alt="The Best of Lennon &amp; McCartney for Acoustic Guitar (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxb.smpgfx.com/060x080/HL-00320333.GIF" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4553677">The Best of Lennon &amp; McCa...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4553677"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td> <td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5391745"><img alt="Rush - Legendary Licks for Drums (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxc.smpgfx.com/060x080/5391745.gif" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5391745">Rush - Legendary Licks fo...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5391745"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td> <td align="center" height="101" valign="middle" width="70">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=7645239"><img alt="Paul Gilbert, Intense Rock: Complete (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" hspace="5" src="http://gfxb.smpgfx.com/060x080/7645239.gif" width="60"/>
</a>
</td>
<td valign="middle"><font face="Verdana, Arial" size="2">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=7645239">Paul Gilbert, Intense Roc...</a><br/>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=7645239"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</font></td>
</tr>
</table>
</td></tr>
</table>
<p>
<script type="text/javascript"><!--
google_ad_client = "pub-9837599300929880";
/* 468x60, created 6/3/08 */
google_ad_slot = "8839672642";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script>
</p><p>
</p><h1>Give Peace A Chance</h1>
<blockquote>
<center>
<h1>John Lennon &amp; Paul McCartney</h1>
</center>
<p>Two, one two three four<br/>
Ev'rybody's talking about<br/>
Bagism, Shagism, Dragism, Madism, Ragism, Tagism<br/>
This-ism, that-ism, is-m, is-m, is-m.</p>
<p>All we are saying is give peace a chance<br/>
All we are saying is give peace a chance</p>
<p>C'mon<br/>
Ev'rybody's talking about Ministers,<br/>
Sinisters, Banisters and canisters<br/>
Bishops and Fishops and Rabbis and Pop eyes,<br/>
And bye bye, bye byes.</p>
<p>All we are saying is give peace a chance<br/>
All we are saying is give peace a chance</p>
<p>Let me tell you now<br/>
Ev'rybody's talking about<br/>
Revolution, evolution, masturbation,<br/>
flagellation, regulation, integrations,<br/>
meditations, United Nations,<br/>
Congratulations.</p>
<p>All we are saying is give peace a chance<br/>
All we are saying is give peace a chance</p>
<p>Ev'rybody's talking about<br/>
John and Yoko, Timmy Leary, Rosemary,<br/>
Tommy Smothers, Bobby Dylan, Tommy Cooper,<br/>
Derek Taylor, Norman Mailer,<br/>
Alan Ginsberg, Hare Krishna,<br/>
Hare, Hare Krishna</p>
<p>All we are saying is give peace a chance<br/>
All we are saying is give peace a chance</p>
</blockquote>
<center><center>
<p>
</p></center>
<p>
</p><center>
<p>
</p><p><b><font size="-1"><a href="/bfp-classes.htm">Blues Guitar</a> <a href="/lyrics.htm">Blues Lyrics</a> | <a href="/blues-
mp3.htm">Blues Mp3</a> | <a href="/events.htm">Blues Concerts</a> | <a href="/eric-clapton.htm">Blues Video</a><br/><a href="/guitar-tab.htm">Guitar TAB</a> | <a href="/unsung-heroes.htm">Blues Heroes</a> | <a href="/music.htm">Blues Music</a> 
| | <a href="/allman-brothers.htm">Allman Bros.</a>
<br/><a href="/clapton.htm">Eric Clapton</a> | <a href="/bbking.htm ">BB King</a> | <a href="/hendrix.htm">Jimi Hendrix</a> | 
<a href="/acollins.htm">Albert Collins</a> | <a href="/stevie-ray-vaughan.htm">SRV</a> | <a href="/zz-top.htm">ZZ 
TOP</a></font></b></p></center>
<p><font size="-1"><b>© 1998-2008 Blues for Peace Corporation. All rights reserved.</b></font></p></center>
</td>
<td bgcolor="#e5ecf9" valign="top" width="160">
p&gt;   <b><font size="+1">Play the Blues! 
<br/>   <a href="/blues-licks-pg1.htm">Blues Licks</a>
<br/>   <a href="/blueschords.htm">Blues Chords</a>
<br/>   <a href="/blues-guitar-guru.htm">Guitar Guru</a>
<br/>   <a href="/blues-guitar-riffs.htm">Blues Riffs</a>
<br/>   <a href="/bass-guitar-part.htm">Bass Guitar</a>
<br/>   <a href="/blues-jam-tracks.htm">Blues Jam Trax</a>
</font></b>
<p>
</p><p></p><center>
<b>
<font size="-0">Guitar TAB</font>
<font size="-1"><br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=b.b.%20king">B.B. King</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=eric%20clapton">Eric Clapton</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=beatles">The Beatles</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=chuck%20berry">Chuck Berry</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=jimi%20hendrix">Jimi Hendrix</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=led%20zeppelin">Led Zeppelin</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=lynyrd%20skynyrd">Lynyrd Skynyrd</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=allman%20brothers">Allman Brothers</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=clearwater%20revival">Creedence..</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=bonnie%20raitt">Bonnie Raitt</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=Red%20Hot%20Chili%20Peppers">Chili Peppers</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=rolling%20stones">Rolling Stones</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=Stevie%20Ray%20Vaughan">Stevie Ray V..</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=guns%20roses">Guns &amp; Roses</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=bruce%20springsteen">B. Springsteen</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=dave%20matthews">Dave Matthews</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=ray%20charles">Ray Charles</a>
<br/><a href="http://www.sheetmusicplus.com/a/phrase.html?id=78010&amp;phrase=zz%20top">ZZ TOP</a></font></b></center>
<table bgcolor="#000000" border="0" cellpadding="0" cellspacing="1" width="120">
<tr>
<td align="center" height="30">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=3425047"><img border="0" height="30" src="http://gfxb.smpgfx.com/gfx/smp_logo_100_black.gif" width="100"/></a>
</td>
</tr>
<tr>
<td>
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="2" width="118">
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=3425047"><img alt="Look inside this title" border="0" height="15" src="http://gfxb.smpgfx.com/smp/lookinside-sr.gif" vspace="3" width="60"/><br/><img alt="Blues Solos For Guitar - sheet music at www.sheetmusicplus.com" border="0" height="80" src="http://gfxb.smpgfx.com/060x080/3425047.gif" width="60"/></a>
</td>
</tr>
<tr>
<td align="center"><font face="Arial, Helvetica" size="2"><b>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=3425047">Blues Solos For Guitar</a>
</b></font></td>
</tr>
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=3425047"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<p>
</p><table bgcolor="#000000" border="0" cellpadding="0" cellspacing="1" width="120">
<tr>
<td align="center" height="30">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260"><img border="0" height="30" src="http://gfxb.smpgfx.com/gfx/smp_logo_100_black.gif" width="100"/></a>
</td>
</tr>
<tr>
<td>
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="2" width="118">
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260"><img alt="The Best of the Allman Brothers Band (DVD) - sheet music at www.sheetmusicplus.com" border="0" height="80" src="http://gfxb.smpgfx.com/060x080/HL-00320230.GIF" vspace="3" width="60"/></a>
</td>
</tr>
<tr>
<td align="center"><font face="Arial, Helvetica" size="2"><b>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260">The Best of the Allman Br...</a>
</b></font></td>
</tr>
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=4711260"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<p>
</p><table bgcolor="#000000" border="0" cellpadding="0" cellspacing="1" width="120">
<tr>
<td align="center" height="30">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5037169"><img border="0" height="30" src="http://gfxb.smpgfx.com/gfx/smp_logo_100_black.gif" width="100"/></a>
</td>
</tr>
<tr>
<td>
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="2" width="118">
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5037169"><img alt="Look inside this title" border="0" height="15" src="http://gfxb.smpgfx.com/smp/lookinside-sr.gif" vspace="3" width="60"/><br/><img alt="The Best of ZZ Top - sheet music at www.sheetmusicplus.com" border="0" height="80" src="http://gfxc.smpgfx.com/060x080/5037169.gif" width="60"/></a>
</td>
</tr>
<tr>
<td align="center"><font face="Arial, Helvetica" size="2"><b>
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5037169">The Best of ZZ Top</a>
</b></font></td>
</tr>
<tr>
<td align="center">
<a href="http://www.sheetmusicplus.com/a/item.html?id=78010&amp;item=5037169"><img border="0" height="19" src="http://gfxb.smpgfx.com/smp/smp_but_buynow.gif" vspace="3" width="63"/></a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<p>
</p><p>
</p><p>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-4657361-1";
urchinTracker();
</script>
</p></td></table></body>
</html>
