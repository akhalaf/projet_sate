<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/template/css/highslide.css" rel="stylesheet"/>
<link href="/template/css/ui.dialog.css" rel="stylesheet"/>
<link href="/template/css/main.css?v=5.1980" rel="stylesheet"/>
<link href="/template/css/style_new.css?v=34909" rel="stylesheet"/>
<link href="/template/lib/owl.carousel.css" rel="stylesheet"/>
<link href="/template/lib/owl.theme.default.css" rel="stylesheet"/>
<link href="/template/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<script src="/template/js/highslide-full.js"></script>
<script src="/template/js/lib/jquery.min.js"></script>
<!--   <script
src="http://code.jquery.com/jquery-1.8.3.min.js"
integrity="sha256-YcbK69I5IXQftf/mYD8WY0/KmEDCv1asggHpJk1trM8="
crossorigin="anonymous"></script> -->
<script src="/template/js/lib/pxgradient-1.0.2.jquery.js"></script>
<script src="/template/js/pages.js?v=1" type="text/javascript"></script>
<script src="/template/js/hs_config2.js"></script>
<meta content="ошибка, 404, страница, находить" name="keywords"/>
<meta content="Ошибка 404 - адрес страницы введен неправильно, либо страница отсутствует." name="description"/>
<meta content="https://www.bordur32.ru/re23zcb7" name="canonical"/>
<title>
    Ошибка 404 - страница не найдена  </title>
</head>
<body>
<div class="banner-new">
<div class="banner-new-block">
<img class="close-new-banner" src="/include/close.png"/>
<div class="some-earth"><img src="/include/photo.png"/></div>
<div class="text">
<p>В продаже имеется известковый
  щебень фракция 5-20.</p>
<p class="price">1100 р/тонна.</p>
</div>
</div>
</div>
<style>
  .body.no_bg{
    border-top: 5px solid #fdca00;
    padding-top: 30px;
  }
  #header{
    height: 175px;
  }
  </style>
<style>
  div#sub_menu {
    background: white !important;
  }
  #sub_menu .active{
    background: #e2e2e2;
  }
</style>
<div id="fix-header"></div>
<div class="body header-fixed">
<div class="wrapper">
<header id="header">
<a href="/"><img alt="Тротуарная плитка, бордюры, элементы колодцев" height="116" id="logo_img" src="/template/images/logo-new.png" title="Тротуарная плитка, бордюры, элементы колодцев" width="320"/></a>
<div class="logo_img_block"><a href="/"><img alt="Тротуарная плитка, бордюры, элементы колодцев" height="116" id="logo_img_m" src="/template/images/logo_mob.png" title="Тротуарная плитка, бордюры, элементы колодцев" width="320"/></a></div>
<div id="phones">
<span class="phone-code">+7 (4832)</span><span class="phone"><a href="tel:+7 (4832) 33-52-04"> 33-52-04</a></span><span class="divider"></span><span class="phone"><a href="tel:75-89-99">75-89-99</a></span><span class="divider"></span><span class="phone"><a href="tel:63-64-22">63-64-22</a></span> <a class="button_send_callback_mob mini" href="#"></a>
<a class="button_vk mob mini" href="http://vk.com/bordur32" rel="nofollow"></a>
<a class="button_ok mob mini" href="https://ok.ru/group/55202067316755" rel="nofollow"></a>
</div>
<!-- <div id="delivery"></div> -->
<a class="button_send_callback no-mob" href="#">Заказать обратный звонок</a>
<a class="link-map no-mob" href="/contacts/">г. Брянск, ул. Транспортная, д. 5, оф. 412</a>
<a class="button_vk no-mob" href="http://vk.com/bordur32" rel="nofollow">Вконтакте</a>
<a class="button_ok no-mob" href="https://ok.ru/group/55202067316755" rel="nofollow">Одноклассники</a>
<div class="contact_mob">
<a class="link-map-mob" href="/contacts/">г. Брянск, ул. Транспортная, д. 5, оф. 412</a>
<a class="button_send_callback_mob" href="#">Заказать обратный звонок</a>
<a class="button_vk mob" href="http://vk.com/bordur32" rel="nofollow">ВК</a>
<a class="button_ok mob" href="https://ok.ru/group/55202067316755" rel="nofollow">ОК</a>
</div>
<nav id="main"><ul><li><a href="/">Главная</a></li>
<li><a href="/about/">О компании</a></li>
<li><span class="sub_m">Продукция
          <div id="sub_menu"><ul id="sub_list"><li><a href="/catalog/trotuarnaja-plitka/">Тротуарная плитка</a></li><li><a href="/catalog/bordyury/">Бордюры</a></li><li><a href="/catalog/elementy-kolodcev/">Железобетонные кольца</a></li><li><a href="/catalog/fundamentnyje-bloki/">Фундаментные блоки</a></li><li><a href="/catalog/stenovyje-bloki/">Стеновые блоки</a></li><li><a href="/catalog/tovarnii-beton/">Товарный бетон</a></li><li><a href="/catalog/rastvor/">Раствор строительный</a></li><li><a href="/catalog/scheben/">Щебень</a></li></ul></div>
</span></li><li><a href="/partner/">Партнеры</a></li>
<li><a href="/gallery/">Фотогалерея</a></li>
<li><a href="/delivery/">Доставка и оплата</a></li>
<li><a href="/contacts/">Контакты</a></li>
</ul></nav> <img alt="" height="142" id="new-year" src="/template/img/new_year_image.png" title="" width="1159"/>
<nav class="top_menu_mob">
<div class="top_menu_button">≡</div>
<div class="menu_mob">
<ul><li><a href="/">Главная</a></li>
<li><a href="/about/">О компании</a></li>
<li><span class="sub_m">Продукция
          <div id="sub_menu"><ul id="sub_list"><li><a href="/catalog/trotuarnaja-plitka/">Тротуарная плитка</a></li><li><a href="/catalog/bordyury/">Бордюры</a></li><li><a href="/catalog/elementy-kolodcev/">Железобетонные кольца</a></li><li><a href="/catalog/fundamentnyje-bloki/">Фундаментные блоки</a></li><li><a href="/catalog/stenovyje-bloki/">Стеновые блоки</a></li><li><a href="/catalog/tovarnii-beton/">Товарный бетон</a></li><li><a href="/catalog/rastvor/">Раствор строительный</a></li><li><a href="/catalog/scheben/">Щебень</a></li></ul></div>
</span></li><li><a href="/partner/">Партнеры</a></li>
<li><a href="/gallery/">Фотогалерея</a></li>
<li><a href="/delivery/">Доставка и оплата</a></li>
<li><a href="/contacts/">Контакты</a></li>
</ul> </div>
</nav>
</header>
<!--.wrapper--></div>
<!--div.body--></div>
<!-- <div class="pod-header"></div> -->
<!-- #header-->
<div id="wrapper_slider">
<div class="slaid_mob">
<div class="block_share" style="background-repeat-x: repeat;"><span class="text_share">До 15 ноября скидка 30 рублей с квадратного метра при оплате наличными.</span></div>
</div>
</div>
<div class="body no_bg">
<div class="wrapper">
<div id="content" style="padding-bottom: 20px;">
<div id="real-content">
<h1>Ошибка 404</h1>
<p>Запрашиваемая Вами страница не найдена.</p>
<img float:center="" src="/upload/editor/error-404.jpg" style="width:920px; height:380px;"/>
<p>Перейдите на <a href="/">главную</a> страницу сайта</p>
</div>
</div>
</div>
<!--wrapper-->
<!-- #content-->
<!--id wrapper</div>-->
<!-- #wrapper -->
<!-- <div class="polosa vniz fot_vniz">
  <div></div>
</div> -->
<div style="clear:both; height: 170px; width: 100%;"></div>
<div class="wrap-footer">
<footer id="footer">
<div class="left">
<div class="left_map">
<div class="site-name">
  		  ООО "Брянский бордюр", 2013 - 2021  		</div>
<div class="address">
          г. Брянск, ул. Транспортная, д. 5, оф. 412        </div>
<ul class="site-phones"><li><a href="tel:+7 (4832) 63-64-22">+7 (4832) 63-64-22</a></li><li><a href="tel:+7 (4832) 75-89-99">+7 (4832) 75-89-99</a></li></ul> <div class="divider">
</div>
</div>
<!--div class="middle m_adress"-->
<div id="footer_address">
</div>
<div class="divider">
</div>
<div class="clear">
</div>
</div>
<div class="middle m_product">
<div id="footer_products">
<ul><li><a href="/catalog/trotuarnaja-plitka/">Тротуарная плитка</a></li><li><a href="/catalog/bordyury/">Бордюры</a></li><li><a href="/catalog/elementy-kolodcev/">Железобетонные кольца</a></li><li><a href="/catalog/fundamentnyje-bloki/">Фундаментные блоки</a></li><li><a href="/catalog/stenovyje-bloki/">Стеновые блоки</a></li></ul><ul style="padding-left:10px;"><li><a href="/catalog/tovarnii-beton/">Товарный бетон</a></li><li><a href="/catalog/rastvor/">Раствор строительный</a></li><li><a href="/catalog/scheben/">Щебень</a></li><li><a href="/ukladka-trotuarnoj-plitki/">Укладка тротуарной плитки</a></li><li><a href="/decorativnye-bloki/">Декоративные блоки</a></li></ul> </div>
<div class="divider">
</div>
<div class="clear">
</div>
</div>
<div class="right">
<div class="develop">
<a href="https://www.obrazstroy.ru">Создание и продвижение сайта</a>
<div style="width:100%">
<div id="logo"></div>
</div>
</div>
<div class="footer_pod" style="clear:both; margin-top:65px; color:#9d9d9d; font-family: HelveticaNeue-Light;">
      Поделиться с друзьями:<br/>
<script type="text/javascript">(function(w,doc) {
  if (!w.__utlWdgt ) {
      w.__utlWdgt = true;
      var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
      s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
      s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
      var h=d[g]('body')[0];
      h.appendChild(s);
  }})(window,document);
  </script>
<div class="uptolike-buttons" data-background-alpha="0.0" data-background-color="#ffffff" data-buttons-color="#FFFFFF" data-counter-background-alpha="1.0" data-counter-background-color="#ffffff" data-exclude-show-more="false" data-following-enable="false" data-hover-effect="scale" data-icon-color="#ffffff" data-like-text-enable="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-mobile-view="true" data-mode="share" data-orientation="horizontal" data-pid="1545821" data-preview-mobile="false" data-selection-enable="false" data-share-counter-size="12" data-share-counter-type="disable" data-share-shape="rectangle" data-share-size="30" data-share-style="1" data-sn-ids="fb.vk.tw.ok.gp.mr." data-text-color="#000000" data-top-button="false"></div>
</div>
</div>
</footer>
</div>
<div class="metrics" style="display: none;">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t18.2;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='88' height='31'><\/a>")
//--></script><!--/LiveInternet-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter10772185 = new Ya.Metrika({id:10772185,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/10772185" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4606037-1', 'bordur32.ru');
  ga('send', 'pageview');

</script>
</div>
<div class="dialogs" id="callback_form">
<!--<div style="padding: 10px 25px; overflow: visible;">
    <form name="form_callback" id="form_callback" method="post" action="/">
      <input type="hidden" name="submit_callback" value="submit_callback" />
      <input type="hidden" name="form[robot_callback]" value="" />

      <div class="wrap_item_form">
        <div class="form_title">Ваше имя <span class="red">*</span></div>
        <input name="form[name_callback]" id="name_callback" type="text" />
        <div class="alert"><span>Введите имя</span></div>
      </div>

      <div class="wrap_item_form">
        <div class="form_title">Телефон <span class="red">*</span></div>
        <input name="form[phone_callback]" id="phone_callback" type="text"/>
        <div class="alert"><span>Введите корректный телефон</span></div>
      </div>

      <div class="wrap_item_form">
        <div class="form_title">Текст сообщения</div>
        <textarea name="form[quest_callback]" id="quest_callback" class="sm_area"></textarea>
        <div class="alert"><span>Введите текст сообщения</span></div>
      </div>
      <div class="wrap_item_form">
        <input id="check" style="width:15px;height: 17px;" type="checkbox" name="check" />
        <span style="font-size: 14px;font-family: HelveticaNeue-Light;">Я принимаю <a href="/politika-kon/">Условия использования персональных данных</a></span>
        <div  class="alert personal"><span>Примите условие</span></div>
      </div>
      <button id="submit_callback" value="Отправить" type="submit" name="submit_callback" class="btn site-btn">Оставить заявку</button>

    </form>
    </div>-->
</div>
<div class="dialogs" id="success-message_callback">
</div>
<script src="/template/js/lib/jquery.ui.js"></script>
<script src="/template/js/jquery.jcarousel.js"></script>
<script src="/template/js/main.js"></script>
<script src="/template/js/carousels.js"></script>
<!--div.body--></div>
</body>
</html>