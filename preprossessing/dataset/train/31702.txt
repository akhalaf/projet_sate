<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]--><!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <![endif]--><!--[if IE 8]><html class="no-js lt-ie9" <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- <meta name="description" content="Home - 500 Startups"> -->
<title>Home - 500 Startups</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css" rel="stylesheet"/>
<link href="/site/themes/default/assets/app.css" rel="stylesheet"/>
<link href="https://www.500.co/" rel="canonical"/>
<meta content="Started in 2010 with its flagship early-stage startup accelerator, 500 Startups is the most active global venture capital firm known for its various funds and founder programs across the globe. The firm has grown to a family of over 23 funds and has invested in over 2400 companies internationally." name="description"/>
<meta content="" name="keywords"/>
<!-- Minimal -->
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=32&amp;h=32&amp;fit=fit_center&amp;s=568a50ed9e2631b3b405d77917ce9283" rel="shortcut icon" type="image/png"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=152&amp;h=152&amp;fit=fit_center&amp;s=1c1bf10a970081caf2135825c7ce513e" rel="android-touch-icon"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=32&amp;h=32&amp;fit=fit_center&amp;s=568a50ed9e2631b3b405d77917ce9283" rel="icon" type="image/png"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=152&amp;h=152&amp;fit=fit_center&amp;s=1c1bf10a970081caf2135825c7ce513e" rel="icon" sizes="152x152"/>
<!-- Apple -->
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=152&amp;h=152&amp;fit=fit_center&amp;s=1c1bf10a970081caf2135825c7ce513e" rel="apple-touch-icon"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=76&amp;h=76&amp;fit=fit_center&amp;s=d75b41c0482c51a7aa0b961cd742e711" rel="apple-touch-icon" sizes="76x76"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=120&amp;h=120&amp;fit=fit_center&amp;s=9607efa46c5f347144c9fe26e0cf3f35" rel="apple-touch-icon" sizes="120x120"/>
<link href="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=152&amp;h=152&amp;fit=fit_center&amp;s=1c1bf10a970081caf2135825c7ce513e" rel="apple-touch-icon" sizes="152x152"/>
<!-- Microsoft -->
<meta content="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=150&amp;h=150&amp;fit=fit_center&amp;s=65fa93e070bb9a62ee843efcec13b8b4" name="msapplication-square150x150logo"/>
<meta content="https://www.500.co/img/assets/general/500-logo-white-transparent.png?w=310&amp;h=150&amp;fit=crop&amp;s=aab5e16dcf740938ac7436052b9bffa2" name="msapplication-wide310x150logo"/>
<!-- Facebook Data -->
<meta content="500 Startups - Home" property="og:title"/>
<meta content="500 Startups" property="og:site_name"/>
<meta content="https://www.500.co" property="og:url"/>
<meta content="https://www.500.co/assets/general/og-500.png" property="og:image"/>
<meta content="Started in 2010 with its flagship early-stage startup accelerator, 500 Startups is the most active global venture capital firm known for its various funds and founder programs across the globe. The firm has grown to a family of over 23 funds and has invested in over 2400 companies internationally." property="og:description"/>
<!--Twitter Card Data-->
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@500startups" name="twitter:site"/>
<meta content="@500startups" name="twitter:creator"/>
<meta content="500 Startups - Home" name="twitter:title"/>
<meta content="Started in 2010 with its flagship early-stage startup accelerator, 500 Startups is the most active global venture capital firm known for its various funds and founder programs across the globe. The firm has grown to a family of over 23 funds and has invested in over 2400 companies internationally." name="twitter:description"/>
<meta content="https://www.500.co/assets/general/og-500.png" name="twitter:image"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-61696460-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61696460-2');
</script>
<!-- Start of HubSpot Embed Code -->
<script async="" defer="" id="hs-script-loader" src="//js.hs-scripts.com/8199345.js" type="text/javascript"></script>
<!-- End of HubSpot Embed Code -->
</head>
<body>
<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
<div class="container">
<a class="navbar-brand" href="/" style="background-image: url('assets/general/500-logo-black-transparent.svg');">500 Startups</a>
<button aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNavDropdown" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-between" id="navbarNavDropdown">
<ul class="navbar-nav">
<li class="nav-item ">
<a class="nav-link" href="/about">
                            About
                        </a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/startups">
                            Portfolio
                        </a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/funds">
                            Funds
                        </a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/accelerators">
                            Accelerators
                        </a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/ecosystems">
                            Ecosystems
                        </a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/events">
                            Events
                        </a>
</li>
<li class="nav-item">
<a class="nav-link" href="/blog" target="_blank">Insights</a>
</li>
</ul>
<ul class="navbar-nav">
<li class="nav-item dropdown mb-3 mb-lg-0">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdownMenuLink">
<i class="fa fa-fw fa-globe"></i> 500 Worldwide
                        </a>
<div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
<a class="dropdown-item" href="https://durians.500.co/" target="_blank">500 Southeast Asia</a>
<a class="dropdown-item" href="https://www.500canada.ca/" target="_blank">500 Canada</a>
<a class="dropdown-item" href="https://istanbul.500.co/" target="_blank">500 Istanbul</a>
<a class="dropdown-item" href="https://korea.500.co/" target="_blank">500 Korea</a>
<a class="dropdown-item" href="https://latam.500.co/" target="_blank">500 Latin America</a>
<a class="dropdown-item" href="https://mena.500.co/" target="_blank">500 MENA</a>
<a class="dropdown-item" href="https://vietnam.500.co/" target="_blank">500 Vietnam</a>
<a class="dropdown-item" href="https://thailand.500.co/" target="_blank">500 Thailand</a>
<a class="dropdown-item" href="https://brasil.500.co/" target="_blank">500 Brasil</a>
</div>
</li>
<li class="nav-item dropdown mb-3 mb-lg-0">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdownMenuLink">
                                English
                            </a>
<div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
<a class="dropdown-item" href="/">English</a>
</div>
</li>
</ul>
</div>
</div>
</nav>
</header>
<div id="billboard">
<div class="parallax parallax-container">
<div class="parallax-item ">
<img alt="" class="w-100" src="/img/assets/general/vietnam.jpg?w=1920&amp;fit=crop&amp;s=d45569ddb87a01f879012eb215ee6752"/>
</div>
<div class="parallax-item parallax-up ">
<img alt="" class="w-100" src="/img/assets/general/vietnam-fore.png?w=1920&amp;fit=crop&amp;s=d209517bd681dd992fd5cb2e438f3cce"/>
</div>
<div class="data d-flex flex-column justify-content-center align-items-center h-100 position-absolute parallax-down">
<div class="container">
<div class="row justify-content-center text-center">
<div class="col-12 col-md-8">
<h1>Uplifting people and economies through entrepreneurship</h1>
</div>
</div>
</div>
<div class="anchor">
<a href="#"><i class="fa-fw fas fa-arrow-down"></i></a>
</div>
</div>
</div>
<img alt="" class="fallback" src="/img/assets/general/vietnam.jpg?w=800&amp;fit=crop&amp;s=9ca4d0b6535ec2c11836377403e51b6a"/>
<div class="data d-flex flex-column justify-content-center align-items-center d-lg-none d-md-none">
<div class="container">
<div class="row justify-content-center text-center">
<div class="col-12 col-md-8">
<h1>Uplifting people and economies through entrepreneurship</h1>
</div>
</div>
</div>
<div class="anchor">
<a href="#"><i class="fa-fw fas fa-arrow-down"></i></a>
</div>
</div>
</div>
<section class="block stats-block">
<div class="stats-content">
<div class="container">
<div class="row">
<div class="col-12 col-md-8 mb-2 mb-md-0">
<div class="">
<h2 class="title">Most active early stage investor in the world</h2> <p>500 Startups is the most active global venture capital firm of 2019. We’re on a mission to uplift people and economies around the world through entrepreneurship.
</p>
</div>
<div class="row my-4">
<div class="col-md-6">
<h5 class="primary-statistic">#1 <span>Most Active VC Globally in Exits</span></h5>
</div>
<div class="col-md-6">
<h5 class="primary-statistic">#1 <span>Most Active Global Investor by VC Deal Count</span></h5>
</div>
</div>
<div class="footnote">
<p>Source: <a href="http://bit.ly/37ExjPP" target="_blank">PitchBook</a>
</p>
</div>
</div>
<div class="col-md-1 d-none d-md-flex justify-content-center">
<div class="divider"></div>
</div>
<div class="col-12 col-md-3 d-flex flex-column justify-content-center">
<h5 class="secondary-statistic">2400+ <span>Startups Invested</span></h5><h5 class="secondary-statistic">75+ <span>Countries</span></h5><h5 class="secondary-statistic">5000+ <span>Founders in our Portfolio</span></h5>
</div>
</div>
</div>
</div>
<div class="stats-block-support collection-slider-block collection-slider-block-">
<div class="swiper-overflow-fix">
<div class="swiper-container">
<div class="swiper-wrapper container">
<div class="swiper-slide">
<div class="card card-style-region">
<a class="link" href="/startups?filter=1&amp;region=US+-+Non+CA,LatAm,US+-+CA,Canada"></a>
<img alt="" class="card-img" height="800" src="/img/assets/general/regions/america-cover.png?w=450&amp;h=800&amp;fit=crop&amp;s=5c428e2d691d5dfd11cd93721e1e7571" width="450"/>
<div class="card-img-overlay">
<h5 class="card-title">Americas</h5>
<img alt="" class="logo" height="200" src="/img/assets/general/regions/america.png?w=200&amp;h=200&amp;fit=crop&amp;s=704b04a2cf07fb7a9a5ccc6029af7064" width="200"/>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="card card-style-region">
<a class="link" href="/startups?filter=1&amp;region=SE+Asia,East+Asia,South+Asia"></a>
<img alt="" class="card-img" height="800" src="/img/assets/general/singapore-2570184_1280.jpg?w=450&amp;h=800&amp;fit=crop&amp;s=cb75e3fb03e9b81bc70767f061e9d04d" width="450"/>
<div class="card-img-overlay">
<h5 class="card-title">Asia</h5>
<img alt="" class="logo" height="200" src="/img/assets/general/regions/asia.png?w=200&amp;h=200&amp;fit=crop&amp;s=f7042aab3545b6e1788e0aa87acfba5b" width="200"/>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="card card-style-region">
<a class="link" href="/startups?filter=1&amp;region=Europe"></a>
<img alt="" class="card-img" height="800" src="/img/assets/general/london.jpeg?w=450&amp;h=800&amp;fit=crop&amp;s=6159e8b14a5b9e32c5669464ec07d1e6" width="450"/>
<div class="card-img-overlay">
<h5 class="card-title">Europe</h5>
<img alt="" class="logo" height="200" src="/img/assets/general/regions/europe.png?w=200&amp;h=200&amp;fit=crop&amp;s=8ca99637d245de80c5ccffa366759d6e" width="200"/>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="card card-style-region">
<a class="link" href="/startups?filter=1&amp;region=ME+%26+Africa"></a>
<img alt="" class="card-img" height="800" src="/img/assets/general/dubai.jpg?w=450&amp;h=800&amp;fit=crop&amp;s=68c7ca1c282944069df500b02a475321" width="450"/>
<div class="card-img-overlay">
<h5 class="card-title">Middle East &amp; Africa</h5>
<img alt="" class="logo" height="200" src="/img/assets/general/logo-mena-a.png?w=200&amp;h=200&amp;fit=crop&amp;s=f04042f88dfe52957140aa90682af85e" width="200"/>
</div>
</div>
</div>
<div class="swiper-slide">
<div class="card card-style-region">
<a class="link" href="/startups?filter=1&amp;region=Australia+%26+NZ"></a>
<img alt="" class="card-img" height="800" src="/img/assets/general/sydney.jpg?w=450&amp;h=800&amp;fit=crop&amp;s=1818de0b1d31d834ed748c21db6afd4f" width="450"/>
<div class="card-img-overlay">
<h5 class="card-title">Oceania</h5>
<img alt="" class="logo" height="200" src="/img/assets/general/regions/oceania.png?w=200&amp;h=200&amp;fit=crop&amp;s=81caeceb080a6249458a13f320fd9aa0" width="200"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="block py-5 bg-white">
<div class="text-center mb-5">
<h2 class="title">What We Do</h2>
</div>
<div class="container">
<div class="card-row row">
<div class="col-12 ">
<div class="card card-hr">
<img alt="" class="card-img order-1" height="400" src="/img/assets/general/multi-logos_updated_white.png?w=550&amp;h=400&amp;fit=crop&amp;s=68f4aabe0db73a0037a86b770c4245cf" width="550"/>
<div class="card-body order-0">
<p class="card-subtitle">Portfolio</p> <h4 class="card-title mb-2">We create diversified tech portfolios.</h4> <div class="card-text"><p>500 Startups casts a wide net, and we don’t shy away from underserved markets. We review more deals than just about anyone, which gives us a comprehensive view of markets and trends worldwide. Our goal is to back the best, regardless of race, gender and geography.
</p>
</div>
<div>
<a class="card-btn" href="/startups">Learn More →</a>
</div>
</div>
</div>
</div>
<div class="col-12 col-md-6">
<div class="card ">
<img alt="" class="card-img " height="400" src="/img/assets/general/accelerators/sf-accelerator.png?w=550&amp;h=400&amp;fit=crop&amp;s=17b6eb525a7b6c11f4772cd02668903b" width="550"/>
<div class="card-body ">
<p class="card-subtitle">Funds</p> <h4 class="card-title mb-2">We back the world’s most talented entrepreneurs.</h4> <div class="card-text"><p>Great ideas exist everywhere, but they need capital. Networks are also critical components in setting up founders for success. We provide early-stage investment through our global funds and regional investment vehicles in Southeast Asia, MENA, South Korea, Latin America, Turkey, Vietnam and Thailand.
</p>
</div>
<div>
<a class="card-btn" href="/funds">Learn More →</a>
</div>
</div>
</div>
</div>
<div class="col-12 col-md-6">
<div class="card ">
<img alt="" class="card-img " height="400" src="/img/assets/general/ecosytems/home_et.jpg?w=550&amp;h=400&amp;fit=crop&amp;s=0e9bfeef88fc1fdf62a8b8292f066e4b" width="550"/>
<div class="card-body ">
<p class="card-subtitle">Ecosystems</p> <h4 class="card-title mb-2">We build thriving startup ecosystems.</h4> <div class="card-text"><p>500 Startups leverages its global expertise to build comprehensive entrepreneurial ecosystems. We partner with governments and foundations to build tailored accelerator programs, as well as, partner with corporations to facilitate meaningful relationships with startups. In addition to all of this, we also teach the art of investing.
</p>
</div>
<div>
<a class="card-btn" href="/ecosystems">Learn More →</a>
</div>
</div>
</div>
</div>
<div class="col-12 ">
<div class="card card-hr">
<img alt="" class="card-img order-1" height="400" src="/img/assets/general/b25-class-photo---cropped.jpg?w=550&amp;h=400&amp;fit=crop&amp;s=13c5a34f12afe9e9952415863b73b824" width="550"/>
<div class="card-body order-0">
<p class="card-subtitle">ACCELERATORS</p> <h4 class="card-title mb-2">We bring startups to the next level.</h4> <div class="card-text"><p>500 Startups started as an accelerator in the heart of Silicon Valley, and we have expanded our founder programs across the globe. Learn more about our founder programs for Seed and Series A stage startups and apply today.
</p>
</div>
<div>
<a class="card-btn" href="/accelerators">Learn More →</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="grid-block block py-5">
<div class="container">
<div class="text-center mb-5">
<h2 class="title">Meet a few of our global stars</h2>
</div>
<div class="grid-container">
<div class="row no-gutters">
<div class="col-12 col-md-6">
<ul class="nav nav-tabs h-100" id="globalStarsTab" role="tablist">
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link active " data-toggle="tab" href="#globalStarsTab1" id="globalstars-1" role="tab" style="--bg-color: #d71149">
<img alt="" class="img-fluid" src="assets/general/portfolio/bukalapak-1588878942.png"/>
</a>
</li>
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link " data-toggle="tab" href="#globalStarsTab2" id="globalstars-2" role="tab" style="--bg-color: #64dc94">
<img alt="" class="img-fluid" src="assets/general/1280px-credit_karma_logo.svg.png"/>
</a>
</li>
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link " data-toggle="tab" href="#globalStarsTab3" id="globalstars-3" role="tab" style="--bg-color: #172241">
<img alt="" class="img-fluid" src="assets/general/portfolio/talkdesk.png"/>
</a>
</li>
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link " data-toggle="tab" href="#globalStarsTab4" id="globalstars-4" role="tab" style="--bg-color: #00b150">
<img alt="" class="img-fluid" src="assets/general/portfolio/grab.png"/>
</a>
</li>
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link " data-toggle="tab" href="#globalStarsTab5" id="globalstars-5" role="tab" style="--bg-color: #000000">
<img alt="" class="img-fluid" src="assets/general/portfolio/therealreal.png"/>
</a>
</li>
<li class="nav-item">
<a aria-controls="home" aria-selected="true" class="ratio r16-9 nav-link alt" data-toggle="tab" href="#globalStarsTab6" id="globalstars-6" role="tab">
<img alt="" class="img-fluid" src="assets/general/portfolio/canva.png"/>
</a>
</li>
</ul>
</div>
<div class="col-12 col-md-6">
<div class="tab-content">
<div aria-labelledby="globalstars-1" class="tab-pane fade show active" id="globalStarsTab1" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/portfolio/featured-bukalapak.png?w=600&amp;h=517&amp;fit=crop&amp;s=758e59dda010a6e37a08e2b3d9a996c0"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">Indonesia</span>
</div>
<h4 class="card-title">Achmad Zaky</h4>
<p class="card-text">One of the largest e-commerce companies in Indonesia with $1B+ valuation</p>
</div>
</div>
</div>
<div aria-labelledby="globalstars-2" class="tab-pane fade " id="globalStarsTab2" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/credit_karma_sasan-goodarzi-and-kenneth-lin.jpg?w=600&amp;h=517&amp;fit=crop&amp;s=63326d54fdf00673ef01e5c850595869"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">USA</span>
</div>
<h4 class="card-title">Kenneth Lin, Nichole Mustard, Ryan Graciano</h4>
<p class="card-text">A personal finance website dedicated to helping consumers better understand the power of their credit and overall financial health</p>
</div>
</div>
</div>
<div aria-labelledby="globalstars-3" class="tab-pane fade " id="globalStarsTab3" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/home/talkdesk-1280x720.jpg?w=600&amp;h=517&amp;fit=crop&amp;s=91efca3c3f385dec39a1373f9d4b9d9d"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">Portugal</span>
</div>
<h4 class="card-title">Tiago Paiva, Cristina Fonseca</h4>
<p class="card-text">One of the largest cloud-based contact center, unified communications and artificial intelligence software providers with $1B+ valuation</p>
</div>
</div>
</div>
<div aria-labelledby="globalstars-4" class="tab-pane fade " id="globalStarsTab4" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/grab.jpg?w=600&amp;h=517&amp;fit=crop&amp;s=1c93b3195359a3db71467aebcf01b73c"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">Singapore</span>
</div>
<h4 class="card-title">Tan Hooi Ling, Anthony Tan</h4>
<p class="card-text">A Singapore-based technology company offering ride-hailing transport services, food delivery and payment solutions with $1B+ valuation</p>
</div>
</div>
</div>
<div aria-labelledby="globalstars-5" class="tab-pane fade " id="globalStarsTab5" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/realreal.jpg?w=600&amp;h=517&amp;fit=crop&amp;s=cfdd08ac9de263c9c85212b8d182d852"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">U.S.</span>
</div>
<h4 class="card-title">Julie Wainwright</h4>
<p class="card-text">The global leader in authenticated luxury consignment (IPO:2019)</p>
</div>
</div>
</div>
<div aria-labelledby="globalstars-6" class="tab-pane fade " id="globalStarsTab6" role="tabpanel">
<div class="card">
<img alt="" class="card-img" src="/img/assets/general/home/canva-1590701163.jpg?w=600&amp;h=517&amp;fit=crop&amp;s=509b4f1ee40196215048a4b45dcc6a46"/>
<div class="card-body card-img-overlay">
<div class="card-meta">
<span class="country">Australia</span>
</div>
<h4 class="card-title">Melanie Perkins, Cliff Obrecht, Cameron Adams</h4>
<p class="card-text">Australian-based design tool maker with $3.2B+ valuation</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="block py-5">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-md-6 order-1 order-md-0">
<div class="">
<h6 class="subtitle">content</h6> <h4 class="title">500 Insights</h4> <p>We've collected insights and stories from some of our top performing startups, partners, and mentors in the 500 ecosystem. Whether you are a founder looking to learn the latest in growth hacking or a fellow investor looking to find what is the latest in early-stage investing, you'll find the best insights on our platform.
</p>
</div>
<div>
<a class="more-info" href="https://500.co/blog" target="_blank">Read Now</a>
</div>
</div>
<div class="col-12 col-md-6 order-0 order-md-1">
<img alt="" class="img-fluid" height="400" src="/img/assets/general/petronas-futuretech-day-2---414.JPG?w=800&amp;h=400&amp;fit=crop&amp;s=2d42a98b07bfd099b782b49b7f83658c" width="800"/>
</div>
</div>
</div>
</div>
<footer class="mt-5 position-relative">
<div class="position-absolute col-12 col-md-7 col-lg-8 h-100 footer-background">
</div>
<div class="container">
<div class="row no-gutters">
<div class="col-12 col-md-5 col-lg-4 order-1 order-md-0">
<div class="py-3 py-md-5 pr-md-3">
<a class="logo mb-4" href="/" style="background-image: url('assets/general/500-logo-black-transparent.svg'); background-size: 100%;">500 Startups</a>
<div class="address mb-4">
</div>
<div class="dropdown mb-4">
<button aria-expanded="false" aria-haspopup="true" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" id="fundDropdownMenuButton" type="button">
                            500 Worldwide
                        </button>
<div aria-labelledby="fundDropdownMenuButton" class="dropdown-menu">
<a class="dropdown-item" href="https://durians.500.co/" target="_blank">500 Southeast Asia</a>
<a class="dropdown-item" href="https://www.500canada.ca/" target="_blank">500 Canada</a>
<a class="dropdown-item" href="https://istanbul.500.co/" target="_blank">500 Istanbul</a>
<a class="dropdown-item" href="https://korea.500.co/" target="_blank">500 Korea</a>
<a class="dropdown-item" href="https://latam.500.co/" target="_blank">500 Latin America</a>
<a class="dropdown-item" href="https://mena.500.co/" target="_blank">500 MENA</a>
<a class="dropdown-item" href="https://vietnam.500.co/" target="_blank">500 Vietnam</a>
<a class="dropdown-item" href="https://thailand.500.co/" target="_blank">500 Thailand</a>
<a class="dropdown-item" href="https://brasil.500.co/" target="_blank">500 Brasil</a>
</div>
</div>
<ul class="social-links list-unstyled mb-4">
<li class="list-inline-item">
<a class="instagram" href="https://www.instagram.com/500startups/"><i class="fa-fw fa-lg fab fa-instagram-square"></i></a>
</li>
<li class="list-inline-item">
<a class="facebook" href="https://www.facebook.com/500startups"><i class="fa-fw fa-lg fab fa-facebook-f"></i></a>
</li>
<li class="list-inline-item">
<a class="twitter" href="https://twitter.com/500startups"><i class="fa-fw fa-lg fab fa-twitter"></i></a>
</li>
<li class="list-inline-item">
<a class="youtube" href="http://www.youtube.com/user/500startups"><i class="fa-fw fa-lg fab fa-youtube"></i></a>
</li>
<li class="list-inline-item">
<a class="linkedin" href="http://www.linkedin.com/company/1443207"><i class="fa-fw fa-lg fab fa-linkedin-in"></i></a>
</li>
</ul>
<div class="secondary-links mb-4">
<div class="row">
<div class="col-12 col-md-auto"><a href="/privacy">Privacy Policy</a></div>
<div class="col-12 col-md-auto"><a href="/terms-of-use">Terms of Use</a></div>
<div class="col-12 col-md-auto"><a href="/code-of-conduct">Code of Conduct</a></div>
</div>
<div class="row d-block d-md-none">
<div class="col-12 primary-links-mobile">
<button aria-controls="navbarNavDropdownFooter" aria-expanded="false" aria-label="Toggle navigation" class="dropdown-toggle" data-target="#navbarNavDropdownFooter" data-toggle="collapse" type="button">
                                    More links
                                </button>
<div class="collapse navbar-collapse justify-content-between" id="navbarNavDropdownFooter">
<ul>
<li><a href="/about">About Us</a></li>
<li><a href="/startups">Portfolio</a></li>
<li><a href="/funds">Funds</a></li>
<li><a href="/accelerators">Accelerators</a></li>
<li><a href="/ecosystems">Ecosystems</a></li>
</ul>
<ul>
<li><a href="/events">Events</a></li>
<li><a href="/contact-us">Contact Us</a></li>
<li><a href="/team">Team</a></li>
<li><a href="/invest">Invest in 500</a></li>
<li><a href="/directory">Directory</a></li>
</ul>
<ul>
<li><a href="/partners">Network Partners</a></li>
<li><a href="/careers">Careers at 500</a></li>
<li><a href="https://jobs.500.co" target="_blank">Portfolio Jobs</a></li>
<li><a href="/press">Press</a></li>
<li class="divider"></li>
<li><a href="">Sitemap</a></li>
<li><a href="/ccpa">Do Not Sell My Info</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="copyright">
<p>© 2010-2020 500 Startups</p>
<p><small></small></p>
</div>
</div>
</div>
<div class="col-12 col-md-7 col-lg-8 order-0 order-md-1 ">
<div class="py-3 py-md-5 pl-md-5 w-100">
<div class="primary-links mb-4 d-none d-md-block">
<div class="row">
<div class="col-12 col-md-4">
<ul>
<li><a href="/about">About Us</a></li>
<li><a href="/startups">Portfolio</a></li>
<li><a href="/funds">Funds</a></li>
<li><a href="/accelerators">Accelerators</a></li>
<li><a href="/ecosystems">Ecosystems</a></li>
</ul>
</div>
<div class="col-12 col-md-4">
<ul>
<li><a href="/events">Events</a></li>
<li><a href="/contact-us">Contact Us</a></li>
<li><a href="/team">Team</a></li>
<li><a href="/invest">Invest in 500</a></li>
<li><a href="/directory">Directory</a></li>
</ul>
</div>
<div class="col-12 col-md-4">
<ul>
<li><a href="/partners">Network Partners</a></li>
<li><a href="/careers">Careers at 500</a></li>
<li><a href="https://jobs.500.co" target="_blank">Portfolio Jobs</a></li>
<li><a href="/press">Press</a></li>
<li class="divider"></li>
<li><a href="">Sitemap</a></li>
<li><a href="/ccpa">Do Not Sell My Info</a></li>
</ul>
</div>
</div>
</div>
<div class="newsletter bg-white">
<div class="row align-items-center">
<div class="col-12 col-md-auto mb-2 mb-md-0">
<h5>Get Our Newsletter</h5>
<p class="small">Practical resources. No spam ever.</p>
</div>
<div class="col-12 col-md-auto flex-md-grow-1">
<!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
<script charset="utf-8" src="//js.hsforms.net/forms/v2.js" type="text/javascript"></script>
<script>
hbspt.forms.create({
    portalId: "698640",
    formId: "2758922b-987b-4509-8fc1-323a388f244c"
});
</script>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<script src="/site/themes/default/assets/init.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script src="/site/themes/default/assets/app.js"></script>
</body>
</html>
