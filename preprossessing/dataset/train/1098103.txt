<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- insert CSS for Default Concrete Theme //-->
<style type="text/css">@import "/concrete/css/ccm.default.theme.css";</style>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>eResource Center :: </title>
<meta content="" name="description"/>
<meta content="concrete5 - 5.4.0.5" name="generator"/>
<script type="text/javascript">
var CCM_DISPATCHER_FILENAME = '/index.php';var CCM_CID = 1;var CCM_EDIT_MODE = false;var CCM_ARRANGE_MODE = false;var CCM_IMAGE_PATH = "/concrete/images";
var CCM_TOOLS_PATH = "/index.php/tools/required";
var CCM_REL = "";

</script>
<link href="/concrete/css/ccm.base.css?v=17481c7869c48b52cf01ded0ea94280b" rel="stylesheet" type="text/css"/>
<script src="/concrete/js/jquery.js?v=17481c7869c48b52cf01ded0ea94280b" type="text/javascript"></script>
<script src="/concrete/js/ccm.base.js?v=17481c7869c48b52cf01ded0ea94280b" type="text/javascript"></script>
</head>
<body>
<div id="ccm-logo"><img alt="Concrete CMS" height="49" src="/concrete/images/logo_menu.png" width="49"/></div>
<div id="ccm-theme-wrapper">
<div style="position: relative">
<h1>Sign in to eResource Center</h1>
<h5 style="margin:-1em 0 .5em">Terms of Use:</h5>
<p style="font-size:11px; line-height:1.3em;">Papa Romano’s is the owner of certain trade names and trademarks, including “Papa Romano's” and “The Little Bambino” (the “Trademarks”).  Papa Romano’s and Vendor desire to enter into a relationship (or have entered into a relationship) for Vendor to manufacture and/or supply products to Papa Romano’s or its affiliates.  Before entering into or continuing a relationship with Vendor, Papa Romano’s wants to ensure that Vendor will respect the ownership and maintain the confidentiality of Papa Romano’s proprietary information, and trademarks.</p>
<p style="font-size:11px; line-height:1.3em;">In consideration of the purchase or continuing purchase of the products by Papa Romano’s or its affiliates or franchisees from Vendor, and other valuable consideration, the receipt of which is acknowledged by the parties, Papa Romano’s and Vendor agree and follows:</p>
<p style="font-size:11px; line-height:1.3em;"><b><u>Reproduction of Confidential Information.</u></b>  Vendor must not reproduce Confidential Information in any form except as necessary to manufacture and/or supply products to Papa Romano’s and its affiliates and franchisees.  Any reproduction of any Confidential Information will remain the property of Papa Romano’s and will contain any and all confidential or proprietary notices of legends that appear on the original, unless otherwise authorized in writing by Papa Romano’s</p>
<p style="font-size:11px; line-height:1.3em;"><b>By logging in, Vendor agrees to the agreement as stated.</b></p>
<div class="ccm-form">
<form action="/index.php/login/do_login/" method="post">
<div>
<label for="uName">		Username	</label><br/>
<input class="ccm-input-text" id="uName" name="uName" type="text"/>
</div>
<br/>
<div>
<label for="uPassword">Password</label><br/>
<input class="ccm-input-text" id="uPassword" name="uPassword" type="password"/>
</div>
<hr/>
<input class="ccm-input-checkbox" id="uMaintainLogin" name="uMaintainLogin" type="checkbox" value="1"/> <label for="uMaintainLogin">Remember Me</label>
<div class="ccm-button">
<input class="ccm-input-submit " id="submit" name="submit" type="submit" value="Sign In &gt;"/> </div>
<input name="rcID" type="hidden" value="1"/>
</form>
</div>
<div class="ccm-form">
<h2 style="margin-top:32px">Forgot Your Password?</h2>
<p>If you've forgotten your password, enter your email address below. We will reset it to a new password, and send the new one to you.</p>
</div>
<div class="ccm-form">
<a name="forgot_password"></a>
<form action="/index.php/login/forgot_password/" method="post">
<label for="uEmail">Email Address</label><br/>
<input name="rcID" type="hidden" value="1"/>
<input class="ccm-input-text" name="uEmail" type="text" value=""/>
<div class="ccm-button">
<input class="ccm-input-submit " id="submit" name="submit" type="submit" value="Reset and Email Password &gt;"/> </div>
</form>
</div>
<script type="text/javascript">
	document.getElementById("uName").focus();
</script>
</div>
</div>
</body>
</html>