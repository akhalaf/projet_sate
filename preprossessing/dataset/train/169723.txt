<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>What does German Luftwaffe mean? definition, meaning and audio pronunciation (Free English Language Dictionary)</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Definition of German Luftwaffe in the AudioEnglish.org Dictionary. Meaning of German Luftwaffe. What does German Luftwaffe mean? Proper usage  and audio pronunciation of the word German Luftwaffe. Information about German Luftwaffe in the AudioEnglish.org dictionary, synonyms and antonyms." name="description"/>
<meta content="INDEX, FOLLOW" name="ROBOTS"/>
<meta content="german luftwaffe, definition, sense, context examples, synonyms, dictionary, english learning, study, audio, pronunciation" name="keywords"/>
<meta content="no" name="apple-mobile-web-app-capable"/>
<meta content="telephone=yes" name="format-detection"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width" name="MobileOptimized"/>
<meta content="on" http-equiv="cleartype"/>
<link href="" media="handheld" rel="alternate"/>
<link href="https://securepubads.g.doubleclick.net" rel="dns-prefetch"/>
<link href="https://contextual.media.net" rel="dns-prefetch"/>
<script type="text/javascript">
var t_start=Number(new Date()), pgcats='4222';
function BrowseType(v) {window.location.href=v;}
function f(n,d){var p,i,x;if(!d)d=document;if((p=n.indexOf("?"))>0&&parent.frames.length){d=parent.frames[n.substring(p+1)].document;n=n.substring(0,p)}if(!(x=d[n])&&d.all)x=d.all[n];for(i=0;!x&&i<d.forms.length;i++)x=d.forms[i][n];for(i=0;!x&&d.layers&&i<d.layers.length;i++)x=f(n,d.layers[i].document);if(!x&&d.getElementById)x=d.getElementById(n);return x};
var mO=1;function MobiMenu() {if(mO){if(f('b3')){f('b3').innerHTML=f('tdTM1').innerHTML+f('b2').innerHTML;f('b3').style.display='block';}mO=0;}else{if(f('b3')){f('b3').innerHTML='';f('b3').style.display='none';}mO=1;}}
function ldAC(){if(typeof window.loadAC!=='undefined'){loadAC();}else{setTimeout("ldAC()",200);}}
var tm=0;
function Aud1(f,p){window.fId=f;window.Pyd=p;Aud2();}
function Aud2(){if(tm){clearTimeout(tm);};if(typeof window.Aud3 === "function"){Aud3();}else{tm=setTimeout("Aud2()",100);}}
</script>
<link href="https://www.audioenglish.org/wn_lib/dictionary_v7.2020071004.css" rel="stylesheet" type="text/css"/>
<!-- Start Consent Manager Head -->
<script async="true" type="text/javascript">
(function() {
function addFrame(){if(!window.frames['__cmpLocator']) {if(document.body) {var body=document.body, iframe=document.createElement('iframe');iframe.style='display:none';iframe.name='__cmpLocator';body.appendChild(iframe);}else{setTimeout(addFrame, 5);}}};var gdprAppliesGlobally=false;addFrame();function cmpMsgHandler(event) {var msgIsString=typeof event.data === "string";var json;if(msgIsString){json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};}else{json=event.data;};if(json.__cmpCall){var i=json.__cmpCall;window.__cmp(i.command, i.parameter, function(retValue, success){var returnMsg={"__cmpReturn": {"returnValue": retValue,"success": success,"callId": i.callId}};event.source.postMessage(msgIsString ? JSON.stringify(returnMsg) : returnMsg, '*');});}}
window.__cmp=function(c){var b=arguments;
if(!b.length){return __cmp.a;}
else if (b[0] === 'ping'){b[2]({"gdprAppliesGlobally": gdprAppliesGlobally,"cmpLoaded": false}, true);}
else if (c=='__cmp'){return false;}
else{if (typeof __cmp.a === 'undefined'){__cmp.a=[];};__cmp.a.push([].slice.apply(b));}
}
window.__cmp.gdprAppliesGlobally=gdprAppliesGlobally;window.__cmp.msgHandler=cmpMsgHandler;if(window.addEventListener){window.addEventListener('message', cmpMsgHandler, false);}else{window.attachEvent('onmessage', cmpMsgHandler);}
})();
</script>
<!-- End Consent Manager Head -->
<!-- Start DFP Head -->
<script async="" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
if(typeof window.console == "undefined") { window.console = {log: function (msg) {} }; }
window.googletag = window.googletag || {cmd: []};
window.hasGPTAds=1;
window.gptLoaded=1;//don't lazy-load gpt.js
function getCookie(name){var n=name+"=";var ca=document.cookie.split(";");for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==" ")c=c.substring(1,c.length);if(c.indexOf(n)==0)return c.substring(n.length,c.length);};return null;}
function LeoJs() {
	if(!document.getElementsByTagName){setTimeout(LeoJs, 50);return;}
	console.log("loading leo.js");var elem=document.createElement("script");elem.src="/users/leo.php";elem.async=true;elem.type="text/javascript";var scpt=document.getElementsByTagName("script")[0];scpt.parentNode.insertBefore(elem, scpt);
}
function TestLeo() {
	var callLeo=1,cMP=getCookie("ttconsent"),cR=[],nEEA=getCookie("notEEAuser"),ttu=getCookie("ttuser"),ctry=getCookie("ttcountry"),ca=getCookie("USCA");
	if(!!cMP){cR=cMP.split(".");}
	if (cR.length>=4){callLeo=0;}else{
		if(nEEA){var d=new Date(),n=d.getTime();if(nEEA>n-60*60*24*1000){if(ctry&&((ctry!=="US")||ca)){callLeo=0;}}}
	}
	if(ttu){callLeo=1;}
	if(callLeo) {console.log("need leo.js");window.leoCalled=1;window.wait4leo=1;LeoJs();}
}
TestLeo();

//OneTagHead.off
</script>
<!-- End DFP Head -->
<!--mnet.head.off-->
<!--b.adsense.head-->
<script async="" data-ad-client="ca-pub-4227854031748288" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script type="text/javascript">
//do not lazy-load adsbygoogle.js
window.abgjs=1;
// Default to NonPersonalizedAds; will use PersonalizedAds only with consent
(adsbygoogle=window.adsbygoogle||[]).requestNonPersonalizedAds=1;
// Pause ads and wait for consent
(adsbygoogle=window.adsbygoogle||[]).pauseAdRequests=1;
</script>
<!--e.adsense.head-->
<!--[if lt IE 9]>
<script src="/lib/java.ie8.mediaqueries.min.js" type="text/javascript"></script>
<![endif]-->
</head>
<body leftmargin="0" rightmargin="0" topmargin="0">
<div id="pWp">
<!--b.HEAD-->
<div id="T1C">
<!--b.TMENU-1-->
<div id="dTM1">
<table align="right" border="0" cellpadding="0" cellspacing="0" class="blueDiv"><tr><td class="bDL"></td><td id="tdTM1" valign="middle">
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_basic_for_beginners.htm" id="FrstLk">English for Beginners</a>
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_practical_english.htm">Practical English</a>
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_travel.htm">Travel English</a>
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_telephone.htm">Telephone English</a>
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_banking.htm">Banking English</a>
<a class="hdlksUp" href="https://www.audioenglish.org/english-learning/efl_accounting.htm">Accounting English</a>
<a class="hdlksUpOn" href="https://www.audioenglish.org/dictionary/">Dictionary</a>
</td><td class="bDR"></td></tr></table>
</div>
<!--end.TMENU-1-->
<div class="clearB"></div>
<!--b.TopLogoSRC-->
<div id="tlg"><a href="https://www.audioenglish.org/"><img alt="Online English learning Courses" border="0" height="58" src="https://www.audioenglish.org/lib/audioenglish_logo_20_tiny.png" width="315"/></a></div>
<div id="tll"><a href="javascript:MobiMenu();" id="tlla" title="Menu"></a></div>
<div id="cbatll"></div>
<div id="tsf"><!--b.TopSrcForm--><table align="right" border="0" cellpadding="0" cellspacing="0">
<form action="https://www.audioenglish.org/english-learning/search" name="DicForm" onsubmit="javascript:return KeySrc(this);">
<tr><td align="right" width="98%"><input autocomplete="off" id="Tsk" maxlength="40" name="sk" onfocus="ldAC()" size="16" style="width:90%" type="text"/><input name="where" type="hidden" value="dictionary"/><input name="mode" type="hidden" value="allwords"/></td>
<td style="min-width:10px" width="1%"> </td>
<td style="vertical-align:top" width="1%"><input type="submit" value="Search"/></td></tr>
<tr><td colspan="3" style="font-size:11px; color:#999999;vertical-align:top"><span class="hSS" style="color:#D0DBEF">Double-click any word on the page to look it up in the dictionary.</span></td></tr>
</form>
</table><!--end.TopSrcForm--></div>
<div class="clearB"></div>
<!--end.TopLogoSRC-->
<!--b.TMENU-2-->
<div class="bluedown" id="b2">
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_dialogue.htm');">Dialogues</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_narration.htm');">Narrations</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_useful_phrases.htm');">Phrases</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_pronunciation.htm');">Pronunciation</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_roleplay.htm');">Role-play exercises</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_questions_and_answers.htm');">Q&amp;A</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_test.htm');">Online tests</a>
<a class="hdlks" href="javascript:BrowseType('https://www.audioenglish.org/english-learning/type_all.htm');">All English-learning resources</a>
</div>
<!--end.TMENU-2-->
<div id="blueln"></div>
<div id="b3"></div>
<br/>
<!--b.adsense728x90-->
<div class="gad1dfp" id="div-gpt-ad-1572098088435-0"></div><!--end.adsense728x90-->
</div> <!--end.T1C-->
<!--end.HEAD-->
<!--b.BODYSECTION-->
<div id="T2C">
<div id="TAMid"> <!--ex.Layer1-->
<div id="ColDicBody">
<div class="aebook" id="withDia">
<table border="0" cellpadding="0" cellspacing="0" class="urhr" id="urhrt" width="100%">
<tr><td class="hello" style="font-size:10px"><a href="https://www.audioenglish.org/">AudioEnglish.org</a> » <a href="https://www.audioenglish.org/dictionary/">Dictionary</a> » <a href="https://www.audioenglish.org/letter/7.htm">G</a> » <a href="https://www.audioenglish.org/entries/geriatric_._geryon.htm">Geriatric ... Geryon</a></td></tr>
</table>
<div id="rdln"></div>
<span class="Sz14"><br/></span>
<!--b.beforeBody-->
<!--e.beforeBody-->
<!--b.BODYTXT-->
<div class="ndd"><h1 class="aedh">GERMAN LUFTWAFFE</h1>
<h2 class="aedlz"></h2><table border="0"><tr><td>Pronunciation (US): </td><td><!--start-pron-items-US--><a href="javascript:Aud1('/audiodic/w2/3/57403_G22RJ8RM.mp3','All');"><img alt="Play" border="0" height="32" src="https://www.audioenglish.org/lib/sound3.png" width="32"/></a><!--end-pron-items-US--></td><td>  (GB): </td><td><!--start-pron-items-GB--><a href="javascript:Aud1('/audiodic/w1/3/57403_G22RJ8RM.mp3','All');"><img alt="Play" border="0" height="32" src="https://www.audioenglish.org/lib/sound3.png" width="32"/></a><!--end-pron-items-GB--></td></tr></table>
<p class="aedlz"><span class="aedfs"> <b>Dictionary entry overview: What does German Luftwaffe mean?</b> </span></p><p class="nip2">• <a href="#noun"><span class="aedg"><b>GERMAN LUFTWAFFE</b></span> <span class="aedp">(noun)</span></a><br/>
  The noun <b>GERMAN LUFTWAFFE</b> has 1 sense:</p>
<p class="nip3"><b>1.</b> <span>the German airforce</span><a class="snd" href="javascript:Aud1('/audiodic/d1/69/108212169_06064BCF.mp3','All');"><img alt="play" height="18" src="/lib/snd3b.png" width="18"/></a></p>
<p class="nip2">  <i>Familiarity information: <b>GERMAN LUFTWAFFE</b> used as a noun is very rare.</i></p><span class="postnip1"><br/></span>
</div>
<!--b.after.ov--><!--e.after.ov-->
<div class="ndd"><p class="aedlz"><span class="aedfs"> <b>Dictionary entry details</b> </span></p><span class="prenip1"><br/></span><p class="nip1"><a id="noun" name="noun"></a>• <b><span class="aedg">GERMAN LUFTWAFFE</span> <span class="aedp">(noun)</span></b></p>
<span class="prenip2"><br/></span><p class="nip2"><span class="aeds">Sense 1</span></p>
<p class="nip3"><b>Meaning:</b></p>
<p class="nip3">The German airforce</p>
<p class="nip3"><b>Classified under:</b></p>
<p class="nip3">Nouns denoting groupings of people or objects</p>
<p class="nip3"><b>Synonyms:</b></p>
<p class="nip3">German Luftwaffe; <a href="https://www.audioenglish.org/dictionary/luftwaffe.htm">Luftwaffe</a></p>
<p class="nip3"><b>Hypernyms ("German Luftwaffe" is a kind of...):</b></p>
<p class="nip3"><a href="https://www.audioenglish.org/dictionary/air_force.htm">air force</a>; <a href="https://www.audioenglish.org/dictionary/airforce.htm">airforce</a> (the airborne branch of a country's armed forces)</p>
</div>
<!--b.pub.eod--><div id="adstria"><div class="adWrapper"><div class="gad2dfp" id="div-gpt-ad-1594304480716-0"></div></div></div><!--e.pub.eod-->
<br/><table border="0" cellpadding="0" cellspacing="0" class="bluetabTbl" width="auto"><tr><td class="bluetabLeft" width="7"></td><td class="bluetabMid" valign="bottom"> Learn English with... <span class="aedp">Proverbs</span> </td><td class="bluetabRight" width="7"></td></tr></table><div class="provrb">
"Don't look a gift horse in the mouth." <i>(English proverb)</i><br/><span class="sPa3"><br/></span>
"He who would do great things should not attempt them all alone." <i>(Native American proverb, Seneca)</i><br/><span class="sPa3"><br/></span>
"The only trick the incapable has, are his tears." <i>(Arabic proverb)</i><br/><span class="sPa3"><br/></span>
"Have no respect at table and in bed." <i>(Corsican proverb)</i><br/>
</div><br/>
<!--pop-src-box1-->
<br/>
<table border="0" cellpadding="0" cellspacing="0" id="PrevNextEntries">
<tr><td id="tdicu"><a href="https://www.audioenglish.org/dictionary/german_lesson.htm"><b><img alt="Dictionary: go up" border="0" class="dicu" height="20" src="/lib/px1.gif" width="160"/></b></a></td></tr>
<tr><td><div id="NxtDicMenu"><a href="https://www.audioenglish.org/dictionary/german_luftwaffe.htm"><div class="NxtDicMenuCrt">GERMAN LUFTWAFFE<br/></div></a><a href="https://www.audioenglish.org/dictionary/german_mark.htm"><p>GERMAN MARK<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_measles.htm"><p>GERMAN MEASLES<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_millet.htm"><p>GERMAN MILLET<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_monetary_unit.htm"><p>GERMAN MONETARY UNIT<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_nazi.htm"><p>GERMAN NAZI<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_pancake.htm"><p>GERMAN PANCAKE<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_police_dog.htm"><p>GERMAN POLICE DOG<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_rampion.htm"><p>GERMAN RAMPION<br/></p></a><a href="https://www.audioenglish.org/dictionary/german_shepherd.htm"><p>GERMAN SHEPHERD<br/></p></a></div></td></tr>
<tr><td id="tdicd"><a href="https://www.audioenglish.org/dictionary/german_shepherd_dog.htm"><b><img alt="Dictionary: go down" border="0" class="dicd" height="20" src="/lib/px1.gif" width="160"/></b></a></td></tr>
</table>
<br/>
<!--old.adsense160x600-->
<!--courses.links.box-->
<br/><!--pop-src-box2--><br/>
<div id="TTBdiv"></div>
<div id="srcpb"><span class="aedp">AudioEnglish Definitions... Just One Click Away!</span><br/>
<a href="#" onclick="addSrcProvider(); return false;">Click here to add the AudioEnglish.org dictionary</a> to your browser's search box.<br/>
</div>
<span id="pdTm">
<!--w:57310-->
<!--nw:57403-->
</span>
<!--e.BODYTXT-->
</div>
</div> <!--end.ColDicBody-->
<div id="interSpa"><img alt="" border="0" class="linevert" height="591" src="/lib/px1.gif" width="40"/></div>
<div id="ColDicMenu">
<div id="subscribe_div_left"><div style="padding:10px"><!--key3.off--><span style="color:#ffffff">Get unrestricted access to all the English-Learning Units!</span><br/><a class="yellow_bt" href="https://www.audioenglish.org/ssl.audioenglish.org/new_account.php">Subscribe now</a></div></div>
<!--b.LEFT2-->
<!--b.adsense160x600-->
<div class="AdPlaceHold" id="eatrph"></div>
<!--end.adsense160x600-->
<!--e.LEFT2-->
<span class="sPa3"><br/><br/><br/><br/></span>
<!--afef.off-->
</div> <!--end.ColDicMenu-->
<div class="clearB"></div>
</div><!--end.TAMid-->
</div> <!--end.T2C-->
<!--e.BODYSECTION-->
<!--b.FOOTER-->
<div id="T3C">
<div id="Layer2Dic">
<!--b.FOOTER.CT-->
<br/>
<table bgcolor="#999999" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td height="1"><img alt="" border="0" height="1" src="/lib/px1.gif" width="1"/></td></tr></table>
<div class="FootR"><a href="https://www.audioenglish.org/ssl.audioenglish.org/new_account.php" rel="nofollow">subscriptions</a> | <a href="javascript:window.location.href='https://www.audioenglish.org/wn_terms_of_use.php';" rel="nofollow">terms of use</a> | <a href="javascript:window.location.href='https://www.audioenglish.org/privacy_policy.php';" rel="nofollow">privacy policy</a> | <a href="javascript:window.location.href='https://www.audioenglish.org/wn_copyright_notice.php';" rel="nofollow">copyright</a> | <a href="javascript:window.location.href='https://www.audioenglish.org/wn_credits.php';" rel="nofollow">credits</a> | <a href="https://www.audioenglish.org/wn_sitemapindex.php">dictionary sitemap</a> | <a class="FootR" href="https://www.audioenglish.org/english-learning/contact.htm">contact</a><br/>
<span style="font-size:9px;color:#666666"></span></div>
<br/>
</div>
</div> <!--end.T3C-->
<!--e.FOOTER-->
</div>
<div id="iplrAll"></div>
<font color="#FFFFFF" size="1"><a href="https://www.audioenglish.org/wnsbt.htm" rel="nofollow" target="_top"><img alt="" border="0" height="1" src="/lib/px1.gif" width="1"/></a><span id="s2s"></span></font>
<noscript><img alt="" height="1" src="https://www.audioenglish.org/wnstats2.px?js=off" width="1"/></noscript>
<script src="https://www.audioenglish.org/lib/st_audio_dic7.2020071004.js" type="text/javascript"></script>
</body>
</html>
