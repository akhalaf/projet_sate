<!DOCTYPE html>
<html lang="en-gb">
<head>
<meta charset="utf-8"/>
<meta content="all,follow" name="robots"/>
<meta content="index,follow,snippet,archive" name="googlebot"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>ACCU</title>
<meta content="ACCU" name="author"/>
<meta content="accu, C++" name="keywords"/>
<meta content="ACCU - professionalism in programming" name="description"/>
<meta content="Hugo 0.74.1" name="generator"/>
<link href="//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800" rel="stylesheet" type="text/css"/>
<link href="//use.fontawesome.com/releases/v5.11.2/css/all.css" rel="stylesheet"/>
<link crossorigin="anonymous" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" rel="stylesheet"/>
<link href="/css/animate.css" rel="stylesheet"/>
<link href="/css/style.default.css" id="theme-stylesheet" rel="stylesheet"/>
<link href="/css/custom.css" rel="stylesheet"/>
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/img/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="/css/owl.carousel.css" rel="stylesheet"/>
<link href="/css/owl.theme.css" rel="stylesheet"/>
<link href="https://accu.org/index.xml" rel="alternate" title="ACCU" type="application/rss+xml"/>
<meta content="2021-01-06T08:00:00Z" property="og:updated_time"/>
<meta content="summary" name="twitter:card"/>
<meta content="ACCU" name="twitter:title"/>
<meta content="ACCU - professionalism in programming" name="twitter:description"/>
<script>

    <!-- The functions getCookie and checkCookie were copied from
         https:
	-->

    function getCookie ( cname )
    {
        var name          = cname + "=";
        var decodedCookie = decodeURIComponent ( document.cookie );
        var ca            = decodedCookie.split ( ';' );

        for ( var i = 0;   i < ca.length;   i++ )
        {
            var c = ca[ i ];
            while ( c.charAt ( 0 ) == ' ' )
            {
                c = c.substring ( 1 );
            }

            if ( c.indexOf ( name ) == 0 )
            {
                return c.substring ( name.length, c.length );
            }
        }

        return "";
    }

    function checkCookie ()
    {
        var username = getCookie ( "login-name" );

        if ( username != "" )
        {
            document.getElementById ( "login" ).innerHTML   = "Logged in as " + username;
            document.getElementById ( "already" ).innerHTML = username + " is already logged into this session."
        }
        else
        {
            document.getElementById ( "login" ).innerHTML = "";
        }
    }

  </script>
</head>
<body onload="checkCookie()">
<div id="all">
<header>
<div id="top">
<div class="container">
<div class="row">
<div class="col-xs-5">
<p id="login"></p>
</div>
<div class="col-xs-7">
<div class="social">
<a href="https://www.twitter.com/accuorg" style="opacity: 1;" target="_blank"><img src="/img/twitter.png"/></a>
<a href="https://www.facebook.com/accuorg" style="opacity: 1;" target="_blank"><img src="/img/facebook.png"/></a>
<a href="https://www.linkedin.com/company/accu/" style="opacity: 1;" target="_blank"><img src="/img/linkedin.png"/></a>
<a href="https://www.github.com/accu-org" style="opacity: 1;" target="_blank"><img src="/img/github.png"/></a>
<a href="https://www.flickr.com/groups/accu-org" style="opacity: 1;" target="_blank"><img src="/img/flickr.png"/></a>
<a href="https://www.youtube.com/channel/UCJhay24LTpO1s4bIZxuIqKw" style="opacity: 1;" target="_blank"><img src="/img/youtube.png"/></a>
<a href="https://accu.org/index.xml" style="opacity: 1;" target="_blank"><img src="/img/rss.png"/></a>
</div>
</div>
</div>
</div>
</div>
<div class="navbar-affixed-top" data-offset-top="200" data-spy="affix-top">
<div class="navbar navbar-default yamm" id="navbar" role="navigation">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand home" href="/">
<img alt="ACCU logo" class="hidden-xs hidden-sm" src="/img/logo.png"/>
<img alt="ACCU logo" class="visible-xs visible-sm" src="/img/logo-small.png"/>
<span class="sr-only">ACCU - go to homepage</span>
</a>
<div class="navbar-buttons">
<button class="navbar-toggle btn-template-main" data-target="#navigation" data-toggle="collapse" type="button">
<span class="sr-only">Toggle Navigation</span>
<i class="fas fa-align-justify"></i>
</button>
</div>
</div>
<div class="navbar-collapse collapse" id="navigation">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown active">
<a href="/">Home</a>
</li>
<li class="dropdown">
<a href="/news/">News</a>
</li>
<li class="dropdown">
<a href="/conf-main/main/">Conferences</a>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Videos <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="/video/online/">ACCU Online</a></li>
<li><a href="/video/autumn-2019/">ACCU Autumn 2019 Belfast</a></li>
<li><a href="/video/spring-2019-day-1/">ACCU 2019 Bristol</a></li>
<li><a href="/video/spring-2018-day-1/">ACCU 2018 Bristol</a></li>
<li><a href="/video/spring-2017-day-1/">ACCU 2017 Bristol</a></li>
<li><a href="/video/spring-2016-day-1/">ACCU 2016 Bristol</a></li>
<li><a href="/video/spring-2015/">ACCU 2015 Bristol</a></li>
<li><a href="/video/spring-2014/">ACCU 2014 Bristol</a></li>
<li><a href="/video/spring-2013/">ACCU 2013 Bristol</a></li>
<li><a href="/menu-overviews/all-conferences/">Older Conferences</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Journals <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="/menu-overviews/journals-overview/">Journals Overview</a></li>
<li><a href="/journals/nonmembers/overload_issue_members/">Overload by Issue</a></li>
<li><a href="/journals/nonmembers/overload_article_members/">Overload by Article</a></li>
<li><a href="/journals/nonmembers/overload_author_members/">Overload by Author</a></li>
<li><a href="/journals/nonmembers/cvu_issue_neutered/">C Vu by Issue</a></li>
<li><a href="/journals/nonmembers/cvu_article_neutered/">C Vu by Article</a></li>
<li><a href="/journals/nonmembers/cvu_author_neutered/">C Vu by Author</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">Reviews <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="/menu-overviews/reviews-overview/">Reviews Overview</a></li>
<li><a href="/reviews/by_date/">Reviews by Date</a></li>
<li><a href="/reviews/by_title/">Reviews by Title</a></li>
<li><a href="/reviews/by_author/">Reviews by Author</a></li>
<li><a href="/reviews/by_reviewer/">Reviews by Reviewer</a></li>
<li><a href="/reviews/by_publisher/">Reviews by Publisher</a></li>
<li><a href="/reviews/by_rating/">Reviews by Rating</a></li>
</ul>
</li>
<li class="dropdown">
<a href="/menu-overviews/local-groups/">Local Groups</a>
</li>
<li class="dropdown">
<a href="/menu-overviews/membership/">JOIN!</a>
</li>
<li class="dropdown">
<a href="/loginout/log-in/">Log In</a>
</li>
</ul>
</div>
<div class="collapse clearfix" id="search">
<form class="navbar-form" role="search">
<div class="input-group">
<input class="form-control" placeholder="Search" type="text"/>
<span class="input-group-btn">
<button class="btn btn-template-main" type="submit"><i class="fas fa-search"></i></button>
</span>
</div>
</form>
</div>
</div>
</div>
</div>
</header>
<section>
<div class="home-carousel">
<div class="dark-mask"></div>
<div class="container">
<div class="homepage owl-carousel">
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>Conference</h1>
<ul class="list-style-none">
<li>Established annual technical conference</li>
<li>All aspects of development with a C++ focus</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/HerbSutterACCU2017.jpg"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>Journals</h1>
<ul class="list-style-none">
<li>Monthly printed journal</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/cvu-overload-covers.jpg"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>ACCU 2021 Conference</h1>
<ul class="list-style-none">
<li>10th March through 13th March, 2021</li>
<li>Preconference workshops 9th &amp; 14th March</li>
<li>Click <a href="/conf-main/main/">here</a> for more information.</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/HerbSutterACCU2017.jpg"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>CVu</h1>
<ul class="list-style-none">
<li>General development topics</li>
<li>Bi-monthly</li>
<li>Members Only</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/CVu245Cover.png"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>ACCU 2021 Keynotes</h1>
<ul class="list-style-none">
<li>Wednesday- Emily Bache</li>
<li>Thursday - Kevlin Henney</li>
<li>Friday - Patricia Aas</li>
<li>Saturday - Sean Parent</li>
<li>Click <a href="https://web.cvent.com/event/b02cb201-20c2-4ee4-8d8d-09b92ac2a80f">here</a> to register.</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/keynote-mosaic.png"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>Overload</h1>
<ul class="list-style-none">
<li>Technical articles with heavy C++ bias</li>
<li>Bi-monthly</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/Overload146Cover.png"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>ACCU 2021 Schedule</h1>
<ul class="list-style-none">
<li>10th March through 13th March, 2021</li>
<li>Preconference workshops 9th &amp; 14th March</li>
<li>Full schedule to be announced soon!</li>
<li>Click <a href="/conf-main/main/">here</a> for more information.</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/HerbSutterACCU2017.jpg"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>Video</h1>
<ul class="list-style-none">
<li>Video of conference</li>
<li>sessions</li>
<li>Hosted on YouTube,</li>
<li>InfoQ, and</li>
<li>Skills Matter</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/Video.png"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>ACCU 2021 Registration</h1>
<ul class="list-style-none">
<li>Registration now open!</li>
<li>Click <a href="https://web.cvent.com/event/b02cb201-20c2-4ee4-8d8d-09b92ac2a80f">here</a> to register.</li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/conferences/accu2021_361x117.png"/>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-5 right">
<h1>Why Join ACCU?</h1>
<ul class="list-style-none">
<li></li>
</ul>
</div>
<div class="col-sm-7">
<img alt="" class="img-responsive" src="img/carousel/why.svg"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<style>
    .my_box {
  background: #fff;
  margin: 10 10 30px;
  border: solid 5px #003399;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 20px 20px;
}
.my_box .my_box_header {
  background-color: #003399;  
  color: white;
  font-family: "Roboto", Helvetica, Arial, sans-serif;
  font-size: 24px;
  font-weight: 900;
  line-height: 1.1;
  margin: -20px -20px 10px;
  padding: 10px;
}
</style>
<section>
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="my_box">
<div class="my_box_header">
                        ACCU News
                    </div>
<ul>
<li>
<b>ACCU 2021 Registration is now open.</b><br/>
<i>29 December 2020</i>
<p></p><p><a href="https://cvent.me/rM0BR0">Registration</a> for ACCU 2021 is now open!</p>
<p>For any delegates who deferred their registration from 2020, we will be in touch in the New Year to discuss any adjustments to your existing booking as a result of the event being fully virtual. Thank you for your support, and we look forward to âseeingâ you at ACCU 2021!</p>
</li>
<li>
<b>December's Overload Journal has been published.</b><br/>
<i>28 December 2020</i>
<p></p><p>The December 2020 ACCU Overload journal has been published and should have arrived at members' addresses. Overload 160 and previous issues of Overload can be accessed via the Journals menu.</p>
</li>
<li>
<b>November's C Vu Journal has been published.</b><br/>
<i>16 November 2020</i>
<p></p><p>The November 2020 ACCU C Vu journal has been published and should arrive at members' addresses in the next few days. C Vu 32-5 and previous issues of C Vu can be accessed via the Journals menu (ACCU members only).</p>
</li>
<li>
<b>October's Overload Journal has been published.</b><br/>
<i>19 October 2020</i>
<p></p><p>The October 2020 ACCU Overload journal has been published and should arrive at members' addresses in the next few days. Overload 159 and previous issues of Overload can be accessed via the Journals menu.</p>
</li>
<li>
<b>Site Updates</b><br/>
<i>23 September 2020</i>
<p></p><p>I have rolled out some updates and enhancements to the website:</p>
<p></p><ul>
<li><i>Reviews</i> have been updated to include a new index - <a href="/reviews/by_rating/"><i>Reviews by Rating</i></a> - and review ratings have been added to all of the indexes.</li>
<li>The first ACCU Online video has been added to <i>Videos</i>.  You can find Chris Oldwood's "A Test of Strength" <a href="/video/online/">here</a>.</li>
<li>All pages have been re-proportioned to give a little more room to the content, and a little less room for the advertising.</li>
<li>There is now a big <i>Join ACCU</i> button on the right-hand-side of most pages.</li>
<li>Several members-only AGM-related pages have been migrated from the old site.</li>
</ul>
<p>Here are some things you might have missed on the new website:</p>
<p></p><ul>
<li>Want to get these news items delivered?  Sign up for the RSS feed.  The RSS link can be found by clicking on the RSS symbol at the top of each page, or the corresponding item in the <i>Links</i> column at the bottom of each page.</li>
<li>The carousel on the home page stops rotating if you place your computer cursor on it.</li>
<li>The <i>World of Code</i> blog aggregator, and the <i>Essential Books</i> wiki, haven't disappeared. They are still available via entries in the <i>Links</i> column in the footer.</li>
</ul>
<p>If you find a problem (like a broken link), or have a suggestion, send an email to the Web Editor.  You will find the email link in the <i>Contacts</i> column in the footer.</p>
<p>Jim and I have gotten help with the new site:</p>
<p></p><ul>
<li>Russel Winder and Ian Bruntlett provided feedback on the content and layouts of the <i>Reviews</i> indexes.</li>
<li>Daniel James and Russel Winder reviewed the code that underpins the login processing.</li>
</ul>
<p>Thanks to everyone who have provided feedback or reported broken links.  I appreciate the input.</p>
</li>
</ul>
<br/>
<a href="/news/page/2">Older News</a>
</div>
</div>
<div class="col-md-6">
<div class="my_box">
<div class="my_box_header">
                        Latest Journal
                    </div>
<p><a href="/journals/overload/28/160/overload160.pdf" target="_new" title="Open Overload PDF in new tab or window.">
<img alt="Overload cover" src="/journals/overload/28/160/overload160cover.png" style="width: 67%;
                   float: left;
                   margin: 2px 0 16px;
                   border-color: lightgray;"/>
<br style="clear:both"/>

Overload 160 · December 2020 (PDF)</a>
</p><h3>Contents</h3>
<ul>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=4">
Debt â My First Thirty Years</a>.
            <br/>
            Reflecting on code often reveals gnarliness. Frances Buontempo reminds herself about all the tech debt sheâs ever caused.
        </p>
</li>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=6">
Questions on the Form of Software</a>.
            <br/>
            Writing software can be difficult. Lucian Teodorescu considers whether these difficulties are rooted in the essence of development.
        </p>
</li>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=10">
Building g++ From the GCC Modules Branch</a>.
            <br/>
            Using the WSL to build the g++ modules branch. Roger Orr demonstrates how to get a compiler that supports modules up and running.
        </p>
</li>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=12">
Consuming the uk-covid19 API</a>.
            <br/>
            Covid-19 data is available in many places. Donald Hernik demonstrates how to wrangle data out of the UK API.
        </p>
</li>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=15">
What is the Strict Aliasing Rule and Why Do We Care?</a>.
            <br/>
            Type Punning, Undefined Behavior and Alignment, Oh My! Strict aliasing is explained.
        </p>
</li>
<li>
<p>
<a href="/journals/overload/28/160/overload160.pdf#page=22">
Afterwood</a>.
            <br/>
            Design Patterns emerged last century. Chris Oldwood explains why he thinks they are still relevant.
        </p>
</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<style>
  .dropbtn {
    background: #555555;
    color: #38a7bb;
    padding: 0px;
    border: none;
  }
  
  .dropdown {
    position: relative;
    display: inline-block;
  }
  
  .dropdown-content {
    display: none;
    padding: 16px;
  }
  
  .dropdown-content a {
    display: block
  }
  
  .dropdown-content a:hover {background-color: #ddd;}
  
  .dropdown:hover .dropdown-content {display: block;}
  
  .dropdown:hover .dropbtn {background-color: #555555;}
</style>
<footer id="footer">
<div class="container">
<div class="col-md-2 col-sm-2">
<h4>FAQ:</h4>
<a href="/faq/about"> About Us          </a>
<a href="/faq/what-is"> What is ACCU?     </a>
<a href="/faq/conference"> Conferences       </a>
<a href="/faq/short-committee"> Committee Members </a>
<a href="/faq/constitution"> Constitution      </a>
<a href="/faq/values"> Values            </a>
<a href="/faq/study-groups-faq"> Study Groups      </a>
<a href="/faq/mailing-lists-faq"> Mailing Lists     </a>
<a href="/faq/privacy-policy"> Privacy Policy    </a>
<hr class="hidden-md hidden-lg hidden-sm"/>
</div>
<div class="col-md-3 col-sm-3">
<h4>Members Only:</h4>
<a href="/members/study-groups"> Study Groups            </a>
<a href="/members/mailing-lists"> Mailing Lists           </a>
<a href="/members/review-a-book"> Review a Book           </a>
<div class="dropdown">
<button class="dropbtn"> ACCU Committee</button>
<div class="dropdown-content" style="min-width:max-content">
<a href="/members/long-committee"> Committee Members  </a>
<a href="/members/posts-and-roles"> Posts and Roles    </a>
<a href="/members/attending-meetings"> Attending Meetings </a>
<a href="/members/minutes"> Minutes            </a>
<a href="/members/archive"> Archive            </a>
</div>
</div>
<a href="/members/agm"> Annual General Meetings </a>
<a href="/members/complaints"> Complaints Procedure    </a>
<a href="/loginout/log-out"> Log Out                 </a>
<hr class="hidden-md hidden-lg hidden-sm"/>
</div>
<div class="col-md-2 col-sm-2">
<h4>Contact:</h4>
<a href="mailto:info@accu.org?subject=[ACCU]"> General Information </a>
<a href="mailto:accumembership@accu.org?subject=[ACCU]"> Membership          </a>
<a href="mailto:local-groups@accu.org?subject=[ACCU]"> Local Groups        </a>
<a href="mailto:ads@accu.org?subject=[ACCU]"> Advertising         </a>
<a href="mailto:webmaster@accu.org?subject=[ACCU]"> Web Master          </a>
<a href="mailto:webeditor@accu.org?subject=[ACCU]"> Web Editor          </a>
<a href="mailto:conference@accu.org?subject=[ACCU]"> Conference          </a>
<hr class="hidden-md hidden-lg hidden-sm"/>
</div>
<div class="col-md-2 col-sm-2">
<h4>Links:</h4>
<a href="https://blogs.accu.org"> World of Code   </a>
<a href="https://github.com/accu-org/essential-books/wiki"> Essential Books </a>
<a href="https://www.twitter.com/accuorg"> Twitter         </a>
<a href="https://www.facebook.com/accuorg"> Facebook        </a>
<a href="https://www.linkedin.com/company/accu/"> LinkedIn        </a>
<a href="https://www.github.com/accu-org"> GitHub          </a>
<a href="https://www.flickr.com/groups/accu-org"> flickr          </a>
<a href="https://www.youtube.com/channel/UCJhay24LTpO1s4bIZxuIqKw"> YouTube         </a>
<a href="https://accu.org/index.xml"> RSS Feed        </a>
<hr class="hidden-md hidden-lg hidden-sm"/>
</div>
<div class="col-md-3 col-sm-3">
<h4>Buttons for your website:</h4>
<a download="" href="/img/accu/button-logo-120x32.png" title="ACCU: professionalism in programming (120x32 px)">
<img alt="ACCU: professionalism in programming (120x32 px)" src="/img/accu/button-logo-120x32.png"/>
</a><br/>
<a download="" href="/img/accu/button-logo-225x60.png" title="ACCU: professionalism in programming (225x60 px)">
<img alt="ACCU: professionalism in programming (225x60 px)" src="/img/accu/button-logo-225x60.png"/>
</a>
</div>
</div>
</footer>
<div id="copyright">
<div class="container">
<div class="row">
<div class="col-md-4">
<p class="pull-left">Copyright (c) 2018-2020 ACCU; all rights reserved.</p>
</div>
<div class="col-md-4">
<p>
</p><h5 align="center" class="panel-title">Advertisement</h5>
<p>
<a href="https://ads.accu.org/www/delivery/ck.php?n=a7cc1ec0&amp;cb=78725481">
<img src="https://ads.accu.org/www/delivery/avw.php?zoneid=3&amp;cb=78725481&amp;n=a7cc1ec0"/>
</a>
</p>
</div>
<div class="col-md-4">
<p class="pull-right">
              Template by <a href="https://bootstrapious.com/p/universal-business-e-commerce-template">Bootstrapious</a>.
              
            </p>
<p class="pull-right">
              Ported to Hugo by <a href="https://github.com/devcows/hugo-universal-theme">DevCows</a>.
            </p>
<p class="pull-right">
              Customized for ACCU by Jim and Bob.
            </p>
<p class="pull-right">
              Hosting provided by <a href="http://www.bytemark.co.uk">Bytemark</a>.
            </p>
</div>
</div>
</div>
</div>
</div>
<script crossorigin="anonymous" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" src="//code.jquery.com/jquery-3.1.1.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0/jquery.counterup.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-parallax/1.1.3/jquery-parallax.js"></script>
<script src="/js/front.js"></script>
<script src="/js/owl.carousel.min.js"></script>
</body>
</html>
