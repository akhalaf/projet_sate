<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://centrodeartemexicano.edu.mx/xmlrpc.php" rel="pingback"/>
<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport"/>
<title>No se encontró la página – Centro del Arte</title>
<script type="application/javascript">var qodeCoreAjaxUrl = "https://centrodeartemexicano.edu.mx/wp-admin/admin-ajax.php"</script><link href="//centrodeartemexicano.edu.mx" rel="dns-prefetch"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://centrodeartemexicano.edu.mx/feed/" rel="alternate" title="Centro del Arte » Feed" type="application/rss+xml"/>
<link href="https://centrodeartemexicano.edu.mx/comments/feed/" rel="alternate" title="Centro del Arte » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/centrodeartemexicano.edu.mx\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://centrodeartemexicano.edu.mx/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/plugins/LayerSlider/static/css/layerslider.css?ver=5.6.8" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/style.css?ver=5.2.9" id="qode_startit_default_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/plugins.min.css?ver=5.2.9" id="qode_startit_modules_plugins-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/modules.min.css?ver=5.2.9" id="qode_startit_modules-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/font-awesome/css/font-awesome.min.css?ver=5.2.9" id="qodef_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/elegant-icons/style.min.css?ver=5.2.9" id="qodef_font_elegant-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/ion-icons/css/ionicons.min.css?ver=5.2.9" id="qodef_ion_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/linea-icons/style.css?ver=5.2.9" id="qodef_linea_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/simple-line-icons/simple-line-icons.css?ver=5.2.9" id="qodef_simple_line_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/dripicons/dripicons.css?ver=5.2.9" id="qodef_dripicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/style_dynamic.css?ver=1504794536" id="qode_startit_style_dynamic-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/modules-responsive.min.css?ver=5.2.9" id="qode_startit_modules_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/blog-responsive.min.css?ver=5.2.9" id="qode_startit_blog_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/style_dynamic_responsive.css?ver=1504794536" id="qode_startit_style_dynamic_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://centrodeartemexicano.edu.mx/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=4.12" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCabin%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;subset=latin%2Clatin-ext&amp;ver=1.0.0" id="qode_startit_google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/LayerSlider/static/js/greensock.js?ver=1.11.8" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var LS_Meta = {"v":"5.6.8"};
/* ]]> */
</script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.6.8" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.6.8" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"es","strings":{"mejs.install-flash":"Est\u00e1s usando un navegador que no tiene Flash activo o instalado. Por favor, activa el componente del reproductor Flash o descarga la \u00faltima versi\u00f3n desde https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Salir de pantalla completa","mejs.fullscreen-on":"Ver en pantalla completa","mejs.download-video":"Descargar v\u00eddeo","mejs.fullscreen":"Pantalla completa","mejs.time-jump-forward":["Saltar %1 segundo hacia adelante","Salta hacia adelante %1 segundos"],"mejs.loop":"Alternar bucle","mejs.play":"Reproducir","mejs.pause":"Pausa","mejs.close":"Cerrar","mejs.time-slider":"Control de tiempo","mejs.time-help-text":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos.","mejs.time-skip-back":["Saltar atr\u00e1s 1 segundo","Retroceder %1 segundos"],"mejs.captions-subtitles":"Pies de foto \/ Subt\u00edtulos","mejs.captions-chapters":"Cap\u00edtulos","mejs.none":"Ninguna","mejs.mute-toggle":"Desactivar sonido","mejs.volume-help-text":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","mejs.unmute":"Activar sonido","mejs.mute":"Silenciar","mejs.volume-slider":"Control de volumen","mejs.video-player":"Reproductor de v\u00eddeo","mejs.audio-player":"Reproductor de audio","mejs.ad-skip":"Saltar anuncio","mejs.ad-skip-info":["Saltar en 1 segundo","Saltar en %1 segundos"],"mejs.source-chooser":"Selector de origen","mejs.stop":"Parar","mejs.speed-rate":"Tasa de velocidad","mejs.live-broadcast":"Transmisi\u00f3n en vivo","mejs.afrikaans":"Africano","mejs.albanian":"Albano","mejs.arabic":"\u00c1rabe","mejs.belarusian":"Bielorruso","mejs.bulgarian":"B\u00falgaro","mejs.catalan":"Catal\u00e1n","mejs.chinese":"Chino","mejs.chinese-simplified":"Chino (Simplificado)","mejs.chinese-traditional":"Chino (Tradicional)","mejs.croatian":"Croata","mejs.czech":"Checo","mejs.danish":"Dan\u00e9s","mejs.dutch":"Holand\u00e9s","mejs.english":"Ingl\u00e9s","mejs.estonian":"Estonio","mejs.filipino":"Filipino","mejs.finnish":"Fin\u00e9s","mejs.french":"Franc\u00e9s","mejs.galician":"Gallego","mejs.german":"Alem\u00e1n","mejs.greek":"Griego","mejs.haitian-creole":"Creole haitiano","mejs.hebrew":"Hebreo","mejs.hindi":"Indio","mejs.hungarian":"H\u00fangaro","mejs.icelandic":"Island\u00e9s","mejs.indonesian":"Indonesio","mejs.irish":"Irland\u00e9s","mejs.italian":"Italiano","mejs.japanese":"Japon\u00e9s","mejs.korean":"Coreano","mejs.latvian":"Let\u00f3n","mejs.lithuanian":"Lituano","mejs.macedonian":"Macedonio","mejs.malay":"Malayo","mejs.maltese":"Malt\u00e9s","mejs.norwegian":"Noruego","mejs.persian":"Persa","mejs.polish":"Polaco","mejs.portuguese":"Portugu\u00e9s","mejs.romanian":"Rumano","mejs.russian":"Ruso","mejs.serbian":"Serbio","mejs.slovak":"Eslovaco","mejs.slovenian":"Esloveno","mejs.spanish":"Espa\u00f1ol","mejs.swahili":"Swahili","mejs.swedish":"Sueco","mejs.tagalog":"Tagalo","mejs.thai":"Thai","mejs.turkish":"Turco","mejs.ukrainian":"Ukraniano","mejs.vietnamese":"Vietnamita","mejs.welsh":"Gal\u00e9s","mejs.yiddish":"Yiddish"}};
</script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<meta content="Powered by LayerSlider 5.6.8 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." name="generator"/>
<!-- LayerSlider updates and docs at: https://kreaturamedia.com/layerslider-responsive-wordpress-slider-plugin/ -->
<link href="https://centrodeartemexicano.edu.mx/wp-json/" rel="https://api.w.org/"/>
<link href="https://centrodeartemexicano.edu.mx/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://centrodeartemexicano.edu.mx/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<!--[if IE 9]><link rel="stylesheet" type="text/css" href="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/css/ie9_stylesheet.min.css" media="screen"><![endif]--> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://centrodeartemexicano.edu.mx/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://centrodeartemexicano.edu.mx/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 qode-core-1.0 startit-ver-1.4.1 qodef-smooth-scroll qodef-grid-1200 qodef-header-standard qodef-sticky-header-on-scroll-down-up qodef-default-mobile-header qodef-sticky-up-mobile-header qodef-dropdown-default qodef-light-header wpb-js-composer js-comp-ver-4.12 vc_responsive" data-rsssl="1">
<div class="qodef-wrapper">
<div class="qodef-wrapper-inner">
<header class="qodef-page-header">
<div class="qodef-menu-area">
<div class="qodef-vertical-align-containers">
<div class="qodef-position-left">
<div class="qodef-position-left-inner">
<div class="qodef-logo-wrapper">
<a href="https://centrodeartemexicano.edu.mx/" style="height: 38px;">
<img alt="logo" class="qodef-normal-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/>
<img alt="dark logo" class="qodef-dark-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/> <img alt="light logo" class="qodef-light-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/> </a>
</div>
</div>
</div>
<div class="qodef-position-right">
<div class="qodef-position-right-inner">
<nav class="qodef-main-menu qodef-drop-down qodef-default-nav">
<ul class="clearfix" id="menu-principal"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home narrow" id="nav-menu-item-15"><a class="" href="https://centrodeartemexicano.edu.mx/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">INICIO</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-14"><a class="" href="https://centrodeartemexicano.edu.mx/nosotros/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">NOSOTROS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-3422"><a class="" href="https://centrodeartemexicano.edu.mx/liecenciaturas/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">LICENCIATURAS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-3486"><a class="" href="https://centrodeartemexicano.edu.mx/maestrias/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">POSGRADOS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-3420"><a class="" href="https://centrodeartemexicano.edu.mx/diplomados/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">DIPLOMADOS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-3419"><a class="" href="https://centrodeartemexicano.edu.mx/talleres/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">CURSOS Y TALLERES</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-3418"><a class="" href="https://centrodeartemexicano.edu.mx/fechas-de-inscripcion/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">FECHAS DE INSCRIPCIÓN</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-13"><a class="" href="https://centrodeartemexicano.edu.mx/contacto/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">CONTACTO</span></span><span class="plus"></span></span></a></li>
</ul></nav>
</div>
</div>
</div>
</div>
<div class="qodef-sticky-header">
<div class="qodef-sticky-holder">
<div class=" qodef-vertical-align-containers">
<div class="qodef-position-left">
<div class="qodef-position-left-inner">
<div class="qodef-logo-wrapper">
<a href="https://centrodeartemexicano.edu.mx/" style="height: 38px;">
<img alt="logo" class="qodef-normal-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/>
<img alt="dark logo" class="qodef-dark-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/> <img alt="light logo" class="qodef-light-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/> </a>
</div>
</div>
</div>
<div class="qodef-position-right">
<div class="qodef-position-right-inner">
<nav class="qodef-main-menu qodef-drop-down qodef-sticky-nav">
<ul class="clearfix" id="menu-principal-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home narrow" id="sticky-nav-menu-item-15"><a class="" href="https://centrodeartemexicano.edu.mx/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">INICIO</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-14"><a class="" href="https://centrodeartemexicano.edu.mx/nosotros/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">NOSOTROS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-3422"><a class="" href="https://centrodeartemexicano.edu.mx/liecenciaturas/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">LICENCIATURAS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-3486"><a class="" href="https://centrodeartemexicano.edu.mx/maestrias/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">POSGRADOS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-3420"><a class="" href="https://centrodeartemexicano.edu.mx/diplomados/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">DIPLOMADOS</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-3419"><a class="" href="https://centrodeartemexicano.edu.mx/talleres/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">CURSOS Y TALLERES</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-3418"><a class="" href="https://centrodeartemexicano.edu.mx/fechas-de-inscripcion/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">FECHAS DE INSCRIPCIÓN</span></span><span class="plus"></span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="sticky-nav-menu-item-13"><a class="" href="https://centrodeartemexicano.edu.mx/contacto/"><span class="item_outer"><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">CONTACTO</span></span><span class="plus"></span></span></a></li>
</ul></nav>
</div>
</div>
</div>
</div>
</div>
</header>
<header class="qodef-mobile-header">
<div class="qodef-mobile-header-inner">
<div class="qodef-mobile-header-holder">
<div class="qodef-grid">
<div class="qodef-vertical-align-containers">
<div class="qodef-mobile-menu-opener">
<a href="javascript:void(0)">
<span class="qodef-mobile-opener-icon-holder">
<i class="qodef-icon-font-awesome fa fa-bars "></i> </span>
</a>
</div>
<div class="qodef-position-center">
<div class="qodef-position-center-inner">
<div class="qodef-mobile-logo-wrapper">
<a href="https://centrodeartemexicano.edu.mx/" style="height: 38px">
<img alt="mobile-logo" src="https://centrodearte.portafolio.okhosting.com/wp-content/uploads/2016/07/logo-02.png"/>
</a>
</div>
</div>
</div>
<div class="qodef-position-right">
<div class="qodef-position-right-inner">
</div>
</div>
</div> <!-- close .qodef-vertical-align-containers -->
</div>
</div>
<nav class="qodef-mobile-nav">
<div class="qodef-grid">
<ul class="" id="menu-principal-2"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home " id="mobile-menu-item-15"><a class="" href="https://centrodeartemexicano.edu.mx/"><span>INICIO</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-14"><a class="" href="https://centrodeartemexicano.edu.mx/nosotros/"><span>NOSOTROS</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-3422"><a class="" href="https://centrodeartemexicano.edu.mx/liecenciaturas/"><span>LICENCIATURAS</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-3486"><a class="" href="https://centrodeartemexicano.edu.mx/maestrias/"><span>POSGRADOS</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-3420"><a class="" href="https://centrodeartemexicano.edu.mx/diplomados/"><span>DIPLOMADOS</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-3419"><a class="" href="https://centrodeartemexicano.edu.mx/talleres/"><span>CURSOS Y TALLERES</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-3418"><a class="" href="https://centrodeartemexicano.edu.mx/fechas-de-inscripcion/"><span>FECHAS DE INSCRIPCIÓN</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-13"><a class="" href="https://centrodeartemexicano.edu.mx/contacto/"><span>CONTACTO</span></a></li>
</ul> </div>
</nav>
</div>
</header> <!-- close .qodef-mobile-header -->
<a href="#" id="qodef-back-to-top">
<span class="qodef-icon-stack">
<i class="qodef-icon-font-awesome fa fa-chevron-up "></i> </span>
</a>
<div class="qodef-content">
<div class="qodef-content-inner">
<div class="qodef-title qodef-breadcrumb-type qodef-content-left-alignment qodef-animation-right-left" data-height="100" style="height:100px;">
<div class="qodef-title-image"></div>
<div class="qodef-title-holder">
<div class="qodef-container clearfix">
<div class="qodef-container-inner">
<div class="qodef-title-subtitle-holder" style="">
<div class="qodef-title-subtitle-holder-inner">
<div class="qodef-title-breadcrumbs-holder">
<h1><span>404 - Page not found</span></h1>
<div class="qodef-breadcrumbs-holder"> <div class="qodef-breadcrumbs"><div class="qodef-breadcrumbs-inner"><a href="https://centrodeartemexicano.edu.mx/">Centro del Arte</a><span class="qodef-delimiter"> &gt; </span><span class="qodef-current">Error 404</span></div></div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="qodef-container">
<div class="qodef-container-inner qodef-404-page">
<div class="qodef-page-not-found">
<h2>
					Page you are looking is not found				</h2>
<h4>
					The page you are looking for does not exist. It may have been moved, or removed altogether. Perhaps you can return back to the site's homepage and see if you can find what you are looking for.				</h4>
<a class="qodef-btn qodef-btn-medium qodef-btn-default" href="https://centrodeartemexicano.edu.mx/" target="_self">
<span class="qodef-btn-text">Back to Home Page</span>
<span class="qodef-btn-text-icon"></span>
</a> </div>
</div>
</div>
</div> <!-- close div.content_inner -->
</div> <!-- close div.content -->
<footer>
<div class="qodef-footer-inner clearfix">
<div class="qodef-footer-top-holder">
<div class="qodef-footer-top ">
<div class="qodef-container">
<div class="qodef-container-inner">
<div class="clearfix">
<div class="qode_column qodef-column1">
<div class="qodef-column-inner">
<div class="widget qodef-footer-column-1 widget_text" id="text-4"> <div class="textwidget"><center>
<img alt="" src="../wp-content/uploads/2016/08/footer-02.png" usemap="#Map"/>
<map id="Map" name="Map">
<area alt="" coords="0,99,277,114" href="../contacto" shape="rect" title=""/>
<area alt="" coords="210,121,300,300" href="https://www.facebook.com/centrodeartemexicano" shape="rect" title=""/>
</map></center></div>
</div> </div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="qodef-footer-bottom-holder">
<div class="qodef-footer-bottom-holder-inner">
<div class="qodef-container">
<div class="qodef-container-inner">
<div class="qodef-two-columns-50-50 clearfix">
<div class="qodef-two-columns-50-50-inner">
<div class="qodef-column">
<div class="qodef-column-inner">
</div>
</div>
<div class="qodef-column">
<div class="qodef-column-inner">
</div>
</div>
</div>
</div> </div>
</div>
</div>
</div>
</div>
</footer>
</div> <!-- close div.qodef-wrapper-inner  -->
</div> <!-- close div.qodef-wrapper -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/centrodeartemexicano.edu.mx\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/tabs.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/accordion.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/js/third-party.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=4.12" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/js/smoothPageScroll.js?ver=5.2.9" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var qodefGlobalVars = {"vars":{"qodefAddForAdminBar":0,"qodefElementAppearAmount":-150,"qodefFinishedMessage":"No more posts","qodefMessage":"Loading new posts...","qodefTopBarHeight":0,"qodefStickyHeaderHeight":0,"qodefStickyHeaderTransparencyHeight":60,"qodefLogoAreaHeight":0,"qodefMenuAreaHeight":100,"qodefStickyHeight":60,"qodefMobileHeaderHeight":100}};
var qodefPerPageVars = {"vars":{"qodefStickyScrollAmount":0,"qodefHeaderTransparencyHeight":0}};
/* ]]> */
</script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/js/modules.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=4.12" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-content/themes/startit/assets/js/like.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://centrodeartemexicano.edu.mx/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
</body>
</html>