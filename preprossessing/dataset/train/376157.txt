<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="us"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="us"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="us"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html lang="us"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<!-- [START] 'blocks/head.rain' -->
<!--

  (c) 2008-2021 Lightspeed Netherlands B.V.
  http://www.lightspeedhq.com
  Generated: 12-01-2021 @ 22:58:33

-->
<link href="https://www.csemobility.com/Drpbox2016/" rel="canonical"/>
<link href="https://www.csemobility.com/index.rss" rel="alternate" title="New products" type="application/rss+xml"/>
<meta content="noodp,noydir" name="robots"/>
<meta content="1u1VVe2Rok-xN5GxFaOsPf8Wt_tKW2TNGtt0" name="google-site-verification"/>
<meta content="https://www.csemobility.com/Drpbox2016/?source=facebook" property="og:url"/>
<meta content="CSE Mobility and Scrubs" property="og:site_name"/>
<meta content="CSE Mobility and Scrubs" property="og:title"/>
<meta content="CSE Mobility &amp; Scrubs is DFWs premier medical supply dealer.  Specializing in Mobility equipment and Medical Scrubs.  Come see us today!" property="og:description"/>
<!--[if lt IE 9]>
<script src="https://cdn.shoplightspeed.com/assets/html5shiv.js?2020-09-30"></script>
<![endif]-->
<!-- [END] 'blocks/head.rain' -->
<title>CSE Mobility and Scrubs - CSE Mobility and Scrubs</title>
<meta content="CSE Mobility &amp; Scrubs is DFWs premier medical supply dealer.  Specializing in Mobility equipment and Medical Scrubs.  Come see us today!" name="description"/>
<meta content="" name="keywords"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/favicon.ico?20190628190416" rel="shortcut icon" type="image/x-icon"/>
<link href="//fonts.googleapis.com/css?family=Varela:400,300,600" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Varela:400,300,600" rel="stylesheet" type="text/css"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/favicon.ico?20190628190416" rel="shortcut icon" type="image/x-icon"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/bootstrap.css?20190628190416" rel="stylesheet"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/responsive.css?20190628190416" rel="stylesheet"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/style.css?20190628190416" rel="stylesheet"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/settings.css?20190628190416" rel="stylesheet"/>
<link href="https://cdn.shoplightspeed.com/assets/gui-2-0.css?2020-09-30" rel="stylesheet"/>
<link href="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/custom.css?20190628190416" rel="stylesheet"/>
<script src="https://cdn.shoplightspeed.com/assets/jquery-1-9-1.js?2020-09-30" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/assets/jquery-ui-1-10-1.js?2020-09-30" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/assets/jquery-migrate-1-1-1.js?2020-09-30" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/global.js?20190628190416" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/assets/gui.js?2020-09-30" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/uspticker.js?20190628190416" type="text/javascript"></script>
<script src="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/share42.js?20190628190416" type="text/javascript"></script>
<script async="" src="/cdn-cgi/bm/cv/669835187/api.js"></script></head>
<body>
<header class="container">
<div class="align">
<div class="vertical">
<a href="https://www.csemobility.com/" title="CSE Mobility and Scrubs">
<img alt="CSE Mobility and Scrubs" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/logo.png?20190628190416"/>
</a>
</div>
<div class="vertical text-right no-underline">
<div class="cart-account">
<a class="cart" href="https://www.csemobility.com/cart/">
<span class="glyphicon glyphicon-shopping-cart"></span> 
              Cart ($0.00)
            </a>
<a class="my-account" href="https://www.csemobility.com/account/">
<span class="glyphicon glyphicon glyphicon-user"></span>
                            My account
                          </a>
</div>
</div>
</div>
</header>
<div class="container wrapper">
<nav>
<ul class="no-list-style no-underline topbar">
<li class="item home active">
<a class="itemLink" href="https://www.csemobility.com/">Home</a>
</li>
<li class="item ">
<a class="itemLink" href="https://www.csemobility.com/collection/">All products</a>
</li>
<li class="item ">
<a class="itemLink" href="https://www.csemobility.com/catalog/">All categories</a>
</li>
<li class="item"><a class="itemLink" href="https://www.csemobility.com/service/about/" title="About us">About us</a></li>
<li class="item"><a class="itemLink" href="https://www.csemobility.com/service/" title="Customer support">Customer support</a></li>
<li class="item">
<a class="itemLink" href="https://www.csemobility.com/service/rental-equipment/" title="Rental Equipment">
      Rental Equipment
    </a>
</li>
<li class="item">
<a class="itemLink" href="https://www.csemobility.com/service/used-mobility-equipment-for-sale/" title="Used Equipment">
      Used Equipment
    </a>
</li>
</ul>
</nav>
<div class="sidebar col-xs-12 col-sm-12 col-md-3">
<span class="burger glyphicon glyphicon-menu-hamburger hidden-md hidden-lg"></span>
<div class="search">
<form action="https://www.csemobility.com/search/" id="formSearch" method="get">
<span class="glyphicon glyphicon-search" onclick="$('#formSearch').submit();" title="Search"></span>
<input name="q" placeholder="Find your product" type="text" value=""/>
</form>
</div>
<ul class="no-underline no-list-style sidebarul">
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/scrubs/" title="Scrubs">Scrubs</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/womens-scrub-top/" title="Women's Scrub Top">Women's Scrub Top
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/womens-scrub-pant/" title="Women's Scrub Pant">Women's Scrub Pant
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/womens-print-top/" title="Women's Print Top">Women's Print Top
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/womens-layers-jackets/" title="Women's Layers &amp; Jackets">Women's Layers &amp; Jackets
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/mens-scrubs/" title="Men's Scrubs">Men's Scrubs
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/nursing-supplies/" title="Nursing Supplies">Nursing Supplies
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scrubs/clearance/" title="Clearance">Clearance
        </a>
</li>
</ul>
</li>
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/compression-socks/" title="Compression Socks">Compression Socks</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/compression-socks/womens/" title="Women's">Women's
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/compression-socks/mens/" title="Men's">Men's
        </a>
</li>
</ul>
</li>
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/scooters/" title="Scooters">Scooters</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/scooters/scooter-accessories/" title="Scooter Accessories">Scooter Accessories
        </a>
</li>
</ul>
</li>
<li class="item foldingsidebar ">
<a class="itemLink " href="https://www.csemobility.com/power-wheel-chairs/" title="Power Wheel Chairs">Power Wheel Chairs</a>
</li>
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/lift-chairs/" title="Lift Chairs">Lift Chairs</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/lift-chairs/3-position/" title="3-Position">3-Position
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/lift-chairs/infinite-position/" title="Infinite Position">Infinite Position
        </a>
</li>
</ul>
</li>
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/mobility-equipment-parts/" title="Mobility Equipment Parts">Mobility Equipment Parts</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/mobility-equipment-parts/parts/" title="Parts">Parts
        </a>
</li>
</ul>
</li>
<li class="item foldingsidebar ">
<a class="itemLink " href="https://www.csemobility.com/orthopedic-products/" title="Orthopedic Products">Orthopedic Products</a>
</li>
<li class="item foldingsidebar ">
<div class="subcat"><span class="glyphicon glyphicon-chevron-down"></span></div>
<a class="itemLink hassub" href="https://www.csemobility.com/cbd/" title="CBD">CBD</a>
<ul class="subnav">
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/cbd/topical/" title="Topical">Topical
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/cbd/gummies/" title="Gummies">Gummies
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/cbd/tablets/" title="Tablets">Tablets
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/cbd/oil/" title="Oil">Oil
        </a>
</li>
<li class="subitem">
<a class="subitemLink " href="https://www.csemobility.com/cbd/other/" title="Other">Other
        </a>
</li>
</ul>
</li>
<li class="item">
<a class="itemLink" href="https://www.csemobility.com/brands/" title="Brands">Brands</a>
</li>
</ul> </div>
<div class="content col-xs-12 col-sm-12 col-md-9">
<script src="https://cdn.shoplightspeed.com/shops/617075/themes/6339/assets/jcarousel.js?20190628190416" type="text/javascript"></script>
<div class="row">
<div class="headline col-md-12">
<div class="slider">
<ul>
<li>
<a href="https://www.csemobility.com/go/category/978718/" title="Our Lift Chairs"> <img alt="Our Lift Chairs" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/18635323/805x325x1/our-lift-chairs.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/go/category/978398/" title="Our Power Mobility Scooters"> <img alt="Our Power Mobility Scooters" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/18635228/805x325x1/our-power-mobility-scooters.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/go/brand/683542/" title="Healing Hands Scrubs"> <img alt="Healing Hands Scrubs" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/14619730/805x325x1/healing-hands-scrubs.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/go/product/13472009/" title="Golden Buzz Around LT Scooter "> <img alt="Golden Buzz Around LT Scooter " class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/14620166/805x325x1/golden-buzz-around-lt-scooter.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/plr975-pride-vivalift-elegance-lift-chair.html" title="Viva Elegance Lift Chair"> <img alt="Viva Elegance Lift Chair" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/14799176/805x325x1/viva-elegance-lift-chair.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/power-wheel-chairs/" title="Power Wheelchairs"> <img alt="Power Wheelchairs" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/14815514/805x325x1/power-wheelchairs.jpg"/>
</a> </li>
<li>
<a href="https://www.csemobility.com/golden-cloud-pr-510-lift-chair.html" title="Golden Cloud PR510"> <img alt="Golden Cloud PR510" class="img-responsive" src="https://cdn.shoplightspeed.com/shops/617075/files/14800751/805x325x1/golden-cloud-pr510.jpg"/>
</a> </li>
</ul>
</div>
<p class="slider-pagination"></p>
</div>
</div>
<div class="categories row">
<div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/scrubs/">
<img alt="Scrubs" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/14621180/177x140x1/scrubs.jpg" width="177"/>
<div class="info text-center">
<h3>Scrubs</h3>
</div>
</a>
</div>
</div> <div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/compression-socks/">
<img alt="Compression Socks" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/17675482/177x140x1/compression-socks.jpg" width="177"/>
<div class="info text-center">
<h3>Compression Socks</h3>
</div>
</a>
</div>
</div> <div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/cbd/">
<img alt="CBD" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/14803079/177x140x1/cbd.jpg" width="177"/>
<div class="info text-center">
<h3>CBD</h3>
</div>
</a>
</div>
</div> <div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/scooters/">
<img alt="Scooters" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/14620922/177x140x1/scooters.jpg" width="177"/>
<div class="info text-center">
<h3>Scooters</h3>
</div>
</a>
</div>
</div> <div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/lift-chairs/">
<img alt="Lift Chairs" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/14621014/177x140x1/lift-chairs.jpg" width="177"/>
<div class="info text-center">
<h3>Lift Chairs</h3>
</div>
</a>
</div>
</div> <div class="col-md-3 col-xs-6 col-sm-6">
<div class="category">
<a href="https://www.csemobility.com/mobility-equipment-parts/parts/">
<img alt="Parts" class="img-responsive" height="140" src="https://cdn.shoplightspeed.com/shops/617075/files/14628464/177x140x1/parts.jpg" width="177"/>
<div class="info text-center">
<h3>Parts</h3>
</div>
</a>
</div>
</div> </div>
<div class="products row">
<a href="https://www.csemobility.com/golden-cloud-pr-510-lift-chair.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/22614632/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/golden-cloud-pr-510-lift-chair.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/golden-cloud-pr-510-lift-chair.html">
<img alt="Golden Lift Chair MaxiComfort Cloud PR-510" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/18624567/270x270x2/golden-lift-chair-maxicomfort-cloud-pr-510.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/golden-cloud-pr-510-lift-chair.html">
<h3>Lift Chair MaxiComfort Cloud PR-510</h3>
</a>
<span class="price">
                $1,748.00  
      </span>
</div>
</div>
</div>
</a>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ju.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/21477376/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ju.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ju.html">
<img alt="Healing Hands Healing Hands Purple Label Yoga Juliet Top 2245" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/7696336/270x270x2/healing-hands-healing-hands-purple-label-yoga-juli.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ju.html">
<h3>Healing Hands Purple Label Yoga Juliet Top 2245</h3>
</a>
<span class="price">
                $27.99  
      </span>
</div>
</div>
</div>
</a>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ja.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/21477749/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ja.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ja.html">
<img alt="Healing Hands Healing Hands Purple Label Jasmin Top 2278" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/14171542/270x270x2/healing-hands-healing-hands-purple-label-jasmin-to.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/healing-hands-healing-hands-womens-purple-label-ja.html">
<h3>Healing Hands Purple Label Jasmin Top 2278</h3>
</a>
<span class="price">
                $24.99  
      </span>
</div>
</div>
</div>
</a>
<a href="https://www.csemobility.com/healing-hands-womens-purple-label-tori-pant-9133.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/21881484/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/healing-hands-womens-purple-label-tori-pant-9133.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/healing-hands-womens-purple-label-tori-pant-9133.html">
<img alt="Healing Hands Healing Hands Purple Label Tori Pant 9133" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/7855157/270x270x2/healing-hands-healing-hands-purple-label-tori-pant.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/healing-hands-womens-purple-label-tori-pant-9133.html">
<h3>Healing Hands Purple Label Tori Pant 9133</h3>
</a>
<span class="price">
                $29.99  
      </span>
</div>
</div>
</div>
</a>
<a href="https://www.csemobility.com/golden-buzzaround-lt-3-wheel-scooter.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/22169134/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/golden-buzzaround-lt-3-wheel-scooter.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/golden-buzzaround-lt-3-wheel-scooter.html">
<img alt="Golden Golden BuzzAround LT 3 Wheel Power Travel Mobility Scooter Wheel Chair GB107D" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/18487765/270x270x2/golden-golden-buzzaround-lt-3-wheel-power-travel-m.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/golden-buzzaround-lt-3-wheel-scooter.html">
<h3>Golden BuzzAround LT 3 Wheel Power Travel Mobility Scooter Wheel Chair GB107D</h3>
</a>
<span class="price">
                $999.99  
      </span>
</div>
</div>
</div>
</a>
<a href="https://www.csemobility.com/plr975-pride-vivalift-elegance-lift-chair.html">
<div class="col-md-2 col-xs-6 col-sm-2">
<div class="product">
<div class="image-wrap">
<div class="hover">
<div class="circle no-underline">
<a href="https://www.csemobility.com/cart/add/22416376/">
<span class="glyphicon glyphicon-shopping-cart"></span>
</a>
<a href="https://www.csemobility.com/plr975-pride-vivalift-elegance-lift-chair.html">
<span class="glyphicon glyphicon-search"></span>
</a>
</div>
</div>
<a href="https://www.csemobility.com/plr975-pride-vivalift-elegance-lift-chair.html">
<img alt="Pride Mobility VivaLift Elegance Lift Chair PLR-975" class="img-responsive" height="270" src="https://cdn.shoplightspeed.com/shops/617075/files/18629130/270x270x2/pride-mobility-vivalift-elegance-lift-chair-plr-97.jpg" width="270"/>
</a>
</div>
<div class="info text-center">
<a class="no-underline" href="https://www.csemobility.com/plr975-pride-vivalift-elegance-lift-chair.html">
<h3>VivaLift Elegance Lift Chair PLR-975</h3>
</a>
<span class="price">
                $1,529.99  
      </span>
</div>
</div>
</div>
</a>
</div>
<div class="brands">
<div class="slider">
<ul>
<li>
<a href="https://www.csemobility.com/brands/dalton-medical/" title="Dalton Medical">
<img alt="Dalton Medical" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14642157/150x80x2/dalton-medical.jpg" title="Dalton Medical" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/drive-medical/" title="Drive Medical">
<img alt="Drive Medical" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14628742/150x80x2/drive-medical.jpg" title="Drive Medical" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/golden/" title="Golden">
<img alt="Golden" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14623309/150x80x2/golden.jpg" title="Golden" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/invacare/" title="Invacare">
<img alt="Invacare" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14640251/150x80x2/invacare.jpg" title="Invacare" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/maevn/" title="Maevn">
<img alt="Maevn" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14624614/150x80x2/maevn.jpg" title="Maevn" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/med-couture/" title="Med Couture">
<img alt="Med Couture" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14621773/150x80x2/med-couture.jpg" title="Med Couture" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/prestige-medical/" title="Prestige Medical">
<img alt="Prestige Medical" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14628941/150x80x2/prestige-medical.jpg" title="Prestige Medical" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/pride-mobility/" title="Pride Mobility">
<img alt="Pride Mobility" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/7637193/150x80x2/pride-mobility.jpg" title="Pride Mobility" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/shoprider/" title="Shoprider">
<img alt="Shoprider" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/14642595/150x80x2/shoprider.jpg" title="Shoprider" width="150"/>
</a>
</li>
<li>
<a href="https://www.csemobility.com/brands/wonderwink/" title="WonderWink">
<img alt="WonderWink" height="80" src="https://cdn.shoplightspeed.com/shops/617075/files/7893592/150x80x2/wonderwink.jpg" title="WonderWink" width="150"/>
</a>
</li>
</ul>
</div>
<a class="slider-prev" href="#prev" title="Previous"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a class="slider-next" href="#next" title="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
</div>
<div class="clearfix"></div>
<footer>
<div class="row items top no-list-style no-underline">
<div class="contact-adres col-md-3 col-xs-12 border-right">
<label class="collapse" for="_1">
<h3>      CSE Mobility &amp; Scrubs</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_1" type="checkbox"/>
<div class="list">
<span class="contact-description">Your source for all your medical needs!</span> <div class="contact">
<span class="glyphicon glyphicon-earphone"></span>
              972-757-7636
            </div>
<div class="contact">
<span class="glyphicon glyphicon-envelope"></span>
<a href="/cdn-cgi/l/email-protection#e192808d8492a18292848c8e83888d889598cf828e8c" title="Email"><span class="__cf_email__" data-cfemail="e794868b8294a78494828a88858e8b8e939ec984888a">[email protected]</span></a>
</div>
</div>
</div>
<div class="service-links col-md-3 col-xs-12 border-left">
<label class="collapse" for="_2">
<h3>Customer service</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_2" type="checkbox"/>
<div class="list">
<ul>
<li><a href="https://www.csemobility.com/service/rental-equipment/" title="Rental Equipment">Rental Equipment</a></li>
<li><a href="https://www.csemobility.com/service/used-mobility-equipment-for-sale/" title="Used Mobility Equipment for Sale">Used Mobility Equipment for Sale</a></li>
<li><a href="https://www.csemobility.com/service/about/" title="About us">About us</a></li>
<li><a href="https://www.csemobility.com/service/general-terms-conditions/" title="General terms &amp; conditions">General terms &amp; conditions</a></li>
<li><a href="https://www.csemobility.com/service/disclaimer/" title="Disclaimer">Disclaimer</a></li>
</ul>
</div>
</div>
<div class="service-links col-md-3 col-xs-12 border-left">
<label class="collapse" for="_3">
<h3>More</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_3" type="checkbox"/>
<ul>
<li><a href="https://www.csemobility.com/service/privacy-policy/" title="Privacy policy">Privacy policy</a></li>
<li><a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">Payment methods</a></li>
<li><a href="https://www.csemobility.com/service/shipping-returns/" title="Shipping &amp; returns">Shipping &amp; returns</a></li>
<li><a href="https://www.csemobility.com/service/" title="Customer support">Customer support</a></li>
<li><a href="https://www.csemobility.com/sitemap/" title="Sitemap">Sitemap</a></li>
</ul>
</div>
<div class="service-links col-md-3 col-xs-12 border-left">
<label class="collapse" for="_4">
<h3>My account</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_4" type="checkbox"/>
<ul>
<li><a href="https://www.csemobility.com/account/" title="My account">My account</a></li>
<li><a href="https://www.csemobility.com/account/orders/" title="My orders">My orders</a></li>
<li><a href="https://www.csemobility.com/account/tickets/" title="My tickets">My tickets</a></li>
<li><a href="https://www.csemobility.com/account/wishlist/" title="My wishlist">My wishlist</a></li>
</ul>
</div>
</div>
<div class="row items bottom">
<div class="widget col-md-3 hidden-sm hidden-xs border-right">
</div>
<div class="newsletter col-xs-12 col-md-3 border-left">
<label class="collapse" for="_5">
<h3>Newsletter</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_5" type="checkbox"/>
<div class="list">
<form action="https://www.csemobility.com/account/newsletter/" id="formNewsletter" method="post">
<input name="key" type="hidden" value="6423ba63e9e68145d09eb563baeb71b6"/>
<input id="formNewsletterEmail" name="email" placeholder="Enter your email adress" type="email" value=""/><br/><br/>
<a class="btn" href="#" onclick="$('#formNewsletter').submit(); return false;" title="Subscribe">Subscribe</a>
</form>
</div>
</div>
<div class="social-media col-md-3 col-xs-12 ">
<label class="collapse" for="_6">
<h3>Social media</h3>
<span class="glyphicon glyphicon-chevron-down hidden-sm hidden-md hidden-lg"></span></label>
<input class="hidden-md hidden-lg hidden-sm" id="_6" type="checkbox"/>
<div class="list">
<div class="social-media">
<a class="social-icon facebook" href="https://www.facebook.com/csescrubs" target="_blank" title="Facebook CSE Mobility and Scrubs"></a> </div>
</div>
</div>
<div class="hallmarks hidden-xs hidden-sm col-md-3 no-underline">
</div>
</div>
<div class="row copyright-payments no-underline">
<div class="copyright col-md-6">
<small>
            © Copyright 2021 CSE Mobility and Scrubs
                        - Powered by
                        <a href="http://www.lightspeedhq.com" target="_blank" title="Lightspeed">Lightspeed</a>
</small>
</div>
<div class="payments col-md-6 text-right">
<a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">
<img alt="PayPal" src="https://cdn.shoplightspeed.com/assets/icon-payment-paypal.png?2020-09-30"/>
</a>
<a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">
<img alt="MasterCard" src="https://cdn.shoplightspeed.com/assets/icon-payment-mastercard.png?2020-09-30"/>
</a>
<a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">
<img alt="Visa" src="https://cdn.shoplightspeed.com/assets/icon-payment-visa.png?2020-09-30"/>
</a>
<a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">
<img alt="American Express" src="https://cdn.shoplightspeed.com/assets/icon-payment-americanexpress.png?2020-09-30"/>
</a>
<a href="https://www.csemobility.com/service/payment-methods/" title="Payment methods">
<img alt="Discover Card" src="https://cdn.shoplightspeed.com/assets/icon-payment-discover.png?2020-09-30"/>
</a>
</div>
</div>
</footer>
</div>
<!-- [START] 'blocks/body.rain' -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
(function () {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = 'https://www.csemobility.com/services/stats/pageview.js';
  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild(s);
})();
</script>
<script>
    if (typeof ga === 'undefined') {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-43424686-1', 'auto', {'allowLinker': true});
        ga('set', 'forceSSL', true);
        ga('require', 'linker');
        ga('linker:autoLink', ['www.csemobility.com', 'shoplightspeed.com'], false, true);
                                ga('send', 'pageview');
            }
</script>
<!-- [END] 'blocks/body.rain' -->
<script>
      $(".glyphicon-menu-hamburger").click(function(){
    $(".sidebarul").toggle(400);
});
    </script><script>
$(".subcat").click(function(){
    $(this).siblings(".subnav").toggle();
});
  </script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'610a7b9c9ea1e25b',m:'ee7267ae34d4aa2bf38d3cdb0c0a2ada672c1373-1610492314-1800-Ab/u1PV6uVl0NaZ566pZVDwSP06sB82ZzwWDSZ2RqVFoXrEbzPh4ZTwvCm4c1vQ2sA865akdc92SkfxGaZEFyXSriSrddXjdARa64nTTkMpFVZzlF9FkgO0T/IsOHEzspg==',s:[0x11ef069925,0xf6be353567],}})();</script></body>
</html>