<!DOCTYPE html>
<html lang="en">
<head>
<title>Jquery Slideshow Maker - 404 Page Not Found</title>
<meta content="Slider Maker - Best Jquery Slideshow Maker - Page Not Found" name="description"/>
<meta content=" jquery slideshow maker, photo slideshow maker, jquery slider, image slider builder" name="keywords"/>
<meta content="Global" name="locations"/>
<meta content="index, follow" name="robots"/>
<meta content=" index, follow" name="Googlebot"/>
<meta content="Global" name="distribution"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="en" name="language"/>
<meta content=" slider-maker" name="generator"/>
<meta content="1 days" name="revisit-after"/>
<meta content="Gilles Migliori" name="author"/>
<meta content="default-src 'self'; style-src 'self' https://fonts.googleapis.com/ http://fonts.googleapis.com 'unsafe-inline'; img-src 'self' https://stats.g.doubleclick.net data: http://www.google-analytics.com; font-src data: 'self' https://fonts.gstatic.com http://fonts.googleapis.com http://fonts.gstatic.com; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://code.jquery.com/ http://www.google-analytics.com https://ajax.cloudflare.com nonce-aab6d9ce2532ac4e6d3f975c9f756103" http-equiv="Content-Security-Policy"/>
<meta content="summary" name="twitter:card"/>
<meta content="@miglisoft" name="twitter:creator"/>
<meta content="https://www.slider-maker.com" name="twitter:domain"/>
<meta content="Slider Maker - jQuery Slideshow Builder with Admin Panel" property="og:title"/>
<meta content="Build jQuery Photo Slideshows without coding using Admin Panel." property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="Slider Maker" property="og:name"/>
<meta content="Slider Maker" property="og:site_name"/>
<meta content="https://www.slider-maker.com/assets/images/slider-maker-card-1.jpg" property="og:image"/>
<meta content="https://www.slider-maker.com/" property="og:url"/>
<meta content="en_GB" property="og:locale"/>
<link href="https://www.slider-maker.com" rel="canonical"/>
<link href="/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-touch-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<link href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
<link href="/slidermaker/admin/assets/lib/materialize/css/materialize.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/slidermaker/demo-files/assets/css/demo.min.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" property="stylesheet" rel="stylesheet"/>
<!--Let browser know website is optimized for mobile-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<script nonce="aab6d9ce2532ac4e6d3f975c9f756103">document.cookie='resolution='+Math.max(screen.width,screen.height)+("devicePixelRatio" in window ? ","+devicePixelRatio : ",1")+'; path=/';</script><!-- adaptative-images -->
</head>
<body style="background:#333039">
<div id="demo">
<div class="navbar-fixed">
<nav id="top-nav">
<div class="nav-wrapper">
<a class="brand-logo z-depth-1" href="http://codecanyon.net/item/slider-maker/15417250?ref=migli" id="slider-maker-logo" itemscope="" itemtype="http://schema.org/Brand" rel="nofollow" title="Slider Maker"><img alt="Slider Maker" itemprop="logo" src="/slidermaker/demo-files/assets/slider-maker-logo.png" title="Slider Maker"/></a>
<a class="button-collapse" data-activates="mobile-demo" href="#" title="Menu"><i class="material-icons">menu</i></a>
<ul class="hide-on-med-and-down" id="nav-mobile">
<li><a href="#home" title="Slider Maker Home">HOME</a></li>
<li><a href="#features" title="Slider Maker Features">FEATURES</a></li>
<li><a href="#themes" title="Slider Maker Themes">THEMES</a></li>
<li><a href="/slidermaker/demo-files/index.php" title="Slider Maker Templates">TEMPLATES</a></li>
<li><a href="/slidermaker/admin/index.php" title="Slider Maker Admin">DEMO ADMIN</a></li>
<li><a href="/slidermaker/index.html" title="Documentation">DOCUMENTATION</a></li>
</ul>
<a class="btn brand-logo right waves-effect waves-light indigo accent-3" href="http://codecanyon.net/item/slider-maker/15417250?ref=migli" id="buy-nav-btn" itemscope="" itemtype="http://schema.org/Brand" title="Buy Slider Maker"><i class="material-icons right">file_download</i>Buy <span class="hide-on-med-and-down" itemprop="name">Slider Maker</span></a>
<a class="waves-effect waves-light btn btn-sm center-align" href="/slidermaker/demo-files/index.php" id="back-btn" title="back to slideshows list"><i class="material-icons left" title="Back to Templates">chevron_left</i><span class="hide-on-small-only">back to slideshows list</span></a>
<ul class="side-nav" id="mobile-demo">
<li><a href="#home" title="Slider Maker Home">HOME</a></li>
<li><a href="#features" title="Slider Maker Features">FEATURES</a></li>
<li><a href="#themes" title="Slider Maker Themes">THEMES</a></li>
<li><a href="/slidermaker/demo-files/index.php" title="Slider Maker Templates">TEMPLATES</a></li>
<li><a href="/slidermaker/admin/index.php" title="Slider Maker Admin">DEMO ADMIN</a></li>
<li><a href="/slidermaker/index.html" title="Documentation">DOCUMENTATION</a></li>
</ul>
</div>
</nav>
</div>
<div class="center-align">
<img src="https://www.slider-maker.com/slidermaker/demo-files/assets/images/404.png"/>
</div>
</div>
<script defer="" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script defer="" src="/slidermaker/admin/assets/lib/materialize/js/materialize.min.js" type="text/javascript"></script>
<script defer="" src="assets/js/main.min.js" type="text/javascript"></script>
</body>
</html>
