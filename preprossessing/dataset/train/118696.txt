<!DOCTYPE HTML>
<html>
<head>
<title>Ampli5yd - Your Story, Your Brand</title>
<link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<style type="text/css">@import url("css/style.css");</style>
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet" type="text/css"/>
<link href="css/flexslider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="images/favicon.ico" rel="shortcut icon"/>
<link href="css/megamenu.css" rel="stylesheet"/>
<link href="css/blue.css" rel="stylesheet"/>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/megamenu.js" type="text/javascript"></script>
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>
</head>
<body><!-- header -->
<div class="banner">
<div class="container">
<div class="header">
<div class="logo">
<a href="home"><img alt="" src="images/logo1.png"/></a>
</div>
<div class="head-nav">
<span class="menu"> </span>
<ul class="megamenu">
<li class="active"><a href="home">HOME</a></li>
<li><a href="#">ABOUT</a>
<ul class="dropdown">
<li><a href="about">About Ampli5yd</a></li>
<li><a href="our_approach">Our Approach</a></li>
<li><a href="our_perspective">Our Perspective</a></li>
</ul>
</li>
<li><a href="#">SERVICES</a>
<ul class="dropdown">
<li><a href="Technology.php">Technology</a></li>
<li><a href="Strategy.php">Strategy</a></li>
<li><a href="Content_Solutions.php">Content Solutions</a></li>
</ul>
</li>
<li><a href="training">TRAINING</a>
</li>
<li><a href="http://ampli5yd.com/blog/">BLOG</a></li>
<li><a href="contact">CONTACT</a></li>
<div class="clearfix"> </div>
</ul> </div>
<div class="clearfix"> </div>
</div>
<div class="banner-info">
<h1>TRANSFORMING LEARNING WITH CUTTING EDGE TECHNOLOGY</h1>
<label></label><br/>
<a class="play-icon popup-with-zoom-anim get" href="#small-dialog">get in touch</a>
</div>
</div>
</div>
<!-- header -->
<!-- about -->
<div class="about">
<div class="container">
<h2 class="hdclr">Our<span>  Niche</span></h2>
<center><label></label></center>
<div class="col-md-6 about-left">
<h3>INTRODUCTION <span>TO WHAT WE DO</span></h3>
<p>Ampli5yd is an innovative Learning and Development consultancy that designs, develops and delivers technology-based learning solutions for corporates and academic institutions. Our expertise is in providing learning strategy, education technology solutions and content development solutions.</p>
<p>We help our clients respond to the increasing demand for effective learning by leveraging on technology. We take pride in creating technology enabled Learning solutions that work with your budget, timeline, and expectations. Our consultant expertise is delivered on-demand—exactly what you need, when you need it.</p>
</div>
<div class="col-md-6 about-right">
<div class="progress-head">
<h6>Learning Consultancy </h6>
<p class="percent">95%</p>
<div class="clearfix"></div>
<div class="progressy1"><p></p></div>
</div>
<div class="progress-head1">
<h6>Strategy &amp; Analysis</h6>
<p class="percent">75%</p>
<div class="clearfix"></div>
<div class="progressy2"><p></p></div>
</div>
<div class="progress-head1">
<h6>Product Management</h6>
<p class="percent">75%</p>
<div class="clearfix"></div>
<div class="progressy2"><p></p></div>
</div>
</div>
<div class="clearfix"> </div>
</div>
</div>
<!-- about -->
<!-- work -->
<div class="work">
<div class="container">
<h2>what we do?</h2>
<label></label>
<div class="gallery">
<div class="col-md-3 gallery1">
<a href="#"><img alt="" class="img-responsive" src="images/icon1.png"/></a>
<label></label>
<h6><b>Technology</b></h6>
</div>
<div class="col-md-3 gallery1">
<a href="#"><img alt="" class="img-responsive" src="images/icon2.png"/></a>
<label></label>
<h6><b>Strategy</b></h6>
</div>
<div class="col-md-3 gallery1">
<a href="#"><img alt="" class="img-responsive" src="images/icon3.png"/></a>
<label></label>
<h6><b>Content Solutions</b></h6>
</div>
<div class="col-md-3 gallery1">
<a href="#"><img alt="" class="img-responsive" src="images/icon4.png"/></a>
<label></label>
<h6><b>Training</b></h6>
</div>
<div class="clearfix"> </div>
</div>
</div>
</div>
<!-- work -->
<!-- difference -->
<div class="difference">
<div class="container">
<div class="col-md-7">
<h3>OUR PERSPECTIVE</h3>
<label></label>
<p>We are passionate about delivering the best learning solutions to help your organization or academic institution evaluate strengths, develop talent, train leadership, and manage change. Our eLearning solutions focus on innovation with tangible learning outcomes.</p>
</div>
<div class="col-md-5 player">
<img src="images/difference_img.png"/>
</div>
<div class="clearfix"> </div>
</div>
</div>
<!-- difference -->
<!-- slider -->
<section class="slider">
<div class="flexslider">
<ul class="slides">
<li>
<div class="testimonial">
<div class="container">
<div class="testimonial-info">
<!--i class="drop"> </i-->
<h4>THE AMPLI5YD APPROACH<span></span></h4>
<br/>
<p>Learning Solutions That Work With Your Budget, Timeline, &amp; Expectations</p>
<label></label>
</div>
</div>
</div>
</li>
<li>
<div class="testimonial">
<div class="container">
<div class="testimonial-info">
<!--i class="drop"> </i-->
<h4>THE AMPLI5YD APPROACH<span></span></h4>
<br/>
<p>Customised Content Development for classroom and online training</p>
<label></label>
</div>
</div>
</div>
</li>
<li>
<div class="testimonial">
<div class="container">
<div class="testimonial-info">
<!--i class="drop"> </i-->
<h4>THE AMPLI5YD APPROACH<span></span></h4>
<br/>
<p>Crafting your perfect eLearning solution</p>
<label></label>
</div>
</div>
</div>
</li>
<li>
<div class="testimonial">
<div class="container">
<div class="testimonial-info">
<!--i class="drop"> </i-->
<h4>THE AMPLI5YD APPROACH<span></span></h4>
<br/>
<p>Real learning outcomes that will benefit your business</p>
<label></label>
</div>
</div>
</div>
</li>
</ul>
</div>
</section>
<div class="mfp-hide" id="small-dialog">
<div class="select-city">
<!--h3>new form</h3-->
<h3 id="lheading">Send us a message to request more information</h3>
<div class="confirmation">
<form action="download2.php" id="DownloadForm" method="post" name="DownloadForm">
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Learning Management Systems</label>
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Custom Content Development</label>
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Learning Consultancy</label>
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Marketing Strategy &amp; Analysis</label>
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Teaching with Technology </label>
<input class="col-md-1" id="introduction_to_product_management" name="introduction_to_product_management" type="checkbox" value="introduction to product management"/>
<label class="col-md-5">Technology In Early Childhood Education</label>
<input class="col-md-10 npt form-control" id="name" name="name" placeholder="Name" type="text"/>
<input class="col-md-10 npt form-control" id="email" name="email" pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?" placeholder="Email" title="Enter a valid email" type="text"/>
<input class="col-md-10 npt form-control" id="number" maxlength="10" name="number" pattern="[1-9]{1}\d{9}" placeholder="Telephone Number" title="Enter a valid mobile number" type="text"/>
<input id="cap" name="cap" type="hidden" value="137"/>
<input id="downloadbtn" name="submit" type="submit" value="Send"/>
</form>
</div>
<div class="clearfix"></div>
</div>
</div>
<!-- FlexSlider -->
<script defer="" src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
							$(function(){
							  SyntaxHighlighter.all();
							});
							$(window).load(function(){
							  $('.flexslider').flexslider({
								animation: "slide",
								start: function(slider){
								  $('body').removeClass('loading');
								}
							  });
							});
						  </script>
<!-- FlexSlider --><!-- slider -->
<!-- footer -->
<div class="footer">
<div class="container">
<div class="col-md-5 footer1">
<div class="col-md-8 studio">
<h3>contact<span> details</span></h3>
<h6><b>Cell:</b> 27 71 007 1825</h6>
<h6><b>Tel:</b> +27 11 791 5062</h6>
<h6><b>E-mail: </b><a href="mailto:info@ampli5yd.com">info@ampli5yd.com</a></h6>
</div>
<div class="col-md-4 explore">
<h5>Site Map</h5>
<!--h3>site<span> map</span></h3-->
<ul>
<li><a href="home">Home</a></li>
<li><a href="about">About</a></li>
<li><a href="services">Services</a></li>
<li><a href="training">Training</a></li>
<li><a href="http://ampli5yd.com/blog/">Blog</a></li>
<li><a href="contact">Contact</a></li>
<div class="clearfix"> </div>
</ul>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-md-7 blog">
<div class="col-md-6">
<a class="twitter-timeline" data-chrome="nofooter" data-tweet-limit="1" data-widget-id="677044232591425537" href="https://twitter.com/ampli5yd">Tweets by @ampli5yd</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<div class="clearfix"> </div>
</div>
<div class="col-md-6 explore">
<h5>From the blog</h5>
<div class="">
</div>
<div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>
</div>
</div>
<!-- footer -->
<div class="footer-bottom">
<div class="container">
<div class="col-md-6">
<p>© Copyright Ampli5yd 2015. Website developed by <a href="http://apicaledge.com" target="_blank">Apical Edge</a></p>
</div>
<div class="col-md-6 social">
<ul>
<!--li><a href="#"><i class="fb"> </i></a></li-->
<li><a href="https://twitter.com/ampli5yd" target="_blank"><i class="twt"> </i></a></li>
<div class="clearfix"> </div>
</ul>
</div>
<div class="clearfix"> </div>
</div>
</div></body>
</html>