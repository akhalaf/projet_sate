<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="Mike Campbell" name="author"/>
<meta content="Find the meaning, history and popularity of given names from around the world. Get ideas for baby names or discover your own name's history." name="description"/>
<meta content="meaning, baby names, list, dictionary, encyclopedia, etymology, name, etymologies, origin, meanings, origins, onomastics, boy names, girl names, reference" name="keywords"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" id="viewport" name="viewport"/>
<title>The Meaning and History of First Names - Behind the Name</title>
<link href="/style.php?v=302" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="icon" type="image/vnd.microsoft.icon"/>
<link href="/images/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/images/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/btn.js?version=63" type="text/javascript"></script>
<meta content="171755776192813" property="fb:page_id"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-6043105-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-6043105-1');
</script>
<script src="https://tagan.adlightning.com/math-aids/op.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/layout-728.js"></script>
<script src="//www.googletagservices.com/tag/js/gpt.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/cmp.js"></script>
<script src="//cdn.districtm.ca/merge/merge.169485.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/prebid.js"></script>
<script src="https://qd.admetricspro.com/js/behindthename/engine.js"></script>
</head>
<body>
<nav>
<div class="menubar-wrapper">
<div class="menubar">
<table class="menubar-table"><tr>
<td id="menucell1">
</td>
<td id="menucell2">
</td>
<td id="menucell3">
<ul class="btnmenu">
<li><a href="" onclick="return false;">Names</a>
<ul class="btnsubmenu">
<li><a href="/info/names">Introduction</a></li>
<li><a href="/names/list">Browse Names</a></li>
<li><a href="/names/search">Advanced Search</a></li>
<li><a href="/top/">Popularity</a></li> <li><a href="/namesakes/">Namesakes</a></li>
<li><a href="/namedays/">Name Days</a></li> <li><a href="/submit/">Submitted Names</a></li>
</ul>
</li>
<li><a href="" onclick="return false;">Interact</a>
<ul class="btnsubmenu">
<li><a href="/bb/">Message Boards</a></li>
<li><a href="/polls/">Polls</a></li> <li><a href="/derby/">Predict Rankings</a></li> <li><a href="/submit/submit">Submit a Name</a></li>
</ul>
</li>
<li><a href="" onclick="return false;">Tools</a>
<ul class="btnsubmenu">
<li><a href="/random/">Random Renamer</a></li> <li><a href="/names/translate">Name Translator</a></li> <li><a href="/names/themes">Name Themes</a></li> <li><a href="/anagram/">Anagrams</a></li> <li><a href="/guide/">Baby Name Expert</a></li> <li><a href="https://surnames.behindthename.com">Surname Site</a></li> <li><a href="https://places.behindthename.com">Place Name Site</a></li> </ul>
</li>
</ul>
</td>
<td id="menucell0">
<div id="mobileopts">
<div id="mobileoptsheading">
<img alt="Menu" src="/images/menu3.png" style="height:18px"/>
<span style="display:block;font-size:12px;line-height:12px;margin-top:6px;">Menu</span>
</div>
<div id="mobilemenu">
<ul id="mobilemenu-user">
<li><a href="https://www.behindthename.com/members/login">Sign In</a></li>
<li><a href="https://www.behindthename.com/members/signup">Register</a></li>
<li><hr/></li>
<li><a href="https://surnames.behindthename.com">Surname Site</a></li> <li><a href="https://places.behindthename.com">Place Name Site</a></li></ul>
<ul id="mobilemenu-main">
<li><a href="/info/names">Introduction</a></li>
<li><a href="/names/list">Browse Names</a></li>
<li><a href="/names/search">Advanced Search</a></li>
<li><a href="/top/">Popularity</a></li> <li><a href="/namesakes/">Namesakes</a></li>
<li><a href="/namedays/">Name Days</a></li> <li><a href="/submit/">Submitted Names</a></li>
<li><a href="/bb/">Message Boards</a></li>
<li><a href="/polls/">Polls</a></li> <li><a href="/derby/">Predict Rankings</a></li> <li><a href="/submit/submit">Submit a Name</a></li>
<li><a href="/random/">Random Renamer</a></li> <li><a href="/names/translate">Name Translator</a></li> <li><a href="/names/themes">Name Themes</a></li> <li><a href="/anagram/">Anagrams</a></li> <li><a href="/guide/">Baby Name Expert</a></li></ul>
</div>
</div>
</td>
<td id="menucell4">
<div style="padding:0px 10px">
<a href="https://www.behindthename.com/members/login">Sign In</a>  
<a href="https://www.behindthename.com/members/signup">Register</a>
</div>
</td>
</tr></table>
</div>
</div>
</nav>
<div style="text-align:center">
<div class="bannerad">
<!-- /192633929/behindthename-728x90-ATF -->
<div id="div-gpt-ad-1581477966538-0">
<script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1581477966538-0'); });
  </script>
</div>
</div>
</div>
<div class="body-wrapper">
<div class="body">
<div class="body-inner">
<div class="info_message"></div>
<center>
<div id="logo">
<img alt="Behind the Name" src="/images/btn6.png"/>
<div>the etymology and history of first names</div>
</div>
<form action="/names/search.php" method="get" style="margin:20px;">
<div id="main_search_box">
<table cellpadding="0" cellspacing="0"><tr>
<td><input autocapitalize="off" autocomplete="off" autocorrect="off" autofocus="" id="main_search" name="terms" spellcheck="false" type="text"/></td>
<td><button id="main_search_button" type="submit"><img alt="Search" src="/images/search_header.png"/></button></td>
<td><button id="main_search_settings" onclick="location.href='/names/search';" type="button"><img alt="Advanced" src="/images/gear_header.png"/></button></td>
</tr></table>
</div>
</form>
<script type="text/javascript">
$(function() {
	set_search_autocomplete("#main_search");
});
</script>
<div class="letterlinks">
<a href="/names/letter/a">A</a><a href="/names/letter/b">B</a><a href="/names/letter/c">C</a><a href="/names/letter/d">D</a><a href="/names/letter/e">E</a><a href="/names/letter/f">F</a><a href="/names/letter/g">G</a><a href="/names/letter/h">H</a><a href="/names/letter/i">I</a><a href="/names/letter/j">J</a><a href="/names/letter/k">K</a><a href="/names/letter/l">L</a><a href="/names/letter/m">M</a><a href="/names/letter/n">N</a><a href="/names/letter/o">O</a><a href="/names/letter/p">P</a><a href="/names/letter/q">Q</a><a href="/names/letter/r">R</a><a href="/names/letter/s">S</a><a href="/names/letter/t">T</a><a href="/names/letter/u">U</a><a href="/names/letter/v">V</a><a href="/names/letter/w">W</a><a href="/names/letter/x">X</a><a href="/names/letter/y">Y</a><a href="/names/letter/z">Z</a></div>
<div class="genderlinks">
<img alt="Masculine" src="/images/masculine.png" style="height:15px;width:15px;"/>  <a href="/names/gender/masculine">Masculine</a>    
<img alt="Feminine" src="/images/feminine.png" style="height:15px;width13px;"/>  <a href="/names/gender/feminine">Feminine</a>
</div>
<div class="usagelinks">
<div><a href="/names/usage/english">English Names</a></div>
<div><a href="/names/usage/french">French Names</a></div>
<div><a href="/names/usage/german">German Names</a></div>
<div><a href="/names/usage/italian">Italian Names</a></div>
<div><a href="/names/usage/spanish">Spanish Names</a></div>
<div><a href="/names/usage/arabic">Arabic Names</a></div>
<div><a href="/names/usage/indian">Indian Names</a></div>
<div><a href="/names/usage/irish">Irish Names</a></div>
<div><a href="/names/usage/mythology">Mythology Names</a></div>
<div><a href="/names/usage/biblical">Biblical Names</a></div>
<div><a href="/names/usage/african">African Names</a></div>
<div><b><a href="/names/list">More ...</a></b></div>
</div>
</center>
<div>
<div class="mainsection">
<div class="mainheading">
<div class="mainheadingfloat"><div>
<a href="/notd/" style="color:white">Previous</a>
</div></div>
<h1>Name of the Day</h1></div>
<div style="padding:10px 15px;">
<div class="notd-section"><a class="notd" href="/name/frank">FRANK</a><div class="notd-gender"><span class="masc">Masculine</span></div></div><div class="notd-section">From a Germanic name that referred to a member of the Germanic tribe, the Franks. The Franks settled in the regions now called France and the Netherlands in the 3rd and 4th century. They possibly derived their tribal name from the name of a type of spear that they used.</div><div class="notd-section"><a href="/name/frank">more...</a></div></div>
</div><!-- no linebreak here! --><div class="mainsection">
<div class="mainheading">
<div class="mainheadingfloat"><div>
<form action="" method="get">
<select class="popdrop" id="popcode" onchange="pop_swap(this);">
<option value="us">the United States
</option><option value="au">Australia
</option><option value="at">Austria
</option><option value="be">Belgium
</option><option value="ca">Canada
</option><option value="ch">Chile
</option><option value="dk">Denmark
</option><option value="ew">England &amp; Wales
</option><option value="fi">Finland
</option><option value="fr">France
</option><option value="hu">Hungary
</option><option value="ir">Ireland
</option><option value="it">Italy
</option><option value="lt">Lithuania
</option><option value="nl">the Netherlands
</option><option value="no">Norway
</option><option value="pl">Poland
</option><option value="sc">Scotland
</option><option value="sp">Spain
</option><option value="sw">Sweden
</option><option value="sl">Slovenia
</option></select>
</form>
</div></div>
<h1>Popular Names</h1></div>
<div style="padding:10px 15px;">
<center>
<table class="poptable">
<tbody id="popbody">
<tr><td class="rank">1.</td><td><a href="/name/liam">Liam</a></td><td class="topspacer"></td><td class="rank">1.</td><td><a href="/name/olivia">Olivia</a></td></tr>
<tr><td class="rank">2.</td><td><a href="/name/noah-1">Noah</a></td><td class="topspacer"></td><td class="rank">2.</td><td><a href="/name/emma">Emma</a></td></tr>
<tr><td class="rank">3.</td><td><a href="/name/oliver">Oliver</a></td><td class="topspacer"></td><td class="rank">3.</td><td><a href="/name/ava-1">Ava</a></td></tr>
<tr><td class="rank">4.</td><td><a href="/name/william">William</a></td><td class="topspacer"></td><td class="rank">4.</td><td><a href="/name/sophia">Sophia</a></td></tr>
<tr><td class="rank">5.</td><td><a href="/name/elijah">Elijah</a></td><td class="topspacer"></td><td class="rank">5.</td><td><a href="/name/isabella">Isabella</a></td></tr>
<tr><td class="rank">6.</td><td><a href="/name/james">James</a></td><td class="topspacer"></td><td class="rank">6.</td><td><a href="/name/charlotte">Charlotte</a></td></tr>
<tr><td class="rank">7.</td><td><a href="/name/benjamin">Benjamin</a></td><td class="topspacer"></td><td class="rank">7.</td><td><a href="/name/amelia">Amelia</a></td></tr>
<tr><td class="rank">8.</td><td><a href="/name/lucas">Lucas</a></td><td class="topspacer"></td><td class="rank">8.</td><td><a href="/name/mia">Mia</a></td></tr>
<tr><td class="rank">9.</td><td><a href="/name/mason">Mason</a></td><td class="topspacer"></td><td class="rank">9.</td><td><a href="/name/harper">Harper</a></td></tr>
<tr><td class="rank">10.</td><td><a href="/name/ethan">Ethan</a></td><td class="topspacer"></td><td class="rank">10.</td><td><a href="/name/evelyn">Evelyn</a></td></tr>
<tr><td align="center" colspan="5"><a href="/top/lists/united-states/2019">more...</a></td></tr>
</tbody>
</table>
<script type="text/javascript">
<!--
function pop_swap(sel) {
	var region = $('#popcode').val();
	$('.poptable').css('height', $('.poptable').height()+'px');
	$('#popbody').html('<'+'tr><'+'td><'+'/td><'+'/tr>');
	$.get('/top/ajax_get_top.php?region='+region, function(data) {
		$('#popbody').html(data);
		$('.poptable').css('height', '');
	});
}
$(function() {
	if ($('#popcode').val() != 'us') {
		pop_swap();
	}
});
//-->
</script>
</center>
</div>
</div>
<div class="features">
<div class="featuresheading"><h2><span>Popular Features</span></h2></div>
<div>
<center>
<div class="feature"><a href="/bb/"><img src="/images/boards.png"/><br/>Message Boards</a></div>
<div class="feature"><a href="/random/"><img src="/images/random.png"/><br/>Random Name</a></div>
<div class="feature"><a href="/top/"><img src="/images/popular.png"/><br/>Most Common</a></div>
<div class="feature"><a href="https://surnames.behindthename.com"><img src="/images/surnames.png"/><br/>Surnames</a></div>
</center>
</div>
</div>
</div>
<div class="social_home">
<a class="socic socic-facebook" href="https://www.facebook.com/BehindTheNamedotcom" target="_blank"><img src="/images/social/facebook.svg" style="width:26px;height:26px;"/></a><a class="socic socic-twitter" href="https://www.twitter.com/onomast" target="_blank"><img src="/images/social/twitter.svg" style="width:26px;height:26px;"/></a><a class="socic socic-pinterest" href="https://www.pinterest.com/behindthename" target="_blank"><img src="/images/social/pinterest.svg" style="width:26px;height:26px;"/></a></div>
</div>
</div>
</div>
<footer>
<div class="footer">
<table style="width:100%">
<tr><td style="padding:3px" valign="top">
<span class="tiny">
<a href="/info/news">Updated November 20, 2020</a>
</span>
</td><td align="right" style="padding:3px">
<nav>
<span class="tiny">
<a href="/info">About</a> ·
<a href="/info/help">Help</a> ·
<a href="/info/copyright">Copyright</a> ·
<a href="/info/terms">Terms</a> ·
<a href="/info/privacy">Privacy</a> ·
<a href="/info/contact">Contact</a>
</span>
</nav>
</td></tr>
</table>
</div>
</footer>
</body>
</html>
