<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Shana Fishbein</title>
<style media="all" type="text/css">@import "style.css";</style>
<meta content="Shana Fishbein" name="author"/>
<meta content="Shana Fishbein is a visual effects compositor specialising in Inferno, Flame, AE and Nuke." name="description"/>
<meta content="Shana Fishbein, visual effects, vfx, compositing, Inferno, Flame, AE, After Effects, Nuke" name="keywords"/>
<meta content="copyright © 2004 Shana Fishbein" name="copyright"/>
</head>
<body>
<div id="container">
<a href="index.php"><img src="images/WebHeaderImageCrop.gif"/></a>
href="index.php"&gt;<img src="images/HeaderName5.gif"/>
<div id="menu">
<li><a href="about.php">about</a></li>
<li><a href="resume.php">resume</a></li>
<li><a href="movies.php">movies</a></li>
<li><a href="contact.php">contact</a></li>
</div>
<div id="main">
<p>I love compositing and graphics. I enjoy the problem solving. And now I am also enjoying producing and coordinating projects.</p>
<p>I started in the post-production industry in NYC in 1992, where I was an intern at MTV
		 	using Quantel Paintbox.  Since then I have worked on films, commercials, and TV shows mainly as a compositor and at times supervised on set, 
			been a software demonstrator, a certified Flame trainer, and I've taught compositing courses.
			Most recently, I have taken on a marketing/advertising role which has included video production.  I am now transitioning to producer / production manager.  </p>
<p>From 1996 to 2004 I lived and worked as a freelancer in Sydney, Australia. 
			I did some great work there.</p>
<p>I've used a lot of compositing software in my career:  Various Quantel boxes, Illusion, Inferno/Flame/Combustion,  
			Shake, AE, Nuke, Photoshop and a few others. Recently I have started to use InDesign and Illustrator for designing and layout of marketing material.</p>
<p>I feel extremely lucky that I'm doing 
			what what I enjoy and look forward to each new project. </p>
<p>I am now based in NYC and Baltimore, MD.  &amp; I can work in Australia and USA.</p>
		

			
			
			&gt;
			&gt;
			<p>Please visit my Vimeo for more work: </p>
<a href="http://vimeo.com/shoshanafishbein/albums">http://vimeo.com/shoshanafishbein/albums</a>
</div>
<div id="footer">
<div class="copy"></div>
</div>
</div></body>
</html>