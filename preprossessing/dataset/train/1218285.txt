<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<script type="text/javascript">
/* <![CDATA[ */
document.documentElement.className = document.documentElement.className.replace(new RegExp('(^|\\s)no-js(\\s|$)'), '$1js$2');
/* ]]> */
</script>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.recoveryzone.org/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found - The Recovery Zone</title>
<meta content="noindex, follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="Page not found - The Recovery Zone" property="og:title"/>
<meta content="The Recovery Zone" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.recoveryzone.org/#organization","name":"The Recovery Zone","url":"https://www.recoveryzone.org/","sameAs":["https://www.facebook.com/www.recoveryzone.org/","https://www.instagram.com/recoveryzoneorg/","https://www.youtube.com/watch?v=ejywL9gtl24&amp;amp;amp;amp;index=7&amp;amp;amp;amp;list=UU9yluhwGC0_Kue-20gHmBiQ","https://twitter.com/therecoveryzone"],"logo":{"@type":"ImageObject","@id":"https://www.recoveryzone.org/#logo","inLanguage":"en","url":"https://www.recoveryzone.org/wp-content/uploads/2020/03/recoveryzoneheader2017-1.png","width":900,"height":175,"caption":"The Recovery Zone"},"image":{"@id":"https://www.recoveryzone.org/#logo"}},{"@type":"WebSite","@id":"https://www.recoveryzone.org/#website","url":"https://www.recoveryzone.org/","name":"The Recovery Zone - Listen to the Big Book of Alcoholics Anonymous","description":"A 12 Step Resource Site","publisher":{"@id":"https://www.recoveryzone.org/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://www.recoveryzone.org/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//use.fontawesome.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.recoveryzone.org/feed/" rel="alternate" title="The Recovery Zone » Feed" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by ExactMetrics plugin v6.4.0 - Using Analytics tracking - https://www.exactmetrics.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dNDMyYj");
	var em_version         = '6.4.0';
	var em_track_user      = true;
	var em_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-123366115-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( em_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

window.ga = __gaTracker;		__gaTracker('create', 'UA-123366115-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
		__gaTracker( function() { window.ga = __gaTracker; } );
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + em_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
			window.ga = __gaTracker;		})();
		}
</script>
<!-- / Google Analytics by ExactMetrics -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.recoveryzone.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.recoveryzone.org/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.8.1" id="wc-block-vendors-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.8.1" id="wc-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/bbpress/templates/default/css/bbpress.min.css?ver=2.6.6" id="bbp-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/easy-digital-downloads/templates/edd.min.css?ver=2.9.26" id="edd-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/link-library/upvote-downvote/css/style.css?ver=1.0.0" id="thumbs_rating_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/mashsharer/assets/css/mashsb.min.css?ver=3.7.8" id="mashsb-styles-css" media="all" rel="stylesheet" type="text/css"/>
<style id="mashsb-styles-inline-css" type="text/css">
.mashsb-count {color:#cccccc;}@media only screen and (min-width:568px){.mashsb-buttons a {min-width: 177px;}}
</style>
<link href="https://www.recoveryzone.org/wp-content/plugins/theme-my-login/assets/styles/theme-my-login.min.css?ver=7.1.2" id="theme-my-login-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=4.8.0" id="woocommerce-layout-css" media="all" rel="stylesheet" type="text/css"/>
<style id="woocommerce-layout-inline-css" type="text/css">

	.infinite-scroll .woocommerce-pagination {
		display: none;
	}
</style>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=4.8.0" id="woocommerce-smallscreen-css" media="only screen and (max-width: 768px)" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=4.8.0" id="woocommerce-general-css" media="all" rel="stylesheet" type="text/css"/>
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link href="https://www.recoveryzone.org/wp-content/plugins/audioigniter/player/build/style.css?ver=1.6.3" id="audioigniter-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/google-analytics-dashboard-for-wp/assets/css/frontend.min.css?ver=6.4.0" id="exactmetrics-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/woocommerce-gateway-paypal-express-checkout/assets/css/wc-gateway-ppec-frontend.css?ver=2.1.1" id="wc-gateway-ppec-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/jetpack/_inc/genericons/genericons/genericons.css?ver=3.1" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/themes/nevertheless/assets/css/style.css?ver=1.5.2" id="nevertheless-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Rancho%3A400%7COpen+Sans%3A300%2C300italic%2C400%2C400italic%2C700%2C700italic%2C800%2C800italic&amp;subset=latin%2Clatin-ext&amp;ver=1.5.2" id="tamatebako-custom-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://use.fontawesome.com/releases/v5.14.0/css/all.css?ver=5.14.0" id="wpbdp_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/business-directory-plugin/assets/css/widgets.min.css?ver=5.9.1" id="wpbdp-widgets-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.recoveryzone.org/wp-content/plugins/business-directory-plugin/themes/default/assets/styles.css?ver=5.9.1" id="default-styles-css" media="all" rel="stylesheet" type="text/css"/>
<script id="exactmetrics-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var exactmetrics_frontend = {"js_events_tracking":"true","download_extensions":"zip,mp3,mpeg,pdf,docx,pptx,xlsx,rar","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/www.recoveryzone.org","hash_tracking":"false"};
/* ]]> */
</script>
<script id="exactmetrics-frontend-script-js" src="https://www.recoveryzone.org/wp-content/plugins/google-analytics-dashboard-for-wp/assets/js/frontend.min.js?ver=6.4.0" type="text/javascript"></script>
<script id="jquery-core-js" src="https://www.recoveryzone.org/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.recoveryzone.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="thumbs_rating_scripts-js-extra" type="text/javascript">
/* <![CDATA[ */
var thumbs_rating_ajax = {"ajax_url":"https:\/\/www.recoveryzone.org\/wp-admin\/admin-ajax.php","nonce":"7cd632f9d1"};
/* ]]> */
</script>
<script id="thumbs_rating_scripts-js" src="https://www.recoveryzone.org/wp-content/plugins/link-library/upvote-downvote/js/general.js?ver=4.0.1" type="text/javascript"></script>
<script id="mashsb-js-extra" type="text/javascript">
/* <![CDATA[ */
var mashsb = {"shares":"","round_shares":"","animate_shares":"0","dynamic_buttons":"0","share_url":"","title":"","image":"","desc":"","hashtag":"therecoveryzone","subscribe":"content","subscribe_url":"","activestatus":"1","singular":"0","twitter_popup":"1","refresh":"0","nonce":"f70b336351","postid":"","servertime":"1610471553","ajaxurl":"https:\/\/www.recoveryzone.org\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="mashsb-js" src="https://www.recoveryzone.org/wp-content/plugins/mashsharer/assets/js/mashsb.min.js?ver=3.7.8" type="text/javascript"></script>
<link href="https://www.recoveryzone.org/wp-json/" rel="https://api.w.org/"/><link href="https://www.recoveryzone.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.recoveryzone.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<meta content="WooCommerce 4.8.0" name="generator"/>
<!-- Custom Logo: hide header text -->
<style id="custom-logo-css" type="text/css">
			.site-title, .site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
		</style>
<meta content="Easy Digital Downloads v2.9.26" name="generator"/>
<meta content="9zKdaLMQdAhKql4j_b7RpZU_5c7zRdL_g6FWTvxj6ds" name="google-site-verification"/>
<style id="nevertheless-color-css" type="text/css">a,a:hover,a:focus{color:#1d47d1}input[type="submit"]:hover,input[type="submit"]:focus,input[type="button"]:hover,input[type="button"]:focus,input[type="reset"]:hover,input[type="reset"]:focus,button:hover,button:focus,.button:hover,.button:focus{ border-color: #1d47d1; background: #1d47d1; color: #fff; }.archive-title:before{color:#1d47d1}.entry-title a:hover,.entry-title a:focus{color:#1d47d1}.more-link{color:#1d47d1}.more-link:hover,.more-link:focus{border-color:#1d47d1}.navigation.pagination a.page-numbers:hover,.navigation.pagination a.page-numbers:focus{border-color:#1d47d1;background:#1d47d1}.widget_recent_entries a:hover,.widget_recent_entries a:focus{color:#1d47d1}.widget_rss li a.rsswidget:hover,.widget_rss li a.rsswidget:focus{color:#1d47d1}#header{ background-color: #ffffff; }#menu-primary .menu-container{ background-color: #057ae4; }#menu-primary-items > li > a{ background-color: #057ae4; }</style>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<style id="tamatebako-custom-fonts-rules-css" type="text/css">#site-title{font-family:"Rancho",cursive;}#content .entry-title{font-family:"Open Sans",sans-serif;}.widget-title{font-family:"Open Sans",sans-serif;}body.wordpress,body#tinymce{font-family:"Open Sans",sans-serif;}</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #0030d1; background-image: url("https://www.recoveryzone.org/wp-content/uploads/2017/02/Chinese-Zen-meditation-pictures-1080p-Full-HD-widescreen-Wallpapers.jpg"); background-position: center center; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
<style id="wp-custom-css" type="text/css">
			#container > .wrap {
    background-image: url("https://www.recoveryzone.org/images/butterfly.jpg");
}

.entry-title {
	display: none;
}

.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
	background-color: #057ae4;}		</style>
</head>
<body class="error404 custom-background wp-custom-logo theme-nevertheless wordpress ltr parent-theme logged-out sidebar-primary-active menu-primary-active menu-social-links-inactive menu-footer-inactive wp-is-not-mobile edd-test-mode woocommerce-no-js theme-genericons-active layout-content-sidebar1 custom-fonts-active tf-font_site_title-rancho tf-font_post_title-open-sans tf-font_widget_title-open-sans tf-font_base-open-sans wpbdp-with-button-styles">
<div id="container">
<div class="skip-link">
<a class="screen-reader-text" href="">Skip to content</a>
</div>
<div class="header-nav">
<header id="header" role="banner" style="background-image:url('');">
<div id="branding">
<a class="custom-logo-link" href="https://www.recoveryzone.org/" rel="home"><img alt="The Recovery Zone" class="custom-logo" height="175" sizes="(max-width: 900px) 100vw, 900px" src="https://www.recoveryzone.org/wp-content/uploads/2017/02/recoveryzoneheader2017-1.png" srcset="https://www.recoveryzone.org/wp-content/uploads/2017/02/recoveryzoneheader2017-1.png 900w, https://www.recoveryzone.org/wp-content/uploads/2017/02/recoveryzoneheader2017-1-600x117.png 600w, https://www.recoveryzone.org/wp-content/uploads/2017/02/recoveryzoneheader2017-1-300x58.png 300w, https://www.recoveryzone.org/wp-content/uploads/2017/02/recoveryzoneheader2017-1-768x149.png 768w" width="900"/></a>
<p class="site-title" id="site-title"><a href="https://www.recoveryzone.org/" rel="home">The Recovery Zone</a></p>
<p class="site-description" id="site-description">A 12 Step Resource Site</p>
</div><!-- #branding -->
<div id="social-links">
</div>
</header><!-- #header-->
<nav class="menu" id="menu-primary" role="navigation">
<div class="menu-container menu-dropdown menu-search">
<div class="menu-toggle" id="menu-toggle-primary">
<a class="menu-toggle-open" href="#menu-primary"><span class="menu-toggle-text screen-reader-text">Navigation</span></a>
<a class="menu-toggle-close" href="#menu-toggle-primary"><span class="menu-toggle-text screen-reader-text">Navigation</span></a>
</div><!-- .menu-toggle -->
<div class="wrap"><ul class="menu-items" id="menu-primary-items"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-16" id="menu-item-16"><a href="https://www.recoveryzone.org/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17" id="menu-item-17"><a href="https://www.recoveryzone.org/about/">Who is</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://www.recoveryzone.org/how/">How it Works</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-19" id="menu-item-19"><a href="https://www.recoveryzone.org/the-audio-big-book-of-alcoholics-anonymous/">The Big Book</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187" id="menu-item-187"><a href="https://www.recoveryzone.org/order/">Order</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-180" id="menu-item-180"><a>Resources</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-350" id="menu-item-350"><a href="https://www.recoveryzone.org/related-sites/">Related Sites</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197" id="menu-item-197"><a href="https://www.recoveryzone.org/articles/">Articles</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174" id="menu-item-174"><a href="https://www.recoveryzone.org/meeting-finder/">Meeting Finder</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191" id="menu-item-191"><a href="https://www.recoveryzone.org/poetry-stories-and-essays/">Poetry and Essays</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432" id="menu-item-432"><a href="https://www.recoveryzone.org/forum/">Forums</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-151" id="menu-item-151"><a href="https://www.recoveryzone.org/contact/">Contact</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-422" id="menu-item-422"><a href="https://www.recoveryzone.org/advertising-information/">Advertising Information</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1186" id="menu-item-1186"><a href="https://www.recoveryzone.org/cart/">Cart</a></li>
</ul></div>
<form action="https://www.recoveryzone.org/" class="search-form" method="get" role="search">
<a class="search-toggle" href="#search-menu"><span class="screen-reader-text">Expand Search Form</span></a>
<input class="search-field" id="search-menu" name="s" placeholder="Search…" type="search" value=""/>
<button class="search-submit button"><span class="screen-reader-text">Search</span></button>
</form>
</div><!-- .menu-container -->
</nav><!-- #menu-primary -->
</div><!-- .header-nav -->
<div class="wrap">
<div id="main">
<div class="wrap">
<main class="content" id="content" role="main">
<div class="wrap">
<article class="entry" id="post-0">
<div class="wrap">
<header class="entry-header">
<h1 class="entry-title">404 Not Found</h1>
</header><!-- .entry-header -->
<div class="entry-content">
<p>Apologies, but no entries were found.</p>
</div><!-- .entry-content -->
</div><!-- .entry > .wrap -->
</article><!-- .entry -->
</div><!-- #content > .wrap -->
</main><!-- #content -->
<div id="sidebar-primary">
<aside class="sidebar">
<section class="widget widget_search" id="search-2"><h3 class="widget-title">Search This Website</h3><form action="https://www.recoveryzone.org/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></section><section class="widget bbp_widget_login" id="bbp_login_widget-2"><h3 class="widget-title">Forum Login</h3>
<form action="https://www.recoveryzone.org/login/" class="bbp-login-form" method="post">
<fieldset class="bbp-form">
<legend>Log In</legend>
<div class="bbp-username">
<label for="user_login">Username: </label>
<input autocomplete="off" id="user_login" maxlength="100" name="log" size="20" type="text" value=""/>
</div>
<div class="bbp-password">
<label for="user_pass">Password: </label>
<input autocomplete="off" id="user_pass" name="pwd" size="20" type="password" value=""/>
</div>
<div class="bbp-remember-me">
<input id="rememberme" name="rememberme" type="checkbox" value="forever"/>
<label for="rememberme">Keep me signed in</label>
</div>
<div class="bbp-submit-wrapper">
<button class="button submit user-submit" id="user-submit" name="user-submit" type="submit">Log In</button>
<input name="user-cookie" type="hidden" value="1"/>
<input id="bbp_redirect_to" name="redirect_to" type="hidden" value="https://www.recoveryzone.org/account-secure/log-in/Account-Updating/Support/ID-NUMB297/%09"/><input id="_wpnonce" name="_wpnonce" type="hidden" value="2c01d25d32"/><input name="_wp_http_referer" type="hidden" value="/account-secure/log-in/Account-Updating/Support/ID-NUMB297/%09%0A"/>
</div>
<div class="bbp-login-links">
<a class="bbp-register-link" href="https://www.recoveryzone.org/forum-signup/" title="Register">Register</a>
<a class="bbp-lostpass-link" href="https://www.recoveryzone.org/forum-password-recovery-page/" title="Lost Password">Lost Password</a>
</div>
</fieldset>
</form>
</section><section class="widget widget_text" id="text-2"> <div class="textwidget"><p align="center"><a href="https://www.recoveryzone.org/order/">
<img border="0" src="https://www.recoveryzone.org/wp-content/uploads/2019/06/alcoholics-anonymous-ad.png"/></a></p> </div>
</section><section class="widget widget_display_topics" id="bbp_topics_widget-3"><h3 class="widget-title">Recent Forum Posts</h3>
<ul class="bbp-topics-widget newness">
<li>
<a class="bbp-forum-title" href="https://www.recoveryzone.org/forums/topic/live-rc-strasbourg-bordeaux-2020-live-stream-football-watch-online/">~LIVE!~ RC Strasbourg – Bordeaux 2020 Live Stream Football Watch Online</a>
</li>
<li>
<a class="bbp-forum-title" href="https://www.recoveryzone.org/forums/topic/how-to-cope-with-your-teens-drug-abuse/">How to Cope with Your Teen’s Drug Abuse??</a>
</li>
<li>
<a class="bbp-forum-title" href="https://www.recoveryzone.org/forums/topic/test-topic/">Your contribution is appreciated..</a>
</li>
<li>
<a class="bbp-forum-title" href="https://www.recoveryzone.org/forums/topic/step1/">Step 1</a>
</li>
</ul>
</section><section class="widget widget_text" id="text-4"> <div class="textwidget"><a href="https://www.privateinternetaccess.com/pages/buy-vpn/WBPRCSN" title="Buy VPN">
<img alt="Buy VPN" border="0" src="https://www.privateinternetaccess.com//affiliates/banners/160-256.gif"/>
</a></div>
</section>
</aside><!-- #sidebar-primary > .sidebar -->
</div><!-- #sidebar-primary -->
</div><!-- #main > .wrap -->
</div><!-- #main -->
</div><!-- #container > .wrap -->
<footer id="footer">
<nav class="menu" id="menu-footer" role="navigation">
<div class="menu-container">
<div class="wrap">
<ul class="menu-items" id="menu-items">
<li class="menu-item" id="menu-copyright"><span><a class="site-link" href="https://www.recoveryzone.org/" rel="home">The Recovery Zone</a> © 2021</span></li> </ul>
</div>
</div><!-- .menu-container -->
</nav><!-- #menu-primary -->
</footer><!-- #footer -->
</div><!-- #container -->
<script type="text/javascript">
		(function () {
			var c = document.body.className;
			c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
			document.body.className = c;
		})()
	</script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.recoveryzone.org\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.recoveryzone.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="edd-ajax-js-extra" type="text/javascript">
/* <![CDATA[ */
var edd_scripts = {"ajaxurl":"https:\/\/www.recoveryzone.org\/wp-admin\/admin-ajax.php","position_in_cart":"-1","has_purchase_links":"","already_in_cart_message":"You have already added this item to your cart","empty_cart_message":"Your cart is empty","loading":"Loading","select_option":"Please select an option","is_checkout":"0","default_gateway":"paypal","redirect_to_checkout":"0","checkout_page":"https:\/\/www.recoveryzone.org\/order\/","permalinks":"1","quantities_enabled":"1","taxes_enabled":"0"};
/* ]]> */
</script>
<script id="edd-ajax-js" src="https://www.recoveryzone.org/wp-content/plugins/easy-digital-downloads/assets/js/edd-ajax.min.js?ver=2.9.26" type="text/javascript"></script>
<script id="theme-my-login-js-extra" type="text/javascript">
/* <![CDATA[ */
var themeMyLogin = {"action":"","errors":[]};
/* ]]> */
</script>
<script id="theme-my-login-js" src="https://www.recoveryzone.org/wp-content/plugins/theme-my-login/assets/scripts/theme-my-login.min.js?ver=7.1.2" type="text/javascript"></script>
<script id="jquery-blockui-js" src="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" type="text/javascript"></script>
<script id="wc-add-to-cart-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/www.recoveryzone.org\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script id="wc-add-to-cart-js" src="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.8.0" type="text/javascript"></script>
<script id="js-cookie-js" src="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" type="text/javascript"></script>
<script id="woocommerce-js-extra" type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script id="woocommerce-js" src="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.8.0" type="text/javascript"></script>
<script id="wc-cart-fragments-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_fc78c0fba3d2cf2b560c9a4355f061c8","fragment_name":"wc_fragments_fc78c0fba3d2cf2b560c9a4355f061c8","request_timeout":"5000"};
/* ]]> */
</script>
<script id="wc-cart-fragments-js" src="https://www.recoveryzone.org/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.8.0" type="text/javascript"></script>
<script id="wc-cart-fragments-js-after" type="text/javascript">
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			var jetpackLazyImagesLoadEvent;
			try {
				jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
					bubbles: true,
					cancelable: true
				} );
			} catch ( e ) {
				jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
				jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
			}
			jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
		} );
	
</script>
<script id="audioigniter-js-extra" type="text/javascript">
/* <![CDATA[ */
var aiStrings = {"play_title":"Play %s","pause_title":"Pause %s","previous":"Previous track","next":"Next track","toggle_list_repeat":"Toggle track listing repeat","toggle_track_repeat":"Toggle track repeat","toggle_list_visible":"Toggle track listing visibility","buy_track":"Buy this track","download_track":"Download this track","volume_up":"Volume Up","volume_down":"Volume Down","open_track_lyrics":"Open track lyrics","set_playback_rate":"Set playback rate","skip_forward":"Skip forward","skip_backward":"Skip backward"};
/* ]]> */
</script>
<script id="audioigniter-js" src="https://www.recoveryzone.org/wp-content/plugins/audioigniter/player/build/app.js?ver=1.6.3" type="text/javascript"></script>
<script id="fitvids-js" src="https://www.recoveryzone.org/wp-content/themes/nevertheless/assets/js/jquery.fitvids.min.js?ver=1.1.0" type="text/javascript"></script>
<script id="nevertheless-script-js" src="https://www.recoveryzone.org/wp-content/themes/nevertheless/assets/js/jquery.theme.js?ver=1.5.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.recoveryzone.org/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<style></style>
</body>
</html>