<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>AnalystForum - CFA Exam, CAIA Exam, FRM Exam Forums</title>
<meta content="CFA exam discussion for Chartered Financial Analyst candidates." name="description"/>
<meta content="Discourse 2.5.0.beta7 - https://github.com/discourse/discourse version 17c4f76eacb7f94de470c03850bf396d4b5170fe" name="generator"/>
<link href="https://analystforum-uploads.s3.dualstack.us-east-1.amazonaws.com/optimized/2X/5/5d51f2dcc332b4a2e1e4bc2072a3784222989a7c_2_32x32.png" rel="icon" type="image/png"/>
<link href="https://analystforum-uploads.s3.dualstack.us-east-1.amazonaws.com/optimized/2X/7/7c4e6862e7e9af5a346bd88dda9dc4cf35635a4e_2_180x180.png" rel="apple-touch-icon" type="image/png"/>
<meta content="#0088cc" name="theme-color"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=yes, viewport-fit=cover" name="viewport"/>
<link href="https://www.analystforum.com/" rel="canonical"/>
<script type="application/ld+json">{"@context":"http://schema.org","@type":"WebSite","url":"https://www.analystforum.com","potentialAction":{"@type":"SearchAction","target":"https://www.analystforum.com/search?q={search_term_string}","query-input":"required name=search_term_string"}}</script>
<link href="https://www.analystforum.com/opensearch.xml" rel="search" title="AnalystForum Search" type="application/opensearchdescription+xml"/>
<link data-target="desktop" data-theme-id="3" href="/stylesheets/desktop_2_d62139e13117e79180ffacb6d7fc0f9d14ce0081.css?__ws=www.analystforum.com" media="all" rel="stylesheet"/>
<link data-target="desktop_theme" data-theme-id="3" href="/stylesheets/desktop_theme_3_f724775ad7fc509ce1178212cb1ce87141f58081.css?__ws=www.analystforum.com" media="all" rel="stylesheet"/>
<script src="/theme-javascripts/83f22b922ed592c8aa0bb4f7b62a529737f5b179.js?__ws=www.analystforum.com"></script>
<script src="/theme-javascripts/d884b1b1f46a3d87cf381d32cf4d669c3a0d7cac.js?__ws=www.analystforum.com"></script>
<meta data-auto-link-domains="" data-json='{"cookieDomain":"auto"}' data-tracking-code="UA-20981924-1" id="data-ga-universal-analytics"/>
<link as="script" href="/assets/google-universal-analytics-00f5cdf7dfd45cba1ae2d258c3366c371c5671023250abbd964a1f16fc2c11a7.js" rel="preload"/>
<script src="/assets/google-universal-analytics-00f5cdf7dfd45cba1ae2d258c3366c371c5671023250abbd964a1f16fc2c11a7.js"></script>
<meta content="AnalystForum" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://analystforum-uploads.s3.dualstack.us-east-1.amazonaws.com/original/2X/7/7c4e6862e7e9af5a346bd88dda9dc4cf35635a4e.png" name="twitter:image"/>
<meta content="https://analystforum-uploads.s3.dualstack.us-east-1.amazonaws.com/original/2X/7/7c4e6862e7e9af5a346bd88dda9dc4cf35635a4e.png" property="og:image"/>
<meta content="https://www.analystforum.com/" property="og:url"/>
<meta content="https://www.analystforum.com/" name="twitter:url"/>
<meta content="AnalystForum" property="og:title"/>
<meta content="AnalystForum" name="twitter:title"/>
<meta content="CFA exam discussion for Chartered Financial Analyst candidates." property="og:description"/>
<meta content="CFA exam discussion for Chartered Financial Analyst candidates." name="twitter:description"/>
</head>
<body class="crawler">
<script src="/theme-javascripts/65597b2c363ae42c9d0e0f596c6ed3360abfead0.js?__ws=www.analystforum.com"></script>
<header>
<a href="/">
<img alt="AnalystForum" id="site-logo" src="https://analystforum-uploads.s3.dualstack.us-east-1.amazonaws.com/original/2X/8/8e7be8e6512cde25d070f18d332292fb5a3804d9.png" style="max-width: 150px;"/>
</a>
</header>
<div class="wrap" id="main-outlet">
<div itemscope="" itemtype="http://schema.org/ItemList">
<meta content="http://schema.org/ItemListOrderDescending" itemprop="itemListOrder"/>
<table class="category-list">
<thead>
<tr>
<th class="category">Category</th>
<th class="topics">Topics</th>
</tr>
</thead>
<tbody>
<tr>
<td class="category" style="border-color: #62BB47;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="0" itemprop="position"/>
<meta content="/c/test-prep/86" itemprop="url"/>
<h3>
<a href="/c/test-prep/86">
<span itemprop="name">Test Prep</span>
</a>
</h3>
<div itemprop="description">Find test prep solutions to help you pass.</div>
</div>
</td>
<td class="topics">
<div title="0 Topics">0</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #0088CC;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="1" itemprop="position"/>
<meta content="/c/cfa-exam-general-discussion/8" itemprop="url"/>
<h3>
<a href="/c/cfa-exam-general-discussion/8">
<span itemprop="name">CFA General</span>
</a>
</h3>
<div itemprop="description">General discussion of the CFA exam.</div>
</div>
</td>
<td class="topics">
<div title="19363 Topics">19363</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #78C3E5;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="2" itemprop="position"/>
<meta content="/c/cfa-exam-level-i/5" itemprop="url"/>
<h3>
<a href="/c/cfa-exam-level-i/5">
<span itemprop="name">CFA Level I</span>
</a>
</h3>
<div itemprop="description">The first step on your CFA journey.</div>
</div>
</td>
<td class="topics">
<div title="21300 Topics">21300</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #40A6D9;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="3" itemprop="position"/>
<meta content="/c/cfa-exam-level-II/6" itemprop="url"/>
<h3>
<a href="/c/cfa-exam-level-II/6">
<span itemprop="name">CFA Level II</span>
</a>
</h3>
<div itemprop="description">Get over the hump with the Level II CFA exam.</div>
</div>
</td>
<td class="topics">
<div title="28652 Topics">28652</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #0088CC;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="4" itemprop="position"/>
<meta content="/c/cfa-exam-level-iii/7" itemprop="url"/>
<h3>
<a href="/c/cfa-exam-level-iii/7">
<span itemprop="name">CFA Level III</span>
</a>
</h3>
<div itemprop="description">The final installment of the CFA trilogy. You’ve got this.</div>
</div>
</td>
<td class="topics">
<div title="27949 Topics">27949</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #Ff6600;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="5" itemprop="position"/>
<meta content="/c/caia-exam/9" itemprop="url"/>
<h3>
<a href="/c/caia-exam/9">
<span itemprop="name">CAIA</span>
</a>
</h3>
<div itemprop="description">The Chartered Alternative Investment Analyst exams.</div>
</div>
</td>
<td class="topics">
<div title="1164 Topics">1164</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #4A4C9F;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="6" itemprop="position"/>
<meta content="/c/frm-exam/10" itemprop="url"/>
<h3>
<a href="/c/frm-exam/10">
<span itemprop="name">FRM</span>
</a>
</h3>
<div itemprop="description">The Financial Risk Manager exams.</div>
</div>
</td>
<td class="topics">
<div title="1218 Topics">1218</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #981F1F;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="7" itemprop="position"/>
<meta content="/c/financial-modeling-valuation-analyst-exam/98" itemprop="url"/>
<h3>
<a href="/c/financial-modeling-valuation-analyst-exam/98">
<span itemprop="name">Financial Modeling &amp; Valuation Analyst</span>
</a>
</h3>
<div itemprop="description"><strong>Financial modeling and the FMVA exam.</strong></div>
</div>
</td>
<td class="topics">
<div title="24 Topics">24</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #5FB746;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="8" itemprop="position"/>
<meta content="/c/news-special-offers/55" itemprop="url"/>
<h3>
<a href="/c/news-special-offers/55">
<span itemprop="name">News and Special Offers</span>
</a>
</h3>
<div itemprop="description">Special offers, exam tips and more from our sponsors.</div>
</div>
</td>
<td class="topics">
<div title="0 Topics">0</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #CE4534;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="9" itemprop="position"/>
<meta content="/c/events/61" itemprop="url"/>
<h3>
<a href="/c/events/61">
<span itemprop="name">Events</span>
</a>
</h3>
<div itemprop="description">Events and key dates for the CFA, CAIA and FRM exams.</div>
</div>
</td>
<td class="topics">
<div title="0 Topics">0</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #5879BC;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="10" itemprop="position"/>
<meta content="/c/study-groups/84" itemprop="url"/>
<h3>
<a href="/c/study-groups/84">
<span itemprop="name">Study Groups</span>
</a>
</h3>
<div itemprop="description">Find local study groups for the CFA, CAIA and FRM exams.</div>
</div>
</td>
<td class="topics">
<div title="2601 Topics">2601</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #78C3E5;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="11" itemprop="position"/>
<meta content="/c/water-cooler/4" itemprop="url"/>
<h3>
<a href="/c/water-cooler/4">
<span itemprop="name">Water Cooler</span>
</a>
</h3>
<div itemprop="description">A category exclusive to members with trust level 1 and higher.</div>
</div>
</td>
<td class="topics">
<div title="11369 Topics">11369</div>
</td>
</tr>
<tr>
<td class="category" style="border-color: #808281;">
<div itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
<meta content="12" itemprop="position"/>
<meta content="/c/site-feedback/2" itemprop="url"/>
<h3>
<a href="/c/site-feedback/2">
<span itemprop="name">Site Feedback</span>
</a>
</h3>
<div itemprop="description">Discussion about this site, its organization, how it works, and how we can improve it.</div>
</div>
</td>
<td class="topics">
<div title="129 Topics">129</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<footer class="container wrap">
<nav class="crawler-nav">
<ul>
<li itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<span itemprop="name">
<a href="/" itemprop="url">Home </a>
</span>
</li>
<li itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<span itemprop="name">
<a href="/categories" itemprop="url">Categories </a>
</span>
</li>
<li itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<span itemprop="name">
<a href="/guidelines" itemprop="url">FAQ/Guidelines </a>
</span>
</li>
<li itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<span itemprop="name">
<a href="/tos" itemprop="url">Terms of Service </a>
</span>
</li>
<li itemscope="" itemtype="http://schema.org/SiteNavigationElement">
<span itemprop="name">
<a href="/privacy" itemprop="url">Privacy Policy </a>
</span>
</li>
</ul>
</nav>
<p class="powered-by-link">Powered by <a href="https://www.discourse.org">Discourse</a>, best viewed with JavaScript enabled</p>
</footer>
</body>
</html>
