<!DOCTYPE html>
<html class="no-touch" lang="nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.damedi.nl/xmlrpc.php" rel="pingback"/>
<title>Pagina niet gevonden - damedi</title>
<!-- This site is optimized with the Yoast SEO plugin v11.9 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="nl_NL" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Pagina niet gevonden - damedi" property="og:title"/>
<meta content="damedi" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="Pagina niet gevonden - damedi" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.damedi.nl/#website","url":"https://www.damedi.nl/","name":"damedi","potentialAction":{"@type":"SearchAction","target":"https://www.damedi.nl/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.damedi.nl" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.damedi.nl/feed/" rel="alternate" title="damedi » Feed" type="application/rss+xml"/>
<link href="https://www.damedi.nl/comments/feed/" rel="alternate" title="damedi » Reactiesfeed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.damedi.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.damedi.nl/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider.css?ver=6.7.6" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://www.damedi.nl/wp-includes/css/dashicons.min.css?ver=4.9.3" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-includes/js/thickbox/thickbox.css?ver=4.9.3" id="thickbox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-content/plugins/uncode-privacy/assets/css/uncode-privacy-public.css?ver=1.0.0" id="uncode-privacy-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-content/themes/uncode/library/css/style.css?ver=2136342130" id="uncode-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="uncode-style-inline-css" type="text/css">

@media (min-width: 960px) { .limit-width { max-width: 1200px; margin: auto;}}
@media (min-width: 960px) { .main-header, .vmenu-container { width: 360px; }}
.menu-primary ul.menu-smart > li > a, .menu-primary ul.menu-smart li.dropdown > a, .menu-primary ul.menu-smart li.mega-menu > a, .vmenu-container ul.menu-smart > li > a, .vmenu-container ul.menu-smart li.dropdown > a { text-transform: uppercase; }
</style>
<link href="https://www.damedi.nl/wp-content/themes/uncode/library/css/uncode-icons.css?ver=2136342130" id="uncode-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.damedi.nl/wp-content/themes/uncode/library/css/style-custom.css?ver=2136342130" id="uncode-custom-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="uncode-custom-style-inline-css" type="text/css">
.onepage-pagination{display:none !Important;}
</style>
<link href="//fonts.googleapis.com/css?family=Poppins%3A300%2Cregular%2C500%2C600%2C700%7CHind%3A300%2Cregular%2C500%2C600%2C700%7CDroid+Serif%3Aregular%2Citalic%2C700%2C700italic%7CDosis%3A200%2C300%2Cregular%2C500%2C600%2C700%2C800%7CPlayfair+Display%3Aregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7COswald%3A300%2Cregular%2C700%7CRoboto%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic%7CNunito%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C900%2C900italic&amp;subset=devanagari%2Clatin-ext%2Clatin%2Ccyrillic%2Cvietnamese%2Cgreek%2Ccyrillic-ext%2Cgreek-ext&amp;ver=1.9.1" id="uncodefont-google-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.damedi.nl/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var LS_Meta = {"v":"6.7.6"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/plugins/LayerSlider/static/layerslider/js/greensock.js?ver=1.19.0" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.7.6" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.transitions.js?ver=6.7.6" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"nl","strings":{"mejs.install-flash":"Je gebruikt een browser die geen Flash Player heeft ingeschakeld of ge\u00efnstalleerd. Zet de Flash Player-plugin aan of download de nieuwste versie van https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Volledig scherm uitzetten","mejs.fullscreen-on":"Volledig scherm","mejs.download-video":"Video downloaden","mejs.fullscreen":"Volledig scherm","mejs.time-jump-forward":["Ga 1 seconde vooruit","Ga %1 seconden vooruit"],"mejs.loop":"Loop aan-\/uitzetten ","mejs.play":"Afspelen","mejs.pause":"Pauzeren","mejs.close":"Sluiten","mejs.time-slider":"Tijdschuifbalk","mejs.time-help-text":"Gebruik de Links\/Rechts-pijltoetsen om \u00e9\u00e9n seconde vooruit te spoelen, Omhoog\/Omlaag-pijltoetsen om tien seconden vooruit te spoelen.","mejs.time-skip-back":["1 seconde terugspoelen","Ga %1 seconden terug"],"mejs.captions-subtitles":"Ondertitels","mejs.captions-chapters":"Hoofdstukken","mejs.none":"Geen","mejs.mute-toggle":"Geluid aan-\/uitzetten","mejs.volume-help-text":"Gebruik Omhoog\/Omlaag-pijltoetsen om het volume te verhogen of te verlagen.","mejs.unmute":"Geluid aan","mejs.mute":"Afbreken","mejs.volume-slider":"Volumeschuifbalk","mejs.video-player":"Videospeler","mejs.audio-player":"Audiospeler","mejs.ad-skip":"Advertentie overslaan","mejs.ad-skip-info":["Overslaan in 1 seconde","Overslaan in %1 seconden"],"mejs.source-chooser":"Kies bron","mejs.stop":"Stop","mejs.speed-rate":"Snelheidsniveau","mejs.live-broadcast":"Live uitzending","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanees","mejs.arabic":"Arabisch","mejs.belarusian":"Wit-Russisch","mejs.bulgarian":"Bulgaars","mejs.catalan":"Catalaans","mejs.chinese":"Chinees","mejs.chinese-simplified":"Chinees (Versimpeld)","mejs.chinese-traditional":"Chinees (Traditioneel)","mejs.croatian":"Kroatisch","mejs.czech":"Tsjechisch","mejs.danish":"Deens","mejs.dutch":"Nederlands","mejs.english":"Engels","mejs.estonian":"Estlands","mejs.filipino":"Filipijns","mejs.finnish":"Fins","mejs.french":"Frans","mejs.galician":"Galicisch","mejs.german":"Duits","mejs.greek":"Grieks","mejs.haitian-creole":"Ha\u00eftiaans Creools","mejs.hebrew":"Hebreeuws","mejs.hindi":"Hindi","mejs.hungarian":"Hongaars","mejs.icelandic":"IJslands","mejs.indonesian":"Indonesisch","mejs.irish":"Iers","mejs.italian":"Italiaans","mejs.japanese":"Japans","mejs.korean":"Koreaans","mejs.latvian":"Lets","mejs.lithuanian":"Litouws","mejs.macedonian":"Macedonisch","mejs.malay":"Maleis","mejs.maltese":"Maltees","mejs.norwegian":"Noors","mejs.persian":"Perzisch","mejs.polish":"Pools","mejs.portuguese":"Portugees","mejs.romanian":"Roemeens","mejs.russian":"Russisch","mejs.serbian":"Servisch","mejs.slovak":"Slovaaks","mejs.slovenian":"Sloveens","mejs.spanish":"Spaans","mejs.swahili":"Swahili","mejs.swedish":"Zweeds","mejs.tagalog":"Tagalog","mejs.thai":"Thais","mejs.turkish":"Turks","mejs.ukrainian":"Oekra\u00efens","mejs.vietnamese":"Vietnamees","mejs.welsh":"Wels","mejs.yiddish":"Jiddisch"}};
</script>
<script src="https://www.damedi.nl/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=4.9.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script data-breakpoints-images="258,516,720,1032,1440,2064,2880" data-home="/" data-path="/" id="uncodeAI" src="/wp-content/themes/uncode/library/js/ai-uncode.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var SiteParameters = {"days":"days","hours":"hours","minutes":"minutes","seconds":"seconds","constant_scroll":"on","scroll_speed":"2","parallax_factor":"0.25","loading":"Loading\u2026","slide_name":"slide","slide_footer":"footer","ajax_url":"https:\/\/www.damedi.nl\/wp-admin\/admin-ajax.php","nonce_adaptive_images":"24e8b9cbc9"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/themes/uncode/library/js/init.js?ver=2136342130" type="text/javascript"></script>
<meta content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." name="generator"/>
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<link href="https://www.damedi.nl/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.damedi.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.damedi.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.3" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.damedi.nl/wp-content/plugins/uncode-js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<link href="https://www.damedi.nl/wp-content/uploads/2017/06/cropped-damedi_flavicon_ronde-hoek-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.damedi.nl/wp-content/uploads/2017/06/cropped-damedi_flavicon_ronde-hoek-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.damedi.nl/wp-content/uploads/2017/06/cropped-damedi_flavicon_ronde-hoek-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.damedi.nl/wp-content/uploads/2017/06/cropped-damedi_flavicon_ronde-hoek-270x270.png" name="msapplication-TileImage"/>
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<script async="" data-cbid="0dac39cc-0c4a-446a-974d-0461a1dda25a" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script>
</head>
<body class="error404 style-color-lxmt-bg smooth-scroller vmenu vmenu-middle vmenu-center vmenu-position-left header-full-width main-center-align menu-mobile-animated menu-mobile-transparent wpb-js-composer js-comp-ver-5.5.2 vc_responsive" data-border="0">
<div class="body-borders" data-border="0"><div class="top-border body-border-shadow"></div><div class="right-border body-border-shadow"></div><div class="bottom-border body-border-shadow"></div><div class="left-border body-border-shadow"></div><div class="top-border style-light-bg"></div><div class="right-border style-light-bg"></div><div class="bottom-border style-light-bg"></div><div class="left-border style-light-bg"></div></div> <div class="box-wrapper">
<div class="box-container">
<script type="text/javascript">UNCODE.initBox();</script>
<div class="main-header">
<div class="masthead-vertical menu-hide-only-vertical" id="masthead">
<div class="vmenu-container menu-container menu-primary menu-light submenu-light style-light-original style-color-xsdn-bg vmenu-borders menu-shadows menu-no-arrows menu-hide-vertical">
<div class="row row-parent"><div class="row-inner restrict row-brand">
<div class="col-lg-12 logo-container" id="logo-container-mobile">
<div class="style-light">
<a class="navbar-brand" data-minheight="14" href="https://www.damedi.nl"><div class="logo-image logo-skinnable" data-maxheight="60" style="height: 60px;"><img alt="logo" class="img-responsive" height="70" src="https://www.damedi.nl/wp-content/uploads/2017/06/logo-damedi_final-01.svg" width="200"/></div></a>
</div>
<div class="mmb-container"><div class="mobile-menu-button mobile-menu-button-light lines-button x2"><span class="lines"></span></div></div>
</div>
</div><div class="row-inner expand">
<div class="main-menu-container">
<div class="vmenu-row-wrapper">
<div class="vmenu-wrap-cell">
<div class="row-inner expand">
<div class="menu-sidebar navbar-main">
<div class="menu-sidebar-inner">
<div class="menu-accordion"><ul class="menu-primary-inner menu-smart sm sm-vertical" id="menu-17"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58189 menu-item-link" id="menu-item-58189"><a href="#welcome" title="home">home<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58190 menu-item-link" id="menu-item-58190"><a href="#over-ons" title="over ons">over ons<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58191 menu-item-link" id="menu-item-58191"><a href="#onze-diensten" title="onze diensten">onze diensten<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58192 menu-item-link" id="menu-item-58192"><a href="#latest-news" title="Nieuws">Nieuws<i class="fa fa-angle-right fa-dropdown"></i></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58193 menu-item-link" id="menu-item-58193"><a href="#contact" title="contact">contact<i class="fa fa-angle-right fa-dropdown"></i></a></li>
</ul></div></div>
</div>
</div><div class="row-inner restrict">
<div class="menu-sidebar">
<div class="menu-sidebar-inner">
<div class="nav navbar-nav navbar-social"><ul class="menu-smart sm menu-social mobile-hidden tablet-hidden"><li class="menu-item-link social-icon tablet-hidden mobile-hidden social-163696"><a href="https://nl.linkedin.com/in/greysoekijad" target="_blank"><i class="fa fa-linkedin"></i></a></li><li class="menu-item-link social-icon tablet-hidden mobile-hidden social-918584"><a href="https://twitter.com/datametdimensie" target="_blank"><i class="fa fa-twitter"></i></a></li><li class="menu-item-link social-icon tablet-hidden mobile-hidden social-120990"><a href="https://www.facebook.com/dam3di/" target="_blank"><i class="fa fa-facebook"></i></a></li><li class="menu-item-link social-icon tablet-hidden mobile-hidden social-182541"><a href="https://www.instagram.com/dam3di/" target="_blank"><i class="fa fa-instagram"></i></a></li><li class="menu-item-link social-icon tablet-hidden mobile-hidden social-192804"><a href="" target="_blank"><i class="fa fa-youtube-play"></i></a></li></ul></div><div class="mobile-hidden tablet-hidden vmenu-footer style-light"><p><a href="../http://www.damedi.nl/disclaimer/ ">©2018 damedi | </a><a href="../http://www.damedi.nl/cookiebeleid/ ">3D VR BIM &amp; REVIT</a></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> <script type="text/javascript">UNCODE.fixMenuHeight();</script>
<div class="main-wrapper">
<div class="main-container">
<div class="page-wrapper">
<div class="sections-container">
<div id="page-header"><div class="header-wrapper header-uncode-block">
<div class="vc_row style-accent-bg row-container boomapps_vcrow" data-parent="true"><div class="row-background background-element">
<div class="background-wrapper">
<div class="background-inner" style="background-repeat: no-repeat;background-position: center center;background-attachment: scroll;background-size: cover;"></div>
<div class="block-bg-overlay style-accent-bg" style="opacity: 0.95;"></div>
</div>
</div><div class="row limit-width row-parent row-header" data-height-ratio="full"><div class="row-inner"><div class="pos-middle pos-center align_center column_parent col-lg-12 boomapps_vccolumn single-internal-gutter"><div class="uncol style-dark"><div class="uncoltable"><div class="uncell boomapps_vccolumn"><div class="uncont no-block-padding col-custom-width" style="max-width:504px;"><div class="heading-text el-text tablet-hidden mobile-hidden"><h1 class="font-762333 fontsize-739966 fontheight-578034 fontspace-111509 font-weight-700 text-uppercase"><span>404</span></h1></div><div class="clear"></div><div class="heading-text el-text"><h5 class="font-762333 h1 fontheight-179065 font-weight-700 text-uppercase"><span>Page Not Found</span></h5><div class="text-lead text-top-reduced"><p>The page requested couldn't be found. This could be a spelling error in the URL or a removed page.</p>
</div></div><div class="clear"></div><div class="divider-wrapper ">
<hr class="border-default-color separator-no-padding"/>
</div>
<div class="empty-space empty-single"><span class="empty-space-inner"></span></div>
<span class="btn-container btn-inline"><a class="custom-link btn btn-default btn-circle btn-icon-left" href="http://www.damedi.nl"><i class="fa fa-arrow-left3"></i>Back Homepage</a></span></div></div></div></div></div><script data-row="script-509450" id="script-509450" type="text/javascript">if ( typeof UNCODE !== "undefined" ) UNCODE.initRow(document.getElementById("script-509450"));</script></div></div></div></div></div><script type="text/javascript">UNCODE.initHeader();</script><div class="page-body style-light-bg">
<div class="post-wrapper">
<div class="post-body"></div>
</div>
</div> </div><!-- sections container -->
</div><!-- page wrapper -->
<footer class="site-footer" id="colophon">
<div class="row-container style-dark-bg footer-last desktop-hidden">
<div class="row row-parent style-dark limit-width no-top-padding no-h-padding no-bottom-padding">
<div class="site-info uncell col-lg-6 pos-middle text-left"><p><a href="../http://www.damedi.nl/disclaimer/ ">©2018 damedi | </a><a href="../http://www.damedi.nl/cookiebeleid/ ">3D VR BIM &amp; REVIT</a></p>
</div><!-- site info -->
</div>
</div> </footer>
</div><!-- main container -->
</div><!-- main wrapper -->
</div><!-- box container -->
</div><!-- box wrapper -->
<div class="style-light footer-scroll-top"><a class="scroll-top" href="#"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a></div><!--Start of Tawk.to Script (0.3.3)-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{};
var Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/599c177d1b1bed47ceb05fda/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script (0.3.3)--><div class="gdpr-overlay"></div><div class="gdpr gdpr-privacy-preferences">
<div class="gdpr-wrapper">
<form action="https://www.damedi.nl/wp-admin/admin-post.php" class="gdpr-privacy-preferences-frm" method="post">
<input name="action" type="hidden" value="uncode_privacy_update_privacy_preferences"/>
<input id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" type="hidden" value="cd58987915"/><input name="_wp_http_referer" type="hidden" value="/Confirm-My-Identity/%09%0A"/> <header>
<div class="gdpr-box-title">
<h3>Privacy Preference Center</h3>
<span class="gdpr-close"></span>
</div>
</header>
<div class="gdpr-content">
<div class="gdpr-tab-content">
<div class="gdpr-consent-management gdpr-active">
<header>
<h4>Privacy Preferences</h4>
</header>
<div class="gdpr-info">
<p></p>
</div>
</div>
</div>
</div>
<footer>
<input class="btn-accent btn-flat" type="submit" value="Save Preferences"/>
</footer>
</form>
</div>
</div>
<script src="https://www.damedi.nl/wp-includes/js/underscore.min.js?ver=1.8.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var DavesWordPressLiveSearchConfig = {"resultsDirection":"","showThumbs":"false","showExcerpt":"false","displayPostCategory":"false","showMoreResultsLink":"true","activateWidgetLink":"true","minCharsToSearch":"0","xOffset":"0","yOffset":"0","blogURL":"https:\/\/www.damedi.nl","ajaxURL":"https:\/\/www.damedi.nl\/wp-admin\/admin-ajax.php","viewMoreText":"View more results","outdatedJQuery":"Dave's WordPress Live Search requires jQuery 1.2.6 or higher. WordPress ships with current jQuery versions. But if you are seeing this message, it's likely that another plugin is including an earlier version.","resultTemplate":"<ul id=\"dwls_search_results\" class=\"search_results dwls_search_results\">\n<input type=\"hidden\" name=\"query\" value=\"<%- resultsSearchTerm %>\" \/>\n<% _.each(searchResults, function(searchResult, index, list) { %>\n        <%\n        \/\/ Thumbnails\n        if(DavesWordPressLiveSearchConfig.showThumbs == \"true\" && searchResult.attachment_thumbnail) {\n                liClass = \"post_with_thumb\";\n        }\n        else {\n                liClass = \"\";\n        }\n        %>\n        <li class=\"post-<%= searchResult.ID %> daves-wordpress-live-search_result <%- liClass %>\">\n\n        <a href=\"<%= searchResult.permalink %>\" class=\"daves-wordpress-live-search_title\">\n        <% if(DavesWordPressLiveSearchConfig.displayPostCategory == \"true\" && searchResult.post_category !== undefined) { %>\n                <span class=\"search-category\"><%= searchResult.post_category %><\/span>\n        <% } %><span class=\"search-title\"><%= searchResult.post_title %><\/span><\/a>\n\n        <% if(searchResult.post_price !== undefined) { %>\n                <p class=\"price\"><%- searchResult.post_price %><\/p>\n        <% } %>\n\n        <% if(DavesWordPressLiveSearchConfig.showExcerpt == \"true\" && searchResult.post_excerpt) { %>\n                <%= searchResult.post_excerpt %>\n        <% } %>\n\n        <% if(e.displayPostMeta) { %>\n                <p class=\"meta clearfix daves-wordpress-live-search_author\" id=\"daves-wordpress-live-search_author\">Posted by <%- searchResult.post_author_nicename %><\/p><p id=\"daves-wordpress-live-search_date\" class=\"meta clearfix daves-wordpress-live-search_date\"><%- searchResult.post_date %><\/p>\n        <% } %>\n        <div class=\"clearfix\"><\/div><\/li>\n<% }); %>\n\n<% if(searchResults[0].show_more !== undefined && searchResults[0].show_more && DavesWordPressLiveSearchConfig.showMoreResultsLink == \"true\") { %>\n        <div class=\"clearfix search_footer\"><a href=\"<%= DavesWordPressLiveSearchConfig.blogURL %>\/?s=<%-  resultsSearchTerm %>\"><%- DavesWordPressLiveSearchConfig.viewMoreText %><\/a><\/div>\n<% } %>\n\n<\/ul>"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/plugins/uncode-daves-wordpress-live-search/js/daves-wordpress-live-search.js?ver=4.9.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.damedi.nl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var uncode_irecommendthis = {"i18n":"You already recommended this","ajaxurl":"https:\/\/www.damedi.nl\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/plugins/uncode-core/i-recommend-this/js/dot_irecommendthis.js?ver=3.0.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var thickboxL10n = {"next":"Volgende >","prev":"< Vorige","image":"Afbeelding","of":"van","close":"Sluiten","noiframes":"Deze functie vereist inline-frames. Je hebt inline-frames uitgeschakeld of de browser ondersteunt ze niet.","loadingAnimation":"https:\/\/www.damedi.nl\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-includes/js/thickbox/thickbox.js?ver=3.1-20121105" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/plugins/uncode-privacy/assets/js/js-cookie.min.js?ver=2.2.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var Uncode_Privacy_Parameters = {"accent_color":"#0cb4ce"};
/* ]]> */
</script>
<script src="https://www.damedi.nl/wp-content/plugins/uncode-privacy/assets/js/uncode-privacy-public.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.9.3" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/themes/uncode/library/js/plugins.js?ver=2136342130" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-content/themes/uncode/library/js/app.js?ver=2136342130" type="text/javascript"></script>
<script src="https://www.damedi.nl/wp-includes/js/wp-embed.min.js?ver=4.9.3" type="text/javascript"></script>
</body>
</html>
