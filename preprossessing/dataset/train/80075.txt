<!DOCTYPE html>
<html ng-app="reachspaces" ng-controller="appCtrl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<base href="/"/>
<!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
        <!--[endif]-->
<title ng-bind="title"></title>
<link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" media="all" ng-if="isMobileAdaptedPage()" rel="stylesheet" type="text/css"/>
<link href="/min/f=static/css/reset.css,static/font/flaticon/flaticon.css,static/css/lib/jquery.fancybox.css,static/css/add.css,static/css/kozhuhds.css,static/css/datepicker.css,static/css/toastr.min.css,static/css/lib/colorpicker.min.css,static/css/lib/jquery-ui.css,static/css/lib/jquery.tokenize.css,static/css/lib/selectize.css,bower_components/angular-loading/angular-loading.css,static/css/ngDialog/ngDialog.min.css,static/css/ngDialog/ngDialog-theme-plain.min.css,static/css/ngDialog/ngDialog-theme-default.min.css,static/css/ngDialog/ngDialog-custom-width.css,static/css/auth.css,static/js/lib/bootstrap-daterangepicker/daterangepicker.css,static/css/style.css,static/css/lib/glyphicons.css,static/css/croppie.css" rel="stylesheet" type="text/css"/>
<link href="static/css/adscraft-nav.css" media="all" rel="stylesheet" type="text/css"/>
<link href="bower_components/angular-material/angular-material.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="bower_components/angular-bootstrap-colorpicker/css/colorpicker.min.css" media="all" rel="stylesheet" type="text/css"/>
<link ng-href="{{user.current.favicon}}" rel="shortcut icon" type="image/svg+xml"/>
<script src="/min/f=static/js/lib/jquery-1.9.1.min.js,static/js/lib/jquery.validate.js,static/js/lib/zebra_datepicker.js,static/js/lib/jquery-ui.js,static/js/lib/fbq.js,static/js/lib/jquery.autocomplete.multiselect.js,static/js/lib/jquery.tokenize.js,static/js/lib/selectize.js,static/js/lib/toastr.min.js,static/js/lib/jquery.maskedinput.js,bower_components/angular/angular.js,bower_components/angular-ui-router/release/angular-ui-router.min.js,bower_components/angular-cookies/angular-cookies.js,bower_components/angular-animate/angular-animate.js,bower_components/angular-aria/angular-aria.js,static/js/lib/underscore.js,static/js/lib/restangular.js,static/js/lib/highcharts.js,static/js/lib/exporting.js,static/js/lib/spin.js,bower_components/angular-loading/angular-loading.min.js,static/js/lib/bootstrap-colorpicker-module.min.js,static/js/lib/ngDialog.min.js,static/js/lib/jquery.payment.min.js,static/js/lib/jquery.fancybox.pack.js,static/js/lib/moment/min/moment.min.js,static/js/lib/jquery.textarea_autosize.min.js,static/js/lib/bootstrap-daterangepicker/daterangepicker.js,static/js/lib/angular-selectize.js,static/js/app/app.js,static/js/app/constants.js,static/js/app/apiSettings.js,static/js/app/config.js,static/js/app/appdirectives/directives.js,static/js/app/appdirectives/table.js,static/js/app/appdirectives/preSavedControl.js,static/js/app/appdirectives/multiplySelect.js,static/js/app/appdirectives/validation.js,static/js/app/filters.js,static/js/app/appCtrl.js,static/js/app/dirPagination.js,static/js/app/utils.js,static/js/app/appservices/services.js,static/js/app/appservices/request.js,bower_components/angular-md5/angular-md5.js,bower_components/stripe-angular/stripe-angular.js,bower_components/angular-translate/angular-translate.min.js,static/js/app/browser/browser.js,static/js/app/os/os.js,static/js/app/campaigns/campaign.js,static/js/app/campaigns/campaign.ctrls.js,static/js/app/campaigns/campaign.constants.js,static/js/app/reports/report.js,static/js/app/reports/report.ctrls.js,static/js/app/reports/report.directives.js,static/js/app/reports/report.filters.js,static/js/app/auth/auth.js,static/js/app/auth/controllers.js,static/js/app/auth/services.js,static/js/app/billing/billing.js,static/js/app/billing/billing.ctrls.js,static/js/app/billing/billing.directives.js,static/js/app/user/user.js,static/js/app/user/user.ctrls.js,static/js/app/user/user.constants.js,static/js/app/user/user.filters.js,static/js/app/geolist/geolist.js,static/js/app/geolist/geolist.ctrls.js,static/js/app/geolist/geolist.directives.js,static/js/app/geolist/geolist.filters.js,static/js/app/feed/feed.js,static/js/app/feed/feed.ctrls.js,static/js/app/settings/settings.js,static/js/app/settings/settings.ctrls.js,static/js/app/channels/channels.js,static/js/app/channels/channels.ctrls.js,static/js/app/invoices/invoices.js,static/js/app/invoices/invoices.ctrls.js,static/js/app/creatives/creative.js,static/js/app/creatives/creative.ctrls.js,static/js/app/pixels/pixels.js,static/js/app/pixels/pixels.ctrls.js,static/js/app/intercom/intercom.js,static/js/app/intercom/intercom.ctrls.js,static/js/app/intercom/intercom.directive.js,static/js/app/category/category.js,static/js/app/category/category.ctrls.js,static/js/app/category/category.directives.js,static/js/lib/croppie.js,static/js/lib/angular-croppie.js" type="text/javascript"></script>
<script src="bower_components/js-sha256/src/sha256.js" type="text/javascript"></script>
<script ng-if="isMobileAdaptedPage()" src="bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/tinymce/tinymce.js" type="text/javascript"></script>
<script src="bower_components/angular-ui-tinymce/src/tinymce.js" type="text/javascript"></script>
<script src="bower_components/angular-material/angular-material.min.js" type="text/javascript"></script>
<script src="bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="bower_components/inputmask-multi/js/jquery.inputmask-multi.js" type="text/javascript"></script>
<script src="bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js" type="text/javascript"></script>
<script src="static/js/lib/sourcebuster.min.js" type="text/javascript"></script>
<script src="https://js.stripe.com/v3/"></script>
<!-- Bug trackers -->
<!--script src="//dmc1acwvwny3.cloudfront.net/atatus.js"></script-->
<!--script type="text/javascript"></script-->
<!--        <script>
        (function(z,e,rr,s){_errs=[s];var c=z.onerror;z.onerror=function(){var a=arguments;_errs.push(a);
        c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
        c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)}; z.addEventListener?z.addEventListener("load",b,!1):z.attachEvent("onload",b)})
        (window,document,"script","573ba795f0251e9e64001719");
                </script>
                        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>-->
<!--script src="https://cdn.ravenjs.com/3.0.4/angular/raven.min.js"></script-->
<script type="text/javascript">
                    (function (e, t) {
                        var n = e.amplitude || {_q: []};
                        var r = t.createElement("script");
                        r.type = "text/javascript";
                        r.async = true;
                        r.src = "https://d24n15hnbwhuhn.cloudfront.net/libs/amplitude-2.12.1-min.gz.js";
                        r.onload = function () {
                            e.amplitude.runQueuedFunctions()
                        };
                        var s = t.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(r, s);
                        function i(e, t) {
                            e.prototype[t] = function () {
                                this._q.push([t].concat(Array.prototype.slice.call(arguments, 0)));
                                return this
                            }
                        }
                        var o = function () {
                            this._q = [];
                            return this
                        };
                        var a = ["add", "append", "clearAll", "prepend", "set", "setOnce", "unset"];
                        for (var u = 0; u < a.length; u++) {
                            i(o, a[u])
                        }
                        n.Identify = o;
                        var c = function () {
                            this._q = [];
                            return this;
                        };
                        var p = ["setProductId", "setQuantity", "setPrice", "setRevenueType", "setEventProperties"];
                        for (var l = 0; l < p.length; l++) {
                            i(c, p[l])
                        }
                        n.Revenue = c;
                        var d = ["init", "logEvent", "logRevenue", "setUserId", "setUserProperties", "setOptOut", "setVersionName", "setDomain", "setDeviceId", "setGlobalUserProperties", "identify", "clearUserProperties", "setGroup", "logRevenueV2", "regenerateDeviceId"];
                        function v(e) {
                            function t(t) {
                                e[t] = function () {
                                    e._q.push([t].concat(Array.prototype.slice.call(arguments, 0)));
                                }
                            }
                            for (var n = 0; n < d.length; n++) {
                                t(d[n])
                            }
                        }
                        v(n);
                        e.amplitude = n
                    })(window, document);
        </script>
<script async="" src="https://script.tapfiliate.com/tapfiliate.js" type="text/javascript"></script>
<script type="text/javascript">
            (function(t,a,p){t.TapfiliateObject=a;t[a]=t[a]||function(){ (t[a].q=t[a].q||[]).push(arguments)}})(window,'tap');

            tap('create', '10604-ee08e6');
            tap('detect');
        </script>
<!-- Hotjar Tracking Code for http://admachine.co -->
<script>
            (function(h,o,t,j,a,r){
                var hjid;
                switch(window.location.host) {
                    case 'reachperformance.admachine.co':
                    case 'my.reachperformance.pro':
                        hjid = 327797;
                        break;
                    case 'simple.reachnetwork.pro':
                        hjid = 333826;
                        break;
                    case 'adx-simple.loc':
                    case 'my.richpush.co':
                    case 'push.admachine.co':
                    case 'my.richpartners.co':
                        return;
                    default:
                        hjid = 333825;
                        break;
                }
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid: hjid,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
<script type="text/javascript">
    (function(){
      function Build(name, args){return function(){window.carrotquestasync.push(name, arguments);} }
      if (typeof carrotquest === 'undefined') {
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
        s.src = '//cdn.carrotquest.io/api.min.js';
        var x = document.getElementsByTagName('head')[0]; x.appendChild(s);
        window.carrotquest = {}; window.carrotquestasync = []; carrotquest.settings = {};
        var m = ['connect', 'track', 'identify', 'auth', 'open', 'onReady', 'addCallback', 'removeCallback', 'trackMessageInteraction'];
        for (var i = 0; i < m.length; i++) carrotquest[m[i]] = Build(m[i]);
      }
    })();
        </script>
</head>
<body class="bottomly-spaced">
<script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        </script>
<span class="hide" ga="" ng-if="user.current.subdomain_id"></span>
<span class="hide" ng-if="user.current.yandex_metrik_key" ya=""></span>
<span amplitude="" class="hide" ng-if="user.current.amplitude_key"></span>
<span class="hide" hotjar="" ng-if="user.current.hotjar_key"></span>
<span carrotquest="" class="hide" ng-if="user.current.carrotqest_key"></span>
<div class="loading" ng-show="loading || userNotLoaded"></div>
<div ng-if="user.current.html_domain" ng-include="user.current.html_domain+'index.html?'+CURRENT_TIME"></div>
<span class="hide" data-ng-if="ppcMateSubdomain.indexOf(user.current.subdomain_id) == -1" intercom=""></span>
<span data-ng-if="user.current.subdomain_id == 2052 &amp;&amp;
                                user.allowed([USER_TYPES.PUBLISHER, USER_TYPES.ADVERTISER])" zembed=""></span>
<!-- Yandex.Metrika counter -->
<!--script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function () {
                            try {
                                w.yaCounter36931065 = new Ya.Metrika({
                                    id: 36931065,
                                    clickmap: true,
                                    trackLinks: true,
                                    accurateTrackBounce: true,
                                    webvisor: true
                                });
                            } catch (e) {
                            }
                        });

                        var n = d.getElementsByTagName("script")[0],
                                s = d.createElement("script"),
                                f = function () {
                                    n.parentNode.insertBefore(s, n);
                                };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = "https://mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                        } else {
                            f();
                        }
                    })(document, window, "yandex_metrika_callbacks");
        </script-->
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/36931065" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<div id="bottomAd" style="font-size: 2px;"> </div>
<!-- Global site tag (gtag.js) - Google AdWords: 991099076 -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-991099076"></script>
<script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'AW-991099076');
        </script>
<!-- Event snippet for Sign Up Admachine conversion page
        In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
        function gtag_report_conversion(url) {
          var callback = function () {
            if (typeof(url) != 'undefined') {
              window.location = url;
            }
          };
          gtag('event', 'conversion', {
              'send_to': 'AW-991099076/BSQJCJif5n0QxPHL2AM',
              'event_callback': callback
          });
          return false;
        }
        </script>
<div id="bottomAd" style="font-size: 2px;"> </div>
<script>
            if (!$('#bottomAd').height()) {
                // adblocker detected, show fallback
                alert('Please turn off your ad blocker or add the website to the exception of your ad blocker. Otherwise it may cause problems with displaying the website UI.');
            } else {
                $('#bottomAd').remove();
            }
        </script>
</body>
</html>
