<html>
<head>
<meta content="animation, Ryan, Larkin, NFB, Canada, Walking, Street Musique, drugs, alcohol, McLaren" name="keyword"/>
<meta content="Chris Robinson delves into the bizarre story of Ryan Larkin, once an NFB wunderkind, now a homeless man living in Montreal." name="description"/>
<title>Last Exit on St. Laurent Street: The Wonderfully Fucked Up World of Ryan Larkin</title>
</head>
<body bgcolor="#ffffff" link="#000099" vlink="#000066">
<blockquote>
<table align="center" border="0" cellpadding="2" cellspacing="0" width="500">
<tr>
<td>
<center>
<script language="php">
        include("/opt/awn/public_html/mag/issue5.08/0011banners.txt");
        </script>
</center>
</td>
</tr>
</table>
<p></p><hr/>
<center>
<p><a href="http://www.awn.com/mag"><font size="-1">A</font><font size="-2">NIMATION 
      </font><font size="-1">W</font><font size="-2">ORLD </font><font size="-1">M</font><font size="-2">AGAZINE</font></a><font size="-2"> 
      - ISSUE 5.8 - NOVEMBER 2000</font></p>
<table border="0" cellpadding="1" cellspacing="1" height="8" width="520">
<tr valign="top">
<td>
<h1 align="center"><b>Last Exit on St. Laurent Street: The Wonderfully Fucked Up World of Ryan Larkin</b></h1>
<p align="right"><b><font size="+1">by Chris Robinson</font></b></p>
<p><i>"Being an artist doesn't take much, just everything you've got."</i><br/>
-- Hubert Selby Jr.</p>
<table align="left" border="0" cellpadding="2" cellspacing="2" width="75">
<tr>
<td><img height="250" src="../5.08images/robinson01.gif" width="180"/></td>
</tr>
<tr align="center">
<td><font size="-1">Ryan Larkin. Photo courtesy of and © National Film Board of Canada. All rights reserved.</font></td>
</tr>
</table>
<p>Okay, to avoid any potential construction of pathos, let's cut to the chase. In the 1960s, Ryan Larkin was a 19-year-old protégé of Norman McLaren. With McLaren's support, Larkin was given a rare carte blanche at the National Film Board of Canada (NFB) and made one of the most influential animation films of all time, <i>Walking</i> (1968). By 1999, Larkin was living on welfare in a mission house and panhandling for spending money. How the hell did this happen? Who knows? NFB types say one thing, Ryan says another. The reality is likely, as usual, a combination of the two stories. I'm not out to turn Larkin into a victim or a martyr. He made choices...well, actually it was his inability to choose that caused the problem. He is living with his indecision.</p>
<p>Cliché #1: "Must have come from a broken home." Not so. Larkin's family lived in a classic 1950s suburb of Montreal, Dorval. His father was an airplane mechanic and his mom worked as a secretary. Right from the beginning Larkin (the middle of two brothers) proved to be a special child. By the age of about 10, he was already doing oil paintings, and at 13 was accepted into the prestigious Montreal School of Fine Arts. The school was already very familiar to Larkin. As a child, his father used to drive him there every Saturday for classes. One of his early artistic influences was his teacher, Arthur Lismer, who was also a well-known Group of Seven painter. At the school, Larkin received an extensive education in classic forms: life drawing, sculpting, oil painting. Larkin excelled at the school and within a few years he was being considered for a job at the National Film Board of Canada.</p>
<p>Cliché #2: "He ruined his life with drugs and booze." Not quite. Yes, as we shall see, Larkin was a cokehead and drinker (but not a drunk), but there is a much deeper and more traumatic episode behind all of these escapes. Larkin was very close with his older brother. "I had a rock and roll teenage hood. I played drums, was in rock bands. My older brother was very popular in the area. He drove a convertible and always had girls around him. I looked like a greaser punk and was the typical younger brother, always hanging out with him." During the summer of 1958, Larkin, his brother and friends are playing on a boat in the lake. Something goes wrong and Larkin's brother is dead. "It was a terrible boating accident. I was unable to save him. We were very close. It hit me very hard. I was on the boat and was physically unable to save his life. It was a major block for me. I felt terrible and missed him greatly." This, more then snorts and chugs, caused his eventual spiral. </p>
<table align="right" border="0" cellpadding="2" cellspacing="2" width="75">
<tr>
<td><img height="190" src="../5.08images/robinson02.gif" width="250"/></td>
</tr>
<tr align="center">
<td><font size="-1"><i>Syrinx.</i> © National Film Board of Canada. All rights reserved.</font></td>
</tr>
</table>
<p><b>A Bright Start</b><br/>
With the help of his father, Larkin got an interview at the NFB and surprisingly, given that he had no animation experience, got a job at the age of 19. He initially worked as an animator on educational films for the army and navy including the spine tingling epics <i>Ball Resolver in Antic</i> (1964) and <i>The Canadian Forces Hydrofoil Ship: Concept and Design</i> (1967). The work was generally mindless crap requiring Larkin to follow storyboards and do tracing and painting for other animators.</p>
<p>During his 2<sup>nd</sup> or 3<sup>rd</sup> year at the NFB, Larkin became a friend of Norman McLaren and his "gang" including Guy Clover and Grant Munro. McLaren had recently begun holding an after-hours session in a small room at the Board. The relationship with McLaren opened Larkin up to a completely new world. "They were sophisticated. They had huge libraries and invited me home and showed me their libraries. It was fascinating. I was just working class. In my house we had pictures of airplanes." Larkin absorbed books, paintings and classical music. "I was young and really impressed with all this new information."</p>
<p> </p>
</td>
</tr>
<tr align="center" valign="top">
<td><b><font size="+1">
        1 | 
        <a href="robinsonlarkin2.php3">2</a> | 
        <a href="robinsonlarkin3.php3">3</a> | 
        <a href="robinsonlarkin4.php3">4</a> | 
        <a href="robinsonlarkin5.php3">5</a>
<a href="robinsonlarkin2.php3"><img align="texttop" border="0" height="15" src="../../buttons/forward.gif" width="15"/></a>
</font></b></td>
</tr>
</table>
</center>
<center>
<p>
<script language="php">
        include("/opt/awn/public_html/mag/issue5.08/0011pagefooter.txt");
        </script>
</p><p>
</p></center>
</blockquote>
</body>
</html>
