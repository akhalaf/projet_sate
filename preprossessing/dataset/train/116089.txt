<!DOCTYPE html PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Mozilla/4.79 [en] (Windows NT 5.0; U) [Netscape]" name="GENERATOR"/>
<meta content="American Electric of Terre Haute" name="Author"/>
<meta content="American Electric of Terre Haute - Residential, Commercial &amp; Industrial" name="Description"/>
<meta content="American Electric of Terre Haute, residential electrical, commercial electrical, industrial electrical service" name="KeyWords"/>
<title>American Electric of Terre Haute</title>
</head>
<body alink="#FF0000" bgcolor="#FFFFFF" link="#FF0000" text="#000099" vlink="#FF0000">
 
<center>
<p><img height="119" src="AMERICANELECTRIC.jpg" width="197"/></p></center>
<p><br/>
</p><center><table cols="2" width="75%">
<tr>
<td valign="TOP" width="20%"><b><font face="Arial,Helvetica">Home</font></b>
<br/><font face="Arial,Helvetica"><a href="profile.htm">Profile</a></font>
<br/><font face="Arial,Helvetica"><a href="services.htm">Services</a></font>
<br/><font face="Arial,Helvetica"><a href="projects.htm">Projects</a></font>
<br/><font face="Arial,Helvetica"><a href="contacts.htm">Contacts</a></font>
<br/><font face="Arial,Helvetica"><a href="links.htm">Links</a></font></td>
<td><font face="Arial,Helvetica"><font size="-1">For the past decade American
Electric of Terre Haute has built solid relationships with companies and
clients all over Terre Haute, IN and the Wabash Valley. </font></font>
<p><font face="Arial,Helvetica"><font size="-1">We are proud to be members
of the
<a href="http://www.terrehautechamber.com/">Terre Haute Chamber
of Commerce</a>, the <a href="http://www.hbaterrehaute.com/">Terre Haute
Home Builders Association</a> and the <a href="http://www.ibew.com/">International
Brotherhood of Electrical Workers IBEW</a>. </font></font>
</p><p><font face="Arial,Helvetica"><font size="-1">Whether your electrical requirements
are for <a href="residential.htm">Residential</a>, <a href="commercial.htm">Commercial</a>
or <a href="industrial.htm">Industrial</a> please call us at </font><b>812.478.3813</b><font size="-1">
and allow us the honor of earning your business today. </font></font></p></td>
</tr>
</table></center>
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
<br/> 
<center>
<p><font face="Arial,Helvetica"><font size="-2">Copyright © American
Electric of Terre Haute, Inc 2005. All Rights Reserved</font></font></p></center>
</body>
</html>
