<!DOCTYPE html>
<html xmlns:wb="http://open.weibo.com/wb">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="amigoOS，amigo，Android，amigo刷机，amigo2.0，amigo系统，金立amigo，ELIFE手机，amigo操作系统，刷机，amigo社区，amigo论坛，安卓论坛" name="keywords"/>
<meta content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<meta content="amigoOS，基于安卓深度定制，强调人与机器的关系，主动学习、自我升级，打造像朋友一样陪伴你的手机操作系统。" name="description"/>
<meta content="aaa64307f39af434" property="wb:webmaster"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>amigoOS官方网站 - amigoOS，够朋友的手机ROM</title>
<link href="https://www.amigo.cn/static/img/logox.png" rel="icon"/>
<link href="https://www.amigo.cn/static/css/base.css" rel="stylesheet" type="text/css"/>
<link href="https://www.amigo.cn/static/css/common.css" rel="stylesheet" type="text/css"/>
<script src="https://www.amigo.cn/static/js/jquery1.8.3.min.js" type="text/javascript"></script>
<script src="https://www.amigo.cn/static/js/jquery.easing.1.2.js" type="text/javascript"></script>
<script charset="utf-8" src="https://tjs.sjs.sinajs.cn/open/api/js/wb.js?appkey=2754448375" type="text/javascript"></script>
<script src="https://www.amigo.cn/static/js/NavAction.js" type="text/javascript"></script>
<script src="https://www.amigo.cn/ami_stat/ami_stat.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="https://www.amigo.cn/static/js/html5shiv.js"></script>
<style type="text/css">.main_head{width:100%;}.container{width:1000px;}.mini{background:url(https://www.amigo.cn/static/img/E6mini.jpg) top left no-repeat;}</style>
<script type="text/javascript" src="https://www.amigo.cn/static/js/selectivizr-min.js"></script>
<![endif]-->
<script type="text/javascript">
var isAnimation=true;
$(function(){
	new NavAction(".main_head .menu",true);
	$(".colordeer").animate({"left":0},400,"linear");
	$(".video-btn").on("click",function(){
	  var index=$(".video-btn").index($(this));
	  var video=$(["#my_video_1","#my_video_2"][index]);
	  $(".video-show").removeClass("none");
	  video.removeClass("none").stop().animate({width:"800px",height:"450px"},300,"swing",function(){
		  video[0].play&&video[0].play();
		 // $(".close-btn,.next-btn").fadeIn("slow");
		  $(".close-btn").fadeIn("slow");
		}).siblings("video").addClass("none");
		
	});});
</script>
<link href="https://www.amigo.cn/static/css/birthday2.css?" rel="stylesheet"/>
<!--[if lt IE 9]>
<style type="text/css">
  #my_video_1,#my_video_2{margin:0 auto;}
  .next-btn{display:none !important;}
  .bbs_btn{background:url(/static/img/ie8png.png) no-repeat -60px 0;}
  .bbs_btn:hover{background-position:0 0; border:none}
  .move_1 .screen_black{display:none}
  
</style>
<link href="https://vjs.zencdn.net/4.5/video-js.css" rel="stylesheet">
<script src="https://www.amigo.cn/static/js/video.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $(".close-btn").click(function(){
    $(".vjs-playing.vjs-play-control").trigger("click");
    $("#my_video_1,#my_video_2,.video-show").addClass("none");
  });
})
</script>
<![endif]-->
<!--[if lt IE 10]>
<style type="text/css">
	.ban_2.selected .banimg_6,.ban_2 .banimg_6{display:none!important}
</style>
<script type="text/javascript">
	 isAnimation=false;
</script>
<![endif]-->
</head>
<body>
<div class="share-img" style="display:none"><img src="https://www.amigo.cn/static/img/amigo3/share.jpg"/></div>
<!--[if lt IE 8]><div style="position:fixed;top:0;left:0;right:0;bottom:0;background:black;z-index:999999999;text-align:center;"><a href="http://godarkforie.org/upgrade?lang=en"><img src="http://godarkforie.org/splash/en.jpg" alt="You are using an old version of Internet Explorer" /></a></div><![endif]-->
<header class="main_head pr">
<div class="hd_top">
<div class="ht_con">
<div class="ht_user">
<a href="https://id.amigo.cn/members/start?client_id=AC9ABF21CD604450B27DF05D921643D2&amp;response_type=code&amp;redirect_uri=https%3A%2F%2Fwww.amigo.cn%2F&amp;state=&amp;title=&amp;r=true">登录</a>|
			<a href="https://id.amigo.cn/members/start#/register?client_id=AC9ABF21CD604450B27DF05D921643D2&amp;response_type=code&amp;redirect_uri=https%3A%2F%2Fwww.amigo.cn%2F&amp;state=&amp;title=">注册</a>|
			<a href="https://id.amigo.cn/members/reset.html?client_id=AC9ABF21CD604450B27DF05D921643D2&amp;response_type=code&amp;redirect_uri=https%3A%2F%2Fwww.amigo.cn%2F&amp;state=&amp;title=">忘记密码</a>
</div>
<a class="first ui_icons icons_gioneer" href="http://www.gionee.com" target="_blank">金立官网</a>|
		<a class="ui_icons icons_os" href="http://www.amigo.cn">amigoOS</a>|<a class="ui_icons icons_game" href="http://game.amigo.cn/" target="_blank">游戏</a>|
		<a class="ui_icons icons_soft" href="http://www.anzhuoapk.com/" target="_blank">软件</a>|
		<a class="ui_icons icons_cloud" href="http://yun.amigo.cn/" target="_blank">云服务</a>
</div>
</div>
</header>
<div id="content">
<div class="banbox_amigo">
<div class="ban_nav"><div class="container pr"><a class="logo" href="https://www.amigo.cn/"></a>
<div class="mobile-drop-btn" id="mobileDropBtn"><img alt="" src="https://www.amigo.cn/static/img/btn-mobile.png"/></div>
<div class="menu" menuid="index">
<ul>
<li class="selected current" id="menu_index"><a href="https://www.amigo.cn/"><span class="tit">首页</span></a></li>
<li id="menu_intro"><a href="https://www.amigo.cn/amigoOS3.php"><span class="tit">amigoOS 3</span></a></li>
<li id="menu_down"><a href="https://www.amigo.cn/down.php" onclick="ami_stat('name', 'down');"><span class="tit">固件下载</span></a></li>
<li id="menu_faq"><a href="https://www.amigo.cn/faq/"><span class="tit">FAQ</span></a></li>
<li><a href="http://bbs.amigo.cn/" target="_blank"><span class="tit">论坛</span></a></li>
<li id="menu_more"><a href="javascript:;" id="drop"><span class="tit">...</span><ul class="drop-menu">
<li class="weather" onclick="location.href='https://www.amigo.cn/amiweather/'">
<span class="ml30">Ami天气</span></li>
<li class="launcher" onclick="location.href='https://www.amigo.cn/amilauncher/'">
<span class="ml30">Ami桌面</span></li>
<li class="first" id="amishow" onclick="location.href='https://www.amigo.cn/amishow/'">
<span class="ml30">Ami说</span></li>
<li class="album" onclick="location.href='https://www.amigo.cn/amialbum/'">
<span class="ml30">Ami相册</span></li>
</ul></a></li>
</ul>
</div>
</div>
</div>
<div class="slide_box">
<ul>
<li class="ban_1">
<div id="ban_amigo">
<canvas height="658" id="amigo3_word">
                         canvas not supported
                        </canvas>
</div>
<span class="ban_word" id="amigo3txt"></span>
<a class="bbslink" href="https://bbs.amigo.cn" id="bbslink" target="_blank">进入论坛</a>
</li>
<li class="ban_2">
<img class="banimg_1" src="https://www.amigo.cn/static/img/amigo3/tit_amigostyle.png"/>
<img class="banimg_2" src="https://www.amigo.cn/static/img/amigo3/tit_amigostyledesc.png"/>
<img class="banimg_3" src="https://www.amigo.cn/static/img/amigo3/s7phone.png"/>
<img class="banimg_4" src="https://www.amigo.cn/static/img/amigo3/s7wallpaper_1.png"/>
<img class="banimg_5" src="https://www.amigo.cn/static/img/amigo3/s7wallpaper_2.png"/>
<img class="banimg_6" src="https://www.amigo.cn/static/img/amigo3/s7wallpaper_3.png"/>
<p class="amigo3link"><a class="amigo3btn" href="https://www.amigo.cn/amigoOS3.php" target="_blank">功能介绍</a></p>
</li>
</ul>
</div>
<div class="slide_nav">
<a href="javascript:void(0)"></a><a href="javascript:void(0)"></a>
</div>
</div>
<div class="banbox_ad">
<a href="" style="background-image:url();" target="_blank"></a>
<a href="" style="background-image:url();" target="_blank"></a>
<a class="video-btn" style="background-image:url(https://www.amigo.cn/static/img/amigo3/banner3.jpg);"></a>
<a href="" style="background-image:url();" target="_blank"></a>
</div>
</div>
<table class="centered-block video-show none">
<tr>
<td>
<video class="video-js vjs-default-skin none" controls="" data-setup="{}" height="450" id="my_video_1" preload="auto" width="800">
<source src="https://www.amigo.cn/static/swf/amigo3.0.mp4" type="video/mp4"/>
<source src="https://www.amigo.cn/static/swf/amigo3.0.flv" type="video/flv">
<source src="https://www.amigo.cn/static/swf/amigo3.0.webm" type="video/webm"/>
<embed height="450" src="https://www.amigo.cn/static/swf/amigo3.0.mp4" width="800">
</embed></source></video>
<span class="close-btn"></span>
<span class="next-btn"></span>
</td><td>
</td></tr>
</table>
<script src="https://www.amigo.cn/static/js/requestNextAnimationFrame.js"></script>
<script src="https://www.amigo.cn/static/js/sprite.js"></script>
<script src="https://www.amigo.cn/static/js/points.js"></script>
<script src="https://www.amigo.cn/static/js/slideAmigo.js"></script>
<script src="https://www.amigo.cn/static/js/index_2birthday.js?" type="text/javascript"></script>
<script type="text/javascript">
  if ( document.documentMode &&　document.documentMode < 9 ){
    document.write( '<script src="https://www.amigo.cn/static/js/video.js" type="javascript/text">' );
  }
</script>
<div class="link">
<div class="link_item">
<h2>Ami出品</h2>
<a href="https://www.amigo.cn/amiweather/">Ami天气</a>
<a href="https://www.amigo.cn/amilauncher/">Ami桌面</a>
<a href="https://www.amigo.cn/amialbum/">Ami相册</a>
</div>
<div class="link_line"></div>
<div class="link_item">
<h2>测试参与</h2>
<a href="https://bbs.amigo.cn/thread-220290-1-1.html" target="_blank">内测申请</a>
<a href="https://bbs.amigo.cn/thread-23895-1-1.html" target="_blank">公测体验</a>
<a href="https://bbs.amigo.cn/thread-2789-1-1.html" target="_blank">正式优先</a>
</div>
<div class="link_line"></div>
<div class="link_item">
<h2>论坛交流</h2>
<a href="https://bbs.amigo.cn/forum-2-1.html" target="_blank">OS讨论</a>
<a href="https://bbs.amigo.cn/forum-43-1.html" target="_blank">功能建议</a>
<a href="https://bbs.amigo.cn/forum-45-1.html" target="_blank">BUG反馈</a>
</div>
<div class="link_line"></div>
<div class="link_item">
<h2>媒体互动</h2>
<a href="https://weibo.com/amigoOS1" target="_blank">微博帐号</a>
<a class="wx_btn" href="">微信帐号</a>
<a href="https://tieba.baidu.com/f?kw=amigoos&amp;fr=ala0" target="_blank">百度贴吧</a>
</div>
</div>
<footer class="footer container pr tc">
<address class="copyright">Copyright ©2015 Gionee Inc.版权所有<br/><a href="http://gd.beian.miit.gov.cn/">粤ICP备05087105号-4</a><!-- <a href="https://www.amigo.cn/?mobile=1" class="btn_a">手机版</a> -->
<br/>
<div style="margin-top:5px;">
<!-- https://szcert.ebs.org.cn/govicon.js?id=29e3f7c5-23c7-4841-b239-f3f04fd8d8f3&width=29&height=40&type=1 -->
<script>var weburl='https://www.amigo.cn'</script>
<script charset="utf-8" id="ebsgovicon" src="https://www.amigo.cn/static/js/govicon.js?id=29e3f7c5-23c7-4841-b239-f3f04fd8d8f3&amp;width=29&amp;height=40&amp;type=1" type="text/javascript"></script>
</div>
</address>
</footer>
<div style="display: none">
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1000513071'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/z_stat.php%3Fid%3D1000513071' type='text/javascript'%3E%3C/script%3E"));</script>
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F3471775b1a8720c88161efd8f6e9560e' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>
</body>
</html>
