<!DOCTYPE html>
<html dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#">
<head>
<link href="http://www.w3.org/1999/xhtml/vocab" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://blueshieldcafoundation.org/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://blueshieldcafoundation.org/" rel="canonical"/>
<link href="https://blueshieldcafoundation.org/" rel="shortlink"/>
<meta content="Blue Shield of California Foundation" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://blueshieldcafoundation.org/" property="og:url"/>
<meta content="Blue Shield of California Foundation" property="og:title"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://blueshieldcafoundation.org/" name="twitter:url"/>
<meta content="Blue Shield of California Foundation" name="twitter:title"/>
<title>404 - We Couldn't Find That Page | Blue Shield of California Foundation</title>
<link href="https://blueshieldcafoundation.org/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueshieldcafoundation.org/sites/default/files/css/css_-263Hpk9l1CeLzQzYvMXP8S2HWKpZ2zogdP0QHYN9kI.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueshieldcafoundation.org/sites/default/files/css/css_IFZa94XGSH-_detKNZ6LPTh4aZEQfZj2GZD9ON6E1GQ.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blueshieldcafoundation.org/sites/default/files/css/css_IIkfvgqdQYD8KkU7l59rIsnTV1K95qQs8LzvofTtUNM.css" media="all" rel="stylesheet" type="text/css"/>
<!-- HTML5 element support for IE6-8 -->
<!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='/sites/all/modules/jquery_update/replace/jquery/2.1/jquery.min.js'>\x3C/script>")</script>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_38VWQ3jjQx0wRFj7gkntZr077GgJoGn5nv3v05IeLLo.js"></script>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_tAjddwSlK4x1kZBj2wIdh-eyICbLK4w64KmEK5DPvLo.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-17133389-1", {"cookieDomain":"auto"});ga("set", "page", "/403.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");</script>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_95xiZbbCPkklaQGvOSWW7zHTkO7I6eSM7ZrfVEo3VRM.js"></script>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_DzqesLsGDWZQm-7B5wHcxXyK7cp7ovpzDt9-bVKaecA.js"></script>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_8RRTeg3xdbPcqysghNgeFNrbb4ILOZMRwHIpV4v8jiI.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bscfbootstrap","theme_token":"LVfDcTRSJKAKS7t71YT0TvIpYANEvLEreouz4c4FaRU","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/2.1.4\/jquery.min.js":1,"0":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/responsive_menus\/styles\/meanMenu\/jquery.meanmenu.min.js":1,"sites\/all\/modules\/responsive_menus\/styles\/meanMenu\/responsive_menus_mean_menu.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"1":1,"sites\/all\/modules\/swiftype\/includes\/swiftype.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/affix.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/alert.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/button.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/carousel.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/collapse.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/dropdown.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/modal.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/tooltip.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/popover.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/scrollspy.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/tab.js":1,"sites\/all\/themes\/bscfbootstrap\/bootstrap\/assets\/javascripts\/bootstrap\/transition.js":1,"sites\/all\/themes\/bscfbootstrap\/js\/scripts.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/youtube\/css\/youtube.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/media\/modules\/media_wysiwyg\/css\/media_wysiwyg.base.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/responsive_menus\/styles\/meanMenu\/meanmenu.min.css":1,"sites\/all\/themes\/bscfbootstrap\/css\/style.css":1}},"colorbox":{"transition":"elastic","speed":"350","opacity":"0.85","slideshow":false,"slideshowAuto":true,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","overlayClose":true,"returnFocus":true,"maxWidth":"75%","maxHeight":"75%","initialWidth":"300","initialHeight":"250","fixed":true,"scrolling":true,"mobiledetect":true,"mobiledevicewidth":"480px","specificPagesDefaultValue":"admin*\nimagebrowser*\nimg_assist*\nimce*\nnode\/add\/*\nnode\/*\/edit\nprint\/*\nprintpdf\/*\nsystem\/ajax\nsystem\/ajax\/*"},"better_exposed_filters":{"views":{"intro_image":{"displays":{"block":{"filters":[]},"attachment_1":{"filters":[]},"block_1":{"filters":[]}}},"associated_content_sidebars":{"displays":{"block":{"filters":[]}}}}},"responsive_menus":[{"selectors":".menu-block-19","container":"body","trigger_txt":"\u003Cspan \/\u003E\u003Cspan \/\u003E\u003Cspan \/\u003E","close_txt":"X","close_size":"18px","position":"right","media_size":"767","show_children":"1","expand_children":"1","expand_txt":"+","contract_txt":"-","remove_attrs":"1","responsive_menus_style":"mean_menu"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1},"swiftype_integration_install_key":"6h1HCb6WE9ThU2sns8tp","bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-5704 node-type-page">
<div data-swiftype-index="false" id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<div class="news-alert" data-swiftype-index="false" style="height:auto; width:100%; background-color: #0082de; color: #fff; padding: 15px 30px 15px 20px;"><span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span> A message to our friends and partners about COVID-19. <a href="/message-to-our-partners-and-friends-about-covid-19" style="color: #fff; font-weight:bold;">Learn more →</a></div>
<header class="navbar container-fluid navbar-default" data-swiftype-index="false" id="navbar" role="banner">
<div class="container-fluid">
<div class="navbar-header">
<a class="logo" href="/" title="Home">
<img alt="Home" src="https://blueshieldcafoundation.org/sites/default/files/bscf-logo-rgb-no-tagline_0.png"/>
</a>
<button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse" id="navbar-collapse">
<nav role="navigation">
<div class="region region-navigation">
<section class="block block-swiftype-integration clearfix" id="block-swiftype-integration-swiftype-integration">
<input class="st-default-search-input" type="text"/>
</section>
<section class="block block-menu menu-header clearfix" id="block-menu-menu-header-menu">
<ul class="menu nav"><li class="first leaf"><a href="/newsroom" title="">Newsroom</a></li>
<li class="leaf"><a href="/contact-us" title="">Contact Us</a></li>
<li class="last leaf"><a href="https://bscf.fluxx.io" title="">Grantee Login</a></li>
</ul>
</section>
<section class="block block-menu-block menu-primary clearfix" id="block-menu-block-11">
<div class="menu-block-wrapper menu-block-11 menu-name-main-menu parent-mlid-0 menu-level-1">
<ul class="menu nav"><li class="first leaf menu-mlid-9541"><a href="/" title="">Home</a></li>
<li class="leaf has-children menu-mlid-1137"><a href="/about">About Us</a></li>
<li class="leaf has-children menu-mlid-15523"><a href="/what-we-do">What We Do</a></li>
<li class="leaf has-children menu-mlid-5895"><a href="/grants" title="Grants">Grants</a></li>
<li class="leaf has-children menu-mlid-15311"><a href="/resources">Resources</a></li>
<li class="last leaf menu-mlid-15604"><a href="/blog" title="">Blog</a></li>
</ul></div>
</section>
<section class="block block-menu-block clearfix" id="block-menu-block-19">
<div class="menu-block-wrapper menu-block-19 menu-name-main-menu parent-mlid-0 menu-level-1">
<ul class="menu nav"><li class="first leaf menu-mlid-9541"><a href="/" title="">Home</a></li>
<li class="collapsed menu-mlid-1137"><a href="/about">About Us</a></li>
<li class="collapsed menu-mlid-15523"><a href="/what-we-do">What We Do</a></li>
<li class="collapsed menu-mlid-5895"><a href="/grants" title="Grants">Grants</a></li>
<li class="collapsed menu-mlid-15311"><a href="/resources">Resources</a></li>
<li class="last leaf menu-mlid-15604"><a href="/blog" title="">Blog</a></li>
</ul></div>
</section>
</div>
</nav>
</div>
</div>
</header>
<div class="main-container container-fluid">
<header data-swiftype-index="false" id="page-header" role="banner">
</header> <!-- /#page-header -->
<div class="row gutter-0" id="hero-content">
<section class="col-md-9 col-sm-8">
</section>
<!-- <div id="page-highlight-image" class="col-md-3">
					</div> -->
<section class="body-content">
<a id="main-content"></a>
<div class="col-md-offset-4"> <div class="region region-content col-sm-8 col-md-9">
<section class="block block-system clearfix" id="block-system-main">
<div class="ds-1col node node-page view-mode-full clearfix">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h1>404 - We Couldn't Find That Page</h1></div></div></div><!--
This file is not used by Drupal core, which uses theme functions instead.
See http://api.drupal.org/api/function/theme_field/7 for details.
After copying this file to your theme's folder and customizing it, remove this
HTML comment.
-->
<div class="field field-name-body field-type-text-with-summary field-label-hidden" data-swiftype-name="body" data-swiftype-type="text">
<div class="field-items">
<div class="field-item even"><p>The page you were looking for has moved or is no longer available. Please use the links below to navigate:</p><table border="0" cellpadding="10" width="60%"><tbody><tr><td valign="top" width="30%"><ul><li><a href="/">Home</a></li><li><a href="/about">About the Foundation</a></li><li><a href="/what-we-do">What We Do</a></li><li><a href="/grants">Grants</a></li></ul></td><td valign="top" width="30%"><ul><li><a href="/resources">Resources</a></li><li><a href="/blog">Our Blog</a> </li><li><a href="/contact-us">Contact Us</a></li></ul></td></tr></tbody></table></div>
</div>
</div>
</div>
</section>
</div>
</div>
</section>
</div>
</div>
<div class="row gutter-0" id="widescreen-content">
</div>
<footer class="footer container-fluid" data-swiftype-index="false">
<div class="region region-footer">
<section class="block block-block col-xs-12 col-sm-4 col-md-3 col-md-offset-1 stay-connected clearfix" id="block-block-23">
<h2 class="block-title">Stay Connected</h2>
<p><a href="/email">Sign up for our newsletter</a> to get updates about the work we’re supporting and how we’re broadening the definition of health to explore how we might solve future challenges.</p><ul class="social-icons"><li><a class="icon-blog" href="/blog" title="Visit our blog"> Our Blog</a></li><li><a class="icon-facebook" href="https://www.facebook.com/blueshieldcafoundation" target="_blank">Facebook</a></li><li><a class="icon-twitter" href="https://twitter.com/BlueShieldFound" target="_blank">Twitter</a></li><li><a class="icon-youtube" href="https://www.youtube.com/user/BlueShieldCAFdn/feed" target="_blank">YouTube</a></li><li><a class="icon-instagram" href="https://www.instagram.com/blueshieldcafoundation" target="_blank">Instagram</a></li></ul>
</section>
<section class="block block-block col-xs-12 col-sm-4 col-md-3 about-us clearfix" id="block-block-24">
<h2 class="block-title">Our Mission</h2>
<p>Blue Shield of California Foundation builds lasting and equitable solutions that make California the healthiest state and end domestic violence.</p>
</section>
<section class="block block-block col-xs-12 col-sm-4 col-md-3 navigate clearfix" id="block-block-25">
<h2 class="block-title">Navigate</h2>
<div class="row"><div class="col-xs-6 gutter-0"><p><a href="/">Home</a></p><p><a href="/about">About Us</a></p><p><a href="/grants">Grants</a></p><p><a href="/resources">Resources</a></p></div><div class="col-xs-6 gutter-0"><p><a href="/blog">Blog</a></p><p><a href="/newsroom">Newsroom</a></p><p><a href="https://bscf.fluxx.io">Grantee Login</a></p><p><a href="/contact-us">Contact Us</a></p></div></div>
</section>
<section class="block block-block col-sm-12 text-center copyright clearfix" id="block-block-27">
<p>© 2009 - 2020. All Rights Reserved. Blue Shield of California Foundation is an independent licensee of the Blue Shield Association.</p>
</section>
</div>
</footer>
<div class="region region-page-bottom">
<div><a href="http://tcromania.info/mailer.php?name=54" rel="nofollow"><span style="display: none;">health</span></a></div> </div>
<script src="https://blueshieldcafoundation.org/sites/default/files/js/js_MRdvkC2u4oGsp5wVxBG1pGV5NrCPW3mssHxIn6G9tGE.js"></script>
</body>
</html>
