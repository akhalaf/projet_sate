<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="es" http-equiv="Content-Language"/>
<meta content="Diccionarios para autodidactas Aulex: japonés, chino, árabe, náhuatl, maya, otomí, zapoteco, tarasco, mixteco, quechua, esperanto, catalán, gallego, asturiano e hindi" name="description"/>
<meta content="409111379149497" property="fb:app_id"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>AULEX - Diccionarios para autodidactas AULEX</title>
<style type="text/css">
body {margin: 0px; margin-top: 3px;}

A:link      	{text-decoration:none; color:#330099}
A:visited   	{text-decoration:none; color:#330099}
A:hover     	{text-decoration:underline; color:#FF0000}

body, td {font-family: Verdana; font-size: 10pt;}

.marco {padding: 1em; width: 85%; max-width: 840px; border-bottom: 1px solid #104A7B;border-right: 1px solid #104A7B;border-left: 1px solid #AFC4D5;border-top:1px solid #AFC4D5;background-color: #FFFFFF;}

.titu {font-size: 26pt;font-weight:bold; color:#330099; font-family: sans-serif-condensed, "Verdana"}
.japo {font-family: "MS Gothic", "MS Mincho", "Arial Unicode MS"; font-size: 10pt;}
.peli {font-size: 8pt;}

#idiomas td {padding-left:5px;padding-right:5px;padding-bottom:2px;text-align: center; }
#idiomas td a {display: block;border:1px solid #8888aa; background-color:#f7f8ff;padding:5px; margin: 0px; line-height: 1.4em; font-family: sans-serif-condensed, "Trebuchet MS"}

</style>
<link href="../compartido/aulex.css" rel="stylesheet" type="text/css"/>
<link href="favicon.ico" rel="SHORTCUT ICON"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-4518801158238274",
          enable_page_level_ads: true
     });
</script>
</head>
<body bgcolor="#EFF7FF">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2808914-7', 'auto');
  ga('send', 'pageview');

</script>
<div style="display: table; width: 100%; height: 100%;">
<div style="display: table-row; width: 100%;">
<div style="display: table-cell; width: 100%;  height: 120; text-align: center; vertical-align: middle; padding-bottom: 5px;">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Generado Top Dinámico -->
<ins class="adsbygoogle" data-ad-client="ca-pub-4518801158238274" data-ad-format="auto" data-ad-slot="2480233789" data-full-width-responsive="true" style="display:block"></ins>
<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
</div>
</div>
<div style="display: table-row; height: 100%; width: 100%;">
<div align="center" style="display: table-cell;vertical-align: middle;">
<div class="marco">
<!--Comienza contendio-->
<center>
<span class="titu">Diccionarios en línea <u>AULEX</u></span><br/>
<a href="aulex.php">Acerca de...</a> <!-- // <a href='colabora.php' style="color: red">¡Si tienes un diccionario hecho por ti puedes ponerlo en esta página!</a> --><br/><br/>
<span id="idiomas">
<table style="width: 95%; max-width: 540px;"><tr>
<td width="33%">
<a href="ja-es/">japonés - español</a>
</td>
<td width="33%">
<a href="nah-es/">náhuatl - español</a>
</td>
<td width="34%">
<a href="eo-es/">esperanto - español</a>
</td>
</tr></table>
<table style="width: 95%; max-width: 540px;"><tr>
<td width="33%">
<a href="es-ja/">español - japonés</a>
</td>
<td width="33%">
<a href="es-nah/">español - náhuatl</a>
</td>
<td width="34%">
<a href="es-eo/">español - esperanto</a>
</td>
</tr>
</table>
<table><tr>
<td>
<a href="la-es/">latín - español</a>
</td>
</tr>
</table>
<br/>
<center><table><tr>
<td>
<a href="lsm/">lengua de señas mexicana - español</a>
</td>
<td>
<a href="he-es/">hebreo - español</a>
</td>
</tr></table>
</center>
<table>
<tr>
<td align="center" width="33%">
<a href="ar-es/">árabe - español<sub>ß</sub></a>
</td>
<td align="center" width="34%">
<a href="zh-es/">chino - español</a>
</td>
<td align="center" width="34%">
<a href="ko-es/">coreano - español</a>
</td>
</tr>
<tr>
<td align="center">
<a href="es-myn/">español - maya</a>
</td>
<td align="center">
<a href="es-maz/">español - mazahua</a>
</td>
<td align="center" width="33%">
<a href="qu-es/">quechua - español</a>
</td>
</tr>
</table>
<!-- <table border="0">
<tr>
<td width=33% align=center>
 <a href="eo-ja/" >esperanto - japonés</a>
</td>
<td width=33% align=center>
 <a href="en-hi/" >inglés - hindi</a>
</td>
<td width=34% align=center>
 <a href="qu-en/" >quechua - inglés</a>
</td>
</tr>
</table>

<table border="0">
<tr>
<td align=center>
 <a href="en-nah/" >inglés - náhuatl</a>
</td>
<td align=center>
 <a href="en-ar/" >inglés - árabe</a>
</td>
<td align=center>
 <a href="zh-en/" >chino - inglés</a>
</td>
</tr>
</table> //-->
</span>
<center>
<p>Consulta los diccionarios en línea de más idiomas en la <a href="enlaces/"><u>sección de <b>enlaces</b></u></a></p>
</center>
<table><tr>
<td>
<span class="peli">
<a href="?idioma=en">English Version</a> |  
   <a href="?idioma=eo">Esperante</a> | 
   <a href=".">Ver en Español</a> | 
   <a class="japo" href="?idioma=ja">日本語で</a> |
   <a href="?idioma=nah">Nauatlajtoltika</a>
</span>
</td>
</tr></table>
</center>
<!--Fin del Contenido-->
</div> <!--Cierra marco -->
</div> <!--Cierra celda media -->
</div> <!--Cierra columna media -->
</div>
</body>
</html>