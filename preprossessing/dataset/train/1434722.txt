<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>the entertainers</title>
<meta content="The Entertainers - because life needs more !" name="description"/>
<meta content="the entertainers, entertainers, entertainment, show business, events management, cultural activities, advertising services, multimedia, film making, videography, literary services, cultural training, website development, software development, kolkata, kolkatta, calcutta, india" name="keywords"/>
<link href="inputs/style-home.css" rel="stylesheet" type="text/css"/>
<script src="jQuery.js"></script>
<script type="text/javascript">

window.onload = function() {

    if(!window.location.hash) {
        window.location = window.location + '#loaded';
        window.location.reload();
    }


}

</script>
</head>
<body>
<table cellpadding="0" cellspacing="0" height="100%" id="basic" width="100%">
<tr>
<td align="right" height="82"><img alt="Commitment" height="82" src="inputs/header.png" width="515"/></td>
</tr>
<tr>
<td align="center" style="padding:30px" valign="middle"><table cellpadding="0" cellspacing="0">
<tr>
<td><a class="plainlink" href="about.html"><img alt="About" border="0" height="100" src="inputs/tab-about.gif" width="200"/></a></td>
<td><img height="100" src="inputs/spacer.gif" width="30"/></td>
<td><a href="range.html"><img alt="Services" border="0" height="100" src="inputs/tab-range.gif" width="200"/></a></td>
<td><img height="100" src="inputs/spacer.gif" width="30"/></td>
<td><a href="career.html"><img alt="Career" border="0" height="100" src="inputs/tab-career.gif" width="200"/></a></td>
</tr>
<tr>
<td><img height="30" src="inputs/spacer.gif" width="200"/></td>
<td><img height="30" src="inputs/spacer.gif" width="30"/></td>
<td><img height="30" src="inputs/spacer.gif" width="200"/></td>
<td><img height="30" src="inputs/spacer.gif" width="30"/></td>
<td><img height="30" src="inputs/spacer.gif" width="200"/></td>
</tr>
<tr>
<td><a href="payment.html"><img alt="Payment" border="0" height="100" src="inputs/tab-payment.gif" width="200"/></a></td>
<td><img height="100" src="inputs/spacer.gif" width="30"/></td>
<td><a href="support.html"><img alt="Support" border="0" height="100" src="inputs/tab-support.gif" width="200"/></a></td>
<td><img height="100" src="inputs/spacer.gif" width="30"/></td>
<td><a href="contact.html"><img alt="Contact" border="0" height="100" src="inputs/tab-contact.gif" width="200"/></a></td>
</tr>
<tr>
<td align="center" colspan="5"><img height="30" src="inputs/spacer.gif" width="660"/></td>
</tr>
<tr>
<td align="center" colspan="5"><img alt="Announcement" border="0" height="50" src="inputs/announce.gif" width="660"/></td>
</tr>
</table></td>
</tr>
<tr>
<td align="center" style="padding-top:0px; padding-left:30px; padding-right:30px; padding-bottom:30px" valign="middle"><table cellpadding="0" cellspacing="0" width="960">
<tr>
<td><img height="30" src="inputs/corner-1.gif" width="30"/></td>
<td align="center" background="inputs/corner-2.gif"><img height="30" src="inputs/spacer.gif" width="900"/></td>
<td><img height="30" src="inputs/corner-3.gif" width="30"/></td>
</tr>
<tr>
<td background="inputs/corner-4.gif"><img height="200" src="inputs/spacer.gif" width="30"/></td>
<td bgcolor="#ffffff" style="padding-right:20px; padding-bottom:20px" valign="top"><table cellpadding="0" cellspacing="0" id="announce">
<tr>
<td valign="top"><img src="inputs/bullet-red.gif"/></td>
<td class="announce" style="padding-top:13px; padding-left:8px" valign="top">How about pampering your old hobby or hidden desire to act in a drama within your own circle ?<br/>
              As an interesting part of our Events Management Division, we also undertake training up people from corporate houses, organizations and clubs for staging a drama (in English, Hindi or Bengali) at an annual event or some other occasion. Optional videography in cinematic style editing also available.<br/>
              It's a real fun ! <img height="20" src="inputs/emot_01.gif" width="19"/><br/>
<a class="bluelink" href="contact.html">ContactÂ us</a></td>
</tr>
<tr>
<td valign="top"><img src="inputs/bullet-red.gif"/></td>
<td class="announce" style="padding-top:13px; padding-left:8px" valign="top">Great news for true music lovers who want to learn (basic/ advanced) <b>!</b><br/>
              Very senior, high profile professional teaches the following subjects near Doordarshan Bhawan (TV Centre) in SouthÂ Kolkata (about 1<b>.</b>25 Kms from South City) or at selected pupils' places <b>:</b><br/>
              •  Ethnic/ old songs (vocal)<br/>
              •  Synthesizer<br/>
              •  Piano<br/>
<a class="bluelink" href="contact.html">ContactÂ us</a></td>
</tr>
</table></td>
<td background="inputs/corner-6.gif"><img height="200" src="inputs/spacer.gif" width="30"/></td>
</tr>
<tr>
<td><img height="30" src="inputs/corner-7.gif" width="30"/></td>
<td align="center" background="inputs/corner-8.gif"><img height="30" src="inputs/spacer.gif" width="900"/></td>
<td><img height="30" src="inputs/corner-9.gif" width="30"/></td>
</tr>
</table>
<p><img alt="Slogan" height="36" src="inputs/slogan.png" width="530"/></p></td>
</tr>
<tr>
<td background="inputs/bgbottom.gif" bgcolor="#666666" height="210" style="padding-top:40px; padding-left:30px; padding-right:30px; padding-bottom:0px" valign="top"><table cellpadding="0" cellspacing="0" id="footerbasic" width="100%">
<tr>
<td valign="top" width="86"><img alt="Quality" height="120" src="inputs/logo.gif" width="86"/></td>
<td valign="top" width="100"><img height="100" src="inputs/spacer.gif" width="100"/></td>
<td align="center" class="smalltext" nowrap="" valign="top" width="90"><p>A venture of</p>
<p><img alt="ARC Group" border="0" height="80" src="inputs/logo-arc.png" width="80"/></p>
<p>(Since 1982)</p></td>
<td valign="top" width="100"><img height="100" src="inputs/spacer.gif" width="100"/></td>
<td valign="top" width="120"><img alt="Quality" height="150" src="inputs/quality.png" width="176"/></td>
<td valign="top"> </td>
<td valign="top" width="220"><a href="index.html"> Home </a><br/>
<a href="about.html"> About us </a><br/>
<a href="range.html"> Range of services </a><br/>
<a href="career.html"> Career with us </a><br/>
<a href="payment.html"> Payment options </a><br/>
<a href="support.html"> Online support </a><br/>
<a href="contact.html"> Contact us </a></td>
<td valign="top"> </td>
<td align="right" valign="top" width="160"><table cellpadding="0" cellspacing="0" id="socialnets">
<tr>
<td><a href="netfacebook.html" target="_blank"><img alt="Facebook" border="0" height="32" src="inputs/logo_facebk.gif" width="32"/></a></td>
<td><img height="32" src="inputs/spacer.gif" width="32"/></td>
<td><a href="netlinkedin.html" target="_blank"><img alt="Linkedin" border="0" height="32" src="inputs/logo_linkdn.gif" width="32"/></a></td>
<td><img height="32" src="inputs/spacer.gif" width="32"/></td>
<td><a href="nettwitter.html" target="_blank"><img alt="Twitter" border="0" height="32" src="inputs/logo_twittr.gif" width="32"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
