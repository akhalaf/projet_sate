<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="es"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="es"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="es"> <![endif]--><!--[if IE 9 ]><html class="ie ie9" lang="es"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html lang="es"> <!--<![endif]-->
<head>
<title>Eyger Cargo | Página web de la empresa Eyger Cargo, S.A.</title>
<meta content=" | Página web de la empresa Eyger Cargo, S.A." name="description"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.eyger.es/wp-content/themes/theme52382/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://www.eyger.es/feed/" rel="alternate" title="Eyger Cargo" type="application/rss+xml"/>
<link href="https://www.eyger.es/feed/atom/" rel="alternate" title="Eyger Cargo" type="application/atom+xml"/>
<link href="https://www.eyger.es/wp-content/themes/theme52382/bootstrap/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/themes/theme52382/bootstrap/css/responsive.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/themes/CherryFramework/css/camera.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/themes/theme52382/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.eyger.es/feed/" rel="alternate" title="Eyger Cargo » Feed" type="application/rss+xml"/>
<link href="https://www.eyger.es/comments/feed/" rel="alternate" title="Eyger Cargo » Feed de los comentarios" type="application/rss+xml"/>
<link href="https://www.eyger.es/home/feed/" rel="alternate" title="Eyger Cargo » Comentario Home del feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.eyger.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=58e0a0cf1555f785214978af41b143cf"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/flexslider.css?ver=2.2.0" id="flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.carousel.css?ver=1.24" id="owl-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.theme.css?ver=1.24" id="owl-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=3.2.1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/cherry-plugin/includes/css/cherry-plugin.css?ver=1.2.8.2" id="cherry-plugin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-includes/css/dist/block-library/style.min.css?ver=58e0a0cf1555f785214978af41b143cf" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/cherry-lazy-load/css/lazy-load.css?ver=1.0" id="cherry-lazy-load-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/cherry-parallax/css/parallax.css?ver=1.0" id="cherry-parallax-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/themes/theme52382/main-style.css" id="theme52382-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/themes/CherryFramework/css/magnific-popup.css?ver=0.9.3" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin" id="options_typography_Open+Sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/motopress-content-editor/includes/css/theme.css?ver=1.5.8" id="mpce-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.eyger.es/wp-content/plugins/motopress-content-editor/bootstrap/bootstrap-grid.min.css?ver=1.5.8" id="mpce-bootstrap-grid-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery-1.7.2.min.js?ver=1.7.2" type="text/javascript"></script>
<script id="easing-js" src="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/jquery.easing.1.3.js?ver=1.3" type="text/javascript"></script>
<script id="elastislide-js" src="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/elasti-carousel/jquery.elastislide.js?ver=1.2.8.2" type="text/javascript"></script>
<script id="googlemapapis-js" src="//maps.googleapis.com/maps/api/js?v=3&amp;signed_in=false&amp;key&amp;ver=58e0a0cf1555f785214978af41b143cf" type="text/javascript"></script>
<script id="parallaxSlider-js" src="https://www.eyger.es/wp-content/themes/theme52382/js/parallaxSlider.js?ver=1.0" type="text/javascript"></script>
<script id="migrate-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery-migrate-1.2.1.min.js?ver=1.2.1" type="text/javascript"></script>
<script id="swfobject-js" src="https://www.eyger.es/wp-includes/js/swfobject.js?ver=2.2-20120417" type="text/javascript"></script>
<script id="modernizr-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/modernizr.js?ver=2.0.6" type="text/javascript"></script>
<script id="jflickrfeed-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jflickrfeed.js?ver=1.0" type="text/javascript"></script>
<script id="custom-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/custom.js?ver=1.0" type="text/javascript"></script>
<script id="bootstrap-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/bootstrap/js/bootstrap.min.js?ver=2.3.0" type="text/javascript"></script>
<link href="https://www.eyger.es/wp-json/" rel="https://api.w.org/"/><link href="https://www.eyger.es/wp-json/wp/v2/pages/203" rel="alternate" type="application/json"/><link href="https://www.eyger.es/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.eyger.es/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://www.eyger.es/" rel="canonical"/>
<link href="https://www.eyger.es/" rel="shortlink"/>
<link href="https://www.eyger.es/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.eyger.es%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.eyger.es/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.eyger.es%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script>
 var system_folder = 'https://www.eyger.es/wp-content/themes/CherryFramework/admin/data_management/',
	 CHILD_URL ='https://www.eyger.es/wp-content/themes/theme52382',
	 PARENT_URL = 'https://www.eyger.es/wp-content/themes/CherryFramework', 
	 CURRENT_THEME = 'theme52382'</script>
<style type="text/css">
body { background-color:#292c31 }

</style>
<style type="text/css">
h1 { font: normal 60px/60px Open Sans;  color:#FFFFFF; }
h2 { font: normal 40px/40px Open Sans;  color:#FFFFFF; }
h3 { font: normal 20px/24px Open Sans;  color:#15c1c6; }
h4 { font: normal 15px/22px Open Sans;  color:#FFFFFF; }
h5 { font: normal 50px/50px Open Sans;  color:#15c1c6; }
h6 { font: normal 30px/30px Open Sans;  color:#FFFFFF; }
body { font-weight: normal;}
.logo_h__txt, .logo_link { font: normal 60px/60px Open Sans;  color:#FFFFFF; }
.sf-menu > li > a { font: normal 14px/18px Open Sans;  color:#ffffff; }
.nav.footer-nav a { font: normal 12px/18px Arial, Helvetica, sans-serif;  color:#bbbaba; }
</style>
<script src="https://www.eyger.es/wp-content/plugins/wp-spamshield/js/jscripts.php" type="text/javascript"></script>
<!--[if lt IE 9]>
		<div id="ie7-alert" style="width: 100%; text-align:center;">
			<img src="http://tmbhtest.com/images/ie7.jpg" alt="Upgrade IE 8" width="640" height="344" border="0" usemap="#Map" />
			<map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" /><area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />
			</map>
		</div>
	<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.mobile.customized.min.js" type="text/javascript"></script>
<script type="text/javascript">
			jQuery(function(){
				jQuery('.sf-menu').mobileMenu({defaultText: "Navegación"});
			});
		</script>
<!--<![endif]-->
<script type="text/javascript">
		// Init navigation menu
		jQuery(function(){
		// main navigation init
			jQuery('ul.sf-menu').superfish({
				delay: 1000, // the delay in milliseconds that the mouse can remain outside a sub-menu without it closing
				animation: {
					opacity: "show",
					height: "show"
				}, // used to animate the sub-menu open
				speed: "normal", // animation speed
				autoArrows: false, // generation of arrow mark-up (for submenu)
				disableHI: true // to disable hoverIntent detection
			});

		//Zoom fix
		//IPad/IPhone
			var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
				ua = navigator.userAgent,
				gestureStart = function () {
					viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
				},
				scaleFix = function () {
					if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
						viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
						document.addEventListener("gesturestart", gestureStart, false);
					}
				};
			scaleFix();
		})
	</script>
<!-- stick up menu -->
<script type="text/javascript">
		jQuery(document).ready(function(){
			if(!device.mobile() && !device.tablet()){
				jQuery('.nav__primary').tmStickUp({
					correctionSelector: jQuery('#wpadminbar')
				,	listenSelector: jQuery('.listenSelector')
				,	active: true				,	pseudo: true				});
			}
		})
	</script>
</head>
<body class="home page-template page-template-page-home page-template-page-home-php page page-id-203">
<div class="main-holder" id="motopress-main">
<!--Begin #motopress-main-->
<header class="motopress-wrapper header">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5fffd7a7611e9" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
<div class="extra_header">
<div class="row">
<div class="span6" data-motopress-static-file="static/static-logo.php" data-motopress-type="static">
<!-- BEGIN LOGO -->
<div class="logo pull-left">
<a class="logo_h logo_h__img" href="https://www.eyger.es/"><img alt="Eyger Cargo" src="https://www.eyger.es/wp-content/uploads/2015/10/logo.png" title="Página web de la empresa Eyger Cargo, S.A."/></a>
</div>
<!-- END LOGO --> </div>
<div class="span6" data-motopress-static-file="static/static-search.php" data-motopress-type="static">
<!-- BEGIN SEARCH FORM -->
<!-- END SEARCH FORM --> </div>
</div>
<div class="row">
<div class="span12" data-motopress-static-file="static/static-nav.php" data-motopress-type="static">
<!-- BEGIN MAIN NAVIGATION -->
<nav class="nav nav__primary clearfix">
<ul class="sf-menu" id="topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-203 current_page_item" id="menu-item-1914"><a href="https://www.eyger.es/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-1917"><a href="https://www.eyger.es/about-us-2/">Quiénes somos</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2170"><a href="#">Productos y Servicios</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2169"><a href="https://www.eyger.es/services/representante-aduanero/">Representante aduanero</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2171"><a href="#">Transporte Internacional</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2168"><a href="https://www.eyger.es/transporte-internacional-aereo/">Aéreo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2177"><a href="https://www.eyger.es/transporte-internacional-maritimo/">Marítimo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2178"><a href="https://www.eyger.es/transporte-internacional-terrestre/">Terrestre</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2185"><a href="https://www.eyger.es/almacenaje/">Almacenaje</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2184"><a href="https://www.eyger.es/seguros/">Seguros</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-1911"><a href="https://www.eyger.es/contacts/">Contacto</a></li>
</ul></nav><!-- END MAIN NAVIGATION --> </div>
</div>
</div>
</div>
</div>
</div>
</header>
<div class="motopress-wrapper content-holder clearfix">
<script type="text/javascript">
		jQuery(function() {
			var isparallax = true;
			if(!device.mobile() && !device.tablet()){
				isparallax = true;
			}else{
				isparallax = false;
			}

				jQuery('#parallax-slider-5fffd7a762539').parallaxSlider({
					animateLayout: 'simple-fade-eff'
				,	duration: 1000
				,	parallaxEffect: isparallax
				});
			
		});
		jQuery(window).load(function(){
			if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
				window.onmousewheel = document.onmousewheel = wheel;

				var time = 330;
				var distance = 100;

				function wheel(event) {
					if (event.wheelDelta) delta = event.wheelDelta / 90;
					else if (event.detail) delta = -event.detail / 3;
					handle();
					if (event.preventDefault) event.preventDefault();
						event.returnValue = false;
				}

				function handle() {
					jQuery('html, body').stop(stop).animate({
						scrollTop: jQuery(window).scrollTop() - (distance * delta)
					}, time);
				}
		})
</script>
<div class="parallax-slider" id="parallax-slider-5fffd7a762539"><ul class="baseList"><li data-img-height="922" data-img-width="1950" data-preview="https://www.eyger.es/wp-content/uploads/2015/10/slide-1.jpg"><div class="slider_caption&gt;"><h2>Expertos en
comercio<br/> internacional</h2></div></li><li data-img-height="922" data-img-width="1950" data-preview="https://www.eyger.es/wp-content/uploads/2015/10/slide-2.jpg"><div class="slider_caption&gt;"><h2>Tramitación aduanera<br/>con garantía.</h2></div></li><li data-img-height="922" data-img-width="1950" data-preview="https://www.eyger.es/wp-content/uploads/2015/10/slide-3.jpg"><div class="slider_caption&gt;"><h2><a href="https://www.eyger.es/interdum-vitaedapibus-ac/donec-tempor-libero/">Más de
35 años
de experiencia</a></h2></div></li></ul></div>
<div class="container">
<div class="row">
<div class="span12" data-motopress-wrapper-file="page-home.php" data-motopress-wrapper-type="content">
<div class="row">
<div class="span12" data-motopress-loop-file="loop/loop-page.php" data-motopress-type="loop">
<div class="page post-203 type-page status-publish has-post-thumbnail hentry" id="post-203">
<section class="lazy-load-box trigger effect-slidefromright " data-delay="80" data-speed="550" style="-webkit-transition: all 550ms ease; -moz-transition: all 550ms ease; -ms-transition: all 550ms ease; -o-transition: all 550ms ease; transition: all 550ms ease;">
<div class="hero-unit "><p>Estamos a su servicio.</p>
<p><em>Contacte con nosotros</em></p><div class="btn-align"><a class="btn btn-primary btn-normal btn-primary" href="https://www.eyger.es/contacts/" target="_self" title="Contacto">Contacto</a></div></div><!-- .hero-unit (end) -->
</section>
<div class="row-fluid ">
<div class="span3 "><section class="lazy-load-box trigger effect-slidefromleft " data-delay="580" data-speed="400" style="-webkit-transition: all 400ms ease; -moz-transition: all 400ms ease; -ms-transition: all 400ms ease; -o-transition: all 400ms ease; transition: all 400ms ease;">
<div class="service-box "><div class="service-box_title"><div class="extrabox"><figure class="icon"><i class="icon-time"></i><span>1</span></figure></div><h2 class="title"><a href="https://www.eyger.es/services/representante-aduanero/" target="_self" title="REPRESENTANTE&lt;br /&gt;
&lt;em&gt;ADUANERO&lt;/em&gt;">REPRESENTANTE<br/>
<em>ADUANERO</em></a></h2></div><div class="service-box_body"><div class="service-box_txt">Optimizamos los servicios aduaneros con soluciones óptimas para cada caso.</div><div class="btn-align"><a class="btn btn-inverse btn-normal btn-primary " href="https://www.eyger.es/services/representante-aduanero/" target="_self" title="Más info">Más info</a></div></div></div><!-- /Service Box -->
</section></div>
<div class="span3 "><section class="lazy-load-box trigger effect-slidefromleft " data-delay="480" data-speed="400" style="-webkit-transition: all 400ms ease; -moz-transition: all 400ms ease; -ms-transition: all 400ms ease; -o-transition: all 400ms ease; transition: all 400ms ease;">
<div class="service-box "><div class="service-box_title"><div class="extrabox"><figure class="icon"><i class="icon-thumbs-up-alt"></i><span>2</span></figure></div><h2 class="title"><a href="https://www.eyger.es/transporte-internacional-terrestre/" target="_self" title="TRANSPORTE&lt;br /&gt;
&lt;em&gt;internacional&lt;/em&gt;">TRANSPORTE<br/>
<em>internacional</em></a></h2></div><div class="service-box_body"><div class="service-box_txt">Ofrecemos soluciones flexibles para llevar tus mercancías a cualquier parte.</div><div class="btn-align"><a class="btn btn-inverse btn-normal btn-primary " href="https://www.eyger.es/transporte-internacional-terrestre/" target="_self" title="Más info">Más info</a></div></div></div><!-- /Service Box -->
</section></div>
<div class="span3 "><section class="lazy-load-box trigger effect-slidefromleft " data-delay="380" data-speed="400" style="-webkit-transition: all 400ms ease; -moz-transition: all 400ms ease; -ms-transition: all 400ms ease; -o-transition: all 400ms ease; transition: all 400ms ease;">
<div class="service-box "><div class="service-box_title"><div class="extrabox"><figure class="icon"><i class="icon-lightbulb"></i><span>3</span></figure></div><h2 class="title"><a href="https://www.eyger.es/almacenaje/" target="_self" title="ALMACENAJE&lt;br /&gt;
">ALMACENAJE<br/>
</a></h2></div><div class="service-box_body"><div class="service-box_txt">Disponemos de centro de almacenaje y distribución, con las máximas garantías de control y seguridad.</div><div class="btn-align"><a class="btn btn-inverse btn-normal btn-primary " href="https://www.eyger.es/almacenaje/" target="_self" title="Más info">Más info</a></div></div></div><!-- /Service Box -->
</section></div>
<div class="span3 "><section class="lazy-load-box trigger effect-slidefromleft " data-delay="280" data-speed="400" style="-webkit-transition: all 400ms ease; -moz-transition: all 400ms ease; -ms-transition: all 400ms ease; -o-transition: all 400ms ease; transition: all 400ms ease;">
<div class="service-box "><div class="service-box_title"><div class="extrabox"><figure class="icon"><i class="icon-search"></i><span>4</span></figure></div><h2 class="title"><a href="https://www.eyger.es/seguros/" target="_self" title="SEGUROS&lt;br /&gt;
">SEGUROS<br/>
</a></h2></div><div class="service-box_body"><div class="service-box_txt">Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit. bibendum et condiment metusip dolore</div><div class="btn-align"><a class="btn btn-inverse btn-normal btn-primary " href="https://www.eyger.es/seguros/" target="_self" title="Más info">Más info</a></div></div></div><!-- /Service Box -->
</section></div>
</div> <!-- .row-fluid (end) -->
<div class="clear"></div>
<!--.pagination-->
</div><!--post-->
</div>
</div>
</div>
</div>
</div>
</div>
<footer class="motopress-wrapper footer">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5fffd7a7639c4" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
<div class="footer-widgets">
<div class="row">
<div class="span3">
<div data-motopress-sidebar-id="footer-sidebar-1" data-motopress-type="dynamic-sidebar">
</div>
<div class="social-nets-wrapper" data-motopress-static-file="static/static-social-networks.php" data-motopress-type="static">
</div>
</div>
<div class="span3">
<div data-motopress-sidebar-id="footer-sidebar-2" data-motopress-type="dynamic-sidebar">
</div>
</div>
<div class="span3">
<div data-motopress-sidebar-id="footer-sidebar-3" data-motopress-type="dynamic-sidebar">
</div>
</div>
<div class="span3">
<div data-motopress-sidebar-id="footer-sidebar-4" data-motopress-type="dynamic-sidebar">
</div>
</div>
</div>
</div>
<div class="row">
<div class="span12" data-motopress-static-file="static/static-footer-nav.php" data-motopress-type="static">
</div>
<div class="span12" data-motopress-static-file="static/static-footer-text.php" data-motopress-type="static">
<div class="footer-text" id="footer-text">
		
			 	Eyger Cargo, S.A.  C/ Eduardo Torroja nº22, Naves 9 y 10, Pol. Ind. de Coslada, 28823 - Coslada - Madrid /// Tlf: +34 91 669 24 75  | 
<a href="https://webartesanal.com/mantenimiento-web/" rel="nofollow" target="_blank">Mantenimiento Web Artesanal</a> <!--<a rel="nofollow" href="http://www.templatemonster.com/wordpress-themes.php" target="_blank">TemplateMonster</a> Design. -->
</div> </div>
</div> </div>
</div>
</div>
</footer>
<!--End #motopress-main-->
</div>
<div class="visible-desktop" id="back-top-wrapper">
<p id="back-top">
<a href="#top"><span></span></a> </p>
</div>
<script type="text/javascript">
/* <![CDATA[ */
r3f5x9JS=escape(document['referrer']);
hf4N='1be3e515ed6eed468080f410f11dc871';
hf4V='89d70417e1f95cd62065177740f59830';
cm4S="form[action='https://www.eyger.es/wp-comments-post.php']";
jQuery(document).ready(function($){var e="#commentform, .comment-respond form, .comment-form, "+cm4S+", #lostpasswordform, #registerform, #loginform, #login_form, #wpss_contact_form, .wpcf7-form";$(e).submit(function(){$("<input>").attr("type","hidden").attr("name","r3f5x9JS").attr("value",r3f5x9JS).appendTo(e);$("<input>").attr("type","hidden").attr("name",hf4N).attr("value",hf4V).appendTo(e);return true;});$("#comment").attr({minlength:"15",maxlength:"15360"})});
/* ]]> */
</script>
<script id="comment-reply-js" src="https://www.eyger.es/wp-includes/js/comment-reply.min.js?ver=58e0a0cf1555f785214978af41b143cf" type="text/javascript"></script>
<script id="flexslider-js" src="https://www.eyger.es/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/jquery.flexslider-min.js?ver=2.2.2" type="text/javascript"></script>
<script id="cherry-plugin-js-extra" type="text/javascript">
/* <![CDATA[ */
var items_custom = [[0,1],[480,2],[768,3],[980,4],[1170,5]];
/* ]]> */
</script>
<script id="cherry-plugin-js" src="https://www.eyger.es/wp-content/plugins/cherry-plugin/includes/js/cherry-plugin.js?ver=1.2.8.2" type="text/javascript"></script>
<script id="cherry-lazy-load-js" src="https://www.eyger.es/wp-content/plugins/cherry-lazy-load/js/cherry.lazy-load.js?ver=1.0" type="text/javascript"></script>
<script id="device-check-js" src="https://www.eyger.es/wp-content/plugins/cherry-lazy-load/js/device.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="cherry-parallax-js" src="https://www.eyger.es/wp-content/plugins/cherry-parallax/js/cherry.parallax.js?ver=1.0" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.eyger.es\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.eyger.es/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="superfish-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/superfish.js?ver=1.5.3" type="text/javascript"></script>
<script id="mobilemenu-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.mobilemenu.js?ver=1.0" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.magnific-popup.min.js?ver=0.9.3" type="text/javascript"></script>
<script id="playlist-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jplayer.playlist.min.js?ver=2.3.0" type="text/javascript"></script>
<script id="jplayer-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.jplayer.min.js?ver=2.6.0" type="text/javascript"></script>
<script id="tmstickup-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/tmstickup.js?ver=1.0.0" type="text/javascript"></script>
<script id="device-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/device.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="zaccordion-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.zaccordion.min.js?ver=2.1.0" type="text/javascript"></script>
<script id="camera-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/camera.min.js?ver=1.3.4" type="text/javascript"></script>
<script id="debouncedresize-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.debouncedresize.js?ver=1.0" type="text/javascript"></script>
<script id="ba-resize-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.ba-resize.min.js?ver=1.1" type="text/javascript"></script>
<script id="isotope-js" src="https://www.eyger.es/wp-content/themes/CherryFramework/js/jquery.isotope.js?ver=1.5.25" type="text/javascript"></script>
<script id="wpss-jscripts-ftr-js" src="https://www.eyger.es/wp-content/plugins/wp-spamshield/js/jscripts-ftr-min.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.eyger.es/wp-includes/js/wp-embed.min.js?ver=58e0a0cf1555f785214978af41b143cf" type="text/javascript"></script>
<div class="cf-cookie-banner-wrap alert fade in" id="cf-cookie-banner">
<div class="container">
<button class="close" data-dismiss="alert" type="button">×</button>
				We use Cookies - By using this site or closing this you agree to our Cookies policy.			</div>
</div>
<!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>