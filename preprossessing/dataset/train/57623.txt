<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<title>ABeam Consulting</title>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="xteNadRR4z-o_O0PTZT72eW0jC8dnLJfc8XXaHijL2U" name="google-site-verification"/>
<link href="./shared/reset.css" rel="stylesheet"/>
<link href="./shared/drawer.min.css" rel="stylesheet"/>
<link href="./shared/common.css" rel="stylesheet"/>
<link href="./shared/wSelect.css" rel="stylesheet"/>
<style>
	i img {
		margin-right: 10px;
	}
	.maininner .l-grid {
		margin-top: 20px;
	}
	@media screen and (max-width: 768px){
		.logo {
			padding: 0 15px;
		}
	}
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P9K6KZF');</script>
<!-- End Google Tag Manager -->
</head>
<body id="component">
<main>
<div class="maininner logo">
<section>
<div>
<img alt="ABeam Consulting" height="80" src="./shared/logo.png" width="200"/>
</div>
</section>
</div>
<div id="content">
<article>
<div class="maininner">
<section>
<h2 class="ttl02">Please select your region.</h2>
<div class="l-grid c-grid-col-2 match-box">
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/jp/ja"><i><img src="./shared/ico-lang-jp.png"/></i>Japan</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/cn/en"><i><img src="./shared/ico-lang-cn.png"/></i>China</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/kr/en"><i><img src="./shared/ico-lang-kr.png"/></i>Korea</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/th/en"><i><img src="./shared/ico-lang-th.png"/></i>Thailand</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/sg/en"><i><img src="./shared/ico-lang-sg.png"/></i>Singapore</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/my/en"><i><img src="./shared/ico-lang-my.png"/></i>Malaysia</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/id/en"><i><img src="./shared/ico-lang-id.png"/></i>Indonesia</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/am/en"><i><img src="./shared/ico-lang-us.png"/><img src="./shared/ico-lang-br.png"/></i>Americas</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/eu/en"><i><img src="./shared/ico-lang-gb.png"/><img src="./shared/ico-lang-de.png"/></i>Europe</a></p>
</div>
<div>
<p class="u-fs-m"><a href="https://www.abeam.com/vn/en"><i><img src="./shared/ico-lang-vn.png"/></i>Vietnam</a></p>
</div>
</div>
</section>
<script src="./core/assets/vendor/jquery/jquery.min.js?v=2.2.4"></script>
<script src="./cookieraw/js/cookie-raw.js"></script>
</div></article></div></main></body>
</html>
