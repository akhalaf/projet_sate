<!DOCTYPE html>
<html>
<head>
<title>Welcome to Auroville | Auroville</title>
<meta content="Auroville is a universal city in the making in south-India dedicated to the ideal of human unity based on the vision of Sri Aurobindo and The Mother." name="description"/>
<meta content="Sri Aurobindo, The Mother, Integral Yoga, City of Dawn, Spiritual Transformation" name="keywords"/>
<link href="/assets/favicon-179894fc1ffd2c71b0eb386b989af8d9.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="https://auroville.org/system/slideshow_images/images/000/000/004/slideshow/HomePageSlide1-490.jpg?1592292921" property="og:image"/>
<link href="/assets/application-bb031e715c25bacd628782c7caa8d37a.css" media="all" rel="stylesheet"/>
<link href="/assets/print-ced2df685ea666864a3cdfcd3ad623bb.css" media="print" rel="stylesheet"/>
<script src="/assets/modernizr-c8beaa31a4e5f73ca79311373470aa83.js"></script>
<script src="/assets/application-66687dc2645f2069c0cc38ebf05d1c02.js"></script>
<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53a1863824bdfa3d" type="text/javascript"></script>
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-526fb84863316edd"></script> -->
<!-- AddThis Smart Layers END -->
<meta content="authenticity_token" name="csrf-param"/>
<meta content="j8QRpREIgJbWY8qbx+bIcXIR2KEwKNVz7V+ttz/vMcA=" name="csrf-token"/>
<link href="/assets/favicon-179894fc1ffd2c71b0eb386b989af8d9.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="2ut-8d3v9owyfg3t5zalxhkm2vwrbn1khgr2ojbohd17y2kqvnf303scu5xmhev5jse9lwda0sn0uz5wo3er46gd5obcxs90j3-dffp4tjb7xneq1rosbdzoiaxv-9ni" name="norton-safeweb-site-verification"/>
<script>
          (function(h,e,a,t,m,p) {
          m=e.createElement(a);m.async=!0;m.src=t;
          p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
          })(window,document,'script','https://u.heatmap.it/log.js');
        </script>
</head>
<body class="homes homes-index anon">
<div id="wrap">
<header class="top-nav">
<div class="menu-wrapper">
<a class="logo" href="/">
<img alt="Logo" src="/assets/logo-04a587026b3708641904f08a5e2fa77a.png"/>
</a>
<a class="logo small" href="/">
<img alt="Logo small" src="/assets/logo-small-980cb31f9322db7f4d18a992c5f1cc65.png"/>
</a>
<div class="top-nav-tools">
<div id="auto-save-indicator"><div class="spinner"></div><span id="auto-saving">Saving...</span></div>
<div class="search-bar small">
<div class="search-and-submit">
<form action="/search" class="search-form" method="get">
<input class="search-input" name="q" placeholder="Search" type="search" value=""/>
<button type="submit">
<i class="fa fa-search"></i>
</button>
</form>
</div>
</div>
</div>
<p class="top-nav-menu-button" id="js-mobile-menu"><i class="fa fa-bars"></i></p>
<div class="nav">
<ul id="top-nav-menu">
<li class="nav-link more">
<a href="javascript:void(0)">Vision</a>
<ul class="submenu">
<li><a href="/contents/1">The Auroville Charter</a></li>
<li><a href="/contents/197">A Dream</a></li>
<li><a href="/contents/526">To be a True Aurovilian</a></li>
<li><a href="/contents/691">The Galaxy Concept of the City</a></li>
<li><a href="/contents/678">Matrimandir - Soul of the City</a></li>
<li><a href="/categories/9">Founder: The Mother</a></li>
<li><a href="/categories/10">Visionary: Sri Aurobindo</a></li>
<li><a href="/categories/96">Words of Wisdom</a></li>
<li><a href="/categories/8">Integral Yoga</a></li>
</ul>
</li>
<li class="nav-link more">
<a href="javascript:void(0)">Activities</a>
<ul class="submenu">
<li><a href="/categories/6">Matrimandir</a></li>
<li><a href="/categories/22">Planning &amp; Architecture</a></li>
<li><a href="/categories/23">Green Practices</a></li>
<li><a href="/categories/24">Education &amp; Research</a></li>
<li><a href="/categories/26">Art &amp; Culture</a></li>
<li><a href="/categories/25">Health &amp; Wellness</a></li>
<li><a href="/categories/27">Social Enterprises</a></li>
<li><a href="/categories/31">Media &amp; Communication</a></li>
<li><a href="/categories/28">Rural Development</a></li>
<li><a href="/categories/29">City Services</a></li>
</ul>
</li>
<li class="nav-link more">
<a href="javascript:void(0)">Community</a>
<ul class="submenu">
<li><a href="/categories/12">Auroville in Brief</a></li>
<li><a href="/categories/20">Testimonials &amp; Support</a></li>
<li><a href="/categories/13">Organisation &amp; Governance</a></li>
<li><a href="/categories/115">Society</a></li>
<li><a href="/categories/15">Economy</a></li>
<li><a href="/categories/18">History</a></li>
<li><a href="/categories/17">People</a></li>
</ul>
</li>
<li class="nav-link more">
<a href="javascript:void(0)">What You Can Do</a>
<ul class="submenu">
<li><a href="/categories/43">Visit &amp; Stay</a></li>
<li><a href="/categories/46">Volunteer &amp; Intern</a></li>
<li><a href="/contents/4820">Study in Auroville</a></li>
<li><a href="/categories/45">Join Auroville</a></li>
<li><a href="/categories/47">Workshops &amp; Therapies</a></li>
<li><a href="/categories/48">Donate</a></li>
<li><a href="/categories/49">Shop </a></li>
<li><a href="/contact">Contact Auroville</a></li>
<li><a href="/categories/231">Contact Nearby Centre</a></li>
</ul>
</li>
<li class="nav-link more nav-link-language">
<a href="#"><img alt="Flags" src="/assets/flags-139a2a4ac40f89a74852565efbe3ebaa.gif"/></a>
<ul class="submenu">
<li><a href="/language/ar">العربية</a></li>
<li><a href="/language/bg">Български</a></li>
<li><a href="/language/zh-CN">中文</a></li>
<li><a href="/language/nl">Dutch</a></li>
<li><a href="/language/fr">Français</a></li>
<li><a href="/language/de">Deutsch</a></li>
<li><a href="/language/hi">हिन्दी</a></li>
<li><a href="/language/it">Italiano</a></li>
<li><a href="/language/jp">日本語</a></li>
<li><a href="/language/ko">한국어</a></li>
<li><a href="/language/pl">Polskie</a></li>
<li><a href="/language/pt-PT">Português</a></li>
<li><a href="/language/ru">Русский</a></li>
<li><a href="/language/es">Español</a></li>
<li><a href="/language/sv">Svenska</a></li>
<li><a href="/language/ta">தமிழ்</a></li>
</ul>
</li>
</ul>
</div>
</div>
</header>
<script>
  $(function() {
    var menu = $('#top-nav-menu');
    var menuToggle = $('#js-mobile-menu');
    var signUp = $('.sign-up');
    var search_input = $('.search-form input[type=search]');
    var search_btn = $('.search-form button[type=submit]');

    $(menuToggle).on('click', function(e) {
      e.preventDefault();
      menu.slideToggle(function(){
        if(menu.is(':hidden')) {
          menu.removeAttr('style');
          menu.removeClass('.mobile-menu');
        } else {
          menu.addClass('.mobile-menu');
        }
      });
    });

  });
</script>
<div id="alerts">
</div>
<div id="main" role="main">
<div class="content-nosidebar home-page">
<div class="primary-content">
<div class="carousel slide" data-ride="carousel" id="carousel-homepage-slide">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="1" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="2" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="3" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="4" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="5" data-target="#carousel-homepage-slide"></li>
<li data-slide-to="6" data-target="#carousel-homepage-slide"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<img alt="Homepageslide1 490" src="/system/slideshow_images/images/000/000/004/slideshow/HomePageSlide1-490.jpg?1592292921"/>
<div class="carousel-caption">
        Greetings from Auroville, the City of Dawn
      </div>
</div>
<div class="item">
<img alt="Homepageslide2 490" src="/system/slideshow_images/images/000/000/007/slideshow/HomePageSlide2-490.jpg?1592292921"/>
<div class="carousel-caption">
                 Auroville was inaugurated on February 28th, 1968, in the presence of 5000 people
      </div>
</div>
<div class="item">
<img alt="Homepageslide3 490" src="/system/slideshow_images/images/000/000/006/slideshow/HomePageSlide3-490.jpg?1592292922"/>
<div class="carousel-caption">
                 Youth representing 124 Nations participated in the inauguration of Auroville
      </div>
</div>
<div class="item">
<img alt="Homepageslide4 490)" src="/system/slideshow_images/images/000/000/001/slideshow/HomePageSlide4-490).jpg?1592292922"/>
<div class="carousel-caption">
                 To build an ideal society, an ideal city
      </div>
</div>
<div class="item">
<img alt="107882330 10158082109931865 4657927251360510946 n" src="/system/slideshow_images/images/000/000/005/slideshow/107882330_10158082109931865_4657927251360510946_n.jpg?1594445343"/>
<div class="carousel-caption">
                 "Coming to Auroville...means coming to a gigantic effort for progress"
      </div>
</div>
<div class="item">
<img alt="Homepageslide6 490" src="/system/slideshow_images/images/000/000/003/slideshow/HomePageSlide6-490.jpg?1592292923"/>
<div class="carousel-caption">
                 The landscape is now a lush green forest...
      </div>
</div>
<div class="item">
<img alt="Homepageslide7 490" src="/system/slideshow_images/images/000/000/002/slideshow/HomePageSlide7-490.jpg?1592292924"/>
<div class="carousel-caption">
                 At present, Auroville is a growing community of over 3000 people from 58 nations...
      </div>
</div>
</div>
</div>
<div class="below-slideshow">
<div class="content">
<p>Auroville wants to be a universal town where men and women of all countries are able to live in peace and progressive harmony above all creeds, all politics and all nationalities. 
<br/>The purpose of Auroville is to realise human unity.</p>
</div>
</div>
<div class="announcement-region">
<div class="announcement-title"><h2>Visitors Centre and Matrimandir Viewing Point are now open.</h2></div>
<div class="announcement-description"><p>Matrimandir Inner Chamber remains closed till further notice.
<br/>Advance Online booking at https://visit.auroville.org/  is now recommended for day visits to Auroville Visitors Centre and the Matrimandir Viewing Point from which the Matrimandir can be seen from the outside. 
<br/>Kindly note that entries per hour are limited to avoid overcrowding.</p>
<p>All visitors are requested to follow the mandatory protocols as per government regulations, such as wearing masks, hands sanitization, social distancing, etc...</p></div>
</div>
<div class="below-announcement-region">
<div class="block">
<a href="/contents/95">
<div class="thumbnail">
<div class="img">
<img alt="Avinbrief" src="/system/text_blocks/images/000/000/004/thumb_rect/AVinBrief.jpg?1406465997"/>
</div>
<div class="link">
                  Learn about Auroville
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/categories/43">
<div class="thumbnail">
<div class="img">
<img alt="India auroville 2" src="/system/text_blocks/images/000/000/003/thumb_rect/India-Auroville-2.jpg?1406650735"/>
</div>
<div class="link">
                  Visit &amp; Stay
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/252">
<div class="thumbnail">
<div class="img">
<img alt="Visitmm" src="/system/text_blocks/images/000/000/006/thumb_rect/VisitMM.jpg?1406650736"/>
</div>
<div class="link">
                  Visit the Matrimandir
                </div>
</div>
</a>
</div>
<div class="block">
<a href="https://www.auroville.org/contents/158">
<div class="thumbnail">
<div class="img">
<img alt="Press and media" src="/system/text_blocks/images/000/000/010/thumb_rect/press_and_media.jpg?1591866679"/>
</div>
<div class="link">
                  Media and communication
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/4449">
<div class="thumbnail">
<div class="img">
<img alt="Joining auroville0242" src="/system/text_blocks/images/000/000/009/thumb_rect/joining_Auroville0242.jpg?1523908975"/>
</div>
<div class="link">
                  Joining Auroville
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/categories/46">
<div class="thumbnail">
<div class="img">
<img alt="Volunteer" src="/system/text_blocks/images/000/000/005/thumb_rect/Volunteer.jpg?1406651811"/>
</div>
<div class="link">
                  Volunteer &amp; Intern
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/categories/47">
<div class="thumbnail">
<div class="img">
<img alt="Classes" src="/system/text_blocks/images/000/000/008/thumb_rect/Classes.jpg?1406651812"/>
</div>
<div class="link">
                  Enjoy Workshops &amp; Therapies
                </div>
</div>
</a>
</div>
<div class="block">
<a href="https://www.auroville.com">
<div class="thumbnail">
<div class="img">
<img alt="Comnew image 1" src="/system/text_blocks/images/000/000/007/thumb_rect/comNEW-image-1.jpg?1610532959"/>
</div>
<div class="link">
                  Auroville products
                </div>
</div>
</a>
</div>
</div>
<div class="region-5-wrapper">
<div class="region-inner new-content">
<h2><span>Featured</span></h2>
<div class="content">
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/008/268/hover_tile/a-z.gif?1407567376"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/135">Auroville - A to Z</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/013/776/hover_tile/Auroville_in_the_media.jpg?1591184754"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/3780">Auroville in the Media - updates</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/682/hover_tile/fountain-sunset_4131.jpg?1605720526"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/4160">Greetings from Auroville</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/007/994/hover_tile/Dawnfire.jpg?1407370249"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/95">Auroville in brief</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/001/268/hover_tile/inaug0.jpg?1407349046"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/542">Background</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/605/hover_tile/pond_4158.jpg?1602680534"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/3776">Videos about Auroville and Auroville-related events</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/007/767/hover_tile/donate.jpg?1407369464"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/2835">Donating to Auroville projects, online or offline</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/009/010/hover_tile/research-2.jpg?1417152386"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/3373">Academic Research Papers on Auroville</a>
</div>
</div>
</div>
</div>
</div>
<div class="region-inner new-content">
<h2><span>New</span></h2>
<div class="content">
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/825/hover_tile/comNEW-image-1.jpg?1610463592"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/2788">Growing Auroville’s Economy - the Auroville Online Store</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/010/916/hover_tile/land.png?1471340178"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/3221">Acres for Auroville</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/777/hover_tile/Pilgrims-of-the-Infinite-image-small1.jpg?1607510580"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5155">Pilgrims of the Infinite - Manoj Pavithran</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/773/hover_tile/unnamed.png?1607248787"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5154">Eternal Divers</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/766/hover_tile/map-1.jpg?1607080243"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5153">The Galaxy: A pedestrian-based city (1968, 2003-5, 2015)</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/718/hover_tile/Dhruv_Bhasker_Profile.jpg?1606493163"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5151">Dhruv Bhasker</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/645/hover_tile/ABDA-logo_2020-11-04-at-11.40.01.jpg?1604486670"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5122">Auroville Bio-regional Development Activities</a>
</div>
</div>
</div>
<div class="hover-tile-outer">
<div class="hover-tile-container">
<div class="hover-tile hover-tile-visible">
<img src="/system/image_attachments/images/000/014/514/hover_tile/screencapture-www-youtube-com-c-TubeThambiAuroville-channels-1600683710225.png?1600684063"/>
</div>
<div class="hover-tile hover-tile-hidden">
<a href="/contents/5079">Tube Thambi, Doorway to All the Youtube Channels from Auroville</a>
</div>
</div>
</div>
</div>
</div>
<div class="region-inner radio-feed">
<h2><span>Radio</span></h2>
<div class="content">
<ul>
<li>
<div class="cover-image"> <a href="https://www.aurovilleradio.org/ripples-of-indian-light-classical-music-concert-at-pitanga/"><img alt="2019 04 06 music light classical indian music rec in pitanga sound 1" src="/system/radio_contents/images/000/002/432/thumb/2019_04_06_music_light_classical_indian_music_rec_in_pitanga_sound_1.jpg?1564026588"/></a></div>
<div class="title"> <a href="https://www.aurovilleradio.org/ripples-of-indian-light-classical-music-concert-at-pitanga/">Ripples of Indian Light Classical Music – Concert at Pitanga</a></div>
<div class="player">
<audio controls="">
<source src="https://www.aurovilleradio.org/wp-content/uploads/import/2019_04_06_music_light_classical_indian_music_rec_in_pitanga_sound_ed.mp3" type="audio/mpeg">
<!--  <source src="horse.ogg" type="audio/ogg"> -->
                    Your browser does not support this audio format.
                  </source></audio>
</div>
</li>
<li>
<div class="cover-image"> <a href="https://www.aurovilleradio.org/une-srie-hebdomadaire-de-lectures-par-gangalakshmi-en-franais-299/"><img alt="2019 04 24 performance ganga reading french 1" src="/system/radio_contents/images/000/002/431/thumb/2019_04_24_performance_ganga_reading_french_1.jpg?1564026587"/></a></div>
<div class="title"> <a href="https://www.aurovilleradio.org/une-srie-hebdomadaire-de-lectures-par-gangalakshmi-en-franais-299/">Une série hebdomadaire de lectures par Gangalakshmi (en Français) – 299</a></div>
<div class="player">
<audio controls="">
<source src="https://www.aurovilleradio.org/wp-content/uploads/import/2019_04_24_performance_ganga_reading_french_ed.mp3" type="audio/mpeg">
<!--  <source src="horse.ogg" type="audio/ogg"> -->
                    Your browser does not support this audio format.
                  </source></audio>
</div>
</li>
</ul>
<div class="fb-feed">
<h2><span>Facebook</span></h2>
<div class="fb-content-wrapper">
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like-box" data-colorscheme="light" data-header="true" data-href="https://www.facebook.com/auroville.page" data-show-border="false" data-show-faces="false" data-stream="true"></div>
</div>
</div>
</div>
</div>
</div>
<div class="region-6-wrapper">
<div class="block">
<a href="contents/185">
<div class="thumbnail">
<div class="img">
<img alt="Avi" src="/system/text_blocks/images/000/000/014/thumb_rect/AVI.jpg?1408077452"/>
</div>
<div class="link">
                  Auroville International
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/4186">
<div class="thumbnail">
<div class="img">
<img alt="News notesth" src="/system/text_blocks/images/000/000/011/thumb_rect/News-NotesTh.jpg?1406784187"/>
</div>
<div class="link">
                  News &amp; Notes
                </div>
</div>
</a>
</div>
<div class="block">
<a href="/avtoday">
<div class="thumbnail">
<div class="img">
<img alt="Av today" src="/system/text_blocks/images/000/000/012/thumb_rect/AV-Today.gif?1406784524"/>
</div>
<div class="link">
                  Auroville Today Magazine
                </div>
</div>
</a>
</div>
<div class="block">
<a href="https://www.auroville.org/contents/4187">
<div class="thumbnail">
<div class="img">
<img alt="Events" src="/system/text_blocks/images/000/000/013/thumb_rect/events.jpg?1555738882"/>
</div>
<div class="link">
                  Regular events in Auroville
                </div>
</div>
</a>
</div>
</div>
<div class="region-7-wrapper">
<h3>Statements of Support</h3>
<div>
<div class="block">
<a href="/contents/870">
<div class="thumbnail">
<div class="img">
<img alt="Support c" src="/system/text_blocks/images/000/000/016/thumb_rect/Support_c.jpg?1516689902"/>
</div>
<div class="link">
                    Government of India
                  </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/538">
<div class="thumbnail">
<div class="img">
<img alt="Unesco" src="/system/text_blocks/images/000/000/015/thumb_rect/UNESCO.jpg?1406616753"/>
</div>
<div class="link">
                    UNESCO
                  </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/871">
<div class="thumbnail">
<div class="img">
<img alt="Dalailamathumb" src="/system/text_blocks/images/000/000/017/thumb_rect/DalaiLamaThumb.jpg?1406645290"/>
</div>
<div class="link">
                    Dalai Lama
                  </div>
</div>
</a>
</div>
<div class="block">
<a href="/contents/1778">
<div class="thumbnail">
<div class="img">
<img alt="Supportth" src="/system/text_blocks/images/000/000/018/thumb_rect/SupportTh.jpg?1406803287"/>
</div>
<div class="link">
                    Others
                  </div>
</div>
</a>
</div>
</div>
</div>
</div>
<div class="region-8-wrapper">
<div class="facebook-like">
<h3>Keep in touch</h3>
<div id="fb-root"></div>
<script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-action="like" data-href="https://www.facebook.com/auroville.page" data-layout="standard" data-share="true" data-show-faces="true" data-width="200"></div>
</div>
<div class="share-this">
<h3>Share</h3>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_tumblr"></a>
<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
</div>
</div>
<!-- <div class="main-list-subscribe">
          <h3>Get Our Newsletter</h3>
            <div class="form-group">
          </div>
          <div class="form-group">
          </div>
        </div> -->
</div>
</div>
</div> <!--! end of #main -->
</div>
<footer class="footer">
<div class="footer-links">
<ul>
<li><h3>Vision</h3></li>
<li><a href="/contents/1">The Auroville Charter</a></li>
<li><a href="/contents/197">A Dream</a></li>
<li><a href="/contents/526">To be a True Aurovilian</a></li>
<li><a href="/contents/691">The Galaxy Concept of the City</a></li>
<li><a href="/contents/678">Matrimandir - Soul of the City</a></li>
<li><a href="/categories/9">Founder: The Mother</a></li>
<li><a href="/categories/10">Visionary: Sri Aurobindo</a></li>
<li><a href="/categories/96">Words of Wisdom</a></li>
<li><a href="/categories/8">Integral Yoga</a></li>
</ul>
<ul>
<li><h3>Activities</h3></li>
<li><a href="/categories/6">Matrimandir</a></li>
<li><a href="/categories/22">Planning &amp; Architecture</a></li>
<li><a href="/categories/23">Green Practices</a></li>
<li><a href="/categories/24">Education &amp; Research</a></li>
<li><a href="/categories/26">Art &amp; Culture</a></li>
<li><a href="/categories/25">Health &amp; Wellness</a></li>
<li><a href="/categories/27">Social Enterprises</a></li>
<li><a href="/categories/31">Media &amp; Communication</a></li>
<li><a href="/categories/28">Rural Development</a></li>
<li><a href="/categories/29">City Services</a></li>
</ul>
<ul>
<li><h3>Community</h3></li>
<li><a href="/categories/12">Auroville in Brief</a></li>
<li><a href="/categories/20">Testimonials &amp; Support</a></li>
<li><a href="/categories/13">Organisation &amp; Governance</a></li>
<li><a href="/categories/115">Society</a></li>
<li><a href="/categories/15">Economy</a></li>
<li><a href="/categories/18">History</a></li>
<li><a href="/categories/17">People</a></li>
</ul>
<ul>
<li><h3>What You Can Do</h3></li>
<li><a href="/categories/43">Visit &amp; Stay</a></li>
<li><a href="/categories/46">Volunteer &amp; Intern</a></li>
<li><a href="/contents/4820">Study in Auroville</a></li>
<li><a href="/categories/45">Join Auroville</a></li>
<li><a href="/categories/47">Workshops &amp; Therapies</a></li>
<li><a href="/categories/48">Donate</a></li>
<li><a href="/categories/49">Shop </a></li>
<li><a href="/categories/231">Contact Nearby Centre</a></li>
</ul>
</div>
</footer>
<div class="disclaimer">
<div class="wrapper">
<div class="fb-share">
<div class="fb-like" data-action="like" data-href="https://www.facebook.com/auroville.page" data-layout="button_count" data-share="false" data-show-faces="true"></div>
</div>
<a href="/contents/3112">Disclaimer</a>
<a class="contact-us" data-toggle="modal" href="/contact">Contact</a>
<a class="sign-in" data-target="#loginModal" data-toggle="modal" href="javascript:void(0)">Sign in</a>
</div>
</div>
<div aria-hidden="true" aria-labelledby="loginModalLabel" class="modal fade" id="loginModal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
<h4 class="modal-title" id="actionLpgomModalLabel">Sign in</h4>
</div>
<div class="modal-body">
<div class="content-nosidebar">
<div class="primary-content">
<form accept-charset="UTF-8" action="/users/sign_in" class="simple_form user" method="post" novalidate="novalidate"><div style="display:none"><input name="utf8" type="hidden" value="✓"/><input name="authenticity_token" type="hidden" value="j8QRpREIgJbWY8qbx+bIcXIR2KEwKNVz7V+ttz/vMcA="/></div>
<div class="form-group">
<div class="control-group email optional user_email"><label class="email optional control-label" for="user_email">Email</label><div class="controls"><input autofocus="autofocus" class="string email optional form-control" id="user_email" name="user[email]" type="email"/></div></div>
</div>
<div class="form-group">
<div class="control-group password optional user_password"><label class="password optional control-label" for="user_password">Password</label><div class="controls"><input class="password optional form-control" id="user_password" name="user[password]" type="password"/></div></div>
</div>
<div class="form-group">
</div>
<div class="form-actions">
<input class="btn btn btn-success" name="commit" type="submit" value="Sign in"/>
</div>
<div class="forget-fgt">
<a href="/users/password/new">Forgot your password?</a><br/>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-147212-1', 'auto');
  ga('send', 'pageview');

</script>
<script async="async" src="//script.crazyegg.com/pages/scripts/0070/1216.js" type="text/javascript"></script>
</body>
</html>
