<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="pl-pl" xml:lang="pl-pl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 - Nie znaleziono widoku [nazwa, typ, przedrostek]: article, php, contentView.</title>
<link href="/templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/position.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/layout.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/print.css" media="Print" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/personal.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/general.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/personal.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 6]>
			<link href="/templates/beez_20/css/ieonly.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<!--[if IE 7]>
			<link href="/templates/beez_20/css/ie7only.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<style type="text/css">
			<!--
			#errorboxbody
			{margin:30px}
			#errorboxbody h2
			{font-weight:normal;
			font-size:1.5em}
			#searchbox
			{background:#eee;
			padding:10px;
			margin-top:20px;
			border:solid 1px #ddd
			}
			-->
</style>
</head>
<body>
<div id="all">
<div id="back">
<div id="header">
<div class="logoheader">
<h1 id="logo">
<img alt="Joomla!" src="/images/joomla_black.gif"/>
<span class="header1">
                                        Open Source Content Management                                        </span></h1>
</div><!-- end logoheader -->
<ul class="skiplinks">
<li><a class="u2" href="#wrapper2">Skocz do obszaru komunikatu błędu oraz wyszukiwarki</a></li>
<li><a class="u2" href="#nav">Skocz do nawigacji</a></li>
</ul>
<div id="line"></div>
</div><!-- end header -->
<div id="contentarea2">
<div class="left1" id="nav">
<h2 class="unseen">Nawigacja</h2>
<ul class="nav menu mod-list">
<li class="item-464 current active"><a href="/">Strona główna</a></li><li class="item-444"><a href="/sample-sites-2.html">Dla smakoszy</a></li><li class="item-471"><a href="/noclegi.html">Noclegi</a></li><li class="item-468"><a href="/2013-06-26-17-12-01.html">Informacje praktyczne</a></li><li class="item-513"><a href="/kontakt.html">Kontakt</a></li></ul>
</div>
<!-- end navi -->
<div id="wrapper2">
<div id="errorboxbody">
<h2>Nieznany błąd<br/>
								Nie znaleziono żądanej strony.</h2>
<div id="searchbox">
<h3 class="unseen">Szukaj</h3>
<p>Możesz przeszukać witrynę albo przejść na stronę startową.</p>
									Error</div></div></div></div></div></div></body></html>