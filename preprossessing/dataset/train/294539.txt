<!DOCTYPE html>
<html dir="ltr" lang="es-es" prefix="og: https://ogp.me/ns#" xml:lang="es-es" xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.cayosalinas.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="abogado bolivia, abogado cochabamba, firma abogado, bufete abogado" name="keywords"/>
<meta content="Nos especializamos en asistir legalmente a empresas unipersonales y sociedades comerciales en el ejercicio de su giro societario. Para el efecto, brindamos asesoramiento en el proceso de constitución, formación, acreditación legal, escisión, fusión, transformación, disolución y liquidación de sociedades." name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>INICIO</title>
<link href="https://www.cayosalinas.com/index.php" hreflang="es-ES" rel="alternate"/>
<link href="https://www.cayosalinas.com/index.php/en/" hreflang="en-GB" rel="alternate"/>
<link href="/templates/lt_law/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://www.cayosalinas.com/index.php/component/search/?id=1&amp;Itemid=204&amp;format=opensearch" rel="search" title="Buscar cayosalinas.com" type="application/opensearchdescription+xml"/>
<link href="https://cdn.jsdelivr.net/npm/simple-line-icons@2.4.1/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/k2.css?v=2.10.3" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_sppagebuilder/assets/css/sppagebuilder.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,800,600,regular&amp;latin" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/default.css" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/legacy.css" rel="stylesheet" type="text/css"/>
<link href="/templates/lt_law/css/template.css" rel="stylesheet" type="text/css"/>
<link class="preset" href="/templates/lt_law/css/presets/preset4.css" rel="stylesheet" type="text/css"/>
<link href="https://www.cayosalinas.com/media/com_uniterevolution2/assets/rs-plugin/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="https://www.cayosalinas.com/media/com_uniterevolution2/assets/rs-plugin/css/dynamic-captions.css" rel="stylesheet" type="text/css"/>
<link href="https://www.cayosalinas.com/media/com_uniterevolution2/assets/rs-plugin/css/static-captions.css" rel="stylesheet" type="text/css"/>
<link href="/media/mod_languages/css/template.css?9811fbe689dc2ef4fa2d88072b59146a" rel="stylesheet" type="text/css"/>
<style type="text/css">
body{font-family:Open Sans, sans-serif; font-weight:300; }h1{font-family:Open Sans, sans-serif; font-weight:800; }h2{font-family:Open Sans, sans-serif; font-weight:600; }h3{font-family:Open Sans, sans-serif; font-weight:normal; }h4{font-family:Open Sans, sans-serif; font-weight:normal; }h5{font-family:Open Sans, sans-serif; font-weight:600; }h6{font-family:Open Sans, sans-serif; font-weight:600; }.sp-megamenu-parent .sp-dropdown {
  z-index: 999; 
}#sp-top-bar{ background-color:#f5f5f5; }
	</style>
<script src="/media/jui/js/jquery.min.js?9811fbe689dc2ef4fa2d88072b59146a" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?9811fbe689dc2ef4fa2d88072b59146a" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?9811fbe689dc2ef4fa2d88072b59146a" type="text/javascript"></script>
<script src="/media/k2/assets/js/k2.frontend.js?v=2.10.3&amp;b=20200429&amp;sitepath=/" type="text/javascript"></script>
<script src="/components/com_sppagebuilder/assets/js/sppagebuilder.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.cookie.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.sticky.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/main.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/waypoints.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.mixitup.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.stellar.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/wow.min.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/custom.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/scroll.js" type="text/javascript"></script>
<script src="/templates/lt_law/js/jquery.nav.js" type="text/javascript"></script>
<script src="https://www.cayosalinas.com/media/com_uniterevolution2/assets/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="https://www.cayosalinas.com/media/com_uniterevolution2/assets/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<meta content="INICIO" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.cayosalinas.com/" property="og:url"/>
<link href="https://www.cayosalinas.com/index.php" hreflang="x-default" rel="alternate"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-145398129-1', 'www.cayosalinas.com');
  ga('send', 'pageview');

</script>
<!-- Universal Google Analytics Plugin by PB Web Development -->
</head>
<body class="site com-sppagebuilder view-page no-layout no-task itemid-204 es-es ltr sticky-header layout-fluid">
<div class="body-innerwrapper">
<section id="sp-top-bar"><div class="container"><div class="row"><div class="col-sm-6 col-md-6" id="sp-top1"><div class="sp-column "><ul class="social-icons"><li><a href="https://www.facebook.com/cayosalinasabogados" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="https://twitter.com/?lang=es" target="_blank"><i class="fa fa-twitter"></i></a></li></ul><div class="sp-module "><div class="sp-module-content"><div class="mod-languages">
<ul class="lang-inline" dir="ltr">
<li class="lang-active">
<a href="https://www.cayosalinas.com/">
<img alt="Español" src="/media/mod_languages/images/es.gif" title="Español"/> </a>
</li>
<li>
<a href="/index.php/en/">
<img alt="English (UK)" src="/media/mod_languages/images/en.gif" title="English (UK)"/> </a>
</li>
</ul>
</div>
</div></div></div></div><div class="col-sm-6 col-md-6" id="sp-top2"><div class="sp-column "><ul class="sp-contact-info"><li class="sp-contact-phone"><i class="fa fa-phone"></i> +591 4 4259998</li><li class="sp-contact-email"><i class="fa fa-envelope"></i> <a href="mailto:consultas@cayosalinas.com">consultas@cayosalinas.com</a></li></ul></div></div></div></div></section><header id="sp-header"><div class="container"><div class="row"><div class="col-xs-8 col-sm-3 col-md-3" id="sp-logo"><div class="sp-column "><a class="logo" href="/"><h1><img alt="cayosalinas.com" class="sp-default-logo hidden-xs" src="/images/ultimo_logo_cayo.png"/><img alt="cayosalinas.com" class="sp-retina-logo hidden-xs" height="90" src="/images/ultimo_logo_cayo.png" width="336"/><img alt="cayosalinas.com" class="sp-default-logo visible-xs" src="/images/ultimo_logo_cayo.png"/></h1></a></div></div><div class="col-xs-4 col-sm-9 col-md-9" id="sp-menu"><div class="sp-column "> <div class="sp-megamenu-wrapper">
<a href="#" id="offcanvas-toggler"><i class="fa fa-bars"></i></a>
<ul class="sp-megamenu-parent menu-fade hidden-xs"><li class="sp-menu-item current-item active"><a href="/index.php">INICIO</a></li><li class="sp-menu-item sp-has-child"><a href="/index.php/intro">PRESENTACIÓN</a><div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/intro/antecedentes-1">ANTECEDENTES</a></li><li class="sp-menu-item"><a href="/index.php/intro/vision">VISIÓN</a></li><li class="sp-menu-item"><a href="/index.php/intro/mision">MISIÓN</a></li><li class="sp-menu-item"><a href="/index.php/intro/quienes-somos">QUIENES SOMOS</a></li></ul></div></div></li><li class="sp-menu-item sp-has-child"><a href="/index.php/servicios">SERVICIOS</a><div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/servicios/campo-de-ejercicio">CAMPO DE EJERCICIO</a></li><li class="sp-menu-item"><a href="/index.php/servicios/asesoramiento-en-litigios"> ASESORAMIENTO EN LITIGIOS</a></li><li class="sp-menu-item"><a href="/index.php/servicios/asesoramiento-preventivo">ASESORAMIENTO PREVENTIVO</a></li><li class="sp-menu-item"><a href="/index.php/servicios/auditorias-legales">AUDITORÍAS LEGALES</a></li></ul></div></div></li><li class="sp-menu-item sp-has-child"><a href="/index.php/interes">INTERÉS</a><div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/interes/clientes">CARTERA DE CLIENTES</a></li><li class="sp-menu-item"><a href="/index.php/interes/boletines">BOLETÍNES</a></li><li class="sp-menu-item sp-has-child"><a href="/index.php/interes/publicaciones">PUBLICACIONES</a><div class="sp-dropdown sp-dropdown-sub sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/interes/publicaciones/ensayos">ENSAYOS</a></li><li class="sp-menu-item"><a href="/index.php/interes/publicaciones/conferencias">CONFERENCIAS</a></li></ul></div></div></li><li class="sp-menu-item"><a href="/index.php/interes/leyes-de-bolivia">LEYES DE BOLIVIA</a></li><li class="sp-menu-item"><a href="/index.php/interes/sitios-de-interes"> SITIOS DE INTERÉS</a></li><li class="sp-menu-item sp-has-child"><a href="/index.php/interes/videoteca">VIDEOTECA</a><div class="sp-dropdown sp-dropdown-sub sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/interes/videoteca/caballero-pregunta">CABALLERO PREGUNTA</a></li></ul></div></div></li></ul></div></div></li><li class="sp-menu-item sp-has-child"><a href="/index.php/contactos">CONTACTOS</a><div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;"><div class="sp-dropdown-inner"><ul class="sp-dropdown-items"><li class="sp-menu-item"><a href="/index.php/contactos/oficinas">OFICINAS</a></li><li class="sp-menu-item"><a href="/index.php/contactos/contacto">CONTACTO</a></li></ul></div></div></li><li class="sp-menu-item"><a href="/index.php/blogs">BLOG</a></li></ul> </div>
</div></div></div></div></header><section id="sp-page-title"><div class="row"><div class="col-sm-12 col-md-12" id="sp-title"><div class="sp-column "><div class="sp-module "><div class="sp-module-content"><!-- START REVOLUTION SLIDER 4.6 fullwidth mode -->
<div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_1_1_wrapper" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:569px;">
<div class="rev_slider fullwidthabanner" id="rev_slider_1_1" style="display:none;max-height:569px;height:569px;">
<ul> <!-- SLIDE  1-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/topbanner/cayo-salinas-000.jpg" data-title="Slide 0" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo-salinas-000" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/topbanner/cayo-salinas-000.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  2-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/cayo3.jpg" data-title="Slide 2" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo3" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/cayo3.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  3-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/topbanner/cayo-salinas-005.jpg" data-title="Slide 3" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo-salinas-005" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/topbanner/cayo-salinas-005.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  4-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/cayo1.jpg" data-title="Slide 1" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo1" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/cayo1.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  5-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/cayo4.jpg" data-title="Slide 4" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo4" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/cayo4.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  6-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/cayo2.jpg" data-title="Slide 5" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo2" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/cayo2.jpg"/>
<!-- LAYERS -->
</li>
<!-- SLIDE  7-->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-thumb="https://www.cayosalinas.com/images/topbanner/cayo-salinas-002.jpg" data-title="Slide 6" data-transition="random">
<!-- MAIN IMAGE -->
<img alt="cayo-salinas-002" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="https://www.cayosalinas.com/images/topbanner/cayo-salinas-002.jpg"/>
<!-- LAYERS -->
</li>
</ul>
<div class="tp-bannertimer"></div> </div>
<script type="text/javascript">

					
				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
								
				 
						var setREVStartSize = function() {
							var	tpopt = new Object(); 
								tpopt.startwidth = 960;
								tpopt.startheight = 569;
								tpopt.container = jQuery('#rev_slider_1_1');
								tpopt.fullScreen = "off";
								tpopt.forceFullWidth="off";

							tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
						};
						
						/* CALL PLACEHOLDER */
						setREVStartSize();
								
				
				var tpj=jQuery;				
				tpj.noConflict();				
				var revapi1;
				
				
				
				tpj(document).ready(function() {
				
					
								
				if(tpj('#rev_slider_1_1').revolution == undefined)
					revslider_showDoubleJqueryError('#rev_slider_1_1');
				else
				   revapi1 = tpj('#rev_slider_1_1').show().revolution(
					{
						dottedOverlay:"none",
						delay:9000,
						startwidth:960,
						startheight:569,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:5,
													
						simplifyAll:"off",						
						navigationType:"none",
						navigationArrows:"solo",
						navigationStyle:"custom",						
						touchenabled:"on",
						onHoverStop:"on",						
						nextSlideOnWindowFocus:"off",
						
						swipe_threshold: 75,
						swipe_min_touches: 1,
						drag_block_vertical: false,
																		
																		
						keyboardNavigation:"off",
						
						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,
								
						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

						spinner:"spinner0",
						
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",
						
						autoHeight:"off",						
						forceFullWidth:"off",						
												
												
												
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,
						
												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						isJoomla: true
					});
					
					
					
									
				});	/*ready*/
									
			</script>
</div>
<!-- END REVOLUTION SLIDER --> </div></div></div></div></div></section><section id="sp-main-body"><div class="row"><div class="col-sm-12 col-md-12" id="sp-component"><div class="sp-column "><div id="system-message-container">
</div>
<div class="sp-page-builder page-1" id="sp-page-builder">
<div class="page-content">
<section class="sppb-section " style=""><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-12"><div class="sppb-addon-container" style=""><div class="sppb-addon sppb-addon-module "><div class="sppb-addon-content">
<div class="custom">
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; color: #000000; font-size: xx-large;"><span style="box-sizing: border-box; font-family: terminal, monaco;"><br/><br/>DERECHO EMPRESARIAL</span></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><strong style="box-sizing: border-box;"><span style="box-sizing: border-box; font-size: xx-large;"><span style="box-sizing: border-box; color: #000000;"> </span></span></strong></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: justify;">Nos especializamos en asistir legalmente a empresas unipersonales y sociedades comerciales en el ejercicio de su giro societario. Para el efecto, brindamos asesoramiento en el proceso de constitución, formación, acreditación legal, escisión, fusión, transformación, disolución y liquidación de sociedades. Asimismo, brindamos asistencia y asesoramiento  legal a empresas unipersonales y sociedades comerciales en las diferentes áreas del derecho, ante la concurrencia de situaciones que requieran el concurso de abogados ya sea de manera preventiva o en el patrocinio  ante situaciones que ameriten asesoramiento legal especializado dentro o fuera de tribunales.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: justify;"> </p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; font-size: xx-large;"><span style="box-sizing: border-box; color: #000000;"><span style="box-sizing: border-box; font-family: terminal, monaco;">ARBITRAJE</span></span></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; font-size: xx-large;"><span style="box-sizing: border-box; color: #000000;"> </span></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: justify;">Como Estudio de Abogados poseemos amplia experiencia en la materia, por lo que ofrecemos a nuestros clientes asistencia legal en la redacción de contratos donde se pacten cláusulas arbitrales, así como en el proceso de planificación, diseño de estrategias, formulación de demandas arbitrales y procuración de procesos arbitrales tanto nacionales como internacionales. Asimismo, estamos registrados en la lista de árbitros de los más importantes Centros de Conciliación y Arbitraje.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;"> </span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; color: #000000;"><span style="box-sizing: border-box; font-size: xx-large;"><span style="box-sizing: border-box; font-family: terminal, monaco;">AUDITORIAS LEGALES</span></span></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; color: #000000;"><span style="box-sizing: border-box; font-size: xx-large;"> </span></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: justify;">Diversas empresas han requerido nuestros servicios para la elaboración de informes de Auditoría Legal y/o Due Dilligence. Al ser así, poseemos amplia experiencia en la materia, por lo que estamos en condiciones de establecer la situación legal de una empresa unipersonal o una sociedad comercial en las áreas de su desenvolvimiento, vgr., contratación civil con terceros, cargas laborales, cumplimiento de normas comerciales, impositivas, aduaneras y otros; obligaciones con terceros y su grado de ejecutabilidad; análisis de contingencias judiciales, todo ello con el propósito de contar con una radiografía de la empresa que permita su diagnóstico estratégico, competitivo y operativo.</p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"> </p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px;"><span style="box-sizing: border-box; font-family: arial, helvetica, sans-serif;"> </span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: right;"><em style="box-sizing: border-box;"><span style="box-sizing: border-box; font-size: x-large;"><span style="box-sizing: border-box; color: #000000;"><span style="box-sizing: border-box; font-size: xx-large;">"Da mihi factum, dabo tibi ius"</span></span></span></em></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 24px; text-align: right;">Dame los hechos, yo te daré el derecho</p></div>
</div></div></div></div></div></div></section><section class="sppb-section " style=""><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-6"><div class="sppb-addon-container" style=""><div class="sppb-addon sppb-addon-module "><h3 class="sppb-addon-title" style="font-weight:CCCCCCCC;">Oferta de Servicios</h3><div class="sppb-addon-content"><ul class="category-module mod-list">
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/221-arbitraje-nacional-e-internacional">Arbitraje Nacional e Internacional</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/222-due-diligence">Auditoría Legal y/o Due Dilligence</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/223-derecho-civil">Derecho Civil</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/224-derecho-comercial">Derecho Comercial</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/228-derecho-constitucional">Derecho Constitucional</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/229-derecho-de-la-propiedad-intelectual">Derecho de la Propiedad Intelectual</a>
</li>
</ul>
</div></div><a class="sppb-btn sppb-btn-primary sppb-btn- " href="/index.php/servicios/campo-de-ejercicio" role="button" target="">Leer Más</a></div></div><div class="sppb-col-sm-6"><div class="sppb-addon-container" style=""><div class="sppb-addon sppb-addon-single-image sppb-text-center "><div class="sppb-addon-content"><img alt="" class="sppb-img-responsive" src="/images/img_3.jpg"/></div></div></div></div></div></div></section><section class="sppb-section " style=""><div class="sppb-container"><div class="sppb-section-title sppb-text-center"><h3 class="sppb-title-heading" style="font-size:30px;line-height: 30px;margin-top:50px;">ALGUNOS DE NUESTROS SERVICIOS</h3><p class="sppb-title-subheading" style="font-size:px;">El profesional del derecho se debe a sí mismo y a su misión de auxiliar de la justicia otorgada por la ley, una conducta íntegra y ceñida a los parámetros de lo moral, de la equidad, desprendimiento de sus propios intereses con tal de favorecer plenamente aquellos del cliente que son siempre el motivo de nuestra labor, te mostramos algunos de nuestros servicios.</p></div></div><div class="sppb-row"><div class="sppb-col-sm-3"><div class="sppb-addon-container lt-intro-sub sppb-wow fadeInDown" data-sppb-wow-delay="600ms" data-sppb-wow-duration="900ms" style=""><div class="sppb-addon sppb-addon-feature sppb-text-center lt-introduction-sub"><div class="sppb-addon-content"><div class="sppb-icon"><span style="display:inline-block;text-align:center;;"><i class="fa fa-users" style="font-size:48px;width:48px;height:48px;line-height:48px;;"></i></span></div><h3 class="sppb-feature-box-title" style="font-size:14px;line-height:14px;">ASESORAMIENTO EN LITIGIOS</h3><div class="sppb-addon-text"><p>Representamos a nuestros clientes ante tribunales ordinarios en todo el territorio de la República, y en... <a href="/index.php/servicios/asesoramiento-en-litigios">leer mas</a></p></div></div></div></div></div><div class="sppb-col-sm-3"><div class="sppb-addon-container600 sppb-wow fadeInDown" data-sppb-wow-delay="900ms" style=""><div class="sppb-addon sppb-addon-feature sppb-text-center lt-introduction-sub"><div class="sppb-addon-content"><div class="sppb-icon"><span style="display:inline-block;text-align:center;;"><i class="fa fa-globe" style="font-size:48px;width:48px;height:48px;line-height:48px;;"></i></span></div><h3 class="sppb-feature-box-title" style="font-size:14px;line-height:14px;">ASESORAMIENTO PREVENTIVO</h3><div class="sppb-addon-text"><p>Las relaciones sociales y comerciales de las personas pueden derivar en conflictos de intereses ...... <a href="/index.php/servicios/asesoramiento-preventivo">leer mas</a></p></div></div></div></div></div><div class="sppb-col-sm-3"><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="600ms" data-sppb-wow-duration="900ms" style=""><div class="sppb-addon sppb-addon-feature sppb-text-center lt-introduction-sub"><div class="sppb-addon-content"><div class="sppb-icon"><span style="display:inline-block;text-align:center;;"><i class="fa fa-legal" style="font-size:48px;width:48px;height:48px;line-height:48px;;"></i></span></div><h3 class="sppb-feature-box-title" style="font-size:14px;line-height:14px;">AUDITORÍAS LEGALES</h3><div class="sppb-addon-text"><p>Establece la situación legal de una empresa unipersonal o una sociedad comercial en las áreas de su desenvolvimiento, ... <a href="/index.php/servicios/campo-de-ejercicio/222-due-diligence">leer mas</a></p></div></div></div></div></div><div class="sppb-col-sm-3"><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="600ms" data-sppb-wow-duration="900ms" style=""><div class="sppb-addon sppb-addon-feature sppb-text-center lt-introduction-sub"><div class="sppb-addon-content"><div class="sppb-icon"><span style="display:inline-block;text-align:center;;"><i class="fa fa-university" style="font-size:48px;width:48px;height:48px;line-height:48px;;"></i></span></div><h3 class="sppb-feature-box-title" style="font-size:14px;line-height:14px;">CAMPO DE EJERCICIO</h3><div class="sppb-addon-text"><p>Varios campos de ejercico de nuestra profesión, como la defensa de los derechos e intereses de las personas. <a href="/index.php/servicios/campo-de-ejercicio">leer mas</a></p></div></div></div></div></div></div></section><section class="sppb-section " style="background-color:#f5f5f5;"><div class="sppb-container"><div class="sppb-row"><div class="sppb-col-sm-3"><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="300ms" data-sppb-wow-duration="300ms" style="padding:10px 20px 10px 20px;"><div class="sppb-addon sppb-addon-text-block sppb-text-center "><h3 class="sppb-addon-title" style="">Acerca de Nosotros</h3><div class="sppb-addon-content"><div style="text-align: justify;">Nuestro Estudio agrupa profesionales capacitados en diferentes áreas del Derecho, a fin de prestar asesoramiento y orientación al cliente en la toma de decisiones de índole legal y en la defensa de sus intereses en las instancias que correspondan.</div></div></div><a class="sppb-btn sppb-btn-primary sppb-btn- " href="/index.php/inicio/presentacion" role="button" target="">Leer Mas</a></div></div><div class="sppb-col-sm-3"><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="300ms" data-sppb-wow-duration="300ms" style="padding:10px 20px 10px 20px;"><div class="sppb-addon sppb-addon-text-block sppb-text-center "><h3 class="sppb-addon-title" style="">Dirección Contacto</h3><div class="sppb-addon-content"><div style="text-align: justify;"><strong>COCHABAMBA:</strong> Av. Oquendo Nº 1080 Edificio Los Tiempos Torre II 8º Piso  Tels.: +591 4 4255554 - 4253007 - 4259998 Fax: +591 4 4539451 Cel: +591 70370000</div>
<div style="text-align: justify;"><strong>SUCURSALES:</strong>  Santa Cruz de la Sierra, La Paz, Sucre, Tarija, Oruro, Potosi.</div></div></div></div></div><div class="sppb-col-sm-3"><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="300ms" data-sppb-wow-duration="300ms" style="padding:10px 20px 10px 20px;"><div class="sppb-addon sppb-addon-module "><h3 class="sppb-addon-title" style="">Oferta de Servicios</h3><div class="sppb-addon-content"><ul class="category-module mod-list">
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/221-arbitraje-nacional-e-internacional">Arbitraje Nacional e Internacional</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/222-due-diligence">Auditoría Legal y/o Due Dilligence</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/223-derecho-civil">Derecho Civil</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/224-derecho-comercial">Derecho Comercial</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/228-derecho-constitucional">Derecho Constitucional</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/campo-de-ejercicio/229-derecho-de-la-propiedad-intelectual">Derecho de la Propiedad Intelectual</a>
</li>
</ul>
</div></div><a class="sppb-btn sppb-btn-primary sppb-btn- " href="/index.php/servicios/campo-de-ejercicio" role="button" target="">Leer Más</a></div></div><div class="sppb-col-sm-3 "><div class="sppb-addon-container sppb-wow fadeInDown" data-sppb-wow-delay="300ms" data-sppb-wow-duration="300ms" style="padding:10px 20px 10px 20px;"><div class="sppb-addon sppb-addon-module nav menu"><h3 class="sppb-addon-title" style="">Zona de Practicas</h3><div class="sppb-addon-content"><ul class="category-module mod-list">
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/asesoramiento-en-litigios">ASESORAMIENTO EN LITIGIOS</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/asesoramiento-preventivo">ASESORAMIENTO PREVENTIVO</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/11-campo-de-ejercicio">CAMPO DE EJERCICIO</a>
</li>
<li>
<a class="mod-articles-category-title " href="/index.php/servicios/105-experiencia-especifica-en-auditorias-legales-en-contratos-superiores-a-us-48000000">EXPERIENCIA ESPECIFICA EN AUDITORIAS LEGALES EN CONTRATOS SUPERIORES A  $US48’000.000.-</a>
</li>
</ul>
</div></div></div></div></div></div></section> </div>
</div>
</div></div></div></section><footer id="sp-footer"><div class="container"><div class="row"><div class="col-sm-12 col-md-12" id="sp-footer1"><div class="sp-column "><span class="sp-copyright"> © 2016 Cayo Salinas &amp; Asociados. Todos los derechos reservados. Diseñado por IT-VIRTUAL</span></div></div></div></div></footer>
<div class="offcanvas-menu">
<a class="close-offcanvas" href="#"><i class="fa fa-remove"></i></a>
<div class="offcanvas-inner">
<div class="sp-module "><div class="sp-module-content"><ul class="nav menu mod-list">
<li class="item-204 default current active"><a href="/index.php">INICIO</a></li><li class="item-106 deeper parent"><a href="/index.php/intro">PRESENTACIÓN</a><ul class="nav-child unstyled small"><li class="item-219"><a href="/index.php/intro/antecedentes-1">ANTECEDENTES</a></li><li class="item-108"><a href="/index.php/intro/vision">VISIÓN</a></li><li class="item-109"><a href="/index.php/intro/mision">MISIÓN</a></li><li class="item-111"><a href="/index.php/intro/quienes-somos">QUIENES SOMOS</a></li></ul></li><li class="item-114 deeper parent"><a href="/index.php/servicios">SERVICIOS</a><ul class="nav-child unstyled small"><li class="item-115"><a href="/index.php/servicios/campo-de-ejercicio">CAMPO DE EJERCICIO</a></li><li class="item-116"><a href="/index.php/servicios/asesoramiento-en-litigios"> ASESORAMIENTO EN LITIGIOS</a></li><li class="item-117"><a href="/index.php/servicios/asesoramiento-preventivo">ASESORAMIENTO PREVENTIVO</a></li><li class="item-118"><a href="/index.php/servicios/auditorias-legales">AUDITORÍAS LEGALES</a></li></ul></li><li class="item-143 deeper parent"><a href="/index.php/interes">INTERÉS</a><ul class="nav-child unstyled small"><li class="item-119"><a href="/index.php/interes/clientes">CARTERA DE CLIENTES</a></li><li class="item-123"><a href="/index.php/interes/boletines">BOLETÍNES</a></li><li class="item-125 deeper parent"><a href="/index.php/interes/publicaciones">PUBLICACIONES</a><ul class="nav-child unstyled small"><li class="item-133"><a href="/index.php/interes/publicaciones/ensayos">ENSAYOS</a></li><li class="item-134"><a href="/index.php/interes/publicaciones/conferencias">CONFERENCIAS</a></li></ul></li><li class="item-137"><a href="/index.php/interes/leyes-de-bolivia">LEYES DE BOLIVIA</a></li><li class="item-138"><a href="/index.php/interes/sitios-de-interes"> SITIOS DE INTERÉS</a></li><li class="item-135 deeper parent"><a href="/index.php/interes/videoteca">VIDEOTECA</a><ul class="nav-child unstyled small"><li class="item-136"><a href="/index.php/interes/videoteca/caballero-pregunta">CABALLERO PREGUNTA</a></li></ul></li></ul></li><li class="item-142 deeper parent"><a href="/index.php/contactos">CONTACTOS</a><ul class="nav-child unstyled small"><li class="item-120"><a href="/index.php/contactos/oficinas">OFICINAS</a></li><li class="item-121"><a href="/index.php/contactos/contacto">CONTACTO</a></li></ul></li><li class="item-218"><a href="/index.php/blogs">BLOG</a></li></ul>
</div></div><div class="sp-module "><div class="sp-module-content"><div class="search">
<form action="/index.php" method="post">
<input class="inputbox search-query" id="mod-search-searchword" maxlength="200" name="searchword" placeholder="Ingrese su búsqueda..." size="0" type="text"/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="204"/>
</form>
</div>
</div></div>
</div>
</div>
</div>
</body>
</html>