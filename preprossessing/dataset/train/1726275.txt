<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Homepage</title> <script src="//use.typekit.net/wtz5xyq.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="/css/styles.css" rel="stylesheet"/>
<script src="/js/vendor/modernizr-2.8.0.min.js"></script> </head>
<body class="page-home view-home ">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-36214568-45', 'auto');
    ga('send', 'pageview');
  </script> <header id="header"><button id="btn-menu"><span></span><span></span><span></span></button><div class="wrapper"><a class="logo" href="/" title="Homepage"><img alt="Logo Zenso" src="/img/logo.png"/></a><div id="nav"><nav class="nav-main"><ul><li><a href="/medical-solutions/" title="Medical Solutions"><span class="icon-holder"><i class="icon icon-medical2 hover"></i><i class="icon icon-medical"></i></span>Medical Solutions</a></li><li><a href="/industrial-solutions/" title="Industrial Solutions"><span class="icon-holder"><i class="icon icon-industrial2 hover"></i><i class="icon icon-industrial"></i></span>Industrial Solutions</a></li><li><a href="/consumer-solutions/" title="Consumer Solutions"><span class="icon-holder"><i class="icon icon-consumer2 hover"></i><i class="icon icon-consumer"></i></span>Consumer Solutions</a></li></ul></nav><nav class="nav-pages"><ul><li><a class="active" href="/" title="Homepage">Home</a></li><li class="with-sub"><a href="/methodology-expertise/" title="Methodology &amp; Expertise">Expertise<span class="icon-holder"><i class="icon icon-sub2 hover"></i><i class="icon icon-sub"></i></span></a><ul><li><a href="/methodology-expertise/5/methodology/" title="Methodology">Methodology</a></li><li><a href="/methodology-expertise/13/methodology-applied/" title="Methodology applied">Methodology applied</a></li><li><a href="/methodology-expertise/6/expertise/" title="Expertise">Expertise</a></li></ul></li><li class="with-sub"><a href="/references/" title="References">References<span class="icon-holder"><i class="icon icon-sub2 hover"></i><i class="icon icon-sub"></i></span></a><ul><li><a href="/references/7/medical/" title="Medical">Medical</a></li><li><a href="/references/8/industrial/" title="Industrial">Industrial</a></li><li><a href="/references/9/consumer/" title="Consumer">Consumer</a></li></ul></li><li class="with-sub"><a href="/company/" title="Company">Company<span class="icon-holder"><i class="icon icon-sub2 hover"></i><i class="icon icon-sub"></i></span></a><ul><li><a href="/company/1/about/" title="About">About</a></li><li><a href="/company/2/management/" title="Management">Management</a></li><li><a href="/company/3/careers/" title="Careers">Careers</a></li><li><a href="/company/12/quality/" title="Quality">Quality</a></li></ul></li><li><a href="/news/" title="News">News</a></li><li><a href="/contact/" title="Contact">Contact</a></li><li><a href="http://server.zenso.be:8080/customers/" target="_blank" title="Customer Login">Customer Login</a></li></ul></nav></div></div></header> <div class="page-viewport"><section class="slider-holder"><div class="flexslider slider wrapper" id="slider"><ul class="slides"><li><figure class="fig cover" style="background-image: url(/phpthumb/cache/uploads/home/brain/w675h280zc1q100/brain.jpg);"><img alt="" src="/phpthumb/cache/uploads/home/brain/w675h280zc1q100/brain.jpg"/></figure><div class="content"><h2>Deep Brain<br/><strong>Stimulator</strong></h2><p>Advanced electronic design for challenging active implantable medical device (AIMD)</p><a class="btn btn-icon" href="/medical-solutions/" title="More medical solutions"><i class="icon icon-more"></i>More medical solutions</a></div></li><li><figure class="fig cover" style="background-image: url(/phpthumb/cache/uploads/home/kindinauto_breed/w675h280zc1q100/kindinauto_breed.jpg);"><img alt="" src="/phpthumb/cache/uploads/home/kindinauto_breed/w675h280zc1q100/kindinauto_breed.jpg"/></figure><div class="content"><h2>Gabriel, Opel’s safety car key for child detection</h2><p>A heat stroke preventive device designed for Opel</p><a class="btn btn-icon" href="/consumer-solutions/" title="More consumer solutions"><i class="icon icon-more"></i>More consumer solutions</a></div></li><li><figure class="fig cover" style="background-image: url(/phpthumb/cache/uploads/home/4easys_moving_bus/w675h280zc1q100/4easys_moving_bus.jpg);"><img alt="" src="/phpthumb/cache/uploads/home/4easys_moving_bus/w675h280zc1q100/4easys_moving_bus.jpg"/></figure><div class="content"><h2> <strong>Green</strong> technology</h2><p>Energy storage and power delivery solutions for heavy duty applications</p><a class="btn btn-icon" href="/industrial-solutions/" title="More industrial solutions"><i class="icon icon-more"></i>More industrial solutions</a></div></li></ul><button class="slider-next" id="slider-next"><span class="square"><i class="icon icon-next"></i></span><div class="label-holder"><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/brain/w91h91zc1q100/brain.jpg"/><span class="label-title">Deep Brain<br/><strong>Stimulator</strong></span></span><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/kindinauto_breed/w91h91zc1q100/kindinauto_breed.jpg"/><span class="label-title">Gabriel, Opel’s safety car key for child detection</span></span><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/4easys_moving_bus/w91h91zc1q100/4easys_moving_bus.jpg"/><span class="label-title"> <strong>Green</strong> technology</span></span></div></button></div><button class="slider-prev" id="slider-prev"><span class="square"><i class="icon icon-prev"></i></span><div class="label-holder"><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/brain/w91h91zc1q100/brain.jpg"/><span class="label-title">Deep Brain<br/><strong>Stimulator</strong></span></span><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/kindinauto_breed/w91h91zc1q100/kindinauto_breed.jpg"/><span class="label-title">Gabriel, Opel’s safety car key for child detection</span></span><span class="label"><img alt="" src="/phpthumb/cache/uploads/home/4easys_moving_bus/w91h91zc1q100/4easys_moving_bus.jpg"/><span class="label-title"> <strong>Green</strong> technology</span></span></div></button></section><section class="text-home wrapper"><div class="wysi"><h3><em><strong>Zenso, Your Partner for Innovative Electronics</strong></em></h3>
<p>Zenso’s core competence exists in<strong> state-of-the-art electronic design</strong>. Zenso strengthens your project with a full-fledged turnkey solution or offers dedicated design services aligned with your project needs. Zenso manages for you the electronic design from idea till finalized and integrated product. Equally, Zenso delivers specific electronic design services aligned with the different steps of the customer’s development trajectory.</p>
<p>At the edge of new technologies Zenso wants to <strong>think along with the customer and help to create products that outperform the competition.</strong> As an independent electronic design house we guarantee the customer <strong>best of breed solutions</strong> complying with their specifications. Zenso is active in <a href="/medical-solutions/">medical</a>, <a href="/industrial-solutions/">industrial</a> and <a href="/consumer-solutions/">consumer electronics</a>.</p></div><aside class="green-fts"><h3>Zenso Core Business</h3><ul>
<li><a href="/methodology-expertise/">Feasibility study</a></li>
<li><a href="/methodology-expertise/">Electronic system design</a></li>
<li><a href="/methodology-expertise/">Software design</a></li>
<li><a href="/methodology-expertise/">Prototyping</a></li>
<li><a href="/methodology-expertise/">Test and validation</a></li>
<li><a href="/methodology-expertise/">System integration</a></li>
<li><a href="/methodology-expertise/">Production</a></li>
</ul></aside></section><section class="white-bar"><div class="wrapper clearfix"><h2>Let our <i>specialist</i><br/>competence<br/><strong>strengthen</strong><br/>yours.</h2><div class="content"><p>Zenso is an  <strong><a href="/company/12/quality/">ISO13485</a></strong> certified company for medical design.</p>
<p>Zenso is the first independent engineering house approved by <strong>IWT</strong> (Flemish institute for science and technology) to work with the <a href="/company/12/quality/" target="_blank">KMO portefeuille - Technology Survey</a><strong> </strong><em>(dutch: pijler technologieverkenning)</em>. For Zenso’s customer (incorporated in Flanders) this means that in a feansibility study executed by Zenso, they can be granted a subsidy.</p>
<p> </p><a class="left" href="http://zenso.be/company/12/quality/" target="_blank" title="KMO-portefeuille"><img alt="KMO-portefeuille" src="/img/logo-kmo.jpg"/></a><a class="right" href="http://zenso.be/company/12/quality/" target="_blank" title="ISO13485"><img alt="ISO13485" src="/img/logo-iso2.jpg"/></a></div></div></section><section class="blog-section wrapper"><h2>What's happening...</h2><a class="btn btn-icon" href="/news/"><i class="icon icon-more2"></i>View more articles</a><div class="blog-items clearfix" id="blogitems"><article class="blog-item grid-col-33"><figure class="fig"><a href="/news/12/come-meet-us-at-ieee-job-fair-student-branch-leuven/" title="Come meet us at IEEE job fair Student Branch Leuven"><img alt="" src="/phpthumb/cache/uploads/news/IEEE_stand_2018/w640h2640zc0q100/IEEE_stand_2018.jpg"/></a></figure><div class="content"><h3>Come meet us at IEEE job fair Student Branch Leuven</h3><small class="small">29-11-2019 - <a href="/news/category/1/internal-news/" title="Internal news">Internal news</a></small><p>Come and talk to our collegues about life at Zenso at the IEEE Job Fair Campus Bib Arenberg on December 4 from 13:30 to 18:30. </p><a class="btn btn-icon" href="/news/12/come-meet-us-at-ieee-job-fair-student-branch-leuven/" title="Come meet us at IEEE job fair Student Branch Leuven"><i class="icon icon-more3"></i>More details</a></div></article><article class="blog-item grid-col-33"><figure class="fig"><a href="/news/13/danish-breakthrough-in-treatment-of-epilepsy/" title="Danish breakthrough in treatment of epilepsy"><img alt="" src="/phpthumb/cache/uploads/news/uneeg_logger_box_small/w640h2640zc0q100/uneeg_logger_box_small.png"/></a></figure><div class="content"><h3>Danish breakthrough in treatment of epilepsy</h3><small class="small">10-04-2019 - <a href="/news/category/2/external-news/" title="External news">External news</a></small><p>Press Release - Uneeg has invented an implant that will help epilepsy patients avoid feared epileptic seizures and may also help save lives. The implant has just gained CE approval.
Zenso developped and manfuactures  part of this system.</p><a class="btn btn-icon" href="/news/13/danish-breakthrough-in-treatment-of-epilepsy/" title="Danish breakthrough in treatment of epilepsy"><i class="icon icon-more3"></i>More details</a></div></article><article class="blog-item grid-col-33"><figure class="fig"><a href="/news/9/kipando-op-innovatiebeurs/" title="Kipando op innovatiebeurs"><img alt="" src="/phpthumb/cache/uploads/news/kipando_fiets/w640h2640zc0q100/kipando_fiets.jpg"/></a></figure><div class="content"><h3>Kipando op innovatiebeurs</h3><small class="small">08-10-2015 - <a href="/news/category/2/external-news/" title="External news">External news</a></small><p>Fietsen is ‘in’, daar kan je niet naast kijken en de trend zal alleen maar groter worden in de (nabije) toekomst. Maar steeds minder voelen we de noodzaak om een eigen fiets te bezitten en groeit de noodzaak om de fiets in te zetten als mobiliteitsoplossing wanneer en waar de eindgebruiker het wenst… Kipando ontwikkelt hiervoor een fietsdeelsysteem van de volgende generatie. </p><a class="btn btn-icon" href="/news/9/kipando-op-innovatiebeurs/" title="Kipando op innovatiebeurs"><i class="icon icon-more3"></i>More details</a></div></article></div><a class="btn btn-icon bottom" href="/news/"><i class="icon icon-more2"></i>View more articles</a></section></div> <footer id="footer">
<div class="footer-links wrapper grid-row">
<div class="grid-col-275">
<h3>Zenso - Electronics</h3><p>Romeinse straat 18<br/> 3001 Heverlee<br/> Belgium<br/> +32 16 404547</p> </div>
<div class="grid-col-275">
<h3>Sales</h3><p><script type="text/javascript">
<!--
var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
var path = 'hr' + 'ef' + '=';
var addy63795 = '&#115;&#97;&#108;&#101;&#115;' + '&#64;';
addy63795 = addy63795 + '&#122;&#101;&#110;&#115;&#111;&#46;&#98;&#101;';
var addy_text63795 = '&#115;&#97;&#108;&#101;&#115;' + '&#64;' + '&#122;&#101;&#110;&#115;&#111;&#46;&#98;&#101;';

document.write('<a ' + path + '\'' + prefix + ':' + addy63795 + '\'>');
document.write(addy_text63795);
document.write('<\/a>');
//-->
 </script><script type="text/javascript">
<!--
document.write('<span style=\'display: none;\'>');
//-->
</script>This email address is being protected from spambots. You
need JavaScript enabled to view it.
<script type="text/javascript">
<!--
document.write('</');
document.write('span>');
//-->
</script><br/> +32 16 404547</p> </div>
<div class="grid-col-45">
<h3>You have a question about one of our products?</h3><p>Let our specialist competence strengthen yours.</p><a class="btn" href="/contact/" title="Feel free to contact us">Feel free to contact us</a> </div>
<div class="clear"></div>
</div>
<div class="footer-bottom">
<div class="wrapper clearfix">
<div class="copyright"><span>© Copyright Zenso Electronics - All rights reserved                     - </span><a href="/sitemap/" title="Sitemap">Sitemap</a> </div>
<a class="by" href="http://www.weblounge.be" target="_blank" title="Webdesign en grafisch ontwerp door Weblounge uit Brugge">webdesign weblounge</a>
</div>
</div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
<script src="/js/min/plugins-min.js"></script>
<script src="/js/min/main-min.js"></script>
<!-- Start of StatCounter Code for Dreamweaver -->
<script type="text/javascript">
var sc_project=10056840; 
var sc_invisible=1; 
var sc_security="4f3d87e1"; 
var sc_https=1; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a href="http://statcounter.com/shopify/" target="_blank" title="shopify site
analytics"><img alt="shopify site analytics" class="statcounter" src="http://c.statcounter.com/10056840/0/4f3d87e1/1/"/></a></div></noscript>
<!-- End of StatCounter Code for Dreamweaver --> </body>
</html>