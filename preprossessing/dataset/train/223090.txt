<!DOCTYPE html>
<!--[if IE]><![endif]--><!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]--><!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>The page you requested cannot be found!</title>
<base href="http://biotech-uae.com/"/>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet"/>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css?v=18" rel="stylesheet"/>
<script src="catalog/view/javascript/common.js?v=18" type="text/javascript"></script>
<link href="http://biotech-uae.com/image/catalog/cart.png" rel="icon"/>
</head>
<body class="error-not_found">
<nav id="top">
<div class="container">
<div class="nav pull-right" id="top-links">
<ul class="list-inline">
<li><a href="http://biotech-uae.com/index.php?route=information/contact"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md">+971 4 881 4857</span></li>
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="1111" title="My Account"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">My Account</span> <span class="caret"></span></a>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="http://biotech-uae.com/index.php?route=account/register">Register</a></li>
<li><a href="http://biotech-uae.com/index.php?route=account/login">Login</a></li>
</ul>
</li>
<li><a href="http://biotech-uae.com/index.php?route=account/wishlist" id="wishlist-total" title="Wish List (0)"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md">Wish List (0)</span></a></li>
<li><a href="http://biotech-uae.com/index.php?route=checkout/cart" title="Shopping Cart"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Shopping Cart</span></a></li>
<li><a href="http://biotech-uae.com/index.php?route=checkout/checkout" title="Checkout"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md">Checkout</span></a></li>
</ul>
</div>
</div>
</nav>
<header>
<div class="container">
<a class="hamburger" href="#" onclick="$('#sidemenu').toggleClass('active'); return false;"> </a>
<div id="logo">
<a href="http://biotech-uae.com/index.php?route=common/home"><img alt="Biotechnology International Co. L.L.C." class="img-responsive" src="http://biotech-uae.com/image/catalog/design/logo.png" title="Biotechnology International Co. L.L.C."/></a>
</div>
<div class="topcart">
<div class="btn-group btn-block" id="cart">
<button class="btn btn-inverse btn-block btn-lg dropdown-toggle" onclick="openCartModal('http://biotech-uae.com/index.php?route=checkout/cart');" type="button"><i class="fa fa-shopping-cart"></i> <span id="cart-total"><span>0</span> 0.00 AED</span></button>
<ul class="dropdown-menu pull-right">
<li>
<p class="text-center">Your shopping cart is empty!</p>
</li>
</ul>
</div>
<div class="modal fade" id="cart-modal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
      ...
    </div>
</div>
</div> </div>
<script>
				$(document).ready(function(){
					$('.addmenu a.search').not('done').on('click', function(){ 
						$(this).addClass('done');
						if($('#search').hasClass('showed'))
						{
							$('#search').removeClass('showed');
							return false;
						}
						else
						{
							$('#search').addClass('showed');
							$('#search .form-control').focus();
						}

						return false;
					});
					$('#search .form-control').on('blur', function(){ $('#search').removeClass('showed'); })
				});
			</script>
<div class="addmenu">
<a class="search" href="#">
<span class="hide-on-mobile">Search</span>
<div class="input-group" id="search">
<input class="form-control input-lg" name="search" placeholder="Search" type="text" value=""/>
<span class="input-group-btn">
<button class="btn btn-default btn-lg" type="button"><i class="fa fa-search"></i></button>
</span>
</div> </a>
<a class="login" href="http://biotech-uae.com/index.php?route=account/login"><span class="hide-on-mobile">Sales Login</span></a>
<ul>
<li><a href="/">Home</a></li>
<li><a href="/index.php?route=information/information&amp;information_id=4">About Us</a></li>
<!--<li><a href="/">Services</a></li>-->
<li><a href="/index.php?route=information/contact">Contact Us</a></li>
</ul>
</div>
<div class="clearfix"></div>
</div>
</header>
<nav id="sidemenu">
<a class="close" href="#" onclick="$('#sidemenu').toggleClass('active'); return false;">Закрыть</a>
<ul class="tree tree-cats">
<li><a href="http://biotech-uae.com/index.php?route=product/category&amp;path=59">Clean Up</a></li>
<li><a href="http://biotech-uae.com/index.php?route=product/category&amp;path=60">Super Touch</a></li>
<li><a href="http://biotech-uae.com/car-care">Clean Me</a></li>
</ul>
<ul class="tree">
<li><a href="/index.php?route=information/information&amp;information_id=4">About Us</a></li>
<!--<li><a href="/">Services</a></li>-->
<li><a href="/index.php?route=information/contact">Contact Us</a></li>
</ul>
</nav><div class="container">
<ul class="breadcrumb">
<li><a href="http://biotech-uae.com/index.php?route=common/home"><i class="fa fa-home"></i></a></li>
<li><a href="http://biotech-uae.com/index.php?route=error/not_found">The page you requested cannot be found!</a></li>
</ul>
<div class="row"> <div class="col-sm-12" id="content"> <h1>The page you requested cannot be found!</h1>
<p>The page you requested cannot be found.</p>
<div class="buttons hidebuttons clearfix">
<div class="pull-right"><a class="btn btn-primary" href="http://biotech-uae.com/index.php?route=common/home">Continue</a></div>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="footer-row">
<div class="footer-col">
<h5>Information</h5>
<ul class="list-unstyled">
<li><a href="http://biotech-uae.com/about_us">About Us</a></li>
<li><a href="http://biotech-uae.com/delivery">Delivery Information</a></li>
<li><a href="http://biotech-uae.com/privacy">Ordering Procedure</a></li>
<li><a href="http://biotech-uae.com/index.php?route=information/sitemap">Site Map</a></li>
<li><a href="http://biotech-uae.com/index.php?route=information/contact">Contact Us</a></li>
</ul>
</div>
<div class="footer-col">
<h5>Products</h5>
<ul class="list-unstyled">
<li><a href="http://biotech-uae.com/index.php?route=product/category&amp;path=59">Clean Up</a></li>
<li><a href="http://biotech-uae.com/index.php?route=product/category&amp;path=60">Super Touch</a></li>
<li><a href="http://biotech-uae.com/car-care">Clean Me</a></li>
</ul>
</div>
<div class="footer-col">
<!--<h5>Legal</h5>
				
				<ul class="list-unstyled">
					<li><a href="http://biotech-uae.com/index.php?route=information/contact">Contact Us</a></li>
					<li><a href="http://biotech-uae.com/index.php?route=account/return/add">Returns</a></li>
					<li><a href="http://biotech-uae.com/index.php?route=information/sitemap">Site Map</a></li>
				</ul>
				-->
</div>
<div class="footer-col footer-col-pre-last">
<h5>Contact Us</h5>
<ul class="list-unstyled">
<li>
						Jebel Ali Free Zone,<br/>
P.O. Box: 30339<br/>
Dubai, UAE.					</li>
</ul>
</div>
<div class="footer-col footer-col-last">
<h5> </h5>
<ul class="list-unstyled">
<li>Tel: +971 4 881 4857,</li>
<li>Fax: +971 4 881 4859</li>
<li><a href="mailto:sales@biotech-uae.com">sales@biotech-uae.com</a></li>
</ul>
</div>
</div>
<p class="copy">
			© 2017 BioTechnology International Company LLC. All rights reserved.
		</p>
</div>
</footer>
<div class="modal fade" id="banner-modal" role="dialog" tabindex="-1">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="text-align:center">
<a href="#">
<img src="/image/catalog/welcome.png"/>
</a>
</div>
</div>
</div>
<script>
	$(document).ready(function(){
		var img = new Image();
		img.onload = function(){
			$('#banner-modal .modal-dialog').css('top', (Math.floor(window.innerHeight - this.height) / 2)+'px').css('margin', '0 auto');
		};
		img.src = $('#banner-modal').find('img').attr('src');

		setTimeout(function(){
			$('#banner-modal').modal('show');
		}, 10000);
	});
</script>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
</body></html>