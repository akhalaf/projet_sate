<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>PNC Suppliers Pvt. Ltd.</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://pncsuppliers.com/feed/" rel="alternate" title="PNC Suppliers Pvt. Ltd. » Feed" type="application/rss+xml"/>
<link href="https://pncsuppliers.com/comments/feed/" rel="alternate" title="PNC Suppliers Pvt. Ltd. » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/pncsuppliers.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://pncsuppliers.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C700%2C900%7CRaleway%3A400%2C700%2C900%7Citalic&amp;subset=latin%2Clatin-ext" id="transportex-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/bootstrap.css?ver=5.2.9" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/supplier/style.css?ver=5.2.9" id="transportex-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/colors/default.css?ver=5.2.9" id="transportex_color-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/jquery.smartmenus.bootstrap.css?ver=5.2.9" id="smartmenus-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/owl.carousel.css?ver=5.2.9" id="carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/owl.transitions.css?ver=5.2.9" id="owl_transitions-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/font-awesome.css?ver=5.2.9" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/animate.css?ver=5.2.9" id="animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/magnific-popup.css?ver=5.2.9" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/css/bootstrap-progressbar.min.css?ver=5.2.9" id="boot-progressbar-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/transportex/style.css?ver=5.2.9" id="supplier-parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/supplier/style.css?ver=5.2.9" id="supplier-child-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://pncsuppliers.com/wp-content/themes/supplier/css/colors/default.css?ver=5.2.9" id="supplier-default-css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://pncsuppliers.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-content/themes/transportex/js/navigation.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-content/themes/transportex/js/bootstrap.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-content/themes/transportex/js/jquery.smartmenus.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-content/themes/transportex/js/jquery.smartmenus.bootstrap.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://pncsuppliers.com/wp-content/themes/transportex/js/owl.carousel.min.js?ver=5.2.9" type="text/javascript"></script>
<link href="https://pncsuppliers.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://pncsuppliers.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://pncsuppliers.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<link href="https://pncsuppliers.com/" rel="canonical"/>
<link href="https://pncsuppliers.com/" rel="shortlink"/>
<link href="https://pncsuppliers.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fpncsuppliers.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://pncsuppliers.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fpncsuppliers.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="https://pncsuppliers.com/wp-content/uploads/2019/09/PNC-Logo-150x150.jpg" rel="icon" sizes="32x32"/>
<link href="https://pncsuppliers.com/wp-content/uploads/2019/09/PNC-Logo.jpg" rel="icon" sizes="192x192"/>
<link href="https://pncsuppliers.com/wp-content/uploads/2019/09/PNC-Logo.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://pncsuppliers.com/wp-content/uploads/2019/09/PNC-Logo.jpg" name="msapplication-TileImage"/>
</head>
<body class="home page-template-default page page-id-214">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="wrapper">
<!--
Skip to content<div class="wrapper">
<header class="transportex-trhead">
	<!--==================== Header ====================-->
<div class="transportex-main-nav" style="display: none;">
<nav class="navbar navbar-default navbar-wp">
<div class="container">
<div class="navbar-header">
<!-- Logo -->
<a class="navbar-brand" href="https://pncsuppliers.com/">PNC Suppliers Pvt. Ltd.			<br/>
<span class="site-description"></span>
</a>
<!-- Logo -->
<button class="navbar-toggle collapsed" data-target="#wp-navbar" data-toggle="collapse" type="button"> <span class="sr-only">
			Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
</div>
<!-- /navbar-toggle -->
<!-- Navigation -->
<div class="collapse navbar-collapse" id="wp-navbar">
<ul class="nav navbar-nav navbar-right" id="menu-navigation"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-214 current_page_item menu-item-216 active" id="menu-item-216"><a href="https://pncsuppliers.com/" title="Home">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 dropdown" id="menu-item-93"><a href="https://pncsuppliers.com/about-us/" title="About Us">About Us <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123" id="menu-item-123"><a href="https://pncsuppliers.com/about-us/ceo-message/" title="CEO Message">CEO Message</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-87 dropdown" id="menu-item-87"><a href="https://pncsuppliers.com/projects/" title="Our Clients">Our Clients <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189" id="menu-item-189"><a href="https://pncsuppliers.com/projects/zte/" title="Zhongxing Telecom Pakistan (ZTE)">Zhongxing Telecom Pakistan (ZTE)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-128" id="menu-item-128"><a href="https://pncsuppliers.com/projects/miniso/" title="Miniso Pakistan">Miniso Pakistan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-254" id="menu-item-254"><a href="https://pncsuppliers.com/sungrow-power-china/" title="Sungrow Power China">Sungrow Power China</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136" id="menu-item-136"><a href="https://pncsuppliers.com/projects/cnb/" title="CNB Mining Corporation">CNB Mining Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166" id="menu-item-166"><a href="https://pncsuppliers.com/projects/xinjiang-beixin-road-bridge/" title="Xinjiang Beixin Road &amp; Bridge">Xinjiang Beixin Road &amp; Bridge</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222" id="menu-item-222"><a href="https://pncsuppliers.com/china-railway-first-group/" title="China Railway First Group">China Railway First Group</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-231" id="menu-item-231"><a href="https://pncsuppliers.com/china-railway-corporation/" title="China Railway Corporation">China Railway Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-142" id="menu-item-142"><a href="https://pncsuppliers.com/projects/cccc/" title="China Communications Construction">China Communications Construction</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245" id="menu-item-245"><a href="https://pncsuppliers.com/sichuan-sarwar-silian-chongqing-luyang-jv/" title="SICHUAN SARWAR SILIAN CHONGQING LUYANG JV">SICHUAN SARWAR SILIAN CHONGQING LUYANG JV</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147" id="menu-item-147"><a href="https://pncsuppliers.com/projects/dahua/" title="Dahua Technology">Dahua Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152" id="menu-item-152"><a href="https://pncsuppliers.com/projects/cht/" title="China Huarun Technology">China Huarun Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255" id="menu-item-255"><a href="https://pncsuppliers.com/sino-hydro-corporation/" title="Sino Hydro Corporation">Sino Hydro Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157" id="menu-item-157"><a href="https://pncsuppliers.com/projects/glsw/" title="Liang Steel Works">Liang Steel Works</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-173" id="menu-item-173"><a href="https://pncsuppliers.com/hbmtc/" title="Hengze Building Material">Hengze Building Material</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-179" id="menu-item-179"><a href="https://pncsuppliers.com/projects/jhme/" title="JIUZHOU HENGTONG MACHINERY">JIUZHOU HENGTONG MACHINERY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184" id="menu-item-184"><a href="https://pncsuppliers.com/projects/kstar/" title="Shenzhen Kstar Science and Technology">Shenzhen Kstar Science and Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193" id="menu-item-193"><a href="https://pncsuppliers.com/projects/tansu/" title="Tansu International Trading">Tansu International Trading</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199" id="menu-item-199"><a href="https://pncsuppliers.com/projects/trustell/" title="TRUSTELL ENTERPRISES">TRUSTELL ENTERPRISES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-204" id="menu-item-204"><a href="https://pncsuppliers.com/projects/credo-stat-pakistan/" title="Credo Stat Pakistan">Credo Stat Pakistan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-252" id="menu-item-252"><a href="https://pncsuppliers.com/china-gezhouba-group-co-ltd/" title="CHINA GEZHOUBA GROUP CO LTD">CHINA GEZHOUBA GROUP CO LTD</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114" id="menu-item-114"><a href="https://pncsuppliers.com/getaquote/" title="Get a Quote">Get a Quote</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98" id="menu-item-98"><a href="https://pncsuppliers.com/contact-us/" title="Contact Us">Contact Us</a></li>
</ul> </div>
<!-- /Navigation -->
</div>
</nav>
</div>
<!-- #masthead -->
<header class="transportex-headwidget">
<!--==================== TOP BAR ====================-->
<div class="transportex-head-detail hidden-xs hidden-sm" style="display: none;">
<div class="container">
<div class="row">
<div class="col-md-6 col-xs-12 col-sm-6">
<ul class="info-left">
<li><a><i class="fa fa-clock-o "></i>Open-Hours:10 am to 7pm</a></li>
<li><a href="mailto:info@themeansar.com" title="Mail Me"><i class="fa fa-envelope"></i> info@themeansar.com</a></li>
</ul>
</div>
<div class="col-md-6 col-xs-12">
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="transportex-nav-widget-area">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-4 text-center-xs">
<div class="navbar-header">
<!-- Logo -->
<a class="navbar-brand" href="https://pncsuppliers.com/">PNC Suppliers Pvt. Ltd.      <br/>
<span class="site-description"></span>
</a>
<!-- Logo -->
</div>
</div>
<div class="col-md-9 col-sm-8">
<div class="header-widget">
<div class="col-md-3 col-sm-3 col-xs-6 hidden-sm hidden-xs">
<div class="transportex-header-box wow animated flipInX">
<div class="transportex-header-box-icon">
<i class="fa fa-phone"></i> </div>
<div class="transportex-header-box-info">
<h4>Call Us:</h4> <p>+92 51 8350281</p>
</div>
</div>
</div>
<div class="col-md-3 col-sm-3 col-xs-6 hidden-sm hidden-xs">
<div class="transportex-header-box">
<div class="transportex-header-box-icon">
<i class="fa fa-envelope-o"></i> </div>
<div class="transportex-header-box-info">
<h4>Email Us:</h4> <p>info@pncsuppliers.com</p>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-6 hidden-sm hidden-xs">
<div class="transportex-header-box">
<div class="transportex-header-box-icon">
<i class="fa fa-clock-o"></i> </div>
<div class="transportex-header-box-info">
<h4>9:00 AM - 5:00 PM</h4> <p>Monday to Saturday</p>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 hidden-sm hidden-xs">
<div class="transportex-header-box wow animated flipInX text-right">
<a class="btn btn-theme" href="https://pncsuppliers.com/getaquote/" target="_blank">Get a Quote</a>
</div>
</div>
</div>
</div>
</div>
</div></div>
<div class="container">
<div class="transportex-menu-full">
<nav class="navbar navbar-default navbar-static-top navbar-wp">
<!-- navbar-toggle -->
<button class="navbar-toggle collapsed" data-target="#navbar-wp" data-toggle="collapse" type="button"> <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
<!-- /navbar-toggle -->
<!-- Navigation -->
<div class="collapse navbar-collapse" id="navbar-wp">
<ul class="nav navbar-nav" id="menu-navigation-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-214 current_page_item menu-item-216 active"><a href="https://pncsuppliers.com/" title="Home">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-93 dropdown"><a href="https://pncsuppliers.com/about-us/" title="About Us">About Us <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123"><a href="https://pncsuppliers.com/about-us/ceo-message/" title="CEO Message">CEO Message</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-87 dropdown"><a href="https://pncsuppliers.com/projects/" title="Our Clients">Our Clients <span class="caret"></span></a>
<ul class=" dropdown-menu" role="menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189"><a href="https://pncsuppliers.com/projects/zte/" title="Zhongxing Telecom Pakistan (ZTE)">Zhongxing Telecom Pakistan (ZTE)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-128"><a href="https://pncsuppliers.com/projects/miniso/" title="Miniso Pakistan">Miniso Pakistan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-254"><a href="https://pncsuppliers.com/sungrow-power-china/" title="Sungrow Power China">Sungrow Power China</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136"><a href="https://pncsuppliers.com/projects/cnb/" title="CNB Mining Corporation">CNB Mining Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="https://pncsuppliers.com/projects/xinjiang-beixin-road-bridge/" title="Xinjiang Beixin Road &amp; Bridge">Xinjiang Beixin Road &amp; Bridge</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222"><a href="https://pncsuppliers.com/china-railway-first-group/" title="China Railway First Group">China Railway First Group</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-231"><a href="https://pncsuppliers.com/china-railway-corporation/" title="China Railway Corporation">China Railway Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-142"><a href="https://pncsuppliers.com/projects/cccc/" title="China Communications Construction">China Communications Construction</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-245"><a href="https://pncsuppliers.com/sichuan-sarwar-silian-chongqing-luyang-jv/" title="SICHUAN SARWAR SILIAN CHONGQING LUYANG JV">SICHUAN SARWAR SILIAN CHONGQING LUYANG JV</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147"><a href="https://pncsuppliers.com/projects/dahua/" title="Dahua Technology">Dahua Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152"><a href="https://pncsuppliers.com/projects/cht/" title="China Huarun Technology">China Huarun Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255"><a href="https://pncsuppliers.com/sino-hydro-corporation/" title="Sino Hydro Corporation">Sino Hydro Corporation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-157"><a href="https://pncsuppliers.com/projects/glsw/" title="Liang Steel Works">Liang Steel Works</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-173"><a href="https://pncsuppliers.com/hbmtc/" title="Hengze Building Material">Hengze Building Material</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-179"><a href="https://pncsuppliers.com/projects/jhme/" title="JIUZHOU HENGTONG MACHINERY">JIUZHOU HENGTONG MACHINERY</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184"><a href="https://pncsuppliers.com/projects/kstar/" title="Shenzhen Kstar Science and Technology">Shenzhen Kstar Science and Technology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="https://pncsuppliers.com/projects/tansu/" title="Tansu International Trading">Tansu International Trading</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199"><a href="https://pncsuppliers.com/projects/trustell/" title="TRUSTELL ENTERPRISES">TRUSTELL ENTERPRISES</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-204"><a href="https://pncsuppliers.com/projects/credo-stat-pakistan/" title="Credo Stat Pakistan">Credo Stat Pakistan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-252"><a href="https://pncsuppliers.com/china-gezhouba-group-co-ltd/" title="CHINA GEZHOUBA GROUP CO LTD">CHINA GEZHOUBA GROUP CO LTD</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114"><a href="https://pncsuppliers.com/getaquote/" title="Get a Quote">Get a Quote</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98"><a href="https://pncsuppliers.com/contact-us/" title="Contact Us">Contact Us</a></li>
</ul> <!-- Right nav -->
<!-- /Right nav -->
</div>
<!-- /Navigation -->
</nav>
</div>
</div>
</header>
<!-- #masthead --> <div class="transportex-breadcrumb-section" style='background: url("https://pncsuppliers.com/wp-content/uploads/2019/10/cropped-rtx1hljy-e1527285080340.jpg") #50b9ce;'>
<div class="overlay">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="transportex-breadcrumb-title">
<h1>Home</h1>
</div>
<ul class="transportex-page-breadcrumb">
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div><main id="content">
<div class="container">
<div class="row">
<!-- Blog Area -->
<div class="col-md-9">
<h3><strong>Pakistan</strong></h3>
<p>Pakistan is a country situated in southern part of Asia, in its north lies China the Central Asian states and the Russian federation, to the west is Afghanistan and Iran while to th<img alt="" class="wp-image-266 alignright" height="251" sizes="(max-width: 236px) 100vw, 236px" src="https://pncsuppliers.com/wp-content/uploads/2019/10/Pakistan-Map-282x300.png" srcset="https://pncsuppliers.com/wp-content/uploads/2019/10/Pakistan-Map-282x300.png 282w, https://pncsuppliers.com/wp-content/uploads/2019/10/Pakistan-Map.png 293w" width="236"/>e east is India. Pakistan has four provinces namely Punjab, Baluchistan, KP and Sindh. Islamabad is the capital of Pakistan, and five districts of the northern areas are administered directly from the Capital. Officially these districts are called Federally Administered Northern Areas<br/>
(FANA). Pakistan came into being as the result of the partition of British India in 1947.</p>
<p>Pakistan is one of the diversified countries of the world in terms of its culture, landscape, and climate. To the north of Pakistan is the highest mountain system of world, having its harsh climate. To the west are the fertile plains of Indus valley. While to the western and southern part exist the steaming deserts. Within its borders situate the Arabian Sea, best root of Asia; this sea moderates the climate of its southern parts.</p>
<p>The area of Pakistan is 881,913 km² while its population is around 212.7 million. Pakistan is an agricultural country. Its major exports include the agricultural products like cotton, sugar, rice and textile. The literacy rate of the population is 62.3% as of 2018. The rate of male literacy is 72.5% while the rate of female literacy is 51.8%. Literacy rates vary by region and particularly by sex; as one example, tribal areas female literacy is 9.5%,<sup class="reference" id="cite_ref-fata.gov.pk_489-0">[486]</sup> while Azad Jammu &amp; Kashmir has a literacy rate of 74%. <sup class="reference" id="cite_ref-490"></sup>With the advent of computer literacy in 1995, the government launched a nationwide initiative in 1998 with the aim of eradicating illiteracy and providing a basic education to all children.<sup class="reference" id="cite_ref-491"></sup> Through various educational reforms, by 2015 the Ministry of Education expected to attain 100% enrollment levels among children of primary school age and a literacy rate of ~86% among people aged over 10. <sup class="reference" id="cite_ref-moe4_492-0"></sup>Pakistan is currently spending 2.2 percent of its GDP on education; <sup class="reference" id="cite_ref-493"></sup>which according to the Institute of Social and Policy Sciences is one of the lowest in South Asia.<sup class="reference" id="cite_ref-494"></sup> Pakistan’s cultural heritage is very rich it has inherited the rich cultures of the thousand of years old Indus and Ghandara civilizations.</p>
<p> </p>
<h3><img alt="" class="wp-image-265 alignleft" height="77" sizes="(max-width: 75px) 100vw, 75px" src="https://pncsuppliers.com/wp-content/uploads/2019/10/Profile-PNC-SUPPLIERS-PVT-LTD-295x300.jpg" srcset="https://pncsuppliers.com/wp-content/uploads/2019/10/Profile-PNC-SUPPLIERS-PVT-LTD-295x300.jpg 295w, https://pncsuppliers.com/wp-content/uploads/2019/10/Profile-PNC-SUPPLIERS-PVT-LTD.jpg 452w" width="75"/><strong>PNC Suppliers Pvt. Ltd </strong></h3>
<p>Ever since its establishment in 2016, the PNC Suppliers Pvt. Ltd. has strives to maintain high ethical standards, products and services that provide value to our customers, and become a company trusted and chosen by all of its stakeholders, including customers, under the motto of “Better Products, Better Services.”</p>
<p>The PNC Suppliers Pvt. Ltd. is currently focusing on leveraging its strengths in Supply and Service Provision across Pakistan on all Major Projects to increasing the sophistication of infrastructure systems and services indispensable to society. Through these business activities, PNC Suppliers Pvt. Ltd. remains committed to collaborating closely with each and every one of its stakeholders to create an “society friendly to humans and the earth” based on value that helps ensure safety, security, efficiency and equality, enabling people to live more abundant lives.</p>
<p>Going forward, we will aim to establish a corporate culture in which every member of the PNC Suppliers Pvt. Ltd. possesses the spirit of self-help and takes initiative in carefully assessing societal and customer expectations, considering what can be done to meet these expectations, taking appropriate action, and providing value, as well as to build stronger relationships of trust with all of our stakeholders.</p>
<div class="col-md-9">
<div class="comments-area" id="comments">
</div><!-- #comments -->
</div> <!-- /Blog Area -->
</div>
<!--Sidebar Area-->
<aside class="col-md-3">
<aside class="widget-area" id="secondary" role="complementary">
<div class="transportex-sidebar" id="sidebar-right">
<div class="transportex-widget widget_search" id="search-2"><form action="https://pncsuppliers.com/" id="searchform" method="get">
<div class="form-group">
<input class="form-control" id="s" name="s" placeholder="Type to search" required="" type="text"/>
</div>
<button class="btn" type="submit">Search</button>
</form></div> </div>
</aside><!-- #secondary -->
</aside>
<!--Sidebar Area-->
</div>
</div>
</main></div>
<!--==================== transportex-FOOTER AREA ====================-->
<footer>
<div class="overlay">
<div class="transportex-footer-copyright">
<div class="container">
<div class="row">
<div class="col-md-6">
<p>© 2021 PNC Suppliers Pvt. Ltd. | Theme by <a href="https://www.themeansar.com" rel="designer">Themeansar</a></p>
</div>
<div class="col-md-6 text-right text-xs">
<ul class="transportex-social">
<li><span class="icon-soci"> <a href=""><i class="fa fa-facebook"></i></a></span></li>
<li><span class="icon-soci"><a href=""><i class="fa fa-twitter"></i></a></span></li>
<li><span class="icon-soci"><a href=""><i class="fa fa-linkedin"></i></a></span></li>
<li><span class="icon-soci"><a href=""><i class="fa fa-google-plus"></i></a></span></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!--/overlay-->
</footer>
<!--==================== feature-product ====================-->
<script type="text/javascript">
(function($) {
  "use strict";
function homeslider() {
    $("#ta-slider").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        navigation: true, // Show next and prev buttons
        slideSpeed: 200,
        pagination: true,
        paginationSpeed: 400,
        singleItem: true,
        autoPlay: true,
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ]
    });
}
homeslider();
})(jQuery);
</script>
<script src="https://pncsuppliers.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
</body>
</html>