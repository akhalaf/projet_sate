<!DOCTYPE html>
<html lang="en">
<head>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Regis" name="description"/>
<meta content="cloud-based,event management,platform,system,events" name="keywords"/>
<meta content="artegis admin" name="author"/>
<meta content="notranslate" name="google"/>
<title>cloud-based event management platform</title>
<!-- Bootstrap core CSS -->
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/bootstrap/css/regisSpecifics.css" rel="stylesheet"/>
<link href="/bootstrap/css/regisCheckBoxAndRadio.css" rel="stylesheet"/>
<!-- <link href="/bootstrap/css/fontawesome.css" rel="stylesheet"> -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Custom styles for this template -->
<link href="/urlhost/templates/layout/1425/public/stylesheet.css" rel="stylesheet"/>
<link href="/templates/layout/default/public/modal.css" rel="stylesheet" type="text/css"/>
<link href="/urlhost/templates/layout/1425/public/registration.css" rel="stylesheet" type="text/css"/>
<script src="/utils/js/modal.js"></script>
</head>
<body class="PageBody">
<div class="container-fluid regisBanner">
<div class="row">
<p><img alt="artegis" class="img-responsive" onclick="location='https://www.artegis.com/lz/CustomContent?custom=1391&amp;page=25875&amp;pageTop=41252&amp;pageBottom=27414&amp;Application=artegis&amp;Directory=tv&amp;protocol=http&amp;navigation=0'" src="/urlhost/artegis/customers/1391/.lwtemplates/layout/default/public/artegis_banner_2020_X_s.jpeg"/></p>
</div> <!-- end row-->
</div> <!-- end container-->
<div class="container-fluid regisFullContent">
<div class="row regisPageContent">
<div class="col-sm-12 regisMainContent">
<div class="row">
<div class="col-sm-6">
<h1>Event managment: Regis</h1>
<p class="alternate" style="text-align: justify;">Whatever your event: product launch, seminar, exhibition, scientific conference, gala dinner, incentive or VIP event from set-up to managing of data the <a href="/lz/CustomContent?T=1&amp;custom=1391&amp;navid=1087">online event management system Regis</a> is with you every step of the way. The range of <a href="/lz/CustomContent?T=1&amp;custom=1391&amp;navid=4988">features</a> on the online event, registration and abstract management system Regis is tailored to meet your specific needs!</p>
<p class="alternate" style="text-align: justify;"><a href="/lz/CustomContent?T=1&amp;custom=1391&amp;navid=1089">Artegis</a> has provided event managers with its high-tech and powerful online solution Regis since 2001. Our mission is to deliver solid event management expertise with the online system Regis for worldwide use. Our advantage you can<a href="/lz/CustomContent?T=1&amp;custom=1391&amp;navid=1087"> build with Regis events</a> in a few minutes and create the most sophisticated event website and communication platform for you and your customers. Regis, your personalized multi-task online system. <a href="https://www.artegis.com/lz/Registration?formName=customerFormTemplate12406&amp;PageContext=PUBLIC&amp;formName=regDemoCustomersPublicForm&amp;custom=1391&amp;pageBottom=27414&amp;pageRight=26002&amp;pageTitle=44164&amp;pageTop=41252" target="_blank">Get started with Regis and sign up for a free trial.</a></p>
<p><img alt="" class="img-responsive" src="/urlhost/artegis/customers/1391/.lwtemplates/layout/default/public/REG_HUB_09_modifie.jpg"/><br/>
 </p>
</div>
<div class="col-sm-6">
<h1>Direct sales: Directissime</h1>
<p>OUR CONCEPT By minimizing the middlemen and offering the shortest circuit, we hope to bring you closer to the producer. This way, a relationship of trust is established and you know exactly where the goods come from. At Directissime, we take care of all the IT infrastructure necessary for such an operation to run smoothly. Setting up supplier websites, invoicing, means of payment, stock management, delivery planning and optimisation are among the services we provide.</p>
<p>For more information visit <a href="http://www.directissime.ch">www.directissime.ch</a></p>
<p> </p>
<p><img class="img-responsive" src="/urlhost/artegis/customers/1391/.lwtemplates/layout/default/system/images/83227631_xl.jpg"/></p>
</div>
</div>
</div> <!-- end col-->
</div> <!-- end row-->
</div> <!-- end container-->
<div class="container-fluid regisFooterContent">
<div class="row">
<div class="col-xs-12">
<p class="unspaced"><small>Copyright © 2001 - 2021 Artegis. All rights reserved. Artegis, Chemin du Vallon 18, CH-1260 Nyon.</small><small><span style="color: rgb(51, 153, 255);">  </span></small><br/>
<small><span style="color: rgb(51, 153, 255);"><a href="mailto:info@artegis.com">contact</a>  <a href="https://asp.artegis.com/urlhost/artegis/sitedocs/privacy.html">privacy policy</a></span></small></p>
<p style="padding:0px;border:0px;margin:0px;color:transparent;background-color:transparent;height:4px;font-size:4px">Copyright © 2001 - 2021 Artegis. All rights reserved. Artegis, Rue du Marché 5, CH-1260 Nyon. <a href="www.artegis.com" style="padding:0px;border:0px;margin:0px;color:transparent;background-color:transparent;height:4px;font-size:4px">event management system</a></p>
</div> <!-- end col-->
</div> <!-- end row-->
</div> <!-- end container-->
<!-- Bootstrap core JavaScript ================================================== -->
<p style="padding:0px;border:0px;margin:0px;color:transparent;background-color:transparent;height:0px;font-size:4px">Copyright © 2001 - 2021 Artegis. All rights reserved. Artegis,
  Ch. du Vallon, 18, CH-1260 Nyon. <a href="www.artegis.com" style="padding:0px;border:0px;margin:0px;color:transparent;background-color:transparent;height:0px;font-size:4px">event management system</a></p>
<!-- Placed at the end of the document so the pages load faster -->
<script src="/bootstrap/js/jquery.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/bootstrap/js/docs.min.js"></script>
<script src="/bootstrap/js/bootstrap3-typeahead.min.js"></script>
<script src="/bootstrap/js/regisSpecifics.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
