<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "46303",
      cRay: "6108cb117c7f194f",
      cHash: "ba46b40a6ce3436",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuc2lrYXlldHZhci5jb20vdmFraWZiYW5rL2ZpbmFucy9iYW5rYWNpbGlrL3N1YmUteWF0aXJpbS1pc2xlbWxlcmkvJTA5JTBB",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "FgUUJQYtJ/4QrYUiaJqllWTj0z3pxm9gdX8sc9shFNybIdoRAgMlR0izFo73oEZTsueOBhm3aQExjsQERy6IBBLTiAUcew7i+pCqf8NK9WA8qnazygkcgOLynxAFKsDX8UlvToX0GXszJBzh3c6qH48gCUQ4AfNXARUkuIzuJPB4DLvnKGiMYV2bBvLok5JMQJgHHZpFMfd6Z8ojt3pqfTmlU/WfW0Q6BdaNKbInN6riAqSjmpRXJeVKdb+cC1de1BqpsyrsIEuAj2OzLdgBzmg2e7B40HBLqJl/WnfUz6eM1n/IEui/+bDGtPwKvd1GpcILud4LC5/hEd5WOjM1xs2iiOvdUTX3EX3YP38sEdt9fMb+QydZvIc9CIoxK3h34kW4Xt4MYwRiApeqktcVF4Hlagp4eRmjXaQ8KoQFNF86TbbryX+ZHxhRUo9YWyJpdnkcqRLzt9QgY6G4AhAzPVCc1cgzHdeUTOW5SuF+sqx7z7eqm1Eg9hhkl8GxyVgCvH3sRFkFd9c4hPRfdJmXOXfcupcGcy3z/s0T04Q/fbNBawAwI2i0MLM4MReg78X1MMU6x9ZxCqgbzUTBak/8kmKGbkyQb5epk+zD4iwR/BHbbX88UiVX50v+9wF1cbDjgyRehjJcwoUCvbrsBgoxPRg0idLRgsaHTWBc/3vgLaoYKiWrimxVyVDXv8JNG4eWfKCWBYwXtEVzFE4dsuzmGIKTgbATXCYAOWNdlAFwwpyCGO4/G71syvIRSzEjUPuBKw4JPbuOji4JmZVSWXcwsA==",
        t: "MTYxMDQ3NDU5Ni4wOTkwMDA=",
        m: "5ONyU5Mav20JezxmUj9aqmfknM+LB+KH2WH/l8qZWwk=",
        i1: "2z6F4yUoiFueF0ODqO8hww==",
        i2: "5CfMgkYH4y5YKvseWDks4A==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "dvSwrQ0scL2QT9ciKcklD5z/iW0piS9WYyk0rXNJFzo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.sikayetvar.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/vakifbank/finans/bankacilik/sube-yatirim-islemleri/%09%0A?__cf_chl_captcha_tk__=c92f9bbef1dc4a04cac7552d689eaf55947c5c4d-1610474596-0-AYqMVVPix9Gmhcz-ZrHZr7FgqJX_xzDT4l2MqOGAGbMaF-xQTl8Ry9LhotVPInC2Q7-BM_UjakWmbakjYwW-fVFb4HJEth0KQHdq63zpXa2adlOxRCEx4eXXxoBXhO-WySrIzfuhanSLJ3QT_AORTVG8IYONSjg0x9d1_3fQgdLZTWNSmf61E-sMS-G2fVL8qy6aB157j1K3GdF14ZV1debmuHOppmASC8JA2ib4i7GKMiLp_RMK24RCei_ocBu7er6vjzjONGIv8S73O_jZu75zHOoibdjODYq5JYm7T-dZfEQDzmOb1GmHlBygNWLpYALLvxyrgDt1hcpafGCfYBm_RSyjYd-FgGzFfVFQsIXaP1ezWLVjiKhtSkDpE0E9P3qfcuFGlH5H5eBIjkyYapo-Ui09pb2w1uFx-EuDeDHzfrBbLP_VxkfvgOjOQzd5nBiE3lFNfQR6WzkEgTqdR8H8LV5w-ZDH4uUzhrc0LsnH8jC1d-Ok1typMcUbpydfhmhUA3OWTEPdvsH5Y1j1qa4UpZa6genLX5aM6JNhde7GswwH3WyktFnsZ49wbdF_JuvtUEQkqcGK3c8sLD6D6rvM1kQUNvObzUbJYtVtP9L54DZB9Atv3Z1l_Zpp6s0Myg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="4251aab78be8435d7542c2b6c1e03d9b0a474668-1610474596-0-AQjqsxHoNBt/iRJtfrBGr9/xTo7oz6QVwJ8cC+h6NUsu02g5fqvwuo7c4dq6hlDjoEfOK8/4a44i4pyLjs6NT4sCsmoMUDf3qzvt3HQSeN8trcIY+I/rG8Oa/pgMsLTee6HX5lc+28kzxHsovpYd2yf8iTDEQ8AMA+DNOZGNCS3yra1EeAd8Y9cj4NUzYLvYyCWhHI0ToJQOifrEmLuy38M6y8K6zXniFYN2Dq4QXLfRBBWkLEsnTUFbnFwz7QjbDnuP/kkqXMqyVbZYqZgipKeHh7E7Vdxca+P9AM/CRfjGkAaCTn+VvfpE6RrXpH8XkROApScMqnuVlo1gKOfd3k0iI0Y44bJhUlP6FolcNeuypgKwUB13CLN5b9K+rGrSdsu39ClPdOQf4lagvtPIuyAnZlCo8MiZndYKv3UMQQ6yDezZrZ9hkRYspPOuOxTLYoH06+MnfO89hg71wYDNXdY+s9pSOtvFztGkWEO0YYrA/VP+cVFL23FPhDmeyFML91OBjL8mY5853Q6lgr1QqPnwrPpCS18vP+fYF+EvzuHRuhhnhIDFvCZvc7ZsirkMD+vNCrIMr7+bQ3n1ynKJbrNP44LVKiSPjga8NCJpnDrro7M2wx+Jy0igzAMTOgSDTMkaAnb+KU6H+u/KpJuuMnDVYKPcuza78UGOjwGjesj5c8OhWI0iw78gB3Mo8h1NdWgpNgyGjCRWi6Ubp6hbOthkHZOh5gT0MN8lXKbGnTexnj9vhRi9czzMDx3MdynSR0WOf9zy1TdfNxjYYnTFdiyWWkivRgCo+2yB6IniGmINvVUsi//Lt6Zgj9SW1rcuA/UaDdn02/fccOjh6eciO3WQZ6JLmvvMXQRmeOr9jNNhdtN70fbS9Ld2UaljI3kSwFR4HQT6+RIZS85chbyXCMGYKDPbqzHhvtleJFioKot7SzdGqF2skOBH5jASY7RksoqC0Z0ilK/qSv/KZLQjA8Z1njs5OdEVoH1akj0GQ61AM0CI2XjkF0Eo5krtcHkqvOBMpwEUzcoIoTkuSXIyk12YoDwm3Rt46Aaf1e9pP5CT6kFW29ADgJNzr2871Q/wGY3Dc6mGx3WTAv4RF6GLymSmF8tF/50rtTVzSSjg0Ft3UaUY7OTsiiKJciGqGhv4TPOjb2v3USbgMqKTsHVGqcrJkQVJzQiDDv65uUP99IwQTK1wFlBPnx96ZW805peo4miF/AE4SDY1k1A0autpPhIVd5VGgkzP2g+PsCYT9o68DDzxebppnAp1sFZYIjGAa89SlYRz41iNg8HkM3+/x5KEcIKF5uz1ssvqtbJHiCB6b7ylDG+VV4cXJN0+7UKPKQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="74758507aa78125618578c3a9038dd99"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6108cb117c7f194f')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6108cb117c7f194f</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
