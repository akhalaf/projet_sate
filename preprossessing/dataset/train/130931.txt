<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TM62L9R');</script>
<!-- End Google Tag Manager -->
<title>Anxieties.com</title>
<meta content="initial-scale=1.0,width=device-width" name="viewport"/>
<!--<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0" id="viewport-meta"/>-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="anxiety attacks, anxieties, Anxiety Disorders Clinic, Anxiety Disorders Treatment Center, anxiety disorder, anxiety symptoms, anxiety medication, anxiety depression, anxiety attack symptom, anxiety treatment, anxiety drugs, body dysmorphic disorder, body dysmorphia, Cary NC, Chapel Hill NC, childhood sexual abuse, compulsions, Dr. Julie Pike, Dr. Vito Guerra, Dr. Reid Wilson, Durham NC, fear of flying, flying phobia, generalized anxiety disorder, health worries, help, hoarding, hoarders, hypochondriasis, OCD, obsessions, obsessive-compulsive disorder, obsessive compulsive disorder, panic attacks, panic, panic medication, phobia, post-traumatic stress disorder, posttraumatic stress disorder, PTSD, Raleigh NC, social anxiety disorder, social anxieties, social anxiety disorder, social phobia, tourette syndrome, trauma, trauma treatment, trichotillomania, worry, worries" name="keywords"/>
<meta content="Anxiety Disorders Treatment Center" name="publisher"/>
<meta content="USA, global" name="distribution"/>
<meta content="en" name="language"/>
<meta content="index, follow" name="robots"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/css/jScrollPane/jquery.jscrollpane.ver1598975311.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/layout.ver1602743037.css" media="" rel="stylesheet" type="text/css"/>
<link href="/css/messages.ver1598975311.css" media="" rel="stylesheet" type="text/css"/>
<link href="/css/main.ver1599218645.css" media="" rel="stylesheet" type="text/css"/>
<link href="/css/text.ver1599210879.css" media="" rel="stylesheet" type="text/css"/>
<link href="/css/input.ver1600435963.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="/sfJqueryReloadedPlugin/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/js/main.ver1599224053.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.8.18.custom.min.ver1598975311.js" type="text/javascript"></script>
<script src="/js/jScrollPane/jquery.jscrollpane.min.ver1599131539.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){

		searchOptionDisplay();
		$(window).resize(function(){
			searchOptionDisplay();
		});
	});
	function searchOptionDisplay()
	{
		var w = window.innerWidth;
		if(w <= 640 ) //mob version only
		{
			if($("#search_options").length > 0 && $("#search_option_container").length < 1) $("#tabs").before("<div id='search_option_container'>"+$("#search_options").html()+"</div>");
			else $("#search_option_container").show();
		}
		else if($("#search_option_container").length > 0) $("#search_option_container").hide();

	}
</script>
<!-- Start Visual Website Optimizer Asynchronous Code -->
<!--<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=359738,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
/* DO NOT EDIT BELOW THIS LINE */
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>-->
<!-- End Visual Website Optimizer Asynchronous Code -->
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-512cc0ea102fe03f"></script>-->
<!-- facebook -->
<!--<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1664121837161247');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1664121837161247&ev=PageView&noscript=1"
		/></noscript>-->
<!-- End Facebook -->
<script type="text/javascript">
			$(document).ready(function() {
			    if(localStorage.getItem('popup_info') != 'shown')
				{
			        $("#info").delay(1000).fadeIn();

				}

			    $("#close_info").click(function(){
			    	localStorage.setItem('popup_info','shown')
					$("#info").fadeOut();
				});
			});
		</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-TM62L9R" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script type="text/javascript">
	var activeAjaxRequests = 0;
	var jScrollPaneApi = null;
	var resizeTimeout;
	var scrollTimeout;
			
	
	$(function(){
		var w = window.innerWidth;
		if(w > 640 )
		{
			var jScrollPane = $('.scroll-pane').jScrollPane({
				autoReinitialise: true,
				autoReinitialiseDelay: 500
			});
			jScrollPaneApi = jScrollPane.data('jsp');

			$(window).scroll(function(){
				window.clearTimeout(scrollTimeout);
				scrollTimeout = window.setTimeout("scrollSides()", 0);
			});

			$("#sub-menu").mouseover( function(){ $(this).stop(); });
			$("#sub-menu").mouseout( function(){ scrollMenu(); });
			$("aside").mouseover( function(){ $(this).stop(); });
			$("aside").mouseout( function(){ scrollAside(); });

		}
	});
</script>
<!--<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30732665-1']);
  _gaq.push(['_setDomainName', 'anxieties.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>--> <!-- Message -->
<div id="mainMessage" style="">
<div id="mainMessageBox" style="display: none">
<div id="mainMessageText">A</div>
<div id="mainMessageBoxToolbar"></div>
</div>
</div>
<!-- EOF Message -->
<div id="layout">
<div id="header-wrapper">
<!-- 				<p>this is homepage</p> -->
<header>
<script type="text/javascript">
	var is_ajax_called = false; //if root menu was loaded
	var cms_page_id = "0";

	$(function()
	{
		resizeHeader();
				$("#mob_header_soc_btn").html($("#follow_us").html()); //copy social buttons

		//menu
		$("#menu_btn").click(function(event){
			event.preventDefault();
			activateMenuMobFull();
		});

		//menu
		$("#menu_btn_sticky").click(function(event){
			event.preventDefault();
			activateMenuMobFull();
		});
	});

	var width = $(window).width();
	$(window).resize(function(){
		if($(window).width() != width)
		{
            resizeHeader();
			$(".mob_more_less_link").hide(); //hide elements
        }

	});

	function activateMenuMobFull(is_hidden)//load, hide root menu
	{
		if($("#menu_container").css("display") == "none")
		{
			if(is_ajax_called === false)
			{
				//is called only once
				$.ajax({
					url: "/default/mobMenu",
					type: "POST",
					dataType: "html",
					success: function (data)
					{
						if(data && data != "")
						{
							$("#menu_container").html(data);
							if(!is_hidden) $("#menu_container").show();
							is_ajax_called = true;
							var menu_items_called_cnt = 0;

							//activate current page in the menu
																				}
					}
				});
			}
			else { $("#menu_container").show();	}
		}
		else { $("#menu_container").hide(); }
	}

	function initMenuItemClick(ul_id)
	{
		$(ul_id+" .mob_more_less_link").click(function(){
			var page_id = $(this).attr("page_id");
			if($(this).attr("type") === "more")
			{
				if($("#"+page_id+"_content").html() === "") //load menu
				{
					$.ajax({
						url: "/cms/menu",
						type: "POST",
						data: {page_id : page_id, is_mob: true},
						dataType: "html",
						success: function (data) {
							if(data && data != "")
							{
								$("#"+page_id+"_content").html(data);
							}
						}
					});
				}
				$("#"+page_id+"_content").show();
				$(this).attr("type", "less");
				$("img[page_id="+page_id+"]").addClass("mob_menu_less");
				$("img[page_id="+page_id+"]").removeClass("mob_menu_more");

				return;
			}
			else
			{
				$("#"+page_id+"_content").hide();
				$(this).attr("type", "more");
				$("img[page_id="+page_id+"]").removeClass("mob_menu_less");
				$("img[page_id="+page_id+"]").addClass("mob_menu_more");
			}
		});
	}

	function resizeHeader()
	{
		if($(window).width() < 641) { $("#homepage").css("height", $(window).width()/4.8); }
		else { $("#menu_container").hide(); }
	}
	
	$( document ).ready(function() {
		$('#menu_btn_sticky').on('click', function() {
			setTimeout(function(){
				var orig = $(".no_background .links_wrapper ").find(".links_container");
				var clone = $(orig).clone().show();
				if( $("#menu_container").css("display") == "block")
				{
					if( !$("#sub-menu .links_container").hasClass("dont_clone") )
					{
						$(clone).prependTo("#sub-menu");
						$("#sub-menu .links_container").addClass("dont_clone");
					}
				}
				else
				{

				}
		 	}, 200);
		});
	});
</script>
<div id="header-big">
<div class="mob_hdr_spacer"></div>
<div class="mob_menu">
<a class="mob_menu_item" href="/sitemap" id="menu_btn"><img src="/images/ico/menu-white.png"/></a> <!-- <a href="javascript:history.back();" class="mob_menu_item"></a> -->
<a class="mob_menu_item" href="/search/index"><img alt="Search" class="mob_menu_item_search" src="/images/ico/search-white.png" title="Search"/></a> <a href="/"><img alt="Homepage" class="mob_menu_item_home" src="/images/ico/home-white.png" title="Go to homepage"/></a> </div>
<div class="mob_header_social_btn">
<a href="https://twitter.com/DrReidWilson" target="blank_"><img alt="Twitter" class="social_twitter" src="/images/ico/twitter-white.png" title="Twitter"/></a> <a href="https://www.facebook.com/anxietiesdotcom/" target="blank_"><img alt="Facebook" class="social_fb" src="/images/ico/fb-white.png" title="Facebook"/></a> <a href="https://www.youtube.com/user/ReidWilsonPhD" target="blank_"><img alt="Youtube" class="social_yt" src="/images/ico/youtube-white.png" title="Youtube"/></a> </div>
<div class="mob_homepage">
<img alt="Homepage" src="/images/ico/logo-white.png" title="Homepage"/> </div>
</div>
<div class="motto">
<p>Helping people who suffer from <span>anxieties</span> is our mission and our passion</p>
<img alt="head" src="/images/header/head-brain.png" title="head"/> </div>
<div class="mob_v" id="header-small">
<div>
<a class="mob_menu_item mob_v" href="/sitemap" id="menu_btn_sticky"><img src="/images/ico/menu-black.png"/></a> <a href="/"><img alt="Homepage" class="header_small_home" src="/images/ico/logo-black.png" title="Homepage"/></a> <a href="/search/index"><img alt="Search" class="sticky_menu_item_search mob_v" src="/images/ico/search-black.png" title="Search"/></a> <a href="/"><img alt="Homepage" class="sticky_menu_item_home mob_v" src="/images/ico/home-black.png" title="Go to homepage"/></a> </div>
</div>
<div id="menu_container"></div>
<!-- <div id="mob_header_soc_btn"></div> -->
<!-- needed to show them in menu -->
<div class="links_wrapper" style="display: none;">
<div class="links_container">
<a href="/229/videos">
<div class="link_element">
<div><img class="video" src="/images/ico/video.png"/></div>
<div class="green_arrow"><p>Watch Dr. Wilson's Free Self-Help Videos</p></div>
</div>
</a>
<a href="/1/free">
<div class="link_element">
<div><img class="books" src="/images/ico/books.png"/></div>
<div class="green_arrow"><p>Study in our Free Self-Help Section</p></div>
</div>
</a>
<a href="/257/noise-in-your-head">
<div class="link_element">
<div><img class="video2" src="/images/ico/video2.png"/></div>
<div class="green_arrow"><p>Preview Dr. Wilson's 4.5-Hour Self-Help Course</p></div>
</div>
</a>
<a href="/254/online-self-help-coaching-anxiety-ocd">
<div class="link_element">
<div><img class="remote_help" src="/images/ico/remote-help.png"/></div>
<div class="green_arrow"><p>Get Remote Self-Help Coaching with Dr. Wilson</p></div>
</div>
</a>
<a href="/23/where-are-we">
<div class="link_element">
<div><img class="get_help" src="/images/ico/get-help.png"/></div>
<div class="green_arrow"><p>Get Help at one of our Treatment Centers</p></div>
</div>
</a>
</div>
</div>
<div id="header-menu">
<nav>
<!-- 
<div style="position:absolute; margin: 60px 0 0 -90px">
	<form method="get" style="float: right; margin-top: 2px;" action="/search">	<input type="search" class="text" placeholder="Search for ..." name="q" title="search" alt="Search" id="q" />	</form>
</div>
-->
<div id="ico_home">
<a class="purple home" href="/"><img src="/images/ico/home.png"/></a></div>
<div class="header-menu-item">
<a class="header-menu-item-button green" href="/1/free">Free Help</a> </div>
<div class="header-menu-item">
<a class="header-menu-item-button blue" href="/3/about">Get Treatment</a> </div>
<div class="header-menu-item">
<a class="header-menu-item-button purple" href="/4/get-trained">Get Trained</a> </div>
<div class="header-menu-item">
<a class="header-menu-item-button orange" href="/5/shop">Shop</a> </div>
<div id="ico_search">
<a href="/search/index"><img alt="Search" src="/images/ico/search-big.png" title="Search"/></a></div>
<div class="addthis_desktop">
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox_mgjd"></div>
<div class="instagram_page"><a href="https://www.instagram.com/rreidwilson/" target="_blank"><img src="/images/ico/instagram_color.png"/></a></div>
</div>
<!--
<div class="header-menu-item">
	<a class="header-menu-item-button" href="/search/index"><img src="/images/ico/search.gif" /></a></div>
--> </nav>
</div>
</header>
</div>
<div id="content-wrapper">
<!-- <div class="horizontal_ad_homepage"></div> -->
<aside>
<div class="teaser-column-content">
<div class="vertical_ad">
<a href="https://anxieties.com/noise-in-your-head" target="_blank"><img src="/uploads/Image/ReidWorries.jpg"/></a> </div>
</div>
<div class="teaser-column-bottom"></div>
</aside>
<div id="content-wide">
<script>
$(function(){
	switchLayout();
	$(window).resize(function(){
		switchLayout();
	});

	$(window).load(function () {
		if($.browser.opera)
			fix_flash();
	});
});

function fix_flash()
	{
    // loop through every object tag on the site
    var objects = document.getElementsByTagName('object');
    for (i = 0; i < objects.length; i++) {
        object = objects[i];
        var new_object;
        // object is an IE specific tag so we can use outerHTML here
        if (object.outerHTML) {
            var html = object.outerHTML;
            // replace an existing wmode parameter
            if (html.match(/<param\s+name\s*=\s*('|")wmode('|")\s+value\s*=\s*('|")[a-zA-Z]+('|")\s*\/?\>/i))
                new_object = html.replace(/<param\s+name\s*=\s*('|")wmode('|")\s+value\s*=\s*('|")window('|")\s*\/?\>/i, "<param name='wmode' value='transparent' />");
            // add a new wmode parameter
            else
                new_object = html.replace(/<\/object\>/i, "<param name='wmode' value='transparent' />\n</object>");
            // loop through each of the param tags
            var children = object.childNodes;
            for (j = 0; j < children.length; j++) {
                try {
                    if (children[j] != null) {
                        var theName = children[j].getAttribute('name');
                        if (theName != null && theName.match(/flashvars/i)) {
                            new_object = new_object.replace(/<param\s+name\s*=\s*('|")flashvars('|")\s+value\s*=\s*('|")[^'"]*('|")\s*\/?\>/i, "<param name='flashvars' value='" + children[j].getAttribute('value') + "' />");
                        }
                    }
                }
                catch (err) {
                }
            }
            // replace the old embed object with the fixed versiony
            object.insertAdjacentHTML('beforeBegin', new_object);
            object.parentNode.removeChild(object);
        }
    }
}

function switchLayout()
{
	var w = window.innerWidth;
	var action;
	var redirect_url = "";
	if(w > 573 )
	{
		redirect_url = "/homepage/index";
		action = "indexMain";
	}
	else
	{
		redirect_url = "/homepage/index-m";
		action = "indexMobile";
	}

	if("index" != action) location.href = redirect_url;
}
</script> </div>
</div>
</div>
<div class="mob_v" id="footer-small-sticky">
<div>
<a class="mob_menu_item" href="/shop" id="menu_btn_sticky"><img src="/images/ico/visit-our-shop.png"/></a> </div>
<div>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<span>Share: </span><div class="addthis_inline_share_toolbox"></div>
</div>
</div>
<div id="footer-wrapper">
<script type="text/javascript">
	$(function(){
		$(".page_up").click(function(){
			window.scrollTo(0, 0);
		});
	});
</script>
<div class="mob_v" id="footer-small">
<div>
<a class="mob_menu_item" href="/shop" id="menu_btn_sticky"><img src="/images/ico/visit-our-shop.png"/></a> </div>
<div>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<span>Share: </span><div class="addthis_inline_share_toolbox"></div>
</div>
<div class="clear_all"></div>
</div>
<footer>
<div class="addthis_desktop_footer">
<a class="footer_instagram_link" href="https://www.instagram.com/rreidwilson/" target="blank_"><img alt="Follow us on Instagram" class="footer_instagram" src="/images/ico/pink-instagram3.png" title="Follow us on Instagram"/></a>
<a class="footer_instagram_link_small" href="https://www.instagram.com/rreidwilson/" target="blank_"><img alt="Follow us on Instagram" class="footer_instagram_small" src="/images/ico/pink-instagram-small.png" title="Follow us on Instagram"/></a>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_inline_share_toolbox_5t7j"></div>
</div>
<div id="footer-menu">
<div class="page_up"><img src="/images/ico/mob_up.png"/></div>
<div class="page_back"><a href="javascript:history.back();"><img src="/images/ico/mob_back.png"/></a></div>
<div class="footer-menu-item">
<a alt="Contact Dr. Wilson" class="footer-menu-item-button" href="/contact" title="Contact Dr. Wilson">Contact Dr. Wilson</a> </div>
<div class="footer-menu-item">
<a alt="Terms and Conditions" class="footer-menu-item-button" href="/terms-and-conditions" title="Terms and Conditions">Terms and Conditions</a> </div>
<div class="footer-menu-item">
<a alt="Sitemap" class="footer-menu-item-button" href="/sitemap" title="Sitemap">Sitemap</a> </div>
</div>
<div class="footer_socials">
<div>
<a href="https://www.youtube.com/user/ReidWilsonPhD" target="blank_"><img alt="Youtube" class="footer_yt" src="/images/ico/youtube-white.png" title="Youtube"/></a> <a href="https://www.facebook.com/anxietiesdotcom/" target="blank_"><img alt="Facebook" class="footer_fb" src="/images/ico/fb-white.png" title="Facebook"/></a> <a href="https://twitter.com/DrReidWilson" target="blank_"><img alt="Twitter" class="footer_tw" src="/images/ico/twitter-white.png" title="Twitter"/></a> </div>
<span>2021  ©  anxieties.com</span>
</div>
</footer>
</div>
</body>
<script type="text/javascript">
//<![CDATA[

			
		
//]]>
</script></html>
