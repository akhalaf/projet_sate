<!DOCTYPE html>
<html lang="pt">
<head>
<title>CNSB</title>
<meta content="Colégio de Nossa Senhora da Bonança" name="description"/>
<meta content="colégio, bonança, cnsb, porto, vila nova gaia, ensino, escola, creche, berçário, pré-escolar, ciclo, secundário, internato" name="keywords"/>
<meta content="H2.3 Web Marketing" name="author"/>
<meta charset="utf-8"/>
<meta content="telephone=no" name="format-detection"/>
<link href="images/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="css/style.css" rel="stylesheet"/>
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/scroll_to_top.js"></script>
<script src="js/script.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/superfish.js"></script>
<script src="js/jquery.mobilemenu.js"></script>
<script src="js/touchTouch.jquery.js"></script>
<script src="js/jquery.tools.min.js"></script>
<script src="js/tmStickUp.js"></script>
<!-- font-awesome font -->
<link href="css/font-awesome.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- fontello font -->
<script src="js/camera.js"></script>
<!--[if (gt IE 9)|!(IE)]><!-->
<script src="js/jquery.mobile.customized.min.js"></script>
<!--<![endif]-->
<!--[if lt IE 8]>
		 <div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
				<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
	 <![endif]-->
<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	 <![endif]-->
</head>
<body>
<!--button back top-->
<div id="back-top"><i class="fa fa-chevron-up"></i></div>
<div class="main">
<div class="div-content">
<!--==============================header=================================-->
<div id="stuck_container">
<header class="page1">
<div class="menu_holder">
<div class="container">
<div class="row">
<div class="grid_3">
<h1><a href="index.php"><img alt="CNSB" src="images/logo.png"/></a></h1>
</div>
<form action="busca.php" class="formbu" method="GET">
<input name="busca" placeholder="Procurar Noticias" style="padding: 10px; background-color: #d8d8d8; border: 0px; width: 190px;" type="text"/>
<input style="cursor:pointer; padding: 9px; background-color: #2050c0; border: 0px; color: white;" type="submit" value="Buscar"/>
</form>
<div class="grid_9">
<nav>
<ul class="sf-menu header_menu" data-type="navbar">
<li><a href="index.php">Inicio</a></li>
<li class="dropdown"><a href="index.php">O Colégio</a>
<ul class="submenu">
<li><a href="paginas/quem-somos">História</a></li>
<li><a href="paginas/rgos-de-gesto">Órgãos de Gestão</a></li>
<li><a href="paginas/instalaes-recursos">Intalações / Recursos</a></li>
<li><a href="paginas/segurana">Segurança</a></li>
<li><a href="paginas/misso-viso-e-valores">Missão, Visão e Valores</a></li>
<li><a href="paginas/calendrio-escolar-2020-2021">Calendário Escolar</a></li>
<li><a href="paginas/manuais-escolares">Manuais Escolares</a></li>
<li><a href="http://www.cnsb.pt/paginas/regulamento-interno" target="_blank">Regulamento Interno</a></li>
<li><a href="paginas/horrios-2017-2018">Horários</a></li>
<li><a href="paginas/regulamento-geral-da-proteo-de-dados-rgpd">RGPD</a></li>
<li><a href="paginas/contratos-de-apoio-famlia-">Contratos de apoio à família</a></li>
<li><a href="paginas/coronavrus-covid-19">Coronavírus COVID-19</a></li>
<li><a href="paginas/renovao-de-matrculas-2020-2021">Renovação de Matrículas</a></li>
<li><a href="paginas/conselhos-ed">Conselhos E@D</a></li>
<li><a href="paginas/ementas-creche-e-pr-escolar">Ementas Creche e Pré-escolar</a></li>
</ul>
</li>
<li class="dropdown"><a href="paginas/projeto-educativo">Projeto Educativo</a>
<ul class="submenu">
<li><a href="paginas/projeto-educativo">Projeto Educativo</a></li>
<li><a href="paginas/iderio-da-confhic">Ideário da CONFHIC</a></li>
<li><a href="paginas/projeto-cnsb-intercultural-hospitalidade-e-educao">CNSB Intercultural</a></li>
</ul>
</li>
<li class="dropdown"><a href="index.php">Oferta Formativa</a>
<ul class="submenu">
<li><a href="paginas/creche">Creche</a></li>
<li><a href="paginas/pr-escolar">Pré-Escolar</a></li>
<li><a href="paginas/1-ciclo">1º Ciclo</a></li>
<li><a href="paginas/2-ciclo">2º Ciclo</a></li>
<li><a href="paginas/3-ciclo">3º Ciclo</a></li>
<li><a href="paginas/secundrio">Secundário</a></li>
<li><a href="paginas/internato-feminino">Internato Feminino</a></li>
<li class="dropdown"><a href="index.php">Oferta Complementar</a>
<ul class="submenu">
<li><a href="paginas/apoios-educativos">Apoios Educativos</a></li>
<li><a href="paginas/educao-para-a-cidadania">Educação para a Cidadania</a></li>
<li><a href="paginas/biblioteca">Biblioteca</a></li>
<li><a href="paginas/spo-servio-de-psicologia-e-orientao">SPO</a></li>
<li><a href="paginas/catequese">Catequese</a></li>
<li><a href="paginas/projetos-e-parcerias">Projetos e Parcerias</a></li>
<li><a href="paginas/atividades-de-enriquecimento-curricular">Atividades de Enriquecimento Curricular</a></li>
</ul>
</li>
</ul>
</li>
<li class="dropdown"><a href="index.php">Inscrições</a>
<ul class="submenu">
<li><a href="paginas/critrios-de-admisso">Critérios de Admissão</a></li>
<li><a href="paginas/formulrio-de-pr-inscrio">Pré-Inscrição Online</a></li>
<li><a href="contactos.php">Marcação de Visita (entrevista)</a></li>
</ul>
</li>
<li><a href="noticias.php">Notícias</a></li>
<li class="dropdown"><a href="paginas/e-escola">E-Escola</a>
<ul class="submenu">
<li><a href="https://epass.cnsb.pt/epasshttps/" target="_blank">epass funcionários</a></li>
<li><a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&amp;redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&amp;response_type=code%20id_token&amp;scope=openid%20profile&amp;response_mode=form_post&amp;nonce=637216828736586" target="_blank">Office 365</a></li>
<li><a href="http://www.escolavirtual.pt/" target="_blank">Escola Virtual</a></li>
<li><a href="http://moodle.cnsb.pt" target="_blank">Moodle</a></li>
<li><a href="https://alunoscnsbonanca.eschoolingserver.com/" target="_blank">Portal do Aluno e-community</a></li>
</ul>
</li>
<li><a href="contactos.php">Contactos</a></li>
</ul> <!--<ul class="sf-menu header_menu">
											<li class="current"><a href="index.html">Home<strong></strong></a></li>
											<li><a href="index-1.html"><span></span>About District<strong></strong></a>
													<ul class='submenu'>
														<li><a href="#">Lorem ipsum dolor sit amet</a></li>
														<li><a href="#">Conse ctetur adipisicing</a></li>
														<li class='sub-menu'><a href="#">Elit sed do eiusmod tempor</a>
															 <ul class='submenu2'>
																	<li><a href="#">Fresh</a></li>
																	<li><a href="#">Archive</a></li>
															 </ul>
														</li>
														<li><a href="#">Incididunt ut labore </a></li>
														<li><a href="#">Et dolore magna aliqua</a></li>
													</ul>
											 </li>
											 <li><a href="index-2.html">Departments<strong></strong></a></li>
											 <li><a href="index-3.html">News<strong></strong></a></li>
											 <li id="last-li"><a href="index-4.html">Contacts<strong></strong></a></li>
										</ul>-->
</nav>
</div>
<!--<div class="grid_2">
									<form id="search1" action="search.php" method="GET" accept-charset="utf-8">
										<input type="text" name="s" value="" onfocus="myFocus(this);" onblur="myBlur(this);"/>
										<a onclick="document.getElementById('search1').submit()" class="more_btn3"></a>
									</form>
								</div>-->
</div>
</div>
</div>
<!-- <div class="container">
						<p class="txt1">Providing safe</p>
						<p class="txt2">and educational environment</p>
						<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
					</div> -->
</header>
</div>
<div class="slider_wrapper">
<div class="" id="camera_wrap">
<div data-src="uploads/slideshow/FOTO-4.jpg">
</div>
<div data-src="uploads/slideshow/FOTO-3.jpg">
</div>
</div>
</div>
<!--=======content================================-->
<div class="wrapper marTop1">
<div class="container">
<div class="row">
<div class="grid_4">
<div class="banner1 bg2">
<p class="txt3">Formulário de Pré-inscrição </p>
<p class="color1"> 
Preencha o seguinte formulário para enviar a sua Pré-Candidatura online:
 
...</p>
<a class="link1 marTop2 fleft" href="paginas/formulrio-de-pr-inscrio"><i class="fa fa-arrow-circle-o-right icon1"></i></a>
<div class="clear"></div>
</div>
</div>
<div class="grid_4">
<div class="banner1 bg2">
<p class="txt3">Santo Natal e Próspero Ano Novo</p>
<p class="color1">...</p>
<a class="link1 marTop2 fleft color1" href="paginas/lorem-ipsum"><i class="fa fa-arrow-circle-o-right icon1"></i></a>
<div class="clear"></div>
</div>
</div>
<div class="grid_4">
<div class="banner1 bg2">
<p class="txt3">Novos Alunos</p>
<p class="color1">O CNSB assenta num ideário de matriz cristã.
No Colégio, os nossos alunos são convidados a participar na nossa proposta educativa de uma forma interventiva.  
 
O aluno do Colégio...</p>
<a class="link1 marTop2 fleft" href="paginas/novos-alunos"><i class="fa fa-arrow-circle-o-right icon1"></i></a>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="grid_3">
<ul class="listWithMarker">
<li><a href="paginas/manuais-escolares"> Manuais Escolares 2020-2021</a></li>
<li><a href="paginas/renovao-de-matrculas-2020-2021">Renovação de Matrículas 2020-2021</a></li>
<li><a href="paginas/calendrio-escolar-2020-2021">Calendário escolar 2020 - 2021</a></li>
</ul>
<div class="box-1">
<h2 class="marTop0">História</h2>
<p class="marTop3">1894

Fundação do Colégio de Nossa Senhora do Rosário em Vila Nova de Gaia, e seu funcionamento no extinto Convento Corpus Christi sob a mão das Irmãs Franciscanas Hospitaleiras Portu...</p>
<a class="more_btn" href="paginas/quem-somos">Ler Mais</a>
</div>
</div>
<div class="grid_6">
<h2>Noticias e Acontecimentos</h2>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/194">Momento de formativo</a></p>
<p>Artesãos da Cultura da Solidariedade Hospitaleira</p>
<a href="noticias/194"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/189">Janeiras 2020</a></p>
<p>O cantar das janeiras é já um costume assumido pelo Colégio de Nossa Senhora da Bonança </p>
<a href="noticias/189"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/187">Migrações e desenvolvimento sustentável</a></p>
<p>visite a exposição itinerante na nossa biblioteca</p>
<a href="noticias/187"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/183">Visita de Estudo a Famalicão</a></p>
<p>No dia 4 de novembro de 2019, os alunos do 6º ano do CNSB foram a Famalicão visitar o Museu Bernardino Machado e o Museu da Indústria Têxtil, no âmbito da disciplina de História e Geografia de Portugal.</p>
<a href="noticias/183"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/182">Visita de estudo à Barragem de Crestuma-Lever</a></p>
<p> No dia 11 de novembro, realizou-se a visita de estudo à Barragem de Crestuma-Lever, uma das barragens do rio Douro.</p>
<a href="noticias/182"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/181">Dia Mundial da Saúde Mental</a></p>
<p>MEMÓRIA, SONO E DESEMPENHO NA APRENDIZAGEM</p>
<a href="noticias/181"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/180">Dia da biblioteca escolar</a></p>
<p>No âmbito das comemorações do dia da Biblioteca Escolar e do dia Mundial da Saúde Mental recebemos, a convite do Serviço de Psicologia e Orientação, a autora Manuela Mota Ribeiro, para apresentar a sua obra ?Palavras Mágicas?. </p>
<a href="noticias/180"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/179">Encontro dos alunos do 4º ano da CONFHIC</a></p>
<p>Os alunos do 4º ano dos Colégios da CONFHIC, no dia 11 de outubro, reuniram-se no Externato Santa Joana</p>
<a href="noticias/179"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/177">Workshop de Musicoterapia</a></p>
<p>No âmbito do Projeto Música e Afetos, o Serviço de Psicologia e Orientação organizou</p>
<a href="noticias/177"><p class="txt4">Ler Mais</p></a>
</div>
<div class="hline2">
<p class="txt5"><a class="link2" href="noticias/169">Visita Cultural do Rota do Românico</a></p>
<p>No dia 4 de maio, um grupo de alunos Nacionais de Países Terceiros (NPT) do nosso Colégio, acompanhados pelas</p>
<a href="noticias/169"><p class="txt4">Ler Mais</p></a>
</div>
<p class="txt7"><a href="noticias.php">Ver Todas</a></p>
</div>
<div class="grid_3">
<h2>Destaques</h2>
<a class="link_img" href="noticias/197">
<div class="img_section">
<img alt="GUILHERME DE OLIVEIRA PERTO DO PÓDIO NA SUA ESTREIA NA F4" src="uploads/noticias/GuilhermeOliveira.jpg"/>
<div class="img_section_txt">
<p class="txt9">GUILHERME DE OLIVEIRA PERTO DO PÓDIO NA SUA ESTREIA NA F4 </p>
<p class="txt8">Com apenas 15 anos, Guilh...</p>
</div>
</div>
</a>
<a class="link_img" href="noticias/196">
<div class="img_section">
<img alt="Olimpíadas de Física 2020 " src="uploads/noticias/ol_f_1.jpg"/>
<div class="img_section_txt">
<p class="txt9">Olimpíadas de Física 2020  </p>
<p class="txt8">Todos os anos, o CNSB con...</p>
</div>
</div>
</a>
<a class="link_img" href="noticias/195">
<div class="img_section">
<img alt="Olimpíadas da Geologia" src="uploads/noticias/olimpiadasdafisica.jpg"/>
<div class="img_section_txt">
<p class="txt9">Olimpíadas da Geologia </p>
<p class="txt8">A caminho da Rússia...</p>
</div>
</div>
</a>
<a class="link_img" href="noticias/193">
<div class="img_section">
<img alt="O Oceano faz-nos falta!" src="uploads/noticias/DiaEscolaAzul.jpg"/>
<div class="img_section_txt">
<p class="txt9">O Oceano faz-nos falta! </p>
<p class="txt8">A dezanove de maio comemo...</p>
</div>
</div>
</a>
<a class="link_img" href="noticias/192">
<div class="img_section">
<img alt="Agradecimento aos profissionais de saúde" src="uploads/noticias/Maria Coutinho.jpg"/>
<div class="img_section_txt">
<p class="txt9">Agradecimento aos profissionais de saúde </p>
<p class="txt8">Carta aberta de uma aluna...</p>
</div>
</div>
</a>
<!--<a href="#" class="link_img">
								<div class="img_section">
									<img src="images/page1_pic2.jpg" alt="">
									<div class="img_section_txt">
										<p class="txt8">3/31/2014  -  8:00 AM</p>
										<p class="txt9">Ipsum dolor sit amet conse ctetur adipisicing </p>
									</div>
								</div>
							</a>

							<a href="#" class="link_img">
								<div class="img_section">
									<img src="images/page1_pic3.jpg" alt="">
									<div class="img_section_txt">
										<p class="txt8">3/31/2014  -  8:00 AM</p>
										<p class="txt9">Excepteur sint occaecat cupidatat non proident</p>
									</div>
								</div>
							</a>-->
<p class="txt7"><a href="noticias.php">Ver Todos</a></p>
</div>
</div>
</div>
<!--=======footer=================================-->
</div>
<footer>
<div class="footer_priv">
<script type="text/javascript">
(function(u,t,d){
    var i=d.createElement(t);i.type='text/javascript';i.async=true;i.src='//'+u;
    var s=d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(i,s);
})('eu2-track.inside-graph.com/gtm/IN-1000902/include.js','script',document);
</script>
<div class="container">
<p class="txt_priv">Colégio Nossa Senhora da Bonança © <span id="copyright-year"></span> | Powered by <a class="link2" href="http://h23.pt" target="_blank">H2.3 Web Marketing</a></p>
<ul class="soc_icons">
</ul>
</div>
</div>
</footer>
</div>
<script type="text/javascript">

	$(document).ready(function(){
		
		jQuery('#camera_wrap').camera({
				loader: false,
				pagination: false,
				thumbnails: false,
				height: '45.3125%',
				fx: 'simpleFade',
				rows: '1',
				slicedCols: '1',
				slicedRows: '1',
				caption: false
			});

	 });

	 $(document).ready(function(){
		// Initialize the gallery
		$('.magnifier2').touchTouch();
	 });

</script>
</body>
</html>