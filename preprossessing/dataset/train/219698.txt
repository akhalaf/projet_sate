<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!--[if lt IE 9]>
        <script src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/html5shiv.js"></script>
        <![endif]-->
<title>Imóvel Site -</title>
<!-- This site is optimized with the Yoast SEO plugin v8.0 - https://yoast.com/wordpress/plugins/seo/ -->
<link href="https://bilenio.com.br/" rel="canonical"/>
<meta content="pt_BR" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Imóvel Site -" property="og:title"/>
<meta content="https://bilenio.com.br/" property="og:url"/>
<meta content="Imóvel Site" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Imóvel Site -" name="twitter:title"/>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/bilenio.com.br\/","name":"Im\u00f3vel Site","potentialAction":{"@type":"SearchAction","target":"https:\/\/bilenio.com.br\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bilenio.com.br/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.9.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/plugins/creame-whatsapp-me/public/css/whatsappme.css?ver=1.4.3" id="whatsappme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/themes/proimovel/style.css?ver=1.2" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/themes/proimovel/css/boot/boot.css?ver=1588258276" id="boot-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/themes/proimovel/css/fontawesome/css/font-awesome.min.css?ver=1588258276" id="font-awesome.min-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/themes/proimovel/css/default.css?ver=1588258276" id="default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-content/themes/proimovel/css/bxslider.css?ver=1588258276" id="bxslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/wp-admin/admin-ajax.php?action=pro_dynamic_css&amp;ver=1.2" id="pro-dynamic-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bilenio.com.br/?sccss=1&amp;ver=dc94a26a6319eec5479db0e1fe65af79" id="sccss_style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://bilenio.com.br/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<link href="https://bilenio.com.br/wp-json/" rel="https://api.w.org/"/><link href="https://bilenio.com.br/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bilenio.com.br/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<!-- Favicon -->
<link href="https://bilenio.com.br/wp-content/uploads/2020/04/favicon-bilenio.png" rel="shortcut icon"/>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-136098253-14"></script>
<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}gtag('js',new Date());gtag('config','UA-136098253-14');</script> </head>
<body class="home blog" data-rsssl="1">
<header>
<h1 class="main_title">Imóvel Site - </h1>
</header>
<!--  MENU-->
<div class="menu_mobile container">
<ul class="content-menu">
<li>
<a class="mobile_action" href="#" title="Menu"><i class="fa fa-bars fa-fw"></i></a>
</li>
<li id="open_tel">
<a class="open_tel" href="#" title="Atendimento"><i class="fa fa-phone fa-fw"></i></a>
<ul class="menu_view">
<li class="atendimento">
<strong> - Atendimento</strong><br/>(51) 3035 7004                            </li>
</ul>
</li>
<li id="open_whatsapp">
<a class="open_whatsapp" href="#" title="WhatsApp"><i class="fa fa-whatsapp fa-fw"></i></a>
<ul class="menu_view">
<li class="whatsapp">
<strong> - WhatsApp</strong><br/><a href="https://api.whatsapp.com/send?phone=555198524615&amp;text=Olá!">(51) 99852-4615</a> </li>
</ul>
</li>
<li><a href="https://bilenio.com.br/contato" title="Contato"><i class="fa fa-envelope fa-fw"></i></a></li>
<li><a href="https://bilenio.com.br/meus-favoritos" title="Favoritos"><i class="fa fa-heart-o fa-fw"></i></a></li>
<li>
<a class="art_search" href="#" title="Pesquisar"><i class="fa fa-search fa-fw"></i></a>
</li>
</ul>
</div>
<header class="main_header container bg-custom-top">
<div class="content content_header">
<div class="main_header_logo box_header">
<a href="https://bilenio.com.br" title="Imóvel Site">
<img alt="Imóvel Site" src="https://bilenio.com.br/wp-content/uploads/2020/04/Logotipo-bilenio.png" title="Imóvel Site"/>
</a>
</div>
<div class="box_header">
<ul class="contacts">
<li>
<div class="ico icon_phone"><i class="fa fa-phone"></i></div>
<div class="service text_phone">Atendimento<br/><span><strong>(51) 3035 7004</strong></span></div>
</li>
<li>
<div class="ico icon_whatsapp"><i class="fa fa-whatsapp"></i></div>
<div class="service text_whatsapp">WhatsApp<br/><span><strong><a href="https://api.whatsapp.com/send?phone=555198524615&amp;text=Olá!">(51) 99852-4615</a></strong></span></div>
</li>
<li>
<div class="ico icon_email"><i class="fa fa-envelope-o"></i></div>
<div class="service text_email">E-mail<br/><span><strong>contato@bilenio.com.br</strong></span></div>
</li>
</ul>
</div> <div class="clear"></div>
</div>
</header>
<div id="second_header">
<div class="content content_header">
<div class="main_header_logo box_header">
<a href="https://bilenio.com.br" title="Imóvel Site">
<img alt="Imóvel Site" src="https://bilenio.com.br/wp-content/uploads/2020/04/Logotipo-bilenio.png" title="Imóvel Site"/>
</a>
</div>
<div class="box_header">
<ul class="contacts">
<li>
<div class="ico icon_phone"><i class="fa fa-phone"></i></div>
<div class="service text_phone">Atendimento<br/><span><strong>(51) 3035 7004</strong></span></div>
</li>
<li>
<div class="ico icon_whatsapp"><i class="fa fa-whatsapp"></i></div>
<div class="service text_whatsapp">WhatsApp<br/><span><strong><a href="https://api.whatsapp.com/send?phone=555198524615&amp;text=Olá!">(51) 99852-4615</a></strong></span></div>
</li>
<li>
<div class="ico icon_email"><i class="fa fa-envelope-o"></i></div>
<div class="service text_email">E-mail<br/><span><strong>contato@bilenio.com.br</strong></span></div>
</li>
</ul>
</div> <div class="clear"></div>
</div>
</div>
<!--  MENU-->
<div class="menu_header container bg-menu-color">
<div class="content-menu" id="call_info">
<ul class="call_info">
<li id="my_favorite">
<a class="my_favorite" href="https://bilenio.com.br/meus-favoritos" title="Meus Favoritos"><i class="fa fa-star"></i> Favoritos</a>
</li>
<li>
<a class="menu_login" href="https://bilenio.com.br/wp-login.php" title="Login"><i class="fa fa-sign-in"></i> Login</a>
</li>
</ul>
</div>
<ul class="main_nav content-menu" id="menu-menu-principal"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-555" id="menu-item-555"><a aria-current="page" href="/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-553" id="menu-item-553"><a href="https://bilenio.com.br/contato/">Contato</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-554" id="menu-item-554"><a href="https://bilenio.com.br/sobre/">Sobre</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-572" id="menu-item-572"><a href="https://bilenio.com.br/categoria/apartamentos/">Apartamentos</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-573" id="menu-item-573"><a href="https://bilenio.com.br/categoria/comerciais/">Comerciais</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-574" id="menu-item-574"><a href="https://bilenio.com.br/categoria/casas/">Casas</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-575" id="menu-item-575"><a href="https://bilenio.com.br/categoria/terrenos/">Terrenos</a></li>
</ul>
<div class="clear"></div>
</div>
<div class="container" id="ad_search">
<div class="header_search_imovel container"><!-- SEARCHIMOVEL -->
<div class="search_imovel content-search-imovel">
<form action="https://bilenio.com.br/" class="form_search_imovel" id="advanced-form_imovel" method="get" role="search">
<input id="imovel" name="imovel" type="hidden" value="pesquisa"/>
<input id="name" name="s" type="hidden" value=""/>
<div class="form-group">
<input class="form-control" id="codimob" name="code" placeholder="Cód." type="text"/>
</div>
<div class="form-group">
<select class="form-control" id="business_id" name="business">
<option disabled="disabled" selected="selected" value="">Finalidade</option>
<option value="venda">Venda</option>
</select>
</div>
<div class="form-group">
<select class="form-control" id="tipo_id" name="tipo">
<option disabled="disabled" selected="selected" value="">Tipo</option>
<option value="apartamentos">Apartamentos</option>
<option value="casas">Casas</option>
<option value="terrenos">Terrenos</option>
</select>
</div>
<div class="form-group">
<select class="form-control" id="cidade_id" name="city">
<option disabled="disabled" selected="" value="">Cidades</option>
<option value="novo-hamburgo">Novo Hamburgo</option><option value="porto-alegre">Porto Alegre</option><option value="sao-leopoldo">São Leopoldo</option> </select>
</div>
<div class="form-group">
<select class="form-control" id="bairro_id" name="bairro">
<option disabled="disabled" selected="" value="">Bairros</option>
<option disabled="disabled" value="">Selecione uma cidade</option>
</select>
</div>
<div class="form-group">
<input class="radius" id="searchsubmit" type="submit" value="Pesquisar"/>
</div>
<div class="clear"></div>
</form>
</div>
</div><!-- FECHA SEARCHIMOVEL -->
</div>
<!--CONTEÚDO-->
<section class="container">
<div class="content grid cs-style-3">
<!--Categoria apartamentos-->
<header class="main_imov_title">
<h2 class="bg-custom-btn-rent radius al-center boxshadow textshadow">Apartamentos</h2>
</header>
<article class="main_imov_item">
<div class="thumb property-mask">
<div class="himage">
<span class="status">
	Pronto	</span>
<a href="https://bilenio.com.br/apartamento-2-dormitorios-condominio-fechado/" title="Apartamento 2 dormitórios Condomínio Fechado">
<picture>
<source media="(min-width: 1024px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/FOTO-CONDOMÍNIO-404x242.jpeg"/>
<source media="(min-width: 540px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/FOTO-CONDOMÍNIO-600x450.jpeg"/>
<source media="(min-width: 1px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/FOTO-CONDOMÍNIO-480x380.jpeg"/>
<img alt="Apartamento 2 dormitórios Condomínio Fechado" src="https://bilenio.com.br/wp-content/uploads/2020/05/FOTO-CONDOMÍNIO-600x450.jpeg " title="Apartamento 2 dormitórios Condomínio Fechado"/>
</picture>
</a>
<span class="figcaption">
<a class="hand_pointer" href="https://bilenio.com.br/apartamento-2-dormitorios-condominio-fechado/" title="Apartamento 2 dormitórios Condomínio Fechado">
<i class="fa fa-hand-pointer-o"></i>
</a>
</span>
<p class="property-price">
<span class="cat-price"> R$ 130.000,00	</span>
<span class="picon"><i class="fa fa-tag"></i></span>
</p>
<p class="property_business">
<span class="property_business_title">Venda</span>
</p>
</div>
</div>
<div class="property-info">
<span><i class="fa fa-bed"></i> 2 Quartos</span>
<span><i class="fa fa-male"></i> 1 Banheiro</span>
<span><i class="fa fa-car"></i> 1 Vaga </span>
</div>
<div class="main_imov_item_desc">
<a class="property_item_desc" href="https://bilenio.com.br/apartamento-2-dormitorios-condominio-fechado/" title="Saiba Mais"><h2>Apartamento 2 dormitórios Condomíni...</h2></a>
</div>
</article>
<article class="main_imov_item">
<div class="thumb property-mask">
<div class="featured-ribbon"><i class="fa fa-star" title="Destaque"></i></div>
<div class="himage">
<span class="status">
	Usado	</span>
<a href="https://bilenio.com.br/apartamento-2-dormitorios-centro-de-sao-leopoldo/" title="Apartamento 2 dormitórios Centro de São Leopoldo">
<picture>
<source media="(min-width: 1024px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fotos-fachada-1-404x242.jpeg"/>
<source media="(min-width: 540px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fotos-fachada-1-600x450.jpeg"/>
<source media="(min-width: 1px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fotos-fachada-1-480x380.jpeg"/>
<img alt="Apartamento 2 dormitórios Centro de São Leopoldo" src="https://bilenio.com.br/wp-content/uploads/2020/05/Fotos-fachada-1-600x450.jpeg " title="Apartamento 2 dormitórios Centro de São Leopoldo"/>
</picture>
</a>
<span class="figcaption">
<a class="hand_pointer" href="https://bilenio.com.br/apartamento-2-dormitorios-centro-de-sao-leopoldo/" title="Apartamento 2 dormitórios Centro de São Leopoldo">
<i class="fa fa-hand-pointer-o"></i>
</a>
</span>
<p class="property-price">
<span class="cat-price"> R$ 220.000,00	</span>
<span class="picon"><i class="fa fa-tag"></i></span>
</p>
<p class="property_business">
<span class="property_business_title">Venda</span>
</p>
</div>
</div>
<div class="property-info">
<span><i class="fa fa-male"></i> 1 Banheiro</span>
<span><i class="fa fa-car"></i> 1 Vaga </span>
</div>
<div class="main_imov_item_desc">
<a class="property_item_desc" href="https://bilenio.com.br/apartamento-2-dormitorios-centro-de-sao-leopoldo/" title="Saiba Mais"><h2>Apartamento 2 dormitórios Centro de...</h2></a>
</div>
</article>
<article class="main_imov_item">
<div class="thumb property-mask">
<div class="featured-ribbon"><i class="fa fa-star" title="Destaque"></i></div>
<div class="himage">
<span class="status">
	Novo	</span>
<a href="https://bilenio.com.br/apartamento-2-dormitorios-na-vila-rosa/" title="Apartamento 2 dormitórios na Vila Rosa">
<picture>
<source media="(min-width: 1024px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-404x242.jpeg"/>
<source media="(min-width: 540px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-600x450.jpeg"/>
<source media="(min-width: 1px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-480x380.jpeg"/>
<img alt="Apartamento 2 dormitórios na Vila Rosa" src="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-600x450.jpeg " title="Apartamento 2 dormitórios na Vila Rosa"/>
</picture>
</a>
<span class="figcaption">
<a class="hand_pointer" href="https://bilenio.com.br/apartamento-2-dormitorios-na-vila-rosa/" title="Apartamento 2 dormitórios na Vila Rosa">
<i class="fa fa-hand-pointer-o"></i>
</a>
</span>
<p class="property-price">
<span class="cat-price"> R$ 199.000,00	</span>
<span class="picon"><i class="fa fa-tag"></i></span>
</p>
<p class="property_business">
<span class="property_business_title">Venda</span>
</p>
</div>
</div>
<div class="property-info">
<span><i class="fa fa-arrows-alt"></i> Área total 55²</span>
<span><i class="fa fa-bed"></i> 2 Quartos</span>
<span><i class="fa fa-male"></i> 1 Banheiro</span>
<span><i class="fa fa-car"></i> 1 Vaga </span>
</div>
<div class="main_imov_item_desc">
<a class="property_item_desc" href="https://bilenio.com.br/apartamento-2-dormitorios-na-vila-rosa/" title="Saiba Mais"><h2>Apartamento 2 dormitórios na Vila R...</h2></a>
</div>
</article>
<div class="clear"></div>
<div class="clear"></div>
</div>
</section>
<!--Close block One-->
<!--Open block Two-->
<section class="container">
<div class="content grid cs-style-3">
<header class="main_imov_title">
<h2 class="bg-custom-btn-sale radius al-center boxshadow textshadow">Casas</h2>
</header>
<article class="main_imov_item">
<div class="thumb property-mask">
<div class="himage">
<span class="status">
	Usado	</span>
<a href="https://bilenio.com.br/casa-no-belem-novo/" title="Casa no Belém Novo">
<picture>
<source media="(min-width: 1024px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-2-404x242.jpeg"/>
<source media="(min-width: 540px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-2-600x450.jpeg"/>
<source media="(min-width: 1px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-2-480x380.jpeg"/>
<img alt="Casa no Belém Novo" src="https://bilenio.com.br/wp-content/uploads/2020/05/Fachada-1-2-600x450.jpeg " title="Casa no Belém Novo"/>
</picture>
</a>
<span class="figcaption">
<a class="hand_pointer" href="https://bilenio.com.br/casa-no-belem-novo/" title="Casa no Belém Novo">
<i class="fa fa-hand-pointer-o"></i>
</a>
</span>
<p class="property-price">
<span class="cat-price"> R$ 250.000,00	</span>
<span class="picon"><i class="fa fa-tag"></i></span>
</p>
<p class="property_business">
<span class="property_business_title">Venda</span>
</p>
</div>
</div>
<div class="property-info">
<span><i class="fa fa-arrows-alt"></i> Área total 238²</span>
<span><i class="fa fa-bed"></i> 2 Quartos</span>
<span><i class="fa fa-male"></i> 1 Banheiro</span>
<span><i class="fa fa-car"></i> 2 Vagas </span>
</div>
<div class="main_imov_item_desc">
<a class="property_item_desc" href="https://bilenio.com.br/casa-no-belem-novo/" title="Saiba Mais"><h2>Casa no Belém Novo</h2></a>
</div>
</article>
<div class="clear"></div>
<div class="clear"></div>
</div>
</section>
<!--Open block Three-->
<section class="container">
<div class="content grid cs-style-3">
<header class="main_imov_title">
<h2 class="bg-custom-btn-block-three radius al-center boxshadow textshadow">Terrenos</h2>
</header>
<article class="main_imov_item">
<div class="thumb property-mask">
<div class="featured-ribbon"><i class="fa fa-star" title="Destaque"></i></div>
<div class="himage">
<span class="status">
	Novo	</span>
<a href="https://bilenio.com.br/terreno-2-000-mts2-a-100-mts-da-br-116-regiao-central/" title="Terreno 2.000 mts2  à 100 mts da BR 116 Região Central">
<picture>
<source media="(min-width: 1024px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Foto-de-fachada-do-Zeca-404x242.jpeg"/>
<source media="(min-width: 540px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Foto-de-fachada-do-Zeca-600x450.jpeg"/>
<source media="(min-width: 1px)" srcset="https://bilenio.com.br/wp-content/uploads/2020/05/Foto-de-fachada-do-Zeca-480x380.jpeg"/>
<img alt="Terreno 2.000 mts2  à 100 mts da BR 116 Região Central" src="https://bilenio.com.br/wp-content/uploads/2020/05/Foto-de-fachada-do-Zeca-600x450.jpeg " title="Terreno 2.000 mts2  à 100 mts da BR 116 Região Central"/>
</picture>
</a>
<span class="figcaption">
<a class="hand_pointer" href="https://bilenio.com.br/terreno-2-000-mts2-a-100-mts-da-br-116-regiao-central/" title="Terreno 2.000 mts2  à 100 mts da BR 116 Região Central">
<i class="fa fa-hand-pointer-o"></i>
</a>
</span>
<p class="property-price">
<span class="cat-price"> R$ 1.700.000,00	</span>
<span class="picon"><i class="fa fa-tag"></i></span>
</p>
<p class="property_business">
<span class="property_business_title">Venda</span>
</p>
</div>
</div>
<div class="property-info">
</div>
<div class="main_imov_item_desc">
<a class="property_item_desc" href="https://bilenio.com.br/terreno-2-000-mts2-a-100-mts-da-br-116-regiao-central/" title="Saiba Mais"><h2>Terreno 2.000 mts2  à 100 mts da BR...</h2></a>
</div>
</article>
<div class="clear"></div>
<div class="clear"></div>
</div>
</section>
<!--Open block Four-->
<section class="container">
<div class="content grid cs-style-3">
<header class="main_imov_title">
<h2 class="bg-custom-btn-block-four radius al-center boxshadow textshadow">Comerciais</h2>
</header>
<h2 class="none al-center">No momento não há imóvel disponivel nessa categoria!</h2>
<div class="clear"></div>
</div>
</section>
<!-- FIM CONTEÚDO -->
<!--Start CATEGORIAS-->
<!--END CATEGORIAS-->
<!--CENTRAL DE RELACIONAMENTO-->
<!--FECHA CENTRAL DE  RELACIONAMENTO-->
<!--Start Agent-->
<!--END Agent-->
<footer class="main_footer container bg-custom-footer">
<section class="content">
<h2 class="main_title">Saiba mais Sobre Imóvel Site</h2>
<div class="main_social">
<ul class="main_footer_social al-center">
<li><a class="twitter" href="https://twitter.com/twitter.com" target="_blank" title=" no Twitter"><i class="fa fa-twitter"></i></a></li>
<li><a class="facebook" href="https://www.facebook.com/facebook.com" target="_blank" title=" no Facebook"><i class="fa fa-facebook"></i></a></li>
<li><a class="googleplus" href="https://plus.google.com/googleplus.com" target="_blank" title=" no Google +"><i class="fa fa-google-plus"></i></a></li>
<li><a class="profile_instagram" href="https://www.instagram.com/instagram.com" target="_blank" title=" Instagram"><i class="fa fa-instagram"></i></a></li>
<li class="last"><a class="mail" href="https://bilenio.com.br/contato" title="Entre em Contato"><i class="fa fa-envelope"></i></a></li>
</ul>
</div>
<div class="clear"></div>
<div class="line"></div>
<div class="main_footer_copy">
<p class="copy">© 2020 - Imóvel Site, Todos os Direitos Reservados!</p>
<p class="creci">Corretor de imóveis CRECI 39913</p>
</div>
<div class="clear"></div>
</section>
</footer>
<footer class="main_footer_developer container background_developer"><!-- Start Developer -->
<div class="content developer_height">
<div class="clear"></div>
</div>
<div class="position_bottom">
<div class="go_topo"><a class="ajaxGoto" href="#topo" style="padding: 10px;"><span><i class="fa fa-arrow-up"></i></span></a></div>
</div>
</footer><!-- END developer -->
<script type="text/javascript">WebFontConfig={google:{families:['Titillium+Web:300,400,600:latin','Days+One','Roboto','Lato','Source+Sans+Pro','Dosis','Open+Sans']}};(function(){var wf=document.createElement('script');wf.src='https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js';wf.type='text/javascript';wf.async='true';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf,s);})();</script>
<div class="whatsappme whatsappme--right" data-settings='{"show":true,"telephone":"5551998524615","message_text":"Ol\u00e1! Bilenio Im\u00f3veis?","message_delay":10000,"message_send":"Ol\u00e1! Como podemos ajudar?","mobile_only":false,"position":"right"}'>
<div class="whatsappme__button">
<svg height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z" fill="currentColor"></path></svg>
</div>
<div class="whatsappme__box">
<header class="whatsappme__header">
<svg height="28" viewbox="0 0 120 28" width="120" xmlns="http://www.w3.org/2000/svg"><path d="M117.2 17c0 .4-.2.7-.4 1-.1.3-.4.5-.7.7l-1 .2c-.5 0-.9 0-1.2-.2l-.7-.7a3 3 0 0 1-.4-1 5.4 5.4 0 0 1 0-2.3c0-.4.2-.7.4-1l.7-.7a2 2 0 0 1 1.1-.3 2 2 0 0 1 1.8 1l.4 1a5.3 5.3 0 0 1 0 2.3zm2.5-3c-.1-.7-.4-1.3-.8-1.7a4 4 0 0 0-1.3-1.2c-.6-.3-1.3-.4-2-.4-.6 0-1.2.1-1.7.4a3 3 0 0 0-1.2 1.1V11H110v13h2.7v-4.5c.4.4.8.8 1.3 1 .5.3 1 .4 1.6.4a4 4 0 0 0 3.2-1.5c.4-.5.7-1 .8-1.6.2-.6.3-1.2.3-1.9s0-1.3-.3-2zm-13.1 3c0 .4-.2.7-.4 1l-.7.7-1.1.2c-.4 0-.8 0-1-.2-.4-.2-.6-.4-.8-.7a3 3 0 0 1-.4-1 5.4 5.4 0 0 1 0-2.3c0-.4.2-.7.4-1 .1-.3.4-.5.7-.7a2 2 0 0 1 1-.3 2 2 0 0 1 1.9 1l.4 1a5.4 5.4 0 0 1 0 2.3zm1.7-4.7a4 4 0 0 0-3.3-1.6c-.6 0-1.2.1-1.7.4a3 3 0 0 0-1.2 1.1V11h-2.6v13h2.7v-4.5c.3.4.7.8 1.2 1 .6.3 1.1.4 1.7.4a4 4 0 0 0 3.2-1.5c.4-.5.6-1 .8-1.6.2-.6.3-1.2.3-1.9s-.1-1.3-.3-2c-.2-.6-.4-1.2-.8-1.6zm-17.5 3.2l1.7-5 1.7 5h-3.4zm.2-8.2l-5 13.4h3l1-3h5l1 3h3L94 7.3h-3zm-5.3 9.1l-.6-.8-1-.5a11.6 11.6 0 0 0-2.3-.5l-1-.3a2 2 0 0 1-.6-.3.7.7 0 0 1-.3-.6c0-.2 0-.4.2-.5l.3-.3h.5l.5-.1c.5 0 .9 0 1.2.3.4.1.6.5.6 1h2.5c0-.6-.2-1.1-.4-1.5a3 3 0 0 0-1-1 4 4 0 0 0-1.3-.5 7.7 7.7 0 0 0-3 0c-.6.1-1 .3-1.4.5l-1 1a3 3 0 0 0-.4 1.5 2 2 0 0 0 1 1.8l1 .5 1.1.3 2.2.6c.6.2.8.5.8 1l-.1.5-.4.4a2 2 0 0 1-.6.2 2.8 2.8 0 0 1-1.4 0 2 2 0 0 1-.6-.3l-.5-.5-.2-.8H77c0 .7.2 1.2.5 1.6.2.5.6.8 1 1 .4.3.9.5 1.4.6a8 8 0 0 0 3.3 0c.5 0 1-.2 1.4-.5a3 3 0 0 0 1-1c.3-.5.4-1 .4-1.6 0-.5 0-.9-.3-1.2zM74.7 8h-2.6v3h-1.7v1.7h1.7v5.8c0 .5 0 .9.2 1.2l.7.7 1 .3a7.8 7.8 0 0 0 2 0h.7v-2.1a3.4 3.4 0 0 1-.8 0l-1-.1-.2-1v-4.8h2V11h-2V8zm-7.6 9v.5l-.3.8-.7.6c-.2.2-.7.2-1.2.2h-.6l-.5-.2a1 1 0 0 1-.4-.4l-.1-.6.1-.6.4-.4.5-.3a4.8 4.8 0 0 1 1.2-.2 8.3 8.3 0 0 0 1.2-.2l.4-.3v1zm2.6 1.5v-5c0-.6 0-1.1-.3-1.5l-1-.8-1.4-.4a10.9 10.9 0 0 0-3.1 0l-1.5.6c-.4.2-.7.6-1 1a3 3 0 0 0-.5 1.5h2.7c0-.5.2-.9.5-1a2 2 0 0 1 1.3-.4h.6l.6.2.3.4.2.7c0 .3 0 .5-.3.6-.1.2-.4.3-.7.4l-1 .1a21.9 21.9 0 0 0-2.4.4l-1 .5c-.3.2-.6.5-.8.9-.2.3-.3.8-.3 1.3s.1 1 .3 1.3c.1.4.4.7.7 1l1 .4c.4.2.9.2 1.3.2a6 6 0 0 0 1.8-.2c.6-.2 1-.5 1.5-1a4 4 0 0 0 .2 1H70l-.3-1v-1.2zm-11-6.7c-.2-.4-.6-.6-1-.8-.5-.2-1-.3-1.8-.3-.5 0-1 .1-1.5.4a3 3 0 0 0-1.3 1.2v-5h-2.7v13.4H53v-5.1c0-1 .2-1.7.5-2.2.3-.4.9-.6 1.6-.6.6 0 1 .2 1.3.6.3.4.4 1 .4 1.8v5.5h2.7v-6c0-.6 0-1.2-.2-1.6 0-.5-.3-1-.5-1.3zm-14 4.7l-2.3-9.2h-2.8l-2.3 9-2.2-9h-3l3.6 13.4h3l2.2-9.2 2.3 9.2h3l3.6-13.4h-3l-2.1 9.2zm-24.5.2L18 15.6c-.3-.1-.6-.2-.8.2A20 20 0 0 1 16 17c-.2.2-.4.3-.7.1-.4-.2-1.5-.5-2.8-1.7-1-1-1.7-2-2-2.4-.1-.4 0-.5.2-.7l.5-.6.4-.6v-.6L10.4 8c-.3-.6-.6-.5-.8-.6H9c-.2 0-.6.1-.9.5C7.8 8.2 7 9 7 10.7c0 1.7 1.3 3.4 1.4 3.6.2.3 2.5 3.7 6 5.2l1.9.8c.8.2 1.6.2 2.2.1.6-.1 2-.8 2.3-1.6.3-.9.3-1.5.2-1.7l-.7-.4zM14 25.3c-2 0-4-.5-5.8-1.6l-.4-.2-4.4 1.1 1.2-4.2-.3-.5A11.5 11.5 0 0 1 22.1 5.7 11.5 11.5 0 0 1 14 25.3zM14 0A13.8 13.8 0 0 0 2 20.7L0 28l7.3-2A13.8 13.8 0 1 0 14 0z" fill="currentColor" fill-rule="evenodd"></path></svg>
<div class="whatsappme__close">×</div>
</header>
<div class="whatsappme__message">Olá! Bilenio Imóveis?</div>
</div>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">//<![CDATA[
var wpcf7={"apiSettings":{"root":"https:\/\/bilenio.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Verifique se voc\u00ea n\u00e3o \u00e9 um rob\u00f4."}}};
//]]></script>
<script id="contact-form-7-js" src="https://bilenio.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.9.2" type="text/javascript"></script>
<script id="whatsappme-js" src="https://bilenio.com.br/wp-content/plugins/creame-whatsapp-me/public/js/whatsappme.js?ver=1.4.3" type="text/javascript"></script>
<script id="picturefill.min-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/picturefill.min.js?ver=1588258276" type="text/javascript"></script>
<script id="maskMoney-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/jquery.maskMoney.min.js?ver=1588258276" type="text/javascript"></script>
<script id="cookie-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/jquery.cookie.js?ver=1588258276" type="text/javascript"></script>
<script id="bxslider-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/bxslider.min.js?ver=1588258276" type="text/javascript"></script>
<script id="favorite-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/favorite.js?ver=1588258276" type="text/javascript"></script>
<script id="pro-scripts-js-extra" type="text/javascript">//<![CDATA[
var ajax_pro_params={"ajax_url":"https:\/\/bilenio.com.br\/wp-admin\/admin-ajax.php"};
//]]></script>
<script id="pro-scripts-js" src="https://bilenio.com.br/wp-content/themes/proimovel/cdn/pro-scripts.js?ver=1588258276" type="text/javascript"></script>
<script id="wp-embed-js" src="https://bilenio.com.br/wp-includes/js/wp-embed.min.js?ver=dc94a26a6319eec5479db0e1fe65af79" type="text/javascript"></script>
</body>
</html>