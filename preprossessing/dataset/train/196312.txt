<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<title>403 Forbidden</title>
<meta content="Copyright XSERVER Inc." name="copyright"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<meta content="width=device-width,initial-scale=1.0,minimum-scale=1.0" name="viewport"/>
<style type="text/css">
* {
    margin: 0;
    padding: 0;
}
img {
    border: 0;
}
ul {
    padding-left: 2em;
}
html {
    overflow-y: scroll;
    background: #3b79b7;
}
body {
    font-family: "¥á¥¤¥ê¥ª", Meiryo, "£Í£Ó £Ð¥´¥·¥Ã¥¯", "MS PGothic", "¥Ò¥é¥®¥Î³Ñ¥´ Pro W3", "Hiragino Kaku Gothic Pro", sans-serif;
    margin: 0;
    line-height: 1.4;
    font-size: 75%;
    text-align: center;
    color: white;
}
h1 {
    font-size: 24px;
    font-weight: bold;
}
h1 {
    font-weight: bold;
    line-height: 1;
    padding-bottom: 20px;
    font-family: Helvetica, sans-serif;
}
h2 {
    text-align: center;
    font-weight: bold;
    font-size: 27px;
}
p {
    text-align: center;
    font-size: 14px;
    margin: 0;
    padding: 0;
    color: white;
}
.explain {
    border-top: 1px solid #fff;
    border-bottom: 1px solid #fff;
    line-height: 1.5;
    margin: 30px auto;
    padding: 17px;
}
#cause {
    text-align: left;
}
#cause li {
    color: #666;
}
h3 {
    letter-spacing: 1px;
    font-weight: bold;
    padding: 0;
}
#white_box {
    margin: 15px auto 0;
    background-color: white;
}

/* ====================

  ¥¹¥Þ¡¼¥È¥Õ¥©¥ó

======================= */
@media only screen and (min-width: 0) and (max-width: 767px) {
	#base {
		padding: 30px 10px;
	}
	h1 {
		font-size: 26px;
	}
	h1 span {
		font-size: 60px;
	}
	h2 {
		font-size: 16px;
	}
	.explain {
		font-size: 14px;
	}
	h3 {
		margin-top: 45px;
		font-size: 16px;
	}
	#cause {
		padding: 20px;
		font-size: 12px;
	}
}

/* ====================

   ¥Ñ¥½¥³¥ó¡õ¥¿¥Ö¥ì¥Ã¥È

======================= */

@media only screen and (min-width: 768px) {
	#base {
		margin-top: 80px;
	}
	h1 {
		font-size: 50px;
	}
	h1 span {
		font-size: 110px;
	}
	.explain {
		font-size: 16px;
		width: 660px;
	}
	#white_box {
		width: 680px;
		margin-bottom: 50px;
	}
	h3 {
		font-size: 20px;
		margin-top: 80px;
	}
	#cause {
		padding: 30px;
		font-size: 14px;
	}
}
</style>
</head>
<body>
<div id="base">
<h1><span>403</span><br/>
        Forbidden</h1>
<h2>¥¢¥¯¥»¥¹¤·¤è¤¦¤È¤·¤¿¥Ú¡¼¥¸¤ÏÉ½¼¨¤Ç¤­¤Þ¤»¤ó¤Ç¤·¤¿¡£</h2>
<p class="explain">¤³¤Î¥¨¥é¡¼¤Ï¡¢É½¼¨¤¹¤ë¥Ú¡¼¥¸¤Ø¤Î¥¢¥¯¥»¥¹µö²Ä¤¬¤Ê¤«¤Ã¤¿¤³¤È¤ò°ÕÌ£¤·¤Þ¤¹¡£</p>
<h3>°Ê²¼¤Î¤è¤¦¤Ê¸¶°ø¤¬¹Í¤¨¤é¤ì¤Þ¤¹¡£</h3>
<div id="white_box">
<div id="cause">
<ul>
<li>¥¢¥¯¥»¥¹¤¬µö²Ä¤µ¤ì¤Æ¤¤¤Ê¤¤¡Ê¥Ñ¡¼¥ß¥Ã¥·¥ç¥óÅù¤Ë¤è¤Ã¤Æ¶Ø»ß¤µ¤ì¤Æ¤¤¤ë¡Ë¡£</li>
<li>¥Ç¥Õ¥©¥ë¥È¥É¥­¥å¥á¥ó¥È¡Êindex.html, index.htm Åù¡Ë¤¬Â¸ºß¤·¤Ê¤¤¡£</li>
</ul>
</div>
</div>
<!--//base--></div>
</body>
</html>
