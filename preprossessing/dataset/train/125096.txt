<!DOCTYPE html>
<html lang="en">
<head>
<title>Anitube</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Assistir Animes Online grátis nunca foi tão fácil, no Anitube você pode ver Naruto Shippuden, Bleach Online, todos os seus animes Legendados" name="description"/>
<meta content="pt_BR" property="og:locale"/>
<meta content="Anitube!" property="og:title"/>
<meta content="Assista animes online de graça em FULLHD no seu computador , Tablet , Smartfone , Celular , como dragon ball, naruto e one piece, Animes legendados e dublados online." property="og:description"/>
<meta content="https://www.anitube.in/image.jpg" property="og:image"/>
<meta content="" property="og:url"/>
<meta content="Anitube" property="og:site_name"/>
<meta content="ie=edge" http-equiv="X-UA-Compatible"/>
<link href="https://www.anitube.in" rel="canonical"/>
<link href="https://www.anitube.in/style.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
<link href="/manifest.json" rel="manifest"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="#00ccfa" name="theme-color"/>
</head>
<body>
<div class="searchContainer">
<form action="https://www.anitube.in/busca.php" method="GET">
<input name="s" placeholder="Busque seu anime favorito..." type="text"/>
<input name="submit" type="submit" value="Buscar"/>
</form>
</div>
<div class="mHeader">
<meta content="EBCE347DE747301ACB3AC119313FCAD3" name="msvalidate.01"/>
<div class="mwidth">
<div class="mNav">
<div class="headerBlockOne">
<div class="icoMenuContainer">
<i class="spr icoMenu"></i>
</div>
<div class="mMenu">
<ul>
<li><a href="https://www.anitube.in/">Home</a></li>
<li><a href="https://www.anitube.in/anime">Animes</a></li>
<li><a href="https://www.anitube.in/animes-dublado">Animes Dublados</a></li>
<li><a href="https://www.anitube.in/doramas">Doramas</a></li>
<li><a href="https://www.anitube.in/donghua">DongHuas</a></li>
<li><a href="https://www.anitube.in/tokusatsu">Tokusatsus</a></li>
<li><a href="https://www.anitube.in/generos">Gêneros</a></li>
<li><a href="https://www.anitube.in/contato.php">Contato</a></li>
<li class="on_mobile"><a href="https://www.anitube.in/cadastro.php">Cadastro</a></li>
<li class="on_mobile"><a href="https://www.anitube.in/login.php">Login</a></li>
</ul>
</div>
<a class="linkLogo" href="https://www.anitube.in" title="Anitube!">
<div class="logoHeader"></div>
</a>
</div>
<div class="headerBlockTwo">
<div class="searchButton">
<i class="spr icoSearch"></i>
</div>
</div>
</div>
<div class="slide_infos">
<h2>Dr. Stone</h2>
<div class="slide_generos">
<a href="#">Aventura</a>
<a href="#">Ficção</a>
<a href="#">Shounen</a>
</div>
<div class="slide_sinopse">
Um dia fatídico, toda a humanidade foi petrificada por um flash ofuscante de luz. Depois de vários milênio o colegial Taiju desperta e se vê perdido em um mundo de estátuas <a href="https://www.anitube.in/anime/dr-stonedr-stonedr-stone">Dr Stone Assistir Online</a>
</div>
</div>
</div> </div>
<div class="mContainer mwidth">
<div class="mContainer_titulo">
<div class="mContainer_titulo_content">
<span class="no_mobile">Anitube </span>Lançamentos
</div>
</div>
<div class="left">
<div class="mContainer_content threeItensPerContent">
<div class="epi_loop_item">
<a href="/video/119621" title="Tenchi Souzou Design-bu ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 12:39:43</div>
<div class="epi_loop_img_time">23:50</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Tenchi Souzou Design-bu ep 2" src="https://www.anitube.in/media/videos/tmb/119621/1.jpg" title="Tenchi Souzou Design-bu ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Tenchi Souzou Design-bu ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119619" title="Yuru Camp△ Season 2 ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 12:04:47</div>
<div class="epi_loop_img_time">23:40</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Yuru Camp△ Season 2 ep 2" src="https://www.anitube.in/media/videos/tmb/119619/1.jpg" title="Yuru Camp△ Season 2 ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Yuru Camp△ Season 2 ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119618" title="Dr. Stone: Stone Wars ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 11:36:45</div>
<div class="epi_loop_img_time">24:00</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Dr. Stone: Stone Wars ep 1" src="https://www.anitube.in/media/videos/tmb/119618/1.jpg" title="Dr. Stone: Stone Wars ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Dr. Stone: Stone Wars ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119601" title="Adachi to Shimamura ep 9">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:04:10</div>
<div class="epi_loop_img_time">24:04</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Adachi to Shimamura ep 9" src="https://www.anitube.in/media/videos/tmb/119601/1.jpg" title="Adachi to Shimamura ep 9"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Adachi to Shimamura ep 9 </div>
<div class="epi_loop_nome_sub tWrap">
EP 9 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119579" title="Yu-Gi-Oh! Sevens ep 31">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 19:45:54</div>
<div class="epi_loop_img_time">24:00</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Yu-Gi-Oh! Sevens ep 31" src="https://www.anitube.in/media/videos/tmb/119579/1.jpg" title="Yu-Gi-Oh! Sevens ep 31"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Yu-Gi-Oh! Sevens ep 31 </div>
<div class="epi_loop_nome_sub tWrap">
EP 31 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119578" title="Noblesse - Dublado ep 9">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 17:42:36</div>
<div class="epi_loop_img_time">23:44</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Noblesse - Dublado ep 9" src="https://www.anitube.in/media/videos/tmb/119578/1.jpg" title="Noblesse - Dublado ep 9"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Noblesse - Dublado ep 9 </div>
<div class="epi_loop_nome_sub tWrap">
EP 9 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119566" title="Nanatsu no Taizai: Fundo no Shinpan ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 15:46:42</div>
<div class="epi_loop_img_time">24:12</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Nanatsu no Taizai: Fundo no Shinpan ep 1" src="https://www.anitube.in/media/videos/tmb/119566/1.jpg" title="Nanatsu no Taizai: Fundo no Shinpan ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Nanatsu no Taizai: Fundo no Shinpan ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119554" title="Log Horizon: Entaku Houkai ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 15:00:06</div>
<div class="epi_loop_img_time">24:37</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Log Horizon: Entaku Houkai ep 1" src="https://www.anitube.in/media/videos/tmb/119554/1.jpg" title="Log Horizon: Entaku Houkai ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Log Horizon: Entaku Houkai ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119551" title="Kaifuku Jutsushi no Yarinaoshi ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 14:57:07</div>
<div class="epi_loop_img_time">24:39</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Kaifuku Jutsushi no Yarinaoshi ep 1" src="https://www.anitube.in/media/videos/tmb/119551/1.jpg" title="Kaifuku Jutsushi no Yarinaoshi ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Kaifuku Jutsushi no Yarinaoshi ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119510" title="I★Chu: Halfway Through the Idol ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 12:06:56</div>
<div class="epi_loop_img_time">24:00</div>
<div class="epi_loop_img_play spr"></div>
<img alt="I★Chu: Halfway Through the Idol ep 2" src="https://www.anitube.in/media/videos/tmb/119510/1.jpg" title="I★Chu: Halfway Through the Idol ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
I★Chu: Halfway Through the Idol ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119509" title="Re:Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 11:34:16</div>
<div class="epi_loop_img_time">27:29</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Re:Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 ep 2" src="https://www.anitube.in/media/videos/tmb/119509/1.jpg" title="Re:Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Re:Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119506" title="Yatogame-chan Kansatsu Nikki Sansatsume ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 00:18:37</div>
<div class="epi_loop_img_time">03:30</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Yatogame-chan Kansatsu Nikki Sansatsume ep 1" src="https://www.anitube.in/media/videos/tmb/119506/1.jpg" title="Yatogame-chan Kansatsu Nikki Sansatsume ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Yatogame-chan Kansatsu Nikki Sansatsume ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119451" title="Tatoeba Last Dungeon Mae no Mura no Shounen ga Joban no Machi de Kurasu Youna Monogatari ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-12 20:35:14</div>
<div class="epi_loop_img_time">24:11</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Tatoeba Last Dungeon Mae no Mura no Shounen ga Joban no Machi de Kurasu Youna Monogatari ep 2" src="https://www.anitube.in/media/videos/tmb/119451/1.jpg" title="Tatoeba Last Dungeon Mae no Mura no Shounen ga Joban no Machi de Kurasu Youna Monogatari ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Tatoeba Last Dungeon Mae no Mura no Shounen ga Joban no Machi de Kurasu Youna Monogatari ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119448" title="Shingeki no Kyojin: The Final Season - Dublado ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-12 18:39:43</div>
<div class="epi_loop_img_time">24:12</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Shingeki no Kyojin: The Final Season - Dublado ep 1" src="https://www.anitube.in/media/videos/tmb/119448/1.jpg" title="Shingeki no Kyojin: The Final Season - Dublado ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Shingeki no Kyojin: The Final Season - Dublado ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119444" title="Wonder Egg Priority ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-12 17:09:05</div>
<div class="epi_loop_img_time">23:02</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Wonder Egg Priority ep 1" src="https://www.anitube.in/media/videos/tmb/119444/1.jpg" title="Wonder Egg Priority ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Wonder Egg Priority ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
</div>
<div class="mContainer_pagination">
<div class="pagination">
<a class="page-numbers" href="/?page=1">« Anterior</a>
<a class="page-numbers " href="/?page=1">1</a>
<a class="page-numbers " href="/?page=2">2</a>
<a class="page-numbers " href="/?page=3">3</a>
<a class="page-numbers " href="/?page=4">4</a>
<a class="page-numbers " href="/?page=5">5</a>
<a class="page-numbers" href="/?page=2">Próximo »</a>
</div>
</div>
</div>
<div class="right">
<div class="widget">
<div class="widget_titulo">
ADS
</div>
<div class="widget_content">
<iframe allowtransparency="true" data-aa="1550572" scrolling="no" src="//acceptable.a-ads.com/1550572" style="border:0px; padding:0; width:100%; height:100%; overflow:hidden"></iframe>
</div>
</div>
<div class="widget">
<div class="widget_titulo">
ADS
</div>
<div class="widget_content">
<iframe allowtransparency="true" data-aa="1550572" scrolling="no" src="//acceptable.a-ads.com/1550572" style="border:0px; padding:0; width:100%; height:100%; overflow:hidden"></iframe>
</div>
</div> </div>
</div>
<div class="mContainer mwidth">
<div class="mContainer_titulo">
<div class="mContainer_titulo_content">
<span class="no_mobile">Sendo</span> Assistido Neste Momento
</div>
</div>
<div class="mContainer_content">
<div class="epi_loop_item">
<a href="/video/119621" title="Tenchi Souzou Design-bu ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 12:39:43</div>
<div class="epi_loop_img_time">23:50</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Tenchi Souzou Design-bu ep 2" src="https://www.anitube.in/media/videos/tmb/119621/1.jpg" title="Tenchi Souzou Design-bu ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Tenchi Souzou Design-bu ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119619" title="Yuru Camp△ Season 2 ep 2">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 12:04:47</div>
<div class="epi_loop_img_time">23:40</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Yuru Camp△ Season 2 ep 2" src="https://www.anitube.in/media/videos/tmb/119619/1.jpg" title="Yuru Camp△ Season 2 ep 2"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Yuru Camp△ Season 2 ep 2 </div>
<div class="epi_loop_nome_sub tWrap">
EP 2 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119618" title="Dr. Stone: Stone Wars ep 1">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-14 11:36:45</div>
<div class="epi_loop_img_time">24:00</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Dr. Stone: Stone Wars ep 1" src="https://www.anitube.in/media/videos/tmb/119618/1.jpg" title="Dr. Stone: Stone Wars ep 1"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Dr. Stone: Stone Wars ep 1 </div>
<div class="epi_loop_nome_sub tWrap">
EP 1 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119614" title="Overflow 08">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:27:21</div>
<div class="epi_loop_img_time">07:08</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Overflow 08" src="https://www.anitube.in/media/videos/tmb/119614/1.jpg" title="Overflow 08"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Overflow 08 </div>
<div class="epi_loop_nome_sub tWrap">
EP 08 </div>
</div>
</a>
</div>
</div>
</div>
<div class="mContainer mwidth">
<div class="mContainer_titulo">
<div class="mContainer_titulo_content">
Enviados Recentemente
</div>
</div>
<div class="mContainer_content">
<div class="epi_loop_item">
<a href="/video/119614" title="Overflow 08">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:27:21</div>
<div class="epi_loop_img_time">07:08</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Overflow 08" src="https://www.anitube.in/media/videos/tmb/119614/1.jpg" title="Overflow 08"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Overflow 08 </div>
<div class="epi_loop_nome_sub tWrap">
EP 08 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119612" title="Overflow 07">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:26:28</div>
<div class="epi_loop_img_time">07:10</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Overflow 07" src="https://www.anitube.in/media/videos/tmb/119612/1.jpg" title="Overflow 07"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Overflow 07 </div>
<div class="epi_loop_nome_sub tWrap">
EP 07 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119611" title="Overflow 06">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:25:05</div>
<div class="epi_loop_img_time">07:20</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Overflow 06" src="https://www.anitube.in/media/videos/tmb/119611/1.jpg" title="Overflow 06"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Overflow 06 </div>
<div class="epi_loop_nome_sub tWrap">
EP 06 </div>
</div>
</a>
</div>
<div class="epi_loop_item">
<a href="/video/119610" title="Overflow 05">
<div class="epi_loop_img">
<div class="epi_loop_img_data">2021-01-13 22:24:17</div>
<div class="epi_loop_img_time">06:31</div>
<div class="epi_loop_img_play spr"></div>
<img alt="Overflow 05" src="https://www.anitube.in/media/videos/tmb/119610/1.jpg" title="Overflow 05"/>
</div>
<div class="epi_loop_infos">
<div class="epi_loop_nome tWrap">
Overflow 05 </div>
<div class="epi_loop_nome_sub tWrap">
EP 05 </div>
</div>
</a>
</div>
</div>
</div>
<div class="mFooter">
<div class="logoFooterC">
<div class="logoFooter"></div>
<div class="textLogoFooter">
Copyright © 2009 - 2021 <a href="https://www.anitube.in">Anitube!
</a></div>
</div>
<div class="LinksFooter">
<a href="https://animesheaven.net/" target="_BLANK">animesheaven</a>
</div>
</div>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-164321367-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164321367-1');
   gtag('event', 'page_view');
</script>
<script src="https://www.anitube.in/js/main.js?v=2.0.0.0_debug" type="text/javascript"></script>
</body>
</html>