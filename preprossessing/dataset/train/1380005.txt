<!-- 
  _____  _______   _______     __              __                __             __            _____                          _
 / ____||__   __| |__   __|   /  \             \ \      __      / /  _______   | |           | ____ \    _______    ______  |_|   ______    _  ____
| /        | |       | |     / /\ \             \ \    /  \    / /  | ____  |  | |______     | |   \ |  | ____  |  / _____|  _   / ___  |  | |/ _  |
| |        | |       | |    / /__\ \             \ \  / /\ \  / /   | | ____|  |  ____  \    | |   | |  | | ____|  |______  | | | |   | |  | |/  | |
| |____  __| |__     | |   / /____\ \             \ \/ /  \ \/ /    | |_____   | |____| |    | |___/ |  | |_____    _____ \ | |  \____| |  | |   | |
 \_____||_______|    |_|  /_/      \_\             \__/    \__/     \_______|  |________|    |______/   \_______|  |______| |_|       | |  |_|   |_|
                                                                                                                                  ____/ /
                                                                                                                                 |_____/
--><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<base href="http://www.strctrading.com/"/>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="CITA - www.thecita.net" name="author"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>STRC Trading Co.,Ltd.  - www.strctrading.com</title>
<meta content="www.strctrading.com" name="keywords"/>
<meta content="" name="description"/>
<meta content="en_US" property="og:locale"/><meta content="http://www.strctrading.com/" property="og:url"/>
<meta content="http://www.strctrading.com/" property="twitter:url"/><meta content="http://www.strctrading.com/img/tab_logo.png" property="og:image"/>
<meta content="http://www.strctrading.com/img/tab_logo.png" property="twitter:image"/>
<!-- Place favicon.ico in the root directory -->
<link href="http://www.strctrading.com/files/site_description/1546485312strc_trading.png" rel="icon" type="image/png"/>
<link href="http://www.strctrading.com/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/plugins/owlcarousel/docs/assets/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/plugins/owlcarousel/docs/assets/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/plugins/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="http://www.strctrading.com/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="css/mdb.min.css" rel="stylesheet"/>
<link href="http://www.strctrading.com/css/custom.css?v2" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet"/>
<!-- JavaScript -->
<script src="http://www.strctrading.com/js/jquery-3.2.1.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="http://www.strctrading.com/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="http://www.strctrading.com/js/popper.min.js"></script>
<script src="http://www.strctrading.com/js/bootstrap.min.js"></script>
<style type="text/css">
        /*body{
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }*/
    </style>
</head>
<body class="white-skin" id="top-section">
<div class="loadingdiv spinner "></div>
<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
<script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        var BASE_URL = "http://www.strctrading.com/";
        var $BASE_URL = "http://www.strctrading.com/en/";
        var Lang = "en";
       
    </script>
<!--Scroll Top-->
<div class="btn-wrapper back-to-top show"><a class="btn btn-transparent" href="javascript:void(0);" style="outline: none;"><i class="fa fa-angle-up"></i></a></div>
<!--End Scroll Top-->
<header>
<div class="container-fluid px-0">
<div class="wrapper-top"></div>
<div class="container hidden-moblie ">
<div class="row align-items-start py-4">
<div class="col-6">
<a class="mobile_logo d-inline-block " href="http://www.strctrading.com/en/">
<img class="img-fluid " src="http://www.strctrading.com/files/site_description/1546485312strc_trading.png"/>
</a>
</div>
<div class="col-6 mt-2">
<div class="d-flex justify-content-end">
<div class="d-block"><img src="img/call.jpg"/></div>
<div class="ml-2 font-weight-bold">
<span style="color:#444444;">ASK OUR EXPERT</span><br/>
<span class="f-12" style="color:#ff6600;">017666618</span>
</div>
</div>
<form action="http://www.strctrading.com/en/search" class="d-flex mt-2">
<div class="input-group flex-nowrap justify-content-end search-group" style="height: 38px;">
<div class="input-group-prepend ">
<select class=" custom-select h-100" name="category" style="border-top-right-radius:0;border-bottom-right-radius:0;border-right:0;border-bottom-color: #ced4da;max-width: 180px;overflow: hidden;text-overflow: ellipsis;">
<option value="">All Categories</option>
<option value="cleaning-machine">Cleaning Machine. </option><option value="automatic-scrubbers">-  Automatic Scrubbers</option><option value="auto-burnisher">-  Auto Burnisher</option><option value="burnishers-cord-electric">-  Burnishers ( cord electric)</option><option value="wet-dry-vacuum">-  Wet &amp; Dry Vacuum</option><option value="carpet-extractor">-  Carpet Extractor</option><option value="dry-vacuum">-  Dry Vacuum</option><option value="floor-machine">-  Floor Machine</option><option value="sweepers">-  Sweepers</option><option value="electro-mist-4b">-  Electro-Mist 4B</option><option value="nss-video-demonstration">-  NSS Video demonstration</option><option value="cleaning-chemical">Cleaning  Chemical.</option><option value="bcl-housekeeping">-  BCL- Housekeeping</option><option value="bcl-laundry">-  BCL Laundry</option><option value="bcl-kitchen">-  BCL Kitchen</option><option value="canberra-chemical">Canberra Chemical </option><option value="check-mate-system">-  Check Mate System</option><option value="f-b-chemical-nsf-registed">-  F&amp;B Chemical , NSF Registed.</option><option value="disinfectant-chemical">-  Disinfectant chemical </option><option value="squeeze-pour-concentrates">-  Squeeze &amp; Pour Concentrates</option><option value="carpet-care">-  Carpet Care</option><option value="drain-opener">-  Drain Opener</option><option value="odor-eliminator">-  Odor Eliminator </option><option value="mold-mildew-control">-  Mold &amp; Mildew Control </option><option value="training-video">-  Training Video</option><option value="restroom-product">Restroom Product</option><option value="escalator-cleaning-machine">Escalator Cleaning Machine</option><option value="lavor-high-pressure">Lavor High Pressure                       </option><option value="escalator-cleaning-tool">Escalator Cleaning Tool.</option><option value="americo-pad">Americo Pad</option><option value="pest-control-supplies">Pest Control Supplies</option><option value="pest-control-machine">-  Pest Control Machine</option><option value="pest-control-chemical">-  Pest Control Chemical </option><option value="jaws-chemical">JAWS Chemical </option><option value="cleaning-tool">Cleaning Tool</option><option value="marble-granite-concrete-tools">Marble, Granite, Concrete Tools</option><option value="marble-polishing-equipment">-  Marble Polishing Equipment</option><option value="granite-polishing-equipment">-  Granite Polishing Equipment</option><option value="concrete-polishing-equipment">-  Concrete Polishing Equipment</option> </select>
</div>
<input class="form-control " name="txtsearch" placeholder="Enter your key search" style="border-left: 0 !important;height: 36px;padding: 0rem .75rem;" type="text" value=""/>
<div class="input-group-append ">
<button class="input-group-text btn btn-success m-0 z-depth-0" type="submit">
<i aria-hidden="true" class="fa fa-search"></i>  SEARCH
							    </button>
</div>
</div>
</form>
</div>
</div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark z-depth-0 py-0 mt-3 mt-lg-0">
<div class="container pos-rel p-0 rounded menu-bg">
<a class="hidden-lg-up" href="http://www.strctrading.com/en/">
<img class="img-fluid mobile_logo pb-1" src="http://www.strctrading.com/files/site_description/1546485312strc_trading.png"/>
</a>
<div>
<button class="navbar-toggler search-options-btn1" type="button">
<i class="fa fa fa-search" style="color: rgba(200, 28, 106, 0.9);"></i>
</button>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
</div>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav flex-wrap">
<li class="nav-item active ">
<a class="nav-link" href="http://www.strctrading.com/en/home">Home</a>
</li>
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
					          Categories
					        </a>
<ul class="dropdown-menu">
<li class="dropdown-submenu">
<a class="dropdown-item dropdown-toggle" href="http://www.strctrading.com/en/products/cleaning-machine">
						          Cleaning Machine.  <span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/automatic-scrubbers">Automatic Scrubbers</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/auto-burnisher">Auto Burnisher</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/burnishers-cord-electric">Burnishers ( cord electric)</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/wet-dry-vacuum">Wet &amp; Dry Vacuum</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/carpet-extractor">Carpet Extractor</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/dry-vacuum">Dry Vacuum</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/floor-machine">Floor Machine</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/sweepers">Sweepers</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/electro-mist-4b">Electro-Mist 4B</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-machine/nss-video-demonstration">NSS Video demonstration</a></li>
</ul>
</li>
<li class="dropdown-submenu">
<a class="dropdown-item dropdown-toggle" href="http://www.strctrading.com/en/products/cleaning-chemical">
						          Cleaning  Chemical. <span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-chemical/bcl-housekeeping">BCL- Housekeeping</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-chemical/bcl-laundry">BCL Laundry</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-chemical/bcl-kitchen">BCL Kitchen</a></li>
</ul>
</li>
<li class="dropdown-submenu">
<a class="dropdown-item dropdown-toggle" href="http://www.strctrading.com/en/products/canberra-chemical">
						          Canberra Chemical  <span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/check-mate-system">Check Mate System</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/f-b-chemical-nsf-registed">F&amp;B Chemical , NSF Registed.</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/disinfectant-chemical">Disinfectant chemical </a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/squeeze-pour-concentrates">Squeeze &amp; Pour Concentrates</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/carpet-care">Carpet Care</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/drain-opener">Drain Opener</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/odor-eliminator">Odor Eliminator </a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/mold-mildew-control">Mold &amp; Mildew Control </a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/canberra-chemical/training-video">Training Video</a></li>
</ul>
</li>
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/restroom-product">Restroom Product</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/escalator-cleaning-machine">Escalator Cleaning Machine</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/lavor-high-pressure">Lavor High Pressure                       </a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/escalator-cleaning-tool">Escalator Cleaning Tool.</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/americo-pad">Americo Pad</a></li>
<li class="dropdown-submenu">
<a class="dropdown-item dropdown-toggle" href="http://www.strctrading.com/en/products/pest-control-supplies">
						          Pest Control Supplies <span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/pest-control-supplies/pest-control-machine">Pest Control Machine</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/pest-control-supplies/pest-control-chemical">Pest Control Chemical </a></li>
</ul>
</li>
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/jaws-chemical">JAWS Chemical </a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/cleaning-tool">Cleaning Tool</a></li>
<li class="dropdown-submenu">
<a class="dropdown-item dropdown-toggle" href="http://www.strctrading.com/en/products/marble-granite-concrete-tools">
						          Marble, Granite, Concrete Tools <span class="caret"></span>
</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item" href="http://www.strctrading.com/en/products/marble-granite-concrete-tools/marble-polishing-equipment">Marble Polishing Equipment</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/marble-granite-concrete-tools/granite-polishing-equipment">Granite Polishing Equipment</a></li><li><a class="dropdown-item" href="http://www.strctrading.com/en/products/marble-granite-concrete-tools/concrete-polishing-equipment">Concrete Polishing Equipment</a></li>
</ul>
</li>
</ul>
</li>
<li class="nav-item ">
<a class="nav-link" href="http://www.strctrading.com/en/online-shop">Online Shop</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="http://www.strctrading.com/en/about-us">About US</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="http://www.strctrading.com/en/contact-us">Contact US</a>
</li> </ul>
</div>
</div>
</nav>
<nav class="navbar navbar-dark mobile-search" style="box-shadow: rgba(0, 0, 0, 0.09) 0px 2px 5px 0px, rgba(0, 0, 0, 0.09) 0px 1px 2px 0px !important;">
<div class="container pos-rel p-0 ">
<form action="http://www.strctrading.com/en/search" class="d-flex mt-2 w-100">
<div class="input-group flex-nowrap justify-content-end search-group" style="height: 38px;">
<select class=" custom-select h-100" name="category" style="border-top-right-radius:0;border-bottom-right-radius:0;border-right:0;border-bottom-color: #ced4da;max-width: 180px;overflow: hidden;text-overflow: ellipsis;">
<option value="">All Categories</option>
<option value="cleaning-machine">Cleaning Machine. </option><option value="automatic-scrubbers">-  Automatic Scrubbers</option><option value="auto-burnisher">-  Auto Burnisher</option><option value="burnishers-cord-electric">-  Burnishers ( cord electric)</option><option value="wet-dry-vacuum">-  Wet &amp; Dry Vacuum</option><option value="carpet-extractor">-  Carpet Extractor</option><option value="dry-vacuum">-  Dry Vacuum</option><option value="floor-machine">-  Floor Machine</option><option value="sweepers">-  Sweepers</option><option value="electro-mist-4b">-  Electro-Mist 4B</option><option value="nss-video-demonstration">-  NSS Video demonstration</option><option value="cleaning-chemical">Cleaning  Chemical.</option><option value="bcl-housekeeping">-  BCL- Housekeeping</option><option value="bcl-laundry">-  BCL Laundry</option><option value="bcl-kitchen">-  BCL Kitchen</option><option value="canberra-chemical">Canberra Chemical </option><option value="check-mate-system">-  Check Mate System</option><option value="f-b-chemical-nsf-registed">-  F&amp;B Chemical , NSF Registed.</option><option value="disinfectant-chemical">-  Disinfectant chemical </option><option value="squeeze-pour-concentrates">-  Squeeze &amp; Pour Concentrates</option><option value="carpet-care">-  Carpet Care</option><option value="drain-opener">-  Drain Opener</option><option value="odor-eliminator">-  Odor Eliminator </option><option value="mold-mildew-control">-  Mold &amp; Mildew Control </option><option value="training-video">-  Training Video</option><option value="restroom-product">Restroom Product</option><option value="escalator-cleaning-machine">Escalator Cleaning Machine</option><option value="lavor-high-pressure">Lavor High Pressure                       </option><option value="escalator-cleaning-tool">Escalator Cleaning Tool.</option><option value="americo-pad">Americo Pad</option><option value="pest-control-supplies">Pest Control Supplies</option><option value="pest-control-machine">-  Pest Control Machine</option><option value="pest-control-chemical">-  Pest Control Chemical </option><option value="jaws-chemical">JAWS Chemical </option><option value="cleaning-tool">Cleaning Tool</option><option value="marble-granite-concrete-tools">Marble, Granite, Concrete Tools</option><option value="marble-polishing-equipment">-  Marble Polishing Equipment</option><option value="granite-polishing-equipment">-  Granite Polishing Equipment</option><option value="concrete-polishing-equipment">-  Concrete Polishing Equipment</option> </select>
<input class="form-control m-0" name="txtsearch" placeholder="Enter your key search" style="border-left: 0 !important;height: 36px;padding: 0rem .75rem;" type="text" value=""/>
<div class="input-group-append ">
<button class="input-group-text btn btn-success m-0 z-depth-0 btn-search-mobile" type="submit">
<i aria-hidden="true" class="fa fa-search"></i>  SEARCH
							    </button>
</div>
</div>
</form>
</div>
</nav>
</div>
</header>
<!--___________________________SLIDER___________________________-->
<div class="container px-0 mt-3 wow fadeInRight z-depth-1">
<div class="owl-carousel owl-theme myslider owl-btn-vertical-center" id="myslider">
<div class="item view ">
<img src="http://www.strctrading.com/files/slide/1549969810nss slide.jpg"/>
</div><div class="item view ">
<img src="http://www.strctrading.com/files/slide/1549970096lavor1.jpg"/>
</div><div class="item view ">
<img src="http://www.strctrading.com/files/slide/1549970392bcl kitchne slide.jpg"/>
</div><div class="item view ">
<img src="http://www.strctrading.com/files/slide/1549970487BCL laundry slide.jpg"/>
</div><div class="item view ">
<img src="http://www.strctrading.com/files/slide/1559732118Step150.jpg"/>
</div><div class="item view ">
<img src="http://www.strctrading.com/files/slide/1570338689WeChat Image_20191006113404.jpg"/>
</div> </div>
</div>
<!--___________________________END SLIDER___________________________-->
<div class="container ">
<div class="row">
<div class="d-block w-100 px-3 px-sm-0">
<a class="s-title d-inline-block mt-4" href="http://www.strctrading.com/en/online-shop" style="color: #9394b0;">PRODUCTS CATEGORIES</a>
<hr class="my-2" style="background: #e9b0b0;"/>
</div>
</div>
<div class="row category">
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/cleaning-machine">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549522108NSS group 800x600.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Cleaning Machine. </p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/cleaning-chemical">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549877427BCL logo icon.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Cleaning  Chemical.</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/canberra-chemical">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1573835370new-jaws-group-pic.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Canberra Chemical </p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/restroom-product">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549880550images.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Restroom Product</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/escalator-cleaning-machine">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1559724712rotomac360_produkt.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Escalator Cleaning Machine</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/lavor-high-pressure">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1570093844lavor banner.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Lavor High Pressure                       </p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/escalator-cleaning-tool">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549880760escalator icon.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Escalator Cleaning Tool.</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/americo-pad">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549880477Americo banner1.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Americo Pad</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/pest-control-supplies">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1549876854pest control logo.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Pest Control Supplies</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/jaws-chemical">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1551083976JAWS icon.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">JAWS Chemical </p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/cleaning-tool">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1566556401cleaning-tools-500x500.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Cleaning Tool</p>
</div>
</div>
</a>
</div>
<div class="col-md-3 mb-3">
<a class="d-block" href="http://www.strctrading.com/en/products/marble-granite-concrete-tools">
<div class="overlay zoom z-depth-1">
<div class="view ">
<img class="img-fluid" src="http://www.strctrading.com/files/category/1570336981ECnnfmdU8AATudp.jpg"/>
<div class="mask rgba-black-strong waves-effect waves-light"></div>
</div>
<div class="card-body p-2 text-center">
<p class="font ">Marble, Granite, Concrete Tools</p>
</div>
</div>
</a>
</div>
</div>
</div>
<!--<div class="container " >
	<div class="row">
		<div class="d-block w-100 px-3 px-sm-0">
			<a href="http://www.strctrading.com/en/online-shop" class="s-title d-inline-block mt-4" style="color: #9394b0;">ONLINE PRODUCTS</a>
			<hr class="my-2" style="background: #e9b0b0;">
		</div>
	</div>
	
	<div class="row" >-->
<!--</div>
</div>-->
<div class="footer f-14 ">
<hr class="my-2" style="background: #e9b0b0;"/>
<div class="container-fluid" style="color:#616163;">
<div class="container ">
<div class="row py-3 ">
<div class="col-12 col-md-4 mb-4 mb-lg-1 ">
<div class=" px-0 px-sm-2">
<h5 class="font text-uppercase mb-4">Quick Link</h5>
<ul class="quick-link pt5 web_font_titles">
<li class="active"><a href="http://www.strctrading.com/en/home">Home</a></li><li><a href="http://www.strctrading.com/en/products">Categories</a></li><li><a href="http://www.strctrading.com/en/online-shop">Online Shop</a></li><li><a href="http://www.strctrading.com/en/about-us">About US</a></li><li><a href="http://www.strctrading.com/en/contact-us">Contact US</a></li>
</ul>
</div>
</div>
<div class="col-12 col-md-4 mb-4 mb-lg-1 ">
<div class=" px-0 px-sm-2">
<h5 class="font text-uppercase mb-4">Contact Us</h5>
<div class="flex mb-1">
<i aria-hidden="true" class="fa fa-map"></i>
<span style="margin:0 10px 5px">
								#169, Street 105, Toultumpong, Chamkamorn, Phnom Penh, Kingdom of Cambodia.							</span>
</div>
<div class="flex mb-1">
<i aria-hidden="true" class="fa fa-phone"></i>
<p style="margin:0 10px 5px">
								017666618							</p>
</div>
<div class="flex mb-1">
<i aria-hidden="true" class="fa fa-envelope"></i>
<p style="margin:0 10px 5px">
<a href="mailto:ky@strctrading.com">ky@strctrading.com</a>
</p>
</div>
</div>
</div>
<div class="col-12 col-md-4 mb-4 mb-lg-1 ">
<div class=" px-0 px-sm-2 f-counter">
<h5 class="font text-uppercase mb-4">Find Us On Facebook</h5>
<div class="facebookpage ">
<div class="fb-page" data-adapt-container-width="true" data-hide-cover="false" data-href="https://www.facebook.com/strctrading" data-show-facepile="true" data-small-header="false">
<blockquote cite="https://www.facebook.com/strctrading" class="fb-xfbml-parse-ignore">
<a href="https://www.facebook.com/strctrading"></a>
</blockquote></div> </div>
</div>
</div>
</div>
</div>
</div>
<hr class="m-0" style="background: #e9b0b0;"/>
<div class="container-fluid" style="background: #f9f9f9;">
<div class="container ">
<div class="d-md-flex align-items-center justify-content-center py-3 py-sm-2 font-weight-bold">
<div class=" text-center">
<span>Copyrights © 2019 by STRC Trading. All Rights Reserved. </span>
</div>
<div class="ml-auto text-center">
<a href="http://www.thecita.net/" target="_blank">Web Design : CITA</a>
</div>
</div>
</div>
</div>
</div>
<!-- 
 ****** CITA ******
 * CODING ID: SKS0017  
-->
<script src="http://www.strctrading.com/js/mdb.min.js"></script>
<script src="http://www.strctrading.com/plugins/jquery-ui/jquery.ui.touch-punch.min.js"></script>
<script src="http://www.strctrading.com/plugins/owlcarousel/docs/assets/owlcarousel/owl.carousel.js"></script>
<script src="http://www.strctrading.com/plugins/bootstrap-sweetalert-master/dist/sweetalert.js"></script>
<script src="http://www.strctrading.com/js/jquery.validate.js"></script>
<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a598539e3fb0a99" type="text/javascript"></script>
<script src="http://www.strctrading.com/js/custom.js?v1"></script>
</body>
</html>
