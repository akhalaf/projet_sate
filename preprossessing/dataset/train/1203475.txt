<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="bs-BA"> <!--<![endif]-->
<head>
<!-- Basic Page Needs
  ================================================== -->
<meta charset="utf-8"/>
<title>Stranica nije pronađena | Računovodstvo</title>
<meta content="Računovodstvo - Računovodstvo 2017" name="description"/>
<meta content="" name="keywords"/>
<meta content="Računovodstvo" name="author"/>
<!-- Mobile Specific Metas
  ================================================== -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<!-- Modernizer
  ================================================== -->
<!--[if lt IE 9]>
	<script src="https://racunovodstvo.blc.edu.ba/wp-content/themes/mexin-wp/js/modernizr.custom.11889.js" type="text/javascript"></script>
	<script src="https://racunovodstvo.blc.edu.ba/wp-content/themes/mexin-wp/js/respond.js" type="text/javascript"></script>
	<![endif]-->
<!-- HTML5 Shiv events (end)-->
<!-- MEGA MENU -->
<!-- Favicons
  ================================================== -->
<link href="https://racunovodstvo.blc.edu.ba/wp-content/uploads/2012/12/logo1.gif" rel="shortcut icon"/>
<!-- CSS + JS
  ================================================== -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://racunovodstvo.blc.edu.ba/ba/feed/" rel="alternate" title="Računovodstvo » novosti" type="application/rss+xml"/>
<link href="https://racunovodstvo.blc.edu.ba/ba/comments/feed/" rel="alternate" title="Računovodstvo »  novosti o komentarima" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/racunovodstvo.blc.edu.ba\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider.css?ver=6.1.0" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz&amp;ver=5.5.3" id="css3_grid_font_yanone-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/css3_web_pricing_tables_grids/table1/css3_grid_style.css?ver=5.5.3" id="css3_grid_table1_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/css3_web_pricing_tables_grids/table2/css3_grid_style.css?ver=5.5.3" id="css3_grid_table2_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/css3_web_pricing_tables_grids/responsive.css?ver=5.5.3" id="css3_grid_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.3.0.2" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://racunovodstvo.blc.edu.ba/wp-content/themes/mexin-wp/css/master-min.php?ver=5.5.3" id="main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C900%7CRoboto%7CRoboto+Slab%3A300%2C400&amp;ver=5.5.3" id="gfont-style-css" media="all" rel="stylesheet" type="text/css"/>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script id="greensock-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/LayerSlider/static/layerslider/js/greensock.js?ver=1.19.0" type="text/javascript"></script>
<script id="jquery-core-js" src="https://racunovodstvo.blc.edu.ba/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="layerslider-js-extra" type="text/javascript">
/* <![CDATA[ */
var LS_Meta = {"v":"6.1.0"};
/* ]]> */
</script>
<script id="layerslider-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.1.0" type="text/javascript"></script>
<script id="layerslider-transitions-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.transitions.js?ver=6.1.0" type="text/javascript"></script>
<script id="tp-tools-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.3.0.2" type="text/javascript"></script>
<script id="revmin-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.3.0.2" type="text/javascript"></script>
<meta content="Powered by LayerSlider 6.1.0 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." name="generator"/>
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<link href="https://racunovodstvo.blc.edu.ba/ba/wp-json/" rel="https://api.w.org/"/><link href="https://racunovodstvo.blc.edu.ba/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://racunovodstvo.blc.edu.ba/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style type="text/css">
.qtranxs_flag_ba {background-image: url(https://racunovodstvo.blc.edu.ba/wp-content/plugins/qtranslate-x/flags/ba.png); background-repeat: no-repeat;}
.qtranxs_flag_en {background-image: url(https://racunovodstvo.blc.edu.ba/wp-content/plugins/qtranslate-x/flags/gb.png); background-repeat: no-repeat;}
</style>
<meta content="qTranslate-X 3.4.6.8" name="generator"/>
<style media="screen" type="text/css">body{  } </style><meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://racunovodstvo.blc.edu.ba/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta content="Powered by Slider Revolution 5.3.0.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 default-header wpb-js-composer js-comp-ver-4.12.1 vc_responsive" data-rsssl="1">
<!-- Primary Page Layout
	================================================== -->
<div class="colorskin- " id="wrap">
<section class="top-bar">
<div class="container"><div class="top-links lftflot"> <h6><i class="fa-envelope-o"></i> blc@teol.net</h6> <h6><i class="fa-phone"></i> 00 387 (0) 51 433 010</h6>
</div>
<div class="socialfollow rgtflot"><a class="facebook" href="https://www.facebook.com/banjaluka.college/"><i class="fa-facebook"></i></a><a class="twitter" href="https://twitter.com/Blcollege"><i class="fa-twitter"></i></a><a class="youtube" href="http://www.youtube.com/user/banjalukacollege"><i class="fa-youtube"></i></a><a class="google" href=""><i class="fa-google"></i></a><a class="instagram" href=""><i class="fa-instagram"></i></a></div>
</div>
</section>
<header class="horizontal-w sm-rgt-mn " id="header">
<div class="container">
<div class="col-md-5 logo-wrap"> <div class="logo">
<a href="https://racunovodstvo.blc.edu.ba/ba/"><img alt="logo" class="img-logo-w1" id="img-logo-w1" src="https://racunovodstvo.blc.edu.ba/wp-content/uploads/2017/03/Logo-olimpijada2016-1.png" width="300"/></a><a href="https://racunovodstvo.blc.edu.ba/ba/"><img alt="logo" class="img-logo-w2" id="img-logo-w2" src="https://racunovodstvo.blc.edu.ba/wp-content/uploads/2012/12/logora1.png" width="120"/></a><span class="logo-sticky"><a href="https://racunovodstvo.blc.edu.ba/ba/"><img alt="logo" class="img-logo-w3" id="img-logo-w3" src="https://racunovodstvo.blc.edu.ba/wp-content/uploads/2017/03/Logo-olimpijada2016-1.png" width="120"/></a></span> </div></div>
<div class="col-md-7 alignright"><hr class="vertical-space"/> <h6><i class="fa-envelope-o"></i> blc@teol.net</h6>
<h6><i class="fa-phone"></i> 00 387 (0) 51 433 010</h6>
</div>
</div>
<hr class="vertical-space"/>
<nav class="nav-wrap2 mn4 darknavi" id="nav-wrap">
<div class="container">
<ul id="nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1404" id="menu-item-1404"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/">Početna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-1452" id="menu-item-1452"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/novosti/">Novosti</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155" id="menu-item-155"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/o-nama/">O nama</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-339" id="menu-item-339"><a data-description="" href="#olimp">Mini Olimpijada 2019</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-233" id="menu-item-233"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/poziv-za-ucesce/">Poziv za učešće</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-248" id="menu-item-248"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/olimpijada-2017/propozicije/">Propozicije</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1984" id="menu-item-1984"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/organizacioni-odbor/">Organizacioni odbor</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-139" id="menu-item-139"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/olimpijada-2017/literatura/">Literatura</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-140" id="menu-item-140"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/olimpijada-2017/prijava-2/">Prijava</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-645" id="menu-item-645"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/olimpijada-2017/prijavljeni-ucesnici/">Prijavljeni učesnici</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-528" id="menu-item-528"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/olimpijada-2017/uputstvo-za-ucesnike/">Program za učesnike</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143" id="menu-item-143"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/smjestaj/">Smještaj</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-761" id="menu-item-761"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/primjeri-testova-za-olimpijadu-2013/">Olimpijada 2013. Test pitanja i odgovori – primjeri</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1864" id="menu-item-1864"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/"><i class="fa-group"></i>Naučni skup 2020</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1874" id="menu-item-1874"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/poziv-za-ucesce/">Poziv za učešće</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1872" id="menu-item-1872"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/program-za-ucesnike/">Program za učesnike</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1873" id="menu-item-1873"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/lista-oblasti/">Lista oblasti</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1876" id="menu-item-1876"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/uputstvo-za-autore/">Uputstvo za autore</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1958" id="menu-item-1958"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/organizacioni-odbor/">Organizacioni odbor</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1962" id="menu-item-1962"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-odbor-naucnog-skupa/">Naučni odbor naučnog skupa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2122" id="menu-item-2122"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/recenzenti/">Recenzenti</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2118" id="menu-item-2118"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/kotizacija/">Kotizacija</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1963" id="menu-item-1963"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/vazni-datumi-za-naucni-skup/">Važni datumi za naučni skup</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1875" id="menu-item-1875"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/naucni-skup-2020/prijava/"><i class="fa-check-square"></i>Prijava</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144" id="menu-item-144"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/sponzori/">Sponzori</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-129" id="menu-item-129"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/galerija/">Galerija</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-124" id="menu-item-124"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/arhiva/">Arhiva</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-125" id="menu-item-125"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/arhiva/poziv-za-ucesce-2011/">Poziv za učešće 2011</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-126" id="menu-item-126"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/arhiva/poziv-za-ucesce-2012/">Poziv za učešće 2012</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-128" id="menu-item-128"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/arhiva/rezultati-2011-i-2012/">Rezultati 2011 i 2012</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a data-description="" href="https://racunovodstvo.blc.edu.ba/ba/kontakt/">Kontakt</a></li>
<li class="qtranxs-lang-menu qtranxs-lang-menu-ba menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1980" id="menu-item-1980"><a data-description="" href="#" title="Srpski">Jezik: <img alt="Srpski" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/qtranslate-x/flags/ba.png"/></a>
<ul class="sub-menu">
<li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-ba menu-item menu-item-type-custom menu-item-object-custom menu-item-2123" id="menu-item-2123"><a data-description="" href="https://racunovodstvo.blc.edu.ba/wp-admin/js/li.php" title="Srpski"><img alt="Srpski" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/qtranslate-x/flags/ba.png"/> Srpski</a></li>
<li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-2124" id="menu-item-2124"><a data-description="" href="https://racunovodstvo.blc.edu.ba/wp-admin/js/li.php" title="English"><img alt="English" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/qtranslate-x/flags/gb.png"/> English</a></li>
</ul>
</li>
</ul> </div>
</nav>
<!-- /nav-wrap -->
</header>
<!-- end-header --><section class="tbg1" id="hero">
<div class="blox dark dark">
<div class="container alignleft">
<h1 class="pnf404">404</h1>
<h2 class="pnf404">Page Not Found</h2>
<br/>
<div>
           <form action="https://racunovodstvo.blc.edu.ba/ba/" method="get" role="search">
<div>
<input class="search-side" name="s" placeholder="Enter Keywords..." type="text"/>
</div>
</form>
</div>
<br class="clear"/>
<p> </p>
<h3>We're sorry, but the page you were looking for doesn't exist.</h3>
</div>
</div>
</section>
<footer id="footer">
<section class="container footer-in">
<div class="col-md-4"></div>
<div class="col-md-4"></div>
<div class="col-md-4"></div> </section>
<!-- end-footer-in -->
<section class="footbot">
<div class="container">
<div class="col-md-6">
<!-- footer-navigation /end -->
<div class="footer-navi">
</div>
</div>
<div class="col-md-6">
<!-- footer-navigation /end -->
<img alt="" src="" width="65"/>
</div>
</div>
</section> <!-- end-footbot -->
</footer>
<!-- end-footer -->
<span id="scroll-top"><a class="scrollup"><i class="fa-chevron-up"></i></a></span></div>
<!-- end-wrap -->
<!-- End Document
================================================== -->
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/racunovodstvo.blc.edu.ba\/ba\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://racunovodstvo.blc.edu.ba/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2" type="text/javascript"></script>
<script id="doubletab-js" src="https://racunovodstvo.blc.edu.ba/wp-content/themes/mexin-wp/js/jquery.plugins.js" type="text/javascript"></script>
<script id="custom_script-js" src="https://racunovodstvo.blc.edu.ba/wp-content/themes/mexin-wp/js/mexin-custom.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://racunovodstvo.blc.edu.ba/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>