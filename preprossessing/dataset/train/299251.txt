<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>
		   Community Energy Development Co-operative - Green Energy	</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<link href="https://cedco-op.com/wp-content/themes/co-op/favicon.ico" rel="shortcut icon"/>
<link href="https://cedco-op.com/xmlrpc.php" rel="pingback"/>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="The CED Co-op develops professional investment opportunities by supporting distributed power generation projects across Ontario." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://cedco-op.com/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Community Energy Development Co-operative - Green Energy" property="og:title"/>
<meta content="The CED Co-op develops professional investment opportunities by supporting distributed power generation projects across Ontario." property="og:description"/>
<meta content="https://cedco-op.com/" property="og:url"/>
<meta content="CED Co-op" property="og:site_name"/>
<meta content="2017-05-31T18:48:33+00:00" property="article:modified_time"/>
<meta content="https://cedco-op.com/wp-content/uploads/2016/02/main-header2.jpg" property="og:image"/>
<meta content="Est. reading time" name="twitter:label1"/>
<meta content="0 minutes" name="twitter:data1"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://cedco-op.com/#website","url":"https://cedco-op.com/","name":"CED Co-op","description":"","potentialAction":[{"@type":"SearchAction","target":"https://cedco-op.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"ImageObject","@id":"https://cedco-op.com/#primaryimage","inLanguage":"en-US","url":"https://cedco-op.com/wp-content/uploads/2016/02/main-header2.jpg","width":1600,"height":500},{"@type":"WebPage","@id":"https://cedco-op.com/#webpage","url":"https://cedco-op.com/","name":"Community Energy Development Co-operative - Green Energy","isPartOf":{"@id":"https://cedco-op.com/#website"},"primaryImageOfPage":{"@id":"https://cedco-op.com/#primaryimage"},"datePublished":"2016-02-25T02:12:46+00:00","dateModified":"2017-05-31T18:48:33+00:00","description":"The CED Co-op develops professional investment opportunities by supporting distributed power generation projects across Ontario.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://cedco-op.com/"]}]}]}</script>
<meta content="8PiKkJWjljtA0F8mVHEE2C3RXtyXX_MpdwSXVcNBH8E" name="google-site-verification"/>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cedco-op.com/feed/" rel="alternate" title="CED Co-op » Feed" type="application/rss+xml"/>
<link href="https://cedco-op.com/comments/feed/" rel="alternate" title="CED Co-op » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cedco-op.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" id="bootstrap-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/ced-coop-sync/css/custom.css" id="cedcoopsync-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/uploads/shadowbox-js/src/shadowbox.css" id="shadowbox-css-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/shadowbox-js/css/extras.css" id="shadowbox-extras-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/contact-form-7/includes/css/styles.css" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/jquery-collapse-o-matic/light_style.css" id="collapseomatic-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/wp-responsive-menu-pro/css/wprmenu.css" id="wprmenu.css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/wp-responsive-menu-pro/inc/icons/style.css" id="wpr_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/wishlist-member/ui/css/frontend.css" id="wlm3_frontend_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/wp-paginate/css/wp-paginate.css" id="wp-paginate-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/royal-slider/lib/royalslider/royalslider.css" id="new-royalslider-core-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cedco-op.com/wp-content/plugins/royal-slider/lib/royalslider/skins/universal/rs-universal.css" id="rsUni-css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-script-js" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
<script id="bootstrap-script-js" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" type="text/javascript"></script>
<script id="jquery-core-js" src="https://cedco-op.com/wp-includes/js/jquery/jquery.min.js" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://cedco-op.com/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script id="jquery.transit-js" src="https://cedco-op.com/wp-content/plugins/wp-responsive-menu-pro/js/jquery.transit.min.js" type="text/javascript"></script>
<script id="sidr-js" src="https://cedco-op.com/wp-content/plugins/wp-responsive-menu-pro/js/jquery.sidr.js" type="text/javascript"></script>
<script id="wprmenu.js-js-extra" type="text/javascript">
/* <![CDATA[ */
var wprmenu = {"zooming":"yes","from_width":"1024","parent_click":"no","swipe":"yes","submenu_open_icon":"wpr-icon-arrow-right9","submenu_close_icon":"wpr-icon-arrow-down9","submenu_display":"no"};
/* ]]> */
</script>
<script id="wprmenu.js-js" src="https://cedco-op.com/wp-content/plugins/wp-responsive-menu-pro/js/wprmenu.js" type="text/javascript"></script>
<link href="https://cedco-op.com/wp-json/" rel="https://api.w.org/"/><link href="https://cedco-op.com/wp-json/wp/v2/pages/1022" rel="alternate" type="application/json"/><link href="https://cedco-op.com/" rel="shortlink"/>
<link href="https://cedco-op.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcedco-op.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://cedco-op.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcedco-op.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<meta content="Custom Login v3.2.11" name="generator"/>
<!-- Running WishList Member v3.8.7381 -->
<style type="text/css">
/* CSS Code for the Registration Form */

/* The Main Registration Form Table */
.wpm_registration{
	clear:both;
	padding:0;
	margin:10px 0;
}
.wpm_registration td{
	text-align:left;
}
/*CSS for Existing Members Login Table*/
.wpm_existing{
	clear:both;
	padding:0;
	margin:10px 0;
}
/* CSS for Registration Error Messages */
p.wpm_err{
	color:#f00;
	font-weight:bold;
}

/* CSS for custom message sent to registration url */
p.wlm_reg_msg_external {
	border: 2px dotted #aaaaaa;
	padding: 10px;
	background: #fff;
	color: #000;
}

/* CSS Code for the Registration Instructions Box */

/* The Main Instructions Box */
div#wlmreginstructions{
	background:#ffffdd;
	border:1px solid #ff0000;
	padding:0 1em 1em 1em;
	margin:0 auto 1em auto;
	font-size:1em;
	width:450px;
	color:#333333;
}

/* Links displayed in the Instructions Box */
#wlmreginstructions a{
	color:#0000ff;
	text-decoration:underline;
}

/* Numbered Bullets in the Instructions Box */
#wlmreginstructions ol{
	margin:0 0 0 1em;
	padding:0 0 0 1em;
	list-style:decimal;
	background:none;
}

/* Each Bullet Entry */
#wlmreginstructions li{
	margin:0;
	padding:0;
	background:none;
}


/* The Main Widget Enclosure */
.WishListMember_Widget{ }


/* The Main Login Merge Code Enclosure */
.WishListMember_LoginMergeCode{ }
</style> <style id="wprmenu_css" type="text/css">
			/* apply appearance settings */
			#wprmenu_bar {
				background: #10542b;
			}
			#wprmenu_bar .menu_title, #wprmenu_bar .wprmenu_icon_menu, .toggle-search i {
				color: #F2F2F2;
			}
			#wprmenu_menu, .search-expand {
				background: #2E2E2E!important;
			}
			#wprmenu_menu.wprmenu_levels ul li {
				border-bottom:1px solid #131212;
				border-top:1px solid #0D0D0D;
			}
			#wprmenu_menu ul li a {
				color: #CFCFCF;
			}
			.wpr_search button.wpr_submit,.toggle-search {
				color: ;
			}
			#wprmenu_menu ul li a:hover {
				color: #606060;
			}
			#wprmenu_menu.wprmenu_levels a.wprmenu_parent_item {
				border-left:1px solid #0D0D0D;
			}
			#wprmenu_menu .wprmenu_icon_par {
				color: #CFCFCF;
			}
			#wprmenu_menu .wprmenu_icon_par:hover {
				color: #606060;
			}
			#wprmenu_menu.wprmenu_levels ul li ul {
				border-top:1px solid #131212;
			}
			#wprmenu_bar .wprmenu_icon i {
				color: #FFFFFF;
				font-size: 26px;
				line-height: 20px;
			}
			.wpr_social_icons a i {
				color: 			}
			.wpr_social_icons{
				border-bottom:2px solid #131212;
				border-top:1px solid #0D0D0D;
			}
			
			#wprmenu_menu.left {
				width:80%;
				left: -80%;
			    right: auto;
			}
			#wprmenu_menu.right {
			    width:80%;
			    right: -80%;
			    left: auto;
			}

							#wprmenu_bar .wprmenu_icon {
					float: right!important;
					margin-right:0px!important;
				}
				#wprmenu_bar .bar_logo {
					pading-left: 0px;
				}
			

			/* show the bar and hide othere navigation elements */
			@media only screen and (max-width: 1024px) {
				html { padding-top: 42px!important; }
				#wprmenu_bar { display: block!important; }
				div#wpadminbar { position: fixed; }
				nav { display:none!important; }			}
		</style>
<!--[if lt IE 7]> <meta id="intense-browser-check" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]> <meta id="intense-browser-check" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]> <meta id="intense-browser-check" class="no-js ie8 oldie"> <![endif]-->
<!--[if IE 9]> <meta id="intense-browser-check" class="no-js ie9 oldie"> <![endif]-->
<!--[if gt IE 9]><!--> <meta class="no-js" id="intense-browser-check"/> <!--<![endif]--><style type="text/css">.intense.pagination .active > a { 
  background-color: #1a8be2; 
  border-color: #006dc4; 
  color: #fff;  } 
/* custom css styles */
                                    

</style><!--[if lt IE 9]><script src="https://cedco-op.com/wp-content/plugins/intense/assets/js/respond/dest/respond.min.js"></script><![endif]--> <!--[if lt IE 7]>
<script language="JavaScript">
function correctPNG() // correctly handle PNG transparency in Win IE 5.5 & 6.
{
   var arVersion = navigator.appVersion.split("MSIE")
   var version = parseFloat(arVersion[1])
   if ((version >= 5.5) && (document.body.filters)) 
   {
      for(var i=0; i<document.images.length; i++)
      {
         var img = document.images[i]
         var imgName = img.src.toUpperCase()
         if (imgName.substring(imgName.length-3, imgName.length) == "PNG")
         {
            var imgID = (img.id) ? "id='" + img.id + "' " : ""
            var imgClass = (img.className) ? "class='" + img.className + "' " : ""
            var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
            var imgStyle = "display:inline-block;" + img.style.cssText 
            if (img.align == "left") imgStyle = "float:left;" + imgStyle
            if (img.align == "right") imgStyle = "float:right;" + imgStyle
            if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
            var strNewHTML = "<span " + imgID + imgClass + imgTitle
            + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
            + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
            + "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>" 
            img.outerHTML = strNewHTML
            i = i-1
         }
      }
   }    
}
window.attachEvent("onload", correctPNG);
</script>
<![endif]-->
<!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" href="https://cedco-op.com/wp-content/themes/co-op/css/ie.css" />
<![endif]-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,600italic,700,700italic,800,800italic,300italic" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- remove responsive formatting <meta name="viewport" content="width=1000"> -->
<link href="https://cedco-op.com/wp-content/themes/co-op/css/animate.css" rel="stylesheet"/>
<script charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script charset="utf-8" src="https://cedco-op.com/wp-content/themes/co-op/fonts/easytabs.js" type="text/javascript"></script>
<script src="https://cedco-op.com/wp-content/themes/co-op/js/wow.min.js"></script>
<script>
 new WOW().init();
</script>
<link href="https://cedco-op.com/wp-content/themes/co-op/style.css" rel="stylesheet"/>
</head>
<body class="home page-template page-template-page-main2 page-template-page-main2-php page page-id-1022">
<header>
<div id="page-wrap">
<div id="logo"><a href="https://cedco-op.com/"><img alt="CED Co-op" height="96" src="https://cedco-op.com/wp-content/themes/co-op/images/ced-co-op-logo.png" width="373"/></a></div>
<nav>
<div class="menu-menu-1-container"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912" id="menu-item-912"><a href="https://cedco-op.com/invest/">Invest</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113" id="menu-item-113"><a href="https://cedco-op.com/membership/">Membership</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194" id="menu-item-194"><a href="https://cedco-op.com/projects/">Projects</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023" id="menu-item-1023"><a href="https://cedco-op.com/recent-news">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1756" id="menu-item-1756"><a href="https://cedco-op.com/wp-login.php">Members</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112" id="menu-item-112"><a href="https://cedco-op.com/connect/">Connect</a></li>
</ul></div> </nav>
</div>
</header>
<div id="banner-main">
<div id="banner-img">
<p><img alt="" class="aligncenter wp-image-1027 size-full" height="500" loading="lazy" sizes="(max-width: 1600px) 100vw, 1600px" src="https://cedco-op.com/wp-content/uploads/2016/02/main-header2.jpg" srcset="https://cedco-op.com/wp-content/uploads/2016/02/main-header2.jpg 1600w, https://cedco-op.com/wp-content/uploads/2016/02/main-header2-300x94.jpg 300w, https://cedco-op.com/wp-content/uploads/2016/02/main-header2-768x240.jpg 768w, https://cedco-op.com/wp-content/uploads/2016/02/main-header2-960x300.jpg 960w" width="1600"/></p>
</div>
</div>
<div id="banner-message"><div id="page-wrap"><h2>Making it easy to earn superior returns by investing in renewable energy generation</h2>
<h3>Community Energy Development Co-operative develops professional investment opportunities by supporting distributed power generation projects across Ontario.</h3></div></div>
<div id="features"><div id="page-wrap">
<div id="feature"><a href="https://cedco-op.com/invest">
<img alt="Invest" src="https://cedco-op.com/wp-content/themes/co-op/images/invest-main-icon.png"/>
<h2>Invest in our co-op</h2>
<p>Strengthen your portfolio and see strong returns through stable investments that align with your values.</p></a>
</div>
<div id="feature"><a href="https://cedco-op.com/membership">
<img alt="Join" src="https://cedco-op.com/wp-content/themes/co-op/images/join-main-icon.png"/>
<h2>Join Our Co-op</h2>
<p>Join over 750 members supporting renewable energy and take an active role in reducing climate change.</p></a>
</div>
<div id="feature"><a href="https://cedco-op.com/projects">
<img alt="Projects" src="https://cedco-op.com/wp-content/themes/co-op/images/solar-main-icon.png"/>
<h2>View Our Projects</h2>
<p>Our community solar projects are building wealth for our investors, and generating power for Ontario.</p></a>
</div>
</div><div id="clear"></div>
</div>
<div id="news-prev"><div id="page-wrap">
<h3>Recent News</h3>
<div id="blog-post">
<a href="https://cedco-op.com/2020/03/18/covid-19-update-march-18-2020/">
<img src="https://cedco-op.com/wp-content/themes/co-op/images/blog-thumb-temp.jpg"/>
<h2>COVID-19 update: March 18, 2020</h2>
<p>As the COVID-19 situation continues to develop, CED Co-op realizes this may be a challenging time for you, your families, and your organizations. Like you, we are continuously adapting...</p></a>
</div>
<div id="blog-post">
<a href="https://cedco-op.com/2020/03/16/covid-19-and-ced-co-op-march-16-2020/">
<img src="https://cedco-op.com/wp-content/themes/co-op/images/blog-thumb-temp.jpg"/>
<h2>COVID-19 and CED Co-op: March 16, 2020</h2>
<p>CED Co-op has been closely monitoring the developments pertaining to the COVID-19 (Coronavirus) pandemic and are taking necessary steps to protect our customers, suppliers, and staff. Effective immediately, CED...</p></a>
</div>
<div id="blog-post">
<a href="https://cedco-op.com/2019/05/23/annual-general-meeting/">
<img src="https://cedco-op.com/wp-content/themes/co-op/images/blog-thumb-temp.jpg"/>
<h2>2019 Investment Offering Statement</h2>
<p>TIME TO INVEST! We have officially launched our third investment opportunity!  CED Co-op plans to raise up to a maximum of $3 million through the sale of Bonds.  Since...</p></a>
</div>
</div>
</div>
<footer>
<div id="page-wrap">
<div id="f-column">
<h3>Touch Base</h3>
<p>860 Trillium Drive, 2nd Floor,
<br/>

Kitchener, ON N2R 1K4</p>
<p>Phone: 519-279-4045<br/>

Toll Free: 855-274-6890<br/>

Fax: 519-279-4631<br/>

Email: <a href="mailto:info@cedco-op.com">info@cedco-op.com</a></p>
</div>
<div class="wow fadeIn" data-wow-delay="0.5s" data-wow-duration="1.5s" id="f-column" style="text-align:center">
<!-- Counter for footer goes here... -->
<h3>Energy Generated</h3>
<h4 style="text-align:center">43,310,394 kWh</h4>
<h3>CO2 Emissions Saved</h3>
<h4 style="text-align:center">17,289,509 kg</h4>
</div>
<div id="f-column">
<h3>Stay Connected</h3>
<p>Find us on social media for news and updates</p>
<p class="social"><a href="https://www.facebook.com/CEDCoop" target="_blank"><img alt="" height="45" src="https://cedco-op.com/wp-content/themes/co-op/images/facebook-icon.png" width="45"/></a>   <a href="https://twitter.com/cedcooperative" target="_blank"><img alt="" height="45" src="https://cedco-op.com/wp-content/themes/co-op/images/twitter-icon.png" width="45"/></a>   <a href="https://www.linkedin.com/company/community-energy-development-co-operative/" target="_blank"><img alt="" height="45" src="https://cedco-op.com/wp-content/themes/co-op/images/linkedin-icon.png" width="45"/></a></p>
</div>
</div>
<!-- End Page Wrap -->
<div id="clear"></div>
</footer><!-- End Footer -->
<div id="email-footer">
<div id="page-wrap">
<h6>Stay Plugged In</h6>
<p>Sign up to receive monthly emails</p>
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="//cedco-op.us10.list-manage.com/subscribe/post?u=1be54770dec1532b6bd8a3368&amp;id=1f67729e7f" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group">
<input class="required email" id="mce-EMAIL" name="EMAIL" onfocus="this.value='';return false;" type="email" value="youremail@somedomain.com"/>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div style="position: absolute; left: -5000px;"><input name="b_1be54770dec1532b6bd8a3368_bf933c47e4" tabindex="-1" type="text" value=""/></div>
<div class="clear"><input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe"/></div>
</div>
</form>
</div>
<!--End mc_embed_signup-->
<div id="clear"></div>
</div>
</div><!-- End Email-Footer -->
<div id="bottom-footer">
<div id="page-wrap">
<div id="f-news">
<p>© 

    <script language="JavaScript" type="text/javascript">

       var now = new Date();

       document.write(+ now.getFullYear());

       </script> 

    Community Energy Development Co-operative <br/>

    

    Design by <a href="http://qtweb.ca" target="_blank">QT Web Designs</a></p>
</div>
<div class="footer-logo" id="f-column">
<p><a href="https://cedco-op.com/"><img alt="" height="96" src="https://cedco-op.com/wp-content/themes/co-op/images/ced-co-op-logo.png" width="373"/></a> </p>
</div>
</div>
</div> <!-- End bottom-footer -->
<!-- End Page-Wrap -->
<script type="text/javascript">
var colomatduration = 'fast';
var colomatslideEffect = 'slideFade';
var colomatpauseInit = '';
var colomattouchstart = '';
</script><link href="https://cedco-op.com/wp-content/uploads/intense-cache/intense_820c6146878461aaa75ab6abd7886c004e4075c7.css" id="intense-custom-css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="ced-script-js" src="https://cedco-op.com/wp-content/plugins/ced-coop-sync/js/ced.js" type="text/javascript"></script>
<script id="shadowbox-js" src="https://cedco-op.com/wp-admin/admin-ajax.php?action=shadowboxjs&amp;cache=6e159b34b5dfb60e04b1b5ec5ccb3501&amp;ver=3.0.3" type="text/javascript"></script>
<script id="wlm3_js-js" src="https://cedco-op.com/wp-content/plugins/wishlist-member/ui/js/frontend.js" type="text/javascript"></script>
<script id="comment-reply-js" src="https://cedco-op.com/wp-includes/js/comment-reply.min.js" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/cedco-op.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://cedco-op.com/wp-content/plugins/contact-form-7/includes/js/scripts.js" type="text/javascript"></script>
<script id="collapseomatic-js-js" src="https://cedco-op.com/wp-content/plugins/jquery-collapse-o-matic/js/collapse.js" type="text/javascript"></script>
<script id="page-links-to-js" src="https://cedco-op.com/wp-content/plugins/page-links-to/dist/new-tab.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://cedco-op.com/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script id="intense_modernizr-js" src="https://cedco-op.com/wp-content/plugins/intense/assets/js/modernizr.min.js" type="text/javascript"></script>
<script id="new-royalslider-main-js-js" src="https://cedco-op.com/wp-content/plugins/royal-slider/lib/royalslider/jquery.royalslider.min.js" type="text/javascript"></script>
<!-- Begin Shadowbox JS v3.0.3.10 -->
<!-- Selected Players: html, iframe, img, qt, swf, wmp -->
<script type="text/javascript">
/* <![CDATA[ */
	var shadowbox_conf = {
		autoDimensions: false,
		animateFade: true,
		animate: true,
		animSequence: "sync",
		autoplayMovies: true,
		continuous: false,
		counterLimit: 10,
		counterType: "default",
		displayCounter: true,
		displayNav: true,
		enableKeys: true,
		flashBgColor: "#000000",
		flashParams: {bgcolor:"#000000", allowFullScreen:true},
		flashVars: {},
		flashVersion: "9.0.0",
		handleOversize: "resize",
		handleUnsupported: "link",
		initialHeight: 160,
		initialWidth: 320,
		modal: false,
		overlayColor: "#000",
		showMovieControls: true,
		showOverlay: true,
		skipSetup: false,
		slideshowDelay: 0,
		useSizzle: false,
		viewportPadding: 20
	};
	Shadowbox.init(shadowbox_conf);
/* ]]> */
</script>
<!-- End Shadowbox JS -->
<div class="wprmenu_bar" id="wprmenu_bar">
<div class="wprmenu_icon">
<i class="wpr_open wpr-icon-th-menu"></i>
<i class="wpr_close wpr-icon-cancel2"></i>
</div>
<div class="menu_title">
<span class="wpr_title">MENU</span>
</div>
</div>
<div class="wprmenu_levels left wprmenu_custom_icons" id="wprmenu_menu">
<ul id="wprmenu_menu_ul">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912"><a href="https://cedco-op.com/invest/">Invest</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113"><a href="https://cedco-op.com/membership/">Membership</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194"><a href="https://cedco-op.com/projects/">Projects</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023"><a href="https://cedco-op.com/recent-news">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1756"><a href="https://cedco-op.com/wp-login.php">Members</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112"><a href="https://cedco-op.com/connect/">Connect</a></li>
</ul>
</div>
<script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="https://cedco-op.com/wp-content/themes/co-op/js/jquery.scrolly.js"></script>
<script>

    $(document).ready(function(){

       $('.parallax').scrolly({bgParallax: true});

    });

</script>
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-60115377-24', 'auto');

  ga('send', 'pageview');



</script>
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-76444540-1', 'auto', 'clientTracker');

  ga('clientTracker.send', 'pageview');



</script>
</body>
</html>
<!-- Page generated by LiteSpeed Cache 3.6.1 on 2021-01-11 20:48:25 -->