<!DOCTYPE html>
<html lang="pl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="kgLmPqCPVhYid35d99iI4L3N1N/1aSILyJjhZAfAuPY=" name="verify-v1"/>
<meta content="5raxxl2eldzsbga555-qiim2lywx-50758aryz4l2tdspqptpm80xgajv--70grk3wwm8poua96qdk223eqihwm0sr8uz9d11jpv6fo7wtng-egwez032xvx97nlv5mq" name="norton-safeweb-site-verification"/>
<meta content="ifD8JPj0-PBn2jJ_BS3pH-sX9ls2UDD0ibN8jZz7XNM" name="google-site-verification"/>
<meta content="index, follow, archive" name="Robots"/>
<meta content="darmowe, skracanie, adresĂłw, linki, darmo, skracacz, skracacze, tiny, adresy, url, dlugie, krĂłtki, skrĂłcic, najkrĂłtszy, najszybszy, prosty, szybki, alias, domeny, przekierowanie" name="keywords"/>
<meta content="Darmowe skracanie adresĂłw URL." name="description"/>
<meta content="https://tiny.pl" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Tiny.pl" property="og:title"/>
<meta content="Free URL shortening service." property="og:description"/>
<meta content="https://tiny.pl/assets/Tiny-Logo-bk-wide.png" property="og:image"/>
<title>Tiny.pl - Skracanie linków. Zamień długi adres na krótki i łatwy!</title>
<script src="assets/jquery.min.js"></script>
<link href="assets/style.css?v=4" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
		var browser_lang = "pl";

		jQuery( document ).ready(function( $ ) {
  			if (window!=window.top) {
  				window.top.location = window.location;
  			}

  			$("#form-link").focus();

  			var l = document.links
			var a, h
			for (a=0;a<l.length;a++)
			{
				if (h = l[a].getAttribute("data-mt"))
				{
					h = h.replace(/-/g,"")
					h = h.replace("malpa","@")
					h = h.replace("kropka",".")
					l[a].setAttribute("href","m" + "ai" + "lto" + ":" + h)
				}
			}
		});
		function setCookie(c_name,value,expiredays)
		{
			var exdate=new Date();
			exdate.setDate(exdate.getDate()+expiredays);
			document.cookie=c_name+ "=" +escape(value) + ((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
		}

		// Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent 
		window.cookieconsent_options = {
			"message": "Używamy cookies dla personalizacji treści oraz reklam, dostarczenia usług social media oraz do analizowania ruchu na stronie. Udostępniamy też informacje o twoim użyciu tej strony z wymienionymi rodzajami usług.",
			"dismiss":"Rozumiem!",
			"learnMore":"Więcej",
			"link":"/cookies.php",			
			"container": ".cookies-info-wrapper"
			};
	</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js" type="text/javascript"></script>
<link href="/favicon.ico" rel="shortcut icon"/>
</head>
<body>
<noscript>Twoja przeglądarka nie obsługuje JavaScript bądź jest on wyłączony. Tiny.pl działa najlepiej ze wsparciem JavaScript.</noscript>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="top-wrapper">
<br/><br/>
<div class="logo-wrapper">
<div class="fb-wrapper">
<div class="fb-like" data-action="like" data-href="https://tiny.pl" data-layout="button_count" data-share="false" data-show-faces="false"></div>
</div>
<a href="//tiny.pl"><img alt="Tiny.pl LOGO" class="logo" src="assets/tiny-logo.png" style="z-index: 1000" title="Tiny.pl"/></a>
</div>
<div class="menu-wrapper">
<ul id="menu">
<li><a class="menu-active" href="/" title="Strona główna">Tiny.pl</a></li>
<li><a class="" href="faq.php" title="Najczęściej zadawane pytania">FAQ</a></li>
<li><a class="" href="help.php" title="Pomoc">Pomoc</a></li>
<li><a data-mt="ti-ny-mal-pa-ti-ny-kro-pka-p-l" href="#ti-ny-mal-pa-ti-ny-kro-pka-p-l" title="Napisz do nas">Kontakt</a></li>
</ul>
</div>
<br/><br/>
<strong>
<strong class="notification">Błąd 404 / Error 404</strong><br/><br/>
Przepraszamy! Podany adres nie istnieje.<br/>
Sprawdź czy poprawnie wpisałeś adres w oknie przeglądarki.
<br/><br/>
Kliknij <a href="/">tutaj</a> aby przejść na stronę główną serwisu.
</strong></div>
<div class="cookies-info-wrapper"></div>
<footer data-position="fixed">
<span class="copyright">
© 2021 Tiny.pl
</span>
<span>
<a href="spam.php" title="Zgłoś skrót wykorzystywany przez SPAMERÓW">zgłoś spam</a>
</span>
<span>
<a href="help.php#b" title="rozszyfruj skrót">rozszyfruj<span class="hide-small"> skrót</span></a>
</span>
<span class="hide-small">
<a href="help.php#z" title="Dodaj skryptozakładkę do paska narzędzi">dodaj do paska narzędzi</a>
</span>
<span>
<a href="faq.php#wk" title="warunki korzystania">warunki<span class="hide-small"> korzystania</span></a>
</span>
<span>
<a href="/cookies.php">cookies</a>
</span>
<span class="hide-small">
<a href="http://cleantalk.org" target="_blank">cleantalk.org<span class="hide-small"> - anti-spam for websites</span></a>
</span>
</footer>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-16240944-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>