<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>406 Status Error</title>
<meta charset="utf-8"/>
<meta content="noindex" name="robots"/>
<link href="/406error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="bdiv">
<div id="content">
<div id="title">
<b>406 status code</b>  - AutomationDirect - Not Acceptable Response
	            </div>
<div id="header-bar">
	             please wait a few seconds and try again...
	            </div>
<div id="info">
<img alt="AutomationDirect.com" height="40" src="/406error.jpg" width="52"/>
	                Our server is having trouble processing your request for some reason. You may try again or contact our webmaster for support.<br/><br/>
</div>
</div>
</div>
</body>
</html>
