<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="ja" xml:lang="ja" xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!--<meta http-equiv="X-Ua-Compatible" content="IE=EmulateIE7" />--><title>保安用品など路上駐車場安全対策用品専門サイト | 株式会社アルコム</title>
<meta content="保安用品,安全,保安,用品,減速帯,駐車場,ハンプ,ロードハンプ,安全" name="Keywords"/>
<meta content="保安用品で路上駐車場の安全対策。株式会社アルコムではロードハンプ(減速帯)等駐車場保安用品を豊富に取扱い。駐車場の安全対策には当社の保安用品をご利用ください。減速帯の施工例や実績もご紹介しております。" name="Description"/>
<meta content="text/css" http-equiv="content-style-type"/>
<meta content="text/javascript" http-equiv="content-script-type"/>
<link href="https://www.777ar.com/" rel="start" title="保安用品など路上駐車場安全対策用品専門サイト | 株式会社アルコム"/>
<link href="https://www.777ar.com/index.html" rel="index"/>
<link href="https://www.777ar.com/favicon.ico" rel="shortcut icon"/><!-- *** stylesheet *** -->
<link href="https://www.777ar.com/css/import.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.777ar.com/css/thickbox.css" media="all" rel="stylesheet" type="text/css"/>
<link href="./css/homepage.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://777ar.spz.link/" media="only screen and (max-width: 640px)" rel="alternate"/>
<!-- *** javascript *** -->
<script src="https://www.777ar.com/js/jquery-1.3.2.js" type="text/javascript"></script>
<script src="https://www.777ar.com/js/thickbox.js" type="text/javascript"></script>
<script charset="utf-8" src="https://www.777ar.com/js/yuga-0.7.1.js" type="text/javascript"></script>
<script charset="utf-8" src="https://www.777ar.com/js/hoverintent.js" type="text/javascript"></script>
<script charset="utf-8" src="https://www.777ar.com/js/heightLine.js" type="text/javascript"></script>
<script charset="utf-8" src="https://www.777ar.com/js/analytics_ar.js" type="text/javascript"></script>
<script charset="utf-8" src="https://www.777ar.com/js/jquery.fitted.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function(){$('.clickable').fitted();});
</script>
<script charset="utf-8" src="js/jquery_tab_fadein.js" type="text/javascript"></script>
</head>
<body id="homepage">
<div id="page">
<div id="header">
<h1>駐車場保安用品　輸入建具・サッシ　輸入タイル　LEDフレーム　安全対策</h1>
<div class="con_header">
<h2><a href="https://www.777ar.com/"><img alt="株式会社アルコム" src="https://www.777ar.com/images/h_logo.png"/></a></h2>
<div class="box_r">
<ul id="hnav">
<li><a href="https://www.777ar.com/commitments/"><img alt="お客様への想い" src="https://www.777ar.com/images/hnav_omoi.png"/></a></li>
<li><a href="https://www.777ar.com/company/"><img alt="会社案内" src="https://www.777ar.com/images/hnav_compa.png"/></a></li>
<li><a href="https://www.777ar.com/sash/"><img alt="輸入建具・サッシ" src="https://www.777ar.com/images/hnav_sash.png"/></a></li>
<li><a href="https://www.777ar.com/tile/"><img alt="輸入タイル" src="https://www.777ar.com/images/hnav_tile.png"/></a></li>
<li class="end"><a href="https://www.777ar.com/led/"><img alt="LEDフレーム" src="https://www.777ar.com/images/hnav_led.png"/></a></li>
</ul>
<p class="txt_h_tel"><img alt="052-221-5536" src="https://www.777ar.com/images/h_tel.png"/></p>
<p class="btn_h_contact"><a href="https://www.777ar.com/contact/"><img alt="資料請求・お問い合わせ" src="https://www.777ar.com/images/btn_h_contact.png"/></a></p>
</div>
</div><!-- /.con_header -->
<div class="con_gnav">
<ul>
<li class="gnav_hump"><a href="https://www.777ar.com/hump/">減速ロードハンプ(減速帯)</a></li>
<li class="gnav_pole"><a href="https://www.777ar.com/pole/">視線誘導標(車線分離標)</a></li>
<li class="gnav_goods"><a href="https://www.777ar.com/goods/">商品一覧</a>
<div class="sub">
<ul>
<li><a href="https://www.777ar.com/goods/#pole"><img alt="車線分離標" src="https://www.777ar.com/images/bn_pole.jpg"/></a></li>
<li><a href="https://www.777ar.com/goods/#hansya"><img alt="道路鋲・縁石鋲・反射板" src="https://www.777ar.com/images/bn_hansya.jpg"/></a></li>
<li><a href="https://www.777ar.com/goods/#mirror"><img alt="カーブミラー" src="https://www.777ar.com/images/bn_mirror.jpg"/></a></li>
<li><a href="https://www.777ar.com/goods/#corner"><img alt="コーナーガード" src="https://www.777ar.com/images/bn_corner.jpg"/></a></li>
<li><a href="https://www.777ar.com/goods/#stop"><img alt="ゴム製車止め" src="https://www.777ar.com/images/bn_stop.jpg"/></a></li>
</ul>
</div>
</li>
<li class="gnav_work"><a href="https://www.777ar.com/works/">施工販売実績</a></li>
<li class="gnav_case"><a href="https://www.777ar.com/case/">施工例</a></li>
<li class="gnav_pro"><a href="https://www.777ar.com/process/">施工方法</a></li>
<li class="gnav_tc"><a href="https://www.777ar.com/tc/">タイルカーペット</a></li>
</ul>
</div><!-- /.con_gnav -->
</div><!-- /#header -->
<div id="contents">
<div id="main">
<p class="img_main"><a href="hump/index.php"><img alt="駐車場の安全対策は万全ですか？" src="images/home/img_main01.jpg"/></a><a href="pole/index.php"><img alt="駐車場の安全対策は万全ですか？" src="images/home/img_main02.jpg"/></a></p>
<div class="con_guide">
<h3><img alt="駐車場保安用品をお探しのお客様へ" src="images/home/st_guid.gif"/></h3>
<div class="box_guide">
<p class="txt_guide"><img alt="各お客様に合った、最適なご案内をいたします。" src="images/home/img_guide.gif"/></p>
<ul>
<li><a href="guide/kankosyocho.php"><img alt="官公省庁・役所職員のお客様" src="images/home/btn_kankosyocho.jpg"/></a></li>
<li><a href="guide/sekkei.php"><img alt="設計事務所・コンサルのお客様" src="images/home/btn_sekkei.jpg"/></a></li>
<li><a href="guide/zenekon.php"><img alt="総合建設業（ゼネコン）のお客様" src="images/home/btn_zenekon.jpg"/></a></li>
<li><a href="guide/douro.php"><img alt="道路会社のお客様" src="images/home/btn_douro.jpg"/></a></li>
<li><a href="guide/syosya.php"><img alt="商社のお客様" src="images/home/btn_syosya.jpg"/></a></li>
<li><a href="guide/gyosya.php"><img alt="業者のお客様" src="images/home/btn_gyosya.jpg"/></a></li>
<li><a href="guide/sisetsu.php"><img alt="施設管理者・管理組合のお客様" src="images/home/btn_sisetsukanri.jpg"/></a></li>
<li><a href="guide/kojo.php"><img alt="工場・物流関係のお客様" src="images/home/btn_kojo.jpg"/></a></li>
<li><a href="guide/syokunin.php"><img alt="職人のお客様" src="images/home/btn_syokunin.jpg"/></a></li>
<li><a href="guide/tenpo.php"><img alt="店舗のお客様" src="images/home/btn_tenpo.jpg"/></a></li>
<li><a href="guide/yugijo.php"><img alt="パチンコ店のお客様" src="images/home/btn_yugiyo.jpg"/></a></li>
</ul>
</div><!-- /.box_guide -->
</div><!-- /.con_guide -->
<div class="con_works heightLine-cnt">
<h3><img alt="施工実績" src="images/home/st_works.gif"/></h3>
<div class="box_works">
<ul>
<li>
<p class="img_l"><a class="thickbox" href="works/01_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800"><img alt="横須賀市役所（神奈川県）" src="works/images/img_01_06.jpg"/></a></p>
<dl>
<dt>
<span>市役所</span><br/>
<a class="thickbox" href="works/01_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">横須賀市役所（神奈川県）</a></dt>
<dd>・横須賀市役所の大型立体駐車場。安全管理だけではなくセキュリティーも万全。</dd>
<a class="ic_link thickbox" href="works/01_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">続きを読む</a>
</dl>
</li>
<li>
<p class="img_l"><a class="thickbox" href="works/02_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800"><img alt="大阪市立大学（大阪府）" src="works/images/img_02_06.jpg"/></a></p>
<dl>
<dt>
<span>教育施設</span><br/>
<a class="thickbox" href="works/02_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">大阪市立大学（大阪府）</a></dt>
<dd>・歴史ある日本最初の市立大学<br/>
・それぞれのゲート出入り口に設置し... </dd>
<a class="ic_link thickbox" href="works/02_06.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">続きを読む</a>
</dl>
</li>
<li class="end">
<p class="img_l"><a class="thickbox" href="works/04_01.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800"><img alt="大手電気メーカー工場（三重県）" src="works/images/img_04_01.jpg"/></a></p>
<dl>
<dt>
<span>工場</span><br/>
<a class="thickbox" href="works/04_01.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">大手電機メーカー工場（三重県）</a></dt>
<dd>・構内交差点での一時停止。<br/>
・公道への出口での一時停止。 </dd>
<a class="ic_link thickbox" href="works/04_01.htm?keepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=800">続きを読む</a>
</dl>
</li>
</ul>
<p class="link_all"><a class="ic_link" href="works/">一覧はこちら</a></p>
</div><!-- /.box_works -->
</div><!-- /.con_works -->
<div class="con_goods heightLine-cnt">
<h3><img alt="商品一覧" src="images/home/st_goods.gif"/></h3>
<div class="box_goods">
<ul>
<li><a href="./hump/"><img alt="減速ロードハンプ（減速帯）" src="images/bn_hump.jpg"/></a></li>
<li><a href="./goods/#pole"><img alt="車線分離標" src="images/bn_pole.jpg"/></a></li>
<li><a href="./goods/#hansya"><img alt="道路鋲・縁石鋲・反射板" src="images/bn_hansya.jpg"/></a></li>
<li><a href="./goods/#mirror"><img alt="カーブミラー" src="images/bn_mirror.jpg"/></a></li>
<li class="end"><a href="./goods/#corner"><img alt="コーナーガード" src="images/bn_corner.jpg"/></a></li>
<li class="end"><a href="./goods/#stop"><img alt="ゴム製車止め" src="images/bn_stop.jpg"/></a></li>
</ul>
<p class="link_all"><a class="ic_link" href="goods/">一覧はこちら</a></p>
</div><!-- /.box_goods -->
</div><!-- /.con_goods -->
<div class="con_case">
<h3><img alt="施工例" src="images/home/st_case.gif"/></h3>
<div class="box_case">
<p class="angle"><img alt="" src="images/home/angle.png"/></p>
<div class="container_tab heightLine-case">
<div class="box_cnt" id="tab1">
<h4><img alt="減速帯（直線通路）" src="images/home/sst_case01.gif"/></h4>
<p class="img"><img alt="減速帯（直線通路）" src="images/home/img_case01.jpg"/></p>
<p>直線距離が長く速度超過になりがちな場所に2～3箇所が効果的。</p>
<p class="btn_detail"><a href="hump/"><img alt="減速帯の詳細" src="images/home/btn_case.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab2">
<h4><img alt="車止めロング（駐車場所）" src="images/home/sst_case02.gif"/></h4>
<p class="img"><img alt="車止めロング（駐車場所）" src="images/home/img_case02.jpg"/></p>
<p>壁や他の車との接触を防止。夜間等の視認性にも優れている。車幅の狭い軽自動車でも安心。
車止めロングの詳細へ</p>
<p class="btn_detail"><a href="goods/#stop"><img alt="車止めロングの詳細" src="images/home/btn_case_stop.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab3">
<h4><img alt="道路鋲（中央車路）" src="images/home/sst_case03.gif"/></h4>
<p class="img"><img alt="道路鋲（中央車路）" src="images/home/img_case03.jpg"/></p>
<p>夜間や暗い場所でのラインなどの視認性を高める。</p>
<p class="btn_detail"><a href="goods/#hansya"><img alt="道路鋲の詳細" src="images/home/btn_case_hansya.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab4">
<h4><img alt="減速帯（歩行者用通路）" src="images/home/sst_case04.gif"/></h4>
<p class="img"><img alt="減速帯（歩行者用通路）" src="images/home/img_case04.jpg"/></p>
<p>横断歩道、歩行者の往来がある場所の手前で注意と減速を促す。</p>
<p class="btn_detail"><a href="hump/"><img alt="減速帯の詳細" src="images/home/btn_case.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab5">
<h4><img alt="ロードポール（進入禁止場所）" src="images/home/sst_case05.gif"/></h4>
<p class="img"><img alt="ロードポール（進入禁止場所）" src="images/home/img_case05.jpg"/></p>
<p>進入禁止場所や車の誘導に効果的。移動、取外しも簡単で簡易的な誘導にも最適。</p>
<p class="btn_detail"><a href="goods/#pole"><img alt="ロードポールの詳細" src="images/home/btn_case_pole.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab6">
<h4><img alt="カーブミラー（出入口）" src="images/home/sst_case06.gif"/></h4>
<p class="img"><img alt="カーブミラー（出入口）" src="images/home/img_case06.jpg"/></p>
<p>出入り口等の視界の悪い場所での歩行者、車の確認。</p>
<p class="btn_detail"><a href="goods/#mirror"><img alt="カーブミラーの詳細" src="images/home/btn_case_mirror.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab7">
<h4><img alt="減速帯（出入口）" src="images/home/sst_case07.gif"/></h4>
<p class="img"><img alt="減速帯（出入口）" src="images/home/img_case07.jpg"/></p>
<p>出口、公道への接続に注意と減速を促し安全の認識を高める。交通誘導員のコスト削減に効果。</p>
<p class="btn_detail"><a href="hump/"><img alt="減速帯の詳細" src="images/home/btn_case.gif"/></a></p>
</div><!-- /.box_cnt -->
<div class="box_cnt" id="tab8">
<h4><img alt="段差プレート（出入口）" src="images/home/sst_case08.gif"/></h4>
<p class="img"><img alt="段差プレート（出入口）" src="images/home/img_case08.jpg"/></p>
<p>段差で車の乗り入れがしにくく、敬遠される場所に。</p>
</div><!-- /.box_cnt -->
</div>
<div class="box_map heightLine-case">
<p><img alt="" border="0" src="images/home/img_map.jpg" usemap="#Map"/></p>
</div><!-- /.box_map -->
<ul class="btn_tab">
<li><a class="selected" href="#tab1" style="position: absolute; left: 282px; top: 173px;"><img alt="○" src="images/home/circle_l.png"/></a></li>
<li><a href="#tab2" style="position: absolute; left: 416px; top: 14px;"><img alt="○" src="images/home/circle_m.png"/></a></li>
<li><a href="#tab3" style="position: absolute; left: 358px; top: 245px;"><img alt="○" src="images/home/circle_s.png"/></a></li>
<li><a href="#tab4" style="position: absolute; left: 574px; top: 230px;"><img alt="○" src="images/home/circle_m.png"/></a></li>
<li><a href="#tab5" style="position: absolute; left: 790px; top: 123px;"><img alt="○" src="images/home/circle_m.png"/></a></li>
<li><a href="#tab6" style="position: absolute; left: 786px; top: 20px;"><img alt="○" src="images/home/circle_m.png"/></a></li>
<li><a href="#tab7" style="position: absolute; left: 808px; top: 53px;"><img alt="○" src="images/home/circle_s.png"/></a></li>
<li><a href="#tab8" style="position: absolute; left: 838px; top: 50px;"><img alt="○" src="images/home/circle_s.png"/></a></li>
</ul>
</div><!-- /.box_case -->
</div><!-- /.con_case -->
</div><!-- /#main -->
<div id="f_contact"><p><a href="https://www.777ar.com/contact/"><img alt="資料請求・お問い合わせはお気軽に。TEL:052-221-5536 FAX:052-221-5537" src="https://www.777ar.com/images/f_contact.gif"/></a></p></div><!--#f_contact -->
</div><!-- /#contents -->
<div id="footer">
<p class="pagetop"><a href="#page"><img alt="ページトップへ" src="https://www.777ar.com/images/pagetop.gif"/></a></p>
<div class="con_footer">
<div class="box_l">
<h4><a href="https://www.777ar.com/"><img alt="株式会社アルコム" src="https://www.777ar.com/images/f_logo.gif"/></a></h4>
<h5>株式会社アルコム</h5>
<address>〒460-0011　愛知県名古屋市中区大須1-21-19 DIX4ビル6F<br/>
			TEL. 052-221-5536　FAX. 052-221-5537</address>
</div><!-- /.box_l -->
<div class="box_r">
<ul id="fnav">
<li><a href="https://www.777ar.com/">HOME</a></li>
<li><a href="https://www.777ar.com/sash/">輸入建具・サッシ</a></li>
<li><a href="https://www.777ar.com/tile/">輸入タイル</a></li>
<li><a href="https://www.777ar.com/led/">LEDフレーム</a></li>
<li><a href="https://www.777ar.com/contact/">資料請求・お問い合わせ</a></li>
<li><a href="https://www.777ar.com/commitments/">お客様への想い</a></li>
<li><a href="https://www.777ar.com/company/">会社案内</a></li>
<li><a href="https://www.777ar.com/recruit/">採用情報</a></li>
</ul>
<p class="copyright">Copyright © 
		<script type="text/javascript">
		<!--
		document.write(new Date().getFullYear());
		-->
		</script> ARCOM. All Rights Reserved.</p>
</div><!-- /.box_r -->
</div><!-- /.con_footer -->
</div><!-- /#footer -->
</div><!-- /#page -->
</body>
</html>