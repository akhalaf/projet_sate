<!DOCTYPE html>
<html class="no-js mh-no-sb" lang="en-US">
<head>
<meta charset="utf-8"/>
<title>Clay Chism, Sheriff</title>
<link href="https://www.callawaysheriff.org/wp-content/uploads/2015/07/logo.png" rel="shortcut icon"/>
<!--[if lt IE 9]>
<script src="https://callawaysheriff.org/wp-content/themes/mh_magazine/js/css3-mediaqueries.js"></script>
<![endif]-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://callawaysheriff.org/xmlrpc.php" rel="pingback"/>
<link href="//secure.gravatar.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://callawaysheriff.org/feed/" rel="alternate" title="Clay Chism, Sheriff » Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/callawaysheriff.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://c0.wp.com/c/5.3.6/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css" type="text/css">
.has-text-align-justify{text-align:justify;}
</style>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css?ver=4.1.0" id="font-awesome-4-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://callawaysheriff.org/wp-content/plugins/superior-faq/css/superior-faq.css?ver=1.0.2" id="superior-faq-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://callawaysheriff.org/wp-content/themes/mh_magazine/style.css?ver=2.4.2" id="mh-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700" id="mh-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://callawaysheriff.org/wp-content/plugins/tablepress/css/default.min.css?ver=1.11" id="tablepress-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/p/jetpack/8.6.1/css/jetpack.css" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://c0.wp.com/c/5.3.6/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.3.6/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script src="https://callawaysheriff.org/wp-content/plugins/superior-faq/js/jquery.smart_autocomplete.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://callawaysheriff.org/wp-content/themes/mh_magazine/js/scripts.js?ver=5.3.6" type="text/javascript"></script>
<link href="https://callawaysheriff.org/wp-json/" rel="https://api.w.org/"/>
<link href="https://callawaysheriff.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://callawaysheriff.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://callawaysheriff.org/" rel="canonical"/>
<link href="https://wp.me/P6KOfl-6" rel="shortlink"/>
<link href="https://callawaysheriff.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcallawaysheriff.org%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://callawaysheriff.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcallawaysheriff.org%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="//v0.wordpress.com" rel="dns-prefetch"/>
<link href="//c0.wp.com" rel="dns-prefetch"/>
<style type="text/css">img#wpstats{display:none}</style> <style type="text/css">
    	    	    	    	    		.ticker-title, .header-nav .menu-item:hover, .main-nav li:hover, .footer-nav, .footer-nav ul li:hover > ul, .slicknav_menu, .slicknav_btn, .slicknav_nav .slicknav_item:hover,
    		.slicknav_nav a:hover, .slider-layout2 .flex-control-paging li a.flex-active, .sl-caption, .subheading, .pt-layout1 .page-title, .wt-layout2 .widget-title, .wt-layout2 .footer-widget-title,
    		.carousel-layout1 .caption, .page-numbers:hover, .current, .pagelink, a:hover .pagelink, input[type=submit], #cancel-comment-reply-link, .post-tags li:hover, .tagcloud a:hover, .sb-widget .tagcloud a:hover, .footer-widget .tagcloud a:hover, #infinite-handle span { background: #5c6c48; }
    		.slide-caption, .mh-mobile .slide-caption, [id*='carousel-'], .wt-layout1 .widget-title, .wt-layout1 .footer-widget-title, .wt-layout3 .widget-title, .wt-layout3 .footer-widget-title,
    		.ab-layout1 .author-box, .cat-desc, textarea:hover, input[type=text]:hover, input[type=email]:hover, input[type=tel]:hover, input[type=url]:hover, blockquote { border-color: #5c6c48; }
    		.dropcap, .carousel-layout2 .caption { color: #5c6c48; }
    	    	    	    	    	    	    	    		a:hover, .meta a:hover, .breadcrumb a:hover, .related-title:hover, #ticker a:hover .meta, .slide-title:hover, .sl-title:hover, .carousel-layout2 .carousel-item-title:hover { color: #5c6c48; }
    	    	.superior-effect-accordion a { color: #000; }	</style>
<style type="text/css">
				/* If html does not have either class, do not show lazy loaded images. */
				html:not( .jetpack-lazy-images-js-enabled ):not( .js ) .jetpack-lazy-image {
					display: none;
				}
			</style>
<script>
				document.documentElement.classList.add(
					'jetpack-lazy-images-js-enabled'
				);
			</script>
<meta content="website" property="og:type"/>
<meta content="Clay Chism, Sheriff" property="og:title"/>
<meta content="https://callawaysheriff.org/" property="og:url"/>
<meta content="Clay Chism, Sheriff" property="og:site_name"/>
<meta content="https://s0.wp.com/i/blank.jpg" property="og:image"/>
<meta content="en_US" property="og:locale"/>
<meta content="Home" name="twitter:text:title"/>
<meta content="summary" name="twitter:card"/>
<meta content="Visit the post for more." name="twitter:description"/>
</head>
<body class="home page-template page-template-page-homepage page-template-page-homepage-php page page-id-6 mh-right-sb wt-layout1 pt-layout2 ab-layout2 rp-layout2 loop-layout2">
<div class="mh-container">
<header class="header-wrap">
<a href="https://callawaysheriff.org/" rel="home" title="Clay Chism, Sheriff">
<div class="logo-wrap" role="banner">
<img alt="Clay Chism, Sheriff" height="62" src="https://callawaysheriff.org/wp-content/uploads/2016/10/cropped-logo.png" width="228"/>
<div class="logo logo-overlay">
<h1 class="logo-name">Clay Chism, Sheriff</h1>
</div>
</div>
</a>
<nav class="main-nav clearfix">
<div class="menu-menu-1-container"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-6 current_page_item menu-item-8" id="menu-item-8"><a aria-current="page" href="https://callawaysheriff.org/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-53" id="menu-item-53"><a href="https://callawaysheriff.org/sex-offenders/">Sex Offenders</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2058" id="menu-item-2058"><a href="https://callawaysheriff.org/map-registered-sex-offenders/">Map of registered sex offenders</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2059" id="menu-item-2059"><a href="https://callawaysheriff.org/sex-offenders/">Sex Offenders</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54" id="menu-item-54"><a href="https://callawaysheriff.org/crime-stoppers/">Crime Tips</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-402" id="menu-item-402"><a href="https://callawaysheriff.org/faq-general/">FAQ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-720" id="menu-item-720"><a href="http://www.callawaysheriff.org/faq/how-do-i-obtain-a-concealed-carry-permit/">CCW</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-721" id="menu-item-721"><a href="http://www.callawaysheriff.org/faq/how-do-i-obtain-a-concealed-carry-permit/">How do I obtain a CCW</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-722" id="menu-item-722"><a href="https://callawaysheriff.org/concealed-carry-instructors/">Concealed Carry Instructors</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-723" id="menu-item-723"><a href="https://callawaysheriff.org/concealed-carry-permits/">Concealed Carry Permits</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-184" id="menu-item-184"><a href="https://callawaysheriff.org/category/press-releases/">Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401" id="menu-item-401"><a href="https://callawaysheriff.org/warrants/">Wanted</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-901" id="menu-item-901"><a href="https://callawaysheriff.org/careers/">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59" id="menu-item-59"><a href="https://callawaysheriff.org/contact-us/">Contact us</a></li>
</ul></div> </nav>
</header>
<div class="news-ticker clearfix">
<div class="ticker-title">
News Ticker </div>
<div class="ticker-content">
<ul id="ticker"> <li class="ticker-item">
<a href="https://callawaysheriff.org/deputies-start-off-the-new-year-with-11-arrests-for-various-felonies-during-patrols-and-investigtions/" title="Deputies start off the new year with 11 arrests for various felonies during patrols and investigations">
<span class="meta ticker-item-meta">
January 11, 2021 in Press Releases: </span>
<span class="meta ticker-item-title">
Deputies start off the new year with 11 arrests for various felonies during patrols and investigations </span>
</a>
</li>
<li class="ticker-item">
<a href="https://callawaysheriff.org/deputies-arrest-11-for-various-felonies-between-december-18-and-december-31/" title="Deputies arrest 11 for various felonies between December 18 and December 31">
<span class="meta ticker-item-meta">
January 4, 2021 in Press Releases: </span>
<span class="meta ticker-item-title">
Deputies arrest 11 for various felonies between December 18 and December 31 </span>
</a>
</li>
<li class="ticker-item">
<a href="https://callawaysheriff.org/deputies-arrest-11-between-december-7-and-december-17-for-various-felonies-during-patrol-and-investigations/" title="Deputies arrest 11 between December 7 and December 17 for various felonies during patrol and investigations">
<span class="meta ticker-item-meta">
December 18, 2020 in Press Releases: </span>
<span class="meta ticker-item-title">
Deputies arrest 11 between December 7 and December 17 for various felonies during patrol and investigations </span>
</a>
</li>
<li class="ticker-item">
<a href="https://callawaysheriff.org/deputies-arrest-14-for-various-felonies-during-patrol-and-investigations/" title="Deputies arrest 14 for various felonies during patrol and investigations">
<span class="meta ticker-item-meta">
December 8, 2020 in Press Releases: </span>
<span class="meta ticker-item-title">
Deputies arrest 14 for various felonies during patrol and investigations </span>
</a>
</li>
<li class="ticker-item">
<a href="https://callawaysheriff.org/deputies-arrest-10-for-various-felonies-during-patrol-and-investigations/" title="Deputies arrest 10 for various felonies during patrol and investigations">
<span class="meta ticker-item-meta">
November 29, 2020 in Press Releases: </span>
<span class="meta ticker-item-title">
 Deputies arrest 10 for various felonies during patrol and investigations </span>
</a>
</li>
</ul>
</div>
</div> <div class="mh-wrapper hp clearfix">
<div class="hp-main">
<div class="sb-widget home-1 home-wide"><h4 class="widget-title">Latest News</h4> <div class="textwidget"></div>
</div> <div class="hp-columns clearfix">
<div class="hp-content left" id="main-content">
<div class="sb-widget home-2 home-wide"> <div class="textwidget"><p><img class=" jetpack-lazy-image" data-lazy-src="http://callawaysheriff.org/old/images/banner-main.jpg?is-pending-load=1" src="https://callawaysheriff.org/old/images/banner-main.jpg" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"/></p><noscript><img src="http://callawaysheriff.org/old/images/banner-main.jpg"/></noscript>
</div>
</div> <div class="clearfix">
<div class="hp-sidebar hp-home-3">
<div class="sb-widget home-3"><div class="tiled-gallery type-rectangular tiled-gallery-unresized" data-carousel-extra='{"blog_id":1,"permalink":"https:\/\/callawaysheriff.org\/","likes_blog_id":99814255}' data-original-width="500" itemscope="" itemtype="http://schema.org/ImageGallery"> <div class="gallery-row" data-original-height="198" data-original-width="500" style="width: 500px; height: 198px;"> <div class="gallery-group images-1" data-original-height="198" data-original-width="198" style="width: 198px; height: 198px;"> <div class="tiled-gallery-item tiled-gallery-item-small" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject"> <a border="0" href="https://callawaysheriff.org/6d-web-button-round-400x400px/" itemprop="url"> <meta content="194" itemprop="width"/> <meta content="194" itemprop="height"/> <img alt="6D-Web Button-round-400x400px" data-attachment-id="2177" data-comments-opened="" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="6D-Web Button-round-400x400px" data-large-file="https://callawaysheriff.org/wp-content/uploads/2017/10/6D-Web-Button-round-400x400px.gif" data-medium-file="https://callawaysheriff.org/wp-content/uploads/2017/10/6D-Web-Button-round-400x400px-300x300.gif" data-orig-file="https://callawaysheriff.org/wp-content/uploads/2017/10/6D-Web-Button-round-400x400px.gif" data-orig-size="400,400" data-original-height="194" data-original-width="194" height="194" itemprop="http://schema.org/image" src="https://i1.wp.com/callawaysheriff.org/wp-content/uploads/2017/10/6D-Web-Button-round-400x400px.gif?w=194&amp;h=194&amp;crop=1&amp;ssl=1" style="width: 194px; height: 194px;" title="6D-Web Button-round-400x400px" width="194"/> </a> </div> </div> <div class="gallery-group images-1" data-original-height="198" data-original-width="302" style="width: 302px; height: 198px;"> <div class="tiled-gallery-item tiled-gallery-item-large" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject"> <a border="0" href="https://callawaysheriff.org/crime-stoppers-2/" itemprop="url"> <meta content="298" itemprop="width"/> <meta content="194" itemprop="height"/> <img alt="crime-stoppers" data-attachment-id="2039" data-comments-opened="" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"1"}' data-image-title="crime-stoppers" data-large-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-medium-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-orig-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-orig-size="231,150" data-original-height="194" data-original-width="298" height="194" itemprop="http://schema.org/image" src="https://i0.wp.com/callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg?w=298&amp;h=194&amp;ssl=1" style="width: 298px; height: 194px;" title="crime-stoppers" width="298"/> </a> </div> </div> </div> </div></div><div class="sb-widget home-3"><h4 class="widget-title">Crime Stoppers</h4><a href="http://www.callawaysheriff.org/crime-stoppers/"><img alt="" class="image wp-image-2039 attachment-full size-full jetpack-lazy-image" data-attachment-id="2039" data-comments-opened="0" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"1"}' data-image-title="crime-stoppers" data-large-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-lazy-src="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg?is-pending-load=1" data-medium-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-orig-file="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" data-orig-size="231,150" data-permalink="https://callawaysheriff.org/crime-stoppers-2/" height="150" src="https://callawaysheriff.org/wp-content/uploads/2017/08/crime-stoppers.jpg" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="max-width: 100%; height: auto;" width="231"/></a></div><div class="sb-widget home-3"> <div class="textwidget"><p><a href="http://www.callawaysheriff.org/crime-stoppers/" rel="noopener noreferrer" target="_blank">Provide crime stoppers information online here.</a></p>
</div>
</div> </div>
<div class="hp-sidebar sb-right hp-home-4">
<div class="sb-widget home-4"> <h4 class="widget-title">Press Releases</h4> <ul>
<li>
<a href="https://callawaysheriff.org/deputies-start-off-the-new-year-with-11-arrests-for-various-felonies-during-patrols-and-investigtions/">Deputies start off the new year with 11 arrests for various felonies during patrols and investigations</a>
</li>
<li>
<a href="https://callawaysheriff.org/deputies-arrest-11-for-various-felonies-between-december-18-and-december-31/">Deputies arrest 11 for various felonies between December 18 and December 31</a>
</li>
<li>
<a href="https://callawaysheriff.org/deputies-arrest-11-between-december-7-and-december-17-for-various-felonies-during-patrol-and-investigations/">Deputies arrest 11 between December 7 and December 17 for various felonies during patrol and investigations</a>
</li>
<li>
<a href="https://callawaysheriff.org/deputies-arrest-14-for-various-felonies-during-patrol-and-investigations/">Deputies arrest 14 for various felonies during patrol and investigations</a>
</li>
<li>
<a href="https://callawaysheriff.org/deputies-arrest-10-for-various-felonies-during-patrol-and-investigations/">Deputies arrest 10 for various felonies during patrol and investigations</a>
</li>
</ul>
</div> </div>
</div>
<div class="sb-widget home-5 home-wide"><h4 class="widget-title">Mission Statement</h4> <div class="textwidget"><p>We, the Callaway County Sheriff’s Office, exist to serve all people with respect, fairness, and sensitivity. We are committed to the prevention of crime and the protection of life and property; the preservation of peace, order, and safety; the enforcement of laws, and the safeguarding of a citizen’s constitutional guarantees.</p>
<p>We are driven to enhance the quality of life, conduct thorough investigations, seek solutions and foster a sense of safety and security within our communities and towards its individual residents. We will nurture public trust by holding ourselves to the highest standards of professionalism, performance, and ethics. </p>
<p>We, as public servants, shall make ourselves available to respond to citizen needs. We shall foster an arena of communication and compassion through a positive customer service approach. We will demand excellence at each level of our command structure. </p>
</div>
</div> </div>
<div class="mh-sidebar hp-sidebar hp-home-6">
<div class="sb-widget home-6"><h4 class="widget-title">Quick Links</h4><div class="menu-home-menu-right-container"><ul class="menu" id="menu-home-menu-right"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36" id="menu-item-36"><a href="https://callawaysheriff.org/about-us/">About us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37" id="menu-item-37"><a href="https://callawaysheriff.org/sheriffs-blog/">Sheriff’s Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38" id="menu-item-38"><a href="https://callawaysheriff.org/records-division/">Record’s Division</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39" id="menu-item-39"><a href="http://callawaycounty.org/">Other County Offices</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40" id="menu-item-40"><a href="https://callawaysheriff.org/eoc-911-operations/">EOC / 911 Operations</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41" id="menu-item-41"><a href="https://callawaysheriff.org/neigborhood-watch/">Neigborhood Watch</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-42" id="menu-item-42"><a href="https://www.courts.mo.gov/casenet/base/welcome.do">Case Net</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-43" id="menu-item-43"><a href="http://maps.modot.mo.gov/travelerinformation/TravelerInformation.aspx">MODOT Road Information</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-44" id="menu-item-44"><a href="http://www.mshp.dps.missouri.gov/MSHPWeb/Root/index.html">Missouri State Highway Patrol</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-45" id="menu-item-45"><a href="http://www.callawaycardv.org/">Domestic Violence Assistance</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-46" id="menu-item-46"><a href="http://www.connectmidmissouri.com/weather/radar.aspx#.UQBmpyeYt8E">KRCG Weather Lab</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47" id="menu-item-47"><a href="https://callawaysheriff.org/g-w-law-award-recipents/">G.W. Law Award Recipents</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://callawaysheriff.org/previous-callaway-county-sheriffs/">Previous Callaway County Sheriff’s</a></li>
</ul></div></div><div class="sb-widget home-6"><h4 class="widget-title">Contact Information</h4> <div class="textwidget"><p>Callaway County Sheriff’s Office<br/>
1201 State Road O<br/>
PO Box 817<br/>
Fulton, MO 65251<br/>
Phone: 573-642-7291<br/>
Fax: 573-592-2440</p>
</div>
</div> </div>
</div>
</div>
</div>
<div class="copyright-wrap">
<p class="copyright">Copyright Callaway County Sheriff's Office, 2015</p>
</div>
</div>
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/lazy-images/js/lazy-images.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/c/5.3.6/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/spin.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/jquery.spin.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var jetpackCarouselStrings = {"widths":[370,700,1000,1200,1400,2000],"is_logged_in":"","lang":"en","ajaxurl":"https:\/\/callawaysheriff.org\/wp-admin\/admin-ajax.php","nonce":"a051ee7e8e","display_exif":"1","display_comments":"1","display_geo":"1","single_image_gallery":"1","single_image_gallery_media_file":"","background_color":"black","comment":"Comment","post_comment":"Post Comment","write_comment":"Write a Comment...","loading_comments":"Loading Comments...","download_original":"View full size <span class=\"photo-size\">{0}<span class=\"photo-size-times\">\u00d7<\/span>{1}<\/span>","no_comment_text":"Please be sure to submit some text with your comment.","no_comment_email":"Please provide an email address to comment.","no_comment_author":"Please provide your name to comment.","comment_post_error":"Sorry, but there was an error posting your comment. Please try again later.","comment_approved":"Your comment was approved.","comment_unapproved":"Your comment is in moderation.","camera":"Camera","aperture":"Aperture","shutter_speed":"Shutter Speed","focal_length":"Focal Length","copyright":"Copyright","comment_registration":"0","require_name_email":"1","login_url":"https:\/\/callawaysheriff.org\/wp-login.php?redirect_to=https%3A%2F%2Fcallawaysheriff.org%2F","blog_id":"1","meta_data":["camera","aperture","shutter_speed","focal_length","copyright"],"local_comments_commenting_as":"<fieldset><label for=\"email\">Email (Required)<\/label> <input type=\"text\" name=\"email\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-email-field\" \/><\/fieldset><fieldset><label for=\"author\">Name (Required)<\/label> <input type=\"text\" name=\"author\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-author-field\" \/><\/fieldset><fieldset><label for=\"url\">Website<\/label> <input type=\"text\" name=\"url\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-url-field\" \/><\/fieldset>"};
/* ]]> */
</script>
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/carousel/jetpack-carousel.min.js" type="text/javascript"></script>
<script src="https://c0.wp.com/p/jetpack/8.6.1/_inc/build/tiled-gallery/tiled-gallery/tiled-gallery.min.js" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:8.6.1',blog:'99814255',post:'6',tz:'0',srv:'callawaysheriff.org'} ]);
	_stq.push([ 'clickTrackerInit', '99814255', '6' ]);
</script>
</body>
</html>