<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Портал ваших знаний - znay.co</title>
<meta content="Безграничная кладезь уникальных знаний" name="description"/>
<meta content="биографии, наука, знание, история, обучение, информация, здоровье, советы,  дети и родители,  домашнее хозяйство, звёздная жизнь,  мода и стиль, правовые вопросы, вера и религия, значение сновидений, тайны и загадки, эзотерика,  лекарства, медицина, значения слов,увлечения и хобби,финансы, народная медицина,интернет и соц, сети," name="keywords"/>
<meta content="DataLife Engine (http://dle-news.ru)" name="generator"/>
<link href="https://znay.co/engine/opensearch.php" rel="search" title="Портал ваших знаний - znay.co" type="application/opensearchdescription+xml"/>
<script src="/engine/classes/min/index.php?charset=utf-8&amp;g=general&amp;19" type="text/javascript"></script>
<meta content="86b985b5e1be0bcc" name="yandex-verification"/>
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="default" name="apple-mobile-web-app-status-bar-style"/>
<link href="/engine/classes/min/index.php?f=/templates/Blogg-Y/css/style.css,/templates/Blogg-Y/css/engine.css,/templates/Blogg-Y/css/adaptive.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!--<link rel="apple-touch-icon" href="/templates/Blogg-Y/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="/templates/Blogg-Y/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/templates/Blogg-Y/images/apple-touch-icon-114x114.png">-->
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script async="" data-ad-client="ca-pub-6521248831690269" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script src="https://g52bxi1v1w.com/script.js"></script>
</head>
<body>
<script>
    (function (w, d, c) {
    (w[c] = w[c] || []).push(function() {
        var options = {
            project: 7106262,
        };
        try {
            w.top100Counter = new top100(options);
        } catch(e) { }
    });
    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src =
    (d.location.protocol == "https:" ? "https:" : "http:") +
    "//st.top100.ru/top100/top100.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(window, document, "_top100q");
</script>
<script type="text/javascript">
<!--
var dle_root       = '/';
var dle_admin      = '';
var dle_login_hash = '';
var dle_group      = 5;
var dle_skin       = 'Blogg-Y';
var dle_wysiwyg    = '0';
var quick_wysiwyg  = '2';
var dle_act_lang   = ["Да", "Нет", "Ввод", "Отмена", "Сохранить", "Удалить", "Загрузка. Пожалуйста, подождите..."];
var menu_short     = 'Быстрое редактирование';
var menu_full      = 'Полное редактирование';
var menu_profile   = 'Просмотр профиля';
var menu_send      = 'Отправить сообщение';
var menu_uedit     = 'Админцентр';
var dle_info       = 'Информация';
var dle_confirm    = 'Подтверждение';
var dle_prompt     = 'Ввод информации';
var dle_req_field  = 'Заполните все необходимые поля';
var dle_del_agree  = 'Вы действительно хотите удалить? Данное действие невозможно будет отменить';
var dle_spam_agree = 'Вы действительно хотите отметить пользователя как спамера? Это приведёт к удалению всех его комментариев';
var dle_complaint  = 'Укажите текст Вашей жалобы для администрации:';
var dle_big_text   = 'Выделен слишком большой участок текста.';
var dle_orfo_title = 'Укажите комментарий для администрации к найденной ошибке на странице';
var dle_p_send     = 'Отправить';
var dle_p_send_ok  = 'Уведомление успешно отправлено';
var dle_save_ok    = 'Изменения успешно сохранены. Обновить страницу?';
var dle_reply_title= 'Ответ на комментарий';
var dle_tree_comm  = '0';
var dle_del_news   = 'Удалить статью';
var allow_dle_delete_news   = false;
var dle_search_delay   = false;
var dle_search_value   = '';
jQuery(function($){
FastSearch();
});
//-->
</script>
<header class="header">
<div class="wrap">
<h5 class="logo">
<a href="/"><img alt="Znay.co - портал знаний." src="/templates/Blogg-Y/images/logo.png"/></a>
</h5>
<button class="nav-btn"></button>
<nav class="nav">
<div class="cat-menu">
<a href="/obshchestvo/"><div class="cat-menu-btn">Общество</div></a>
<ul class="cat-menu-list">
<li><a href="/deti-i-roditeli/">Дети и родители</a></li>
<li><a href="/domashnee-hozyajstvo/">Домашнее хозяйство</a></li>
<li><a href="/zvezdnaya-zhizn/">Звездная жизнь</a></li>
<li><a href="/moda-i-stil/">Мода и стиль</a></li>
<li><a href="/otnosheniya-i-psihologiya/">Отношения и психология</a></li>
<li><a href="/politika/">Политика</a></li>
<li><a href="/pravovye-voprosy/">Правовые вопросы</a></li>
<li><a href="/biografii/">Биографии</a></li>
<li><a href="/razvlecheniya-i-otdyh/">Развлечения и отдых</a></li>
</ul>
</div>
</nav>
<nav class="nav">
<div class="cat-menu">
<a href="/neobyasnimoe/"><div class="cat-menu-btn">Необъяснимое</div></a>
<ul class="cat-menu-list">
<li><a href="/vera-i-religiya/">Вера и религия</a></li>
<li><a href="/znachenie-snovidenij/">Значение сновидений</a></li>
<li><a href="/tajny-i-zagadki/">Тайны и загадки</a></li>
<li><a href="/ehzoterika/">Эзотерика</a></li>
</ul>
</div>
</nav>
<nav class="nav">
<div class="cat-menu">
<a href="/zdorove/"><div class="cat-menu-btn">Здоровье</div></a>
<ul class="cat-menu-list">
<li><a href="/beremennost-i-rody/">Беременность и роды</a></li>
<li><a href="/medicina/">Медицина</a></li>
<li><a href="/vred-i-polza/">Вред и польза</a></li>
<li><a href="/krasota-i-uhod/">Красота и уход</a></li>
<li><a href="/kulinariya/">Кулинария</a></li>
<li><a href="/lekarstva-ih-primenenie/">Лекарства</a></li>
<li><a href="/narodnaya-medicina/">Народная медицина</a></li>
<li><a href="/pitanie-i-zozh/">Питание и ЗОЖ</a></li>
<li><a href="/sport-i-fitnes/">Спорт и фитнес</a></li>
</ul>
</div>
</nav>
<nav class="nav">
<div class="cat-menu">
<a href="/znaniya/"><div class="cat-menu-btn">Знания</div></a>
<ul class="cat-menu-list">
<li><a href="/hi-tech-i-kompyuter/">Hi-tech и компьютер</a></li>
<li><a href="/znachenie-slov/">Значение слов</a></li>
<li><a href="/internet-i-socseti/">Интернет и соцсети</a></li>
<li><a href="/istoricheskie-fakty/">Исторические факты</a></li>
<li><a href="/obrazovanie/">Образование</a></li>
<li><a href="/rasteniya-i-zhivotnye/">Растения и животные </a></li>
<li><a href="/uvlecheniya-i-hobbi/">Увлечения и хобби</a></li>
<li><a href="/finansy/">Финансы </a></li>
<li><a href="/poleznye-sovety/">Полезные советы</a></li>
<li><a href="/tekhnika-i-tekhnologii/">Техника и технологии</a></li>
</ul>
</div>
</nav>
<nav class="nav">
<div class="cat-menu">
<div class="cat-menu-btn">Разное</div>
<ul class="cat-menu-list">
<li><a href="/raznye-voprosy/">Разные вопросы</a></li>
</ul>
</div>
</nav>
<div class="header-controls">
<div class="searchblock">
<form action="" method="post">
<input name="do" type="hidden" value="search"/>
<input name="subaction" type="hidden" value="search"/>
<input class="searchform" id="story" name="story" onblur='if (this.value == "") { this.value="Поиск"; }' onfocus='if (this.value == "Поиск") { this.value=""; }' type="text" value="Поиск"/>
<input class="searchbt" title="Найти" type="button" value=" "/>
</form>
</div>
<div class="login-btn"></div>
<div class="login_block">
<div class="close"></div>
<form action="" class="login_form" method="post">
<label for="login_name">Логин:</label>
<input class="inp" id="login_name" name="login_name" type="text"/>
<label for="login_password">Пароль (<a href="https://znay.co/index.php?do=lostpassword">забыли?</a>):</label>
<input class="inp" id="login_password" name="login_password" type="password"/>
<div><input class="ch_box_save" id="login_not_save" name="login_not_save" type="checkbox" value="1"/>
<label class="not_save_label" for="login_not_save"><span></span>Чужой компьютер</label></div>
<div style="margin-bottom:10px;">
</div>
<button class="enter-btn site_button" onclick="submit();" title="Войти" type="submit">Войти</button>
<input id="login" name="login" type="hidden" value="submit"/>
<div class="reg-link"><a href="https://znay.co/index.php?do=register" title="Регистрация на сайте">Регистрация</a></div>
</form>
</div>
</div>
</div>
</header>
<section class="container">
<div class="top-adv"><!-- Yandex.RTB R-A-603480-1 -->
<div id="yandex_rtb_R-A-603480-1"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-603480-1",
                renderTo: "yandex_rtb_R-A-603480-1",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script></div>
<div class="wrap content cf">
<div class="left">
<div class="hblock cf">
<h4>Последние статьи:</h4>
</div>
<div id="dle-content"><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/301-on-menya-ne-lyubit.html" style="background-image:url(https://znay.co/uploads/posts/2018-08/1533195568_on-menya-ne-lyubit-3.jpg);" title="Он меня не любит... Как быть?">
<div class="short_post_counts">
<span class="pop_views">1 067</span>
<span class="pop_comments">0</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/301-on-menya-ne-lyubit.html" title="Он меня не любит... Как быть?">Он меня не любит... Как быть?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a> / <a href="https://znay.co/obshchestvo/otnosheniya-i-psihologiya/">Отношения и психология</a></span>
</div>
<p class="short-story_post">Любовь – процесс обоюдный. Однако не всегда человек, который вам симпатичен отвечает вам тем же. Безответная любовь – частая спутница подросткового возраста, однако иногда она случается и в более старшем возрасте. Спустя некоторое время после возникновения чувств и проявления симпатии</p>
<div class="read_bt"><a href="https://znay.co/301-on-menya-ne-lyubit.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/598-chto-rasskazat-parnyu.html" style="background-image:url(https://znay.co/uploads/posts/2019-05/1558783242_chto-interesnogo-rasskazat-parnyu-6.jpg);" title="Что можно рассказать парню интересного?">
<div class="short_post_counts">
<span class="pop_views">15 645</span>
<span class="pop_comments">0</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/598-chto-rasskazat-parnyu.html" title="Что можно рассказать парню интересного?">Что можно рассказать парню интересного?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a>  / <a href="https://znay.co/obshchestvo/otnosheniya-i-psihologiya/">Отношения и психология</a></span>
</div>
<p class="short-story_post">Если вы с парнем знакомы уже длительное время, то тему для разговора найти будет несложно. Хотя даже зная друг друга, интересы не всегда совпадают. Поэтому время от времени придётся потрудиться. Расскажем, что можно рассказать интересного о себе и не только парню, с которым вы начали общаться.</p>
<div class="read_bt"><a href="https://znay.co/598-chto-rasskazat-parnyu.html">Читать далее</a></div>
</div>
</article><!-- Yandex.RTB R-A-603480-2 -->
<div id="yandex_rtb_R-A-603480-2"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-603480-2",
                renderTo: "yandex_rtb_R-A-603480-2",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/568-pochemu-v-odesse-mnogo-evreev.html" style="background-image:url(https://znay.co/uploads/posts/2019-05/1557314078_pochemu-v-odesse-mnogo-evreev-3.jpg);" title="Почему в Одессе так много евреев?">
<div class="short_post_counts">
<span class="pop_views">9 588</span>
<span class="pop_comments">0</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/568-pochemu-v-odesse-mnogo-evreev.html" title="Почему в Одессе так много евреев?">Почему в Одессе так много евреев?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/znaniya/">Знания</a>   / <a href="https://znay.co/znaniya/istoricheskie-fakty/">Исторические факты</a></span>
</div>
<p class="short-story_post">У многих Одесса ассоциируется c «еврейским» городом. В памяти сразу всплывает пара анекдотов про Абрамовича, а люди с хорошим образованием припомнят «Одесские рассказы» писателя Бабеля. Действительно ли в крупном украинском городе сегодня так много евреев и есть ли для этого</p>
<div class="read_bt"><a href="https://znay.co/568-pochemu-v-odesse-mnogo-evreev.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/675-kak-perezhit-smert-lyubimogo-muzha.html" style="background-image:url(https://znay.co/uploads/posts/2019-10/1571075111_kak-perezhit-smert-lyubimogo-muzha-4.jpg);" title="Как жене пережить смерть мужа?">
<div class="short_post_counts">
<span class="pop_views">664</span>
<span class="pop_comments">0</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/675-kak-perezhit-smert-lyubimogo-muzha.html" title="Как жене пережить смерть мужа?">Как жене пережить смерть мужа?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a>    / <a href="https://znay.co/obshchestvo/otnosheniya-i-psihologiya/">Отношения и психология</a></span>
</div>
<p class="short-story_post">Даже сильные духом мужчины тяжело переживают смерть близкого, на протяжении долгого времени живя воспоминаниями о нем. Что уж говорить про женщин, которые более эмоциональны. В некоторых случаях сильные переживания из-за смерти мужа доводят несчастных вдов до лечения у психиатра. В такой трудный</p>
<div class="read_bt"><a href="https://znay.co/675-kak-perezhit-smert-lyubimogo-muzha.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/466-pochemu-nelzya-pominat-vodkoj.html" style="background-image:url(https://znay.co/uploads/posts/2019-01/1548179641_pochemu-nelzya-pominat-vodkoy-3.jpg);" title="Почему нельзя поминать водкой?">
<div class="short_post_counts">
<span class="pop_views">1 285</span>
<span class="pop_comments">0</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/466-pochemu-nelzya-pominat-vodkoj.html" title="Почему нельзя поминать водкой?">Почему нельзя поминать водкой?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/neobyasnimoe/">Необъяснимое</a>     / <a href="https://znay.co/neobyasnimoe/vera-i-religiya/">Вера и религия</a></span>
</div>
<p class="short-story_post">Многие люди, особенно религиозные, придают большое значение различным традиционным обрядам. Среди них одним из наиболее значительных и важных является обряд поминок. По обычаю их устраивают родственники усопшего в память о нём. Чаще всего это мероприятие представляет собой коллективную трапезу в</p>
<div class="read_bt"><a href="https://znay.co/466-pochemu-nelzya-pominat-vodkoj.html">Читать далее</a></div>
</div>
</article><!-- Yandex.RTB R-A-603480-3 -->
<div id="yandex_rtb_R-A-603480-3"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-603480-3",
                renderTo: "yandex_rtb_R-A-603480-3",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/18-k-chemu-snitsya-zolotoe-kolco.html" style="background-image:url(https://znay.co/uploads/posts/2016-10/1475427713_k-chemu-snitsya-zolotoe-kolco-6.jpg);" title="К чему может сниться золотое кольцо?">
<div class="short_post_counts">
<span class="pop_views">1 044</span>
<span class="pop_comments">5</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/18-k-chemu-snitsya-zolotoe-kolco.html" title="К чему может сниться золотое кольцо?">К чему может сниться золотое кольцо?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/neobyasnimoe/">Необъяснимое</a>      / <a href="https://znay.co/neobyasnimoe/znachenie-snovidenij/">Значение сновидений</a></span>
</div>
<p class="short-story_post">К чему снится золотое кольцо, золотые украшения? Что означает такой сон?   Люди издавна обращали повышенное внимание на свои ночные грезы. Вещий сон может помочь избежать очень серьезного несчастья и воодушевить в трудную минуту. Очень часто женщин и девушек мучает вопрос, к чему снится</p>
<div class="read_bt"><a href="https://znay.co/18-k-chemu-snitsya-zolotoe-kolco.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/294-sms-dlya-podnyatiya-nastroeniya-muzhchine.html" style="background-image:url(https://znay.co/uploads/posts/2018-07/1532965366_sms-dlya-podnyatiya-nastroeniya-muzhchine-3.jpg);" title="SMS для поднятия настроения мужчине">
<div class="short_post_counts">
<span class="pop_views">122 296</span>
<span class="pop_comments">1</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/294-sms-dlya-podnyatiya-nastroeniya-muzhchine.html" title="SMS для поднятия настроения мужчине">SMS для поднятия настроения мужчине</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a>       / <a href="https://znay.co/obshchestvo/otnosheniya-i-psihologiya/">Отношения и психология</a></span>
</div>
<p class="short-story_post">Мы не всегда можем быть рядом с любимыми и друзьями и в данном случае современные технологии помогаю поддерживать общение. Если у любимого или знакомого парня неприятности и необходимо поддержать его, развеселить, отправьте ему забавное сообщение. Ниже мы приведём примеры смс для поднятия</p>
<div class="read_bt"><a href="https://znay.co/294-sms-dlya-podnyatiya-nastroeniya-muzhchine.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/486-slova-podderzhki-v-trudnuyu-minutu.html" style="background-image:url(https://znay.co/uploads/posts/2019-02/1550085405_slova-podderzhki-v-trudnuyu-minutu-3.jpg);" title="Какими словами можно поддержать человека в трудную минуту?">
<div class="short_post_counts">
<span class="pop_views">290 186</span>
<span class="pop_comments">1</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/486-slova-podderzhki-v-trudnuyu-minutu.html" title="Какими словами можно поддержать человека в трудную минуту?">Какими словами можно поддержать человека в трудную минуту?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a>        / <a href="https://znay.co/obshchestvo/otnosheniya-i-psihologiya/">Отношения и психология</a></span>
</div>
<p class="short-story_post">Часто бывает так, что на нас нападает косноязычие, на ум не приходят нужные слова, а банальные фразы и чужие цитаты рискуют быть восприняты с обидой. Хорошо, если под рукой окажутся слова поддержки в трудную минуту, написанные или сказанные простым языком.</p>
<div class="read_bt"><a href="https://znay.co/486-slova-podderzhki-v-trudnuyu-minutu.html">Читать далее</a></div>
</div>
</article><!--Тизерная ads.bid внизу под статьей-->
<div id="joka-devaqavoleqeyecoyebi">
<script>
!(function(w,m){(w[m]||(w[m]=[]))&&w[m].push(
{id:'joka-devaqavoleqeyecoyebi',block:'72893'}
);})(window, 'mtzBlocks');
</script>
</div><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/131-pochemu-v-rossii-tak-ploho-zhivut-lyudi.html" style="background-image:url(https://znay.co/uploads/posts/2017-10/1506958962_pochemu-v-rossii-tak-ploho-zhivut-lyudi-3.jpg);" title="Почему люди в России так бедно живут?">
<div class="short_post_counts">
<span class="pop_views">2 971</span>
<span class="pop_comments">15</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/131-pochemu-v-rossii-tak-ploho-zhivut-lyudi.html" title="Почему люди в России так бедно живут?">Почему люди в России так бедно живут?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/obshchestvo/">Общество</a>         / <a href="https://znay.co/obshchestvo/politika/">Политика</a></span>
</div>
<p class="short-story_post">Почему в России так плохо живут люди, в чем причина низкого уровня населения? Список самых худших городов по уровню жизни     Мы живем в одном из самых богатых природными ресурсами государств на планете. Мы по территории больше, чем Плутон. В военном отношении нам никто не может в</p>
<div class="read_bt"><a href="https://znay.co/131-pochemu-v-rossii-tak-ploho-zhivut-lyudi.html">Читать далее</a></div>
</div>
</article><article class="shortstory cf">
<a class="short_post post_img" href="https://znay.co/160-chto-podarit-podruge-na-novyj-god.html" style="background-image:url(https://znay.co/uploads/posts/2017-11/1511212151_chto-podarit-podruge-na-novyy-god-6.jpg);" title="Что можно подарить на Новый год подруге?">
<div class="short_post_counts">
<span class="pop_views">816</span>
<span class="pop_comments">9</span>
</div>
</a>
<div class="short_post_content">
<h2 class="short_title"><a href="https://znay.co/160-chto-podarit-podruge-na-novyj-god.html" title="Что можно подарить на Новый год подруге?">Что можно подарить на Новый год подруге?</a></h2>
<div class="short_post_meta">
<span class="short_cat"><a href="https://znay.co/znaniya/">Знания</a>          / <a href="https://znay.co/znaniya/poleznye-sovety/">Полезные советы</a></span>
</div>
<p class="short-story_post">Что подарить подруге на Новый год? 10 идей и советов, какой подарок выбрать, который точно ей понравится   В преддверии зимних праздников представители сильной половины населения России ломают голову над вечным вопросом: «Что подарить подруге на Новый год?». Но проблема при более</p>
<div class="read_bt"><a href="https://znay.co/160-chto-podarit-podruge-na-novyj-god.html">Читать далее</a></div>
</div>
</article><div class="navigation-holder basecont ignore-select">
<div class="navigation">
<b class="prev"><span>&lt;</span></b><span>1</span> <a href="https://znay.co/page/2/">2</a> <a href="https://znay.co/page/3/">3</a> <a href="https://znay.co/page/4/">4</a> <a href="https://znay.co/page/5/">5</a> <a href="https://znay.co/page/6/">6</a> <a href="https://znay.co/page/7/">7</a> <a href="https://znay.co/page/8/">8</a> <a href="https://znay.co/page/9/">9</a> <a href="https://znay.co/page/10/">10</a> <span class="nav_ext">...</span> <a href="https://znay.co/page/72/">72</a><b class="next"><a href="https://znay.co/page/2/">&gt;</a></b>
</div>
</div></div>
</div>
<aside class="right">
<div class="side_block">
<div class="side_header">Самое интересное</div>
<ul class="max_r_inter">
<img src="https://znay.co/uploads/posts/2017-09/1505930263_vitaliy-ostrovskiy-kto-eto-6.jpg" style="max-width: 100%"/>
<a href="https://znay.co/127-vitalij-ostrovskij-kto-ehto.html">
		Кто такой Виталий Островский?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2018-03/1521042950_kak-nayti-uchastnika-vov-po-familii-6.jpg" style="max-width: 100%"/>
<a href="https://znay.co/260-najti-veterana-vov.html">
		Где и как искать данные об участниках ВОВ?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2017-10/1509124282_pochemu-zapretili-svideteley-iegovy-v-rossii-6.jpg" style="max-width: 100%"/>
<a href="https://znay.co/147-pochemu-zapretili-svidetelej-iegovy-v-rossii.html">
		Почему секта свидетелей Иеговы запрещена в России?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2017-09/1506776334_pochemu-v-rossii-ne-lyubyat-lgbt-2.jpg" style="max-width: 100%"/>
<a href="https://znay.co/130-pochemu-v-rossii-ne-lyubyat-lgbt.html">
		Почему в России не любят ЛГБТ?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2019-09/1568112449_gde-vzyat-deneg-bezvozmezdno-3.jpg" style="max-width: 100%"/>
<a href="https://znay.co/654-gde-vzyat-deneg-bezvozmezdno.html">
		Где взять денег безвозмездно?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2019-03/1551723234_gde-seychas-gorbachev-5.jpg" style="max-width: 100%"/>
<a href="https://znay.co/503-gde-sejchas-zhivet-gorbachev.html">
		Где сейчас живет Горбачев?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2017-11/1510086882_poyas-van-allena-chto-eto-3.jpg" style="max-width: 100%"/>
<a href="https://znay.co/156-poyas-van-allena-chto-ehto.html">
		Что такое Пояс Ван Аллена?
	</a>
<p> </p> <img src="https://znay.co/uploads/posts/2017-10/1507108223_pochemu-v-rossii-chinovniki-ne-boyatsya-vorovat-4.jpg" style="max-width: 100%"/>
<a href="https://znay.co/132-pochemu-v-rossii-chinovniki-ne-boyatsya-vorovat.html">
		Почему российские чиновники не боятся воровать?
	</a>
<p> </p>
</ul>
</div>
<!-- Блок Гугл Адс справа в сайдбаре -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Блок справа в сайдбаре -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6521248831690269" data-ad-format="auto" data-ad-slot="2485522029" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!--noindex-->
<!-- Правающий блоjк справа!! -->
<script language="javascript"> $(document).ready(function(){ var floatsidebar = $("#float-sidebar"); var offset = floatsidebar.offset(); var left = offset.left; var top = offset.top; var width = $("#float-sidebar").width(); var height = $("#float-sidebar").height();  $(window).scroll(function(){ var scrollTop = $(window).scrollTop(); if (scrollTop >= top) { $('#float-sidebar').css({ left:left+'px', position:'fixed', top:"10px", width:width+"px" }); } else {  $('#float-sidebar').css({ position:'static', }); } }); }); </script>
<div id="float-sidebar" rel="nofollow" style="margin-top: 30px">
<p> </p>
<!-- Yandex.RTB БЛОК ПЛАВ!!-->
</div>
<!-- Крнец блока справа!!  -->
<!--/noindex-->
</aside>
</div>
<footer class="footer wrap cf">
<div class="left">
<div class="logo">
<a href="/"><img alt="Портал знаний: znay.co" src="/templates/Blogg-Y/images/logo.png"/></a>
</div>
<!--noindex-->
<div class="footer_menu">
<ul>
<li><a href="/index.php?do=feedback">Обратная связь</a></li>
<li><a href="/index.php?do=feedback">Контакты</a></li>
</ul>
</div>
<p> </p>
<div class="footer-text pull-left">
<span style="font-size: 12pt; color: #ffeb00;"><b>Заметили ошибку? Сообщите нам! Выделите ее и нажмите Ctrl+Enter. Спасибо!</b></span>
</div>
<p> </p>
<!--/noindex-->
<div class="about">
                Вся графическая и текстовая информация на данном сайте предоставлена исключительно с целью ознакомления и носит развлекательный характер. Перед применением любого лекарственного средства или метода лечения настоятельно рекомендуем обратиться к лечащему врачу!
            </div>
<div class="copyrights">Copyright © <script language="javascript" type="text/javascript">
	<!--
    var d = new Date();
    document.write(" " + d.getFullYear() + "");
    //-->
	</script> все права защищены.</div>
<div class="counters">
<span>
<!--Счетчики!!.-->
<!--noindex-->
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t44.11;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65797630, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/65797630" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--/noindex-->
</span>
</div>
</div>
<div class="right">
<ul class="social_menu">
<li><a class="fb_icon" href="#" rel="nofollow" target="_blank" title="Facebook">Facebook</a></li>
<li><a class="tw_icon" href="#" rel="nofollow" target="_blank" title="Twitter">Twitter</a></li>
<li><a class="vk_icon" href="#" rel="nofollow" target="_blank" title="ВКонтакте">ВКонтакте</a></li>
<li><a class="gp_icon" href="#" rel="nofollow" target="_blank" title="Google+">Google</a></li>
</ul>
</div>
</footer>
</section>
<script type="text/javascript"> 
$(function() { 
$(window).scroll(function() { 
if($(this).scrollTop() != 0) { 
$('#totop').fadeIn(); 
} else { 
$('#totop').fadeOut(); 
} 
}); 
$('#totop').click(function() { 
$('body,html').animate({scrollTop:0},800); 
}); 
}); 
</script>
<div id="totop"></div>
<script async="" src="/templates/Blogg-Y/js/jquery-migrate-1.2.1.js" type="text/javascript"></script>
<script async="" src="/templates/Blogg-Y/js/libs.js" type="text/javascript"></script>
<script src="//vk.com/js/api/openapi.js?101" type="text/javascript"></script>
<!--noindex-->
<!-- PushProfit -->
<script async="" src="https://allstat-pp.ru/1007060/a191167723a6f6586d10cc62765d5e9a7b1792c2.js"></script>
<!-- MuRSAniz -->
<div class="myblockbottom" style="display: none">
<div class="myblockbottom__close"></div>
</div>
<script type="text/javascript">
setTimeout(function(){
(function() {
  
  document.addEventListener("scroll", checkIfNearPosition)

	document.querySelector('.myblockbottom__close').addEventListener('click', function(e) {
  	e.target.parentNode.style.display = 'none', document.removeEventListener("scroll", checkIfNearPosition)
  })
  
  function checkIfNearPosition(e) {
		if(!/iPhone|iPad|iPod|webOS|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile|Android/i.test(navigator.userAgent)
) return;
    var x = document.querySelector('.myblockbottom');
  	window.pageYOffset > 100 && (x.style.display = 'block') || (x.style.display = 'none')
  }
  
})() }, 10000);
</script>
<!-- /MuRSAniz -->
<!--/noindex-->
</body>
</html>