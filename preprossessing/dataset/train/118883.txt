<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<body><p>This site (www.ams-net.org) is used only for the AMS Doctoral Dissertations in Musicology and Awards sites. </p>
<p>For other American Musicological Society information, please see the main AMS website, <strong><a href="https://www.amsmusicology.org">www.amsmusicology.org</a></strong><a href="https://www.amsmusicology.org">.</a></p>
<p><a href="https://www.ams-net.org/ddm/">Go to Doctoral Dissertations in Musicology</a></p>
<p><a href="https://www.ams-net.org/awards/Nominations.php">Go to AMS Awards</a></p>
</body></html>