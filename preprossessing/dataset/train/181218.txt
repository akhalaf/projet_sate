<html>
<head><title>b-complete.se</title>
<style type="text/css">

body, p, td{
font-family:Verdana;
font-size: 8pt;
font-weight:normal;
color:#FFFFFF;
margin:0px;
}

blockquote, ol, ul{
margin: 0px 0px 0px 40px;
padding: 0px;
}

h1{
font-family:Times New Roman;
font-size: 36pt;
font-weight:normal;
color:#FFFFFF;
margin:0px;
}

h2{
font-family:Times New Roman;
font-size: 24pt;
font-weight:normal;
color:#FFFFFF;
margin:0px;
}

h3{
font-family:Times New Roman;
font-size: 18pt;
font-weight:normal;
color:#FFFFFF;
margin:0px;
}

h4{
font-family:Times New Roman;
font-size: 12pt;
font-weight:normal;
color:#FFFFFF;
margin:0px;
}

pre{
margin: 0px;
}

a{
color:#ff7074;
text-decoration:none;
}

a:hover{
color:#FFFFFF;
text-decoration:underline;
}

.menu td a
{
  font-family:Verdana;
  font-size: 8pt;
  color:#FFFFFF; 
  font-weight:normal;
}

.menu td a:hover
{
  color:#fb4537; 
  text-decoration:none;
}
</style>
</head>
<body style="MARGIN-TOP: 75px; BACKGROUND: #e1d6b4; MARGIN-BOTTOM: 50px">
<div align="center" id="maindiv1"><div id="maindiv2" style="WIDTH: 854px; POSITION: relative">
<table align="center" border="1" bordercolor="#aa2729" cellpadding="0" cellspacing="0" id="maintable" style="BACKGROUND: #a92426; POSITION: relative; BORDER-COLLAPSE: collapse" width="857">
<tbody>
<tr>
<td height="12" style="BACKGROUND: url(/images/valentine/top-bg.gif) repeat-x" valign="top"><img height="1" width="1"/></td></tr>
<tr>
<td valign="top"><img src="/images/valentine/ribbon.gif" style="LEFT: 534px; POSITION: absolute; TOP: 3px"/>
<table border="0" cellpadding="0" cellspacing="0" class="menu" meta="menu" metaid="Min hemsida" style="LEFT: 64px; POSITION: absolute; TOP: 12px; BORDER-COLLAPSE: collapse">
<tbody>
<tr>
<td valign="top"><font color="#fb4537">Hem</font></td></tr></tbody></table>
<table align="left" border="0" cellpadding="0" cellspacing="0" style="BORDER-COLLAPSE: collapse" width="100%">
<tbody>
<tr>
<td style="PADDING-RIGHT: 50px; PADDING-LEFT: 64px" valign="top"><img src="/images/valentine/cupid-small.gif" style="LEFT: 408px; POSITION: absolute; TOP: 69px"/>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<h1><i><b>Jonas &amp; Emelie</b></i></h1>
<p><span style="FONT-SIZE: 12pt"><strong><font face="Monotype Corsiva, Lucida Handwriting, Apple Chancery, Brush Script MT, Cursive">alla hjärtan dag 2009</font></strong></span></p>
<p> </p>
<p> </p>
<p align="justify"><span style="FONT-SIZE: 12pt"><font face="Monotype Corsiva, Lucida Handwriting, Apple Chancery, Brush Script MT, Cursive">Om kärlek var vatten, jag skulle ge dig ett hav.<br/>Om lycka var vindar, jag skulle ge dig en hel orkan.<br/>Om pengar var eld, jag skulle köpa dig en kamin.<br/>Om sex var regn, skulle jag ge dig en översvämning. </font></span></p>
<p><span style="FONT-SIZE: 12pt"><font face="Monotype Corsiva, Lucida Handwriting, Apple Chancery, Brush Script MT, Cursive"></font></span> </p>
<p align="justify"><span style="FONT-SIZE: 12pt"><font face="Monotype Corsiva, Lucida Handwriting, Apple Chancery, Brush Script MT, Cursive">Det fins dagar som jag tvivlar, det fins dagar som jag tror, men alla dom dagarna jag tvivlar och tror har en gemensam bro som leder till ditt hjärta som jag föralltid tror.</font></span></p>
<p> </p>
<p>Av Jonas till Emelie 14/2-2009</p>
<p> </p>
<p> </p>
<p> </p>
<p> </p></td>
<td style="PADDING-RIGHT: 20px; PADDING-LEFT: 15px; BACKGROUND: url(/images/valentine/strawberries.jpg) no-repeat right bottom" valign="top" width="365">
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p>
<p> </p></td></tr></tbody></table></td></tr>
<tr>
<td height="12" style="BACKGROUND: url(/images/valentine/bot-bg.gif) repeat-x" valign="top"><img height="1" width="1"/></td></tr></tbody></table>
</div></div>
</body></html>