<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
<base href="https://www.buzoneodirecto.com/"/>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>ERROR 404</title>
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.buzoneodirecto.com/404" hreflang="es-ES" rel="alternate"/>
<link href="https://www.buzoneodirecto.com/404" rel="canonical"/>
<meta content="index,follow" name="robots"/>
<!-- CSS -->
<link href="css/reset.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/hover.css" rel="stylesheet"/>
<link href="css/font-awesome.css" rel="stylesheet"/>
<link href="css/animate.css" rel="stylesheet" type="text/css"/>
<link href="./dist/skitter.css" media="all" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-61187163-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-61187163-2');
</script>
</head><body><div class="container">
<!-- CONTENIDO WEB -->
<!-- HEADER -->
<div class="house_idiomas">
<div class="house_idiomas_in">
<div class="house"><a href="https://www.buzoneodirecto.com"><span aria-hidden="true" class="fa fa-home"></span></a></div>
<div class="tel_email">
<a href="https://www.facebook.com/buzoneo.directo/" target="_blank"><span aria-hidden="true" class="fa fa-facebook-square"></span></a>
<a href="tel:+34931148939"><span aria-hidden="true" class="fa fa-phone-square"></span> 93 114 89 39</a>
<a href="mailto:buzoneodirecto@buzoneodirecto.com">
<span aria-hidden="true" class="fa fa-envelope"></span> buzoneodirecto@buzoneodirecto.com</a>
</div>
<div class="clear"></div>
</div>
</div>
<!-- Menu -->
<div class="menu_logo">
<div class="menu_logo_in">
<div class="logo"><a href="https://www.buzoneodirecto.com">
<img alt="Buzoneo en Barcelona" src="imgs/buzoneo-en-barcelona.jpg"/></a></div>
<div class="menu">
<ul>
<li>
<h2><a class="hvr-bounce-to-top" href="https://www.buzoneodirecto.com">Quienes Somos</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="servicios">Servicios</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="geobd">GEOBD</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="ofertas">Ofertas</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="contacto">Contacto</a></h2>
</li>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
<!-- Menu Mobil-->
<div class="menu_logo_mov">
<div class="menu_logo_mov_in">
<p><a href="https://www.buzoneodirecto.com">
<img alt="Buzoneo en Barcelona" src="imgs/buzoneo-en-barcelona.jpg"/></a></p>
<ul>
<li>
<h2><a class="hvr-bounce-to-top" href="https://www.buzoneodirecto.com">Quienes Somos</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="servicios">Servicios</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="geobd">GEOBD</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="ofertas">Ofertas</a></h2>
</li>
<li>
<h2><a class="hvr-bounce-to-top" href="contacto">Contacto</a></h2>
</li>
</ul>
</div>
</div>
<!-- SLIDE -->
<div class="slide">
<div class="skitter skitter-large">
<ul>
<li><a href="https://www.buzoneodirecto.com">
<img alt="buzoneodirecto.com" class="directionTop" src="slide/empresa-buzoneo-barcelona.jpg"/></a></li>
<li><a href="https://www.buzoneodirecto.com">
<img alt="buzoneodirecto.com" class="directionBottom" src="slide/reparto-publicidad-parcelona.jpg"/></a></li>
</ul>
</div>
</div>
<!-- / slide -->
<!-- contenido web -->
<div class="contenido_web">
<div class="contenido_web_in">
<h1 class="titulo_h1">ERROR 404</h1>
<p>La página que busca no existe.</p>
</div>
</div>
<!-- / CONTENIDO WEB -->
</div>
<!-- pie -->
<div class="pie">
<ul>
<li>
<div>
<h2>CENTRAL</h2>
<p>Ctra. Nacional 150 Km 2.6 Nave 9 · Pol. Ind. Hermes 08110 Montcada i Reixac (Barcelona) <a class="ir_a_google" href="https://www.google.es/maps/place/Buzoneo+Directo,+S.L/@41.6890949,0.6230645,8z/data=!3m1!4b1!4m5!3m4!1s0x12a4bdeb23e8e7ff:0xed3e5e3aee9d1180!8m2!3d41.8117184!4d1.5035151?dcr=0" target="_blank"><span aria-hidden="true" class="fa fa-street-view"></span> Ir a Google Maps</a></p>
</div>
</li>
<li>
<div>
<h2>LOCALES DEL GARRAF</h2>
<p>Carretera BV2112 Km 4 - 08810 Sant Pere de Ribes
					<br/>(Barcelona)
				</p>
</div>
</li>
<li>
<div>
<h2><a href="tel:+34931148939">Teléfono: 93 114 89 39</a></h2>
<h2>Fax: 93 313 62 15</h2>
<h2><a href="mailto:buzoneodirecto@buzoneodirecto.com">Email: buzoneodirecto@buzoneodirecto.com</a></h2>
</div>
</li>
</ul>
<p class="disseny"><a href="https://www.potenciatuimagen.es" target="_blank">Diseño</a> <a href="aviso-legal">Aviso Legal</a></p>
</div>
<!-- Javascripts -->
<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="./dist/jquery.skitter.min.js"></script>
<script>
	    
	    $(document).ready(function() {
	$('.skitter-large').skitter({
		numbers: false,
		interval: 2000,
		velocity: 2,
		dots: false
	});
});
		    
		    </script>
<div class="cookies">
<!-- <h2 class="cookies__titulo">¿Quieres nuestras Cookies?</h2>-->
<p class="cookies__texto">Utilizamos cookies propias y de terceros para el correcto funcionamiento del sitio web, y si nos da su consentimiento, <br/>también utilizaremos cookies para recopilar datos de sus visitas para obtener estadísticas agregadas para mejorar nuestros servicios.</p>
<div class="cookies__botones">
<!-- Si -->
<button class="cookies__boton cookies__boton--si">Aceptar cookies</button>
<!-- No -->
<button class="cookies__boton cookies__boton--no">Rechazar cookies</button>
<!-- Ir a -->
<button onclick="location.href='../politica-de-cookies'">Mas información</button>
</div>
</div>
<script>
	
	
	document.addEventListener('DOMContentLoaded', () => {

    let cookies = () => {
        //======================================================================
        // COOKIES
        //======================================================================

        //-----------------------------------------------------
        // Variables
        //-----------------------------------------------------
        let seccionCookie = document.querySelector('.cookies');
        let cookieSi = document.querySelector('.cookies__boton--si');
        let cookieNo = document.querySelector('.cookies__boton--no');

        //-----------------------------------------------------
        // Funciones
        //-----------------------------------------------------

        /**
         * Método que oculta la sección de Cookie para siempre
         */
        function ocultarCookie() {
            // Borra la sección de cookies en el HTML
            seccionCookie.remove();
        }

        /**
         * Método que marca las cookies como aceptadas
         */
        function aceptarCookies() {
            // Oculta el HTML de cookies
            ocultarCookie();
            // Guarda que ha aceptado
            localStorage.setItem('cookie', true);
            // Tu codigo a ejecutar si aceptan las cookies
            ejecutarSiAcepta();
        }

        /**
         * Método que marca las cookies como denegadas
         */
        function denegarCookies() {
            // Oculta el HTML de cookies
            ocultarCookie();
            // Guarda que ha aceptado
            localStorage.setItem('cookie', false);
        }

        /**
         * Método que ejecuta tu código si aceptan las cookies
         */
        function ejecutarSiAcepta() {
            /////////////////// Tu código ////////////////
            ////////////  ¿Google Analítics? /////////////
        }

        /**
         * Método que inicia la lógica
         */
        function iniciar() {
            // Comprueba si en el pasado el usuario ha marcado una opción
            if (localStorage.getItem('cookie') !== null) {
                if(localStorage.getItem('cookie') === 'true') {
                    // Aceptó
                    aceptarCookies();
                } else {
                    // No aceptó
                    denegarCookies();
                }
            }
        }

        //-----------------------------------------------------
        // Eventos
        //-----------------------------------------------------
        cookieSi.addEventListener('click',aceptarCookies, false);
        cookieNo.addEventListener('click',denegarCookies, false);

        return {
            iniciar: iniciar
        }
    }

    // Activa el código. Comenta si quieres desactivarlo.
    cookies().iniciar();
});


</script>
</body>
</html>
