<!DOCTYPE html>
<html>
<head>
<title>梆梆安全 - 移动安全领导品牌，保护智能生活，共建智慧城市！</title>
<meta content="webkit|ie-comp|ie-stand" name="renderer"/>
<meta content="webkit" name="renderer"/>
<meta content="梆梆,应用加固,应用保护,APP加固,应用加密,安卓加固,APP保护,APK加密,安卓文件加密,应用安全,防破解,APK反编译,防止反编译,安卓盗版,移动应用安全,移动应用保护,Android应用加固,渠道监控,漏洞扫描,盗版监控" name="keywords"/>
<meta content="梆梆安全是全球领先的移动应用加固服务提供商，专业的应用加密加固技术，能有效防止安卓APP被破解、反编译、二次打包等威胁，同时提供APK安全检测、渠道盗版监测、移动安全威胁感知等服务。" name="description"/>
<link href="/public/images/favicon.ico" rel="shortcut icon"/>
<meta charset="utf-8"/>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=1280,initial-scale=0.2" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="/public/stylesheets/bootstrap-3.0.3.min.css" rel="stylesheet"/>
<link href="/public/stylesheets/common.css" rel="stylesheet" type="text/css"/>
<link href="/public/stylesheets/nav.css" rel="stylesheet" type="text/css"/>
<link href="/public/stylesheets/index.css" rel="stylesheet" type="text/css"/>
<script src="/public/javascripts/jquery-1.11.3.js"></script>
<script src="/public/javascripts/bootstrap-3.0.3.min.js"></script>
<script language="" src="/public/javascripts/nav.js"></script>
<script src="/public/javascripts/common.js"></script>
<script language="" src="/public/javascripts/index.js"></script>
<script language="" src="/public/javascripts/kissy.js"></script>
<script language="" src="/public/javascripts/share.js"></script>
<script language="" src="/public/javascripts/jgestures.min.js"></script>
<script language="" src="/public/javascripts/jquery.qrcode.min.js"></script>
<!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/public/stylesheets/ie.css">
    <script src="/public/javascripts/html5.js"></script>
    <script src="/public/javascripts/respond.min.js"></script>
    <![endif]-->
<script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?a4f7c328c5b0ee92ab38b177e65fb92f";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>
<body>
<div class="nav">
<div class="nav-in">
<img alt="" id="logo_min" onclick="window.location.href='/'" src="/upload/file/20160527/14643448981299.png"/>
<img id="btn_china" src="/public/images/china.png"/>
<div class="phone-nav">
<div class="nav-btn">
<span class="bt-l"></span>
<span class="bt-r"></span>
</div>
<img alt="" class="phone-logo" src="/public/images/logo.png"/>
</div>
<h1 class="logo">
<a href="/"></a>
</h1>
<ul class="nav-l">
<li>
<a class="top_nav_active" href="/">首页</a>
</li>
<li class="nav-it" id="btn-1">
<a href="/products/productindex" id="productCategory">产品与服务</a>
<i class="dost" id="dost-1"></i>
<div class="nav-next" id="nav-1">
<div class="nav-in">
<div class="back"><i class="iconfont text-center"></i></div>
<input id="hidden-1" type="hidden" value="8"/>
<ul class=" clearfix next-list">
<li class="first_li">
<a href="/products/productindex?category_id=1">泰固</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=1'">
                                        移动应用安全加固
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=2'">
                                        移动应用源代码加固
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=3'">
                                        安全密钥白盒
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=16'">
                                        安全软键盘SDK
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=17'">
                                        防界面劫持SDK
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=19'">
                                        反外挂SDK
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=21'">
                                        通信协议加密SDK
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=35'">
                                        数据防泄露系统
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=11">泰聚</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=41'">
                                        应用安全开发管控平台
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=2">泰镜</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=4'">
                                        渠道监测及响应
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=7'">
                                        漏洞监测及响应
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=26'">
                                        业务流审计系统
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=10">泰知</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=22'">
                                        移动威胁感知平台
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=3">泰睿</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=5'">
                                        移动应用测评云平台
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=8'">
                                        移动应用渗透性测试
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=27'">
                                        移动应用合规性测试
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=28'">
                                        移动应用源代码审计
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=34'">
                                        移动应用质量测试
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=36'">
                                        APM性能监控
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=4">泰极</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=6'">
                                        移动应用业务安全咨询
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=9'">
                                        安全开发及设计咨询
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=30'">
                                        大数据采集
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=31'">
                                        大数据存储与分析
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=32'">
                                        大数据安全建模
                                    </li>
</ul>
</li>
<li class="first_li">
<a href="/products/productindex?category_id=13">IoT安全</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=43'">
                                        IoT安全编译器SI²S
                                    </li>
<li onclick="window.location.href='/products/productindex?product_id=44'">
                                        固件安全检测平台
                                    </li>
</ul>
</li>
<li class="first_li" style="border-right:none">
<a href="/products/productindex?category_id=14">SDK安全</a>
<ul class="second_ul">
<li onclick="window.location.href='/products/productindex?product_id=46'">
                                        SDK安全测评平台
                                    </li>
</ul>
</li>
<div style="clear:both"></div>
</ul>
<div style="clear:both"></div>
</div>
</div>
</li>
<li class="nav-it" id="btn-2">
<a href="/solutions/index.html" id="solutionCategory">行业方案</a>
<i class="dost" id="dost-2"></i>
<div class="nav-next" id="nav-2">
<div class="nav-in">
<input id="hidden-2" type="hidden" value="8"/>
<div class="back"><i class="iconfont text-center"></i></div>
<ul class=" clearfix next-list">
<li class="first_li1">
<a href="/solutions/index.html?category_id=5">金融</a>
<ul class="second_ul">
<li class="second_li">银行、证券、保险、基金、互联网金融、第三方支付、期货等</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=17">开放银行</a>
<ul class="second_ul">
<li class="second_li">国有银行、股份制银行、城商行</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=6">游戏</a>
<ul class="second_ul">
<li class="second_li">手机游戏、工具类应用</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=7">政企</a>
<ul class="second_ul">
<li class="second_li">政府机构（税务、交通、测评机构、政府平台、智能政务、移动OA等）、大型企业（电力、能源、医疗等）</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=8">运营商</a>
<ul class="second_ul">
<li class="second_li">游戏基地、计费通道、应用发行商店、各省子公司增值移动应用</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=9">IoT</a>
<ul class="second_ul">
<li class="second_li">物联网、穿戴设备、智能汽车、智能家电、无人机、机器人</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=15">教育</a>
<ul class="second_ul">
<li class="second_li">高等院校、职业院校、中小学、幼儿园、学前教育、启蒙教育、学校管理服务、教育企业、家长服务等</li>
</ul>
<div style="clear:both"></div>
</li>
<li class="first_li1">
<a href="/solutions/index.html?category_id=16">中小企业</a>
<ul class="second_ul">
<li class="second_li">开发者，互联网，电商</li>
</ul>
<div style="clear:both"></div>
</li>
</ul>
<div style="clear:both"></div>
</div>
</div>
</li>
<li>
<a href="/articles/index.html">梆梆资讯</a>
</li>
<!--    <li>
                                                <a class="party-color-hover" href="/partybuild/index.html">梆梆党建</a>
                            </li>-->
<li>
<a href="/development/index.html">合作发展</a>
</li>
<li>
<a href="/abouts/index">关于梆梆</a>
</li>
</ul>
<ul class="nav-r fr">
<!-- <li class="btn_china"><a><img src="/public/images/china.png"></a></li> -->
<li>
<button class="kfz_btn" onclick="window.open('http://dev.bangcle.com/')"></button>
</li>
</ul>
</div>
<!--二级导航 begin-->
<!--二级导航end-->
<!--二级导航第二个 begin-->
</div>
<div class="doLayout" id="doLayout">
<script>
$(document).ready(function(){
    //手势右滑 回到上一个画面
    $('#myCarousel1,#myCarousel').bind('swiperight swiperightup swiperightdown',function(){
        $("#myCarousel1,#myCarousel").carousel('prev');
    })
    //手势左滑 进入下一个画面
    $('#myCarousel1,#myCarousel').bind('swipeleft swipeleftup swipeleftdown',function(){
        $("#myCarousel1,#myCarousel").carousel('next');
    })
})
</script>
<div style="position: relative;">
<div class="carousel slide" data-ride="carousel" id="myCarousel">
<!--水平 轮播（Carousel）指标 --><!-- data-ride="carousel" -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"><div></div></li>
<li data-slide-to="1" data-target="#myCarousel"><div></div></li>
<li data-slide-to="2" data-target="#myCarousel"><div></div></li>
</ol>
<!-- 轮播（Carousel）项目 -->
<div class="carousel-inner">
<div class="item top active" style="min-height: 600px;">
<div class="header" style="background-image: url('/upload/file/20200723/15954895784704.jpg');background-size: cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/upload/file/20200723/15954895784704.jpg',sizingMethod='scale');background: url(/upload/file/20200723/15954895784704.jpg) no-repeat scroll center center / 100% 100%;">
</div>
<p class="topNews_title" style="text-align:left"><br/>
<a href="https://www.bangcle.com/articles/detail?article_id=724" target="_blank"><button class="topNews_btn" onmouseout="out_color(this,'#2D92DA')" onmouseover="over_color(this,'#1b5983')" style="background:#2D92DA;border:none;">了解详情</button></a>
</p>
</div>
<div class="item top" style="min-height: 600px;">
<div class="header" style="background-image: url('/upload/file/20160629/14671907409983.jpg');background-size: cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/upload/file/20160629/14671907409983.jpg',sizingMethod='scale');background: url(/upload/file/20160629/14671907409983.jpg) no-repeat scroll center center / 100% 100%;">
</div>
<p class="topNews_title" style="text-align:left"><br/>
<a href="http://dev.bangcle.com" target="_blank"><button class="topNews_btn" onmouseout="out_color(this,'#2D92DA')" onmouseover="over_color(this,'#1b5983')" style="background:#2D92DA;border:none;">免费体验</button></a>
</p>
</div>
<div class="item top" style="min-height: 600px;">
<div class="header" style="background-image: url('/upload/file/20201020/16031755624046.jpg');background-size: cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/upload/file/20201020/16031755624046.jpg',sizingMethod='scale');background: url(/upload/file/20201020/16031755624046.jpg) no-repeat scroll center center / 100% 100%;">
</div>
<p class="topNews_title" style="text-align:left"><br/>
</p>
</div>
</div>
</div>
<div class="news">
<div class="container" style="min-width: 1170px;background: #000;">
<div class="new_pc" style="position: relative;">
<div class="news_index">
<img onclick="window.location.href='/articles/index.html?category_id=0&amp;page=1&amp;pagesize=10'" src="/public/images/news_title.png" style="cursor:pointer;float:left;margin-top:17px;"/>
<div id="div1">
<a href="/articles/detail?article_id=782" target="_blank" title="一心为民，砥砺奋进新征程  [2021-01-10]"><span class="new_span1">一心为民，砥砺奋进新征程<span class="new_span2">[2021-01-10]</span></span></a>
<a href="/articles/detail?article_id=781" target="_blank" title="喜报 | 梆梆安全成功入选北京市专精特新“小巨人”企业  [2020-12-21]"><span class="new_span1">喜报 | 梆梆安全成功入选北京市专精特新“小巨人”企业<span class="new_span2">[2020-12-21]</span></span></a>
<a href="/articles/detail?article_id=779" target="_blank" title="榜上有名｜2020中关村金融科技30强榜单  [2020-12-18]"><span class="new_span1">榜上有名｜2020中关村金融科技30强榜单<span class="new_span2">[2020-12-18]</span></span></a>
<a href="/articles/detail?article_id=778" target="_blank" title="实力入选物联网安全创新领域十强，获评2020年网络安全物创新能力100强企业  [2020-12-17]"><span class="new_span1">实力入选物联网安全创新领域十强，获评2020年网络安全物创新能力100强企业<span class="new_span2">[2020-12-17]</span></span></a>
<a href="/articles/detail?article_id=777" target="_blank" title="移动办公安全，梆梆安全是这样做的  [2020-12-15]"><span class="new_span1">移动办公安全，梆梆安全是这样做的<span class="new_span2">[2020-12-15]</span></span></a>
<a href="/articles/detail?article_id=776" target="_blank" title="梆梆安全出席第四届智能协同云技术与产业发展高峰论坛  [2020-12-14]"><span class="new_span1">梆梆安全出席第四届智能协同云技术与产业发展高峰论坛<span class="new_span2">[2020-12-14]</span></span></a>
</div>
</div>
<div class="news_index_more"><a href="/articles/index.html">More&gt;&gt;</a></div>
</div>
<table class="new_phone table ">
<tr>
<td>
<img src="/public/images/news_title.png"/>
</td>
<td>
<div id="div2">
<a>一心为民，砥砺奋进新征程<span>[2021-01-10 00:00]</span></a>
<a>喜报 | 梆梆安全成功入选北京市专精特新“小巨人”企业<span>[2020-12-21 00:00]</span></a>
<a>榜上有名｜2020中关村金融科技30强榜单<span>[2020-12-18 00:00]</span></a>
<a>实力入选物联网安全创新领域十强，获评2020年网络安全物创新能力100强企业<span>[2020-12-17 00:00]</span></a>
<a>移动办公安全，梆梆安全是这样做的<span>[2020-12-15 00:00]</span></a>
<a>梆梆安全出席第四届智能协同云技术与产业发展高峰论坛<span>[2020-12-14 00:00]</span></a>
</div>
</td>
<td class="more"><a href="/articles/index.html">More&gt;&gt;</a></td>
</tr>
</table>
</div>
</div>
</div>
<div class="container" style="margin-bottom: 100px;">
<div class="core_staff">
<div class="tarticle_div">
<div class="box-shadow-4">
<img class="share_img" onclick="window.open('/articles/detail?article_id=748')" src="/upload/16037715087559.jpg"/>
<div class="share">
<p class="p1"><a href="/articles/detail?article_id=748" target="_blank" title="心十载 梆未来——热烈祝贺梆梆安全成立十周年！">心十载 梆未来——热烈祝贺梆梆安全成立十周年！</a></p>
<div class="share_all" id="share_all1">
<img class="share_btn" id="share_btn1" src="/public/images/share_btn.png"/>
<div></div><div class="weixin"></div><div class="qq_share" onclick="jump('qq',this);"></div><div class="sina_share" onclick="jump('weibosina',this);"></div>
</div>
</div>
</div>
<p class="eng_info">祝梆梆安全十周岁生日快乐！</p>
</div>
<div class="tarticle_div">
<div class="box-shadow-4">
<img class="share_img" onclick="window.open('/articles/detail?article_id=740')" src="/upload/16008309471451.jpg"/>
<div class="share">
<p class="p1"><a href="/articles/detail?article_id=740" target="_blank" title="Android11正式发布，梆梆安全率先完成升级部署！">Android11正式发布，梆梆安全率先完成升级部署！</a></p>
<div class="share_all" id="share_all2">
<img class="share_btn" id="share_btn1" src="/public/images/share_btn.png"/>
<div></div><div class="weixin"></div><div class="qq_share" onclick="jump('qq',this);"></div><div class="sina_share" onclick="jump('weibosina',this);"></div>
</div>
</div>
</div>
<p class="eng_info">完美兼容，“零修改”！</p>
</div>
<div class="tarticle_div">
<div class="box-shadow-4">
<img class="share_img" onclick="window.open('/articles/detail?article_id=742')" src="/upload/16008308709695.jpg"/>
<div class="share">
<p class="p1"><a href="/articles/detail?article_id=742" target="_blank" title="5G赋能金融行业中的安全探索">5G赋能金融行业中的安全探索</a></p>
<div class="share_all" id="share_all3">
<img class="share_btn" id="share_btn1" src="/public/images/share_btn.png"/>
<div></div><div class="weixin"></div><div class="qq_share" onclick="jump('qq',this);"></div><div class="sina_share" onclick="jump('weibosina',this);"></div>
</div>
</div>
</div>
<p class="eng_info">5G金融梆梆给您解决方案</p>
</div>
</div>
</div>
<div style="position:relative;">
<div id="video1" style="height:0px;">
<video class="lg-video-object lg-html5" controls="" loop="" preload="none">
<source src="http://video.bangcle.com/a975b1f814bc46fb831cf90dd92f9bcb/103836d40cda4fb8a411bd0d4a348744-5287d2089db37e62345123a1be272f8b.mp4" type="video/mp4"> 您的浏览器不支持HTML5视频播放
      </source></video>
<a class="lg-close lg-icon" href="javascript:;"></a>
</div>
<a class="html5video" href="javascript:;" style="height: 350px;background:#000 url(/upload/file/20200330/15855363440563.png) no-repeat scroll center center ">
<div class="play_vi play_aa"></div>
</a>
</div>
<div class="product" style="min-height: 750px;">
<div class="container">
<p class="pro_title">我们的产品</p>
<p class="pro_sub_title">梆梆安全，保护智能生活！</p>
<div class="pro_img" style="position:relative;">
<ul class="pro_ul1">
<li onclick="window.location.href='/products/productindex?product_id=19'" onmouseout="hide_product(1)" onmouseover="show_product(1)">
<img class="pro1" id="product1" src="/upload/file/20160429/14619432540767.png"/>
<img class="pro1 img_h" id="product1H" src="/upload/file/20160429/14619432625989.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=3'" onmouseout="hide_product(2)" onmouseover="show_product(2)">
<img class="pro1" id="product2" src="/upload/file/20160429/14619392742002.png"/>
<img class="pro1 img_h" id="product2H" src="/upload/file/20160429/14619392827204.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=1'" onmouseout="hide_product(3)" onmouseover="show_product(3)">
<img class="pro1" id="product3" src="/upload/file/20160429/14619391007746.png"/>
<img class="pro1 img_h" id="product3H" src="/upload/file/20160429/14619391091025.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=32'" onmouseout="hide_product(4)" onmouseover="show_product(4)">
<img class="pro1" id="product4" src="/upload/file/20160429/14619433471293.png"/>
<img class="pro1 img_h" id="product4H" src="/upload/file/20160429/14619433565977.png"/>
</li>
<div style="clear:both"></div>
</ul>
<ul class="pro_ul2">
<li onclick="window.location.href='/products/productindex?product_id=5'" onmouseout="hide_product(5)" onmouseover="show_product(5)">
<img class="pro1" id="product5" src="/upload/file/20160429/14619393747127.png"/>
<img class="pro1 img_h" id="product5H" src="/upload/file/20160429/14619393821980.png"/>
</li>
<li><img class="pro_logo" src="/public/images/logo.png" style="margin: 85px auto;"/></li>
<li onclick="window.location.href='/products/productindex?product_id=26'" onmouseout="hide_product(6)" onmouseover="show_product(6)">
<img class="pro1" id="product6" src="/upload/file/20160429/14619433944896.png"/>
<img class="pro1 img_h" id="product6H" src="/upload/file/20160429/14619434023993.png"/>
</li>
<div style="clear:both"></div>
</ul>
<ul class="pro_ul3 ">
<li onclick="window.location.href='/products/productindex?product_id=8'" onmouseout="hide_product(7)" onmouseover="show_product(7)">
<img class="pro1" id="product7" src="/upload/file/20160429/14619393114467.png"/>
<img class="pro1 img_h" id="product7H" src="/upload/file/20160429/14619393207342.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=22'" onmouseout="hide_product(8)" onmouseover="show_product(8)">
<img class="pro1" id="product8" src="/upload/file/20160429/14619437217090.png"/>
<img class="pro1 img_h" id="product8H" src="/upload/file/20160429/14619437288738.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=4'" onmouseout="hide_product(9)" onmouseover="show_product(9)">
<img class="pro1" id="product9" src="/upload/file/20160429/14619441719970.png"/>
<img class="pro1 img_h" id="product9H" src="/upload/file/20160822/14718445483115.png"/>
</li>
<li onclick="window.location.href='/products/productindex?product_id=28'" onmouseout="hide_product(10)" onmouseover="show_product(10)">
<img class="pro1" id="product10" src="/upload/file/20160429/14619439014689.png"/>
<img class="pro1 img_h" id="product10H" src="/upload/file/20160429/14619439099309.png"/>
</li>
<div style="clear:both"></div>
</ul>
</div>
<div style="clear:both;height:60px;"></div>
</div>
</div>
<div class="with" style="min-height: 416px;">
<div class="container">
<p>与梆梆安全携手保护智能生活</p>
<ul>
<li><img src="/upload/file/20161027/14775643889534.png"/></li>
<li><img src="/upload/file/20190213/15500531342423.jpg"/></li>
<li><img src="/upload/file/20161027/14775639075705.png"/></li>
<li><img src="/upload/file/20161027/14775644653384.png"/></li>
<li><img src="/upload/file/20170727/15011380656514.jpg"/></li>
<li><img src="/upload/file/20161027/14775645372953.png"/></li>
<li><img src="/upload/file/20161027/14775645563841.png"/></li>
<li><img src="/upload/file/20161027/14775645710967.png"/></li>
<li><img src="/upload/file/20161027/14775645909661.png"/></li>
<li><img src="/upload/file/20161027/14775646037355.png"/></li>
</ul>
</div>
</div>
<script>
var slider = {
    //判断设备是否支持touch事件
    touch:('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
    slider:document.getElementById(Carousel_id_),

    //事件
    events:{
        index:0,     //显示元素的索引
        slider:this.slider,     //this为slider对象
        //icons:document.getElementById('icons'),
        //icon:this.icons.getElementsByTagName('span'),
        handleEvent:function(event){
            var self = this;     //this指events对象
            if(event.type == 'touchstart'){
                self.start(event);
            }else if(event.type == 'touchmove'){
                self.move(event);
            }else if(event.type == 'touchend'){
                self.end(event);
            }
        },
        //滑动开始
        start:function(event){
            var touch = event.targetTouches[0];     //touches数组对象获得屏幕上所有的touch，取第一个touch
            startPos = {x:touch.pageX,y:touch.pageY,time:+new Date};    //取第一个touch的坐标值
            isScrolling = 0;   //这个参数判断是垂直滚动还是水平滚动
            document.getElementById(Carousel_id_).addEventListener('touchmove',this,false);
            document.getElementById(Carousel_id_).addEventListener('touchend',this,false);
        },
        //移动
        move:function(event){
            //当屏幕有多个touch或者页面被缩放过，就不执行move操作
            if(event.targetTouches.length > 1 || event.scale && event.scale !== 1) return;
            var touch = event.targetTouches[0];
            endPos = {x:touch.pageX - startPos.x,y:touch.pageY - startPos.y};
            isScrolling = Math.abs(endPos.x) < Math.abs(endPos.y) ? 1:0;    //isScrolling为1时，表示纵向滑动，0为横向滑动
            if(isScrolling === 0){
                event.preventDefault();      //阻止触摸事件的默认行为，即阻止滚屏
                //this.slider.className = 'cnt';
                //document.body.style.left = -this.index*600 + endPos.x + 'px';
            }
        },
        //滑动释放
        end:function(event){
            var duration = +new Date - startPos.time;    //滑动的持续时间
            if(isScrolling === 0){    //当为水平滚动时
                //this.icon[this.index].className = '';
                if(Number(duration) > 10){     
                    //判断是左移还是右移，当偏移量大于10时执行
                    if(endPos.x > 10){
                        if(this.index !== 0) this.index -= 1;
                    }else if(endPos.x < -10){
                        //if(this.index !== this.icon.length-1) this.index += 1;
                    }
                }
                //this.icon[this.index].className = 'curr';
                //this.slider.className = 'cnt f-anim';
                //document.body.style.left = -this.index*600 + 'px';
            }
            //解绑事件
            document.getElementById(Carousel_id_).removeEventListener('touchmove',this,false);
            document.getElementById(Carousel_id_).removeEventListener('touchend',this,false);
        }
    },
    
    //初始化
    init:function(id){
        Carousel_id_=id;
        var self = this;     //this指slider对象
        if(!!self.touch) document.getElementById(Carousel_id_).addEventListener('touchstart',self.events,false);    //addEventListener第二个参数可以传一个对象，会调用该对象的handleEvent属性
    }
};
var Carousel_id_="";
if(document.getElementById('myCarousel')){
  slider.init('myCarousel');
}else if(document.getElementById('myCarousel1')){
  slider.init('myCarousel1');
}
</script>
<div class="service_info">
<div class="container ">
<div class="service_info_div">
<ul>
<li><img src="/public/images/service1.png"/></li>
<li><span class="service_title">渠道商申请</span></li>
<li><span class="service_text">
                    真诚期待各位合作伙伴加盟，同梆梆安全一起塑造辉煌</span></li>
<li><a href="/development/index.html?select=second">立即申请 &gt; &gt;</a></li>
</ul>
</div>
<div class="service_info_div">
<ul>
<li><img src="/public/images/service2.png"/></li>
<li><span class="service_title">服务热线</span></li>
<li><span class="service_text">在使用过程中遇到的任何问题，请随时联系梆梆安全客服人员，我们将第一时间为您解答</span></li>
<li><a>4008-881-881</a></li>
</ul>
</div>
<div class="service_info_div">
<ul>
<li><img src="/public/images/service3.png"/></li>
<li><span class="service_title">在线反馈</span></li>
<li><span class="service_text">用心聆听用户声音，您的建议、评价、吐槽，是我们产品前进的动力</span></li>
<li><a href="/home/feedback.html" target="blank_">我要反馈 &gt; &gt;</a></li>
</ul>
</div>
<div class="service_info_div">
<ul>
<li><img src="/public/images/service4.png"/></li>
<li><span class="service_title">帮助中心</span></li>
<li><span class="service_text">为您提供产品描述、功能介绍、使用方法、常见问题解答等内容，便于快速了解梆梆各项产品服务</span></li>
<li><a href="http://dev.bangcle.com/home/help" target="_blank">点击前往 &gt; &gt;</a></li>
</ul>
</div>
</div>
</div>
<div id="qq_service_server" style="display: none;">
<!-- 在线QQ代码 WPA Button Begin -->
<a href="http://wpa.b.qq.com/cgi/wpa.php?ln=1&amp;key=XzkzODA0MTA5N180MjU4NDJfNDAwODg4MTg4MV8yXw" target="_blank"></a>
<!-- WPA Button End -->
</div>
<div class="contact">
<a class="bar1 top_qq" href="http://a1.7x24cc.com/phone_webChat.html?accountId=N000000006149&amp;chatId=ypwy-5a241160-32c8-11e6-be0b-1d3aaaf6419d" target="_blank"></a>
<a class="bar2"></a>
<a class="bar3" id="back-to-top"></a>
<!-- <a class="bar2_h"></a> -->
</div>
<div class="footer">
<div class="container">
<div class="row row1" style="margin: 0">
<div class="footer_row1">
<div class="row" style="margin: 0">
<div class="footer_row11">
<div>
<p class="footer_title">产品与服务</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=1')">泰固</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=11')">泰聚</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=2')">泰镜</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=10')">泰知</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=3')">泰睿</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=4')">泰极</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=13')">IoT安全</p>
<p class="footer_info" onclick="window.open('/products/productindex?category_id=14')">SDK安全</p>
</div>
</div>
<div class="footer_row11">
<div>
<p class="footer_title">解决方案</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=5')">
                                    金融</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=17')">
                                    开放银行</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=6')">
                                    游戏</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=7')">
                                    政企</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=8')">
                                    运营商</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=9')">
                                    IoT</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=15')">
                                    教育</p>
<p class="footer_info" onclick="window.open('/solutions/index.html?category_id=16')">
                                    中小企业</p>
</div>
</div>
<div class="footer_row11">
<div>
<p class="footer_title">梆梆资讯</p>
<p class="footer_info" onclick="go_articleIndex(1)">梆梆观点</p>
<p class="footer_info" onclick="go_articleIndex(2)">安全资讯</p>
<p class="footer_info" onclick="go_articleIndex(3)">安全技术</p>
<p class="footer_info" onclick="go_articleIndex(5)">市场活动</p>
</div>
</div>
<div class="footer_row11">
<div>
<p class="footer_title">关于梆梆</p>
<p class="footer_info" onclick="window.open('/abouts/index')">公司介绍</p>
<p class="footer_info" onclick="window.open('/abouts/index?select=second')">荣誉资质</p>
<p class="footer_info" onclick="window.open('/abouts/index?select=fourth')">
                                    梆梆研究院</p>
<p class="footer_info" onclick="window.open('/abouts/index?select=fifth')">招贤纳士</p>
<p class="footer_info" onclick="window.open('http://passport.bangcle.com/authcenter/users/agreement')">
                                    服务协议</p>
</div>
</div>
</div>
</div>
<div class="footer_row1">
<div class="row" style="margin: 0">
<div class="footer_row12">
<div>
<p class="footer_title">联系我们</p>
<p class="footer_info"><img src="/public/images/littlephone.png"/>  电话：4008-881-881
                                </p>
<p class="footer_info top_qq"><a href="http://a1.7x24cc.com/phone_webChat.html?accountId=N000000006149&amp;chatId=ypwy-5a241160-32c8-11e6-be0b-1d3aaaf6419d" target="_blank"><img src="/public/images/littleOnline.png"/>  在线客服：联系客服</a></p>
<p class="footer_info"><a href="mailto:service@bangcle.com" style="text-decoration: none;"><img src="/public/images/littleemail.png"/>  电子邮箱：service@bangcle.com</a>
</p>
<p class="footer_info" onclick="window.open('/abouts/index?select=sixth')">
                                    更多联系方式&gt;&gt;</p>
</div>
</div>
<div class="footer_row12">
<div style="margin-left:35px;">
<p class="footer_title">关注我们</p>
<div class="attention">
<div class="div1">梆梆安全微信公众号</div>
<div class="div2" onclick="window.open('http://weibo.com/bbazfx');">梆梆安全官方微博</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row row2" style="margin: 0">
<div>
<p class="footer_title">友情链接：</p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.apicloud.com/" target="_blank">APICloud</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.dianjoy.com/" target="_blank">点乐</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://aograph.com/" target="_blank">人人云图</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="https://www.iapppay.com" target="_blank">爱贝云计费</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.yibanquan.com.cn/guide.jsp?channelMark=bangbang" target="_blank">易版权</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="https://www.shoujins.com/" target="_blank">首创金服</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.anysdk.com/" target="_blank">AnySDK</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.idreamsky.com/" target="_blank">乐逗游戏</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="https://www.youmi.net/" target="_blank">有米</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="http://www.bmob.cn/" target="_blank">Bmob</a></p>
</div>
<div class="friendLink">
<p class="footer_info"><a href="https://www.quicksdk.com/" target="_blank">QuickSDK</a></p>
</div>
</div>
<div class="row row3">
<span style="height:75px;line-height:30px;vertical-align: top;">Copyright ©2020 Bangcle. All Rights Reserved 京ICP证160618号 <a href="https://beian.miit.gov.cn" style="text-decoration:none;color:#6d6e7e;" target="_blank">京ICP备11006574号</a><br/></span>
<div><a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010802024511" style="display:inline-block;text-decoration:none;height:20px;line-height:20px;" target="_blank"><img src="/public/images/icon_beian.png" style="float:left;"/>
<p style="float:left;height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#6d6e7e;">京公网安备
                        11010802024511号</p></a></div>
<span class="Copyright" style="display: inline-block;height: 30px;line-height: 30px;vertical-align: top;"><script language="JavaScript" src="https://s21.cnzz.com/stat.php?id=4980712&amp;web_id=4980712&amp;show=pic1"></script>
<!--
                    <script type="text/javascript">
                        var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " https://");
                        document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F2def6e2d54d459aa4774d8fec3ef3990' type='text/javascript'%3E%3C/script%3E").replace("https://bs.baidu.com/hmt/icon/31.swf","/public/images/31.swf"));
                    </script>
                    -->
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" height="18" id="HolmesIcon1470890600" style="position: relative;top: 6px;" width="67"><param name="movie" value="/public/images/31.swf"/><param name="flashvars" value="s=http://tongji.baidu.com/hm-web/welcome/ico?s=2def6e2d54d459aa4774d8fec3ef3990"/><param name="allowscriptaccess" value="always"/>
<embed allowscriptaccess="always" flashvars="s=http://tongji.baidu.com/hm-web/welcome/ico?s=2def6e2d54d459aa4774d8fec3ef3990" height="18" name="HolmesIcon1470890600" src="/public/images/31.swf" type="application/x-shockwave-flash" width="67">
</embed></object>
</span>
</div>
<div style="height: 30px;"></div>
</div>
</div>
</div>
</body>
</html>
