<!DOCTYPE html>
<html>
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<link href="//www.fedex.com/images/c/s1/fx-favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<title>FedEx Page Not Found</title>
<meta content="FedEx Page Not Found" name="title"/>
<meta content="FedEx Page Not Found" name="keywords"/>
<meta content="FedEx Page Not Found" name="description"/>
<meta content="pagenotfound" name="wsstitle"/>
<meta content="us/en/fedex" name="wssmlc"/>
<link href="//www.fedex.com/css/t2/global-wrapper-min.css" rel="stylesheet" type="text/css"/>
<link href="//www.fedex.com/css/t2/pagenotfound-min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	var pnfRedirect=window.setTimeout(function(){
		window.location.href = (window.fdx_cPathCode) ? "/" + window.fdx_cPathCode + "/" : "/us/";
	},30000);
</script>
<link href="//images.fedex.com/css/t2/global-wrapper-min.css" rel="stylesheet" type="text/css"/>
<link href="//images.fedex.com/css/common/1.0/app-css/common-min.css" rel="stylesheet" type="text/css"/>
<link href="//images.fedex.com/images/c/s1/fx-favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style id="antiClickjack">body{display:none !important;}</style>
<script type="text/javascript">
            if (self === top) {
                var antiClickjack = document.getElementById("antiClickjack");
                antiClickjack.parentNode.removeChild(antiClickjack);
            } else {
                top.location = self.location;
            }
        </script>
</head>
<!--[if IE 8 ]>    <body id="fx-respond" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body id="fx-respond" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body id="fx-respond"> <!--<![endif]-->
<script>var fx_appshell = true;var fx_responsive = "true";</script>
<div id="container">
<script>var vh=0;
var fxg_header=true;
</script>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>

 ï»¿<link href="//www.fedex.com/css/legacy/main-min.css" rel="stylesheet" type="text/css"/>
<!-- HEAD SCRIPTS -->
<script>
vh=1;
var fxg_header=true;


/* @param [string]  [styleName] [filename with suffix e.g. "style.css"]
 * @param [boolean] [disabled]  [true disables style]
 */

        


var fx_device_type = "desktop";
var fxg_header = true;


var disableStyle = function(styleName, disabled) {
    var styles = document.styleSheets;
    var href = "";
try {
    for (var i = 0; i < styles.length; i++) {
    	  var re = new RegExp(".*?//.*?/", "");
  	    var cssStr =styles[i].href.replace(re,"/");
    	  var r = /[^\/]*$/;
    	  //alert('cssStr   ' + cssStr);
				var dirPath = cssStr.replace(r, '');
				//alert ('dirPath   ' + dirPath);
        href = styles[i].href.split("/");
        href = href[href.length - 1];
 
        if (href == styleName) {
            styles[i].disabled = disabled;
            var cssPath = dirPath + styleName;            
            loadcssfile(cssPath);
            break;
        }
    }
  }
  catch(err){}
};
function loadcssfile(styleName){
				var fileName = styleName.replace(".css","-legacy.css");
				//alert('fileName1   ' + fileName);
				//fileName = window.location.protocol + window.location.host + fileName;
				fileName = window.location.protocol + "//www.fedex.com" + fileName;
				//alert('fileName2   ' + fileName);
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", fileName)
        if (typeof fileref!="undefined"){document.getElementsByTagName("head")[0].appendChild(fileref)}
}
disableStyle('master-responsive-min.css', true);
disableStyle('global-wrapper-min.css', true);
disableStyle('common-min.css', true);
disableStyle('master-min.css', true);
disableStyle('ie6-min.css', true)
disableStyle('ie-min.css', true);</script>
<!-- HEAD STYLES -->
<!-- HEADER START -->
<div class="header" id="fxg-header-container">
<header class="fxg-header fxg-header--sticky" id="fxg-header--sticky">
<div>
<!-- GLOBAL NAV START -->
<div class="global_nav_reference">
<div class="global_navigation">
<div class="fxg-nav">
<div class="fxg-wrapper">
<a href="https://www.fedex.com/en-us/home.html" target="_self"><img alt="FedEx Logo" class="fxg-header__logo" src="//www.fedex.com/images/legacy/logo.png"/></a>
<!-- DROPDOWNS START -->
<ul class="fxg-dropdown fxg-global-nav">
<!-- SHIPPING DROPDOWN START -->
<div class="dropdown">
<li class="fxg-dropdown__item">
<a class="fxg-link fxg-dropdown-js" href="#">Shipping</a>
<div class="fxg-dropdown__sub-menu">
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/cgi-bin/ship_it/interNetShip?origincountry=us&amp;locallang=us&amp;urlparams=us" id="ShipExp" target="_self" title="Create a Shipment">Create a Shipment</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/apps/shipping-rates/" target="_self" title="Shipping Rates &amp; Delivery Times">Shipping Rates &amp; Delivery Times</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/shipping/schedule-manage-pickups.html" target="_self" title="Schedule &amp; Manage Pickups">Schedule &amp; Manage Pickups</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/shipping/packing-supplies.html" target="_self" title="Packing &amp; Shipping Supplies">Packing &amp; Shipping Supplies</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/shipping/international-shipping-guide.html" target="_self" title="International Shipping Guide">International Shipping Guide</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/shipping/store/shipping-services.html" target="_self" title="In-Store Shipping Services">In-Store Shipping Services</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--blue" href="https://www.fedex.com/en-us/shipping.html" target="_self" title="All Shipping Services">ALL SHIPPING SERVICES</a>
</div>
</div>
</li>
</div>
<!-- SHIPPING DROPDOWN END -->
<!-- TRACKING DROPDOWN START -->
<div class="dropdown">
<li class="fxg-dropdown__item">
<a class="fxg-link fxg-dropdown-js" href="#">Tracking</a>
<div class="fxg-dropdown__sub-menu">
<!-- TRACKING MODULE -->
<div class="tracking_module">
<div class="fxg-tracking-module">
<form action="#" id="HeaderTrackingModule" method="POST">
<div class="fxg-field">
<input aria-required="true" class="fxg-field__input-text fxg-field__input--required" data-errmsg="Please enter at least one tracking number." id="trackingnumberinput" name="trackingNumber" title="Tracking ID" type="text"/>
<span class="fxg-field__placeholder fxg-field__floating-placeholder">TRACKING ID</span>
</div>
<button class="fxg-button fxg-button--orange" type="submit">TRACK</button>
</form>
</div>
</div>
<!-- END TRACKING MODULE -->
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://www.fedex.com/us/fedextracking/" target="_self" title="Advanced Shipment Tracking">Advanced Shipment Tracking</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/apps/fdmenrollment/" target="_self" title="Manage Your Delivery">Manage Your Delivery</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--blue" href="https://www.fedex.com/en-us/tracking.html" target="_self" title="All Tracking Services">ALL TRACKING SERVICES</a>
</div>
</div>
</li>
</div>
<!-- TRACKING DROPDOWN END -->
<!-- PRINTING DROPDOWN START -->
<div class="dropdown">
<li class="fxg-dropdown__item">
<a class="fxg-link fxg-dropdown-js" href="#">Printing Services</a>
<div class="fxg-dropdown__sub-menu">
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/printing/online-printing.html" target="_self" title="Start Online Printing Order">Start Online Printing Order</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/printing/poster-sign-banner-printing.html" target="_self" title="Posters, Signs &amp; Banners">Posters, Signs &amp; Banners</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/printing/presentations-and-manuals.html" target="_self" title="Presentations &amp; Manuals">Presentations &amp; Manuals</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/printing/marketing-materials.html" target="_self" title="Marketing Materials">Marketing Materials</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/printing/custom-solutions.html" target="_self" title="Ideas &amp; Custom Solutions">Ideas &amp; Custom Solutions</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/coupon-offers.html" target="_self" title="Get Coupons &amp; Deals">Get Coupons &amp; Deals</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--blue" href="https://www.fedex.com/en-us/printing.html" target="_self" title="All Printing Services">ALL PRINTING SERVICES</a>
</div>
</div>
</li>
</div>
<!-- PRINTING DROPDOWN END -->
<!-- LOCATIONS DROPDOWN START -->
<div class="dropdown">
<li class="fxg-dropdown__item">
<a class="fxg-link fxg-dropdown-js " href="#">Locations</a>
<div class="fxg-dropdown__sub-menu">
<div class="link section"><a class="fxg-link default " href="https://www.fedex.com/en-us/shipping/store/all-location-types.html" target="_self" title="All Location Types">All Location Types</a></div>
<div class="link"><a class="fxg-link fxg-link--blue " href="http://www.fedex.com/locate/index.html?locale=en_US#" target="_self" title="Find a Location">FIND A LOCATION</a></div>
</div>
</li>
</div>
<!-- LOCATIONS DROPDOWN END -->
<!-- SUPPORT DROPDOWN START -->
<div class="dropdown">
<li class="fxg-dropdown__item">
<a class="fxg-link fxg-dropdown-js" href="#">Support</a>
<div class="fxg-dropdown__sub-menu">
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/get-started.html/" target="_self" title="New Customer Center">New Customer Center</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://smallbusiness.fedex.com/home.html" target="_self" title="Small Business Center">Small Business Center</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/service-guide.html" target="_self" title="FedEx Service Guide">FedEx Service Guide</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/create-account/account-management.html" target="_self" title="Account Management Tools">Account Management Tools</a>
</div>
<div class="link">
<a alt="File a Claim" class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/customer-support/claims.html" target="_self">File a Claim</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://www.fedex.com/en-us/billing-online.html" target="_self" title="View &amp; Pay Bill">View &amp; Pay Bill</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--blue" href="https://www.fedex.com/en-us/customer-support.html" target="_self" title="Customer Support">CUSTOMER SUPPORT</a>
</div>
</div>
</li>
</div>
<!-- SUPPORT DROPDOWN END -->
</ul>
<!-- UTILITY NAV START -->
<!-- UTILITY NAV START -->
<div class="fxg-user-options" id="global-login-wrapper">
<div class="fxg-user-options__option fxg-user-options__sign-in " id="fxg-global-header">
<a class="fxg-link fxg-dropdown-js fx-global-prelog-link" href="#" id="global-login-link"><span class="prelog-text" id="global-login-link-span">Sign In </span><img class="fxg-user-options__icon" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="fxg-user-options__option fxg-user-options__search-btn">
<img alt="Search" class="fxg-user-options__icon" id="SrchBtn" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/>
</div>
<button class="hamburger hamburger--slider fxg-user-options__menu-btn" id="fxg-mobile-menu-btn" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
<div class="fxg-search ">
<div class="fxg-user-options__option fxg-user-options__search">
<form action="#" class="fxg-form" id="fxg-search-header" method="POST">
<img alt="Search" class="fxg-user-options__icon fxg-search-btn" src="https://www.fedex.com/content/dam/fedex-com/common/sprite-placeholder.png"/>
<input class="fxg-user-options__search-field fxg-search-js search_closeMark" id="fxg-search-text" placeholder="Search or Tracking Numbers" title="Search or Tracking Numbers" type="text"/>
<a aria-label="Exit Search " class="fxg-search-close-btn" href="#"><img alt="Exit Search " class="fxg-user-options__icon fxg-close-btn" src="https://www.fedex.com/content/dam/fedex-com/common/sprite-placeholder.png"/></a>
</form>
</div>
<div class="fxg-user-options__search-results fxg-search-results">
<div class="fxg-search-results__row fxg-search-results__title"><span>Top Searched </span></div>
<a class="fxg-search-results__row " data-analytics="TopSearched|Door Tag" href="https://www.fedex.com/en-us/search.html?q=door%20tag&amp;sp_cs=UTF-8&amp;cc=en_us" target="_self">Door Tag</a>
<a class="fxg-search-results__row " data-analytics="TopSearched|Supplies" href="https://www.fedex.com/en-us/search.html?q=supplies&amp;sp_cs=UTF-8&amp;cc=en_us" target="_self">Supplies</a>
<a class="fxg-search-results__row " data-analytics="TopSearched|Billing" href="https://www.fedex.com/en-us/search.html?q=billing&amp;sp_cs=UTF-8&amp;cc=en_us&amp;sp_staged=1" target="_self">Billing</a>
<a class="fxg-search-results__row " data-analytics="TopSearched|Delivery" href="https://www.fedex.com/en-us/search.html?q=delivery&amp;sp_cs=UTF-8&amp;cc=en_us" target="_self">Delivery</a>
<a class="fxg-search-results__row " data-analytics="TopSearched|Schedule Pickup" href="https://www.fedex.com/en-us/search.html?q=schedule%20pickup&amp;sp_cs=UTF-8&amp;cc=en_us" target="_self">Schedule Pickup</a>
</div>
</div>
<!-- UTILITY NAV END -->
</div>
</div>
</div>
</div>
<!-- GLOBAL NAV END -->
</div>
</header>
</div>
<!-- HEADER END -->
<div id="auxhead"></div>
<script>


function loadDoc1(file) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("auxhead").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", file, true);
  xhttp.send();
}
	
if (vh == 0) {
  if ( /MSIE 7/i.test(navigator.userAgent) || /MSIE 8/i.test(navigator.userAgent) ) {
    loadDoc1("/templates/components/headers/us/gl_old.html");
  } else {
    loadDoc1("/templates/components/headers/us/gl_new.html");
  }
}
</script>
<div id="placement_101"></div>
<div id="content">
<div style="height: 100px; position: relative;">
<div class="fx-loading-overlay">
<span class="fx-loading-icon" style="margin:0px;"></span>
</div>
</div>
</div>
<div id="contentMktpage" style="background:white;"></div>
<div id="placement_102"></div>
<script>var vf=0;</script>
<!-- NEW FOOTER START -->
<script>vf=1;</script>
<div class="global_footer_reference" id="global_footer_reference">
<div class="cq-dd-paragraph">
<div class="footer">
<footer class="fxg-footer">
<div class="fxg-footer__primary">
<div class="fxg-wrapper container">
<!-- COLUMN CONTROL START -->
<div class="column_control section">
<div class="row">
<!-- 4 Columns -->
<div class="fxg-col fxg-col--fxg-col--mt20 fxg-col-- col-sm-4">
<div class="title section">
<div class="fxg-title fxg-title--default " style="text-align: left;" tabindex="0">Our Company</div>
</div>
<div class="column_control section">
<!-- COLUMN CONTROL START -->
<div class="row">
<!-- 2 Columns -->
<div class="fxg-col fxg-col-- fxg-col-- col-sm-6">
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://about.fedex.com/" target="_self" title="About FedEx">About
														FedEx</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://about.van.fedex.com/our-story/company-structure/" target="_self" title="Our Portfolio">Our Portfolio</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://www.fedex.com/us/investorrelations/" style="white-space: nowrap;" target="_self" title="Investor Relations">Investor Relations</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="https://careers.fedex.com/fedex/" target="_self" title="FedEx Careers">Careers</a>
</div>
</div>
<div class="link fxg-col2-nt">
<a class="fxg-link fxg-link--gray" href="http://about.van.fedex.com/blog/" target="_self" title="FedEx Blog">FedEx Blog</a>
</div>
<div class="link fxg-col2-nt">
<a class="fxg-link fxg-link--gray" href="http://about.van.fedex.com/social-responsibility/csr-policy-statements/" style="white-space: nowrap;" target="_self" title="Corporate Responsibility">Corporate Responsibility</a>
</div>
<div class="link fxg-col2-nt">
<a class="fxg-link fxg-link--gray" href="http://about.van.fedex.com/newsroom/global-english/" target="_self" title="News Room">Newsroom</a>
</div>
<div class="link fxg-col2-nt">
<a class="fxg-link fxg-link--gray" href="http://www.fedex.com/us/customersupport/email/index.html" target="_self" title="Contact Us">Contact Us</a>
</div>
</div>
</div>
</div>
<div class="fxg-col fxg-col--fxg-col--mt20 fxg-col-- col-sm-3">
<div class="title section">
<div class="fxg-title fxg-title--default " style="text-align: left;" tabindex="0">More From FedEx</div>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://www.fedex.com/us/compatible/" target="_self" title="FedEx Compatible">FedEx Compatible</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://www.fedex.com/us/developer" target="_self" title="Developer Resource Center">Developer Resource Center</a>
</div>
<div class="link">
<a class="fxg-link fxg-link--gray" href="http://crossborder.fedex.com/us" target="_self" title="FedEx Cross Border">FedEx Cross Border</a>
</div>
</div>
<div class="fxg-col fxg-col--fxg-col--mt20 fxg-col-- col-sm-2">
</div>
<div class="fxg-col fxg-col--fxg-col--mt20 fxg-col-- col-sm-3">
<div class="title">
<div class="fxg-title fxg-title--default " style="text-align: left;">Language</div>
</div>
<!-- LANGUAGE DROPDOWN START -->
<div class="language_dropdown">
<div class="icon_link">
<a class="fxg-link" href="http://www.fedex.com/?location=home" target="_self"><img alt="Change Country" class="fxg-icon fxg-icon--flag-default" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/>Change Country</a>
</div>
<!--Bootstrap dropdown for uniformity across all OS's-->
<div class="dropdown fxg-bootstrap-dropdown" style="float: none;">
<button aria-expanded="true" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu" type="button">
												English <span class="fxg-icon"></span>
</button>
<ul aria-labelledby="dropdownMenu" class="dropdown-menu">
<li><a href="https://www.fedex.com/es-us/home.html" target="_self">EspaÃ±ol</a></li>
</ul>
</div>
</div>
<!-- LANGUAGE DROPDOWN END -->
</div>
</div>
</div>
<!-- COLUMN CONTROL END -->
<div class="hr">
<hr class="fxg-mobile--hide"/>
</div>
</div>
</div>
<!-- BEGIN SOCIAL BAR -->
<div class="fxg-footer__social">
<div class="fxg-wrapper container">
<!-- COLUMN CONTROL START -->
<div class="column_control">
<div class="row">
<!-- 2 Columns -->
<div class="fxg-col fxg-col--fxg-col--mt20 fxg-col-- col-sm-8">
<div class="title">
<div class="fxg-title fxg-title--default" style="text-align: left;">Follow FedEx</div>
</div>
<div class="icon_link">
<a class="fxg-link" href="http://www.fedex.com/us/email" target="_self" title="Newsletter Signup"><img alt="Newsletter Signup" class="fxg-icon fxg-icon--email" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div> <div class="icon_link">
<a class="fxg-link" href="https://www.facebook.com/fedex" target="_self"><img alt="Facebook" class="fxg-icon fxg-icon--facebook" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="icon_link">
<a class="fxg-link" href="https://twitter.com/fedex" target="_self"><img alt="Twitter" class="fxg-icon fxg-icon--twitter" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="icon_link">
<a class="fxg-link" href="http://www.instagram.com/fedex" target="_self"><img alt="Instagram" class="fxg-icon fxg-icon--instagram " src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="icon_link">
<a class="fxg-link" href="http://www.linkedin.com/company/fedex" target="_self"><img alt="LinkedIn" class="fxg-icon fxg-icon--linkedin " src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="icon_link">
<a class="fxg-link" href="http://www.youtube.com/fedex" target="_self"><img alt="YouTube" class="fxg-icon fxg-icon--youtube " src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<!--<div class="icon_link">
                                            <a href="#" target="_self" class="fxg-link"><img src="//www.fedex.com/images/legacy/sprite-placeholder.png" alt="Tumblr" class="fxg-icon fxg-icon--tumblr   "/></a>
                                        </div>-->
<div class="icon_link">
<a class="fxg-link" href="https://www.pinterest.com/fedex" target="_self"><img alt="Pinterest" class="fxg-icon fxg-icon--pinterest" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
<div class="icon_link">
<a class="fxg-link" href="https://www.fedex.com/en-us/home.html" target="_self"><img alt="Google Plus" class="fxg-icon fxg-icon--googleplus" src="//www.fedex.com/images/legacy/sprite-placeholder.png"/></a>
</div>
</div>
</div>
</div>
<!-- COLUMN CONTROL END -->
</div>
</div>
<!-- END SOCIAL BAR -->
<!-- BEGIN COPYRIGHT BAR -->
<div class="fxg-copyright">
<div class="fxg-wrapper container">
<div class="column_control">
<div class="row">
<!-- 2 Columns -->
<div class="fxg-col fxg-col-- fxg-col-- col-sm-4">
<div class="title">
<div class="fxg-title fxg-title--default" style="text-align: left;">© FedEx 1995-2018
											</div>
</div>
</div>
<div class="row">
<!-- 2 Columns -->
<div class="fxg-col fxg-col-- fxg-col-- ">
<div class="link section">
<a alt="Feedback" class="fxg-link default fxg-copyright-feedback-link " href="http://www.fedex.com/us/customersupport/email/index.html" target="_self">Feedback</a>
<span class="fxg-link__separator "> | </span>
</div>
</div>
<div class="fxg-col fxg-col-- fxg-col-- ">
<div class="link section">
<a alt="Site Map" class="fxg-link default " href="http://www.fedex.com/us/sitemap.html" target="_self">Site Map</a>
<span class="fxg-link__separator "> | </span>
</div>
<div class="link section">
<a alt="Terms of Use" class="fxg-link default " href="http://www.fedex.com/us/legal/" target="_self">Terms of Use</a>
<span class="fxg-link__separator "> | </span>
</div>
<div class="link section">
<a alt="Security &amp; Privacy" class="fxg-link default " href="http://www.fedex.com/us/security/index.html" target="_self">Security &amp; Privacy</a>
</div>
</div>
</div>
</div>
</div>
<!-- COLUMN CONTROL END -->
</div>
</div>
</footer>
</div>
</div>
</div>
<!-- END FOOTER -->
<!-- </noindex> -->
</div>
<script type="text/javascript">	var reg_account = "fedexus"; </script>
<script src="//www.fedex.com/templates/components/apps/contentim/contentim_controller-min.js" type="text/javascript"></script>
<script src="//www.fedex.com/templates/components/javascript/v2/app-min.js" type="text/javascript"></script>
<script src="//www.fedex.com/templates/components/javascript/legacy/main-min.js" type="text/javascript"></script>
<div id="aux"></div>
<script>


function loadDoc(file) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("aux").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", file, true);
  xhttp.send();
}
	
if (vf == 0) {

   if ( /MSIE 7/i.test(navigator.userAgent) || /MSIE 8/i.test(navigator.userAgent) || /\/labelportal\//.test(location.href) ) {
    loadDoc("/templates/components/footers/us/gl_old.html");
  } else {
    loadDoc("/templates/components/footers/us/gl_new.html");
  }
}

</script>
</body>
</html>
