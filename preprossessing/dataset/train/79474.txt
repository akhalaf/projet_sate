<!DOCTYPE html>
<html lang="en">
<head>
<link href="/images/favicon.ico" rel="icon" type="image/ico"/>
<base href="https://www.adventuregamestudio.co.uk"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Adventure Game Studio</title> <link href="/includes/index.css" rel="stylesheet"/>
<link href="/includes/trumbowyg/ui/trumbowyg.css" rel="stylesheet"/>
<!-- Hotjar Tracking Code for adventuregamestudio.co.uk -->
<script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1153912,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>')</script>
<script src="/includes/trumbowyg/trumbowyg.js"></script>
<script type="text/javascript">
	    var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-3634090-7']);
_gaq.push(['_setDomainName', 'adventuregamestudio.co.uk']);
_gaq.push(['_setAllowLinker', true]);
_gaq.push(['_trackPageview']);

(function () {
        var ga = document.createElement("script");
        ga.type = "text/javascript";
        ga.async = true;
        ga.src = ("https:" === document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(ga, s);
    }
)();

function toggleDivVisible(div_to_toggle_id) {
    var div_to_toggle = document.getElementById(div_to_toggle_id);
    var div_expander_id = div_to_toggle_id.concat("Expander");
    var div_expander = document.getElementById(div_expander_id);
    var original_string = div_expander.innerHTML;
    var new_string;
    if (div_to_toggle.style.display === "block") {
        new_string = original_string.replace("&lt;&lt;", "&gt;&gt;");
        div_to_toggle.style.display = "none";
    } else {
        new_string = original_string.replace("&gt;&gt;", "&lt;&lt;");
        div_to_toggle.style.display = "block";
    }
    div_expander.innerHTML = new_string;
}

function expandComment(comment_number) {
    var div_to_expand_id = "userCommentRest".concat(comment_number);
    var div_to_expand = document.getElementById(div_to_expand_id);
    var div_expander = document.getElementById(div_to_expand_id.concat("Expander"));
    var original_string = div_expander.innerHTML;
    div_expander.innerHTML = original_string.replace("&gt;&gt;", "");
    div_to_expand.style.display = "block";
}

function toggleCommentDivVisible(comment_number) {
    var user_comment_rest_id = "userCommentRest".concat(comment_number);
    var user_comment_first = document.getElementById("userCommentFirst".concat(comment_number));
    var user_comment_rest = document.getElementById(user_comment_rest_id);
    var div_expander = document.getElementById(user_comment_rest_id.concat("Expander"));
    var original_string = div_expander.innerHTML;
    var new_string;
    if (user_comment_rest.style.display === "none") {
        new_string = original_string.replace("&gt;&gt;", "&lt;&lt;");
        user_comment_first.style.display = "block";
        user_comment_rest.style.display = "none";
    } else {
        new_string = original_string.replace("&lt;&lt;", "&gt;&gt;");
        user_comment_first.style.display = "none";
        user_comment_rest.style.display = "block";
    }
    div_expander.innerHTML = new_string;
}

function toggleCupRatingPopup() {
    $('#cupRatingPopup').toggle();
}

function toggleDescPopup() {
    $('#descPopup').toggle();
}

function validateNominationsForm(character_award_id) {
    for (i = 1; i <= 10; i++) {
        var this_select = document.getElementsByName(character_award_id + "#" + i)[0];
        var this_text = document.getElementsByName(character_award_id + "#" + i + "!extra_data")[0];
        if (((this_select.value !== 0) && (this_text.value === "")) || ((this_select.value === 0) && (this_text.value !== ""))) {
            alert("You must select a game AND enter a character name if you nominate a game for Best Character.");
            return false;
        }
    }
    var counter = 0;
    var all_elements = document.getElementById("nominations").elements;
    for (var i = 0; i < all_elements.length; i++) {
        this_element = all_elements[i];
        if ((this_element.tagName === "SELECT") && (this_element.value !== 0)) counter++;
    }
    if (counter === 0) {
        alert("You must nominate at least one game for at least one category!");
        return false;
    }
    return true;
}

function showZoomPopup() {
    if (typeof window.current_image === 'undefined') window.current_image = document.getElementsByName('unZoomedMainScreenshot').item(0).getAttribute('src');
    $('#zoomPopup').hide();
    if ((document.getElementsByName('smallScreenshot2').length === 1) || (document.getElementsByName('smallScreenshot3').length === 1)) {
        $('#imagesLeft').hide();
        $('#imagesRight').hide();
    }
    var current_div = document.getElementsByName('zoomPopup').item(0);
    var current_image = document.getElementsByName('zoomedMainScreenshot').item(0);
    var original_width = document.getElementsByName('unZoomedMainScreenshot').item(0).naturalWidth;
    var original_height = document.getElementsByName('unZoomedMainScreenshot').item(0).naturalHeight;
    var new_width_image = (original_width * window.zoom_level);
    var new_width = (new_width_image + 500);
    var new_left = (new_width / 2);
    var new_left_css = '-' + new_left.toString() + 'px';
    var new_width_css = new_width.toString() + 'px';
    var new_height_image = (original_height * window.zoom_level);
    var new_height = (new_height_image + 100);
    var new_top = (new_height / 2);
    var new_top_css = '-' + new_top.toString() + 'px';
    var new_height_css = new_height.toString() + 'px';
    current_div.style.width = new_width_css;
    current_div.style.height = new_height_css;
    current_div.style.marginLeft = new_left_css;
    current_div.style.marginTop = new_top_css;
    current_image.width = new_width_image;
    current_image.height = new_height_image;
    $('#zoomPopup').show();
}

function hideZoomPopup() {
    $('#zoomPopup').hide();
}

function parseZoom(direction) {
    if ((direction === 'plus') && (window.zoom_level <= 2.0)) window.zoom_level += 0.5;
    else if ((direction === 'minus') && (window.zoom_level >= 1.0)) window.zoom_level -= 0.5;
    showZoomPopup();
}

function catchTheKeys(event) {
    if (document.getElementsByName('zoomPopup').item(0) != null) {
        if (document.getElementsByName('zoomPopup').item(0).style.display === "block") {
            var key_pressed = event.key || event.keyCode;
            if ((key_pressed === 27) || (key_pressed === "Escape")) { // escape key
                event.preventDefault();
                hideZoomPopup();
            } else if ((key_pressed === 37) || (key_pressed === "ArrowLeft")) { // left key
                event.preventDefault();
                swapZoomImage('left');
            } else if ((key_pressed === 39) || (key_pressed === "ArrowRight")) { // right key
                event.preventDefault();
                swapZoomImage('right');
            } else if ((key_pressed === 38) || (key_pressed === "ArrowUp")) { // up key
                event.preventDefault();
                parseZoom('plus');
            } else if ((key_pressed === 40) || (key_pressed === "ArrowDown")) { // down key
                event.preventDefault();
                parseZoom('minus');
            }
        }
    }
}

function showOneCupPopup() {
    $('#cupRatingPopup').hide();
    $('#oneCupPopup').show();
}

function hideOneCupPopup() {
    $('#oneCupPopup').hide();
}

function showOldComments() {
    $('#oldUserComments').show();
    $('#oldUserCommentsLink').hide();
}

function swapZoomImage(swap_direction) {
    var current_image = document.getElementsByName('zoomedMainScreenshot').item(0).getAttribute('src');
    if (swap_direction === 'right') {
        if (current_image === document.getElementsByName('smallScreenshot1').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot2').length !== 0) new_image = document.getElementsByName('smallScreenshot2').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot3').length !== 0) new_image = document.getElementsByName('smallScreenshot3').item(0).getAttribute('src');
            }
        } else if (current_image === document.getElementsByName('smallScreenshot2').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot3').length !== 0) new_image = document.getElementsByName('smallScreenshot3').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot1').length !== 0) new_image = document.getElementsByName('smallScreenshot1').item(0).getAttribute('src');
            }
        } else if (current_image === document.getElementsByName('smallScreenshot3').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot1').length !== 0) new_image = document.getElementsByName('smallScreenshot1').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot2').length !== 0) new_image = document.getElementsByName('smallScreenshot2').item(0).getAttribute('src');
            }
        }
    } else if (swap_direction === 'left') {
        if (current_image === document.getElementsByName('smallScreenshot1').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot3').length !== 0) new_image = document.getElementsByName('smallScreenshot3').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot2').length !== 0) new_image = document.getElementsByName('smallScreenshot2').item(0).getAttribute('src');
            }
        } else if (current_image === document.getElementsByName('smallScreenshot2').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot1').length !== 0) new_image = document.getElementsByName('smallScreenshot1').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot3').length !== 0) new_image = document.getElementsByName('smallScreenshot3').item(0).getAttribute('src');
            }
        } else if (current_image === document.getElementsByName('smallScreenshot3').item(0).getAttribute('src')) {
            if (document.getElementsByName('smallScreenshot2').length !== 0) new_image = document.getElementsByName('smallScreenshot2').item(0).getAttribute('src');
            else {
                if (document.getElementsByName('smallScreenshot1').length !== 0) new_image = document.getElementsByName('smallScreenshot1').item(0).getAttribute('src');
            }
        }
    }
    swapMainImages(new_image);
    showZoomPopup();
}

function swapMainImages(url) {
    window.current_image = url;
    document.getElementsByName('unZoomedMainScreenshot').item(0).setAttribute('src', url);
    document.getElementsByName('zoomedMainScreenshot').item(0).setAttribute('src', url);
    if (document.getElementsByName('zoomPopup').item(0).style.display === "block") showZoomPopup();
}

function ajaxSearcher(search_element_value, element_to_update_id_1, element_to_update_id_2, search_type) {
    var element_to_update_1 = document.getElementById(element_to_update_id_1); // uses element ID to select the div we want to write to
    if (element_to_update_id_2 !== "") var element_to_update_2 = document.getElementById(element_to_update_id_2); // uses element ID to select the div we want to write to
    if ((search_element_value === 0) || (search_element_value === '') || (search_element_value === null)) // checks if search term is zero, empty or null; if so, empties the div
    {
        element_to_update_1.innerHTML = "";
        if (element_to_update_id_2 !== "") element_to_update_2.innerHTML = "";
    } else // else if search term has contents
    {
        if (search_type === "author_search") search_element_value = search_element_value + "£$%€" + element_to_update_id_1;

        if (window.XMLHttpRequest) // creates HTTP request object; code for all browsers except those listed below
        {
            xmlhttp = new XMLHttpRequest();
        } else // creates HTTP request object; code for IE5 & IE6
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () // triggered when request object's state changes (request is below)
        {
            if (this.readyState === 4 && this.status === 200) // if readyState is 4 (done) and HTTP status code returned is 200 (success)
            {
                element_to_update_1.style.display = "none"; // hide the div to write to
                element_to_update_1.innerHTML = ""; // empty the div
                element_to_update_1.innerHTML = this.responseText; // write to div
                element_to_update_1.style.display = "block"; // show the div
                if (element_to_update_2) element_to_update_2.style.display = "block"; // show the div
            }
        };
        xmlhttp.open("GET", "includes/ajax_searcher.php?search_type=" + search_type + "&search_term=" + search_element_value, true); // opens file ajax_searcher.php, sending the search_type and search_term
        xmlhttp.send(); // sends HTTP request to the file we opened
    }
}

function processAuthor(member_id, real_name, count) {
    real_name = decodeURIComponent(real_name).replace(/\+/g, " ");
    document.getElementById("author_" + count + "_name_display").value = real_name;
    document.getElementById("author_" + count + "_name_display").disabled = true;
    document.getElementById("author_" + count + "_name").value = real_name;
    document.getElementById("author_" + count + "_memberid").value = member_id;
    document.getElementById("author_" + count + "_search_div").style.display = "none";
    document.getElementById("author_" + count + "_padding_div").style.display = "none";
    document.getElementById("author_" + count + "_edit_link").style.display = "block";
}

function edit_author_row(count) {
    document.getElementById("author_" + count + "_name_display").value = "";
    document.getElementById("author_" + count + "_name_display").disabled = false;
    document.getElementById("author_" + count + "_name").value = "";
    document.getElementById("author_" + count + "_memberid").value = 0;
    document.getElementById("author_" + count + "_edit_link").style.display = "none";
}

function add_author_row(count) {
    var next_count = (Date.now() + (Math.floor((Math.random() * 100) + 1)));
    new_html =
        "<tr id=\"tr_1_" + next_count + "\" height=\"25px\">" +
        "<td width=\"300px\">" +
        "<input type=\"text\" id=\"author_" + next_count + "_name_display\" name=\"author_" + next_count + "_name_display\" value=\"\" size=44 onkeyup=\"ajaxSearcher(this.value, 'author_" + next_count + "_search_div', 'author_" + next_count + "_padding_div', 'author_search');\"/>" +
        "<input type=\"hidden\" id=\"author_" + next_count + "_name\" name=\"author_" + next_count + "_name\" value=\"0\"/>" +
        "<input type=\"hidden\" id=\"author_" + next_count + "_memberid\" name=\"author_" + next_count + "_memberid\" value=\"0\"/>" +
        "</td>" +
        "<td width=\"13px\">" +
        "<a id=\"author_" + next_count + "_edit_link\" onclick=\"edit_author_row('" + next_count + "');\" style=\"cursor: pointer; text-decoration: none; display: none;\">&#9998;</a>" +
        "</td>" +
        "<td width=\"300px\">" +
        "<input type=\"text\" id=\"author_" + next_count + "_position\" name=\"author_" + next_count + "_position\" value=\"\" size=44/>" +
        "</td>" +
        "<td width=\"13px\">" +
        "<a id=\"author_" + next_count + "_add_link\" onclick=\"add_author_row('" + next_count + "');\" style=\"cursor: pointer; text-decoration: none;\">&#10133;</a>" +
        "</td>" +
        "<td width=\"13px\">" +
        "<a id=\"author_" + next_count + "_remove_link\" onclick=\"remove_author_row('" + next_count + "');\" style=\"cursor: pointer; text-decoration: none;\">&#10134;</a>" +
        "</td>" +
        "</tr>" +
        "<tr id=\"tr_2_" + next_count + "\">" +
        "<td id=\"author_" + next_count + "_search_div\" style=\"display: none;\">&nbsp;</td><td id=\"author_" + next_count + "_padding_div\" style=\"display: none;\">" +
        "&nbsp;" +
        "</td>" +
        "</tr>";
    if (count === 0) {
        top_tr = document.getElementById("top_tr");
        top_tr.style.display = "none";
        top_tr.insertAdjacentHTML('afterend', new_html);
    } else document.getElementById("tr_2_" + count).insertAdjacentHTML('afterend', new_html);
}

function remove_author_row(count) {
    document.getElementById("author_" + count + "_name_display").value = "";
    document.getElementById("author_" + count + "_name_display").parentNode.removeChild(document.getElementById("author_" + count + "_name_display"));
    document.getElementById("author_" + count + "_name").name = "";
    document.getElementById("author_" + count + "_name").value = "";
    document.getElementById("author_" + count + "_name").parentNode.removeChild(document.getElementById("author_" + count + "_name"));
    document.getElementById("author_" + count + "_memberid").name = "";
    document.getElementById("author_" + count + "_memberid").value = "";
    document.getElementById("author_" + count + "_memberid").parentNode.removeChild(document.getElementById("author_" + count + "_memberid"));
    document.getElementById("author_" + count + "_position").name = "";
    document.getElementById("author_" + count + "_position").value = "";
    document.getElementById("author_" + count + "_position").parentNode.removeChild(document.getElementById("author_" + count + "_position"));
    document.getElementById("author_" + count + "_edit_link").parentNode.removeChild(document.getElementById("author_" + count + "_edit_link"));
    document.getElementById("author_" + count + "_add_link").parentNode.removeChild(document.getElementById("author_" + count + "_add_link"));
    document.getElementById("author_" + count + "_remove_link").parentNode.removeChild(document.getElementById("author_" + count + "_remove_link"));
    document.getElementById("author_" + count + "_search_div").parentNode.removeChild(document.getElementById("author_" + count + "_search_div"));
    document.getElementById("author_" + count + "_padding_div").parentNode.removeChild(document.getElementById("author_" + count + "_padding_div"));
    document.getElementById("tr_1_" + count).parentNode.removeChild(document.getElementById("tr_1_" + count));
    document.getElementById("tr_2_" + count).parentNode.removeChild(document.getElementById("tr_2_" + count));
    var found = false;
    var list = document.getElementById("table").getElementsByTagName("tr");
    for (var i = 0; i < list.length; i++) {
        if (list[i].id.indexOf("tr_1_") !== -1) found = true;
    }
    if (found === false) document.getElementById("top_tr").style.display = "table-row";
}

function cookiesYes() {
    document.getElementById("cookies").style.display = "none";
    var date = new Date();
    date.setTime(+date + (730 * 86400000)); // 1000ms * 60s * 60m * 24h
    document.cookie = "AGS_agreed_cookies=true; expires=" + date.toGMTString() + "; path=/";
}

function showSpoiler(element) {
    element.style.display = "none";
    element.parentNode.getElementsByClassName("spoiler")[0].style.display = "inline";
}

function toggleAndResetDownload(checkboxChecked, count) {
    var elementToToggle = document.getElementById("operatingSystemsWrapper_" + count);
    if (checkboxChecked.checked === true) elementToToggle.style.display = "block";
    else {
        if (document.getElementById("dl_url_" + count) != null) {
            document.getElementById("dl_url_" + count).value = "";
            document.getElementById("mirror_url_" + count).value = "";
        }
        if (document.getElementById("store_url_" + count) != null) {
            document.getElementById("store_url_" + count).value = "";
            document.getElementById("demo_url_" + count).value = "";
        }
        elementToToggle.style.display = "none";
    }
}

function processLanguage(language_id, name, count) {
    name = decodeURIComponent(name).replace(/\+/g, " ");
    document.getElementById("language_name_" + count).value = name;
    document.getElementById("language_name_" + count).disabled = true;
    document.getElementById("language_id_" + count).value = language_id;
    document.getElementById("language_search_results_" + count).style.display = "none";
    document.getElementById("language_add_" + count).style.display = "inline";
    document.getElementById("language_edit_" + count).style.display = "inline";
    if (count !== 1) document.getElementById("language_remove_" + count).style.display = "inline";
}

function add_language_row(next_count) {
    var count = next_count - 1;
    var next_next_count = next_count + 1;
    var new_html =
        "<tr id=\"language_tr_" + next_count + "\">\r\n" +
        "<td>\r\n" +
        "</td>\r\n" +
        "<td>\r\n" +
        "<input type=\"text\" id=\"language_name_" + next_count + "\" placeholder=\"Search for Language\" value=\"\" onkeyup=\"ajaxSearcher(this.value + '£$%€' + '" + next_count + "', 'language_search_results_" + next_count + "', '', 'language_search');\"/>&nbsp;&nbsp;<a id=\"language_add_" + next_count + "\" onclick=\"add_language_row(" + next_next_count + ");\" style=\"cursor: pointer; text-decoration: none; display:none;\">&#10133;</a>&nbsp;&nbsp;<a id=\"language_edit_" + next_count + "\" onclick=\"edit_language_row(" + next_count + ");\" style=\"cursor: pointer; text-decoration: none; display: none;\">&#9998;</a>&nbsp;&nbsp;<a id=\"language_remove_" + next_count + "\" onclick=\"remove_language_row(" + next_count + ");\" style=\"cursor: pointer; text-decoration: none; display:none;\">&#10134;</a><input type=\"hidden\" name=\"language_id_" + next_count + "\" id=\"language_id_" + next_count + "\" value=\"\" />\r\n" +
        "<div id=\"language_search_results_" + next_count + "\">\r\n" +
        "</div>\r\n" +
        "</td>\r\n" +
        "</tr>";
    document.getElementById("language_add_" + count).style.display = "none";
    document.getElementById("language_edit_" + count).style.display = "none";
    document.getElementById("language_remove_" + count).style.display = "none";
    document.getElementById("language_tr_" + count).insertAdjacentHTML('afterend', new_html);
}

function remove_language_row(count) {
    count_previous = count - 1;
    document.getElementById("language_add_" + count_previous).style.display = "inline";
    document.getElementById("language_edit_" + count_previous).style.display = "inline";
    if (count_previous !== 1) document.getElementById("language_remove_" + count_previous).style.display = "inline";
    document.getElementById("language_tr_" + count).parentNode.removeChild(document.getElementById("language_tr_" + count));
}

function edit_language_row(count) {
    document.getElementById("language_name_" + count).value = "";
    document.getElementById("language_name_" + count).disabled = false;
    document.getElementById("language_id_" + count).value = "";
    document.getElementById("language_add_" + count).style.display = "none";
    document.getElementById("language_edit_" + count).style.display = "none";
    document.getElementById("language_remove_" + count).style.display = "none";
}

function toggleDownloadsPopup() {
    var divToToggle = document.getElementById("downloadsPopup");

    divToToggle.style.width = "750px";
    divToToggle.style.height = "400px";
    var new_left = (750 / 2);
    divToToggle.style.marginLeft = '-' + new_left.toString() + 'px';
    var new_top = (400 / 2);
    divToToggle.style.marginTop = '-' + new_top.toString() + 'px';
    if (divToToggle.style.display !== "block") divToToggle.style.display = "block";
    else divToToggle.style.display = "none";
}

function toggleCommercial() {
    for (var i = 1; i <= 5; i++) {
        var dl_url_element = document.getElementById("dl_url_" + i);
        var mirror_url_element = document.getElementById("mirror_url_" + i);
        var link_label_1_element = document.getElementById("link_label_1_" + i);
        var link_label_2_element = document.getElementById("link_label_2_" + i);
        if ((dl_url_element != null) && (mirror_url_element != null)) {
            dl_url_element.name = ("store_url_" + i);
            dl_url_element.id = ("store_url_" + i);
            mirror_url_element.name = ("demo_url_" + i);
            mirror_url_element.id = ("demo_url_" + i);
            link_label_1_element.innerHTML = "Store link:";
            link_label_2_element.innerHTML = "Demo download link:";
            continue;
        }
        var store_url_element = document.getElementById("store_url_" + i);
        var demo_url_element = document.getElementById("demo_url_" + i);
        if ((store_url_element != null) && (demo_url_element != null)) {
            store_url_element.name = ("dl_url_" + i);
            store_url_element.id = ("dl_url_" + i);
            demo_url_element.name = ("mirror_url_" + i);
            demo_url_element.id = ("mirror_url_" + i);
            link_label_1_element.innerHTML = "Download link:";
            link_label_2_element.innerHTML = "Download mirror link:";
        }
    }
}

function showDownloadButton() {
    if (document.getElementById("game_button_no_javaScript") != null) document.getElementById("game_button_no_javaScript").style.display = "none";
    if (document.getElementById("game_button_javaScript") != null) document.getElementById("game_button_javaScript").style.display = "inline";
}    </script>
<meta content="AGS is an Adventure Engine to create graphical point-and-click adventure games - Adventure Game Studio" name="description"/>
<meta content="AGS is an Adventure Engine to create graphical point-and-click adventure games - Adventure Game Studio" name="DC.Description"/>
</head>
<body onkeydown="catchTheKeys(event);" onload="window.zoom_level = 1.0; showDownloadButton();">
<div class="fullWidthMainWrapper">
<div id="cookies" style="display: block;">
<h1>⚠ Cookies ⚠</h1>
<p>By continuing to use this site you agree to the use of <a href="https://en.wikipedia.org/wiki/HTTP_cookie" target="_BLANK">cookies</a>.  Please visit <a href="/site/cookies/">this page</a> to see exactly how we use these.</p>
<button class="cookieButton" onclick="cookiesYes()">Okay</button>
</div>
<div class="fullWidthMainWrapper">
<div id="header">
<div id="title">
<a href="/">
<img alt="Adventure Game Studio Banner" src="/images/bannerNew.png"/>
</a>
</div>
<div id="menucontainer">
<div id="emptyLeftArea">
<a href="https://www.facebook.com/groups/adventuregamestudio/" target="_BLANK">
<img alt="Facebook logo" height="29" onerror="this.src='/images/facebook_logo.png'" src="/images/facebook_logo.svg"/>
</a>
<a href="https://discord.gg/BSvN5ZF" target="_BLANK">
<img alt="Discord logo" height="29" onerror="this.src='/images/discord_logo.png'" src="/images/discord_logo.svg"/>
</a>
<a href="/site/community/donate/" target="_BLANK">
<img alt="Donate" height="29" onerror="this.src='/images/donate.png'" src="/images/donate.svg"/>
</a>
</div>
<div id="logindisplay">
				Please <a href="/forums/index.php?action=login">sign in</a> at the <a href="/forums/">forums</a> </div>
<div id="menu">
<a href="/">Home</a>
<a href="/site/ags/">AGS</a>
<a href="/site/games/">Games</a>
<a href="/site/community/">Community</a>
</div>
</div>
</div><div class="main" id="main"> <div class="rightHandColumn">
<div style="height:44px">
<img alt="Play Games" src="/images/sidebox_titlegames.png"/>
</div>
<div class="rightHandContentAreaContainer">
<div class="rightHandContentArea">
<div id="rssIconFloat">
<a href="/includes/gamesrss.xml">
<img alt="RSS Feed" src="/images/rss.png"/>
</a>
</div>
<p class="rightHandAreaTitle">Most Recent Games</p>
<p>
<a href="/site/games/game/2508-shoaly-you-can-t-be-serious-mags-version-/">Shoaly You Can't Be Serious! (MAGS version)</a>
</p>
<p>
<a href="/site/games/game/2507-i-want-to-die-remake/">I want to die - Remake</a>
</p>
<p>
<a href="/site/games/game/2506-starship-light-beta-/">Starship Light [BETA]</a>
</p>
<p>
<a href="/site/games/game/2505-grandma-episode-1-confinement-2021-/">GrandMa episode 1 'Confinement 2021'</a>
</p>
<p>
<a href="/site/games/game/2504-space-quest-xii-demo-/">Space Quest XII (demo)</a>
</p>
</div>
</div>
<img alt="Sidebar bottom" src="/images/sidebox_borderbottom.png"/>
<div style="height: 44px;">
<img alt="From the Forums" src="/images/sidebox_titleforumssingle.png"/>
</div>
<div class="rightHandContentAreaContainer">
<div class="rightHandContentArea">
<p class="recentForumPost">
<a href="/forums/index.php?topic=50980.msg636631068#new" target="_BLANK">Re: A sticky list with open source AGS games?</a>
</p>
<div class="forumPostTag">
				        Posted by <strong><a href="/forums/index.php?action=profile;u=16324" target="_BLANK"></a><a href="/forums/index.php?action=profile;u=16324" target="_BLANK">eri0o</a></strong><br/>2 hours ago
				    </div>
<p class="recentForumPost">
<a href="/forums/index.php?topic=57850.msg636631067#new" target="_BLANK">Re: Global Pandemic Lockdown</a>
</p>
<div class="forumPostTag">
				        Posted by <strong><a href="/forums/index.php?action=profile;u=2754" target="_BLANK"></a><a href="/forums/index.php?action=profile;u=2754" target="_BLANK">Khris</a></strong><br/>2 hours ago
				    </div>
<p class="recentForumPost">
<a href="/forums/index.php?topic=58743.msg636631066#new" target="_BLANK">Re: A jab or not a jab....that is the question</a>
</p>
<div class="forumPostTag">
				        Posted by <strong><a href="/forums/index.php?action=profile;u=2754" target="_BLANK"></a><a href="/forums/index.php?action=profile;u=2754" target="_BLANK">Khris</a></strong><br/>2 hours ago
				    </div>
<p class="sectionLinkRight">
<a href="/site/community/">
                        Visit the Forums &gt;&gt;
                    </a>
</p>
</div>
</div>
<img src="/images/sidebox_borderbottomface.png"/>
</div>
<div align="center" class="mainScreenshot">
<a href="/site/ags/"><img alt="AGS Screenshots" src="/images/agsScreens.png"/>
<p><button class="indexButton">Download AGS!</button></p></a>
</div>
<div class="mainContentColumn">
<div class="titleArea">
<a href="/site/ags/">
<img alt="Make Games" src="/images/title_makegames.png"/>
</a>
</div>
<div class="leftFloatingCup">
</div>
<p><strong>Think you've got what it takes?</strong></p>
<p>Adventure Game Studio (AGS) provides the tools to make your
		own adventure, <strong>for free</strong>! Bring your story and artwork and slot it in, and let AGS do the rest.</p>
<p>AGS provides everything you need from within one easy-to-use application. Create, test and debug your
		game, all in one place. Why wait? Get cracking on your first game now!</p>
<p class="sectionLink">
<a href="/site/ags/">Find out more &gt;&gt;</a>
</p>
</div>
<div class="mainContentColumnLast">
<div class="titleArea">
<a href="/site/games/">
<img alt="Play Games" src="/images/title_playgames.png"/>
</a>
</div>
<div class="leftFloatingCup">
</div>
<p><strong>Hundreds of games have been made with AGS</strong>.</p>
<p>From quick time-fillers to full-length epic adventures, we've got
		plenty of choice of <strong>free</strong> games for you to play!</p>
<div class="gameScreeniesArea">
<a href="/site/games/game/2423-the-mind-mags-march-2020-/">
<img alt="Screenshot 1 of The Mind [MAGS March 2020]" height="50" src="https://www.adventuregamestudio.co.uk/images/games/2423_1_thumb_50.png" title="Screenshot 1 of The Mind [MAGS March 2020]"/>
</a>
				  
				<a href="/site/games/game/1704-the-rebirth-the-reaper/">
<img alt="Screenshot 1 of The Rebirth/The Reaper" height="50" src="https://www.adventuregamestudio.co.uk/images/games/1704_1_thumb_50.png" title="Screenshot 1 of The Rebirth/The Reaper"/>
</a>
				  
				<a href="/site/games/game/2289-sims-hogwarts/">
<img alt="Screenshot 1 of Sims Hogwarts" height="50" src="https://www.adventuregamestudio.co.uk/images/games/2289_1_thumb_50.jpg" title="Screenshot 1 of Sims Hogwarts"/>
</a>
</div>
<p style="clear: right;"></p>
<p class="sectionLink">
<a href="/site/games/">Find Games to Play &gt;&gt;</a>
</p>
</div>
<div style="clear: both;">
</div>
<div class="forceSpaceForFooter"></div>
</div>
</div>
<div id="footerFullWidth">
<div id="footerCopyright">
		© Adventure Game Studio 2021.  Page design by <a href="/forums/index.php?action=profile;u=988">loominous</a> and <a href="/forums/index.php?action=profile;u=817">Darth Mandarb</a>; coding by <a href="/forums/index.php?action=profile;u=39">AGA</a>.<br/>
		Page generated on 14 Jan 2021 at 16:41:38</div>
</div>
</div></body>
</html>