<!DOCTYPE html>
<html class="no-js" lang="cs-CZ">
<head>
<meta charset="utf-8"/>
<title>  Stránka nebyla nalezena | Krajský svaz stolního tenisu PK</title>
<link href="//www.google-analytics.com" rel="dns-prefetch"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/img/icons/favicon.ico" rel="shortcut icon"/>
<meta content="#da532c" name="msapplication-TileColor"/>
<meta content="https://www.stpk.cz/wp-content/themes/stpk/img/icons/browserconfig.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.stpk.cz\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.15"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.stpk.cz/wp-content/themes/stpk/normalize.css?ver=1.0" id="normalize-css" media="all" rel="stylesheet"/>
<link href="https://www.stpk.cz/wp-content/themes/stpk/style.css?ver=1.3" id="html5blank-css" media="all" rel="stylesheet"/>
<script src="https://www.stpk.cz/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-content/plugins/dropdown-menu-widget/scripts/include.js?ver=4.8.15" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-content/themes/stpk/js/lightbox.min.js?ver=4.3" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-content/themes/stpk/js/lity.min.js?ver=4.3" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-content/themes/stpk/js/jquery.ui.map.js?ver=2.7.1" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-content/themes/stpk/js/scripts.js?ver=1.0.2" type="text/javascript"></script>
<link href="https://www.stpk.cz/wp-json/" rel="https://api.w.org/"/>
<!-- Dropdown Menu Widget Styles by shailan (https://metinsaylan.com) v1.9.7 on wp4.8.15 -->
<link href="https://www.stpk.cz/wp-content/plugins/dropdown-menu-widget/css/shailan-dropdown.min.css" rel="stylesheet" type="text/css"/>
<link href="https://www.stpk.cz/wp-content/plugins/dropdown-menu-widget/themes/web20.css" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
	ul.dropdown { white-space: nowrap; }
ul.dropdown li.parent>a{
	padding-right:25px;
}
ul.dropdown li.parent>a:after{
	content:""; position:absolute; top: 45%; right:6px;width:0;height:0;
	border-top:4px solid rgba(0,0,0,0.5);border-right:4px solid transparent;border-left:4px solid transparent }
ul.dropdown li.parent:hover>a:after{
	content:"";position:absolute; top: 45%; right:6px; width:0; height:0;
	border-top:4px solid rgba(0,0,0,0.5);border-right:4px solid transparent;border-left:4px solid transparent }
ul.dropdown li li.parent>a:after{
	content:"";position:absolute;top: 40%; right:5px;width:0;height:0;
	border-left:4px solid rgba(0,0,0,0.5);border-top:4px solid transparent;border-bottom:4px solid transparent }
ul.dropdown li li.parent:hover>a:after{
	content:"";position:absolute;top: 40%; right:5px;width:0;height:0;
	border-left:4px solid rgba(0,0,0,0.5);border-top:4px solid transparent;border-bottom:4px solid transparent }


</style>
<!-- /Dropdown Menu Widget Styles -->
</head>
<body class="error404">
<!-- header -->
<header class="header clear" role="banner">
<div id="headerTop">
<div class="container">
<div class="row">
<!-- logo -->
<div class="col-xxs-12" id="headerUpper">
<p class="logo">
<a href="https://www.stpk.cz"><!--
--><img alt="Krajský svaz stolního tenisu PK" src="https://www.stpk.cz/wp-content/themes/stpk/img/logo.png"/><!--
--><strong>Krajský svaz stolního tenisu Pardubického kraje</strong><!--
--></a>
</p>
</div>
<!-- /logo -->
<!--<div class="col-xxs-12 col-sm-4" id="headerRight">
                                            </div>-->
</div>
</div>
</div>
<a class="linesButton x showMenu" href="#">
<span class="lines"></span>
</a>
</header>
<!-- /header -->
<div id="menuCnt">
<div id="menuCntIn">
<div class="container">
<div class="row">
<nav class="nav col-xxs-12" role="navigation">
<ul class="clearfix">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1961" id="menu-item-1961"><a href="https://www.stpk.cz/">Úvod</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1983" id="menu-item-1983"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201819/">Dospělí 2018/19</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1984" id="menu-item-1984"><a href="https://www.stpk.cz/category/mladez/">Mládež</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1962" id="menu-item-1962"><a href="https://www.stpk.cz/krajske-prebory/">Krajské přebory</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1966" id="menu-item-1966"><a href="https://www.stpk.cz/rozhodci/">Rozhodčí</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1987" id="menu-item-1987"><a href="https://www.stpk.cz/category/zebricky/">Žebříčky</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-1985" id="menu-item-1985"><a href="https://www.stpk.cz/category/dokumenty/">Dokumenty</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1986" id="menu-item-1986"><a href="https://www.stpk.cz/category/dokumenty/dokumenty-rady/">Řády</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1965" id="menu-item-1965"><a href="https://www.stpk.cz/napiste-nam/">Napište nám</a></li>
<li id="searchFormCnt">
<!-- search -->
<form action="https://www.stpk.cz" class="search" method="get" role="search">
<input class="search-input" name="s" placeholder="Zadejte hledaný výraz" type="search"/>
<button class="search-submit" role="button" type="submit">Search</button>
</form>
<!-- /search -->
</li>
</ul>
</nav>
</div>
</div>
</div>
</div>
<section class="content">
<div class="container">
<div class="row">
<div class="col-xxs-12 col-md-2 sidebar sidebar-left">
<div class="sidebar-in">
<div class="row">
<div class="col-xxs-12">
<ul>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1968" id="menu-item-1968"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/zpravy/">Zprávy</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1982" id="menu-item-1982"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201819/">Sezóna 2018/19</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1969" id="menu-item-1969"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201718/">Sezóna 2017/18</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1970" id="menu-item-1970"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201617/">Sezóna 2016/17</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1971" id="menu-item-1971"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201516/">Sezóna 2015/16</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1972" id="menu-item-1972"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201415/">Sezóna 2014/15</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1973" id="menu-item-1973"><a href="https://www.stpk.cz/category/sportovne-technicka-komise/sezona-201314/">Sezóna 2013/14</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1975" id="menu-item-1975"><a href="https://www.stpk.cz/category/mladez/">Mládež</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1979" id="menu-item-1979"><a href="https://www.stpk.cz/category/mladez/vcbtm/">VčBTM</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1981" id="menu-item-1981"><a href="https://www.stpk.cz/category/zebricky/">Žebříčky</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1980" id="menu-item-1980"><a href="https://www.stpk.cz/category/dokumenty/dokumenty-rady/">Řády</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1976" id="menu-item-1976"><a href="https://www.stpk.cz/category/dokumenty/">Dokumenty</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1977" id="menu-item-1977"><a href="https://www.stpk.cz/category/turnaje/">Turnaje</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1978" id="menu-item-1978"><a href="https://www.stpk.cz/category/vykonny-vybor/">Výkonný výbor</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-xxs-12 col-md-8 main-content">
<h1>Chyba 404 - stránka nenalezena</h1>
<p>Můžete se vrátít zpět na <a href="https://www.stpk.cz">hlavní stránku</a>, nebo můžete použít fulltextové vyhledávání.</p>
</div>
<div class="col-xxs-12 col-md-2 sidebar sidebar-right">
<div class="sidebar-in">
<div class="row">
<div class="col-xxs-12">
<ul>
<li><a href="https://www.stpk.cz/2019/05/09/krajska-konference-2019-informace/">Krajská konference 2019 - informace</a></li><li><a href="https://www.stpk.cz/2019/04/23/krajske-zebricky-muzu-2019/">Krajské žebříčky mužů 2019</a></li><li><a href="https://www.stpk.cz/2019/04/18/povinne-hodnoceni-krajskych-soutezi-2019/">Povinné hodnocení krajských soutěží 2019</a></li><li><a href="https://www.stpk.cz/2019/04/14/zpravy-stk-12-kolo-2019-04-14/">Zprávy STK 12.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2019/04/01/zpravy-stk-11-kolo-2019-04-01/">Zprávy STK 11.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2019/03/08/cesky-pohar-i-stupen-2019-20/">Český pohár - I.stupeň - pro sezónu 2019-20 - zájemci hlaste se</a></li><li><a href="https://www.stpk.cz/2019/02/17/zpravy-stk-9-kolo-2019-02-17/">Zprávy STK 9.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2019/01/24/gdpr-vlozene-souhlasy-do-registru/">GDPR - vložené souhlasy do REGISTRu</a></li><li><a href="https://www.stpk.cz/2019/01/06/zpravy-stk-8-kolo-2019-05-1/">Zprávy STK 8.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/12/14/zpravy-stk-5-kolo-2018-19-2/">Důležité zprávy STK po polovině krajských soutěží 2018-19</a></li><li><a href="https://www.stpk.cz/2018/11/18/zpravy-stk-5-kolo-2018-19/">Zprávy STK 5.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/11/13/dulezite-souhlas-se-zpracovanim-osobnich-udaju-registr/">Důležité - Souhlas se zpracováním osobních údajů - Registr</a></li><li><a href="https://www.stpk.cz/2018/11/04/dulezita-zprava-registracni-a-informacni-komise-cast/">Důležitá zpráva Registrační a informační komise ČAST</a></li><li><a href="https://www.stpk.cz/2018/11/04/zpravy-stk-4-kolo-2018-19/">Zprávy STK 4.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/10/22/zpravy-stk-3-kolo-2018-19/">Zprávy STK 3.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/10/08/zpravy-stk-15-a-16-kolo-2017-18-2-3-2/">Zprávy STK 2.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/09/23/zpravy-stk-15-a-16-kolo-2017-18-2-3/">Zprávy STK 1.kolo 2018-19</a></li><li><a href="https://www.stpk.cz/2018/09/19/zpravy-stk-1-kolo-2017-18-2/">Zprávy STK před zahájením soutěží 2018-19</a></li><li><a href="https://www.stpk.cz/2018/07/15/vcbtm-2018-2019/">VčBTM 2018-2019</a></li><li><a href="https://www.stpk.cz/2018/07/02/informace-z-krajskych-soutezi-na-sezonu-201819/">Informace z krajských soutěží na sezónu 2018/19</a></li> </ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- footer -->
<footer class="footer" role="contentinfo">
<div class="container">
<div class="row">
<div class="col-xxs-12">
<p class="copyright text-center">
                    © 2021 Copyright Krajský svaz stolního tenisu PK                </p>
</div>
</div>
</div>
</footer>
<!-- /footer -->
<script src="https://www.stpk.cz/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script src="https://www.stpk.cz/wp-includes/js/wp-embed.min.js?ver=4.8.15" type="text/javascript"></script>
</body>
</html>
