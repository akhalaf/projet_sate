<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page non trouvée - Tétine en folie</title>
<!-- This site is optimized with the Yoast SEO plugin v7.5.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="fr_FR" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page non trouvée - Tétine en folie" property="og:title"/>
<meta content="Tétine en folie" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page non trouvée - Tétine en folie" name="twitter:title"/>
<!-- / Yoast SEO plugin. -->
<link href="//s0.wp.com" rel="dns-prefetch"/>
<link href="//secure.gravatar.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.tetine-en-folie.com/feed" rel="alternate" title="Tétine en folie » Flux" type="application/rss+xml"/>
<link href="https://www.tetine-en-folie.com/comments/feed" rel="alternate" title="Tétine en folie » Flux des commentaires" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.0.6 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-120027095-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-120027095-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('require', 'linkid', 'linkid.js');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.tetine-en-folie.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C400italic%2C700italic%7CPoppins%3A300%2C600&amp;ver=4.9.16#038;subset=latin%2Clatin-ext" id="eightydays-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tetine-en-folie.com/wp-content/themes/eightydays-lite/css/bootstrap.css?ver=3.3.5" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tetine-en-folie.com/wp-content/themes/eightydays-lite/style.css?ver=4.9.16" id="eightydays-lite-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tetine-en-folie.com/wp-content/plugins/instagram-slider-widget/assets/css/instag-slider.css?ver=1.4.3" id="instag-slider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tetine-en-folie.com/wp-content/plugins/jetpack/modules/widgets/social-icons/social-icons.css?ver=20170506" id="jetpack-widget-social-icons-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.tetine-en-folie.com/wp-content/plugins/jetpack/css/jetpack.css?ver=6.2.2" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.tetine-en-folie.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.tetine-en-folie.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","is_debug_mode":"false","download_extensions":"doc,exe,js,pdf,ppt,tgz,zip,xls","inbound_paths":"","home_url":"https:\/\/www.tetine-en-folie.com","track_download_as":"event","internal_label":"int","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.tetine-en-folie.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.0.6" type="text/javascript"></script>
<script src="https://www.tetine-en-folie.com/wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js?ver=2.2" type="text/javascript"></script>
<link href="https://www.tetine-en-folie.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.tetine-en-folie.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.tetine-en-folie.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<link href="//v0.wordpress.com" rel="dns-prefetch"/>
<link href="//i0.wp.com" rel="dns-prefetch"/>
<link href="//i1.wp.com" rel="dns-prefetch"/>
<link href="//i2.wp.com" rel="dns-prefetch"/>
<style type="text/css">img#wpstats{display:none}</style> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<link href="https://i1.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-150.jpg?fit=32%2C32&amp;ssl=1" rel="icon" sizes="32x32"/>
<link href="https://i1.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-150.jpg?fit=150%2C150&amp;ssl=1" rel="icon" sizes="192x192"/>
<link href="https://i1.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-150.jpg?fit=150%2C150&amp;ssl=1" rel="apple-touch-icon-precomposed"/>
<meta content="https://i1.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-150.jpg?fit=150%2C150&amp;ssl=1" name="msapplication-TileImage"/>
</head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-120027095-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120027095-1');
</script>
<body class="error404 wp-custom-logo" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Aller au contenu</a>
<div class="top-bar">
<div class="container">
<div class="top-bar-left pull-left">
</div>
<div class="top-bar-right pull-right text-right">
</div>
</div>
</div><!-- .top-bar -->
<header class="site-header" id="masthead" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="site-branding text-center">
<a class="custom-logo-link" href="https://www.tetine-en-folie.com/" itemprop="url" rel="home"><img alt="Tétine en folie" class="custom-logo" data-attachment-id="6" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="logo-tétine-en-folie-100" data-large-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-medium-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-orig-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-orig-size="100,100" data-permalink="https://www.tetine-en-folie.com/logo-tetine-en-folie-100" height="100" itemprop="logo" sizes="(max-width: 100px) 100vw, 100px" src="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" srcset="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?w=100&amp;ssl=1 100w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?resize=67%2C67&amp;ssl=1 67w" width="100"/></a> <div class="site-description">Tétine en folie : conseils de mamans</div>
</div>
<nav class="main-navigation" id="site-navigation">
<div class="container"><ul class="primary-menu text-center" id="primary-menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12" id="menu-item-12"><a href="https://www.tetine-en-folie.com/./la-poussee-dentaire">Bébé fait ses dents</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13" id="menu-item-13"><a href="https://www.tetine-en-folie.com/./soulager-bebe-poussee-dentaire">Soulager bébé</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14" id="menu-item-14"><a href="https://www.tetine-en-folie.com/./accessoires-dentition">Accessoires de dentition</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-15" id="menu-item-15"><a href="https://www.tetine-en-folie.com/qui-sommes-nous">Qui sommes nous ?</a></li>
</ul></div> </nav><!-- #site-navigation -->
</header><!-- #masthead -->
<div class="container">
<main class="site-main" id="main">
<h1 class="not-found-title">404</h1>
<h2 class="not-found-message">Oups ! On dirait que la page n’est plus là.</h2>
<a class="not-found-back" href="https://www.tetine-en-folie.com/">Retour à l’accueil</a>
</main>
</div><!-- .container -->
<footer class="site-footer" id="colophon" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
<div class="footer-widgets">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6 widget widget_text" id="text-4"><h4 class="widget-title"><span>A propos</span></h4> <div class="textwidget"><p>Parce que la poussée dentaire est un moment douloureux, nous regroupons sur le site tétine en folie tous les conseils et astuces pour soulager bébé lorsqu’il fait ses dents.</p>
<p>Astuces naturelles, accessoires pour la dentition ou encore conseils de mamans expérimentés afin que bébé fasse ses dents sans douleurs !</p>
</div>
</div><div class="col-md-3 col-sm-6 widget eightydays-recent-posts-widget" id="eightydays-recent-posts-widget-3"><h4 class="widget-title"><span>Bébé fait ses dents</span></h4> <ul>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/la-poussee-dentaire/poussee-dentaire-quels-symptomes-et-quelle-duree.php" title="Poussée dentaire : quels symptômes et quelle durée ?">
<img alt="poussee-dentaire-symptomes" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="53" data-comments-opened="1" data-image-description="&lt;p&gt;poussee-dentaire-symptomes&lt;/p&gt;
" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="poussee-dentaire-symptomes" data-large-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?fit=600%2C372&amp;ssl=1" data-medium-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?fit=300%2C186&amp;ssl=1" data-orig-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?fit=600%2C372&amp;ssl=1" data-orig-size="600,372" data-permalink="https://www.tetine-en-folie.com/la-poussee-dentaire/poussee-dentaire-quels-symptomes-et-quelle-duree.php/attachment/poussee-dentaire-symptomes-2" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?resize=150%2C150&amp;ssl=1 150w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?resize=67%2C67&amp;ssl=1 67w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/poussee-dentaire-symptomes.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/la-poussee-dentaire/poussee-dentaire-quels-symptomes-et-quelle-duree.php" rel="bookmark" title="Permalien vers Poussée dentaire : quels symptômes et quelle durée ?">Poussée dentaire : quels symptômes et quelle durée ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-03-16T22:21:06+00:00">16 mars 2018</time>
</div>
</div>
</li>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/la-poussee-dentaire/a-quel-age-bebe-fait-il-ses-dents.php" title="À quel âge bébé fait-il ses dents ?">
<img alt="bebe-fait-ses-dents" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="55" data-comments-opened="1" data-image-description="&lt;p&gt;bebe-fait-ses-dents&lt;/p&gt;
" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="bebe-fait-ses-dents" data-large-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?fit=390%2C219&amp;ssl=1" data-medium-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?fit=300%2C168&amp;ssl=1" data-orig-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?fit=390%2C219&amp;ssl=1" data-orig-size="390,219" data-permalink="https://www.tetine-en-folie.com/la-poussee-dentaire/a-quel-age-bebe-fait-il-ses-dents.php/attachment/bebe-fait-ses-dents" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?resize=150%2C150&amp;ssl=1 150w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?resize=67%2C67&amp;ssl=1 67w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/bebe-fait-ses-dents.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/la-poussee-dentaire/a-quel-age-bebe-fait-il-ses-dents.php" rel="bookmark" title="Permalien vers À quel âge bébé fait-il ses dents ?">À quel âge bébé fait-il ses dents ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-03-04T22:24:06+00:00">4 mars 2018</time>
</div>
</div>
</li>
</ul>
</div><div class="col-md-3 col-sm-6 widget eightydays-recent-posts-widget" id="eightydays-recent-posts-widget-4"><h4 class="widget-title"><span>Soulager bébé</span></h4> <ul>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/que-mettre-dans-sa-valise-trousseau-de-maternite.php" title="Que mettre dans sa valise / trousseau de maternité">
<img alt="" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="89" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"3dvin - Fotolia","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="trousseau-maternite" data-large-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?fit=870%2C353&amp;ssl=1" data-medium-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?fit=300%2C122&amp;ssl=1" data-orig-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?fit=880%2C357&amp;ssl=1" data-orig-size="880,357" data-permalink="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/que-mettre-dans-sa-valise-trousseau-de-maternite.php/attachment/trousseau-maternite" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?resize=150%2C150&amp;ssl=1 150w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?resize=67%2C67&amp;ssl=1 67w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2019/05/trousseau-maternite.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/que-mettre-dans-sa-valise-trousseau-de-maternite.php" rel="bookmark" title="Permalien vers Que mettre dans sa valise / trousseau de maternité">Que mettre dans sa valise / trousseau de maternité</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2019-05-03T10:39:14+00:00">3 mai 2019</time>
</div>
</div>
</li>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/tetines-et-sucettes-pour-bebe-avantages-et-inconvenients.php" title="Tétines et sucettes pour bébé : avantages et inconvénients">
<img alt="" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="75" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="dents-bébé-ordre-credit-bobo-prevention" data-large-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?fit=533%2C355&amp;ssl=1" data-medium-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?fit=300%2C200&amp;ssl=1" data-orig-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?fit=533%2C355&amp;ssl=1" data-orig-size="533,355" data-permalink="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/tetines-et-sucettes-pour-bebe-avantages-et-inconvenients.php/attachment/dents-bebe-ordre-credit-bobo-prevention" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?resize=67%2C67&amp;ssl=1" srcset="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?resize=150%2C150&amp;ssl=1 150w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?resize=67%2C67&amp;ssl=1 67w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/dents-bébé-ordre-credit-bobo-prevention.png?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/tetines-et-sucettes-pour-bebe-avantages-et-inconvenients.php" rel="bookmark" title="Permalien vers Tétines et sucettes pour bébé : avantages et inconvénients">Tétines et sucettes pour bébé : avantages et inconvénients</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-06-10T15:05:54+00:00">10 juin 2018</time>
</div>
</div>
</li>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/comment-choisir-la-sucette-tetine-de-bebe%e2%80%89.php" title="Comment choisir la sucette de bébé ?">
<img alt="choisir-tetine-bebe" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="31" data-comments-opened="1" data-image-description="&lt;p&gt;choisir-tetine-bebe&lt;/p&gt;
" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="choisir-tetine-bebe" data-large-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?fit=800%2C450&amp;ssl=1" data-medium-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?fit=300%2C169&amp;ssl=1" data-orig-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?fit=800%2C450&amp;ssl=1" data-orig-size="800,450" data-permalink="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/comment-choisir-la-sucette-tetine-de-bebe%e2%80%89.php/attachment/choisir-tetine-bebe" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?resize=150%2C150&amp;ssl=1 150w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?resize=67%2C67&amp;ssl=1 67w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/choisir-tetine-bebe.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/soulager-bebe-poussee-dentaire/comment-choisir-la-sucette-tetine-de-bebe%e2%80%89.php" rel="bookmark" title="Permalien vers Comment choisir la sucette de bébé ?">Comment choisir la sucette de bébé ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-05-20T17:03:58+00:00">20 mai 2018</time>
</div>
</div>
</li>
</ul>
</div><div class="col-md-3 col-sm-6 widget eightydays-recent-posts-widget" id="eightydays-recent-posts-widget-5"><h4 class="widget-title"><span>Accessoires de dentition</span></h4> <ul>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/accessoires-dentition/collier-de-naissance-quel-type-de-collier-acheter-pour-une-femme-enceinte.php" title="Collier de naissance : quel type de collier acheter pour une femme enceinte ?">
<img alt="" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="79" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="IMG_7630-600×400" data-large-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?fit=600%2C400&amp;ssl=1" data-medium-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?fit=300%2C200&amp;ssl=1" data-orig-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?fit=600%2C400&amp;ssl=1" data-orig-size="600,400" data-permalink="https://www.tetine-en-folie.com/accessoires-dentition/collier-de-naissance-quel-type-de-collier-acheter-pour-une-femme-enceinte.php/attachment/img_7630-600x400" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?resize=150%2C150&amp;ssl=1 150w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?resize=67%2C67&amp;ssl=1 67w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/06/IMG_7630-600x400.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/accessoires-dentition/collier-de-naissance-quel-type-de-collier-acheter-pour-une-femme-enceinte.php" rel="bookmark" title="Permalien vers Collier de naissance : quel type de collier acheter pour une femme enceinte ?">Collier de naissance : quel type de collier acheter pour une femme enceinte ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-06-12T15:41:45+00:00">12 juin 2018</time>
</div>
</div>
</li>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/accessoires-dentition/doit-on-mettre-un-anneau-de-dentition-au-frigo-ou-congelateur.php" title="Doit-on mettre un anneau de dentition au frigo ou congélateur ?">
<img alt="anneau-dentition-refrigere" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="67" data-comments-opened="1" data-image-description="&lt;p&gt;anneau-dentition-refrigere&lt;/p&gt;
" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="anneau-dentition-refrigere" data-large-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?fit=301%2C167&amp;ssl=1" data-medium-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?fit=300%2C166&amp;ssl=1" data-orig-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?fit=301%2C167&amp;ssl=1" data-orig-size="301,167" data-permalink="https://www.tetine-en-folie.com/accessoires-dentition/doit-on-mettre-un-anneau-de-dentition-au-frigo-ou-congelateur.php/attachment/anneau-dentition-refrigere" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?resize=67%2C67&amp;ssl=1" srcset="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?resize=150%2C150&amp;ssl=1 150w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?resize=67%2C67&amp;ssl=1 67w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/anneau-dentition-refrigere.jpeg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/accessoires-dentition/doit-on-mettre-un-anneau-de-dentition-au-frigo-ou-congelateur.php" rel="bookmark" title="Permalien vers Doit-on mettre un anneau de dentition au frigo ou congélateur ?">Doit-on mettre un anneau de dentition au frigo ou congélateur ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-05-30T08:41:04+00:00">30 mai 2018</time>
</div>
</div>
</li>
<li>
<a class="eightydays-thumb" href="https://www.tetine-en-folie.com/accessoires-dentition/bois-tissu-silicone-alimentaire-comment-choisir-son-attache-tetine.php" title="Bois, tissu, silicone alimentaire : comment choisir son attache tétine ?">
<img alt="" class="attachment-eightydays-widget-thumb size-eightydays-widget-thumb wp-post-image" data-attachment-id="36" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="attache-tetine-en-bois-tissu-silicone" data-large-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?fit=870%2C435&amp;ssl=1" data-medium-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?fit=300%2C150&amp;ssl=1" data-orig-file="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?fit=1000%2C500&amp;ssl=1" data-orig-size="1000,500" data-permalink="https://www.tetine-en-folie.com/accessoires-dentition/bois-tissu-silicone-alimentaire-comment-choisir-son-attache-tetine.php/attachment/attache-tetine-en-bois-tissu-silicone" height="67" sizes="(max-width: 67px) 100vw, 67px" src="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?resize=67%2C67&amp;ssl=1" srcset="https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?resize=150%2C150&amp;ssl=1 150w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?resize=67%2C67&amp;ssl=1 67w, https://i2.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/attache-tetine-en-bois-tissu-silicone.jpg?zoom=3&amp;resize=67%2C67 201w" width="67"/> </a>
<div class="eightydays-text">
<div class="eightydays-title">
<a href="https://www.tetine-en-folie.com/accessoires-dentition/bois-tissu-silicone-alimentaire-comment-choisir-son-attache-tetine.php" rel="bookmark" title="Permalien vers Bois, tissu, silicone alimentaire : comment choisir son attache tétine ?">Bois, tissu, silicone alimentaire : comment choisir son attache tétine ?</a>
</div>
<div class="eightydays-meta">
<time class="eightydays-date" datetime="2018-04-20T17:13:01+00:00">20 avril 2018</time>
</div>
</div>
</li>
</ul>
</div> </div>
</div>
</div>
<div class="site-info clearfix">
<div class="container">
<a class="custom-logo-link" href="https://www.tetine-en-folie.com/" itemprop="url" rel="home"><img alt="Tétine en folie" class="custom-logo" data-attachment-id="6" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="logo-tétine-en-folie-100" data-large-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-medium-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-orig-file="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" data-orig-size="100,100" data-permalink="https://www.tetine-en-folie.com/logo-tetine-en-folie-100" height="100" itemprop="logo" sizes="(max-width: 100px) 100vw, 100px" src="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?fit=100%2C100&amp;ssl=1" srcset="https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?w=100&amp;ssl=1 100w, https://i0.wp.com/www.tetine-en-folie.com/wp-content/uploads/2018/05/logo-tétine-en-folie-100.png?resize=67%2C67&amp;ssl=1 67w" width="100"/></a> <div class="credit">
				Copyright © 2021 <a href="https://www.tetine-en-folie.com/" rel="home">Tétine en folie</a>. Tous droits réservés.<br/>Fièrement propulsé par <a href="https://wordpress.org">WordPress</a>. Thème <a href="http://gretathemes.com/wordpress-themes/eightydays">EightyDays Lite</a> par GretaThemes.			</div>
</div>
</div><!-- .site-info -->
</footer>
</div><!-- #page -->
<aside class="mobile-sidebar">
<nav class="mobile-navigation">
<ul class="primary-menu-mobile" id="primary-menu-mobile"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12"><a href="https://www.tetine-en-folie.com/./la-poussee-dentaire">Bébé fait ses dents</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13"><a href="https://www.tetine-en-folie.com/./soulager-bebe-poussee-dentaire">Soulager bébé</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14"><a href="https://www.tetine-en-folie.com/./accessoires-dentition">Accessoires de dentition</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-15"><a href="https://www.tetine-en-folie.com/qui-sommes-nous">Qui sommes nous ?</a></li>
</ul> </nav>
</aside>
<a href="#" id="scroll-to-top"><span class="genericon genericon-collapse"></span></a>
<div style="display:none">
</div>
<script src="https://www.tetine-en-folie.com/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20130122" type="text/javascript"></script>
<script src="https://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=202102" type="text/javascript"></script>
<script src="https://secure.gravatar.com/js/gprofiles.js?ver=2021Janaa" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script src="https://www.tetine-en-folie.com/wp-content/plugins/jetpack/modules/wpgroho.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://www.tetine-en-folie.com/wp-content/themes/eightydays-lite/js/script.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://www.tetine-en-folie.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:6.2.2',blog:'147118297',post:'0',tz:'1',srv:'www.tetine-en-folie.com'} ]);
	_stq.push([ 'clickTrackerInit', '147118297', '0' ]);
</script>
</body>
</html>
