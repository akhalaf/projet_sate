<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Bureau of Education &amp; Research | Professional Staff Development and Training for K-12 Educators in U.S. and Canada</title>
<!-- --- Organization Structured Data --- -->
<script type="application/ld+json">
{
  "@context" : "https://schema.org",
  "@type" : "Organization",
  "name" : "Bureau of Education & Research",
  "logo" : "http://www.ber.org/img/STdataLogo.jpg",
  "telephone" : "1-800-735-3503",
  "email" : "info@ber.org",
  "areaServed" : "USA",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "915 118th Ave SE Suite 230",
    "addressLocality" : "Bellevue",
    "addressRegion" : "WA",
    "addressCountry" : "USA",
    "postalCode" : "98005"
  },
  "url" : "http://www.ber.org/",
  "sameAs": [
    "https://www.facebook.com/bureauofeducationandresearch",
    "https://www.youtube.com/user/BERStaffDevelopment",
    "https://twitter.com/bureauofed",
    "https://pinterest.com/bureauofed/",
	"https://www.linkedin.com/company/bureau-of-education-&-research"
  ]
}
</script>
<!-- --- End Organization Structured Data --- -->
<link href="css/subnav.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/mainnav.css" rel="stylesheet" type="text/css"/>
<!--[if IE]>
<style>
   indexright p.indexquote {
	line-height:19px !important;
	}
</style>
<![endif]-->
<script src="js/linkedphotoshuffler.js" type="text/javascript"></script>
<!-- ------ Start Page Custom Metadata ------ -->
<!-- Custom Description Text -->
<!-- Custom Keyword Text -->
<!-- ------ End Page Custom Metadata ------ -->
<script crossorigin="anonymous" src="https://kit.fontawesome.com/7a4332ecfd.js"></script>
<!--<link rel="canonical" href="https://www.ber.org/"/>-->
<link href="https://www.ber.org/index.cfm" rel="canonical"/>
<meta content="initial-scale=1" name="viewport"/>
<meta content="BER Home | Bureau of Education &amp; Research (BER) is a sponsor of staff development training for professional educators in the United States and Canada offering seminars, PD Kits, self-study resources, and Online courses." name="description"/>
<meta content="Homepage, workshops, conferences, ber, seminars, continuing education, bureau of education and research, ber seminars, bureau of education, teacher, educator, kindergarten, staff development, teacher training, training, institute, professional development, teacher resources, singapore math, differentiated instruction, difficult students, Difficult students pd, RTI pd, RTI, co-teaching, Co-teaching for educators, teacher inservice, inservice, in-service, teacher workshop, workshop, teacher conference, conference, Teacher seminar, special education, spaecial ed, PD, K12 training, K12 training for teachers, Differentiated Instruction for teachers, Education seminar, PD Kits, PD Resource Kits, PD Resources, video training, videos for educators," name="keywords"/>
<script src="/js/ga_social_tracking.js" type="text/javascript"></script>
<script>
(function(){
var twitterWidgets = document.createElement('script');
twitterWidgets.type = 'text/javascript';
twitterWidgets.async = true;
twitterWidgets.src = 'https://platform.twitter.com/widgets.js';
// Setup a callback to track once the script loads.
twitterWidgets.onload = _ga.trackTwitter;
document.getElementsByTagName('head')[0].appendChild(twitterWidgets);
})();
</script>
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '176943986220365');
  fbq('track', 'PageView');
  fbq('track', 'ViewContent');
  fbq('track', 'Search');
  fbq('track', 'Purchase');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=176943986220365&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-3986842-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3986842-1');
</script>
</head>
<body>
<center>
<!-- begin logo and main nav -->
<div id="headersticky">
<div id="headercenter">
<table border="0" cellpadding="0" cellspacing="0" height="74" width="900">
<tr>
<td align="left" rowspan="2" valign="top" width="215">
<a href="/index.cfm"><img alt="Bureau of Education &amp; Research Logo" border="0" height="61" src="/img/logo.jpg" vspace="0" width="213"/></a>
</td>
<td align="right" height="42" valign="top">
<div class="smallnav">
<ul id="smallnav">
<li><a href="/about/index.cfm">About BER</a></li>
<li><a href="/about/contact.cfm">Contact Us</a></li>
<!--<li><a href="/store/Tools-for-Teachers_Blog/Default.aspx">Blog</a></li>-->
<li><a href="/about/faq.cfm">FAQ</a></li>
<li><a href="/seminars/credit/ceus.cfm">CEUs/Credit</a></li>
<!--<li><a href="/seminars/directions.cfm">Event Directions</a></li>-->
<li class="last"><a href="https://www.ber.org/store/AccountLogin.aspx">Online Learning and Store Login</a></li>
</ul>
</div>
</td>
</tr>
<tr>
<td valign="bottom">
<div id="header">
<ul class="mainnav">
<li id="firstLi">
<a href="/seminars/index.cfm" id="firstLink">
Live Online PD Events</a>
</li>
<li>
<a href="/seminars/recorded-events/index.cfm">
Recorded PD Events</a>
</li>
<li>
<a href="/online/index.cfm">
On-Demand Online Courses</a>
</li>
<li>
<a href="/onsite/index.cfm">
On-Site PD</a>
</li>
<li id="lastLi">
<a href="/pdresourcekits/index.cfm" id="lastLink">
PD Resource Kits</a>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td bgcolor="#7fa6cf" colspan="2" height="10">
</td>
</tr>
</table>
<!-- --- Navigation Structured Data --- -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "SiteNavigationElement",
    "name": [
        "Home",
        "Seminars & Conferences",
        "Online Learning",
        "On-Site PD",
        "PD Resource Kits",
        "About BER",
        "Contact Us"
    ],
    "url": [
        "https://www.ber.org/",
        "https://www.ber.org/seminars/index.cfm",
        "/store/content/Default.aspx",
        "https://www.ber.org/onsite/index.cfm",
        "https://www.ber.org/pdkits/",
        "https://www.ber.org/about/index.cfm",
        "https://www.ber.org/about/contact.cfm"
    ]
}
</script>
<!-- --- End Navigation Structured Data --- -->
</div>
</div>
<div id="headerspacer">
</div>
<div id="eventStatus" style="width: 860px; padding: 10px 20px; background-color: darkred;">
<p style="color: #FFF; font-size: 12px;">With the challenges presented by the coronavirus (COVID-19), all live in-person events are being presented in a live online format. <a href="/live-event-status/index.cfm" style="color: #FFF; font-weight: bold;">Learn More</a></p>
</div>
<!-- end logo and main nav -->
<!-- begin content -->
<table border="0" cellpadding="0" cellspacing="0" height="90" style="background-color:#FFFFFF;" width="900">
<tr>
<td valign="top" width="595">
<div class="indextop">
<!--<div id="photodiv">
            	<a id="photoanchor" href="/seminars/index.cfm">
            	<img src="img/index/shuffle_images/seminars.jpg" name="photoimg" id="photoimg" border="none" alt="Bureau of Education & Research Offers" />
                </a>
            </div>-->
<div style="width: 580px; height: 346px; background: url('/img/index/LiveOnlineEvents.jpg'); position: relative;">
<div style="background-color: #7FA6CF; width: 580; height: 30px;padding: 20px">
<p style="margin: 0px; color: #FFF; font-family: 'Helvetica Neue', Helvetica Neue, sans-serif; font-size: 30px; font-weight: 500; text-align: center; line-height: 30px; text-shadow: 2px 2px 2px rgba(0, 0, 0, .75);">New PD Options</p>
</div>
<div style="background-color: rgba(255,255,255,.85); padding-top: 40px; height: calc(100% - 110px);">
<ul style="font-size: 18px; display: block; width: 72%; margin-left: auto; margin-right: auto; margin-top: 0px; padding-left: 0px; list-style-type: none; color: #013185; font-weight: bold;">
<li style="padding: 0px 0px 10px 0px"><i class="fas fa-laptop"></i>  Live Online Seminars and Conferences</li>
<li style="padding: 10px 0px;"><i class="far fa-file-alt"></i>    Extensive Digital Resource Handbooks</li>
<li style="padding: 10px 0px 0px;"><i class="fas fa-video"></i>  Can't Attend? New Recorded Event Options</li>
</ul>
<style>
						a#summerPDbttn {font-size: 20px; padding: 10px 20px; display: block; width: 150px; margin-top: 40px; margin-left: auto; margin-right: auto; background-color: #7FA6CF; text-align: center; opacity: 1.0; color: #FFF; text-shadow: 2px 2px 2px rgba(0, 0, 0, .5); text-decoration: none;border-radius: 5px; border: solid 0px #013185;}
						a#summerPDbttn:hover {background-color: #013185;}
					</style>
<a href="/seminars/index.cfm" id="summerPDbttn">Learn More</a>
</div>
</div>
</div>
</td>
<td valign="top" width="305">
<div class="indexmenu2"><a href="/seminars/index.cfm" style="margin-top:15px; border-top:0px;"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">Live Online PD Events</span><br/>
    Search for Professional Development seminars <br/>
    and conferences presented Live Online.</a>
<a href="/seminars/recorded-events/index.cfm" style="padding-bottom:9px; border-top: 0px; padding-left: 45px; width: 240px; margin-top: -3px; padding-top: 5px;"><span style="display: inline-block; height: 17px; font-size: 15px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 3px auto;">Recorded PD Events</span><br/>
	Can't attend a live event?<br/>
    Take the course online at your convenience.</a>
<br style="clear:both;"/>
</div>
<!--<div class="indexmenu2">
    <a style="padding-bottom:10px;" href="/seminars/recorded-events/index.cfm"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">Recorded PD Events</span><br />
    Take courses online at your convenience.<br>
	Receive the extensive resource handbook.</a>
	<br style="clear:both;" />
    </div>-->
<div class="indexmenu2"><a href="/online/index.cfm" style="height: 52px;"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">On-Demand Online PD Courses</span><br/>Learn when it's most convenient for you.<br/>Ideal for individuals or groups.</a>
<!--<a id="grpdsc" href="/online/groupdiscounts/index.cfm"><img id="homepagenew2" src="/img/new.png" height="24" border="none" alt="New!" />&nbsp;&nbsp;&nbsp;&nbsp;Group Discounts</a>--><br style="clear:both;"/></div>
<div class="indexmenu2"><a href="/onsite/index.cfm" style="height: 52px;"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">On-Site PD <!--<span style="font-size: 14px;">&amp;</span><img id="homepagenew1" style="margin: -13px -24px -24px 85px;" src="/img/new.png" height="24" border="none" alt="New!" />&nbsp;Live Online PD--></span><br/>Choose the topics and presenters to
    bring <br/>to your location or in a Live Online format.</a><br style="clear:both;"/></div>
<div class="indexmenu2"><a href="/pdresourcekits/index.cfm" style="height: 52px;"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">PD Resource Kits</span><br/>
    Browse outstanding resources you can <br/>
    use to conduct   PD with your own staff.</a>
<br style="clear:both;"/>
</div>
<!--<br style="clear:both;" />
    
    <div class="indexmenu2">
    <a style="padding-bottom:10px;" href="/store/content/audioseminars.aspx"><span style="display: inline-block; height: 17px; font-size: 18px; font-weight: bold; color: #013185; font-family: 'Arial Narrow', Arial, sans-serif;margin: -4px auto 7px auto;">Listen to PD Seminars</span><br />
    Home, school, or in your car &#8212; listen <br />
  to BER seminars at your convenience.</a>
    </div>-->
</td>
</tr>
</table>
<!-- end header -->
<table border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;" width="900">
<tr>
<td colspan="2" height="118" rowspan="2" valign="top">
<div class="indexleft h1">
<h1>Professional Development for Teachers and Other Educators</h1>
<p class="indexintro">Bureau of Education &amp; Research (BER) is the leading provider of professional<br/> development and PD training resources for teachers and other educators in North America.</p>
</div>
<div style="text-align: center; background-color:#E3ECF5; margin-left: 15px; margin-bottom: 15px; padding: 15px;">
<h2 style="font-size: 20px; color: #013185; line-height: 1.5;">New DISTANCE LEARNING Live Online Seminars </h2>
<p style="text-align: center;font-weight: bold; font-size: 18px;">DISTANCE LEARNING: Maximize Student Success</p>
<div style="width: 50%; text-align: center;margin: 30px 0px; float: left;">
<p style="text-align: center; font-size: 16px;">Grades K-6</p>
<a href="/seminars/topics/distance_learning.cfm?grade=E" id="summerPDbttn" style="font-size: 14px; width: 100px; margin-top: 20px;">Learn More</a>
</div>
<div style="width: 50%; text-align: center;margin: 30px 0px; float: right;">
<p style="text-align: center; font-size: 16px;">Grades 6-12</p>
<a href="/seminars/topics/distance_learning.cfm?grade=S" id="summerPDbttn" style="font-size: 14px; width: 100px; margin-top: 20px;">Learn More</a>
</div>
<div style="clear: both;"></div>
</div>
</td>
<!-- end left col -->
<td rowspan="2" valign="top" width="305">
<!-- start right col content -->
<!--<div class="indexright">
		<p><strong>We are currently experiencing  problems with our 1-800 number, please call 1-425-453-2121 for any inquiries. We apologize any inconvenience; we are working to resolve the issue.</strong></p>
        <p class="indexquote" style="font-size:13px; line-height:25px;">&quot;Every BER seminar I have attended has far surpassed my expectations. Information is always practical and teacher friendly.&quot;<br />
          <div class="indexquotename">&mdash; Pam Gibson, EC Teacher</div><!-- closes content div -->
<!--</p>
</div>-->
<!--<div class="indexright" style="margin-left:15px; margin-top:10px; margin-right:15px; width:auto; padding:8px 8px 1px 8px; background-color:#E3ECF5">
      <p class="indexquote" style="font-size:13px; margin:0px;">&quot;I have taught for 30 years and I always go away from a BER seminar with new ideas.&quot;<br />
        <div class="indexquotename">&mdash; Barbara Stone, Teacher</div>
      </p>
</div>-->
<style>
	a.learnMoreBttn {
		padding: 3px 12px; 
		color: #FFF;
		background-color: #A30000;
		display: inline-block; 
		border: solid 2pt #7C0002; 
		border-radius: 3px;
		text-decoration: none;
		margin: 50px 0 0 5px;
	}
	a.learnMoreBttn:hover {
		text-decoration: none;
		background-color: #7C0002;
		color: #FFF;
		/*box-shadow: 0px 0px 5px 2px rgba(119, 119, 119, 0.75);*/ 
	}
	a.learnMoreBttn p {
		margin: 0 0 2px 0; 
		font-size: 15px; 
		font-weight: bold;
	}
	@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
     /* IE10+ CSS styles go here */
		.onlinPD {
			margin-left: -25px !important;
		}
	}
	.onlinPD {
			margin-left: -25px\9 !important;
		}
	
</style>
<div class="onlinPD" style="padding: 0px 30px 0px 20px;">
<div style="background-image: url('/img/index/OLLsmall.jpg'); background-repeat: no-repeat; background-position: top right;">
<h2 style="font-size: 16px; color: #013185; line-height: 1.5;">On-Demand<br/>Online PD Courses<br/>for Teachers</h2>
<a class="learnMoreBttn" href="https://www.ber.org/online/index.cfm">
<p>Learn More</p>
</a>
<p style="font-weight: bold; color: #013185; font-size: 15px; margin: 75px 0px 10px 0px;">Over 90 Courses Available</p>
<ul style="font-weight: bold; margin: 0px 0px 30px 0px;">
<li>At Your Convenience</li>
<li style="margin-top: 10px;">Video-Based</li>
<li style="margin-top: 10px;">Practical and Affordable</li>
<li style="margin-top: 10px;">Group Discounts</li>
<li style="margin-top: 10px;"><img alt="New!" border="none" height="24" id="homepagenew1" src="/img/new.png" style="margin: -8px 0px -24px -45px;"/>State CEUs - Clock Hours</li>
</ul>
</div>
</div>
<br clear="all"/>
</td>
<!-- end right col content -->
</tr>
</table>
</center>
<!-- Start Footer -->
<div class="footer">
<div class="contact">
<p><strong>© Bureau of Education &amp; Research</strong> </p>
<p>
		915 118th Ave SE<br/>
		PO Box 96068 <br/>
		Bellevue, WA 98009-9668</p>
<p>Customer Service: <!--1-425-453-2121-->1-800-735-3503<br/>
		Email: <a href="mailto:info@ber.org?subject=Email%20from%20the%20website">info@ber.org</a><br/>
<!-- Office Hours -->
<span class="officeHours">Weekdays 5:30 a.m. – 4:00 p.m. PST</span>
</p>
</div>
<div class="sitelinks">
<p><strong>Site Links</strong></p>
<ul>
<!--<li><a href="/seminars/index.cfm">Seminars &amp; Conferences</a></li>-->
<li><a href="/seminars/index.cfm">Live Online PD Events</a></li>
<li><a href="/institute/index.cfm">Train-the-Trainer Events</a></li>
<!--<li><a href="/online/index.cfm">Online PD Courses</a></li>-->
<li><a href="/online/index.cfm">On-Demand Online Courses</a></li>
<li><a href="/onsite/index.cfm">On-Site PD</a></li>
<li><a href="/pdresourcekits/index.cfm">PD Resource Kits</a></li>
</ul>
</div>
<div class="info">
<p><strong>Information</strong></p>
<ul>
<li><a href="/about/index.cfm">About BER</a></li>
<li><a href="/about/contact.cfm">Contact Us</a></li>
<li><a href="/about/faq.cfm">FAQ</a></li>
<!--<li><a href="/seminars/directions.cfm">Event Directions</a></li>-->
<li><a href="/seminars/credit">CEUs &amp; University Credit</a></li>
<li><a href="/request-access/index.cfm">Live Online Event Access Request </a></li>
</ul>
</div>
<div class="social">
<p><strong>Connect With Us</strong></p>
<a href="/about/contact.cfm">
<div id="socfooter">
<div id="facebook">
<p><img align="absmiddle" alt="Facebook" border="none" height="17" src="/img/icons/footer/FB-f-Logo__blue_50.png" style="margin-right:5px;" width="17"/>Facebook</p>
</div>
<div id="youtube">
<p><img align="absmiddle" alt="YouTube" border="none" height="17" src="/img/icons/footer/icon-new-youtube_50.png" style="margin-right:5px;" width="17"/>YouTube</p>
</div>
<div id="twitter">
<p><img align="absmiddle" alt="Twittwr" border="none" height="17" src="/img/icons/footer/Twitter_logo_blue.png" style="margin-right:5px;" width="17"/>Twitter</p>
</div>
<div id="pinterest">
<p><img align="absmiddle" alt="Pinterest" border="none" height="17" src="/img/icons/footer/pinterest_badge_red_50.png" style="margin-right:5px;" width="17"/>Pinterest</p>
</div>
<div id="linkedin">
<p><img align="absmiddle" alt="LinkedIn" border="none" height="17" src="/img/icons/footer/LinkedIn_Logo50px.png" style="margin-right:5px;" width="17"/>LinkedIn</p>
</div>
</div><!-- /socfooter -->
</a>
</div><!-- /social -->
</div>
<link href="/js/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet"/>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<style>
.ui-dialog {z-index: 1002 !important;}
.ui-widget-overlay.modal-opened {background-color: #000; opacity: 0.75; filter: Alpha(Opacity=80);}
</style>
<div id="weatherModal" title="BER Inclement Weather">
<div id="inclementWeatherDiv"></div>
</div>
<script src="/js/weatherModal.js"></script>
<script src="/js/Ticker.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<script src="/js/tickerSetup.js" type="text/javascript"></script>
<!-- End Footer -->
</body>
</html>
