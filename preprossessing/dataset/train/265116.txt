<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="pl-pl" xml:lang="pl-pl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 - Artykułu nie znaleziono</title>
<link href="/templates/system/css/error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - Artykułu nie znaleziono</div>
<div id="errorboxbody">
<p><strong><strong>Możliwe, że nie możesz zobaczyć tej strony, ponieważ</strong></strong></p>
<ol>
<li>Użyta zakładka jest nieaktualna</li>
<li>Twoja wyszukiwarka <strong>nie odświeżyła jeszcze mapy naszej witryny</strong></li>
<li>Adres został wpisany z błędem</li>
<li>Nie masz uprawnień do obejrzenia tej strony</li>
<li>Joomla nie może zlokalizować wskazanego zasobu.</li>
<li>Wystąpił błąd podczas wykonywania powierzonego zadania.</li>
</ol>
<p><strong>Spróbuj jednej z następujących stron:</strong></p>
<ul>
<li><a href="/index.php" title="Przejdź na stronę startową">Strona startowa</a></li>
</ul>
<p>Jeśli problem się powtarza, skontaktuj się z administratorem witryny.</p>
<div id="techinfo">
<p>Artykułu nie znaleziono</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
