<!DOCTYPE html>
<html dir="ltr" lang="en" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
<head>
<meta charset="utf-8"/>
<meta content="Drupal 8 (https://www.drupal.org)" name="Generator"/>
<meta content="width" name="MobileOptimized"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/sites/default/files/favicon_0.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/node/1" rel="canonical"/>
<link href="/node/1" rel="shortlink"/>
<link href="/node/1" rel="revision"/>
<title>Bira 91 | Imagined in India, For the World | bira</title>
<link href="/core/themes/stable/css/system/components/ajax-progress.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/align.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/autocomplete-loading.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/fieldgroup.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/container-inline.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/clearfix.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/details.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/hidden.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/item-list.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/js.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/nowrap.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/position-container.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/progress.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/reset-appearance.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/resize.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/sticky-header.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/system-status-counter.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/system-status-report-counters.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/system-status-report-general-info.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/tabledrag.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/tablesort.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/core/themes/stable/css/system/components/tree-child.module.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/themes/bira/css/base/global.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/themes/bira/css/base/animate.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/themes/bira/css/base/jquery.bxslider.min.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/themes/bira/css/fonts/fonts.css?q4z0u9" media="all" rel="stylesheet"/>
<link href="/themes/bira/css/components/home/home.css?q4z0u9" media="all" rel="stylesheet"/>
<!--[if lte IE 8]>
<script src="/core/assets/vendor/html5shiv/html5shiv.min.js?v=3.7.3"></script>
<![endif]-->
</head>
<body class="bodyback">
<a class="visually-hidden focusable" href="#main-content">
      Skip to main content
    </a>
<div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas="">
<header id="header">
<div class="wrapper clearfix">
<div class="logo"><a href="/"><img alt="logo" src="/sites/default/files/bira-logo.png"/></a></div>
<div class="mainNav">
<div id="side_menu_controler">
<ul>
<li></li>
<li class="middle_li"></li>
<li></li>
</ul>
</div>
<nav class="nav" style="">
<div class="links">
<div class="accordion_container">
<div class="accordion_head"><span class="plusminus">-</span> Our beers</div><div class="accordion_body" style="display: block;"><ul class="list2"><li><a href="/beer">All beers</a></li><li><a href="/light-beer">Light</a></li><li><a href="/white-beer">White ale</a></li><li><a href="/strong-beer">Strong ale</a></li><li><a href="/blonde-beer">Blonde lager</a></li><li><a href="/ipa-beer">Ipa</a></li><li><a href="/boom-strong">Boom strong</a></li><li><a href="/boom-classic">Boom classic</a></li></ul><div class="imagesBeer">
<ul class="nav_details clearfix"><li><a href="/beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_allvariant.jpg"/></div>
<span class="imgDetail">All variants</span></a></li><li><a href="/light-beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_light.jpg"/></div>
<span class="imgDetail">Light lager</span></a></li><li><a href="/white-beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_whiteale.jpg"/></div>
<span class="imgDetail">White ale</span></a></li><li><a href="/strong-beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_strong.jpg"/></div>
<span class="imgDetail">Strong ale</span></a></li><li><a href="/blonde-beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-07/Blonde-tile-optimized.jpg"/></div>
<span class="imgDetail"></span></a></li><li><a href="/ipa-beer">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-07/IPA-tile-optimized.jpg"/></div>
<span class="imgDetail"></span></a></li><li><a href="/boom-strong">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-05/nav_boomstrong.jpg"/></div>
<span class="imgDetail">Boom strong</span></a></li><li><a href="/boom-classic">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-05/nav_boomclassic.jpg"/></div>
<span class="imgDetail">Boom classic</span></a></li></ul>
</div></div><div class="accordion_head"><span class="plusminus">+</span> Hot stuff</div><div class="accordion_body" style="display: none;"><ul class="list2"><li><a href="/hot-stuff">Hot stuff home</a></li><li><a href="/hot-stuff/food-festivals">Food festivals</a></li><li><a href="/hot-stuff/videos">Hot stuff videos</a></li><li><a href="/hot-stuff/hot-sauce">Hot sauce</a></li></ul><div class="imagesBeer">
<ul class="nav_details clearfix"><li><a href="/hot-stuff">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_hotstuff.jpg"/></div>
<span class="imgDetail">Home</span></a></li><li><a href="/hot-stuff/food-festivals">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_hotstuff_ff.jpg"/></div>
<span class="imgDetail">Food festivals</span></a></li><li><a href="/hot-stuff/videos">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_hotstuff_v.jpg"/></div>
<span class="imgDetail">Videos</span></a></li><li><a href="/hot-stuff/hot-sauce">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_hotstuff_hs.jpg"/></div>
<span class="imgDetail">Hot sauce</span></a></li></ul>
</div></div><div class="accordion_head"><span class="plusminus">+</span> Freeflow</div><div class="accordion_body" style="display: none;"><ul class="list2"><li><a href="/freeflow">Freeflow home</a></li><li><a href="/freeflow/videos">Video</a></li><li><a href="internal:/freeflow/gallery">Events</a></li><li><a href="/free-flow/music">Music</a></li></ul><div class="imagesBeer">
<ul class="nav_details clearfix"><li><a href="/freeflow">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/freeflow-h.jpg"/></div>
<span class="imgDetail">Home</span></a></li><li><a href="/freeflow/videos">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_freeflow_videos.jpg"/></div>
<span class="imgDetail">Videos</span></a></li><li><a href="internal:/freeflow/gallery">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_freeflow_events.jpg"/></div>
<span class="imgDetail">Events</span></a></li><li><a href="/free-flow/music">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_freeflow_music.jpg"/></div>
<span class="imgDetail">Music</span></a></li></ul>
</div></div><div class="accordion_head"><span class="plusminus">+</span> Life at bira 91</div><div class="accordion_body" style="display: none;"><ul class="list2"><li><a href="/about">About us</a></li><li><a href="/founder">Bira 91 squad</a></li><li><a href="/careers">Careers</a></li></ul><div class="imagesBeer">
<ul class="nav_details clearfix"><li><a href="/about">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_aboutus.jpg"/></div>
<span class="imgDetail">About us</span></a></li><li><a href="/founder">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_birasquad.jpg"/></div>
<span class="imgDetail">Bira 91 squad</span></a></li><li><a href="/careers">
<div class="imgbox"><img alt="img" src="https://bira91.com/sites/default/files/2019-02/nav_careers.jpg"/></div>
<span class="imgDetail">Careers</span></a></li></ul>
</div></div>
<ul class="list3"><li><a href="http://shop.bira91.com/">Shop</a></li><li><a href="/contact-us">Contact us</a></li><li><a href="/newsroom">Newsroom</a></li></ul>
</div>
<div class="social"> <a class="padding" href="https://www.facebook.com/bira91beer/" target="_blank"><img alt="fb_icon" src="/themes/bira/images/facebook.svg"/></a> <a class="padding" href="https://twitter.com/bira91" target="_blank"><img alt="twitter_icon" src="/themes/bira/images/twitter.svg"/></a> <a href="https://www.instagram.com/bira91beer/" target="_blank"><img alt="fb_icon" src="/themes/bira/images/insta.svg"/></a> <a href="https://www.youtube.com/channel/UCB6wPBNG-pg0YgMzSAOsAJg" target="_blank"><img alt="twitter_icon" src="/themes/bira/images/youtube.svg"/></a> </div>
</div>
</nav>
</div>
</div>
</header>
<div>
<article about="/node/1" data-history-node-id="1" role="article">
<div class="banner fadeInRight animated2 wow" id="slider1">
<div class="classy" id="sliderDk">
<ul class="bxslider dekstopSlider">
<li class="one"><a href="beer">
<h2>The Indian Pale Ale</h2>
<img alt="img" class="banner" src="/sites/default/files/2019-08/IPA-Hero-banner0-optimized.jpg"/> </a></li>
<li class="two"> <a href="http://shop.bira91.com" target="_blank">
<h2>BIRA91 SHOP</h2>
<img alt="img" class="banner" src="/sites/default/files/2019-10/Bira91-shop-orignal.jpg"/></a></li>
<li class="three"> <a href="https://www.youtube.com/watch?v=yZVbK2MoFXM">
<h2>MAKEPLAY</h2>
<img alt="img" class="banner" src="/sites/default/files/2019-08/slider1_img3.jpg"/></a></li>
</ul>
</div>
</div>
<section class="section1 "> <img class="fadeInUp wow imgCan desktop" src="/sites/default/files/2018-04/birawhiteImg.jpg"/> <img alt="img" class="fadeInUp wow imgCan mobile" src="/sites/default/files/2018-04/birawhiteImg-M.jpg"/>
<div class="text ">
<h2 class="animated fadeInUp wow animated2">THE DAWN OF A NEW WORLD NEVER TASTED BETTER.</h2>
<a class="button1 animated2 fadeInUp wow " href="/beer">Find out more <img src="/themes/bira/images/buttonArrow_black.png"/> </a> </div>
</section>
<section class="section2 ">
<ul class="list1 clearfix">
<li class=" imgCon fadeInUp wow ">
<div class="details ">
<h3 class="fadeInDown wow">APRIL FOOLS' FEST</h3>
<a class="button1 fadeInUp wow" href="https://www.bira91.com/AFF/aff-2019.html">Find out more <img src="/themes/bira/images/buttonArrow_white.png"/> </a>
</div>
<img alt="img" class="scrollableImg" src="/sites/default/files/2019-06/AFF_1.jpg"/>
</li>
<li class="imgCon animated2 fadeInUp wow ">
<div class="details ">
<h3 class=" fadeInDown wow animated2">BIRA 91 BOOM</h3>
<a class="button1 fadeInUp wow animated2" href="/boom-strong">Find out more <img src="/themes/bira/images/buttonArrow_white.png"/> </a>
</div>
<img alt="img" class="scrollableImg" src="/sites/default/files/2019-06/Boom-Strong-v2-min.jpg"/>
</li>
</ul>
<ul class="list1 clearfix margin0">
<li class="imgCon fadeInUp wow urbanDisc">
<div class="details">
<h3 class="animated fadeInDown wow animated2">FREEFLOW</h3>
<a class="button1 animated fadeInUp wow animated2" href="/freeflow">Find out more <img src="/themes/bira/images/buttonArrow_white.png"/> </a>
</div>
<img alt="img" class="scrollableImg" src="/sites/default/files/2019-06/img1_0.jpg"/>
</li>
<li class="imgCon animated2 fadeInUp wow biraLightLi">
<div class="details">
<div class="biralightlogo">
<img class="fadeInDown wow animated2" src="/sites/default/files/2019-06/hot-stuff_0.png"/>
</div>
<a class="button1 animated fadeInUp wow animated2" href="/hot-stuff">Find out more <img src="/themes/bira/images/buttonArrow_white.png"/> </a>
</div>
<img alt="img" class="scrollableImg" src="/sites/default/files/2019-06/img2_0.jpg"/>
</li>
</ul>
</section>
<section class="section3 shopBanner ">
<div class="wrapper"> <img alt="img" class="img1 slideInUp animated wow" src="/themes/bira/images/shop_bottle.png"/> <img alt="img" class="img2 slideInDown animated2 wow" src="/themes/bira/images/shop_monkey1.png"/> <img alt="img" class="img3 slideInDown animated3 wow" src="/themes/bira/images/shop_monkey2.png"/> <img alt="img" class="img4 slideInDown animated2 wow" src="/themes/bira/images/shop_monkeyCricle.png"/> <img alt="img" class="img5 slideInDown animated1 wow" src="/themes/bira/images/shop_keychain.png"/> <img alt="img" class="img6 slideInUp animated2 wow" src="/themes/bira/images/shop_tshirt.png"/>
<div class="text post ">
<div class="fadeInDown wow shopLogo"><img src="/themes/bira/images/birashop_logo.png"/></div>
<p class="fadeInDown wow">OUR BREWS THAT AREN’T BEER </p>
<a class="button1 fadeInUp wow" href="http://shop.bira91.com/" target="_blank">Buy Now <img src="/themes/bira/images/buttonArrow_black.png"/> </a> </div>
</div>
</section>
<section class="section4 ">
<video autoplay="" class="desktop" loop="" muted="">
<source src="/sites/default/files/2018-08/Beer.mp4" type="video/mp4">
</source></video>
<img class="mobile" src="/sites/default/files/2018-04/video.gif"/>
<div class="text post ">
<h2 class="fadeInDown wow">OUR BEERS</h2>
<p class=" fadeInDown wow animated2">FLAVOURFUL BEERS FOR THE NEW WORLD.</p>
<a class="button1 fadeInUp wow animated2" href="/beer">Explore <img src="/themes/bira/images/buttonArrow_white.png"/> </a> </div>
</section>
<!--<section class="section5 fadeInUp wow">
      <p><img src="common/images/insta_img1.jpg" alt="img"/></p>
      <div class="bxslider2outer" id="instaSlider">
    <ul class="bxslider2">
          <li><img src="common/images/insta_img2.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img3.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img2.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img3.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img2.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img3.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img2.jpg" alt="img"/></li>
          <li><img src="common/images/insta_img3.jpg" alt="img"/></li>
        </ul>
    <a class="button1" href="#"> More on Instagram <img src="common/images/insta_arrow.jpg" alt="insta"/> </a> </div>
    </section>-->
<section class="section6">
<div class="wrapper post">
<h2 class="tac fadeInDown wow">STAY UPDATED WITH WHAT'S FROTHING AT OUR END.</h2>
<div class="clearfix form fadeInUp wow animated2">
<div class="inputBox "> <span class="label"> Email Address </span>
<input class="input1" id="emaildata" type="textbox"/>
</div>
<a class="arrow" href="javascript:void(0);" id="emailsubmit"> <img alt="imgd" class="imgArrow" src="/themes/bira/images/arrow.jpg"/> </a> </div>
<div class="formMessage"> <span id="successmessage"></span> </div>
<div class="formMessage"> <span id="errormessage"></span> </div>
<div class="progressbar" style="display:none"><img src="/themes/bira/images/bar.gif"/></div>
</div>
</section>
</article>
</div>
<footer id="footer">
<div class="wrapper clearfix">
<div class="clearfix bottomClas">
<div class="logoFooter"> <img alt="logo" src="/themes/bira/images/monekyLogo.svg"/> </div>
<div class="fivecolumnLayout clearfix">
<div class="column"><h4><a href="">Our beers</a></h4><a href="/light-beer">Light</a><a href="/white-beer">White ale</a><a href="/strong-beer">Strong ale</a><a href="/blonde-beer">Blonde lager</a><a href="/ipa-beer">Ipa</a><a href="/boom-strong">Boom strong</a><a href="/boom-classic">Boom classic</a></div><div class="column"><h4><a href="">Hot stuff</a></h4><a href="/hot-stuff/hot-sauce">Hot sauce</a><a href="/hot-stuff/videos">Hot stuff videos</a><a href="/hot-stuff/food-festivals">Food festivals</a></div><div class="column"><h4><a href="">Freeflow</a></h4><a href="/freeflow/videos">Videos</a><a href="/freeflow/gallery">Events</a><a href="/free-flow/music">Music</a></div><div class="column"><h4><a href="">Life at bira 91</a></h4><a href="/about">About us</a><a href="/founder">Bira 91 squad</a><a href="/careers">Careers</a></div>
<div class="column"> <a href="http://shop.bira91.com/"><span class="bigText">Shop</span></a><a href="/newsroom"><span class="bigText">Newsroom</span></a><a href="/contact-us"><span class="bigText">Contact us</span></a></div>
</div>
</div>
<div class="footercopy">
<p class="tac copy">PLEASE DRINK RESPONSIBLY. © 2019 B9 BEVERAGES PVT. LTD. ALL RIGHTS RESERVED.</p>
</div>
<p class="tac"><img alt="img" src="/themes/bira/images/footerBira91Logo.jpg"/></p>
</div>
</footer>
</div>
<script data-drupal-selector="drupal-settings-json" type="application/json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"node\/1","currentPathIsAdmin":false,"isFront":true,"currentLanguage":"en"},"pluralDelimiter":"\u0003","ajaxPageState":{"libraries":"bira\/global-styling,bira\/home-page,core\/html5shiv,system\/base","theme":"bira","theme_token":null},"ajaxTrustedUrl":[],"user":{"uid":0,"permissionsHash":"7e79b9179412c482164f5ddd6338aa412a560605c8f45a7e165886449986eef6"}}</script>
<script src="/core/assets/vendor/domready/ready.min.js?v=1.0.8"></script>
<script src="/?v=3.2.1"></script>
<script src="/core/assets/vendor/jquery-once/jquery.once.min.js?v=2.2.0"></script>
<script src="/core/misc/drupalSettingsLoader.js?v=8.5.1"></script>
<script src="/core/misc/drupal.js?v=8.5.1"></script>
<script src="/core/misc/drupal.init.js?v=8.5.1"></script>
<script src="/core/misc/progress.js?v=8.5.1"></script>
<script src="/core/misc/ajax.js?v=8.5.1"></script>
<script src="/themes/bira/js/jquery-3.1.1.min.js?v=8.5.1"></script>
<script src="/themes/bira/js/wow.js?v=8.5.1"></script>
<script src="/themes/bira/js/jquery.bxslider.min.js?v=8.5.1"></script>
<script src="/themes/bira/js/nav.js?v=8.5.1"></script>
<script src="/themes/bira/js/home/home.js?v=8.5.1"></script>
</body>
</html>
