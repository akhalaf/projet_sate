<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "18488",
      cRay: "611685ed5fa41975",
      cHash: "5defee60023990c",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cud2VmdW5kZmxpcHMuY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "KNC5lnhMbTFp4JHEGmUHcCllFz6jsMjbLHi8kH6YGyzvecJVjPSyU/essXM+CrQL9XMmPWTIxump0DD/qMK+W5QIzeY6n+JmhHI4TI5XRr7b79oDzlcarZqteUF05jXpl7mAO8O7kJJkwigSuIZNpRd1RcFn4mrOskspJn4QypovNg+rTixM/yID75hhTw7W+Y7S3f6l5RmBxCZlCbUKJdo3BwyX04i5XuSCg8Ztsavp7B7c9K6kBTfXiZeH5cjvqhNW7r7RLsa2o7xmY6kyLfdimFOsx4Y40bLVvxinydM4Mk+CRAbhMEkh7l59OpWLR1HQ8LGQgaoxpB5NsKOUPDensqBc5+rwl98mjmftZtgR7Yt0BDYlJDRhwK9PuI77gK0GXntqRYUqW6nYqZrOeKuklAxsqE7ik+VcM8Xvj8p0WjD3zqknN0tS3dKSE7myJha0T2wlr0HGwfw+TzbXCG/pGrurgnwY1tmszk3zuAXcti8Z3a48UlIyxlyIGUpIyFHrE219i6vq92yWXzmssodOIsrgmGSt0SmMYuXmRQuzHO8A5S8dEzER9VEAD1hFa+DO0U9jWBL7SK8e0fTrX1yR+H+71sAqj7fKVFaVC0vAyQq+sSf9En5IAxNaonVqfVyyDNaki61JcjTXcOU+5e1OTCo3BbLrlGveecBsLG5ANe0d4Onw9LomNsFyrNujTYKM6eH+Cq9+bcNWvV+fHSh8zdlY2Z55ztzG3rtbdGZilqbKZ1qqzrvMkrflQlmy2yCoFWU/aaZkb7sAwfRsES7ryxktafji2zsjGcq1wQY=",
        t: "MTYxMDYxODU2NC43MTAwMDA=",
        m: "SHI2WMT7smgD9W5Oo29LLbEF4C9vC6TBt5FFJBAZGuQ=",
        i1: "JjjCGt4L++OngISvVVAw1Q==",
        i2: "YjqdEdCDEblu9bn03q4ELw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "wpwWg8tFIogr+ociQFSs9Qw/qu8/b3dTy1QY3dZ9H1c=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=611685ed5fa41975");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> www.wefundflips.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=d18c82285999a01a93017ac4d0324678f1f5dd51-1610618564-0-ASv31aUV7EVnk1tQdMJxOSD__SUUTAc_s0QkT5kLVNFD2h9nZV80J_F4IhgvVZjkmNMhJZcva98PUnVpGttvDDeRM4pmWAvhztgsEmuwOjTxZlcaowNA7AsYApHoQNnS_cuyxaYp9tFSnKPNc4PTtjM-on3S9hzq1j-OGi9kZFglkwHBjFxUWXHbxm9tysjePOgO7wISc3az7Aa46-ckcAak3FBh1hJ-kt4iH2QsKCtud4rQf5E6ZVaw05UCojYtjA9kKJM__C3_eiRId3q8-g27a8PuwvUGRW24OyLRfLUBu-UX_CEw7-qYUqUBUno_afcxZ4skI6Q9xMaRRpknrLjHd7gds2fOuZWz7X1NKoSq1W4dabXkfonN6n8TJE1U2A" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="a90fd7e8db49d8f157e19ef9ee37419aa594a069-1610618564-0-AQ2X/avqsTlUl1MvwXNIp6WemOLFtYYAzOySYEgq4UaZ1CwQ7MBR6SZnU4pyVhp9ndkv5ewHVWj5xy2NUnjpk+BJqsBO8vVc3J2/P4GnyHOd/HXbaR3KAVYESNwU3+yeVD6KPHLFiNak06eQd/fQLV8KHjAV11UqrOwtclu3HpBME+YxUFZzUG/7PijZMa/glJjJwzQa48ndaxKvYYTtdVqMPk52BsKWncphcpq6XD10PET4JVXPetw8Fw8VEyraVZR0YasllVd57RsTPO5BR+AOIgJnW7OtJx8XjDnKe1FuaTuno1LkMmUhYYbL9WLvr1dwRyVaAeo4QORe+5PcECTIWuSjacYleb9g3Q7MkIInLJ7wPzKmI0p+aPjIwJUynGsi6RoVUWCPobBIRn4Qe1J4dXzJU9A7z1LXdfH87ZCPRMSZUYjMSBycAQnZ2HRkcxt/JFttYTQA4Gd/KaNNbQAufYxykoQ7dmPbXvITSX2tLBKHv0SaopFLim3/R48JOnTpppZk38nae2XAl6jDnjvHHB/85pfdxIQUcKVEUHFtpg2aljBqG5p+yWk6N9YnUdL3ro6aVLpLMBAsjZKuRw5Ls4Ng1z/tonW3vFa3TTRNHhavjkU1woF9PnV23nS/Mj1lEHAgEE7lyjxrfozxSHa7Ic9k6U0CYJyq6zEmGKPyoLPO8V1LWA8D0zKX6UV3hOeHKT1V3BQ5g9wroWTTX6zSquXcKcoc8VQL7N/UPb6zqZkR3FihdjGZcHeywN4i8u3MHIQKLWzAGQGFj+y8ft42mYVCmeFRw6LYyV9oUq1iHkeM4TL2oOasgcepYT+xbFcIYNscg+S2il0EFG1HN1djr2V9H+vBr6XZL/L0Qc0NEUgkMmHJOPV0SRygi/iNJeq7sPSIX9l0rIwKvQ2gOo5f3gGYshHydlDKQzP3ZdPmBLhyk8ITl1mwgECHe7cFFnmlVQ4g6ai/eyeHkaGYKwgHW2MBmy1fwE67zP9oMrtRz6Hw//A3MDs1fn3X71ZG2SQwwIp6h9q7enFftvEvCO48WDNwuFIWjak/WQGf6dCiT2HpbG54AlWAnLLLfh4uufnpqWoiq2k+Gu916aYMK7aW/H+DINH5XtVW0MKadjdb+jWj0urZ/mND73H0HDWNP4XFqmk+zv1bU9NCYUhMmPY7KWV3rdZt4345uyaP+GNLmcc++3FwZaSc78ZUxVRpL/5+f5iDPjkODc1KWlsm9LYxQH+9eYUtcbYLcolmAVIZqz+3xZDtwPtUEhAc7hq4t4W6U98/JcWn8fLdFEKHeAzWw+gRl91QJWwBlj7Z9Nwa+/KEgC5lYZsEyBVkdyIqm7KowSbVR8LPheLQaFVN7Xs="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="01739639b5c29bf7ef9231debf2dddaf"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610618568.71-d0cleEN8Sy"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=611685ed5fa41975')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>611685ed5fa41975</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
