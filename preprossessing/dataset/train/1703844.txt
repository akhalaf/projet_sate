<!DOCTYPE html>
<html lang="tr">
<head>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="320" name="MobileOptimized"/>
<meta content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0" name="viewport"/>
<link href="https://www.yetigames.net/wp-content/uploads/2020/10/favicon.png" rel="shortcut icon"/> <link href="https://www.yetigames.net/xmlrpc.php" rel="pingback"/>
<title>Sayfa bulunamadı – Yeti Games</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.yetigames.net/feed/" rel="alternate" title="Yeti Games » beslemesi" type="application/rss+xml"/>
<link href="https://www.yetigames.net/comments/feed/" rel="alternate" title="Yeti Games » yorum beslemesi" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.yetigames.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.yetigames.net/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.23" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/pix-icons.css?ver=3.4.3" id="composer-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/bootstrap.min.css?ver=3.1.1" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/animate.min.css?ver=3.4.3" id="composer-animate-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/main.css?ver=3.4.3" id="composer-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/bbpress.css?ver=1.0" id="bbpress-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/plugins.css?ver=3.4.3" id="composer-plugins-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/uploads/composer/custom.css?ver=1610377323" id="composer-custom-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/themes/composer/_css/responsive.css?ver=3.4.3" id="composer-responsive-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<style id="composer-responsive-stylesheet-inline-css" type="text/css">
#sub-header, .composer-header-dark #sub-header {
            
        }
        #sub-header .sub-banner-title, .banner-header .sub-banner-title, .breadcrumb li a, .breadcrumb li span, #sub-header .current {
            
        }
        #sub-header .pattern {
            
        }
        body, #wrapper {
            background-color:#fff;
        }
        #wrapper {
            background-color:#ffffff !important;
        }
</style>
<link href="//fonts.googleapis.com/css?family=Raleway%3A300%2C400%2C400italic%2C700%2C700italic%7CPoppins%3A300%2C400%2C500%2C600%2C700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A100%7CPoppins%3A100%7CPoppins%3A700%7CPoppins%3A100%7CPoppins%3A700%7CRoboto%3Aregular%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A100%7CPoppins%3A700%7CPoppins%3A700%7CPoppins%3A100%7CPoppins%3A700%7CPoppins%3A600%7CPoppins%3A900%7CPoppins%3A700%7CPoppins%3A100%7CPoppins%3A700%26subset%3Dlatin&amp;ver=1.0.0" id="pix_theme_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yetigames.net/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=3.19.6" id="bsf-Defaults-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.yetigames.net/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.yetigames.net/wp-content/plugins/enable-jquery-migrate-helper/js/jquery-migrate-1.4.1-wp.js?ver=1.4.1-wp" type="text/javascript"></script>
<script id="tp-tools-js" src="https://www.yetigames.net/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.2.23" type="text/javascript"></script>
<script id="revmin-js" src="https://www.yetigames.net/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.23" type="text/javascript"></script>
<script id="modernizr-js" src="https://www.yetigames.net/wp-content/themes/composer/_js/libs/modernizr.custom.min.js?ver=2.5.3" type="text/javascript"></script>
<link href="https://www.yetigames.net/wp-json/" rel="https://api.w.org/"/><link href="https://www.yetigames.net/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.yetigames.net/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<meta content="Powered by Slider Revolution 6.2.23 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript><style id="yellow-pencil">
/*
	The following CSS generated by YellowPencil Plugin.
	https://yellowpencil.waspthemes.com
*/
.main-nav .menu > .menu-item > .external{margin-left:15px !important;margin-right:15px;}
</style></head>
<body class="error404 wp-custom-logo seperate-mobile-nav composer-wide full-header wpb-js-composer js-comp-ver-6.4.1 vc_responsive">
<div class="mobile-menu-nav menu-dark "><div class="mobile-menu-inner">
<ul class="menu clearfix" id="menu-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10108 pix-submenu external" id="menu-item-10108"><a class="external" data-scroll="true" href="http://yetigames.net">Anasayfa</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10107 pix-submenu external" id="menu-item-10107"><a class="external" data-scroll="true" href="https://www.yetigames.net/hakkimizda/">Hakkımızda</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10091 pix-submenu external" id="menu-item-10091"><a class="external" data-scroll="true" href="https://www.yetigames.net/oyunlar/">Oyunlar</a><span class="pix-dropdown-arrow"></span>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10038 external" id="menu-item-10038"><a class="external" data-scroll="true" href="https://www.yetigames.net/secim-oyunu-2/">Seçim Oyunu 2</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10274 external" id="menu-item-10274"><a class="external" data-scroll="true" href="https://www.yetigames.net/yerel-secim-oyunu/">Yerel Seçim Oyunu – İstanbul</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10037 external" id="menu-item-10037"><a class="external" data-scroll="true" href="https://www.yetigames.net/secim-oyunu-partiler-yarisiyor/">Seçim Oyunu – Partiler Yarışıyor</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10275 external" id="menu-item-10275"><a class="external" data-scroll="true" href="https://www.yetigames.net/poco/">POCO</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10276 external" id="menu-item-10276"><a class="external" data-scroll="true" href="https://www.yetigames.net/hayrettin/">Hayrettin</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10289 external" id="menu-item-10289"><a class="external" data-scroll="true" href="https://www.yetigames.net/pets-planes/">Pets &amp; Planes</a><span class="pix-dropdown-arrow"></span></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10120 pix-submenu external" id="menu-item-10120"><a class="external" data-scroll="true" href="https://www.yetigames.net/iletisim/">İletişim</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-10669 pix-submenu external" id="menu-item-10669"><a class="external" data-scroll="true" href="https://www.yetigames.net/blog/">Blog</a><span class="pix-dropdown-arrow"></span></li>
</ul> </div></div>
<div id="content-pusher">
<p class=" hide-on-mobile" id="back-top"><a href="#top"><span class="pixicon-arrow-angle-up"></span></a></p> <div class="header-wrap solid-color-bg header-line-no sub-menu-dark ">
<div class="header-con sticky-light pix-sticky-header pix-sticky-header-scroll-up menu-header-3 menu-dark ">
<div class="pix-menu-align-center">
<header class="header">
<div class="container">
<div class="wrap clearfix" id="inner-header">
<div class="sticky-logo-yes" id="logo"><a class="mobile-logo-yes" href="https://www.yetigames.net/" itemprop="url" rel="home"><img alt="Yeti Games" class="dark-logo" data-rjs="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo2.png" src="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo_s-1.png"/><img alt="Yeti Games" class="light-logo" data-rjs="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo_s2.png" src="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo_sw-1.png"/><img alt="Yeti Games" class="sticky-logo" data-rjs="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo2.png" src="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo_s-1.png"/><img alt="Yeti Games" class="mobile-res-logo" src="https://www.yetigames.net/wp-content/uploads/2020/10/yeti-game-studio_logo_s-1.png"/></a></div>
<div class="pix-menu">
<div class="pix-menu-trigger">
<span class="mobile-menu">Menu</span>
</div>
</div>
<div class="widget-right">
</div>
<nav class="main-nav">
<ul class="menu clearfix" id="menu-menu-1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10108 pix-submenu external"><a class="external" data-scroll="true" href="http://yetigames.net">Anasayfa</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10107 pix-submenu external"><a class="external" data-scroll="true" href="https://www.yetigames.net/hakkimizda/">Hakkımızda</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10091 pix-submenu external"><a class="external" data-scroll="true" href="https://www.yetigames.net/oyunlar/">Oyunlar</a><span class="pix-dropdown-arrow"></span>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10038 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/secim-oyunu-2/">Seçim Oyunu 2</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10274 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/yerel-secim-oyunu/">Yerel Seçim Oyunu – İstanbul</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10037 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/secim-oyunu-partiler-yarisiyor/">Seçim Oyunu – Partiler Yarışıyor</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10275 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/poco/">POCO</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10276 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/hayrettin/">Hayrettin</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10289 external"><a class="external" data-scroll="true" href="https://www.yetigames.net/pets-planes/">Pets &amp; Planes</a><span class="pix-dropdown-arrow"></span></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10120 pix-submenu external"><a class="external" data-scroll="true" href="https://www.yetigames.net/iletisim/">İletişim</a><span class="pix-dropdown-arrow"></span></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-10669 pix-submenu external"><a class="external" data-scroll="true" href="https://www.yetigames.net/blog/">Blog</a><span class="pix-dropdown-arrow"></span></li>
</ul> </nav>
</div>
</div>
</header></div> </div>
</div>
<div class="clearfix" id="main-wrapper">
<div data-ajaxtransin="fadeInUp" data-ajaxtransout="fadeOutDown" data-preloadtrans="fadeInUp" id="wrapper">
<div class=" container boxed">
<div class="row">
<div class="col-md-12">
<div id="errorCon">
<p class=""><img alt="" src="https://www.yetigames.net/wp-content/themes/composer/_images/404.png"/></p><h3 class="error-text">Page Not Found</h3><p class="emphasis">Sorry, but the page you were looking for cannot be found. Please inform us about this error.</p><section class="search"><form action="https://www.yetigames.net/" class="searchform" method="get"><input class="s" name="s" placeholder="Search" type="text" value=""/>
<button class="searchsubmit" type="submit"></button>
</form><p></p></section>
</div>
</div>
</div>
</div>
</div> <!-- End of Wrapper -->
</div> <!-- End of Main Wrap -->
<footer class=" footer-dark " id="footer">
<!-- Copyright -->
<div class="footer-bottom">
<div class="container">
<div class="copyright row">
<div class="col-md-12"><div class="header-elem"><p class="copyright-text">© Yeti Games, All Rights Reserved.</p></div></div> </div>
</div>
</div>
</footer>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.yetigames.net\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.yetigames.net/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2" type="text/javascript"></script>
<script id="waypoints-js" src="https://www.yetigames.net/wp-content/themes/composer/_js/waypoints.min.js?ver=2.0.4" type="text/javascript"></script>
<script id="composer-plugins-js-js-extra" type="text/javascript">
/* <![CDATA[ */
var pix_composer = {"rootUrl":"https:\/\/www.yetigames.net\/","ajaxurl":"https:\/\/www.yetigames.net\/wp-admin\/admin-ajax.php","rtl":"false"};
/* ]]> */
</script>
<script id="composer-plugins-js-js" src="https://www.yetigames.net/wp-content/themes/composer/_js/plugins.js?ver=3.4.3" type="text/javascript"></script>
<script id="composer-js-js" src="https://www.yetigames.net/wp-content/themes/composer/_js/scripts.js?ver=3.4.3" type="text/javascript"></script>
<script id="like-me-scripts-js-extra" type="text/javascript">
/* <![CDATA[ */
var pixLike = {"ajaxurl":"https:\/\/www.yetigames.net\/wp-admin\/admin-ajax.php","liked":"You already liked this!"};
/* ]]> */
</script>
<script id="like-me-scripts-js" src="https://www.yetigames.net/wp-content/themes/composer/framework/extras/composer-like-me/js/like-me.js?ver=2.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.yetigames.net/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
