<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Page not found | Feeling Conduite</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://feelingconduite.com/feed/" rel="alternate" title="Feeling Conduite » Flux" type="application/rss+xml"/>
<link href="https://feelingconduite.com/comments/feed/" rel="alternate" title="Feeling Conduite » Flux des commentaires" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/feelingconduite.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=d2e0522d1ee79a9974380d2329a4d7f7"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://feelingconduite.com/wp-includes/css/dist/block-library/style.min.css?ver=d2e0522d1ee79a9974380d2329a4d7f7" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-includes/css/dist/block-library/theme.min.css?ver=d2e0522d1ee79a9974380d2329a4d7f7" id="wp-block-library-theme-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-content/uploads/sites/349/tablepress-combined.min.css?ver=10" id="tablepress-default-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-content/plugins/bb-plugin/fonts/fontawesome/5.14.0/css/all.min.css?ver=2.4.0.4" id="font-awesome-5-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-content/plugins/bb-plugin/css/jquery.magnificpopup.min.css?ver=2.4.0.4" id="jquery-magnificpopup-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-content/themes/bb-theme/css/bootstrap.min.css?ver=1.7.7" id="bootstrap-css" media="all" rel="stylesheet"/>
<link href="https://feelingconduite.com/wp-content/uploads/sites/349/bb-theme/skin-5f58d2de8e014.css?ver=1.7.7" id="fl-automator-skin-css" media="all" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Josefin+Sans%3A300%2C400%2C700%2C400%7CVidaloka%3A400&amp;ver=d2e0522d1ee79a9974380d2329a4d7f7" id="fl-builder-google-fonts-d42c2e3af3d2f640abca15186a7d318b-css" media="all" rel="stylesheet"/>
<script id="jquery-core-js" src="https://feelingconduite.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1"></script>
<script id="jquery-migrate-js" src="https://feelingconduite.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2"></script>
<script id="ai-js-js-extra">
var MyAjax = {"ajaxurl":"https:\/\/feelingconduite.com\/wp-admin\/admin-ajax.php","security":"b3574a8216"};
</script>
<script id="ai-js-js" src="https://feelingconduite.com/wp-content/plugins/advanced-iframe/js/ai.js?ver=692061"></script>
<script id="imagesloaded-js" src="https://feelingconduite.com/wp-includes/js/imagesloaded.min.js?ver=d2e0522d1ee79a9974380d2329a4d7f7"></script>
<link href="https://feelingconduite.com/wp-json/" rel="https://api.w.org/"/><link href="https://feelingconduite.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://feelingconduite.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<!-- SEO meta tags powered by SmartCrawl https://premium.wpmudev.org/project/smartcrawl-wordpress-seo/ -->
<!-- /SEO -->
<script type="text/javascript">
            var jQueryMigrateHelperHasSentDowngrade = false;

			window.onerror = function( msg, url, line, col, error ) {
				// Break out early, do not processing if a downgrade reqeust was already sent.
				if ( jQueryMigrateHelperHasSentDowngrade ) {
					return true;
                }

				var xhr = new XMLHttpRequest();
				var nonce = 'bcc8aa8761';
				var jQueryFunctions = [
					'andSelf',
					'browser',
					'live',
					'boxModel',
					'support.boxModel',
					'size',
					'swap',
					'clean',
					'sub',
                ];
				var match_pattern = /\)\.(.+?) is not a function/;
                var erroredFunction = msg.match( match_pattern );

                // If there was no matching functions, do not try to downgrade.
                if ( typeof erroredFunction !== 'object' || typeof erroredFunction[1] === "undefined" || -1 === jQueryFunctions.indexOf( erroredFunction[1] ) ) {
                    return true;
                }

                // Set that we've now attempted a downgrade request.
                jQueryMigrateHelperHasSentDowngrade = true;

				xhr.open( 'POST', 'https://feelingconduite.com/wp-admin/admin-ajax.php' );
				xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
				xhr.onload = function () {
					var response,
                        reload = false;

					if ( 200 === xhr.status ) {
                        try {
                        	response = JSON.parse( xhr.response );

                        	reload = response.data.reload;
                        } catch ( e ) {
                        	reload = false;
                        }
                    }

					// Automatically reload the page if a deprecation caused an automatic downgrade, ensure visitors get the best possible experience.
					if ( reload ) {
						location.reload();
                    }
				};

				xhr.send( encodeURI( 'action=jquery-migrate-downgrade-version&_wpnonce=' + nonce ) );

				// Suppress error alerts in older browsers
				return true;
			}
        </script>
<!-- Google Analytics tracking code output by Beehive Analytics Pro -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-88128226-1&amp;l=beehiveDataLayer"></script>
<script>
						window.beehiveDataLayer = window.beehiveDataLayer || [];
			function beehive_ga() {beehiveDataLayer.push(arguments);}
			beehive_ga('js', new Date())
						beehive_ga('config', 'UA-88128226-1', {
				'anonymize_ip': true,
				'allow_google_signals': false,
			})
					</script>
<style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <style id="wp-custom-css">
			

/*
CSS Migrated from BB theme:
*/

.fl-page-header-text h4{
    margin:0;padding:0;
}
.fl-social-icons .fl-icon {
    font-size: 36px;
    height: 32px;
    line-height: 32px;
    width: 32px;
}
.desactive .fl-pricing-table .fl-button {
    display: none;
}
.container , .fl-page-header-container{
    max-width:100%!important;
}
.fl-page-nav-col, .fl-page-nav, .fl-page-fixed-nav-wrap {
    padding-left: 0;
}
.fl-page-nav {
      padding-top: 20px;
}		</style>
<style type="text/css">
		#wpadminbar, #wpadminbar .menupop .ab-sub-wrapper { background: #498fcc;}
                                        #wpadminbar a.ab-item, #wpadminbar>#wp-toolbar span.ab-label, #wpadminbar>#wp-toolbar span.noticon { color: #ffffff }
                                        #wpadminbar .ab-top-menu>li>.ab-item:focus, #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus, #wpadminbar .ab-top-menu>li:hover>.ab-item, #wpadminbar .ab-top-menu>li.hover>.ab-item, #wpadminbar .quicklinks .menupop ul li a:focus, #wpadminbar .quicklinks .menupop ul li a:focus strong, #wpadminbar .quicklinks .menupop ul li a:hover, #wpadminbar-nojs .ab-top-menu>li.menupop:hover>.ab-item, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item, #wpadminbar .quicklinks .menupop ul li a:hover strong, #wpadminbar .quicklinks .menupop.hover ul li a:focus, #wpadminbar .quicklinks .menupop.hover ul li a:hover, #wpadminbar li .ab-item:focus:before, #wpadminbar li a:focus .ab-icon:before, #wpadminbar li.hover .ab-icon:before, #wpadminbar li.hover .ab-item:before, #wpadminbar li:hover #adminbarsearch:before, #wpadminbar li:hover .ab-icon:before, #wpadminbar li:hover .ab-item:before, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:focus, #wpadminbar.nojs .quicklinks .menupop:hover ul li a:hover, #wpadminbar li:hover .ab-item:after, #wpadminbar>#wp-toolbar a:focus span.ab-label, #wpadminbar>#wp-toolbar li.hover span.ab-label, #wpadminbar>#wp-toolbar li:hover span.ab-label { color: #1c1c1c }

                                        .quicklinks li.wpshape_site_title { width: 200px !important; }
                                        .quicklinks li.wpshape_site_title a{ outline:none; border:none;
                                                                                 }

                                        #wpadminbar .ab-top-menu>li>.ab-item:focus, #wpadminbar-nojs .ab-top-menu>li.menupop:hover>.ab-item, #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus, #wpadminbar .ab-top-menu>li:hover>.ab-item, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item, #wpadminbar .ab-top-menu>li.hover>.ab-item { background: none }
                                        #wpadminbar .quicklinks .menupop ul li a, #wpadminbar .quicklinks .menupop ul li a strong, #wpadminbar .quicklinks .menupop.hover ul li a, #wpadminbar.nojs .quicklinks .menupop:hover ul li a { color: #ffffff; font-size:13px !important }
                                        #wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a img {	width: 20px; height: 20px; border-radius: 100px; -moz-border-radius: 100px; -webkit-border-radius: 100px; 	border: none; }
		</style>
<link href="https://feelingconduite.com/wp-content/themes/bb-theme-child/style.css" rel="stylesheet"/></head>
<body class="error404 fl-framework-bootstrap fl-preset-bold fl-full-width fl-fixed-header fl-scroll-to-top" itemscope="itemscope" itemtype="https://schema.org/WebPage">
<a aria-label="Aller au contenu" class="fl-screen-reader-text" href="#fl-main-content">Aller au contenu</a><div class="fl-page">
<header class="fl-page-header fl-page-header-primary fl-page-nav-right fl-page-nav-toggle-button fl-page-nav-toggle-visible-mobile" itemscope="itemscope" itemtype="https://schema.org/WPHeader" role="banner">
<div class="fl-page-header-wrap">
<div class="fl-page-header-container container">
<div class="fl-page-header-row row">
<div class="col-sm-12 col-md-4 fl-page-header-logo-col">
<div class="fl-page-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">
<a href="https://feelingconduite.com/" itemprop="url"><img alt="Feeling Conduite" class="fl-logo-img" data-no-lazy="1" data-retina="" itemscope="" itemtype="https://schema.org/ImageObject" loading="false" src="https://feelingconduite.com/wp-content/uploads/sites/349/2017/07/header-2.png" title=""/><meta content="Feeling Conduite" itemprop="name"/></a>
</div>
</div>
<div class="col-sm-12 col-md-8 fl-page-nav-col">
<div class="fl-page-nav-wrap">
<nav aria-label="Menu de l'en-tête" class="fl-page-nav fl-nav navbar navbar-default navbar-expand-md" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" role="navigation">
<button class="navbar-toggle navbar-toggler" data-target=".fl-page-nav-collapse" data-toggle="collapse" type="button">
<span>Menu</span>
</button>
<div class="fl-page-nav-collapse collapse navbar-collapse">
<ul class="nav navbar-nav navbar-right menu fl-theme-menu" id="menu-pincipal"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-4736 nav-item" id="menu-item-4736"><a class="nav-link" href="https://feelingconduite.com">Accueil</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5049 nav-item" id="menu-item-5049"><a class="nav-link" href="#">Inscriptions</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4738 nav-item" id="menu-item-4738"><a class="nav-link" href="https://feelingconduite.com/pieces-a-fournir-2/">Pièces à fournir</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4972 nav-item" id="menu-item-4972"><a class="nav-link" href="https://feelingconduite.com/reglement-interieur/">Règlement intérieur</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4969 nav-item" id="menu-item-4969"><a class="nav-link" href="https://feelingconduite.com/mode-de-financement/">Mode de Financement</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4737 nav-item" id="menu-item-4737"><a class="nav-link" href="#">Nos Formations</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5014 nav-item" id="menu-item-5014"><a class="nav-link" href="https://feelingconduite.com/code-de-la-route/">Code de la Route</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5042 nav-item" id="menu-item-5042"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/2020-programme-et-horaire-code.pdf" rel="noopener" target="_blank">Les horaires du code théorique</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5043 nav-item" id="menu-item-5043"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/baracuda1.1_EVALUATION-DE-DEPART-BV-MANUELLE1.pdf" rel="noopener" target="_blank">le procédé de l’évaluation</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5031 nav-item" id="menu-item-5031"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/2.2-les-enjeux-Déroulement-Examen.pdf" rel="noopener" target="_blank">Les enjeux de la formation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4844 nav-item" id="menu-item-4844"><a class="nav-link" href="https://feelingconduite.com/permis-b/">Permis B</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4954 nav-item" id="menu-item-4954"><a class="nav-link" href="https://jemelabellise.fr/wp-content/uploads/sites/537/2018/06/1.1_EVALUATION-DE-DEPART-star-ae-v3.pdf" rel="noopener" target="_blank">Le Programme</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5029 nav-item" id="menu-item-5029"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/3.1_Parcours_d-_un_candidat_B.pdf" rel="noopener" target="_blank">Le parcours et le déroulement de la formation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4843 nav-item" id="menu-item-4843"><a class="nav-link" href="https://feelingconduite.com/conduite-accompagnee-aac/">Conduite accompagnée (AAC)</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4955 nav-item" id="menu-item-4955"><a class="nav-link" href="https://jemelabellise.fr/wp-content/uploads/sites/537/2018/06/1.1_EVALUATION-DE-DEPART-star-ae-v3.pdf" rel="noopener" target="_blank">Le Programme</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5027 nav-item" id="menu-item-5027"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/3.1_Parcours_d-_un_candidat_B.pdf" rel="noopener" target="_blank">Le parcours et le déroulement de la formation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4985 nav-item" id="menu-item-4985"><a class="nav-link" href="https://feelingconduite.com/conduite-supervisee/">Conduite supervisée</a><div class="fl-submenu-icon-wrap"><span class="fl-submenu-toggle-icon"></span></div>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5030 nav-item" id="menu-item-5030"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/1.3-Programme_Théorique_et_Pratique.pdf" rel="noopener" target="_blank">Le Programme</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5028 nav-item" id="menu-item-5028"><a class="nav-link" href="https://feelingconduite.com/wp-content/uploads/sites/349/2020/01/3.1_Parcours_d-_un_candidat_B.pdf" rel="noopener" target="_blank">Le parcours et le déroulement de la formation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4984 nav-item" id="menu-item-4984"><a class="nav-link" href="https://feelingconduite.com/remise-a-niveau/">Remise à niveau</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5137 nav-item" id="menu-item-5137"><a class="nav-link" href="https://feelingconduite.com/permis-moto/">Permis Moto</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5035 nav-item" id="menu-item-5035"><a class="nav-link" href="https://feelingconduite.com/post-permis/">La formation post-permis</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4740 nav-item" id="menu-item-4740"><a class="nav-link" href="https://feeling-conduite-pau.packweb3.com/" rel="noopener" target="_blank">Code en ligne</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4742 nav-item" id="menu-item-4742"><a class="nav-link" href="https://feelingconduite.com/agence/">Agence</a></li>
</ul> </div>
</nav>
</div>
</div>
</div>
</div>
</div>
</header><!-- .fl-page-header -->
<div class="fl-page-content" id="fl-main-content" itemprop="mainContentOfPage" role="main">
<div class="container">
<div class="row">
<div class="fl-content col-md-12">
<article class="fl-post fl-404">
<header class="fl-post-header" role="banner">
<h2 class="fl-post-title">Désolé ! Cette page n'existe pas.</h2>
</header><!-- .fl-post-header -->
<div class="fl-post-content clearfix">
<form action="https://feelingconduite.com/" aria-label="Recherche" method="get" role="search" title="Entrez mot(s) clé pour rechercher.">
<input aria-label="Recherche" class="fl-search-input form-control" name="s" onblur="if (this.value === '') this.value='Recherche';" onfocus="if (this.value === 'Recherche') { this.value = ''; }" type="search" value="Recherche"/>
</form>
</div><!-- .fl-post-content -->
</article>
<!-- .fl-post -->
</div>
</div>
</div>
</div><!-- .fl-page-content -->
<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="https://schema.org/WPFooter" role="contentinfo">
<div class="fl-page-footer">
<div class="fl-page-footer-container container">
<div class="fl-page-footer-row row">
<div class="col-sm-6 col-md-6 text-left clearfix"><div class="fl-page-footer-text fl-page-footer-text-1"><a href="/mentions-legales">Mentions Légales</a> | © Ediser 2019</div></div> <div class="col-sm-6 col-md-6 text-right clearfix"><div class="fl-page-footer-text fl-page-footer-text-2"><a href="https://feeling-conduite-pau.packweb3.com/">ACCES BCD</a> | <a href="/manager">MANAGER</a></div></div> </div>
</div>
</div><!-- .fl-page-footer -->
</footer>
</div><!-- .fl-page -->
<a href="#" id="fl-to-top"><span class="sr-only">Faire défiler vers le haut</span><i aria-hidden="true" class="fas fa-chevron-up"></i></a><script id="wf-cookie-consent-cookiechoices-js" src="https://feelingconduite.com/wp-content/plugins/wf-cookie-consent/js/cookiechoices.min.js?ver=d2e0522d1ee79a9974380d2329a4d7f7"></script>
<script id="jquery-throttle-js" src="https://feelingconduite.com/wp-content/plugins/bb-plugin/js/jquery.ba-throttle-debounce.min.js?ver=2.4.0.4"></script>
<script id="jquery-magnificpopup-js" src="https://feelingconduite.com/wp-content/plugins/bb-plugin/js/jquery.magnificpopup.min.js?ver=2.4.0.4"></script>
<script id="jquery-fitvids-js" src="https://feelingconduite.com/wp-content/plugins/bb-plugin/js/jquery.fitvids.min.js?ver=1.2"></script>
<script id="bootstrap-js" src="https://feelingconduite.com/wp-content/themes/bb-theme/js/bootstrap.min.js?ver=1.7.7"></script>
<script id="fl-automator-js-extra">
var themeopts = {"medium_breakpoint":"992","mobile_breakpoint":"768"};
</script>
<script id="fl-automator-js" src="https://feelingconduite.com/wp-content/themes/bb-theme/js/theme.min.js?ver=1.7.7"></script>
<script id="wp-embed-js" src="https://feelingconduite.com/wp-includes/js/wp-embed.min.js?ver=d2e0522d1ee79a9974380d2329a4d7f7"></script>
<script type="text/javascript">
	window._wfCookieConsentSettings = {"wf_cookietext":"Les cookies nous permettent de vous proposer nos services plus facilement. En utilisant nos services, vous nous donnez express\u00e9ment votre accord pour exploiter ces cookies.","wf_dismisstext":"OK","wf_linktext":"En savoir plus","wf_linkhref":"https:\/\/feelingconduite.com\/mentions-legales\/","wf_position":"bottom","language":"fr"};
</script>
</body>
</html>
