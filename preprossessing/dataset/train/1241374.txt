<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Page not found | Roanoke Valley Adult Soccer League</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/html5.js" type="text/javascript"></script><![endif]-->
<!-- Optimized by SG Optimizer plugin version - 5.7.11 --><link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://roanokesoccer.com/feed/" rel="alternate" title="Roanoke Valley Adult Soccer League » Feed" type="application/rss+xml"/>
<link href="https://roanokesoccer.com/comments/feed/" rel="alternate" title="Roanoke Valley Adult Soccer League » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/roanokesoccer.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://roanokesoccer.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/plugins/prettyphoto/css/prettyPhoto.css?ver=5.6" id="prettyPhoto-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/css/plugins.css?ver=5.6" id="themeblvd_plugins-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/css/themeblvd.css?ver=5.6" id="themeblvd-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/nav.min.css?ver=1.0" id="themeblvd_alyeska_nav-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/theme.min.css?ver=1.0" id="themeblvd_theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/menus.css?ver=1.0" id="themeblvd_alyeska_menu-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/shape/boxed-light.min.css?ver=1.0" id="themeblvd_alyeska_shape-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/style/light.min.css?ver=1.0" id="themeblvd_alyeska_style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='themeblvd_ie-css'  href='https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/ie.css?ver=1.0' type='text/css' media='all' />
<![endif]-->
<link href="https://roanokesoccer.com/wp-content/themes/alyeska/assets/css/responsive.min.css?ver=1.0" id="themeblvd_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://roanokesoccer.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://roanokesoccer.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="prettyPhoto-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/prettyphoto.js?ver=3.1.3" type="text/javascript"></script>
<script id="superfish-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/superfish.js?ver=1.4.8" type="text/javascript"></script>
<script id="flexslider-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/flexslider-2.js?ver=2.0" type="text/javascript"></script>
<script id="roundabout-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/roundabout.js?ver=1.1" type="text/javascript"></script>
<script id="themeblvd-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/assets/js/alyeska.min.js?ver=1.0" type="text/javascript"></script>
<script id="ios-orientationchange-fix-js" src="https://roanokesoccer.com/wp-content/themes/alyeska/framework/frontend/assets/js/ios-orientationchange-fix.js?ver=5.6" type="text/javascript"></script>
<link href="https://roanokesoccer.com/wp-json/" rel="https://api.w.org/"/><link href="https://roanokesoccer.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://roanokesoccer.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet" type="text/css"/>
<style>
body {background: #254306 url(https://roanokesoccer.com/wp-content/themes/alyeska/assets/images/layout/skin/footer-green.png) 0 bottom repeat-x;}#wrapper {background: url(https://roanokesoccer.com/wp-content/themes/alyeska/assets/images/layout/skin/bokeh-green.jpg) center 0 no-repeat;}a {color: #2a9ed4;}a:hover,article .entry-title a:hover,.widget ul li a:hover,#breadcrumbs a:hover,.tags a:hover,.entry-meta a:hover,#footer_sub_content .copyright .menu li a:hover {color: #1a5a78;}body {font-family: "Lucida Sans", "Lucida Grande", "Lucida Sans Unicode", sans-serif;font-size: 12px;}h1, h2, h3, h4, h5, h6, .slide-title {font-family: Yanone Kaffeesatz, Arial, sans-serif;}#branding .header_logo .tb-text-logo,#featured .media-full .slide-title,#content .media-full .slide-title,#featured_below .media-full .slide-title,.element-slogan .slogan .slogan-text,.element-tweet,.special-font {font-family: Yanone Kaffeesatz, Arial, sans-serif;}table.soccer {
	border-collapse:collapse;
	border:1px solid #e0e0e0;
	
}

table.soccer tr td {
padding:5px;
border:1px solid #e0e0e0;	
	
}


</style>
</head>
<body class="error404 hide-featured-area hide-featured-area-above sidebar-layout-full_width">
<div id="wrapper">
<div id="container">
<!-- HEADER (start) -->
<div id="top">
<header id="branding" role="banner">
<div class="content">
<div class="header-above"></div><!-- .header-above (end) --> <div id="header_content">
<div class="container">
<div class="inner">
<div class="header_logo header_logo_image">
<a class="tb-image-logo" href="https://roanokesoccer.com" title="Roanoke Valley Adult Soccer League"><img alt="Roanoke Valley Adult Soccer League" src="http://roanokesoccer.com/wp-content/uploads/2012/10/rvasl-2.png"/></a> </div><!-- .tbc_header_logo (end) -->
<div class="header-addon">
<div class="social-media">
</div><!-- .social-media (end) -->
</div><!-- .header-addon (end) -->
<div class="clear"></div>
</div><!-- .inner (end) -->
</div><!-- .container (end) -->
</div><!-- #header_content (end) -->
<div id="menu-wrapper">
<form action="" class="responsive-nav" method="post"><select class="tb-jump-menu"><option value="">Navigation</option><option value="http://roanokesoccer.com/under-35-roanoke-adult-soccer-league-schedule/">Schedule</option><option value="https://roanokesoccer.com/league-standings/">Standings</option><option value="https://roanokesoccer.com/register/">Join RASL</option><option value="#">Our League</option><option value="https://roanokesoccer.com/league-rules/">League Rules</option><option value="https://roanokesoccer.com/league-insurance/">League Insurance</option><option value="https://roanokesoccer.com/soccer-fields/">Soccer Fields</option><option value="https://roanokesoccer.com/suspensions/">Suspensions</option></select></form> <div id="main-top">
<div class="main-top-left"></div>
<div class="main-top-right"></div>
<div class="main-top-middle"></div>
</div>
<div id="main-menu">
<div class="classic-light" id="menu-inner">
<div class="menu-left"><!-- --></div>
<div class="menu-middle">
<div class="menu-middle-inner">
<ul class="menu" id="menu-main"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-749" id="menu-item-749"><a href="http://roanokesoccer.com/under-35-roanoke-adult-soccer-league-schedule/">Schedule</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1079" id="menu-item-1079"><a href="https://roanokesoccer.com/league-standings/">Standings</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1080" id="menu-item-1080"><a href="https://roanokesoccer.com/register/">Join RASL</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-244" id="menu-item-244"><a href="#">Our League</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49" id="menu-item-49"><a href="https://roanokesoccer.com/league-rules/">League Rules</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46" id="menu-item-46"><a href="https://roanokesoccer.com/league-insurance/">League Insurance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://roanokesoccer.com/soccer-fields/">Soccer Fields</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1301" id="menu-item-1301"><a href="https://roanokesoccer.com/suspensions/">Suspensions</a></li>
</ul> <div id="search-popup-wrapper">
<a href="#" id="search-trigger" title="Search the site...">Search the site...</a>
<div class="search-popup-outer">
<div class="search-popup">
<div class="search-popup-inner">
<form action="https://roanokesoccer.com" method="get">
<fieldset>
<input class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" type="text" value="Search the site..."/>
<input class="submit" type="submit" value=""/>
</fieldset>
</form>
</div><!-- .search-popup-inner (end) -->
</div><!-- .search-popup (end) -->
</div><!-- .search-popup-outer (end) -->
</div><!-- #search-popup-wrapper (end) -->
</div><!-- .menu-middle-inner (end) -->
</div><!-- .menu-middle (end) -->
<div class="menu-right"><!-- --></div>
</div><!-- #menu-inner (end) -->
</div><!-- #main-menu (end) -->
</div><!-- #menu-wrapper (end) -->
</div><!-- .content (end) -->
</header><!-- #branding (end) -->
</div><!-- #top (end) -->
<!-- HEADER (end) -->
<!-- MAIN (start) -->
<div class="full_width" id="main">
<div class="main-inner">
<div class="main-content">
<div class="grid-protection">
<div class="main-top"></div><!-- .main-top (end) --><div id="breadcrumbs"><div class="breadcrumbs-inner"><div class="breadcrumbs-content"><a class="home-link" href="https://roanokesoccer.com" title="Home">Home</a>» <span class="current">Error 404</span></div><!-- .breadcrumbs-content (end) --></div><!-- .breadcrumbs-inner (end) --></div><!-- #breadcrumbs (end) -->
<div id="sidebar_layout">
<div class="sidebar_layout-inner">
<div class="grid-protection">
<!-- CONTENT (start) -->
<div id="content" role="main">
<div class="inner">
<div class="article-wrap">
<article>
<div class="entry-content">
<h1>404 Error</h1>
			Apologies, but the page you're looking for can't be found.		</div><!-- .entry-content -->
</article>
</div><!-- .article-wrap (end) --> </div><!-- .inner (end) -->
</div><!-- #content (end) -->
<!-- CONTENT (end) -->
</div><!-- .grid-protection (end) -->
</div><!-- .sidebar_layout-inner (end) -->
</div><!-- .sidebar-layout-wrapper (end) -->
<div class="main-bottom"></div><!-- .main-bottom (end) --> <div class="clear"></div>
</div><!-- .grid-protection (end) -->
</div><!-- .main-content (end) -->
</div><!-- .main-inner (end) -->
</div><!-- #main (end) -->
<!-- MAIN (end) -->
<!-- FOOTER (start) -->
<div id="bottom">
<footer id="colophon" role="contentinfo">
<div class="content">
<div id="footer_sub_content">
<div class="container">
<div class="content">
<div class="copyright">
<span class="text">(c) 2018 Roanoke Valley Adult Soccer League | webmaster@roanokesoccer.com</span>
<span class="menu"></span>
</div><!-- .copyright (end) -->
<div class="clear"></div>
</div><!-- .content (end) -->
</div><!-- .container (end) -->
</div><!-- .footer_sub_content (end) -->
<div class="footer-below"></div><!-- .footer-below (end) --> </div><!-- .content (end) -->
</footer><!-- #colophon (end) -->
</div><!-- #bottom (end) -->
<!-- FOOTER (end) -->
<div id="after-footer">
<div class="after-footer-left"></div>
<div class="after-footer-right"></div>
<div class="after-footer-middle"></div>
</div>
</div><!-- #container (end) -->
</div><!-- #wrapper (end) -->
<script id="wp-embed-js" src="https://roanokesoccer.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>