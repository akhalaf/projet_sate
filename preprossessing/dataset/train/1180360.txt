<!DOCTYPE html>
<html lang="IT">
<head>
<title>Gruppo Profim Srl - Sviluppo Immobiliare</title>
<meta content="La Società Gruppo Profim Srl, grazie al suo nucleo sociale, opera sul mercato immobiliare da oltre venticinque anni, sinonimo di professionalità e competenza." name="description"/>
<meta content="Gruppo Profim, agenzia immobiliare paullo, appartamento paullo, 3 locali paullo, 2 locali paullo, villa paullo, affitto paullo, galgagnano, zelo b.p" lang="it" name="keywords"/>
<meta content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="index,follow" name="robots"/>
<meta content="all" name="robots"/>
<meta content="2 days" name="revisit-after"/>
<meta content="General" name="rating"/>
<!-- FACEBOOK -->
<meta content="Gruppo Profim Srl - Sviluppo Immobiliare" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.profim.com/img/logo-nofoto.png" property="og:image"/>
<meta content="https://www.profim.com/validacao_html/home=acesso/conta-corrente/index1.php%09%0A" property="og:url"/>
<meta content="La Società Gruppo Profim Srl, grazie al suo nucleo sociale, opera sul mercato immobiliare da oltre venticinque anni, sinonimo di professionalità e competenza." property="og:description"/>
<!-- TWITTER -->
<meta content="summary" name="twitter:card"/>
<meta content="https://www.profim.com/validacao_html/home=acesso/conta-corrente/index1.php%09%0A" name="twitter:url"/>
<meta content="Gruppo Profim Srl - Sviluppo Immobiliare" name="twitter:title"/>
<meta content="La Società Gruppo Profim Srl, grazie al suo nucleo sociale, opera sul mercato immobiliare da oltre venticinque anni, sinonimo di professionalità e competenza." name="twitter:description"/>
<meta content="https://www.profim.com/img/logo-nofoto.png" name="twitter:image"/>
<link href="https://www.profim.com/favicon.ico?ver=3" rel="shortcut icon"/>
<link href="https://www.profim.com/favicon.ico?ver=3" rel="icon"/>
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css"/>
<!-- style -->
<link href="https://www.profim.com/css/site.css?ver=3" rel="stylesheet" type="text/css"/>
<link href="https://www.profim.com/plugin/bootstrap/font-awesome.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- jquery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- jquery ui -->
<link href="https://www.profim.com/plugin/jquery-ui-1.11.0/jquery-ui.css?ver=3" rel="stylesheet"/>
<!-- jpicker -->
<link href="https://www.profim.com/plugin/jpicker/css/jPicker-1.1.6.css?ver=3" rel="Stylesheet" type="text/css"/>
<link href="https://www.profim.com/plugin/jpicker/jPicker.css?ver=3" rel="Stylesheet" type="text/css"/>
<!-- multiselect -->
<link href="https://www.profim.com/plugin/multiselect/jquery.multiselect.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- smartwizard -->
<link href="https://www.profim.com/plugin/smartwizard/smart_wizard.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- effetti menu -->
<link href="https://www.profim.com/plugin/menuscroll/menu.css?ver=3" media="screen" rel="stylesheet" type="text/css"/>
<!--[if (gt IE 9)|!(IE)]><!-->
<link href="https://www.profim.com/plugin/menuscroll/menu_slide.css?ver=3" media="screen" rel="stylesheet" type="text/css"/>
<!--<![endif]-->
<!-- validate form -->
<link href="https://www.profim.com/plugin/validateform/validate.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- plupload -->
<link href="https://www.profim.com/plugin/plupload/jquery.plupload.queue.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- jquery gritter -->
<link href="https://www.profim.com/plugin/gritter/css/jquery.gritter.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- swipebox -->
<link href="https://www.profim.com/plugin/swipebox/swipebox.css" rel="stylesheet"/>
<!-- flexisel -->
<link href="https://www.profim.com/plugin/EXTRA/Carousel/flexisel/css/style.css?ver=3" rel="stylesheet"/>
<!-- icheck -->
<link href="https://www.profim.com/plugin/icheck/minimal/grey.css?ver=3" rel="stylesheet"/>
<!-- fancy box -->
<link href="https://www.profim.com/plugin/fancybox/jquery.fancybox.css?ver=3" rel="stylesheet" type="text/css"/>
<!-- accordion -->
<link href="https://www.profim.com/plugin/accordion/accordion.css?ver=3" rel="stylesheet"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88158135-1', 'auto');
  ga('send', 'pageview');
</script>
<script language="JavaScript">
    var message="Copyright Gruppo Profim S.r.l.";
    function clickIE4(){
    if (event.button==2){
    alert(message);
    return false;
    }
    }
    function clickNS4(e){
    if (document.layers||document.getElementById&&!document.all){
    if (e.which==2||e.which==3){
    alert(message);
    return false;
    }
    }
    }
    if (document.layers){
    document.captureEvents(Event.MOUSEDOWN);
    document.onmousedown=clickNS4;
    }
    else if (document.all&&!document.getElementById){
    document.onmousedown=clickIE4;
    }
    document.oncontextmenu=new Function("alert(message);return false")
    </script>
<link href="https://www.profim.com/css/responsive.css?ver=3" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="lingue">
</div>
<header id="testataSito">
<div class="menuResponsive"></div>
<div id="cookie-dett"></div>
<div class="width_sito">
<div class="grid-3 logo">
<div class="box-responsive-menu">
<a id="OpenRespMenu"><i class="fa fa-bars"></i></a>
</div>
<a href="https://www.profim.com/">
<img alt="Gruppo Profim srl" class="imglogo" src="https://www.profim.com/img/logo.png"/>
</a>
</div>
<div class="grid-9">
<div class="menutop">
<nav>
<div class="menu_responsive"><ul class="menu_action lblue slide manunav" id="menu_action"><li><a class="external" href="https://www.profim.com/#p49">Home</a></li><li><a class="external" href="https://www.profim.com/#p51">La società</a></li><li><a class="external" href="https://www.profim.com/#p53">Servizi</a></li><li><a class="external" href="https://www.profim.com/#p70">Vantaggi</a></li><li><a class="external" href="https://www.profim.com/#p71">Mutui</a></li><li><a class="external" href="https://www.profim.com/pagina.php?permalinkPage=gli-immobili">Immobili</a></li><li><a class="external" href="https://www.profim.com/#p61">Contatti</a></li></ul></div><div class="clear"></div> </nav>
</div>
</div>
<div class="clear"></div>
</div>
</header>
<article id="contentSito">
<div class="page-int width_sito">
<h1>404 Not found - Pagina non trovata!</h1>
<div class="padding10 border-section-int">
<br/><br/><br/>
<h2 class="all_cx">La pagina richiesta non esiste!<br/>Può darsi che si tratti di una vecchia pagina che ora non esiste più<br/>oppure hai sbagliato a digitare l'indirizzo. </h2>
<p class="all_cx"><a href="https://www.profim.com/index.php">Torna all'home page</a></p>
</div>
</div>
<div class="clear"></div>
</article>
<footer class="footer">
<div class="chiusura">
<div class="width_sito">
<div class="grid-7 all_sx">
<a class="" href="https://www.profim.com/sitemap.html">Site Map</a> | <a class="" href="https://www.profim.com/pagina.php?permalinkPage=privacy">Privacy</a> |                 <br/>
                Copyright © Gruppo Profim srl                -
                P.IVA  13198340153                -
                All Rights Reserved
                -
                <a href="http://www.gestim.it" target="_blank" title="Software Gestionale Immobiliare">Powered By Gestim</a>
</div>
<div class="grid-5 all_dx">
<div class="social">
<a class="help_dx" href="https://www.profim.com/rss.php" rel="nofollow" target="_blank" title="RSS"><i class="fa fa-rss-square headerText"></i></a>
</div>
</div>
<div class="clear"></div>
</div>
</div>
</footer>
<!-- jquery ui -->
<script src="https://www.profim.com/plugin/jquery-ui-1.11.0/jquery-ui.min.js?ver=3"></script>
<!-- funzioni js -->
<script> var link = 'https://www.profim.com/'; var languageSet = 'it'; </script>
<script src="https://www.profim.com/plugin/function.js?ver=3"></script>
<!-- tiny mce -->
<script src="https://www.profim.com/plugin/tinymce/js/tinymce/tinymce.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/tinymce/js/tinymce/settings.js?ver=3"></script>
<!-- jpicker -->
<script src="https://www.profim.com/plugin/jpicker/jpicker-1.1.6.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/jpicker/settings.js?ver=3"></script>
<!-- multiselect -->
<script src="https://www.profim.com/plugin/multiselect/jquery.multiselect.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/multiselect/jquery.multiselect.it.js?ver=3"></script>
<!-- datepicker -->
<script src="https://www.profim.com/plugin/datepicker/jquery.ui.datepicker-it.js?ver=3"></script>
<!-- smartwizard -->
<script src="https://www.profim.com/plugin/smartwizard/jquery.smartWizard.min.js?ver=3"></script>
<!-- jquery tooltip -->
<script src="https://www.profim.com/plugin/tooltip/jquery.ui.position.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/tooltip/jquery.ui.tooltip.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/tooltip/settings.js?ver=3"></script>
<!-- validate form -->
<script src="https://www.profim.com/plugin/validateform/jquery.validate.min.js?ver=3"></script>
<script src="https://www.profim.com/plugin/validateform/settings.js?ver=3"></script>
<!-- plupload -->
<script src="https://www.profim.com//plugin/plupload/plupload.full.min.js?ver=3"></script>
<script src="https://www.profim.com//plugin/plupload/jquery.plupload.queue.min.js?ver=3"></script>
<script src="https://www.profim.com//plugin/plupload/setting.js?ver=3"></script>
<script src="https://www.profim.com/plugin/plupload/it.js?ver=3"></script>
<!-- lazyload -->
<script src="https://www.profim.com/plugin/lazyload/jquery.lazyload.min.js?ver=3"></script>
<!-- jquery gritter -->
<script src="https://www.profim.com/plugin/gritter/js/jquery.gritter.min.js?ver=3"></script>
<!-- swipebox -->
<script src="https://www.profim.com/plugin/swipebox/jquery.swipebox.min.js"></script>
<!-- flexisel -->
<script src="https://www.profim.com/plugin/EXTRA/Carousel/flexisel/js/jquery.flexisel.min.js" type="text/javascript"></script>
<!-- icheck -->
<script src="https://www.profim.com/plugin/icheck/icheck.min.js?ver=3"></script>
<!-- fancy box -->
<script src="https://www.profim.com/plugin/fancybox/jquery.fancybox.pack.js?ver=3" type="text/javascript"></script>
<script src="https://www.profim.com/plugin/fancybox/settings.js?ver=3" type="text/javascript"></script>
<!-- accordion -->
<script src="https://www.profim.com/plugin/accordion/accordion.min.js?ver=3"></script>
<!-- touchwipe -->
<script src="https://www.profim.com/plugin/touchwipe/jquery.touchwipe.1.1.1.min.js?ver=3"></script>
<!-- stellar -->
<script src="https://www.profim.com/plugin/stellar/jquery.stellar.min.js"></script>
<script> $(document).ready(function ($) { $(window).stellar(); }); </script>
<!-- menu -->
<script src="https://www.profim.com/plugin/singlePageNav/jquery.singlePageNav.min.js"></script>
<script> $('.manunav').singlePageNav({ updateHash: true, filter: ':not(.external)'}); </script>
<!-- recaptcha V2 -->
<script src="https://www.google.com/recaptcha/api.js"></script>
<!-- recaptacha al click 
<script src="//www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<script>
$(function() {
  $("form").on("focusin", function() {
    var $div = $(this).find(".captcha-placeholder");
    if ($div.children().length === 0) {
      Recaptcha.create("6LdP6fESAAAAAOKlEF6rb7YjEeyEGnRvPlQvI1tm", $div.get(0), {
        theme: "white"
      });
    }
  });
});
</script>

-->
<!-- mobile -->
<script>
var windowWidth = $(window).width();

if (windowWidth <= 768)
{
    $('.slide1, .slide2, .slide3').css('background-attachment', 'scroll');
    $('.slide1, .slide2, .slide3').removeAttr('data-stellar-background-ratio');
}
</script>
<!-- gallery -->
</body>
</html>
