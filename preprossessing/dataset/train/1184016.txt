<!DOCTYPE htm>
<html><body><htm lang="en">
<!-- Added by HTTrack --><meta content="text/htm;charset=utf-8" http-equiv="content-type"/><!-- /Added by HTTrack -->
<title>ProPlan Contracting &amp; Projects Management</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="text/htm; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Land Investors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" name="keywords"/>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--css links-->
<link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/><!--bootstrap-->
<link href="css/font-awesome.css" rel="stylesheet"/><!--font-awesome-->
<link href="css/flexslider.css" media="screen" rel="stylesheet" type="text/css"/> <!-- Flexslider-CSS -->
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/><!--stylesheet-->
<!--//css links-->
<!--fonts-->
<link href="http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
<!--//fonts-->
<script src="../../../../../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script><script src="../../../../../../../m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
	(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "../../../../../../../vdo.ai/core/w3layouts/vdo.ai.js");
	</script>
<meta content="http://14000.tel/Website/images/logo-w.png" itemprop="image" property="og:image"/>
<link href="images/logo-w.png" rel="apple-touch-icon"/>
<link href="http://14000.tel/Website/images/logo-w.png" itemprop="thumbnailUrl"/>
<span itemprop="thumbnail" itemscope="" itemtype="http://schema.org/ImageObject">
<link href="http://14000.tel/Website/images/logo-w.png" itemprop="url"/>
</span>
<!-- Header -->
<div class="banner w3l" id="home">
<div class="container">
<div class="header-nav">
<nav class="navbar navbar-default">
<div class="navbar-header logo">
<button class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<h1>
<img class="photo" height="150" src="images/logo.png" width="auto"/>
</h1>
</div>
<!-- navbar-header -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav navbar-right">
<li><a class="active" href="index.htm">Home</a></li>
<li><a href="about.htm">About</a></li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Services<b class="caret"></b></a>
<ul class="dropdown-menu agile_short_dropdown">
<li><a href="General-Contractor.htm">General Contractor</a></li>
<li><a href="Design.htm">Design â Build Projects</a></li>
<li><a href="Construction.htm">Projects /Construction Management</a></li>
</ul>
</li>
<li><a href="Projects.htm">Projects</a></li>
<li><a href="contact.htm">Contact</a></li>
<li><a href="index-A.htm">Ø§ÙØ¹Ø±Ø¨ÙØ©</a></li><img class="photo" height="20" src="images/A.png" style="margin:20px -20px; position: absolute;" width="auto"/>
</ul>
</div>
<div class="clearfix"> </div>
</nav>
<div class="clearfix"> </div>
</div>
<div class="banner-info wow bounceInDown" data-wow-delay="0s" data-wow-duration="1s">
<div class="callbacks_container">
<ul class="rslides" id="slider3">
<li>
<div class="banner-text">
<h6>PROFILE</h6>
<h3>ProPlan Contracting &amp; Projects Management</h3>
<a class="read-agileits" data-toggle="modal" href="http://14000.tel/English-Profile/index#ProPlan-Profile/page1">Download </a>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
<!-- //Header -->
<!---728x90--->
<!-- about -->
<div class="about w3layouts-agileinfo">
<div class="container">
<h3 class="agile-titlew3">ProPlan <span> Contracting &amp; Projects Management</span></h3>
<div class="about-top w3ls-agile">
<div class="col-sm-6 red">
<h5>We believe in the power of great ideas.<br/>Proplan strives to be one of the most trusted contracting companies in Egypt and Middle East.</h5>
</div>
<div class="col-sm-6 about-wel">
</div>
<div class="clearfix"> </div>
</div>
</div>
</div>
<!-- //about -->
<!---728x90--->
<!-- Stats -->
<div class="stats-agileits" id="stats">
<center><h3 class="agile-titlew3">Services </h3></center>
<div class="container">
<div class="stats-info agileits w3layouts">
<div class="col-md-3 agileits w3layouts col-sm-6 stats-grid stats-grid-1">
<div class="stat-info-w3ls">
<i aria-hidden="true" class="fa fa-magic"></i>
<h4 class="agileits w3layouts">General Contractor</h4><br/>
<p><font color="#fff">As the general contractor, we first create the highest level of trust and integrity with our clients. We value our role in the success of your Projects, and take all the steps needed to ensure the end product is one that meets your needs and makes us proud. </font></p>
</div>
</div>
<div class="col-md-3 col-sm-6 agileits w3layouts stats-grid stats-grid-2">
<div class="stat-info-w3ls">
<i aria-hidden="true" class="fa fa-bars"></i>
<h4 class="agileits w3layouts">Design â Build Projects</h4><br/>
<p><font color="#fff">Our creativity and innovation is best expressed through our design build approach to construction in mosques, large and small scale commercial, light industrial and residential building Projects.</font></p>
</div>
</div>
<div class="col-md-3 col-sm-6 stats-grid agileits w3layouts stats-grid-3">
<div class="stat-info-w3ls">
<i aria-hidden="true" class="fa fa-columns"></i>
<h4 class="agileits w3layouts">Projects /Construction Management</h4><br/>
<p><font color="#fff">Whether you are an owner, contractor or investor, you need to deal with a range of requirements during every phase of your construction Projects.</font></p>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- //Stats -->
<!---728x90--->
<!-- news -->
<div class="w3ls-section services news">
<div class="container">
<h2 class="agileits-title">Projects</h2>
<div class="news-agileinfo">
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Frafra.htm"><img alt="" class="img-responsive zoom-img" src="images/Frafra.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Frafra Market<br/></h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Syndicate-of-Agricultural.htm"><img alt="" class="img-responsive zoom-img" src="images/Syndicate.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Agricultural Professions Syndicat</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="ElBouhose.htm"> <img alt="" class="img-responsive zoom-img" src="images/ElBouhose.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>National Research Centre Employee Club<br/></h4>
</div>
</div>
</div>
<div class="clearfix"> </div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Balina.htm"><img alt="" class="img-responsive zoom-img" src="images/Al Balina.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Baliana Hotel<br/><br/> </h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Royal.htm"><img alt="" class="img-responsive zoom-img" src="images/Royal.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Royal Hall - Scientific Professions Club</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Customs.htm"><img alt="" class="img-responsive zoom-img" src="images/Customs.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Gymnasiums Building-Custom Club</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Sphinx.htm"><img alt="" class="img-responsive zoom-img" src="images/Sphinx.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Sphinx</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Xceed.htm"> <img alt="" class="img-responsive zoom-img" src="images/Xceed.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Training Center-Xceed</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Plam.htm"> <img alt="" class="img-responsive zoom-img" src="images/Plam.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Plam Flat</h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Scientific.htm"> <img alt="" class="img-responsive zoom-img" src="images/Scientific.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Administration Apartment - Scientific Professions </h4>
</div>
</div>
</div>
<div class="col-sm-4 news-w3lgrids">
<div class="news-gridtext">
<div class="news-w3img">
<a href="Lamerada.htm"> <img alt="" class="img-responsive zoom-img" src="images/Lamerada.jpg"/></a>
</div>
<div class="news-w3imgtext">
<h4>Lamerada Compound </h4>
</div>
</div>
</div>
<div class="clearfix"> </div>
</div>
</div>
<!-- //news -->
<!-- newsletter -->
<div class="footer-top">
<div class="container">
<h3 class="agileits-title">Subscribe Here</h3>
<div class="agile-leftmk">
<form action="#" method="post">
<input name="email" placeholder="E-mail" required="" type="email"/>
<input type="submit" value="Subscribe"/>
<div class="clearfix"></div>
</form>
</div>
</div>
</div>
<!-- //newsletter -->
<!-- testimonials -->
<div class="testimonials">
<div class="container">
<h3 class="agileits-title">Our Clients</h3>
<section class="slider">
<div class="flexslider">
<ul class="slides">
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p> National Customs Research Center</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/National.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p>Customs club New Cairo âmeanâ</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/Customs-club.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p> Namaa for engineering Consultancy</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/Namaa.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p> Syndicate of Scientiï¬c Engineering</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/Scientiï¬c.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p> Expertise House for engineering consultancy</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/Expertise.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p>Xceed</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/Xceed2.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
<li>
<div class="banner-bottom-2">
<div class="about-midd-main">
<p> Kuwaiti office for charity work</p>
<div class="test-right-w3layouts">
<img alt="clients" class="agile-img" src="images/office.jpg"/>
</div>
<div class="clearfix"></div>
</div>
</div>
</li>
</ul>
</div>
<div class="clear"> </div>
</section>
</div>
</div>
<!-- //testimonials -->
<!-- footer -->
<div class="footer">
<div class="container">
<div class="w3_agile_footer_grids">
<div class="col-md-4 w3_agile_footer_grid">
<h3 class="white-w3ls">About ProPlan</h3>
<ul class="agile_footer_grid_list">
<li><i aria-hidden="true" class="fa fa-hand-o-rightr"></i>Since its founding, Structure has earned itâs as a builder of a landmark, one-of-a-kind structures. Structureâs success is grounded in the companyâs dedication to the success of its clients and its people.</li>
</ul>
</div>
<div class="col-md-4 w3_agile_footer_grid">
<h3 class="white-w3ls">Social Media</h3>
<ul class="agile_footer_grid_list">
<li><a href="https://www.facebook.com/Proplan14000/"><i aria-hidden="true" class="fa fa-facebook"></i>Facebook</a></li>
</ul>
</div>
<div class="col-md-4 w3_agile_footer_grid">
<h3 class="white-w3ls">Navigation</h3>
<ul class="agileits_w3layouts_footer_grid_list">
<li><i aria-hidden="true" class="fa fa-hand-o-right"></i><a href="index.htm">Home</a></li>
<li><i aria-hidden="true" class="fa fa-hand-o-right"></i><a href="about.htm">About</a></li>
<li><i aria-hidden="true" class="fa fa-hand-o-right"></i><a href="Projects.htm">Projects</a></li>
<li><i aria-hidden="true" class="fa fa-hand-o-right"></i><a href="contact.htm">Contact</a></li>
</ul>
</div>
<div class="w3_agileits_footer_grid_left">
<a data-toggle="modal" href="http://14000.tel">
<img alt=" " class="img-responsive" src="images/14000.png"/>
</a>
</div>
<div class="clearfix"> </div>
</div>
<div class="w3ls_address_mail_footer_grids">
<div class="col-md-4 w3ls_footer_grid_left">
<div class="wthree_footer_grid_left">
<i aria-hidden="true" class="fa fa-map-marker"></i>
</div>
<p>Flat no.2 , Building no.3163, front of future school,
Almearag City, Maadi, Cairo</p>
</div>
<div class="col-md-4 w3ls_footer_grid_left">
<div class="wthree_footer_grid_left">
<i aria-hidden="true" class="fa fa-phone"></i>
</div>
<p>(+2)011-5444-7441 </p>
<p>(+2)010-0082-2875 </p>
</div>
<div class="col-md-4 w3ls_footer_grid_left">
<div class="wthree_footer_grid_left">
<i aria-hidden="true" class="fa fa-envelope-o"></i>
</div>
<p><a href="mailto:info@example.com">info@proplancpm.com</a>
</p></div>
<div class="clearfix"> </div>
</div>
<div class="agileinfo_copyright">
<p>Â© Powered by: <a href="https://140.tel">eHotline</a></p>
</div>
</div>
</div>
<!--//footer -->
<!-- js -->
<script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
<!--//js -->
<!-- responsiveslides -->
<script src="js/responsiveslides.min.js"></script>
<script>
									// You can also use "$(window).load(function() {"
									$(function () {
									 // Slideshow 4
									$("#slider3").responsiveSlides({
										auto: true,
										pager: false,
										nav: true,
										speed: 500,
										namespace: "callbacks",
										before: function () {
									$('.events').append("<li>before event fired.</li>");
									},
									after: function () {
										$('.events').append("<li>after event fired.</li>");
										}
										});
										});
			</script>
<!-- //responsiveslides -->
<!-- OnScroll-Number-Increase-JavaScript -->
<script src="js/numscroller-1.0.js" type="text/javascript"></script>
<!-- //OnScroll-Number-Increase-JavaScript -->
<!--Scrolling-top -->
<script src="js/move-top.js" type="text/javascript"></script>
<script src="js/easing.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('htm,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!--//Scrolling-top -->
<!-- FlexSlider -->
<script defer="" src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
					$(function(){
					 
					});
					$(window).load(function(){
					  $('.flexslider').flexslider({
						animation: "slide",
						start: function(slider){
						  $('body').removeClass('loading');
						}
					  });
					});
				  </script>
<!-- FlexSlider -->
<!-- script for responsive tabs -->
<script src="js/easy-responsive-tabs.htm"></script>
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script>
<!-- script for responsive tabs -->
<!-- smooth scrolling -->
<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
<a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script src="js/bootstrap-3.1.1.min.js" type="text/javascript"></script>
</htm></body></html>