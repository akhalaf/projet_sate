<!DOCTYPE HTML>
<html dir="ltr" lang="ru-ru">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.dapos.ru/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>О студии - Dapos</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/templates/yootheme/css/theme.9.css?1608041840" rel="stylesheet" type="text/css"/>
<script src="/templates/yootheme/vendor/assets/uikit/dist/js/uikit.min.js?2.3.25" type="text/javascript"></script>
<script src="/templates/yootheme/vendor/assets/uikit/dist/js/uikit-icons-max.min.js?2.3.25" type="text/javascript"></script>
<script src="/templates/yootheme/js/theme.js?2.3.25" type="text/javascript"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function() {
            Array.prototype.slice.call(document.querySelectorAll('a span[id^="cloak"]')).forEach(function(span) {
                span.innerText = span.textContent;
            });
        });
	</script>
<script>var $theme = {};</script>
</head>
<body class="uk-flex uk-flex-middle" uk-height-viewport="">
<div class="tm-offline uk-container uk-container-small uk-text-center">
<div data-messages="[]" id="system-message-container">
</div>
<h1>Dapos</h1>
<p>Сайт закрыт на техническое обслуживание.<br/>Пожалуйста, зайдите позже.</p>
<form action="/" class="uk-panel uk-margin" method="post">
<div class="uk-margin">
<input class="uk-input" name="username" placeholder="Логин" type="text"/>
</div>
<div class="uk-margin">
<input class="uk-input" name="password" placeholder="Пароль" type="password"/>
</div>
<div class="uk-margin">
<button class="uk-button uk-button-primary uk-width-1-1" name="Submit" type="submit">Войти</button>
</div>
<div class="uk-margin">
<label>
<input name="remember" placeholder="Запомнить меня" type="checkbox" value="yes"/>
                        Запомнить меня                    </label>
</div>
<input name="option" type="hidden" value="com_users"/>
<input name="task" type="hidden" value="user.login"/>
<input name="return" type="hidden" value="aHR0cHM6Ly93d3cuZGFwb3MucnUv"/>
<input name="103950af03ae611a02e625147d511b78" type="hidden" value="1"/>
</form>
</div>
</body>
</html>
