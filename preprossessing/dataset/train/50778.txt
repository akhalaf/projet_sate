<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "78809",
      cRay: "610b5366cd98e247",
      cHash: "a706a9d7777b90c",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cDovL3d3dy45MWthbi5ldS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "aRSmDn94Evo6gbtw3udL/Ib7SVkPu6YikhEfEhK0DyuCXPJA9sGYpOt8WKUl8EjbhouJD5i2EeLym37uxKjWNpFQFAz+B/BLaXqBaSp/5qKaDKfgG+qMOJpvizTAxAYey79KCAxoRYQjsbsI7QSHEv32xJFyPkv0LYQC8lrMsYnR5g20zQjDxB6PwdaU7wM9tS1CW7IuWxScoWw2W6Kj1Dn4yPXileeFrKJyFEjv7qE7zXGnmYRHdOEJ+It5HUpALkYg7p82/toMZ3y0PBZWrYg2xkpzeXPD4ft5SoiBfYJ5WVflvZKygVEaaHEYyBREhGkU1JYwB8I5PI4uTeyndOjENGIi87yOYBl+KzzHR4gU9Dd7mI8/yclyWePIr2547egYiWFpHFktyW4Rw7qJ01ylb54mFiiRcafQpSZdWW1OZsfivUdLlu/crKJnWEkVG1sbdGrN79e7IygBxNn28+pACQ+tCEFf3Q7lFqpt3goeCfNTlnQ4IysunkE7atVuONdYlWl3ra8lZ9BeXK2RI4qu6m2cSKIP53lxEyZFkm5ShzuxlOaHcjdI9ux4jxT87cN4zR92mgTD1Hx9gi02fHO2F6rrodS92fqb4mYTsQRRovReKoyU3ID0wchozGm0gqHiFKIIoYrmMMhbxL1pVVr0JbXbU7lPLC2gm2F7ndK8omaaUwedyF+CiXtesvXtyiH3BgShmP0pzwu/FrmZnLlZyFFOlSqyDQE3N48Jj1KnXPeyN+Xc+Bify60C+Lf0",
        t: "MTYxMDUwMTE1MS44MTIwMDA=",
        m: "eqDRBHPZngJtoFkqSlzEJAD/TrF5M3aqKto8KH9ZEOc=",
        i1: "4B/WCF0RxS3EQeY1304MoA==",
        i2: "ZANHRzTJCz1mTU7jBMIRyA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "SvRVux7fiSZoGJHcRuervpw7dKMeQgHWrp1S8f8XKGQ=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610b5366cd98e247");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> 91kan.eu.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<a href="http://bt50.net/concerned.php?tool=53" style="position: absolute; top: -250px; left: -250px;"></a>
<form action="/?__cf_chl_jschl_tk__=bc394e2c29829fa729dca17e898ed44dd34d7ead-1610501151-0-AZoTHC_ZVSKmzHsHYf4Ik2jIEQYf0XP3QG6bXNmk4J28jlWw0RBZTe1SWTNX4HzDDPw3y2bnNOrmNgA1hZcZFDSX-5Z8liw0aCY0hgMMEmrlLIQxoeAui1TRrzKi6miLspbL8XKzNR4pU_6QqKN4xaODWVQ7ZdC7jLVfWQbAjOyTfCN1Mg2-TRVjJMFlFYkye7UZAS-KVdDUq4BROjmBpOzy7r0pNXYkW24i2RT5VLgi5vyrPMJynpB3WogX3ueT33EIoEYfWKeicPjop0Qjz47Es2FzW16PFLV9fhx2gZTHOs4tSlPBy3K_TIOtA46z_g" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="0c30db5685140233749dbfe83d78d78aae1e5b04-1610501151-0-ARb8tpvgiQstOdrCFAzzwYTv9Cy1HitXKGptIIWQfx11iMQasHdSRrhbxP3M4+4bQCy7MU+7pELTAXD4IO88lGPaWaX3XkAI21pQHzIUUX1ftntgUFvnwNL7I62RNcBK+Z+Fv7b4Tl5U3yy9mqv6L6e6SHzsb25yRtQcmDfq+bsEX739CAZsRcVm2vXbdAu2xE8yB7/s0BJT/jw6J79iOviOrvRVnzi9gA2GFOCpGnj1+QTM4+jDWk7B4q5BziNdzIPH32eBdn3ao8VPkkA2vN/NqHuBPgSnc8dRJAdhKRYcipBr/uG1nsf3wGhgNO08G9EXG7sYMiCgkLWrO2QBQHUywOYeLVkRIAOP9nxTPOh66lfdq8vHEy5+VAOBytGcH60PYmpQ7PHmGzl9f7ouDeydwMMlMql1DibpUknz9P7l0bfkUbZdABbtUtqB7eH+bygGBlEUzig4ZXBSkFk+yDlxcfBqnB+TJ1xZATKxdMYTO+bFb6FFt7cO1u7mreZgrtvETGkaH56Tp5TXdmWdpTwL6c/tTDgAoK/xSWO8aWNc973Iybe07PTLCYjIOiY4Z3ZYJoo9Ytrg/yWdZBLeO6mJl2oWaX0+M1nPYPBoQ71dg6aPQYGwXeN4BqaQc2/VWQ=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="0cc5ef96fde748ee40b39605ad896e57"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610501155.812-7tjTATR0Zj"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610b5366cd98e247')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610b5366cd98e247</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
