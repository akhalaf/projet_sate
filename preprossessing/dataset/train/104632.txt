<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "93859",
      cRay: "610ca666897121a6",
      cHash: "b405e85362777bf",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWxwaW5laG9tZWFpci5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "WOjJFjpG9D8ms93FZ0CYg81rR/izlpqM/FwFqOGH7tV9lvLlrAmc5/G32m5l5rOgPjv1UL5YI17UOhZHmIAiVD6HmGgJv4RccIhcjKBfxjTjbnqKgzNTuw8DeHlM1RmqCQpYy8OUaC+4H5wkwmgTLQ+63Kzsv7FmHxJVghFHa9H9MzbowSy90TMGtKSNPgMf/+fygG/k70iFMjggjm7a5jVkUh9VVTbZ4r4mayvLm0jqPMPKzJP+ZbCu4eYpNoCODmvzanop4bl3YXnEmT+kDKr/2GhBF3BZjmNhsnxtjq6nvzDAatHEA6c/M+eUBLXS8i/IrvMHAn9svq4n5TcMUg5gheyvJDjNi+g4RREEzV3wOrE41bW8Jy4BPYz+pEUr+d1pNbKMZ6SnIxa0iiRpvzVXJxK73aXDNUz0ZezrgBW/vN1GbxqOhj747MQuKOzw8mKrK1pAeN5C7TAoTmWMjGLnJqrPrn/AAMgd7SJgKjTYETiIR9igCIXzR+Gh3b4QZsJcY6eR0F7UJxju0jlEzQ4n4wOgXPLKd6c0zgN6sk+4fSO6c6SWUt6Hk5QmN+BMQY1IuBIVUtWoWtgBvjlPQMtzuXrMcKR4mc1ULWmb+DVzgle+QKiNLkDSeLIyqFiRWrPYusHTFHKKIGW4xTRrK1AJKOkU1xmLX5sStV5Pca5ELqDuKLZw0LE3U9eowESB0/8S7WLrAzdwmknTarmO2SC6KWbE5kBj3CYQ8npXHQqsdds5/NQPnWaHTpMAaZxU",
        t: "MTYxMDUxNTAzNy4yMDkwMDA=",
        m: "KQYfW06G8Ct637kvBFMWNfEgH+FA7idWIbZr3RqOE7E=",
        i1: "DkFQ+jsxXGpJfg27aJ1yqw==",
        i2: "lkMCZximcPRdgjwitEU53Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "7K1es7YgMK9si6HFUb7Wc7Z7qfmtgWmjKra38OH74OA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.alpinehomeair.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=944dbd45be34206267aeb21dde4994893a5a5c98-1610515037-0-AXBg45JP87XqHEXSWqHvD5Xzv9jwHQHDD_ezw9yRqR3y5dw2w6JAIEayxU5SsH6V0g6PBH1a-l8e0WCV3yO1UtumtZqPn4cySphGvEFx7zvi6-c53z6OiRa2Q-Y0UZ0EuCtBBX2c8zSCATJxXeVttnr-OTz0hSVrv3VGffC7t-egLmtyB1j2GWxgtEKP4twvK12JIVO2yWYpUO5LacOKJnkvyylwnHdBUjQroBzNTHE7lPG5KRNNFzkdvgS2FdG3ZRTqO9ntMeiuUI7TOvy4ZyjDy_tJLkPPTQYNSbxAj14XWurl0ssygXYBuzGr1IDO9h1NigDEbvpwmos71aeDfM8ULjvtvbc0oFGetWF3ldc11f4kXQDGhs69MGgLEOFX8iiQYKiJG5mZdeMQDu6_Y-za4n2poKBAMQrDy2Z938F8z9tYg2aM7pPntWKnEe8-lD7seEOZKxAkLxCcLS46v0i9UJsXCA1Ac-E6FfeeqgpURDceAu3HprbWOTeepvwAed4TyyQ6OA1qQANJ2Oh_nZs" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="aa93df711ba1b815376ac6cd8de4c03fb51d8dfe-1610515037-0-AZor2+52rYEqclHvtWJYZ/++3Sv1lU6oxO6haXhTHCnxv6NAS+wQjY9DWPBsDSBM5v13xToraaeFAtMtewMyYreAahV+jEbzAbEmxR2jrOIczzVPD82kDmBAc0lzyG3qwLUMrgyiqfYiOQksayvGlLxalGhgTiJXpDYw4CxpNNcEWczTXjNWi0mQlql+jRZQxqdN2P+aBDTuheVNq7bBG0fcPbULRp2JaL6pP4pA1zXYYrnSJiiW2jAnWHxGdu01AMtZ6jmWXAmRXfnzLN+5i+la6u7QCN/38Qc09QzRqKjZmTlTDfB/U/J3Qfgy4IQrA10rB97zFJmr2p/kYrS+/xuxlD0c3lgQnm/VO87tMrwecPVYiTGddEk1cBwyn/B9ysAhPQGHYf1IFsExkNpOD275LIWMw/U3cZ/IEl0ezEluXGgm8VYr7ZltbHmLX6C4T+u2VKGsIcPHaHlAOzB7UHIbMb74IuJVjzpjRJouKEfxcQxxFFlJg7yL6Ucnt4085XX1jxuJSXiIxzRf+yIqsvo4Q2J9HFGU+aFqoyJ/MLnUBv37H/sFofU0zXyKhsvITbbH3MGNnUIbe29GjuVxgmo+Y1CpwnW0aYDwdtt8D6END4GG+0bBs212rb001vmUhekhRYHlzbGuoCsbr68I7M4vgHnDGD5lO0meKN+qNixwasUdFMYGkqqFsOaOabYQP6cI23Y8CHICAuE2ZtIV2Kt6H2+1tn7fv5Sm4w6u/80H6d4gKX95ktBfnhfCKt7Cw7B+nUeiq9c6HJRIqRNhHXfJPt1uzLndj7tTBLqRyumeX0XRP5jA1UEjLoEHgV6/RDl6+HidM7d5kAEk9UryGzQERQnt2n1UdvMELa6s+8MIhu/Xw+zAxxmULoA5VjbEM93eodqu5jC0DKENkOQhQa8u/PNh4jtKcbQaL+StHCP8nbVpPZW/EKjQQQQiWS4zJ9wPzrdAavkFk7U0gLQKFc9m3+bsN9XXtO88FkpepMfsYKH4+1H/2Ej5JH+ypym9fqp9qIPfdMRcU0Nukq5GMM55Qr76nH9nvP/xWCSq3uMrM9ntbM16cNthJCBzh1t1vLEAMOBmOFJ87EYMMmiFq1LpQva1ZYlgRay8d0697ICNxPVrhYBUvHDMsvrF/i8gepTAhmMFRlLYAIpQ4Rgws9pHlnEBc95qofu5liqjlJ52b/pijzVMTy9OWy1BR4IAjIyRaAswKxszH7sAvummHOMEOlNFD2IKi8oNq7sURD0euyzk8afHjHkZFD+GO64R/Mg6dcGGoeKbDOAPPBJIg5QnL8kyh7P/hSzOzfUpcFPB"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="9642fcc5ee8a9362d8502aeb79639057"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ca666897121a6')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ca666897121a6</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
