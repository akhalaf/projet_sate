<!DOCTYPE html>
<html lang="en">
<head>
<title>Alliance Bank Routing Number - Banks America</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Routing number for Alliance Bank and other details such as contact number, branch location. Alliance Bank routing number is a 9 digit number issued by ABA and thus also called ABA routing number." name="description"/>
<link href="//banks-america.com/favi.png" rel="shortcut icon"/>
<link href="//banks-america.com/routing-number/alliance-bank/" rel="canonical"/>
<!-- Bootstrap core CSS -->
<link href="//banks-america.com/bootstrap/css/bootstrap.min.css?v=1" rel="stylesheet"/>
</head>
<body>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="//banks-america.com"><img src="//banks-america.com/banks-america.png"/></a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li><a href="//banks-america.com/routing-number/">Routing Number</a></li>
<li><a href="//banks-america.com/routing/">All Banks</a></li>
<li><a href="//banks-america.com/credit-union/">All Credit Unions</a></li>
<li><a href="//banks-america.com/swift-code/">Swift Codes</a></li>
<li><a href="//banks-america.com/blog/">Blog</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="//banks-america.com/pages/disclaimer/"><b>Disclaimer</b></a></li>
</ul>
</div><!--/.nav-collapse -->
</div>
</div>
<div class="container">
<h1 class="header">Routing number for Alliance Bank</h1>
<ol class="breadcrumb pan">
<li><a href="//banks-america.com">Home</a></li>
<li><a href="//banks-america.com/routing-number/">All Banks</a></li>
<li><a href="//banks-america.com/routing/alliance-bank/">Alliance Bank</a></li>
<li class="active hidden-xs">Routing Number</li>
</ol>
<style type="text/css">
					.adslot_1 { display:inline-block; width: 728px; height: 15px; }
					@media (max-width:800px) { .adslot_1 { display: none; } }
					</style>
<ins class="adsbygoogle adslot_1" data-ad-client="ca-pub-6724490620352589" data-ad-slot="4462770385"></ins>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>(adsbygoogle = window.adsbygoogle || []).push({});
					</script> <div class="row">
<div class="col-md-8 col">
<div class="panel panel-info pan">
<div class="panel-heading hidden-xs">Routing Number for Alliance Bank</div>
<div class="panel-body">
<ul class="nav nav-tabs" role="tablist"><li><a class="question" href="//banks-america.com/routing/alliance-bank/">Branches</a></li>
<li class="active"><a class="question" href="//banks-america.com/routing-number/alliance-bank/">Routing No.</a></li>
<li><a class="question" href="//banks-america.com/swift-code/alliance-bank/">Swift</a></li>
</ul><div style="float:right;margin:12px;"><br/><style>
			.ba-responsive3 { width: 320px; height: 100px; }
			@media(min-width: 500px) { .ba-responsive3 { width: 300px; height: 250px; } }
			@media(min-width: 800px) { .ba-responsive3 { width: 336px; height: 280px; } }
			</style>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- BA - Responsive2 -->
<ins class="adsbygoogle ba-responsive3" data-ad-client="ca-pub-6724490620352589" data-ad-slot="5411453181" style="display:inline-block"></ins>
<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script><br/></div><br/><strong>Routing number for Alliance Bank</strong> is a 9 digit bank code used for various bank transactions such as direct deposits, electronic payments, wire transfers, check ordering and many more. Routing numbers are also known as bank routing numbers, routing transit numbers (RTNs), ABA numbers, ACH routing numbers. Routing numbers may differ depending on where your account was initially opened and the type of transaction made.<br/><br/>Alliance Bank is a FDIC Insured Bank (Non-member Bank) and its FDIC Certification ID is 11569. The RSSD ID for Alliance Bank is 176464. <br/><br/>The EIN (Employer Identification Number, also called IRS Tax ID) for Alliance Bank is 750591480. <h2>Alliance Bank Routing Number</h2><div class="well well-sm">There are several banks with name of Alliance Bank. Hence the list of Routing Numbers based on their websites and their Locations are as follows:<br/><br/>
<table class="table table-striped table-condensed"><tr><th>Name of the Bank</th>
<th>Official Website</th>
<th>Location</th>
<th>Routing Number</th></tr>
<tr><td>Alliance Bank</td>
<td>www.myalliancebank.com</td>
<td>Indiana</td>
<td>122220593</td></tr>
<tr><td>Alliance Bank</td>
<td>www.alliancebanking.com</td>
<td>Missouri</td>
<td>081518773</td></tr>
<tr><td>Alliance Bank</td>
<td>www.alliancebank.com</td>
<td>Texas</td>
<td>111901975</td></tr>
</table>
</div><h3>Find Alliance Bank Routing Number on a Check</h3>The best way to find the routing number for your Alliance Bank checking, savings or business account is to look into the lower left corner of the bank check.<br/><a href="//banks-america.com/info/wp-content/uploads/2014/08/sample-check-routing.png"><img alt="Alliance Bank routing number on check" src="//banks-america.com/info/wp-content/uploads/2014/08/sample-check-routing.png"/></a><hr/><strong>Find all routing number for Alliance Bank in the below table.</strong><br/><br/><table class="table table-striped table-condensed"><tr><th>Routing Number</th><th>Bank</th><th>Address**</th><th>State, Zip</th></tr><tr><td>074912674</td><td>ALLIANCE BANK</td><td>P.O BOX 188 FRANCESVILLE</td><td>Indiana, 47946</td></tr>
<tr><td>081518773</td><td>ALLIANCE BANK</td><td>217 N KINGS HIGHWAY CAPE GIRARDEAU</td><td>Missouri, 63702</td></tr>
<tr><td colspan="4"><style>
			.ba-responsive2 { width: 320px; height: 100px; }
			@media(min-width: 500px) { .ba-responsive2 { width: 468px; height: 60px; } }
			@media(min-width: 900px) { .ba-responsive2 { width: 728px; height: 90px; } }
			</style>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- BA - Responsive2 -->
<ins class="adsbygoogle ba-responsive2" data-ad-client="ca-pub-6724490620352589" data-ad-slot="5411453181" style="display:inline-block"></ins>
<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script></td></tr><tr><td>091901215</td><td>ALLIANCE BANK</td><td>PO BOX 757 NEW ULM</td><td>Minnesota, 56073</td></tr>
<tr><td>101114798</td><td>ALLIANCE BANK</td><td>3001 SW WANAMAKER RD TOPEKA</td><td>Kansas, 66614</td></tr>
<tr><td>111901975</td><td>ALLIANCE BANK</td><td>408 ROSEMONT SULPHER SPRINGS</td><td>Texas, 75482</td></tr>
<tr><td>053112550</td><td>ALLIANCE BANK &amp; TRUST</td><td>GASTON COUNTY GASTONIA</td><td>North Carolina, 28052</td></tr>
<tr><td>111911156</td><td>ALLIANCE BANK CENTRAL TEXAS</td><td>4721 BOSQUE BLVD WACO</td><td>Texas, 76710</td></tr>
<tr><td><del>111918845</del> 111911156</td><td>ALLIANCE BANK CENTRAL TEXAS</td><td>4721 BOSQUE BLVD WACO</td><td>Texas, 76710</td></tr>
<tr><td>091803355</td><td>ALLIANCE BANK MONDOVI OFFICE</td><td>P.O. BOX 187 MONDOVI</td><td>Wisconsin, 54755</td></tr>
</table>**Address mentioned in the table may differ from your branch office address. Routing number of a bank usually differ only by state and is generally same for all branches in a state.<br/><br/><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Ad Link Resp White -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6724490620352589" data-ad-format="link" data-ad-slot="1619183188" style="display:block"></ins>
<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
<h3><u>Post Questions / Comments Below</u></h3>
<div id="disqus_thread"></div>
<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = 'banksamerica'; // required: replace example with your forum shortname

				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function() {
					var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				})();
			</script>
<noscript>Please enable JavaScript to view the <a href="//disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a class="dsq-brlink" href="//disqus.com">comments powered by <span class="logo-disqus">Disqus</span></a>
</div></div></div>
<div class="col-md-4 col">
<div class="panel panel-info pan">
<div class="panel-heading">Routing Number</div>
<div class="panel-body">
<form action="//www.google.com" id="cse-search-box" target="_blank">
<div>
<input name="cx" type="hidden" value="partner-pub-6724490620352589:4799664380"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input name="q" size="30" type="text"/>
<input name="sa" type="submit" value="Search"/>
</div>
</form>
<script src="//www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en" type="text/javascript"></script>
<h3>Top 30 Banks</h3>
<a class="question" href="//banks-america.com/routing/bancorpsouth-bank/">BancorpSouth Bank</a><br/>
<a class="question" href="//banks-america.com/routing/bank-of-america-na/">Bank of America NA</a><br/>
<a class="question" href="//banks-america.com/routing/bank-of-the-west/">Bank of the West</a><br/>
<a class="question" href="//banks-america.com/routing/bbva-usa/">BBVA USA</a><br/>
<a class="question" href="//banks-america.com/routing/bmo-harris-bank-na/">BMO Harris Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/branch-banking-and-trust-company/">Branch Banking and Trust Company</a><br/>
<a class="question" href="//banks-america.com/routing/capital-one-na/">Capital One NA</a><br/>
<a class="question" href="//banks-america.com/routing/citibank-na/">Citibank NA</a><br/>
<a class="question" href="//banks-america.com/routing/citizens-bank-na/">Citizens Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/comerica-bank/">Comerica Bank</a><br/>
<a class="question" href="//banks-america.com/routing/fifth-third-bank/">Fifth Third Bank</a><br/>
<a class="question" href="//banks-america.com/routing/first-citizens-bank-%26-trust-company/">First Citizens Bank &amp; Trust Company</a><br/>
<a class="question" href="//banks-america.com/routing/first-national-bank-of-pennsylvania/">First National Bank of Pennsylvania</a><br/>
<a class="question" href="//banks-america.com/routing/first-national-bank-texas/">First National Bank Texas</a><br/>
<a class="question" href="//banks-america.com/routing/jpmorgan-chase-bank-na/">JPMorgan Chase Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/keybank-na/">KeyBank NA</a><br/>
<a class="question" href="//banks-america.com/routing/m-and-t-bank/">M and T Bank</a><br/>
<a class="question" href="//banks-america.com/routing/mufg-union-bank-na/">MUFG Union Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/people%27s-united-bank-na/">People's United Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/pnc-bank-na/">PNC Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/regions-bank/">Regions Bank</a><br/>
<a class="question" href="//banks-america.com/routing/santander-bank-na/">Santander Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/suntrust-bank/">SunTrust Bank</a><br/>
<a class="question" href="//banks-america.com/routing/tcf-national-bank/">TCF National Bank</a><br/>
<a class="question" href="//banks-america.com/routing/td-bank-na/">TD Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/the-huntington-national-bank/">The Huntington National Bank</a><br/>
<a class="question" href="//banks-america.com/routing/u.s.-bank-na/">U.S. Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/wells-fargo-bank-na/">Wells Fargo Bank NA</a><br/>
<a class="question" href="//banks-america.com/routing/woodforest-national-bank/">Woodforest National Bank</a><br/>
<a class="question" href="//banks-america.com/routing/zions-bancorporation-na/">Zions Bancorporation NA</a><br/>
<br/><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- banksamerica responsive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6724490620352589" data-ad-format="auto" data-ad-slot="1074141988" style="display:block"></ins>
<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
<h3>Top 15 Credit Unions</h3>
<a class="question" href="//banks-america.com/credit-union/alaska-usa/">Alaska Usa Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/america-first/">America First Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/boeing-employees/">Boeing Employees Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/lake-michigan/">Lake Michigan Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/mountain-america/">Mountain America Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/navy-federal-credit-union/">Navy Federal Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/randolph-brooks/">Randolph Brooks Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/schoolsfirst/">Schoolsfirst Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/security-service/">Security Service Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/space-coast/">Space Coast Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/state-employees%27/">State Employees' Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/suncoast/">Suncoast Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/the-golden-1/">The Golden 1 Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/unify-financial/">Unify Financial Credit Union</a><br/>
<a class="question" href="//banks-america.com/credit-union/vystar/">Vystar Credit Union</a><br/>
</div></div></div>
</div>
</div>
<!--
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="centered-pills">
<ul class="nav nav-pills">

  <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
  <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a></li>
  <li><a href="#"><i class="fa fa-google-plus"></i> Google+</a></li>

</ul></div>
-->
<div class="footer1">
<div align="justify" class="container">
<br/>
<small><strong>ABA Routing Number:</strong> Routing numbers are also referred to as "Check Routing Numbers", "ABA Numbers", or "Routing Transit Numbers" (RTN). The ABA routing number is a 9-digit identification number assigned to financial institutions by The American Bankers Association (ABA). This number identifies the financial institution upon which a payment is drawn. Routing numbers may differ depending on where your account was opened and the type of transaction made.
 Each routing number is unique to a particular bank, large banks may have more than one routing number for different states.</small><br/><br/>
<small><strong>ACH Routing Number:</strong> ACH Routing Number stands for Automated Clearing House (ACH). This routing number is used for electronic financial transactions in the United States. ACH helps to improves payment processing efficiency and accuracy, and reduce expenses. Banks offer ACH services for businesses who want to collect funds and make payments electronically in batches through the national ACH network. ACH routing number is a nine digit number.
The first four digits identify the Federal Reserve district where the bank is located. The next four numbers identify the specific bank. The last number is called as a check digit number which is a confirmation number. ACH Routing Numbers are used for direct deposit of payroll, dividends, annuities, monthly payments and collections, federal and state tax payments etc.</small><br/><br/>
<small><strong>Fedwire Routing Number:</strong> Fedwire Transfer service is the fastest method for transferring funds between business account and other bank accounts.
It is used for domestic or international transactions in which no cash or check exchange is involved, but the account balance is directly debited electronically and the funds are transferred to another account in real time. To complete a wire transfer, the sender must provide his bank name and account number of the recipient, the receiving account number, the city and state of the receiving bank and the bank's routing number.</small><br/><br/>
</div>
</div>
<div class="container">
<ul class="nav nav-pills nav-justified">
<li><a href="//banks-america.com/routing-number/">Routing Number</a></li>
<li><a href="//banks-america.com/swift-code/">Swift Codes</a></li>
<li><a href="//banks-america.com/pages/disclaimer/">Disclaimer</a></li>
<li><a href="//banks-america.com/pages/privacy-policy/">Privacy Policy</a></li>
</ul>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-17301808-27', 'auto');
  ga('send', 'pageview');

</script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//banks-america.com/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
