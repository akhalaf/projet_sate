<!DOCTYPE HTML>
<html><head>
<meta content="text/html" http-equiv="content-type"/>
<title>Blowfish Unlocks</title>
<style>body{font-family: Arial;}</style>
<script src="includes/js/jquery.js" type="text/javascript"></script>
<script src="includes/js/countdown.js" type="text/javascript"></script>
</head>
<body><script type="text/javascript">
$(document).ready(function() {
	$("#time").countdown({
		date: "Jun 15, 2019 04:58",  
        htmlTemplate: "<div class=counter> <p class=specialp><small>If all goes as planned, we hope to be back in: </small></p> <p><span>%{d}  </span><br/> <small>Days</small></p> <p><span>%{h} </span><br/> <small>Hours</small></p> <p> <span>%{m} </span><br/> <small>Minutes</small> </p>  <p><span>%{s} </span><br/> <small>Seconds</small></p><div style='clear: both';></div></div>",
		onChange: function( event, timer ){
		},
		onComplete: function( event ){
			$(this).html("");
		},
		leadingZero: true,
		direction: "down"
	});
});
</script>
<style>
    .outer{width:780px;background: #f4f4f4;padding:40px;margin: auto;-moz-border-radius-topleft: 7px;-moz-border-radius-topright: 7px;-moz-border-radius-bottomright: 0px;-moz-border-radius-bottomleft: 0px;-webkit-border-radius: 7px 7px 0px 0px;border-radius: 7px 7px 0px 0px; }
    .innertext{font-size: 12px;color:#333333;  background: white;padding: 30px;  -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;-webkit-box-shadow: 0px 2px 2px 1px #e3e2e2;-moz-box-shadow: 0px 2px 2px 1px #e3e2e2;box-shadow: 0px 2px 2px 1px #e3e2e2;}    
    .counter{text-align: center;width:780px;background: #f4f4f4 url('images/outer_bg.png') repeat-x top;margin: auto;padding:30px 40px;-moz-border-radius-topleft: 0px;-moz-border-radius-topright: 0px;-moz-border-radius-bottomright: 7px;-moz-border-radius-bottomleft: 7px;-webkit-border-radius: 0px 0px 7px 7px;border-radius: 0px 0px 7px 7px; }    
     .time .counter p{display: block;float: left;text-align: center;margin:0 10px;background: url('images/counter_bg.png') repeat-x top;width: 90px;padding: 10px 0; -webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;}
    .time .counter span{font-weight: bold;color:white;text-shadow:0 1px 1px #666;}
    .time .counter small{margin-top: 5px;display:block;color:white;text-shadow:0 1px 1px #666;}
    .specialp{background:url('images/counter_text_bg.png') repeat-x top !important;width: 270px !important;padding: 20px 20px !important;}
    
    input,textarea{font-family:arial;padding:10px;border:#ddd solid 1px;border-radius:2px;}
    textarea{width:90%;height:100px}
    fieldset{border:none;font-size:15px;padding:0;}
    .control-group{padding:10px 0;text-align: left;}
    .control-label{display: inline-block; width:200px;}
    .controls{}
    .btn{cursor: pointer;}

</style>
<div class="outer">
<div class="innertext"> <span style="font-size: 32px;color:#deb200"> Blowfish Unlocks </span> <div><p>Dear Clients,</p><br/>
<p>We have decided to pursue other interests and have permanently closed down the website.  We thank you for your past patronage.</p><br/>
<p>Blowfish Unlocks</p>
</div></div></div>
<div class="time" id="time"></div>
</body>
</html>