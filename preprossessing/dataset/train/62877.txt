<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>ACBS.COM.VN - Công ty chứng khoán ACBS</title>
<meta content="ACBS - Hệ thống chứng khoán ACBS" name="description"/>
<meta content="ACBC - Công ty chứng khoán" name="keywords"/>
<meta content="ACBS.COM.VN - Công ty chứng khoán ACBS" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="http://www.acbs.com.vn/" property="og:url"/>
<meta content="/static/front/images/logo.png" property="og:image"/>
<meta content="ACBS - Hệ thống chứng khoán ACBS" property="og:description"/>
<meta content="index, follow" name="robots"/>
<meta content="" name="author"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/static/front/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/static/front/css/style.css" rel="stylesheet"/>
<link href="/static/front/css/customize.css" rel="stylesheet"/>
<script src="/static/front/js/lib/respond.js"></script>
<script src="/static/front/js/lib/modernizr-v2.8.3.js"></script>
</head>
<body class="home">
<header class="header">
<div class="header-inner">
<div class="container-logo">
<div class="logo">
<a href="/" title="ACBS.COM.VN - Công ty chứng khoán ACBS">
<img alt="ACBS - ACBS.COM.VN - Công ty chứng khoán ACBS" class="img-responsive invisible_phone" src="/static/front/images/logo.jpg"/>
<img alt="ACBS - ACBS.COM.VN - Công ty chứng khoán ACBS" class="img-responsive invisible_desk" src="/static/front/images/logo-small.jpg"/>
<img alt="ACBS - ACBS.COM.VN - Công ty chứng khoán ACBS" class="img-responsive visible_xphone" src="/static/front/images/logo-xsmall.jpg"/>
</a>
</div>
</div>
<a class="toggle-nav" title="Toggle Nav"><span>Toggle Nav</span></a>
<div class="header-content">
<div class="top-info">
<ul class="list-horizontal">
<li class="invisible_phone"><span class="hotline"><strong>Hotline:</strong> 1900 5555 33</span></li>
<li class="container-search">
<a class="link-search search" title="Tìm kiếm">
<span class="search-btn fa icon-search"></span>
<span class="label-search">Tìm kiếm</span>
</a>
<div class="search-open">
<div class="search-open-inner">
<div class="container">
<a class="btn-close js-btn-close" href="javascript:;"> </a>
<form action="/tim-kiem" class="form-general is-form-search" name="form-search">
<label class="label-site" for="txt-search">Tìm kiếm: </label>
<div class="form-row">
<input class="form-input" id="txt-search" name="search" placeholder="Nhập từ khóa ..." type="text"/>
<input class="btn-search" type="submit" value=""/>
</div>
</form>
</div>
</div>
</div>
</li>
<li class="invisible_phone ">
<a class="lang lang-en" href="/en" title="English">
                                English
                            </a>
</li>
<li class="invisible_phone block-accounts"><a href="http://trade.acbs.com.vn/" title="Tài khoản"><span class="icon-account"></span> <strong>Tài khoản</strong></a></li>
</ul>
</div>
<nav class="nav-main">
<div class="container">
<a href="/"><img alt="ACBS - ACBS.COM.VN - Công ty chứng khoán ACBS" class="img-responsive" src="/static/front/images/logo.png"/></a>
<ul class="nav-list">
<li>
<a href="javascript:;" title="ACBS">ACBS</a>
<div class="container-nav-sub">
<div class="container">
<div class="summary-text">
<h2 class="headline is-headline-small">
        ACBS
    </h2>
<div class="desc">
        Là một trong những công ty chứng khoán hàng đầu trên thị trường Việt Nam, ACBS đáp ứng tất cả các điều kiện của Ủy ban Chứng khoán Nhà nước để thực hiện các nghiệp vụ kinh doanh của công ty chứng khoán.
    </div>
</div>
<div class="content-nav-sub">
<ul class="nav-list is-nav-sub-list">
<li>
<a href="/danh-muc/50/gioi-thieu-chung" title="Giới thiệu chung">Giới thiệu chung</a>
<a href="/cong-bo-thong-tin" title="Công bố thông tin">Công bố thông tin</a>
</li><li> <a href="/co-hoi-nghe-nghiep" title="Cơ hội nghề nghiệp">Cơ hội nghề nghiệp</a>
<a href="/to-chuc" title="Tổ chức">Tổ chức</a>
</li><li> <a href="/danh-muc/44/acbs" title="Tin tức và sự kiện">Tin tức và sự kiện</a>
<a href="/dia-chi" title="Mạng lưới hoạt động">Mạng lưới hoạt động</a>
</li>
</ul>
</div>
</div>
</div>
</li>
<li>
<a href="javascript:;" title="Sản phẩm &amp; Dịch vụ">Sản phẩm &amp; Dịch vụ</a>
<div class="container-nav-sub">
<div class="container">
<div class="summary-text">
<h2 class="headline is-headline-small">
        Sản phẩm &amp; Dịch vụ
    </h2>
<div class="desc">
        Với phương châm “Chỉ có bạn” của toàn tập đoàn, ACBS đảm bảo cung cấp các dịch vụ và giải pháp tài chính tốt nhất cho các khách hàng trên cơ sở nghiên cứu kỹ lưỡng nhu cầu của từng khách hàng.
    </div>
</div>
<div class="content-nav-sub">
<ul class="nav-list is-nav-sub-list">
<li>
<a href="/thong-tin-dich-vu/54-khach-hang-dinh-che" title="Khách hàng định chế">Khách hàng định chế</a>
<a href="/thong-tin-dich-vu/52-khach-hang-ca-nhan" title="Khách hàng cá nhân">Khách hàng cá nhân</a>
</li><li> <a href="/thong-tin-dich-vu/53-khach-hang-doanh-nghiep" title="Khách hàng doanh nghiệp">Khách hàng doanh nghiệp</a>
<a href="/thong-tin-dich-vu/58-dich-vu-truc-tuyen" title="Dịch vụ trực tuyến">Dịch vụ trực tuyến</a>
</li><li> <a href="/danh-muc/71/ban-tin-dau-gia" title="Thông tin đấu giá">Thông tin đấu giá</a>
<a href="/thong-tin-dich-vu/60-cong-ty-quan-ly-quy" title="Công ty quản lý quỹ">Công ty quản lý quỹ</a>
</li>
</ul>
</div>
</div>
</div>
</li>
<li>
<a href="javascript:;" title="Trung tâm phân tích">Trung tâm phân tích</a>
<div class="container-nav-sub">
<div class="container">
<div class="summary-text">
<h2 class="headline is-headline-small">
        Trung tâm phân tích
    </h2>
<div class="desc">
        ACBS  cung cấp đến khách hàng những nhận định chuyên sâu, độc lập, khách quan về kinh tế vĩ mô, doanh nghiệp niêm yết, thị trường trái phiếu nhằm phục vụ nhu cầu đầu tư đa dạng của khách hàng.
    </div>
</div>
<div class="content-nav-sub">
<ul class="nav-list is-nav-sub-list">
<li>
<a href="/danh-muc/70/ban-tin-cafe-sang" title="Chatty Stocks">Chatty Stocks</a>
<a href="/danh-muc/87/bao-cao-doanh-nghiep" title="Báo cáo cập nhật doanh nghiệp">Báo cáo cập nhật doanh nghiệp</a>
</li><li> <a href="/danh-muc/85/phan-tich-ky-thuat" title="Bản tin Lướt Sóng">Bản tin Lướt Sóng</a>
<a href="/danh-muc/88/tin-trai-phieu" title="Báo cáo ngành">Báo cáo ngành</a>
</li><li> <a href="/danh-muc/89/flash-news" title="Flash news">Flash news</a>
<a href="/danh-muc/90/goc-nhin-chuyen-gia" title="Góc nhìn chuyên gia">Góc nhìn chuyên gia</a>
</li>
</ul>
</div>
</div>
</div>
</li>
<li>
<a href="javascript:;" title="Hỗ trợ khách hàng">Hỗ trợ khách hàng</a>
<div class="container-nav-sub">
<div class="container">
<div class="summary-text">
<h2 class="headline is-headline-small">
        Hỗ trợ khách hàng
    </h2>
<div class="desc">
        Trang hỗ trợ chính thức của ACBS để hướng dẫn, giải đáp và  nhận ý kiến góp ý của khách hàng khi sử dụng dịch vụ, sản phẩm của công ty chúng tôi.
    </div>
</div>
<div class="content-nav-sub">
<ul class="nav-list is-nav-sub-list">
<li>
<a href="/y-kien-khach-hang" title="Ý kiến khách hàng">Ý kiến khách hàng</a>
<a href="/huong-dan" title="Hướng dẫn sử dụng">Hướng dẫn sử dụng</a>
</li><li> <a href="/trang/266" title="Danh mục chứng khoán">Danh mục chứng khoán</a>
<a href="/khao-sat" title="Khảo sát">Khảo sát</a>
</li><li> <a href="/trang/265" title="Biểu phí">Biểu phí</a>
</li>
</ul>
</div>
</div>
</div>
</li>
<li class="invisible_desk ">
<a class="lang lang-en" href="/en" title="English"><strong>English</strong></a>
</li>
<li class="invisible_desk active">
<strong>Tiếng Việt</strong>
</li>
<li class="invisible_desk block-accounts"><a class="btn btn-blue btn-account" href="#" title="Tài khoản"><span class="icon-account"></span> <strong>Tài khoản</strong></a></li>
</ul>
</div>
</nav>
</div>
</div>
</header>
<section class="main">
<div class="site-top">
<div class="panel is-panel-fullsite panel-main-top">
<div class="panel-inner">
<div class="panel-header">
<h2 class="headline">Sản phẩm <strong>dịch vụ</strong></h2>
</div>
<div class="panel-body">
<div class="banner-carousel">
<div class="item">
<div class="item-content">
<a class="thumb" href="/thong-tin-dich-vu/54-khach-hang-dinh-che" title="Khách Hàng Định Chế">
<img alt="Khách Hàng Định Chế" src="/upload/images/KH ca nhan.jpg"/>
</a>
<h3 class="headline is-headline-medium">
<a class="thumb" href="/thong-tin-dich-vu/54-khach-hang-dinh-che" title="Khách Hàng Định Chế">
                                           Khách hàng định chế
                                        </a>
</h3>
<p>Với vị thế là một công ty chứng khoán hàng đầu, ACBS đã, đang và sẽ tiếp tục là đối tác tin cậy của khách hàng định chế trong hoạt động đầu tư tại Việt Nam.</p>
</div>
</div>
<div class="item">
<div class="item-content">
<a class="thumb" href="/thong-tin-dich-vu/53-khach-hang-doanh-nghiep" title="Khách Hàng Doanh Nghiệp">
<img alt="Khách Hàng Doanh Nghiệp" src="/upload/images/KH doanh nghiep.jpg"/>
</a>
<h3 class="headline is-headline-medium">
<a class="thumb" href="/thong-tin-dich-vu/53-khach-hang-doanh-nghiep" title="Khách Hàng Doanh Nghiệp">
                                           Khách hàng doanh nghiệp
                                        </a>
</h3>
<p>ACBS luôn mong muốn góp phần vào sự thành công của các doanh nghiệp với tư cách là nhà tư vấn và thu xếp tài chính đáng tin cậy và hiệu quả.</p>
</div>
</div>
<div class="item">
<div class="item-content">
<a class="thumb" href="/thong-tin-dich-vu/52-khach-hang-ca-nhan" title="Khách Hàng Cá Nhân">
<img alt="Khách Hàng Cá Nhân" src="/upload/images/KH dinh che.jpg"/>
</a>
<h3 class="headline is-headline-medium">
<a class="thumb" href="/thong-tin-dich-vu/52-khach-hang-ca-nhan" title="Khách Hàng Cá Nhân">
                                           Khách hàng cá nhân
                                        </a>
</h3>
<p>ACBS phát triển những sản phẩm dịch vụ tài chính tối ưu, linh hoạt và đáp ứng nhu cầu của từng khách hàng.</p>
</div>
</div>
</div>
<div class="control-action is-control-scroll">
<a class="link-scroll-page-down" href="#" title="scroll down"><span class="fa fa-angle-down"></span></a>
</div>
</div>
</div>
</div><!-- //panel-main-top -->
<div class="panel is-panel-fullsite panel-hotnews">
<div class="panel-before"></div>
<div class="panel-inner">
<div class="row">
<div class="col-md-12">
<div class="panel-title">
<a href="/danh-muc/44/acbs"><h2 class="headline">Tin nổi bật</h2></a>
</div>
</div>
<div class="col-md-3">
<a class="event-url" href="http://www.acbs.com.vn/tin-tuc/n-a-4648-44" title="Thông báo mua lại trước hạn Trái phiếu ACB (Mã TPACB0216, TPACB0316, TPACB0416)"><h3 class="event-title">Thông báo mua lại trước hạn Trái phiếu ACB (Mã TPACB0216, TPACB0316, TPACB0416)</h3></a>
<time class="date-publish">02-12-2020 13:15</time>
</div>
<div class="col-md-3">
<a class="event-url" href="http://www.acbs.com.vn/tin-tuc/n-a-4642-44" title="Bản cáo bạch Niêm yết HOSE của ACB và các Phụ lục đính kèm."><h3 class="event-title">Bản cáo bạch Niêm yết HOSE của ACB và các Phụ lục đính kèm.</h3></a>
<time class="date-publish">25-11-2020 14:14</time>
</div>
<div class="col-md-3">
<a class="event-url" href="http://www.acbs.com.vn/tin-tuc/n-a-4604-44" title="Thông báo v/v Tạm ứng cổ tức đợt 2 năm 2020 của Công ty cổ phần Đầu tư phát triển nhà Hà Nội số 5"><h3 class="event-title">Thông báo v/v Tạm ứng cổ tức đợt 2 năm 2020 của Công ty cổ phần Đầu tư phát triển nhà Hà Nội số 5</h3></a>
<time class="date-publish">28-10-2020 15:01</time>
</div>
<div class="col-md-3">
<a class="event-url" href="http://www.acbs.com.vn/tin-tuc/n-a-4554-44" title="THÔNG BÁO VỀ VIỆC NHẬN GIẤY CHỨNG NHẬN SỞ HỮU CHỨNG KHOÁN ĐỢT PHÁT HÀNH CỔ TỨC BẰNG CỔ PHIẾU 2019 ĐỐI VỚI CỔ ĐÔNG CHƯA LƯU KÝ CỔ PHIẾU ACB"><h3 class="event-title">THÔNG BÁO VỀ VIỆC NHẬN GIẤY CHỨNG NHẬN SỞ HỮU CHỨNG KHOÁN ĐỢT PHÁT HÀNH CỔ TỨC BẰNG CỔ PHIẾU 2019 ĐỐI VỚI CỔ ĐÔNG CHƯA LƯU KÝ CỔ PHIẾU ACB</h3></a>
<time class="date-publish">18-09-2020 16:27</time>
</div>
</div>
</div>
</div>
<div class="panel is-panel-fullsite panel-main-infos">
<div class="panel-before"></div>
<div class="panel-inner">
<div class="row">
<div class="col-md-8 col-sm-12 col-left col-access">
<div class="panel-title">
<h2 class="headline">Truy cập <strong>nhanh</strong></h2>
</div>
<div class="panel-body">
<div class="slider-fast-access-acbs">
<div class="item">
<a class="link-download-app cboxElement" href="#modal-download-app" title="Tải ứng dụng giao dịch">
<span class="wi-icon icon-1"></span>
<strong class="label">Tải ứng dụng giao dịch</strong>
</a>
</div>
<div class="item">
<a href="http://trade.acbs.com.vn/" title="Truy cập web giao dịch">
<span class="wi-icon icon-2"></span>
<strong class="label">Truy cập web giao dịch</strong>
</a>
</div>
<div class="item">
<a href="/huong-dan/1912-n-a" title="Tải hướng dẫn sử dụng">
<span class="wi-icon icon-3"></span>
<strong class="label">Tải hướng dẫn sử dụng</strong>
</a>
</div>
<div class="item">
<a href="http://info.acbs.com.vn/" title="Bảng giá trực tuyến">
<span class="wi-icon icon-4"></span>
<strong class="label">Bảng giá trực tuyến</strong>
</a>
</div>
<div class="item">
<a href="/trang/265" title="Biểu phí giao dịch">
<span class="wi-icon icon-5"></span>
<strong class="label">Biểu phí giao dịch</strong>
</a>
</div>
</div><!-- //slider-fast-access-acbs -->
</div>
</div>
<div class="col-md-4 col-sm-12 col-right">
<div class="container-video">
<a class="youtube" href="http://acb.com.vn/" title=".">
<img alt="." src="http://acbs.com.vn/upload/images/acb1.jpg"/>
<div class="home-video-title">.</div>
</a>
</div>
</div>
</div>
</div>
</div> <!-- //panel-main-infos -->
</div> <div class="panel is-panel-fullsite panel-highlight-content">
<div class="panel-inner">
<div class="row">
<div class="col-md-8 col-sm-12 col-left">
<div class="row-analytic">
<h2 class="headline">Báo cáo <strong>phân tích</strong></h2>
<a class="btn btn-blue btn-view-detail" href="/danh-muc/70-ban-tin-cafe-sang">Xem tất cả<span class="fa fa-angle-right"></span></a>
</div>
<div class="row">
<div class="col-md-4 col-sm-12 col-summary">
                        Chúng tôi cung cấp các báo cáo phân tích chuyên sâu và độc lập cho các doanh nghiệp niêm yết với tổng giá trị vốn hóa lên tới 70% tổng giá trị vốn hóa của hai sàn HSX và HNX nhằm đảm bảo cung cấp nhà đầu tư cái nhìn toàn diện về thị trường cũng như tìm kiếm thông tin.
                    </div>
<div class="col-md-4 col-sm-12 col-news">
<a class="thumb" href="/tin-tuc/n-a-4708-70" title="Bản tin Chatty Stocks 08/01/2021">
<img alt="Bản tin Chatty Stocks 08/01/2021" src="http://www.acbs.com.vn/media/cache/thumb/upload/photos/post/antrongtam-2D_093508_094646.jpg"/></a>
<h4 class="title"><a href="/tin-tuc/n-a-4708-70" title="Bản tin Chatty Stocks 08/01/2021">Bản tin Chatty Stocks 08/01/2021</a></h4>
<p class="datetime">08-01-2021 09:46</p>
<a class="thumb" href="/tin-tuc/n-a-4707-70" title="Bản tin Chatty Stocks 06/01/2021">
<img alt="Bản tin Chatty Stocks 06/01/2021" src="http://www.acbs.com.vn/media/cache/thumb/upload/photos/post/antrongtam-2D_093508_094515.jpg"/></a>
<h4 class="title"><a href="/tin-tuc/n-a-4707-70" title="Bản tin Chatty Stocks 06/01/2021">Bản tin Chatty Stocks 06/01/2021</a></h4>
<p class="datetime">08-01-2021 09:44</p>
</div>
<div class="col-md-4 col-sm-12 col-list-news">
<ul class="list-data-verticle list-latest-news">
<li>
<h4 class="title"><a href="/tin-tuc/n-a-4709-71" title="Thông Báo Bán Đấu Giá: Công Ty Mẹ - Tổng Công Ty Phát Điện">Thông báo bán đấu giá: Công ty mẹ - Tổng Công ty Phát điện - 08/01/2021</a></h4>
<p></p>
</li>
<li>
<h4 class="title"><a href="/tin-tuc/n-a-4705-71" title="Thông Báo Không Tổ Chức Phiên Bán Đấu Giá Cả Lô Cổ Phần Của Tổng Công Ty Đầu Tư Và Kinh Doanh Vốn Nhà Nước Tại Tổng Công Ty Công Nghiệp Dầu Thực Vật Việt Nam – Ctcp">THÔNG BÁO KHÔNG TỔ CHỨC PHIÊN BÁN ĐẤU GIÁ CẢ LÔ CỔ PHẦN CỦA TỔNG CÔNG TY ĐẦU TƯ VÀ KINH DOANH VỐN NHÀ NƯỚC TẠI TỔNG CÔNG TY CÔNG NGHIỆP DẦU THỰC VẬT VIỆT NAM – CTCP - 08/01/2021</a></h4>
<p></p>
</li>
<li>
<h4 class="title"><a href="/tin-tuc/n-a-4680-71" title="Công Ty Cổ Phần Xây Dựng Và Giao Thông Bình Dương">Công ty Cổ phần Xây dựng và Giao thông Bình Dương - 29/12/2020</a></h4>
<p></p>
</li>
<li>
<h4 class="title"><a href="/tin-tuc/n-a-4678-71" title="Thông Báo Gia Hạn Thời Gian Cbtt Và Điều Chỉnh Thời Gian Tổ Chức Đấu Giá Cổ Phần Tại Vocarimex.">Thông báo gia hạn thời gian CBTT và điều chỉnh thời gian tổ chức đấu giá cổ phần tại Vocarimex. - 21/12/2020</a></h4>
<p></p>
</li>
<li>
<h4 class="title"><a href="/tin-tuc/n-a-4674-71" title="Không Tổ Chức Phiên Bán Đấu Giá Cả Lô Cổ Phần Của Công Ty Cổ Phần Phát Triển Nhà Xã Hội Hud.vn Do Tổng Công Ty Đầu Tư Phát Triển Nhà Và Đô Thị Sở Hữu">KHÔNG TỔ CHỨC PHIÊN BÁN ĐẤU GIÁ CẢ LÔ CỔ PHẦN CỦA CÔNG TY CỔ PHẦN PHÁT TRIỂN NHÀ XÃ HỘI HUD.VN DO TỔNG CÔNG TY ĐẦU TƯ PHÁT TRIỂN NHÀ VÀ ĐÔ THỊ SỞ HỮU - 16/12/2020</a></h4>
<p></p>
</li>
</ul>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-right">
<div class="bg-blue">
<h2 class="headline"><a href="/lich-su-kien">Lịch <strong>Sự kiện</strong></a></h2>
<div class="table-custom table-event-calendar">
<table cellpadding="0" cellspacing="0" class="table">
</table>
</div>
</div>
</div>
</div>
</div>
</div> <div class="panel is-panel-fullsite panel-about-acbs">
<div class="panel-inner">
<div class="row">
<div class="col-md-6 col-img-acbs">
<a alt="ACBS Top Trader" href="http://trade.acbs.com.vn/" target="_blank">
<img alt="" class="img-responsive" src="/upload/images/PICTURE/phaisinh.png"/>
</a>
</div>
<div class="col-md-6 col-info-acbs">
<div class="panel-header">
<h2 class="headline">Hệ thống <strong>Giao dịch Chứng khoán Phái sinh</strong></h2>
</div>
<div class="panel-body">
<p>Từ ngày 8/10/2018, Công ty Chứng khoán ACB chính thức vận hành Hệ thống Giao dịch Chứng khoán Phái sinh</p>
<p>- <strong>Thông minh</strong><br/>- <strong>Ổn định</strong><br/>- <strong>Bảo mật</strong></p>
<p>Chi tiết truy cập: <a href="http://trade.acbs.com.vn/">trade.acbs.com.vn</a></p>
</div>
</div>
</div>
</div>
</div> <div class="panel is-panel-green panel-newsletter">
<div class="panel-inner">
<form action="/newsletter-ajax" class="form-site form-newsletter" id="form-newsletter" method="post">
<div class="text-note invisible_phone" data-errors="E-mail đã tồn tại, bạn vui lòng nhập e-mail mới." data-errors-nothing="Có lỗi xảy ra" data-succsec="E-mail của bạn được gửi đi thành công." id="succsec-email">
</div>
<div class="form-row">
<label class="form-label" for="email">Đăng ký bản tin</label>
<div class="text-note invisible_desk">Báo cáo phân tích, lịch sự kiện và nhiều hơn nữa - đăng ký nhận bản tin của chúng tôi</div>
<div class="form-element">
<input class="form-input" data-bvalidator="required,alpha" data-bvalidator-email="Please enter a valid email." data-bvalidator-required="Please enter your email." id="contact_newsletter_email" name="contact_newsletter[email]" placeholder="Vui lòng nhập email." required="required" type="email"/>
<input class="btn btn-register-newsletter" type="submit" value="Đăng ký"/>
<div class="is-form-input-error" id="err_email"></div>
<input id="contact_newsletter__token" name="contact_newsletter[_token]" type="hidden" value="McqTmY0SVDEh0XtUMKkbf7OPLmF7jNGAYflzCuZEpjM"/>
</div>
</div>
<div class="text-note invisible_phone">Báo cáo phân tích, lịch sự kiện và nhiều hơn nữa - đăng ký nhận bản tin của chúng tôi</div>
</form>
</div>
</div> </section>
<footer class="footer">
<!-- start .footer -->
<div class="control-page">
<div class="container">
<a class="link-hide-page-infos" href="#" title="Ẩn thông tin trang"><span class="label-link">Ẩn thông tin trang </span><span class="fa fa-angle-down"></span></a>
<a class="link-scroll-top" href="#" title="Về đầu trang"><span class="label-link">Về đầu trang </span><span class="fa fa-angle-up"></span></a>
</div>
</div>
<div class="control-social">
<div class="container">
<ul class="list-socialmedia">
<li>Follow us: </li>
<li><a href="https://www.facebook.com/acbs" title="facebook"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" title="twitter"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" title="youtube"><i class="fa fa-youtube"></i></a></li>
<li><a href="#" title="google-plus"><i class="fa fa-google-plus"></i></a></li>
</ul>
</div>
</div>
<!-- FOOTER CONTENT  -->
<!-- start .footer -->
<div class="footer-content">
<div class="container">
<div class="row">
<div class="col-md-2 col-xs-6">
<h4 class="headline is-headline-small">Thông tin website</h4>
<ul class="nav-list">
<li><a href="/danh-muc/50/gioi-thieu-chung" title="Giới thiệu">Giới thiệu</a></li>
<li><a href="/to-chuc" title="Tổ chức">Tổ chức</a></li>
<li><a href="/cong-bo-thong-tin" title="Công bố thông tin">Công bố thông tin</a></li>
<li><a href="/co-hoi-nghe-nghiep" title="Tuyển dụng">Tuyển dụng</a></li>
<li><a href="/lien-he" title="Liên hệ">Liên hệ</a></li>
</ul>
</div>
<div class="col-md-2 col-xs-6">
<h4 class="headline is-headline-small">Hỗ trợ</h4>
<ul class="nav-list">
<li><a href="/dia-chi" title="Mạng lưới">Mạng lưới</a></li>
<li><a href="/trang/268" title="Tin quảng cáo">Tin quảng cáo</a></li>
<li><a href="/trang/222" title="Điều khoản">Điều khoản</a></li>
<li><a href="/so-do-trang" title="Sơ đồ">Sơ đồ</a></li>
</ul>
</div>
<div class="col-md-4 col-xs-12">
<h4 class="headline is-headline-small">Trụ sở chính</h4>
<div class="address-copy">
<p>41 Mạc Đĩnh Chi, phường Đa Kao, quận 1, TP. HCM</p>
<p>H: 1900 5555 33 – E: <a href="mailto:acbs@acbs.com.vn">acbs@acbs.com.vn</a></p>
<p>T: (028) 38234159 – F: (08) 38235060</p>
<div class="terms-copy">Chịu trách nhiệm quản lý nội dung: <br/>TGĐ Ông <strong>Trịnh Thanh Cần</strong></div>
<div class="terms-copy">Tên cơ quan chủ quản: Ngân hàng TMCP Á Châu<br/> Giấy phép số 46/GP-TTĐT ngày cấp 08/05/2015 Sở Thông tin và Truyền thông Tp.HCM</div>
<div class="control-action"><a class="btn btn-green btn-network" href="/dia-chi" title="Mạng lưới hoạt động">Mạng lưới hoạt động </a></div>
</div>
</div>
<div class="col-md-4 col-xs-12">
<div class="container-map"><a href="/dia-chi"><img alt="" class="img-responsive" height="267" src="/upload/images/PICTURE/map.png" width="383"/></a></div>
</div>
</div>
</div>
</div>
<div class="bottom">
<div class="container">
<p class="brand">Bloomberg: ACBS &lt; GO &gt;</p>
<p class="copyright">Bản quyền © 2015 ACBS</p>
<ul class="list-socialmedia">
<li>Mạng xã hội: </li>
<li><a href="https://www.facebook.com/acbs" title="facebook"><span class="fa fa-facebook"></span></a></li>
<li><a href="#" title="twitter"><span class="fa fa-twitter"></span></a></li>
<li><a href="https://www.youtube.com/watch?v=GeZxueBlGlI&amp;feature=youtu.be" title="youtube"><span class="fa fa-youtube"></span></a></li>
<li><a href="https://google.com/#" title="google-plus"><span class="fa fa-google-plus"></span></a></li>
</ul>
</div>
</div>
<!-- end .footer -->
<!-- start area modal -->
<div class="modal is-modal-download-app" id="modal-download-app">
<div class="modal-inner">
<a href="javascript:;" title="close"><span class="icon-close"></span></a>
<div class="modal-header">
<h2 class="headline">Tải ứng dụng giao dịch</h2>
</div>
<div class="modal-body">
<ul class="list-app-download">
<li>
<a href="http://vip.acbs.com.vn/vip/VIP.application?siteid=ACBS" title="Tải ứng dụng ACBS Trading cho PC"><img src="/static/front/images/windows-vn.jpg"/></a>
</li>
<li>
<a href="https://itunes.apple.com/us/app/acbs-trade/id1140840220?ls=1&amp;mt=8" title="Tải ứng dụng ACBS Trading cho iOS"><img src="/static/front/images/ios-vn.jpg"/></a>
</li>
<li>
<a href="https://play.google.com/store/apps/details?id=com.afe_solutions.vn.ACBStrade" title="Tải ứng dụng ACBS Trading cho Android"><img src="/static/front/images/android-vn.jpg"/></a>
</li>
</ul>
</div>
</div>
</div><!-- is-modal-download-app -->
<!-- end area modal -->
</footer>
<script src="/static/front/js/lib/jquery.js"></script>
<script src="/static/front/js/form.js"></script>
<script src="/static/front/js/lib/plugin.js"></script>
<script src="/static/front/js/main.js"></script>
<script type="text/javascript">
            jQuery(document).ready(function() {
                    function beeLeft() {
                        jQuery(".link-scroll-page-down span").animate({
                            bottom: '-=10'
                        }, 350, "linear", beeRight);
                    }
                    function beeRight() {
                        jQuery(".link-scroll-page-down span").animate({
                            bottom: '+=10'
                        }, 350, "linear", beeLeft);
                    }
                    beeRight();
            });
        </script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3ioWBzbnHwZh7HIigEFcaUw6VItDvu0s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>
