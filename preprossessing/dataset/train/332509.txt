<!DOCTYPE html>
<html lang="es_ES">
<head>
<title>
            Acceso - Clientes Spain Is More	</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<link href="/imgs/favicon.ico" rel="shortcut icon"/>
<script src="/js/cg-funciones.js"></script>
<link href="/area_clientes/css/normalize.min.css?1571693371" rel="stylesheet" type="text/css"/>
<link href="/area_clientes/css/lineal.css?1579738007" media="screen" rel="stylesheet" type="text/css"/>
<link href="/area_clientes/css/tablet.css?1578136363" media="screen and (min-width: 768px)" rel="stylesheet" type="text/css"/>
<link href="/area_clientes/css/pc.css?1574385914" media="screen and (min-width: 1000px)" rel="stylesheet" type="text/css"/>
<!--<link rel="stylesheet" href="/area_clientes/css/general-clientes.css?1571731415"  type="text/css">-->
<!--<link href="/css/general.css" rel="stylesheet" type="text/css">-->
<link href="/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery-1.6.2.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,400,400i,600,600i,700,700i,900,900i" rel="stylesheet"/>
<script src="/js/jquery-ui-1.8.16.custom.min.js"></script>
<script src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script src="https://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
</head>
<body>
<main class="mLogin">
<h1>
<a href="https://clientes.spainismore.com" id="logoinfo" tabindex="0" title="Ir a la página principal">
<img alt="Spain Is More" src="/imgs/logo.svg"/>
</a>
</h1>
<h2>Acceso de clientes</h2>
<form action="https://clientes.spainismore.com/ac/es" id="form_login" method="post" name="form_login">
<label id="usuario">
<span>Usuario:</span>
<input autofocus="" name="usuario" tabindex="1" type="text" value=""/>
</label>
<label id="contrasena">
<span>Contraseña:</span>
<input name="password" tabindex="2" type="password" value=""/>
</label>
<div class="botonera">
<button name="submit" tabindex="3" type="submit" value="1">
                    Acceso a "Mi Viaje"                    <small>Ver tu viaje, facturas, realizar pagos, etc.</small>
</button>
<button tabindex="4" type="button"><a href="/es/recuperar-password">
                    ¿Olvidó su contraseña?                    <small>Haga click aquí para recuperarla</small></a>
</button>
</div>
</form>
</main>
</body>
</html>