<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "81995",
      cRay: "6108caec7a85d1d7",
      cHash: "4bad191c94b3440",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuc2lrYXlldHZhci5jb20vYmFuay1hc3lhJTA5JTBB",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "WrZzNTW+HR1PchhvwEFMBxTSNeBVtKapmSM617GMo8/I9IblyX4utajgdPc7WBOoeiRqwYWcs7aI7Sr3lNsDJF81aHo0Y1/RNLge1qnIqMMcxJpzVpEBdoseSSO1gZwLj6XY+7WmUkcim/TJQQZKcoMN1JAg6AEtsj9cRjSdswDx43EteSQPzinYiQYcD6iplo468bCDczyklsGkVp3dPnKgVGjLOgdDGbvETuxZgylX/aMONzUDPLM2sG33ZGG6NOWmr9xHcWHcsjyBJ6NwBflonN6u49lwir2C70s7DC+hy5jAOgLYI1WUhvPVHp8PiHzqE1dPE4zfl8kpXTAIrleDMIMjIxGdS0uIIAtG1l5TtWICTjP/XWFPVftMnXTdqjy0u7yZWuyaKCMQpFQEP0BdC2j652HBhmSGxJubldBMg4NBhMvBTjUUKISyo72105Kwx6t2dBIUbFsrHrmLwwE8vTdM04hn2fPx9uxJXHWA+fgWKxK5uq31fL2OZ1Ny1GjQF9TEl7ug3NkX0zNrBkdj7FSEjIlm5B7UPbt7zu4Qj0QCbCwFPgDBoJ3ELThMRBK2t+hnSFLmXBVq3t3n9DJZ0TCvhh4gfrPH1IQuOGveRQP7ULqlqOcnlBIJM6NdXAsu/mOJYE8XH3QUHkIAjLXSv9u2s4CfyrVrj+sipPXUklyTXQy7KmQc+mC9AMyL9pkQaO5kcA/mKkPDlAbQnrBcndHHZts/g2j73JzAZ6Y4MfWMwVnHlQLbPBJXKhlLtxBSQvSKb9aLnAJppmiJlQ==",
        t: "MTYxMDQ3NDU5MC4xNjYwMDA=",
        m: "5hCa6zKTSoX0E+OTzxivan3BEix7Mx1LJ78Zzp0qy8s=",
        i1: "akeFa2KYBe+oHd3023TUGw==",
        i2: "k0Q3b/nl0FwDTVtmtkVdFQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "dvSwrQ0scL2QT9ciKcklD5z/iW0piS9WYyk0rXNJFzo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.sikayetvar.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/bank-asya%09%0A?__cf_chl_captcha_tk__=34300b31ef21a404eb3a6329a407623de662d30a-1610474590-0-AQd5vl47vQBqKa1mjPpGWdNQvu1PiEeAS9P4E3sca2FmdKgm7ldj3DA9-xcCzfjxfdma6TMl4c2Z2swsW7c_fRJF8_mIZzxRoBs_9oajGgIFEHoUNBg5AKmXxluOPqS52fOgRA1WUlo8cyksbFLraWjTkxPVclFSF9PBRZzFhQhTKAoFulwwXhfrE1fbcBb29AIR3Q6tm3J5qKt-JrGKMWQn-sKnzXzGWpC-LsUAFqEJA6EIh5yqAywfkB8d-rjW2JpzbKIaZHtOxEtuSoVTw0t4uzPx4_WXkkHgZp7_5jbOsOhsdFPot28DlI2sH_OAoEY0KwQcrPJgD-94XkO84qgt3vpMyNrtB7waSXjb7OyD12U1L6DwfxFh1Or-AX7Q2Mnns5R9pVG53URX2k6gpp54rW9vdttoNw58EXVf0I482NdjUomtcoaomWdP_9b7fYpAWmTOTkgZMIn_fSgaEjd9ys4qIMSAYPrFjwLrqPggGxL_AJBfi60mifDJqiQhHgt5_iqWpSzV693FPLR7tbxo_D7IM2vo9VTrKy1CJUuLuCRIgF50bZv7hSMItXsNnFyO5MpOBgazxSqApERSVKQ" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="78c79f8fbaa9a6e7fe17526c18c86e5848ca06ef-1610474590-0-AZiMl1K8UAGRFIoprStIDmDvGjUcgY/TZ99fm8vbPgr7ZPIg06ICBpW7WiHqi0q0xUTdsz/7jKsZMS39FGE9N1Kfor3/o+w6c/KLr7rp7gW1dL6FRzofTUGbOQjSLXbCYfbLjsLp7T36RMDSGV92wN+RkcUMKoH//lMJrBEtxbOsLtfsLU8Lfk+zu/jpWgQD3+U8my0OPtF9gbVK74s+NUewCJk2sJmpY03vOq0+0i1EBVIyNDtBwhpySn5Cf7Z3EBdJ+Xj8GpC6d1vhR+Qiq7gbo0cFvnJlhV1CmxqlA2/AK2/P1LNWmgiE6AnfvNmH/QByMCTlUlV+OhsVUpYEWV5Cjgb/eZk9cUS1ZD/zfi7mdKdiJKTVc0MRCuA1aOskqe4CglSx5uzjbG7nm27Iiz56v6jg8AhtrL7rHhMbA0H2C1SLObpbP/dkfWzMwFdQdeILjqIuJPkmMCKiAKtjx76eglVc8gEAizdEr87i1YoY0CtcnHH6Rt0K+mUjfiXCVdGVebEQakecdrfdulkOHYLnTu2TqH8yw+rgL6GH66OO8+fhm2QhfKA+Fvq780cCYvR5itU/3Mh2n5juv/xGx7uOkhZeJMWNKTwgMNNeiXTX5IwUWCLVrZCA5+W3jfrkR1yN8WNcTAiwi1KsmpzMhbACXtYGnB7gpXYvArhH6xHlnWayXqocK5vQfwLNz1VYVixTk1NiHh24Cw9gSga449lf8HTGSJgUUXkW4LAu9YVDc3yQ69iaRveLGb9YMkFV3VvMTED+6thDiAxyZMPaZR4QNISDA97miqj3W7la3of+zLYxvJTRgWer/ULyp1vPd3e/pFlOdQeTnJ7hHGanGbkrjTFQC+xjOf5g62tOC8vu25hHEh/GILq4phYBzNmjJkfb4HHCw2GX+Zx62iKafZWKX4tDJGrrMzBKKanr3QJ3VEqvegWqnW/baN+eARhMJqKKgS5oGveS0YZFxS/vsnXis2XD0x7DqlleVbouPcES70j0avkwn29P986NiQyFyAMnBtx4vYooC603jANBDIBITR6B07z0gupQ6Z5A/Qz/PN6iOLtTIRnNHD2dvY5DBE+qKjEABZfmx6930hqYi5q/sgvAwmtvFalkB4ZYX3jqTpyuAc6wLMLYBJIo3TijUbzTiCKZ1ZA1YyPdTQsZFTtoyvE+a6dl/MRFHejrrTeXEp4bZYnPijdHGZ9fIo1SsaCwLgrEFXXe6dOgsa0yuyVamQsSxDmPE7Kb1AkWasN0gnpF9fIwsNdhvUEYmNJqvJo2WKDt0RoCxGIFm7raWb2Zny9wpzVO1lYAmwnzpTpSaWD+EQh/Qg/gb/qRCMZKTQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="3983e14f1004651ed819c830c94390cb"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6108caec7a85d1d7')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6108caec7a85d1d7</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
