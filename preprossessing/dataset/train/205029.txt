<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "52021",
      cRay: "6109f5843b1419b0",
      cHash: "ddc21cfb1193eb0",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmViaW5kby5jb20vY2dpX2Jpbi9zZXNzSUQtNThhNzE4MDQxZjY0OF8yMzc5LWVkYTgwYTNkNWIzNDRiYzQwZjNiYzA0ZjY1YjdhMzU3LTQxMWIyMmIyNTdhZDA1MTlkNDUwMDUzOWRhM2M4YmNmNGRkLyUwOSUwQQ==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "10Z1M0xKV82w9lwSoWdjHr8qWhS0bjhR5DkFnL3M8UBhwQX4OjnM3jF9kFe4CSA+W6XuQwqJLCWW4VhU+G639eKiW9oUN4iGeEyV04yM7OUzQOO5Yy/uQpEk2oZWXAtGBb9dhaHRf4wzyGRhpJopBekFtaANXFP60kK5eA5brhJK3hEyv6k8iKKJuSvqIjg0pR5NeKhWxcrrHF/fLEincNiZY1H2mAKttM15w/hsgo5QBXWsvPgxNHMU/FLKHRCwbA4/caT4MkTqUilCkIs/oygGV5oJrVWVoXA6Mzh8Pa6QkkXUUfyd+kbPzF/e2wTCWA5bAve5+WFV/FUjlFvGCxjjozei3yH9GYI10blqmIg63hytjRWyYJdVnkZhITHcVNCqiZ3wXFY+wOx9kn+GvmgyDqXsG3o8dlmTijcm3v1uKXO7Ya85/Q9WGhxKNTxqB7njgGSUXIvuojBv1ts/WoqfCvXYPefs0wm6daiRViXODesnnjSzBX/jH5iCy0qk3bcPVMgXL1ZFWnOjAWVHhqNihZ66C2/snRQLsIMQC3OXwii9QbsmW+c3suMc9Yc8CC9JFz3K+uOVQCWY59rxlq0Gew1c9cjtYf/NYAfqxUWsJ15q681C/M5o3UYuyIegmxQuHZIQcpDfYVePojGrx8kLUmEzjSs2/TvpVZieseL8HR3TY0zvyzXeopx4CAGV7jkLlQq3nGpX7dpbdx5IauE+ngpUVP4tcqgw/mFmXGObvn9z7D3EpCcIUk05cmmA",
        t: "MTYxMDQ4NjgyMC41MTYwMDA=",
        m: "kRvE9UphvzXxPD+j3Fcu8DgrCqJzXAKKTcZQUM3OY0k=",
        i1: "j4APoBtXzU/QnSjwewAhxA==",
        i2: "BQUGnUMBHSSV5DkMk5F1fw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "T42kaLQig+ji/3O2CeG5Z980fvsqMuW5oNMm8uqn27U=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bebindo.com</h2>
</div><!-- /.header -->
<a href="https://nhs-foi.com/aberrantgrump.php?blog=2307"></a>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/cgi_bin/sessID-58a718041f648_2379-eda80a3d5b344bc40f3bc04f65b7a357-411b22b257ad0519d4500539da3c8bcf4dd/%09%0A?__cf_chl_captcha_tk__=fa67845ac2f40a41a50dc7b24768e02b4a3984d9-1610486820-0-Ac7KxVW2ZyIsicwZX-Pb8KOeIKoHHuVTNz9bK2JKbJdXxmMhahd8bvJU-iCWTM7Q4xGiZ3dMBVzFMlPZfKGXhoyWQlm3wKX033Hg4FQKDB3GJkRn8JZd0EJ9EIlrD-FoMPAmBAkSD5AHZBDbCsKrjl3y5HapAu_oYSotRLCX-GY5Rpmx_pWc-R5Mz2mQyT-fY3yGYMm1XNLqpdXJDAl09dsWNTXfhFMqBA6Y458mC1tZ-9RI9QnKE8zCUiOa_TALg-uLY6vflckXHbeOYE1bNMw319y42pGaedqz3CF8f97LNpzVthGql9yMQoAip6s6VazeKkqHnQ7U7x24IYwrAInky7X_StsQLgYxFCTYaZ2RmllP_7ykvCwbhib9jFDRiWsGXAq8jhp9mTmhrGcuTAtNBnj9GHejONw9ZPj9FtLKKOyaQDmz8UEbl6Erv7s7JTmswmgmO5XMkdFl2L-dkrsw3vks_YJH-2GabhmhxzXxU8Y8yeHZGZbmD0MOcRv1eTo-RPmiQr19YqZoc-AhhK0-dp167IYEuJhXv413B1OjTR6wPEIPQyu4lktvR7K2cFE5ysk6mWAHNTD4UinpUR-P4Bkp4qQap-XFDxcvqYZMX4mnlCmztSjSrxHf62yVY8Ko7JW61OEP8CW_3DFGrRd9tMaofD4vZOl4YqmDZD0q" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="3d1bb51b528f221622b0a39f769b61063984b44b-1610486820-0-AaRON0dJbo4bRdHRZOph7/g6KIcyHEWWllokwYmMajNd3fEOJylCa5uMk5Uf5tB6NqL+wGQNIh/y3ouiglsmuObA+5L5CKsJYLsLNbjFEyVUl+VgYG1/j5DTwwhXGf/vPTUr7Qa+uPLalYTLfJ2Xxb00BJXR76BUVZ3QpVKfPT0AucgD/r2WKLaVsDvBChVRi1LV2xYqOPo+cCJPRHFn8HDw0jYC5yGaz51IP/DyPTjR44Su/aoRrsPOLQFsYvFt+aw+Uq3Fc7xeie/EwwlI50rQ2TiCvLwY59tPk8zVlnpYxnSTYn/kvHLStV3357AsWWwc6P/lD+MF00rPH5W6W+L9OUJCk9duQyYTXuU6EhRe1dDBaBeEDVwG5Qz9/oXQiIbMTg/H8yylwyDsW9gQ0jrC5R3h3oWjD8b2k37Pe1tRO85blSieroitakErE4kzqdR3p1XibKGDnGY7ONii9M+k43LL6+UxYYfdhTgWaDIEDMw/nZ8XVw9YcwQlHBeVPeAaRLiPf3bmmDORClYCQeTIPNNxdNzX0T+MlPi5ylJb/g7sTBM0KOeksWAlIrfnFZYPsqnjZJQiK9AaNkgyTRCQHwXfU8WcAse9RRmtKM1bcAEoUig4fQrYfrayr6YDsxOnMGihTNAiEN+Tjbxqniz9tUfBJjNzJ2iGEUc/t0UTW2E18OO4hxXKpYc0wL2bjf/uw7SjjwTqVc0TVyZmWLxQPEhqRxKSV81L2ZAfwV4zR2pIdGKdL7m07+A0c0M4e7G3fs8P65e+wgwmth3suI/CSTImRArfRALRU1X1aWBvhGzrWtVrhSnDVwaEDjPdt7+mhyLAaNhpt8FBtJz1FJWIbEbjCon58A06OIcLlrREpnLi9SYbHBIc2JwL/ydM4G621zccVfahhm7licp4n4F8PXOilYz8ltPrRQtPbLfg2o04qxvsOXPbiLIKVEXzzLzz4uW+WnC8yn5eKC9H6qiFQqsVWzyGHwF/NNt9G+IFG08femdRTKBjYbvzR1KSdoVjextMGdd8okwTlYAllC+r3IPb+1dQH1PD2ojYQMB9MQb0zgAWoLHYlqh8Tz7YqoBnx8eZHEncvpOhUqy3FH198uRlCMk4xD6F4MFK1NkhNASuuAEBIjYC4Gp8mflxWB3rF+wGNmuim4Ikkem+LFJto18i0OnOT6yPsRY6ol1Q9YA6I6r8nIPLgtTYsez8pNGkMfNlZlC/0xa4r3f1oQaCKPLMcHTLh0yuNy7/tBTRmTzkKKxrRXtbWZvzrIjZeRdk9PzEqUH7qo78YsGgPjJakMXCVGmWHbtMj5KlC4QZHdKvwooe1qU0T6rT/AR8AQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="0247139ab7e9ef6b08c0056e8c719858"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6109f5843b1419b0')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6109f5843b1419b0</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
