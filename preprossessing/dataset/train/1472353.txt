<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>Transgasa</title>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="apple-touch-icon.png" rel="apple-touch-icon"/>
<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]-->
<!-- Place favicon.ico in the root directory -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,300,700" rel="stylesheet" type="text/css"/>
<link href="css/normalize.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/owl.carousel.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
</head>
<body>
<!-- start preloader -->
<div id="loader-wrapper">
<div class="logo"></div>
<div id="loader">
</div>
</div>
<!-- end preloader -->
<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<!-- Start Header Section -->
<header class="main_menu_sec navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-12">
<div class="lft_hd"><a href="index.html"></a><a href="index.html"><img alt="" height="60" src="img/logo.png" width="155"/></a></div>
</div>
<div class="col-lg-9 col-md-9 col-sm-12">
<div class="rgt_hd">
<div class="main_menu">
<nav id="nav_menu">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div id="navbar">
<ul>
<li><a class="page-scroll" href="index.html">Inicio</a></li>
<li><a class="page-scroll" href="#clt_sec">Clientes</a></li>
<li><a class="page-scroll" href="#lts_sec">Empresa</a></li>
<li><a class="page-scroll" href="#tm_sec">Personal</a></li>
<li><a class="page-scroll" href="#pr_sec">Servicios</a></li>
<li><a class="page-scroll" href="#protfolio_sec">Galeria</a></li>
<li><a class="page-scroll" href="#ctn_sec">Contacto</a></li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</div>
</div>
</header>
<!-- End Header Section -->
<!-- start slider Section -->
<section id="slider_sec">
<div class="container">
<div class="row">
<div class="slider">
<div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
<!-- Indicators -->
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
<li data-slide-to="1" data-target="#carousel-example-generic"></li>
<li data-slide-to="2" data-target="#carousel-example-generic"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<div class="item active">
<div class="wrap_caption">
<div class="caption_carousel">
<h1>TRANSPORTES TRANSGASA</h1>
<p>30 AÃOS DE TRAYECTORIA</p>
</div>
</div>
</div>
<div class="item">
<div class="wrap_caption">
<div class="caption_carousel">
<h1>SEGURIDAD Y CONFIANZA</h1>
<p>ASEGURA TU CARGA</p>
</div>
</div>
</div>
<div class="item ">
<div class="wrap_caption">
<div class="caption_carousel">
<h1>CALIDAD Y SERVICIO</h1>
<p>UNA VARIADA GAMA DE CAMIONES Y BODEGAJE</p>
</div>
</div>
</div>
</div>
<!-- Controls -->
<a class="left left_crousel_btn" data-slide="prev" href="#carousel-example-generic" role="button">
<i class="fa fa-angle-left"></i>
<span class="sr-only">Previous</span>
</a>
<a class="right right_crousel_btn" data-slide="next" href="#carousel-example-generic" role="button">
<i class="fa fa-angle-right"></i>
<span class="sr-only">Next</span>
</a>
</div>
</div>
</div>
</div>
</section>
<!-- End slider Section -->
<!-- start about Section -->
<!--<section id="abt_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1>ABOUT</h1>
					<h2>WEâRE BRANDING & DIGITAL STUDIO FROM VIET NAM</h2>
				</div>			
			</div>		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="abt">
					<p>Mauris luctus aliquet nunc quis consectetur. Curabitur elit massa, consequat vel velit sit amet, scelerisque hendrerit mi. Cras pellentesque sem turpis, quis interdum mi sagittis a. Donec mattis porttitor eleifend</p>
				</div>
			</div>			
		</div>
	</div>
</section> -->
<!-- End About Section -->
<!-- start Counting section-->
<!--
<section id="counting_sec">
<div class="container">
	<div class="row">	

		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-user"></i>
			<h2 class="counter">43,753</h2>
			<h4>Happy Clients</h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-desktop"></i>
			<h2 class="counter">20,210</h2>
			<h4>Complete Project</h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-ticket"></i>
			<h2 class="counter">43,753</h2>
			<h4>Answered Tickets</h4>	
			</div>
		</div>			
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="counting_sl">
			<i class="fa fa-clock-o"></i>
			<h2 class="counter">45,105</h2>
			<h4>Development Hours</h4>	
			</div>
		</div>										
	</div>					
</div>
</section>

-->
<!-- start progress bar Section -->
<!--
<section id="skill_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1>Our Skill diagram</h1>
					<h2>WEâRE BRANDING & DIGITAL STUDIO FROM VIET NAM</h2>
				</div>			
			</div>			
		  <div class="skills progress-bar1">		  
				<ul class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft">
					  <li class="progress">
							<div class="progress-bar" data-width="85">
								  Wordpress 85%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="65">
								  Graphic Design 65%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="90">
								  HTML/CSS Design 90%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="60">
								  SEO 60%
							</div>
					  </li>
				</ul>
				<ul class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
					  <li class="progress">
							<div class="progress-bar" data-width="75">
								  Agencying 75%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="95">
								  App Development 95%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="70">
								  IT Consultency 70%
							</div>
					  </li>
					  <li class="progress">
							<div class="progress-bar" data-width="90">
								  Theme Development 90%
							</div>
					  </li>
				</ul>
		  </div>
                     
		
		</div>
	</div>
</section>

-->
<!-- End progress bar Section -->
<!-- start Service Section -->
<section id="pr_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>Servicios</h1>
<h2>
                    Nos responsabilizamos en todo momento de su carga, gracias al respaldo de la compaÃ±Ã­a de seguros Royal Sunalliance (pÃ³liza nÂº 3589341 Ofic. Los Angeles). 
                    </h2><p>Su empresa siempre estarÃ¡ informada del estado y la ubicaciÃ³n de su carga.</p>
<p>Contamos con un staff de servicios en distintas Ã¡reas cÃ³mo por ejemplo: </p>
</div>
</div>
<!--        1 SERVICIOS                                               -->
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="service">
<i class="fa fa-globe"></i>
<h2>Transporte</h2>
<div class="service_hoverly">
<i class="fa fa-truck"></i>
<h2>TRANSPORTE</h2>
<p>"Ramplas planas, acoplados, furgones, cama baja, varios de estos equipados con generadores de tipo underline"</p>
</div>
</div>
</div>
<!--        2 SERVICIOS                                               -->
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="service">
<!--<i class="fa fa-hourglass-end"></i>-->
<i class="fa fa-paper-plane"></i>
<h2>Bodegaje</h2>
<!--<i class="fa fa-paper-plane"></i>-->
<div class="service_hoverly">
<i class="fa fa-hourglass-end"></i>
<h2>Bodegaje</h2>
<!--<i class="fa fa-paper-plane"></i>-->
<p>"Patio para almacenamiento y manipuleo de contenedores, servicio de conecciÃ³n para Reffer. Arriendo de bodegas con servicio de grÃºa horquilla, rampa para desconsolidado de container".</p>
</div>
</div>
</div>
<!--        3 SERVICIOS                                               -->
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="service">
<!--<i class="fa fa-wordpress"></i>-->
<i class="fa fa-map-marker"></i>
<h2>Maquinarias</h2>
<div class="service_hoverly">
<!--<i class="fa fa-wordpress"></i>-->
<i aria-hidden="true" class="fa fa-industry"></i>
<h2>Maquinarias</h2>
<p>"Servicios de arriendo de GrÃºas horquilla de alto tonelaje, motoniveladora, Retroexcavadora, rodillo y cama baja".</p>
</div>
</div>
</div>
<!--        4 SERVICIOS                                               -->
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="service">
<!--<i class="fa fa-paint-brush"></i>-->
<i aria-hidden="true" class="fa fa-map-signs"></i>
<h2>Container</h2>
<div class="service_hoverly">
<!--<i class="fa fa-paint-brush"></i>-->
<i aria-hidden="true" class="fa fa-users"></i>
<h2>Container</h2>
<p>"Contamos con un equipo Sidelifter para 30 tons, para la carga y descarga de contenedores, asÃ­ como tambiÃ©n patio para su almacenamiento".</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- End Service Section -->
<!-- start portfolio Section -->
<section id="protfolio_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>Galeria</h1>
<h2>Presentamos NUESTRA variADA FLOTA.</h2>
</div>
</div>
<div class="col-lg-12">
<div class="portfolio-filter text-uppercase text-center">
<ul class="filter">
<li class="active" data-filter="*">TODOS</li>
<li data-filter=".web-design">CAMIONES</li>
<li data-filter=".graphic">BODEGAS</li>
<li data-filter=".photography">OFICINA</li>
<!--<li data-filter=".motion-video">Motion video</li>-->
</ul>
</div>
<div class="all-portfolios">
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_1.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_2.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_3.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_4.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_5.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_6.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_7.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_8.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_9.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_10.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_11.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_12.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_13.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_14.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_15.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_16.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_17.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_18.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_19.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_20.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_21.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio web-design">
<img alt="" class="img-responsive" src="images/ca_22.jpg"/>
</div>
</div>
<!-- OFICINA -->
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio photography">
<img alt="" class="img-responsive" src="images/of_3.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio photography">
<img alt="" class="img-responsive" src="images/of_4.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio photography">
<img alt="" class="img-responsive" src="images/of_5.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio photography">
<img alt="" class="img-responsive" src="images/of_6.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio photography">
<img alt="" class="img-responsive" src="images/of_7.jpg"/>
</div>
</div>
<!--<div class="col-sm-12  col-lg-12 col-md-12 col-xs-12 ">
					<div class="single-portfolio photography">
					<img class="img-responsive" src="images/of_8.jpg" alt="">
					</div>				
					</div>-->
<!-- BODEGAS -->
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio graphic">
<img alt="" class="img-responsive" src="images/co_1.jpg"/>
</div>
</div>
<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 ">
<div class="single-portfolio graphic">
<img alt="" class="img-responsive" src="images/co_2.jpg"/>
</div>
</div>
<!-- VIDEOS  
                    
                    <div class="col-sm-12  col-lg-12 col-md-12 col-xs-12 ">
					<div class="single-portfolio motion-video">
					<img class="img-responsive" src="images/videos/carga.mp4" alt="">
					</div>				
					</div>	
                    	-->
</div>
</div>
<!--
				<div class="post_btn">
					<div class="hover_effect_btn" id="hover_effect_btn">
						<a href="#hover_effect_btn" data-hover="view more post"><span>view more post</span></a>
					</div>
				</div>	-->
</div>
</div>
</section>
<!-- End Portfolio Section -->
<!-- start our team Section -->
<section id="tm_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>PERSONAL </h1>
<h2>Nuestro Equipo Humano </h2>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12">
<div class="all_team">
<div class="sngl_team">
<img alt="" height="150" src="img/mario.jpg" width="130"/>
<h3> Mario Garrido Sabag. <span>Gerente </span></h3>
<p>DueÃ±o Transportes Transgasa</p>
</div>
<div class="sngl_team">
<img alt="" height="150" src="img/flor.jpg" width="130"/>
<h3>Flor HernÃ¡ndez Zapata. <span> Contador </span></h3>
<p>Administrativo y FacturaciÃ³n</p>
</div>
<div class="sngl_team">
<img alt="" height="150" src="img/jose.jpg" width="130"/>
<h3> JosÃ© Torres PÃ©rez. <span>Operaciones</span></h3>
<p>Jefe de Operaciones</p>
</div>
<div class="sngl_team">
<img alt="" height="150" src="img/ernesto.jpg" width="130"/>
<h3> Ernesto Jara Inostroza. <span>Taller</span></h3>
<p>Jefe MantenciÃ³n</p>
</div>
<!--
                    <div class="sngl_team">						
						<img src="img/jorge.jpg" alt="" width="130" height="150"/>
						<h3> Jorge HernÃ¡ndez G.<span>Administrativo</span></h3>
						<p>RR.HH.</p>						
					</div>			-->
</div>
</div>
</div>
</div>
</section>
<!-- End our team Section -->
<!-- start our teastimonial Section -->
<section id="tstm_sec">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="all_tstm">
<div class="clnt_tstm">
<div class="sngl_tstm">
<i class="fa fa-quote-right"></i>
<h3>PRESUPUESTO </h3>
<p>Tendremos la transparencia para entregar cotizaciones y solicutedes,</p>
<h6>- Transgasa</h6>
</div>
</div>
<div class="clnt_tstm">
<div class="sngl_tstm">
<i class="fa fa-quote-right"></i>
<h3>TIEMPO</h3>
<p>
                        Aseguramos que tu carga llegara en el tiempo establecido,</p>
<h6>- Transgasa</h6>
</div>
</div>
<div class="clnt_tstm">
<div class="sngl_tstm">
<i class="fa fa-quote-right"></i>
<h3>CONFIANZA</h3>
<p>Informaremos a tiempo al cliente para entregar en su destino la carga correctamente,</p>
<h6>- Transgasa</h6>
</div>
</div>
<div class="clnt_tstm">
<div class="sngl_tstm">
<i class="fa fa-quote-right"></i>
<h3>CALIDAD</h3>
<p>Tenemos un prestigio de casi 30 aÃ±os de servicios a  nuestros clientes,</p>
<h6>- Transgasa</h6>
</div>
</div>
<div class="clnt_tstm">
<div class="sngl_tstm">
<i class="fa fa-quote-right"></i>
<h3>GARANTIA</h3>
<p>Investiga, pregunta y analiza quÃ© garantÃ­as te ofrecen, tanto en tiempo como en servicio,</p>
<h6>- Transgasa</h6>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- End our teastimonial Section -->
<!-- start Latest post Section -->
<section id="lts_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>EMPRESA</h1>
<h2>PRESENTAMOS NUESTRA INFRAESTRUCTURA</h2>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
<div class="lts_pst">
<img alt="" src="images/off_2.jpg"/>
<h2>MISION</h2>
<p><span lang="es">Ser una alternativa eficiente en el transporte de todo tipo de carga a toda empresa que requiera soluciones completas e integrales. Un servicio multimodal de gran calidad, con la seguridad, economÃ­a y puntualidad exigida por su carga</span></p>
<!--	<a href="single.html">Read more <i class="fa fa-long-arrow-right"></i></a>					-->
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
<div class="lts_pst">
<img alt="" src="images/off_0.jpg"/>
<h2>NUESTRAS OFICINAS</h2>
<p><span lang="es">
                    
                    Es una empresa que nace en el rubro de Transporte. Desde nuestros comienzos hemos demostrado el profesionalismo y dedicaciÃ³n con que realizamos nuestros trabajos, creciendo de la mano con las exigencias del mercado actual.
                    .</span></p>
<!--<a href="">Read more <i class="fa fa-long-arrow-right"></i></a>					-->
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12">
<div class="lts_pst">
<img alt="" src="images/off_1.jpg"/>
<h2>VisiÃ³n</h2>
<p>Posicionar nuestra empresa en el mercado, con un alto nivel de excelencia y personalizaciÃ³n, diversificando su accionar a otros servicios que nos permitan continuar con nuestro desarrollo empresarial, contando para ello con personal calificado y comprometido con nuestros objetivos, y cumpliendo con los estÃ¡ndares de calidad establecidos.
                    </p>
<!--<a href="">Read more <i class="fa fa-long-arrow-right"></i></a>					-->
</div>
</div>
<!--	<div class="post_btn">
				<div class="hover_effect_btn" id="hover_effect_btn">
					<a href="#hover_effect_btn" data-hover="view more post"><span>view more post</span></a>
				</div>
			</div>			-->
</div>
</div>
</section>
<!-- End Latest post Section -->
<!-- start pricing Section -->
<!--

<section id="pricing_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
				<div class="title_sec">
					<h1>Our Pricing Plan</h1>
					<h2>WEâRE BRANDING & DIGITAL STUDIO FROM VIET NAM</h2>
				</div>			
			</div>		
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_1">Basic</h2>
					<h3><span class="currency">$</span>30<span class="monuth">/  mo</span></h3>
					<ul>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li><a href="" class="btn pricing_btn">Send</a></li>
						
					</ul>		
				</div>
			</div>			
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_2">Standerd</h2>
					<h3><span class="currency">$</span>50<span class="monuth">/  mo</span></h3>
					<ul>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li><a href="" class="btn pricing_btn">Send</a></li>
					</ul>		
				</div>
			</div>			
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="sngl_pricing">											
					<h2 class="title_bg_3">Extended</h2>
					<h3><span class="currency">$</span>90<span class="monuth">/  mo</span></h3>
					<ul>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li>30 GB of Storage</li>
						<li><a href="" class="btn pricing_btn">Send</a></li>
					</ul>		
				</div>
			</div>	
		</div>
	</div>
</section>

-->
<!-- End pricing Section -->
<!-- start Happy Client Section -->
<section id="clt_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>Clientes</h1>
<h2>Algunos de nuestros clientes</h2>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12">
<div class="al_clt">
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_1.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_2.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_3.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_4.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_5.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_6.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_7.jpg"/></a>
</div>
<div class="sngl_clt">
<a href=""><img alt="" src="cliente/cli_8.jpg"/></a>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- End Happy Client  Section -->
<!-- start contact us Section -->
<section id="ctn_sec">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 ">
<div class="title_sec">
<h1>Contacto</h1>
<h2>Cualquier consulta o cotizacion envianos tu SOLICITUD</h2>
</div>
</div>
<!--	<div class="col-sm-6"> 
				<div id="cnt_form">
					<form id="contact-form" class="contact" name="contact-form" method="post" action="send-email.php" >
						<div class="form-group">
						<input type="text" name="name" class="form-control name-field" required placeholder="Nombres" >
						</div>
						<div class="form-group">
							<input type="email" name="email" class="form-control mail-field" required placeholder="Mail" >
						</div> 
                                         
                        <div class="form-group">
							<input type="subject" name="subject" class="form-control subject-field" required placeholder="Asunto" >
						</div>                                                        
                                                                
						<div class="form-group">
						   	<textarea name="message" id="message" required class="form-control" rows="8" placeholder="Mensaje"></textarea>
                            
						</div> 
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Enviar </button>
						</div>
					</form> 
				</div>
			</div>-->
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="cnt_info">
<ul>
<li><i class="fa fa-home fa-fw"></i><p>Ruta Q-50 NÂ° 2291, Cabrero. Octava RegiÃ³n Del biobio.</p></li>
<li><i class="fa fa-envelope"></i><p><a href="mailto:contacto@transgasa.cl"> contacto@transgasa.cl</a></p></li>
<!-- <li><i class="fa fa-envelope"></i><p>contacto@transgasa.cl</p></li>-->
<li><i class="fa fa-phone"></i><p>Celular : +56 984191802</p></li>
</ul>
</div>
</div>
</div>
</div>
</section>
<!-- End contact us  Section -->
<!-- start located map Section -->
<!-- <section id="ltd_map_sec">
 <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="map">						
			<h1>UbicaciÃ³n GeogrÃ¡fica</h1>
            <a href="#slidingDiv" class="show_hide" rel="#slidingDiv"><i class="fa fa-angle-up"></i></a>
            <a href="#slidingDiv" class="show_hide" rel="#slidingDiv"  <img src="cliente/cli_1.jpg" alt=""/><i class="fa fa-angle-up"></i></a>
            <a href=""><img src="img/map-marker.png" alt="" width="564" height="207" width:100%;height:200px/></a>
           <p> </p>
			<div id="slidingDiv">
				<div class="map_area">
					<div id="googleMap" style="width:100%;height:300px;"></div>
				</div>
			</div>	
			</div>
		</div>
	</div>
</div> 
</section> -->
<!-- End located map  Section -->
<!-- start footer Section -->
<footer id="ft_sec">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="ft">
<ul>
<li><a href="index.html"><i class="fa fa-home"></i></a></li>
<li><a href="#clt_sec"><i class="fa fa-users"></i></a></li>
<li><a href="#lts_sec"><i class="fa fa-university"></i></a></li>
<li><a href="#tm_sec"><i class="fa fa-user"></i></a></li>
<li><a href="#pr_sec"><i class="fa fa-sun-o"></i></a></li>
<li><a href="#protfolio_sec"><i class="fa fa-picture-o"></i></a></li>
<li><a href="#ctn_sec"><i class="fa fa-envelope"></i></a></li>
</ul>
</div>
<ul class="copy_right">
<li>Pagina WEB: www.transgasa.cl</li>
<li>Desarrollado Por: Cristian Inostroza DÃ­az - 2017</li>
</ul>
</div>
</div>
</div>
</footer>
<!-- End footer Section -->
<script src="http://code.jquery.com/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/showHide.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/scrolling-nav.js"></script>
<script src="js/plugins.js"></script>
<!-- Google Map js -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
			function initialize() {
			  var mapOptions = {
				zoom: 14,
				scrollwheel: false,
				center: new google.maps.LatLng(41.092586000000000000, -75.592688599999970000)
			  };
			  var map = new google.maps.Map(document.getElementById('googleMap'),
				  mapOptions);
			  var marker = new google.maps.Marker({
				position: map.getCenter(),
				animation:google.maps.Animation.BOUNCE,
				icon: 'img/map-marker.png',
				map: map
			  });
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
<script src="js/main.js"></script>
<script src="showHide.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){


   $('.show_hide').showHide({			 
		speed: 1000,  // speed you want the toggle to happen	
		easing: '',  // the animation effect you want. Remove this line if you dont want an effect and if you haven't included jQuery UI
		changeText: 1, // if you dont want the button text to change, set this to 0
		showText: 'View',// the button text to show when a div is closed
		hideText: 'Close' // the button text to show when a div is open
					 
	}); 


});

</script>
<script>
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });
</script>
<script>

  //Hide Overflow of Body on DOM Ready //
$(document).ready(function(){
    $("body").css("overflow", "hidden");
});

// Show Overflow of Body when Everything has Loaded 
$(window).load(function(){
    $("body").css("overflow", "visible");        
    var nice=$('html').niceScroll({
	cursorborder:"5",
	cursorcolor:"#00AFF0",
	cursorwidth:"3px",
	boxzoom:true, 
	autohidemode:true
	});

});
</script>
</body>
</html>
