<!DOCTYPE HTML>
<html class="uk-height-1-1 tm-error" dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>404 - Page not found</title>
<link href="" rel="apple-touch-icon-precomposed"/>
<link href="/templates/yoo_gusto/styles/custom/css/bootstrap.css" rel="stylesheet"/>
<link href="/templates/yoo_gusto/styles/custom/css/theme.css" rel="stylesheet"/>
</head>
<body class="uk-height-1-1 uk-vertical-align uk-text-center">
<div class="uk-vertical-align-middle uk-container-center">
<i class="tm-error-icon uk-icon-frown-o"></i>
<h1 class="tm-error-headline">404</h1>
<h2 class="uk-h3 uk-text-muted">Page not found</h2>
<p>The Page you are looking for doesn't exist or another error occurred.<br class="uk-hidden-small"/> <a href="javascript:history.go(-1)">Go back</a>, or head over to <a href="https://www.fairgraphica.de/">Fairgraphica Trading Team</a> to choose a new direction.</p>
</div>
</body>
</html>