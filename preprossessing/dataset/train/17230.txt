<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>1fichier.com: Cloud Storage</title>
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
<link href="https://img.1fichier.com/favicon.ico" rel="shortcut icon"/>
<link href="https://img.1fichier.com/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://img.1fichier.com/favicon.png" rel="icon" type="image/png"/>
<link href="https://img.1fichier.com/favicon.png" rel="apple-touch-icon"/>
<link href="https://img.1fichier.com/favicon.png" rel="image_src"/>
<link href="https://img.1fichier.com/css/style.css" rel="stylesheet" type="text/css"/>
<link href="https://img.1fichier.com/css/jquery.ui.css" rel="stylesheet" type="text/css"/>
<script src="https://img.1fichier.com/js/jquery.js" type="text/javascript"></script>
</head>
<body>
<script type="text/javascript">
	<!-- 
		$( function() {
	    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
  	    _renderItem: function( ul, item ) {
    	    var li = $("<li>"), wrapper = $("<div>", { text: item.label });
					if( item.element.attr("data-imagesrc") )	{ $( "<span>", { style: "background-image: url( \"" + item.element.attr("data-imagesrc") + "\")", "class": "ui-icon avatar" }).appendTo( wrapper ); }
					else { $( "<span>", { style: "background-image:none", "class": "ui-icon avatar" }).appendTo( wrapper ); }
	        return li.append( wrapper ).appendTo( ul );
      	},
				_renderButtonItem: function( item ) {
				  var buttonItem;
					if( item.element.attr("data-imagesrc") ) { buttonItem = $( "<span>", { style: "background-image: url( \"" + item.element.attr("data-imagesrc") + "\")", "class": "ui-selectmenu-text avatar" }) }
					else {  buttonItem = $( "<span>", { style: "background-image:none", "class": "ui-selectmenu-text avatar" }) }
				  this._setText( buttonItem, item.label );
				  return buttonItem;
				}
    	});

			
    	$(".lselector").iconselectmenu({ width:200, change: function( event, ui ) { window.location.href=this.value; }  }).iconselectmenu("menuWidget").addClass( "ui-menu-icons" );
  	});
	-->
</script>
<div id="header">
<a href="https://1fichier.com" title="1fichier.com"><img alt="logo" height="70" id="logo" src="https://img.1fichier.com/logo.png" width="240"/></a>
<div id="btn-container">
<select class="lselector">
<option data-imagesrc="https://img.1fichier.com/flags/en.png" selected="selected" value="/?lg=en">English</option>
<option data-imagesrc="https://img.1fichier.com/flags/fr.png" value="/?lg=fr">Français</option>
</select>
<a class="ui-button ui-corner-all" href="https://1fichier.com/tarifs.html" title="Prices">Prices</a>
<a class="ui-button ui-corner-all" href="https://1fichier.com/register.pl" title="Register">Register</a>
<a class="ui-button ui-corner-all" href="https://1fichier.com/login.pl" title="Login">My Account</a>
</div>
<span class="spacer" style="height:1px"></span>
</div>
<div class="spacer" style="height:140px"></div>
<div class="center-container2">
<div class="bloc2">
 		  The requested file does not exist<br/>It could be deleted by its owner.
			<span class="spacer spacer-20"></span>
</div>
</div>
<span class="spacer" style="height:90px"></span>
<div id="footer">
<div class="center-container">
<a href="https://1fichier.com" title="Back to home page"><img alt="1fichier.com" class="gauche" src="https://img.1fichier.com/logo-footer.png"/></a>
<div id="link-container">
<a href="https://1fichier.com/cgu.html">Legal &amp; Terms</a>   
			<a href="https://1fichier.com/abus.html">Abuse</a>   
			<a href="https://1fichier.com/tarifs.html">Prices</a>   
			<a href="https://1fichier.com/revendeurs.html">Resellers</a>   
			<a href="https://1fichier.com/hlp.html">Help</a>   
			<a href="https://1fichier.com/network.html">Network/Speedtest</a>   
			<a href="https://1fichier.com/contact.html">Contact</a>
<br/>
<a href="https://twitter.com/1fichiercom" target="_new"><img alt="Twitter" src="https://img.1fichier.com/twitter.png" style="padding:0;border:0"/></a>   
      <a href="https://facebook.com/1fichiercom" target="_new"><img alt="Facebook" src="https://img.1fichier.com/facebook.png" style="padding:0;border:0"/></a>   
			<a href="https://1fichier.com/api.html" style="vertical-align:top">API</a>   
      <a href="https://1fichier.com/occasions.html" style="vertical-align:top">Used Equipments</a>   
      <a href="https://1fichier.com/servers.html" style="vertical-align:top">Dedicated Servers</a>   
      <a href="https://dstorage.fr" style="vertical-align:top" target="_new">DStorage SASU © 2009-2020</a>
</div>
</div>
<span class="spacer"></span>
</div>
</body>
</html>
