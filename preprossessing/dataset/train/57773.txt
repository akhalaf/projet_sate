<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
<meta charset="utf-8"/>
<meta content="مكتب التربية العربي لدول الخليج هو إحدى بذور التعاون الخليجي أنشئ المكتب عام 1975 بنظام أساسي اكتسب صفة الوثيقة الدولية حين وقعه أصحاب الجلالة والسمو قادة الدول الأعضاء المؤسِّسة للمكتب : دولة الإمارات العربية المتحدة، ومملكة البحرين، ودولة الكويت، والمملكة العربية السعودية، وسلطنة عمان, ودولة قطر، ثم انضمت جمهورية اليمن لعضوية المكتب عام 2002" name="description"/>
<meta content="التربية ، التعليم ، التميز ، الالتزام ، الشفافية ، الجودة ، المبادرة ، التشاركية ، التنسيق والتعاون ، تنمية النشء ، قيم المواطنة ، السياسات التعليمية ، أفضل الممارسات ، اللغة العربية ، التربية الإسلامية ، التعلم الإلكتروني ، التعليم عن بعد ، الأسرة ، تطوير المناهج ، المهارات الحياتية ، مهارات المستقبل ، تطوير المناهج" name="keywords"/>
<meta content="index" name="robots"/>
<meta content="مكتب التربية العربي لدول الخليج" name="author"/>
<title> بوابة مكتب التربية العربي لدول الخليج </title>
<base href="/"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
    _atrk_opts = {
      atrk_acct: "ZHsMm1a4KM+2em",
      domain: "abegs.org",
      dynamic: true
    };
    (function () {
      var as = document.createElement('script');
      as.type = 'text/javascript';
      as.async = true;
      as.src = "https://certify-js.alexametrics.com/atrk.js";
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(as, s);
    })();
  </script>
<noscript><img alt="" height="1" src="https://certify.alexametrics.com/atrk.gif?account=ZHsMm1a4KM+2em" style="display:none" width="1"/></noscript>
<!-- End Alexa Certify Javascript -->
<link href="styles.bfdddeef0448b922676e.css" rel="stylesheet"/></head>
<body>
<app-root></app-root>
<script src="runtime-es2015.69e18bba42b8f3949309.js" type="module"></script><script defer="" nomodule="" src="runtime-es5.69e18bba42b8f3949309.js"></script><script defer="" nomodule="" src="polyfills-es5.8856b1c0dfe3ebd79cff.js"></script><script src="polyfills-es2015.5b10b8fd823b6392f1fd.js" type="module"></script><script defer="" src="scripts.d137d5543bc53b345a19.js"></script><script src="main-es2015.703c74446e35f6f5a732.js" type="module"></script><script defer="" nomodule="" src="main-es5.703c74446e35f6f5a732.js"></script></body>
</html>
