<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.axiumfoods.com/xmlrpc.php" rel="pingback"/>
<title></title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.axiumfoods.com/feed" rel="alternate" title=" » Feed" type="application/rss+xml"/>
<link href="https://www.axiumfoods.com/comments/feed" rel="alternate" title=" » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.axiumfoods.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.axiumfoods.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.axiumfoods.com/wp-content/themes/optimizer/style.css?ver=5.6" id="optimizer-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="optimizer-style-inline-css" type="text/css">
#optimizer_front_about-3{ background-color: #ffffff!important; }#optimizer_front_about-3 .about_header, #optimizer_front_about-3 .about_pre, #optimizer_front_about-3 span.div_middle{color: #222222!important; }#optimizer_front_about-3 span.div_left, #optimizer_front_about-3 span.div_right{background-color: #222222!important; }#optimizer_front_about-3 .about_content{color: #a8b4bf!important; }
</style>
<link href="https://www.axiumfoods.com/wp-content/themes/optimizer/style_core.css?ver=5.6" id="optimizer-style-core-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.axiumfoods.com/wp-content/themes/optimizer/assets/fonts/font-awesome.css?ver=5.6" id="optimizer-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3Aregular%2Citalic%2C700%26subset%3Dlatin%2C" id="optimizer_google_fonts-css" media="screen" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.axiumfoods.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.axiumfoods.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="jquery-migrate-js-after" type="text/javascript">
jQuery(document).ready(function(){   jQuery(".so-panel.widget").each(function (){   jQuery(this).attr("id", jQuery(this).find(".so_widget_id").attr("data-panel-id"))  });  });
</script>
<script id="optimizer_js-js" src="https://www.axiumfoods.com/wp-content/themes/optimizer/assets/js/optimizer.js?ver=1" type="text/javascript"></script>
<script id="optimizer_otherjs-js" src="https://www.axiumfoods.com/wp-content/themes/optimizer/assets/js/other.js?ver=1" type="text/javascript"></script>
<script id="optimizer_lightbox-js" src="https://www.axiumfoods.com/wp-content/themes/optimizer/assets/js/magnific-popup.js?ver=1" type="text/javascript"></script>
<link href="https://www.axiumfoods.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.axiumfoods.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.axiumfoods.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style type="text/css">

/*Fixed Background*/
html body.custom-background{ background-attachment:fixed;}
	/*BOXED LAYOUT*/
	.site_boxed .layer_wrapper, body.home.site_boxed #slidera {width: 100%;float: left;margin: 0 0%;
	background: #ffffff;}
	.site_boxed .stat_bg, .site_boxed .stat_bg_overlay{width: 100%;}
	.site_boxed .social_buttons{background: #ffffff;}
	.site_boxed .center {width: 95%;margin: 0 auto;}
	.site_boxed .head_top .center{ width:95%!important;}



/*Site Content Text Style*/
body, input, textarea{ 
	font-family:Open Sans; 	font-size:16px; }

.single_metainfo, .single_post .single_metainfo a, a:link, a:visited, .single_post_content .tabs li a{ color:#999999;}


/*LINK COLOR*/
.org_comment a, .thn_post_wrap a:link, .thn_post_wrap a:visited, .lts_lightbox_content a:link, .lts_lightbox_content a:visited, .athor_desc a:link, .athor_desc a:visited{color:#3590ea;}
.org_comment a:hover, .thn_post_wrap a:link:hover, .lts_lightbox_content a:link:hover, .lts_lightbox_content a:visited:hover, .athor_desc a:link:hover, .athor_desc a:visited:hover{color:#1e73be;}

/*-----------------------------Static Slider Content box width------------------------------------*/
.stat_content_inner .center{width:75%;}
.stat_content_inner{bottom:41%; color:#ffffff;}


/*STATIC SLIDE CTA BUTTONS COLORS*/
.static_cta1.cta_hollow, .static_cta1.cta_hollow_big{ background:transparent!important; color:#ffffff;}
.static_cta1.cta_flat, .static_cta1.cta_flat_big, .static_cta1.cta_rounded, .static_cta1.cta_rounded_big, .static_cta1.cta_hollow:hover, .static_cta1.cta_hollow_big:hover{ background:#c1d181!important; color:#ffffff; border-color:#c1d181!important;}

.static_cta2.cta_hollow, .static_cta2.cta_hollow_big{ background:transparent; color:#ffffff;}
.static_cta2.cta_flat, .static_cta2.cta_flat_big, .static_cta2.cta_rounded, .static_cta2.cta_rounded_big, .static_cta2.cta_hollow:hover, .static_cta2.cta_hollow_big:hover{ background:#c1d181!important; color:#ffffff;border-color:#c1d181!important;}


/*-----------------------------COLORS------------------------------------*/
		/*Header Color*/
		.header{ position:relative!important; background:#f5f5f5;}
				
				
				.home.has_trans_header.page .header{background:#f5f5f5!important;}
		@media screen and (max-width: 480px){
		.home.has_trans_header .header{ background:#f5f5f5!important;}
		}
		


		/*LOGO*/
				.logo h2, .logo h1, .logo h2 a, .logo h1 a{ 
						font-size:30px;			color:#555555;
		}
		body.has_trans_header.home .header .logo h2, body.has_trans_header.home .header .logo h1, body.has_trans_header.home .header .logo h2 a, body.has_trans_header.home .header .logo h1 a, body.has_trans_header.home span.desc{ color:#fff;}
		#simple-menu{color:#888888;}
		body.home.has_trans_header #simple-menu{color:#fff;}
		span.desc{color:#555555;}

		/*MENU Text Color*/
		#topmenu ul li a{color:#888888;}
		body.has_trans_header.home #topmenu ul li a, body.has_trans_header.home .head_soc .social_bookmarks.bookmark_simple a{ color:#fff;}
		#topmenu ul li.menu_hover a{border-color:#cecece;}
		#topmenu ul li.menu_hover>a, body.has_trans_header.home #topmenu ul li.menu_hover>a{color:#cecece;}
		#topmenu ul li.current-menu-item>a{color:#3590ea;}
		#topmenu ul li ul{border-color:#cecece transparent transparent transparent;}
		#topmenu ul.menu>li:hover:after{background-color:#cecece;}
		
		#topmenu ul li ul li a:hover{ background:#8ab1ce; color:#FFFFFF;}
		.head_soc .social_bookmarks a{color:#888888;}
		.head_soc .social_bookmarks.bookmark_hexagon a:before {border-bottom-color: rgba(136,136,136, 0.3)!important;}
		.head_soc .social_bookmarks.bookmark_hexagon a i {background:rgba(136,136,136, 0.3)!important;}
		.head_soc .social_bookmarks.bookmark_hexagon a:after { border-top-color:rgba(136,136,136, 0.3)!important;}
		

		/*BASE Color*/
		.widget_border, .heading_border, #wp-calendar #today, .thn_post_wrap .more-link:hover, .moretag:hover, .search_term #searchsubmit, .error_msg #searchsubmit, #searchsubmit, .optimizer_pagenav a:hover, .nav-box a:hover .left_arro, .nav-box a:hover .right_arro, .pace .pace-progress, .homeposts_title .menu_border, .pad_menutitle, span.widget_border, .ast_login_widget #loginform #wp-submit, .prog_wrap, .lts_layout1 a.image, .lts_layout2 a.image, .lts_layout3 a.image, .rel_tab:hover .related_img, .wpcf7-submit, .woo-slider #post_slider li.sale .woo_sale, .nivoinner .slide_button_wrap .lts_button, #accordion .slide_button_wrap .lts_button, .img_hover, p.form-submit #submit, .optimposts .type-product a.button.add_to_cart_button{background:#8ab1ce;} 
		
		.share_active, .comm_auth a, .logged-in-as a, .citeping a, .lay3 h2 a:hover, .lay4 h2 a:hover, .lay5 .postitle a:hover, .nivo-caption p a, .acord_text p a, .org_comment a, .org_ping a, .contact_submit input:hover, .widget_calendar td a, .ast_biotxt a, .ast_bio .ast_biotxt h3, .lts_layout2 .listing-item h2 a:hover, .lts_layout3 .listing-item h2 a:hover, .lts_layout4 .listing-item h2 a:hover, .lts_layout5 .listing-item h2 a:hover, .rel_tab:hover .rel_hover, .post-password-form input[type~=submit], .bio_head h3, .blog_mo a:hover, .ast_navigation a:hover, .lts_layout4 .blog_mo a:hover{color:#8ab1ce;}
		#home_widgets .widget .thn_wgt_tt, #sidebar .widget .thn_wgt_tt, #footer .widget .thn_wgt_tt, .astwt_iframe a, .ast_bio .ast_biotxt h3, .ast_bio .ast_biotxt a, .nav-box a span, .lay2 h2.postitle:hover a{color:#8ab1ce;}
		.pace .pace-activity{border-top-color: #8ab1ce!important;border-left-color: #8ab1ce!important;}
		.pace .pace-progress-inner{box-shadow: 0 0 10px #8ab1ce, 0 0 5px #8ab1ce;
		  -webkit-box-shadow: 0 0 10px #8ab1ce, 0 0 5px #8ab1ce;
		  -moz-box-shadow: 0 0 10px #8ab1ce, 0 0 5px #8ab1ce;}
		
		.fotorama__thumb-border, .ast_navigation a:hover{ border-color:#8ab1ce!important;}
		
		
		/*Text Color on BASE COLOR Element*/
		.icon_round a, #wp-calendar #today, .moretag:hover, .search_term #searchsubmit, .error_msg #searchsubmit, .optimizer_pagenav a:hover, .ast_login_widget #loginform #wp-submit, #searchsubmit, .prog_wrap, .rel_tab .related_img i, .lay1 h2.postitle a, .nivoinner .slide_button_wrap .lts_button, #accordion .slide_button_wrap .lts_button, .lts_layout1 .icon_wrap a, .lts_layout2 .icon_wrap a, .lts_layout3 .icon_wrap a, .lts_layout1 .icon_wrap a:hover{color:#FFFFFF;}
		.thn_post_wrap .listing-item .moretag:hover, body .lts_layout1 .listing-item .title, .lts_layout2 .img_wrap .optimizer_plus, .img_hover .icon_wrap a, body .thn_post_wrap .lts_layout1 .icon_wrap a, .wpcf7-submit, .woo-slider #post_slider li.sale .woo_sale, p.form-submit #submit, .optimposts .type-product a.button.add_to_cart_button{color:#FFFFFF;}




/*Sidebar Widget Background Color */
#sidebar .widget{ background:#FFFFFF;}
/*Widget Title Color */
#sidebar .widget .widgettitle, #sidebar .widget .widgettitle a{color:#666666;}
#sidebar .widget li a, #sidebar .widget, #sidebar .widget .widget_wrap{ color:#999999;}
#sidebar .widget .widgettitle, #sidebar .widget .widgettitle a{font-size:10px;}



#footer .widgets .widgettitle, #copyright a{color:#ffffff;}

/*FOOTER WIDGET COLORS*/
#footer{background: #f2a737;}
#footer .widgets .widget a, #footer .widgets{color:#d6d6d6;}
/*COPYRIGHT COLORS*/
#copyright{background: #333333;}
#copyright a, #copyright{color: #999999;}
.foot_soc .social_bookmarks a{color:#999999;}
.foot_soc .social_bookmarks.bookmark_hexagon a:before {border-bottom-color: rgba(153,153,153, 0.3);}
.foot_soc .social_bookmarks.bookmark_hexagon a i {background:rgba(153,153,153, 0.3);}
.foot_soc .social_bookmarks.bookmark_hexagon a:after { border-top-color:rgba(153,153,153, 0.3);}



/*-------------------------------------TYPOGRAPHY--------------------------------------*/

/*Post Titles, headings and Menu Font*/
h1, h2, h3, h4, h5, h6, #topmenu ul li a, .postitle, .product_title{ font-family:Open Sans;  }

#topmenu ul li a, .midrow_block h3, .lay1 h2.postitle, .more-link, .moretag, .single_post .postitle, .related_h3, .comments_template #comments, #comments_ping, #reply-title, #submit, #sidebar .widget .widgettitle, #sidebar .widget .widgettitle a, .search_term h2, .search_term #searchsubmit, .error_msg #searchsubmit, #footer .widgets .widgettitle, .home_title, body .lts_layout1 .listing-item .title, .lay4 h2.postitle, .lay2 h2.postitle a, #home_widgets .widget .widgettitle, .product_title, .page_head h1{ text-transform:uppercase; letter-spacing:1px;}

#topmenu ul li a{font-size:12px;}
#topmenu ul li {line-height: 12px;}

/*Body Text Color*/
body, .home_cat a, .contact_submit input, .comment-form-comment textarea{ color:#999999;}
.single_post_content .tabs li a{ color:#999999;}
.thn_post_wrap .listing-item .moretag{ color:#999999;}
	
	

/*Post Title */
.postitle, .postitle a, .nav-box a, h3#comments, h3#comments_ping, .comment-reply-title, .related_h3, .nocomments, .lts_layout2 .listing-item h2 a, .lts_layout3 .listing-item h2 a, .lts_layout4 .listing-item h2 a, .author_inner h5, .product_title, .woocommerce-tabs h2, .related.products h2, .optimposts .type-product h2.postitle a, .woocommerce ul.products li.product h3{ text-decoration:none; color:#666666;}

/*Woocommerce*/
.optimposts .type-product a.button.add_to_cart_button:hover{background-color:#FFFFFF;color:#8ab1ce;} 
.optimposts .lay2_wrap .type-product span.price, .optimposts .lay3_wrap .type-product span.price, .optimposts .lay4_wrap  .type-product span.price, .optimposts .lay4_wrap  .type-product a.button.add_to_cart_button{color:#666666;}
.optimposts .lay2_wrap .type-product a.button.add_to_cart_button:before, .optimposts .lay3_wrap .type-product a.button.add_to_cart_button:before{color:#666666;}
.optimposts .lay2_wrap .type-product a.button.add_to_cart_button:hover:before, .optimposts .lay3_wrap .type-product a.button.add_to_cart_button:hover:before, .optimposts .lay4_wrap  .type-product h2.postitle a{color:#8ab1ce;}



@media screen and (max-width: 480px){
body.home.has_trans_header .header .logo h1 a{ color:#555555!important;}
body.home.has_trans_header .header #simple-menu{color:#888888!important;}
}

/*USER'S CUSTOM CSS---------------------------------------------------------*/
/*---------------------------------------------------------*/
</style>
<!--[if IE]>
<style type="text/css">
.text_block_wrap, .home .lay1, .home .lay2, .home .lay3, .home .lay4, .home .lay5, .home_testi .looper, #footer .widgets{opacity:1!important;}
#topmenu ul li a{display: block;padding: 20px; background:url(#);}
</style>
<![endif]-->
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #9e9894; background-image: url("https://www.axiumfoods.com/wp-content/uploads/2015/10/white_restaurant_torts.jpg"); background-position: left top; background-size: auto; background-repeat: repeat; background-attachment: scroll; }
</style>
</head>
<body class="home blog custom-background site_boxed is_boxed">
<!--HEADER-->
<div class="header_wrap layer_wrapper">
<!--HEADER STARTS-->
<div class="header">
<div class="center">
<div class="head_inner">
<!--LOGO START-->
<div class="logo hide_sitetagline">
<a class="logoimga" href="https://www.axiumfoods.com/" title=""><img src="https://www.axiumfoods.com/wp-content/uploads/2014/07/new_axium_foods_logo_small_email.png"/></a>
<span class="desc"></span>
</div>
<!--LOGO END-->
<!--MENU START-->
<!--MOBILE MENU START-->
<a href="#sidr" id="simple-menu"><i class="fa-bars"></i></a>
<!--MOBILE MENU END-->
<div class="" id="topmenu">
<div class="menu-header"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2010" id="menu-item-2010"><a href="https://www.axiumfoods.com/about-us">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2011" id="menu-item-2011"><a href="https://www.axiumfoods.com/about-us/our-history">Our History</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2012" id="menu-item-2012"><a href="https://www.axiumfoods.com/about-us/quality">Axium Quality</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2014" id="menu-item-2014"><a href="https://www.axiumfoods.com/our-brands">Our Brand</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2016" id="menu-item-2016"><a href="https://www.axiumfoods.com/our-brands/pajedas">Pajeda’s</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2017" id="menu-item-2017"><a href="https://www.axiumfoods.com/private-brands">Private Brands</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2018" id="menu-item-2018"><a href="https://www.axiumfoods.com/private-brands/private-brand-products">Private Brand Products</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2020" id="menu-item-2020"><a href="https://www.axiumfoods.com/contact-us">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2021" id="menu-item-2021"><a href="https://www.axiumfoods.com/careers">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2171" id="menu-item-2171"><a href="https://www.axiumfoods.com/covid-19-response">COVID-19 Response</a></li>
</ul></div> <!--LOAD THE HEADR SOCIAL LINKS-->
<div class="head_soc">
</div>
</div>
<!--MENU END-->
</div>
</div>
</div>
<!--HEADER ENDS--></div><!--layer_wrapper class END-->
<!--Slider START-->
<div class="layer_wrapper " id="slidera">
<div class=" stat_has_img" id="stat_img">
<div class="stat_content stat_content_center">
<div class="stat_content_inner">
<div class="center">
<h2><em><span style="color: #474040">We're the snack partner that works a little harder to exceed your expectations</span></em></h2>
<div class="cta_buttons">
<a class="static_cta1 lts_button lt_rounded cta_hollow" href="https://www.axiumfoods.com/about-us">about us</a>
<a class="static_cta2 lts_button lt_rounded cta_flat" href="https://www.axiumfoods.com/private-brands">private brand solutions</a>
</div>
</div>
</div>
</div>
<img alt="" class="stat_bg_img" id="statimg_99" src="https://www.axiumfoods.com/wp-content/uploads/2015/10/white_restaurant_torts_edited-1.jpg"/>
</div> </div>
<!--Slider END-->
<div class="home_wrap layer_wrapper">
<div class="fixed_wrap fixindex">
<!--FRONTPAGE WIDGET AREA-->
<div class="frontpage_sidebar" id="frontsidebar">
<div class="widget optimizer_front_about aboutblock" data-widget-id="optimizer_front_about-3" id="optimizer_front_about-3"><div class="widget_wrap"><div class="text_block_wrap"><div class="center"><div class="about_inner"><span class="about_pre">Axium Foods is...</span><h2 class="about_header">Your private brand solution</h2><div class="optimizer_divider "><span class="div_left"></span><span class="div_middle"><i class="fa fa-stop"></i></span><span class="div_right"></span></div><div class="about_content"><p><a href="https://www.axiumfoods.com/private-brands">If you’re looking for a solution to your private label, co-packing, or contract manufacturing needs, Axium Foods can help. Read more about our capabilities and learn why we’re an industry leader in snack foods.</a></p>
</div></div></div></div></div></div> </div>
</div>
</div><!--layer_wrapper class END-->
<a class="to_top "><i class="fa-angle-up fa-2x"></i></a>
<!--Footer Start-->
<div class="footer_wrap layer_wrapper ">
<div id="footer">
<div class="center">
</div>
<!--Copyright Footer START-->
<div class="soc_right" id="copyright">
<div class="center">
<!--Site Copyright Text START-->
<div class="copytext"><p>© Copyright Axium Foods, Inc.</p></div>
<!--Site Copyright Text END-->
<div class="foot_right_wrap">
<!--FOOTER MENU START-->
<!--FOOTER MENU END-->
<!--SOCIAL ICONS START-->
<div class="foot_soc">
<div class="social_bookmarks bookmark_simple bookmark_size_normal">
</div></div>
<!--SOCIAL ICONS END-->
</div>
</div><!--Center END-->
</div>
<!--Copyright Footer END-->
</div>
<!--Footer END-->
</div><!--layer_wrapper class END-->
<script type="text/javascript">
	jQuery(window).on('load',function() {
		//STATIC SLIDER IMAGE FIXED
		var statimgheight = jQuery(".stat_has_img img").height();
		var hheight = jQuery(".header").height();		jQuery('.stat_bg').css({"background-position-y":hheight+"px", "top":hheight+"px"});
		jQuery('.stat_bg_overlay').css({ "top":hheight+"px"});
		});		
		jQuery(window).on('scroll', function() {
			var scrollTop = jQuery(this).scrollTop();
			var hheight = jQuery(".header").height();
				if ( !scrollTop ) {
					jQuery('.stat_bg').css({"background-position-y":hheight+"px"});
				}else{
					jQuery('.stat_bg').css({"background-position-y":"0px"});
				}
		});

</script>
<script id="wp-embed-js" src="https://www.axiumfoods.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>