<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
<title>Error</title>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="../assets/favicons/apple-touch-icon-57x57.png" rel="apple-touch-icon-precomposed" sizes="57x57"/>
<link href="../assets/favicons/apple-touch-icon-114x114.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="../assets/favicons/apple-touch-icon-72x72.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="../assets/favicons/apple-touch-icon-144x144.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="../assets/favicons/apple-touch-icon-60x60.png" rel="apple-touch-icon-precomposed" sizes="60x60"/>
<link href="../assets/favicons/apple-touch-icon-120x120.png" rel="apple-touch-icon-precomposed" sizes="120x120"/>
<link href="../assets/favicons/apple-touch-icon-76x76.png" rel="apple-touch-icon-precomposed" sizes="76x76"/>
<link href="../assets/favicons/apple-touch-icon-152x152.png" rel="apple-touch-icon-precomposed" sizes="152x152"/>
<link href="../assets/favicons/favicon-196x196.png" rel="icon" sizes="196x196" type="image/png"/>
<link href="../assets/favicons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="../assets/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="../assets/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="../assets/favicons/favicon-128.png" rel="icon" sizes="128x128" type="image/png"/>
<meta content="Baltan Laboratories" name="application-name"/>
<meta content="#FFFFFF" name="msapplication-TileColor"/>
<meta content="../assets/favicons/mstile-144x144.png" name="msapplication-TileImage"/>
<meta content="../assets/favicons/mstile-70x70.png" name="msapplication-square70x70logo"/>
<meta content="../assets/favicons/mstile-150x150.png" name="msapplication-square150x150logo"/>
<meta content="../assets/favicons/mstile-310x310.png" name="msapplication-square310x310logo"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@baltanlaboratories" name="twitter:site"/>
<meta content="Error." name="twitter:title"/>
<meta content="Error." name="og:title"/>
<meta content="https://www.baltanlaboratories.org/error" name="twitter:url"/>
<meta content="https://www.baltanlaboratories.org/error" name="og:url"/>
<meta content="" name="twitter:description"/>
<meta content="" name="og:description"/>
<meta content=" " name="twitter:image"/>
<meta content=" " name="og:image"/>
<link href="https://www.baltanlaboratories.org/assets/css/main.min.css" rel="stylesheet"/>
<link href="https://www.baltanlaboratories.org/assets/css/exploreitem.min.css" rel="stylesheet"/> <link href="https://www.baltanlaboratories.org/assets/css/search.min.css" rel="stylesheet"/>
<script async="" src="https://www.baltanlaboratories.org/assets/js/fontawesome.js"></script> <script src="https://www.baltanlaboratories.org/assets/js/jquery3.2.1.js"></script>
<link href="https://www.baltanlaboratories.org/assets/plugins/embed/css/embed.css" rel="stylesheet"/> <script async="" src="https://www.baltanlaboratories.org/assets/js/paper-full.js"></script> <script async="" src="https://www.baltanlaboratories.org/assets/js/modernizr.js"></script> <script async="" src="https://www.baltanlaboratories.org/assets/js/main.js"></script> <script async="" src="https://www.baltanlaboratories.org/assets/js/modernizr.custom.js"></script> <style>div#player{display:none;}</style>
<script type="text/javascript">
    $('.ajax-link').click(function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      loadLink(href)
    })

  function loadLink(url, nostatechange) {
    var loadUrl = url + " #main-container";
    var loadArea = $("#load");
    loadArea.fadeTo('400', '0', function() {
      loadArea.load(loadUrl, function () {

        function updateMenu() {
          var active = $('#main-container').attr('data-page');
          $('.menu__list').removeClass('active');
          $('.menu-item' + active).addClass('active');
        }
        if (!nostatechange) { updateState(url) };
        loadArea.fadeTo('400', '1');
      })
    })
  }

  function updateState(url) {
    var rootstring = window.location.origin;
    var newurl = url.replace(rootstring,'');
    history.pushState({}, "page", newurl);
  }

  $(window).on('popstate', function(e) {
    loadLink(window.location.href, true);
  })

  function onScroll(event){
    if (scrollPos >= explorePos && scrollPos != 0) {
        $('.header-cont').removeClass("first");
     }
    else{
        $('.header-cont').addClass("first");    }
      }
  var explorePos = '';
  var scrollPos = '';
  var lastScrollTop = 0;

  $(window).scroll(function(event){
    scrollPos = $(this).scrollTop();
    onScroll();
    if ($(window).width() < 640){
      if (scrollPos > lastScrollTop && scrollPos >= 80){$('.header-cont').css('top', '-56px');
      lastScrollTop = scrollPos;
    } else {
      if (scrollPos+100 <= lastScrollTop){
        $('.header-cont').css('top', '0px');
        lastScrollTop = scrollPos;}
      }
    } else {$('.header-cont').css('top', '0px')}
  });

  function onScroll(event){
    if (scrollPos === 0){$('.header-cont').addClass('showVignet');}
    else {$('.header-cont').removeClass('showVignet');}
    if (scrollPos >= explorePos && scrollPos != 0) {                    
      $('.header').addClass("leftanimation");  
      $('.header').addClass("marginheader");      
      document.getElementById("sb-search").style.display = "block";
      $('#exploreButtons-mobile').css('bottom', 0); 
    }
    else{         
      $('.header').removeClass("leftanimation"); 
      $('#exploreButtons-mobile').css('bottom', '-50px'); 
    }
    if ($('.panelA.from-right').hasClass('is-visible') || $('.cd-panel.from-left').hasClass('is-visible')){
      $('#exploreButtons-mobile').css('bottom', '0'); 
    }
  };
</script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-89019111-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-89019111-13');
</script>
</head>
<body>
<div class="about" id="tenyears">
<div class="vignet"></div>
</div>
<div class="header-cont first" style="background-color:;">
<header class="header marginheader" role="banner">
<div class="whiteheader">
<div class="blue1 blue2 innermenu">
<div class="branding">
<a href="https://www.baltanlaboratories.org" rel="home">
<svg version="1.1" viewbox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g fill="white">
<polygon id="Page-1" points="25 0 21 21 0 25 21 29 25 50 29 29 50 25 29 21"></polygon>
</g>
</svg>
</a>
</div>
<nav class="navigation" id="menu-center" role="navigation">
<ul class="menu menu__list">
<li class="menu-item menu__list__item exploreli">
<a class="list__item--link" href="https://www.baltanlaboratories.org/#explore">Explore</a>
</li>
<li class="menu-item menu__list__item aboutli">
<a class="list__item--link" href="https://www.baltanlaboratories.org/about">About</a>
</li>
</ul>
</nav>
</div>
</div>
<div class="searchdiv">
<div class="sb-search" id="sb-search">
<form>
<input class="sb-search-input js-search" id="search" name="search" placeholder="Enter your search term..." type="text" value=""/>
<input class="sb-search-submit" type="submit" value=""/>
<span class="sb-icon-search" id="iconspan"><i aria-hidden="true" class="fa fa-search" id="iconSearch"></i></span>
</form>
</div>
<div class="gallery-block searchbackground searchhide" id="mainsearch">
<div class="ajax-container searchcontainer" id="msg">
</div>
</div>
</div>
<script>
$('div#sb-search').on('click',function(){ 
    console.log("klik werkt");        
});
var inputMain                       = jQuery('.sb-search-input');
var searchResultsContainerMain      = jQuery('#msg');

$('.exit').hide();

inputMain.on('keyup', function(e) {    
    if(inputMain.val().length > 2) {
       $('html').css({overflow: 'hidden' });
       $('div.searchbackground').removeClass('searchhide');
        $('.exit').show();
        $('.ajax-container').css('overflowY', 'auto'); 
        $('.exit').on('click',function(){
        //location.reload();
     });
    var searchval = inputMain.val();
    $.ajax({
        url: "/search/"+searchval.toLowerCase().split(" ")+"",
        context: jQuery('#msg')
    }).done(function(data) {
        var results = JSON.parse(data);
        console.log(results);
        
        // Flush the container
        searchResultsContainerMain.html('');
console.log(results);
       
        if(!results.length){
                var string = '<div class="content-box"> <h3 class="content-box__title">Oops no results found.</h3><p>Try another search term.</p> </div>'; 
            }
        // loop trough the objects in results and display them
        jQuery.each(results, function(index, object) {    
            if(typeof object.content.soort != 'undefined' && typeof object.content.subtitle != 'undefined' && object.parent != 'about' ){
                var string = ' <div class="content-box"><a href="'+object.url+'" class="full-link"><time class="content-box__datetime">'+object.content.soort+'</time> <h3 class="content-box__title">'+object.content.title+'</h3><p>'+object.content.subtitle+'</p></a> </div> ';  
            }
            else if(typeof object.content.soort != 'undefined' && object.parent != 'about'){
                var string = ' <div class="content-box"><a href="'+object.url+'" class="full-link"><time class="content-box__datetime">'+object.content.soort+'</time> <h3 class="content-box__title">'+object.content.title+'</h3></a> </div> ';   
            }
             else if(object.parent == 'about'){
                var string = ' <div class="content-box"><a href="'+object.url+'" class="full-link"><h3 class="content-box__title">'+object.content.title+'</h3><p class="pageparent">' +object.parent+'</p></a> </div> ';   
            }
            else if(typeof object.content.subtitle != 'undefined' && object.parent != 'about'){
                var string = ' <div class="content-box"><a href="'+object.url+'" class="full-link"><h3 class="content-box__title">'+object.content.title+'</h3> <p>'+object.content.subtitle+'</p> <p class="pageparent">' +object.parent+'</p> </a> </div> ';   
            }
            else{
                var string = ' <div class="content-box"><a href="'+object.url+'" class="full-link"> <h3 class="content-box__title">'+object.content.title+'</h3> </a> </div> ';   
            }
                    // Append the results to the container
                    searchResultsContainerMain.append(string);        
            });
            searchResultsContainerMain.append(string);     
        });
    } 
    else {
        // If there are less than 2 or 0 characters in the input, flush the container
        searchResultsContainerMain.html('');
    }
});
</script> </header>
</div>
<!-- Baltan Cutter !-->
<script canvas="canvas" type="text/paperscript">
    var colors = ["rgba(0,0,0,1)"];                
    var rand = Math.floor(Math.random()*colors.length);       

    var intersections = [],
    path,
    cuts = [],
    cutRatio = 40;
    project.currentStyle.fillColor = colors[rand];

    function onResize() {
    project.activeLayer.children = [];
    new Path.Rectangle(view.bounds);
    }

    function getIntersections(point1, point2) {
    var intersections = [];
    var hitResult = project.hitTest(point2);

    if (!hitResult) {
    return;
    }

    path = hitResult.item;

    var curves = path.curves;
    var vector = point2 - point1;
    var line = new Line(point1, point1 + vector, true);

    for (var i = 0, l = curves.length; i < l; i++) {
    var curve = curves[i];
    var pathLine = new Line(curve.point1, curve.point2, false);
    var intersection = line.intersect(pathLine);
    if (intersection) {
    intersections.push(intersection);
    }
    }
    return intersections;
    }

    function makeCut(cut) {
    var first = cut.intersections[0];
    var second = cut.intersections[1];
    var center = first + (second - first) / 2;
    var path = project.hitTest(center).item;
    path.strokeColor = colors[rand];
    for (var i = 0, l = cut.intersections.length; i < l; i++) {
    var intersection = cut.intersections[i];
    var hitResult = path.hitTest(intersection, {tolerance: 5, stroke: true});
    if (!hitResult) {
    return;
    }
    var curve = hitResult.location.curve;
    var vector = curve.point1 - curve.point2;
    var splitted = path.split(path.getNearestLocation(intersection));
    // Move the split segment up a bit, so as to make a cut:
    var segment = splitted.segments[0];
    segment.point -= vector / cutRatio;

    if (splitted !== path) {
    splitted.closed = true;
    path.closed = true;

    if (cut.remove) {
    if (path.length > splitted.length) {
    splitted.remove();
    } else {
    path.remove();
    }
    }
    }
    }
    }

    function onMouseDrag(event) {
    intersections = getIntersections(event.downPoint, event.point);
    if (intersections) {
    var tempLine = new Path(intersections);
    tempLine.strokeColor = 'white';
    tempLine.removeOn({
    up: true,
    drag: true
    });
    }
    }

    function onMouseUp(event) {
    if (!intersections || intersections.length < 2)
    return;
    var cut = {
    intersections: intersections,
    remove: event.modifiers.shift
    };
    cuts.push(cut);
    makeCut(cut);
    }

    var Line = this.Line = Base.extend({
    initialize: function(point1, point2, infinite) {
    if (arguments.length === 3) {
    this.point = point1;
    this.vector = point2.subtract(point1);
    this.infinite = infinite;
    } else {
    this.point = point1;
    this.vector = point2;
    this.infinite = true;
    }
    },

    intersect: function(line) {
    var cross = this.vector.cross(line.vector);
    // Avoid divisions by 0, and errors when getting too close to 0
    if (Math.abs(cross) <= 10e-12)
    return null;
    var v = line.point.subtract(this.point),
    t1 = v.cross(line.vector) / cross,
    t2 = v.cross(this.vector) / cross;
    // Check the ranges of t parameters if the line is not allowed to
    // extend beyond the definition points.
    return (this.infinite || 0 <= t1 && t1 <= 1)
    && (line.infinite || 0 <= t2 && t2 <= 1)
    ? this.point.add(this.vector.multiply(t1)) : null;
    },

    getSide: function(point) {
    var v1 = this.vector,
    v2 = point.subtract(this.point),
    ccw = v2.cross(v1);
    if (ccw == 0) {
    ccw = v2.dot(v1);
    if (ccw > 0) {
    ccw = v2.subtract(v1).dot(v1);
    if (ccw < 0)
    ccw = 0;
    }
    }
    return ccw < 0 ? -1 : ccw > 0 ? 1 : 0;
    },

    getDistance: function(point) {
    var m = this.vector.y / this.vector.x, // slope
    b = this.point.y - (m * this.point.x); // y offset
    // Distance to the linear equation
    var dist = Math.abs(point.y - (m * point.x) - b) / Math.sqrt(m * m + 1);
    return this.infinite ? dist : Math.min(dist,
    point.getDistance(this.point),
    point.getDistance(this.point.add(this.vector)));
    }
    });
  </script>
<main class="main article-page detail-header" role="main">
<p class="article-page-title"></p>
<li class="headerImage project showcase-item article-item highlight">
<canvas id="canvas" resize="" style="background-color:white; mix-blend-mode:lighten;-webkit-tap-highlight-color: rgba(255, 255, 0, 1) !important;"></canvas>
<div class="overlaycolor" style="z-index: -1">
<img class="clicktocut" src="../assets/images/clicktocut.svg"/>
</div>
<div class="caption" style="pointer-events:none;">
<div class="wrap">
<div class="article page-title">
<h1 class="title"><span> </span></h1>
</div>
</div>
</div>
</li>
<div style="clear:both;"></div>
<div class="wrap">
<div class="socialdiv">
<p>Be Sociable, Share:</p>
<div><a class="fb-icon" href="http://www.facebook.com/sharer/sharer.php?u=https://www.baltanlaboratories.org/error" target="_blank"></a></div>
<div><a class="twitter-share-button tw-icon" href="http://twitter.com/share?text=Error &amp;url=https://www.baltanlaboratories.org/error" target="_blank"></a></div>
</div>
</div>
</main>
<script type="text/javascript">
    $('.headerImage').bind('touchstart mousedown', function(){
      $('.clicktocut').animate({'opacity': '0'}, 80);
    });
  </script>
<script src="https://www.baltanlaboratories.org/assets/plugins/embed/js/embed.js"></script>
<main class="main" role="main">
<div class="wrap wide">
<h2>The page has not been found.</h2><br/>
<h2></h2><p><a href="https://www.baltanlaboratories.org">&gt; homepage</a></p>
</div>
</main>
<div class="news-gallery privacy" id="opened-gallery-privacy">
<div id="flickrembed"></div>
<div>
<header class="cd-panel-header flickrheader">
<a class="cd-panel-close privacy-close" href="#gallery" id="closePrivacy">Close Button</a>
</header>
<div class="description">
<h2>Privacy Statement</h2>
<p>Baltan Laboratories Baltan Laboratories, gevestigd aan Kastanjelaan 500, 5616 LZ Eindhoven, is verantwoordelijk voor de verwerking van persoonsgegevens zoals weergegeven in deze privacyverklaring.</p>
<p>Contactgegevens:<br/>
                www.baltanlaboratories.org Postbus 4042<br/>
            5604 EA Eindhoven +31616717166</p>
<h4>Persoonsgegevens die wij verwerken</h4>
<p>Baltan Laboratories verwerkt je persoonsgegevens doordat je gebruik maakt van onze diensten en/of omdat je deze gegevens zelf aan ons verstrekt.</p>
<p>Hieronder vind je een overzicht van de persoonsgegevens die wij verwerken:</p>
<ul class="list">
<li>Voor- en achternaam</li>
<li>Adresgegevens</li>
<li>Telefoonnummer</li>
<li>E-mailadres</li>
<li>Gegevens over jouw activiteiten op onze website</li>
<li>Bankrekeningnummer</li>
</ul>
<h4>Bijzondere en/of gevoelige persoonsgegevens die wij verwerken</h4>
<p>Baltan Laboratories verwerkt de volgende bijzondere en/of gevoelige persoonsgegevens van jou:</p>
<ul class="list">
<li>burgerservicenummer (BSN)</li>
</ul>
<h4>Met welk doel en op basis van welke grondslag wij persoonsgegevens verwerken</h4>
<p>Baltan Laboratories verwerkt jouw persoonsgegevens voor de volgende doelen:</p>
<ul class="list">
<li>Het afhandelen van jouw betaling</li>
<li>Verzenden van onze nieuwsbrief en/of reclamefolder</li>
<li>Om goederen en diensten bij je af te leveren</li>
<li>Baltan Laboratories verwerkt ook persoonsgegevens als wij hier wettelijk toe verplicht zijn, zoals gegevens die wij nodig hebben voor onze belastingaangifte.</li>
</ul>
<h4>Geautomatiseerde besluitvorming</h4>
<p>Baltan Laboratories neemt niet op basis van geautomatiseerde verwerkingen besluiten over zaken die (aanzienlijke) gevolgen kunnen hebben voor personen. Het gaat hier om besluiten die worden genomen door computerprogramma's of -systemen, zonder dat daar een mens (bijvoorbeeld een medewerker van Baltan Laboratories) tussen zit.</p>
<h4>Hoe lang we persoonsgegevens bewaren</h4>
<p>Baltan Laboratories bewaart je persoonsgegevens niet langer dan strikt nodig is om de doelen te realiseren waarvoor je gegevens worden verzameld. Wij hanteren een bewaartermijn van maximaal 2 jaar voor persoonsgegevens en contactinformatie met als reden de garantie van continuïteit in de uitvoering van de programma's van Baltan Laboratories.</p>
<h4>Delen van persoonsgegevens met derden</h4>
<p>Baltan Laboratories verkoopt jouw gegevens niet aan derden en zal deze uitsluitend verstrekken indien dit nodig is voor de uitvoering van onze overeenkomst met jou of om te voldoen aan een wettelijke verplichting. Met bedrijven die jouw gegevens verwerken in onze opdracht, sluiten wij een bewerkersovereenkomst om te zorgen voor eenzelfde niveau van beveiliging en vertrouwelijkheid van jouw gegevens. Baltan Laboratories blijft verantwoordelijk voor deze verwerkingen.</p>
<h4>Cookies, of vergelijkbare technieken, die wij gebruiken</h4>
<p>Baltan Laboratories gebruikt geen cookies of vergelijkbare technieken.</p>
<h4>Gegevens inzien, aanpassen of verwijderen</h4>
<p>Je hebt het recht om je persoonsgegevens in te zien, te corrigeren of te verwijderen. Daarnaast heb je het recht om je eventuele toestemming voor de gegevensverwerking in te trekken of bezwaar te maken tegen de verwerking van jouw persoonsgegevens door Baltan Laboratories en heb je het recht op gegevensoverdraagbaarheid. Dat betekent dat je bij ons een verzoek kan indienen om de persoonsgegevens die wij van jou beschikken in een computerbestand naar jou of een ander, door jou genoemde organisatie, te sturen.</p>
<p>Je kunt een verzoek tot inzage, correctie, verwijdering, gegevensoverdraging van je persoonsgegevens of verzoek tot intrekking van je toestemming of bezwaar op de verwerking van jouw persoonsgegevens sturen naar <a href="info@baltanlaboratories.org" target="_top">info@baltanlaboratories.org</a>.</p>
<p>Om er zeker van te zijn dat het verzoek tot inzage door jou is gedaan, vragen wij jou een kopie van je identiteitsbewijs met het verzoek mee te sturen. Maak in deze kopie je pasfoto, MRZ (machine readable zone, de strook met nummers onderaan het paspoort), paspoortnummer en Burgerservicenummer (BSN) zwart. Dit ter bescherming van je privacy. We reageren zo snel mogelijk, maar binnen vier weken, op jouw verzoek.</p>
<p>Baltan Laboratories wil je er tevens op wijzen dat je de mogelijkheid hebt om een klacht in te dienen bij de nationale toezichthouder, de Autoriteit Persoonsgegevens. Dat kan via de volgende link: <a href="https://autoriteitpersoonsgegevens.nl/nl/contact-met-de-autoriteit-persoonsgegevens/tip-ons" target="_blank">https://autoriteitpersoonsgegevens.nl/nl/contact-met-de-autoriteit-persoonsgegevens/tip-ons</a></p>
<h4>Hoe wij persoonsgegevens beveiligen</h4>
<p>Baltan Laboratories neemt de bescherming van jouw gegevens serieus en neemt passende maatregelen om misbruik, verlies, onbevoegde toegang, ongewenste openbaarmaking en ongeoorloofde wijziging tegen te gaan. Als jij het idee hebt dat jouw gegevens toch niet goed beveiligd zijn of er aanwijzingen zijn van misbruik, neem dan contact op met ons via <a href="info@baltanlaboratories.org" target="_top">info@baltanlaboratories.org</a>.</p>
</div>
</div>
</div>
<script type="text/javascript">
  function checkForm(form)
  {
    if(!form.optin.checked) {
        var circle = document.getElementsByClassName('errorcircle')[0];
        var formcontainer = document.getElementById('mc-embedded-subscribe-form');
        circle.classList.add('show');
        formcontainer.classList.add('is-checked');
        form.optin.focus();
        return false;
    }
    return true;
  }
</script>
<footer class="footer cf" id="footer" role="contentinfo">
<div class="newsletter newsletterFooter" id="mc_embed_signup">
<h4 id="newsletterTitle">Stay up to date with Baltan's activities.</h4>
<form action="https://baltanlaboratories.us7.list-manage.com/subscribe/post?u=53e523a063d916680096d60ad&amp;id=b968e307eb" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" onsubmit="return checkForm(this);" target="_blank" validate="">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group newsletterMail">
<h4 class="event"><input class="required email event" id="mce-EMAIL" name="EMAIL" placeholder="e-mail address" type="email" value=""/></h4>
</div>
<div class="clear blue-button">
<h4 class="event"><input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe"/></h4>
</div>
<div class="clear" id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div aria-hidden="true" style="position: absolute; left: -5000px;"><input name="b_484445d437bc0d02d929dfbef_225d3a20bd" tabindex="-1" type="text" value=""/>
</div>
</div>
<label class="check-container">
<input class="checkbox required" id="gdpr_253" name="optin" required="" type="checkbox"/>
<span class="errorcircle"></span>
<span class="checkmark"></span>
</label>
<p class="opt-in">I would like to receive further marketing related email communications from Baltan Laboratories.</p>
<p class="privacy-link">Read our <span class="gallery-block privacy" id="open-gallery-privacy"><a>Privacy Statement</a></span>
            to see how your data is processed.</p>
</form>
</div>
<script src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js" type="text/javascript"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<div class="event-page">
<div class="wrap">
<hr/>
<div class="footerlogo"></div>
<div class="wrapper">
<div class="footercontact">
<div class="line1">
<div class="label"><h4>Office</h4></div>
<div class="content">
<p>Galileistraat 2</p>
<p>5621 AE Eindhoven</p>
<p><a href="mailto:info@Baltanlaboratories.org">info@Baltanlaboratories.org</a></p>
<p><a href="tel:0031616717166">+31 (0)6 16 717 166</a></p>
</div>
<div class="social">
<h4>Follow</h4>
<div class="facebook" onclick="window.open('https://www.facebook.com/baltanlaboratories/', '_blank')"></div>
<div class="twitter" onclick="window.open('https://twitter.com/baltanlab', '_blank')"></div>
<div class="linkedin" onclick="window.open('https://www.linkedin.com/company/baltan-laboratories/', '_blank')"></div>
</div>
</div>
<div class="line2">
<div class="label"><h4>Visit</h4></div>
<div class="content">
<p>NatLab</p>
<p>Kastanjelaan 500</p>
<p>5616 LZ Eindhoven</p>
</div>
<div class="social">
<h4>Explore more</h4>
<div class="vimeo" onclick="window.open('https://vimeo.com/baltanlaboratories', '_blank')"></div>
<div class="flickr" onclick="window.open('https://www.flickr.com/photos/baltanlaboratories/', '_blank')"></div>
<div class="instagram" onclick="window.open('https://www.instagram.com/baltanlaboratories/', '_blank')"></div>
</div>
</div>
<div class="line4">
<div class="label"><h4>Postal</h4></div>
<div class="content">
<p>Postbus 4042</p>
<p>5604 EA Eindhoven</p>
</div>
</div>
<div class="line3">
<div class="social">
<h4>Follow</h4>
<div class="facebook" onclick="window.open('https://www.facebook.com/baltanlaboratories/', '_blank')"></div>
<div class="twitter" onclick="window.open('https://twitter.com/baltanlab', '_blank')"></div>
<div class="linkedin" onclick="window.open('https://www.linkedin.com/company/baltan-laboratories/', '_blank')"></div>
</div>
<div class="social">
<h4>Explore more</h4>
<div class="vimeo" onclick="window.open('https://vimeo.com/baltanlaboratories', '_blank')"></div>
<div class="flickr" onclick="window.open('https://www.flickr.com/photos/baltanlaboratories/', '_blank')"></div>
<div class="instagram" onclick="window.open('https://www.instagram.com/baltanlaboratories/', '_blank')"></div>
</div>
</div>
</div>
<div class="footerpartners">
<a href="https://stimuleringsfonds.nl/en/"><div class="partner1"></div></a>
<a href="https://www.cultuureindhoven.nl/"><div class="partner2"></div></a>
<a href="http://brabant.nl/"><div class="partner3"></div></a>
</div>
</div>
</div>
<div style="text-align:right; width:100%; padding-bottom:1rem">
<a href="http://heyheydehaas.com"><img src="../../assets/images/hhdh_credit.svg" style="width:100px; height: 30px"/></a>
</div>
</div>
</footer>
<script src="https://www.baltanlaboratories.org/assets/js/script.js"></script>
<script src="https://www.baltanlaboratories.org/assets/js/classie.js"></script> <script src="https://www.baltanlaboratories.org/assets/js/uisearch.js"></script><script>
    new UISearch( document.getElementById( 'sb-search' ) );
 </script>
<script type="text/javascript">
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    if (isIE || isEdge) {
        if ($('#canvas').length){
            $('#canvas').css('opacity', '0');
        }  
        if ($('.overlayImage').length){
            $('.overlaycolor').css({'mix-blend-mode': 'unset', 'opacity': '0.6'})
        }
    }
  </script>
</body>
</html>