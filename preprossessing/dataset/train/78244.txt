<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"7bd318c881",applicationID:"115713378"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,w=["click","keydown","mousedown","pointerdown","touchstart"];w.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?u(e,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){e&&a&&e(n,r,i);for(var c=t(i),f=v(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[h[n]];return p&&p.push([b,n,r,c]),c}}function o(e,t){y[e]=v(e).concat(t)}function m(e,t){var n=y[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return y[e]||[]}function g(e){return p[e]=p[e]||i(n)}function w(e,t){s(e,function(e,n){t=t||"feature",h[n]=t,t in d||(d[t]=[])})}var y={},h={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:w,abort:c,aborted:!1};return b}function o(e){return u(e,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=e("gos"),s=e(6),d={},p={},l=t.exports=i();t.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(y,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var w=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1194.min.js"},h=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:w,features:{},xhrWrappable:h,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var x=0},{}],"wrap-function":[function(e,t,n){function r(e,t){function n(t,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],e)}c(n+"start",[o,a,f],s,u);try{return p=t.apply(a,o)}catch(m){throw c(n+"err",[o,a,m],s,u),m}finally{c(n+"end",[o,a,p],s,u)}}return a(t)?t:(n||(n=""),nrWrapper[p]=t,o(t,nrWrapper,e),nrWrapper)}function r(e,t,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<t.length;u++)f=t[u],c=e[f],a(c)||(e[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!m||t){var c=m;m=!0;try{e.emit(n,r,o,t,a)}catch(f){i([f,n,r,o],e)}m=c}}return e||(e=s),n.inPlace=r,n.flag=p,n}function i(e,t){t||(t=s);try{t.emit("internal-error",e)}catch(n){}}function o(e,t,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(e);return r.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(o){i([o],n)}for(var a in e)l.call(e,a)&&(t[a]=e[a]);return t}function a(e){return!(e&&e instanceof Function&&e.apply&&!e[p])}function c(e,t){var n=t(e);return n[p]=e,o(e,n,s),n}function f(e,t,n){var r=e[t];e[t]=c(r,n)}function u(){for(var e=arguments.length,t=new Array(e),n=0;n<e;++n)t[n]=arguments[n];return t}var s=e("ee"),d=e(7),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;t.exports=r,t.exports.wrapFunction=c,t.exports.wrapInPlace=f,t.exports.argsToArray=u},{}]},{},["loader"]);</script>
<title>AdSpruce | Mobile Video Advertising | Ad Serving Platform</title>
<meta content="initial-scale=1.0, width=device-width, user-scalable=no" name="viewport"/>
<meta content="Learn about online video advertising and mobile video advertising opportunities for smartphones, tablets, feature phones and desktop from AdSpruce." name="description"/>
<meta content="Premium Inventory, Video, Mobile, Smartphone, Feature Phone, Desktop, Advertising, Ads, Rich Media, Monetise" name="keywords"/>
<meta content="The Mobile Video Advertising Platform" name="DC.title"/>
<meta content="GB-HCK" name="geo.region"/>
<meta content="London" name="geo.placename"/>
<meta content="51.522651;-0.085768" name="geo.position"/>
<meta content="51.522651, -0.085768" name="ICBM"/>
<!-- Style Sheets -->
<link href="../../stylesheets/materialize.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic" rel="stylesheet" type="text/css"/>
<!-- Favicon -->
<link href="/ico/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/ico/favicon.ico" rel="shortcut icon"/>
<link href="/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="../../js/bin/materialize.min.js"></script>
<script src="../../js/main.min.js"></script>
<script src="../../js/showads.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('.modal-trigger').leanModal();
        $(".button-collapse").sideNav();
        $('.slider').slider();
        $('.carousel').carousel();

        function setBlur () {
            $('.carousel-item').each(function(){

                var z = parseInt($(this).css('z-index'));

                if(z === -1) {
                    $(this).css({"-webkit-filter":"blur(1px)","-moz-filter":"blur(1px)", "filter":"blur(1px)"});
                }else if(z === -2) {
                    $(this).css({"-webkit-filter":"blur(2px)","-moz-filter":"blur(2px)", "filter":"blur(2px)"});
                }else{
                    $(this).css({"-webkit-filter":"blur(0px)","-moz-filter":"blur(0px)", "filter":"blur(0px)"});
                }
            });
        }

        var blocktimer = setInterval(function() {
            $('.carousel').carousel('next');
            setTimeout(setBlur,300);
        }, 7000);

        $('.carousel-item').on('click', function(){
            clearInterval(blocktimer);
            setTimeout(setBlur,300);
        });

        setBlur();
        $('ul.tabs').tabs();
        // Initialize collapsible
        $('.collapsible').collapsible();
        $('.tooltipped').tooltip({"delay": 50});
        $('select').material_select();
        $('.dropdown-button').dropdown({
                inDuration: 300,
                outDuration: 225,
                constrain_width: true, // Does not change width of dropdown to that of the activator
                hover: false
            }
        );

        // Check if an ad blocker is detected:
        if (window.canRunAds === undefined) {
            showAdBlockerWarning();
        }
    });

    function showAdBlockerWarning() {
        $('#blocker-warning-modal').openModal();
    }

</script>
<script src="../js/wow.min.js"></script>
<script>
    var wow = new WOW(
        {
            boxClass: 'animation-adspruce',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 150,          // distance to the element when triggering the animation (default is 0)
            mobile: false,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated

            }
        }
    );
    wow.init();
</script>
<!-- Iubenda Cookie Solution Code -->
<style type="text/css"> #iubenda-cs-banner { bottom: 0px !important; left: 0px !important; position: fixed !important; width: 100% !important; z-index: 99999998 !important; background-color: black; } .iubenda-cs-content { display: block; margin: 0 auto; padding: 20px; width: auto; font-family: Helvetica,Arial,FreeSans,sans-serif; font-size: 14px; background: #000; color: #fff;} .iubenda-cs-rationale { max-width: 900px; position: relative; margin: 0 auto; } .iubenda-banner-content > p { font-family: Helvetica,Arial,FreeSans,sans-serif; line-height: 1.5; } .iubenda-cs-close-btn { margin:0; color: #fff; text-decoration: none; font-size: 14px; position: absolute; top: 0; right: 0; border: none; } .iubenda-cs-cookie-policy-lnk { text-decoration: underline; color: #fff; font-size: 14px; font-weight: 900; } </style> <script type="text/javascript"> var _iub = _iub || []; _iub.csConfiguration = {"banner":{"slideDown":false,"applyStyles":false},"lang":"en","siteId":1212952,"cookiePolicyUrl":"/cookies","cookiePolicyInOtherWindow":true,"cookiePolicyId":99465726}; </script><script async="" charset="UTF-8" src="//cdn.iubenda.com/cookie_solution/safemode/iubenda_cs.js" type="text/javascript"></script>
<!-- Iubenda Consent Solution Code -->
<script src="https://cdn.iubenda.com/consent_solution/iubenda_cons.js" type="text/javascript"></script>
<script type="text/javascript">
        _iub.cons.init({
            api_key: "oGuf3peVhfKHB6UqPYPEO0BSSyAqMRgT"
        });
    </script>
</head>
<body>
<nav>
<div class="nav-wrapper">
<div class="container">
<a class="brand-logo" href="/"><span>AdSpruce</span></a>
<ul class="right side-nav collapsible collapsible-accordion" id="nav-mobile">
<li><a href="/advertisers" title="Advertise">Advertise</a></li>
<li><a href="/publishers" title="Monetise">Monetise</a></li>
<li><a href="/mobile-video-ad-serving" title="Ad Server">Ad Server </a></li>
<li class="no-padding">
<div class="collapsible-header">Ad Formats<i class="mdi-navigation-arrow-drop-down"></i></div>
<div class="collapsible-body">
<ul>
<li><a href="/advertisers/pre-roll-video-advertising" title="Pre-Roll">Pre-Roll</a></li>
<li><a href="/advertisers/outstream-video-advertising" title="Outstream Video Advertising">Outstream Video Advertising</a></li>
<li><a href="/advertisers/video-in-banner" title="Video Banner">Video In-Banner</a></li>
<li><a href="/advertisers/video-in-page" title="G-Spot">Video In-Page</a></li>
<li><a href="/advertisers/full-page-takeover" title="Full-Page Takeover">Full-Page Takeover</a></li>
</ul>
</div>
</li>
<li class="no-padding">
<div class="collapsible-header">Technology<i class="mdi-navigation-arrow-drop-down"></i></div>
<div class="collapsible-body">
<ul>
<li><a href="/technology/adspruce-adk" title="AdSpruce ADK">AdSpruce ADK</a></li>
<li><a href="/technology/any-screen-lander" title="AnyScreen Lander">AnyScreen Lander</a></li>
<li><a href="/technology/hot-analytics" title="Hot Analytics">Hot Analytics</a></li>
<li><a href="/technology/html5-ad-studio" title="HTML5 Ad Studio">HTML5 Ad Studio</a></li>
<li><a href="/technology/i-endcard" title="iEndCard">iEndCard</a></li>
<li><a href="/technology/videodax" title="VideoDax">VideoDax</a></li>
<li><a href="/technology/videoserve" title="VideoServe">VideoServe</a></li>
<li><a href="/technology/viewability" title="Viewability">Viewability</a></li>
</ul>
</div>
</li>
<li><a href="/contact-adspruce" title="Contact">Contact</a></li>
<li><a class="login-nav" href="/login" target="_blank" title="Log in">Log in</a></li>
</ul>
<!-- Include this line below -->
<a class="button-collapse" data-activates="nav-mobile" href="#"><i class="mdi-navigation-menu"></i></a>
<!-- End -->
</div>
</div>
</nav>
<div class="fixed-bg main homepage row">
<div class="container">
<div class="intro ad-formats animation-adspruce fadeInUp" data-wow-delay="0s" data-wow-duration="1s">
<h1 class="home">Programmatic Mobile Video Technology</h1>
<h2 class="home">AdSpruce delivers beautiful rich-media ads and interactive video to more devices than anyone else</h2>
</div><!-- .intro -->
<div class="row">
<div class="slider phone-slider">
<ul class="slides">
<li>
<img alt="James Bond – Full Page Takeover" class="caption left-align" src="/images/fpto-covers/bond.png" title="James Bond – Full Page Takeover"/>
</li>
<li class="lvb">
<img alt="Beats 1 – Full Page Takeover" class="caption right-align" src="/images/lvb-covers/beats1.png" title="Beats 1 – Full Page Takeover"/>
</li>
<li>
<img alt="Reese's Peanut Butter Cup – Full Page Takeover" class="caption center-align" src="/images/fpto-covers/reeses-peanut-butter-cups.png" title="Reese's Peanut Butter Cup – Full Page Takeover"/>
</li>
<li class="lvb">
<img alt="Listerine – Full Page Takeover" class="caption left-align" src="/images/lvb-covers/listerine.png" title="Listerine – Full Page Takeover"/>
</li>
<li>
<img alt="Game Of Thrones – Full Page Takeover" class="caption right-align" src="/images/fpto-covers/got-hbo.png" title="Game Of Thrones – Full Page Takeover"/>
</li>
<li class="lvb">
<img alt="H &amp; M – Full Page Takeover" class="caption center-align" src="/images/lvb-covers/h&amp;m.png" title="H &amp; M – Full Page Takeover"/>
</li>
<li>
<img alt="Skittles – Full Page Takeover" class="caption left-align" src="/images/fpto-covers/skittles.png" title="Skittles – Full Page Takeover"/>
</li>
<li class="lvb">
<img alt="Barbie – Full Page Takeover" class="caption right-align" src="/images/lvb-covers/barbie.png" title="Barbie – Full Page Takeover"/>
</li>
<li>
<img alt="Godzilla – Full Page Takeover" class="caption center-align" src="/images/fpto-covers/godzilla.png" title="Godzilla – Full Page Takeover"/>
</li>
<li>
<img alt="Puma – Full Page Takeover" class="caption left-align" src="/images/fpto-covers/puma.png" title="Puma – Full Page Takeover"/>
</li>
<li>
<img alt="Oral B – Full Page Takeover" class="caption right-align" src="/images/fpto-covers/oral-b.png" title="Oral B – Full Page Takeover"/>
</li>
</ul>
</div>
<div class="phone-slider-holder">
</div>
</div>
</div><!-- .container -->
</div><!-- .fixed-bg .publishers -->
<div class="row main" id="first-block">
<div class="container">
<h2 class="sub-main animation-adspruce fadeInUp" data-wow-delay="0" data-wow-duration="1s">The Complete Mobile Web<br/>Advertising Platform</h2>
<div class="row">
<div class="col s12 m6 l4 animation-adspruce fadeIn" data-wow-delay="0.1s" data-wow-duration="1s">
<div class="icon-block">
<i class="large material-icons"></i>
</div><!-- .icon-block -->
<div class="content-plain center">
<h3>Publishers</h3>
<p>Generate ad revenue<br/>from your mobile website</p>
</div><!-- .content-plain -->
</div><!-- col s12 m6 l4 -->
<div class="col s12 m6 l4 animation-adspruce fadeIn" data-wow-delay="0.2s" data-wow-duration="1s">
<div class="icon-block">
<i class="large material-icons"></i>
</div><!-- .icon-block -->
<div class="content-plain center">
<h3>Advertisers</h3>
<p>Interactive ad spots for brand<br/>building and performance campaigns</p>
</div><!-- .content-plain -->
</div><!-- col s12 m6 l4 -->
<div class="col s12 offset-m3 m6 l4 animation-adspruce fadeIn" data-wow-delay="0.3s" data-wow-duration="1s">
<div class="icon-block">
<i class="large material-icons"></i>
</div><!-- .icon-block -->
<div class="content-plain center">
<h3>Games Developers</h3>
<p>Earn more revenue by monetising<br/>any HTML5 game on the mobile web</p>
</div><!-- .content-plain -->
</div><!-- col s12 m6 l4 -->
</div><!-- .row -->
</div><!-- .container -->
</div><!-- #first-block -->
<div class="grey lighten-3 row main" id="first-block">
<div class="container">
<h2 class="sub-main animation-adspruce fadeInUp" data-wow-delay="0" data-wow-duration="1s">Breakthrough Technology &amp; Advertising Solutions</h2>
<div class="carousel">
<div class="carousel-item one">
<img alt="Programmatic Mobile Video SSP" src="/images/homepage/programmatic-mobile-video-ssp2.png" title="Programmatic Mobile Video SSP"/>
<div class="content-info">
<h3>Programmatic Mobile Video SSP</h3>
<p>AdSpruce’s Ads Development Kit (ADK) is the industry’s most powerful
                monetisation suite. Our technology will enhance your revenue by delivering
                new opportunities for your mobile web inventory and in-app video.
                It’s quick and easy to get started.</p>
</div>
<div class="content-action">
<a href="/technology/adspruce-adk" title="Programmatic Mobile Video SSP">Learn More</a>
</div>
</div>
<div class="carousel-item two">
<img alt="Publisher Mobile Ad Server" src="/images/homepage/publisher-mobile-ad-server2.png" title="Publisher Mobile Ad Server"/>
<div class="content-info">
<h3>Publisher Mobile Ad Server</h3>
<p>Maximise available inventory through outstream ads, video banners,
                in-stream ads, pre-roll ads, full-page interstitials, in-page ads and
                rich-media ads. AdSpruce can deliver engaging video and rich-media experiences
                at any part of the user journey. </p>
</div>
<div class="content-action">
<a href="/technology/videoserve" title="Publisher Mobile Ad Server">Learn More</a>
</div>
</div>
<div class="carousel-item three">
<img alt="Video &amp; Rich-Media Ad Formats" src="/images/homepage/video-and-rich-media-ad-formats2.png" title="Video &amp; Rich-Media Ad Formats"/>
<div class="content-info">
<h3>Video &amp; Rich-Media Ad Formats</h3>
<p>The world's only AdServer that works on all mobile devices and optimises
                interactive video for mobile networks. Proprietary delivery and player
                technology ensures mobile video ads play instantly on
                smartphones regardless of connection speed.</p>
</div>
<div class="content-action">
<a href="/advertisers" title="Video &amp; Rich-Media Ad Formats">Learn More</a>
</div>
</div>
<div class="carousel-item four">
<img alt="Analytics Dashboard &amp; Monetisation Control" src="/images/homepage/analytics-dashboard-and-monetisation-control2.png" title="Analytics Dashboard &amp; Monetisation Control"/>
<div class="content-info">
<h3>Analytics Dashboard &amp; Monetisation Control</h3>
<p>Adspruce has reinvented how ads work on mobile devices, and gives
                Publishers the dashboard controls to change frequency of ads and powerful
                video monetisation tools.</p>
</div>
<div class="content-action">
<a href="/technology/hot-analytics" title="Analytics Dashboard &amp; Monetisation Control">Learn More</a>
</div>
</div>
<div class="carousel-item five">
<img alt="Introducing Full-Page Takeover" src="/images/homepage/fpto-ad-spot.png" title="Introducing Full-Page Takeover"/>
<div class="content-info">
<h3>Introducing Full-Page Takeover</h3>
<p>High-impact advertising that demands attention, Full-Page Takeover combines
                rich-media elements with lightning-fast video </p>
</div>
<div class="content-action">
<a href="/advertisers/full-page-takeover" title="Introducing Full-Page Takeover">Learn More</a>
</div>
</div>
</div>
</div>
</div>
<!-- .row -->
<footer class="grey darken-2">
<div class="container">
<div class="row">
<div class="col l6 m12 s12 footer-about">
<h5 class="white-text">About AdSpruce</h5>
<p class="grey-text text-lighten-4">AdSpruce is the leading programmatic video advertising Supply
                    Side Platform (SSP) on the mobile web dedicated to helping publishers maximize the value of
                    their inventory through high-performance video ad formats and direct sales to brands and
                    agencies. Our ad server can deliver video to more devices than anyone else and we own our
                    whole technology stack enabling us to deliver industry-leading ad experiences, targeting,
                    tracking, security and ad spot controls to customers. </p>
</div><!-- .col l6 s12 -->
<div class="col l6 s12">
<h5 class="white-text">Browse Pages</h5>
<ul class="col s12 m4 no-pad browse-links">
<span class="white-text nav-subheading">Publishers</span>
<li><a class="white-text" href="/technology/adspruce-adk" title="Ads Development Kit (ADK)">Ads Development Kit</a></li>
<li><a class="white-text" href="/publishers" title="Monetisation">Monetisation</a></li>
<li><a class="white-text" href="/technology/html5-ad-studio" title="HTML5 Ad Studio">HTML5 Ad Studio</a></li>
<li><a class="white-text" href="/html5-game-monetisation" title="HTML5 Game Monetisation">HTML5 Game Monetisation</a></li>
<li><a class="white-text" href="/demo" title="Demos">Demos</a></li>
</ul><!-- .browse-links -->
<ul class="col s12 m4 no-pad browse-links">
<span class="white-text nav-subheading">Advertisers</span>
<li><a class="white-text" href="/mobile-video-ad-serving" title="Ad Server">Ad Server</a></li>
<li><a class="white-text" href="/technology/i-endcard" title="iEndCard">iEndCard</a></li>
<li><a class="white-text" href="/technology/any-screen-lander" title="AnyScreen Lander">AnyScreen Lander</a></li>
<li><a class="white-text" href="/technology/viewability" title="Viewability">Viewability</a></li>
<li><a class="white-text" href="/technology/videodax" title="VideoDax">VideoDax</a></li>
<li><a class="white-text" href="/technology/hot-analytics" title="Hot Analytics">Hot Analytics</a></li>
</ul><!-- .browse-links -->
<ul class="col s12 m4 no-pad browse-links">
<span class="white-text nav-subheading">Company</span>
<li><a class="white-text" href="/about-adspruce" title="About AdSpruce">About</a></li>
<li><a class="white-text" href="/adspruce-press-kit" title="Press Room">Press Room</a></li>
<li><a class="white-text" href="/our-video-advertising-clients" title="Previous Campaigns">Previous Campaigns</a></li>
<li><a class="white-text" href="/advertising-marketing-industry-whitepapers" title="Industry Whitepapers">Industry Whitepapers</a></li>
<li><a class="white-text" href="/video-advertising-careers" title="Video Advertising Careers">Video Advertising Careers</a></li>
<li><a class="white-text" href="https://assets.adspruce.com/website/branding/adspruce-brand-guide.pdf" target="_blank" title="Adspruce Brand Guide">Brand Guide</a></li>
</ul><!-- .browse-links -->
</div><!-- .col l6 s12 -->
</div><!-- .row -->
</div><!-- .container -->
<div class="footer-copyright">
<div class="container">
<div class="row">
<div class="col s12 m12 l6 footer-link-holder">
<a class="footer-link" href="/advertiser-terms" title="Advertiser Terms">Advertiser Terms</a>
<a class="footer-link" href="/publisher-terms" title="Publisher Terms">Publisher Terms</a>
<a class="footer-link" href="/privacy" title="Privacy Policy">Privacy Policy</a>
<a class="footer-link" href="/sitemap" title="Sitemap">Sitemap</a>
</div><!-- .footer-link-holder -->
<div class="col s12 m12 offset-l3 l3 social-media">
<a class="tooltipped" data-position="top" data-tooltip="Facebook" href="http://www.facebook.com/pages/Ad-Spruce/436907619716207" target="_blank" title="Adspruce Facebook"><i class="fa fa-facebook"></i><span class="none">AdSpruce Facebook</span></a>
<a class="tooltipped" data-position="top" data-tooltip="Twitter" href="http://twitter.com/AdSpruce" target="_blank" title="Adspruce Twitter"><i class="fa fa-twitter"></i><span class="none">Adspruce Twitter</span></a>
<a class="tooltipped" data-position="top" data-tooltip="Google+" href="https://plus.google.com/+AdSpruce" rel="publisher" target="_blank" title="Adspruce Google+"><i class="fa fa-google"></i><span class="none">Adspruce Google+r</span></a>
<a class="tooltipped" data-position="top" data-tooltip="LinkedIn" href="http://www.linkedin.com/company/ad-spruce" target="_blank" title="Adspruce LinkedIn"><i class="fa fa-linkedin"></i><span class="none">Adspruce LinkedIn</span></a>
<a class="tooltipped" data-position="top" data-tooltip="Pinterest" href="http://pinterest.com/adspruce/" target="_blank" title="Adspruce Pinterest"><i class="fa fa-pinterest"></i><span class="none">Adspruce Pinterest</span></a>
</div><!-- .social-media -->
</div><!-- .row -->
</div><!-- .container -->
</div><!-- .footer-copyright -->
</footer><!-- .grey darken-2 -->
<div class="modal" id="blocker-warning-modal">
<div class="modal-content">
<h4>Ad Blocker Detected!</h4>
<p>We have detected that you are currently running an ad blocker. We will
        NOT show you any adverts on this site, however, ad blockers will block
        certain parts of this website and it will therefore not run as intended.</p>
<p>Please turn off your ad blocker, or disable it for this website, and then
        refresh the page to continue using the AdSpruce website.</p>
</div>
<div class="modal-footer">
<a class="modal-action modal-close waves-effect btn-flat" href="#">OK</a>
</div>
</div>
<!-- Google Analytics Code -->
<script>

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {

                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),

            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-40722506-1', 'auto');

    ga('send', 'pageview');

</script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"7bd318c881","applicationID":"115713378","transactionName":"ZABQYhNUXkVRUUxcVl1KZ0QIGllYVFdAG0lbFQ==","queueTime":0,"applicationTime":0,"atts":"SEdTFFtOTUs=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
