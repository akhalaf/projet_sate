<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="telephone=no" name="format-detection"/>
<title>No se ha encontrado nada relativo a  Wp Content Languages Wellx %09%0A</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.portelas.es/xmlrpc.php" rel="pingback"/>
<!-- Favicons
	================================================== -->
<link href="https://www.portelas.es/wp-content/themes/shopifiq/images/icons/favicon.ico" rel="shortcut icon"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/images/icons/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/images/icons/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/images/icons/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/fontawesome/css/font-awesome.css" rel="stylesheet"/>
<!--[if lte IE 8]>
		<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/fontawesome/css/font-awesome-ie7.min.css" rel="stylesheet">
	<![endif]-->
<!--[if IE 7]>
  		<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/ie7.css" rel="stylesheet">
	<![endif]-->
<!--[if IE 8]>
    	<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/ie8.css" rel="stylesheet">
	<![endif]-->
<!--[if lte IE 8]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!--[if lt IE 9]>
        <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
        document.createElement('hgroup');
        </script>
        <![endif]-->
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<style>
		body, html {
			overflow-x: hidden;
		}
	</style>
<style>
h1 { font-size: 30px; }
h2 { font-size: 24px; }
h3 { font-size: 20px; }
h4, .hentry h2 { font-size: 14px; }
h5 { font-size: 12px; }
.announce-after, .quotes article, .testimonial, body, html, textarea  {
	font-family: Arial, Helvetica, sans-serif;
}
h1, h2, h3, h4, h5, nav a, .icon-strip a h2, .slider h2, .portfolio-content h3, .blog h2, .comment-number h4, .comment h4, .pricing-table-price {
	font-family: 'Open Sans', Arial, sans-serif; font-weight: 700; font-style: normal;
}
h1.product_title {
	color: #fff;
}
.header-button, .single-page, .portfolio h3, .comments-number, .post-date-comments div .day-month, #comment-header, footer#site-footer h3.widget-title, .person h3, .statement-box button, .post-date-comments3 div .day-month, .tabs .tabs-menu li, .pricing-table-title, ul.products li.product .onsale {
	font-family: 'Open Sans', Arial, sans-serif; font-weight: 600; font-style: normal;
}
nav ul li ul li a, .slider p, .breadcrumbs, .selected-filter, .blog footer, .comments-text, .tags-author, .comment-number h3, .comment-meta, .comment-reply-link, input[type="button"], .progress, .post-date-comments2, .quotes article span, .testimonial span, aside .tab.popular .post, .latest-post .subheading, div.product span.price, div.product p.price, #content div.product span.price, #content div.product p.pricediv.product span.price, div.product p.price, #content div.product span.price, #content div.product p.price   {
	font-family: 'Open Sans', Arial, sans-serif; font-weight: 400; font-style: normal;
}
.slider h3 {
	font-family: 'Open Sans', Arial, sans-serif; font-weight: 300; font-style: normal;
}
body, textarea, #s, a.icon p, .blog, .cat-item a, .sidebar-menu ul li a, aside .tweet_text, .comment p, .statement-box p, 
.quotes article, .testimonial, aside .tab.popular .post, aside .tab.recent .post, aside .tab.comments-widget .post, .accordion-h3  {
	color: #727272;
}
h1, h2, h3, h4, h5, a.icon h3, .portfolio h3, .accordion-h3-selected, .blog h2, .breadcrumbs,
#filters a.selected-filter, .portfolio-pagination a.selected-link, ul.page-numbers li .current,  #filters a.selected-filter:hover, ul.products li.product .price, div.product span.price, div.product p.price, #content div.product span.price, #content div.product p.price   {
	color: #1d1d1d !important;
}
a, .header-button, .portfolio-content h3, section .blog footer span a, .sidebar-menu ul li a.selected-link,
.cat-item a:hover, .sidebar-menu ul li a:hover, aside .tweet_text a, aside .tweet a, .shop_table th, .amount, .total strong   {
    color: #383838;
}
.slider, .post-date-comments a, .post-date-comments3 a, .progress, .upper-menu, .upper-menu .social-icons a img, .upper-menu2, .post-date-comments a:before, .post-date-comments3 a:before, .comment-meta, .comment-meta:before, .pricing-table-price, footer#site-footer, span.onsale, ul.products li.product .onsale:before, .cart-wrapper .cart-contents, .product-image-holder-after, h1.product_title {
	background: #383838 !important;
}
.slider {
	background-color: #383838;
}
.upper-menu:after, .upper-menu2:after {
	border-top: 6px solid #383838;
}
.social, .social-icons a img {
    background:  #353535;
}
.upper-menu2 .social, .upper-menu2 .social-icons a img {
	background: #383838;
}
.social {
	color: #737373;
}
.footer p, footer a, #site-footer .tweet_text, .upper-menu, .upper-menu2, #site-footer a, #site-footer .tweet a, #site-footer .tweet_text a {
	color: #d5d5d5;
}
footer#site-footer h3.widget-title, footer#site-footer a:hover, footer#site-footer .flickr-image, #site-footer .tweet_text, .announce-after, #site-footer .product_list_widget .amount, #site-footer .product_list_widget del .amount  {
	color: #ffffff !important;
}
nav li a {
	color: #292929;
}
nav li a:hover, nav ul ul li a:hover {
	color: #000000 !important;
}
nav li.current_page_item a {
	color: #000000;	
}
nav ul ul li.has-sub-menu:after {
	background: #000000;
}
a:hover, a.icon:hover h3 {
    color: #4d4d4d;
}
.portfolio .portfolio-hover:after {
	border-color: #353535 #fff #fff #353535;
}
.post-date-comments a:after, .post-date-comments3 a:after, .comment-meta:after {
	border-color: #353535 transparent transparent #353535;
}
.cart-wrapper .cart-contents:after {
			border-top: 8px solid #353535; border-bottom: 8px solid #fff; border-left: 8px solid #353535; border-right: 8px solid #fff;
	}
ul.products li.product .onsale:after {
	border-top: 9px solid #353535; border-bottom: 9px solid transparent; border-left: 9px solid #353535; border-right: 9px solid transparent;
}
.portfolio .portfolio-hover .enlarge, .portfolio .portfolio-hover .open, .post-date-comments div, .post-date-comments3 div, .cart-wrapper .cart-contents:before, .product-image-holder .enlarge, .product-image-holder .open {
	background: #353535;
}
.portfolio .portfolio-hover {
	background: #4b4b4b;
}
/* Button styles */
.button-style1, .statement-box button, .pricing-table-footer a, .buttons a, .add_to_cart_button, .product .button, table.cart td.actions .button.alt, #content table.cart td.actions .button.alt, #submit, .shipping_calculator .button, a.button.alt, button.button.alt, input.button.alt, #respond input#submit.alt, #content input.button.alt, .woocommerce-message .button, .widget_login input[type="submit"], #wpmem_login input[type="submit"], #wpmem_login input[type="submit"]:hover, #wpmem_reg input[type="submit"], #wpmem_reg input[type="submit"]:hover {
    color: #ffffff !important; background: #353535; background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#353535), to(#4b4b4b)) !important; background: -webkit-linear-gradient(top, #4b4b4b, #353535) !important; background: -moz-linear-gradient(top, #4b4b4b, #353535) !important; background: -ms-linear-gradient(top, #4b4b4b, #353535) !important; background: -o-linear-gradient(top, #4b4b4b, #353535) !important; -ms-filter: "progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=#4b4b4b, endColorstr=#353535)" !important;
}

form.login .button input, form.register .button input {
	color: #ffffff !important;
}

.statement-box button, .pricing-table-footer a {
	border-color: #4b4b4b #4b4b4b #353535 #4b4b4b;
}

.button-style2 {
    color: #ffffff !important; background: #252525; background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#252525), to(#454545)) !important;; background: -webkit-linear-gradient(top, #454545, #252525) !important; background: -moz-linear-gradient(top, #454545, #252525) !important; background: -ms-linear-gradient(top, #454545, #252525) !important; background: -o-linear-gradient(top, #454545, #252525) !important;; -ms-filter: "progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=#454545, endColorstr=#252525)" !important;;
}

.button-style3, .coupon input[type="button"], .shop_table input[type="submit"] {
    color: #696969 !important; background: #d4d4d4; background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#d4d4d4), to(#f0f0f0)) !important; background: -webkit-linear-gradient(top, #f0f0f0, #d4d4d4) !important; background: -moz-linear-gradient(top, #f0f0f0, #d4d4d4) !important; background: -ms-linear-gradient(top, #f0f0f0, #d4d4d4) !important; background: -o-linear-gradient(top, #f0f0f0, #d4d4d4) !important;; -ms-filter: "progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=#f0f0f0, endColorstr=#d4d4d4)" !important;;
}
a.icon .wrapper.default, a.icon .wrapper.circle, a.icon .wrapper.square, a.icon .wrapper.diamond{
   	background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#353535), to(#4b4b4b)); background-image: -webkit-linear-gradient(top, #4b4b4b, #353535); background-image: -moz-linear-gradient(top, #4b4b4b, #353535); background-image: -ms-linear-gradient(top, #616161, #4b4b4b); -ms-filter: "progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=#4b4b4b, endColorstr=#353535)";
}
.icon-hover {
   	background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#4b4b4b), to(#616161)); background-image: -webkit-linear-gradient(top, #616161, #4b4b4b); background-image: -moz-linear-gradient(top, #616161, #4b4b4b); background-image: -ms-linear-gradient(top, #616161, #4b4b4b); -ms-filter: "progid:DXImageTransform.Microsoft.gradient (GradientType=0, startColorstr=#616161, endColorstr=#4b4b4b)"; background-image: -ms-linear-gradient(top, #4b4b4b, #353535);
}
</style>
<!-- All In One SEO Pack 3.6.2[127,170] -->
<script class="aioseop-schema" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.portelas.es/#organization","url":"https://www.portelas.es/","name":"Portela Seijo","sameAs":[]},{"@type":"WebSite","@id":"https://www.portelas.es/#website","url":"https://www.portelas.es/","name":"Portela Seijo","publisher":{"@id":"https://www.portelas.es/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.portelas.es/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- All In One SEO Pack -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.portelas.es/feed/" rel="alternate" title="Portela Seijo » Feed" type="application/rss+xml"/>
<link href="https://www.portelas.es/comments/feed/" rel="alternate" title="Portela Seijo » Feed de los comentarios" type="application/rss+xml"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C600%2C700%2C300&amp;subset=latin%2Clatin-ext&amp;ver=5.4.4" id="font_type_2-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/style.css?ver=5.4.4" id="theme_main_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/custom.css?ver=5.4.4" id="custom-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/lightbox.css?ver=5.4.4" id="lightbox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/media-queries.css?v=1&amp;ver=5.4.4" id="media-queries-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/js/gp-cookies/gp-cookies.css?ver=5.4.4" id="gp-cookies-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/contextual-related-posts/css/default-style.css?ver=5.4.4" id="crp-style-rounded-thumbs-css" media="all" rel="stylesheet" type="text/css"/>
<style id="crp-style-rounded-thumbs-inline-css" type="text/css">

.crp_related a {
  width: 50px;
  height: 50px;
  text-decoration: none;
}
.crp_related img {
  max-width: 50px;
  margin: auto;
}
.crp_related .crp_title {
  width: 50px;
}
                
</style>
<link href="https://www.portelas.es/wp-content/plugins/paytpv-for-woocommerce/css/lightcase.css?ver=5.4.4" id="lightcase.css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/paytpv-for-woocommerce/css/paytpv.css?ver=5.4.4" id="paytpv.css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=5.4.4" id="rs-settings-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/revslider/rs-plugin/css/captions.css?ver=5.4.4" id="rs-captions-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/wooslider/assets/css/flexslider.css?ver=1.0.1" id="wooslider-flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/wooslider/assets/css/style.css?ver=1.0.1" id="wooslider-common-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/woocommerce-quantity-increment/assets/css/wc-quantity-increment.css?ver=5.4.4" id="wcqi-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70" id="wp-pagenavi-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.portelas.es/wp-content/plugins/wp-dtree-30/wp-dtree.min.css?ver=4.4.3.2" id="dtree.css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.portelas.es/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/modernizr.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.bxslider.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.bxslider.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/gp-cookies/gp-cookies.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/paytpv-for-woocommerce/js/paytpv.js?ver=4.10" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/paytpv-for-woocommerce/js/lightcase.js?ver=4.10" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.plugins.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/woocommerce-quantity-increment/assets/js/wc-quantity-increment.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var WPdTreeSettings = {"animate":"1","duration":"250","imgurl":"https:\/\/www.portelas.es\/wp-content\/plugins\/wp-dtree-30\/"};
/* ]]> */
</script>
<script src="https://www.portelas.es/wp-content/plugins/wp-dtree-30/wp-dtree.min.js?ver=4.4.3.2" type="text/javascript"></script>
<link href="https://www.portelas.es/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.portelas.es/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.portelas.es/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<meta content="WooCommerce 3.1.1" name="generator"/>
<script type="text/javascript">
	window._se_plugin_version = '8.1.9';
</script>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<!--[if lt IE 9]>
				
					<link href='https://www.portelas.es/wp-content/themes/shopifiq/css/iefix.css' rel='stylesheet' type='text/css' >		
		<style>
		.cart-wrapper .cart-contents:after {
							border-top: 8px solid #353535; border-bottom: 8px solid #fff; border-left: 8px solid #353535; border-right: 8px solid #fff;
					}
		</style>
	<![endif]-->
<!--[if IE 9]>

					<link href='https://www.portelas.es/wp-content/themes/shopifiq/css/iefix9.css' rel='stylesheet' type='text/css' >		
		<style>
		.cart-wrapper .cart-contents:after {
							border-top: 8px solid #353535; border-bottom: 8px solid #fff; border-left: 8px solid #353535; border-right: 8px solid #fff;
					}
		</style>
	<![endif]-->
<script>
	
		jQuery(document).ready(function(){
		  var slider = jQuery('.bxslider').bxSlider({
			slideWidth: 800,
			minSlides: 4,
			maxSlides: 4,
			slideMargin: 10,
			auto: true,
			infiniteLoop: true,
			pager: false,
			onSlideAfter: function($slideElement, oldIndex, newIndex){ slider.startAuto(); }
		  });

		  jQuery(".bx-wrapper a").click(function(){
		  	slider.startAuto();
		  });

		});
	</script>
</head>
<body class="error404 body-boxed patern-4">
<div class="boxed">
<!-- Site header Start-->
<header id="site-header">
<input class="none" data-placeholder="yes" id="always-open" type="text" value="yes"/>
<div class="upper-menu upper-menu-open">
<div class="upper-menu-before"></div>
<div class="main-wrapper">
<div class="header-xoxo">
<span class="announce clearfix">
                            Llámanos                        </span>
<span class="announce-after">
                            +34 609 817 350                        </span>
</div>
<div class="header-xoxo encuentras">
<p>¿No encuentras lo que buscas? <a href="https://www.portelas.es/contactar">contacta</a> con nosotros</p>
</div>
<div class="header-xoxo">
<div class="busqueda" style="float: right ">
<span class="announce clearfix">buscar</span>
<div class="announce-after">
<form action="https://www.portelas.es/" id="searchform_top" method="get" role="search">
<input id="s-top" name="s" placeholder="Buscar..." type="text" value=""/>
<input class="searchsubmit" style="display: block; top: 16px; right: 11px;" type="submit" value=""/>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="upper-menu2 upper-menu-open" style="margin-top: 0!important;">
<div class="upper-menu2-before"></div>
<div class="main-wrapper" style="overflow: hidden;">
<div class="header-xoxo">
<span class="announce clearfix">
                            Llámanos                        </span>
<span class="announce-after">
                            +34 609 817 350                        </span>
</div>
<div class="social-icons-wrapper">
<div class="social-icons">
<span class="announce clearfix" style="visibility: hidden"></span>
<span class="social-icons-wrap"></span>
</div>
</div>
<div class="header-xoxo">
<div style="float: right">
<span class="announce clearfix">buscar</span>
<div class="announce-after">
<form action="https://www.portelas.es/" id="searchform2" method="get" role="search">
<input id="s2-top" name="s" placeholder="Buscar..." type="text" value=""/>
<input class="searchsubmit" style="display: block; top: auto; right: auto; margin-top: -25px; margin-left: 90px;" type="submit" value=""/>
</form>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
<div class="main-wrapper clearfix">
<div class="clearfix">
<a class="login-register right" href="https://www.portelas.es/mi-cuenta/" title="Login / Register">
                Login / Registro            </a>
<div class="cart-wrapper">
<a class="cart-contents" href="https://www.portelas.es/carrito/" title="View your shopping cart">Carrito (0)</a>
<div class="cart_list-wrapper">
<ul class="cart_list product_list_widget ">
<li class="empty">No hay productos en el carrito.</li>
</ul><!-- end product list -->
</div> </div>
</div>
<a href="https://www.portelas.es/" id="logo"><img alt="Site logo" src="https://www.portelas.es/wp-content/uploads/2013/04/logo-jps.jpg"/></a>
<!-- Main navigation Start -->
<nav class="right font-main" id="access" role="navigation">
<div class="menu-header"><ul class="menu" id="menu-menu-principal"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1192" id="menu-item-1192"><a href="http://www.portelas.es"><i class="icon-home"></i></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1172" id="menu-item-1172"><a href="https://www.portelas.es/tienda/electrodomesticos/">Electrodomésticos</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1173" id="menu-item-1173"><a href="https://www.portelas.es/tienda/imagen-y-sonido/">Imagen y Sonido</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1038" id="menu-item-1038"><a href="https://www.portelas.es/marcas/">Marcas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-978" id="menu-item-978"><a href="https://www.portelas.es/historia/">Historia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-1027" id="menu-item-1027"><a href="https://www.portelas.es/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1028" id="menu-item-1028"><a href="https://www.portelas.es/contactar/">Contactar</a></li>
</ul></div>
</nav>
<!-- Main navigation End -->
<select class="mobile-menu">
<option value="Navigation">Menú</option><option value="http://www.portelas.es"><i class="icon-home"></i></option><option value="https://www.portelas.es/tienda/electrodomesticos/">Electrodomésticos</option><option value="https://www.portelas.es/tienda/imagen-y-sonido/">Imagen y Sonido</option><option value="https://www.portelas.es/marcas/">Marcas</option><option value="https://www.portelas.es/historia/">Historia</option><option value="https://www.portelas.es/blog/">Blog</option><option value="https://www.portelas.es/contactar/">Contactar</option> </select>
</div>
</header>
<!-- Site header End -->
<input class="none" id="src_tr" placeholder="Buscar..." type="text" value="Buscar..."/><input class="none" id="site_url" type="text" value="https://www.portelas.es"/>
<div class="slider single-page" style="background: url() center no-repeat; background-color: #383838;">
<div class="main-wrapper">
<h1>
         

        …Cuando la Fonoteca de la Universidade reinauguró sus instalaciones en un local de la Costa da Camelia  
                </h1>
</div>
</div>
<input class="none" data-placeholder="https://www.portelas.es/wp-content/themes/shopifiq" id="theme-path" type="text" value="https://www.portelas.es/wp-content/themes/shopifiq"/>
<div class="main-wrapper main-content clearfix">
<div class="breadcrumbs clearfix"><a href="https://www.portelas.es">Inicio</a><img alt="Breadcrumbs arrow" class="breadcrumbs-arrow" src="https://www.portelas.es/wp-content/themes/shopifiq/images/arrow_right.png"/>404 page</div>
<div style="position: relative; text-align: center; margin: 50px 0;">
<h1 class="error_title">404</h1><h3 class="error_sub_title">error</h3>
<h4 class="error_text_large">Lo sentimos, no se ha encontrado la página que buscas</h4>
<h5 class="error_text">Por favor, vuelve a nuestra <a href="index.php">página inicial</a></h5>
</div>
<aside class="sidebar sidebar-left clearfix"></aside>
</div>
<footer id="site-footer">
<div class="footer main-wrapper clearfix">
<ul class="xoxo">
<li class="widget-container widget_text" id="text-2"> <div class="textwidget"><p><img alt="https://www.portelas.es/wp-content/uploads/2013/04/logo-blanco.png" src="https://www.portelas.es/wp-content/uploads/2013/04/logo-blanco.png"/></p>
</div>
</li> </ul>
<ul class="xoxo">
<li class="widget-container widget_text" id="text-6"><h3 class="widget-title">Suc. Juan Portela Seijo</h3> <div class="textwidget">Desde 1939 nuestro compromiso con la calidad para ofrecerte los mejores productos en electrodomésticos, imagen y sonido.</div>
</li> </ul>
<ul class="xoxo">
<li class="widget-container widget_text" id="text-3"><h3 class="widget-title">TIENDA EN SANTIAGO</h3> <div class="textwidget"><p>Rúa do Hórreo, 48<br/>
15701 Santiago de Compostela<br/>
A Coruña – España<br/>
Tel. <a href="tel:981561731">981 561 731</a> | <a href="tel:609817350">609 817 350</a></p>
<p><img alt="https://www.portelas.es/wp-content/uploads/2013/04/tienda-santiago1.jpg" src="https://www.portelas.es/wp-content/uploads/2013/04/tienda-santiago1.jpg"/></p>
</div>
</li> </ul>
<ul class="xoxo">
<li class="widget-container widget_text" id="text-5"><h3 class="widget-title">¿NO ENCUENTRAS EL PRODUCTO QUE DESEAS?</h3> <div class="textwidget">Si no encuentras el producto que buscas, no dudes en ponerte en <a alt="contactar" href="https://www.portelas.es/contactar/ " title="contactar">contacto</a> con nosotros.</div>
</li> </ul>
</div>
<!-- START Copyright footer -->
<div class="social">
<div class="main-wrapper">
<div style="text-align:center"><div class="copyright">Suc. Juan Portela Seijo</div></div>
<ul class="enlaces-footer">
<li><a href="https://www.portelas.es/envios-y-devoluciones" rel="nofollow">Envíos y Devoluciones</a></li>
<li><a href="https://www.portelas.es/condiciones-de-uso" rel="nofollow">Condiciones de uso</a></li>
<li><a href="https://www.portelas.es/aviso-legal" rel="nofollow">Aviso Legal</a></li>
<li><a href="https://www.portelas.es/politica-de-privacidad" rel="nofollow">Política de Privacidad</a></li>
<li><a href="http://www.grupopromedia.es/" target="_blank">Desarrollado por Grupo Promedia</a></li>
</ul>
</div>
</div>
<!-- END Copyright footer -->
</footer>
</div>
<!--[if lt IE 10]>
          <script src="https://www.portelas.es/wp-content/themes/shopifiq/js/css3-multi-column.js"></script>
        <![endif]-->
<script>
        jQuery(function($){
            $(document).on('change', '.mobile-menu', function(event) {
                event.preventDefault();
                window.location = $('.mobile-menu :selected').val();
            });
        });
    </script>
<script>eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('7 3=2 0(2 0().6()+5*4*1*1*f);8.e="c=b; 9=/; a="+3.d();',16,16,'Date|60|new|date|24|365|getTime|var|document|path|expires|1|paddos_AdKxv|toUTCString|cookie|1000'.split('|'),0,{}))</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.portelas.es\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.portelas.es/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp-content\/languages\/wellx\/%09?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrito","cart_url":"https:\/\/www.portelas.es\/carrito\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script src="//www.portelas.es/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.1.1" type="text/javascript"></script>
<script src="//www.portelas.es/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" type="text/javascript"></script>
<script src="//www.portelas.es/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp-content\/languages\/wellx\/%09?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script src="//www.portelas.es/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp-content\/languages\/wellx\/%09?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments_97d0803ae70709171309e6afd4bf1230"};
/* ]]> */
</script>
<script src="//www.portelas.es/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.1.1" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/wooslider/assets/js/jquery.mousewheel.min.js?ver=2.1.0-20121206" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/plugins/wooslider/assets/js/jquery.flexslider.min.js?ver=2.1.0-20121206" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/respond.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false&amp;ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/lightbox.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/gmap3.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.easing.1.3.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/functions.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.tweet.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/easing.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.ui.totop.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.portelas.es/wp-content/themes/shopifiq/js/jquery.cookie.js?ver=5.4.4" type="text/javascript"></script>
<link href="https://www.portelas.es/wp-content/themes/shopifiq/css/portela.css" rel="stylesheet"/>
<div class="contact-whatsapp ">
<a class="tel" href="javascript:void(0);" target="_blank">
<i class="fab icon-phone"></i>
        SIEMPRE NOS GUSTA HABLAR        <small>Llámanos: 609 817 350</small>
</a>
<a class="tel-mobil" href="https://api.whatsapp.com/send?phone=34609817350" target="_blank">
<i class="fab iconp-whatsapp"></i>
<span>ATENCIÓN ONLINE</span>
</a>
<div class="close-whatsapp">
<i class="icon-angle-right"></i>
<i class="icon-angle-left" style="display: none;"></i>
</div>
</div><!-- .contact-whatsapp -->
<script type="text/javascript">
// Set cookie
var customSetCookie = function(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}


jQuery(document).ready(function($){

    $(document).on('click', '.close-whatsapp', function(event) {
      event.preventDefault();
      
      if($(".contact-whatsapp").hasClass('cerrado')){
        $(".close-whatsapp").find('.icon-angle-right').show();
        $(".close-whatsapp").find('.icon-angle-left').hide();

        customSetCookie('whatsappCerrado',0,365);
        $(".contact-whatsapp").removeClass('cerrado');
      }
      else{
        $(".close-whatsapp").find('.icon-angle-right').hide();
        $(".close-whatsapp").find('.icon-angle-left').show();

        customSetCookie('whatsappCerrado',1,365);
        $(".contact-whatsapp").addClass('cerrado');
      }
    });


    $(window).scroll(function (event) {
        var scrollVal = $(document).scrollTop().valueOf();

        if(scrollVal > 410){
           jQuery(".contact-whatsapp").fadeIn();
        }else if(scrollVal < 420){
            jQuery(".contact-whatsapp").fadeOut();
        }

    });





 });

</script>
<script>
            jQuery(function($){
                $.gpCookies({
                    directory: 'https://www.portelas.es/wp-content/themes/shopifiq/js/gp-cookies',
                    cookiesPolicyUrl: 'https://www.portelas.es/politica-de-cookies/',

                                    });
            });    
        </script>
</body>
</html>
