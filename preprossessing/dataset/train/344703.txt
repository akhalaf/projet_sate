<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
<base href="https://columbovet.eu/"/>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Columbovet</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="index,follow" name="robots"/>
<meta content="index,follow" name="googlebot"/>
<meta content="BORTOS Production" name="author"/>
<link href="files/sites/4109/20200817_122837_115111478.png" rel="shortcut icon" type="image/png"/>
<link href="files/sites/4109/20200817_122837_115111478.png" rel="icon" type="image/png"/>
<!-- All CSS Here
	============================================ -->
<!-- Bootstrap CSS -->
<link href="template/columbovet/assets/css/bootstrap.min.css" rel="stylesheet"/> <link href="libs/css/bootstrapBT.css?v=01" rel="stylesheet"/>
<!-- Icon Font CSS -->
<link href="template/columbovet/assets/css/icons.min.css" rel="stylesheet"/>
<!-- Plugins CSS -->
<link href="template/columbovet/assets/css/plugins.css?v=12" rel="stylesheet"/>
<!-- Revolution Slider CSS -->
<link href="template/columbovet/assets/revolution/css/settings.css?v=12" rel="stylesheet"/>
<link href="template/columbovet/assets/revolution/css/navigation.css?v=12" rel="stylesheet"/>
<!-- Main Style CSS -->
<link href="template/columbovet/assets/css/style.css?v=20" rel="stylesheet"/>
<!-- Modernizer JS -->
<script src="template/columbovet/assets/js/vendor/modernizr-2.8.3.min.js"></script>
<link href="modules/products/css/products.css?v=100" rel="stylesheet" type="text/css"/>
<link href="libs/js/notifIt/notifIt.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="wrapper wrapper-2 wrapper-3">
<header class="header-area header-padding-1 section-padding-1 sticky-bar">
<div class="container">
<div class="row align-items-center">
<div class="col-xl-3 col-lg-2 col-md-6 col-4">
<div class="logo">
<a href="/">
<img alt="Columbovet - Logo" class="img-fluid" src="files/sites/8973/20200817_122819_80844349.png"/>
</a>
</div>
</div>
<div class="col-xl-8 col-lg-8 d-none d-lg-block">
<div class="main-menu main-menu-center">
<nav>
<ul>
<li>
<a href="1-o-nas" target="_self">O nas
                                    </a>
</li>
<li>
<a href="6-koszt-wysylki" target="_self">Koszt wysyłki
                                    </a>
</li>
<li>
<a href="7-jak-zamawiac" target="_self">Jak zamawiać
                                    </a>
</li>
<li>
<a href="" target="_self">Oferta
                                    </a>
<div class="dropdown-menu-style dropdown-width-2">
<ul>
<li>
<a href="drob" target="_self">Drób</a>
</li>
<li>
<a href="golebie" target="_self">Gołębie</a>
</li>
<li>
<a href="trzoda-chlewna" target="_self">Trzoda chlewna</a>
</li>
<li>
<a href="bydlo" target="_self">Bydło</a>
</li>
<li>
<a href="zwierzeta-towarzyszace" target="_self">Zwierzęta towarzyszące</a>
</li>
<li>
<a href="dezynfekcja" target="_self">Dezynfekcja</a>
</li>
</ul>
</div>
</li>
<li>
<a href="kontakt" target="_self">Kontakt
                                    </a>
</li>
</ul>
</nav>
</div>
</div>
<div class="col-xl-1 col-lg-1 col-md-6 col-8">
<div class="header-right-wrap">
<div class="same-style account-satting">
<a class="account-satting-active" href="uzytkownik/logowanie"><i class="negan-icon-users-circle-2"></i></a>
</div>
<div class="same-style cart-wrap">
<a class="cart-active" href="#">
<i class="negan-icon-bag"></i>
<span class="count-style" id="cartProductCont">0</span>
</a>
</div>
<div class="same-style header-off-canvas">
<a class="header-aside-button" href="#"><i class="negan-icon-menu-left"></i></a>
</div>
</div>
</div>
</div>
</div>
</header>
<div class="header-small-mobile sticky-bar">
<div class="container-fluid">
<div class="row align-items-center">
<div class="col-6">
<div class="logo">
<a href="/">
<img alt="Columbovet - Logo" src="files/sites/8973/20200817_122819_80844349.png"/>
</a>
</div>
</div>
<div class="col-6">
<div class="header-right-wrap">
<div class="same-style cart-wrap">
<a class="cart-active" href="#">
<i class="negan-icon-bag"></i>
</a>
</div>
<div class="same-style mobile-off-canvas">
<a class="mobile-aside-button" href="#"><i class="negan-icon-menu-left"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="mobile-off-canvas-active">
<a class="mobile-aside-close"><i class="negan-icon-simple-close"></i></a>
<div class="header-mobile-aside-wrap">
<div class="mobile-search">
<form action="#" class="search-form">
<input placeholder="Search entire store…" type="text"/>
<button class="button-search"><i class="negan-icon-zoom2"></i></button>
</form>
</div>
<div class="mobile-menu-wrap">
<div class="slinky-mobile-menu text-left" id="mobile-menu">
<ul>
<li>
<a href="1-o-nas" target="_self">O nas
                            </a>
</li>
<li>
<a href="6-koszt-wysylki" target="_self">Koszt wysyłki
                            </a>
</li>
<li>
<a href="7-jak-zamawiac" target="_self">Jak zamawiać
                            </a>
</li>
<li>
<a href="" target="_self">Oferta
                            </a>
<ul>
<li>
<a href="drob" target="_self">Drób</a>
</li>
<li>
<a href="golebie" target="_self">Gołębie</a>
</li>
<li>
<a href="trzoda-chlewna" target="_self">Trzoda chlewna</a>
</li>
<li>
<a href="bydlo" target="_self">Bydło</a>
</li>
<li>
<a href="zwierzeta-towarzyszace" target="_self">Zwierzęta towarzyszące</a>
</li>
<li>
<a href="dezynfekcja" target="_self">Dezynfekcja</a>
</li>
</ul>
</li>
<li>
<a href="kontakt" target="_self">Kontakt
                            </a>
</li>
</ul>
</div>
</div>
<div class="mobile-curr-lang-wrap">
</div>
<div class="mobile-social-wrap">
<a href="mailto:" target="_blank" title="Email"><i class="fa fa-envelope-o"></i></a>
<a href="#" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
<a href="#" target="_blank" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
<a href="#" target="_blank" title="Pinterest"><i class="fa fa-pinterest"></i></a>
<a href="#" target="_blank" title="Reddit"><i class="fa fa-reddit"></i></a>
<a href="#" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
<a href="#" target="_blank" title="WhatsApp"><i class="fa fa-whatsapp"></i></a>
</div>
</div>
</div>
<!-- aside start -->
<div class="header-aside-active">
<div class="header-aside-wrap">
<a class="aside-close"><i class="negan-icon-simple-close"></i></a>
<div class="header-aside-top">
<div class="aside-logo">
<a href="/">
<img alt="" src="template/columbovet/assets/img/logo/logo.png"/>
</a>
</div>
<p>Columbovet działa na krajowym rynku od 1995 roku jako specjalistyczna firma zajmującą się zdrowiem
                   drobiu, gołębi i ptaków ozdobnych. Od 2002 rok. poszerzyła swoja ofertę o preparaty dla psów, kotów i
                   zwierząt towarzyszących. Firma jako pierwsza w Polsce zaczęła zajmować się chorobami i profilaktyką
                   hodowli gołębi.</p>
<div class="aside-social">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-pinterest-p"></i></a>
</div>
</div>
<div class="header-aside-middle">
</div>
<div class="header-aside-bottom">
<ul>
<li class="phn">
<span><a href="mailto:782 840 008">782 840 008</a></span>
</li>
<li class="email">
<span><a href="mailto:biuro@columbovet.eu">biuro@columbovet.eu</a></span>
</li>
<li class="address">
<span>Nizinna 12 / U2, Warszawa 04-362</span>
</li>
</ul>
</div>
<div class="header-aside-copyright">
<p>©  - 2021                    <a href="https://columbovet.eu/">COLUMBOVET sp. z o.o..</a>
                   All Rights Reserved
                </p>
</div>
</div>
</div>
<!-- mini cart start -->
<div class="sidebar-cart-active">
<div class="sidebar-cart-all" id="litle_cart">
<a class="cart-close" href="#"><i class="negan-icon-simple-close"></i></a>
<div class="cart-content">
<h3>Koszyk</h3>
<ul>
</ul>
<div class="cart-total">
<h4>W sumie:
					<span>0.00 zł</span>
</h4>
</div>
<div class="cart-checkout-btn">
<a class="btn-hover cart-btn-style" href="koszyk">Zobacz koszyk
				</a>
</div>
</div>
</div>
</div>
<!--error section area start-->
<div class="error_section" style="padding:80px 0px">
<div class="container">
<div class="row">
<div class="col-12">
<div class="error_form text-center">
<h1>404</h1>
<h2>Nie znaleziono podanej strony</h2>
<a href="/">Strona główna</a>
</div>
</div>
</div>
</div>
</div>
<!--error section area end-->
<div class="feature-area pb-25 bg-gray-4 pt-65">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-wrap mb-30 text-center">
<i class="nc-icon-glyph shopping_delivery bg-white-icon"></i>
<h5 class="magin-incress">Darmowa dostawa</h5>
<p>Przy zamówieniach powyżej 500 zł</p>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-wrap mb-30 text-center">
<i class="nc-icon-glyph tech-2_l-security bg-white-icon"></i>
<h5 class="magin-incress">Ochrona kupujących</h5>
<p>Zapewniamy bezpieczeństwo zakupów</p>
</div>
</div>
</div>
</div>
</div>
<footer class="footer-area border-top-1 pt-60">
<div class="footer-top section-padding-1">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-4">
<div class="footer-widget footer-about mb-30">
<img alt="" class="img-fluid" src="template/columbovet/assets/img/logo/logo.png" style="    max-width: 250px;"/>
<p>Columbovet działa na krajowym rynku od 1995 roku jako specjalistyczna firma zajmującą się
                               zdrowiem drobiu, gołębi i ptaków ozdobnych. Od 2002 rok. poszerzyła swoja ofertę o
                               preparaty dla psów, kotów i zwierząt towarzyszących. Firma jako pierwsza w Polsce zaczęła
                               zajmować się chorobami i profilaktyką hodowli gołębi.</p>
<div class="footer-social">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-pinterest-p"></i></a>
</div>
</div>
</div>
<div class="footer-custom-col">
<div class="footer-widget mb-30">
<div class="footer-title">
<h3>Informacje</h3>
</div>
<div class="footer-list">
<ul>
<li>
<a href="1-o-nas" target="_self">O Nas</a>
</li>
<li>
<a href="drob" target="_self">Drób</a>
</li>
<li>
<a href="golebie" target="_self">Gołębie</a>
</li>
<li>
<a href="trzoda-chlewna" target="_self">Trzoda Chlewna</a>
</li>
<li>
<a href="bydlo" target="_self">Bydło</a>
</li>
<li>
<a href="zwierzeta-towarzyszace" target="_self">Zwierzęta Towarzyszące</a>
</li>
<li>
<a href="dezynfekcja" target="_self">Dezynfekcja</a>
</li>
</ul>
</div>
</div>
</div>
<div class="footer-custom-col">
<div class="footer-widget mb-30">
<div class="footer-title">
<h3>Regulaminy</h3>
</div>
<div class="footer-list">
<ul>
<li>
<a href="files/files/1009/20200923_161307_494644826.pdf" target="_self">Regulamin sklepu</a>
</li>
<li>
<a href="files/files/2047/20200923_161349_796317577.pdf" target="_self">Regulamin Konta</a>
</li>
<li>
<a href="files/files/5552/20200923_161339_324248479.pdf" target="_self">Regulamin Newslleter</a>
</li>
<li>
<a href="files/files/6805/20200923_161318_275992761.pdf" target="_self">Polityka Prywatności</a>
</li>
</ul>
</div>
</div>
</div>
<div class="footer-custom-col">
<div class="footer-widget mb-30">
<div class="footer-title">
<h3>Twoje Konto</h3>
</div>
<div class="footer-list">
<ul>
<li>
<a href="https://columbovet.eu/uzytkownik/logowanie" target="_self">Logowanie</a>
</li>
<li>
<a href="https://columbovet.eu/uzytkownik/rejestracja" target="_self">Rejestracja</a>
</li>
</ul>
</div>
</div>
</div>
<div class="col-lg-3 col-md-8">
<div class="footer-widget footer-about mb-30">
<div class="footer-title">
<h3>Newsletter</h3>
</div>
<div class="subscribe-style">
<p>Jeżeli chcesz otrzymawać informację o promocjach, nowościach zostaw nam swój adress
                                   email.</p>
<div class="subscribe-form mt-20" id="mc_embed_signup">
<form class="validate subscribe-form-style" id="mc-embedded-subscribe-form" novalidate="">
<div class="mc-form" id="mc_embed_signup_scroll">
<input class="email" name="EMAIL" placeholder="Podaj adres email" required="" type="email" value=""/>
<div aria-hidden="true" class="mc-news">
<input name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" type="text" value=""/>
</div>
<div class="clear">
<input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subskrybuj"/>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom section-padding-1 pb-15 pt-20">
<div class="container-fluid">
<div class="copyright text-center">
<p>© 2020 - 2021                        <a href="https://columbovet.eu/">COLUMBOVET sp. z o.o.. </a>
                       All Rights Reserved

                    </p>
<p class="pt-25">
<a href="https://bortos.pl" rel="nofollow" target="_blank">
<img alt="BORTOS Production" src="https://static.bortos.pl/images/bortos/logotypes/bortos_powered.png" style="width: 81px;"/>
</a>
</p>
</div>
</div>
</div>
</footer>
</div>
<!-- All JS is here
============================================ -->
<!-- jQuery JS -->
<script src="template/columbovet/assets/js/vendor/jquery-1.12.4.min.js"></script>
<!-- Popper JS -->
<script src="template/columbovet/assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="template/columbovet/assets/js/bootstrap.min.js"></script>
<!-- Revolution Slider JS -->
<script src="template/columbovet/assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="template/columbovet/assets/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="template/columbovet/assets/revolution/revolution-active.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="template/columbovet/assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<!-- Plugins JS -->
<script src="template/columbovet/assets/js/plugins.js"></script>
<!-- Ajax Mail -->
<!-- Main JS -->
<script src="template/columbovet/assets/js/main.js?v=15"></script>
<script src="libs/js/notifIt/notifIt.js"></script>
<script src="modules/header/js/header.js"></script>
<script src="modules/carts/js/carts.js?v=145"></script>
<script src="modules/wish_lists/js/wish_lists.js"></script>
<script src="modules/products/js/products_search.js?v=100"></script>
</body>
</html>