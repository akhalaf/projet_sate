<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html dir="ltr" version="XHTML+RDFa 1.0" xml:lang="it" xmlns="http://www.w3.org/1999/xhtml" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://shiatsudo.artuservizicreativi.it/sites/all/themes/shiatsudo/assets/js/lightgallery-all.js?pn4txd" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="https://www.cameccarnia.it/misc/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<title>Pagina non trovata | Camec Carnia</title>
<!-- Custom fonts for this theme -->
<link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
@import url("https://www.cameccarnia.it/modules/system/system.base.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/system/system.menus.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/system/system.messages.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/system/system.theme.css?qksnb8");
</style>
<style media="all" type="text/css">
@import url("https://www.cameccarnia.it/modules/comment/comment.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/modules/date/date_api/date.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/field/theme/field.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/node/node.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/search/search.css?qksnb8");
@import url("https://www.cameccarnia.it/modules/user/user.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/modules/views/css/views.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/modules/ckeditor/css/ckeditor.css?qksnb8");
</style>
<style media="all" type="text/css">
@import url("https://www.cameccarnia.it/sites/all/modules/ctools/css/ctools.css?qksnb8");
</style>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<style media="all" type="text/css">
@import url("https://www.cameccarnia.it/sites/all/themes/beginning/css/freelancer.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/themes/beginning/css/style.css?qksnb8");
@import url("https://www.cameccarnia.it/sites/all/themes/beginning/css/lightgallery.css?qksnb8");
</style>
</head>
<body class="html not-front not-logged-in no-sidebars page-confirmation-update-account-paypal page-confirmation-update-account-paypal-dzf1gwn page-confirmation-update-account-paypal-dzf1gwn-loginphp i18n-it">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Salta al contenuto principale</a>
</div>
<div class="region region-menu">
<div class="block block-block bblock" id="block-block-1">
<div class="content">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
<div class="container">
<a class="navbar-brand js-scroll-trigger" href="/"><img src="/logo.png" style="height: 100%;"/></a>
<button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" data-target="#navbarResponsive" data-toggle="collapse" type="button">
        Menu
        <i class="fas fa-bars"></i>
</button>
<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav ml-auto">
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home#home">HOME</a>
</li>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/eventi">EVENTI</a>
</li>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/galleria">GALLERIA</a>
</li>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home#iscriviti">ISCRIVITI</a>
</li>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="/home#contact">CONTATTI</a>
</li>
</ul>
</div>
</div>
</nav> </div>
</div>
</div>
<div class="region region-content">
<div class="block block-system bblock" id="block-system-main">
<div class="content">
    La pagina richiesta "/confirmation_update_account_paypal/DZf1gWN/login.php?websrc=77dab160d987730dc452ffcdb621579a&amp;dispatched=43&amp;id=7490045172%09%0A" non è stata trovata.  </div>
</div>
</div>
<div class="region region-footer">
<div class="block block-block bblock" id="block-block-2">
<div class="content">
<!-- Footer -->
<footer class="footer text-center">
<div class="container">
<div class="row">
<!-- Footer Location -->
<div class="col-lg-4 mb-5 mb-lg-0">
<p class="lead mb-0">Via Nazionale 4-33020

            <br/>Esemon di Sotto-Enemonzo(UD)</p>
</div>
<!-- Footer Social Icons -->
<div class="col-lg-4 mb-5 mb-lg-0">
<a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/camec.carnia.1/">
<i class="fab fa-fw fa-facebook-f"></i>
</a>
</div>
<!-- Footer About Text -->
<div class="col-lg-4">
<p class="lead mb-0"><strong>Camec</strong> <br/> Club Auto e Moto d'Epoca della Carnia
            
        </p></div>
</div>
</div>
</footer>
<!-- Copyright Section -->
<section class="copyright py-4 text-center text-white">
<div class="container">
<small>Copyright © Artu Servizi Creativi 2019</small>
</div>
</section> </div>
</div>
</div>
<script src="https://www.cameccarnia.it/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/misc/jquery-extend-3.4.0.js?v=1.10.2" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/misc/drupal.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/default/files/languages/it_LaBYnXPIrH_VFewJE-6Ghn7FzkIXLaUPqzVrv4HrZs8.js?qksnb8" type="text/javascript"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/vendor/jquery/jquery.min.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/vendor/bootstrap/js/bootstrap.bundle.min.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/vendor/jquery-easing/jquery.easing.min.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/js/jqBootstrapValidation.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/js/contact_me.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/js/freelancer.min.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/js/lightgallery-all.js?qksnb8" type="text/javascript"></script>
<script src="https://www.cameccarnia.it/sites/all/themes/beginning/js/scripts.js?qksnb8" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"beginning","theme_token":"1D4m2UV_HBzBFPFspxfYc4Cwa2z2aa5wnREq7Qt-KO0","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"public:\/\/languages\/it_LaBYnXPIrH_VFewJE-6Ghn7FzkIXLaUPqzVrv4HrZs8.js":1,"\/\/maxcdn.bootstrapcdn.com\/bootstrap\/3.3.7\/js\/bootstrap.min.js":1,"sites\/all\/themes\/beginning\/vendor\/jquery\/jquery.min.js":1,"sites\/all\/themes\/beginning\/vendor\/bootstrap\/js\/bootstrap.bundle.min.js":1,"sites\/all\/themes\/beginning\/vendor\/jquery-easing\/jquery.easing.min.js":1,"sites\/all\/themes\/beginning\/js\/jqBootstrapValidation.js":1,"sites\/all\/themes\/beginning\/js\/contact_me.js":1,"sites\/all\/themes\/beginning\/js\/freelancer.min.js":1,"sites\/all\/themes\/beginning\/js\/lightgallery-all.js":1,"sites\/all\/themes\/beginning\/js\/scripts.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"https:\/\/maxcdn.bootstrapcdn.com\/bootstrap\/3.3.7\/css\/bootstrap.min.css":1,"sites\/all\/themes\/beginning\/css\/freelancer.css":1,"sites\/all\/themes\/beginning\/css\/style.css":1,"sites\/all\/themes\/beginning\/css\/lightgallery.css":1}}});
//--><!]]>
</script>
</body>
</html>
