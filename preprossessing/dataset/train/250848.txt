<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="img-landing/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<title>Booster Club Foods</title>
<!-- css -->
<!--  google fonts  -->
<link href="http://fonts.googleapis.com/css?family=Oswald:400,300" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Merienda+One" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css"/>
<link href="css-landing/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="css-landing/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="css-landing/animate.css" rel="stylesheet" type="text/css"/>
<link href="css-landing/custom.css" rel="stylesheet" type="text/css"/>
</head>
<body onload="countdown(year,month,day,hour,minute)">
<div class="body-wrapper">
<!--   begin of header section   -->
<section class="header" id="overlay-1">
<div class="header-wrapper">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center">
<div class="theme-name">
<a class="title" href="#">BOOSTER CLUB FOODS</a>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12 text-center">
<div class="header-text">
<p class="service-text">Our Amazing Website is</p>
<p class="coming">coming soon</p>
</div>
</div>
</div>
</div> <!--  end of .container  -->
</div><!-- end of .header-wrapper  -->
</section>
<!--   end of header section     -->
<!--   begin of footer  section  -->
<section class="footer">
<div class="footer-wrapper">
<div class="container">
<div class="row">
<div class="col-md-6">
<p class="copy-right text-center">© 2017 Booster Club Foods. All rights reserved.</p>
</div>
<div class="col-md-6">
</div>
</div>
</div>
</div><!--  end of .footer-wrapper  -->
</section>
<!--   end of footer section  -->
</div>
<!--  js files -->
<script src="js-landing/jquery.js" type="text/javascript"></script>
<script src="js-landings/bootstrap.min.js" type="text/javascript"></script>
<script src="js-landing/timer.js" type="text/javascript"></script>
<script src="js-landings/script.js" type="text/javascript"></script>
</body>
</html>