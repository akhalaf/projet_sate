<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<meta content="linknowmedia.cc" name="lnm.location"/>
<title>Vancity Plumbing &amp; Heating | Plumber</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://vancityplumbing.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/scripts/html5.js" type="text/javascript"></script>
<![endif]-->
<link async="async" href="https://fonts.googleapis.com/css?family=Raleway:400,300,100" rel="stylesheet" type="text/css"/>
<link async="async" href="https://fonts.googleapis.com/css?family=Oxygen:400,300" rel="stylesheet" type="text/css"/><meta content="Vancity Plumbing &amp; Heating specializes in Plumber, Plumbing Company and Plumbing Services and services Vancouver, Maple Ridge and Burnaby. Call (604) 671-3917" name="description"/><meta content="Vancouver Plumber, Vancouver Plumbing Company, Vancouver Plumbing Services, Maple Ridge Plumber, Maple Ridge Plumbing Company, Maple Ridge Plumbing Services, Burnaby Plumber, Burnaby Plumbing Company, Burnaby Plumbing Services" name="keywords"/><meta content="rplu6" name="lnm.themename"/><link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//cdn.polyfill.io" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://vancityplumbing.com/home/feed/" rel="alternate" title="Vancity Plumbing &amp; Heating » Home Comments Feed" type="application/rss+xml"/>
<link href="https://vancityplumbing.com/wp-content/themes/rplu6/normalize.min.css" id="normalize-css" media="all" rel="stylesheet"/>
<link href="https://vancityplumbing.com/wp-content/themes/rplu6/foundation.css" id="foundation-css" media="all" rel="stylesheet"/>
<link href="https://vancityplumbing.com/wp-content/themes/rplu6/style.css" id="style-css" media="all" rel="stylesheet"/>
<link href="https://vancityplumbing.com/wp-content/themes/rplu6/page-css.php" id="pagestylephp-css" media="all" rel="stylesheet"/>
<link href="https://vancityplumbing.com/wp-content/themes/rplu6/font-awesome-4.6.3/css/font-awesome.min.css" id="fontawesome-css" media="all" rel="stylesheet"/>
<link href="https://vancityplumbing.com/wp-content/plugins/site-reviews/assets/css/site-reviews.css" id="geminilabs-site-reviews-css" media="all" rel="stylesheet"/>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/scripts/jquery.min.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/js/foundation/foundation.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/js/foundation/foundation.topbar.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/scripts/vendor/modernizr.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/scripts/lnm-scripts.min.js"></script>
<script src="https://cdn.polyfill.io/v2/polyfill.js?features=CustomEvent%2CElement.prototype.closest%2CElement.prototype.dataset%2CEvent&amp;flags=gated&amp;ver=4.9.16"></script>
<link href="https://vancityplumbing.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<link href="https://vancityplumbing.com/home/" rel="canonical"/>
<link href="https://vancityplumbing.com/?p=5" rel="shortlink"/>
<script src="https://vancityplumbing.com/?dm=b83bb039c19a6e020b409d0b962db862&amp;action=load&amp;blogid=2002&amp;siteid=1&amp;t=415875887&amp;back=https%3A%2F%2Fvancityplumbing.com%2F"></script><!-- pulled from Schema Post Type --><script type="application/ld+json">{"@context":"http://schema.org/","@type":"LocalBusiness","name":"Vancity Plumbing & Heating","description":"Plumber, Plumbing Company and Plumbing Services","image":"https://vancityplumbing.com/wp-content/themes/rplu6/images/rich-card.png","url":"https://vancityplumbing.com","logo":"","faxNumber":"","email":"dvero@vancityplumbing.com","areaServed":"Vancouver, Maple Ridge and Burnaby","paymentAccepted":"","address":{"@type":"PostalAddress","streetAddress":"","addressLocality":"Maple Ridge","addressRegion":"BC","postalCode":"V2W 0B9","postOfficeBoxNumber":""},"sameAs":["https://www.instagram.com/vancityplumbing/"],"contactPoint":[{"@type":"ContactPoint","telephone":"+1-604-671-3917","contactType":"customer service"}],"openingHoursSpecification":[{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Monday","opens":"08:00","closes":"17:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Tuesday","opens":"08:00","closes":"17:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Wednesday","opens":"08:00","closes":"17:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Thursday","opens":"08:00","closes":"17:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Friday","opens":"08:00","closes":"17:00"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Saturday","opens":"closed","closes":"closed"},{"@type":"OpeningHoursSpecification","dayOfWeek":"http://schema.org/Sunday","opens":"closed","closes":"closed"}]}</script> <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-173662995-9', 'auto');
				ga('send', 'pageview');

			</script>
<meta content="Vancouver, Maple Ridge and Burnaby" id="townorder" name="townorder"/>
<meta content="Plumber, Plumbing Company and Plumbing Services" id="marketspecialties" name="marketspecialties"/>
<meta content="Maple Ridge" id="city" name="city"/>
</head>
<body class="page-template-default page page-id-5">
<header class="header-wrapper">
<div class="navigation-wrapper">
<div class="row">
<nav class="top-bar fading" data-options=" custom_back_text: true; back_text: Back;" data-topbar="" role="navigation">
<ul class="title-area">
<li class="name"></li>
<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
</ul>
<div class="top-bar-section section">
<div class="menu"><ul>
<li class="page_item page-item-5 current_page_item active"><a href="https://vancityplumbing.com/">Home</a></li>
<li class="page_item page-item-6 page_item_has_children has-dropdown not-click"><a href="https://vancityplumbing.com/about-us/">About</a>
<ul class="dropdown">
<li class="page_item page-item-58"><a href="https://vancityplumbing.com/about-us/service-areas/">Service Areas</a></li>
</ul>
</li>
<li class="page_item page-item-7 page_item_has_children has-dropdown not-click"><a href="https://vancityplumbing.com/services/">Services</a>
<ul class="dropdown">
<li class="page_item page-item-8"><a href="https://vancityplumbing.com/services/clogged-drain/">Clogged Drain</a></li>
<li class="page_item page-item-9"><a href="https://vancityplumbing.com/services/drain-camera-inspection/">Drain Camera Inspection</a></li>
<li class="page_item page-item-10"><a href="https://vancityplumbing.com/services/drain-cleaning/">Drain Cleaning</a></li>
<li class="page_item page-item-11"><a href="https://vancityplumbing.com/services/garbage-disposal-repair/">Garbage Disposal Repair</a></li>
<li class="page_item page-item-12"><a href="https://vancityplumbing.com/services/plumbing/">Plumbing</a></li>
<li class="page_item page-item-13"><a href="https://vancityplumbing.com/services/plumbing-contractors/">Plumbing Contractors</a></li>
<li class="page_item page-item-14"><a href="https://vancityplumbing.com/services/sewer-service/">Sewer Service</a></li>
<li class="page_item page-item-15"><a href="https://vancityplumbing.com/services/sump-pump/">Sump Pump</a></li>
<li class="page_item page-item-16"><a href="https://vancityplumbing.com/services/toilet-repair/">Toilet Repair</a></li>
<li class="page_item page-item-17"><a href="https://vancityplumbing.com/services/water-heaters/">Water Heaters</a></li>
</ul>
</li>
<li class="page_item page-item-18"><a href="https://vancityplumbing.com/emergency-plumbing/">Emergency Plumbing</a></li>
<li class="page_item page-item-19"><a href="https://vancityplumbing.com/faq/">FAQ</a></li>
<li class="page_item page-item-59"><a href="https://vancityplumbing.com/blog/">Blog</a></li>
<li class="page_item page-item-63"><a href="https://vancityplumbing.com/testimonials/">Testimonials</a></li>
<li class="page_item page-item-20"><a href="https://vancityplumbing.com/contact-us/">Contact</a></li>
</ul></div>
</div></nav>
</div>
</div>
<div class="row company-header">
<div class="large-4 columns small-only-text-center right text-right">
<p class="contact-phone phone">Give us a Call <a href="tel:(604) 671-3917">(604) 671-3917</a></p>
<h1 class="header-company-serviceorder">Plumbing Contractor in Vancouver, Maple Ridge and Burnaby</h1>
</div>
<div class="large-8 columns left">
<div class="large-1 columns logo-wrapper strip-padding">
<i class="fa fa-wrench"></i>
</div>
<div class="large-11 columns header-company-title">
<a href="/">Vancity Plumbing &amp; Heating</a>
<h2 class="header-company-locationorder">Located in Maple Ridge, serving Maple Ridge and surrounding areas</h2>
</div>
</div>
</div>
</header><div class="clearfix"></div><div class="slider-wrapper">
<div class="slider-inner-wrapper">
<style scoped="">#slider-0, #slider-0 li{width: 1440px;height: 800px; display}</style>
<ul id="slider-0">
<li>
<img alt="" src="https://vancityplumbing.com/wp-content/themes/rplu6/images/sliders/02.jpg"/>
</li>
<li>
<img alt="" src="https://vancityplumbing.com/wp-content/themes/rplu6/images/sliders/04.jpg"/>
</li>
<li>
<img alt="" src="https://vancityplumbing.com/wp-content/themes/rplu6/images/sliders/03.jpg"/>
</li></ul>
<script type="text/javascript">
			$(document).ready(function() {
				$('#slider-0').rhinoslider({
					animateActive: 		 true,
					easing: 			'swing',
					effect: 			'slide',
					effectTime: 		'1500',
					partDelay: 			'100',
					parts: 				'5,3',
					shiftValue: 		'150',
					showTime: 			'5000',
					slideNextDirection: 'toLeft',
					slidePrevDirection: 'toRight',
					changeBullets: 		'after',
					controlFadeTime: 	'650',
					controlsKeyboard: 	 true,
					controlsMousewheel:  false,
					controlsPlayPause: 	 true,
					controlsPrevNext: 	 true,
					nextText: 			'next',
					pauseText: 			'pause',
					playText: 			'play',
					prevText: 			'prev',
					showBullets: 		'never',
					showControls: 		'never',
					autoPlay: 			 true,
					cycled: 			 true,
					pauseOnHover:		 true,
					randomOrder: 		 false,
					captionsFadeTime:	'650',
					captionsOpacity:	'0.7',
					showCaptions: 		'never',
					styles: 			''
				});
			});
		</script>
</div>
<div class="slider-overlay">
<div class="row wide">
<div class="slider-box text-center">
<h2><span class="bolded">Plumbing Services in Vancouver, Maple Ridge and Burnaby</span></h2>
<p>Our courteous staff has offered outstanding plumbing services in the greater Maple Ridge area for many years by focusing on our top priority of exceeding expectations and extending affordable, high-quality, and professional services. </p><p>
</p><p class="text-center"><a class="button button-one" href="/about-us/">Read More</a></p>
</div>
</div>
</div>
</div>
<main>
<article class="large-12 columns strip-padding">
<div class="content-wrapper midbar-wrapper vertical-padding">
<div class="row text-center">
<div class="large-12 columns midbar horizontal-padding">
<i class="fa fa-info"></i>
<h2>Commercial and Residential Plumbing</h2>
<p>Clogged drains, system backups, no one likes dealing with these situations, and at the time they can seem disastrous! You can be without hot water, or any water at all, leaving you unable to prepare properly for the day ahead or really be comfortable in your own home or business.</p>
<p>Vancity Plumbing &amp; Heating was founded on an urge to help local homeowners. Our fully licensed, insured, and bonded team of experienced plumbing professionals can be on the job any day of the week, any time.</p>
<p>At Vancity Plumbing &amp; Heating we understand the importance of emergency plumbing as well as scheduled plumbing. Because, your system is never going to give you a heads up before something goes wrong!</p>
</div>
</div>
</div>
<div class="content-wrapper">
<div class="context" id="context1">
<script type="text/javascript">
			jQuery(function(){
				jQuery("#context1").css("background","url(https://vancityplumbing.com/wp-content/themes/rplu6/images/general-parallax-bg.jpg) 50% 0 no-repeat fixed"); 
				jQuery("#context1").parallax("50%", 0.3);
			});
		</script>
<div class="texture-overlay"> </div>
<div class="row second-part">
<div class="large-12 columns">
<div class="large-8 columns large-centered text-center">
<h1>Plumbing Professionals at Your Service</h1>
<p>Our team puts customer satisfaction first. From dressing professionally and arriving to every job with a smile on our face, to providing you with the highest quality of plumbing work whether we’re on a scheduled or emergency visit. Vancity Plumbing &amp; Heating hasn’t gotten to where it is today by slacking and slouching!</p>
<p>Every member of our team is licensed, insured, and bonded for your benefit as well as our own. Our team is made up of true professionals with years of experience in the industry, hired because they can do the work you need, in the time you need it done.</p>
</div>
</div>
</div>
</div>
</div>
<div class="content-wrapper midbar-wrapper vertical-padding">
<div class="row text-center">
<div class="large-6 columns midbar horizontal-padding">
<i class="fa fa-wrench"></i>
<h3>We Can Fix Anything</h3>
<p>When it comes to plumbing repairs, we’re at the top of our game. From small clogging issues to full sewer line repairs and replacements.</p>
<p>We work with the latest technology to make assessment simple and repair long-lasting. Through video camera inspection, we’re able to see more than ever into the inner workings of your plumbing system to catch existing problems as well as potential ones and make sure they’re stopped dead in their tracks.</p>
<p>You don’t need a large chain of plumbing companies to do the job properly. Rely instead on Vancity Plumbing &amp; Heating. We can give you the personal touch that chains just can’t, and at the competitive rates they’d never dream of offering.</p>
</div>
<div class="large-6 columns midbar horizontal-padding">
<i class="fa fa-users"></i>
<h3>24-Hour Service</h3>
<p>Emergency service is a big part of the world of plumbing. Don’t get roped into paying for shady emergency plumbing services. When you work with Vancity Plumbing &amp; Heating, you’ll receive up-front guaranteed estimates so that you’re not stuck at the end of the job with surprise add-ons.</p>
<p>We’ll arrive on the scene as quickly as possible with all the technology required to handle any problem. And, we won’t just fix the problem, we’ll work to prevent future emergencies as well.</p>
</div>
<div class="large-12 columns midbar horizontal-padding">
<p>No matter what day of the week, or what time it is, you can give us a call whenever you need plumbing services. Going without water is not something anyone wants to go through, especially in cases where you’re experiencing smelly sewer backup as well.</p>
<p>Let us do the dirty work so that you don’t have to. We’ll even provide you with an in-depth estimate for services and materials free of charge, and at no obligation to you.</p>
<p>Give us a call today!</p>
</div>
</div>
</div>
</article>
</main><div class="clearfix"></div>
<footer class="footer-wrapper">
<div class="row">
<section class="large-12 columns strip-padding text-center">
<div class="large-4 medium-4 columns">
<h2>Contact <span class="bolded">Information</span></h2>
</div>
<div class="large-4 medium-4 columns">
<p class="contact-phone phone">Phone: (604) 671-3917</p>
<p class="contact-email email">Email: dvero@vancityplumbing.com</p>
</div>
<div class="large-4 medium-4 columns social-media">
<p>
<a href="https://www.google.com/maps/place/Vancity+Plumbing+%26+Heating/@49.2586327,-123.3769124,9z/data=!3m1!4b1!4m5!3m4!1s0x0:0xf8e8558cf9c443a7!8m2!3d49.259988!4d-122.816501" title="Vancity Plumbing &amp; Heating's Google Map Page"><i class="fa fa-map-marker"></i></a>
<a href="https://www.instagram.com/vancityplumbing/" title="Vancity Plumbing &amp; Heating's Instagram Page"><i class="fa fa-instagram"></i></a>
</p>
</div>
</section>
</div>
<div class="disclaimer">
<div class="row">
<div class="large-12 columns">
<img alt="Logo" src="//linknow.com/linknow_images/linknow-logo-grey.png"/>
</div>
</div>
</div>
</footer><script src="https://vancityplumbing.com/wp-includes/js/comment-reply.min.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/rhinoslider/js/rhinoslider-1.05.min.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/rhinoslider/js/mousewheel.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/rhinoslider/js/easing.min.js"></script>
<script src="https://vancityplumbing.com/wp-content/themes/rplu6/scripts/parallax/jquery.parallax-1.1.3.js"></script>
</body></html>