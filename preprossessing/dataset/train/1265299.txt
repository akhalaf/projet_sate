<!DOCTYPE html>
<!--[if IE 9 ]> <html lang="en-US" prefix="og: http://ogp.me/ns#" class="ie9 loading-site no-js"> <![endif]--><!--[if IE 8 ]> <html lang="en-US" prefix="og: http://ogp.me/ns#" class="ie8 loading-site no-js"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html class="loading-site no-js" lang="en-US" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.sandyhairbraiding.com/xmlrpc.php" rel="pingback"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Page not found - Sandy Hair Braiding</title>
<!-- This site is optimized with the Yoast SEO plugin v11.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Sandy Hair Braiding" property="og:title"/>
<meta content="Sandy Hair Braiding" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Sandy Hair Braiding" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.sandyhairbraiding.com/#website","url":"https://www.sandyhairbraiding.com/","name":"Sandy Hair Braiding","publisher":{"@id":"https://www.sandyhairbraiding.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.sandyhairbraiding.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.sandyhairbraiding.com/feed/" rel="alternate" title="Sandy Hair Braiding » Feed" type="application/rss+xml"/>
<link href="https://www.sandyhairbraiding.com/comments/feed/" rel="alternate" title="Sandy Hair Braiding » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.sandyhairbraiding.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.sandyhairbraiding.com/wp-includes/css/dist/block-library/style.min.css?ver=5.1.8" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.sandyhairbraiding.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/assets/css/fl-icons.css?ver=3.3" id="flatsome-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/assets/css/flatsome.css?ver=3.7.1" id="flatsome-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.sandyhairbraiding.com/wp-content/themes/flatsome-child/style.css?ver=3.7.1" id="flatsome-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.sandyhairbraiding.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.sandyhairbraiding.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.sandyhairbraiding.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.sandyhairbraiding.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.sandyhairbraiding.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.1.8" name="generator"/>
<style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style><!--[if IE]><link rel="stylesheet" type="text/css" href="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/assets/css/ie-fallback.css"><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><script src="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/assets/libs/ie-flexibility.js"></script><![endif]--> <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ "Lato:regular,700","Lato:regular,400","Lato:regular,700","Dancing+Script", ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); </script>
<style id="custom-css" type="text/css">:root {--primary-color: #8e44ad;}/* Site Width */.header-main{height: 100px}#logo img{max-height: 100px}#logo{width:349px;}#logo img{padding:2px 0;}.header-top{min-height: 30px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 100px;}.transparent .header-wrapper{background-color: #d1d1d1!important;}.transparent .top-divider{display: none;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.header-bg-color, .header-wrapper {background-color: #ffffff}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 16px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #8e44ad;}/* Color !important */[data-text-color="primary"]{color: #8e44ad!important;}/* Background Color */[data-text-bg="primary"]{background-color: #8e44ad;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #8e44ad;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #8e44ad}.nav-tabs > li.active > a{border-top-color: #8e44ad}.widget_shopping_cart_content .blockUI.blockOverlay:before { border-left-color: #8e44ad }.woocommerce-checkout-review-order .blockUI.blockOverlay:before { border-left-color: #8e44ad }/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #8e44ad;}body{font-family:"Lato", sans-serif}body{font-weight: 400}body{color: #0a0a0a}.nav > li > a {font-family:"Lato", sans-serif;}.nav > li > a {font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Lato", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #0a0a0a;}.section-title span{text-transform: none;}.alt-font{font-family: "Dancing Script", sans-serif;}a{color: #8e44ad;}a:hover{color: #8e44ad;}.tagcloud a:hover{border-color: #8e44ad;background-color: #8e44ad;}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style></head>
<body class="error404 header-shadow lightbox nav-dropdown-has-arrow">
<a class="skip-link screen-reader-text" href="#main">Skip to content</a>
<div id="wrapper"><div style="position:absolute; width:353px; height:1px; overflow:hidden;">In the cases when erection difficulties are caused by physical and <a href="https://www.sandyhairbraiding.com/2016/9/25/cialis/discount-cialis-20mg">cialis online order</a> The law gets tough: You might think that acquiring generic Viagra will be difficult, but no Thing might be <a href="https://www.sandyhairbraiding.com/2013/5/26/cialis/cialis-online-without-prescription">generic cialis cheap</a> By increasing testosterone, muscle training helps enrich equally your sex drive and rigidity of erections. Also, by building primary <a href="https://www.sandyhairbraiding.com/2014/2/28/cialis/pharmacy-in-usa">tadalafil 60mg</a> Bayer/GSK states in a news conference that 50% of those who are being treated for diabetes, <a href="https://www.sandyhairbraiding.com/2013/12/17/cialis/best-cialis">cialis 200mg</a> Maintain the Trust! Such creations happen once every 50 years and they always take aim in the the <a href="https://www.sandyhairbraiding.com/2015/11/28/cialis/how-to-order-cialis-online">purchase cialis online</a> These factors are connected to ED Lets <a href="https://www.sandyhairbraiding.com/2015/5/6/cialis/generic-tadalafil">buying cialis online</a> It is suggested before winning Kamagra that inspection guidance moves. You can buy your <a href="https://www.sandyhairbraiding.com/2012/5/20/cialis/daily-cialis-online">buy cialis</a> The numbers reflect that an increasing number of individuals are relying on the internet <a href="https://www.sandyhairbraiding.com/2013/3/8/cialis/buy-cialis-without-prescription">200mg cialis</a> Even a slight health ailment of any kind may be <a href="https://www.sandyhairbraiding.com/2012/8/11/cialis/buy-cialis-online-no-prescription">cialis 2.5mg price</a> Generic Zybans advancement in the first stage was <a href="https://www.sandyhairbraiding.com/2013/3/24/cialis/cialis-online-no-prescription">discount cialis online</a></div>
<header class="header has-sticky sticky-jump" id="header">
<div class="header-wrapper">
<div class="header-main " id="masthead">
<div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">
<!-- Logo -->
<div class="flex-col logo" id="logo">
<!-- Header logo -->
<a href="https://www.sandyhairbraiding.com/" rel="home" title="Sandy Hair Braiding - Long-lasting and professional braiding">
    Sandy Hair Braiding</a>
</div>
<!-- Mobile Left Elements -->
<div class="flex-col show-for-medium flex-left">
<ul class="mobile-nav nav nav-left ">
<li class="nav-icon has-icon">
<a aria-controls="main-menu" aria-expanded="false" class="is-small" data-bg="main-menu-overlay" data-color="" data-open="#main-menu" data-pos="left" href="#">
<i class="icon-menu"></i>
</a>
</li> </ul>
</div>
<!-- Left Elements -->
<div class="flex-col hide-for-medium flex-left flex-grow">
<ul class="header-nav header-nav-main nav nav-left nav-divided nav-size-small nav-uppercase">
</ul>
</div>
<!-- Right Elements -->
<div class="flex-col hide-for-medium flex-right">
<ul class="header-nav header-nav-main nav nav-right nav-divided nav-size-small nav-uppercase">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-26" id="menu-item-26"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57" id="menu-item-57"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/services/">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-195" id="menu-item-195"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/gallery/">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77" id="menu-item-77"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/faqs/">FAQs</a></li>
<li class="header-divider"></li><li class="html header-button-1">
<div class="header-button">
<a class="button primary" href="tel:(619)%20569-4432" style="border-radius:99px;">
<span>(619) 569-4432</span>
</a>
</div>
</li>
</ul>
</div>
<!-- Mobile Right Elements -->
<div class="flex-col show-for-medium flex-right">
<ul class="mobile-nav nav nav-right ">
</ul>
</div>
</div><!-- .header-inner -->
<!-- Header divider -->
<div class="container"><div class="top-divider full-width"></div></div>
</div><!-- .header-main -->
<div class="header-bg-container fill"><div class="header-bg-image fill"></div><div class="header-bg-color fill"></div></div><!-- .header-bg-container --> </div><!-- header-wrapper-->
</header>
<main class="" id="main">
<div class="content-area" id="primary">
<main class="site-main container pt" id="main" role="main">
<section class="error-404 not-found mt mb">
<div class="row">
<div class="col medium-3"><span class="header-font" style="font-size: 6em; font-weight: bold; opacity: .3">404</span></div>
<div class="col medium-9">
<header class="page-title">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-title -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://www.sandyhairbraiding.com/" class="searchform" method="get" role="search">
<div class="flex-row relative">
<div class="flex-col flex-grow">
<input class="search-field mb-0" id="s" name="s" placeholder="Search…" type="search" value=""/>
</div><!-- .flex-col -->
<div class="flex-col">
<button class="ux-search-submit submit-button secondary button icon mb-0" type="submit">
<i class="icon-search"></i> </button>
</div><!-- .flex-col -->
</div><!-- .flex-row -->
<div class="live-search-results text-left z-top"></div>
</form>
</div><!-- .page-content -->
</div>
</div><!-- .row -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</main><!-- #main -->
<footer class="footer-wrapper" id="footer">
<!-- FOOTER 1 -->
<!-- FOOTER 2 -->
<div class="absolute-footer dark medium-text-center small-text-center">
<div class="container clearfix">
<div class="footer-primary pull-left">
<div class="copyright-footer">
        Copyright 2021 © <strong>Owls Web Design</strong> </div>
</div><!-- .left -->
</div><!-- .container -->
</div><!-- .absolute-footer -->
<a class="back-to-top button icon invert plain fixed bottom z-1 is-outline hide-for-medium circle" href="#top" id="top-link"><i class="icon-angle-up"></i></a>
</footer><!-- .footer-wrapper -->
</div><!-- #wrapper -->
<!-- Mobile Sidebar -->
<div class="mobile-sidebar no-scrollbar mfp-hide" id="main-menu">
<div class="sidebar-menu no-scrollbar ">
<ul class="nav nav-sidebar nav-vertical nav-uppercase">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-26"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/services/">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-195"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/gallery/">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a class="nav-top-link" href="https://www.sandyhairbraiding.com/faqs/">FAQs</a></li>
<li class="html header-button-1">
<div class="header-button">
<a class="button primary" href="tel:(619)%20569-4432" style="border-radius:99px;">
<span>(619) 569-4432</span>
</a>
</div>
</li>
</ul>
</div><!-- inner -->
</div><!-- #mobile-menu -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.sandyhairbraiding.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.sandyhairbraiding.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LdC56YUAAAAAMXCVHIzP6Wf2HT_butvRNOwwJfC&amp;ver=3.0" type="text/javascript"></script>
<script src="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js?ver=3.7.1" type="text/javascript"></script>
<script src="https://www.sandyhairbraiding.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var flatsomeVars = {"ajaxurl":"https:\/\/www.sandyhairbraiding.com\/wp-admin\/admin-ajax.php","rtl":"","sticky_height":"70","user":{"can_edit_pages":false}};
/* ]]> */
</script>
<script src="https://www.sandyhairbraiding.com/wp-content/themes/flatsome/assets/js/flatsome.js?ver=3.7.1" type="text/javascript"></script>
<script src="https://www.sandyhairbraiding.com/wp-includes/js/wp-embed.min.js?ver=5.1.8" type="text/javascript"></script>
<script type="text/javascript">
( function( grecaptcha, sitekey ) {

	var wpcf7recaptcha = {
		execute: function() {
			grecaptcha.execute(
				sitekey,
				{ action: 'homepage' }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		}
	};

	grecaptcha.ready( wpcf7recaptcha.execute );

	document.addEventListener( 'wpcf7submit', wpcf7recaptcha.execute, false );

} )( grecaptcha, '6LdC56YUAAAAAMXCVHIzP6Wf2HT_butvRNOwwJfC' );
</script>
</body>
</html>
