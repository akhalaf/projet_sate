<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>BORG Krems</title>
<meta content="Hans Peter Kainz" name="author"/>
<meta content="Schule, Bildung, Gymnasium, BORG, Oberstufe, Niederösterreich, Krems, Musik, Mediendesign, Informatik, Naturwissenschaften, Bildnerische Erziehung" name="keywords"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/images/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/css/style.css" rel="stylesheet"/>
<link href="/css/datum.css?v=1" rel="stylesheet"/>
<link href="/js/highslide/highslide.css" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
<div class="container">
<!-- Titel und Schalter werden für eine bessere mobile Ansicht zusammengefasst -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Navigation ein-/ausblenden</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!-- Alle Navigationslinks, Formulare und anderer Inhalt werden hier zusammengefasst und können dann ein- und ausgeblendet werden -->
<div class="collapse navbar-collapse" id="navbar-collapse">
<ul class="nav navbar-nav">
<li class="active"><a href="/index.php">Aktuelles</a></li>
<li><a href="/termine/index.php">Termine</a></li>
<li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Schule <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="/schule/kontakt/kontakt.php">Kontakt</a></li>
<li><a href="/schule/lehrergalerie/lehrergalerie.php">Lehrerinnen und Lehrer</a></li>
<!-- <li><a href="/schule/sprechstunden/sprechstunden.php">Sprechstunden</a></li> -->
<li><a href="https://asopo.webuntis.com/WebUntis/?school=borg-krems#/basic/officehours">Sprechstunden</a></li>
<li><a href="/schule/unterrichtszeiten.php">Unterrichtszeiten</a></li>
<li><a href="/schule/elternverein/index.php">Elternverein</a></li>
<li><a href="https://schulbibliothek-borgkrems.blogspot.co.at/">Schulbibliothek</a></li>
<li class="divider"></li>
<li><a href="/bigband/index.php">BORG Big Band</a></li>
<li><a href="/schule/schulprofil/">Schulprofil</a></li>
<li><a href="/schule/schulpraesentation/index.php">Schulpräsentation</a></li>
<li><a href="/schule/schulfilm/index.php">Schulfilm</a></li>
<li><a href="/schule/schulsong/index.php">BORG Song</a></li>
<li><a href="/schule/night-of-science/index.php">Night of Science</a></li>
</ul>
</li>
<li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Service <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="/service/formulare.php">Formulare</a></li>
<li><a href="/service/stundentafeln.php">Stundentafeln</a></li>
<li><a href="/service/hausordnung.php">Hausordnung</a></li>
<!-- <li><a href="https://www.biblioweb.at/BORGKrems">Bibliotheksrecherche</a></li> -->
<li class="divider"></li>
<li class="dropdown-submenu">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Schüler/innenberatung</a>
<ul class="dropdown-menu">
<li><a href="/service/bildungsberatung/index.php">Personen und Kontakt</a></li>
<li><a href="/service/bildungsberatung/aufgaben.php">Aufgaben</a></li>
<li><a href="/service/bildungsberatung/links.php">Links</a></li>
<li><a href="/service/bildungsberatung/termine/">Termine</a></li>
<!-- <li><a href="/service/bildungsberatung/lerntipp.php">Lerntipp des Monats</a></li> -->
</ul>
</li>
<li class="divider"></li>
<li><a href="/seiten/seite.php?id=9">Schulbuffet</a></li>
<li><a href="/service/vor.php/">VOR-Routenplaner</a></li>
<li><a href="https://asopo.webuntis.com/WebUntis/?school=borg-krems#officehourlist">Webuntis</a></li>
<li><a href="/roundcube/">BORG E-Mail</a></li>
<li><a href="https://cloud.borg-krems.ac.at/"> NextCloud</a></li>
<li><a href="/net2ftp/">BORG FTP</a></li>
<li><a href="https://moodle.borg-krems.ac.at">BORG Moodle</a></li>
<li><a href="/aktuell/archiv.php">Aktuelles (Archiv)</a></li>
</ul>
</li>
<li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Zweige <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="/zweige/mediendesignzweig/index.php">Mediendesign</a></li>
<li><a href="/zweige/musikalischezweige/index.php">Musikalische Zweige</a></li>
<li><a href="/zweige/bildnerischerzweig/index.php">Bildnerischer Zweig</a></li>
<li><a href="/zweige/naturwissenschaftlicherzweig/index.php">Naturwissenschaftlicher Zweig</a></li>
</ul>
</li>
<li class="dropdown ">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Intern <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="/intern-lehrer/index.php">Lehrer/innen</a></li>
<li><a href="/intern-schueler/index.php">Schüler/innen</a></li>
</ul>
</li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="/login/login.php">Login</a></li>
</ul>
<p class="navbar-text navbar-right">(Nicht angemeldet)</p> </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<header id="kopf">
<div class="container">
<a href="https://www.borg-krems.ac.at/">
<img class="thumbnail" src="/images/borg-logo.png" title="BORG Krems"/>
</a>
</div> </header>
<div class="well well-sm">
<div class="container">
<ol class="breadcrumb">
<li><a href="/index.php">Start</a></li>
<li class="active">Aktuelles</li>
</ol>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-9">
<h2>Aktuelles</h2>
</div>
</div>
<div class="row">
<div class="col-md-3 col-md-push-9">
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title"><strong>BORG Krems</strong></div>
</div>
<div class="panel-body">
<p>Heinemannstraße 12,<br/>
        3500 Krems/Donau
    </p>
<p>
</p><p>Sekretariat: 02732 82313<br/>
      (von 7:00 bis 15:00 Uhr)<br/>
      Konferenzzimmer: 02732 82313 15<br/>
      Fax: 02732 82313 17<br/>
      EMail: <a href="mailto:direktion@borg-krems.ac.at">direktion@borg-krems.ac.at</a>
</p>
<p>Persönliche Termine nur nach vorheriger Vereinbarung!</p>
</div>
</div>
<!--
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">Ferien</div>
  </div>
   <div class="panel-body">
    <p>Weihnachtsferien vom 23.&#8239;12.&#8239;2020 bis 06.&#8239;01.&#8239;2021</p>
  </div>
</div>
-->
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Webuntis</div>
</div>
<div class="panel-body">
<a class="thumbnail" href="https://asopo.webuntis.com/WebUntis/?school=borg-krems">
<img src="/images/webuntis.png" title="Webuntis"/>
</a>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Schnuppertage</div>
</div>
<div class="panel-body">
<p>Schnuppertage sind nach telefonischer Anmeldung möglich.</p>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Nächste Termine</div>
</div>
<div class="panel-body"><p class="abschneiden">
<a class="terminlinks" href="/termine/termin.php?id=804">28.01.2021 - Kompensationsprüfungen Frühjahrstermin</a></p><p class="abschneiden">
<a class="terminlinks" href="/termine/termin.php?id=803">01.02.2021 - Semesterferien</a></p><p class="abschneiden">
<a class="terminlinks" href="/termine/termin.php?id=807">29.03.2021 - Osterferien</a></p>
<p><a href="/termine/externistenpruefungstermine-20-21.pdf">Externistenprüfungstermine</a></p>
</div>
<div class="panel-footer">
<p><a href="/termine/index.php">Weitere Termine …</a></p>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Anmeldung BORG Krems
            </div>
</div>
<div class="panel-body"><p>29.01.2021 von 08:00 bis 17:00 Uhr<br/>
08.02.2021 bis 19.02.2021 von 08:00 bis 15:00 Uhr</p>
<!--
<p>22.02.2019 von 14:00 bis 17:00 Uhr<br />
EignungsgesprÃ¤che der musischen Zweige (bildnerisch und besonders musikalisch)</p>
-->
<p><a href="https://www.borg-krems.ac.at/service/formulare/Anmeldung-BORG-Krems.pdf">Nähere Informationen</a> zur Anmeldung.</p>
<p>Anmeldeformular und Gesundheitsblatt unter <a href="https://www.borg-krems.ac.at/service/formulare.php">Service - Formulare - Anmeldung</a> zum Download.</p>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">BORG Moodle</div>
</div>
<div class="panel-body">
<a class="thumbnail" href="https://moodle.borg-krems.ac.at">
<img src="/images/moodle-logo.png" title="BORG Moodle"/>
</a>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Time4Friends.at</div>
</div>
<div class="panel-body">
<p>Du fühlt dich 😭 ?<br/>
    Dein Leben ist gerade 🤮 ?<br/>
    Du kannst mit niemanden darüber reden 🤐 ?</p>
<p>
    Wir sind für dich da!<br/>
    Schreib uns via WhatsApp:<br/> +43 664 1070 144</p>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Portal Digitale Schule</div>
</div>
<div class="panel-body">
<a class="thumbnail" href="https://www.pods.gv.at/">
<img src="/images/pods.png" title="Portal Digitale Schule"/>
</a>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">BORG E-Mail</div>
</div>
<div class="panel-body">
<a class="thumbnail" href="https://www.borg-krems.ac.at/roundcube/">
<img src="/images/rcube_logo.png" title="Roundcube"/>
</a>
</div>
</div>
<!--
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">Lerntipp</div>
  </div>
  <div class="panel-body">
    <a class="thumbnail" href="/service/bildungsberatung/lerntipp.php">
      <img src="/images/lerntipp.png" title="Lerntipp">
    </a>
  </div>
</div>
-->
<div class="panel panel-default panel-body hidden-md hidden-lg">
<a href="/schule/schulpraesentation/index.php"><img class="img-responsive" src="/schule/schulpraesentation/images/infotag-2020.jpg"/></a>
</div>
</div>
<div class="col-md-9 col-md-pull-3">
<div class="panel panel-default panel-body hidden-sm hidden-xs">
<a href="/schule/schulpraesentation/index.php"><img class="img-responsive" src="/schule/schulpraesentation/images/infotag-2020.jpg"/></a>
</div>
<div class="panel panel-default panel-body"><h3 class="panel-titel">BORG Krems <em>Home Office 7D Klassenchor</em> presents <em>A Holly Jolly Christmas</em></h3><div><div class="postdate"><div class="year">2020</div><div class="month">Dez</div><div class="day">22</div></div><div class="thumbnail pull-right abstand-links"><img alt="BORG Krems &lt;em&gt;Home Office 7D Klassenchor&lt;/em&gt; presents &lt;em&gt;A Holly Jolly Christmas&lt;/em&gt;" class="img-responsive" src="/aktuell/2020-12-22-weihnachten/Bildschirmfoto_2020-12-22_um_18.03.52.png"/></div><p></p><p>Die besonders musische 7D des BORG Krems wünscht aus dem Home-schooling - aber dennoch gemeinsam als Chor - musikalisch frohe Weihnachten mit dem bekannten Song <em>A Holly Jolly Christmas</em>.</p>
<p>Klassenchor der 7D<br/>
Drums: Christoph Gurmann<br/>
Cajon: Philipp Gartner<br/>
Musikalische Leitung, Audiomischung und Videoschnitt: Agnes Frittum</p>
<p><a href="https://www.youtube.com/watch?v=oAlTY_quzLQ">https://www.youtube.com/watch?v=oAlTY_quzLQ </a></p>
<p>Die Schulgemeinde des BORG Krems wünscht frohe Weihnachten und viel Glück im neuen Jahr.</p>
</div></div><div class="panel panel-default panel-body"><h3 class="panel-titel"><em>Life is Science</em>  – ein Lehrausgang zu Coronazeiten</h3><div><div class="postdate"><div class="year">2020</div><div class="month">Dez</div><div class="day">13</div></div><p></p><p>Ein <em>Lehrausgang</em> zu Coronazeiten: Geht das? Ja, aber etwas anders. Am 27. November 2020 besuchten die NAWI Klassen  unserer Schule im Rahmen des Unterrichts voller Interesse das Online - <em>Event Live is Science</em>.  <em>Life Is Science</em> war heuer der Titel der jedes Jahr europaweit stattfindenden <em>European Researchers Night</em>. Das Event bot Vorträge und  Workshops mit vielen bekannten Wissenschaftlern und Wissenschaftlerinnen zu den verschiedensten Themen.</p>
<a href="/aktuell/aktuellweiterlesen.php?id=603">Mehr …</a></div></div><div class="panel panel-default panel-body"><h3 class="panel-titel"></h3><div><div class="postdate"><div class="year">2020</div><div class="month">Okt</div><div class="day">24</div></div><p></p><p class="untertitel">Sehr geehrte Eltern, geschätzte Erziehungsberechtigte!</p>
<p>Wir befinden uns bereits mitten im Wintersemester, die ersten österreichweiten Herbstferien stehen vor uns, und es ist diesmal kein Schuljahr, wie wir es „gewohnt“ wären. Wir leben mit Corona, wir haben immer wieder neue Situationen zu bewältigen und wir tun das auch. Es gibt natürlich immer wieder neue Umstände, die zu berücksichtigen sind. Wir werden das nur gemeinsam bewerkstelligen, und daher ersuche ich Sie, wie auch schon in meinem Schreiben zu Schulbeginn, weiterhin um so konstruktive Zusammenarbeit wie bisher.</p>
<p>Derzeit ist unser BORG-Krems-eigener Plan für Schulphase Orange in Warteposition und wird im Falle der Umstellung der Bezirksfarbe auf Rot zum Einsatz kommen. Es wird, entgegen mancher Gerüchte, in Schulfarbe Orange am BORG Krems dann weiterhin Unterricht geben, wenn auch eingeschränkt. Alle diesbezüglichen Informationen bekommen Sie und bekommt Ihr Kind auf jeden Fall rechtzeitig über den Klassenvorstand/die Klassenvorständin.</p>
<p>Falls Sie darüber hinaus Weiteres wissen möchten, kontaktieren Sie uns (KV, Lehrkraft) bitte einfach, damit Sie offene Fragen gleich beantwortet bekommen können und Bescheid wissen.</p>
<p>Ich wünsche Ihnen einen möglichst guten Herbst mit viel Gesundheit und bestmöglichen Erfolg in allen Belangen!</p>
<p>Herzliche Grüße</p>
<p>Mag. Barbara Faltl</p>
<p>Direktorin</p>
</div></div><div class="panel panel-default panel-body"><h3 class="panel-titel">Ehrung »Summa cum laude«</h3><div><div class="postdate"><div class="year">2020</div><div class="month">Sep</div><div class="day">23</div></div><div class="thumbnail pull-right abstand-links"><img alt="Ehrung »Summa cum laude«" class="img-responsive" src="/aktuell/2020-09-23-summacumlaude/IMG_0852.jpg"/></div><p></p><p>Katharina Auer und Florentin Baumgartner, beide ehemals 8D, wurden am Donnerstag, dem 17. September 2020, im Rathaus Krems/Stein gemeinsam mit 18 anderen Kremser Maturantinnen und Maturanten des Jahrgangs 2019/20 für ihre herausragenden Leistungen geehrt: alle Schuljahre der Oberstufe mit ausgezeichnetem Erfolg abgeschlossen sowie die Reifeprüfung mit lauter Sehr gut abgelegt. Sie erhielten von der Stadt Krems jeweils einen extra angefertigten Ehrenring. Bürgermeister Dr. Resch, RegR Grünstäudl (Leiter der Bildungsregion 1, in Vertretung HR Heuras‘) und Bildungsstadträtin Hockauf-Bartaschek »überreichten« verbal (Hygienevorschriften!) und vor ausgewähltem Publikum (Hygienevorschriften…) Lob, Urkunde und Ehrenring.</p>
<p>Das BORG Krems und Klassenvorständin Mag.<sup>a</sup> Agnes Firttum gratulieren zu dieser Auszeichnung ganz besonders herzlich!</p>
</div></div><div class="panel panel-default panel-body"><h3 class="panel-titel">Schulbeginn 2020</h3><div><div class="postdate"><div class="year">2020</div><div class="month">Sep</div><div class="day">04</div></div><p></p><p>Sehr geehrte Eltern, sehr geehrte Erziehungsberechtigte!</p>
<p>Zu Beginn des neuen Schuljahres begrüße ich Sie in einem außergewöhnlichen Herbst.<br/>
Die Schule wird aus heutiger Sicht ganz normal starten. Abstandhalten und das Mitführen/Verwenden eines eigenen Mund-Nasen-Schutzes sind dafür allerdings Voraussetzung, denn wir wollen ja möglichst dauerhaft "normalen" Unterricht halten. Bitte erinnern auch Sie Ihr Kind/ Ihre Kinder in diesem Sinne an die Einhaltung der Maßnahmen!</p>
<p>BM Faßmann übersendet Ihnen zu Schulbeginn 2020 einen persönlichen Brief, den Sie im Anhang finden.</p>
<p>Alle weiteren Informationen erhält Ihr Sohn/Ihre Tochter ab sofort vom jeweiligen Klassenvorstand oder der Klassenvorständin, und auch mein Team und ich werden immer wieder in Kontakt mit Ihnen treten.<br/>
Für Fragen stehen wir Ihnen gerne per Mail oder zu den gewohnten Öffnungszeiten zur Verfügung.<br/>
Zur Erinnerung: Der Begrüßungsgottesdienst beginnt am Montag, 7.9.2020, um 7.45 Uhr, danach ist Unterricht bis 11.30 Uhr.</p>
<p>Mit herzlichen Grüßen</p>
<p>Mag.<sup>a</sup> Barbara Faltl<br/>
Direktorin</p>
<p><a href="/aktuell/2020-09-07-schulbeginn/Schule_im_Herbst.pdf">Schule im Herbst (PDF)</a></p>
<p><a href="/aktuell/2020-09-07-schulbeginn/Elternbrief_BM_Fassmann_Aschbacher.pdf">Elternbrief Minister Fassmann und Ministerin Aschbacher (PDF)</a></p>
</div></div><div class="panel panel-default panel-body"><h3 class="panel-titel">Öffungszeiten während der Ferien</h3><div><div class="postdate"><div class="year">2020</div><div class="month">Jul</div><div class="day">15</div></div><p></p><p>Während der Sommerferien ist das Sekretariat nicht durchgehend besetzt. Bitte richten Sie eventuelle Anfragen per Mail an direktion@borg-krems.ac.at. Diese Mailadresse wird in regelmäßigen Abständen abgefragt.</p>
<p>Parteienverkehr findet an folgenden Dienstagen in der Zeit von 7:30 bis 11:30 Uhr statt:</p>
<p style="margin-left: 3em">21.7.2020<br/>
28.7.2020<br/>
4.8.2020</p>
<p>Bitte unbedingt zuvor einen Termin unter der oben angeführten Mailadresse bzw. an diesen Dienstagen telefonisch unter 02732/82313 vereinbaren.</p>
<p>In der Zeit von 10.8. bis einschließlich 30.8.2020 ist das Sekretariat nicht besetzt!</p>
</div></div><ul class="pagination">
<li class="disabled"><a href="index.php?seite=0">«</a></li><li class="active"><a href="index.php?seite=0">1</a></li><li><a href="index.php?seite=1">2</a></li><li><a href="index.php?seite=2">3</a></li><li><a href="index.php?seite=3">4</a></li><li><a href="index.php?seite=4">5</a></li><li><a href="index.php?seite=1">»</a></li>
</ul> </div>
</div>
</div>
<footer class="footer" id="fuss">
<div class="footer">
<div class="container">
<div class="row">
<div class="col-md-2">
<div class="panel-body">
<a class="thumbnail" href="https://www.mintschule.at/">
<img src="/images/mint-guetesiegel.jpg"/>
</a>
<a class="thumbnail" href="http://www.gymnasium-noe.at/">
<img src="/images/gym_logo.png"/>
</a>
</div>
</div>
<div class="col-md-2">
<div class="panel-body">
<a class="thumbnail" href="https://www.bildung-noe.gv.at/Schule-und-Unterricht/Musikp-dagogik/Singende-klingende-Schule/Allgemeines.html">
<img src="/images/singend-klingende-2019.jpg"/>
</a>
</div>
</div>
<div class="col-md-8">
<ul>
<li><a href="https://genehmigung.ahs-vwa.at/">VWA Genehmigungsdatenbank</a></li>
<li><a href="https://www.sokrates-bund.at/BRZPROD/">Sokrates</a></li>
<li><a href="https://www.digi4school.at">Digi4School</a></li>
<li><a href="https://www.outlook.com/bildung.gv.at">Dienst-EMail</a></li>
<li><a href="https://portal.microsoft.com">Microsoft 365</a></li>
<li><a href="https://www.pods.gv.at/">Portal Digitale Schule</a></li>
<!-- <li><a href="/kremserschulen/schulen.php">Schulen in Krems</a></li> -->
<!-- <li><a href="/bigband/index.php">BORG Big Band</a></li> -->
<li><a href="http://cloud.borg-krems.ac.at/">Nextcloud</a></li>
<li><a href="/net2ftp/">BORG FTP</a></li>
<li><a href="/aktuell/archiv.php">Aktuelles (Archiv)</a></li>
<li><a href="http://www.agchemie-noe.at">AG Chemielehrer/innen NÖ</a></li>
</ul>
</div>
</div>
<div class="row">
<div class="col-md-12 copyright">
<p class="pull-left">© H.P.Kainz 2015</p>
</div>
</div>
</div>
</div>
</footer>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-submenu.js"></script>
<script src="/js/jquery.bgswitcher.js"></script>
<script src="/js/headerchange.js"></script>
<script src="/js/highslide/highslide.js"></script>
</body>
</html>
