<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<title>... | Working In Visas - Australia</title>
<link href="https://www.workingin-visas.com.au/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&amp;family=Muli:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet"/>
<link href="https://www.workingin-visas.com.au/css/style.css" rel="stylesheet"/>
<style>
	.topbar h2 {
    font-size: 24px;

}
	</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-76315444-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-76315444-1');
</script>
<link href="/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="#ffffff" name="msapplication-TileColor"/>
<meta content="/ms-icon-144x144.png" name="msapplication-TileImage"/>
<meta content="#ffffff" name="theme-color"/>
</head>
<body>
<header class="main-header nav-down">
<div class="modal-backdrop" type="reset"></div>
<div class="topbar d-flex allign-items-center justify-content-center" style="">
<h2 style="text-transform:uppercase;max-width:100%;">RECEIVE UPDATES FOR OUR UPCOMING WEBINARS ON EMIGRATING TO AUSTRALIA</h2>
<a class="btn btn-warning" href="https://www.workingin-visas.com.au/webinar/">Find out more</a>
</div>
<nav class="navbar navbar-expand-md navbar-light">
<div class="container">
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon">
<svg viewbox="0 0 30 30" xmlns="http://www.w3.org/2000/svg"><path d="M4 7h22M4 15h22M4 23h22" stroke="#fcc10f" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"></path></svg>
</span>
</button>
<a class="navbar-brand" href="https://www.workingin-visas.com.au">
<img alt="" src="https://www.workingin-visas.com.au/img/logo.png"/>
</a>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto">
<li class="nav-item ">
<a class="nav-link" href="/services/">Services</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://www.workingin-visas.com.au/expertise/">Expertise</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://www.workingin-visas.com.au/employer/">Employer</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://www.workingin-visas.com.au/about/">About</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://www.workingin-visas.com.au/register-now/">Register</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.workingin-visas.com.au/#contact-us">Contact</a>
</li>
</ul>
</div>
</div>
</nav>
</header>
<div id="container">
<style>
		@media screen and (max-width: 600px) {
			.topbar h2 {
    font-size: 20px;
			}
	section.main-banner h1 {
    font-size: 31px;
    line-height: 100%;
    margin-bottom: 24px;
    margin-top: 124px;
}
			
.custom-conatainer {
    padding-top: 09px !important;
}
			section.sec-student-visa.content-right {
    padding-top: 10px;
}
			.small-text, .small-title
			{
				line-height: 50px !important;
			}
			.cardText2 {
    margin-left: 1px !important;
}
			.sec-businesses.content-left img {
    min-height: 303px !important;
    
}

			.sec-two-column
			{
				margin-top:-68px;
			}
			.sec-permament
			{
				margin-top:-64px;
			}
			section.main-banner {
    min-height: 130px;
}
			.text-center {
  
    padding-top: 31px;
}
			
			.w-100 {
    width: 50% !important;
 
    margin: auto;
}
				}
	
		
	</style><main id="content">
<article class="post not-found" id="post-0">
<header class="header">
<h1 class="entry-title">Not Found</h1>
</header>
<div class="entry-content">
<p>Nothing found for the requested page. Try a search instead?</p>
<form action="https://www.workingin-visas.com.au/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></div>
</article>
</main>
<aside id="sidebar">
<div class="widget-area" id="primary">
<ul class="xoxo">
<li class="widget-container widget_search" id="search-2"><form action="https://www.workingin-visas.com.au/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></li>
<li class="widget-container widget_recent_entries" id="recent-posts-2">
<h3 class="widget-title">Recent Posts</h3>
<ul>
<li>
<a href="https://www.workingin-visas.com.au/uncategorized/hello-world/">Hello world!</a>
</li>
</ul>
</li><li class="widget-container widget_recent_comments" id="recent-comments-2"><h3 class="widget-title">Recent Comments</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow ugc">A WordPress Commenter</a></span> on <a href="https://www.workingin-visas.com.au/uncategorized/hello-world/#comment-1">Hello world!</a></li></ul></li></ul>
</div>
</aside><footer class="sec-contact gray-sec" id="contact-us">
<div class="container">
<div class="inner-form text-center">
<h2 class="text-dark" style="text-transform:uppercase;">We’re ready to work with you today</h2>
<ul class="d-flex align-items-center justify-content-center radio-list">
<li>
<label class="radio">
<input name="is_company" type="radio"/>
<span class="checkround"></span>
<span class="text">Business enquiry</span>
</label>
</li>
<li>
<label class="radio">
<input name="is_company" type="radio"/>
<span class="checkround"></span>
<span class="text">Personal enquiry</span>
</label>
</li>
</ul>
<form action="https://www.workingin-visas.co.nz/email/aumail.php" class="contact-form" method="GET" name="form">
<div class="form-group">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
<input class="form-control" name="first_name" placeholder="Your name" required="" type="text"/>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
<input class="form-control" name="company" placeholder="Company name" type="text"/>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
<input class="form-control" name="email" placeholder="Email" required="" type="text"/>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
<input class="form-control" name="phone" placeholder="Phone" type="text"/>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-lg-12">
<textarea class="form-control" name="description" placeholder="Message" required=""> </textarea>
</div>
</div>
</div>
<input class="faxit" name="your_fax" placeholder="Fax" type="text"/>
<div class="form-group">
<input class="btn btn-warning btn-block" name="submit" type="submit" value="Get in touch"/>
</div>
</form>
<div class="contact-detail">
<div class="form-group">
<p>Call us</p>
<a href="tel:+61 2 95754848">+61 2 95754848</a>
</div>
<div class="form-group">
<p>Or email us</p>
<a href="mailto:visa.team@workingin.com">visa.team@workingin.com</a>
</div>
</div>
<div class="contact-list d-flex align-items-center justify-content-center">
<div class="social-list">
<h5>Contact us</h5>
<ul>
<li>
<a href="tel:+61 2 95754848"> <img alt="Icon" src="https://www.workingin-visas.com.au/img/icons/phoneicon.png"/> </a>
</li>
<li>
<a href="mailto:visa.team@workingin.com"> <img alt="Icon" src="https://www.workingin-visas.com.au/img/icons/envelop.png"/></a>
</li>
<li style="display:none;">
<a href="#" style="display:none;"> <img alt="Icon" src="https://www.workingin-visas.com.au/img/icons/linkedin.png"/> </a>
</li>
</ul>
</div>
</div>
</div>
</div>
</footer>
<script src="https://www.workingin-visas.com.au/js/jquery-3.5.1.min.js"></script>
<script data-auto-replace-svg="nest" src="https://use.fontawesome.com/releases/v5.13.1/js/all.js"></script>
<script src="https://www.workingin-visas.com.au/js/bootstrap.min.js"></script>
<script src="https://www.workingin-visas.com.au/js/script.js"></script>
<style>
.faxit {
    display: none;
}

@media (max-width: 600px) {
		h2 {
    font-size: 30px;
    
}

	}
</style>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<div id="fb-pxl-ajax-code"></div></div></body>
</html>