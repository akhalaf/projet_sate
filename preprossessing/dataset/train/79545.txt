<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Adveric.net | Self service ad-buying platform</title>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet"/>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- Owl Carousel -->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="css/owl.theme.default.css" rel="stylesheet" type="text/css"/>
<!-- Magnific Popup -->
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<!-- Font Awesome Icon -->
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<!-- Custom stlylesheet -->
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
</head>
<body>
<!-- Header -->
<header id="home">
<!-- Background Image -->
<div class="bg-img" style="background-image: url('./img/background1.jpg');">
<div class="overlay"></div>
</div>
<!-- /Background Image -->
<!-- Nav -->
<nav class="navbar nav-transparent" id="nav">
<div class="container">
<div class="navbar-header">
<!-- Logo -->
<div class="navbar-brand">
<a href="index.html">
<div><h2 style="color:white">Adveric</h2></div>
</a>
</div>
<!-- /Logo -->
<!-- Collapse nav button -->
<div class="nav-collapse">
<span></span>
</div>
<!-- /Collapse nav button -->
</div>
<!--  Main navigation  -->
<ul class="main-nav nav navbar-nav navbar-right">
<li><a href="#home">Home</a></li>
<li><a href="#about">About</a></li>
<li><a href="#features">Why Us</a></li>
<li><a href="#contact">Contact</a></li>
</ul>
<!-- /Main navigation -->
</div>
</nav>
<!-- /Nav -->
<!-- home wrapper -->
<div class="home-wrapper">
<div class="container">
<div class="row">
<!-- home content -->
<div class="col-md-10 col-md-offset-1">
<div class="home-content">
<h2 class="white-text">Get control of your live campaigns</h2>
<p class="white-text">We give media sellers, advertisers, brands and publishers the infrastructure and tools they requir to make advertising grow and scale.
							</p>
<button class="white-btn">Get Started!</button>
<button class="main-btn">Learn more</button>
</div>
</div>
<!-- /home content -->
</div>
</div>
</div>
<!-- /home wrapper -->
</header>
<!-- /Header -->
<!-- About -->
<div class="section md-padding" id="about">
<!-- Container -->
<div class="container">
<!-- Row -->
<div class="row">
<!-- Section header -->
<div class="section-header text-center">
<h2 class="title">Welcome to Adveric</h2>
</div>
<!-- /Section header -->
<!-- about -->
<div class="col-md-4">
<div class="about">
<i class="fa fa-cogs"></i>
<h3>Brand Safety</h3>
<p>was designed to ensure the highest level of security for advertisers.</p>
<a href="#">Read more</a>
</div>
</div>
<!-- /about -->
<!-- about -->
<div class="col-md-4">
<div class="about">
<i class="fa fa-magic"></i>
<h3>Viewability</h3>
<p>Adveric has implemented smart technology approach to ensure superb viewability of all ads delivered via our platform.</p>
<a href="#">Read more</a>
</div>
</div>
<!-- /about -->
<!-- about -->
<div class="col-md-4">
<div class="about">
<i class="fa fa-mobile"></i>
<h3>Precise targeting</h3>
<p>Find the right user by first-party data, third-party data, ISP, geolocation, behavior, content, device type, OS, and language. Enjoy Adveric granular targeting to reach and convert your target audience all over the world!.</p>
<a href="#">Read more</a>
</div>
</div>
<!-- /about -->
</div>
<!-- /Row -->
</div>
<!-- /Container -->
</div>
<!-- /About -->
<!-- Why Choose Us -->
<div class="section md-padding bg-grey" id="features">
<!-- Container -->
<div class="container">
<!-- Row -->
<div class="row">
<!-- why choose us content -->
<div class="col-md-6">
<div class="section-header">
<h2 class="title">Why Choose Us</h2>
</div>
<p>Adveric technology employs sophisticated learning algorithms to ensure ad serving efficiency. Our platform operates and analyzes a huge amount of data, delivering ad impressions across multiple channels from all over the world.</p>
<div class="feature">
<i class="fa fa-check"></i>
<p>We bring powerful insights in a real-time data analytics.</p>
</div>
<div class="feature">
<i class="fa fa-check"></i>
<p>Deep research to help you better understand your audience and fuel your marketing performance.</p>
</div>
<div class="feature">
<i class="fa fa-check"></i>
<p>connecting advertisers with the premium supply partners.</p>
</div>
</div>
<!-- /why choose us content -->
<!-- About slider -->
<div class="col-md-6">
<div class="owl-carousel owl-theme" id="about-slider">
<img alt="" class="img-responsive" src="./img/about1.jpg"/>
<img alt="" class="img-responsive" src="./img/about2.jpg"/>
<img alt="" class="img-responsive" src="./img/about1.jpg"/>
<img alt="" class="img-responsive" src="./img/about2.jpg"/>
</div>
</div>
<!-- /About slider -->
</div>
<!-- /Row -->
</div>
<!-- /Container -->
</div>
<!-- /Why Choose Us -->
<!-- Contact -->
<div class="section md-padding" id="contact">
<!-- Container -->
<div class="container">
<!-- Row -->
<div class="row">
<!-- Section-header -->
<div class="section-header text-center">
<h2 class="title">Get in touch</h2>
</div>
<!-- /Section-header -->
<!-- contact -->
<div class="col-sm-4">
<div class="contact">
<i class="fa fa-phone"></i>
<h3>Phone</h3>
<p>564-251-4658</p>
</div>
</div>
<!-- /contact -->
<!-- contact -->
<div class="col-sm-4">
<div class="contact">
<i class="fa fa-envelope"></i>
<h3>Email</h3>
<p>info@adveric.net</p>
</div>
</div>
<!-- /contact -->
<!-- contact -->
<div class="col-sm-4">
<div class="contact">
<i class="fa fa-map-marker"></i>
<h3>Address</h3>
<p>5869 malline Road</p>
</div>
</div>
<!-- /contact -->
<!-- contact form -->
<div class="col-md-8 col-md-offset-2">
<form class="contact-form">
<input class="input" placeholder="Name" type="text"/>
<input class="input" placeholder="Email" type="email"/>
<input class="input" placeholder="Subject" type="text"/>
<textarea class="input" placeholder="Message"></textarea>
<button class="main-btn">Send message</button>
</form>
</div>
<!-- /contact form -->
</div>
<!-- /Row -->
</div>
<!-- /Container -->
</div>
<!-- /Contact -->
<!-- Footer -->
<footer class="sm-padding bg-dark" id="footer">
<!-- Container -->
<div class="container">
<!-- Row -->
<div class="row">
<div class="col-md-12">
<!-- footer logo -->
<div class="footer-logo">
<a href="index.html"><h2 style="color:white">Adveric</h2></a>
</div>
<!-- /footer logo -->
<!-- footer follow -->
<ul class="footer-follow">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-youtube"></i></a></li>
</ul>
<!-- /footer follow -->
<!-- footer copyright -->
<div class="footer-copyright">
<p>Copyright Â© 2018. All Rights Reserved. Designed by <a href="https://adveric.net" target="_blank">Adveric.net</a></p>
</div>
<!-- /footer copyright -->
</div>
</div>
<!-- /Row -->
</div>
<!-- /Container -->
</footer>
<!-- /Footer -->
<!-- Back to top -->
<div id="back-to-top"></div>
<!-- /Back to top -->
<!-- Preloader -->
<div id="preloader">
<div class="preloader">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</div>
<!-- /Preloader -->
<!-- jQuery Plugins -->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
</body>
</html>
