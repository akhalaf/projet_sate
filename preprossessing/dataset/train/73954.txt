<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Add Air Lawn Care</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="http://www.addairlawncare.com.au/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/images/fav.ico" rel="shortcut icon"/>
<link href="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/nivo-slider/themes/default/default.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/nivo-slider/nivo-slider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.addairlawncare.com.au/feed/" rel="alternate" title="Add Air Lawn Care » Feed" type="application/rss+xml"/>
<link href="https://www.addairlawncare.com.au/comments/feed/" rel="alternate" title="Add Air Lawn Care » Comments Feed" type="application/rss+xml"/>
<link href="https://www.addairlawncare.com.au/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.3.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/style.css?ver=3.4.2" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.addairlawncare.com.au/wp-includes/js/jquery/jquery.js?ver=1.7.2" type="text/javascript"></script>
<link href="https://www.addairlawncare.com.au/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.addairlawncare.com.au/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 3.4.2" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home blog">
<div class="hfeed site" id="page">
<header class="site-header" id="masthead" role="banner">
<hgroup>
<h1 class="site-title"><a href="https://www.addairlawncare.com.au/" rel="home" title="Add Air Lawn Care"></a></h1>
</hgroup>
<nav class="site-navigation main-navigation" role="navigation">
<h1 class="assistive-text">Menu</h1>
<div class="assistive-text skip-link"><a href="#content" title="Skip to content">Skip to content</a></div>
<div class="menu-header-menu-container"><ul class="menu" id="menu-header-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115" id="menu-item-115"><a href="https://www.addairlawncare.com.au/contact/">Quote Request</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-71" id="menu-item-71"><a href="https://www.addairlawncare.com.au/gallery/">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39" id="menu-item-39"><a href="https://www.addairlawncare.com.au/home/">Home</a></li>
</ul></div> </nav>
</header><!-- #masthead .site-header -->
<div id="main">
<div class="site-content" id="primary">
<div id="content" role="main">
<div class="slider-wrapper theme-default">
<div class="nivoSlider" id="slider">
<img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/four1.jpg"/>
<img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/one1.jpg"/>
<img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/three1.jpg"/>
<img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/two1.jpg"/>
</div>
</div>
<div class="left-col">
<div class="gallery-block">
<div class="gallery-link"><a href="/gallery/"></a></div>
<ul class="thumbnails">
<li>
<a href="/gallery/"><img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/four1-150x150.jpg"/></a>
</li>
<li>
<a href="/gallery/"><img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/one1-150x150.jpg"/></a>
</li>
<li>
<a href="/gallery/"><img alt="" src="http://www.addairlawncare.com.au/wp-content/uploads/2012/06/three1-150x150.jpg"/></a>
</li>
</ul>
</div>
<div class="content-block about">
<div class="block-header about">
<h2>About Us</h2>
</div>
<div class="block-content about">
<p>Add Air Lawn Care is an SA based turf contracting business servicing metropolitan Adelaide &amp; the surrounding southern areas that was established in 1987. After 10 years in the landscaping industry &amp; also being a joint owner of a landscape depot, business founder James Goldfinch started Add Air Lawn Care to cater for the growing market of domestic turf installation in Adelaide, as well all aspects of lawn maintenance. We specialise in the preparation and installation of instant turf as well as irrigation systems &amp; also lawn rejuvenation.</p>
<p>The key to a healthy drought tolerant lawn is the preparation and at Add Air Lawn Care we believe that our preparation is second to none, and as no two yards are the same each job is quoted individually to ensure that the right steps are taken to give your new lawn the best possible start. We have a long standing affiliation with Paul Munns Instant Turf and have been a sub-contractor for them since 1987, so you know that the turf we install is from a quality supplier.</p>
</div>
<div class="free-quote-bar"><a href="/contact/"></a></div>
</div>
</div>
<div class="right-col">
<div class="content-block services">
<div class="block-header services">
<h2>Services</h2>
</div>
<div class="block-content services">
						With the exception of lawn mowing, Add Air Lawn Care provides all the services required to ensure you have a green lush lawn, year round.						<div id="accordion">
<h3>Irrigation</h3>
<div><p>Having the right irrigation system is a key factor in being able to maintain both the aesthetics and health of your lawn and garden. This is achieved through planning and the use of quality products.</p>
<p><strong><span style="text-decoration: underline;">Sub-Surface Drip</span></strong>- Without a doubt the most effective and efficient way to water your lawn, on average using 50% less water than sprinklers. We can install a completly maintenance free system with your new lawn &amp; with our Vibratory Plow we can also install Sub-Surface systems in existing lawns with very minimal turf damage.</p>
<p><strong><span style="text-decoration: underline;">Pop-Ups</span></strong>- Pop-up sprinkler systems have stood the test of time as a practical and cost effective method of watering. We only use the best quality products from trusted irrigation brands such as Toro and Hunter to ensure system longevity.</p>
<p><strong><span style="text-decoration: underline;">Gardens</span></strong>- With the use of either drippers or sprays, we can provide the right system to keep your garden looking its best.</p>
<p><strong><span style="text-decoration: underline;">Automatic or Manual</span></strong>- Wether you are after a fully automated irrigation system or a simple manual control we can install everything you need.</p>
</div>
<h3>Instant lawn</h3>
<div><p>Whether replacing a neglected lawn, installing your first homes new lawn, changing grasses or lawn shapes, we have the machinery, know- how and attention to detail to provide a quality and resilient lawn.</p>
</div>
<h3>Scarifying</h3>
<div><p>Scarifying is a process used to thin out and reduce the height of “spongy” lawns. This not only makes mowing easier but also promotes vigorous new growth and also helps in eradicating weeds.</p>
</div>
<h3>Coring</h3>
<div><p>Coring aerates the soil, allows better water penetration and reduces soil compaction.</p>
</div>
<h3>Topdressing</h3>
<div><p>Using good quality sandy loam, topdressing a lawn helps to fill in any undulations and also promotes growth, it has a very similar effect as fertilizing.</p>
</div>
<h3>Many more...</h3>
<div><p>Rotary Hoeing</p>
<p>Sodcutting</p>
<p>Fertilizing</p>
<p>Weed Control</p>
</div>
</div>
</div>
</div>
</div>
</div><!-- #content -->
</div><!-- #primary .site-content -->
</div><!-- #main -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info">
						© 2012 Add Air Lawn Care			<span class="sep"> | </span>
			ABN: 74 564 769 617			<span class="sep"> | </span>
			Tel: 0438848336			<span class="sep"> | </span>
			Email: info@addairlawncare.com.au			<span class="credit"><a href="http://design.acryliclab.com" rel="designer">AcrylicLab</a></span>
</div><!-- .site-info -->
</footer><!-- .site-footer .site-footer -->
</div><!-- #page .hfeed .site -->
<script src="https://www.addairlawncare.com.au/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.8.20" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.8.20" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-includes/js/jquery/ui/jquery.ui.accordion.min.js?ver=1.8.20" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.18" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/www.addairlawncare.com.au\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://www.addairlawncare.com.au/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.3.1" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/small-menu.js?ver=20120206" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="https://www.addairlawncare.com.au/wp-content/themes/AddAirLawnCare/js/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#slider').nivoSlider({
			effect: 'slideInLeft', // Specify sets like: 'fold,fade,sliceDown'
			animSpeed: 300, // Slide transition speed
			pauseTime: 4000, // How long each slide will show
			startSlide: 0, // Set starting Slide (0 index)
			directionNav: true, // Next & Prev navigation
			directionNavHide: false, // Only show on hover
			controlNav: false, // 1,2,3... navigation
		});
		
		jQuery('#accordion').accordion({
			autoHeight: false,
			navigation: true
		});

		jQuery("a.gallery-images").fancybox({
			'transitionIn'			:	'elastic',
			'transitionOut'			:	'elastic',
			'speedIn'				:	600, 
			'speedOut'				:	200, 
			'overlayShow'			:	true,
			'hideOnOverlayClick'	:	true,
			'titleShow'				: 	true,
			'titlePosition'			:	'over'
		});

	});
	
</script>
</body>
</html>