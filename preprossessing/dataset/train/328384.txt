<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="fr" http-equiv="content-language"/>
<meta content="classe decouverte" name="Author"/>
<meta content="classe decouverte" name="copyright"/>
<title>Classe découverte: L’information pour organiser un séjour ou une sortie scolaire </title>
<link href="/assets/css/top-accueil.min.css" rel="stylesheet"/>
</head>
<body>
<div class="container-fluid" id="container-fluid">
<!--  header  -->
<header>
<div class="container" id="header">
<div class="row top_line">
<div class="col-xs-12 col-sm-6 col-md-7 "><a href="/" id="logo">classe-decouverte.info</a><br/>L’information pour organiser un séjour ou une sortie scolaire </div>
<div class="col-xs-6 col-sm-6 col-md-5 top_line " id="header-right">
<span>
<a href="/inscription">
<svg class="icon"><use xlink:href="#plus"></use></svg>
<span>J'inscris mon centre</span>
</a>
</span>
<span>
<a href="/annuaire-des-classes-decouverte">
<svg class="icon"><use xlink:href="#annuaire"></use></svg>
<span>Annuaire</span>
</a>
</span>
<span>
<a class="contactez-nous" href="#">
<svg class="icon" id="face"><use xlink:href="#contact"></use></svg>
<span>Contact</span>
</a>
</span>
</div>
</div>
</div>
<!-- <div class="clearfix visible-xs-block"></div> -->
<!--  menu -->
<nav id="menu">
<div class="container">
<div class="col-md-4" id="hebergement_block">
<ul class="col-md-4" id="hebergement_blc">
<li class="col-md-1 col-xs-4 onglet" id="montagne_div">
<a href="/classe-de-montagne">
<svg class="icon"><use xlink:href="#montagne"></use></svg>
<span>Montagne</span>
</a>
</li>
<li class="col-md-1 col-xs-4 onglet" id="campagne_div">
<a href="/classe-de-campagne">
<svg class="icon"><use xlink:href="#campagne"></use></svg>
<span>Campagne et Environnement</span>
</a>
</li>
<li class="col-md-1 onglet" id="mer_div">
<a href="/classe-de-mer">
<svg class="icon"><use xlink:href="#mer"></use></svg>
<span>Mer et Monde Aquatique</span>
</a>
</li>
<li class="col-md-12" id="hebergement_libelle">HEBERGEMENT </li>
</ul>
</div>
<div class="col-md-5" id="activite_block">
<ul class="col-md-5" id="activite_blc">
<li class="col-md-1 onglet" id="sportLoisir_div">
<a href="/classe-de-sport-et-loisirs">
<svg class="icon"><use xlink:href="#sportLoisir"></use></svg>
<span class="height_center">Sport et Loisirs</span>
</a>
</li>
<li class="col-md-1 onglet" id="patrimoine_div">
<a href="/classe-de-patrimoine">
<svg class="icon"><use xlink:href="#patrimoine"></use></svg>
<span class="height_center">Patrimoine</span>
</a>
</li>
<li class="col-md-1 onglet" id="scienceTechnologie_div">
<a href="/classe-de-sciences-et-technologie">
<svg class="icon"><use xlink:href="#scienceTechnologie"></use></svg>
<span>Sciences et Technologies</span>
</a>
</li>
<li class="col-md-1 onglet" id="artSpectacle_div">
<a href="/classe-d-arts-et-spectacles">
<svg class="icon"><use xlink:href="#artSpectacle"></use></svg>
<span class="height_center">Arts et Spectacles</span>
</a>
</li>
<li class="col-md-12" id="activite_libelle">ACTIVITES </li>
</ul>
</div>
<div class="col-md-3" id="sejour_block">
<ul class="col-md-3" id="sejour_blc">
<li class="col-md-1 onglet" id="organisationSejour_div">
<a href="/classe-d-organisation-de-sejours">
<svg class="icon"><use xlink:href="#organisationSejour"></use></svg>
<span>Organisation de Séjours</span>
</a>
</li>
<li class="col-md-1 onglet" id="sejourLinguistique_div">
<a href="/sejours-linguistiques">
<svg class="icon"><use xlink:href="#sejourLinguistique"></use></svg>
<span>Séjours Linguistiques</span>
</a>
</li>
<li class="col-md-12" id="sejour_libelle">SEJOURS </li>
</ul>
</div>
</div>
</nav>
</header>
<!--  <div class="clearfix visible-xs-block"></div> -->
<!--<div class="clearfix visible-xs-block"></div> -->
<!-- slider -->
<div class="container-fluid" id="slider">
<div class="carousel slide" data-ride="carousel" id="myCarousel">
</div>
</div>
<section class="container espace-container">
<div class="row">
<div class="col-md-9" id="accueil-container">
<article class="row col-md-12">
<div class="carousel slide" data-ride="carousel" id="actuCarousel">
<div class="espace-actualite carousel-inner">
<div class="actu-rubrique-titre text-center">Les actualités des centres</div>
<div class="item active">
<a class="actu-carousel" href="/actualite/4760/4294">
<div class="img-actu">
<img src="/depot/photos_papier/4760_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre alpin « le Tétras Lyre » - Parc Naturel Régional du Vercors</div>
<div class="actu-texte">
                                                          Centre alpin « le Tétras Lyre » - Parc Naturel Régional du Vercors
 toutes nos offres www.balcon-est-vercors.com ;
Les Chiens de traineaux, conduite du traineaux, baptème pour les plus petits, visite du chenil, découvrez l'univers passionnant de la meute au coeur du Vercors ! + d'infos : 04 76 34 23 60 - contact@balcon-est-vercors.com 

 
                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/6455/4308">
<div class="img-actu">
<img src="/depot/diaporamas/6455_PF5.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Ferme Pédagogique Au Fer à Cheval - Gite de groupe en Occitanie</div>
<div class="actu-texte">
                                                          Envie d'organiser une classe de découverte à la campagne et à la ferme ? La ferme pédagogique dispose d'un agrément pour 2-3 classes (selon les effectifs) et de l'agrément PMI pour les maternelles. Rendez- vous sur notre site https://auferacheval.com/classe-decouverte/ pour découvrir notre catalogue ! 
A bientôt
Alexandra CARREL contact@auferacheval.com                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/10452/4302">
<div class="img-actu">
<img src="/depot/photos_papier/10452_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Association Marc et Montmija - Cap France</div>
<div class="actu-texte">
                                                          Pyrénées  Classes de neige, SKI, PROMOTIONS SCOLAIRES Séjour 5 jours; Eté nombreux thèmes : PATRIMOINE HISTOIRE ENVIRONNEMENT SCIENCES TECHNOLOGIE ARTS et SPECTACLES : Equitation, Spéléologie, Châteaux cathares, Grottes préhistoriques, Montagnes, Sources thermales, Pêche et Rivière Ecologie Energies renouvelables Barrages, Cirque, Théâtre...  : Contact Delphine Petitjean : 05 61 64 88 54                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/4822/4288">
<div class="img-actu">
<img src="/depot/photos_papier/4822_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Séjours Prémonval</div>
<div class="actu-texte">
                                                          FRANCHE COMTE -  séjour HAUT JURA, Station des Rousses, ORGANISATION DE SÉJOURS SKI à des Prix avantageux  CLASSES ENVIRONNEMENT CLASSES NEIGE  PROFITER D'UN DES HAUTS SITES DONT LA PURETÉ EST PRÉSERVÉE . Franck Perraud  03 84 60 78 07                             
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/4760/4300">
<div class="img-actu">
<img src="/depot/photos_papier/4760_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre alpin « le Tétras Lyre » - Parc Naturel Régional du Vercors</div>
<div class="actu-texte">
                                                          Centre alpin « le Tétras Lyre » - Parc Naturel Régional du Vercors
 toutes nos offres www.balcon-est-vercors.com ;
Séjours à ski de fond ou ski alpin, Chiens de traineaux, conduite du traineaux, baptème pour les plus petits, visite du chenil, découvrez l'univers passionnant de la meute au coeur du Vercors ! + d'infos : 04 76 34 23 60 - contact@balcon-est-vercors.com ;                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/6360/4296">
<div class="img-actu">
<img src="/depot/photos/6360_PF.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre de Vacances - Château de Trilbardou en Seine et Marne</div>
<div class="actu-texte">
                                                          Château 18ème grand Parc 10 mn Disney, 40 mn Paris 
Groupes scolaire tous niveaux, Associations et clubs sportifs. En pension complète à 41€; Activités centres équestre, ferme de Cueillette de Rutel... kayak, équitation…proche de Paris et tous ces centres d’intérêt  - Capacité d’accueil : Jusqu’à  76 lits sur nos 5 gites identiques,  chaque gite est équipé d’une grande  salle de classes/activités, sanitaires et 6 Chambres de 1 à 3 lits. Contact : Monsieur ABDOUS 06 11 78 25 67 
                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/12420/4305">
<div class="img-actu">
<img src="/depot/photos_papier/12420_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre Vendéole - CGCV de Longeville-sur-Mer</div>
<div class="actu-texte">
                                                          Activités NAUTIQUES ET SPORTIVES- HISTOIRE ET PATRIMOINE-CULTUREL-PARCS éducatifs et de loisirs -EDUCATION à l'ENVIRONNEMENT-
-Terrains multisports- Préau- Salle de spectacle-

N’hésitez pas à contacter Chloé au 06 82 07 82 44 ou par mail : chloé@cgcv.org  -  http://www.centre-vendeole.fr/ 

                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/10452/4297">
<div class="img-actu">
<img src="/depot/photos_papier/10452_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Association Marc et Montmija - Cap France</div>
<div class="actu-texte">
                                                          PYRENEES Lot Ariège TARIF PROMOTION SEJOURS CLASSES NEIGE SKI SPORTS ENVIRONNEMENT Ateliers PEDAGOGIQUES  Equitation, Spéléologie, Châteaux cathares, Grottes préhistoriques, Montagnes, Sources thermales, Pêche et Rivière Ecologie Energies renouvelables Barrages, Cirque, Théâtre... Réservations : Delphine Petitjean au 05 61 64 88 54                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/12391/4291">
<div class="img-actu">
<img src="/depot/photos_papier/12391_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">DIDRICK Vacances, spécialiste du séjour de pleine nature</div>
<div class="actu-texte">
                                                          séjours Sport et Nature : Canoë / Kayak, Accrobranche, Via ferrata, Spéléologie, Course d’orientation, Sports mécaniques Quad enfant / ados; Apprivoiser la nuit, Traces et indices Faune et flore Géologie-volcanisme, Land art, Teintures végétales, Musique verte, Vannerie Architecture et patrimoine...  Cédric au 04 66 32 97 10 - contact@didrick-vacances.com ;                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/4822/4290">
<div class="img-actu">
<img src="/depot/photos_papier/4822_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Séjours Prémonval</div>
<div class="actu-texte">
                                                          Station des Rousses : Ski Classes de neige a prix avantageux - Parc Naturel Régional du Haut-Jura - Hiver ou Printemps : ateliers Neige, Faune, Flore, Paysages, Eau, Bois, développement durable... en route pour l'aventure Contact : Monsieur Franck Perraud  03 84 60 78 07

                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/6455/4307">
<div class="img-actu">
<img src="/depot/diaporamas/6455_PF5.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Ferme Pédagogique Au Fer à Cheval - Gite de groupe en Occitanie</div>
<div class="actu-texte">
                                                          Pour profiter des monts d'Alban sous la neige, s'occuper des animaux, faire des balades dans le givre mais aussi dîner au coin du feu, venez nous retrouver pour les vacances d'hiver 2021 en séjour à la ferme ! Séjour du 15/02 au 19/02/2021 pour les 7-12 ans : les petits fermiers cavaliers. N'hésitez pas à nous contacter : 
Ferme pédagogique Au fer A Cheval
contact@auferacheval.com
05 63 55 84 57                             
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/12420/4306">
<div class="img-actu">
<img src="/depot/photos_papier/12420_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre Vendéole - CGCV de Longeville-sur-Mer</div>
<div class="actu-texte">
                                                          LA FORMULE
 4 nuitées en pension complète pour vos séjours scolaires!!

Envie d’en savoir plus ?

N’hésitez pas à contacter Chloé au 06 82 07 82 44 ou
 par mail : chloé@cgcv.org



                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/12420/4304">
<div class="img-actu">
<img src="/depot/photos_papier/12420_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre Vendéole - CGCV de Longeville-sur-Mer</div>
<div class="actu-texte">
                                                           Environnement privilégié, entre mer et marais poitevin, pinède et dune, classé Natura 2000, en fait un lieu d’une richesse exceptionnelle. A pied, la plage n’est qu’à 20 mn par un sentier côtier. Toutes les activités ont lieu dans un périmètre inférieur à 10 km. La structure d’accueil est au cœur d’un parc de 12 hectares. Ce qui en fait un lieu idéal pour résider lors de votre séjour en Vendée. 
Contact : La directrice Chloé SUTEAU 06 82 07 82 44.                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/6455/4301">
<div class="img-actu">
<img src="/depot/diaporamas/6455_PF5.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Ferme Pédagogique Au Fer à Cheval - Gite de groupe en Occitanie</div>
<div class="actu-texte">
                                                          MIDI PYRÉNÉES tarifs SEJOURS SCOLAIRES  FERME PEDAGOGIQUE  AGRICULTURE BIOLOGIQUE JARDIN, PONEY ELEVAGE TRACES ANIMAUX, élevage équin, au rythme des SAISONS  Sylvain &amp; Alexandra au 05 63 55 84 57 www.auferacheval.com                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/9562/4293">
<div class="img-actu">
<img src="/depot/photos/9562_2015_PF.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Chalet Annapurna</div>
<div class="actu-texte">
                                                          PROMO 2021-22 NEIGE ET SOLEIL Semaines Hébergement au pied des pistes. Navette gratuite.
PACK SCOLAIRES : 5 nuits en pension complète + 5 jours de ski ( Ski Pass + Location)  

N’HÉSITEZ PAS A NOUS CONTACTER POUR UN DEVIS : Chalet Annapurna 04 76 79 21 14 contact@annapurna2alpes.fr                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/3209/4295">
<div class="img-actu">
<img src="/depot/photos_papier/3209_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Forteresse souterraine du Château de Brézé - Saumur Maine et Loire</div>
<div class="actu-texte">
                                                          Château bâti du XIe au XIXe siècles prés de Saumur. HISTOIRE et PATRIMOINE : Réseau de Souterrains, galeries taillées. ATELIERS pédagogiques SCOLAIRES : Pain - Héraldique - Taille de Pierre - Parcours d'Enigmes - Peinture sur soie - SPECTACLES CHEVALIERS contact : 02.41.51.60.15
                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/6455/4292">
<div class="img-actu">
<img src="/depot/diaporamas/6455_PF5.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Ferme Pédagogique Au Fer à Cheval - Gite de groupe en Occitanie</div>
<div class="actu-texte">
                                                          MIDI PYRÉNÉES - FERME PÉDAGOGIQUE ACCUEIL DE GROUPE SCOLAIRE. CLASSES TRANSPLANTEES balades PONEY, ateliers et séjours, CHEVAL, POULAINS, traces d'animaux, élevage équin, sentier des sens, au rythme des saisons …Appelez-nous : 05 63 55 84 57 : Sylvain &amp; Alexandra vous accueillent; www.auferacheval.com

                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/9562/4303">
<div class="img-actu">
<img src="/depot/photos/9562_2015_PF.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Chalet Annapurna</div>
<div class="actu-texte">
                                                          ENCORE QUELQUES DISPOS!!! 

36,20 € LA PENSION COMPLÈTE

SKIEZ JUSQU’À FIN AVRIL!!!

NEIGE ET SOLEIL GARANTIS!!!!

FORMULE PACKAGEE PC + FORFAIT + MATÉRIEL DE SKI

ORGANISATION CLÉ EN MAIN DE VOTRE SÉJOUR                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/4588/4298">
<div class="img-actu">
<img src="/depot/photos_papier/4588_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre Mas de Bruyère - Vacances Evasion en Hérault</div>
<div class="actu-texte">
                                                          pension complète 3 repas et goûter Doc mat pédago 1 animateur par classe, gratuité Enseignants Ass RC; Tout compris : a partir de 35€/Jour; semaine 5 jours 175 € / Enfant... au pied des montagnes Cévennes entre Forêt et Garrigue 40 km cirque Navacelles, Sports Faune et Flore Randonnée Orientation, visites Apiculteur, Elevages... Contact : Adrien ROBICHON 04 67 73 36 96                            
                            </div>
</div>
</a>
</div>
<div class="item ">
<a class="actu-carousel" href="/actualite/4760/4299">
<div class="img-actu">
<img src="/depot/photos_papier/4760_2020_PP.jpg"/>
</div>
<div class="actu-rubrique-titre text-center">Les actualité des centres</div>
<div class="pull-right blc-actu">
<div class="actu-titre">Centre alpin « le Tétras Lyre » - Parc Naturel Régional du Vercors</div>
<div class="actu-texte">
                                                          Séjours à ski de fond ou ski alpin, toutes nos offres www.balcon-est-vercors.com ; Les Chiens de traineaux reviennent dans nos montagnes !!! conduite du traineaux, visite du chenil, baptème pour les plus petits, découvrez l'univers passionnant de la meute au coeur du Vercors ! et infos : 04 76 34 23 60 - contact@balcon-est-vercors.com ;                            
                            </div>
</div>
</a>
</div>
<!-- Carousel controls -->
<a class="carousel-control left" data-slide="prev" href="#actuCarousel" id="actu-prev">
<span>
<svg><use xlink:href="#actu-left-arrow"></use></svg>
</span>
</a>
<a class="carousel-control right" data-slide="next" href="#actuCarousel" id="actu-next">
<span>
<svg><use xlink:href="#actu-right-arrow"></use></svg>
</span>
</a>
</div>
</div>
</article>
<!-- fin actu -->
<div class="row col-md-12" id="espace-divers">
<div class="col-md-3 block-espace-divers" id="fiche-pratique">
<span class="center-block center-vertical">
<a class="center-block center-vertical" href="/fiches-pratique.html">Fiches Pratiques</a>
</span>
</div>
<div class="col-md-3 block-espace-divers" id="espace">
<div class="center-block center-vertical">
<a class="center-block" href="">
<span class="center-block center-vertical">
<svg><use xlink:href="#espace-centre-icon"></use></svg>
<span>Espace centres</span>
</span>
</a>
<a class="center-block lien" href="/inscription"><span class="center-block center-vertical">Inscrire mon centre</span></a>
<a class="center-block lien" href="/extranet.php/actualites/login"><span class="center-block center-vertical">Saisir mes actualités</span></a>
</div>
</div>
<div class="col-md-3 block-espace-divers" id="guide">
<span class="center-block center-vertical">
<a class="center-block center-vertical" href="http://www.classe-decouverte.info/guide/guide-classe-decouverte.html" target="_blank">Consulter le guide</a>
</span>
</div>
</div>
</div>
<div class="col-md-3" id="pub">
<div class="carousel slide" data-ride="carousel" id="pubCarousel">
<div class="espace-actualite carousel-inner" id="innerpub">
</div>
</div>
</div>
</div>
<div class="container" id="espace-famille">
<div class="col-md-3 content-g">
              Sorties <br/>
<strong>scolaires, familles</strong><br/>
              et <strong>individuels</strong>
</div>
<div class="col-md-9 content-d">
<p>Le site <strong>classe-decouverte.info</strong> est destiné à aider les professeurs et accompagnateurs pour organiser un séjour ou une sortie scolaire. Cependant certains des centres que nous décrivons sur le site, accueillent également des familles, des individuels et des groupes <br/><br/>
          Vous pouvez les repérer sur leur fiche descriptive ou sur les listes-réponses des différentes rubriques (mer, montagne, campagne,...) grâce au logo "Accueil des familles et individuels" qui s'affiche. Vous pourrez même filtrer les listes-réponses en ne sélectionnant que les centres qui proposent cette option.
          </p>
</div>
</div>
<!-- espace rubrique -->
<div class="container" id="espace-rubrique">
<div class="row row-margin">
<article class="col-md-4 blc-montagne">
<a class="rubrique-link" href="/classe-de-montagne/na"><h1>Montagne</h1></a>
<p class="center-bottom">
                   Classe de montagne, classe de neige, classe de ski, la France est riche en massifs
                   montagneux pour organiser des classes découvertes en toute saison et découvrir une
                   nature grandiose.  La géographie, les sciences, la culture s’associent  au sport pour offrir un
                    séjour scolaire inoubliable aux élèves et… à leurs accompagnateurs.
                </p>
</article>
<article class="col-md-4 blc-campagne">
<a class="rubrique-link" href="/classe-de-campagne/na"><h1>Campagne et environment</h1></a>
<p class="center-bottom">
                 Classe verte, classe de campagne, éducation à l’environnement, autant de thématiques pour sensibiliser
                  les élèves aux sciences de la nature, à l’agriculture et à l’économie durable. Éducation mais aussi
                   plaisir de pratiquer collectivement des activités de plein air comme l’équitation, la randonnée ou les sports aquatiques.
               </p>
</article>
<article class="col-md-4 blc-mer">
<a class="rubrique-link" href="/classe-de-mer/na"><h1>Mer et Monde Aquatique</h1></a>
<p class="center-bottom">
                   Classe de mer et découverte du monde aquatique trouve toute leur expression
                   sur le littoral français, le plus long d’Europe. Flore, faune, activités économiques
                    maritimes comme la pêche, la construction navale peuvent être au cœur de ces séjours
                     pédagogiques, couplés, bien entendu, à la pratique en équipe d’activités sportives comme la voile ou la natation.
                </p>
</article>
</div>
<div class="row row-margin">
<article class="col-md-4 blc-sport">
<a class="rubrique-link" href="/classe-de-sport-et-loisirs/na"><h1>Sport et Loisir</h1></a>
<p class="center-bottom">
                  Classes de sport et loisir sont idéales pour une pratique sportive intensive et forger l’esprit d’équipe d’un groupe.
                   Elles peuvent être centrées sur un sport ou proposer une diversité d’activités avec par exemple une activité
                   pédagogique de sensibilisation à la Santé et à la prévention ou à l’élaboration d’une cuisine équilibrée et saine.
                  </p>
</article>
<article class="col-md-4 blc-patrimoine">
<a class="rubrique-link" href="/classe-de-patrimoine/na"><h1>Patrimoine</h1></a>
<p class="center-bottom">
                  Classe de patrimoine, classe découverte de châteaux et de musées, la France offre une incroyable richesse
                   et diversité pour les organiser. Des séjours qui permettront de découvrir l’histoire là où elle s’est faite,
                    d’en découvrir les objets et les lieux qui la gravent dans nos mémoires. Ce sont aussi des voyages inoubliables dans notre patrimoine artistique et architectural.
                </p>
</article>
<article class="col-md-4 blc-science">
<a class="rubrique-link" href="/classe-de-sciences-et-technologie/na"><h1>Sciences et  Technologies</h1></a>
<p class="center-bottom">
                  Classe de sciences et de technologie offre la possibilité d’expérimenter les lois physiques et chimiques,
                   de découvrir les mathématiques de manière ludique, de comprendre le fonctionnement des objets de haute
                    technologie qui nous entourent. Comment mieux faire la synthèse entre les sciences et leurs applications pratiques et quotidiennes ?
                </p>
</article>
</div>
<div class="row">
<article class="col-md-4 blc-art">
<a class="rubrique-link" href="/classe-d-arts-et-spectacles"><h1>Arts et Spéctacles</h1></a>
<p class="center-bottom">
                  Classe découverte musique, classe découverte théâtre, classe découverte cirque, cinéma, danse, audiovisuel,
                  tous les arts sont des sujets passionnants pour un séjour scolaire. Les élèves seront sensibilisés à leur pratique,
                   à leur histoire, à leurs œuvres les plus représentatives, pour les éveiller à la culture et développer leur sensibilité artistique.
                </p>
</article>
<article class="col-md-4 blc-organisation">
<a class="rubrique-link" href="/classe-d-organisation-de-sejours/na"><h1>Organisation de Séjours</h1></a>
<p class="center-bottom">
                  L’organisation de séjours et de voyages scolaires est la spécialité d’organismes spécialisés qui
                   vous proposeront des programmes adaptées à vos demandes et vous soulageront de l’organisation
                   pratique de votre séjour scolaire. Une aide logistique sur place vous permettra aussi de consacrer toute votre attention à votre groupe d’élèves.
                </p>
</article>
<article class="col-md-4 blc-sejour">
<a class="rubrique-link" href="/classe-de-sejour-linguistique"><h1>Séjours Linguistiques</h1></a>
<p class="center-bottom">
                  Séjours linguistiques et voyages linguistiques  sont sans conteste les moyens d’apprentissage des
                   langues étrangères les plus efficaces. Ils marquent les mémoires, proposent un contact direct avec
                   la culture du pays dont on souhaite apprendre la langue. Les échanges avec les habitants locaux sont bien sûr les exercices pratiques les plus enrichissants et les plus captivants
                </p>
</article>
</div>
</div>
</section>
<!-- footer -->
<footer class="container-fluid" id="footer">
<div class="container">
<div class="row">
<div class="col-md-3 footer-block1">
<a class="sous-titre" href="/presentation.html">QUI SOMMES NOUS</a>
<br/>
<span>
          Nous vous proposons une sélection de plus de 2500
          Centres d'hébergement pour vos classes de
          découverte et de 1800 établissements pour vos sorties
          culturelles qui ont spécialement développé des
          programmes pour les groupes scolaires.
        </span>
</div>
<div class="col-md-3 footer-block2">
<span class="sous-titre">NOS SECTIONS</span>
<p>
<a href="/classe-de-montagne">Montagne</a><br/>
<a href="/classe-de-campagne">Campagne environnement</a><br/>
<a href="/classe-de-mer">Mer et sport nautique</a><br/>
<a href="/classe-de-sport-et-loisirs">Sport et Loisir</a><br/>
<a href="/classe-de-patrimoine">Patrimoine</a>
</p>
</div>
<div class="col-md-3 footer-block3">
<span></span>
<p><br/>
<a href="/classe-de-sciences-et-technologie">Science et technologie</a><br/>
<a href="/classe-d-arts-et-spectacles">Arts et spectacles</a><br/>
<a href="/classe-d-organisation-de-sejours">Organisation de séjours</a> <br/>
<a href="/sejours-linguistiques">Séjours linguistiques</a><br/>
</p>
</div>
<div class="col-md-3 footer-block4">
<p>
<a href="/">Accueil</a><br/>
<a href="/fiches-pratique.html">Fiches pratique</a><br/>
<a href="/annuaire-des-classes-decouverte">plan de site</a><br/>
<a class="contactez-nous" href="#">Contact</a> <br/>
<span href="/mentions-legales.html" id="mentionlegale">Mentions légales</span><br/>
<span href="/partenaire.html" id="partenaire">partenaires</span>
</p>
</div>
</div>
</div>
</footer>
<div class="container" id="subfouter">
<div class="col-md-6 text-left center-vertical"><span>COPYRIGHT ONPC 2017</span></div>
<div class="col-md-6 text-right center-vertical">
<a href="">
<span>Retrouvez-nous sur </span>
<svg><use xlink:href="#facebook"></use></svg>
</a>
</div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-61134019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61134019-1');
</script>
</div>
<script defer="" src="/assets/js/bottom-accueil.min.js"></script>
</body>
</html>
