<!DOCTYPE html>
<html class="no-js" lang="">
<head><script type="text/javascript">var configNodeName= "config.publish.prod";</script>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>Page not found</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="" name="tags"/>
<meta content="" name="keywords"/>
<meta content="NOINDEX, NOFOLLOW" name="ROBOTS"/>
<link href="//connect.facebook.com/" rel="dns-prefetch"/>
<link href="//visit.asb.co.nz/" rel="dns-prefetch"/>
<link href="//content.asb.co.nz/" rel="dns-prefetch"/>
<link href="//www.google-analytics.com/" rel="dns-prefetch"/>
<link href="//api.asb.co.nz/" rel="dns-prefetch"/>
<meta content="ProductsServices" name="collectionName"/>
<meta content="Page not found" property="og:title"/>
<meta content="Page not found" name="twitter:title"/>
<meta content="" name="description"/>
<meta content="" property="og:description"/>
<meta content="" name="twitter:description"/>
<meta content="https://www.asb.co.nz/page/error.html" property="og:url"/>
<link href="https://www.asb.co.nz/page/error.html" rel="canonical"/>
<meta content="https://www.asb.co.nz/page/error.html" name="twitter:url"/>
<meta content="article" property="og:type"/>
<meta content="en_NZ" property="og:locale"/>
<meta content="ASB" name="sasiaSiteName"/>
<meta content="ASB Bank" property="og:site_name"/>
<meta content="@ASBBank" name="twitter:site"/>
<meta content="https://www.asb.co.nz/etc/designs/asb/common-blade/images/welcome-confetti-ladies.jpg" property="og:image"/>
<meta content="https://www.asb.co.nz/etc/designs/asb/common-blade/images/welcome-confetti-ladies.jpg" name="twitter:image"/>
<meta content="summary_large_image" name="twitter:card"/>
<link href="//connect.facebook.com/" rel="dns-prefetch"/>
<link href="//visit.asb.co.nz/" rel="dns-prefetch"/>
<link href="//content.asb.co.nz/" rel="dns-prefetch"/>
<link href="//www.google-analytics.com/" rel="dns-prefetch"/>
<link href="//api.asb.co.nz/" rel="dns-prefetch"/>
<!-- Favicon -->
<link href="/favicon.ico" rel="icon" type="image/vnd.microsoft.icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/favicon-32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-48.png" rel="icon" sizes="48x48" type="image/png"/>
<link href="/favicon-96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/favicon-144.png" rel="icon" sizes="144x144" type="image/png"/><script async="" src="/content/dam/asb/analytics/jquery.js" type="text/javascript"></script>
<link href="/etc/designs/asb/common-blade/clientlibrary/appstyle-common-blade.min.2020102193.css" rel="stylesheet" type="text/css"/>
<script src="/etc/designs/asb/common-blade/js/datalayer-script.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
    var digitalData = {};
	digitalData.enable = true;
    digitalData.calculator = {};
	digitalData.error = {};
	digitalData.form = {};
    digitalData.page = {"url":"asb","brand":"asb","platform":"asb","type":"product-page"};
	digitalData.product = {};
	digitalData.user = {"cif":""};
	digitalData.search = {};
	digitalData.interaction = {};
    digitalData.tool = {};
    dl_onPageLoad();
    digitalData['page'].id = 'id2016n2714';
    var isErrorPage = "true";
    if (isErrorPage == 'true') {
		digitalData.error['description'] = "404";
		digitalData.error['message'] = "page not found";
		digitalData.error['type'] = "technical";
		digitalData.page.name = "asb:404";
		digitalData.page.type= "error-page";
		_satellite.track("asb-site-navigate");
    }
</script>
<script src="/analytics/launch-ENd904dafd87714b818b5ba05f2ac69cfd.min.js"></script>
<script type="text/javascript"> 

window.apiKeys = {
     cobButtonApiURL: "https://api.asb.co.nz/v1/customer-onboarding"
  };

</script>
<!-- Apple touch icons, used on home screen of iOS -->
<link href="/etc/designs/asb/help/clientlibs/style/images/touch-icon.png" rel="apple-touch-icon"/>
<link href="/etc/designs/asb/help/clientlibs/style/images/touch-icon-72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/etc/designs/asb/help/clientlibs/style/images/touch-icon-114.png" rel="apple-touch-icon" sizes="114x114"/>
</head><body>
<!--googleoff: all-->
<div data-svg-path="/content/asb/homepage/en/homepage.svgicons.html" id="svg-data">
<svg height="0" style="position:absolute" width="0">
<symbol id="icon-notification-info" viewbox="43 117 41 41"><g fill="none" fill-rule="evenodd" transform="translate(43 117)"><path d="M20.5 40.5c11.046 0 20-8.954 20-20s-8.954-20-20-20-20 8.954-20 20 8.954 20 20 20z" fill="#0064AC"></path><text fill="#FFF" font-family="Calibri" font-size="30"><tspan x="17.058" y="29">i</tspan></text></g></symbol>
<symbol id="icon-asb-logo-colour" viewbox="15 24 63 14"><path d="M30.71 28.746h-7.257c-.32 0-.59.214-.59.57v3.3c0 .308.222.467.498.32 1.714-.914 7.27-3.868 7.376-3.922.205-.104.287-.268-.026-.268zm5.16 9.213h-4.313l-1.372-2.47H21.23l-1.293 2.47h-4.34c-.264 0-.5-.174-.328-.485 1.333-2.39 7.184-12.864 7.24-12.963.17-.292.276-.512.78-.512h4.77c.533 0 .613.22.785.516.05.084 5.988 10.556 7.35 12.956.177.31-.058.487-.326.487zm22.957-.526V24.53a.54.54 0 0 1 .546-.53h12.122c3.51 0 5.212.806 5.212 3.645 0 2.06-.713 2.686-1.74 3.047-.216.07-.198.234 0 .27 1.637.29 2.254 1.108 2.254 3.156 0 3.055-1.86 3.84-4.376 3.84H59.37a.534.534 0 0 1-.543-.523zm4.95-10.468v2.213l6.7-.003c.814 0 1.287-.32 1.287-1.107 0-.806-.473-1.103-1.286-1.103h-6.7zm0 5.44v2.42h6.8c.95 0 1.586-.37 1.586-1.176 0-.94-.635-1.243-1.586-1.243h-6.8zM15 24.573v7.032c0 .303.167.225.274.026.056-.1 3.103-5.486 4.047-7.146.153-.266-.012-.482-.33-.482h-3.403a.556.556 0 0 0-.586.573zm28.996 4.517h7.475c2.405 0 4.35 1.56 4.35 3.48v1.907c0 1.922-1.945 3.482-4.35 3.482h-8.733c-3.74 0-5.17-1.85-5.266-3.708-.002-.173-.06-.57.584-.57h4.372c.012.79.644 1.145 1.588 1.145h5.26c.952 0 1.585-.36 1.585-1.168 0-.94-.634-1.163-1.587-1.163h-6.64c-3.73 0-5.08-1.908-5.08-3.76v-1.383C37.553 25.5 39.43 24 41.745 24h9.304c2.293 0 4.157 1.476 4.19 3.306 0 .234.08.573-.59.573h-4.44a1.668 1.668 0 0 0-.01-.105l-.01-.052c-.107-.572-.56-.757-1.26-.757h-4.934c-.813 0-1.29.243-1.29 1.052 0 .785.477 1.07 1.29 1.07z" fill="currentColor" fill-rule="evenodd"></path></symbol>
<symbol id="icon-arrow-down" viewbox="192 26 14 9"><path d="M204.89 27l-5.938 6L193 27.067" fill="none" stroke="currentColor" stroke-width="1.5"></path></symbol>
<symbol id="icon-login-lock" viewbox="934 621 10 14"><path d="M939.5 631.428a1.75 1.75 0 1 0-1 0V633h1v-1.572zM935.626 627h-.63c-.54 0-.996.446-.996.997v6.006a1 1 0 0 0 .995.997h8.01c.54 0 .995-.446.995-.997v-6.006a1 1 0 0 0-.995-.997h-.463v-2.134c0-1.81-1.555-3.28-3.435-3.28-1.88 0-3.48 1.47-3.48 3.28V627zm3.374-4c-1.134 0-2 .886-2 1.978V627h4v-2.022a1.97 1.97 0 0 0-2-1.978z" fill="#000" fill-rule="evenodd"></path></symbol>
<symbol id="icon-search-header" viewbox="-1 1 17 17"><g fill="none" fill-rule="evenodd" stroke="#FFF" stroke-width="2" transform="translate(0 2)"><circle cx="6.5" cy="6.5" r="6.5"></circle><path d="M14.5 14.5l-3-3" stroke-linecap="square"></path></g></symbol>
</svg>
</div>
<script src="/etc/designs/asb/common-blade/js/svg-icons.js?v=2020102193"></script>
<noscript>
<div class="component component--notification is-active no-js">
<div class="notification-wrapper">
<div class="notification-content-wrapper">
<div class="notification-content show-notification">
<div class="notification-icon">
<svg class="icon"><use xlink:href="#icon-notification-info" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
</div>
<h5>The ASB website functions best with JavaScript enabled.</h5>
</div>
</div>
</div>
</div>
</noscript>
<!--googleon: all-->
<div class="component component--notification " data-component="notification">
<div class="notification-wrapper">
<div class="notification-content-wrapper" data-hook="notification-wrapper" data-notifications-link="/content/asb/notifications/en/notifications/notifications-config.announcements.html"></div>
<div class="notification-close">
<a class="notification-close-link" data-hook="notification-close-link" href="#">
<svg class="icon"><use xlink:href="#icon-notification-close"></use></svg>
</a>
</div>
</div>
</div>
<div class="outline" data-hook="outline" id="outline">
<div class="cq-dd-paragraph" style="display:inline;"><header class="outline-header outline-header-full-width" data-region="header">
<div class="header header-full-width"><!--
--><div class="main-menu-icon-container">
<span class="main-menu-icon-wrapper">
<a class="main-menu-icon" href="#"></a>
</span>
<span class="menu-label">Menu</span>
</div><!--
--><div class="header-logo logo" title="ASB Bank">
<a class="icon icon-asb-logo" href="https://www.asb.co.nz?fm=header:asb" target="_self">
<svg><use xlink:href="#icon-asb-logo-colour"></use></svg><div class="header-logo-overlay"></div></a>
</div><!--
--><div class="right-section">
<div class="search-link-container">
<a class="search-link" href="#">
<svg class="icon icon-search-header"><use xlink:href="#icon-search-header"></use></svg>
<span class="search-link-label">Search</span>
</a>
</div>
<div class="login-container">
<div class="component component-login" data-component="login" data-tracking-sitecat="true">
<div class="environment-selector" data-hook="environment-selector">
<span class="chosen-environment" data-hook="chosen-environment"></span>
<span class="down-arrow"><svg class="icon"><use xlink:href="#icon-arrow-down"></use></svg></span>
<div class="environment-options" data-hook="environment-options">
<div class="environment-option" data-id="fnc" data-type="link" data-url="https://online.asb.co.nz/auth/?fm=header:login">FastNet Classic</div>
<div class="environment-option" data-id="fnb" data-type="inline" data-url="https://fnb.asb.co.nz/SignOn.aspx?fm=header:login">FastNet Business</div>
<div class="environment-option" data-id="ost" data-type="link" data-url="https://online.asb.co.nz/auth/authn?al=&amp;goto=https://online.asb.co.nz/ost/&amp;fm=header:login">Online Share Trading</div>
<svg class="icon"><use xlink:href="#icon-arrow-up"></use></svg>
</div>
</div>
<a class="btn login-button" data-hook="login-button" href="#">
				Log In<svg class="icon icon-desktop"><use xlink:href="#icon-login-lock"></use></svg><svg class="icon icon-mobile collapsed"><use xlink:href="#icon-arrow-down"></use></svg><svg class="icon icon-mobile expanded"><use xlink:href="#icon-arrow-up"></use></svg>
</a>
<div class="inline-login-form">
<div class="header-container header-container-desktop">
<div class="heading">FastNet Business</div>
<div class="close-container">
<div class="operator operator--close" data-hook="close"></div>
</div>
</div>
<div class="header-container header-container-mobile">
<a class="operator--close" href="#">
<svg class="icon"><use xlink:href="#icon-homepage-arrow-left"></use></svg>
<div class="heading">Back</div>
</a>
</div>
<div class="form-container">
<form action="https://fnb.asb.co.nz/SignOn.aspx?fm=header:login" class="login-form" data-hook="inline-login-form" method="post">
<input id="__LASTFOCUS" name="__LASTFOCUS" type="hidden" value=""/>
<input id="__EVENTTARGET" name="__EVENTTARGET" type="hidden" value=""/>
<input id="__EVENTARGUMENT" name="__EVENTARGUMENT" type="hidden" value=""/>
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="/wEPDwULLTE4NDIxMDEzNzRkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBQxTaWduT25CdXR0b27JvgBU8Q0nJZCraeGTSFttJKiogg=="/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="/wEdAAUgMUtc4XMDL3b5gtH4i1TH5mQH2xqSDUA3DFWQkZ0ljw5ZuFYkUo/CDpjPr83x5n12NvjHOkq5wKoqN6Aim8WGM5X+RMXNCVV4LKYbrWgE+wvVI9HMQfz72dHzsjDM6afriKiA"/>
<input name="SignOnButton.x" type="hidden" value="0"/>
<input name="SignOnButton.y" type="hidden" value="0"/>
<fieldset>
<input autocomplete="off" class="" data-hook="client-ID" id="fnbIDHeader" maxlength="6" name="txtSiteNumber" required="" type="text"/>
<label for="fnbIDHeader">Enter your client ID</label>
</fieldset>
<fieldset>
<input autocomplete="off" class="" id="fnbUserHeader" maxlength="8" name="txtUserId" required="" type="text"/>
<label for="fnbUserHeader">Enter your user ID</label>
</fieldset>
<fieldset>
<input autocomplete="off" class="" id="fnbPasswordHeader" maxlength="10" name="txtPassword" required="" type="password"/>
<label for="fnbPasswordHeader">Enter your password</label>
</fieldset>
<a class="btn login-button inline-login-form-submit" data-hook="inline-login-form-submit" href="#">
							Log In<svg class="icon"><use xlink:href="#icon-login-lock" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>
</a>
<div class="login-form-links">
<a class="login-form-link" href="https://www.asb.co.nz/help/trouble-logging-in-to-fastnet-business.html">Forgotten Password</a>
<a class="login-form-link" href="https://www.asb.co.nz/security">About Security</a>
</div>
</form>
</div>
</div>
</div>
<div class="login-mobile-menu">
<div class="login-mobile-menu-links">
<div class="link">
<a data-id="mfnc" href="https://online.asb.co.nz/mb?fm=header:login">
								FastNet Classic Mobile<span class="arrow-container"><svg class="arrow"><use xlink:href="#icon-homepage-arrow-right"></use></svg></span>
</a>
</div>
<div class="link" data-type="link">
<a data-id="fnc" href="https://online.asb.co.nz/auth/?fm=header:login">
									FastNet Classic<span class="arrow-container"><svg class="arrow"><use xlink:href="#icon-homepage-arrow-right"></use></svg></span>
</a>
</div>
<div class="link" data-type="inline">
<a href="https://fnb.asb.co.nz/SignOn.aspx?fm=header:login">
									FastNet Business<span class="arrow-container"><svg class="arrow"><use xlink:href="#icon-homepage-arrow-right"></use></svg></span>
</a>
</div>
<div class="link" data-type="link">
<a data-id="ost" href="https://online.asb.co.nz/auth/authn?al=&amp;goto=https://online.asb.co.nz/ost/&amp;fm=header:login">
									Online Share Trading<span class="arrow-container"><svg class="arrow"><use xlink:href="#icon-homepage-arrow-right"></use></svg></span>
</a>
</div>
</div>
</div>
</div>
</div><!--
--></div>
</header>
<div class="main-menu-overlay" data-component="menu-overlay" data-tracking-sitecat="true">
<div class="container main-menu-container">
<div class="menu-item menu-item-top-level has-children">
<a href="#">Accounts and Cards</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/bank-accounts/joining-asb.html?fm=header:menu:join_asb_top">Join ASB</a>
</div>
<div class="menu-item menu-item-child">
<a href="/bank-accounts?fm=header:menu:bank_accounts">Bank accounts</a>
</div>
<div class="menu-item menu-item-child">
<a href="/credit-cards?fm=header:menu:credit_cards">Credit cards</a>
</div>
<div class="menu-item menu-item-child">
<a href="/bank-accounts/visa-debit.html?fm=header:menu:debit_card">Debit card</a>
</div>
<div class="menu-item menu-item-child">
<a href="/business-banking?fm=header:menu:business_banking">Business banking</a>
</div>
</div>
</div>
<div class="menu-item menu-item-top-level has-children">
<a href="#">Loans and Mortgages</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/home-loans-mortgages?fm=header:menu:home_loans">Home loans</a>
</div>
<div class="menu-item menu-item-child">
<a href="/personal-loans?fm=header:menu:personal_loans">Personal loans</a>
</div>
<div class="menu-item menu-item-child">
<a href="/business-loans?fm=header:menu:business_loans">Business loans</a>
</div>
</div>
</div>
<div class="menu-item menu-item-top-level has-children">
<a href="#">KiwiSaver and Investments</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/investment-advice/types-of-investments.html?fm=header:menu:investment_options">Investment options</a>
</div>
<div class="menu-item menu-item-child">
<a href="/kiwisaver?fm=header:menu:kiwisaver">KiwiSaver</a>
</div>
<div class="menu-item menu-item-child">
<a href="/term-investments?fm=header:menu:term_investments">Term investments</a>
</div>
<div class="menu-item menu-item-child">
<a href="/asb-securities?fm=header:menu:share_trading">Share trading</a>
</div>
</div>
</div>
<div class="menu-item menu-item-top-level has-children">
<a href="#">Insurance</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/home-and-contents-insurance?fm=header:menu:home_and_contents">Home and contents</a>
</div>
<div class="menu-item menu-item-child">
<a href="/car-boat-other-vehicle-insurance?fm=header:menu:car">Car</a>
</div>
<div class="menu-item menu-item-child">
<a href="/life-insurance?fm=header:menu:life,_loan_and_income">Life, loan and income</a>
</div>
<div class="menu-item menu-item-child">
<a href="/travel-insurance?fm=header:menu:travel">Travel</a>
</div>
<div class="menu-item menu-item-child">
<a href="/health-insurance?fm=header:menu:health">Health</a>
</div>
<div class="menu-item menu-item-child">
<a href="/business-insurance?fm=header:menu:business">Business</a>
</div>
</div>
</div>
<div class="menu-item menu-item-top-level has-children">
<a href="#">International and Foreign Exchange</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/foreign-exchange?fm=header:menu:foreign_exchange">Foreign exchange</a>
</div>
<div class="menu-item menu-item-child">
<a href="/moving-to-new-zealand?fm=header:menu:moving_to_new_zealand">Moving to New Zealand</a>
</div>
<div class="menu-item menu-item-child">
<a href="/international-business?fm=header:menu:international_business">International business</a>
</div>
</div>
</div>
<div class="menu-item menu-item-top-level has-children">
<a href="#">Business and Rural</a>
<div class="menu-item-children">
<div class="menu-item menu-item-child">
<a href="/small-business-banking/small-business-banking-options.html?fm=header:menu:small_business">Small business</a>
</div>
<div class="menu-item menu-item-child">
<a href="https://businesshub.asb.co.nz/?fm=header:menu:asb_business_hub">ASB Business Hub</a>
</div>
<div class="menu-item menu-item-child">
<a href="/rural-banking?fm=header:menu:rural">Rural</a>
</div>
<div class="menu-item menu-item-child">
<a href="/commercial-corporate-banking?fm=header:menu:commercial_and_corporate">Commercial and corporate</a>
</div>
</div>
</div>
<div class="secondary-items-container">
<div class="column">
<div class="menu-item">
<a href="/bank-accounts/joining-asb.html?fm=header:menu:join_asb">Join ASB</a>
</div>
<div class="menu-item">
<a href="https://www.asb.co.nz/help?fm=header:menu:help_and_support">Help and Support</a>
</div>
<div class="menu-item">
<a href="https://www.asb.co.nz/rates-and-fees?fm=header:menu:rates_and_fees">Rates and Fees</a>
</div>
<div class="menu-item">
<a href="https://www.asb.co.nz/calculators?fm=header:menu:calculators_and_tools">Calculators and Tools</a>
</div>
<div class="menu-item">
<a href="/documents/terms-and-conditions.html?fm=header:menu:terms_and_conditions">Terms and Conditions</a>
</div>
<div class="menu-item">
<a href="https://www.asb.co.nz/contact-us?fm=header:menu:contact_us">Contact us</a>
</div>
</div>
<div class="column">
<div class="menu-item">
<a href="/about-us?fm=header:menu:about_us">About us</a>
</div>
<div class="menu-item">
<a href="https://careers.asbgroup.co.nz/asb/home?fm=header:menu:careers_at_asb">Careers at ASB</a>
</div>
<div class="menu-item">
<a href="/blog?fm=header:menu:asb_blog">ASB Blog</a>
</div>
<div class="menu-item">
<a href="https://www.asb.co.nz/community?fm=header:menu:community">Community</a>
</div>
<div class="menu-item">
<a href="/how-to?fm=header:menu:how-to_hub">How-to Hub</a>
</div>
</div>
</div>
</div>
</div></div><main class="outline-main" data-region="main">
<div>
<!-- start hero banner -->
<!-- Set gradient overlay if theme is black  or hero--full-gradient-overlay-->
<!-- Set gradient overlay if theme is black -->
<!-- end hero banner -->
<!-- start page nav -->
<!-- end page nav -->
<!-- start blade -->
<section class="blade " data-component="blade">
<div class="component-headline headline--simple" data-component="headline">
<div class="row">
<div class="col full">
<h1 class="h--group gamma">Page not found</h1>
<h4 class="h--group">We were unable to find the page you requested.</h4>
</div>
</div>
</div><div class="container">
<div data-text-image="text-image-bullet">
<div class="row">
<div class="col full">
<p class="lead">
    			Please try the following options:</p>
<ul>
<li>Check the URL you entered to make sure there were no typing or copy-and-paste errors.</li>
<li>Try clicking the magnifying glass in the top right corner and searching for the page you want.<br/>
</li>
<li>If you still can't find what you're looking for, try heading to <a adhocenable="false" data-layer-category="link" data-layer-event="site-interaction" data-layer-location="" data-layer-text="ASB's Home Page" data-layer-type="text" href="http://www.asb.co.nz/">ASB's Home Page</a>.</li>
<li>If you think there may be a problem with our site, please send a message to <a adhocenable="false" data-layer-category="link" data-layer-event="site-interaction" data-layer-location="" data-layer-text="ASB Customer Service" data-layer-type="text" href="/banking-with-asb/general-enquiry-form.html">ASB Customer Service</a>.</li>
</ul>
<ul>
</ul>
</div>
</div>
</div></div>
</section>
<!-- end blade -->
<!-- include terms and conditions -->
<!-- include modal window component -->
<div class="modal-views" data-hook="modals">
</div>
</div>
</main><div class="cq-dd-paragraph" style="display:inline;"><script type="text/javascript">
		var configFastLinks  = {"options":{"forceEnvironment":"special"},"fastLinks":[{"environment":"special","links":[]},{"environment":"fnc","links":[]},{"environment":"fnb","links":[]},{"environment":"ost","links":[]}]};
	</script>
<section class="footer footer-full-width">
<div class="row back-to-top-link-container">
<a>
<span class="back-to-top-link">Back to top</span>
<svg class="icon icon-back-to-top"><use xlink:href="#icon-back-to-top"></use></svg>
</a>
</div>
<div class="row logos-container">
<div class="logos">
<div class="logo">
<a href="/homepage.html?fm=footer:asb" target="_self"><svg class="icon asb-logo"><use xlink:href="#icon-asb-logo-colour"></use></svg></a>
</div>
</div>
<div class="component component--fast-links row links-container" data-component="fast-links">
<div class="links" data-hook="links-container">
</div>
</div>
</div>
<div class="row grey">
<div class="bottom-panel">
<div class="social-links">
<a class="social-link" href="https://www.facebook.com/ASBBank/" target="_blank" title="Follow ASB on Facebook"><svg class="icon-social icon-social-facebook"><use xlink:href="#icon-social-footer-facebook"></use></svg></a>
<a class="social-link" href="https://twitter.com/asbbank/" target="_blank" title="Follow ASB on Twitter"><svg class="icon-social icon-social-twitter"><use xlink:href="#icon-social-footer-twitter"></use></svg></a>
<a class="social-link" href="https://www.linkedin.com/company/asb-bank" target="_blank" title="Follow ASB on Linkedin"><svg class="icon-social icon-social-linkedin"><use xlink:href="#icon-social-footer-linkedin"></use></svg></a>
<a class="social-link" href="https://instagram.com/asbbank" target="_blank" title="Follow ASB on Instagram"><svg class="icon-social icon-social-instagram"><use xlink:href="#icon-social-footer-instagram"></use></svg></a>
<a class="social-link" href="/blog" target="_blank" title="Follow ASB's Blog"><svg class="icon-social icon-social-blog"><use xlink:href="#icon-social-footer-blog"></use></svg></a>
<a class="social-link" href="https://www.snapchat.com/add/asbbank" target="_blank" title="Add us on Snapchat"><svg class="icon-social icon-social-snapchat"><use xlink:href="#icon-social-snapchat-black"></use></svg></a>
<a class="social-link" href="https://www.weibo.com/asbbank" target="_blank" title="Follow ASB on Weibo"><svg class="icon-social icon-social-weibo"><use xlink:href="#icon-social-footer-weibo"></use></svg></a>
<a class="social-link" href="https://www.youtube.com/user/ASBBank/" target="_blank" title="Watch ASB's YouTube channel"><svg class="icon-social icon-social-youtube"><use xlink:href="#icon-social-footer-youtube"></use></svg></a>
</div>
<div class="legal-links">
<span class="legal-link" id="new-footer-copyright">© <span class="yearConstant" style="display:none;"></span> ASB Bank Limited</span>
<a class="legal-link" href="/contact-us?fm=footer:link:contact_us">Contact us</a>
<a class="legal-link" href="/documents/terms-and-conditions.html?fm=footer:link:terms_&amp;_conditions">Terms &amp; Conditions</a>
<a class="legal-link" href="/legal/privacy.html?fm=footer:link:privacy">Privacy</a>
<a class="legal-link" href="/legal/disclosure-statements.html?fm=footer:link:disclosure_statements">Disclosure Statements</a>
<a class="legal-link" href="/legal/notices.html?fm=footer:link:notices">Notices</a>
<a class="legal-link" href="" id="womensRefuge">
<img id="icon-women-refuge" src="/content/dam/asb/images/globalassetchannel/icons/shieldedsite/womens-refuge.png"/>
</a>
</div>
</div>
</div>
</section>
</div></div>
<div class="component component--overlay-search " data-component="overlay-search" data-gsa-site="ASBWEB_COLLECTION" data-gsa-url="https://www.asb.co.nz/search/clicktracking" data-hook="container" data-search-content-url="/content/asb/search/en/search/search-configuration.search-results.html?cq_ck=45345" data-search-url="https://www.asb.co.nz/" data-tracking-sitecat="true">
<div class="wrapper wrapper--relative">
<div class="header-container">
<div class="logo">
<a class="icon icon-asb-logo" href="/"><svg class="icon"><use xlink:href="#icon-asb-logo-colour"></use></svg></a>
</div>
</div>
<div class="close-container">
<div class="operator operator--close" data-hook="close"></div>
</div>
<div class="search-container">
<div class="wrapper wrapper--relative">
<div class="search-input-wrapper container">
<div class="search-input-container">
<svg class="icon icon-search-home"><use xlink:href="#icon-search-home"></use></svg>
<form class="search-form" data-hook="search-form">
<input class="search-input-suggestion" data-hook="search-input-suggestion" type="text" value=""/>
<input class="search-input" data-hook="search-input" placeholder="" type="text" value=""/>
<input class="search-submit" data-hook="search-submit" type="submit" value=""/>
<a class="search-clear" data-hook="search-clear" href="#">Clear</a>
</form>
</div>
<div class="search-correction" data-hook="search-correction">
<p>Did you mean: <a data-hook="suggested-search-term" href="#"></a>?</p>
</div>
</div>
<div class="query-suggestions-container" data-hook="query-suggestions-container">
<ul class="container" data-hook="query-suggestions-list"></ul>
</div>
<div class="search-results-wrapper">
<div class="container search-results-header">
<div class="filters-wrapper" data-hook="filter-wrapper">
<div class="filters" data-hook="filters"></div>
</div>
</div>
<div class="search-results-container" data-hook="search-results-container"></div>
<div class="no-results-container container" data-hook="no-results-container">
<h5></h5>
<p></p>
</div>
</div>
<div class="view-all-link-container"><a class="view-all-link btn" data-hook="view-all-link" href="#"></a></div>
<div class="related-searches-container container">
<div class="related-search-header">
<div class="back-to-top-link-container">
<a href="#"><span class="back-to-top-link"></span><svg class="icon icon-back-to-top"><use xlink:href="#icon-arrow-up"></use></svg></a>
</div>
<h5 class="related-search-title" data-hook="related-search-title"><span data-hook="related-search-title-text"></span><span class="related-search-term" data-hook="related-search-term"></span></h5>
</div>
<div class="related-search-links-container">
<ul class="related-search-links" data-hook="related-search-links"></ul>
</div>
</div>
</div>
</div>
<div class="search-overlay-background-blur" data-hook="search-overlay-background-blur"></div>
</div>
</div>
<div class="modal-outline" data-hook="modal">
<div class="modal-window" data-hook="modal-window">
<a class="modal-close" data-hook="modal-close" href="#"></a>
<div class="modal-region" data-region="modal-main"></div>
</div>
<div class="modal-overlay" data-hook="modal-overlay"></div>
</div>
<script src="/etc/designs/asb/common-blade/clientlibrary/appstyle-common-blade.min.2020102193.js" type="text/javascript"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-11217622-3', 'auto');
    ga('require', 'displayfeatures');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');
</script>
<script type="text/javascript">
            var disableAdobeAnalyticsPageLevelTracking= true;
            var disableGAPageLevelTracking= false;
        </script>
<script type="text/javascript">
	_satellite.pageBottom();
    </script>
</body></html>
