<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="	" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Architects Inc.</title>
<!-- CSS styles -->
<link href="assets/css/base.css" rel="stylesheet"/>
<!-- <link rel="stylesheet" href="assets/css/styles/dark-sidebar.css"> -->
<!-- <link rel="stylesheet" href="assets/css/styles/skin1.css"> -->
<!-- /CSS styles -->
</head>
<body class="show-sidebar light-sidebar">
<a href="#" id="menu-toggle-wrapper">
<div id="menu-toggle"></div>
</a>
<!-- inner bar wrapper -->
<a href="#" id="inner-bar">
</a>
<!-- /inner bar wrapper -->
<div id="side-bar">
<div class="inner-wrapper">
<div id="side-inner">
<div id="logo-wrapper">
<a href="index.html"><img alt="logo" src="assets/img/demo-3/darkLogo.png"/></a>
</div>
<div id="side-contents">
<ul id="navigation">
<li class="current-menu-item"><a href="index.html">Home</a></li>
<li><a href="about-us.html">About</a></li>
<li><a href="portfolio.html">Portfolio</a></li>
<li><a href="founder.html">Founder</a></li>
<li><a href="team.html">Team</a></li>
<li><a href="contact.html">Contact</a></li>
</ul>
</div>
<!-- Sidebar footer -->
<div id="side-footer">
<!-- Social icons -->
<ul class="social-icons">
<li><a href="https://www.facebook.com/www.blorearchitects.inc.net" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="https://www.linkedin.com/pub/architects-inc/a8/8b/563" target="_blank"><i class="fa fa-linkedin"></i></a></li>
</ul>
<!-- /Social icons -->
<div id="copyright">
							Architects Inc.
						</div>
</div>
<!-- /Sidebar footer -->
</div>
</div>
</div>
<!--Page main wrapper -->
<div class="abs light-template" id="main-content">
<div class="kb-slider">
<div class="item">
<img alt="img" src="assets/projects/Alliance/alliance_1.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-1.html" title="sample page">
<!-- 							<span class="sub-title">You are the universe</span>
							<span class="title">Architecture Design</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/emblem/emblem.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">New Story</span>
							<span class="title">Residential Spaces</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/hNpatil/hNpatil_4.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">New Story</span>
							<span class="title">Residential Spaces</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/Pinnacle/pinnacle.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">New Story</span>
							<span class="title">Residential Spaces</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/Prakash/prakash_2.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">New Story</span>
							<span class="title">Residential Spaces</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/Shroff/shroff_8.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">And in that moment, I swear</span>
							<span class="title">Apartment Blocks</span> -->
</a>
</div>
</div>
<div class="item">
<img alt="img" src="assets/projects/NIE_Mysore/nie_courtyard.jpg"/>
<div class="caption" data-pos="bottom-left">
<a href="portfolio-2.html" title="sample page">
<!-- 							<span class="sub-title">And in that moment, I swear</span>
							<span class="title">Apartment Blocks</span> -->
</a>
</div>
</div>
</div>
</div>
<!--/Page main wrapper -->
<!-- jquery core -->
<script src="assets/js/vendors/jquery-1.11.0.min.js" type="text/javascript"></script>
<!-- imagesLoaded jquery plugin -->
<script src="assets/js/vendors/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<!-- jquery isotop plugin -->
<script src="assets/js/vendors/isotope.pkgd.min.js" type="text/javascript"></script>
<!-- jquery history neede for ajax pages -->
<script src="assets/js/vendors/jquery.history.js" type="text/javascript"></script>
<!-- owwwlab jquery kenburn slider plugin -->
<script src="assets/js/jquery.owwwlab-kenburns.js" type="text/javascript"></script>
<!-- owwwlab jquery double carousel plugin -->
<script src="assets/js/jquery.owwwlab-DoubleCarousel.js" type="text/javascript"></script>
<!-- owwwwlab jquery video background plugin -->
<script src="assets/js/jquery.owwwlab-video.js" type="text/javascript"></script>
<!-- tweenmax animation framework -->
<script src="assets/js/vendors/TweenMax.min.js" type="text/javascript"></script>
<!-- jquery nice scroll plugin needed for vertical portfolio page -->
<script src="assets/js/vendors/jquery.nicescroll.min.js" type="text/javascript"></script>
<!-- jquery magnific popup needed for ligh-boxes -->
<script src="assets/js/vendors/jquery.magnific-popup.js" type="text/javascript"></script>
<!-- html5 media player -->
<script src="assets/js/vendors/mediaelement-and-player.min.js" type="text/javascript"></script>
<!-- jquery inview plugin -->
<script src="assets/js/vendors/jquery.inview.min.js" type="text/javascript"></script>
<!-- smooth scroll -->
<script src="assets/js/vendors/smoothscroll.js" type="text/javascript"></script>
<!-- Master Slider -->
<script src="assets/masterslider/jquery.easing.min.js"></script>
<script src="assets/masterslider/masterslider.min.js"></script>
<!-- theme custom scripts -->
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>