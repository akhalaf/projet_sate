<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-130100796-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130100796-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-538BH6P');</script>
<!-- End Google Tag Manager -->
<title>De Mûelenaere Oncology &gt; </title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="cancersa, cancer, treatment, radiation, chemotherapy, Oncology, radiotherapy,De Mûelenaere Oncology, DMO" name="keywords"/>
<meta content="Cancer treatment available in Gauteng, South Africa. De Mûelenaere Oncology (DMO) is a group of radiation oncology practices that offer cancer patients the total spectrum of radiotherapy services." name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<link href="//fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css"/>
<link href="/includes/css/swiper.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/dark.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/font-icons.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/includes/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/style6959.css" id="style" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider369f.css?ver=6.6.4" id="layerslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext" id="ls-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/contact-form-7/includes/css/styles20fd.css?ver=4.9.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/cf7-conditional-fields/stylec412.css?ver=1.3.4" id="cf7cf-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-listing/assets/css/listing6959.css?ver=4.9.3" id="qode_listing_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-listing/assets/css/listing-responsive.min6959.css?ver=4.9.3" id="qode_listing_style_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-membership/assets/css/qode-membership.min6959.css?ver=4.9.3" id="qode_membership_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-membership/assets/css/qode-membership-responsive.min6959.css?ver=4.9.3" id="qode_membership_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-quick-links/assets/css/qode-quick-links.min6959.css?ver=4.9.3" id="qode_quick_links_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-restaurant/assets/css/qode-restaurant.min6959.css?ver=4.9.3" id="qode_restaurant_script-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-restaurant/assets/css/qode-restaurant-responsive.min6959.css?ver=4.9.3" id="qode_restaurant_responsive_script-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/svg-vector-icon-plugin/admin/css/wordpress-svg-icon-plugin-style.min6959.css?ver=4.9.3" id="default-icon-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/superfish6959.css?ver=4.9.3" id="timetable_sf_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/style6959.css?ver=4.9.3" id="timetable_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/event_template6959.css?ver=4.9.3" id="timetable_event_template-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/timetable/style/responsive6959.css?ver=4.9.3" id="timetable_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato%3A400%2C700&amp;ver=4.9.3" id="timetable_font_lato-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/wp-job-manager/assets/css/chosenf488.css?ver=1.1.0" id="chosen-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/wp-job-manager/assets/css/frontend2866.css?ver=1.29.2" id="wp-job-manager-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/style6959.css?ver=4.9.3" id="default_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/font-awesome/css/font-awesome.min6959.css?ver=4.9.3" id="qode_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/elegant-icons/style.min6959.css?ver=4.9.3" id="qode_font_elegant-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/linea-icons/style6959.css?ver=4.9.3" id="qode_linea_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/dripicons/dripicons6959.css?ver=4.9.3" id="qode_dripicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/stylesheet.min6959.css?ver=4.9.3" id="stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/print6959.css?ver=4.9.3" id="qode_print-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/timetable-schedule.min6959.css?ver=4.9.3" id="qode_timetable-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/timetable-schedule-responsive.min6959.css?ver=4.9.3" id="qode_timetable_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-news/assets/css/news-map.min6959.css?ver=4.9.3" id="qode_news_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/responsive.min6959.css?ver=4.9.3" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/qode-news/assets/css/news-map-responsive.min6959.css?ver=4.9.3" id="qode_news_responsive_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/style_dynamic3650.css?ver=1517570090" id="style_dynamic-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/style_dynamic_responsive3650.css?ver=1517570090" id="style_dynamic_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/js_composer/assets/css/js_composer.min5243.css?ver=5.4.5" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/themes/bridge/css/custom_cssd11a.css?ver=1517570086" id="custom_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/plugins/master-slider/public/assets/css/masterslider.main5589.css?ver=3.4.1" id="msl-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/uploads/master-slider/custom7ef2.css?ver=1.5" id="msl-custom-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/includes/wp-content/cache/nextend/web/n2-ss-2/n2-ss-2b6d0.css?1517574399" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic|Oxygen:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic|Quicksand:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- External JavaScripts
	============================================= -->
<script src="/includes/js/jquery.js" type="text/javascript"></script>
<script src="/includes/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/includes/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/includes/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
		var isMobi = false		//var isMobi = 'true';
	</script>
<!-- Footer Scripts
	============================================= -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
</head>
<body class="stretched no-transition">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-538BH6P" style="display:none;visibility:hidden" width="0"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="clearfix" id="wrapper">
<header class="full-header" id="header" style="position:fixed;">
<div id="header-wrap">
<section style="">
<div class="container clearfix" style="text-align:Center;"><div class="visible-lg-block visible-md-block " id="logo" style="text-align:Center;">
<a class="standard-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/" style="text-align:Center;"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="margin-right: auto;margin-left: auto;"/></a>
<a class="retina-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/" style="text-align:Center;"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="margin-right: auto;margin-left: auto;"/></a>
</div>
<div class="visible-xs-block visible-sm-block" id="logo">
<a class="standard-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="height:70px !important"/></a>
<a class="retina-logo" data-dark-logo="/includes/wp-content/uploads/2014/12/DMO-logo.png" href="/"><img alt="De Mûelenaere Oncology" src="/includes/wp-content/uploads/2014/12/DMO-logo.png" style="height:70px !important; padding-top:20px;"/></a>
</div></div>
<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
<div class="visible-lg-block " style="position:relative;float:right;color:#fff;right: 1%;z-index: 9999999;top:20px;">
<!-- <div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/in.png" alt=""></a></div>
	        	<div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/yt.png" alt=""></a></div>
	        	<div style="display:inline-block;width:40px;padding-right:5px;"><a href="" target="_blank"><img style="width:30px" src="/includes/images/ig.png" alt=""></a></div> 
	        	<div style="display:inline-block;width:40px;"><a href="https://www.facebook.com/SandtonOncologyCentre" target="_blank"><img alt="Sandton Oncology Facebook" style="width:30px" src="/includes/images/fb.png" alt=""></a></div>-->
</div>
<nav class="dark" id="primary-menu"><ul><li class=" "><a href="/home">Home</a></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="about">About <b class="caret"></b> </a><ul style="0"><li class=" "><a href="vision">What We Stand For</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="specialists">Specialists <b class="caret"></b> </a><ul style="0"><li class=" "><a href="robert">Dr Robert Alexander Georges (Robbie) de Mûelenaere</a></li><li class=" "><a href="kobie">Dr Kobie Pio</a></li><li class=" "><a href="ingo">Dr Ingo de Mûelenaere</a></li><li class=" "><a href="bernard">Prof Bernard Donde</a></li><li class=" "><a href="sudeshen">Dr Sudeshen M Naidoo</a></li><li class=" "><a href="marlene">Dr Marlene Soares</a></li><li class=" "><a href="nirasha">Dr Nirasha Chiranjan</a></li><li class=" "><a href="maryke">Dr Maryke Etsebeth</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="">Services <b class="caret"></b> </a><ul style="0"><li class=" "><a href="beam">External Beam Radiation Therapy</a></li><li class=" "><a href="stereo">Stereotactic Radiosurgery</a></li><li class=" "><a href="intensity">Intensity-Modulated Radiation Therapy</a></li><li class=" "><a href="brach">Brachytherapy</a></li><li class=" "><a href="ortho">Orthovoltage Radiation Therapy</a></li><li class=" "><a href="forums">Multidisciplinary Forums</a></li><li class=" "><a href="social">Oncology Social Workers for Psychosocial Care</a></li><li class=" "><a href="ct">CT Scanners For Treatment Planning</a></li><li class=" "><a href="survivor">Survivor Run Support Groups</a></li><li class=" "><a href="shuttle">Shuttle Services</a></li><li class=" "><a href="medical">Medical Oncologists</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="patient">Patient Info <b class="caret"></b> </a><ul style="0"><li class=" "><a href="treatment">Treatment Information</a></li><li class=" "><a href="faq">FAQS</a></li><li class=" "><a href="support">Charities, Useful Links and Networks</a></li><li class=" "><a href="testimonial">Testimonials</a></li><li class=" "><a href="covid19">Covid-19<br/>What you need to know</a></li><li class=" "><a href="visitor">Covid-19 Visitor Policy</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="">A DMO center near you <b class="caret"></b> </a><ul style="0"><li class=" "><a href="ahmed">Ahmed Kathrada Cancer Institute</a></li><li class=" "><a href="sandton">Sandton Oncology</a></li><li class=" "><a href="westrand">West Rand Oncology Centre</a></li><li class=" "><a href="muelmed">Muelmed Radiation Oncology</a></li><li class=" "><a href="groenkloof">Groenkloof Radiation Oncology</a></li></ul></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="blog">Blog</a></li><li class="hidden-xs hidden-sm divider-vertical" style="height:15px;;position:relative;"></li><li class=" "><a href="request">Request an Appointment <b class="caret"></b> </a><ul style="0"><li class=" "><a href="request#prepare">How to prepare for your appoint ment</a></li></ul></li></ul></nav>
</section>
</div>
</header>
<link href="../includes/css/404.css" rel="stylesheet"/>
<div class="fof">
<div class="alert alert-danger fade in" role="alert">
		Redirecting...
	</div>
</div>
<script type="text/javascript">
//$(".alert").alert()
window.location = "/";
</script><div class="footer_inner footer-stick clearfix" id="site-footer">
<div class="footer_top_holder">
<div class="footer_top">
<div class="container">
<div class="container_inner">
<div class="three_columns clearfix">
<div class="column1 footer_col1">
<div class="column_inner">
<div class="widget_text widget widget_custom_html" id="custom_html-3"><div class="textwidget custom-html-widget"><img alt="DMO Logo" src="/includes/wp-content/uploads/2014/12/DMO-logo.png"/>
<br/>
<ul>
<!-- <li><a target="_blank" href="mailto:Marketing@cancersa.co.za"><i class="wp-svg-envelop envelop"></i></a></li> -->
<li></li>
<li><a href="https://www.facebook.com/DMOCancerSpecialists/" target="_blank"><i class="wp-svg-facebook-2 facebook-2"></i></a></li>
<!-- <li><a target="_blank" href="https://plus.google.com/u/0/101069306117158681068"><i class="wp-svg-google-plus-3 google-plus-3"></i></a></li> -->
<li><a href="https://twitter.com/DMOcancersa" target="_blank"><i class="wp-svg-twitter-2 twitter-2"></i></a></li>
</ul></div></div> </div>
</div>
<div class="column2 footer_col2">
<div class="column_inner">
<div class="widget_text widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget"><h1>
	A DMO Centre Near You
</h1>
<table>
<td><strong>Ahmed Kathrada Cancer Institute</strong><br/>Ahmed Kathrada Private Hospital<br/>K43 Highway<br/>Lenasia Extension 8<br/>Johannesburg, Gauteng<br/>+27 (10) 900 3199<br/><a href="/ahmed">View Map</a></td>
<td><strong>Sandton Oncology</strong><br/>200 Rivonia Medical Centre<br/>200 Rivonia Road<br/>Morningside, Sandton<br/>+27 (11) 883 0900<br/><a href="/sandton">View Map</a></td>
<td><strong>West Rand Oncology Centre</strong><br/>Flora Clinic<br/>Cnr William Nicol Road &amp; Joseph Lister Avenue<br/>Floracliffe, Roodepoort<br/>+27 (11) 991 3500<br/><a href="/westrand">View Map</a></td>
<td><strong>Muelmed Radiation Oncology</strong><br/>Muelmed Hospital<br/>577 Pretorius Street<br/>Arcadia, Pretoria<br/>+27 (12) 440 8089<br/><a href="/muelmed">View Map</a></td>
<td><strong>Groenkloof Radiation Oncology</strong><br/>Life Groenkloof Hospital<br/>50 George Storrar Dr<br/>Groenkloof, Pretoria<br/>+27 (12) 460 4749<br/><a href="/groenkloof">View Map</a></td>
</table>
<br/>
<p class="" style="text-align: center;">© <a href="/">De Mûelenaere Oncology</a> 2018. </p>
</div></div> </div>
</div>
<div class="column3 footer_col3">
<div class="column_inner">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Go To Top
	============================================= -->
<div class="icon-angle-up" id="gotoTop"></div>
<!-- External JavaScripts
	============================================= -->
<script src="/includes/js/plugins.js" type="text/javascript"></script>
<!-- Footer Scripts
	============================================= -->
<script src="/includes/js/functions.js" type="text/javascript"></script>
</div></body>
</html>