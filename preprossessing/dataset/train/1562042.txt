<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]--><!--[if !(IE 6) & !(IE 7) & !(IE 8)]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>
Page not found | warriorpoetblog	</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://warriorpoetblog.com/wp-content/themes/twentyeleven/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://warriorpoetblog.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
<script src="https://warriorpoetblog.com/wp-content/themes/twentyeleven/js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://warriorpoetblog.com/index.php/feed/" rel="alternate" title="warriorpoetblog » Feed" type="application/rss+xml"/>
<link href="https://warriorpoetblog.com/index.php/comments/feed/" rel="alternate" title="warriorpoetblog » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/warriorpoetblog.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.15"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://warriorpoetblog.com/wp-content/themes/twentyeleven/blocks.css?ver=20181230" id="twentyeleven-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://warriorpoetblog.com/index.php/wp-json/" rel="https://api.w.org/"/>
<link href="https://warriorpoetblog.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://warriorpoetblog.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.8.15" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="error404 single-author two-column right-sidebar">
<div class="hfeed" id="page">
<header id="branding" role="banner">
<hgroup>
<h1 id="site-title"><span><a href="https://warriorpoetblog.com/" rel="home">warriorpoetblog</a></span></h1>
<h2 id="site-description">Just another WordPress site</h2>
</hgroup>
<a href="https://warriorpoetblog.com/">
<img alt="warriorpoetblog" height="288" src="https://warriorpoetblog.com/wp-content/themes/twentyeleven/images/headers/chessboard.jpg" width="1000"/>
</a>
<form action="https://warriorpoetblog.com/" id="searchform" method="get">
<label class="assistive-text" for="s">Search</label>
<input class="field" id="s" name="s" placeholder="Search" type="text"/>
<input class="submit" id="searchsubmit" name="submit" type="submit" value="Search"/>
</form>
<nav id="access" role="navigation">
<h3 class="assistive-text">Main menu</h3>
<div class="skip-link"><a class="assistive-text" href="#content">Skip to primary content</a></div>
<div class="skip-link"><a class="assistive-text" href="#secondary">Skip to secondary content</a></div>
<div class="menu"><ul>
<li><a href="https://warriorpoetblog.com/">Home</a></li><li class="page_item page-item-2"><a href="https://warriorpoetblog.com/index.php/sample-page/">Sample Page</a></li>
</ul></div>
</nav><!-- #access -->
</header><!-- #branding -->
<div id="main">
<div id="primary">
<div id="content" role="main">
<article class="post error404 not-found" id="post-0">
<header class="entry-header">
<h1 class="entry-title">This is somewhat embarrassing, isn’t it?</h1>
</header>
<div class="entry-content">
<p>It seems we can’t find what you’re looking for. Perhaps searching, or one of the links below, can help.</p>
<form action="https://warriorpoetblog.com/" id="searchform" method="get">
<label class="assistive-text" for="s">Search</label>
<input class="field" id="s" name="s" placeholder="Search" type="text"/>
<input class="submit" id="searchsubmit" name="submit" type="submit" value="Search"/>
</form>
<div class="widget widget_recent_entries"> <h2 class="widgettitle">Recent Posts</h2> <ul>
<li>
<a href="https://warriorpoetblog.com/index.php/2017/10/27/the-warriors-1979/">The Warriors (1979)</a>
</li>
<li>
<a href="https://warriorpoetblog.com/index.php/2017/10/27/what-is-a-warrior/">What is a warrior?</a>
</li>
<li>
<a href="https://warriorpoetblog.com/index.php/2017/10/27/soliders-are-a-real-community-leaders/">Soliders are a real community leaders</a>
</li>
</ul>
</div>
<div class="widget">
<h2 class="widgettitle">Most Used Categories</h2>
<ul>
<li class="cat-item cat-item-1"><a href="https://warriorpoetblog.com/index.php/category/uncategorized/">Uncategorized</a> (3)
</li>
</ul>
</div>
<div class="widget widget_archive"><h2 class="widgettitle">Archives</h2><p>Try looking in the monthly archives. 🙂</p> <label class="screen-reader-text" for="archives-dropdown--1">Archives</label>
<select id="archives-dropdown--1" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
<option value="">Select Month</option>
<option value="https://warriorpoetblog.com/index.php/2017/10/"> October 2017 </option>
</select>
</div>
</div><!-- .entry-content -->
</article><!-- #post-0 -->
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- #main -->
<footer id="colophon" role="contentinfo">
<div id="site-generator">
<a class="imprint" href="https://wordpress.org/" title="Semantic Personal Publishing Platform">
					Proudly powered by WordPress				</a>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://warriorpoetblog.com/wp-includes/js/wp-embed.min.js?ver=4.8.15" type="text/javascript"></script>
</body>
</html>
