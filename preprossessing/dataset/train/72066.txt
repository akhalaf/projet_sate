<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html lang="en">
<head>
<meta content="IE=10.000" http-equiv="X-UA-Compatible"/>
<meta charset="utf-8"/>
<title>Active Search Results Search Engine</title>
<link href="/main2.css" rel="stylesheet"/>
<meta content="MSHTML 10.00.9200.17377" name="GENERATOR"/>
<meta content="Active Search Results (ASR) is an independent Internet Search Engine using a proprietary search engine ranking technology. All search results are provided by ASR's internal indexes and databases without relying on outside resources or third party search engines. ASR maintains its own spiders visiting Web sites daily that are submitted to ASR and crawlers that index other popular Web sites on the Internet." name="description"/>
<meta content="search engine, Active Search Results Page Rank,page,rank, signal" name="keywords"/>
<meta content="text/html;  charset=utf-8" http-equiv="Content-Type"/>
<meta content="VspjZcxQ_E2oCE0niooBfvdH3HTwHOrmoSTG8x79h5Q" name="google-site-verification"/>
<meta content="08fb33bbb78a7102" name="y_key"/>
<meta content="fJ1nDw0OBlSJ4sad0fI2nF95oXc" name="alexaVerifyID"/>
<meta content="3022642B7EC87E3898AD0025531DE003" name="msvalidate.01"/>
<meta content="DA660809B6E47EA77BD0EC214543C91E" name="msvalidate.01"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-0529015495139792",
    enable_page_level_ads: true
  });
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19072563-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body class="Site">
<div class="row top-bar">
<div class="holder">
<div class="half-col">
<a href="/">
<img height="48" src="/images2/asr-logo1.png" width="72"/></a>

&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp&amp;nbsp    
<a href="https://twitter.com/asrranking" target="_blank"><img alt="Facebook" border="0" height="25" src="/images/sb_tw.png" width="25"/></a>
<a href="https://www.facebook.com/ASRRanking/" target="_blank"><img alt="Facebook" border="0" height="25" src="/images/sb_fb.png" width="25"/></a>
<!-- <a href="https://plus.google.com/share?app=110&url=http%3A%2F%2Fwww.activesearchresults.com%2F" target="_blank"><img src="/images/sb_gp.png" border="0" alt="Facebook" width="40" height="40"></a> -->
<a href="https://www.linkedin.com/company/active-search-results" target="_blank"><img alt="Facebook" border="0" height="25" src="/images/sb_li.png" width="25"/></a>
<g:plusone size="medium"></g:plusone>
</div>
<div class="half-col right-align">
<ul class="top-nav">
<li><a href="/addwebsite.php">Add a Website</a></li><li><a href="/login/register.php">Register</a></li><li class="no-pad"><a class="sign-btn" href="/login/login.php">Sign In</a></li>
</ul></div></div></div>
<div class="holder search-area">
<div class="vert-large-gap">
<div class="row centered">
<a href="/"><img height="32" src="/images2/asr-logo.png" width="374"/></a>
</div></div>
<div class="row centered large-btn-gap">
<!-- Add Adsense code here --> <!-- Remove holder image -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90, created 03/04/17 Header -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0529015495139792" data-ad-slot="2774549505" style="display:inline-block;width:728px;height:90px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="vert-large-gap">
<div class="row centered">
<form action="/searchsubmit.php" method="POST">
<input class="search-fld" name="wordsall" type="text"/>
<input class="search-btn" name="buttonall" type="submit" value=""/>
<input name="adultfilter" type="hidden" value="0"/>
<input name="perpage" type="hidden" value="10"/>
<input name="asrtech" type="hidden" value="yes"/>
</form>
</div></div>
<div class="row" style="padding-bottom: 12px;">
<div class="centered">
<div class="info-box"><!-- Update path to image to correct location on your server -->
<img class="small-gap" height="98" src="/images2/about-icon.png" width="98"/>
<h2>About Us</h2>
<p>Active Search Results (ASR) is an independent Internet Search Engine using 
Active Search Results Page Ranking Technology (ASR Ranking) with Millions…</p>
<div class="btn"><a href="/help/faq.php">Learn More</a>
</div></div></div>
<div class="centered">
<div class="info-box"><!-- Update path to image to correct location on your server -->
<img class="small-gap" height="98" src="/images2/tech-icon.png" width="98"/>
<h2>Our Technology</h2>
<p>ASR Ranking is a new search engine page ranking algorithm that allows search 
engines to rank search results higher for websites where the owners or…</p>
<div class="btn"><a href="/help/about.php">Learn More</a>
</div></div></div>
<div class="centered">
<div class="info-box no-margin"><!-- Update path to image to correct location on your server -->
<img class="small-gap" height="98" src="/images2/rank-icon.png" width="98"/>
<h2>Earn Higher Ranking</h2>
<p>Ways for website owners and promoters to earn a higher ASR Ranking at Active 
Search Results. As mentioned above, when you perform activity…</p>
<div class="btn"><a href="/help/about.php#activitytypespoints">Learn More</a>
</div></div></div></div>
<div class="row centered vert-large-gap">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90, created 03/04/17 Middle -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0529015495139792" data-ad-slot="7204749104" style="display:inline-block;width:728px;height:90px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="row">
<div class="large-btn-gap">
<h2 class="art-head"><a href="/news/index.php">Press Releases</a></h2><br/>
<h2 class="art-head">Recommended Articles</h2></div>
<div class="art-col">
<h3>Get More Sales And Grow Your Business By Generating Leads From Facebook</h3>
<p>When you are trying your best to generate new leads, it is essential to try 
all possible channels. If you have never considered using Facebook for this 
purpose, now is the time to start. Here are some pointers that will help you be 
more successful than you ever imagined.</p>
<p><a href="/articles2/moresalesgrowbusinessleadsfacebook.php">Continue 
Reading</a></p></div>
<div class="art-col no-margin">
<h3>A Step-by-Step Guide to Updating Your Website</h3>
<p>If you are planning to launch a new version of your website, you will need to 
take some steps to ensure that you do not damage your search engine rankings in 
the process.</p>
<p><a href="/articles2/stepbystepguideupdatingwebsite.php">Continue Reading</a></p>
</div></div>
<div class="row vert-large-gap">
<div class="art-btn centered"><a href="/articles/articlelist1.php">View All Articles</a>
</div></div>
</div><!--end holder-->
<br/>
<center>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 728x90, created 03/04/17 Footer -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0529015495139792" data-ad-slot="4251282705" style="display:inline-block;width:728px;height:90px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!--  Place this tag after the last plusone tag -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<br/>
<br/>
<center>
<a href="https://twitter.com/asrranking" target="_blank"><img alt="Facebook" border="0" height="40" src="/images/sb_tw.png" width="40"/></a>
<a href="https://www.facebook.com/ASRRanking/" target="_blank"><img alt="Facebook" border="0" height="40" src="/images/sb_fb.png" width="40"/></a>
<!-- <a href="https://plus.google.com/share?app=110&url=http%3A%2F%2Fwww.activesearchresults.com%2F" target="_blank"><img src="/images/sb_gp.png" border="0" alt="Facebook" width="40" height="40"></a> -->
<a href="https://www.linkedin.com/company/active-search-results" target="_blank"><img alt="Facebook" border="0" height="40" src="/images/sb_li.png" width="40"/></a>
<g:plusone size="medium"></g:plusone>
</center>
<div class="row footer">
<div class="holder">
<div class="half-col">
<ul class="footer-nav">
<li><a href="/">Home</a></li>
<li><a href="/help/about.php">About ASR</a></li>
<li><a href="/help/privacypolicy.php">Privacy</a></li>
<li><a href="/help/contactus.php">Contact Us</a></li>
<li><a href="/help/linktous.php">Link to Us</a></li>
<li><a href="/searchform.php">Search</a></li>
<!--<LI class="no-pad"><A href="/sponsors.php">Sponsors</A></LI>-->
<li><a href="/help/help.php">Help</a></li>
</ul></div>
<div class="half-col copy right-align">
<p>©2021 Active Search Results&amp;nbsp</p>
</div></div></div>
<!--
<center><a href="http://store.exactseek.com/asr.html" target="_blank">
<img src="/images/oneblackblue.gif" border="0" alt="ExactSeek"></a></center>
</BODY></HTML>
-->
</center></body></html>