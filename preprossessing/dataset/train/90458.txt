<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>
		African Journals Online
					</title>
<meta content="Open Journal Systems 3.1.2.4" name="generator"/>
<link href="https://www.ajol.info/index.php/ajol/$$$call$$$/page/page/css?name=stylesheet" rel="stylesheet" type="text/css"/><link href="//fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Open+Sans:400,400i,700,700i" rel="stylesheet" type="text/css"/><link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css?v=3.1.2.4" rel="stylesheet" type="text/css"/><link href="https://www.ajol.info/plugins/themes/default-child/styles/ajol-child.css?v=3.1.2.4" rel="stylesheet" type="text/css"/><link href="//fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i" rel="stylesheet" type="text/css"/>
</head>
<body class="pkp_page_index pkp_op_index" dir="ltr">
<div id="fb-root"></div>
<script async="" crossorigin="anonymous" defer="" nonce="3vC5jqYq" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&amp;version=v9.0&amp;appId=646790359344356&amp;autoLogAppEvents=1"></script>
<div id="tagline">
<div style="width:100%;">
<div style="margin:5px 0 0 10px; white-space:nowrap; float:left; font-size:14px"></div>
<div id="promoting" style="margin:5px 20px 0 0; white-space:nowrap; float:left; font-size:14px;height:20px">PROMOTING ACCESS TO AFRICAN RESEARCH</div>
<div class="clearer"></div>
</div>
</div>
<div class="clearer"></div>
<div id="navbar">
<div class="menudiv">
<ul class="menu">
<li class="menubullet" id="home"><a href="https://www.ajol.info" style="margin-left:5px;" target="_parent"><span id="menuhometext">AFRICAN JOURNALS ONLINE (AJOL)</span></a></li>
<li class="menubullet"><a href="https://www.ajol.info/index.php/ajol/browseBy/category">Journals</a></li>
<li class="menubullet" id="search"><a href="https://www.ajol.info/index.php/ajol/Gsearch/google?q="><span id="menusearchtext">Search</span></a></li>
<li class="menubullet"><a href="https://www.ajol.info/index.php/ajol/how-researchers-can-use-AJOL">USING AJOL</a></li>
<li class="menubullet"><a href="https://www.ajol.info/index.php/ajol/resources-for-researchers">RESOURCES</a></li>
</ul>
</div>
</div> <div class="cmp_skip_to_content">
<a href="#pkp_content_main">Skip to main content</a>
<a href="#pkp_content_nav">Skip to main navigation menu</a>
<a href="#pkp_content_footer">Skip to site footer</a>
</div>
<div class="pkp_structure_page">
<header class="pkp_structure_head" id="headerNavigationContainer" role="banner">
<div class="pkp_head_wrapper">
<div class="pkp_site_name_wrapper">
<h1 class="pkp_site_name">
<div style="float:left;padding-top:0px;">
<a class="is_text" href="                                  https://www.ajol.info/index.php/ajol/index
                              ">African Journals Online</a>
</div>
<div style="float:left;padding: 6px 0 0 12px;">
</div>
<!--google search-->
<div style="float:right;">
<div style="width:300px;">
<script>
                        (function() {
                          var cx = '007797540432222069508:kprfz3-g5lc';
                          var gcse = document.createElement('script');
                          gcse.type = 'text/javascript';
                          gcse.async = true;
                          gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                          var s = document.getElementsByTagName('script')[0];
                          s.parentNode.insertBefore(gcse, s);
                        })();
                      </script>
<gcse:searchbox-only resultsurl="https://www.ajol.info/index.php/ajol/Gsearch/google" style="width:200px;"></gcse:searchbox-only>
</div>
</div>
<!--google search-->
</h1>
</div>
<nav aria-label="Site Navigation" class="pkp_navigation_primary_row">
<div class="pkp_navigation_primary_wrapper">
</div>
</nav>
<nav aria-label="User Navigation" class="pkp_navigation_user_wrapper" id="navigationUserWrapper">
<ul class="pkp_navigation_user pkp_nav_list" id="navigationUser">
<li class="profile">
<a href="https://www.ajol.info/index.php/ajol/user/register">
					Register
				</a>
</li>
<li class="profile">
<a href="https://www.ajol.info/index.php/ajol/login">
					Login
				</a>
</li>
</ul>
</nav>
</div><!-- .pkp_head_wrapper -->
</header><!-- .pkp_structure_head -->
<div class="pkp_structure_content has_sidebar">
<div class="pkp_structure_main" id="pkp_content_main" role="main">
<h3>Welcome to AJOL!</h3>

        Researchers and policy-makers need access to contextually-relevant quality research publications from Africa in order to develop solutions to address the continent’s challenges in health, education, climate change &amp; under-development.
        <br/><br/>
<b>African Journals OnLine (AJOL)</b> is the world's largest and preeminent platform of African-published scholarly journals.

        <b>AJOL is a Non-Profit Organisation that (since 1998) works to increase global &amp; continental online access, awareness, quality &amp; use of African-published, peer-reviewed research.</b>

        Millions of monthly downloads by site users from nearly every country in the world are an indication of the need and widespread use of the AJOL initiative. More than half of the repeat users are from Africa.
        <br/><br/>
<div class="page_index_journal">
<div id="siteStats" style="text-align:center;">
<!--h2>Hosted by AJOL:</h2-->
      AJOL hosts <b><a href="ajol/browseBy/category">526 Journals</a></b>
<br/>including <a href="ajol/browseBy/alpha?letter=oa"><b>267 Open Access Journals</b></a>
       <a href="ajol/browseBy/alpha?letter=oa"><img src="/templates/images/icons/Open-access-full.png" style="height:14px"/></a>
<br/><br/>
      The site has <b>15 304 Issues</b>
<br/>containing <b>184 800 Abstracts</b>
<br/>with <b>178 467 Full Text Articles</b> for download
      <br/>of which <b>119 435</b> are <b>Open Access</b>
       <a href="ajol/browseBy/alpha?letter=oa"><img src="/templates/images/icons/Open-access-full.png" style="height:14px"/></a>
</div>
<div style="clear:both"></div>
<div style="float:left;width:260px;">
<p><b>HOW TO USE AJOL:</b></p>
<ul style="margin-top:-10px;margin-left:10px;">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-researchers-can-use-AJOL" target="_blank">for Researchers</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-librarians-can-use-AJOL" target="_blank">for Librarians</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-authors-can-use-AJOL" target="_blank">for Authors</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/FAQ" target="_blank">FAQ's</a></li>
</ul>
</div>
<div style="float:left">
<p><b>RESOURCES:</b></p>
<ul style="margin-top:-10px;margin-left:10px;">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-journals" target="_blank">for Journals</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-researchers" target="_blank">for Researchers</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-authors" target="_blank">for Authors</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-policy-makers" target="_blank">for Policy Makers</a></li>
</ul>
</div>
<div style="clear:both"></div>
<div> </div>
<div class="additional_content" style="margin-top:20px;font-size:12px">
<p>Since <strong>1998</strong>, AJOL has been working for <strong>African research to contribute to African development</strong> by:</p>
<ul>
<li class="show">Massively increasing global visibility of <a href="/index.php/ajol/browseBy/category" rel="noopener" target="_blank">partner journals</a> (AJOL is NOT a publisher, we are a journal aggregator platform - millions of site visits from all over the world)</li>
<li class="show">Massively increasing global access to research published in African journals (millions of article downloads each month via AJOL)</li>
<li class="show">Massively increasing African readership of African research</li>
<li class="show">Quality, cost-saving (free) technical services to African-published journals to help their financial sustainability</li>
<li class="show">Facilitating in-person regional workshops throughout the African continent for journal Editors, Editorial Boards, volunteers and staff</li>
<li class="show">Assisting partner journals strengthen their publications, technically and in accordance with ethically sound research and publishing practices and peer-to-peer learning</li>
<li class="show">Sharing relevant and best practice journal publishing practices (informed by global, regional and nationally accepted standards, and input by hundreds of journal Editors from developing countries)</li>
<li class="show">Assisting to show-case credible and trusted African-published research journals (via free assessment and feedback to journal partners and readers (<a href="https://www.journalquality.info/en/" rel="noopener" target="_blank">www.journalquality.info</a>))</li>
<li class="show">Supporting and encouraging Open Access, and fee-free publication models</li>
<li class="show">Providing access to a wide range of free resources to assist African journals, researchers and authors</li>
</ul>
<div style="border:1px solid #E6E2C5;padding:1.5em;margin:2em;">
AJOL is a member of the <a href="https://publicationethics.org/" target="_blank">Committee on Publication Ethics (COPE)</a>
<br/>AJOL is an <a href="https://www.crossref.org/board-and-governance" target="_blank">elected member</a> of the <a href="https://www.crossref.org/" target="_blank">CrossRef</a> Board of Directors
<br/>AJOL is a <a href="http://db.aflia.net/" target="_blank">member</a> of <a href="https://web.aflia.net/" target="_blank">African Library and Information Associations and Institutions (AfLIA)</a>
<br/>AJOL serves on the <a href="http://www.sparcafricasymp.uct.ac.za/international-advisory-committee" target="_blank">International Advisory Committee for SPARC Africa</a>
<br/>AJOL is a past member of the <a href="https://doaj.org/" target="_blank">Directory of Open Access Journals (DOAJ)</a> Advisory Committee
</div>
</div>
</div><!-- .page -->
<div class="additional_content" style="padding-top:0px;padding-bottom:0px;">
<h3>Join the AJOL conversation:</h3>
<div style="float:left;margin:10px;width:320px;border:1px solid #e0e0e0;">
<div class="fb-page" data-adapt-container-width="true" data-height="350" data-hide-cover="false" data-href="https://www.facebook.com/ajol.info" data-show-facepile="true" data-small-header="true" data-tabs="timeline" data-width="320"><blockquote cite="https://www.facebook.com/ajol.info" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ajol.info">African Journals OnLine (AJOL)</a></blockquote></div>
</div>
<div style="float:left;margin:10px;width:320px;border:1px solid #e0e0e0;">
<a class="twitter-timeline" data-border-color="#6C6B3D" data-chrome="noheader nofooter" data-height="350" data-link-color="#6C6B3D" data-theme="light" data-width="320" href="https://twitter.com/AJOLinfo?ref_src=twsrc%5Etfw">Tweets by AJOLinfo</a>
<script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js"></script>
</div>
</div>
<div style="clear:both"></div>
</div><!-- pkp_structure_main -->
<div aria-label="Sidebar" class="pkp_structure_sidebar left" role="complementary">
<div style="font-size: small; margin-left:5px;margin-top:0px;">
<a href="https://www.ajol.info"><img id="AJOLsiteLogoImg" src="https://www.ajol.info/public/site/pageHeaderTitleImage_en_US.jpg" style="border:0;margin-top:0px;"/></a>
<p><b>HOW TO USE AJOL:</b></p>
<ul style="margin-top:-10px">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-researchers-can-use-AJOL" target="_blank">for Researchers</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-librarians-can-use-AJOL" target="_blank">for Librarians</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/how-authors-can-use-AJOL" target="_blank">for Authors</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/FAQ" target="_blank">FAQ's</a></li>
</ul>
<p><b>RESOURCES:</b></p>
<ul style="margin-top:-10px">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-journals" target="_blank">for Journals</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-researchers" target="_blank">for Researchers</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-authors" target="_blank">for Authors</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/resources-for-policy-makers" target="_blank">for Policy Makers</a></li>
</ul>
<p><b>GENERAL:</b></p>
<ul style="margin-top:-10px">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/about-Open-Access" target="_blank">about Open Access</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/JPPS" target="_blank">Journal Quality</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/about-AJOL-African-Journals-Online" target="_blank">More about AJOL</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/AJOL-partners" target="_blank">AJOL's Partners</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/terms-and-conditions" target="_blank">Terms and Conditions of Use</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/contact-AJOL" target="_blank">Contact AJOL</a></li>
</ul>
<div id="dialog" style="width:600px;" title="Donate with Paypal">
<p><span>AJOL is a Non Profit Organisation that cannot function without donations. </span></p>
<p><span>AJOL and the millions of African and international researchers who rely on our free services are deeply grateful for your contribution.</span></p>
<p><span>AJOL is annually audited and was also independently assessed in 2019 by E&amp;Y. </span></p>
<p><span>Your donation is guaranteed to directly contribute to Africans sharing their research output with a global readership.</span></p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" style="width:180px;padding:20px;" target="_blank">
<input name="cmd" type="hidden" value="_s-xclick"/>
<input name="hosted_button_id" type="hidden" value="ZDRWB42VKUHTG"/>
<input alt="Donate with PayPal button" border="0" name="submit" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" title="PayPal - The safer, easier way to pay online!" type="image"/>
<img alt="" border="0" height="1" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1"/>
</form>
<p><span>Please use the link above to donate via Paypal.</span></p>
</div>
<div style="width: 180px;border: solid 1px #A29A47;color: #663;padding:5px 5px 5px 5px;margin:20px">
<div style="text-align:center;width:100%;margin-left auto;margin-right:auto;margin-bottom:2px;font-size:11px;font-family:Arial, Helvetica;">
                 AJOL is a non-profit, relying on your support.
                 <br/>
<img id="opener" src="/public/site/paypal.png" style="cursor: pointer;"/>
</div>
</div>
</div>
<div style="font-size: small; margin-left:5px">
<p><b>526 African Journals</b></p>
<ul style="margin-top:-10px;">
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/browseBy/category"><span id="browseByCat">By Category</span></a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/browseBy/alpha">Alphabetically</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/browseBy/country">By Country</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/browseBy/alpha?letter=all">List All Titles</a></li>
<li class="rightnav"><a href="https://www.ajol.info/index.php/ajol/browseBy/alpha?letter=oa">Free To Read Titles</a> <img alt="This Journal is Open Access" class="icon" src="https://www.ajol.info/templates/images/icons/Open-access.png" style="height:12px" title="This Journal is Open Access"/></li>
</ul>
<div style="clear:both;"></div>
<b>Featuring journals from 32 Countries:</b>
<br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/dz.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=3">Algeria</a> (5)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/bj.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=23">Benin</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/bw.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=28">Botswana</a> (3)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/bf.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=34">Burkina Faso</a> (3)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/cm.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=37">Cameroon</a> (8)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/cg.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=49">Congo, Republic</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ci.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=52">Côte d'Ivoire</a> (4)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/eg.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=63">Egypt, Arab Rep.</a> (14)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/er.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=66">Eritrea</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/sz.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=202">Eswatini</a> (3)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/et.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=68">Ethiopia</a> (30)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/gh.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=82">Ghana</a> (27)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ke.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=110">Kenya</a> (29)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ls.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=119">Lesotho</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ly.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=121">Libya</a> (2)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/mg.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=127">Madagascar</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/mw.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=128">Malawi</a> (4)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/mu.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=136">Mauritius</a> (3)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/mz.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=145">Mozambique</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ng.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=156">Nigeria</a> (222)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/rw.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=177">Rwanda</a> (7)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/sn.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=185">Senegal</a> (6)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/sl.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=187">Sierra Leone</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/za.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=193">South Africa</a> (96)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ss.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=256">South Sudan</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/sd.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=199">Sudan</a> (3)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/tz.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=208">Tanzania</a> (19)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/tg.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=210">Togo</a> (1)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/tn.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=214">Tunisia</a> (2)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/ug.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=219">Uganda</a> (12)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/zm.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=238">Zambia</a> (2)
        <br/>
<img border="0" src="https://www.ajol.info/public/site/iso2/zw.png" style="width:14px;margin-left:10px;"/> 
        <a href="https://www.ajol.info/index.php/ajol/browseBy/country?countryId=239">Zimbabwe</a> (12)
        <br/>
<div style="clear:both;"></div>
</div>
</div><!-- pkp_sidebar.left -->
</div><!-- pkp_structure_content -->
<div class="pkp_structure_footer_wrapper" id="pkp_content_footer" role="contentinfo">
<div class="pkp_structure_footer">
<div class="pkp_footer_content">
<p>email:  info at ajol.info<br/>telephone:  +27 (0) 466 228 058<br/><a aria-describedby="slack-kit-tooltip" class="c-link" href="http://facebook.com/ajol.info" rel="noopener noreferrer" target="_blank">facebook.com/ajol.info</a></p>
<p>CC-BY-SA African Journals Online (AJOL), with the exception of 3rd party content (3rd party content includes <em>inter alia</em> all journal content accessible on or via AJOL. Re-use or sharing of AJOL-hosted journal abstracts and full text articles is not nor has ever been legally permitted unless the journal/s' and/or article/s' displayed copyright and/or license explicitly permits it, or without specific written direct permission from journal/s and/or their publishing entity/ies and/or article author/s if the author/s hold copyright)</p>
<p>African Journals Online (RF) S.A. Non Profit Company (NPC) Registration Number: 2005/033363/08</p>
</div>
<!--div class="pkp_brand_footer" role="complementary">
			<a href="https://www.ajol.info/index.php/ajol/about/aboutThisPublishingSystem">
				<img alt="About this Publishing System" src="https://www.ajol.info/templates/images/ojs_brand.png">
			</a>
		</div>
	</div-->
</div><!-- pkp_structure_footer_wrapper -->
</div><!-- pkp_structure_page -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js?v=3.1.2.4" type="text/javascript"></script><script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js?v=3.1.2.4" type="text/javascript"></script><script src="https://www.ajol.info/lib/pkp/js/lib/jquery/plugins/jquery.tag-it.js?v=3.1.2.4" type="text/javascript"></script><script src="https://www.ajol.info/plugins/themes/default/js/lib/popper/popper.js?v=3.1.2.4" type="text/javascript"></script><script src="https://www.ajol.info/plugins/themes/default/js/lib/bootstrap/util.js?v=3.1.2.4" type="text/javascript"></script><script src="https://www.ajol.info/plugins/themes/default/js/lib/bootstrap/dropdown.js?v=3.1.2.4" type="text/javascript"></script><script src="https://www.ajol.info/plugins/themes/default/js/main.js?v=3.1.2.4" type="text/javascript"></script><script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-267014-1', 'auto');
        ga('send', 'pageview');
        </script>
<script>


  $( document ).ready(function() {
    var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    var isMobileSM = window.matchMedia("only screen and (max-width: 570px)").matches;
    if ((isMobile)||(isMobileSM)) {
      $("#promoting").html("");
      $("#menuhometext").html("HOME");
      $("#menusearchtext").html("SEARCH");
      //$(".pkp_head_wrapper").css("padding-top","0px");
      $(".pkp_search").hide();
      $(".menubullet").css("margin","-2px");
      $("ul.menu li").css("padding-right","8px");
      $("#AJOLsiteLogoImg").css("margin-top","50px");

      $("#browseByCat").html("By Category");
      $("#browseByAlpha").html("Alphabet");
      $("#browseByCountry").html("Country");
      $("#browseByAll").html("All");
      $("#browseByOA").html("Free");
      if (isMobileSM){
        $("ul.menu li").css("padding-right","4px");
        $("ul.menu a").css("font-size","11px");
        $("#navbar a").css("font-size","11px");
      }
    }

    $( "#dialog" ).dialog({
      autoOpen: false,
      width: "60%",
   maxWidth: "768px",
     position: { my: 'top', at: 'top+150' },
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });

    $( "#opener" ).on( "click", function() {
      $( "#dialog" ).dialog( "open" );
    });

 });









  </script>
<link href="/styles/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
</div></body>
</html>
