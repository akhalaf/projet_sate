<!DOCTYPE html>
<html lang="en-CA">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Downtown Eastside Community Action Network – from ideas to action</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://downtowneastsidecan.com/feed/" rel="alternate" title="Downtown Eastside Community Action Network » Feed" type="application/rss+xml"/>
<link href="https://downtowneastsidecan.com/comments/feed/" rel="alternate" title="Downtown Eastside Community Action Network » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/downtowneastsidecan.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://downtowneastsidecan.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://downtowneastsidecan.com/wp-includes/css/dist/block-library/theme.min.css?ver=5.2.9" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://downtowneastsidecan.com/wp-content/themes/twentynineteen-child/style.css?ver=1.0.0" id="twentynineteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://downtowneastsidecan.com/wp-content/themes/twentynineteen/print.css?ver=1.0.0" id="twentynineteen-print-style-css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://downtowneastsidecan.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://downtowneastsidecan.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://downtowneastsidecan.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<link href="https://downtowneastsidecan.com/" rel="canonical"/>
<link href="https://downtowneastsidecan.com/" rel="shortlink"/>
<link href="https://downtowneastsidecan.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdowntowneastsidecan.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://downtowneastsidecan.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdowntowneastsidecan.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
</head>
<body class="home page-template-default page page-id-11 wp-embed-responsive singular image-filters-enabled">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="site-branding-container">
<div class="site-branding">
<p class="site-title"><a href="https://downtowneastsidecan.com/" rel="home">Downtown Eastside Community Action Network</a></p>
<p class="site-description">
				from ideas to action			</p>
</div><!-- .site-branding -->
</div><!-- .site-branding-container -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<section class="content-area" id="primary">
<main class="site-main" id="main">
<article class="post-11 page type-page status-publish hentry entry" id="post-11">
<header class="entry-header">
<h1 class="entry-title">Why</h1>
</header>
<div class="entry-content">
</div><!-- .entry-content -->
</article><!-- #post-11 -->
</main><!-- #main -->
</section><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="site-info">
<a class="site-name" href="https://downtowneastsidecan.com/" rel="home">Downtown Eastside Community Action Network</a>
<a class="site-parent" href="http://quantumideas.com/" target="_blank">Quantum Ideas 2020</a>
<a class="imprint" href="https://en-ca.wordpress.org/">
				Proudly powered by WordPress.			</a>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://downtowneastsidecan.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
</body>
</html>
