<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "84972",
      cRay: "610f77b2fb9ed187",
      cHash: "ecfd375cfb50021",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXZpYXRpb25qb2JzZWFyY2guY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "erR+9OPenBUwywuA/yGSkTaqqGJQ1+wzsNyfVEokRaoWCsL7AAZ1B0R8NLGflUluwmIBC7GESFC8IbOfc+KkcfyUIYaR8OXvFWlVtmcAb8GEw1pfSsok6h5XyZ4PgaAlKeJ8MXIrlu4MI6hKYvfn0DgDS98Alaieo5CeI48YYCAiPmt7W1GG/AtDUbWKr9Ea9/OHR45/iTtFEhWb9cnurETIBtHgP1wFjO7IrFp0W2G/p7sFkQ8pggBHqskTOW65JiM5DGYQzhbEsq1Gt9kj5OVZqB1SuTWnr0NmsNuq2DhUC4wdyTLZO1ybFpAXAla2n3yRiuVngzdFrP7C03NdKnxsrr0+YwCD262k7gESnrtCSvgi9Fo4SD4iI2GMxqE8v3gzUtVp86/JRzvv/igz8EhyDcq6sIxCetVC5+0D9xxahP19MxLzyLImaYq0n2Y71muGMSu8+3DxVpneO9Pe9qPfjdeq9OZYER6iuyWUKA7ZiTYWQc/saVUSkVhDm1tm2FgXvKzgbXEC7XSdRwvxKzAfpkG4QCoXV1fz+hhxAFIV+ZL20s2SJNSm7pp1Z+HMo5ldLuTvIS8XAL/Tdb5mFzhE8wP3QeTk3d07t5Mt6OF5kwv8TFaPq6aCoNYEY/FtXWLyGuBW394nryJM0ZgDJ2BwvkHubsyCdPEmu6KWLPvpGokNGuMXWOE3Ut9S4UG5fMsOEucFiw0ne+YlBifHmaVEp80KZ6TsgGR/+0A8gw7VBcYaaRJ42DR4zIjd9JuG",
        t: "MTYxMDU0NDU4MS41OTkwMDA=",
        m: "JeS5eyOPZ+XZ8ovMNh2Z1TcTNR/1qPaHEBpcvddhLJ8=",
        i1: "9250QJZ8W3EAWhx3MVxYfg==",
        i2: "eWMtbb77FcnL+KzE+NMh7Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "dD0S7QnwCDTjgEb1YxK7qyGzDKigYNtd/VpIKFav/h8=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610f77b2fb9ed187");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> aviationjobsearch.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=0486c286696d888e7f25f204dd73ae16df022479-1610544581-0-Ae5YLo3ApqjEdBcg1BOhseyEd_CHxAGgxAhhtN65asRhkzxofA4c3QoL2ywj21rRx39hzQTQ6wHMrlWH16jx78Gxpfx5y04UKcv9drTkKEhj3MpP2DR3_SgNUUo0TCVtWw7LRyXJ3sWB1Aj-623qdu9oTgN5f7GmbKsOsymUE82PgeJBfeT4N4yqLaylBc5o69oKzODyXeGKX9Vy1-WP5xwbyK5hx3WMJcLJFgfti0cW4szw2otcODuoEdHTIKWXaTe-7zHIshiZHUkDtGPDtU9KxB0UPFsNf08YTS9b_UzxxBkzChcl51FxcEE3OeFn4g" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="8e902ba4218c3e4079fd0e5e3d84a5c2b2f4f74d-1610544581-0-Acj6TE73BYaYhoqz8JB0f2/OwOBNqxuGD9cJveNUUUpu0ADSmcmM238W9LSsZO7lBWSjP4UUlZHLzgIdTnPdoLb0Cft/nFNZDarzQj2W8ZfOzM+d5D7ed7JAFb3bNwhDPO5V2OswcTd3RlRicskrrwTmgeMMQcjp1rJ//EYuIK/bjzu7C75aCdFZtAguhlrQh1jstNB472scyJji7Pj1ElwoXsoZkVznp51k/jglgOS3SU3aItrn2KsA59pDN285f4K6LaAegSQfxi8nsawwAM0GqufNClS6yWyGpaOHGPtUaXnMli8xXH8GKauCcyjKESC9TzS3VdQW9UYcitonk31iRiMvCzY0h9qxfRPHe+lcm5spNXjZLtkUA7tH5idn7b7xUTh+Ub0p5zLT9caq2ANQ6dKSETS/weLOSOPqFKfgQ5HlOgBXSTI8m+ZZPOzKb4tBImVELRRhhVp08QQ53y3T2l5GkvMPSDdcpowgUeShwcY8rMdBO5UWhMOJfsBmtB9aI/dttbvX8oYOSSQNCR9MQdqC3Sf06Y+3vJEpehiE6fvT87Hg14r7HsdhQ3hHS962gQK2nKsFR5/QR2CDMvph7jyCyEZXchEyLFYN98jEM31oBiM4fazDQtNst16x4zBNoFfXdBiNgOPHgxXmRlPKkFe7Ldh3y9E9Bry4xKiQ5I7CgEmM63MrpwjWqZy/cQ7ZSc25svCiOWdBrapsPtWfDVw6MSYkeIEYLJsKme3SxAFI46x3agdT8cob1R7IytHD2CRtplY/7FH2MsdYfWfh9F60/m++zx8JUXfOcqLcKlJo1UAzlnL0O83auUvFSTQaEhSQYamrmrR6jxxMRWHiw/XS4SJ0iZL8CVVsgNlZWlLdKiYSNbg/ftXCwpjbpWNErrkQNhZ3C86R8HNGXfjMgRHQ8k5hQBWC2ipi+K3Jw4RM+k54CnWls1o4IvP8W63Mg1+vV9LFf5SMUbvMxjWYE6SwxUoh1qkRoIUMCI1zrtbvrSpyX06j+i86bis/uOuFKpUhAWXoEBJay3KxtFr7COGFUyNhSur4qejjgfQIx8c1vstntkcMVPNlGSeO8h5ej90eI5hJMb6ibukB4b4Ahw44VzZrqXr66MkNWIOqjahosfcd5rwfLvU9owWSaNC3vAIp31rJChXWkQkZQVqawYjSBEG4LMKvn/EDlUTg9jnc+30SKPd3Uec7ZQI8vBTHxNmByv2hZstTQo0FlN5/wlNk+Iv947zIZ8susHyhmhPm285TSi9luytU3OAaNF3ZG77IH1hUpiNeQStq+QRe9YAq7S0om41GSqRSsdpF7Y2P0ryxyqPEEhT+y8PEhA=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="a7671eec6d9c12b5b96e30ce52357c91"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610544585.599-Fl2rYH8z4w"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610f77b2fb9ed187')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610f77b2fb9ed187</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
