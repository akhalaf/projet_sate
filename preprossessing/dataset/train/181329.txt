<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Chuck Anderson" name="Author"/>
<meta content="Eddie Dean and Lash LaRue" name="Description"/>
<meta content="Eddie Dean, Lash LaRue, Alfred La Rue, Edgar Dean Glosup, Emmett Lynn, Al 'Fuzzy' St. John, Roscoe Ates, Robert Emmett Tansey, Ron Ormond" name="KeyWords"/>
<meta content="Eddie Dean, Lash LaRue, Alfred La Rue, Edgar Dean Glosup, Emmett Lynn, Al 'Fuzzy' St. John, Roscoe Ates, Robert Emmett Tansey, Ron Ormond" name="TARGET"/>
<title>Eddie Dean and Lash LaRue</title>
</head>
<body alink="blue" background="lines.gif" bgcolor="#fffff0" link="blue" text="black" vlink="#008080">
<div align="CENTER"><a href="trio.htm"><img align="MIDDLE" alt="Back to prior page" border="0" height="30" src="pic_a-g/chief-bk.gif" width="60"/></a></div>
<p><br/><br/>
</p><div align="center"><img align="middle" alt="" border="0" height="705" hspace="0" src="pic_a-g/dean0.jpg" vspace="0" width="700"/></div>
<p><br/>
</p><div align="CENTER"><table border="0" cellpadding="0" cellspacing="0" width="700"><tr><td><b>Alfred LaRue (1917? - 1996) and Edgar Glosup (1907 - 1999) were certainly unique individuals ... their backgrounds and experiences were different, almost opposite each other.
<br/><br/>
Somehow, someway, these two performers came together in 1945 at Producers Releasing Corporation (PRC). Eddie Dean was PRC's new 'singing cowboy' and was to begin a series of westerns to be filmed in Cinecolor, a two-strip color process (similar to Republic's TruColor) that was much less expensive than the more traditonal three-strip Technicolor. Dean and LaRue's first film together was Dean's SONG OF OLD WYOMING (PRC, 1945). They would pair up in two more westerns, WILD WEST (PRC, 1946) and THE CARAVAN TRAIL (PRC, 1946), before riding separate trails during the decline of the B western in the post World War II years. (Note: the Dean/LaRue film, PRAIRIE OUTLAWS, was not new, but a shortened, B&amp;W version of WILD WEST.) 
<br/><br/>
A half dozen or so years after SONG OF OLD WYOMING, the starring careers of Eddie Dean and Lash LaRue were over. Collectively, these two men would make about forty <u>starring</u> films in total. But Dean would be remembered as having the greatest singing voice of the movie cowboys. And LaRue would be forever recalled as the black-garbed, bullwhip-cracking, anti-hero who became known as 'Lash'. 
<br/><br/>
Want to read and see more about Eddie Dean and Lash LaRue. Click on the images below, and you'll be transported to their webpages.</b></td></tr></table></div>
<p><br/>
</p><div align="CENTER"><table border="0" cellpadding="0" cellspacing="0" width="700"><tr>
<td width="275"><div align="center"><a href="lash.htm"><img align="MIDDLE" alt="" border="0" height="543" hspace="0" src="pic_a-g/dean0c.jpg" vspace="0" width="264"/><br/><b>Click HERE for Al 'Lash' LaRue</b></a></div></td><td> </td><td width="275"><div align="center"><a href="dean1.htm"><img align="MIDDLE" alt="" border="0" height="544" hspace="0" src="pic_a-g/dean0b.jpg" vspace="0" width="264"/><br/><b>Click HERE for Eddie Dean</b></a></div></td></tr></table></div>
<p><br/>
</p><div align="center"><img align="MIDDLE" alt="" border="0" height="45" src="pic_s-t/triohole.gif" width="174"/></div>
<p><br/>
</p><div align="CENTER"><a href="trio.htm"><img align="MIDDLE" alt="Back to prior page" border="0" height="30" src="pic_a-g/chief-bk.gif" width="60"/></a></div>
</body>
</html>
