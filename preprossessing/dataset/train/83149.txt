<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "25026",
      cRay: "610bea076b4fd952",
      cHash: "729702113100e96",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWZ0YWJjdXJyZW5jeS5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "fq5M4mDyXuEFtj6rYrc+7T56lkzEexsdsnAcrEAXapifcphkVzIkAnb75rSV7fvai6g3p0zsWMHXSN1TromaNzhHHvndFCJwxXot3YBujgkoE/lUI2akpkkZRnKxEeMAofP41eg1ucqUHQPtlE6FuOoC5D1aqge4huvjBNJ9xt+cd3MYmFjFpTc5B3e+lzZ2liB62jHMJXCYt2ZwomEzOYj2223hXcGTi1imnJYpkeWyV0O1jY4Ufq0LWyyYCrNDhJrfL5k6SIYZJ+LiMoijoQjwbg5g+HOg8T7/QZ1/jPpK22/DOI3FJh8vgObO012JdfHVkSMLPOiL8sdrIj0xVAuki9XGcpBEuO7fRsb6WwAVyGhYHouc2mHRH5YTXLfgB1xCRU550h54B8Bza9G3CfWvGT0LMCe/XoRtZhVIWebZ3xM2RpwQjsN6dY80bqMdnAQuGTRUUaR4OCatVrF+Af4A+a7rm+essNx0EU14oFRNcYVqxdIofjXMUF4oaDxWTvBA+U5CNcLaxjlzl2JepOAD9HYxcQuPgUxQzR3gN41jdeWeyxByF9ZH7JIqUinzaGTC1batc7tBkxMqB3sEH0znUenwiKcHCGPdjY/wbajnwxURfwq93l7HGg0EEZ3wofFSt5L7C5hKiQAEFI9q/gEmOwK0B4nOUpOJmMQLeukyNybKWbxOdFFxbzO6LgK4yaUBMzuqlahamXYQLa9VAkk4s0BxljNwA9irgS90Du0R1hRAElWeWGABtdmjcBAw",
        t: "MTYxMDUwNzMyMS41MTAwMDA=",
        m: "hiweU6qXwmbAan3Dt70b32rloiRDMGvlUOSQSSk1Fmo=",
        i1: "7RkO+C8uFVGp67TAiWrqLQ==",
        i2: "+Yvs8STy5h1nyhiSileQ6g==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "mT0HZYVJOnkaS+Z2xjOaE9pksw9PkJsPKwlPm3WTrNU=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.aftabcurrency.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=63f46b69ee95bfd8ce0eb9f41dff37dbe5ec4900-1610507321-0-AWfbHXSlrdpw5_0uHpKqhesycUTAo40m46yB4vlBGCD4YNl-8M10uszKww9EAuFd66aOvfqH9kmhc8DcxSOYK_issK6n5w_W0FkyrEt0h5rNzKUUmZwE8Hyc8LP_hhfpYZBvHGsAjMKTDahR1mwEc_2KtadO_ttK3yOnHlFkWygJi-ZZAw0xGSszx4fkHMF0pMYq4bfltHmGdij9HTV6x8eL5Oo92qQxe9YKfamdwr3P8mLfioDkKsgxIyW4XZK1gVHBY5rjG83yuw1MJAj6W7v9yA5M4pexxjtxH2L7lIEn6jBeXqS-KjEQaQN1shDmVRavZIkV3Edv7vw565HXDdxQDdicDCMUxQmW7PbZFZHR0eFXpM5mkPnIhDX0DF6YNoPiPNbdT8DHtLtfYMPzqGfYdx0j36YRq5AzpKNeQX4Fj3JnWVvQwVIWZ_Lds4WekolleS2v4YQpG0L_ohLa5C3qWTigYqisJOd4Xj2xuhZxa4FatKJxOJelKyapZ21A5I8azrdyPfFnYu9Dl8ISbWI" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="0128d5ea66cc274731fd8d7985c8ce0bc4a1f7fa-1610507321-0-AaEK8okkQE+/JKd2jzhHkAAvocnBZFchXEHuzL5uIAEUERvD4sXj20H9A6o3HvNTKmUExlaPehGFFRuZ6PZkESH0pO9429oYXjzpl4nyIQy/PJfxlq5xrYnmBHJedvNYTJ4px2ekQeuJ7nXpgYELHGaXw588f30XhTFGuN8lPnH9twBqZo7KDKOb9DLrYU6eVnHRWWl7znKWkoJpoqqCDfiib4Ik34ZdBEFr1sm1sU+dvATyUiCqmO+inx3UxCl0fMmaW386o7JoilFGMr0YzxEl+IRrqEEVkMe4mX2fqFx8/JwGIKfR5IJUAyewl9L/u47Nv8rQ2nJxR3th76D0UXtIVUP1HSF0kbeToBSYMfjQyjOwh3ez5kK+2Pnu4Knj4RHAhwKb38sllS6FWsJ/XRfE/bdTXoSnQVaioMsOd6Im+velpGhbq0XT+OVhMuIyJxBGorRuMILNbGFTNZcmqUJCzlaoWWd8M3OVeeVoW3HsSW+A9sTBDvOf4eUUa4km96tdjDrlGG5h9ULp3hTQm8/UaqYbx92otANbCPqvKG2IjQRfbArJvonkarGLFtkB5FkSoTnNZZokBQWuRLkSp4rjpIV3Q7J77TQ4wNlEFGrPLb5s4nV8e/T6ppfXtpsOO9+0kuzRHeY4oBgytrKqbU2yxidZ2ENyR9E87hW8Z4eOxK0UJNhfKQuPy8kOMsSvEaAiJFTtsr8/rnwjeR6DU+lpZV+oNaGYywrEt9zjF7wr+d2v4vWdo3+zMm+KVKSlJGq8qcuEgFhL4lICHMVV7cX8yig31Z/RN/P8USSCjK2NAVR+5LyBA55ozOgw+u8TqXcPMgaqBmt6WXrqhhGdTtK+CTxJS+pwRIrUxS7f1KGzd2RXXIIbpqGyQBaWNgQSolGAuDUcZLaIIuciZChxxK4nIyGAkxSLwtBPoW3Bj71gd8duvpp3qGCUtLdiV/3B7zdmIf6XwFvMBAgflGZpFTSTYjQvPMgr8tuVWLYFdWMn5R0hvr6gl9ZdD/jMXLDaTeu/Mbcj+ivs5PD679qraAJ1EhkrhyWQOSvq0w0Y9dIlfSoPpTCUK8U0qLgwkakbMNqXriHavDvRID5H7uUXBKzpP+MulapX57MSWH1Rxg+Q/4V2LTAPikJn/uMbOcDdhZgr4tzBZxMCc1rZ8O5AbIKFKstQp62KNqjcc4ZQaMz6onHQZedFxVJpPzrPIP9DScpWIqbsvnUgZVECuw3xtUzhqHGHdDrKOeJHLKvyJxBo60sFZ8VyFkHRwz47JTXPdcQd5GyeSXNmB1swe9QT88fCqtUdlzxhXSsZa6iNU6aQJaIpbTL9IYE8SjK0FDabPQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="985c1734a1134e6d501f98b0a2b0195c"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610bea076b4fd952')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610bea076b4fd952</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
