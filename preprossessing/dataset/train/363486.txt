<!DOCTYPE html>
<html dir="ltr" lang="en" prefix="og: http://ogp.me/ns# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#">
<head>
<link href="http://www.w3.org/1999/xhtml/vocab" rel="profile"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://costacruiseindia.com/" rel="canonical"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://costacruiseindia.com/" rel="shortlink"/>
<meta content="Costa Cruise India" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://costacruiseindia.com/" property="og:url"/>
<meta content="Costa Cruise India" property="og:title"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://costacruiseindia.com/" name="twitter:url"/>
<meta content="Costa Cruise India" name="twitter:title"/>
<meta content="Costa Cruise India" itemprop="name"/>
<meta content="Costa Cruise India" name="dcterms.title"/>
<meta content="Text" name="dcterms.type"/>
<meta content="text/html" name="dcterms.format"/>
<meta content="https://costacruiseindia.com/" name="dcterms.identifier"/>
<link href="https://costacruiseindia.com/sites/default/files/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<title>404 | Costa Cruise India</title>
<link href="https://costacruiseindia.com/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_6zemUaNACzZ5sPLowbJJP0jVAcgeofg1dmXJdb1dfGY.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_TsSYvCCYMK_eBtpG48H27yqxAjP_QvW0zU2KBCIlL_Q.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_3yt-b0M1HNJsvKsXwYjh5TEtX2lJGAvJvtmVAfORYZs.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_QisMYVo6v7CeJ3OglIVTC4R72B4O2nWpmPImIUejESI.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_N-L98XcmDr9vaOS9EYzk7-al70QoyHGIa2tlfnkSz38.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://costacruiseindia.com/sites/default/files/css/css_6_ubaOQmq6a_YPFUVfOL2NXo5GTLNZt-16-DhgR2GTY.css" media="all" rel="stylesheet" type="text/css"/>
<!-- HTML5 element support for IE6-8 -->
<!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
<script src="https://costacruiseindia.com/sites/default/files/js/js_EebRuRXFlkaf356V0T2K_8cnUVfCKesNTxdvvPSEhCM.js"></script>
<script src="//cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://costacruiseindia.com/sites/default/files/js/js_R9UbiVw2xuTUI0GZoaqMDOdX0lrZtgX-ono8RVOUEVc.js"></script>
<script src="//cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.js"></script>
<script src="https://costacruiseindia.com/sites/default/files/js/js_7YDBJRol7HguUdInnmVyN-pElG2nkoa33JQXBZlyHO4.js"></script>
<script>(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-84022814-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("set", "page", "/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);ga("send", "pageview");</script>
<script src="https://costacruiseindia.com/sites/default/files/js/js_VI50aAslTuLK83Od2i42LatMNdrS8RYVp6nBmQhRoiw.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"bootstrap","theme_token":"cCV7gqA4gS_QxKIqUvyS4DIpxJFRMnhFVqjpuTuEfHQ","js":{"sites\/all\/themes\/bootstrap\/js\/bootstrap.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.5\/js\/bootstrap.min.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"\/\/cdn.jsdelivr.net\/qtip2\/2.2.1\/jquery.qtip.min.js":1,"sites\/all\/modules\/tb_megamenu\/js\/tb-megamenu-frontend.js":1,"sites\/all\/modules\/tb_megamenu\/js\/tb-megamenu-touch.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/modules\/qtip\/js\/qtip.js":1},"css":{"modules\/system\/system.base.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/library\/library.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"\/\/cdn.jsdelivr.net\/qtip2\/2.2.1\/jquery.qtip.min.css":1,"sites\/all\/modules\/qtip\/css\/qtip.css":1,"https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/font-awesome\/4.4.0\/css\/font-awesome.min.css":1,"sites\/all\/modules\/tb_megamenu\/css\/bootstrap.css":1,"sites\/all\/modules\/tb_megamenu\/css\/base.css":1,"sites\/all\/modules\/tb_megamenu\/css\/default.css":1,"sites\/all\/modules\/tb_megamenu\/css\/compatibility.css":1,"sites\/all\/modules\/addtoany\/addtoany.css":1,"\/\/cdn.jsdelivr.net\/bootstrap\/3.3.5\/css\/bootstrap.min.css":1,"sites\/all\/themes\/bootstrap\/css\/3.3.5\/overrides.min.css":1,"sites\/all\/themes\/bootstrap\/custom.css":1,"public:\/\/css_injector\/css_injector_1.css":1,"public:\/\/css_injector\/css_injector_2.css":1,"public:\/\/css_injector\/css_injector_3.css":1,"public:\/\/css_injector\/css_injector_4.css":1,"public:\/\/css_injector\/css_injector_5.css":1,"public:\/\/css_injector\/css_injector_6.css":1,"public:\/\/css_injector\/css_injector_7.css":1}},"instances":"{\u0022default\u0022:{\u0022content\u0022:{\u0022text\u0022:\u0022\u0022},\u0022style\u0022:{\u0022tip\u0022:false,\u0022classes\u0022:\u0022\u0022},\u0022position\u0022:{\u0022at\u0022:\u0022bottom right\u0022,\u0022adjust\u0022:{\u0022method\u0022:\u0022\u0022},\u0022my\u0022:\u0022top left\u0022,\u0022viewport\u0022:false},\u0022show\u0022:{\u0022event\u0022:\u0022mouseenter \u0022},\u0022hide\u0022:{\u0022event\u0022:\u0022mouseleave \u0022}},\u0022page_tooltip\u0022:{\u0022content\u0022:{\u0022text\u0022:\u0022\u0022},\u0022style\u0022:{\u0022tip\u0022:{\u0022width\u0022:10,\u0022height\u0022:10,\u0022border\u0022:1,\u0022offset\u0022:0,\u0022corner\u0022:true},\u0022classes\u0022:\u0022 qtip-rounded\u0022},\u0022position\u0022:{\u0022at\u0022:\u0022top center\u0022,\u0022my\u0022:\u0022bottom center\u0022,\u0022viewport\u0022:true,\u0022adjust\u0022:{\u0022method\u0022:\u0022flip\u0022}},\u0022show\u0022:{\u0022event\u0022:\u0022mouseenter focus \u0022},\u0022hide\u0022:{\u0022event\u0022:\u0022mouseleave \u0022}}}","qtipDebug":"{\u0022leaveElement\u0022:0}","googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"bootstrap":{"anchorsFix":"0","anchorsSmoothScrolling":"0","formHasError":1,"popoverEnabled":1,"popoverOptions":{"animation":1,"html":0,"placement":"right","selector":"","trigger":"click","triggerAutoclose":1,"title":"","content":"","delay":0,"container":"body"},"tooltipEnabled":1,"tooltipOptions":{"animation":1,"html":0,"placement":"auto left","selector":"","trigger":"hover focus","delay":0,"container":"body"}}});</script>
</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-193 node-type-page desktop">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Skip to main content</a>
</div>
<header class="navbar container-fluid navbar-default" id="navbar" role="banner">
<div class="container-fluid">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse" id="navbar-collapse">
<nav role="navigation">
<div class="region region-navigation">
<section class="block block-block clearfix" id="block-block-1">
<div class="call-us">
<div>
<div class="wave"><a href="https://costacruiseindia.com"><img alt="" src="/sites/default/files/images/costa-logo.png"/></a></div>
</div>
<div class="call-info">
<div class="callback"><a href="tel:+91-22-61792300" style="color: #337ab7;"><img alt="" src="/sites/default/files/icon_call.png" style="width: 30px;"/> <span>Call us at +91-22-61792300</span></a></div>
</div>
</div>
</section>
<section class="block block-block clearfix" id="block-block-5">
<div class="call-us-mobile">
<div class="call-us-content">
<div class="wave-mobile"><a href="http://www.costacruiseindia.com/"><img alt="" src="/sites/default/files/images/costa-logo.png"/></a></div>
</div>
<div class="call-info-mobile">
<div class="callback-mobile"><a href="tel:+91-22-61792300" style="color: rgb(51, 122, 183);"><img alt="" src="/sites/default/files/icon_call.png" style="width: 30px;"/></a></div>
</div>
</div>
</section>
</div>
</nav>
</div>
</div>
</header>
<div class="main-container container-fluid">
<header id="page-header" role="banner">
</header> <!-- /#page-header -->
<div class="row">
<section class="col-sm-12">
<a id="main-content"></a>
<h1 class="page-header">404</h1>
<div class="region region-content">
<section class="block block-system clearfix" id="block-system-main">
<article about="/404" class="node node-page clearfix" id="node-193" typeof="foaf:Document">
<header>
<span class="rdf-meta element-hidden" content="404" property="dc:title"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span> </header>
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><div class="error-page">
<div class="error-caption">Sorry, the page you requested could not be found.</div>
<div class="error-img"><img alt="" src="/sites/default/files/404.png"/></div>
<div class="error-msg">Here are some links that should help you get back on track</div>
</div>
</div></div></div> </article>
</section>
<section class="block block-tb-megamenu clearfix" id="block-tb-megamenu-menu-error-page-menu">
<div class="tb-megamenu tb-megamenu-menu-error-page-menu">
<button class="btn btn-navbar tb-megamenu-button" data-target=".nav-collapse" data-toggle="collapse" type="button">
<i class="fa fa-reorder"></i>
</button>
<div class="nav-collapse always-show">
<ul class="tb-megamenu-nav nav level-0 items-5">
<li class="tb-megamenu-item level-1 mega" data-alignsub="" data-caption="" data-class="" data-group="0" data-hidesub="0" data-hidewcol="0" data-id="1766" data-level="1" data-type="menu_item" data-xicon="">
<a href="/" title="Home">
        
    Home          </a>
</li>
<li class="tb-megamenu-item level-1 mega" data-alignsub="" data-caption="" data-class="" data-group="0" data-hidesub="0" data-hidewcol="0" data-id="1767" data-level="1" data-type="menu_item" data-xicon="">
<a href="/discover-costa" title="Discover Costa">
        
    Discover Costa          </a>
</li>
<li class="tb-megamenu-item level-1 mega" data-alignsub="" data-caption="" data-class="" data-group="0" data-hidesub="0" data-hidewcol="0" data-id="1768" data-level="1" data-type="menu_item" data-xicon="">
<a href="/3-nights-cochin-maldives" title="Cruise From India">
        
    Cruise From India          </a>
</li>
<li class="tb-megamenu-item level-1 mega" data-alignsub="" data-caption="" data-class="" data-group="0" data-hidesub="0" data-hidewcol="0" data-id="1769" data-level="1" data-type="menu_item" data-xicon="">
<a href="/3-nights-singapore-malacca-penang-singapore" title="Cruise from Singapore">
        
    Cruise from Singapore          </a>
</li>
<li class="tb-megamenu-item level-1 mega" data-alignsub="" data-caption="" data-class="" data-group="0" data-hidesub="0" data-hidewcol="0" data-id="1770" data-level="1" data-type="menu_item" data-xicon="">
<a href="/send-your-query" title="Send Your Query">
        
    Send Your Query          </a>
</li>
</ul>
</div>
</div>
</section>
</div>
</section>
</div>
</div>
<footer class="footer container-fluid">
<div class="region region-footer">
<section class="block block-block clearfix" id="block-block-2">
<div class="footer-callinfo"><div style="float:left;"><a href="tel:02261792300" style="color: #fff;"><img src="/sites/default/files/icon_call.png" width="30px"/>Call us at +91-22-61792300 </a><a href="mailto:info@costacruiseindia.com" style="color: #fff;"><img src="/sites/default/files/icon_email.png" width="32px"/> Email us at info@costacruiseindia.com </a><a href="https://www.facebook.com/CostaCruiseIndia/" style="color: #fff;" target="_blank"><img src="/sites/default/files/icon_face.png" width="30px"/>  Like us @CostaCruiseIndia</a>
<br/>
<div class="pri-links"><a href="/privacy-policy">Privacy Policy</a>   |    <a href="/terms-conditions">Terms &amp; Conditions</a></div></div>
<div style="float:right; line-height: 12px; font-size: 12px"><div style="vertical-align: bottom; display: inline-block;">This website is managed and run by Lotus Destinations (Representative of Costa Cruises in India)</div> <div style="vertical-align: bottom; display: inline-block;"><!--<a href="http://www.lotusdestinations.com" target="_blank" style="vertical-align: bottom;">--><img src="/sites/default/files/images/lotus-logo.png"/></div></div></div>
</section>
</div>
</footer>
<script src="https://costacruiseindia.com/sites/default/files/js/js_FbpwIZNwgzwEuuL4Q2HOM07BOSCY5LxL_gwSK4ohQBM.js"></script>
</body>
</html>
