<!DOCTYPE html>
<html lang="ru">
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-107347190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-107347190-1');
</script>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="index, follow" name="robots"/>
<meta content="1С-Битрикс, CMS, PHP, bitrix, система управления контентом" name="keywords"/>
<meta content="1С-Битрикс: Управление сайтом" name="description"/>
<link href="/bitrix/cache/css/s1/si/page_52dc0c21aa4dcbf43901005f42403b83/page_52dc0c21aa4dcbf43901005f42403b83_v1.css?1585266595333" rel="stylesheet" type="text/css"/>
<link data-template-style="true" href="/bitrix/cache/css/s1/si/template_7b5c63663abe557cbef70c6b1f09f1aa/template_7b5c63663abe557cbef70c6b1f09f1aa_v1.css?158526653855090" rel="stylesheet" type="text/css"/>
<script src="/bitrix/cache/js/s1/si/template_d8b6d265a1f99afe5f50f407b9bab0bd/template_d8b6d265a1f99afe5f50f407b9bab0bd_v1.js?1585266538758" type="text/javascript"></script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "6bf4cadab362a8b52ccf45938128f948"]); _ba.push(["host", "www.zamisli.pro"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<title>Карта сайта</title>
<link href="favicon.ico" rel="shortcut icon"/>
<script src="/bitrix/templates/si/js/modernizr.js"></script>
<!--	  <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>-->
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
<!--[if lt IE 9]>
    <link rel="stylesheet" href="//rawgit.com/codefucker/finalReject/master/reject/reject.css"/>
    <script src="//rawgit.com/codefucker/finalReject/master/reject/reject.min.js"></script>
    <script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page page_post" id="scrollTop">
<div class="page__item">
<!-- HEADER START -->
<header class="header">
<div class="container">
<div class="row">
<div class="col-xs-2">
<h2 class="logo"><a class="logo__link" href="/"><span class="icon-logo logo__icon"></span><span class="logo__title">Замысли</span></a></h2> </div>
<div class="col-xs-10">
<div class="fr">
<div class="tbl tbl_mg_15">
<div class="tbl__item">
<nav class="nav_fixed">
<ul class="nav_fixed__list rw">
<li class="nav_fixed__item"><a class="nav_fixed__link" href="/projects"><nobr>Инициативы</nobr></a></li>
<li class="nav_fixed__item"><a class="nav_fixed__link" href="/profile.php">Войти</a></li>
</ul>
</nav>
</div>
<div class="tbl__item"><a class="btn btn_white lh_30 btn_pg_35 header__btn_idea" href="/projects/add.php">Есть идея</a></div>
</div>
</div>
</div>
</div>
</div>
</header>
<!-- HEADER END -->
<!-- MAIN START -->
<main class="main">
<div class="container">
<div class="main-content">
<h1 class="main-title">Карта сайта</h1><table class="map-columns">
<tr>
<td>
<ul class="map-level-0">
<li><a href="/projects">Инициативы</a></li>
</ul>
</td>
</tr>
</table>
</div>
</div>
</main>
<!-- MAIN END -->
</div>
<!-- FOOTER START -->
<footer class="footer">
<div class="container">
<div class="row">
<div class="col-xs-4"><a class="write-us" href="mailto:kabarannikov@gmail.com"><span class="icon-mail fz_30"></span>   <span class="write-us__text">Напишите нам</span></a></div>
<div class="col-xs-8">
<div class="copyright">Институт системных проектов, ГАОУ ВО МГПУ. 2014-2018<br/> Копирование и распространение материалов сайта возможно только по согласованию с администратором</div>
</div>
</div>
</div>
</footer>
<!-- FOOTER END --><a class="scroll-top js-scroll-top js-scroll" href="#scrollTop">Вверх</a>
<script src="/bitrix/templates/si/js/vendors.min.js"></script>
<script src="/bitrix/templates/si/js/theme.js"></script>
</body>
</html>