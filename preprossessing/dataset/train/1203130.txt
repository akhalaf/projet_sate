<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.racetn.com/xmlrpc.php" rel="pingback"/>
<title>RaceTN</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.racetn.com/feed/" rel="alternate" title="RaceTN » Feed" type="application/rss+xml"/>
<link href="https://www.racetn.com/comments/feed/" rel="alternate" title="RaceTN » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.racetn.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.2"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.racetn.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.2" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/plugins/coblocks/dist/blocks.style.build.css?ver=1.19.3" id="coblocks-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/plugins/contact-widgets/assets/css/font-awesome.min.css?ver=4.7.0" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/themes/beonepage/layouts/bootstrap.min.css?ver=3.3.6" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/themes/beonepage/layouts/magnific.popup.css?ver=1.0.1" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/themes/beonepage/style.css?ver=5.3.2" id="beonepage-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/themes/beonepage/layouts/responsive.css?ver=1.3.9" id="beonepage-responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/plugins/contact-widgets/assets/css/style.min.css?ver=1.0.1" id="wpcw-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.racetn.com/wp-content/themes/beonepage/inc/kirki/assets/css/kirki-styles.css" id="kirki-styles-beonepage_kirki-css" media="all" rel="stylesheet" type="text/css"/>
<style id="kirki-styles-beonepage_kirki-inline-css" type="text/css">
.full-screen{background-image:url("http://www.racetn.com/wp-content/uploads/2020/01/sunset.jpg");background-position:center center;}.page-header{background-position:center center;}
</style>
<script src="https://www.racetn.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.racetn.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.racetn.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.racetn.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.2" name="generator"/>
<link href="https://www.racetn.com/" rel="canonical"/>
<link href="https://www.racetn.com/" rel="shortlink"/>
<link href="https://www.racetn.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.racetn.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.racetn.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.racetn.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #000000; }
</style>
<link href="https://www.racetn.com/wp-content/uploads/2020/01/cropped-sunset-1-32x32.jpg" rel="icon" sizes="32x32"/>
<link href="https://www.racetn.com/wp-content/uploads/2020/01/cropped-sunset-1-192x192.jpg" rel="icon" sizes="192x192"/>
<link href="https://www.racetn.com/wp-content/uploads/2020/01/cropped-sunset-1-180x180.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.racetn.com/wp-content/uploads/2020/01/cropped-sunset-1-270x270.jpg" name="msapplication-TileImage"/>
</head>
<body class="home page-template-default page page-id-156 custom-background front-page">
<div class="hfeed site" id="page">
<a class="skip-link sr-only" href="#content">Skip to content</a>
<header class="site-header sticky" id="masthead" role="banner">
<div class="container">
<div class="row">
<div class="col-md-12 clearfix">
<div class="site-branding">
<h1 class="site-title"><a href="https://www.racetn.com/" rel="home">RaceTN</a></h1>
</div><!-- .site-branding -->
<span class="mobile-menu" id="mobile-menu"></span>
<nav class="main-navigation" id="site-navigation" role="navigation">
<ul class="menu clearfix" id="primary-menu"><li class=' class="menu-item menu-item-type-custom menu-item-object-custom menu-item-338"' id="menu-item-338"><a href="https://tennesseenationalraceway.com/"><i class="fa fa-"></i>Tennessee National Raceway</a></li>
<li class=' class="menu-item menu-item-type-custom menu-item-object-custom menu-item-339"' id="menu-item-339"><a href="https://moultonspeedway.net/"><i class="fa fa-"></i>Moulton Speeway</a></li>
<li class=' class="menu-item menu-item-type-custom menu-item-object-custom menu-item-340"' id="menu-item-340"><a href="https://www.facebook.com/TNR2012/"><i class="fa fa-"></i>Tennessee National Raceway Facebook</a></li>
<li class=' class="menu-item menu-item-type-custom menu-item-object-custom menu-item-341"' id="menu-item-341"><a href="https://www.facebook.com/Moulton2019/"><i class="fa fa-"></i>Moulton Speedway Facebook</a></li>
</ul> </nav><!-- #site-navigation -->
</div><!-- .col-md-12 -->
</div><!-- .row -->
</div><!-- .container -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="slider slider-parallax nopadding clearfix" id="slider">
<div class="full-screen">
<div class="container slider-caption text-center clearfix">
<h1>Middle Tennessee Dirt Track Racing</h1>
<div class="slider-btn">
<a class="btn btn-light" href="https://store.racetn.com/">Race Gear</a>
</div><!-- .slider-btn -->
</div><!-- .container -->
</div><!-- .full-screen -->
<div class="scroll-down"></div>
</section><!-- #slider -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer clearfix" id="colophon" role="contentinfo">
<div class="site-branding col-md-12 clearfix">
<h1 class="site-title">RaceTN</h1>
</div><!-- .site-branding -->
<div class="site-info col-md-12">
			Copyrights © 2017. All Rights Reserved. Build with <a href="http://betheme.me/" rel="developer" target="_blank">BeTheme</a>.		</div><!-- .site-info -->
</footer><!-- #colophon -->
<div class="go-to-top btn btn-light" id="go-to-top"><i class="fa fa-angle-up"></i></div>
</div><!-- #page -->
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jrespond.min.js?ver=0.10" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/smooth.scroll.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.transit.js?ver=0.9.12" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.easing.min.js?ver=1.3.2" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/isotope.pkgd.min.js?ver=2.2.2" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.nicescroll.min.js?ver=3.6.6" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.smooth.scroll.min.js?ver=1.6.1" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.magnific.popup.min.js?ver=1.0.1" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/jquery.validate.min.js?ver=1.14.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var app_vars = {"ajax_url":"https:\/\/www.racetn.com\/wp-admin\/admin-ajax.php","home_url":"https:\/\/www.racetn.com\/","current_page_url":"https:\/\/www.racetn.com\/","accent_color":"#ffcc00","nonce":"1f04385498"};
/* ]]> */
</script>
<script src="https://www.racetn.com/wp-content/themes/beonepage/js/app.js?ver=1.3.9" type="text/javascript"></script>
<script src="https://www.racetn.com/wp-includes/js/wp-embed.min.js?ver=5.3.2" type="text/javascript"></script>
</body>
</html>
