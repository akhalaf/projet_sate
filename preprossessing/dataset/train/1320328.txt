<!DOCTYPE html>
<html>
<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="content-type"/>
<title></title>
<link href="../css/dw.css" rel="stylesheet" type="text/css"/>
<link href="../css/rollover1.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../lib/comm.js" type="text/javascript"></script>
<script language="javascript" src="../lib/jquery-latest.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	<!--
	var num = 1;

	$J(document).ready(function() {
		setTimeout("setTO();", 4000);
	});

	function setTO() {
		$J("#img_visual" + num).fadeOut("slow");
		if(num < 2) num++; else num = 1;
		$J("#img_visual" + num).fadeIn("slow");

		setTimeout("setTO();", 4000);
	}
	//-->
	</script>
</head>
<body>
<div class="wrap">
<script type="text/javascript">
<!--
var lnbOn = "";
var lnbOnBo = false;
var elID, elnum = "";

$J(document).ready(function() {
	
	$J('.mnb_wrap > li').mouseenter(function() {
		elID = $J(this).attr('id');
		elnum = elID.substring(elID.length, (elID.length-1));
		
		if (lnbOn != elnum)
		{	// 오픈되어 있던 항목 가림
			if ($J('#mnb'+lnbOn).length > 0)
			{
				$J('.snb'+lnbOn).hide();
			}
		}

		if ($J('#mnb'+elnum).length > 0)
		{
			$J('.snb'+elnum).fadeTo('fast', 1.0);
		}		
		
		lnbOn = elnum;
	});
});
//-->
</script>
<div class="header">
<div class="top_wrap">
<div class="top">
<h1><a href="../main/main.php"><img alt="신일미터텍" src="../imgs/comm/logo1.png"/></a></h1>
<ul class="gnb">
<li class="f"><a href="../main/main.php"><img alt="HOME" src="../imgs/comm/gnb1.png"/></a></li>
<li class="l"><a href="../counsel/counsel1.php"><img alt="CONTACT US" src="../imgs/comm/gnb2.png"/></a></li>
</ul>
<ul class="mnb_wrap">
<li id="mnb1"><a class="rollover" href="../intr/intr1.php"><img alt="회사소개" src="../imgs/comm/mnb1.png"/><img alt="회사소개" class="over" src="../imgs/comm/mnb1_on.png"/></a></li>
<li id="mnb2"><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=1"><img alt="제품소개" src="../imgs/comm/mnb2.png"/><img alt="제품소개" class="over" src="../imgs/comm/mnb2_on.png"/></a></li>
<li id="mnb3"><a class="rollover" href="../tech/tech1.php"><img alt="기술자료" src="../imgs/comm/mnb3.png"/><img alt="기술자료" class="over" src="../imgs/comm/mnb3_on.png"/></a></li>
<li id="mnb4"><a class="rollover" href="../Board_01/List.php?BoardNum=1"><img alt="고객센터" src="../imgs/comm/mnb4.png"/><img alt="고객센터" class="over" src="../imgs/comm/mnb4_on.png"/></a></li>
</ul>
</div>
</div>
<div class="snb_wrap">
<div class="snb">
<ul class="snb1">
<li><a class="rollover" href="../intr/intr1.php"><img alt="인사말" src="../imgs/comm/snb1_1.png"/><img alt="인사말" class="over" src="../imgs/comm/snb1_1_on.png"/></a></li>
<li><a class="rollover" href="../intr/intr2.php"><img alt="오시는 길" src="../imgs/comm/snb1_2.png"/><img alt="오시는 길" class="over" src="../imgs/comm/snb1_2_on.png"/></a></li>
</ul>
<ul class="snb2">
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=1"><img alt="수도미터" src="../imgs/comm/snb2_1.png"/><img alt="수도미터" class="over" src="../imgs/comm/snb2_1_on.png"/></a></li>
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=2"><img alt="오일미터" src="../imgs/comm/snb2_2.png"/><img alt="오일미터" class="over" src="../imgs/comm/snb2_2_on.png"/></a></li>
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=3"><img alt="유/수량계" src="../imgs/comm/snb2_3.png"/><img alt="유/수량계" class="over" src="../imgs/comm/snb2_3_on.png"/></a></li>
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=4"><img alt="난방/적산열량계" src="../imgs/comm/snb2_4.png"/><img alt="난방/적산열량계" class="over" src="../imgs/comm/snb2_4_on.png"/></a></li>
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=5"><img alt="가스미터/기타" src="../imgs/comm/snb2_5.png"/><img alt="가스미터/기타" class="over" src="../imgs/comm/snb2_5_on.png"/></a></li>
<li><a class="rollover" href="../pro/proList.php?C1_Code=1&amp;C2_Code=6"><img alt="시스템" src="../imgs/comm/snb2_6.png"/><img alt="시스템" class="over" src="../imgs/comm/snb2_6_on.png"/></a></li>
</ul>
<ul class="snb3">
<li><a class="rollover" href="../tech/tech1.php"><img alt="수도미터의 분류" src="../imgs/comm/snb3_1.png"/><img alt="수도미터의 분류" class="over" src="../imgs/comm/snb3_1_on.png"/></a></li>
<li><a class="rollover" href="../tech/tech2.php"><img alt="수도미터 설치" src="../imgs/comm/snb3_2.png"/><img alt="수도미터 설치" class="over" src="../imgs/comm/snb3_2_on.png"/></a></li>
<li><a class="rollover" href="../tech/tech3.php"><img alt="적산열량계 설치" src="../imgs/comm/snb3_3.png"/><img alt="적산열량계 설치" class="over" src="../imgs/comm/snb3_3_on.png"/></a></li>
<li><a class="rollover" href="../tech/tech4.php"><img alt="대형수도미터 설치" src="../imgs/comm/snb3_4.png"/><img alt="대형수도미터 설치" class="over" src="../imgs/comm/snb3_4_on.png"/></a></li>
<li><a class="rollover" href="../tech/tech5.php"><img alt="플랜지 규격" src="../imgs/comm/snb3_5.png"/><img alt="플랜지 규격" class="over" src="../imgs/comm/snb3_5_on.png"/></a></li>
</ul>
<ul class="snb4">
<li><a class="rollover" href="../Board_01/List.php?BoardNum=1"><img alt="공지사항" src="../imgs/comm/snb4_1.png"/><img alt="공지사항" class="over" src="../imgs/comm/snb4_1_on.png"/></a></li>
<li><a class="rollover" href="../counsel/counsel1.php"><img alt="온라인 견적" src="../imgs/comm/snb4_2.png"/><img alt="온라인 견적" class="over" src="../imgs/comm/snb4_2_on.png"/></a></li>
<li><a class="rollover" href="../Board_01/List.php?BoardNum=2"><img alt="게시판" src="../imgs/comm/snb4_3.png"/><img alt="게시판" class="over" src="../imgs/comm/snb4_3_on.png"/></a></li>
</ul>
</div>
</div>
</div>
<div class="content">
<div class="ct_l">
<img alt="" src="../imgs/main/img_main1.jpg"/>
</div>
<div class="ct_r">
<div class="img_tit" style="position:relative;height:27px;width:437px;overflow:hidden">
<img alt="Shinil Flow Meter Tech" id="img_visual1" src="../imgs/main/img_tit1.png" style="position:absolute;top:0;left:0"/>
<img alt="Shinil Flow Meter Tech" id="img_visual2" src="../imgs/main/img_tit2.png" style="display:none"/>
</div>
<div class="row1">
<div class="main_board">
<div class="lst_top">
<img alt="News&amp;Notice" class="tit_news" src="../imgs/main/tit_news.png"/>
<a class="more" href="../Board_01/List.php?BoardNum=1"><img alt="" src="../imgs/ico/ico_more.png"/></a>
</div>
<ul class="main_lst">
<li>등록된 정보가 없습니다.</li>
<li> </li>
<li> </li>
<li> </li>
</ul>
</div>
<img alt="전화상담" class="img_call" src="../imgs/main/img_call.png"/>
</div>
<div class="row2">
<a class="ban1" href=""></a>
<a class="ban2" href="../counsel/counsel1.php"></a>
</div>
<div class="row3">
<img alt="Quick Link" class="tit_quick" src="../imgs/main/tit_quick.png"/>
<ul class="quick_lst">
<li><a href="../Board_01/List.php?BoardNum=1"><img alt="고객센터 바로가기" src="../imgs/main/img_ban2.jpg"/></a></li>
<li><a href="../pro/proList.php?C1_Code=1&amp;C2_Code=1"><img alt="제품소개 바로가기" src="../imgs/main/img_ban3.jpg"/></a></li>
<li><a href="../tech/tech1.php"><img alt="기술자료 바로가기" src="../imgs/main/img_ban4.jpg"/></a></li>
<li class="l"><a href="../intr/intr2.php"><img alt="오시는길 바로가기" src="../imgs/main/img_ban5.jpg"/></a></li>
</ul>
</div>
</div>
</div>
<div class="footer">
<div class="ft_wrap">
<h2><img alt="신일미터텍" src="../imgs/comm/logo2.png"/></h2>
<div class="copy">
<address>서울시 금천구 시흥3동 984번지 시흥유통상가 26동 138호<span class="line"> </span>tel: 02-808-8822  02-803-1964<span class="line"> </span>fax : 02-803-1965<span class="line"> </span>E-mail: <a href="mailto:sifmt@sifmt.co.kr" style="color:#fff;">sifmt@sifmt.co.kr</a></address>
<p>COPYRIGHT(C) Sinil metertech  ALL RIGHT RESERVED.</p>
</div>
</div>
</div>
</div>
</body>
</html>