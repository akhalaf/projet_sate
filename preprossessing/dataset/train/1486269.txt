<!DOCTYPE html>
<html><head>
<title>TURL - Tag does not exist</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="TURL URL Shortener Service" name="description"/>
<meta content="Stephen Olesen" name="author"/>
<link href="https://d1jzv8005r51ub.cloudfront.net/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="https://d1jzv8005r51ub.cloudfront.net/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<script src="https://d1jzv8005r51ub.cloudfront.net/js/libs/modernizr-2.5.2.min.js"></script>
<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<style type="text/css">
	.navbar, .navbar-inner { border-radius: 0px; }
	p { text-align: justify; }
	.form-horizontal .control-label { width: 8em; }
	.form-horizontal .controls { margin-left: 9em; }
	.form-horizontal .form-actions { padding-left: 9em; }
	</style>
</head>
<body>
<div class="navbar">
<div class="navbar-inner">
<div class="container">
<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>
<a class="brand" href="/">TURL<span style="color:#dde">.ca</span></a>
<div class="nav-collapse">
<ul class="nav" id="navigation">
<li><a href="/">Create</a></li>
<li><a href="/apidoc.php">API</a></li>
<li><a href="/about.php">About</a></li>
<li><a href="/stats.php">Statistics</a></li>
</ul>
<ul class="nav pull-right"><li><a href="/login.php">Sign In</a></li></ul> </div>
</div>
</div>
</div>
<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
<div class="alert alert-error alert-block">
<p>Sorry, that tag does <b>not</b> exist!</p>
<p>Perhaps you want to <a href="https://turl.ca/?tag=YWFhLWlubm92YXRpb25z">create a tiny URL</a>?
	</p>
</div>
<div class="row-fluid">
<div class="span8">
<div class="">
<form action="/index.php" method="get">
<fieldset>
<legend>Create a TURL</legend>
<div class="control-group">
<label class="control-label" for="url" style="font-weight:bold">URL:</label>
<div class="controls">
<input id="url" name="url" style="width:100%" type="text" value=""/>
<p class="help-block">Simply enter the URL you wish to shorten down in the blank above. This service will take it, shorten it, and provide you with a new URL to share with others.</p>
</div>
</div>
<div class="control-group">
<label class="control-label" for="url" style="font-weight:bold">Tag:</label>
<div class="controls">
<input id="tag" name="tag" size="20" type="text" value="aaa-innovations"/>
<p class="help-block">You may wish to apply a specific tag for your new, short URL. This lets you customize the link or make it something easy to remember for others. If you don't select something, we will create one for you.</p>
</div>
</div>
<div class="form-actions">
<button class="btn btn-primary" type="submit">Create Tiny URL</button>
</div>
</fieldset>
</form>
</div>
</div>
<div class="span4" style="padding-top:2em">
<h4 class="small">What is this?</h4>
<p>TURL is a URL shortener. At this point in the life of the Internet, most people are used to seeing very small addresses for websites. People share these on Twitter, Facebook, and many other places.</p>
<h4 class="small">Why use TURL?</h4>
<p>TURL is simple and to the point. TURL does statistics tracking of how your link is used, allows previewing the URL the user will be redirected to, and other features. At the end of the day, however, it aims to simply take a user to where you want them to go.</p>
<h4 class="small">It could be shorter.</h4>
<p>TURL uses a <i>.ca</i> domain name for a few reasons. One, it is actually short. Two, it doesn't use a foreign country's domain to make a fancy or shorter name.</p>
<p>Proudly written and maintained by <a href="https://slepp.ca">a Canadian</a> for the last 5 years, TURL lets you let others know that <i>.ca</i> isn't a bad thing.</p>
<h4 class="small">Helping TURL grow.</h4>
<p>You can help TURL grow by sharing on your favourite social networking site or just by using it.</p>
<p><a class="twitter-share-button" data-size="small" data-text="Check out TURL!" data-url="https://turl.ca/" data-via="slePP" href="https://twitter.com/share"></a><g:plusone href="https://turl.ca/" size="medium"></g:plusone></p>
</div>
</div>
</div>
</div>
</div>
<div id="other" style="text-align:center;font-size:80%;color:#333;margin-bottom:0.6em">
		Other Projects:	<a href="https://pastebin.ca/">Pastebin</a> | <a href="https://imagebin.ca/">Imagebin</a> | <a href="https://filebin.ca/">Filebin</a>
</div>
<div style="text-align:center;margin-bottom:1em">
<script type="text/javascript"><!--
		google_ad_client = "pub-0367252804969302";
		//TURL leaderboard
		google_ad_slot = "5441384784";
		google_ad_width = 728;
		google_ad_height = 90;
		//--></script>
<script src="//pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript"></script>
</div>
<div class="muted" id="footer" style="background:#dde;text-align:center">
	© <a href="https://slepp.ca/">Stephen Olesen</a>
</div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script type="text/javascript">
          (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
        </script>
<script>
            var _gaq=[['_setAccount','UA-449785-4'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://turl.ca/distinctionrole.php">due-sentence</a></div><a href="https://turl.ca/distinctionrole.php"></a><a href="https://turl.ca/distinctionrole.php"><div style="height: 0px; width: 0px;"></div></a><a href="https://turl.ca/distinctionrole.php"><img border="0" height="1" src="due-sentence.gif" width="1"/></a></body>
</html>
