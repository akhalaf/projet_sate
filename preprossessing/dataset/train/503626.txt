<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.ericandersson.ca/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/js/html5.js"></script>
	<![endif]-->
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Page not found – Eric Andersson</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://www.ericandersson.ca/index.php/feed/" rel="alternate" title="Eric Andersson » Feed" type="application/rss+xml"/>
<link href="https://www.ericandersson.ca/index.php/comments/feed/" rel="alternate" title="Eric Andersson » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.ericandersson.ca\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.ericandersson.ca/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ericandersson.ca/wp-includes/css/dist/block-library/theme.min.css?ver=5.3.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Noto+Sans%3A400italic%2C700italic%2C400%2C700%7CNoto+Serif%3A400italic%2C700italic%2C400%2C700%7CInconsolata%3A400%2C700&amp;subset=latin%2Clatin-ext" id="twentyfifteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/genericons/genericons.css?ver=3.2" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/style.css?ver=5.3.6" id="twentyfifteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/css/blocks.css?ver=20181230" id="twentyfifteen-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentyfifteen-ie-css'  href='https://www.ericandersson.ca/wp-content/themes/twentyfifteen/css/ie.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentyfifteen-ie7-css'  href='https://www.ericandersson.ca/wp-content/themes/twentyfifteen/css/ie7.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<script src="https://www.ericandersson.ca/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.ericandersson.ca/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.ericandersson.ca/index.php/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.ericandersson.ca/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.ericandersson.ca/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<!-- BEGIN ExactMetrics v5.3.10 Universal Analytics - https://exactmetrics.com/ -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-131567569-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- END ExactMetrics Universal Analytics -->
<style id="wp-custom-css" type="text/css">
			.site-info { display: none; }

body, p,  h1, h2, h3, h4, h5, h6 {
 -moz-hyphens: none !important;
-ms-hyphens: none !important;
-webkit-hyphens: none !important;
hyphens: none !important;}		</style>
</head>
<body class="error404 wp-embed-responsive">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="sidebar" id="sidebar">
<header class="site-header" id="masthead" role="banner">
<div class="site-branding">
<p class="site-title"><a href="https://www.ericandersson.ca/" rel="home">Eric Andersson</a></p>
<p class="site-description">M.Eng Mechanical Engineering</p>
<button class="secondary-toggle">Menu and widgets</button>
</div><!-- .site-branding -->
</header><!-- .site-header -->
<div class="secondary" id="secondary">
<nav class="main-navigation" id="site-navigation" role="navigation">
<div class="menu-main-menu-container"><ul class="nav-menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-132" id="menu-item-132"><a href="https://www.ericandersson.ca/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16" id="menu-item-16"><a href="https://www.ericandersson.ca/index.php/projects/">Projects</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123" id="menu-item-123"><a href="https://www.ericandersson.ca/index.php/volunteering-mentorship/">Volunteering &amp; Mentorship</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://www.ericandersson.ca/index.php/contact/">Contact</a></li>
</ul></div> </nav><!-- .main-navigation -->
</div><!-- .secondary -->
</div><!-- .sidebar -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://www.ericandersson.ca/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit screen-reader-text" type="submit" value="Search"/>
</form> </div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- .site-main -->
</div><!-- .content-area -->
</div><!-- .site-content -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info">
<a class="imprint" href="https://wordpress.org/">
				Proudly powered by WordPress			</a>
</div><!-- .site-info -->
</footer><!-- .site-footer -->
</div><!-- .site -->
<script src="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/js/skip-link-focus-fix.js?ver=20141010" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var screenReaderText = {"expand":"<span class=\"screen-reader-text\">expand child menu<\/span>","collapse":"<span class=\"screen-reader-text\">collapse child menu<\/span>"};
/* ]]> */
</script>
<script src="https://www.ericandersson.ca/wp-content/themes/twentyfifteen/js/functions.js?ver=20150330" type="text/javascript"></script>
<script src="https://www.ericandersson.ca/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>
