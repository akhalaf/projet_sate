<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>AhluBayt News Agency - ABNA - Shia News</title>
<meta content="ABNA, News, News Agency, Iran, Qom, Shia, Shiie, Imam, Mohammad, Islam, Islamic" name="news_keywords"/>
<meta content="http://www.abna24.com/intro/" name="twitter:url"/>
<meta content="AhlulBayt News Agenc" name="twitter:title"/>
<meta content="The roots of this oppression and even its further intensification may be sought in the ignorance of Shiite Muslims about each other and unawareness of global communities of such oppressions against Shiites. A large portion of mass media and communication tools is in the hands of rulers and arrogant powers who are fighting in the opposing front against pure Mohammedan Islam." name="twitter:description"/>
<meta content="http://abna24.com/_public/images/logo-abna.jpg" name="twitter:image"/>
<meta content="http://www.abna24.com/intro/" name="DC.Identifier"/>
<meta content="text/html" name="DC.Format"/>
<meta content="AhlulBayt News Agenc" name="DC.Title"/>
<meta content="" name="DC.Creator"/>
<meta content="ABNA, News, News Agency, Iran, Qom, Shia, Shiie, Imam, Mohammad, Islam, Islamic" name="DC.Subject"/>
<meta content="ABNA" name="DC.Publisher"/>
<meta content="" name="DC.Contributor"/>
<meta content="2021-01-14 19:37:22" name="DC.Date"/>
<meta content="text/html" name="DC.Type"/>
<meta content="The roots of this oppression and even its further intensification may be sought in the ignorance of Shiite Muslims about each other and unawareness of global communities of such oppressions against Shiites. A large portion of mass media and communication tools is in the hands of rulers and arrogant powers who are fighting in the opposing front against pure Mohammedan Islam." name="DC.Description"/>
<meta content="2021-01-14 19:37:22" name="DC.Date.X-MetadataLastModified"/>
<meta content="" name="DC.Language"/>
<meta content="AhlulBayt News Agenc" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="http://abna24.com/_public/images/logo-abna.jpg" property="og:image"/>
<meta content="The roots of this oppression and even its further intensification may be sought in the ignorance of Shiite Muslims about each other and unawareness of global communities of such oppressions against Shiites. A large portion of mass media and communication tools is in the hands of rulers and arrogant powers who are fighting in the opposing front against pure Mohammedan Islam." property="og:description"/>
<meta content="http://www.abna24.com/intro/" property="og:url"/>
<meta content="AhlulBayt News Agenc
The roots of this oppression and even its further intensification may be sought in the ignorance of Shiite Muslims about each other and unawareness of global communities of such oppressions against Shiites. A large portion of mass media and communication tools is in the hands of rulers and arrogant powers who are fighting in the opposing front against pure Mohammedan Islam." name="description"/>
<meta content="ABNA, News, News Agency, Iran, Qom, Shia, Shiie, Imam, Mohammad, Islam, Islamic" name="keywords"/>
<meta content="" name="language"/>
<meta content="index, follow" name="googlebot"/>
<meta content="ABNA, News, News Agency, Iran, Qom, Shia, Shiie, Imam, Mohammad, Islam, Islamic" name="tags"/> <meta content="hCQI-pdM9AoQF23No8tzNUapd7WSBOxPb3-5IqGjJlc" name="google-site-verification"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="index, follow" name="robots"/>
<meta content="350935404" name="samandehi"/>
<meta content="473a7af8134e" name="bitly-verification"/>
<link href="/_public/layouts/1/css/default.css?v=1" rel="stylesheet"/>
<link href="/_public/layouts/1/img/favicon.ico" rel="shortcut icon"/>
<link href="http://purl.org/dc/elements/1.1/" rel="schema.DC"/>
<link href="http://purl.org/dc/terms/" rel="schema.DCTERMS"/>
<link href="https://ar.abna24.com/" hreflang="ar" rel="alternate"/>
<link href="https://en.abna24.com/" hreflang="en" rel="alternate"/>
<link href="https://fa.abna24.com/" hreflang="fa" rel="alternate"/>
<link href="https://tr.abna24.com/" hreflang="tr" rel="alternate"/>
<link href="https://ur.abna24.com/" hreflang="ur" rel="alternate"/>
<link href="https://es.abna24.com/" hreflang="es" rel="alternate"/>
<link href="https://id.abna24.com/" hreflang="id" rel="alternate"/>
<link href="https://hi.abna24.com/" hreflang="hi" rel="alternate"/>
<link href="https://fr.abna24.com/" hreflang="fr" rel="alternate"/>
<link href="https://zh.abna24.com/" hreflang="zh" rel="alternate"/>
<link href="https://ru.abna24.com/" hreflang="ru" rel="alternate"/>
<link href="https://de.abna24.com/" hreflang="de" rel="alternate"/>
<link href="https://ms.abna24.com/" hreflang="ms" rel="alternate"/>
<link href="https://ha.abna24.com/" hreflang="ha" rel="alternate"/>
<link href="https://bn.abna24.com/" hreflang="bn" rel="alternate"/>
<link href="https://azl.abna24.com/" hreflang="azl" rel="alternate"/>
<link href="https://azc.abna24.com/" hreflang="azc" rel="alternate"/>
<link href="https://sw.abna24.com/" hreflang="sw" rel="alternate"/>
<link href="https://my.abna24.com/" hreflang="my" rel="alternate"/>
<link href="https://bs.abna24.com/" hreflang="bs" rel="alternate"/>
<link href="https://pt.abna24.com/" hreflang="pt" rel="alternate"/>
<link href="https://ckb.abna24.com/" hreflang="ckb" rel="alternate"/>
<link href="https://ja.abna24.com/" hreflang="ja" rel="alternate"/>
<link href="https://kmr.abna24.com/" hreflang="kmr" rel="alternate"/>
<link href="https://tl.abna24.com/" hreflang="tl" rel="alternate"/> <script charset="utf-8" src="/_public/layouts/1/js/jquery-1.10.2.min.js"></script>
<!--[if lt IE 9]>
            <script src="/_public/layouts/1/js/html5shiv.js"></script>
            <script src="/_public/layouts/1/js/respond.min.js"></script>
            <script src="/_public/layouts/1/js/css3-mediaqueries"></script>
            <link rel="stylesheet" href="/_public/layouts/1/css/ie.css" type="text/css" media="all" />
			<script src="/_public/layouts/1/js/PIE_IE678.js"></script>
		<![endif]-->
</head>
<body>
<div class="row">
<div class="col-lg-8">
<div class="kp-page-header ">
<div class="mt hidden-sm hidden-xs"></div>
<div class="header-middle">
<h1 class="logo" title="ABNA">ABNA World Services</h1>
<h1 class="hidde">AhluBayt News Agency</h1>
<img alt="ABNA World Services" class="img-responsive" src="/_public/layouts/1/images/abna-intro.png"/>
<div class="clear"></div>
</div>
</div>
<!-- kp-page-header -->
<div class="visible-xs" id="headerLanguageList" style="padding:20px 20px">
<a class="langList" dideo-checked="true" href="http://ar.abna24.com/" title="Arabic">
<img alt="Arabic" class="img-responsive" src="/_public/layouts/1/images/language/black/ar.png?v=1.3" style="display:block; width:20%; float:left; "/>
</a>
<a class="langList" dideo-checked="true" href="http://en.abna24.com/" title="English">
<img alt="English" class="img-responsive" src="/_public/layouts/1/images/language/black/en.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://fa.abna24.com/" title="Persian">
<img alt="Persian" class="img-responsive" src="/_public/layouts/1/images/language/white/fa.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://tr.abna24.com/" title="Turkish">
<img alt="Turkish" class="img-responsive" src="/_public/layouts/1/images/language/black/tr.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ur.abna24.com/" title="Urdu">
<img alt="Urdu" class="img-responsive" src="/_public/layouts/1/images/language/black/ur.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://es.abna24.com/" title="Spanish">
<img alt="Spanish" class="img-responsive" src="/_public/layouts/1/images/language/black/es.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://id.abna24.com/" title="Indonesian">
<img alt="Indonesian" class="img-responsive" src="/_public/layouts/1/images/language/black/id.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://hi.abna24.com/" title="Hindi">
<img alt="Hindi" class="img-responsive" src="/_public/layouts/1/images/language/black/hi.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://fr.abna24.com/" title="French">
<img alt="French" class="img-responsive" src="/_public/layouts/1/images/language/black/fr.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://zh.abna24.com/" title="Chinese">
<img alt="Chinese" class="img-responsive" src="/_public/layouts/1/images/language/black/zh.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ru.abna24.com/" title="Russian">
<img alt="Russian" class="img-responsive" src="/_public/layouts/1/images/language/black/ru.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://de.abna24.com/" title="German">
<img alt="German" class="img-responsive" src="/_public/layouts/1/images/language/black/de.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ms.abna24.com/" title="Melayu">
<img alt="Melayu" class="img-responsive" src="/_public/layouts/1/images/language/black/ms.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ha.abna24.com/" title="Hausa">
<img alt="Hausa" class="img-responsive" src="/_public/layouts/1/images/language/black/ha.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://bn.abna24.com/" title="Bengali">
<img alt="Bengali" class="img-responsive" src="/_public/layouts/1/images/language/black/bn.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://azl.abna24.com/" title="Azeri Latin">
<img alt="Azeri Latin" class="img-responsive" src="/_public/layouts/1/images/language/black/azl.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://azc.abna24.com/" style="width:18%; margin-bottom:2px;" title="Azeri Cyrillic">
<img alt="Azeri Cyrillic" class="img-responsive" src="/_public/layouts/1/images/language/black/azc.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://sw.abna24.com/" title="Swahili">
<img alt="Swahili" class="img-responsive" src="/_public/layouts/1/images/language/black/sw.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://my.abna24.com/" title="Myanmar">
<img alt="Myanmar" class="img-responsive" src="/_public/layouts/1/images/language/black/my.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://bs.abna24.com/" title="Bosnian">
<img alt="Bosnian" class="img-responsive" src="/_public/layouts/1/images/language/black/bs.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://pt.abna24.com/" title="Portuguese">
<img alt="Portuguese" class="img-responsive" src="/_public/layouts/1/images/language/black/pt.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ckb.abna24.com/" title="Sourani">
<img alt="Sourani" class="img-responsive" src="/_public/layouts/1/images/language/black/ckb.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://ja.abna24.com/" title="Japanese">
<img alt="Japanese" class="img-responsive" src="/_public/layouts/1/images/language/black/ja.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://kmr.abna24.com/" title="Kurmanji">
<img alt="Kurmanji" class="img-responsive" src="/_public/layouts/1/images/language/black/kmr.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<a class="langList" dideo-checked="true" href="http://tl.abna24.com/" title="Philippines">
<img alt="Philippines" class="img-responsive" src="/_public/layouts/1/images/language/black/tl.png?v=1.3" style="display:block; width:20%; float:left"/>
</a>
<div class="clear clearfix"></div>
</div>
<div class="map hidden-sm hidden-xs">
<div class="languages">
<a class="ar" href="https://ar.abna24.com/" lang="ar">Arabic</a>
<a class="en" href="https://en.abna24.com/" lang="en">English</a>
<a class="fa" href="https://fa.abna24.com/" lang="fa">Persian</a>
<a class="tr" href="https://tr.abna24.com/" lang="tr">Turkish</a>
<a class="ur" href="https://ur.abna24.com/" lang="ur">Urdu</a>
<a class="es" href="https://es.abna24.com/" lang="es">Spanish</a>
<a class="id" href="https://id.abna24.com/" lang="id">Indonesian</a>
<a class="hi" href="https://hi.abna24.com/" lang="hi">Hindi</a>
<a class="fr" href="https://fr.abna24.com/" lang="fr">French</a>
<a class="zh" href="https://zh.abna24.com/" lang="zh">Chinese</a>
<a class="ru" href="https://ru.abna24.com/" lang="ru">Russian</a>
<a class="de" href="https://de.abna24.com/" lang="de">German</a>
<a class="ms" href="https://ms.abna24.com/" lang="ms">Melayu</a>
<a class="ha" href="https://ha.abna24.com/" lang="ha">Hausa</a>
<a class="bn" href="https://bn.abna24.com/" lang="bn">Bengali</a>
<a class="azl" href="https://azl.abna24.com/" lang="az">Azeri Latin</a>
<a class="azc" href="https://azc.abna24.com/" lang="az">Azeri Cyrillic</a>
<a class="sw" href="https://sw.abna24.com/" lang="sw">Swahili</a>
<a class="my" href="https://my.abna24.com/" lang="my">Myanmar</a>
<a class="bs" href="https://bs.abna24.com/" lang="bs">Bosnian</a>
<a class="pt" href="https://pt.abna24.com/" lang="pt">Portuguese</a>
<a class="ckb" href="https://ckb.abna24.com/" lang="ck">Sourani</a>
<a class="ja" href="https://ja.abna24.com/" lang="ja">Japanese</a>
<a class="kmr" href="https://kmr.abna24.com/" lang="km">Kurmanji</a>
<a class="tl" href="https://tl.abna24.com/" lang="tl">Philippines</a>
<!--<a href="//tl.abna24.com" class="tl" lang="tl">Filipino</a>-->
</div>
<img alt="World Map" id="abna-word" src="/_public/layouts/1/images/background/world-back.png"/>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
<div class="col-lg-4 main">
<h3>Latest news in English</h3>
<div class="row">
<div class="col-lg-3 hidden-sm hidden-xs">
<img alt="Photos: Candlelight vigil in commemoration of 11 Hazara Shia martyrs in Quetta" class="img-circle" height="100" src="/cache/image/2021/01/14/cd12b79c27319eeaeb3c66f580377d34.jpg" style="margin-top:25px" width="100"/>
</div>
<div class="col-lg-9">
<ul class="list-unstyled">
<li itemscope="" itemtype="http://schema.org/NewsArticle">
<img alt="Photos: Candlelight vigil in commemoration of 11 Hazara Shia martyrs in Quetta" class=" pull-right hidden-lg hidden-md" src="/cache/image/2021/01/14/cd12b79c27319eeaeb3c66f580377d34.jpg"/>
<h2><a href="https://en.abna24.com/news//photos-candlelight-vigil-in-commemoration-of-11-hazara-shia-martyrs-in-quetta_1105866.html" lang="en">Photos: Candlelight vigil in commemoration of 11 Hazara Shia martyrs in Quetta</a></h2>
<p>AhlulBayt News Agency (ABNA): Candlelight vigil was held in commemoration of 11 Hazara Shia martyrs in Quetta.</p>
<meta content="2021-01-14 13:00:57" itemprop="datePublished"/>
<meta content="https://en.abna24.com/news//photos-candlelight-vigil-in-commemoration-of-11-hazara-shia-martyrs-in-quetta_1105866.html" itemprop="url"/>
<div class="clearfix"></div>
</li>
<li itemscope="" itemtype="http://schema.org/NewsArticle">
<img alt="Lebanon sues Israeli regime in UNSC" class=" pull-right hidden-lg hidden-md" src="/cache/image/2021/01/14/1f551489d1ec99e4128703fec0a187ae.jpg"/>
<h2><a href="https://en.abna24.com/news//lebanon-sues-israeli-regime-in-unsc_1105863.html" lang="en">Lebanon sues Israeli regime in UNSC</a></h2>
<p>Lebanon's Foreign Ministry filed a lawsuit to the United Nations Security Council over the Israeli regime's violation of the Lebanese air space. 
</p>
<meta content="2021-01-14 11:48:26" itemprop="datePublished"/>
<meta content="https://en.abna24.com/news//lebanon-sues-israeli-regime-in-unsc_1105863.html" itemprop="url"/>
<div class="clearfix"></div>
</li>
<li itemscope="" itemtype="http://schema.org/NewsArticle">
<img alt="Photos: Fatiha ceremony at the tombs of 11 Hazara Shiite martyrs in Quetta" class=" pull-right hidden-lg hidden-md" src="/cache/image/2021/01/14/1497e8a9eff5e86ba43424900fc5e708.jpg"/>
<h2><a href="https://en.abna24.com/news//photos-fatiha-ceremony-at-the-tombs-of-11-hazara-shiite-martyrs-in-quetta_1105861.html" lang="en">Photos: Fatiha ceremony at the tombs of 11 Hazara Shiite martyrs in Quetta</a></h2>
<p>AhlulBayt News Agency (ABNA): Fatiha ceremony was held at the tomb of 11 Hazara Shiite martyrs in Quetta, Pakistan</p>
<meta content="2021-01-14 11:14:37" itemprop="datePublished"/>
<meta content="https://en.abna24.com/news//photos-fatiha-ceremony-at-the-tombs-of-11-hazara-shiite-martyrs-in-quetta_1105861.html" itemprop="url"/>
<div class="clearfix"></div>
</li>
<li itemscope="" itemtype="http://schema.org/NewsArticle">
<img alt="Iran has decisive role in Afghan peace process: Shiite figure" class=" pull-right hidden-lg hidden-md" src="/cache/image/2021/01/14/24badd7525d6f49fa2a6eb4e799d7190.jpg"/>
<h2><a href="https://en.abna24.com/news//iran-has-decisive-role-in-afghan-peace-process-shiite-figure_1105860.html" lang="en">Iran has decisive role in Afghan peace process: Shiite figure</a></h2>
<p>Chairman of Hizb-e-Wahdat-e-Islami of Afghanistan termed the Islamic Republic of Iran as a powerful player in the region and a longtime neighbor of Afghanistan and said we consider Tehran's role in the Afghan peace process important and decisive. 
</p>
<meta content="2021-01-14 11:13:26" itemprop="datePublished"/>
<meta content="https://en.abna24.com/news//iran-has-decisive-role-in-afghan-peace-process-shiite-figure_1105860.html" itemprop="url"/>
<div class="clearfix"></div>
</li>
<!--<li><img class="img-responsive" id='apfuesgtfukzwlaoesgt' style='cursor:pointer' onclick='window.open("https://logo.samandehi.ir/Verify.aspx?id=50640&p=dshwobpdgvkaaodsobpd", "Popup","toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30")' alt='logo-samandehi' src='https://logo.samandehi.ir/logo.aspx?id=50640&p=ujynlymawlbqshwllyma'/></li>-->
</ul>
</div>
</div>
</div>
</div>
<script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
        ga('create', 'UA-49437545-1', 'auto');
        ga('send', 'pageview');
        
        	
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-49437545-1']);
          _gaq.push(['_setDomainName', 'www.abna24.com']);
          _gaq.push(['_setAllowLinker', true]);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
<!-- Piwik -->
<!--<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//www.abna24.com/pwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 1]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>-->
</body>
</html>