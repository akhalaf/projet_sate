<!DOCTYPE html>
<html class="col2" lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<title>ベースボールプレイヤードットコム</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="keywords"/>
<meta content="Just another WordPress site" name="description"/>
<link href="https://baseballplayer18.com/" rel="canonical"/>
<link href="https://baseballplayer18.com/page/2/" rel="next"/>
<!-- All in One SEO Pack 3.2.1 によって Michael Torbert の Semper Fi Web Design[110,134] -->
<meta content="プロ野球選手や各球団の情報をまとめたサイトです。 プロ野球選手はもちろんのこと、高校野球などのアマチュア選手の情報も取り上げていくと同時に、各球団の状況や戦力分析にも細かく記事にしていきます。 各選手の評価や年俸、あとは家族構成や彼女や嫁情報、そしてイケメン選手ついても取り上げていくこともあります。" name="description"/>
<meta content="ソフトバンク,ロッテ,楽天,ニュース,西武,巨人,オリックス" name="keywords"/>
<meta content="index,follow" name="robots"/>
<link href="https://baseballplayer18.com/page/2/" rel="next"/>
<script class="aioseop-schema" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://baseballplayer18.com/#organization","url":"https://baseballplayer18.com/","name":"\u30d9\u30fc\u30b9\u30dc\u30fc\u30eb\u30d7\u30ec\u30a4\u30e4\u30fc\u30ba\u30c9\u30c3\u30c8\u30b3\u30e0","sameAs":[]},{"@type":"WebSite","@id":"https://baseballplayer18.com/#website","url":"https://baseballplayer18.com/","name":"\u30d9\u30fc\u30b9\u30dc\u30fc\u30eb\u30d7\u30ec\u30a4\u30e4\u30fc\u30ba\u30c9\u30c3\u30c8\u30b3\u30e0","publisher":{"@id":"https://baseballplayer18.com/#organization"}},{"@type":"WebPage","@id":"https://baseballplayer18.com/#webpage","url":"https://baseballplayer18.com/","inLanguage":"ja","name":"\u30d9\u30fc\u30b9\u30dc\u30fc\u30eb\u30d7\u30ec\u30a4\u30e4\u30fc\u30ba\u30c9\u30c3\u30c8\u30b3\u30e0","isPartOf":{"@id":"https://baseballplayer18.com/#website"},"about":{"@id":"https://baseballplayer18.com/#organization"},"description":"Just another WordPress site"}]}</script>
<link href="https://baseballplayer18.com/" rel="canonical"/>
<meta content="ベースボールプレイヤーズドットコム" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://baseballplayer18.com/" property="og:url"/>
<meta content="https://baseballplayer18.com/wp-content/uploads/2017/03/ベースボールプレイヤーズドットコム.jpg" property="og:image"/>
<meta content="ベースボールプレイヤーズドットコム" property="og:site_name"/>
<meta content="主にプロ野球選手や各球団の情報についてまとめたサイトです。" property="og:description"/>
<meta content="https://baseballplayer18.com/wp-content/uploads/2017/03/ベースボールプレイヤーズドットコム.jpg" property="og:image:secure_url"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="ベースボールプレイヤーズドットコム" name="twitter:title"/>
<meta content="主にプロ野球選手や各球団の情報についてまとめたサイトです。" name="twitter:description"/>
<meta content="https://baseballplayer18.com/wp-content/uploads/2017/03/ベースボールプレイヤーズドットコム.jpg" name="twitter:image"/>
<script type="text/javascript">
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-96628366-1', 'auto');
// Plugins
ga('send', 'pageview');
</script>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<!-- All in One SEO Pack -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://baseballplayer18.com/feed/" rel="alternate" title="ベースボールプレイヤーズドットコム » フィード" type="application/rss+xml"/>
<link href="https://baseballplayer18.com/comments/feed/" rel="alternate" title="ベースボールプレイヤーズドットコム » コメントフィード" type="application/rss+xml"/>
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='https://baseballplayer18.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='toc-screen-css'  href='https://baseballplayer18.com/wp-content/plugins/table-of-contents-plus/screen.min.css?ver=1509' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='wp-polls-css'  href='https://baseballplayer18.com/wp-content/plugins/wp-polls/polls-css.css?ver=2.75.2' type='text/css' media='all' /> -->
<link href="//baseballplayer18.com/wp-content/cache/wpfc-minified/2mok8wzl/2l4vw.css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-polls-inline-css" type="text/css">
.wp-polls .pollbar {
margin: 1px;
font-size: 6px;
line-height: 8px;
height: 8px;
background-image: url('https://baseballplayer18.com/wp-content/plugins/wp-polls/images/default/pollbg.gif');
border: 1px solid #c8c8c8;
}
</style>
<!-- <link rel='stylesheet' id='keni_base-css'  href='https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/base.css?ver=4.9.16' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='keni_rwd-css'  href='https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/rwd.css?ver=4.9.16' type='text/css' media='all' /> -->
<script src="//baseballplayer18.com/wp-content/cache/wpfc-minified/q9k0o9cn/2l4vw.js" type="text/javascript"></script>
<!-- <script type='text/javascript' src='https://baseballplayer18.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script> -->
<!-- <script type='text/javascript' src='https://baseballplayer18.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script> -->
<link href="https://baseballplayer18.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://baseballplayer18.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://baseballplayer18.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<!-- Favicon Rotator -->
<link href="https://baseballplayer18.com/wp-content/uploads/2017/03/ベースボールプレイヤーズドットコム-2-16x16.jpg" rel="shortcut icon"/>
<!-- End Favicon Rotator -->
<script type="text/javascript">
window._wp_rp_static_base_url = 'https://wprp.zemanta.com/static/';
window._wp_rp_wp_ajax_url = "https://baseballplayer18.com/wp-admin/admin-ajax.php";
window._wp_rp_plugin_version = '3.6.4';
window._wp_rp_post_id = '15057';
window._wp_rp_num_rel_posts = '18';
window._wp_rp_thumbnails = true;
window._wp_rp_post_title = '%E3%83%90%E3%83%AC%E3%83%B3%E3%83%86%E3%82%A3%E3%83%B3%E3%81%8C%E3%82%BD%E3%83%95%E3%83%88%E3%83%90%E3%83%B3%E3%82%AF%E7%A7%BB%E7%B1%8D%E3%81%A7%E6%89%93%E9%A0%86%E3%81%AF%E3%81%A9%E3%81%86%E3%81%AA%E3%82%8B%EF%BC%9F%E3%83%99%E3%83%B3%E3%83%81%E6%BF%83%E5%8E%9A%EF%BC%9F';
window._wp_rp_post_tags = ['%E3%83%90%E3%83%AC%E3%83%B3%E3%83%86%E3%82%A3%E3%83%B3', '%E3%82%BD%E3%83%95%E3%83%88%E3%83%90%E3%83%B3%E3%82%AF', '8', '2', '3', 'a', 'alt', '5', 'script', '30'];
window._wp_rp_promoted_content = true;
</script>
<!-- <link rel="stylesheet" href="https://baseballplayer18.com/wp-content/plugins/wordpress-23-related-posts-plugin/static/themes/vertical-m.css?version=3.6.4" /> -->
<link href="//baseballplayer18.com/wp-content/cache/wpfc-minified/e48des7c/2l4vw.css" media="all" rel="stylesheet" type="text/css"/>
<style type="text/css">.broken_link, a.broken_link {
text-decoration: line-through;
}</style><link href="https://baseballplayer18.com/wp-content/uploads/2017/03/cropped-3-100x100.jpg" rel="icon" sizes="32x32"/>
<link href="https://baseballplayer18.com/wp-content/uploads/2017/03/cropped-3-200x200.jpg" rel="icon" sizes="192x192"/>
<link href="https://baseballplayer18.com/wp-content/uploads/2017/03/cropped-3-200x200.jpg" rel="apple-touch-icon-precomposed"/>
<meta content="https://baseballplayer18.com/wp-content/uploads/2017/03/cropped-3-300x300.jpg" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
/*
ここに独自の CSS を追加することができます。
詳しくは上のヘルプアイコンをクリックしてください。
*/
.keikou {
background: linear-gradient(transparent 60%, #ff9 40%);
}
input {
color: #000000;
}
.touhyou {
color: #000000;
}
.single .article-body img,
.page .article-body img {
box-shadow: 0 0 30px rgba(0,0,0,.4);
}
/* 小文字 */
.ten{
font-size: 70%;
}
/* 関連記事装飾 */
.ora {
background-color: #0c1375;
}
.white {
color: #ffffff;
}
.gray {
color: #8A8A8A;
}
.gre {
background-color: #4FB295;
}
/* 画像の影 */
#main img{
box-shadow:rgb(180, 180, 185) 0px 6px 12px 0px;
-webkit-box-shadow:rgb(180, 180, 185) 0px 6px 12px 0px;
-moz-box-shadow:rgb(180, 180, 185) 0px 6px 12px 0px;}		</style>
<!--[if lt IE 9]><script src="https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/js/html5.js"></script><![endif]-->
<meta content="7YV3osNS3DAxWuggWFk0V1dzm17EFu7fdyOrAL9Mepg" name="google-site-verification"/>
<!--アドセンスページ単位-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-1407067078828437",
enable_page_level_ads: true
});
</script>
<!--アドセンスページ単位-->
<!--自動広告-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-1407067078828437",
enable_page_level_ads: true
});
</script>
<!--自動広告-->
</head>
<body class="home blog">
<div class="container">
<header class="site-header " id="top">
<div class="site-header-in">
<div class="site-header-conts">
<h1 class="site-title"><a href="https://baseballplayer18.com"><img alt="ベースボールプレイヤーズドットコム" src="https://baseballplayer18.com/wp-content/uploads/2017/03/ベースボールプレイヤーズドットコム.jpg"/></a></h1>
</div>
</div>
<!--▼グローバルナビ-->
<nav class="global-nav">
<div class="global-nav-in">
<div class="global-nav-panel"><span class="btn-global-nav icon-gn-menu">メニュー</span></div>
<ul id="menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-73"><a href="https://baseballplayer18.com/">ホーム</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6583"><a href="https://baseballplayer18.com/category/amateur-baseball/">アマチュア</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6584"><a href="https://baseballplayer18.com/category/yomiuri-giants/">巨人</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6585"><a href="https://baseballplayer18.com/category/rakuten-eagles/">楽天</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6586"><a href="https://baseballplayer18.com/category/softbank-hawks/">ソフトバンク</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6587"><a href="https://baseballplayer18.com/category/hanshin-tigers/">阪神</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6588"><a href="https://baseballplayer18.com/category/hiroshima-carp/">広島カープ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6589"><a href="https://baseballplayer18.com/category/orix-buffaloes/">オリックス</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6590"><a href="https://baseballplayer18.com/category/chunichi-dragons/">中日</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6591"><a href="https://baseballplayer18.com/category/nipponham-fighters/">日本ハム</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6592"><a href="https://baseballplayer18.com/category/dena-baystars/">横浜DeNA</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6596"><a href="https://baseballplayer18.com/category/yakult-swallows/">ヤクルト</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6597"><a href="https://baseballplayer18.com/category/lotte-marines/">ロッテ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6598"><a href="https://baseballplayer18.com/category/seibu-lions/">西武</a></li>
</ul>
</div>
</nav>
<!--▲グローバルナビ-->
<div class="main-image">
</div>
</header>
<!--▲サイトヘッダー-->
<div class="main-body">
<div class="main-body-in">
<!--▼パン屑ナビ-->
<!--▲パン屑ナビ-->
<!--▼メインコンテンツ-->
<main>
<div class="main-conts">
<article class="section-wrap" id="post-15057">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/softbank-hawks/15057/" title="バレンティンがソフトバンク移籍で打順はどうなる？ベンチ濃厚？">バレンティンがソフトバンク移籍で打順はどうなる？ベンチ濃厚？</a></h2>
<p class="post-date"><time datetime="2019-12-16">2019年12月16日</time></p>
<div class="post-cat">
<span class="cat cat010" style="background-color: #666;"><a href="https://baseballplayer18.com/category/softbank-hawks/" style="color: #FFF;">ソフトバンク</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/softbank-hawks/15057/" title="バレンティンがソフトバンク移籍で打順はどうなる？ベンチ濃厚？"><img alt="バレンティン ソフトバンク 打順" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-246x200.jpg" width="246"/></a></div>
<p>2019年に3年連続日本一となったソフトバンクはヤクルトからFA行使せず、自由契約となっているバレンティン選手を獲得が決定。 シーズン本塁打の日本記録を持つ強力助っ人が加入ということでレギュラー争いはさらに激しくなります・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/softbank-hawks/15057/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14959">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/lotte-marines/14959/" title="【2019年版】ロッテプロテクト・人的補償予想【楽天・美馬学】">【2019年版】ロッテプロテクト・人的補償予想【楽天・美馬学】</a></h2>
<p class="post-date"><time datetime="2019-11-21">2019年11月21日</time></p>
<div class="post-cat">
<span class="cat cat006" style="background-color: #666;"><a href="https://baseballplayer18.com/category/lotte-marines/" style="color: #FFF;">ロッテ</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/lotte-marines/14959/" title="【2019年版】ロッテプロテクト・人的補償予想【楽天・美馬学】"><img alt="ロッテプロテクト・人的補償予想2019" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-246x200.jpg" width="246"/></a></div>
<p>楽天プロテクト・人的補償予想！ 2019年オフ、ロッテは楽天からFA宣言した美馬学投手を獲得しました。 ⇒【2019年版】楽天プロテクト・人的補償予想【ロッテ・鈴木大地】　 FA選手の獲得で生じる補償は、旧所属球団の年俸・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/lotte-marines/14959/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14948">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/rakuten-eagles/14948/" title="【2019年版】楽天プロテクト・人的補償予想【ロッテ・鈴木大地】">【2019年版】楽天プロテクト・人的補償予想【ロッテ・鈴木大地】</a></h2>
<p class="post-date"><time datetime="2019-11-21">2019年11月21日</time></p>
<div class="post-cat">
<span class="cat cat008" style="background-color: #666;"><a href="https://baseballplayer18.com/category/rakuten-eagles/" style="color: #FFF;">楽天</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/rakuten-eagles/14948/" title="【2019年版】楽天プロテクト・人的補償予想【ロッテ・鈴木大地】"><img alt="楽天プロテクト・人的補償予想2019" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-246x200.jpg" width="246"/></a></div>
<p>楽天プロテクト・人的補償予想！ 2019年オフ、楽天はロッテからFA宣言した鈴木大地選手を獲得しました。 ⇒鈴木大地(ロッテ)の嫁の画像や子供は？年俸やネックレスに高校時代も　 FA選手の獲得で生じる補償は、旧所属球団の・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/rakuten-eagles/14948/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14898">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/baseball-news/14898/" title="プロ野球ベストナイン予想2019【パ・リーグ編】候補選手一覧">プロ野球ベストナイン予想2019【パ・リーグ編】候補選手一覧</a></h2>
<p class="post-date"><time datetime="2019-11-07">2019年11月7日</time></p>
<div class="post-cat">
<span class="cat cat071" style="background-color: #666;"><a href="https://baseballplayer18.com/category/baseball-news/" style="color: #FFF;">ニュース</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/baseball-news/14898/" title="プロ野球ベストナイン予想2019【パ・リーグ編】候補選手一覧"><img alt="プロ野球ベストナイン予想2019・パ・リーグ" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/11/79ee17896c8097e9b3f4b542111b32bd-246x200.jpg" width="246"/></a></div>
<p>プロ野球ベストナイン予想2019！ というわけで、2019年のパ・リーグのベストナインを予想します。 ⇒プロ野球ベストナイン予想2019【セ・リーグ編】　 西武の優勝で幕を閉じた2019年のパリーグ。 2018年は西武か・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/baseball-news/14898/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14853">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/baseball-news/14853/" title="プロ野球ベストナイン予想2019【セ・リーグ編】候補選手一覧">プロ野球ベストナイン予想2019【セ・リーグ編】候補選手一覧</a></h2>
<p class="post-date"><time datetime="2019-11-04">2019年11月4日</time></p>
<div class="post-cat">
<span class="cat cat071" style="background-color: #666;"><a href="https://baseballplayer18.com/category/baseball-news/" style="color: #FFF;">ニュース</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/baseball-news/14853/" title="プロ野球ベストナイン予想2019【セ・リーグ編】候補選手一覧"><img alt="プロ野球ベストナイン予想2019・セ・リーグ" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/11/d6a7ff1ea54506b1498fef24228ef0ca-246x200.jpg" width="246"/></a></div>
<p>プロ野球ベストナイン予想2019！ というわけで、2019年のセ・リーグのベストナインを予想します。 ⇒プロ野球ベストナイン予想2019【パ・リーグ編】　 巨人の優勝で幕を閉じた2019年のセ・リーグ。 そのセ・リーグで・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/baseball-news/14853/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14827">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/seibu-lions/14827/" title="秋山翔吾が国内FAなら巨人か楽天？メジャーの評価と残留の可能性は？">秋山翔吾が国内FAなら巨人か楽天？メジャーの評価と残留の可能性は？</a></h2>
<p class="post-date"><time datetime="2019-10-23">2019年10月23日</time></p>
<div class="post-cat">
<span class="cat cat027" style="background-color: #666;"><a href="https://baseballplayer18.com/category/seibu-lions/" style="color: #FFF;">西武</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/seibu-lions/14827/" title="秋山翔吾が国内FAなら巨人か楽天？メジャーの評価と残留の可能性は？"><img alt="秋山翔吾・FA" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-246x200.jpg" width="246"/></a></div>
<p>見事にパ・リーグ2連覇を果たした西武ライオンズ。 山賊打線は主軸の浅村選手が抜けても変わらず健在でしたね。 山川選手や森選手、中村選手にも注目が集まりますが、リードオフマンの秋山選手の活躍は大きかったでしょう。 そんな秋・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/seibu-lions/14827/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14780">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/yomiuri-giants/14780/" title="巨人の歴代人的補償選手の一覧まとめ【当時FAで獲得した選手含む】">巨人の歴代人的補償選手の一覧まとめ【当時FAで獲得した選手含む】</a></h2>
<p class="post-date"><time datetime="2019-10-21">2019年10月21日</time></p>
<div class="post-cat">
<span class="cat cat004" style="background-color: #666;"><a href="https://baseballplayer18.com/category/yomiuri-giants/" style="color: #FFF;">巨人</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/yomiuri-giants/14780/" title="巨人の歴代人的補償選手の一覧まとめ【当時FAで獲得した選手含む】"><img alt="巨人の歴代人的補償選手" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-246x200.jpg" width="246"/></a></div>
<p>プロ野球ストーブリーグの目玉といえばFA移籍。 FA移籍がもっとも多い球団といえば資金力のある巨人ですよね。 ⇒巨人歴代FA補強の一覧まとめ　 FA移籍の権利を取得した選手のランクによっては下記の通り補償制度が設けられて・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/yomiuri-giants/14780/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14728">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/softbank-hawks/14728/" title="福田秀平がFA行使なら移籍先はどこ？候補球団をピックアップ">福田秀平がFA行使なら移籍先はどこ？候補球団をピックアップ</a></h2>
<p class="post-date"><time datetime="2019-10-03">2019年10月3日</time></p>
<div class="post-cat">
<span class="cat cat010" style="background-color: #666;"><a href="https://baseballplayer18.com/category/softbank-hawks/" style="color: #FFF;">ソフトバンク</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/softbank-hawks/14728/" title="福田秀平がFA行使なら移籍先はどこ？候補球団をピックアップ"><img alt="福田秀平・FA" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-246x200.jpg" width="246"/></a></div>
<p>今回は2019年FA市場で注目の選手されてる福田秀平選手。 FA宣言が確実視されている福田秀平選手ですが、複数球団が手を挙げることでしょう。 福田秀平選手のFAランクは「C」ということで、福田秀平選手のような十分な戦力で・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/softbank-hawks/14728/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14671">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/orix-buffaloes/14671/" title="【オリックスの歴代ドラフト1位】活躍選手と失敗選手のまとめ【15選】">【オリックスの歴代ドラフト1位】活躍選手と失敗選手のまとめ【15選】</a></h2>
<p class="post-date"><time datetime="2019-09-19">2019年9月19日</time></p>
<div class="post-cat">
<span class="cat cat040" style="background-color: #666;"><a href="https://baseballplayer18.com/category/orix-buffaloes/" style="color: #FFF;">オリックス</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/orix-buffaloes/14671/" title="【オリックスの歴代ドラフト1位】活躍選手と失敗選手のまとめ【15選】"><img alt="オリックスの歴代ドラフト1位" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-246x200.jpg" width="246"/></a></div>
<p>オリックスの歴代ドラフト1位！ オリックスバファローズからドラフト1位指名で入団した選手の活躍した選手と活躍できなかった代表例を厳選してみました。 オリックスのドラフトといえば、社会人や大卒を中心に獲る即戦力重視の特徴が・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/orix-buffaloes/14671/">続きを読む</a></p>
</div>
</div>
</article>
<article class="section-wrap" id="post-14668">
<div class="section-in">
<header class="article-header">
<h2 class="section-title"><a href="https://baseballplayer18.com/baseball-news/14668/" title="【2019年版】プロ野球MVP予想！セ・パの候補選手を8選">【2019年版】プロ野球MVP予想！セ・パの候補選手を8選</a></h2>
<p class="post-date"><time datetime="2019-09-19">2019年9月19日</time></p>
<div class="post-cat">
<span class="cat cat071" style="background-color: #666;"><a href="https://baseballplayer18.com/category/baseball-news/" style="color: #FFF;">ニュース</a></span>
</div>
</header>
<div class="article-body">
<div class="eye-catch"><a href="https://baseballplayer18.com/baseball-news/14668/" title="【2019年版】プロ野球MVP予想！セ・パの候補選手を8選"><img alt="プロ野球MVP予想2019" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="200" src="https://baseballplayer18.com/wp-content/uploads/2019/09/ad057b0e1befaa9f73070348582c8c5e-246x200.jpg" width="246"/></a></div>
<p>プロ野球MVP予想2019！ 2019シーズンも大詰め、タイトル争いも気になりますが、両リーグで最も活躍した選手に贈られる最優秀選手賞「MVP」の行方も気になりますね。 セ・パそれぞれの候補選手8人をピックアップしMVP・・・</p>
<p class="link-next"><a href="https://baseballplayer18.com/baseball-news/14668/">続きを読む</a></p>
</div>
</div>
</article>
<div class="float-area">
<div class="page-nav-bf">
<ul>
<li class="page-nav-next"><a href="https://baseballplayer18.com/page/2/">以前の記事</a></li>
</ul>
</div>
</div>
</div><!--main-conts-->
</main>
<!--▲メインコンテンツ-->
<!--▼サブコンテンツ-->
<aside class="sub-conts sidebar">
<section class="section-wrap widget-conts widget_search" id="search-2"><div class="section-in"><form action="https://baseballplayer18.com/" id="searchform" method="get">
<div class="search-box">
<input class="search" id="s" name="s" type="text" value=""/><button class="btn-search" id="searchsubmit"><img alt="検索" height="20" src="https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/images/icon/icon-btn-search.png" width="32"/></button>
</div>
</form></div></section><section class="section-wrap widget-conts widget_text" id="text-13"><div class="section-in"><h3 class="section-title">スポンサーリンク</h3> <div class="textwidget"><p><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><br/>
<!-- ベースボールプレイヤーズドットコム（トップ） --><br/>
<ins class="adsbygoogle" data-ad-client="ca-pub-1407067078828437" data-ad-format="auto" data-ad-slot="1349961457" data-full-width-responsive="true" style="display:block"></ins><br/>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></p>
</div>
</div></section><section class="section-wrap widget-conts widget_keni_recent_post" id="keni_recent_post-2"><div class="section-in"><h3 class="section-title">最近の投稿</h3>
<ul class="link-menu-image">
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/softbank-hawks/15057/"><img alt="バレンティン ソフトバンク 打順" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/12/WS005493-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/softbank-hawks/15057/">バレンティンがソフトバンク移籍で打順はどうなる？ベンチ濃厚？</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/lotte-marines/14959/"><img alt="ロッテプロテクト・人的補償予想2019" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005082-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/lotte-marines/14959/">【2019年版】ロッテプロテクト・人的補償予想【楽天・美馬学】</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/rakuten-eagles/14948/"><img alt="楽天プロテクト・人的補償予想2019" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/11/WS005080-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/rakuten-eagles/14948/">【2019年版】楽天プロテクト・人的補償予想【ロッテ・鈴木大地】</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/baseball-news/14898/"><img alt="プロ野球ベストナイン予想2019・パ・リーグ" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/11/79ee17896c8097e9b3f4b542111b32bd-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/11/79ee17896c8097e9b3f4b542111b32bd-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/11/79ee17896c8097e9b3f4b542111b32bd-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/11/79ee17896c8097e9b3f4b542111b32bd-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/baseball-news/14898/">プロ野球ベストナイン予想2019【パ・リーグ編】候補選手一覧</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/baseball-news/14853/"><img alt="プロ野球ベストナイン予想2019・セ・リーグ" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/11/d6a7ff1ea54506b1498fef24228ef0ca-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/11/d6a7ff1ea54506b1498fef24228ef0ca-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/11/d6a7ff1ea54506b1498fef24228ef0ca-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/11/d6a7ff1ea54506b1498fef24228ef0ca-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/baseball-news/14853/">プロ野球ベストナイン予想2019【セ・リーグ編】候補選手一覧</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/seibu-lions/14827/"><img alt="秋山翔吾・FA" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000068-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/seibu-lions/14827/">秋山翔吾が国内FAなら巨人か楽天？メジャーの評価と残留の可能性は？</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/yomiuri-giants/14780/"><img alt="巨人の歴代人的補償選手" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS000050-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/yomiuri-giants/14780/">巨人の歴代人的補償選手の一覧まとめ【当時FAで獲得した選手含む】</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/softbank-hawks/14728/"><img alt="福田秀平・FA" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/10/WS004781-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/softbank-hawks/14728/">福田秀平がFA行使なら移籍先はどこ？候補球団をピックアップ</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/orix-buffaloes/14671/"><img alt="オリックスの歴代ドラフト1位" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-320x320.jpg 320w, https://baseballplayer18.com/wp-content/uploads/2019/09/WS004553-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/orix-buffaloes/14671/">【オリックスの歴代ドラフト1位】活躍選手と失敗選手のまとめ【15選】</a></p>
</li>
<li class="">
<div class="link-menu-image-thumb"><a href="https://baseballplayer18.com/baseball-news/14668/"><img alt="プロ野球MVP予想2019" class="attachment-ss_thumb size-ss_thumb wp-post-image" height="100" sizes="(max-width: 100px) 100vw, 100px" src="https://baseballplayer18.com/wp-content/uploads/2019/09/ad057b0e1befaa9f73070348582c8c5e-100x100.jpg" srcset="https://baseballplayer18.com/wp-content/uploads/2019/09/ad057b0e1befaa9f73070348582c8c5e-100x100.jpg 100w, https://baseballplayer18.com/wp-content/uploads/2019/09/ad057b0e1befaa9f73070348582c8c5e-150x150.jpg 150w, https://baseballplayer18.com/wp-content/uploads/2019/09/ad057b0e1befaa9f73070348582c8c5e-200x200.jpg 200w" width="100"/></a></div>
<p class="post-title"><a href="https://baseballplayer18.com/baseball-news/14668/">【2019年版】プロ野球MVP予想！セ・パの候補選手を8選</a></p>
</li>
</ul>
</div></section><section class="section-wrap widget-conts widget_categories" id="categories-2"><div class="section-in"><h3 class="section-title">カテゴリー</h3> <ul>
<li class="cat-item cat-item-47"><a href="https://baseballplayer18.com/category/amateur-baseball/">アマチュア</a>
</li>
<li class="cat-item cat-item-40"><a href="https://baseballplayer18.com/category/orix-buffaloes/">オリックス</a>
</li>
<li class="cat-item cat-item-10"><a href="https://baseballplayer18.com/category/softbank-hawks/">ソフトバンク</a>
</li>
<li class="cat-item cat-item-71"><a href="https://baseballplayer18.com/category/baseball-news/">ニュース</a>
</li>
<li class="cat-item cat-item-329"><a href="https://baseballplayer18.com/category/mlb/">メジャー</a>
</li>
<li class="cat-item cat-item-14"><a href="https://baseballplayer18.com/category/yakult-swallows/">ヤクルト</a>
</li>
<li class="cat-item cat-item-6"><a href="https://baseballplayer18.com/category/lotte-marines/">ロッテ</a>
</li>
<li class="cat-item cat-item-16"><a href="https://baseballplayer18.com/category/chunichi-dragons/">中日</a>
</li>
<li class="cat-item cat-item-351"><a href="https://baseballplayer18.com/category/samurai-japan/">侍ジャパン</a>
</li>
<li class="cat-item cat-item-4"><a href="https://baseballplayer18.com/category/yomiuri-giants/">巨人</a>
</li>
<li class="cat-item cat-item-32"><a href="https://baseballplayer18.com/category/hiroshima-carp/">広島カープ</a>
</li>
<li class="cat-item cat-item-18"><a href="https://baseballplayer18.com/category/nipponham-fighters/">日本ハム</a>
</li>
<li class="cat-item cat-item-8"><a href="https://baseballplayer18.com/category/rakuten-eagles/">楽天</a>
</li>
<li class="cat-item cat-item-22"><a href="https://baseballplayer18.com/category/dena-baystars/">横浜DeNA</a>
</li>
<li class="cat-item cat-item-393"><a href="https://baseballplayer18.com/category/niform-number/">背番号</a>
</li>
<li class="cat-item cat-item-27"><a href="https://baseballplayer18.com/category/seibu-lions/">西武</a>
</li>
<li class="cat-item cat-item-29"><a href="https://baseballplayer18.com/category/hanshin-tigers/">阪神</a>
</li>
</ul>
</div></section><section class="section-wrap widget-conts widget_text" id="text-4"><div class="section-in"><h3 class="section-title">ブログランキング</h3> <div class="textwidget"><p><a href="//blog.with2.net/link/?1918755:1211"><img src="https://blog.with2.net/img/banner/c/banner_1/br_c_1211_1.gif" title="野球ランキング"/></a><br/><a href="//blog.with2.net/link/?1918755:1211" style="font-size: 0.8em;">野球ランキング</a></p>
</div>
</div></section> </aside>
<!--▲サブコンテンツ-->
</div>
</div>
<!--▼サイトフッター-->
<footer class="site-footer">
<div class="site-footer-in">
<div class="site-footer-conts">
<ul class="site-footer-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6593" id="menu-item-6593"><a href="https://baseballplayer18.com/sitemap/">サイトマップ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6594" id="menu-item-6594"><a href="https://baseballplayer18.com/profile/">運営者情報</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6595" id="menu-item-6595"><a href="https://baseballplayer18.com/contact/">お問い合わせ</a></li>
</ul>
</div>
</div>
<div class="copyright">
<p><small>Copyright (C) 2021 ベースボールプレイヤーズドットコム <span>All Rights Reserved.</span></small></p>
</div>
</footer>
<!--▲サイトフッター-->
<!--▼ページトップ-->
<p class="page-top"><a href="#top"><img alt="ページの先頭へ" class="over" height="80" src="https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/images/common/page-top_off.png" width="80"/></a></p>
<!--▲ページトップ-->
</div><!--container-->
<script type="text/javascript">
jQuery(document).ready(function(){
var wpfcWpfcAjaxCall = function(polls){
if(polls.length > 0){
poll_id = polls.last().attr('id').match(/\d+/)[0];
jQuery.ajax({
type: 'POST', 
url: pollsL10n.ajax_url,
dataType : "json",
data : {"action": "wpfc_wppolls_ajax_request", "poll_id": poll_id, "nonce" : "88d6ef216e"},
cache: false, 
success: function(data){
if(data === true){
poll_result(poll_id);
}else if(data === false){
poll_booth(poll_id);
}
polls.length = polls.length - 1;
setTimeout(function(){
wpfcWpfcAjaxCall(polls);
}, 1000);
}
});
}
};
var polls = jQuery('div[id^=\"polls-\"][id$=\"-loading\"]');
wpfcWpfcAjaxCall(polls);
});
</script><!-- AdSense Manager v4.0.3 (0.492 seconds.) --><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/baseballplayer18.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://baseballplayer18.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var tocplus = {"visibility_show":"\u8868\u793a","visibility_hide":"\u975e\u8868\u793a","width":"Auto"};
/* ]]> */
</script>
<script src="https://baseballplayer18.com/wp-content/plugins/table-of-contents-plus/front.min.js?ver=1509" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var pollsL10n = {"ajax_url":"https:\/\/baseballplayer18.com\/wp-admin\/admin-ajax.php","text_wait":"Your last request is still being processed. Please wait a while ...","text_valid":"Please choose a valid poll answer.","text_multiple":"Maximum number of choices allowed: ","show_loading":"1","show_fading":"1"};
/* ]]> */
</script>
<script src="https://baseballplayer18.com/wp-content/plugins/wp-polls/polls-js.js?ver=2.75.2" type="text/javascript"></script>
<script src="https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/js/socialButton.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://baseballplayer18.com/wp-content/themes/keni70_wp_corp_black_201609051415/js/utility.js?ver=4.9.16" type="text/javascript"></script>
<script src="https://baseballplayer18.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html><!-- WP Fastest Cache file was created in 0.61742401123047 seconds, on 13-01-21 21:09:59 -->