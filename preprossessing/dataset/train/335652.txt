<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Club One Seven Chiang Mai - Gay Guesthouse, Sauna, Gym, Spa.</title>
<link href="https://cluboneseven.net/wp-content/themes/ClubOneSeven/style.css" media="screen" rel="stylesheet" type="text/css"/>
<!--<meta name="nibbler-site-verification" content="b823a3bf851e02906eef5026472ca3d1777140ce" />-->
<meta content="Club, one, seven, gay, sauna, guesthouse, guest, house, gym, chiangmai, chiang, mai, chiengmai " name="keywords"/>
<!-- Main Slideshow Scripts Loaded.  Uses  jQuery Cycle Plugin http://jquery.malsup.com/cycle/ -->
<!-- This site is optimized with the Yoast SEO plugin v12.2 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="A Gay Sauna and Gay Guest house in Chiang Mai. Club One Seven is a fully equipped facility designed for the gay tourist, with pool, massage and spa." name="description"/>
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://cluboneseven.net/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Club One Seven Chiang Mai - Gay Guesthouse, Sauna, Gym, Spa." property="og:title"/>
<meta content="A Gay Sauna and Gay Guest house in Chiang Mai. Club One Seven is a fully equipped facility designed for the gay tourist, with pool, massage and spa." property="og:description"/>
<meta content="https://cluboneseven.net/" property="og:url"/>
<meta content="Club One Seven" property="og:site_name"/>
<meta content="https://cluboneseven.net/wp-content/uploads/2013/05/pool1.jpg" property="og:image"/>
<meta content="https://cluboneseven.net/wp-content/uploads/2013/05/pool1.jpg" property="og:image:secure_url"/>
<meta content="600" property="og:image:width"/>
<meta content="400" property="og:image:height"/>
<meta content="2A8D8B1A85884742642BE15DE5EBF028" name="msvalidate.01"/>
<meta content="QXHcUdjh-I-1vpr8sVBEsO_go-Ukkz9iJKTRERjOBaM" name="google-site-verification"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://cluboneseven.net/#website","url":"https://cluboneseven.net/","name":"Club One Seven","potentialAction":{"@type":"SearchAction","target":"https://cluboneseven.net/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://cluboneseven.net/#primaryimage","url":"https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4.jpg","width":600,"height":400,"caption":"Club One Seven guest House"},{"@type":"WebPage","@id":"https://cluboneseven.net/#webpage","url":"https://cluboneseven.net/","inLanguage":"en-US","name":"Club One Seven Chiang Mai - Gay Guesthouse, Sauna, Gym, Spa.","isPartOf":{"@id":"https://cluboneseven.net/#website"},"primaryImageOfPage":{"@id":"https://cluboneseven.net/#primaryimage"},"datePublished":"2013-05-19T04:56:44+00:00","dateModified":"2019-09-16T09:32:46+00:00","description":"A Gay Sauna and Gay Guest house in Chiang Mai. Club One Seven is a fully equipped facility designed for the gay tourist, with pool, massage and spa."}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://cluboneseven.net/home/feed/" rel="alternate" title="Club One Seven » Club One Seven Chiang Mai Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cluboneseven.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cluboneseven.net/wp-content/plugins/prettyphoto-media/css/prettyPhoto.css?ver=3.1.4" id="prettyphoto-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://cluboneseven.net/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cluboneseven.net/wp-content/plugins/cleaner-gallery/css/gallery.min.css?ver=20130526" id="cleaner-gallery-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://cluboneseven.net/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://cluboneseven.net/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/scripts/chili-1.7.pack.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/scripts/jquery.cycle.all.js?ver=2.9999.5" type="text/javascript"></script>
<script src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/scripts/SlideShow_Main.js?ver=5.2.9" type="text/javascript"></script>
<link href="https://cluboneseven.net/wp-json/" rel="https://api.w.org/"/>
<link href="https://cluboneseven.net/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cluboneseven.net/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<link href="https://cluboneseven.net/" rel="shortlink"/>
<link href="https://cluboneseven.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcluboneseven.net%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://cluboneseven.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fcluboneseven.net%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128223-1', 'clubonesevenchiangmai.com');
  ga('send', 'pageview');

	</script>
</head>
<body class="main-body">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="wrapper">
<div class="top-margin" id="left-top-margin">
</div>
<div id="main-container">
<div id="header">
<div id="Main_Title">
<h1>Club One Seven - Gay Chiang Mai - Guesthouse and Sauna 
					</h1>
</div>
<div id="logo">
<img alt="Club One Seven Chiang Mai" class="main-logo" height="142" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/logo.png" width="525"/>
</div><!-- #logo -->
<nav id="main-navigation">
<div id="main-menu">
<div id="access">
<div class="menu-header"><ul class="menu" id="menu-top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-36" id="menu-item-36"><a aria-current="page" href="https://cluboneseven.net/" title="Club One Seven Chiang Mai – Gay Guest house and Gay Sauna">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-34" id="menu-item-34"><a href="https://cluboneseven.net/guesthouse/" title="Club One Seven Chiang Mai Guest house">Guesthouse</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-363" id="menu-item-363"><a href="https://cluboneseven.net/guesthouse/" title="Club One Seven Chiang Mai Guest house">Guesthouse</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://cluboneseven.net/guesthouse/rooms/" title="Club One Seven Chiang Mai Guest house Rooms">Rooms</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://cluboneseven.net/guesthouse/reservation/" title="Club One Seven Chiang Mai Guest house reservations">Reservation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33"><a href="https://cluboneseven.net/sauna/" title="Club One Seven Chiang Mai Gay Sauna">Sauna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310" id="menu-item-310"><a href="https://cluboneseven.net/massage/" title="Club One Seven Chiang Mai Gay Massage">Massage</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-31" id="menu-item-31"><a href="https://cluboneseven.net/services/" title="Club One Seven Chiang Mai – Services">Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-364" id="menu-item-364"><a href="https://cluboneseven.net/services/" title="Club One Seven Chiang Mai – Services">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://cluboneseven.net/services/dining-caf/" title="Club One Seven Chiang Mai – Dining">Dining Café</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-27" id="menu-item-27"><a href="https://cluboneseven.net/news-and-announcements/">News</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-365" id="menu-item-365"><a href="https://cluboneseven.net/news-and-announcements/" title="Club One Seven Chiang Mai – News and Announcements">Announcements</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://cluboneseven.net/news-and-announcements/guest-book/" title="Club One Seven Chiang Mai – on Trip Advisor">Trip Advisor</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a href="https://cluboneseven.net/gallery/" title="Club One Seven Chiang Mai – Gallery">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-704" id="menu-item-704"><a href="https://cluboneseven.net/news-and-announcements/links/">Links</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://cluboneseven.net/contact/" title="Club One Seven Chiang Mai – Contact and location map">Contact</a></li>
</ul></div> </div>
</div><!-- #main-menu -->
<div id="language-select">
<a href="https://cluboneseven.net/" title="Click for the English Langauge Version">
<img alt="English" height="39" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/englishup.png" width="22"/>
</a><!-- Temporary block for Thai Language select -->
<a href="https://cluboneseven.net/th/" title="Click for the Thai Langauge Version">
<img alt="Thai" height="22" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/thaidown.png" width="22"/>
</a>
<a href="https://cluboneseven.net/cn/" title="Click for the Chinese Langauge Version">
<img alt="Chinese" height="22" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/chinadown.png" width="22"/>
</a>
</div><!-- #language-select -->
</nav><!-- #main-navigation -->
</div> <!-- #header -->
<div id="main-image-container">
<div id="image-left">
</div><!-- #image-left -->
<div id="main-image-slider">
<img alt="Club One Seven Gay Guest House Chiang Mai - Antique Lanna style teakwood house" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide01.jpg" width="660"/>
<img alt="Club One Seven Chiang Mai - penis shaped swimming pool" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide02.jpg" width="660"/>
<img alt="Club One Seven Chiang Mai Guesthouse hostel rooms to stay with friends" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide06.jpg" width="660"/>
<img alt="Club One Seven Gay sauan and gym Massage for men by men Chiang Mai" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide04.jpg" width="660"/>
<img alt="Club One Seven Chiang Mai" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide05.jpg" width="660"/>
<img alt="Club One Seven gay sauna with sexy thai boys in Chiang Mai" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide03.jpg" width="660"/>
<img alt="Club One Seven - sexy thai boys enjoy the pool at gay sauna in Chiang Mai" height="323" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/mainslider/slide07.jpg" width="660"/>
</div> <!-- #main-image-slider -->
<div id="image-right">
</div><!-- #image-right -->
</div> <!-- #main-image-container -->
<div id="content-container">
<div id="post-summary">
<div id="featured-image">
<img alt="Club One Seven Swimming Pool" class="attachment-full size-full" height="150" sizes="(max-width: 456px) 100vw, 456px" src="https://cluboneseven.net/wp-content/uploads/2013/05/intropool.jpg" srcset="https://cluboneseven.net/wp-content/uploads/2013/05/intropool.jpg 456w, https://cluboneseven.net/wp-content/uploads/2013/05/intropool-200x65.jpg 200w, https://cluboneseven.net/wp-content/uploads/2013/05/intropool-300x98.jpg 300w, https://cluboneseven.net/wp-content/uploads/2013/05/intropool-400x131.jpg 400w" width="456"/>
</div><!-- #featured-image -->
<div id="summary-content">
<div class="clear"></div>
<div id="summary-logo">
<img alt="Club One Seven Logo" height="33" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/logo04.png" width="360"/>
</div>
<div class="hidden" id="summary-title">
<h2 class="page-title"> Club One Seven Chiang Mai</h2>
</div>
<div class="clear" id="tagline">
<p>
<span class="excerpt">Located on the banks of the Ping River, 1km from the city centre, is one of Chiang Mai’s hidden secrets. This 130 year old newly refurbished teak wood guesthouse is off the usual tourist track. It is ideally positioned as a refuge and a base from which to discover the riches of Northern Thailand.</span></p>
</div>
</div><!-- #summary-content -->
</div>
<div class="widget-area" id="main-sidebar">
<img alt="Gay Chiang Mai - Club One Seven Guesthouse and Sauna" height="74" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/logo03.png" width="240"/>
<div class="main-sidebar">
<div class="widget-container widget_nav_menu" id="nav_menu-2"><div class="menu-top-menu-container"><ul class="menu" id="menu-top-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-36"><a aria-current="page" href="https://cluboneseven.net/" title="Club One Seven Chiang Mai – Gay Guest house and Gay Sauna">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-34"><a href="https://cluboneseven.net/guesthouse/" title="Club One Seven Chiang Mai Guest house">Guesthouse</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-363"><a href="https://cluboneseven.net/guesthouse/" title="Club One Seven Chiang Mai Guest house">Guesthouse</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="https://cluboneseven.net/guesthouse/rooms/" title="Club One Seven Chiang Mai Guest house Rooms">Rooms</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="https://cluboneseven.net/guesthouse/reservation/" title="Club One Seven Chiang Mai Guest house reservations">Reservation</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://cluboneseven.net/sauna/" title="Club One Seven Chiang Mai Gay Sauna">Sauna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310"><a href="https://cluboneseven.net/massage/" title="Club One Seven Chiang Mai Gay Massage">Massage</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-31"><a href="https://cluboneseven.net/services/" title="Club One Seven Chiang Mai – Services">Services</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-364"><a href="https://cluboneseven.net/services/" title="Club One Seven Chiang Mai – Services">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="https://cluboneseven.net/services/dining-caf/" title="Club One Seven Chiang Mai – Dining">Dining Café</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-27"><a href="https://cluboneseven.net/news-and-announcements/">News</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-365"><a href="https://cluboneseven.net/news-and-announcements/" title="Club One Seven Chiang Mai – News and Announcements">Announcements</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://cluboneseven.net/news-and-announcements/guest-book/" title="Club One Seven Chiang Mai – on Trip Advisor">Trip Advisor</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="https://cluboneseven.net/gallery/" title="Club One Seven Chiang Mai – Gallery">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-704"><a href="https://cluboneseven.net/news-and-announcements/links/">Links</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="https://cluboneseven.net/contact/" title="Club One Seven Chiang Mai – Contact and location map">Contact</a></li>
</ul></div></div><div class="widget-container widget_text" id="text-2"> <div class="textwidget"><div class="other-c17">
<a href="http://cluboneseven.net/home.html" title="Club One Seven Phuket">PHUKET</a>
</div></div>
</div><div class="widget-container widget_text" id="text-3"> <div class="textwidget"><div class="follow-us image35">
<h3>FOLLOW US</h3>
<a href="https://www.facebook.com/pages/Club-One-Seven-Chiang-Mai/130760656962676" title="follow US on Facebook"><img alt="Facebook button" height="35" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/fb35x35.png" width="35"/></a>
<a href="https://twitter.com/@ClubonesevenCM" title="follow US on twitter"> <img alt="Facebook button" height="35" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/twt35x35.png" width="35"/></a>
<a href="http://www.tripadvisor.co.uk/Hotel_Review-g293917-d1792078-Reviews-Club_One_Seven_Chiang_Mai-Chiang_Mai.html" title="follow US on Tripadvisor"> <img alt="Trip Advisor button" height="35" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/ta.png" style="margin-bottom:1px;" width="45"/></a>
</div>
</div>
</div><div class="widget-container widget_text" id="text-4"> <div class="textwidget"><div class="follow-us">
<h3>CHIANG MAI</h3>
<a href="https://cluboneseven.net/chiang-mai/" title="Information about Chiang Mai"> <img alt="Chiang Mai Logo" height="39" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/elephant.png" style="margin-left:0px;" width="52"/></a>
<a href="http://www.gayinchiangmai.com/News/" title="Information about Gay Chiang Mai"> <img alt="Gay Chiang Mai Logo" height="40" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/gayincm40x40.jpg" style="margin-left:5px;" width="40"/></a>
</div></div>
</div> </div>
</div><!-- #main-sidebar -->
<article id="post-content">
<div class="post-5 page type-page status-publish hentry" id="post-5">
<div id="picture-col"><a data-gal="prettyPhoto" href="https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4.jpg" title="Club One Seven guest House"><img alt="Club One Seven guest House" class="alignleft page-image size-medium wp-image-185" height="200" sizes="(max-width: 300px) 100vw, 300px" src="https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4-300x200.jpg" srcset="https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4-300x200.jpg 300w, https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4-200x133.jpg 200w, https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4-400x266.jpg 400w, https://cluboneseven.net/wp-content/uploads/2013/05/guesthouse4.jpg 600w" width="300"/></a><a data-gal="prettyPhoto" href="https://cluboneseven.net/wp-content/uploads/2013/05/pool1.jpg" title="Club One Seven Pool"><img alt="Club One Seven Pool" class="alignleft page-image size-medium wp-image-186" height="200" sizes="(max-width: 300px) 100vw, 300px" src="https://cluboneseven.net/wp-content/uploads/2013/05/pool1-300x200.jpg" srcset="https://cluboneseven.net/wp-content/uploads/2013/05/pool1-300x200.jpg 300w, https://cluboneseven.net/wp-content/uploads/2013/05/pool1-200x133.jpg 200w, https://cluboneseven.net/wp-content/uploads/2013/05/pool1-400x266.jpg 400w, https://cluboneseven.net/wp-content/uploads/2013/05/pool1.jpg 600w" width="300"/></a></div>
<div id="text-col">
<div class="section-title">
<h3 class="page-title">Club One Seven Chiang Mai</h3>
</div>
<p> Club One Seven Chiang Mai is the newest gay facility extending out of One Seven Singapore and Club One Seven Phuket.  Club One Seven Chiang Mai is not only a gay guesthouse but it is also a gay sauna with a unique swimming pool as well as a gay gym, cafe and recently added massage salon.</p>
<p>It is a fully equipped facility designed for the gay traveler. For those wishing to stay at a gay hotel, this is the perfect place. Many of our guests use our place as a base to explore the many temples, markets and surrounding nature that Chiang Mai has to offer. It is also an ideal place to use as a hub for  trips to visit the border with Myanmar as well many of the remote areas in Northern Thailand such as the Golden Triangle and Chiang Rai.</p>
<p>After a day of visiting the sites you can spend the evening meeting new friends in our sauna facility, enjoy dinner by the Ping River and swimming in the pool. You can also have a relaxing massage by one of our skilled masseurs in the privacy of our new massage salon. You don’t have to miss out on your gym routine as we have a fully equipped gym with friendly local members to work out with along side of you.</p>
<p>Our gay guesthouse has 11 different air conditioned rooms in our 2 story traditional Thai teak guesthouse. It is not your usual gay hotel. It is a real Thai experience. You are guaranteed a totally unique vacation with the opportunity to meet friends from not only Thailand but from all over the world in your “Home Away From Home”, Club One Seven Chiang Mai.</p>
</div>
</div>
</article>
</div><!-- #content-container -->
<div class="clear">
</div>
<div id="footer">
<div id="footer-left">
<div class="TA_linkingWidgetRedesign" id="TA_linkingWidgetRedesign114">
<ul class="TA_links bVdSerJWYX" id="ztyf7x2ujIlW" style="list-style:none">
<li class="FiRMe5" id="plkb470icOG">Read reviews of <a href="http://www.tripadvisor.co.uk/Hotel_Review-g293917-d1792078-Reviews-Club_One_Seven_Chiang_Mai-Chiang_Mai.html" target="_blank">Club One Seven Chiang Mai</a></li>
</ul>
</div>
<script src="http://www.jscache.com/wejs?wtype=linkingWidgetRedesign&amp;uniq=114&amp;locationId=1792078&amp;lang=en_UK&amp;border=true"></script>
</div><!-- #footer-left -->
<footer id="footer-center">
<p>385/2 Charoen Prathet Road, Changklan,<br/>
					Muang, Chiang Mai 50100, Thailand<br/>					Tel: +66 (0) 53 274 317, Fax: +66 (0) 53 274 932 <br/>
					Email: chiangmai@cluboneseven.net<br/>
					www.cluboneseven.net</p>
</footer><!-- #footer-center -->
<div id="footer-right">
<a class="Bon-Tong-Logo" href="https://plus.google.com/117876525720363366466/" title="Club One Seven Chiang Mai on Google Plus">
<img alt="Google Plus Logo" height="40" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/gplus-40.png" style="padding-top:15px;" width="40"/></a>
<a class="Bon-Tong-Logo" href="http://instagram.com/C17chiangmai" title="Club One Seven Chiang Mai on Instagram">
<img alt="Instagram Logo" height="40" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/Instagram_Icon_Large.png" style="padding-top:15px;" width="40"/></a>
<a class="Bon-Tong-Logo" href="https://twitter.com/@C17Rob" title="Club One Seven Chiang Mai on Twitter">
<img alt="Twitter Logo" height="40" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/Twitter_logo_blue-40.png" style="padding-top:15px;" width="40"/></a>
<a class="Bon-Tong-Logo" href="https://www.facebook.com/pages/Club-One-Seven-Chiang-Mai/130760656962676" title="Club One Seven Chiang Mai on Facebook">
<img alt="Facebook Logo" height="40" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/fb.png" style="padding-top:15px;" width="40"/></a>
<a class="Bon-Tong-Logo" href="http://www.bontong.com/" title="Website Design by Bon Tong Productions - specialists in website design for gay businesses">
<img alt="Bon Tong Productions - Website Design" height="43" src="https://cluboneseven.net/wp-content/themes/ClubOneSeven/images/btlogo.png" style="padding-top:15px;" width="45"/></a>
</div><!-- #footer-right -->
</div><!-- #footer -->
</div><!-- main-container -->
<div class="top-margin" id="right-top-margin">
</div>
</div><!-- #wrapper -->
<!-- Place this tag after the last +1 button tag. -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	</script>
<script type="text/javascript">
  window.___gcfg = {lang: 'en-GB'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
	</script>
<script src="https://cluboneseven.net/wp-content/plugins/prettyphoto-media/js/jquery.prettyPhoto.min.js?ver=3.1.4" type="text/javascript"></script>
<script src="https://cluboneseven.net/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script>
jQuery(function($) {
$('a[data-gal^="prettyPhoto"]').prettyPhoto();
});
</script>
</body>
</html>
