<!DOCTYPE html>
<html lang="en">
<head>
<title>AppsCase</title>
<link href="images/favicon.ico" rel="shortcut icon" type="image/png"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<!-- for Google -->
<meta content="Our main focus is on creating great products for Firefox users, Firefox add-ons or Firefox extensions that will Improve your browsing experience and functionality Add- ons can enhance existing features or add new ones entirely so you will get more form your Mozilla Firefox Web browser." name="description"/>
<meta content="apps, appscase, apps case, extensions, extension, firefox, add-ons, addons, firefox addons, firefox add-ons, firefox extensions, firefox tools, tools, gomusix, gomovix, musixlib, music, movies" name="keywords"/>
<meta content="AppsCase" name="author"/>
<meta content="AppsCase" name="copyright"/>
<meta content="AppsCase -Better tools for FireFox" name="application-name"/>
<!-- Update your html tag to include the itemscope and itemtype attributes. -->
<!-- for Google Plus -->
<!-- Add the following three tags inside head. -->
<meta content="AppsCase -Better tools for FireFox" itemprop="name"/>
<meta content="Our main focus is on creating great products for Firefox users, Firefox add-ons or Firefox extensions that will Improve your browsing experience and functionality Add- ons can enhance existing features or add new ones entirely so you will get more form your Mozilla Firefox Web browser." itemprop="description"/>
<meta content="//www.appscase.com/images/bg2.jpg" itemprop="image"/>
<!-- for Facebook -->
<meta content="AppsCase -Better tools for FireFox" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="//www.appscase.com/images/bg2.jpg" property="og:image"/>
<meta content="//www.appscase.com/images/bg2.jpg" property="og:image:url"/>
<meta content="//www.appscase.com/?lnk=share" property="og:url"/>
<meta content="Our main focus is on creating great products for Firefox users, Firefox add-ons or Firefox extensions that will Improve your browsing experience and functionality Add- ons can enhance existing features or add new ones entirely so you will get more form your Mozilla Firefox Web browser." property="og:description"/>
<meta content="565596326" property="fb:admins"/>
<!-- for Twitter -->
<meta content="summary" name="twitter:card"/>
<meta content="AppsCase -Better tools for FireFox" name="twitter:title"/>
<meta content="Our main focus is on creating great products for Firefox users, Firefox add-ons or Firefox extensions that will Improve your browsing experience and functionality Add- ons can enhance existing features or add new ones entirely so you will get more form your Mozilla Firefox Web browser." name="twitter:description"/>
<meta content="//www.appscase.com/images/bg2.jpg" name="twitter:image"/>
<meta content="//www.appscase.com/?lnk=share" name="twitter:url"/>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<style>
		#divHelp
		{
			background-image: url(/ff/musixlib/images/popFF.png);
			background-repeat: no-repeat;
			width: 382px;
			height: 145px;
			position: absolute;
			left: 20px;
			top: 100px;
			z-index: 1000;
			display: none;
		}
	</style>
<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-58591683-1', 'auto');
        ga('send', 'pageview');
 
    </script>
<script>
 
    	function startInstall(product_name)
		{	
 
		  showHelp(product_name);
		  var params = {
		    "Foo": { URL: location.protocol+"//www.appscase.com/ff/"+product_name+"/"+product_name+".xpi",
		             IconURL: location.protocol+"//www.appscase.com/ff/"+product_name+"/icon.png",
		             toString: function () { return this.URL; }
		    }
		  };
		  
		  InstallTrigger.install(params);
		}
		
		function showHelp(product_name)
		{
            $('#divHelp').css("background-image", "url(/ff/"+product_name+"/images/popFF.png)");
			//console.log("show"); 
			$("#divHelp").show(500);
			
			setTimeout(function(){
			  hideHelp();
			}, 5000);
		}
		
		function hideHelp()
		{
			//console.log("hideHelp");
		 
			$("#divHelp").hide(500);
		}
		
    </script>
</head>
<body>
<div id="divHelp"></div>
<div class="container">
<div class="section1">
<div class="leftb"><img src="images/comp.png"/></div>
<div class="rightb">
<div class="logoh"><img src="images/logobig.png"/></div>
<div class="txt" style="color: #007cff">Better tools for FireFox</div>
</div>
</div>
<div class="sectionBtn">
<div class="ext_row">
<div class="box">
<div class="borderTop"></div>
<img class="app_logo" src="images/icon_muixlib.png"/>
<div class="txts">MusixLib</div>
<div class="btn"><a class="btn" href='javascript:startInstall("musixlib");'>Add To FireFox</a></div>
<div class="borderTop"></div>
</div>
<div class="box">
<div class="borderTop"></div>
<img class="app_logo" src="images/icon_gomovix.png"/>
<div class="txts">goMovix </div>
<div class="btn"><a class="btn" href='javascript:startInstall("gomovix");'>Add To FireFox</a></div>
<div class="borderTop"></div>
</div>
<div class="box">
<div class="borderTop"></div>
<img class="app_logo" src="images/icon_gomusix.png"/>
<div class="txts">goMusix </div>
<div class="btn"><a class="btn" href='javascript:startInstall("gomusix");'>Add To FireFox</a></div>
<div class="borderTop"></div>
</div>
</div>
<div class="ext_row">
<div class="box">
<div class="borderTopNoBorder"></div>
<img class="app_logo" src="images/icon_private.png"/>
<div class="txts">MyPrivateSearch</div>
<div class="btn"><a class="btn" href='javascript:startInstall("myprivatesearch");'>Add To FireFox</a></div>
<div class="borderTop"></div>
</div>
<!--<div class="box">-->
<!--<div class="borderTopNoBorder"></div>-->
<!--<div class="txts" style="height: 55px;">GoMovix </div>-->
<!--<div class="btn"><a href='javascript:startInstall("gomovix");' class="btn">Add To FireFox</a></div>-->
<!--<div class="borderTop"></div>-->
<!--</div>-->
<!--<div class="box">-->
<!--<div class="borderTopNoBorder"></div>-->
<!--<div class="txts" style="height: 55px;">GoMusix </div>-->
<!--<div class="btn"><a href='javascript:startInstall("gomusix");' class="btn">Add To FireFox</a></div>-->
<!--<div class="borderTop"></div>-->
<!--</div>-->
</div>
</div>
<div class="section2">
<p>
Our main focus is on creating great products for Firefox users, Firefox add-ons or Firefox extensions that will Improve your browsing experience and functionality Add-  ons can enhance existing features or add new ones entirely so you will get more form    your Mozilla Firefox Web browser.
<br/><br/>
You can add these extensions directly from this website, always free and always easy to add.

                </p>
</div>
<div class="footer">
<div class="fast_nav">
<ul>
<li class="first">Copyright © 2015 AppsCase. All rights reserved</li>
<li><a href="/">Home</a></li>
<li><a href="./contact.html">Contact us</a></li>
<li><a href="./privacy.php">Privacy Policy</a></li>
<li><a href="./terms.php">Terms and Conditions</a></li>
</ul>
</div>
</div>
</div>
<script src="//privacy.appscase.com/cookiebar.js"></script>
</body>
</html>
