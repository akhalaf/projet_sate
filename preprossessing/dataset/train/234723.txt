<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>BURGER KING® 汉堡王中国官网</title>
<meta content="www.bkchina.cn,BURGER KING® 汉堡王中国官网" name="keywords"/>
<meta content="www.bkchina.cn,汉堡王中国,汉堡,皇堡,美国汉堡,汉堡王,国王,king KING,BURGERKING,burgerking,薯条 火烤 BK真不一样 要玩就玩真的 来自美国的汉堡,美味 快餐 不油炸的汉堡,布朗尼" name="description"/>
<link href="/website/css/index.css" rel="stylesheet" type="text/css"/>
<link href="/website/css/default.css" rel="stylesheet" type="text/css"/>
<link href="/website/css/default2020.css" rel="stylesheet" type="text/css"/>
<script src="/website/js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/website/js/statistics.js" type="text/javascript"></script>
<link href="/website/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="//bkchina.cn/website/images/BK_Favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="WAfaQtj43F" name="baidu-site-verification"/>
<script>
      var userAgentInfo = navigator.userAgent;
      var Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
      var flag = true;
      for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
          window.location.href = '//bkchina.cn/mobile/main.html';
        }
      }
    </script>
<script>
      $(function(){
        $.get('/restaurant/storeCity',{
          'tim':Date.parse(new Date())
        },function(r){
          $('.ice').html(r);
        },'html');

        $('.choice .region').each(function(e){
          var size = $(this).find('ul li').size()
          for(var i = 0; i<size; i++){
            $(this).find('ul li').last().css('padding-left','0px')
          }
        })
        $('.frame .butt').click(function(){
          $('.choice').show();
        })
        $('.choice .guanbi').click(function(){
          $('.choice').hide();
        })

        $('.eels_fs ul li').eq(2).css('margin-right','0')
        if((screen.width == 1366)){
          $('body').addClass('min_body')
        }

        //首页A标签加_blank
        $('.top').children('a').attr('target','_blank');

      })
      function gotuMap(str){
        var sCity = $("#storeCity").text();
        if(sCity == "" || sCity == "undefined" || sCity == "选择城市"){
          alert("请选择城市");
          return false;
        }
        var sKey = $('#storeKey').val();
        if(sKey == "" || sKey == "undefined"){
          alert("请输入地址，城市或邮编");
          return false;
        }
        //location.href = "/restaurant/index.html";
        window.open("/restaurant/index.html?ac=search&city="+encodeURI(sCity)+"&keyword="+encodeURI(sKey));
        $("#storeCity").text("选择城市");
        $('#storeKey').val("");
      }
      function setStoreCity(sText){
        if(sText == "" || sText == "undefined"){
          bootbox.alert("请选择城市");
        }
        $("#storeCity").text(sText);
      }

      //获取url参数
      function queryString(key){
        var val = (document.location.search.match(new RegExp("(?:^\\?|&)"+key+"=(.*?)(?=&|$)"))||['',null])[1];
        if(val == null){
          val = '';
        }
        return val;
      }
      /*
       * 加载kv图
       */
      $(function(){
        $.post('/discount/kvList',{'type':queryString('type')},function(r){
          var kvHtml = '';
          for(var i in r){
            if(i == 0){
              display = 'block';
            }else{
              display = 'none';
            }
            if(r[i]['FUrl'] == ''){
              r[i]['FUrl'] = 'javascript:void(0)';
            }

            kvHtml += '<div class="Item BigCur" style="display: '+display+';"><a href="'+r[i]['FUrl']+'" ><img src="'+r[i]['FPicUrl']+'" /></a></div>'
          }
          kvHtml+='<div class="Button LeftBtn"></div><div class="Button RightBtn"></div><ul id="PicNum"></ul>';
          $('#IndexBanner').html(kvHtml);
          //渲染数据
          $.getScript('/website/js/fun.js');
        },'json');
      })
    </script>
</head>
<body>
<div class="warp">
<div class="top">
<!--头部开始-->
<div class="top">
<script>
var _hmt = _hmt || [];
(function() {
	var hm = document.createElement("script");
	hm.src = "//hm.baidu.com/hm.js?235cb53c072af0133b0ab9fed6d3931c";
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(hm, s);
})();
$(function(){
	//3月8号那天，展示女士的logo
	var d1="2017-03-08";
	d1Arr=d1.split('-');
	v1=new Date(d1Arr[0],d1Arr[1]-1,d1Arr[2]);
	var starttime=v1.getTime();
	var d2="2017-03-08";
	d2Arr=d2.split('-');
	v2=new Date(d2Arr[0],d2Arr[1]-1,d2Arr[2]);
	var endtime=v2.getTime()+24*60*60*1000;
	var today = new Date();
	var nowtime = today.getTime();
	boo1 = nowtime >starttime;
	boo2 = nowtime <endtime;
	if(boo1&&boo2){
		$('.logo').css('background-image','url("/website/images/index/logoForWomen.png")');
	}
})
</script>
<link href="http://bkchina.cn/website/images/BK_Favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/website/css/index.css" rel="stylesheet" type="text/css"/>
<link href="/website/css/default.css" rel="stylesheet" type="text/css"/>
<link href="/website/css/default2020.css" rel="stylesheet" type="text/css"/>
<div class="top-join">
<a href="/trained/index.html" target="_blank">关于汉堡王</a>
<a href="/about/index.html" target="_blank">加入我们</a>
<a href="/join/index.html" target="_blank">加盟</a>
<a href="/news/index.html" target="_blank">新闻中心</a>
</div>
<div class="nav">
<div class="nav-t">
<a class="logo" href="/"></a>
<a class="nav-lj nav-lj-on" href="/product/index.html" target="_blank"></a>
<a class="nav-lj nav-lj-tw" href="/discount/index.html" target="_blank"></a>
<a class="nav-lj nav-lj-th" href="/restaurant/index.html" target="_blank"></a>
<a class="nav-lj nav-lj-out-foot qishou" href="javascript:;"></a>
<a class="nav-lj nav-lj-four" href="javascript:;" id="subnav-btn"></a>
<a class="nav-lj nav-lj-five" href="/contact/index.html" target="_blank"></a>
<!--		<div class="weix">-->
<!--			<a href="javascript:void(0);" class="wwx"><img src="/website/images/index/wx.png"></a>-->
<!--			<a href="javascript:void(0);" class="zzfb"><img src="/website/images/index/zzfb.png"></a>-->
<!--			<a href="javascript:void(0);" class="qishou"><img src="/website/images/index/qishou.png"></a>-->
<!--		</div>-->
</div>
</div>
<div class="subnav" id="subnav" style="z-index: 99999">
<div class="subnav-box">
<div class="subnav-1">
<span><img src="/website/images/new202005/subnav-1.png"/></span>
<ul>
<li><a class="subnav-1-1" href="/chosen/subnav-1-1.html"></a></li>
<li><a class="subnav-1-2" href="/chosen/subnav-1-2.html"></a></li>
<li><a class="subnav-1-3" href="/chosen/subnav-1-3.html"></a></li>
<li><a class="subnav-1-4" href="/chosen/subnav-1-4.html"></a></li>
<li><a class="subnav-1-5" href="/chosen/subnav-1-5.html"></a></li>
<li><a class="subnav-1-6" href="/chosen/subnav-1-6.html"></a></li>
</ul>
</div>
<div class="subnav-2">
<a href="/chosen/subnav-2-1.html"><img src="/website/images/new202005/subnav-2.png"/></a>
</div>
<div class="subnav-3">
<span><img src="/website/images/new202005/subnav-3.png"/></span>
<ul>
<li><a class="subnav-3-1" href="/chosen/subnav-3-1.html"></a></li>
</ul>
</div>
</div>
</div>
<div class="ewm">
<div class="img" style="width:560px;">
<p style="margin-left:-280px;"><em>微信扫一扫</em><br/>关注“汉堡王中国”官方微信号<br/>选择菜单“BK会员-会员中心” 加入汉堡王会员</p>
<img src="/website/images/index/ewm.png"/>
</div>
</div>
<div class="zfb">
<div class="img" style="width:560px;">
<p style="margin-left:-280px;"><em>支付宝扫一扫</em><br/>关注“汉堡王生活号”<br/>点击“BK会员中心”加入汉堡王会员 优惠享不停</p>
<img src="/website/images/index/zfb.jpg"/>
</div>
</div>
<div class="qs">
<div class="img" style="width:560px;">
<p style="margin-left:-280px;"><em>微信/支付宝扫一扫</em><br/>进入“汉堡王外送”小程序  美味品质送达</p>
<img src="/website/images/index/qshou.png"/>
</div>
</div>
<script>
	$(function () {
		$('.join-top-link a').click(function () {
			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top
			}, 500);
			return false;
		});
		if($(window).width()<1440){
			$("#sizebox-1").css("zoom",$(window).width()/1440)
		}
	})
</script>
<script src="/website/js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/website/js/layer.js"></script>
<script src="/website/js/main2020.js"></script>
<script src="/website/js/jquery.cxscroll.min.js"></script>
<script src="/website/js/statistics.js" type="text/javascript"></script>
<script src="/website/js/superslide.2.1.js" type="text/javascript"></script>
<script>
	//骑手扫一扫
	$('.qishou').click(function(){
		$('.qs').show();
		$('.qs').click(function(){
			$(this).hide();
		})
	})
</script>
</div><!--头部结束-->
</div>
</div>
<div class="banner">
<div id="IndexBanner">
<!--     	    <div class="Item BigCur" style="display: block;"><a href="http://www.bkchina.cn/discount"><img src="http://bkchinacdn1.ozzyad.com/website/discount/smallfire1201703312.jpg" /></a></div>
                       <div class="Item BigCur" style="display: none;"><a href="http://www.bkchina.cn/discount"><img src="http://bkchinacdn1.ozzyad.com/website/discount/jichi20170331.jpg" /></a></div>
                    <div class="Item BigCur" style="display: none;"><a href="http://www.bkchina.cn/discount"><img src="http://bkchinacdn1.ozzyad.com/website/discount/201703312qiyiguo.jpg" /></a></div>
                    <div class="Item BigCur" style="display: none;"><a href="http://www.bkchina.cn/discount"><img src="http://bkchinacdn1.ozzyad.com/website/discount/20170220mobilekvzhixinniangao1.jpg" /></a></div>
                    <div class="Item BigCur" style="display: none;"><a href="http://bkchina.cn/product/shop"><img src="http://bkchinacdn1.ozzyad.com/website/discount/zaocan201703312.jpg" /></a></div>
                    <div class="Button LeftBtn"></div>
                    <div class="Button RightBtn"></div>
                    <ul id="PicNum"></ul> -->
</div>
<!--<div id="IndexBanner">
        <div class="Item BigCur" style="display: block;"><a href="http://bkchina.cn/discount/index.html"><img src="/website/discount/25.jpg" /><a></div>
    </div> -->
</div>
<!--Banner结束-->
<div class="cneter">
<div class="Wheels">
<div class="eels clearboth">
<div class="eels_sc">
<a class="wm_block" href="javascript:void(0);">立即加入</a>
<p>成为堡堡的会员，福利多多停不住</p>
<!-- <i class="ti-on"></i>
            <i class="ti-tw"></i> -->
</div>
<div class="eels_fs">
<i></i>
<ul class="clearboth">
<li class="em-o">
<a href="http://weibo.com/burgerkingchina" target="_blank">
<em></em>
<p>关注微博</p>
</a>
</li>
<li class="em-t">
<em></em>
<p>微信扫一扫</p>
</li>
<li class="em-h" style="margin-right:0;">
<em></em>
<p>支付宝扫一扫</p>
</li>
</ul>
<span>关注官方微博微信，有机会赢取好礼及了解更多优惠信息</span>
</div>
</div>
<script>
  //3月8号那天，展示女士的logo
	var d1="2017-03-08";
	d1Arr=d1.split('-');
	v1=new Date(d1Arr[0],d1Arr[1]-1,d1Arr[2]);
	var starttime=v1.getTime();   
	var d2="2017-03-08";
	d2Arr=d2.split('-');
	v2=new Date(d2Arr[0],d2Arr[1]-1,d2Arr[2]);
	var endtime=v2.getTime()+24*60*60*1000;   
	var today = new Date();
	var nowtime = today.getTime();    
	boo1 = nowtime >starttime;
	boo2 = nowtime <endtime;
	if(boo1&&boo2){
		$('.logo').css('background-image','url("/website/images/index/logoForWomen.png")');
	}
    </script>
</div>
<div class="map-k">
<div class="map ">
<div class="search ">
<span class="rch_db ani bounce" swiper-animate-duration="1s" swiper-animate-effect="bounce"></span>
<em><img src="/website/images/index/31.png"/></em>
<span class="frame">
<a class="butt" id="storeCity" name="storeCity">选择城市</a>
<input class="sear" id="storeKey" name="storeKey" placeholder="请选择城市，输入地址或邮编" type="text" value=""/>
<input class="button" id="gotuMap" onclick="gotuMap();" type="button" value=""/>
</span>
</div>
</div>
</div>
<div class="hamburg-ku">
<div class="hamburg" style="background-image:url(/website/images/ourStory-bg.jpg)">
<span class="urg_hb"></span>
<em class="king">汉堡王是怎样炼成的</em>
<em class="since"><img src="/website/images/index/32.png"/></em>
<p>汉堡王是全球大型连锁餐饮企业，截止于2018年上半年，在100多个国家及地区经营着超17000家餐厅。 </p>
<p>每天，全世界都有超过1千万的顾客光顾我们，每位顾客都坚信汉堡王能给他们带来更多美味。</p>
<a href="/aboutChina/index1.html" target="_blank">查看详情</a>
</div>
</div>
<div class="hamburg-ku">
<div class="hamburg" style="background-image: url('http://bkchinacdn1.ozzyad.com/website/discount/huangguanbiaozhun2018.png')">
<i style="margin-top: 200px;">每一个皇堡都符合汉堡王的皇冠标准</i>
<a href="/nowDo/index" target="_blank">查看详情</a>
</div>
</div>
<div class="hamburg-ku">
<div class="hamburg" style="background-image:url('http://bkchinacdn1.ozzyad.com/website/discount/xiandianxianzuo2018.png')">
<span class="xiand">现点现做</span>
<i>每一个皇堡都根据您的要求现点现做。</i>
<a class="lje" href="/nowDo/index1" target="_blank"></a>
</div>
</div>
</div>
<!--底部开始-->
<div class="top">
<div class="fooer">
<div class="fooer_center">
<!--        <span class="min_nav">-->
<!--            <a href="/product/index.html" target="_blank">菜单</a>-->
<!--            <a href="/discount/index.html" target="_blank">会员福利</a>-->
<!--            <a href="news/index.html">新闻</a>-->
<!--            <a href="/join/index.html" target="_blank">加盟</a> -->
<!--        </span>-->
<!--        <span class="min_fx">-->
<!--            <a href="http://weibo.com/burgerkingchina" target="_blank" class="fx-wx">-->
<!--                <img src="/website/images/index/08.png">-->
<!--            </a>-->
<!--            <a href="javascript:void(0)" class="vvo">-->
<!--                <img src="/website/images/index/09.png">-->
<!--            </a>-->
<!--            <a href="javascript:void(0)" class="zzfb">-->
<!--                <img src="/website/images/index/zzfb.png">-->
<!--            </a>-->
<!--        </span>-->
<span class="clause">
<a href="/clause/index.html" target="_blank">使用条款</a>
<i>|</i>
<a href="/privacy/index.html" target="_blank">隐私权声明</a>
<i>|</i>
<a href="/about/index.html" target="_blank">加入我们</a>
<i>|</i>
<a href="/contact/index.html" target="_blank">联系我们</a>
<i>|</i>
<a href="https://pan.baidu.com/s/1miTHgqW" target="_blank">供应商资质</a>
<i>|</i>
<a href="/website/file/汉堡王储值卡全国使用门店列表_202007.xlsx" target="_blank">储值账户余额可用门店清单</a>
<div style="width:300px;margin:0 auto; padding:20px 0;"> <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=31010402003434" style="display:inline-block;text-decoration:none;height:20px;line-height:20px;" target="_blank"><img src="/website/images/gonganlogo.png" style="float:left;"/>
<p style="float:left;height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#939393;">沪公网安备 31010402003434号</p>
</a> </div>
<!--<a href="#">供应商</a>-->
</span> <span class="put"><a href="https://beian.miit.gov.cn" style="color:#cecece;" target="_blank">沪ICP备15038851号-1</a>      版权所有 © 2010-2016 汉堡王公司 | © 2010-2016 Burger King Corporation.  All rights reserved.</span>
<a class="back" href="javascript:scroll(0,0)">返回顶部</a> <!--<a href="http://www.bk.com" target="_blank" class="site"
                                                                  style="width:110px;">汉堡王全球站</a></div>-->
</div>
<script src="/website/js/main2020.js" type="text/javascript"></script>
<!--Cnzz-->
<div style="display:none;">
<script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cspan id='cnzz_stat_icon_1256273784'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/stat.php%3Fid%3D1256273784' type='text/javascript'%3E%3C/script%3E"));</script>
<!--全站检测-->
<script language="JavaScript" src="https://s95.cnzz.com/z_stat.php?id=1261314233&amp;web_id=1261314233"></script>
</div>
<!-- 外卖公告 开始 -->
<div class="wm_choice" style="display:none;">
<div class="ice">
<span></span>
<ul>
<li><img src="../../website/images/takeout_car.png"/></li>
<li>外送网站正在测试中，更多精彩服务，敬请期待！～</li>
</ul>
<a href="javascript:void(0)"></a>
</div>
</div>
<script src="/website/js/layer.js"></script>
<script>
    $(function () {
        $('.wm_block,.wwx,.em-t,.vvo').click(function () {
            $('.ewm').show();
            $('.ewm').click(function () {
                $(this).hide();
            })
        })

        //支付宝扫一扫
        $('.eels_fs .em-h,.vvz,.em-h,.zzfb').click(function () {
            $('.zfb').show();
            $('.zfb').click(function () {
                $(this).hide();
            })
        })

        $('.wm_foot').click(function () {
            $('.wm_choice').show();
        })

        $('.wm_choice a').click(function () {
            $('.wm_choice').hide();
        })

        //供应商资质链接
        $('.clause a').eq(4).click(function () {
            layer.open({
                type: 1,
                title: '供应商资质下载',
                closeBtn: 1,
                shadeClose: true,
                area: ['300px', '200px'],
                content: '<br><a style="margin-left:110px;font-size: 20px;"  target="_blank" href="https://share.weiyun.com/YE2Y4Eq9">下载链接</a><br><p style="text-align: center;font-size: 20px;">密码：888888</p>'
            });
            return false;
        })

    })
</script>
<!-- 外卖公告 结束-->
</div><!--底部结束-->
</div>
<!--Cnzz-->
<div style="display:none;">
<!--pc端检测--><script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1256273784'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/stat.php%3Fid%3D1256273784' type='text/javascript'%3E%3C/script%3E"));</script>
<!--全站检测--><script language="JavaScript" src="https://s95.cnzz.com/z_stat.php?id=1261314233&amp;web_id=1261314233"></script>
</div>
<style>
    .wm_choice {background: rgba(0, 0, 0, 0) url("/website/images/restaurant/4.png") repeat scroll 0 0;display: none;height: 100%;left: 0;position: fixed;top: 0;width: 100%;z-index: 1000;}
    .wm_choice .wx_ice {background: rgba(0, 0, 0, 0) url("/website/images/restaurant/3.png") no-repeat scroll 0 0;height: 409px;left: 50%;margin: -204px 0 0 -444px;position: absolute;top: 50%;width: 889px;}
    .wm_choice .wx_ice span {display: block;height: 50px;margin: 27px auto 40px;;width: 180px;}
    .wm_choice .wx_ice ul {margin: 0 auto;width: 822px;}
    .wm_choice .wx_ice ul li {border-bottom: 1px solid #d3d3d3;color: #000;font:18px/36px "microsoft yahei";width: 797px;margin:20px 0px 20px 0px;padding-bottom:20px; text-align:center;}
    .wm_choice .wx_ice ul li:last-child{border:none;}
    .wm_choice .wx_ice ul li em {cursor: pointer;margin-right: 65px;}
    .wm_choice a {background: rgba(0, 0, 0, 0) url("/website/images/restaurant/1.png") no-repeat scroll center center;display: block;height: 48px;position: absolute;right: 25px;top: 27px;width: 48px;}
</style>
<div class="wm_choice">
<div class="wx_ice">
<span></span>
<ul>
<li><img src="/website/images/takeout_car.png"/></li>
<li>外送网站正在测试中，更多精彩服务，敬请期待！～</li>
</ul>
<a href="javascript:void(0)"></a>
</div>
</div>
<script>
  $(function(){
//微信扫一扫
    $('.wm_block,.wwx,.em-t,.vvo').click(function(){
      $('.ewm').show();
      $('.ewm').click(function(){
        $(this).hide();
      })
    })


    //支付宝扫一扫
    $('.eels_fs .em-h,.vvz,.em-h,.zzfb').click(function(){
      $('.zfb').show();
      $('.zfb').click(function(){
        $(this).hide();
      })
    })


    $('.wm_foot').click(function(){
      $('.wm_choice').show();
    })
    $('.wm_choice a').click(function(){
      $('.wm_choice').hide();
    })

  })
</script>
<div class="choice"><div class="ice_bg" style="top:1%;"><div class="ice"></div></div></div>
<script src="/website/js/jquery-1.11.0.min.js"></script>
<script src="/website/js/layer.js"></script>
<script src="/website/js/jquery.cxscroll.min.js"></script>
</body>
</html>