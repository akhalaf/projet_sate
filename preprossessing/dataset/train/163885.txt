<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="8HZ_p6Ow_cUmyxhuPHyVtgbl6vj035MqMluNashGCCo" name="google-site-verification"/>
<meta content="K5vNTAlBRX7LG7ry6hg8o9beCMgvz8E1GRHsPTitMsw" name="google-site-verification"/>
<meta content="INDEX, FOLLOW, ARCHIVE" name="ROBOTS"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="astrology,free astrology,numerology,astrology numerology,astrology and numerology,numerology and astrology" name="keywords"/>
<meta content="The basics of astrology and numerology in easy to understand language. Wanting to learn about astrology or numerology? This is the place." name="description"/>
<meta content="B91C85EFA4B29E01455EE31E6264D9D5" name="msvalidate.01"/>
<meta content="&lt;width=960," initial-scale="1," maximum-scale=".33" name="viewport"/>
<meta content="8HZ_p6Ow_cUmyxhuPHyVtgbl6vj035MqMluNashGCCo" google-site-verification=""/><title>Astrology and Numerology</title>
<meta content="Michael McClain" name="author"/></head><body alink="#00007f" link="#00007f" style="background-color: rgb(140, 207, 218);" vlink="#990000">
<center style="font-family: Times New Roman,Times,serif;">
<div id="fb-root"><font size="+3"><img align="bottom" alt="The Ascendant in Astrology" border="0" height="125" naturalsizeflag="3" src="images/an_banner.gif" width="770"/> 
     
 
        <br/>
</font></div>
<table border="2" cellpadding="10" cellspacing="0" style="width: 960px; height: 1px;">
<tbody>
<tr>
<td style="vertical-align: top; background-color: rgb(255, 255, 255);">
<table border="0" cellpadding="15" cellspacing="0" style="text-align: left; width: 95%;">
<tbody>
<tr>
<td style="vertical-align: middle; text-align: center;"><font><font size="+3"><a href="astrology.html">ASTROLOGY </a></font></font><br/>
<font><font size="+3"><a href="astrology.html">INDEX</a></font></font></td>
<td style="vertical-align: middle; text-align: center;"><font><font size="+3"><a href="numerology.html">NUMEROLOGY </a></font></font><br/>
<font><font size="+3"><a href="numerology.html">INDEX</a></font></font></td>
<td align="center" valign="middle"><a href="readings.html"><font size="+3">Personalized <br/>
Readings</font></a></td>
</tr>
</tbody>
</table>
<font size="+3"><br/>
</font>
<h3><big><font size="+3"><big>Astrology
and Numerology</big></font></big></h3>
<font size="+3">Celebrating 20 Years on the WWW December, 2016</font>
<h4><font size="+3">by
Michael McClain</font></h4>
<font size="+3">Welcome to
Astrology-Numerology.com!
 Whether
this is a first
time look
into astrology and numerology or you're a serious novice student, these
pages should be just right for you. This is a unique site providing a
multitude of easy to understand and very informative astrology and
numerology readings and explanations.  All of the content
remains
free for your enjoyment.<br/>
<br/>
The contents of this site is truly the equivalent of several free
astrology and numerology textbooks online. Below find info on the <a href="#site_map">key astrology</a> and <a href="index.html#numerology">key
numerology</a> pages.  We hope to show you that
astrology and numerology can be easy and fun. Write your own horoscope
or numerology reading! The tools are certainly here. The site is
provided for your personal use to gain insight into some of the
elementary and essential elements of astrology, the horoscope, and
numerology.<br/>
        <br/>
The site is divided into two sections, each having its own index page.
Many of the basics are explained in some detail on the pages that
follow, but if you have a question that is not addressed, get confused,
or have any sort of comment, <a href="mailto:michael@astrology-numerology.com">please let me
know</a>. I hope you will
bookmark this page.  If you like what you find here, please
tell your friends.<br/>
<br style="font-weight: bold;"/>
Your Host,
  <a href="photo.html" style="font-weight: bold;">Michael
McClain</a><br/>
<br/>
Choose
your index
and enjoy: <br/>
</font>
<h4><font size="+3"><a href="astrology.html">ASTROLOGY
INDEX</a> or <a href="numerology.html">NUMEROLOGY
INDEX</a></font><font size="+3">    
     <br/>
</font></h4>
<div style="text-align: center;">
</div>
<font size="+3"><br/>
</font>
<h4><font size="+3">Privacy
policy: </font></h4>
<font size="+3">This
site doesn't share or use your email address
or any other
information provided by you or your computer 
for any purpose other than to answer queries and/or provide requested
reports. 
 You can browse
to
your heart's content without worry.<br/>
</font>
<p><font size="+3">©
Michael
McClain 1996-2015.
 Rights reserved..</font></p>
<p><font size="+3"><a href="https://plus.google.com/102007157190460624694?rel=author">My
Google+</a></font></p>
<h1><font size="+3"><a name="site_map"></a><font size="-1">.</font></font></h1>
<h3><big><font size="+3"><big>Astrology</big></font></big></h3>
<h4><font size="+3"><a href="http://astrology-numerology.com/astrology.html">Astrology
Index</a> </font></h4>
<font size="+3">This
is an index in that it links up to all of the astrology pages.
 But it is the home page for lots of interesting astrology
content.
Starting with some astrology key words to remember, it moves into a
discussion on the astrology element - earth, fire, air, and water.
 Next comes the modes of activity - cardinal, mutable, fixed,
AKA
the quadruplicities. Then comes a nice astrology cheat sheet on the
planet and sign symbols, followed by discussions on the great astrology
ages, the influence of the hemispheres, Chiron, the lunar nodes, and
retrograde planets.  <br/>
</font>
<h4><font size="+3"><a href="astrology_signs.html">The Planets
in the Zodiac Signs</a></font></h4>
<font size="+3">As
the title implies, this page provides a reading for the Sun, the Moon,
and the planets in each of the 12 astrology signs.  Know your
Sun
sign? Sure.  But what about your Venus, Moon, and Saturn?
 Find them here and enjoy.<br/>
</font>
<h4><font size="+3"><a href="astrology_aspects.html">The
Planets forming Aspects</a></font></h4>
<font size="+3">In
astrology, when two planets form an aspect together, the influence of
each is hightened  and enhanced.  Learn the potential
of
planets in aspects on this special page.  Also find help
calculating astrology natal chart asepcts, followed by information on
the major astrology aspect configurations such as the Cross, the
T-square, Grand Trine, and Yod.  Finally, the page offers
discussion on transiting aspects.<br/>
</font>
<h4><font size="+3"><a href="astrology_planets.html">Planets
in the twelve houses of the horosscope</a></font></h4>
<font size="+3">Read
about the placement of the Sun, the Moon, and all of the plaents in
your horosocpe.  Learn about the part of your life influenced
by
these placements.<br/>
</font>
<h4><font size="+3"><a href="http://astrology-numerology.com/astrology_houses.html" style="font-weight: bold;">The
12 houses of the horoscope &amp; ruler of each</a></font></h4>
<font size="+3"><br/>
The
twelve houses of the astrology chart define 12 areas of our life.
 The beginning of each of the houses is called the cusp.
 Each house has what is called a natural sign and natural
ruling
planet.  This is the sign and planet which is associated with
that
house in the natural chart beginning with Aries and endingin with
Pisces.  <br/>
</font>
<h4><font size="+3"><a href="astrology_houses.html">The
Ascendant/Rising Signs,
and Other Cusps</a>  </font></h4>
<font size="+3">This
page gets to the heart of reading the horoscope as it discusses the
sign on the cusp of the ascendant, and the eleven house cusps that
follow. Learn how your rising sign may say more about your personality
than any of the planets, or even the Sun and Moon.<br/>
</font>
<h4><font size="+3"><a href="sun-moon.html">Readings
that combine your Sun &amp; Moon signs</a></font></h4>
<font size="+3">This
unique page couples all 144 possible combinations of Sun and Moon signs
in a nutshell reading you will find very interesting.<br/>
</font>
<h4><font size="+3"><a href="astrology_relationships.html">Astrology
and
Relationships</a></font></h4>
<font size="+3">This
page takes an extensive look at relationships, and covers the astrology
synastry fromr all directions.  Be prepared to stay a while!<br/>
</font>
<h2><font size="+3"><a name="Numerology"></a><font size="-1">.</font></font></h2>
<h2><font size="+3"><big>Numerology</big></font></h2>
<h4><font size="+3"><a href="numerology.html">Numerology Index</a></font></h4>
<font size="+3">A
handy numerology start page linking to all of the numerology topics.
 This page is also key for the extensive introductory
explanations.  Next a key word page showing the positive and
negative traits of all of the numbers.  Then comes readings
for
every day of any calendar month, followed by my thoughts on master
numbers, and articles on naming a baby or changing a name. Finally,
there is an interesting essay on the US Presidents and their life path
numbers.<br/>
</font>
<h4><font size="+3"><a href="num-lifepath.html">Numerology
Life Path Numbers</a></font></h4>
<font size="+3">As
the world learns that knowing your life path number is every bit as
intresting as knowing your Sun sign in astrology, the numerology life
path number page has become our most popular page.  Here
you'll
find a extensive reading on each life path number.<br/>
</font>
<h4><font size="+3"><a href="num-birthname.html">Information
all about the Birth Name</a></font></h4>
<font size="+3">So
much about numerology centers on the name that you were given at birth
by your parents; your FULL birth name.  This page covers the
topics
in detail.  Learn about your destiny number, your soul urge
and
inner dream numbers.  Next read about the special traits and
karmic lessons that may show in your birth name, followed by the
progression of
the essences of the name.  Finally, the section on the planes
of expression reveals even more.<br/>
</font>
<h4><font size="+3"><a href="num-cycles.html">Numerology Cycles</a></font></h4>
<font size="+3">Cycles
help us understand what is happening in our lives and look ahead to
build forecasts.  This page discusses personal years, life
path
periods, pinnacles and challenges.  Mini readings are provided
for
each of these for understanding.<br/>
<br/>
</font>
<h4><font size="+3"><a href="num-relationship.html">Numerology
Relationship</a></font></h4>
<font size="+3">This
page is designed to help you find and/or help you understand your
partner in numerology terms.  Mini readings hint at possible
outcomes of the numbers combined in a relationship.<br/>
<br/>
visit my new site construction for <a href="http://web.wctel.net/%7Ehabitathomes">Habitat for Humanity</a>.
      </font></td>
</tr>
</tbody>
</table>
</center>
<font size="+3"><br/>
<br/>
<br/>
<br/>
<br/>
</font>
</body></html>