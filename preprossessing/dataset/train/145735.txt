<!DOCTYPE html>
<html lang="en"><head><meta charset="utf-8"/><title>Automotive Research And Development, Automotive Service Provider, India, Vehicle Type Certification, Automotive Testing And Calibration, Vehicle Design Labs, Advanced Automotive Design Standards, Automotive Engineering Course</title><base href="/"/><!-- Search Engine --><meta content="ARAI Pune offers automotive research and development center in India, vehicle type certification, automotive testing and calibration, vehicle design labs, advanced automotive design standards, automotive engineering course, India" name="description"/><!-- Schema.org for Google --><meta content="ARAI Pune offers automotive research and development center in India, vehicle type certification, automotive testing and calibration, vehicle design labs, advanced automotive design standards, automotive engineering course, India" itemprop="name"/><meta content="ARAI Pune offers automotive research and development center in India, vehicle type certification, automotive testing and calibration, vehicle design labs, advanced automotive design standards, automotive engineering course, India" itemprop="description"/><!-- Twitter --><meta content="summary" name="twitter:card"/><meta content="Automotive Research And Development, Automotive Service Provider, India, Vehicle
        Type Certification, Automotive Testing And Calibration, Vehicle Design Labs, Advanced
        Automotive Design Standards, Automotive Engineering Course" name="twitter:title"/><meta content="ARAI Pune offers automotive research and development center in India, vehicle type certification, automotive testing and calibration, vehicle design labs, advanced automotive design standards, automotive engineering course, India" name="twitter:description"/><meta content="https://cms.araiindia.com/MediaFiles/slider-6_1013.png" name="twitter:image:src"/><!-- Open Graph general (Facebook, Pinterest & Google+) --><meta content="Automotive Research And Development, Automotive Service Provider, India, Vehicle
        Type Certification, Automotive Testing And Calibration, Vehicle Design Labs, Advanced
        Automotive Design Standards, Automotive Engineering Course" name="og:title"/><meta content="website" name="og:type"/><meta content="ARAI Pune offers automotive research and development center in India, vehicle type certification, automotive testing and calibration, vehicle design labs, advanced automotive design standards, automotive engineering course, India" property="og:description"/><link href="favicon.ico" rel="icon" type="image/x-icon"/><base href="/"/><meta content="width=device-width,initial-scale=1" name="viewport"/><link href="/assets/images/logo.png" rel="shortcut icon" type="image/png"/><link href="/assets/images/logo.png" rel="icon" type="image/png"/><link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/><!--font family for titles--><link href="https://fonts.googleapis.com/css?family=Fjalla One" rel="stylesheet"/><!-- Global site tag (gtag.js) - Google Analytics --><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-130484177-1"></script><script>window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130484177-1');</script><style type="text/css">/*loader starts here*/

    .noscroll {
      position: fixed;
      overflow: hidden;
    }

    #overlay {
      display: none;
      position: fixed;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      z-index: 100;
      background: #ffffff;
    }

    #spinner {
      display: none;
      position: fixed;
      width: 40px;
      height: 40px;
      top: 50%;
      left: 50%;
      margin-top: -20px;
      margin-left: -20px;
      z-index: 101;
      background-color: #3498db;
      -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;
      animation: sk-rotateplane 1.2s infinite ease-in-out;
    }

    @-webkit-keyframes sk-rotateplane {
      0% {
        -webkit-transform: perspective(120px)
      }

      50% {
        -webkit-transform: perspective(120px) rotateY(180deg)
      }

      100% {
        -webkit-transform: perspective(120px) rotateY(180deg) rotateX(180deg)
      }
    }

    @keyframes sk-rotateplane {
      0% {
        transform: perspective(120px) rotateX(0deg) rotateY(0deg);
        -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg)
      }

      50% {
        transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
        -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg)
      }

      100% {
        transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
        -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
      }
    }</style><link href="styles.8cb56f4e4c6d085c494f.bundle.css" rel="stylesheet"/></head><body><app-root></app-root><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script>function div_show() {
    //  document.getElementById('abc').style.display = "block";
    }</script><script type="text/javascript">$(document).on('show', '.accordion', function (e) {
      //$('.accordion-heading i').toggleClass(' ');
      $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });

    $(document).on('hide', '.accordion', function (e) {
      $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
      //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });</script><script>function check_empty() {
      if (document.getElementById('name').value == "" || document.getElementById('email').value == "" || document.getElementById(
        'msg').value == "") {
        alert("Fill All Fields !");
      } else {
        document.getElementById('form').submit();
        alert("Form Submitted Successfully...");
      }
    }
    //Function To Display Popup
    function div_show() {
    //  document.getElementById('abc').style.display = "block";
    }
    //Function to Hide Popup
    function div_hide() {
   //   document.getElementById('abc').style.display = "none";
    }</script><script>document.body.setAttribute("class", "noscroll");

   // document.getElementById("overlay").style.display = "block";
  //  document.getElementById("spinner").style.display = "block";


    window.onload = function () {
 //     document.getElementById("spinner").style.display = "none";
 //     document.getElementById("overlay").style.display = "none";

      document.body.className = document.body.className.replace(/\bnoscroll\b/, '');
    }</script><script src="inline.d2d4924f784cd333b196.bundle.js" type="text/javascript"></script><script src="polyfills.495def6342e1c0ff4724.bundle.js" type="text/javascript"></script><script src="main.348e936f149528071b3b.bundle.js" type="text/javascript"></script></body></html>