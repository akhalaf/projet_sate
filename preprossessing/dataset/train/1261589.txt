<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Tryb konserwacji</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<meta content="Stragan literacki" name="author"/>
<meta content="Stragan literacki - Zbiór tekstów, myśli i bezmyślności" name="description"/>
<meta content="Tryb Konserwacji" name="keywords"/>
<meta content="index, follow" name="robots"/>
<link href="https://salak-art.com/wp-content/plugins/wp-maintenance-mode/assets/css/style.min.css" rel="stylesheet"/>
<style>.background { background: url(http://salak-art.com/wp-content/uploads/2019/06/Mar-1.jpg) no-repeat center top fixed; background-size: cover; }</style> </head>
<body class="background">
<div class="wrap">
<h1>Tryb konserwacji</h1>
<h2></h2><p style="text-align: center"><strong>Uzupełniam archiwa. <br/>Planowane otwarcie Straganu - 1 kwietnia 2020. </strong></p>
</div>
<script type="text/javascript">
			var wpmm_vars = {"ajax_url": "https://salak-art.com/wp-admin/admin-ajax.php"};
		</script>
<script src="https://salak-art.com/wp-includes/js/jquery/jquery.js"></script>
<script src="https://salak-art.com/wp-content/plugins/wp-maintenance-mode/assets/js/scripts.min.js"></script>
</body>
</html>