<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="https://athensriviera.eu/xmlrpc.php" rel="pingback"/>
<title>Page not found - Athens Riviera</title>
<!-- This site is optimized with the Yoast SEO plugin v12.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Athens Riviera" property="og:title"/>
<meta content="Athens Riviera" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Athens Riviera" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://athensriviera.eu/#website","url":"https://athensriviera.eu/","name":"Athens Riviera","description":"Best","potentialAction":{"@type":"SearchAction","target":"https://athensriviera.eu/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://athensriviera.eu/feed/" rel="alternate" title="Athens Riviera » Feed" type="application/rss+xml"/>
<link href="https://athensriviera.eu/comments/feed/" rel="alternate" title="Athens Riviera » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/athensriviera.eu\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://athensriviera.eu/wp-content/cache/minify/0eb74.css" media="all" rel="stylesheet"/>
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link href="https://athensriviera.eu/wp-content/cache/minify/b5df6.css" media="all" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Raleway%7CABeeZee%7CAguafina+Script%7COpen+Sans%7CRoboto%7CRoboto+Slab%7CLato%7CTitillium+Web%7CSource+Sans+Pro%7CPlayfair+Display%7CMontserrat%7CKhand%7COswald%7CEk+Mukta%7CRubik%7CPT+Sans+Narrow%7CPoppins%7COxygen%3A300%2C400%2C600%2C700&amp;ver=1.0.5" id="google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://athensriviera.eu/wp-content/cache/minify/58f10.css" media="all" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons&amp;ver=5.3.6" id="mylisting-material-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://athensriviera.eu/wp-content/cache/minify/e3398.css" media="all" rel="stylesheet"/>
<style id="theme-styles-default-inline-css" type="text/css">
.case27-primary-text { font-family: 'Roboto', GlacialIndifference, sans-serif !important; font-weight: 600 !important; }  .featured-section .fc-description h1, .featured-section .fc-description h2, .featured-section .fc-description h3, .featured-section .fc-description h4, .featured-section .fc-description h5, .featured-section .fc-description h6 { font-family: 'Roboto', GlacialIndifference, sans-serif !important; font-weight: 600 !important; }  .case27-secondary-text { font-family: 'Roboto Slab', GlacialIndifference, sans-serif !important; font-weight: 400 !important; }  .title-style-1 h5 { font-family: 'Roboto Slab', GlacialIndifference, sans-serif !important; font-weight: 400 !important; }  .case27-body-text { font-family: 'Roboto', GlacialIndifference, sans-serif !important; font-weight: 400 !important; }  body, p { font-family: 'Roboto', GlacialIndifference, sans-serif !important; }  p { font-weight: 400 !important; } .case27-accent-text { font-family: 'Roboto', GlacialIndifference, sans-serif !important; font-weight: 500 !important; } 
</style>
<link href="https://athensriviera.eu/wp-content/cache/minify/89061.css" media="all" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.3.6" id="google-fonts-1-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://athensriviera.eu/wp-content/cache/minify/3a4e4.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _zxcvbnSettings = {"src":"https:\/\/athensriviera.eu\/wp-includes\/js\/zxcvbn.min.js"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/a7298.js"></script>
<link href="https://athensriviera.eu/wp-json/" rel="https://api.w.org/"/>
<link href="https://athensriviera.eu/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://athensriviera.eu/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<meta content="WooCommerce 3.8.1" name="generator"/>
<meta content="WPML ver:4.3.4 stt:1,4,2;" name="generator"/>
<script type="text/javascript">var MyListing = {"Helpers":{},"MapConfig":{"ClusterSize":35,"AccessToken":"AIzaSyA3OZnZmL7R-455scqUxsJxPYvAlKwNnNw","Language":"default","TypeRestrictions":"geocode","CountryRestrictions":["GR"],"CustomSkins":{}}};</script><script type="text/javascript">var CASE27 = {"ajax_url":"https:\/\/athensriviera.eu\/wp-admin\/admin-ajax.php","mylisting_ajax_url":"\/?mylisting-ajax=1","env":"production","ajax_nonce":"69da86bd86","l10n":{"selectOption":"Select an option","errorLoading":"The results could not be loaded.","loadingMore":"Loading more results\u2026","noResults":"No results found","searching":"Searching\u2026","datepicker":{"format":"DD MMMM, YY","timeFormat":"h:mm A","dateTimeFormat":"DD MMMM, YY, h:mm A","timePicker24Hour":false,"firstDay":1,"applyLabel":"Apply","cancelLabel":"Cancel","customRangeLabel":"Custom Range","daysOfWeek":["Su","Mo","Tu","We","Th","Fr","Sa"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"]},"irreversible_action":"This is an irreversible action. Proceed anyway?","delete_listing_confirm":"Are you sure you want to delete this listing?","copied_to_clipboard":"Copied!","nearby_listings_location_required":"Enter a location to find nearby listings.","nearby_listings_retrieving_location":"Retrieving location...","nearby_listings_searching":"Searching for nearby listings...","geolocation_failed":"You must enable location to use this feature.","something_went_wrong":"Something went wrong.","all_in_category":"All in \"%s\"","invalid_file_type":"Invalid file type. Accepted types:","file_limit_exceeded":"You have exceeded the file upload limit (%d)."},"woocommerce":[],"js_field_html_img":"<div class=\"uploaded-file uploaded-image review-gallery-image job-manager-uploaded-file\">\t<span class=\"uploaded-file-preview\">\t\t\t\t\t<span class=\"job-manager-uploaded-file-preview\">\t\t\t\t<img src=\"\">\t\t\t<\/span>\t\t\t\t<a class=\"remove-uploaded-file review-gallery-image-remove job-manager-remove-uploaded-file\"><i class=\"mi delete\"><\/i><\/a>\t<\/span>\t<input type=\"hidden\" class=\"input-text\" name=\"\" value=\"\"><\/div>","js_field_html":"<div class=\"uploaded-file  review-gallery-image job-manager-uploaded-file\">\t<span class=\"uploaded-file-preview\">\t\t\t\t\t<span class=\"job-manager-uploaded-file-name\">\t\t\t\t<i class=\"mi insert_drive_file uploaded-file-icon\"><\/i>\t\t\t\t<code><\/code>\t\t\t<\/span>\t\t\t\t<a class=\"remove-uploaded-file review-gallery-image-remove job-manager-remove-uploaded-file\"><i class=\"mi delete\"><\/i><\/a>\t<\/span>\t<input type=\"hidden\" class=\"input-text\" name=\"\" value=\"\"><\/div>"};</script> <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><link href="https://athensriviera.eu/wp-content/uploads/2019/12/cropped-Athens_Riviera_Logo-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://athensriviera.eu/wp-content/uploads/2019/12/cropped-Athens_Riviera_Logo-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://athensriviera.eu/wp-content/uploads/2019/12/cropped-Athens_Riviera_Logo-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://athensriviera.eu/wp-content/uploads/2019/12/cropped-Athens_Riviera_Logo-270x270.png" name="msapplication-TileImage"/>
<style id="mylisting-element-queries" type="text/css">.featured-search[max-width~="1000px"] .form-group, .featured-search .filter-count-3 .form-group { width: calc(33.3% - 12px); margin-right: 18px; } .featured-search[max-width~="1000px"] .form-group:nth-child(3n), .featured-search .filter-count-3 .form-group:nth-child(3n) { margin-right: 0; } .featured-search[max-width~="750px"] .form-group, .featured-search .filter-count-2 .form-group { width: calc(50% - 5px); margin-right: 10px !important; } .featured-search[max-width~="750px"] .form-group:nth-child(2n), .featured-search .filter-count-2 .form-group:nth-child(2n) { margin-right: 0 !important; } .featured-search[max-width~="550px"] .form-group, .featured-search .filter-count-1 .form-group { width: 100%; margin-right: 0 !important; } </style></head>
<body class="error404 theme-my-listing woocommerce-no-js my-listing elementor-default">
<div id="c27-site-wrapper"><div class="loader-bg main-loader" style="background-color: rgba(29, 35, 41, 0.98);">
<div class="paper-spinner " style="width: 28px; height: 28px;">
<div class="spinner-container active">
<div class="spinner-layer layer-1" style="border-color: #d19e12;">
<div class="circle-clipper left">
<div class="circle" style="border-width: 3px;"></div>
</div><div class="gap-patch">
<div class="circle" style="border-width: 3px;"></div>
</div><div class="circle-clipper right">
<div class="circle" style="border-width: 3px;"></div>
</div>
</div>
</div>
</div></div>
<header class="c27-main-header header header-style-alternate header-dark-skin header-scroll-dark-skin hide-until-load header-scroll-hide header-fixed header-menu-right">
<div class="header-skin"></div>
<div class="header-container">
<div class="header-top container-fluid">
<div class="mobile-menu">
<a href="#main-menu">
<div class="mobile-menu-lines"><i class="mi menu"></i></div>
</a>
</div>
<div class="logo">
<a class="static-logo" href="https://athensriviera.eu/">
<img src="https://athensriviera.eu/wp-content/uploads/2019/12/Athens_Riviera_Logo-2.png"/>
</a>
</div>
<div class="header-right">
<div class="user-area signin-area">
<i class="mi person user-area-icon"></i>
<a data-target="#sign-in-modal" data-toggle="modal" href="#">Sign in</a>
</div>
<div class="mob-sign-in">
<a data-target="#sign-in-modal" data-toggle="modal" href="#"><i class="mi person"></i></a>
</div>
<div class="search-trigger" data-target="#quicksearch-mobile-modal" data-toggle="modal">
<a href="#"><i class="mi search"></i></a>
</div>
</div>
</div>
<div class="container-fluid header-bottom">
<div class="header-bottom-wrapper row">
<div class="quick-search-instance text-left" data-focus="default" id="c27-header-search-form">
<form action="https://athensriviera.eu/explore/" method="GET">
<div class="dark-forms header-search search-shortcode-light">
<i class="mi search"></i>
<input autocomplete="off" name="search_keywords" placeholder="Type your search..." type="search"/>
<div class="instant-results">
<ul class="instant-results-list ajax-results"></ul>
<button class="buttons full-width button-5 search view-all-results all-results" type="submit">
<i class="mi search"></i>View all results				</button>
<button class="buttons full-width button-5 search view-all-results no-results" type="submit">
<i class="mi search"></i>No results				</button>
<div class="loader-bg">
<div class="paper-spinner center-vh" style="width: 24px; height: 24px;">
<div class="spinner-container active">
<div class="spinner-layer layer-1" style="border-color: #777;">
<div class="circle-clipper left">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="gap-patch">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="circle-clipper right">
<div class="circle" style="border-width: 2.5px;"></div>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</form>
</div>
<div class="i-nav">
<div class="mobile-nav-head">
<div class="mnh-close-icon">
<a href="#close-main-menu">
<i class="mi menu"></i>
</a>
</div>
</div>
<ul class="main-menu main-nav" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-63" id="menu-item-63"><a href="https://athensriviera.eu/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-64" id="menu-item-64"><a href="https://athensriviera.eu/explore/">Explore</a>
<div class="submenu-toggle"><i class="material-icons">arrow_drop_down</i></div><ul class="sub-menu i-dropdown">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3274" id="menu-item-3274"><a href="http://athensbest.eu">Explore Athens</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3275" id="menu-item-3275"><a href="http://aegeanislands.promo">Explore Aegean Islands</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3276" id="menu-item-3276"><a href="http://mykonosbest.eu">Explore Mykonos</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3277" id="menu-item-3277"><a href="http://santorinibest.eu">Explore Santorini</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3278" id="menu-item-3278"><a href="http://parosbest.eu">Explore Paros</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-65" id="menu-item-65"><a href="https://athensriviera.eu/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2790" id="menu-item-2790"><a href="https://athensriviera.eu/team/">Our team</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3279" id="menu-item-3279"><a href="http://flyingtogreece.com">Flying to Greece</a></li>
<li class="menu-item wpml-ls-slot-38 wpml-ls-item wpml-ls-item-en wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-has-children menu-item-wpml-ls-38-en" id="menu-item-wpml-ls-38-en"><a href="https://athensriviera.eu" title="English"><img alt="" class="wpml-ls-flag" src="https://athensriviera.eu/wp-content/plugins/sitepress-multilingual-cms/res/flags/en.png"/><span class="wpml-ls-display">English</span></a>
<div class="submenu-toggle"><i class="material-icons">arrow_drop_down</i></div><ul class="sub-menu i-dropdown">
<li class="menu-item wpml-ls-slot-38 wpml-ls-item wpml-ls-item-fr wpml-ls-menu-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-38-fr" id="menu-item-wpml-ls-38-fr"><a href="https://athensriviera.eu/?lang=fr" title="French"><img alt="" class="wpml-ls-flag" src="https://athensriviera.eu/wp-content/plugins/sitepress-multilingual-cms/res/flags/fr.png"/><span class="wpml-ls-display">French</span></a></li>
<li class="menu-item wpml-ls-slot-38 wpml-ls-item wpml-ls-item-es wpml-ls-menu-item wpml-ls-last-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-38-es" id="menu-item-wpml-ls-38-es"><a href="https://athensriviera.eu/?lang=es" title="Spanish"><img alt="" class="wpml-ls-flag" src="https://athensriviera.eu/wp-content/plugins/sitepress-multilingual-cms/res/flags/es.png"/><span class="wpml-ls-display">Spanish</span></a></li>
</ul>
</li>
</ul>
<div class="mobile-nav-button">
</div>
</div>
<div class="i-nav-overlay"></div>
</div>
</div>
</div>
</header>
<div class="c27-top-content-margin"></div>
<section class="i-section">
<div class="container">
<div class="row text-center">
<div class="no-results-wrapper">
<i class="no-results-icon material-icons">mood_bad</i>
<li class="no_job_listings_found">Error 404: The page you are looking for cannot be found.</li>
<a class="buttons button-2" href="https://athensriviera.eu/">Back to homepage</a>
</div>
</div>
</div>
</section>
</div>
<footer class="footer footer-mini">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="footer-bottom">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 copyright">
<p>© Athens Riviera Best - Website By <a href="https://webee.gr">Webee.gr</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<style type="text/css">
            .c27-main-header .logo img { height: 80px; }.c27-main-header:not(.header-scroll) .header-skin { background: rgba(29, 35, 41, 0.97) !important; }.c27-main-header:not(.header-scroll) .header-skin { border-bottom: 1px solid rgba(29, 35, 41, 0.97) !important; } .c27-main-header.header-scroll .header-skin{ background: rgba(29, 35, 41, 0.97) !important; }.c27-main-header.header-scroll .header-skin { border-bottom: 1px solid rgba(29, 35, 41, 0.97) !important; }         </style>
<!-- Modal - SIGN IN-->
<div class="modal modal-27 " id="sign-in-modal" role="dialog">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<div class="sign-in-box element">
<div class="title-style-1">
<i class="material-icons user-area-icon">person</i>
<h5>Sign in</h5>
</div>
<form action="https://athensriviera.eu/my-account/" class="sign-in-form woocomerce-form woocommerce-form-login login" method="POST">
<div class="form-group">
<input id="username" name="username" placeholder="Username" type="text" value=""/>
</div>
<div class="form-group">
<input id="password" name="password" placeholder="Password" type="password"/>
</div>
<input id="woocommerce-login-nonce" name="woocommerce-login-nonce" type="hidden" value="3fff2ceabc"/><input name="_wp_http_referer" type="hidden" value="/wp-includes/pomo/files/"/>
<div class="form-group">
<button class="buttons button-2 full-width" name="login" type="submit" value="Login">
				Sign in			</button>
</div>
<div class="form-info">
<div class="md-checkbox">
<input id="rememberme" name="rememberme" type="checkbox" value="forever"/>
<label class="" for="rememberme">Remember me</label>
</div>
</div>
<div class="forgot-password">
<a href="https://athensriviera.eu/my-account/lost-password/"><i class="material-icons">lock</i>Forgot password?</a>
</div>
</form>
<div class="paper-spinner center-vh" style="width: 24px; height: 24px;">
<div class="spinner-container active">
<div class="spinner-layer layer-1" style="border-color: #777;">
<div class="circle-clipper left">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="gap-patch">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="circle-clipper right">
<div class="circle" style="border-width: 2.5px;"></div>
</div>
</div>
</div>
</div></div> </div>
</div>
</div>
<!-- Quick view modal -->
<div class="modal modal-27 quick-view-modal c27-quick-view-modal" id="quick-view" role="dialog">
<div class="container">
<div class="modal-dialog">
<div class="modal-content"></div>
</div>
</div>
<div class="loader-bg">
<div class="paper-spinner center-vh" style="width: 28px; height: 28px;">
<div class="spinner-container active">
<div class="spinner-layer layer-1" style="border-color: #ddd;">
<div class="circle-clipper left">
<div class="circle" style="border-width: 3px;"></div>
</div><div class="gap-patch">
<div class="circle" style="border-width: 3px;"></div>
</div><div class="circle-clipper right">
<div class="circle" style="border-width: 3px;"></div>
</div>
</div>
</div>
</div> </div>
</div>
<!-- Modal - WC Cart Contents-->
<div class="modal modal-27" id="wc-cart-modal" role="dialog">
<div class="modal-dialog modal-md">
<div class="modal-content">
<div class="sign-in-box">
<div class="widget woocommerce widget_shopping_cart"><h2 class="widgettitle">Cart</h2><div class="widget_shopping_cart_content"></div></div> </div>
</div>
</div>
</div><!-- Root element of PhotoSwipe. Must have class pswp. -->
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
<!-- Background of PhotoSwipe.
It's a separate element as animating opacity is faster than rgba(). -->
<div class="pswp__bg"></div>
<!-- Slides wrapper with overflow:hidden. -->
<div class="pswp__scroll-wrap">
<!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
<div class="pswp__container">
<div class="pswp__item"></div>
<div class="pswp__item"></div>
<div class="pswp__item"></div>
</div>
<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
<div class="pswp__ui pswp__ui--hidden">
<div class="pswp__top-bar">
<!--  Controls are self-explanatory. Order can be changed. -->
<div class="pswp__counter"></div>
<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
<button class="pswp__button pswp__button--share" title="Share"></button>
<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
<!-- element will get class pswp__preloader--active when preloader is running -->
<div class="pswp__preloader">
<div class="pswp__preloader__icn">
<div class="pswp__preloader__cut">
<div class="pswp__preloader__donut"></div>
</div>
</div>
</div>
</div>
<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
<div class="pswp__share-tooltip"></div>
</div>
<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
</button>
<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
</button>
<div class="pswp__caption">
<div class="pswp__caption__center"></div>
</div>
</div>
</div>
</div><script id="mylisting-dialog-template" type="text/template">
	<div class="mylisting-dialog-wrapper">
		<div class="mylisting-dialog">
			<div class="mylisting-dialog--message"></div><!--
			 --><div class="mylisting-dialog--actions">
				<div class="mylisting-dialog--dismiss mylisting-dialog--action">Dismiss</div>
				<div class="mylisting-dialog--loading mylisting-dialog--action hide">
					
<div class="paper-spinner " style="width: 24px; height: 24px;">
	<div class="spinner-container active">
		<div class="spinner-layer layer-1" style="border-color: #777;">
			<div class="circle-clipper left">
				<div class="circle" style="border-width: 2.5px;"></div>
			</div><div class="gap-patch">
				<div class="circle" style="border-width: 2.5px;"></div>
			</div><div class="circle-clipper right">
				<div class="circle" style="border-width: 2.5px;"></div>
			</div>
		</div>
	</div>
</div>				</div>
			</div>
		</div>
	</div>
</script> <a class="back-to-top" href="#">
<i class="mi keyboard_arrow_up"></i>
</a>
<div class="modal modal-27" id="quicksearch-mobile-modal">
<div class="modal-dialog modal-md">
<div class="modal-content">
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
<div class="quick-search-instance text-left" data-focus="always" id="quicksearch-mobile">
<form action="https://athensriviera.eu/explore/" method="GET">
<div class="dark-forms header-search search-shortcode-light">
<i class="mi search"></i>
<input autocomplete="off" name="search_keywords" placeholder="Type your search..." type="search"/>
<div class="instant-results">
<ul class="instant-results-list ajax-results"></ul>
<button class="buttons full-width button-5 search view-all-results all-results" type="submit">
<i class="mi search"></i>View all results				</button>
<button class="buttons full-width button-5 search view-all-results no-results" type="submit">
<i class="mi search"></i>No results				</button>
<div class="loader-bg">
<div class="paper-spinner center-vh" style="width: 24px; height: 24px;">
<div class="spinner-container active">
<div class="spinner-layer layer-1" style="border-color: #777;">
<div class="circle-clipper left">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="gap-patch">
<div class="circle" style="border-width: 2.5px;"></div>
</div><div class="circle-clipper right">
<div class="circle" style="border-width: 2.5px;"></div>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</form>
</div> </div>
</div>
</div>
<script id="case27-basic-marker-template" type="text/template">
	<a href="#" class="marker-icon">
		<div class="marker-img" style="background-image: url({{marker-bg}});"></div>
	</a>
</script>
<script id="case27-traditional-marker-template" type="text/template">
	<div class="cts-marker-pin">
		<img src="https://athensriviera.eu/wp-content/themes/my-listing/assets/images/pin.png">
	</div>
</script>
<script id="case27-user-location-marker-template" type="text/template">
	<div class="cts-geoloc-marker"></div>
</script>
<script id="case27-marker-template" type="text/template">
	<a href="#" class="marker-icon {{listing-id}}">
		{{icon}}
		<div class="marker-img" style="background-image: url({{marker-bg}});"></div>
	</a>
</script> <div class="wordpress-gdpr-popup wordpress-gdpr-popup-small wordpress-gdpr-popup-bottom" style="background-color: #f7f7f7; color: #333333;">
<div class="wordpress-gdpr-popup-container">
<a class="wordpress-gdpr-popup-close" href="#" id="wordpress-gdpr-popup-close" style="background-color: #000000;">
<i class="fa fa-times" style="color: #FFFFFF;"></i>
</a>
<div class="wordpress-gdpr-popup-text"><p>We use cookies to give you the best online experience. By agreeing you accept the use of cookies in accordance with our cookie policy.</p>
</div>
<div class="wordpress-gdpr-popup-actions">
<div class="wordpress-gdpr-popup-actions-buttons">
<a class="wordpress-gdpr-popup-agree" href="#" style="background-color: #4CAF50; color: #FFFFFF;">I accept</a>
<a class="wordpress-gdpr-popup-decline" href="#" style="background-color: #F44336; color: #FFFFFF;">I decline</a>
<div class="gdpr-clear"></div>
</div>
<div class="wordpress-gdpr-popup-actions-links">
<a class="wordpress-gdpr-popup-read-more" href="https://athensriviera.eu/privacy-center/cookie-policy/" style="color: #FF5722;">Cookie Policy</a>
</div>
</div>
</div>
</div>
<div class="wordpress-gdpr-privacy-settings-popup-container">
<div class="wordpress-gdpr-privacy-settings-popup" style="background-color: #FFFFFF; color: #333333;">
<a class="wordpress-gdpr-privacy-settings-popup-close" href="#" id="wordpress-gdpr-privacy-settings-popup-close" style="background-color: #000000;" title="close">
<i class="fa fa-times" style="color: #FFFFFF;"></i>
</a>
</div>
<div class="wordpress-gdpr-privacy-settings-popup-backdrop"></div>
</div>
<div class="wpfbmb-55064 wpfbmb-button-wrap wpfbmb-animation-1 wpfbmb-button-template-10 wpfbmb-bottom wpfbmb-right wpfbmb-tooltip-disable">
<div class=" wpfbmb-icon-only wpfbmb-button-postion-right wpfbmb-text-inner-wrapper">
<div class="wpfbmb-tooltip-enable-text"> </div>
<a href="https://m.me/moregreece"><i class="socicon-messenger"></i></a>
</div>
</div>
<style>
  .wpfbmb-55064 .wpfbmb-button-template-1 a, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-1 a {
    background-color: ;
    color: ;
    }
   .wpfbmb-55064 .wpfbmb-button-template-2 a, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-2 a {
    background-color:  ;
    color: ;
   }
  .wpfbmb-55064 .wpfbmb-button-template-2 .wpfbmb-text-icon-both a i, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-2 a i {
    background-color: ;
    color: ;
    }
    .wpfbmb-55064 .wpfbmb-button-template-3 a, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-3 a {
    background-color:  ;
    color: ;
    }
    .wpfbmb-55064 .wpfbmb-button-template-4 a, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-4 a {
    background-color:  ;
    color: ;
    }
    .wpfbmb-55064 .wpfbmb-button-template-5 a, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-5 a {
    background-color: ;
    color:;
    }
    .wpfbmb-55064.wpfbmb-button-template-5 .wpfbmb-text-icon-both a i, #wpfbmb-button-menu-template.wpfbmb-menu-button-wrap.wpfbmb-button-template-5 a i {
    background-color: ;
    color: ;
    }
    .wpfbmb-55064.wpfbmb-button-template-10 .wpfbmb-text-icon-both a { 
    background-color: ;
    color: ;
   }
   .wpfbmb-55064.wpfbmb-button-template-10 .wpfbmb-icon-only a { 
    background-color: ;
    color: ;
   }
   .wpfbmb-55064.wpfbmb-button-template-10 .wpfbmb-text-only a { 
    background-color: ;
    color: ;
   }
   .wpfbmb-55064.wpfbmb-button-template-2 .wpfbmb-text-icon-both a i {
    background-color:;
    color: ;  
   }
   .wpfbmb-55064.wpfbmb-button-template-3 a {
    background-color: ;
    color: ;
  }
   .wpfbmb-55064.wpfbmb-button-template-4 a i {
    background-color: ;
    color: ;
  }

  .wpfbmb-55064.wpfbmb-button-template-5 .wpfbmb-text-icon-both a i {
    background-color: ;
    color: ;
  }
  .wpfbmb-55064.wpfbmb-button-template-5 .wpfbmb-text-icon-both a i {
    background-color: ;
    }

  .wpfbmb-55064.wpfbmb-button-template-6 .wpfbmb-text-icon-both a i {
    color: ;
    background-color: ;
  }
  .wpfbmb-55064.wpfbmb-button-template-6 .wpfbmb-text-icon-both.wpfbmb-button-postion-right a i:after {
            }
    .wpfbmb-55064.wpfbmb-button-template-6 .wpfbmb-text-icon-both.wpfbmb-button-postion-left a i:after {
                     }
  .wpfbmb-55064.wpfbmb-button-template-7 .wpfbmb-text-icon-both a i {
    color: ;
    background-color: ;
  }
  .wpfbmb-55064.wpfbmb-button-template-7 .wpfbmb-icon-only a i {
    color: ;
    background-color: ;
  }
  .wpfbmb-55064.wpfbmb-button-template-8 .wpfbmb-text-icon-both a i {
    background-color: ;
    color: ;
   }
  .wpfbmb-55064.wpfbmb-button-template-8 .wpfbmb-text-icon-both.wpfbmb-button-postion-right a i:after {   }
   .wpfbmb-55064.wpfbmb-button-template-8 .wpfbmb-text-icon-both.wpfbmb-button-postion-left a i:after {   }

   .wpfbmb-55064.wpfbmb-button-template-8 a:before {
    border: 3px solid ;
   }
   /*viber button template 9*/
  .wpfbmb-55064.wpfbmb-55064.wpfbmb-button-template-9 .wpfbmb-text-icon-both a i {
    background-color: ;
    color: ;
  }
  .wpfbmb-55064.wpfbmb-button-template-9 .wpfbmb-text-icon-both.wpfbmb-button-postion-right a i:after {
      }
/*.wpfbmb-55064.wpfbmb-button-template-9 .wpfbmb-text-icon-both.wpfbmb-button-postion-left a i:after {
      }*/
.wpfbmb-55064.wpfbmb-button-template-9 .wpfbmb-text-icon-both.wpfbmb-button-postion-left a i:after {}
.wpfbmb-55064.wpfbmb-button-template-9 .wpfbmb-text-icon-both.wpfbmb-button-postion-right a i:after{
      }
  .wpfbmb-55064.wpfbmb-55064.wpfbmb-button-template-10 .wpfbmb-text-icon-both a i:after {
        }
 .wpfbmb-55064.wpfbmb-button-template-10 .wpfbmb-text-icon-both a i {
    background-color:;
    color: ;
}

 
 
</style>
<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/athensriviera.eu\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/6360f.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/athensriviera.eu\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/dc06c.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/63a69.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_949e33eec6a20053669988fc3e14e30d","fragment_name":"wc_fragments_949e33eec6a20053669988fc3e14e30d","request_timeout":"5000"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/b4041.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var gdpr_options = {"ajaxURL":"https:\/\/athensriviera.eu\/wp-admin\/admin-ajax.php","cookieLifetime":"30","geoIP":"0","popupExcludePages":"","acceptanceText":"You must accept our Privacy Policy.","termsAcceptanceText":"You must accept our Terms and Conditions."};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/5cfcd.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Ld909AUAAAAAKL9Kjbf4AiVfjzMnmJ_oCFHE7GA&amp;ver=3.0" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3OZnZmL7R-455scqUxsJxPYvAlKwNnNw&amp;libraries=places&amp;v=3" type="text/javascript"></script>
<script src="https://athensriviera.eu/wp-content/cache/minify/0d01d.js"></script>
<script type="text/javascript">
moment.locale( 'en_US', {"months":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthsShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"weekdaysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"week":{"dow":1},"longDateFormat":{"LT":"g:i a","LTS":null,"L":null,"LL":"F j, Y","LLL":"F j, Y g:i a","LLLL":null}} );
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/1c0dd.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var pwsL10n = {"unknown":"Password strength unknown","short":"Very weak","bad":"Weak","good":"Medium","strong":"Strong","mismatch":"Mismatch"};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/f0d29.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_password_strength_meter_params = {"min_password_strength":"3","stop_checkout":"","i18n_password_error":"Please enter a stronger password.","i18n_password_hint":"Hint: The password should be at least twelve characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! \" ? $ % ^ & )."};
/* ]]> */
</script>
<script src="https://athensriviera.eu/wp-content/cache/minify/9dffd.js"></script>
<script type="text/javascript">
( function( grecaptcha, sitekey, actions ) {

	var wpcf7recaptcha = {

		execute: function( action ) {
			grecaptcha.execute(
				sitekey,
				{ action: action }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		},

		executeOnHomepage: function() {
			wpcf7recaptcha.execute( actions[ 'homepage' ] );
		},

		executeOnContactform: function() {
			wpcf7recaptcha.execute( actions[ 'contactform' ] );
		},

	};

	grecaptcha.ready(
		wpcf7recaptcha.executeOnHomepage
	);

	document.addEventListener( 'change',
		wpcf7recaptcha.executeOnContactform, false
	);

	document.addEventListener( 'wpcf7submit',
		wpcf7recaptcha.executeOnHomepage, false
	);

} )(
	grecaptcha,
	'6Ld909AUAAAAAKL9Kjbf4AiVfjzMnmJ_oCFHE7GA',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
</body>
</html>
