<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-139545368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139545368-1');
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="SemiColonWeb" name="author"/>
<!-- Stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/style.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/swiper.css" rel="stylesheet" type="text/css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.5/js/swiper.min.js"></script>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/dark.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/font-icons.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/js/slick/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/js/slick/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://www.satriabahana.co.id:443/asset/sbs_style/sweetalert/src/sweetalert.css" rel="stylesheet" type="text/css"/>
<script src="https://www.satriabahana.co.id:443/asset/sbs_style/js/jquery.js"></script>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script language="javascript" type="text/javascript">

		    var map;
		    var geocoder;
		    function InitializeMap() {

		        var latlng = new google.maps.LatLng(-34.397, 150.644);
		        var myOptions =
		        {
		            zoom: 8,
		            center: latlng,
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            disableDefaultUI: true
		        };
		        map = new google.maps.Map(document.getElementById("map"), myOptions);
		    }

		    function FindLocaiton() {
		        geocoder = new google.maps.Geocoder();
		        InitializeMap();

		        var address = document.getElementById("addressinput").value;
		        geocoder.geocode({ 'address': address }, function (results, status) {
		            if (status == google.maps.GeocoderStatus.OK) {
		                map.setCenter(results[0].geometry.location);
		                var marker = new google.maps.Marker({
		                    map: map,
		                    position: results[0].geometry.location
		                });

		            }
		            else {
		                alert("Geocode was not successful for the following reason: " + status);
		            }
		        });

		    }


		    function Button1_onclick() {
		        FindLocaiton();
		    }

		    window.onload = InitializeMap;

		</script>
<!-- Document Title -->
<title>SBS Mining Solution</title>
</head>
<body class="stretched no-transition">
<!-- Document Wrapper -->
<div class="clearfix" id="wrapper">
<!-- Header -->
<header class="transparent-header full-header" data-sticky-class="not-dark" id="header">
<div id="header-wrap">
<div class="container clearfix">
<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
<!-- Logo -->
<div id="logo">
<a class="standard-logo" data-dark-logo="https://www.satriabahana.co.id:443/asset/logo/1549422920-logo-baru-sbs.svg" href="https://satriabahana.co.id/"><img alt="SBS Logo" src="https://www.satriabahana.co.id:443/asset/logo/1549422920-logo-baru-sbs.svg"/></a>
<a class="retina-logo" data-dark-logo="https://www.satriabahana.co.id:443/asset/logo/1549422920-logo-baru-sbs.svg" href="https://satriabahana.co.id/"><img alt="SBS Logo" src="https://www.satriabahana.co.id:443/asset/logo/1549422920-logo-baru-sbs.svg"/></a>
</div><!-- #logo end -->
<!-- Primary Navigation -->
<div class="top-search" id="top-search">
<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
<form action="https://satriabahana.co.id/web/search_result">
<input class="form-control search-form" name="q" placeholder="Type &amp; Hit Enter.." type="text" value=""/>
</form>
</div><!-- #top-search end -->
<div class="lang" id="top_language" style="float: right; z-index: 9999999; position: absolute; margin-right: 30px; right: 0; font-size: 12px;">
<a href="https://satriabahana.co.id/web/switchLang/english" id="lang" onchange="this.form.submit()" style="color:#667280; font-weight:800; float: left; margin-right: 10px; text-decoration: underline !important"><img class="img-fluid" src="https://www.satriabahana.co.id:443/asset/img/us.png?&gt;"/> </a>
<a href="https://satriabahana.co.id/web/switchLang/indonesia" id="lang" onchange="this.form.submit()" style="color:#667280; font-weight:800; float: left; "><img class="img-fluid" src="https://www.satriabahana.co.id:443/asset/img/id.png?&gt;"/> </a>
</div>
<nav class="dark" id="primary-menu">
<ul>
<!-- Menu Beranda -->
<li class="">
<a href="https://satriabahana.co.id/">
<div class="main-menu">Home</div>
</a>
</li>
<li class="mega-menu ">
<a href="#">
<div class="main-menu">About Us</div>
</a>
<ul>
<li>
<a href="https://satriabahana.co.id/company-profile#1">
<div class="main-menu">Company Profile</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/organization#2">
<div class="main-menu">Organization</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/ceo-notes#3">
<div class="main-menu">CEO Notes</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/good-corporate-governance#4">
<div class="main-menu">Good Corporate Governance</div>
</a>
</li>
</ul>
</li>
<li class="mega-menu ">
<a href="#">
<div class="main-menu">Business</div>
</a>
<ul>
<li>
<a href="https://satriabahana.co.id/bisnis-heavy-equipment#5">
<div class="main-menu">Heavy Equipment</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/bisnis-mining-contractor#6">
<div class="main-menu">Mining Contractor</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/bisnis-mining-investment#7">
<div class="main-menu">Mining Investment</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/bisnis-asset-management#8">
<div class="main-menu">Asset Management</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/bisnis-projects#9">
<div class="main-menu">Projects</div>
</a>
</li>
</ul>
</li>
<li class="mega-menu ">
<a href="#">
<div class="main-menu">News</div>
</a>
<ul>
<li>
<a href="https://satriabahana.co.id/news#10">
<div class="main-menu">News</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/br-articles#11">
<div class="main-menu">Articles</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/tokoh-inspirasi#49">
<div class="main-menu">Tokoh Inspirasi</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/highlight#50">
<div class="main-menu">Highlight</div>
</a>
</li>
</ul>
</li>
<li class="mega-menu ">
<a href="#">
<div class="main-menu">CSR</div>
</a>
<ul>
<li>
<a href="https://satriabahana.co.id/csr-about-csr#14">
<div class="main-menu">About CSR</div>
</a>
</li>
<li>
<a href="https://satriabahana.co.id/csr-news-activitites#15">
<div class="main-menu">CSR News &amp; Activitites</div>
</a>
</li>
</ul>
</li>
<li class="mega-menu ">
<a href="https://satriabahana.co.id/career">
<div class="main-menu">Career</div>
</a>
</li>
<li class="mega-menu ">
<a href="https://satriabahana.co.id/contact-us">
<div class="main-menu">Contact Us</div>
</a>
</li>
<li class="mega-menu ">
<a href="https://satriabahana.co.id/sdfghjk">
<div class="main-menu">E-PROCUREMENT</div>
</a>
</li>
</ul>
<!-- Top Search -->
</nav><!-- #primary-menu end -->
</div>
</div>
</header><!-- #header end -->
<!-- Header Image -->
<!-- Content --> <section id="content"> <div class="content-wrap"> <div class="container clearfix"> <div class="tabs customjs" id="side-navigation"> <!-- Navigation Tab --> <!-- Tab content --> <!-- Get category based on parent_menu --> </div> </div> </div> </section> <!-- #content end --> <!-- Footer --> <footer class="dark" id="footer"> <div class="footer-container"> <!-- Footer Widgets --> <div class="footer-widgets-wrap clearfix"> <div class="col-lg-12 footer-menu"> <div class="col-lg-12 benny"> <ul class="ul-footer-menu"> <li class="footer-sub-menu"> <div class="footer-judul-sub-menu">About Us</div> <ul class="sub-menu"> <li><a href="https://satriabahana.co.id/company-profile#1">Company Profile</a></li> <li><a href="https://satriabahana.co.id/organization#2">Organization</a></li> <li><a href="https://satriabahana.co.id/ceo-notes#3">CEO Notes</a></li> <li><a href="https://satriabahana.co.id/good-corporate-governance#4">Good Corporate Governance</a></li> </ul> </li> <li class="footer-sub-menu"> <div class="footer-judul-sub-menu">Business</div> <ul class="sub-menu"> <li><a href="https://satriabahana.co.id/bisnis-heavy-equipment#5">Heavy Equipment</a></li> <li><a href="https://satriabahana.co.id/bisnis-mining-contractor#6">Mining Contractor</a></li> <li><a href="https://satriabahana.co.id/bisnis-mining-investment#7">Mining Investment</a></li> <li><a href="https://satriabahana.co.id/bisnis-asset-management#8">Asset Management</a></li> <li><a href="https://satriabahana.co.id/bisnis-projects#9">Projects</a></li> </ul> </li> <li class="footer-sub-menu"> <div class="footer-judul-sub-menu">News</div> <ul class="sub-menu"> <li><a href="https://satriabahana.co.id/news#10">News</a></li> <li><a href="https://satriabahana.co.id/br-articles#11">Articles</a></li> <li><a href="https://satriabahana.co.id/tokoh-inspirasi#49">Tokoh Inspirasi</a></li> <li><a href="https://satriabahana.co.id/highlight#50">Highlight</a></li> </ul> </li> <li class="footer-sub-menu"> <div class="footer-judul-sub-menu">CSR</div> <ul class="sub-menu"> <li><a href="https://satriabahana.co.id/csr-about-csr#14">About CSR</a></li> <li><a href="https://satriabahana.co.id/csr-news-activitites#15">CSR News &amp; Activitites</a></li> </ul> </li> </ul> </div> </div> </div> <!-- Copyrights --> <div id="copyrights"> <div class="container container-copyrights clearfix"> <div class="col_half copyrights-sbs">								Copyrights © 2019 All Rights Reserved SBS Mining Solution<br/> <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> </div> <div class="col_half col_last tright"> <div class="fright clearfix"> <div class="info-perusahaan"> <!-- <span><a href="#">Jamkes</a></span>										<span>|</span> --> <span><a href="https://satriabahana.co.id/career">Career</a></span> <span>|</span> <span><a href="https://satriabahana.co.id/contact-us">Contact us</a></span> </div> <div class="clearfix bottommargin-sm social-media"> <a class="social-icon si-small si-borderless" href="https://www.facebook.com/ptsatriabahanasarana" target="_blank"> <i class="icon-facebook"></i> <i class="icon-facebook"></i> </a> </div> <div class=" clearfix bottommargin-sm social-media"> <a class="social-icon si-small si-borderless" href="https://www.youtube.com/channel/UCuK3fjcaFp7DQVHg_MV7pZA"> <i class="icon-youtube"></i> <i class="icon-youtube"></i> </a> </div> <div class=" clearfix bottommargin-sm social-media"> <a class="social-icon si-small si-borderless" href="https://www.instagram.com/satriabahanasarana/?hl=id"> <i class="icon-instagram"></i> <i class="icon-instagram"></i> </a> </div> </div> <div class="clear"></div> </div> </div> </div><!-- #copyrights end --> </div> </footer> </div><!-- #wrapper end --> <!-- Go To Top --> <div class="icon-angle-up" id="gotoTop"></div> <script>				$(function(){				if (window.matchMedia("(min-width: 900px)").matches){				   if (!!$('#sticky').length) {				      var el = $('#sticky');				      var stickyTop = $('#sticky').offset().top; 				      var footerTop = $('#footer').offset().top; 				      var stickyHeight = $('#sticky').height();				      var limit = footerTop - stickyHeight - 20;				      $(window).scroll(function(){ 				          var windowTop = $(window).scrollTop(); 				            				          if (stickyTop < windowTop){				            el.css({ position: 'fixed', top: 70, width: 300 });				          }				          else {				             el.css('position','static');				          }				            				          if (limit < windowTop) {				          var diff = limit - windowTop;				          el.css({top: diff});				          }     				        });				   }				}		});	</script> <!-- External JavaScripts --> <!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script> --> <script src="https://www.satriabahana.co.id:443/asset/sbs_style/js/plugins.js"></script> <!-- Footer Scripts --> <script src="https://www.satriabahana.co.id:443/asset/sbs_style/js/functions.js"></script> <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script> <script src="https://www.satriabahana.co.id:443/asset/sbs_style/js/slick/slick/slick.min.js" type="text/javascript"></script> <script src="https://www.satriabahana.co.id:443/asset/sbs_style/sweetalert/sweetalert2.all.min.js"></script> <script type="text/javascript">			$(document).ready(function() {  new Swiper('.swiper-container', {    speed: 400,    spaceBetween: 100,    autoplay: true,    disableOnInteraction: true,  });});$(document).ready(function() {	$('.slider-caption h3').Show(function() {  });});		</script> <script type="text/javascript">  		    $(document).ready(function()		    {			    $('.partners-carousel').slick({					dots: false,					autoplay:true,					autoplaySpeed:2000,					infinite: true,				    speed: 300,				    slidesToShow: 4,				    slidesToScroll: 2,				    responsive: [			            {						    breakpoint: 1024,						    settings: {						        slidesToShow: 3,						        slidesToScroll: 3,						        infinite: true,						        dots: true						    }					    },					    {						    breakpoint: 600,						    settings: {						        slidesToShow: 2,						        slidesToScroll: 2					        }					    },					    {						    breakpoint: 500,						    settings: {						        slidesToShow: 2,						        slidesToScroll: 2						    }					    },					    {						    breakpoint: 480,						    settings: {						        slidesToShow: 2,						        slidesToScroll: 2						    },					        breakpoint: 425,						    settings: {						        slidesToShow: 2,						        slidesToScroll: 2						    }					    }					    // You can unslick at a given breakpoint now by adding:					    // settings: "unslick"					    // instead of a settings object					]				});		    });		</script> <script>			// Dropdown Menu			var dropdown = document.querySelectorAll('.dropdown');			var dropdownArray = Array.prototype.slice.call(dropdown,0);			dropdownArray.forEach(function(el)			{				var button = el.querySelector('a[data-toggle="dropdown"]'),				menu = el.querySelector('.dropdown-menu'),				arrow = button.querySelector('i.icon-arrow');				button.onclick = function(event) 				{					if(!menu.hasClass('show')) 					{						menu.classList.add('show');						menu.classList.remove('hide');						arrow.classList.add('open');						arrow.classList.remove('close');						event.preventDefault();					}					else {						menu.classList.remove('show');						menu.classList.add('hide');						arrow.classList.remove('open');						arrow.classList.add('close');						event.preventDefault();					}				};			})			Element.prototype.hasClass = function(className) {		    return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);			};		</script> <script>			$(function() 			{				$( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });			});		</script> <script>			$(document).ready(function()			{				$(".sidebar-menu > li.have-children a").on("click", function(i)				{					i.preventDefault();				    if( ! $(this).parent().hasClass("active") )				    {				        $(".sidebar-menu li ul").slideUp();				        $(this).next().slideToggle();				        $(".sidebar-menu li").removeClass("active");				        $(this).parent().addClass("active");				    }				    else{				        $(this).next().slideToggle();				        $(".sidebar-menu li").removeClass("active");				    }				});			});		</script> <!-- untuk klik langsung ke scroll ke konten yang dituju --> <script>			$('ul.sidebar-menu li ul').find('a').click(function()			{				var $href = $(this).attr('href');				var $anchor = $('#'+$href).offset();				$('body').animate({ scrollTop: $anchor.top });				return false;			});		</script> <script>			$(function() 			{				var id = $(this).attr('data-related'); 				$("div.project-detail").each(function()				{				    $(this).hide();				    				});				$('a#link-detail').on( "click", function(e) {				    e.preventDefault();				    $("div.box-project-detail").show();				    $('.project-detail-navigation').hide();				    var id = $(this).attr('data-related'); 				    $("div.project-detail").each(function()				    {				        $(this).hide();				        if($(this).attr('id') == id) 				        {				            				            $(this).show();				        }				    });				});				// $('#back-to-project').on( "click", function() 				$("a#back-to-project").on('click', function() 				{					$("div.box-project-detail").hide();					$("div.project-detail").each(function()					{					    $(this).hide();					    					});										$('.project-detail-navigation').show();				});			});		</script> <script>							$(function() 			{		    	$('.projects').hide();		    	var v = $('#project-category-selector').find("option:first-child").val();		    	$('#' + v).show();		        $('#project-category-selector').change(function()		        {		            $('.projects').hide();		            $('#' + $(this).val()).show();		        });			});		</script> <!-- <script>			var slideIndex = 1;			showSlides(slideIndex);			function plusSlides(n) 			{				showSlides(slideIndex += n);			}			function currentSlide(n) 			{				showSlides(slideIndex = n);			}			function showSlides(n) 			{				var i;				var slides = document.getElementsByClassName("mySlides");				var dots = document.getElementsByClassName("dot");				if (n > slides.length) {slideIndex = 1}   				if (n < 1) {slideIndex = slides.length}				for (i = 0; i < slides.length; i++) 				{				    slides[i].style.display = "none";  				}				for (i = 0; i < dots.length; i++) {				    dots[i].className = dots[i].className.replace(" active", "");				}				  				slides[slideIndex-1].style.display = "block";  				dots[slideIndex-1].className += " active";			}		</script> --> <script>			$(document).ready(function()			{			    // Activate Carousel			    $("#myCarousel").carousel();			    			    // Enable Carousel Indicators			    $(".item1").click(function()			    {				    $("#myCarousel").carousel(0);			    });			    $(".item2").click(function()			    {				    $("#myCarousel").carousel(1);			    });			    $(".item3").click(function()			    {				    $("#myCarousel").carousel(2);			    });			    $(".item4").click(function()			    {				    $("#myCarousel").carousel(3);			    });			    			    // Enable Carousel Controls			    $(".left").click(function(){				    $("#myCarousel").carousel("prev");			    });			    $(".right").click(function(){				    $("#myCarousel").carousel("next");			    });			});		</script> <script type="text/javascript">		   	$(document).ready(function () 		   	{ 								$('.slider-for').slick({				   slidesToShow: 1,				   slidesToScroll: 1,				   fade: false,				   dots: true,				   autoplay: true				});								// $("#img-slide1").click(function () {				// 	$("#img-1").show();				// 	$("#img-2,#img-3").hide();				// 		$("#img-slide2").click(function () {				// 		$("#img-1,#img-3").hide();				// 		$("#img-2").show();							// 			$("#img-slide3").click(function () {				// 			$("#img-1,#img-2").hide();				// 			$("#img-3").show();				// 			$("#img-slide4").click(function () {				// 				$("#img-1").show();				// 				$("#img-2,#img-3").hide();				// 					$("#img-slide5").click(function () {				// 					$("#img-1,#img-3").hide();				// 					$("#img-2").show();							// 						$("#img-slide6").click(function () {				// 						$("#img-1,#img-2").hide();				// 						$("#img-3").show();				// 						$("#img-slide7").click(function () {				// 							$("#img-1").show();				// 							$("#img-2,#img-3").hide();				// 								$("#img-slide8").click(function () {				// 								$("#img-1,#img-3").hide();				// 								$("#img-2").show();							// 							});				// 						});				// 					});				// 				});				// 			});				// 		});				// 	});				// });									});	   </script> <script type="text/javascript">			$("#carouselExample").on("slide.bs.carousel", function(e) 			{				var $e = $(e.relatedTarget);				var idx = $e.index();				var itemsPerSlide = 3;				var totalItems = $(".carousel-item").length;			    if (idx >= totalItems - (itemsPerSlide - 1)) 			    {				    var it = itemsPerSlide - (totalItems - idx);				    for (var i = 0; i < it; i++) 				    {				        // append slides to end				        if (e.direction == "left") 				        {				            $(".carousel-item")				            .eq(i)				            .appendTo(".carousel-inner");				        } else {				            $(".carousel-item")				            .eq(0)				            .appendTo(".carousel-inner");				        }				    }			    }			});		</script> </body> </html>