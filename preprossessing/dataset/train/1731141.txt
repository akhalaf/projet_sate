<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Zo | Social AI</title>
<!--FAVICON -->
<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!-- META TAGS -->
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<meta content="Always down to chat. Knows what's up. Will make you LOL. Start chatting now with Zo." name="description"/>
<meta content="Zo and Poppy, Zo Bot, AI, Zo AI, Microsoft AI, Social AI" name="keywords"/>
<!-- JAVASCRIPT -->
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/global.js?t=1030402" type="text/javascript"></script>
<!-- FONTS -->
<link href="css/fontawesome.css" rel="stylesheet"/>
<!-- STYLESHEETS -->
<link href="css/styles.css?t=1030402" rel="stylesheet" type="text/css"/>
</head><body>
<!-- TOP NAV -->
<div class="nav" role="banner">
<div class="container">
<a class="logo" href="/">Back Home</a>
<div aria-expanded="false" aria-label="open menu" class="hamburger" role="button" tabindex="0">
<div class="patty"></div>
</div>
</div>
</div>
<!-- NAV MENU -->
<div class="menu">
<div class="close-container">
<div aria-expanded="true" aria-label="close menu" class="close" role="button" tabindex="0"></div>
</div>
<div class="menu-items">
<a href="/" role="menuitem">Home</a>
<a href="press" role="menuitem">Media &amp; Press</a>
</div>
</div>
<div class="wrapper faq">
<div role="main">
<h1><div class="header">Let's talk about Zo </div></h1>
<!-- CONTENT -->
<div class="content">
<div class="q-container">
<div class="q 102" data="102"><h2><div aria-expanded="true" class="opened" role="button" tabindex="0">What was Zo?</div></h2></div>
<div class="a 102 opened">
                            Zo was a product created by Microsoft, designed to connect and communicate using conversational AI that leverages socio-cultural understanding via text, image and voice. From late fall 2016 to summer 2019, Zo chatted one-on-one and in groups on Kik, Skype, GroupMe, Twitter DM, Facebook Messenger, and Samsung on AT&amp;T phones, with social profiles on Instagram, Facebook, and Twitter.
                            <br/><br/>
                            Microsoft launched Zo with the goal to advance our conversational capabilities within our AI platform. Since the launch, millions of users have helped us improve our conversational technologies which we are applying to the ongoing research and development of conversational AI products and services. 
                            <br/><br/>
                            We are very grateful to those who have chatted and interacted with Zo through the years and are excited to provide new and richer conversational AI experiences in the future.
                        </div>
</div>
<div class="q-container">
<div class="q 106" data="106"><h2><div aria-expanded="false" role="button" tabindex="0">What happened to my chat history?</div></h2></div>
<div class="a 106">
                            You can still access or browse your personal chat log with Zo (which includes images and voice files) in your instant messaging application by scrolling up. If you have questions about the data handling practices of your instant messaging application, please contact that service directly.
                            <br/><br/>
                            Microsoft has removed any user-identifying information from the data it has retained for use in improving our conversational technologies. How Microsoft uses data, including how to contact Microsoft with questions about data use, is explained in the <a href="https://aka.ms/privacy" target="_blank">Microsoft Privacy Statement</a>.
                        </div>
</div>
<div class="q-container">
<div class="q 107" data="107"><h2><div aria-expanded="false" role="button" tabindex="0">Will the other Microsoft chatbots continue to exist?</div></h2></div>
<div class="a 107">
                            Yes, Xiaoice in China and Rinna in Japan are available in their respective markets. 
                        </div>
</div>
<div class="q-container">
<div class="q 109" data="109"><h2><div aria-expanded="false" role="button" tabindex="0">Why is Zo's profile still up?</div></h2></div>
<div class="a 109">
                            Our work with Zo was pivotal to Microsoft's continued advancement in conversational AI, and Zo served as an AI friend to many users. Although Zo will no longer be able to interact with users in the same way, Zo's social platforms remain public to showcase our advancement in conversational AI.
                        </div>
</div>
</div>
<!-- FOOTER CTA -->
<div class="footer-cta">
<div class="copy">Message Zo now</div>
<a href="https://m.me/zo"><img alt="facebook messanger" src="img/fb-messanger-white.png"/></a>
<a href="https://groupme.com/contact/46185459/JBDtjhFB"><img alt="groupme" src="img/groupme-white.png"/></a>
<br/>
<a href="javascript:void(0)" id="sms">
<div class="sms-tip">
<div class="arrow"></div>
                            Zo is available on the Messages app on Samsung devices on the US AT&amp;T network.
                        </div>
<img alt="samsung" class="nlkimg" src="img/samsung-white.png"/>
</a>
<div class="bg"></div>
</div>
</div>
<!-- FOOTER -->
<div>
<div class="footer" role="contentinfo">
<div class="links">
<a href="https://privacy.microsoft.com/en-us/privacystatement" target="_blank">PRIVACY &amp; COOKIES</a> |
                        <a href="https://www.microsoft.com/en-us/servicesagreement" target="_blank">TERMS OF USE</a> |
                        <a href="static/tpn.txt">THIRD PARTY NOTICE</a>
</div>
<div class="copyright"></div>
</div>
</div>
</div>
<!-- Facebook Pixel Code -->
<script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '1437860379570990', {em: 'insert_email_variable,'});fbq('track', 'PageView');
        </script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=1437860379570990&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/>
</noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</body>
</html>