<!DOCTYPE html>
<html class="avada-html-layout-wide avada-html-header-position-top" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page not found – View Point Lodge</title>
<link href="//www.viewpointlodge.co.ke" rel="dns-prefetch"/>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.viewpointlodge.co.ke/feed/" rel="alternate" title="View Point Lodge » Feed" type="application/rss+xml"/>
<link href="https://www.viewpointlodge.co.ke/comments/feed/" rel="alternate" title="View Point Lodge » Comments Feed" type="application/rss+xml"/>
<link href="https://www.viewpointlodge.co.ke/wp-content/uploads/favicon-32x32-1.png" rel="shortcut icon" type="image/x-icon"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.viewpointlodge.co.ke\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.viewpointlodge.co.ke/wp-content/themes/Avada/assets/css/style.min.css?ver=6.2.3" id="avada-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://www.viewpointlodge.co.ke/wp-content/themes/Avada/assets/css/ie.min.css?ver=6.2.3' type='text/css' media='all' />
<style id='avada-IE-inline-css' type='text/css'>
.avada-select-parent .select-arrow{background-color:#ffffff}
.select-arrow{background-color:#ffffff}
</style>
<![endif]-->
<link href="https://www.viewpointlodge.co.ke/wp-content/uploads/fusion-styles/d0745b21ff809c25e8c7c201d1daa8a9.min.css?ver=2.2.3" id="fusion-dynamic-css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.viewpointlodge.co.ke/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<link href="https://www.viewpointlodge.co.ke/wp-json/" rel="https://api.w.org/"/><link href="https://www.viewpointlodge.co.ke/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.viewpointlodge.co.ke/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<style id="css-fb-visibility" type="text/css">@media screen and (max-width: 640px){body:not(.fusion-builder-ui-wireframe) .fusion-no-small-visibility{display:none !important;}}@media screen and (min-width: 641px) and (max-width: 1024px){body:not(.fusion-builder-ui-wireframe) .fusion-no-medium-visibility{display:none !important;}}@media screen and (min-width: 1025px){body:not(.fusion-builder-ui-wireframe) .fusion-no-large-visibility{display:none !important;}}</style><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <script type="text/javascript">
			var doc = document.documentElement;
			doc.setAttribute( 'data-useragent', navigator.userAgent );
		</script>
</head>
<body class="error404 fusion-image-hovers fusion-pagination-sizing fusion-button_size-xlarge fusion-button_type-3d fusion-button_span-no avada-image-rollover-circle-no avada-image-rollover-yes avada-image-rollover-direction-fade fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop avada-has-rev-slider-styles fusion-disable-outline fusion-sub-menu-fade mobile-logo-pos-left layout-wide-mode avada-has-boxed-modal-shadow-none layout-scroll-offset-full avada-has-zero-margin-offset-top fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v3 avada-responsive avada-footer-fx-none avada-menu-highlight-style-arrow fusion-search-form-classic fusion-main-menu-search-dropdown fusion-avatar-square avada-sticky-shrinkage avada-dropdown-styles avada-blog-layout-large avada-blog-archive-layout-grid avada-header-shadow-no avada-menu-icon-position-left avada-has-mainmenu-dropdown-divider avada-has-header-100-width avada-has-pagetitle-bg-full avada-has-pagetitle-bg-parallax avada-has-breadcrumb-mobile-hidden avada-has-titlebar-bar_and_content avada-header-border-color-full-transparent avada-has-pagination-padding avada-flyout-menu-direction-fade avada-ec-views-v1">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div id="boxed-wrapper">
<div class="fusion-sides-frame"></div>
<div class="fusion-wrapper" id="wrapper">
<div id="home" style="position:relative;top:-1px;"></div>
<header class="fusion-header-wrapper">
<div class="fusion-header-v3 fusion-logo-alignment fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1 fusion-mobile-menu-design-modern">
<div class="fusion-secondary-header">
<div class="fusion-row">
<div class="fusion-alignleft">
<div class="fusion-contact-info"><span class="fusion-contact-info-phone-number">Call Us Today! <a href="tel:+254112493351">+254112493351</a></span><span class="fusion-header-separator">|</span><span class="fusion-contact-info-email-address"><a href="mailto:info@viewpointlodge.co.ke">info@viewpointlodge.co.ke</a></span></div> </div>
<div class="fusion-alignright">
<nav aria-label="Secondary Menu" class="fusion-secondary-menu" role="navigation"></nav> </div>
</div>
</div>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
<div class="fusion-row">
<div class="fusion-logo" data-margin-bottom="0px" data-margin-left="0px" data-margin-right="0px" data-margin-top="0px">
<a class="fusion-logo-link" href="https://www.viewpointlodge.co.ke/">
<!-- standard logo -->
<img alt="View Point Lodge Logo" class="fusion-standard-logo" data-retina_logo_url="" height="110" src="https://www.viewpointlodge.co.ke/wp-content/uploads/2020/10/View-Point-Lodge_WEB-Logo-1.png" srcset="https://www.viewpointlodge.co.ke/wp-content/uploads/2020/10/View-Point-Lodge_WEB-Logo-1.png 1x" width="107"/>
<!-- mobile logo -->
<img alt="View Point Lodge Logo" class="fusion-mobile-logo" data-retina_logo_url="" height="90" src="https://www.viewpointlodge.co.ke/wp-content/uploads/2020/10/View-Point-Lodge_WEB-Logo-mobile.jpg" srcset="https://www.viewpointlodge.co.ke/wp-content/uploads/2020/10/View-Point-Lodge_WEB-Logo-mobile.jpg 1x" width="88"/>
</a>
</div> <nav aria-label="Main Menu" class="fusion-main-menu"><ul class="fusion-menu" id="menu-travel-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-22" data-item-id="22" id="menu-item-22"><a class="fusion-arrow-highlight" href="https://www.viewpointlodge.co.ke/"><span class="menu-text">HOME<span class="fusion-arrow-svg"><svg height="12px" width="23px">
<path class="header_border_color_stroke" d="M0 0 L11.5 12 L23 0 Z" fill="#ffffff" stroke-width="1"></path>
</svg></span></span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21" data-item-id="21" id="menu-item-21"><a class="fusion-arrow-highlight" href="https://www.viewpointlodge.co.ke/about-us/"><span class="menu-text">ABOUT US<span class="fusion-arrow-svg"><svg height="12px" width="23px">
<path class="header_border_color_stroke" d="M0 0 L11.5 12 L23 0 Z" fill="#ffffff" stroke-width="1"></path>
</svg></span></span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-946" data-item-id="946" id="menu-item-946"><a class="fusion-arrow-highlight" href="https://www.viewpointlodge.co.ke/products-services/"><span class="menu-text">SERVICES<span class="fusion-arrow-svg"><svg height="12px" width="23px">
<path class="header_border_color_stroke" d="M0 0 L11.5 12 L23 0 Z" fill="#ffffff" stroke-width="1"></path>
</svg></span></span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-890" data-item-id="890" id="menu-item-890"><a class="fusion-arrow-highlight" href="https://www.viewpointlodge.co.ke/gallery/"><span class="menu-text">GALLERY<span class="fusion-arrow-svg"><svg height="12px" width="23px">
<path class="header_border_color_stroke" d="M0 0 L11.5 12 L23 0 Z" fill="#ffffff" stroke-width="1"></path>
</svg></span></span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912" data-item-id="912" id="menu-item-912"><a class="fusion-arrow-highlight" href="https://www.viewpointlodge.co.ke/contact-viewpoint/"><span class="menu-text">CONTACT US<span class="fusion-arrow-svg"><svg height="12px" width="23px">
<path class="header_border_color_stroke" d="M0 0 L11.5 12 L23 0 Z" fill="#ffffff" stroke-width="1"></path>
</svg></span></span></a></li></ul></nav> <div class="fusion-mobile-menu-icons">
<a aria-expanded="false" aria-label="Toggle mobile menu" class="fusion-icon fusion-icon-bars" href="#"></a>
</div>
<nav aria-label="Main Menu Mobile" class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
</div>
</div>
</div>
<div class="fusion-clearfix"></div>
</header>
<div id="sliders-container">
</div>
<div class="avada-page-titlebar-wrapper">
<div class="fusion-page-title-bar fusion-page-title-bar-none fusion-page-title-bar-center">
<div class="fusion-page-title-row">
<div class="fusion-page-title-wrapper">
<div class="fusion-page-title-captions">
<h1 class="entry-title">Error 404 Page</h1>
<div class="fusion-page-title-secondary">
<div class="fusion-breadcrumbs"><span class="fusion-breadcrumb-item"><a class="fusion-breadcrumb-link" href="https://www.viewpointlodge.co.ke"><span>Home</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="fusion-breadcrumb-item"><span>404 - Page not Found</span></span></div> </div>
</div>
</div>
</div>
</div>
</div>
<main class="clearfix " id="main">
<div class="fusion-row" style="">
<section class="full-width" id="content">
<div id="post-404page">
<div class="post-content">
<div class="fusion-title fusion-title-size-two sep-" style="margin-top:0px;margin-bottom:0px;">
<h2 class="title-heading-left" style="margin:0;">
						Oops, This Page Could Not Be Found!					</h2>
<div class="title-sep-container">
<div class="title-sep sep-"></div>
</div>
</div>
<div class="fusion-clearfix"></div>
<div class="error-page">
<div class="fusion-columns fusion-columns-3">
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-404">
<div class="error-message">404</div>
</div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 useful-links fusion-error-page-useful-links">
<h3>Helpful Links</h3>
<ul class="fusion-checklist fusion-404-checklist error-menu" id="fusion-checklist-1" style="font-size:14px;line-height:23.8px;"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-22"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.viewpointlodge.co.ke/">HOME</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.viewpointlodge.co.ke/about-us/">ABOUT US</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-946"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.viewpointlodge.co.ke/products-services/">SERVICES</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-890"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.viewpointlodge.co.ke/gallery/">GALLERY</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-912"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.viewpointlodge.co.ke/contact-viewpoint/">CONTACT US</a></div></li></ul> </div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-search">
<h3>Search Our Website</h3>
<p>Can't find what you need? Take a moment and do a search below!</p>
<div class="search-page-search-form">
<form action="https://www.viewpointlodge.co.ke/" class="searchform fusion-search-form fusion-search-form-classic" method="get" role="search">
<div class="fusion-search-form-content">
<div class="fusion-search-field search-field">
<label><span class="screen-reader-text">Search for:</span>
<input aria-label="" aria-required="true" class="s" name="s" placeholder="Search..." required="" type="search" value=""/>
</label>
</div>
<div class="fusion-search-button search-button">
<input class="fusion-search-submit searchsubmit" type="submit" value=""/>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div> <!-- fusion-row -->
</main> <!-- #main -->
<div class="fusion-footer">
<footer class="fusion-footer-copyright-area" id="footer">
<div class="fusion-row">
<div class="fusion-copyright-content">
<div class="fusion-copyright-notice">
<div>
		© Copyright - <script>document.write(new Date().getFullYear());</script>   |   All Rights Reserved   |   Powered by <a href="http://bighostweb.com" target="_blank">BigHost Africa</a> </div>
</div>
<div class="fusion-social-links-footer">
<div class="fusion-social-networks boxed-icons"><div class="fusion-social-networks-wrapper"><a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" data-placement="top" data-title="facebook" data-toggle="tooltip" href="https://www.facebook.com/Viewpoint-lodge-shores-of-lake-Elmentaita-154072117987720/" rel="noopener noreferrer" style="" target="_blank" title="facebook"><span class="screen-reader-text">facebook</span></a><a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" data-placement="top" data-title="twitter" data-toggle="tooltip" href="https://twitter.com/viewpointlodge?s=11" rel="noopener noreferrer" style="" target="_blank" title="twitter"><span class="screen-reader-text">twitter</span></a><a class="fusion-social-network-icon fusion-tooltip fusion-instagram fusion-icon-instagram" data-placement="top" data-title="instagram" data-toggle="tooltip" href="https://www.instagram.com/p/CGrgTXWpxNz/?igshid=1w3y9wgi1p3fh" rel="noopener noreferrer" style="" target="_blank" title="instagram"><span class="screen-reader-text">instagram</span></a></div></div></div>
</div> <!-- fusion-fusion-copyright-content -->
</div> <!-- fusion-row -->
</footer> <!-- #footer -->
</div> <!-- fusion-footer -->
<div class="fusion-sliding-bar-wrapper">
</div>
</div> <!-- wrapper -->
</div> <!-- #boxed-wrapper -->
<div class="fusion-top-frame"></div>
<div class="fusion-bottom-frame"></div>
<div class="fusion-boxed-shadow"></div>
<a class="fusion-one-page-text-link fusion-page-load-link"></a>
<div class="avada-footer-scripts">
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.viewpointlodge.co.ke\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.viewpointlodge.co.ke/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="google-recaptcha-js" src="https://www.google.com/recaptcha/api.js?render=6LdJpeAZAAAAAIZVgjIHoLjLHIQ8Sx_uz8tj-3q0&amp;ver=3.0" type="text/javascript"></script>
<script id="wpcf7-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6LdJpeAZAAAAAIZVgjIHoLjLHIQ8Sx_uz8tj-3q0","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script id="wpcf7-recaptcha-js" src="https://www.viewpointlodge.co.ke/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.3.2" type="text/javascript"></script>
<script id="fusion-scripts-js" src="https://www.viewpointlodge.co.ke/wp-content/uploads/fusion-scripts/b75872d3bdecc97be300d36ae6821eb4.min.js?ver=2.2.3" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.viewpointlodge.co.ke/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script type="text/javascript">
				jQuery( document ).ready( function() {
					var ajaxurl = 'https://www.viewpointlodge.co.ke/wp-admin/admin-ajax.php';
					if ( 0 < jQuery( '.fusion-login-nonce' ).length ) {
						jQuery.get( ajaxurl, { 'action': 'fusion_login_nonce' }, function( response ) {
							jQuery( '.fusion-login-nonce' ).html( response );
						});
					}
				});
				</script>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"name":"Home","item":"https:\/\/www.viewpointlodge.co.ke"}]}</script> </div>
</body>
</html>
