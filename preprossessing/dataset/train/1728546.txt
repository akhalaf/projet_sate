<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!--meta name="viewport"    content="width=device-width" /-->
<meta content="Часы ZIIIRO в России" name="description"/>
<meta content="ZIIIRO" name="keywords"/>
<meta content="index, follow, all" name="robots"/>
<meta content="width=1100,maximum-scale=1.0" name="viewport"/>
<title>Часы ZIIIRO в России</title>
<link href="/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/css/ui-lightness/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=LWq9*dsc1PSLU7J4VmsX6Wa62gDIHI1nj6hK944JpfmH6qmI/JpqF1nxvAj*Agvjkd7snxg2Z6VO3FC7TbJYVa/kZsyBfOSA0ru79/YifFSf7x5Ze8uk4FxvFLcRm*Z3rj/CG*2Gb88CmnOnOLUtJJWFADPE/GdbCre3uMGw3fA-';</script>
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?162",t.onload=function(){VK.Retargeting.Init("VK-RTRG-392783-6KyKp"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img alt="" src="https://vk.com/rtrg?p=VK-RTRG-392783-6KyKp" style="position:fixed; left:-999px;"/></noscript>
<script src="/js/web-application.js" type="text/javascript"></script>
<script src="/js/_jquery.min.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
	$(function() {
		$('select[name=currency_id]').change(function() {
			$(this).closest('form').submit();
		});
	});
	</script>
</head>
<body>
<!--LiveInternet counter--><script type="text/javascript"><!--
new Image().src = "http://counter.yadro.ru/hit?r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random();//--></script><!--/LiveInternet-->
<div id="container">
<div id="headerPan">
<div id="search">
<div id="search_box">
<form action="/products" method="get">
<input class="search_btn" name="Search" type="button"/>
<input class="textfield" name="keyword" type="text" value=""/>
</form>
</div>
</div>
<div id="links">
<a href="/user/register/" id="register">Регистрация</a>
					|
					<a href="/cart/">Корзина (0)</a>
					|
					<a href="/user/login/" id="login">Вход</a>
</div>
</div>
<div id="contentPan">
<div id="sidebar">
<div class="box">
<div class="logo"><a href="/"><img alt="logo" border="0" height="190" src="/images/logo.png" width="214"/></a></div>
<ul><a href="http://ziiiro.ru/Smarty/files/index.html"></a>
<li><a href="#" onclick="ShopListing(); RightSideCart(); return false;" title="SHOP">КАТАЛОГ</a></li>
<li class="subCat">
<a category_id="6" class="subCat" href="/catalog/watches">ЧАСЫ</a>
</li>
<li class="subCat">
<a category_id="7" class="subCat" href="/catalog/braslety">БРАСЛЕТЫ</a>
</li>
<li class="subCat">
<a class="subCat" href="/konstruktor">КОНСТРУКТОР</a>
</li>
<li page_id="3">
<a href="/dostavka">ДОСТАВКА</a>
</li>
<li page_id="2">
<a href="/oplata">ОПЛАТА</a>
</li>
<li page_id="7">
<a href="/faq">FAQ</a>
</li>
<li class="last_menuitem" page_id="6">
<a class="menubottom" href="/contact">КОНТАКТЫ</a>
</li>
</ul>
</div>
</div>

﻿
    <div id="contentArea">
<div id="banner">
<div class="featured_banner">
<ul class="buttons">
<li class="active" id="button1"><a href="#" title="ZIIIRO Eclipse">1</a></li>
<li id="button2"><a href="#" title="ZIIIRO">2</a></li>
<li id="button3"><a href="#" title="ZIIIRO">3</a></li>
<li id="button4"><a href="#" title="ZIIIRO">4</a></li>
<li id="button5"><a href="#" title="ZIIIRO">5</a></li>
<li id="button6"><a href="#" title="ZIIIRO">6</a></li>
</ul>
<ul class="slides">
<li class="slide" id="image1" style="visibility:visible"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 1" height="376" src="/images/home_banner6.jpg" width="747"/></a></li>
<li class="slide" id="image2"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 2" height="376" src="/images/home_banner2.jpg" width="747"/></a></li>
<li class="slide" id="image3"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 3" height="376" src="/images/home_banner1.jpg" width="747"/></a></li>
<li class="slide" id="image4"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 4" height="376" src="/images/home_banner4.jpg" width="747"/></a></li>
<li class="slide" id="image5"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 5" height="376" src="/images/home_banner5.jpg" width="747"/></a></li>
<li class="slide" id="image6"> <a href="/catalog/watches"><img alt="ZIIIRO Banner 6" height="376" src="/images/home_banner3.jpg" width="747"/></a></li></ul>
</div>
</div>
<div id="news">
<h2><span class="text">Новости</span></h2>
<div id="news_section">
<div id="news_item"><span class="highlight">Новая модель Jupiter уже в России!</span><br/>
<p> Первая модель со швейцарским механизмом и магнитами вместо стрелок! </p>
<a href="blog/novaya_model_jupiter_uzhe_v_rossii">Подробнее</a></div>
<div id="news_item"><span class="highlight">Новая поставка</span><br/>
<p> Первое пополнение запасов ZIIIRO в 2020 году! </p>
<a href="blog/novaya_postavka">Подробнее</a></div>
<div id="news_item"><span class="highlight">ZIIIRO Horizon</span><br/>
<p> Новинка 2019 года — часы ZIIIRO Horizon! </p>
<a href="blog/ziiiro_horizon">Подробнее</a></div>
<div id="news_item"><span class="highlight">ZIIIRO Solaris</span><br/>
<p> Новинка! ZIIIRO Solaris - часы с переливающимся цветом циферблата. </p>
<a href="blog/ziiiro_solaris">Подробнее</a></div>
</div>
</div>
</div>
<script type="text/javascript">
jQuery(function($) {
	var timer;
	function button1_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image1").css("visibility","visible");
	$("#image1").css("opacity","0");
	$("#image1").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image1").animate({"opacity":1},500, "linear", null);
	$("#button1").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button2_click"),"6000");
	$("#image1").animate({"opacity":1},500, "linear", null);
	}

	function button2_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image2").css("visibility","visible");
	$("#image2").css("opacity","0");
	$("#image2").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image2").animate({"opacity":1},500, "linear", null);
	$("#button2").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button3_click"),"6000");
	$("#image2").animate({"opacity":1},500, "linear", null);
	}

	function button3_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image3").css("visibility","visible");
	$("#image3").css("opacity","0");
	$("#image3").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image3").animate({"opacity":1},500, "linear", null);
	$("#button3").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button4_click"),"6000");
	$("#image3").animate({"opacity":1},500, "linear", null);
	}

	function button4_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image4").css("visibility","visible");
	$("#image4").css("opacity","0");
	$("#image4").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image4").animate({"opacity":1},500, "linear", null);
	$("#button4").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button5_click"),"6000");
	$("#image4").animate({"opacity":1},500, "linear", null);
	}

	function button5_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image5").css("visibility","visible");
	$("#image5").css("opacity","0");
	$("#image5").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image5").animate({"opacity":1},500, "linear", null);
	$("#button5").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button6_click"),"6000");
	$("#image5").animate({"opacity":1},500, "linear", null);
	}

	function button6_click(event)
	{
	$(".slide").css("visibility","hidden");
	$("#image6").css("visibility","visible");
	$("#image6").css("opacity","0");
	$("#image6").animate({"opacity":1},500, "linear", null);
	$("ul.buttons li").removeClass("active");
	$("#image6").animate({"opacity":1},500, "linear", null);
	$("#button6").addClass("active");
	clearTimeout(timer);
	timer = setTimeout(eval("button1_click"),"6000");
	$("#image6").animate({"opacity":1},500, "linear", null);
	}

	function OnLoad(event)
	{
	clearTimeout(timer);
	timer = setTimeout(eval("button2_click"),"6000");
	}

	$('#button1').bind('click', button1_click);

	$('#button2').bind('click', button2_click);

	$('#button3').bind('click', button3_click);

	$('#button4').bind('click', button4_click);

	$('#button5').bind('click', button5_click);

	$('#button6').bind('click', button6_click);

	OnLoad();
	ALERT('TEDT');

});
</script>
</div>
<div id="footermainPan" style="background-image: none;"><div class="ddd78545" id="ddd78545" style="background:url(/images/facebook.gif);"></div>
<div id="footerPan">
<div id="social">
<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript">
try { var yaCounter13858639 = new Ya.Metrika({id:13858639, enableAll: true, webvisor:true});}
catch(e) { }
</script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/13858639" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<a href="https://vk.com/ziiiro" target="_blank"><img alt="twitter" height="16" src="/images/vkontakte.png" width="16"/></a>
</div>
<div id="theLinks"><a href="/contact">КОНТАКТЫ</a>| <a href="/dostavka">ДОСТАВКА</a>| <a href="/oplata">ОПЛАТА</a>| <a href="/faq">FAQ</a></div>
</div>
</div>
</div>
</body>
</html>
<!--
memory peak usage: 2710976 bytes
page generation time: 0.018908977508545 seconds
-->