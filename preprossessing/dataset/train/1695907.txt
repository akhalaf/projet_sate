<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"/>
<meta content="IE=10,IE=9,IE=8" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" name="viewport"/>
<title>蔓草札记 - 感受生活，感悟工作，感触心灵，许合欢的博客。</title>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://feed.xuhehuan.com/" rel="alternate" title="蔓草札记 RSS Feed" type="application/rss+xml"/>
<script>
window._deel = {
    name: '蔓草札记',
    url: 'https://xuhehuan.com/wp-content/themes/D8_4.0',
    rss: 'http://feed.xuhehuan.com/',
    ajaxpager: '',
    maillist: 'on',
    maillistCode: '7e8ec1a990a48184fa73ff7d9b15e9eafa9f94848d1791d9',
    commenton: 0,
    roll: [0,0],
    tougaoContentmin: 200,
    tougaoContentmax: 5000,
    appkey: {
    	tqq: '801494063',
    	tsina: '1106777879',
    	t163: '',
    	tsohu: ''
    }
}
</script>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://xuhehuan.com/wp-content/themes/D8_4.0/style.css?ver=3.0" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xuhehuan.com/wp-includes/css/dist/block-library/style.min.css?ver=66023064892ee9d19e5dc8a8888440d7" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xuhehuan.com/wp-content/plugins/swiftype-search/Search/../assets/facets.css?ver=66023064892ee9d19e5dc8a8888440d7" id="swiftype-facets-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xuhehuan.com/wp-content/plugins/swiftype-search/Search/../assets/autocomplete.css?ver=66023064892ee9d19e5dc8a8888440d7" id="swiftype-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://xuhehuan.com/wp-content/plugins/enlighter/cache/enlighterjs.min.css?ver=/vz/IKU1N/t3jz2" id="enlighterjs-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://xuhehuan.com/wp-content/themes/D8_4.0/js/jquery.js?ver=3.0" type="text/javascript"></script>
<script id="swiftype-js-extra" type="text/javascript">
/* <![CDATA[ */
var swiftypeParams = {"engineKey":"G2zxcudsx9Qp9sGqKyMJ"};
/* ]]> */
</script>
<script id="swiftype-js" src="https://xuhehuan.com/wp-content/plugins/swiftype-search/Search/../assets/install_swiftype.min.js?ver=66023064892ee9d19e5dc8a8888440d7" type="text/javascript"></script>
<link href="https://xuhehuan.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://xuhehuan.com/wp-content/plugins/auto-highslide/highslide/highslide.css" rel="stylesheet" type="text/css"/>
<script src="https://xuhehuan.com/wp-content/plugins/auto-highslide/highslide/highslide-with-html.packed.js" type="text/javascript"></script>
<script type="text/javascript">
	hs.graphicsDir = "https://xuhehuan.com/wp-content/plugins/auto-highslide/highslide/graphics/";
	hs.outlineType = "rounded-white";
	hs.outlineWhileAnimating = true;
	hs.showCredits = false;
</script>
<meta content="蔓草札记，蔓草博客，许合欢博客，蔓草blog" name="keywords"/>
<meta content="这是一个个人博客，用来记录生活中遇到的事情，工作中碰到的问题和其它一些个人关注的东西。" name="description"/>
<link href="https://xuhehuan.com/wp-content/uploads/2016/04/cropped-clover-512-32x32.jpg" rel="icon" sizes="32x32"/>
<link href="https://xuhehuan.com/wp-content/uploads/2016/04/cropped-clover-512-192x192.jpg" rel="icon" sizes="192x192"/>
<link href="https://xuhehuan.com/wp-content/uploads/2016/04/cropped-clover-512-180x180.jpg" rel="apple-touch-icon"/>
<meta content="https://xuhehuan.com/wp-content/uploads/2016/04/cropped-clover-512-270x270.jpg" name="msapplication-TileImage"/>
<style>.article-content{font-size: 16px;line-height:27px;}.article-content p{margin:20px 0;}@media (max-width:640px){.article-content{font-size:16px}}</style><!--[if lt IE 9]><script src="https://xuhehuan.com/wp-content/themes/D8_4.0/js/html5.js"></script><![endif]-->
<script src="https://xuhehuan.com/wp-content/themes/D8_4.0/js/zanshang.js" type="text/javascript"></script>
</head>
<body class="home blog" data-rsssl="1">
<div class="navbar-wrap">
<div class="navbar">
<h1 class="logo"><a href="https://xuhehuan.com" title="蔓草札记-感受生活，感悟工作，感触心灵，许合欢的博客。">蔓草札记</a></h1>
<ul class="nav">
<li class="page_item page-item-962"><a href="https://xuhehuan.com/archives">归档</a></li>
<li class="page_item page-item-2327"><a href="https://xuhehuan.com/donate">打赏</a></li>
<li class="page_item page-item-9"><a href="https://xuhehuan.com/about-page">关于</a></li>
</ul>
<div class="menu pull-right">
<form action="https://xuhehuan.com/" class="dropdown search-form" method="get">
<input class="search-input" name="s" placeholder="输入关键字搜索" type="text" x-webkit-speech=""/><input class="btn btn-success search-submit" type="submit" value="搜索"/>
<ul class="dropdown-menu search-suggest"></ul>
</form>
<div class="btn-group pull-left">
<button class="btn btn-primary" data-target="#feed" data-toggle="modal">订阅</button>
<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">关注 <i class="caret"></i></button>
<ul class="dropdown-menu pull-right">
<li><a href="http://weibo.com/imancao" target="_blank">新浪微博</a></li> <li><a href="https://twitter.com/xhhjin" target="_blank">Twitter</a></li> </ul>
</div>
</div>
</div>
</div>
<header class="header">
<div class="speedbar">
<div class="pull-right">
</div>
<div class="toptip"><strong class="text-success">最新消息：</strong>蔓草札记的微信公众号开通了，赶紧在微信通讯录公众号中搜索“蔓草札记”关注下吧 :)</div>
</div>
</header>
<section class="container">
<div class="content-wrap">
<div class="content">
<h2 class="entry-title">最新发布</h2><article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/%e6%9c%ba%e5%99%a8%e5%ad%a6%e4%b9%a0">机器学习<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2807.html" title="记 Python whl is not a supported wheel on this platform 的解决方案 - 蔓草札记">记 Python whl is not a supported wheel on this platform 的解决方案</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 2年前 (2019-03-15)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1537浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2807.html#respond">0评论</a></span></p> <p class="note">
		
按照官方教程安装 PyTorch 时出现了类似  *****.whl is not a supported wheel on this platform 的错误，最早怀疑是 Python 版本的问题，查了些资料但都没找到点子上，偶然在查找过程中...		<a class="readmore" href="https://xuhehuan.com/2807.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2807.html"><img alt="记 Python whl is not a supported wheel on this platform 的解决方案" src="https://xuhehuan.com/wp-content/uploads/2019/03/pip-python-get-supported.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/bokejingyan">博客经验<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2789.html" title="WordPress 代码高亮插件 Enlighter - 蔓草札记">WordPress 代码高亮插件 Enlighter</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 2年前 (2019-03-04)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1210浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2789.html#respond">0评论</a></span></p> <p class="note">
		
2018 年 12 月，WordPress 5.0 正式版发布，主要有两个更新：内置默认编辑器由 TinyMCE 更换为更换为 Gutenberg（古腾堡）；新增官方主题 Twenty Nineteen。因为之前在老版本时通过插件的方法体验过古...		<a class="readmore" href="https://xuhehuan.com/2789.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2789.html"><img alt="Wordpress 代码高亮插件 Enlighter" src="https://xuhehuan.com/wp-content/uploads/2019/03/wordpress-enlighter-code-format.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/%e6%9c%ba%e5%99%a8%e5%ad%a6%e4%b9%a0">机器学习<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2769.html" title="PyTorch 中 Tensor 和 PILImage 的相互转换 - 蔓草札记">PyTorch 中 Tensor 和 PILImage 的相互转换</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 2年前 (2019-02-27)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1596浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2769.html#comments">1评论</a></span></p> <p class="note">
		
在 PyTorch 实现图像的 Normalize 和 反 Normalize 的实验中，发现经过这两个转换后存储的图像和原始图像虽然视觉上没什么差异，但在二进制上却不能完全匹配，这里记录下问题的原因分析及最终的解决过程。







下面是...		<a class="readmore" href="https://xuhehuan.com/2769.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2769.html"><img alt="PyTorch 中 Tensor 和 PILImage 的相互转换" src="https://xuhehuan.com/wp-content/uploads/2019/02/ancient-meditation-architecture.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/wangluojiqiao">网络技巧<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2756.html" title="SourceCounter 注册序列号生成 - 蔓草札记">SourceCounter 注册序列号生成</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 2年前 (2018-08-07)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1582浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2756.html#respond">0评论</a></span></p> <p class="note">
		SourceCounter 是一款十分好用的源代码统计工具（官方下载地址），支持 30 多种代码格式，能够统计包括：代码行数、注释、空行、文件大小等数据；另外，它还支持对软件开发项目的各个开发阶段的工数、成本、质量指标等进行分析和预测。如果只是简...		<a class="readmore" href="https://xuhehuan.com/2756.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2756.html"><img alt="SourceCounter 注册序列号生成" src="https://xuhehuan.com/wp-content/uploads/2018/08/SourceCounter-register-serial-number.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/bokejingyan">博客经验<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2747.html" title="写在 WordPress 博客被黑之后 - 蔓草札记">写在 WordPress 博客被黑之后</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 3年前 (2018-07-23)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1999浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2747.html#comments">4评论</a></span></p> <p class="note">
		前段时间，博客接连被黑了几次，这对于我来说还是头一次遭遇。第一次被黑的时候，后台登陆不进去，查看数据库发现账号和密码被改了，以为是密码泄漏了，于是重置主机内容，更换账号密码，用备份数据重新上线；隔了一周，发现又登陆不上了，首页还被篡改了一部分内容...		<a class="readmore" href="https://xuhehuan.com/2747.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2747.html"><img alt="写在 Wordpress 博客被黑之后" src="https://xuhehuan.com/wp-content/uploads/2018/07/blog-is-hacked.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/wangluojiqiao">网络技巧<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2738.html" title="简述 LLVM 与 Clang 及其关系 - 蔓草札记">简述 LLVM 与 Clang 及其关系</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 3年前 (2018-07-19)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1702浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2738.html#respond">0评论</a></span></p> <p class="note">
		随着 Android P 的逐步应用，越来越多的客户要求编译库时用 libc++ 来代替 libstdc++。libc++ 和 libstdc++ 这两个库有关系呢？它们两个都是 C++ 标准库，libc++ 是针对 Clang 编译器特别重写的...		<a class="readmore" href="https://xuhehuan.com/2738.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2738.html"><img alt="简述 LLVM 与 Clang 及其关系" src="https://xuhehuan.com/wp-content/uploads/2018/07/Clang-LLVM-cover.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/%e6%9c%ba%e5%99%a8%e5%ad%a6%e4%b9%a0">机器学习<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2639.html" title="Deepin 15.4.1 安装 CPU 版 Caffe - 蔓草札记">Deepin 15.4.1 安装 CPU 版 Caffe</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 3年前 (2017-11-11)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 1973浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2639.html#respond">0评论</a></span></p> <p class="note">
		最近准备从 Caffe 入手，学习下深度学习的基本知识，于是便在笔记本电脑上的 Deepin 系统中进行了一些实践，其中参考了网上不少资料，特此记录下来。

*  系统配置：
显卡：Intel 集成显卡
操作系统：Deepin 15.4.1 x6...		<a class="readmore" href="https://xuhehuan.com/2639.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2639.html"><img alt="Deepin 15.4.1 安装 CPU 版 Caffe" src="https://xuhehuan.com/wp-content/uploads/2017/11/deepin-caffe.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/tuxiangchuli">图像处理<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2608.html" title="过三点的二次贝塞尔曲线及其升阶 - 蔓草札记">过三点的二次贝塞尔曲线及其升阶</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 4年前 (2017-03-17)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 3346浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2608.html#comments">3评论</a></span></p> <p class="note">
		贝塞尔曲线（Bézier curve）是计算机图形学中相当重要的参数曲线，Photoshop 中的钢笔效果，Flash5 的贝塞尔曲线工具都是它在计算机图形学中的具体应用。 贝塞尔曲线由法国雷诺汽车公司的工程师皮埃尔·贝塞尔（Pierre Béz...		<a class="readmore" href="https://xuhehuan.com/2608.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2608.html"><img alt="过三点的二次贝塞尔曲线及其升阶" src="https://xuhehuan.com/wp-content/uploads/2017/03/quadratic-bezier-sketch.gif"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/wangluojiqiao">网络技巧<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2602.html" title="保留申请的 Google Voice 号码 - 蔓草札记">保留申请的 Google Voice 号码</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 4年前 (2017-03-10)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 4495浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2602.html#comments">3评论</a></span></p> <p class="note">
		Google Voice 是由 Google 推出的 VOIP 服务，其初衷是将个人所用的众多电话号码集中成为一个号码，并且提供更多增值服务。通过申请 Google Voice 可以得到一个美国电话号码，然后就能免费给在美国及加拿大地区的亲朋好友...		<a class="readmore" href="https://xuhehuan.com/2602.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2602.html"><img alt="保留申请的 Google Voice 号码" src="https://xuhehuan.com/wp-content/uploads/2017/03/keep-google-voice.jpg"/></a></div>
</article>
<article class="excerpt">
<header>
<a class="label label-important" href="https://xuhehuan.com/category/wangluojiqiao">网络技巧<i class="label-arrow"></i></a> <h2><a href="https://xuhehuan.com/2573.html" title="Shadowsocks 配置指定软件代理 - 蔓草札记">Shadowsocks 配置指定软件代理</a></h2>
</header>
<p>
<span class="muted"><i class="icon-user icon12"></i> <a href="https://xuhehuan.com/author/xhhjin">xhhjin</a></span>
<span class="muted"><i class="icon-time icon12"></i> 4年前 (2017-01-09)</span> <span class="muted"><i class="icon-eye-open icon12"></i> 6193浏览</span> <span class="muted"><i class="icon-comment icon12"></i> <a href="https://xuhehuan.com/2573.html#respond">0评论</a></span></p> <p class="note">
		由于 Shadowsocks 使用的是 Sockets5 代理，通常都是只保证浏览器的代理正常运行，不能像 VPN 那样进行全局代理。但在日常使用中，我们常常希望将代理应用到一些特定的软件上，比如某些游戏软件，这时就额外需要一款连接网络的代理服务...		<a class="readmore" href="https://xuhehuan.com/2573.html" rel="nofollow" title="阅读全文">阅读全文&gt;&gt;</a>
</p>
<div class="focus"><a class="thumbnail" href="https://xuhehuan.com/2573.html"><img alt="Shadowsocks 配置指定软件代理" src="https://xuhehuan.com/wp-content/uploads/2017/01/sockscap-proxifier.jpg"/></a></div>
</article>
<div class="pagination"><ul><li class="prev-page"></li><li class="active"><span>1</span></li><li><a href="https://xuhehuan.com/page/2">2</a></li><li><a href="https://xuhehuan.com/page/3">3</a></li><li><a href="https://xuhehuan.com/page/4">4</a></li><li><a href="https://xuhehuan.com/page/5">5</a></li><li><span> ... </span></li><li class="next-page"><a href="https://xuhehuan.com/page/2">下一页</a></li></ul></div> </div>
</div>
<aside class="sidebar">
<div class="widget widget_categories"><h3 class="widget_tit">分类目录</h3>
<ul>
<li class="cat-item cat-item-3"><a href="https://xuhehuan.com/category/yejiedongtai">业界动态</a>
</li>
<li class="cat-item cat-item-4"><a href="https://xuhehuan.com/category/bokejingyan">博客经验</a>
</li>
<li class="cat-item cat-item-5"><a href="https://xuhehuan.com/category/tuxiangchuli">图像处理</a>
</li>
<li class="cat-item cat-item-6"><a href="https://xuhehuan.com/category/xinqingsuibi">心情随笔</a>
</li>
<li class="cat-item cat-item-157"><a href="https://xuhehuan.com/category/%e6%9c%ba%e5%99%a8%e5%ad%a6%e4%b9%a0">机器学习</a>
</li>
<li class="cat-item cat-item-7"><a href="https://xuhehuan.com/category/wangluojiqiao">网络技巧</a>
</li>
<li class="cat-item cat-item-8"><a href="https://xuhehuan.com/category/meiwenzhaixuan">美文摘选</a>
</li>
</ul>
</div><div class="widget widget_calendar"><h3 class="widget_tit">日历</h3><div class="calendar_wrap" id="calendar_wrap"><table class="wp-calendar-table" id="wp-calendar">
<caption>2021年1月</caption>
<thead>
<tr>
<th scope="col" title="星期一">一</th>
<th scope="col" title="星期二">二</th>
<th scope="col" title="星期三">三</th>
<th scope="col" title="星期四">四</th>
<th scope="col" title="星期五">五</th>
<th scope="col" title="星期六">六</th>
<th scope="col" title="星期日">日</th>
</tr>
</thead>
<tbody>
<tr>
<td class="pad" colspan="4"> </td><td>1</td><td>2</td><td>3</td>
</tr>
<tr>
<td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
</tr>
<tr>
<td>11</td><td>12</td><td id="today">13</td><td>14</td><td>15</td><td>16</td><td>17</td>
</tr>
<tr>
<td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td>
</tr>
<tr>
<td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
</tr>
</tbody>
</table><nav aria-label="上个月及下个月" class="wp-calendar-nav">
<span class="wp-calendar-nav-prev"><a href="https://xuhehuan.com/date/2019/03">« 3月</a></span>
<span class="pad"> </span>
<span class="wp-calendar-nav-next"> </span>
</nav></div></div><div class="widget widget_text"><h3 class="widget_tit">RevolverMaps</h3> <div class="textwidget"><div style="margin-top:5px;margin-left:5px;">
<script async="async" src="//rf.revolvermaps.com/0/0/1.js?i=7arpn0idevs&amp;s=350&amp;m=0&amp;v=true&amp;r=true&amp;b=000000&amp;n=false&amp;c=ff0000" type="text/javascript"></script>
</div></div>
</div><div class="widget widget_links"><h3 class="widget_tit">友情链接</h3>
<ul class="xoxo blogroll">
<li><a href="http://ju.xuhehuan.com" rel="friend" target="_blank"><img alt="桔子92" src="https://xuhehuan.com/wp-content/uploads/2012/12/juzi92.jpg"/> 桔子92</a></li>
<li><a href="http://zhiquan.me/" rel="friend" target="_blank"><img alt="知泉博客" src="https://xuhehuan.com/wp-content/uploads/2012/12/zhiquanblog.jpg"/> 知泉博客</a></li>
</ul>
</div>
<div class="widget d_postlist"><h3 class="widget_tit">热评文章</h3><ul class="nopic"><li><a href="https://xuhehuan.com/2272.html"><span class="text">搜索拐杖——增强搜索便捷性的浏览器扩展</span><span class="muted">2015-11-23</span><span class="muted">69评论</span></a></li>
<li><a href="https://xuhehuan.com/1368.html"><span class="text">Rss订阅推送到Kindle的几种方法</span><span class="muted">2013-08-05</span><span class="muted">45评论</span></a></li>
<li><a href="https://xuhehuan.com/2027.html"><span class="text">WordPress 博客同步到 CSDN 插件</span><span class="muted">2015-05-10</span><span class="muted">36评论</span></a></li>
<li><a href="https://xuhehuan.com/2037.html"><span class="text">EMlog 5.3.1 For 新浪 SAE</span><span class="muted">2015-05-16</span><span class="muted">30评论</span></a></li>
<li><a href="https://xuhehuan.com/783.html"><span class="text">在Github上搭建Octopress博客</span><span class="muted">2012-08-05</span><span class="muted">25评论</span></a></li>
</ul></div></aside></section>
<footer class="footer">
<div class="footer-inner">
<div class="copyright pull-left">
            版权所有，保留一切权利！ © 2021 <a href="https://xuhehuan.com">蔓草札记</a>　Theme <a href="http://www.daqianduan.com/" target="_blank">D8</a>
</div>
<div class="trackcode pull-right">
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?f614d67e8c2845ed17360303531484ec";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-30144395-1', 'auto');
  ga('send', 'pageview');
</script> </div>
</div>
</footer>
<script id="enlighterjs-js" src="https://xuhehuan.com/wp-content/plugins/enlighter/cache/enlighterjs.min.js?ver=/vz/IKU1N/t3jz2" type="text/javascript"></script>
<script id="enlighterjs-js-after" type="text/javascript">
!function(e,n){if("undefined"!=typeof EnlighterJS){var o={"selectors":{"block":"pre.EnlighterJSRAW","inline":"code.EnlighterJSRAW"},"options":{"indent":4,"ampersandCleanup":true,"linehover":true,"rawcodeDbclick":false,"textOverflow":"break","linenumbers":true,"theme":"droide","language":"generic","retainCssClasses":false,"collapse":false,"toolbarOuter":"","toolbarTop":"{BTN_RAW}{BTN_COPY}{BTN_WINDOW}{BTN_WEBSITE}","toolbarBottom":""}};(e.EnlighterJSINIT=function(){EnlighterJS.init(o.selectors.block,o.selectors.inline,o.options)})()}else{(n&&(n.error||n.log)||function(){})("Error: EnlighterJS resources not loaded yet!")}}(window,console);
</script>
<script id="wp-embed-js" src="https://xuhehuan.com/wp-includes/js/wp-embed.min.js?ver=66023064892ee9d19e5dc8a8888440d7" type="text/javascript"></script>
</body>
</html>
<!-- Dynamic page generated in 2.793 seconds. -->
