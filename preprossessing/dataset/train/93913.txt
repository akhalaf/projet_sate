<!DOCTYPE html>
<html xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>Albérlet, Kiadó lakás, Kiadó Szoba, Lakótárs – Alberlet-Szobatars.hu </title>
<meta content="albérlet, kiadó szoba, kiadó albérlet, kiadó lakás, kiadó ház" name="keywords"/>
<meta content="Albérlet , kiadó lakás, kiadó szoba és lakótárs kereső. Nálunk megtalálod amit keresel. Térképes albérletkeresés. Ingyenes hirdetésfeladás." name="description"/>
<meta content="54543275ccda4e3ee35ba50235599669" name="verification"/>
<link href="/css/alberlet.css?v=20150412" rel="stylesheet" type="text/css"/>
<link href="/css/ui-lightness/jquery.ui.css" rel="stylesheet" type="text/css"/>
<link href="/css/jcarousel.css?v=20120612" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="//maps.google.com/maps/api/js?sensor=false&amp;region=HU&amp;libraries=geometry&amp;key=AIzaSyAGwYAb5uC_8tSyMNNp69Vfe4gUEmMXzTQ" type="text/javascript"></script>
<script src="/js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<script src="/js/plugins.js?v=20120711" type="text/javascript"></script>
<script type="text/javascript">
		if( window.location.hash == '#_=_' ) window.location.hash = '';
	</script>
<script src="/js/site.js?v=20120729" type="text/javascript"></script>
</head>
<body class="hu">
<div id="fb-root"></div>
<div id="outer">
<div id="header">
<h1 class="text">Kiadó albérlet és szoba hirdetések országszerte</h1>
<div class="btns">
<div class="left"><a class="rbtn registerbtn" href="#"><span>REGISZTRÁCIÓ</span></a></div>
<div class="left"><a class="gbtn" href="/alberlet/new/1"><span>HIRDETÉSFELADÁS</span></a></div>
<div class="left" style="position:relative;top:4px;left:10px;"><a href="http://flat-rent-hungary.com" style="color:#9ee2ff;"><img src="/pix/flag-en.png"/></a></div>
<br class="clear"/>
</div>
<div class="logo"><a href="/"><img alt="Albérlet és Szobatárs kereső" src="/pix/logo.jpg"/></a></div>
<div class="tabs">
<div>
<a href="/alberlet/varos/budapest">ALBÉRLET</a>
<a href="/kiado-szoba/kereses?city_id=1">SZOBA</a>
<a href="/alberletet-keres/kereses?city_id=1">ALBÉRLŐ</a>
<a href="/lakotars/kereses?city_id=1">LAKÓTÁRS</a>
</div>
</div>
<div id="fblogin" style="position:absolute;top:43px;margin-left: 793px;">
<a href="https://www.facebook.com/v4.0/dialog/oauth?client_id=732956520489545&amp;state=79f387e7d8ad58fa656330ef35d44f1f&amp;response_type=code&amp;sdk=php-sdk-5.7.0&amp;redirect_uri=https%3A%2F%2Fwww.alberlet-szobatars.hu&amp;scope=email"><img src="/pix/fblogin.jpg"/></a>
</div>
<div class="login" id="loginbtn"><img src="/pix/login.jpg"/></div>
</div>
<div id="main">
<div id="content">
<div id="search">
<ul class="tabs" id="searchtabs">
<li class="act" id="tab_sublet" rel="/alberlet/varos/budapest"><h1>Albérlet</h1></li>
<li class="" id="tab_room" rel="/kiado-szoba/kereses"><h1>Szoba</h1></li>
<li class="" id="tab_sublet_search" rel="/alberletet-keres/kereses"><h1>Albérlő</h1></li>
<li class="" id="tab_room_search" rel="/lakotars/kereses"><h1>Lakótárs</h1></li>
</ul>
<div class="form">
<form action="/alberlet/kereses" id="searchfrm" method="get" name="searchfrm">
<input id="city_id" name="city_id" type="hidden" value="1"/>
<input id="minfee" name="minfee" type="hidden" value=""/>
<input id="maxfee" name="maxfee" type="hidden" value=""/>
<input id="minsize" name="minsize" type="hidden" value=""/>
<input id="maxsize" name="maxsize" type="hidden" value=""/>
<input id="minroom" name="minroom" type="hidden" value=""/>
<input id="maxroom" name="maxroom" type="hidden" value=""/>
<input id="realty_type" name="realty_type" type="hidden" value=""/>
<input id="room_separate" name="room_separate" type="hidden" value=""/>
<div class="mainfields">
<input id="city" name="city" placeholder="Budapest" type="text"/>
<input type="submit" value="Keresés"/>
</div>
</form>
<div class="subfields" id="search_sublet">
<input class="minfee txt" maxlength="3" name="minfeex" placeholder="min" style="width:22px;margin-right:44px;" type="text"/>
<input class="maxfee txt" maxlength="3" name="maxfee" placeholder="max" style="width:22px;margin-right:44px;" type="text"/>
<div class="selectcont2" style="margin-left:160px;margin-top:-15px;width: 80px">
<span class="value">Mindegy</span>
<select class="size select" name="size" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());">
<option value="0">Mindegy</option>
<option value="0_50">50m² alatt</option>
<option value="50_70">50-75m² között</option>
<option value="75_100">75-100m² között</option>
<option value="100_0">100m² felett</option>
</select>
</div>
<div class="selectcont2" style="margin-left:300px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="minroom" name="minroom" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: -42px;">
<option value="0">Mindegy</option>
<option value="1">1 vagy több</option>
<option value="2">2 vagy több</option>
<option value="3">3 vagy több</option>
<option value="4">4 vagy több</option>
<option value="5">5 vagy több</option>
</select>
</div>
<div class="selectcont2" style="margin-left:423px;margin-top:-15px;width: 64px">
<span class="value">Lakás</span>
<select class="type select" name="type" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: 0px;width:83px">
<option selected="true" value="1">Lakás</option>
<option value="2">Ház</option>
</select>
</div>
</div>
<div class="subfields" id="search_room">
<input class="minfee txt" maxlength="3" name="minfeex" placeholder="min" style="width:22px;margin-right:44px;" type="text"/>
<input class="maxfee txt" maxlength="3" name="maxfee" placeholder="max" style="width:22px;margin-right:44px;" type="text"/>
<div class="selectcont2" style="margin-left:160px;margin-top:-15px;width: 80px">
<span class="value">Mindegy</span>
<select class="size select" name="size" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());">
<option value="0">Mindegy</option>
<option value="0_10">10m² alatt</option>
<option value="10_15">10-15m² között</option>
<option value="15_0">15m² felett</option>
</select>
</div>
<div class="selectcont2" style="margin-left:300px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="minroom" name="roommate_distinct" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: -42px;">
<option value="0">Mindegy</option>
<option value="1">Igen</option>
<option value="2">Nem</option>
</select>
</div>
<div class="selectcont2" style="margin-left:423px;margin-top:-15px;width: 64px">
<span class="value">Lakás</span>
<select class="type select" name="type" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: 0px;width:83px">
<option selected="true" value="1">Lakás</option>
<option value="2">Ház</option>
</select>
</div>
</div>
<div class="subfields" id="search_sublet_search">
<input class="minfee txt" maxlength="3" name="minfeex" placeholder="min" style="width:22px;margin-right:44px;" type="text"/>
<input class="maxfee txt" maxlength="3" name="maxfee" placeholder="max" style="width:22px;margin-right:44px;" type="text"/>
<div class="selectcont2" style="margin-left:160px;margin-top:-15px;width: 80px">
<span class="value">Mindegy</span>
<select class="size select" name="size" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());">
<option value="0">Mindegy</option>
<option value="0_50">50m² alatt</option>
<option value="50_75">50-75m² között</option>
<option value="75_100">75-100m² között</option>
<option value="100_0">100m² felett</option>
</select>
</div>
<div class="selectcont2" style="margin-left:300px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="minroom" name="minroom" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: -42px;">
<option value="0">Mindegy</option>
<option value="1">1 vagy több</option>
<option value="2">2 vagy több</option>
<option value="3">3 vagy több</option>
<option value="4">4 vagy több</option>
</select>
</div>
<div class="selectcont2" style="margin-left:423px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="type select" name="realty_type" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: 0px;width:83px">
<option selected="true" value="0">Mindegy</option>
<option value="1">panel lakás</option>
<option value="2">tégla lakás</option>
<option value="3">újépítésű lakás</option>
<option value="4">ház</option>
</select>
</div>
</div>
<div class="subfields" id="search_room_search">
<input class="minfee txt" maxlength="3" name="minfeex" placeholder="min" style="width:22px;margin-right:44px;" type="text"/>
<input class="maxfee txt" maxlength="3" name="maxfee" placeholder="max" style="width:22px;margin-right:44px;" type="text"/>
<div class="selectcont2" style="margin-left:160px;margin-top:-15px;width: 80px">
<span class="value">Mindegy</span>
<select class="size select" name="size" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());">
<option value="0">Mindegy</option>
<option value="0_10">10m² alatt</option>
<option value="10_15">10-15m² között</option>
<option value="15_0">15m² felett</option>
</select>
</div>
<div class="selectcont2" style="margin-left:300px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="room_separate" name="room_separate" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: -42px;">
<option value="0">Mindegy</option>
<option value="1">Igen</option>
<option value="2">Nem</option>
</select>
</div>
<div class="selectcont2" style="margin-left:423px;margin-top:-15px;width: 64px">
<span class="value">Mindegy</span>
<select class="type select" name="realty_type" onchange="$(this).parent().children(':first-child').html($('option:selected',this).text());" style="margin-left: 0px;width:83px">
<option selected="true" value="0">Mindegy</option>
<option value="1">panel lakás</option>
<option value="2">tégla lakás</option>
<option value="3">újépítésű lakás</option>
<option value="4">ház</option>
</select>
</div>
</div>
</div>
<div id="mainmap"><a href="/map/kereses?city_id=1"><img src="/pix/mainmap_hu.png"/></a></div>
</div>
<div class="registerbox">
<div class="registerbtn"><img src="/pix/registerbtn.png"/></div>
<p class="orange14b">Ha albérletet vagy szobát keresel, vagy hirdetést szeretnél feladni.<br/>Regisztrálj ingyenesen és add fel hirdetésedet!</p>
<p>Ha regisztrálsz, elmentheted kereséseidet, kedvenc hirdetéseidet.<br/>Emailben értesülhetsz a legfrissebb, csak téged érdeklő albérletekről, kiadó szobákról.</p>
</div>
<div class="main">
<div id="col1">
<div>
<div class="bluetitle">KIEMELT HIRDETÉSEK</div>
<div class="cframe seeded">
<ul class="jcarousel2 jcarousel-skin-main" id="mainseeded">
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-xiv-kerulet-mogyoródi-/33199"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xiv-kerulet-mogyoródi-/33199"><h3>Budapest, XIV. ker. | 40.000 Ft/hó</h3></a>

						Méret:<span>80 m²</span> | Szobaszám: <span>1 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/alberlet/budapest-xi-kerulet-egér-/32953"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/alberlet/budapest-xi-kerulet-egér-/32953"><h3>Budapest, XI. ker. | 35.000 Ft/hó</h3></a>

						Méret:<span>30 m²</span> | Szobaszám: <span>1 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">11</div>
<a href="/alberlet/budapest-xix-kerulet-kispest-rákóczi-u-27/34550"><img src="/pics/54/tn/54620.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xix-kerulet-kispest-rákóczi-u-27/34550"><h3>Budapest, XIX. ker. | 155.000 Ft/hó</h3></a>

						Méret:<span>59 m²</span> | Szobaszám: <span>1+2 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-xiv-kerulet-mogyoródi-/32975"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xiv-kerulet-mogyoródi-/32975"><h3>Budapest, XIV. ker. | 39.000 Ft/hó</h3></a>

						Méret:<span>65 m²</span> | Szobaszám: <span>3 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-vi-kerulet-erzsebetvaros-csengery-62/33562"><img src="/pics/53/tn/53202.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-vi-kerulet-erzsebetvaros-csengery-62/33562"><h3>Budapest, VI. ker. | 115.000 Ft/hó</h3></a>

						Méret:<span>95 m²</span> | Szobaszám: <span>4 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">15</div>
<a href="/alberlet/budapest-xviii-kerulet-akácos-23/34492"><img src="/pics/54/tn/54494.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xviii-kerulet-akácos-23/34492"><h3>Budapest, XVIII. ker. | 45.000 Ft/hó</h3></a>

						Méret:<span>150 m²</span> | Szobaszám: <span>5 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">10</div>
<a href="/alberlet/budapest-xiii-kerulet-angyalfold-lehel-66/34503"><img src="/pics/54/tn/54546.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xiii-kerulet-angyalfold-lehel-66/34503"><h3>Budapest, XIII. ker. | 155.000 Ft/hó</h3></a>

						Méret:<span>45 m²</span> | Szobaszám: <span>2 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">6</div>
<a href="/alberlet/budapest-ix-kerulet-ferencvaros-ferenc-körút-30/34456"><img src="/pics/54/tn/54405.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-ix-kerulet-ferencvaros-ferenc-körút-30/34456"><h3>Budapest, IX. ker. | 100.000 Ft/hó</h3></a>

						Méret:<span>60 m²</span> | Szobaszám: <span>3 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-xiii-kerulet-reitter-/33656"><img src="/pics/53/tn/53359.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xiii-kerulet-reitter-/33656"><h3>Budapest, XIII. ker. | 40.000 Ft/hó</h3></a>

						Méret:<span>60 m²</span> | Szobaszám: <span>2 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-xi-kerulet-karolina-/33421"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xi-kerulet-karolina-/33421"><h3>Budapest, XI. ker. | 40.000 Ft/hó</h3></a>

						Méret:<span>60 m²</span> | Szobaszám: <span>2 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-xiii-kerulet-ujlipotvaros-tallér-16/34680"><img src="/pics/54/tn/54854.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xiii-kerulet-ujlipotvaros-tallér-16/34680"><h3>Budapest, XIII. ker. | 210.000 Ft/hó</h3></a>

						Méret:<span>76 m²</span> | Szobaszám: <span>2+1 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-xi-kerulet-fehérvári-/33657"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xi-kerulet-fehérvári-/33657"><h3>Budapest, XI. ker. | 30.000 Ft/hó</h3></a>

						Méret:<span>55 m²</span> | Szobaszám: <span>2 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/alberlet/szentendre-fő-/33246"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/alberlet/szentendre-fő-/33246"><h3>Szentendre | 45.000 Ft/hó</h3></a>

						Méret:<span>20 m²</span> | Szobaszám: <span>1 </span>
</li>
<li class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-xi-kerulet-fehérvári-/33422"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xi-kerulet-fehérvári-/33422"><h3>Budapest, XI. ker. | 30.000 Ft/hó</h3></a>

						Méret:<span>55 m²</span> | Szobaszám: <span>2 </span>
</li>
<li class="item last">
<div class="icons">
</div>
<div class="listimage">
<a href="/alberlet/budapest-xviii-kerulet-benjamin-/33295"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/alberlet/budapest-xviii-kerulet-benjamin-/33295"><h3>Budapest, XVIII. ker. | 25.000 Ft/hó</h3></a>

						Méret:<span>70 m²</span> | Szobaszám: <span>1 </span>
</li>
</ul>
</div>
</div>
<div>
<div class="bluetitle">LEGÚJABB HIRDETÉSEK</div>
<div class="cframe">
<div class="subletlist left">
<a href="/alberlet/kereses?city_id=1"><h1>KIADÓ ALBÉRLETEK</h1></a>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-ix-kerulet-ferencvaros-telepy-/34736"><img src="/pics/55/tn/55043.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-ix-kerulet-ferencvaros-telepy-/34736"><h3>Budapest, IX. ker.,  Telepy utca</h3></a>

								Bérleti díj:<span>100.000 Ft/hó</span><br/>
								Méret:<span>35 m²</span><br/>
								Szobaszám:<span>1+1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-xii-kerulet-orbanhegy-ugocsa-/34735"><img src="/pics/55/tn/55042.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xii-kerulet-orbanhegy-ugocsa-/34735"><h3>Budapest, XII. ker.,  Ugocsa utca</h3></a>

								Bérleti díj:<span>75.000 Ft/hó</span><br/>
								Méret:<span>29 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-xiii-kerulet-angyalfold-béke-26/34734"><img src="/pics/55/tn/55038.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xiii-kerulet-angyalfold-béke-26/34734"><h3>Budapest, XIII. ker.,  Béke utca 26..</h3></a>

								Bérleti díj:<span>45.000 Ft/hó</span><br/>
								Méret:<span>30 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-xiii-kerulet-angyalfold-béke-26/34733"><img src="/pics/55/tn/55035.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-xiii-kerulet-angyalfold-béke-26/34733"><h3>Budapest, XIII. ker.,  Béke. utca 26..</h3></a>

								Bérleti díj:<span>45.000 Ft/hó</span><br/>
								Méret:<span>30 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/alberlet/budapest-xiii-kerulet-fiastyuk-/34729"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/alberlet/budapest-xiii-kerulet-fiastyuk-/34729"><h3>Budapest, XIII. ker.,  Fiastyuk utca</h3></a>

								Bérleti díj:<span>40.000 Ft/hó</span><br/>
								Méret:<span>56 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-viii-kerulet-jozsefvaros-rákoczi-út-61/34728"><img src="/pics/55/tn/55020.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-viii-kerulet-jozsefvaros-rákoczi-út-61/34728"><h3>Budapest, VIII. ker.,  Rákoczi út út 61.</h3></a>

								Bérleti díj:<span>52.000 Ft/hó</span><br/>
								Méret:<span>26 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item last">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/alberlet/budapest-iv-kerulet-ujpest-pozsonyi-2-a/32972"><img src="/pics/52/tn/52483.jpg"/></a>
</div>
<a class="address" href="/alberlet/budapest-iv-kerulet-ujpest-pozsonyi-2-a/32972"><h3>Budapest, IV. ker.,  Pozsonyi utca 2/a.</h3></a>

								Bérleti díj:<span>130.000 Ft/hó</span><br/>
								Méret:<span>45 m²</span><br/>
								Szobaszám:<span>1+1 szoba</span><br/>
								panel lakás
								
							</div>
</div>
<div class="roomlist left">
<a href="/kiado-szoba/kereses?city_id=1"><h1>KIADÓ SZOBÁK</h1></a>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-xx-kerulet-pesterzsebet-hosszú-97/34732"><img src="/pics/55/tn/55028.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xx-kerulet-pesterzsebet-hosszú-97/34732"><h3>Budapest, XX. ker.,  Hosszú  utca 97.</h3></a>

								Bérleti díj:<span>70.000 Ft/hó</span><br/>
								Méret:<span>200 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								családi ház ház
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-ix-kerulet-ferencvaros-lónyay-19/21867"><img src="/pics/37/tn/37800.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-ix-kerulet-ferencvaros-lónyay-19/21867"><h3>Budapest, IX. ker.,  Lónyay utca 19.</h3></a>

								Bérleti díj:<span>35.000 Ft/hó</span><br/>
								Méret:<span>73 m²</span><br/>
								Szobaszám:<span>2 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-viii-kerulet-jozsefvaros-józsef-/34731"><img src="/pics/55/tn/55027.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-viii-kerulet-jozsefvaros-józsef-/34731"><h3>Budapest, VIII. ker.,  József körút</h3></a>

								Bérleti díj:<span>45.000 Ft/hó</span><br/>
								Méret:<span>37 m²</span><br/>
								Szobaszám:<span>1+1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<a href="/kiado-szoba/budapest-viii-kerulet-jozsefvaros-mátyás-2/34730"><img src="/pix/noimage.png"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-viii-kerulet-jozsefvaros-mátyás-2/34730"><h3>Budapest, VIII. ker.,  Mátyás tér 2.</h3></a>

								Bérleti díj:<span>43.000 Ft/hó</span><br/>
								Méret:<span>92 m²</span><br/>
								Szobaszám:<span>2+3 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-vii-kerulet-garay-/34726"><img src="/pics/55/tn/55002.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-vii-kerulet-garay-/34726"><h3>Budapest, VII. ker.,  Garay  utca</h3></a>

								Bérleti díj:<span>39.000 Ft/hó</span><br/>
								Méret:<span>82 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								tégla lakás
								
							</div>
<div class="item">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-xvi-kerulet-matyasfold-kerepesi-00/34720"><img src="/pics/54/tn/54991.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xvi-kerulet-matyasfold-kerepesi-00/34720"><h3>Budapest, XVI. ker.,  Kerepesi út 00.</h3></a>

								Bérleti díj:<span>1.000 Ft/hó</span><br/>
								Méret:<span>40 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								panel lakás
								
							</div>
<div class="item last">
<div class="icons">
</div>
<div class="listimage">
<div class="photocount">1</div>
<a href="/kiado-szoba/budapest-xvi-kerulet-matyasfold-kerepesi-00/34719"><img src="/pics/54/tn/54990.jpg"/></a>
</div>
<a class="address" href="/kiado-szoba/budapest-xvi-kerulet-matyasfold-kerepesi-00/34719"><h3>Budapest, XVI. ker.,  kerepesi út 00.</h3></a>

								Bérleti díj:<span>0 Ft/hó</span><br/>
								Méret:<span>40 m²</span><br/>
								Szobaszám:<span>1 szoba</span><br/>
								panel lakás
								
							</div>
</div>
<br class="clear"/>
</div>
</div>
</div>
<div id="col2">
<div class="bluetitle">ALBÉRLET KERESÉS</div>
<div class="cframe">
<div id="rsublets">
<a href="/alberlet/varos/Budapest/buda"><h3>Albérlet Buda</h3></a>
<div>
<ul class="list">
<li><a href="/alberlet/varos/Budapest/i-kerulet"><h3>Albérlet I.kerület (Vár, Vérmező, Tabán)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/ii-kerulet"><h3>Albérlet II.kerület (Hűvösvölgy)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/iii-kerulet"><h3>Albérlet III.kerület (Óbuda)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xi-kerulet"><h3>Albérlet XI.kerület (Gazdagrét, Kelenföld)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xii-kerulet"><h3>Albérlet XII.kerület (Hegyvidék, Zugliget)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xxii-kerulet"><h3>Albérlet XXII.kerület (Budafok)</h3></a></li>
</ul>
</div>
<br/>
<a href="/alberlet/varos/Budapest/pest"><h3>Albérlet Pest</h3></a>
<div>
<ul class="list">
<li><a href="/alberlet/varos/Budapest/iv-kerulet"><h3>Albérlet IV.kerület (Újpest)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/v-kerulet"><h3>Albérlet V.kerület (Belváros, Lipótváros)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/vi-kerulet"><h3>Albérlet VI.kerület (Terézváros)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/vii-kerulet"><h3>Albérlet VII.kerület (Erzsébetváros)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/viii-kerulet"><h3>Albérlet VIII.kerület (Józsefváros)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/ix-kerulet"><h3>Albérlet IX.kerület (Ferencváros)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/x-kerulet"><h3>Albérlet X.kerület (Kőbánya)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xiii-kerulet"><h3>Albérlet XIII.kerület (Angyalföld)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xiv-kerulet"><h3>Albérlet XIV.kerület (Zugló)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xv-kerulet"><h3>Albérlet XV.kerület (Rákospalota)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xvi-kerulet"><h3>Albérlet XVI.kerület (Cinkota, Mátyásföld)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xvii-kerulet"><h3>Albérlet XVII.kerület (Rákoskeresztúr)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xviii-kerulet"><h3>Albérlet XVIII.kerület (Pestszentlőrinc)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xix-kerulet"><h3>Albérlet XIX.kerület (Kispest)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xx-kerulet"><h3>Albérlet XX.kerület (Pesterzsébet)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xxi-kerulet"><h3>Albérlet XXI.kerület (Csepel)</h3></a></li>
<li><a href="/alberlet/varos/Budapest/xxiii-kerulet"><h3>Albérlet XXIII.kerület (Soroksár)</h3></a></li>
</ul>
</div>
<br class="clear"/>
<div>
<div class="left">
<a href="/alberlet/varos/Budapest"><h3>Budapest</h3></a>
<a href="/alberlet/varos/Debrecen"><h3>Debrecen</h3></a>
<a href="/alberlet/varos/Gyor"><h3>Győr</h3></a>
<a href="/alberlet/varos/Kaposvar"><h3>Kaposvár</h3></a>
<a href="/alberlet/varos/Kecskemet"><h3>Kecskemét</h3></a>
<a href="/alberlet/varos/Miskolc"><h3>Miskolc</h3></a>
<a href="/alberlet/varos/Nyiregyhaza"><h3>Nyíregyháza</h3></a>
</div>
<div class="right">
<a href="/alberlet/varos/Pecs"><h3>Pécs</h3></a>
<a href="/alberlet/varos/Sopron"><h3>Sopron</h3></a>
<a href="/alberlet/varos/Szeged"><h3>Szeged</h3></a>
<a href="/alberlet/varos/Szekesfehervar"><h3>Székesfehérvár</h3></a>
<a href="/alberlet/varos/Szombathely"><h3>Szombathely</h3></a>
<a href="/alberlet/varos/Veszprem"><h3>Veszprém</h3></a>
<a href="/alberlet/varos/Zalaegerszeg"><h3>Zalaegerszeg</h3></a>
</div>
<br class="clear"/>
</div>
</div>
</div>
<br/>
<div class="goAdverticum center" id="zone33010"></div>
<br/><br/><br/>
</div>
</div>
<script type="text/javascript">
	
    var prediv;
    
	$(document).ready(function() {	
	
        var prediv='#search_sublet';
    	
		$('.rsublets').click(function(){
			$('#rsublets').show()
			$('.rsublets h1').addClass('act');			
			$('.rrooms h1').removeClass('act');					
			$('#rrooms').hide();			
		});

		$('.rrooms').click(function(){
			$('#rrooms').show()
			$('.rrooms h1').addClass('act');			
			$('.rsublets h1').removeClass('act');					
			$('#rsublets').hide();			
		});

		
		$("#searchfrm").submit(function(){			
            
            $("#minfee").val($(prediv+' .minfee').val());
			$("#maxfee").val($(prediv+' .maxfee').val());
            								
			var value=$(prediv +' .size').val();
			
			if (value.indexOf('_') !=-1)
			{
				var tmp = new Array();
				tmp = value.split('_');
				
				$("#minsize").val(tmp[0]);
				$("#maxsize").val(tmp[1]);
			}			
            
			$("#minroom").val($(prediv+' .minroom').val());
			$('#room_separate').val($(prediv+' .room_separate').val());
			$('#realty_type').val($(prediv+' .type').val());
			
		});
		
		$("#city").autocomplete({
			source: "/ajax/get_cities",
			minLength: 2,			
			select: function( event, ui ) {
				$('#city_id').val(ui.item.id);				
				$('#searchfrm').click();		  				
			}
		});	
		
		
		$('#searchtabs li').click(function(){
			var id=$(this).attr('id').replace('tab_','');
			
            prediv='#search_'+id;
            
			$('#search').css('background-image', 'url("/pix/searchbg_'+id+'2.png")');
			
			$('.subfields').hide();
			
			$('#search_'+id).show();
			
			
			if(!$(this).hasClass('act')){
				var act=$(this).attr('rel');
				$('#searchfrm').attr('action',act);
				$('#searchtabs li').removeClass('act');
				$(this).addClass('act');
			}
			
		});
		
		
		$('#mainseeded').jcarousel({auto: 3,wrap: 'last'});
		
		
	
	});

</script>
</div>
<br clear="all"/>
</div>
</div>
<br/>
<div id="footerbg">
<div class="left"></div>
<div class="right"></div>
</div>
<div id="footer">
<div class="links">
<div class="col1">
<div>ÁLTALÁNOS TUDNIVALÓK</div>
<a href="/adat/rolunk">Rólunk</a>
<a href="/adat/gyik">Segítség</a>
<a href="/feedback">Írjon nekünk</a>
<a href="/adat/adatvedelem">Adatvédelem</a>
<a href="/adat/uzleti-szabalyzat">Üzleti szabályzat</a>
<a href="/alberlet/new/1">Hirdetésfeladás</a>
<a href="/adat/impresszum">Impresszum</a>
</div>
<div class="col1">
<div>KIADÓ LAKÁSOK</div>
<a href="/alberlet/varos/budapest">Budapest</a>
<a href="/alberlet/varos/budapest/buda">Buda</a>
<a href="/alberlet/varos/budapest/pest">Pest</a>
<a href="/alberlet/varos/debrecen">Debrecen</a>
<a href="/alberlet/varos/szeged">Szeged</a>
<a href="/alberlet/varos/miskolc">Miskolc</a>
<a href="/alberlet/varos/kecskemet">Kecskemét</a>
<a href="/alberlet/varos/gyor">Győr</a>
<a href="/alberlet/varos/pecs">Pécs</a>
<a href="/alberlet/varos/szekesfehervar">Székesfehérvár</a>
<a href="/alberlet/varos/szombathely">Szombathely</a>
</div>
<div class="col1">
<div>KIADÓ HÁZAK</div>
<a href="/alberlet/kereses?city_id=1&amp;maxfee=50&amp;city=Budapest&amp;realty_type=2">Budapest</a>
<a href="/alberlet/kereses?city_id=908&amp;maxfee=50&amp;city=Debrecen&amp;realty_type=2">Debrecen</a>
<a href="/alberlet/kereses?city_id=2791&amp;maxfee=50&amp;city=Gy%c5%91r&amp;realty_type=2">Győr</a>
<a href="/alberlet/kereses?city_id=1753&amp;maxfee=50&amp;city=Kaposv%c3%a1r&amp;realty_type=2">Kaposvár</a>
<a href="/alberlet/kereses?city_id=1377&amp;maxfee=50&amp;city=Kecskem%c3%a9t&amp;realty_type=2">Kecskemét</a>
<a href="/alberlet/kereses?city_id=583&amp;maxfee=50&amp;city=Miskolc&amp;realty_type=2">Miskolc</a>
<a href="/alberlet/kereses?city_id=1034&amp;maxfee=50&amp;city=Ny%c3%adregyh%c3%a1za&amp;realty_type=2">Nyíregyháza</a>
<a href="/alberlet/kereses?city_id=1850&amp;maxfee=50&amp;city=P%c3%a9cs&amp;realty_type=2">Pécs</a>
<a href="/alberlet/kereses?city_id=2933&amp;maxfee=50&amp;city=Sopron&amp;realty_type=2">Sopron</a>
<a href="/alberlet/kereses?city_id=1510&amp;maxfee=50&amp;city=Szeged&amp;realty_type=2">Szeged</a>
<a href="/alberlet/kereses?city_id=2125&amp;maxfee=50&amp;city=Sz%c3%a9kesfeh%c3%a9rv%c3%a1r&amp;realty_type=2">Székesfehérvár</a>
</div>
<div class="col1">
<div>OLCSÓ ALBÉRLET</div>
<a href="/alberlet/kereses?city_id=1&amp;maxfee=50&amp;city=Budapest">Budapest</a>
<a href="/alberlet/kereses?city_id=908&amp;maxfee=50&amp;city=Debrecen">Debrecen</a>
<a href="/alberlet/kereses?city_id=583&amp;maxfee=50&amp;city=Miskolc">Miskolc</a>
<a href="/alberlet/kereses?city_id=1510&amp;maxfee=50&amp;city=Szeged">Szeged</a>
<a href="/alberlet/kereses?city_id=1850&amp;maxfee=50&amp;city=P%c3%a9cs">Pécs</a>
<a href="/alberlet/kereses?city_id=2791&amp;maxfee=50&amp;city=Gy%c5%91r">Győr</a>
<a href="/alberlet/kereses?city_id=1377&amp;maxfee=50&amp;city=Kecskem%c3%a9t">Kecskemét</a>
<a href="/alberlet/kereses?city_id=1034&amp;maxfee=50&amp;city=Ny%c3%adregyh%c3%a1za">Nyíregyháza</a>
<a href="/alberlet/kereses?city_id=2125&amp;maxfee=50&amp;city=Sz%c3%a9kesfeh%c3%a9rv%c3%a1r">Székesfehérvár</a>
<a href="/alberlet/kereses?city_id=3042&amp;maxfee=50&amp;city=Szombathely">Szombathely</a>
<a href="/alberlet/kereses?city_id=2203&amp;maxfee=50&amp;city=Veszpr%c3%a9m">Veszprém</a>
</div>
<div class="col4">
<div class="icons">
<a href="javascript:(void());" onclick="window.open('http://www.startlap.hu/sajat_linkek/addlink.php?url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title));return false;" title="Add a Startlaphoz"><img src="/pix/bottostartlap.png"/></a>
<a href="http://www.facebook.com/sharer.php?u=http://www.alberlet-szobatars.hu/" target="_blank" title="Add a Facebook-hoz"><img src="/pix/bottomfb.png"/></a>
<a class="ico tw" href="javascript:void(0);" onclick="window.open('http://twitter.com/home?status='+encodeURIComponent(document.title)+' '+encodeURIComponent(location.href));return false;" title="Add a Twitter-hez"><img src="/pix/bottomtw.png"/></a>
<a href="https://plus.google.com/share?url=http://www.alberlet-szobatars.hu/" target="_blank"><img src="/pix/bottomgplus.png"/></a>
</div>
<a class="logo" href="/"></a>
<!-- PayPal Logo --><div style="position:relative;top:10px;left:25px;"><img alt="PayPal Acceptance Mark" border="0" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700');" src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" width="181"/></div>
</div>
</div>
<div class="bottom"></div>
<div class="copyright">Copyright © 2015 alberlet-szobatars.hu. All rights reserved.</div>
</div>
<script src="//www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
	_uacct = "UA-181314-2";
	urchinTracker();
	</script>
</body>
</html>