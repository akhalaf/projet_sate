<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title>Aktuell</title><meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible"/><meta content="bogensport-kaufering" name="description"/><meta content="bogensport, bogenschießen, bogeninfo, bogenturniere,
bogenvereine, wertungszettel, schießzettel, info-bogensport,
bogensport-kaufering," name="Keywords"/><meta content="false" http-equiv="imagetoolbar"/><link href="mediapool/100/1006502/resources/custom_1610133544879.css" rel="stylesheet" type="text/css"/></head><body><div id="body"><script language="JavaScript" src="js/mm.js" type="text/javascript"></script><script language="JavaScript" src="sound/playsound.js" type="text/javascript"></script><a id="top" name="top"></a><table border="0" cellpadding="0" cellspacing="0" id="mainTable" width="773"><tr><td class="bgLeft" rowspan="2"><img alt="" border="0" height="1" src="designs/design345/color4/images/i.gif" width="19"/></td><td class="menubg" valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><img alt="" border="0" height="57" src="designs/design345/color4/images/imgLogoTop.gif" width="193"/></td></tr><tr><td align="center" valign="top"><img alt="" border="0" id="logo" src="mediapool/100/1006502/resources/logo.png" vspace="5"/></td></tr><tr><td valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr><td><img alt="" border="0" height="5" src="designs/design345/color4/images/menu1/imgMenuTop.gif" width="193"/></td></tr><tr><td><img alt="Aktuell" border="0" name="menuimg11075338" src="mediapool/100/1006502/resources/tree/11075338_419718268c.png" title="Aktuell"/><table border="0" cellpadding="0" cellspacing="0"><tr><td rowspan="2"><table border="0" cellpadding="0" cellspacing="0"><tr><td><a href="abteilungsfuehrung.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075339',''
        ,'mediapool/100/1006502/resources/tree/11075339_1450145016a.png',1)
      "><img alt="Abteilungsführung" border="0" name="menuimg11075339" src="mediapool/100/1006502/resources/tree/11075339_1450145016.png" title="Abteilungsführung"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075339_1450145016a.png')</script><br/></td></tr><tr><td><a href="abteilungs-chronik.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11081687',''
        ,'mediapool/100/1006502/resources/tree/11081687_1576350396a.png',1)
      "><img alt="Abteilungs-Chronik" border="0" name="menuimg11081687" src="mediapool/100/1006502/resources/tree/11081687_1576350396.png" title="Abteilungs-Chronik"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11081687_1576350396a.png')</script><br/></td></tr><tr><td><a href="fotogalerie.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg13011402',''
        ,'mediapool/100/1006502/resources/tree/13011402_1085759320a.png',1)
      "><img alt="Fotogalerie" border="0" name="menuimg13011402" src="mediapool/100/1006502/resources/tree/13011402_1085759320.png" title="Fotogalerie"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/13011402_1085759320a.png')</script><br/></td></tr><tr><td><a href="schueler-jugend.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg13011331',''
        ,'mediapool/100/1006502/resources/tree/13011331_1437939613a.png',1)
      "><img alt="Schüler/Jugend" border="0" name="menuimg13011331" src="mediapool/100/1006502/resources/tree/13011331_1437939613.png" title="Schüler/Jugend"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/13011331_1437939613a.png')</script><br/></td></tr><tr><td><a href="videoclips.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg13011401',''
        ,'mediapool/100/1006502/resources/tree/13011401_1994248914a.png',1)
      "><img alt="Videoclips" border="0" name="menuimg13011401" src="mediapool/100/1006502/resources/tree/13011401_1994248914.png" title="Videoclips"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/13011401_1994248914a.png')</script><br/></td></tr></table></td></tr></table></td></tr><tr><td><a href="bogen-links.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075347',''
        ,'mediapool/100/1006502/resources/tree/11075347_56256623a.png',1)
      "><img alt="Bogen-Links" border="0" name="menuimg11075347" src="mediapool/100/1006502/resources/tree/11075347_56256623.png" title="Bogen-Links"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075347_56256623a.png')</script></td></tr><tr><td><a href="bogenklassen-2021.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg17413842',''
        ,'mediapool/100/1006502/resources/tree/17413842_1131263157a.png',1)
      "><img alt="Bogenklassen 2021" border="0" name="menuimg17413842" src="mediapool/100/1006502/resources/tree/17413842_1131263157.png" title="Bogenklassen 2021"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/17413842_1131263157a.png')</script></td></tr><tr><td><a href="gaestebuch.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg12583425',''
        ,'mediapool/100/1006502/resources/tree/12583425_229978241a.png',1)
      "><img alt="Gästebuch" border="0" name="menuimg12583425" src="mediapool/100/1006502/resources/tree/12583425_229978241.png" title="Gästebuch"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/12583425_229978241a.png')</script></td></tr><tr><td><a href="infos-bogenschuetzen.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075824',''
        ,'mediapool/100/1006502/resources/tree/11075824_78344210a.png',1)
      "><img alt="Infos-Bogenschützen" border="0" name="menuimg11075824" src="mediapool/100/1006502/resources/tree/11075824_78344210.png" title="Infos-Bogenschützen"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075824_78344210a.png')</script></td></tr><tr><td><a href="infos-hauptverein.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075825',''
        ,'mediapool/100/1006502/resources/tree/11075825_1498372924a.png',1)
      "><img alt="Infos-Hauptverein" border="0" name="menuimg11075825" src="mediapool/100/1006502/resources/tree/11075825_1498372924.png" title="Infos-Hauptverein"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075825_1498372924a.png')</script></td></tr><tr><td><a href="infos-zum-training.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075344',''
        ,'mediapool/100/1006502/resources/tree/11075344_1339250582a.png',1)
      "><img alt="Infos zum Training" border="0" name="menuimg11075344" src="mediapool/100/1006502/resources/tree/11075344_1339250582.png" title="Infos zum Training"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075344_1339250582a.png')</script></td></tr><tr><td><a href="impressum.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075352',''
        ,'mediapool/100/1006502/resources/tree/11075352_1664052305a.png',1)
      "><img alt="Impressum" border="0" name="menuimg11075352" src="mediapool/100/1006502/resources/tree/11075352_1664052305.png" title="Impressum"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075352_1664052305a.png')</script></td></tr><tr><td><a href="jahresmeister.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg17777753',''
        ,'mediapool/100/1006502/resources/tree/17777753_1692250484a.png',1)
      "><img alt="Jahresmeister" border="0" name="menuimg17777753" src="mediapool/100/1006502/resources/tree/17777753_1692250484.png" title="Jahresmeister"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/17777753_1692250484a.png')</script></td></tr><tr><td><a href="kaufering.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg19202930',''
        ,'mediapool/100/1006502/resources/tree/19202930_1245355760a.png',1)
      "><img alt="Kaufering" border="0" name="menuimg19202930" src="mediapool/100/1006502/resources/tree/19202930_1245355760.png" title="Kaufering"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/19202930_1245355760a.png')</script></td></tr><tr><td><a href="kontakt.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg12583424',''
        ,'mediapool/100/1006502/resources/tree/12583424_1241508607a.png',1)
      "><img alt="Kontakt" border="0" name="menuimg12583424" src="mediapool/100/1006502/resources/tree/12583424_1241508607.png" title="Kontakt"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/12583424_1241508607a.png')</script></td></tr><tr><td><a href="meisterschaft-2020.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg17798607',''
        ,'mediapool/100/1006502/resources/tree/17798607_1063774085a.png',1)
      "><img alt="Meisterschaft 2020" border="0" name="menuimg17798607" src="mediapool/100/1006502/resources/tree/17798607_1063774085.png" title="Meisterschaft 2020"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/17798607_1063774085a.png')</script></td></tr><tr><td><a href="mitglied-werden.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075341',''
        ,'mediapool/100/1006502/resources/tree/11075341_1646145416a.png',1)
      "><img alt="Mitglied werden" border="0" name="menuimg11075341" src="mediapool/100/1006502/resources/tree/11075341_1646145416.png" title="Mitglied werden"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075341_1646145416a.png')</script></td></tr><tr><td><a href="sitemap.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075353',''
        ,'mediapool/100/1006502/resources/tree/11075353_569266896a.png',1)
      "><img alt="Sitemap" border="0" name="menuimg11075353" src="mediapool/100/1006502/resources/tree/11075353_569266896.png" title="Sitemap"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075353_569266896a.png')</script></td></tr><tr><td><a href="standort.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11137359',''
        ,'mediapool/100/1006502/resources/tree/11137359_433188379a.png',1)
      "><img alt="Standort" border="0" name="menuimg11137359" src="mediapool/100/1006502/resources/tree/11137359_433188379.png" title="Standort"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11137359_433188379a.png')</script></td></tr><tr><td><a href="turnierboegen.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg18750926',''
        ,'mediapool/100/1006502/resources/tree/18750926_604382755a.png',1)
      "><img alt="Turnierbögen" border="0" name="menuimg18750926" src="mediapool/100/1006502/resources/tree/18750926_604382755.png" title="Turnierbögen"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/18750926_604382755a.png')</script></td></tr><tr><td><a href="turniere-teilnehmer.html" onmouseout="MM_swapImgRestore()" onmouseover="
        MM_swapImage('menuimg11075345',''
        ,'mediapool/100/1006502/resources/tree/11075345_284711486a.png',1)
      "><img alt="Turniere/Teilnehmer" border="0" name="menuimg11075345" src="mediapool/100/1006502/resources/tree/11075345_284711486.png" title="Turniere/Teilnehmer"/></a><script type="text/javascript">MM_preloadImages('mediapool/100/1006502/resources/tree/11075345_284711486a.png')</script></td></tr><tr><td><img alt="" border="0" height="5" src="designs/design345/color4/images/menu1/imgMenuBottom.gif" width="193"/></td></tr></table><div id="vcounter" style="overflow: hidden; font-family: Tahoma, sans-serif; display: block;"><br/><br/><div style="text-align: center; font-family: Tahoma, sans-serif;"><span id="counter_text_before"></span><div style="font-size: 3pt;"></div><div id="counter_wrap" style="display: block;"><object align="middle" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" height="30" id="counter" width="116"><param name="AllowScriptAccess" value="always"/><param name="src" value="ce_vcounter/flash/design1.swf?url=%2F%2Fwww.livepages.de%2Factivities%2Fc%3Fp%3D1006502%26c%3Dtrue%26d%3Dtrue"/><param name="movie" value="ce_vcounter/flash/design1.swf?url=%2F%2Fwww.livepages.de%2Factivities%2Fc%3Fp%3D1006502%26c%3Dtrue%26d%3Dtrue"/><param name="menu" value="false"/><param name="quality" value="high"/><param name="wmode" value="transparent"/><param name="bgcolor" value="#ffffff"/><embed align="middle" allowscriptaccess="always" height="30" menu="false" name="counter" pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="ce_vcounter/flash/design1.swf?url=%2F%2Fwww.livepages.de%2Factivities%2Fc%3Fp%3D1006502%26c%3Dtrue%26d%3Dtrue" type="application/x-shockwave-flash" width="116" wmode="transparent"/></object></div><div style="font-size: 3pt;"></div><span id="counter_text_after"></span></div></div><script type="text/javascript">
		if (typeof document.all != 'undefined') {
			var counter = document.getElementById('vcounter');
			if (counter) {
				if (counter.parentNode) {
					var tdAboveCounterWidth = counter.parentNode.getAttribute("width");
					if (tdAboveCounterWidth) {
						counter.style.width = tdAboveCounterWidth;
					}
				}
			}
		}
		</script></td></tr><tr><td valign="top"><img alt="" border="0" height="117" id="keyvisual" src="designs/design345/color4/keyvisuals/keyv0.jpg" style="position:relative;top:20px" width="193"/></td></tr></table></td><td class="bgContent" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="right" valign="top"><img alt="" border="0" height="82" src="designs/design345/color4/images/imgSloganTop.gif" width="561"/></td></tr><tr><td valign="top"><img alt="" border="0" height="21" id="slogan" src="mediapool/100/1006502/resources/slogan_345_4_0.png" width="561"/></td></tr><tr><td id="txt" valign="top" width="100%"><img alt="" border="0" height="20" id="contentMinWidth" src="designs/design345/color4/images/i.gif"/><br/><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="100%">
<div align="left"><p> </p><p style="text-align: center;"><img alt="" border="0" height="667" hspace="0" id="56266379" src="mediapool/100/1006502/resources/big_56266379_0_500-667.jpg" title="" vspace="0" width="500"/></p><p style="text-align: center;"> </p></div>
</td>
</tr>
<tr>
<td valign="top"><img alt="" height="1" src="images/i.gif" width="462"/></td>
</tr>
</table>
</td></tr><tr><td><img alt="" border="0" height="20" src="designs/design345/color4/images/i.gif" width="1"/></td></tr></table></td><td class="bgRight" rowspan="2"><img alt="" border="0" height="24" src="designs/design345/color4/images/bgRight.gif" width="19"/></td></tr><tr><td class="menubg" valign="bottom"><img alt="" border="0" height="59" src="designs/design345/color4/images/imgKeyvBottom.gif" width="193"/></td><td class="bgContent" valign="bottom"><p class="bot"><a href="#top">Top</a></p><p class="botimg"><img alt="" border="0" height="13" src="designs/design345/color4/images/imgCopyrightTop.gif" width="561"/></p><p class="bot"></p><img alt="" border="0" height="59" src="designs/design345/color4/images/imgContentBottom.gif" width="561"/></td></tr></table></div></body></html>