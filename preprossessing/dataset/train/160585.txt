<!DOCTYPE html>
<html dir="ltr" lang="nb-NO">
<head>
<link href="https://askoy.kommune.no/" rel="canonical"/>
<base href="https://askoy.kommune.no/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Forsiden til Askøy kommunes nettportal" name="description"/>
<title>Askøy kommune</title>
<link href="https://askoy.kommune.no/templates/redcomponent/favicons/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="https://askoy.kommune.no/templates/redcomponent/favicons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="https://askoy.kommune.no/templates/redcomponent/favicons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/media/plg_jchoptimize/cache/css/24500119393974c024f71cbf3d881ff5_0.css" rel="stylesheet" type="text/css"/>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"abcfb27609623a4b7d5551e95af80eab","system.paths":{"root":"","base":""}}</script>
<script src="/media/plg_jchoptimize/cache/js/ae89916f3e1042035e76bd2b879aeccd_0.js" type="application/javascript"></script>
<script type="text/javascript">jQuery(window).on('load',function(){new JCaption('img.caption');});jQuery(function($){$('.categories-list').find('[id^=category-btn-]').each(function(index,btn){var btn=$(btn);btn.on('click',function(){btn.find('span').toggleClass('icon-plus');btn.find('span').toggleClass('icon-minus');});});});jQuery(function($){initTooltips();$("body").on("subform-row-add",initTooltips);function initTooltips(event,container){container=container||document;$(container).find(".hasTooltip").tooltip({"html":true,"container":"body"});}});jQuery(document).ready(function(){WfMediabox.init({"base":"\/","theme":"light","width":"","height":"","lightbox":0,"shadowbox":0,"icons":1,"overlay":0,"overlay_opacity":0,"overlay_color":"","transition_speed":300,"close":2,"scrolling":"fixed","labels":{"close":"Close","next":"Next","previous":"Previous","cancel":"Cancel","numbers":"{{numbers}}","numbers_count":"{{current}} of {{total}}","download":"Download"}});});var emailProtector=emailProtector||{};emailProtector.addCloakedMailto=function(g,l){var h=document.querySelectorAll("."+g);for(i=0;i<h.length;i++){var b=h[i],k=b.getElementsByTagName("span"),e="",c="";b.className=b.className.replace(" "+g,"");for(var f=0;f<k.length;f++)for(var d=k[f].attributes,a=0;a<d.length;a++)0===d[a].nodeName.toLowerCase().indexOf("data-ep-a8c2d")&&(e+=d[a].value),0===d[a].nodeName.toLowerCase().indexOf("data-ep-bb6b4")&&(c=d[a].value+c);if(!c)break;b.innerHTML=e+c;if(!l)break;b.parentNode.href="mailto:"+e+c}};GOOGLE_MAPS_API_KEY="AIzaSyCrOdUxxSp5y2u8xJE6HVp0XQJPqZxbIIA";</script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-124368629-1"></script>
<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}
gtag('js',new Date());gtag('config','UA-124368629-1');</script>
<!-- Google sitename markup-->
<script type="application/ld+json">{"@context":"http://schema.org","@type":"WebSite","name":"askoy.kommune.no","url":"https://askoy.kommune.no/"}</script>
<!-- End of Google sitename markup-->
</head>
<body class="is_unknown v_unknown on_unknown b_jdefault h_jdefault id_101 home rev_1.4.1.1"><div class="dj-offcanvas-wrapper"><div class="dj-offcanvas-pusher"><div class="dj-offcanvas-pusher-in">
<header id="header">
<a class="external-link" href="#main" id="content-link-top" tabindex="1">Til innhold</a>
<div class="container relative">
<div class="wrapper-menu">
<nav class="navbar navbar-default" id="menu"><a class="navbar-brand" href="https://askoy.kommune.no/"><img alt="Hjem - Askøy kommune" src="https://askoy.kommune.no/templates/redcomponent/images//logo.png" title="Hjem - Askøy kommune"/></a><div class="navbar-header">
<button class="navbar-toggle" data-target="#nav-menu" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="navbar-module">
<ul class="nav menu navbar-nav">
<li class="item-1465"><a href="/kontakt-oss">Kontakt</a></li></ul>
</div>
</div><div class="collapse navbar-collapse" id="nav-menu">
<ul class="nav menu navbar-nav" role="menu">
<li class="item-643"><a href="/nettstedskart" title="Gå til nettstedskart">Nettstedskart</a></li><li class="item-132"><a href="https://candidate.webcruiter.com/nb-no/Home/companyadverts?&amp;link_source_ID=0&amp;companylock=5323#search" target="_blank" title="Gå til ledige stillinger">Ledige stillinger</a></li><li class="item-133"><a href="/kontakt-oss" title="Gå til kontakt oss side">Kontakt</a></li><li class="item-1002"><a href="https://minside.kommune.no/" target="_blank" title="Gå til minside">MinSide</a></li><li class="item-1104"><a href="https://minarbeidsplan.askoy.kommune.no/minarbeidsplan/auth">Min arbeidsplan</a></li>
</ul>
</div></nav></div>
</div>
</header>
<div id="featured">
<div class="moduletable">
<div class="bannerimage" style="background-image:url(https://askoy.kommune.no/images/gfx/Header_vinter.jpg)"></div>
</div>
<div class="moduletable">
<div class="container"><div class="row"><div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2"><div class="google-search">
<h1>Velkommen til Askøy kommune</h1>
<div class="custom">
<script async="" src="https://cse.google.com/cse.js?cx=a68a5af71e64271b8"></script>
<div class="gcse-search"> </div>
<p><button class="recognition-btn" onclick="startDictation();"><i aria-hidden="true" class="icon-microphone"></i></button></p>
<script type="text/javascript">var recBtns=document.querySelectorAll('button.recognition-btn');if(!window.hasOwnProperty('webkitSpeechRecognition')){for(var i=0;i<recBtns.length;i++){recBtns[i].style['display']='none';}}
function startDictation(){if(window.hasOwnProperty('webkitSpeechRecognition')){document.querySelectorAll('button.recognition-btn')[0].classList.add('active');var recognition=new webkitSpeechRecognition();recognition.continuous=false;recognition.interimResults=false;recognition.lang="nb-NO";recognition.start();recognition.onresult=function(e){document.querySelectorAll('button.recognition-btn')[0].classList.remove('active');document.querySelectorAll('#featured input.gsc-input')[0].value=e.results[0][0].transcript;recognition.stop();simulateClick(document.querySelectorAll('#featured button.gsc-search-button')[0]);};recognition.onerror=function(e){document.querySelectorAll('button.recognition-btn')[0].classList.remove('active');recognition.stop();}}}
function simulateClick(elem){var evt=new MouseEvent('click',{bubbles:true,cancelable:true,view:window});var canceled=!elem.dispatchEvent(evt);}</script></div>
</div></div></div></div> </div>
</div>
<div class="container">
<div class="row" id="main-content">
<section aria-label="content" class="col-md-12" id="main" title="content">
<div id="above-content">
<div class="moduletable full-width jm-blank">
<div class="custom full-width jm-blank">
<a class="box-link" href="/tjenester/koronavirus-rad-og-informasjon/">
<div class="box clearfix"><img alt="Alt om korona på Askøy" src="/images/koronavirusrod-04-small.png" style="float: left;"/>
<h2 class="page-header item-title">
			Alt om korona på Askøy </h2>
<div class="category-desc">
<p>Vaksineinformasjon | Pressemeldinger | Tiltak | Testing | Informasjon til innbyggere og ansatte</p>
</div>
</div>
</a></div>
</div>
</div>
<div id="system-message-container">
</div>
<div class="all-categories">
<div class="first">
<div id="category-104">
<div class="row">
<div class="col-md-3 col-sm-6">
<a class="box-link" href="/barnehage/">
<div class="box">
<h2 class="page-header item-title">
								Barnehage															</h2>
<div class="category-desc">
<p>Søknadsfrist 1. mars | Våre barnehager | Prislister og betalingsordninger</p> </div>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/skole-og-utdanning/">
<div class="box">
<h2 class="page-header item-title">
								Skole og utdanning															</h2>
<div class="category-desc">
<p>Skoleruten | Skolehelsetjenesten | SFO | Våre skoler | Starte eller bytte skole | Eksamen</p> </div>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div> <div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/plan-bygg-og-eiendom/">
<div class="box">
<h2 class="page-header item-title">
								Plan, bygg og eiendom															</h2>
<div class="category-desc">
<p>Byggesøknad | Deling | Oppmåling | Planinnsyn | Kart | Reguleringsplan | Forhåndskonferanse</p> </div>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/teknikk-og-miljo/">
<div class="box">
<h2 class="page-header item-title">
								Teknisk															</h2>
<div class="category-desc">
<p>Brannvern, ildsted og feiing | Miljø og renovasjon | Vann og avløp | Vei og transport | Meld fra om feil på vei og veilys</p> </div>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div><div class="clearfix"></div> <div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/helse-og-omsorgstjenester/">
<div class="box">
<h2 class="page-header item-title">
								Helse- og omsorgstjenester															</h2>
<div class="category-desc">
<p>Barnevern | Helsehjelp | Psykisk helse | Vaksine | Nedsatt funksjonsevne | Fysio-/ergoterapi | Hjemmetjenester | Institusjon</p> </div>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/kultur-idrett-og-fritid/">
<div class="box">
<h2 class="page-header item-title">
								Kultur, idrett og fritid															</h2>
<div class="category-desc">
<p>Bibliotek | Kulturskole | Museer og kulturminner | Idrettsanlegg og svømmehaller | Tilskudd | Prislister</p> </div>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div> <div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/bolig-og-sosiale-tjenester/">
<div class="box">
<h2 class="page-header item-title">
								Bolig og sosiale tjenester															</h2>
<div class="category-desc">
<p>Ditt NAV kontor | Sosiale tjenester | Bolig | Rettshjelp | Flyktninger og inkludering | Borgerlig vigsel | Familier med lav inntekt</p> </div>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="box-link" href="/tjenester/skatt-og-naring/">
<div class="box">
<h2 class="page-header item-title">
								Skatt og næring															</h2>
<div class="category-desc">
<p>Landbruk | Skogbruk | Servering, salg og skjenkebevilling | Etablerertjenester | Starte egen bedrift</p> </div>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div><div class="clearfix"></div> </div>
</div>
</div>
<div>
<div class="page-header">
<h2 class="item-title margin-top">
						Politikk og organisasjon											</h2>
</div>
<div class="green-list" id="category-47">
<div class="row">
<div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk/">
<div class="box">
<h2 class="page-header item-title">
								Politikk															</h2>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/postlister-og-innsyn/">
<div class="box">
<h2 class="page-header item-title">
								Postlister og innsyn															</h2>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div> <div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/kartportaler/">
<div class="box">
<h2 class="page-header item-title">
								Kartportaler															</h2>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/avdelinger-og-foretak/">
<div class="box">
<h2 class="page-header item-title">
								Avdelinger og foretak															</h2>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div><div class="clearfix"></div> <div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/for-ansatte/">
<div class="box">
<h2 class="page-header item-title">
								For ansatte															</h2>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/trafikk-og-veitjenester/">
<div class="box">
<h2 class="page-header item-title">
								Trafikk og veitjenester															</h2>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div> <div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/vann-bygge-og-veiprosjekter/">
<div class="box">
<h2 class="page-header item-title">
								Vann-, bygge- og veiprosjekter															</h2>
</div>
</a>
</div>
<div class="col-md-3 col-sm-6">
<a class="special-box-link" href="/politikk-og-organisasjon/tilskuddsordninger/">
<div class="box">
<h2 class="page-header item-title">
								Tilskuddsordninger															</h2>
</div>
</a>
</div>
<div class="clearfix visible-sm"></div><div class="clearfix"></div> <div class="clearfix"></div>
</div>
</div>
</div>
<div class="last">
</div>
</div>
</section>
</div>
</div>
<div id="below-content">
<div class="container">
<!--<div class="page-header">
          <h2>Nyheter og aktiviteter</h2>
        </div>-->
<div class="row"> <div class="moduletable span4">
<h3>Nyhet</h3>
<div class="allmode-box allmode-newsbreak-dj">
<div class="row">
<div class="allmode-topbox col-md-12">
<div class="article-with-image text-left">
<div class="row">
<div class="col-lg-6">
<div class="big-image"><a href="/nyheter/om-soknadsrunde-for-kompensasjon-av-merverdiavgift-ved-bygging-av-idrettsanlegg-2021"><img alt="Momskompensasjon idrettsanlegg" height="180" src="/images/raxo_thumbs/amp/tb-w246-h180-crop-int-d2f9379d418465cc56a5ec1909d1f49e.jpg" title="Om søknadsrunde for kompensasjon av merverdiavgift ved bygging av idrettsanlegg - 2021" width="246"/></a></div>
</div>
<div class="col-lg-6">
<div class="intro-text">
<h3><a href="/nyheter/om-soknadsrunde-for-kompensasjon-av-merverdiavgift-ved-bygging-av-idrettsanlegg-2021">Om søknadsrunde for kompensasjon av merverdiavgift ved bygging av idrettsanlegg - 2021</a></h3>
<div class="allmode-text">Fra og med 15. januar 2021 åpnes det for innlevering av søknader om…</div>
</div>
</div>
</div>
</div>
<div class="article-with-image text-left">
<div class="row">
<div class="col-lg-6">
<div class="big-image"><a href="/nyheter/sivilforsvaret-tester-tyfonene-13-januar"><img alt="Sivilforsvaret tester tyfonanlegget i januar og juni hvert år." height="180" src="/images/raxo_thumbs/amp/tb-w246-h180-crop-int-983375eed171f334b55eef4aaebe23ce.jpg" title="Sivilforsvaret tester tyfonene 13. januar" width="246"/></a></div>
</div>
<div class="col-lg-6">
<div class="intro-text">
<h3><a href="/nyheter/sivilforsvaret-tester-tyfonene-13-januar">Sivilforsvaret tester tyfonene 13. januar</a></h3>
<div class="allmode-text">Onsdag 13. januar klokken 12.00 tester Sivilforsvaret signalet «Viktig melding…</div>
</div>
</div>
</div>
</div>
<div class="article-with-image text-left">
<div class="row">
<div class="col-lg-6">
<div class="big-image"><a href="/nyheter/problemer-med-vanntilforsel-vardane-omradet"><img alt="Vannkran lite" height="180" src="/images/raxo_thumbs/amp/tb-w246-h180-crop-int-268401f6fe21f269b671885c01e49a90.jpg" title="Problemer med vanntilførsel Erdal" width="246"/></a></div>
</div>
<div class="col-lg-6">
<div class="intro-text">
<h3><a href="/nyheter/problemer-med-vanntilforsel-vardane-omradet">Problemer med vanntilførsel Erdal</a></h3>
<div class="allmode-text">Tirsdag 12. januar kl. 17:00. Som tidligere i dag er det oppstått problemer med…</div>
</div>
</div>
</div>
</div>
</div>
<div class="allmode-itemsbox col-md-12">
</div>
</div>
<div class="allmode-showall"><a href="/nyheter/">Flere nyheter...</a></div></div>
</div>
<div class="moduletable span4">
<h3>Kunngjøring </h3>
<div class="allmode-box allmode-newsbreak-dj">
<div class="row">
<div class="allmode-topbox col-md-12">
<div class="article-with-image text-left">
<div class="row">
<div class="col-lg-6">
<div class="big-image"><a href="/postlister-og-innsyn/kunngjoringer/innspill-til-kommuneplanens-arealdel"><img alt="Kartutsnitt kommuneplanens arealdel" height="180" src="/images/raxo_thumbs/amp/tb-w246-h180-crop-int-8cb365e4518074d86a75bb470b5d435d.jpg" title="Innspill til kommuneplanens arealdel" width="246"/></a></div>
</div>
<div class="col-lg-6">
<div class="intro-text">
<h3><a href="/postlister-og-innsyn/kunngjoringer/innspill-til-kommuneplanens-arealdel">Innspill til kommuneplanens arealdel</a></h3>
<div class="allmode-text">Ny innspillsrunde til kommuneplanens arealdel</div>
</div>
</div>
</div>
</div>
<div class="article-with-image text-left">
<div class="row">
<div class="col-lg-6">
<div class="big-image"><a href="/postlister-og-innsyn/kunngjoringer/hjemmesykepleien-innforer-telefontider"><img alt="mobiltelefon" height="180" src="/images/raxo_thumbs/amp/tb-w246-h180-crop-int-1629a13c369933dff9142a7415e1e689.jpg" title="Slik kontakter du hjemmetjenesten" width="246"/></a></div>
</div>
<div class="col-lg-6">
<div class="intro-text">
<h3><a href="/postlister-og-innsyn/kunngjoringer/hjemmesykepleien-innforer-telefontider">Slik kontakter du hjemmetjenesten</a></h3>
<div class="allmode-text">Hjemmetjenesten ønsker å ha god dialog med tjenestemottakere og pårørende For å…</div>
</div>
</div>
</div>
</div>
</div>
<div class="allmode-itemsbox col-md-12">
</div>
</div>
<div class="allmode-showall"><a href="/postlister-og-innsyn/kunngjoringer/">Flere kunngjøringer...</a></div></div>
</div>
<div class="moduletable span4">
<h3>Aktiviteter</h3>
<div class="mod-dpcalendar-upcoming mod-dpcalendar-upcoming-default mod-dpcalendar-upcoming-442 dp-locations" data-popup="1">
<div class="mod-dpcalendar-upcoming-default__events">
<div class="mod-dpcalendar-upcoming-default__event dp-event dp-event_future">
<div class="dp-flatcalendar">
<span class="dp-flatcalendar__day">18</span>
<span class="dp-flatcalendar__month" style="background-color: #204080;box-shadow: 0 2px 0 #204080;color: #ffffff;">
		jan	</span>
</div>
<div class="mod-dpcalendar-upcoming-default__information">
<a class="dp-event-url dp-link" href="/politisk-motekalender?id=8545&amp;view=event">Eldrerådet</a>
<div class="mod-dpcalendar-upcoming-default__date">
<span class="dp-icon dp-icon_clock"><svg id="dp-icon-clock" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><title>Dato</title><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z"></path></svg></span>
<span class="dp-date dp-time">
<span class="dp-date__start">18.01.2021</span>
<span class="dp-time__start">09:00</span>
<span class="dp-time__separator">-</span>
<span class="dp-time__end">11:00</span>
</span>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__description">
</div>
<div itemscope="" itemtype="http://schema.org/Event">
<meta content="Eldrerådet" itemprop="name"/>
<meta content="2021-01-18T08:00:00+00:00" itemprop="startDate"/>
<meta content="2021-01-18T10:00:00+00:00" itemprop="endDate"/>
<meta content="http://schema.org/EventScheduled" itemprop="eventStatus"/>
<meta content="https://askoy.kommune.no/politisk-motekalender?id=8545&amp;view=event" itemprop="url"/>
<meta content="Siri Oddny Bergensen-Larsen" itemprop="performer"/>
<div itemprop="organizer" itemscope="" itemtype="http://schema.org/Organization">
<meta content="Siri Oddny Bergensen-Larsen" itemprop="name"/>
<meta content="https://sky01.askoy.kommune.no/url/6z43if9fhfatyzea " itemprop="url"/>
</div>
<meta content="" itemprop="description"/>
<meta content="http://schema.org/OnlineEventAttendanceMode" itemprop="eventAttendanceMode"/>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__event dp-event dp-event_future">
<div class="dp-flatcalendar">
<span class="dp-flatcalendar__day">18</span>
<span class="dp-flatcalendar__month" style="background-color: #204080;box-shadow: 0 2px 0 #204080;color: #ffffff;">
		jan	</span>
</div>
<div class="mod-dpcalendar-upcoming-default__information">
<a class="dp-event-url dp-link" href="/politisk-motekalender?id=8546&amp;view=event">Råd for personer med funksjonsnedsettelse</a>
<div class="mod-dpcalendar-upcoming-default__date">
<span class="dp-icon dp-icon_clock"><svg><use href="#dp-icon-clock"></use></svg></span>
<span class="dp-date dp-time">
<span class="dp-date__start">18.01.2021</span>
<span class="dp-time__start">14:30</span>
<span class="dp-time__separator">-</span>
<span class="dp-time__end">16:30</span>
</span>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__description">
</div>
<div itemscope="" itemtype="http://schema.org/Event">
<meta content="Råd for personer med funksjonsnedsettelse" itemprop="name"/>
<meta content="2021-01-18T13:30:00+00:00" itemprop="startDate"/>
<meta content="2021-01-18T15:30:00+00:00" itemprop="endDate"/>
<meta content="http://schema.org/EventScheduled" itemprop="eventStatus"/>
<meta content="https://askoy.kommune.no/politisk-motekalender?id=8546&amp;view=event" itemprop="url"/>
<meta content="Siri Oddny Bergensen-Larsen" itemprop="performer"/>
<div itemprop="organizer" itemscope="" itemtype="http://schema.org/Organization">
<meta content="Siri Oddny Bergensen-Larsen" itemprop="name"/>
<meta content="https://sky01.askoy.kommune.no/url/d8kjij9wswn3qxwp" itemprop="url"/>
</div>
<meta content="" itemprop="description"/>
<meta content="http://schema.org/OnlineEventAttendanceMode" itemprop="eventAttendanceMode"/>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__event dp-event dp-event_future">
<div class="dp-flatcalendar">
<span class="dp-flatcalendar__day">20</span>
<span class="dp-flatcalendar__month" style="background-color: #204080;box-shadow: 0 2px 0 #204080;color: #ffffff;">
		jan	</span>
</div>
<div class="mod-dpcalendar-upcoming-default__information">
<a class="dp-event-url dp-link" href="/politisk-motekalender?id=8547&amp;view=event">Utvalg for oppvekst og levekår</a>
<div class="mod-dpcalendar-upcoming-default__date">
<span class="dp-icon dp-icon_clock"><svg><use href="#dp-icon-clock"></use></svg></span>
<span class="dp-date dp-time">
<span class="dp-date__start">20.01.2021</span>
<span class="dp-time__start">09:00</span>
<span class="dp-time__separator">-</span>
<span class="dp-time__end">16:00</span>
</span>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__description">
</div>
<div itemscope="" itemtype="http://schema.org/Event">
<meta content="Utvalg for oppvekst og levekår" itemprop="name"/>
<meta content="2021-01-20T08:00:00+00:00" itemprop="startDate"/>
<meta content="2021-01-20T15:00:00+00:00" itemprop="endDate"/>
<meta content="http://schema.org/EventScheduled" itemprop="eventStatus"/>
<meta content="https://askoy.kommune.no/politisk-motekalender?id=8547&amp;view=event" itemprop="url"/>
<meta content="Siri Oddny Bergensen-Larsen" itemprop="performer"/>
<div itemprop="organizer" itemscope="" itemtype="http://schema.org/Organization">
<meta content="Siri Oddny Bergensen-Larsen" itemprop="name"/>
<meta content="https://sky01.askoy.kommune.no/url/68fb5ib9ebp3n4dr" itemprop="url"/>
</div>
<meta content="" itemprop="description"/>
<meta content="http://schema.org/OnlineEventAttendanceMode" itemprop="eventAttendanceMode"/>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__event dp-event dp-event_future">
<div class="dp-flatcalendar">
<span class="dp-flatcalendar__day">21</span>
<span class="dp-flatcalendar__month" style="background-color: #204080;box-shadow: 0 2px 0 #204080;color: #ffffff;">
		jan	</span>
</div>
<div class="mod-dpcalendar-upcoming-default__information">
<a class="dp-event-url dp-link" href="/politisk-motekalender?id=8548&amp;view=event">Utvalg for teknikk og miljø</a>
<div class="mod-dpcalendar-upcoming-default__date">
<span class="dp-icon dp-icon_clock"><svg><use href="#dp-icon-clock"></use></svg></span>
<span class="dp-date dp-time">
<span class="dp-date__start">21.01.2021</span>
<span class="dp-time__start">09:00</span>
<span class="dp-time__separator">-</span>
<span class="dp-time__end">16:00</span>
</span>
</div>
</div>
<div class="mod-dpcalendar-upcoming-default__description">
</div>
<div itemscope="" itemtype="http://schema.org/Event">
<meta content="Utvalg for teknikk og miljø" itemprop="name"/>
<meta content="2021-01-21T08:00:00+00:00" itemprop="startDate"/>
<meta content="2021-01-21T15:00:00+00:00" itemprop="endDate"/>
<meta content="http://schema.org/EventScheduled" itemprop="eventStatus"/>
<meta content="https://askoy.kommune.no/politisk-motekalender?id=8548&amp;view=event" itemprop="url"/>
<meta content="Siri Oddny Bergensen-Larsen" itemprop="performer"/>
<div itemprop="organizer" itemscope="" itemtype="http://schema.org/Organization">
<meta content="Siri Oddny Bergensen-Larsen" itemprop="name"/>
<meta content="https://askoy.kommune.no/" itemprop="url"/>
</div>
<meta content="" itemprop="description"/>
<meta content="http://schema.org/OnlineEventAttendanceMode" itemprop="eventAttendanceMode"/>
</div>
</div>
</div>
</div>
<div class="allmode-showall"><a href="/dpcalendar?view=calendar">Flere aktiviteter...</a></div>
<div class="color-box orange">
<p><a href="https://detskjer.askoy.no/" target="blank"><img alt="Ka skjer på Askøy?" src="https://detskjer.askoy.no/images/askoy-logo-footer.svg"/></a>Lyst til å finne på noe? <a href="https://detskjer.askoy.no/" target="blank">
Sjekk ut hva som skjer på øyen!</a></p>
</div> </div>
</div>
</div>
</div>
<div class="wrapper-footer ">
<footer class="footer blue" id="footer">
<div class="container">
<div class="row">
<div class="moduletable col-md-4">
<h3>Kontakt oss</h3>
<div class="custom">
<ul>
<li>
<h4>Direktenumre avdelinger</h4>
<a dir="ltr" href="/stab/ovrige-stabsfunksjoner/kontakt-oss" lang="NO" target="_self" title="Oversikt over direkte telefonnumre til avdelinger og til kommunale vakttelefoner">Direktenumre og vakttelefoner</a></li>
<li>
<h4>E-post</h4>
<br/><a href="javascript:/* This email address is being protected from spambots.*/" title="Send epost til kundemottak">Send e-post<span class="cloaked_email ep_ef10ce53" style="display:none;"><span data-ep-a8c2d="postm" data-ep-bb6b4="no"><span data-ep-a8c2d="ottak" data-ep-bb6b4="mune."><span data-ep-a8c2d="@asko" data-ep-bb6b4="y.kom"></span></span></span></span></a><script type="text/javascript">emailProtector.addCloakedMailto("ep_ef10ce53",1);</script></li>
<li>
<h4>Besøksadresse</h4>
Kundetorget, Rådhuset: ved hovedinngangen<br/><br/></li>
<li>Kundetorgets åpningstider er 08:00 - 10:00 og 12:00 - 14:00 på hverdager.</li>
<li>
<h4>Postadresse</h4>
<br/>Askøy kommune, Klampavikvegen 1, 5300 Kleppestø</li>
<li>
<h4>Organisasjonsnummer</h4>
<br/>964 338 442</li>
</ul></div>
</div>
<div class="moduletable col-md-4">
<h3>Vakttelefoner</h3>
<div class="custom">
<ul>
<li>
<h4>Legevakt</h4>
<a href="tel:116117" title="Ring legevakt ved akutte hendelser">116 117</a></li>
<li>
<h4>Vakttelefon Askøy brann og redning</h4>
<a href="tel:+4793013100" title="Ring Brannvesenet">930 13 100</a></li>
<li>Viltpåkjørsler<br/><a dir="ltr" href="tel:%20+4702800" lang="NO" target="_self" title="Ring viltvakten hos politiet">02800</a></li>
<li>
<h4>Veterinærvakt utenom normalarbeidstid</h4>
<a href="tel:+4782090060">820 900 60</a></li>
<li>
<h4>Barnevernvakten</h4>
<a href="tel:+4740919480" title="Ring barnevernvakten">409 19 480</a></li>
<li>
<h4>Vakttelefon vei og vann utenom normalarbeidstid</h4>
<a href="tel:+4753000460">53 00 04 60</a></li>
<li>
<h4>Kommunal varslingstjeneste, sjekk dine data!</h4>
<a href="https://www.servicevarsling.no" target="_self" title="Har du sjekket at vi har korrekte data på deg slik at du mottar varsling?">Servicevarsling.no</a></li>
</ul></div>
</div>
<div class="moduletable col-md-4">
<h3>Nyttige lenker</h3>
<div class="custom">
<!-- START: Modules Anywhere --><ul class="nav menu navbar-nav">
<li class="item-1536"><a href="/avdelinger-og-foretak/hjelpeartikler-organisasjon/informasjonskapsler">Informasjonskapsler</a></li><li class="item-1537"><a href="/hjelpeartikler-organisasjon/generell-informasjon/om-personvern">Personvernerklæring</a></li><li class="item-1538"><a href="/avdelinger-og-foretak/hjelpeartikler-organisasjon/innsynsrett">Innsynsrett</a></li><li class="item-1539"><a href="/stab/hjelpeartikler-stab/sende-faktura-til-oss">Sende faktura til oss</a></li><li class="item-1540"><a href="https://minarbeidsplan.askoy.kommune.no/minarbeidsplan/auth">For ansatte</a></li><li class="item-1541"><a href="mailto:nettredaktor@askoy.kommune.no">Send e-post til webansvarlig</a></li><li class="item-1542"><a href="/log-in/">Logg på nettstedet</a></li></ul>
<!-- END: Modules Anywhere -->
<ul>
<li><strong>Følg oss i sosiale medier</strong><br/><a class="social vimeo" href="https://vimeo.com/askoykommune" rel="noopener" target="_blank">
<svg viewbox="0 0 508.52 508.52" xmlns="http://www.w3.org/2000/svg">
<defs></defs>
<title>vimeo</title>
<g data-name="vimeo-svg" id="vimeo-svg">
<g data-name="Capa 1" id="Capa_1">
<path class="cls-1" d="M413.17,0H95.35A95.34,95.34,0,0,0,0,95.35V413.17a95.34,95.34,0,0,0,95.35,95.35H413.17a95.34,95.34,0,0,0,95.35-95.35V95.35A95.34,95.34,0,0,0,413.17,0Zm-7.88,185.07C388.22,281.08,293,362.45,264.3,381s-54.79-7.44-64.26-27.14c-10.84-22.38-43.35-144-51.87-154.12s-34.07,10.08-34.07,10.08l-12.4-16.24s51.9-62,91.38-69.73c41.86-8.24,41.79,64.29,51.87,104.53,9.76,38.93,16.27,61.18,24.76,61.18s24.82-21.68,42.62-54.95-.77-62.74-35.63-41.83C290.68,109.17,422.33,89.06,405.29,185.07Z"></path>
</g>
</g>
</svg>
</a><a class="social facebook" href="https://www.facebook.com/AskoyKommune/" rel="noopener" target="_blank">
<svg viewbox="0 0 508.78 508.78" xmlns="http://www.w3.org/2000/svg">
<defs></defs>
<title>facebook</title>
<g data-name="facebook-svg" id="facebook-svg">
<g data-name="Capa 2" id="Capa_2">
<path class="cls-1" d="M413.38,0H95.4A95.4,95.4,0,0,0,0,95.4v318a95.4,95.4,0,0,0,95.4,95.4h318a95.4,95.4,0,0,0,95.4-95.4V95.4A95.4,95.4,0,0,0,413.38,0ZM320.15,254.36l-41.69,0,0,152.63H221.22V254.39H183.07v-52.6l38.15,0-.06-31c0-42.93,11.64-69,62.17-69h42.1v52.62H299.1c-19.68,0-20.64,7.35-20.64,21.05l-.06,26.33h47.31Z"></path>
</g>
</g>
</svg>
</a><a class="social youtube" href="https://www.youtube.com/channel/UCSliIP5grWPQxur-p2dFUcg" rel="noopener" target="_blank">
<svg viewbox="0 0 508.52 508.52" xmlns="http://www.w3.org/2000/svg">
<defs></defs>
<title>youtube</title>
<g data-name="youtube-svg" id="youtube-svg">
<g data-name="Capa 3" id="Capa_3">
<path class="cls-1" d="M413.17,0H95.35A95.34,95.34,0,0,0,0,95.35V413.17a95.34,95.34,0,0,0,95.35,95.35H413.17a95.34,95.34,0,0,0,95.35-95.35V95.35A95.34,95.34,0,0,0,413.17,0Zm-2.7,311.18c-1.71,21.17-17.8,48.18-40.27,52.09-72,5.6-157.29,4.9-231.85,0-23.26-2.92-38.55-30.95-40.27-52.09-3.62-44.43-3.62-69.73,0-114.16,1.72-21.14,17.39-49,40.27-51.55,73.7-6.2,159.49-4.86,231.85,0,25.87.95,38.56,27.62,40.27,48.78C414,238.69,414,266.75,410.47,311.18Z"></path>
<polygon class="cls-1" points="222.48 317.82 317.82 254.26 222.48 190.69 222.48 317.82"></polygon>
</g>
</g>
</svg>
</a></li>
<li><strong>RSS Feed</strong><br/> <a class="rss-icon" href="/postlister-og-innsyn/kunngjoringer/feed/rss/" target="_blank">
<svg height="438.536px" id="Capa_4" style="enable-background: new 0 0 438.536 438.536;" version="1.1" viewbox="0 0 438.536 438.536" width="438.536px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g fill="#ffffff">
<path d="M414.41,24.123C398.333,8.042,378.963,0,356.315,0H82.228C59.58,0,40.21,8.042,24.126,24.123C8.045,40.207,0.003,59.576,0.003,82.225v274.084c0,22.647,8.042,42.018,24.123,58.102c16.084,16.084,35.454,24.126,58.102,24.126h274.084c22.648,0,42.018-8.042,58.095-24.126c16.084-16.084,24.126-35.454,24.126-58.102V82.225C438.532,59.576,430.49,40.204,414.41,24.123z M135.475,354.741c-7.142,7.132-15.752,10.704-25.841,10.704c-10.085,0-18.699-3.572-25.837-10.704c-7.137-7.139-10.708-15.756-10.708-25.844c0-10.082,3.571-18.699,10.708-25.838c7.139-7.139,15.752-10.704,25.837-10.704c10.089,0,18.702,3.565,25.841,10.704c7.135,7.139,10.706,15.756,10.706,25.838C146.181,338.985,142.613,347.603,135.475,354.741z M243.818,362.585c-1.902,1.902-4.093,2.847-6.57,2.847h-36.54c-2.475,0-4.567-0.801-6.28-2.423c-1.714-1.615-2.667-3.661-2.856-6.139c-2.093-29.308-13.608-54.434-34.546-75.377c-20.937-20.93-46.059-32.448-75.374-34.543c-2.474-0.185-4.521-1.137-6.136-2.848c-1.619-1.711-2.428-3.806-2.428-6.283v-36.543c0-2.475,0.953-4.665,2.855-6.567c1.903-1.903,4.187-2.758,6.854-2.57c44.158,2.474,81.843,19.323,113.058,50.537c31.217,31.221,48.064,68.898,50.535,113.062C246.584,358.403,245.722,360.682,243.818,362.585z M353.451,362.585c-1.903,1.902-4.087,2.854-6.563,2.854h-36.546c-2.474,0-4.568-0.856-6.279-2.567s-2.663-3.806-2.854-6.283c-1.334-38.827-11.946-74.802-31.833-107.921c-19.89-33.12-46.394-59.623-79.514-79.509c-33.12-19.892-69.09-30.502-107.921-31.833c-2.474-0.19-4.567-1.143-6.28-2.854c-1.711-1.713-2.568-3.809-2.568-6.283V91.644c0-2.474,0.953-4.661,2.855-6.567c1.713-1.903,3.9-2.758,6.567-2.565c29.309,0.95,57.624,6.276,84.936,15.987c27.312,9.704,52.056,22.98,74.235,39.825c22.176,16.842,41.686,36.354,58.525,58.529c16.841,22.175,30.122,46.921,39.831,74.23c9.702,27.315,15.037,55.631,15.984,84.938C356.407,358.69,355.556,360.874,353.451,362.585z"></path>
</g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
</svg>Kunngjøringer
		</a> <a class="rss-icon" href="/nyheter/feed/rss/" target="_blank">
<svg height="438.536px" id="Capa_5" style="enable-background: new 0 0 438.536 438.536;" version="1.1" viewbox="0 0 438.536 438.536" width="438.536px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g fill="#ffffff">
<path d="M414.41,24.123C398.333,8.042,378.963,0,356.315,0H82.228C59.58,0,40.21,8.042,24.126,24.123C8.045,40.207,0.003,59.576,0.003,82.225v274.084c0,22.647,8.042,42.018,24.123,58.102c16.084,16.084,35.454,24.126,58.102,24.126h274.084c22.648,0,42.018-8.042,58.095-24.126c16.084-16.084,24.126-35.454,24.126-58.102V82.225C438.532,59.576,430.49,40.204,414.41,24.123z M135.475,354.741c-7.142,7.132-15.752,10.704-25.841,10.704c-10.085,0-18.699-3.572-25.837-10.704c-7.137-7.139-10.708-15.756-10.708-25.844c0-10.082,3.571-18.699,10.708-25.838c7.139-7.139,15.752-10.704,25.837-10.704c10.089,0,18.702,3.565,25.841,10.704c7.135,7.139,10.706,15.756,10.706,25.838C146.181,338.985,142.613,347.603,135.475,354.741z M243.818,362.585c-1.902,1.902-4.093,2.847-6.57,2.847h-36.54c-2.475,0-4.567-0.801-6.28-2.423c-1.714-1.615-2.667-3.661-2.856-6.139c-2.093-29.308-13.608-54.434-34.546-75.377c-20.937-20.93-46.059-32.448-75.374-34.543c-2.474-0.185-4.521-1.137-6.136-2.848c-1.619-1.711-2.428-3.806-2.428-6.283v-36.543c0-2.475,0.953-4.665,2.855-6.567c1.903-1.903,4.187-2.758,6.854-2.57c44.158,2.474,81.843,19.323,113.058,50.537c31.217,31.221,48.064,68.898,50.535,113.062C246.584,358.403,245.722,360.682,243.818,362.585z M353.451,362.585c-1.903,1.902-4.087,2.854-6.563,2.854h-36.546c-2.474,0-4.568-0.856-6.279-2.567s-2.663-3.806-2.854-6.283c-1.334-38.827-11.946-74.802-31.833-107.921c-19.89-33.12-46.394-59.623-79.514-79.509c-33.12-19.892-69.09-30.502-107.921-31.833c-2.474-0.19-4.567-1.143-6.28-2.854c-1.711-1.713-2.568-3.809-2.568-6.283V91.644c0-2.474,0.953-4.661,2.855-6.567c1.713-1.903,3.9-2.758,6.567-2.565c29.309,0.95,57.624,6.276,84.936,15.987c27.312,9.704,52.056,22.98,74.235,39.825c22.176,16.842,41.686,36.354,58.525,58.529c16.841,22.175,30.122,46.921,39.831,74.23c9.702,27.315,15.037,55.631,15.984,84.938C356.407,358.69,355.556,360.874,353.451,362.585z"></path>
</g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
<g></g>
</svg>Nyheter
		</a></li>
</ul>
<p> </p></div>
</div>
</div>
<div class="row">
<div class="moduletable col-sm-12">
<div class="custom">
<p><a class="pull-right" href="#main"><i class="icon-chevron-up"></i></a></p></div>
</div>
</div>
</div>
</footer>
</div>
<script type="text/javascript">(function(){var sz=document.createElement('script');sz.type='text/javascript';sz.async=true;sz.src='//siteimproveanalytics.com/js/siteanalyze_6000656.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(sz,s);})();</script>
<script>(function(d){var s=d.createElement("script");s.setAttribute("data-account","u4wXyBGS43");s.setAttribute("src","https://cdn.userway.org/widget.js");(d.body||d.head).appendChild(s);})(document)</script>
<noscript>
		Please ensure Javascript is enabled for purposes of
		<a href="https://userway.org">website accessibility</a>
</noscript>
</div></div></div> <script src="/media/plg_jchoptimize/cache/js/ae89916f3e1042035e76bd2b879aeccd_1.js" type="application/javascript"></script>
<script defer="defer" src="/media/com_dpcalendar/js/dpcalendar/loader.min.js?e36855529619733d9969fbf9f39bfe95" type="text/javascript"></script>
<script defer="defer" src="/media/mod_dpcalendar_upcoming/js/default.min.js?e36855529619733d9969fbf9f39bfe95" type="text/javascript"></script>
</body>
</html>