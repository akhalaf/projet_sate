<!DOCTYPE html>
<html lang="ja" ng-app="NetCommonsApp">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>
		おかやまキュージンワーク	</title>
<link href="/nc_favicon.ico?1609987002" rel="icon" type="image/x-icon"/><link href="/nc_favicon.ico?1609987002" rel="shortcut icon" type="image/x-icon"/><meta content="4" http-equiv="refresh" url="/auth/login"/>
<link href="/components/bootstrap/dist/css/bootstrap.min.css?1609988035" rel="stylesheet" type="text/css"/>
<link href="/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css?1609988043" rel="stylesheet" type="text/css"/>
<link href="/css/net_commons/style.css?1609987561" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap.min.css?1609987049" rel="stylesheet" type="text/css"/>
<link href="/theme/default/css/style.css?1609988026" rel="stylesheet" type="text/css"/>
<script src="/components/jquery/dist/jquery.min.js?1609987781" type="text/javascript"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js?1609988037" type="text/javascript"></script>
<script src="/components/angular/angular.min.js?1609987282" type="text/javascript"></script>
<script src="/components/angular-animate/angular-animate.js?1609987284" type="text/javascript"></script>
<script src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js?1609987288" type="text/javascript"></script>
<script src="/js/net_commons/base.js?1609987563" type="text/javascript"></script>
<script>
NetCommonsApp.constant('NC3_URL', '');
NetCommonsApp.constant('LOGIN_USER', {"id":null});
NetCommonsApp.constant('TITLE_ICON_URL', '');
</script>
</head>
<body ng-controller="NetCommons.base">
<main class="container">
<h2 class="error-title" style="background-image: url(/img/net_commons/redirect_arrow.gif)">
	Not Found</h2>
<div class="error-redirect">
		ページが自動的に更新されない場合は<a href="/auth/login">こちら</a>をクリックしてください。	</div>
<script type="text/javascript">
		setTimeout(
			function() {
				location.href='/auth/login'.replace(/&amp;/ig,"&");
			},
			4000		);
	</script>
</main>
<footer id="nc-system-footer" role="contentinfo">
<div class="box-footer">
<div class="copyright">Powered by NetCommons</div>
</div>
</footer>
<script type="text/javascript">
$(function() {
	$(document).on('keypress', 'input:not(.allow-submit)', function(event) {
		return event.which !== 13;
	});
	$('article > blockquote').css('display', 'none');
	$('<button class="btn btn-default nc-btn-blockquote"><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span> </button>')
		.insertBefore('article > blockquote').on('click', function(event) {
			$(this).next('blockquote').toggle();
		});
});
</script>
</body>
</html>
