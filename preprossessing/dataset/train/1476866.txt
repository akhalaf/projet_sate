<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<!-- <style>
.active{ background : #B4F9AC; font-size: 15px; box-shadow : 0px 0px 5px 6px #ccc; }
.progress-holder{ width: 300px; padding: 2px; background: #CCCCCC; border-radius: 3px; float: left; margin-top: 4px; margin-right: 5px; }
#progress{ height: 6px; display:block; width: 0%; border-radius: 2px; background: -moz-linear-gradient(center top , #13DD13 20%, #209340 80%) repeat scroll 0 0 transparent; /* IE hack */  background: -ms-linear-gradient(bottom, #13DD13, #209340); /* chrome hack */  background-image: -webkit-gradient(linear, 20% 20%, 20% 100%, from(#13DD13), to(#209340)); /* safari hack */  background-image: -webkit-linear-gradient(top, #13DD13, #209340); /* opera hack */  background-image: -o-linear-gradient(#13DD13,#209340); box-shadow:3px 3px 3px #888888; }
.preview{ border: 1px solid #CDCDCD; width: 450px; padding: 10px; height:auto;  overflow: auto; color: #4D4D4D; float: left; box-shadow:3px 3px 3px #888888; border-radius: 2px; }
.percents{ float: right; }
.preview-image{ box-shadow: 3px 3px 3px #888888; width: 70px; height: 70px; float: left; margin-right: 10px; }
.file-info{ height: 50px; float: left; width: auto; margin-bottom: 10px; border: 1px solid blue; }
.file-info span{ margin: 3px 2px; font-size: 12px; float:left; display: block; min-width: 100px; overflow: auto; border: 1px solid red; overflow: none; }
.upload-progress{ display: none; }</style>-->
<!--meta-->
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="WordPress 5.3.6" name="generator"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="Health Medical Clinic WordPress Theme" name="description"/>
<meta content="telephone=no" name="format-detection"/>
<link href="path/to/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"/>
<!--style-->
<link href="https://tricityscaffold.com/feed/" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="https://tricityscaffold.com/xmlrpc.php" rel="pingback"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/images/favicon.ico" rel="shortcut icon"/>
<title>Page not found - Tricity</title>
<!-- This site is optimized with the Yoast SEO plugin v12.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page not found - Tricity" property="og:title"/>
<meta content="Tricity" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page not found - Tricity" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://tricityscaffold.com/#website","url":"https://tricityscaffold.com/","name":"Tricity","potentialAction":{"@type":"SearchAction","target":"https://tricityscaffold.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/tricityscaffold.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://tricityscaffold.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.5" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/final-tiles-grid-gallery-lite/scripts/ftg.css?ver=3.4.18" id="finalTilesGallery_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css?ver=5.3.6" id="fontawesome_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.2.5" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100i%2C300%2C300i%2C400%2C400i%2C500%2C500i%2C700%2C700i%2C900%2C900i&amp;ver=2.7.4" id="simple-job-board-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/simple-job-board/public/css/font-awesome.min.css?ver=4.7.0" id="simple-job-board-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/simple-job-board/public/css/jquery-ui.css?ver=1.12.1" id="simple-job-board-jquery-ui-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/plugins/simple-job-board/public/css/simple-job-board-public.css?ver=3.0.0" id="simple-job-board-frontend-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/reset.css?ver=5.3.6" id="reset-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/superfish.css?ver=5.3.6" id="superfish-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/prettyPhoto.css?ver=5.3.6" id="prettyPhoto-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/jquery.qtip.css?ver=5.3.6" id="jquery-qtip-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style.css?ver=5.3.6" id="parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300&amp;subset=greek-ext&amp;ver=5.3.6" id="google-font-header-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300&amp;subset=latin%2Clatin-ext&amp;ver=5.3.6" id="google-font-open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=PT+Serif%3A400italic&amp;subset=latin%2Clatin-ext&amp;ver=5.3.6" id="google-font-pt-serif-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/odometer-theme-default.css?ver=5.3.6" id="odometer-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/animations.css?ver=5.3.6" id="animations-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter-child/style.css?ver=5.3.6" id="main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/style/responsive.css?ver=5.3.6" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/fonts/features/style.css?ver=5.3.6" id="mc-features-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/fonts/template/style.css?ver=5.3.6" id="mc-template-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/fonts/social/style.css?ver=5.3.6" id="mc-social-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tricityscaffold.com/wp-content/themes/medicenter/custom.css?ver=5.3.6" id="custom-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://tricityscaffold.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.1' type='text/css' media='screen' />
<![endif]-->
<script src="https://tricityscaffold.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.2.5" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.2.5" type="text/javascript"></script>
<link href="https://tricityscaffold.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://tricityscaffold.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://tricityscaffold.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<meta content="Powered by Slider Revolution 5.2.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<!-- Facebook Open Graph protocol plugin NEEDS an admin or app ID to work, please visit the plugin settings page! -->
<style id="wp-custom-css" type="text/css">
			.contactform.wpb_column.vc_column_container.vc_col-sm-8.vc_custom_1553510852603 {
    padding-left: 63px !important;
}
.pdf_title a {
    color: #000;
}
.col-finer br {
    display: none;
}
h4.slider_text {
    color: #7B1306;
    font-size: 26px !important;
    font-weight: 700 !important;
}
.form-control.msg {
    width: 97%;
}
.col-finer {
    padding-top: 11px;
}
.col-finer label {
    width: 46%;
    float: left;
    margin-right: 7px;
}
.page-id-1294 .vc_row.wpb_row.vc_row-fluid.vc_custom_1550125792066.vc_row-has-fill{
	width:100%;
	left:0px;
}
.page-id-1294 .vc_row.wpb_row.vc_row-fluid{
		width:100%;
	left:0px;
}
.page-id-4348 .main-page{
	width:100% !important;
}
.page-id-1294 .main-page{
		width:100% !important;
}
.checked {
    margin-top: 50px !important;
}
video.accessvideo {
    object-fit: fill;
}
.row5.wpb_column.vc_column_container.vc_col-sm-3 {
    width: 23%;
}
.portfoio-grid .vc_row .vc_col-sm-6{
	    width: 635px !important;
}
.row1.wpb_column.vc_column_container.vc_col-sm-3 {
    width: 25%;
}
.vc_row.wpb_row.vc_row-fluid.portfoio-grid.vc_custom_1552371218046.vc_row-has-fill.vc_row-no-padding {
    width: 100% !important;
    left: 0px !important;
    padding: 10px;
}
.vc_custom_1552310050269{
	margin-right:0px !important;
}
.page-id-1294 .home-body-content{
	padding:0px !important;
}
.vc_row.wpb_row.vc_row-fluid.vc_custom_1550122430309.vc_row-has-fill {
    margin-top: 0px !important;
}
#headerContactContainer{
    padding-top: 10%;
    padding-bottom: 20%;
}
.vc_row.wpb_row.vc_row-fluid.client-back.setimg.vc_custom_1552312413704.vc_row-has-fill {
    background-position: 0px 74% !important;
}
.aces-service-content {
height: 330px;
    padding: 50px !important;
}
.vc_row.wpb_row.vc_row-fluid.client-back.setimg.vc_custom_1552309478911.vc_row-has-fill {
    background-position: 0px 80% !important;
}
.vc_row.wpb_row.vc_row-fluid.home-body-content.client-back.setimg.vc_custom_1552313987147.vc_row-has-fill.vc_row-no-padding {
    background-position: 0px 74% !important;
}
.vc_row.wpb_row.vc_row-fluid.home-body-content.client-back.vc_custom_1552051683552.vc_row-has-fill.vc_row-no-padding {
    background-position: 0px 72% !important;
}
.vc_row.wpb_row.vc_row-fluid.client-back.setimg.vc_custom_1552309407399.vc_row-has-fill {
    background-position: 0px 74% !important;
}
.vc_row.wpb_row.vc_row-fluid.client-back.setimg.vc_custom_1552309063389.vc_row-has-fill {
    background-position: 0px 74% !important;
}
.vc_row.wpb_row.vc_row-fluid.home-body-content.client-back.setimg.vc_custom_1552308929999.vc_row-has-fill {
    background-position: 0px 74% !important;
}
.vc_row.wpb_row.vc_row-fluid.home-body-content.client-back.setimg.vc_custom_1552308812855.vc_row-has-fill.vc_row-no-padding {
    background-position: 0px 74% !important;
}
ul#slider-navigation-12 {
    display: none !important;
}
#rev_slider_2_1 .uranus.tparrows:before {
   font-size: 47px;
    font-weight: bold;
    color: #7c0a02;
}

.home .top-bar-slider {
    display: block;
}
ul#slider-navigation-72 {
    display: none;
}
.vc_row .vc_col-sm-8 {
    width: 63.666667%;
    margin-right: 0;
}
.wpb_column.vc_column_container.vc_col-sm-4 {
    width: 32.333333%;
}

.page-id-4278 .safty-service .wpb_column.vc_column_container.vc_col-sm-3{
    width: 25%;
    margin-left: 0;
    margin-right: 0;
}
ul#slider-navigation-67 {
    display: none;
}
.page-id-7 a:hover img, .gallery-box:hover img {
    background: transparent;
}
.overHidden {
    margin-bottom: 6px;
}
.abt-left.wpb_column.vc_column_container.vc_col-sm-4 {
    background: #7c0a02;
    padding: 13px;
}

.service-acess .wpb_column.vc_column_container.vc_col-sm-3 {
    width: 25% !important;
    margin-left: 0;
    margin-right: 0;
}
.ytp-chrome-top.ytp-show-watch-later-title.ytp-share-button-visible.ytp-show-share-title.ytp-show-cards-title {
    display: none;
}		</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript> <!--custom style-->
<style type="text/css">
		h1, h2, h3, h4, h5,
	.header-left a, .logo,
	.top-info-list li .value,
	.footer-banner-box p,
	.rev_slider p,
	table td:first-child, table th:first-child
		{
		font-family: 'Open Sans';
	}
	</style> </head>
<body class="error404 medicenter-child wpb-js-composer js-comp-ver-6.0.1 vc_responsive" data-rsssl="1">
<div class="site-container fullwidth">
<!-- Header -->
<div class="header-container">
<div class="header-top-area">
<div class="header clearfix layout-1">
<div class="header-left">
<a href="https://tricityscaffold.com" title="Tricity">
<img alt="logo" src="https://tricityscaffold.com/wp-content/uploads/2019/02/loggo_color-1.png"/>
</a>
<a class="mobile-menu-switch vertical-align-cell" href="#">
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
<span class="line"></span>
</a>
</div>
<div class="top-bar-right">
<div class="">
<div class="">
<div class="textwidget"><div id="headerContactContainer">
<p class="telTop"><a href="tel:518-895-2587">518-895-2587</a></p>
<p class="emailTop"><a href="mailto:
Info@tricityscaffold.com">Info@tricityscaffold.com</a></p>
</div></div>
</div>
</div>
</div></div></div></div>
<div class="menu-header-center">
<div class="header bottom-clearfix layout-1">
<div class="main-menu">
<div class="menu-main-menu-container"><ul class="sf-menu header-center" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3966" id="menu-item-3966"><a href="https://tricityscaffold.com/">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4246" id="menu-item-4246"><a href="https://tricityscaffold.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4397" id="menu-item-4397"><a href="https://tricityscaffold.com/access-services/">Access services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4367" id="menu-item-4367"><a href="https://tricityscaffold.com/safety-training/">Safety Training</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4366" id="menu-item-4366"><a href="https://tricityscaffold.com/project-library/">Project Library</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4318" id="menu-item-4318"><a href="https://tricityscaffold.com/media/">Media</a></li>
<li class="left-flyout menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3923" id="menu-item-3923"><a href="https://tricityscaffold.com/contact/">CONTACT US</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5242" id="menu-item-5242"><a href="https://tricityscaffold.com/careers/">Careers</a></li>
</ul>
</li>
</ul></div> <div class="mobile-menu-container clearfix">
<div class="mobile-menu-divider"></div>
<nav class="mobile-menu collapsible-mobile-submenus"><ul class="menu" id="menu-main-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3966"><a href="https://tricityscaffold.com/">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4246"><a href="https://tricityscaffold.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4397"><a href="https://tricityscaffold.com/access-services/">Access services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4367"><a href="https://tricityscaffold.com/safety-training/">Safety Training</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4366"><a href="https://tricityscaffold.com/project-library/">Project Library</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4318"><a href="https://tricityscaffold.com/media/">Media</a></li>
<li class="left-flyout menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3923"><a href="https://tricityscaffold.com/contact/">CONTACT US</a><a class="template-arrow-menu" href="#"></a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5242"><a href="https://tricityscaffold.com/careers/">Careers</a></li>
</ul>
</li>
</ul></nav> </div>
</div>
</div>
<!--<script type="text/javascript">
        jQuery(document).ready(function(){  
    var dropbox;  

    dropbox = document.getElementById("dropbox");  
    dropbox.addEventListener("dragenter", dragenter, false);  
    dropbox.addEventListener("dragleave", dragleave, false);  
    dropbox.addEventListener("dragover", dragover, false);  
    dropbox.addEventListener("drop", drop, false);  

    function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
    }
    function dragenter(e) {  
       jQuery(this).addClass("active");
       defaults(e);
    }  

    function dragover(e) { 
       defaults(e);
    }  
    function dragleave(e) {  
       jQuery(this).removeClass("active");
       defaults(e);
    }  

    function drop(e) {  
       jQuery(this).removeClass("active");
       defaults(e);
       var dt = e.dataTransfer;  
       var files = dt.files;  

       handleFiles(files,e);  
    }  
   jQuery(document).on('change', '#fileElem', function(){
    handleFiles(this.files)});
    handleFiles = function (files,e){

        var imageType = /image.*/;  
        var file = files[0];

        var info = '<div class="preview active-win"><div class="preview-image"><img ></div><div class="progress-holder"><span id="progress"></span></div><span class="percents"></span><div style="float:left;">Uploaded <span class="up-done"></span> KB of '+parseInt(file.size / 1024)+' KB</div>';

        jQuery(".upload-progress").show("fast",function(){
            jQuery(".upload-progress").html(info); 
            uploadFile(file);
        });

  }

  uploadFile = function(file){
    // check if browser supports file reader object 
    if (typeof FileReader !== "undefined"){
    //alert("uploading "+file.name);  
    reader = new FileReader();
    reader.onload = function(e){
        //alert(e.target.result);
        jQuery('.preview img').attr('src',e.target.result).css("width","70px").css("height","70px");
    }
    reader.readAsDataURL(file);

    xhr = new XMLHttpRequest();
    xhr.open("post", "ajax_fileupload.php", true);

    xhr.upload.addEventListener("progress", function (event) {
        console.log(event);
        if (event.lengthComputable) {
            jQuery("#progress").css("width",(event.loaded / event.total) * 100 + "%");
            jQuery(".percents").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
            jQuery(".up-done").html((parseInt(event.loaded / 1024)).toFixed(0));
        }
        else {
            alert("Failed to compute file upload length");
        }
    }, false);

    xhr.onreadystatechange = function (oEvent) {  
      if (xhr.readyState === 4) {  
        if (xhr.status === 200) {  
          jQuery("#progress").css("width","100%");
          jQuery(".percents").html("100%");
          jQuery(".up-done").html((parseInt(file.size / 1024)).toFixed(0));
           jQuery(".upload-progress").hide();
        } else {  
          alert("Error"+ xhr.statusText);  
        }  
      }  
    };  

    // Set headers
    xhr.setRequestHeader("Content-Type", "multipart/form-data");
    xhr.setRequestHeader("X-File-Name", file.fileName);
    xhr.setRequestHeader("X-File-Size", file.fileSize);
    xhr.setRequestHeader("X-File-Type", file.type);
    xhr.send(file);

    }else{
        alert("Your browser doesnt support FileReader object");
    }       
    }
    });
    </script>-->
<!-- /Header -->
<div class="theme-page relative">
<div class="clearfix">
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><span class="icon-single mc-icon features-document-missing page-margin-top-section"></span><h1 class="box-header no-border page-margin-top">404. The page you requested was not found.</h1>
<div class="wpb_text_column wpb_content_element description">
<div class="wpb_wrapper">
<p>We’re sorry, but we can’t seem to find the page you requested.<br/>
This might be because you have typed the web address incorrectly.</p>
</div>
</div>
<a class="mc-button more light margin-top-20" href="http://quanticalabs.com/wp_themes/medicenter/" title="BACK TO HOME">BACK TO HOME</a>
</div></div></div>
</div>
</div>
<div class="footer-container">
<div class="footer">
<ul class="footer-banner-box-container clearfix">
</ul>
<div class="footer-box-container vc_row wpb_row vc_row-fluid clearfix">
</div>
</div>
</div>
<div class="copyright-area-container">
<div class="copyright-area clearfix">
<div class="copyright-text">
					© 2019 <a href="#" target="_blank">Tricity</a>. All rights reserved.					</div>
</div>
</div>
</div>
<a class="scroll-top animated-element template-arrow-vertical-3" href="#top" title="Scroll to top"></a>
<link href="https://tricityscaffold.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.1" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/tricityscaffold.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://tricityscaffold.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.5" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/plugins/final-tiles-grid-gallery-lite/scripts/jquery.finalTilesGallery.js?ver=3.4.18" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/ui/accordion.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/ui/tabs.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.ba-bbq.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.history.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.easing.1.3.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.carouFredSel-6.2.1-packed.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.sliderControl.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.timeago.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.hint.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.isotope-packed.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.prettyPhoto.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.qtip.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/jquery.blockUI.js?ver=5.3.6" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var config = [];
config = {"ajaxurl":"https:\/\/tricityscaffold.com\/wp-admin\/admin-ajax.php","themename":"medicenter","home_url":"https:\/\/tricityscaffold.com","is_rtl":0};;
/* ]]> */
</script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/main.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/themes/medicenter/js/odometer.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://tricityscaffold.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.1" type="text/javascript"></script>
<script>
		   // myVid=document.getElementById("aboutiframe");
		   // myVid.muted=true;
		</script>
</div></body>
</html>
