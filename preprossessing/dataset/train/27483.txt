<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<title>3one- Web Development | App Development | Digital Marketing |UI/UX Design </title>
<meta content="3one Technologies Mobile App Development" name="description"/>
<meta content="Web development, App Development, Digital Marketing " name="keywords"/>
<meta content="rajesh-doot" name="author"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="#322d97" name="theme-color"/>
<!--website-favicon-->
<link href="images/favicon.png" rel="icon"/>
<!--plugin-css-->
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/owl.carousel.min.css" rel="stylesheet"/>
<link href="css/magnific-popup.min.css" rel="stylesheet"/>
<link href="../../../cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet"/>
<!--google-fonts-->
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&amp;family=Poppins:wght@400;500;600;700&amp;display=swap" rel="stylesheet"/>
<!-- template-style-->
<link href="css/style.min.css" rel="stylesheet"/>
<link href="css/responsive.min.css" rel="stylesheet"/>
</head>
<body>
<!--Start Header -->
<header class="nav-bg-b main-header navfix fixed-top">
<div class="container-fluid m-pad">
<div class="menu-header">
<div class="dsk-logo"><a class="nav-brand" href="index.html">
<img alt="Logo" class="mega-white-logo" src="images/white-logo.png"/>
<img alt="Logo" class="mega-darks-logo" src="images/logo.png"/>
</a></div>
<div class="custom-nav" role="navigation">
<ul class="nav-list">
<li class="sbmenu"><a class="menu-links" href="#">Home</a>
</li>
<li class="sbmenu"><a class="menu-links" href="#">Company</a></li>
<li class="sbmenu"><a class="menu-links" href="#">Services</a>
<div class="nx-dropdown">
<div class="sub-menu-section">
<div class="container">
<div class="col-md-12">
<div class="sub-menu-center-block">
<div class="sub-menu-column">
<div class="menuheading">Web Developments</div>
<ul>
<li><a href="service-details.html">Web Development</a></li>
<li><a class="disabled" href="#">PHP Development</a></li>
<li><a class="disabled" href="#">Angular JS Development</a></li>
<li><a class="disabled" href="#">Laravel Development</a></li>
<li><a class="disabled" href="#">Node Development</a></li>
<li><a class="disabled" href="#">Codeigniter Development</a></li>
<li><a class="disabled" href="#">ASP.Net Development</a></li>
</ul>
</div>
<div class="sub-menu-column">
<div class="menuheading">Mobile App Development </div>
<ul>
<li><a href="service-details2.html">Mobile App Development</a> </li>
<li><a class="disabled" href="#">Android App Development</a> </li>
<li><a class="disabled" href="#">iPhone App Development</a> </li>
<li><a class="disabled" href="#">Windows App Development</a> </li>
<li><a class="disabled" href="#">Custom Application Development</a> </li>
<li><a class="disabled" href="#">Wearable App Development</a> </li>
</ul>
</div>
<div class="sub-menu-column">
<div class="menuheading">Digital Marketing</div>
<ul>
<li><a href="service-details3.html">Search Engine Optimization</a> </li>
<li><a class="disabled" href="#">Social Media Marketing</a> </li>
<li><a class="disabled" href="#">Pay per Click</a> </li>
<li><a class="disabled" href="#">Content Writing</a> </li>
<li><a class="disabled" href="#">Email Marketing</a> </li>
</ul>
</div>
<div class="sub-menu-column">
<div class="menuheading">Graphic Design</div>
<ul>
<li><a href="service-details4.html">Graphic Design Service </a> </li>
<li><a class="disabled" href="#">Brochure Design</a> </li>
<li><a class="disabled" href="#">Print &amp; Packaging Design</a> </li>
<li><a class="disabled" href="#">Logo Design</a> </li>
<li><a class="disabled" href="#">Branding</a> </li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</li>
<li><a class="menu-links" href="#">Portfolio</a> </li>
<!--menu right border-->
<li><a class="menu-links right-bddr" href="#">Contact</a> </li>
</ul>
</div>
<div class="mobile-menu2">
<ul class="mob-nav2">
<li class="navm-"> <a class="toggle" href="#"><span></span></a></li>
</ul>
</div>
</div>
<!--Mobile Menu-->
<nav id="main-nav">
<ul class="first-nav">
<li><a href="#">Home</a> </li>
<li><a href="#">Company</a></li>
<li><a href="#">Services</a>
<ul>
<li>
<a href="#">Web Development</a>
<ul>
<li><a href="service-details.html">Web Development</a></li>
<li><a class="disabled" href="#">PHP Development</a></li>
<li><a class="disabled" href="#">Angular JS</a></li>
<li><a class="disabled" href="#">Laravel</a></li>
<li><a class="disabled" href="#">Node Js</a></li>
<li><a class="disabled" href="#">Codeigniter</a></li>
<li><a class="disabled" href="#">ASP.Net</a></li>
</ul>
</li>
<li>
<a href="#">Mobile App Development</a>
<ul>
<li><a href="service-details2.html">Mobile App Development</a> </li>
<li><a class="disabled" href="#">Android App</a> </li>
<li><a class="disabled" href="#">Cross App</a> </li>
<li><a class="disabled" href="#">Windows App</a> </li>
<li><a class="disabled" href="#">Custom Application</a> </li>
<li><a class="disabled" href="#">Wearable App</a> </li>
</ul>
</li>
<li>
<a href="#">Digital Marketing</a>
<ul>
<li><a href="service-details3.html">Digital Marketing Service</a> </li>
<li><a class="disabled" href="#">Social Media Marketing</a> </li>
<li><a class="disabled" href="#">Pay per Click</a> </li>
<li><a class="disabled" href="#">Content Writing</a> </li>
<li><a class="disabled" href="#">Email Marketing</a> </li>
</ul>
</li>
<li>
<a href="#">Graphic Design</a>
<ul>
<li><a href="service-details4.html">Graphic Design Service </a> </li>
<li><a class="disabled" href="#">Brochure</a> </li>
<li><a class="disabled" href="#">Print &amp; Packaging</a> </li>
<li><a class="disabled" href="#">Logo Design</a> </li>
<li><a class="disabled" href="#">Branding</a> </li>
</ul>
</li>
</ul>
</li>
<li><a href="portfolio.html">Portfolio</a> </li>
<li><a href="get-quote.html">Contact</a> </li>
</ul>
</nav>
<!--Mobile contact-->
</div>
</header>
<!--End Header -->
<!--Start Hero-->
<section class="hero-card-web bg-gradient12 shape-bg3">
<div class="hero-main-rp container-fluid">
<div class="row">
<div class="col-lg-5">
<div class="hero-heading-sec">
<h2><span>Web.</span> <span>Mobile.</span> <span>Graphic.</span> <span>Marketing.</span></h2>
<p>Website and App development solution for transforming and innovating businesses.</p>
<a class="btn-main bg-btn lnk" href="#">View Case Studies <i class="fas fa-chevron-right fa-ani"></i><span class="circle"></span></a>
</div>
</div>
<div class="col-lg-7">
<div class="hero-right-scmm">
<div class="hero-service-cards">
<div class="owl-carousel service-card-prb">
<div class="service-slide card-bg-a"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/vr.png"/>
</div>
<div class="title-serv-c"><span>VR</span> Solution</div>
</div></a>
</div>
<div class="service-slide card-bg-b"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/app-develop.png"/>
</div>
<div class="title-serv-c"><span>Custom</span> App Solution</div>
</div></a>
</div>
<div class="service-slide card-bg-c"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/startup.png"/>
</div>
<div class="title-serv-c"><span>Startup</span> Solution</div>
</div></a>
</div>
<div class="service-slide card-bg-d"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/car-rental.png"/>
</div>
<div class="title-serv-c"><span>Car</span> Rental Solution</div>
</div></a>
</div>
<div class="service-slide card-bg-e"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/marketing.png"/>
</div>
<div class="title-serv-c"><span>Marketing</span> Solution</div>
</div></a>
</div>
<div class="service-slide card-bg-f"><a href="#">
<div class="service-card-hh">
<div class="image-sr-mm">
<img alt="custom-sport" src="images/service/ewallet.png"/>
</div>
<div class="title-serv-c"><span>e-Wallet</span> Solution</div>
</div></a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End Hero-->
<!--Start About-->
<section class="about-sec-rpb pad-tb">
<div class="container">
<div class="row justify-content-center text-center">
<div class="col-lg-10">
<div class="common-heading">
<span>We Are Creative Agency</span>
<h1 class="mb30">Top-rated Web And Mobile App Development Company</h1>
<p>Since our inception in 2006,<span class="text-radius text-light text-animation bg-b">3One Technologies</span>has been delivering software development and related IT services. We combine proven methodologies, business domain knowledge and technology expertise of skilled software professionals to deliver high quality solutions </p>
<h3 class="mt30 mb30">Big Ideas, creative people, new technology.</h3>
<p>Established in 2006 <span class="text-bold">3one</span> has delivered award-winning Website Design,Mobile App, Graphic Design, and Custom Application Development to <span class="text-bold">clients across the world wide global </span> Our experience, client communication, and refined process  <span class="text-bold">help us</span>  exceed our clientsâ marketing objectives.. </p>
</div>
</div>
</div>
</div>
</section>
<!--End About-->
<!--Start Service-->
<section class="service-section-prb pad-tb">
<div class="container">
<div class="row upset">
<div class="col-lg-6-cus">
<div class="service-sec-brp srvc-bg-nx bg-gradient13 text-w">
<h4 class="mb10">INTEGRATED SERVICES</h4>
<p>3one specializes in website design and development services. Our web experiences are high-performing, feature-packed and digitally transformative, designed to be user-friendly, fully functional, very secure and able to scale as your enterprise grows</p>
<a class="mt20 link-prb" href="javascript:void(0)">Learn More <i class="fas fa-chevron-right"></i></a>
</div>
</div>
<div class="col-lg-3-cus">
<div class="service-sec-list srvc-bg-nx srcl1">
<img alt="service" src="images/icons/development.svg"/>
<h5 class="mb10">Web Development</h5>
<ul class="-service-list">
<li> <a href="#">PHP</a> </li>
<li> <a href="#"><strong>.</strong>Net</a> </li>
<li> <a href="#">Java</a> </li>
<li> <a href="#">Joomla</a></li>
</ul>
<p>3one is a leading web development company based in India, offering all kinds of custom built websites, web portals and web applications.We use cutting edge technologies</p>
</div>
</div>
<div class="col-lg-3-cus">
<div class="service-sec-list srvc-bg-nx srcl2">
<img alt="service" src="images/icons/ecommerce.svg"/>
<h5 class="mb10">Ecommerce Development</h5>
<ul class="-service-list">
<li> <a href="#">Magento </a> </li>
<li> <a href="#">WP</a> </li>
<li> <a href="#">Shopify </a> </li>
<li> <a href="#">Joomla</a></li>
</ul>
<p>Our ecommerce website development solution helps deliver a comprehensive and effective e-business strategy, products and marketing tactics, design and usability, technology and security.</p>
</div>
</div>
<div class="col-lg-3-cus mt30-">
<div class="service-sec-list srvc-bg-nx srcl3">
<img alt="service" src="images/icons/app.svg"/>
<h5 class="mb10">Mobile App Development</h5>
<ul class="-service-list">
<li> <a href="#">iPhone </a> </li>
<li> <a href="#">Android</a> </li>
<li> <a href="#">Cross Platform </a></li>
</ul>
<p>Creating seamless and intuitive mobile applications that offer the best user experience is what our team of mobile app development professionals at 3one, India are experts at. We design &amp; develop all kinds of mobile apps for iOS, Android, etc. depending on your business and end-user requirements.</p>
</div>
</div>
<div class="col-lg-3-cus mt30-">
<div class="service-sec-list srvc-bg-nx srcl4">
<img alt="service" src="images/icons/tech.svg"/>
<h5 class="mb10">Trending Technologies</h5>
<ul class="-service-list">
<li> <a href="#">React.JS </a> </li>
<li> <a href="#">Node.JS </a> </li>
<li> <a href="#"> Angular.JS </a></li>
</ul>
<p>Building the real mobile app
Incorporating the excellence and brilliance of the UI building blocks as in iOS and Android apps, we develop and design React Native apps that is indistinguishable from the apps designed using Objective-C or Java.</p>
</div>
</div>
<div class="col-lg-6-cus mt30-">
<div class="service-sec-list srvc-bg-nx srcl5">
<img alt="service" src="images/icons/seo.svg"/>
<h5 class="mb10">Digital Marketing</h5>
<ul class="-service-list">
<li> <a href="#">SEO </a> </li>
<li> <a href="#">SMO </a> </li>
<li> <a href="#">PPC </a></li>
<li> <a href="#">PPC </a></li>
</ul>
<p>Your site is only good if itâs attracting relevant traffic. That's why our digital marketers work directly with our developers to ensure your website ranks well on Google.</p>
</div>
</div>
</div>
<div class="-cta-btn mt70">
<div class="free-cta-title v-center">
<p>Hire a <span>Dedicated Developer</span></p>
<a class="btn-main bg-btn2 lnk" href="#">Hire Now<i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</section>
<!--End Service-->
<!--Start statistics-->
<div class="statistics-section bg-gradient6 pad-tb">
<div class="container">
<div class="row justify-content-center t-ctr">
<div class="col-lg-4 col-sm-6">
<div class="statistics">
<div class="statistics-img">
<img alt="years" class="img-fluid" src="images/icons/startup.svg"/>
</div>
<div class="statnumb">
<span class="counter">15</span><span>+</span>
<p>Year In Business</p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="statistics">
<div class="statistics-img">
<img alt="team" class="img-fluid" src="images/icons/team.svg"/>
</div>
<div class="statnumb">
<span class="counter">50</span><span>+</span>
<p>Team Members</p>
</div>
</div>
</div>
</div>
<div class="row small t-ctr">
<div class="col-lg-3 col-sm-6">
<div class="statistics">
<div class="statistics-img">
<img alt="happy" class="img-fluid" src="images/icons/deal.svg"/>
</div>
<div class="statnumb">
<span class="counter">450</span>
<p>Happy Clients</p>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics">
<div class="statistics-img">
<img alt="project" class="img-fluid" src="images/icons/computers.svg"/>
</div>
<div class="statnumb counter-number">
<span class="counter">48</span><span>k</span>
<p>Projects Done</p>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics">
<div class="statistics-img">
<img alt="work" class="img-fluid" src="images/icons/worker.svg"/>
</div>
<div class="statnumb">
<span class="counter">95</span><span>k</span>
<p>Hours Worked</p>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics mb0">
<div class="statistics-img">
<img alt="support" class="img-fluid" src="images/icons/customer-service.svg"/>
</div>
<div class="statnumb">
<span class="counter">24</span><span>/</span><span class="counter">7</span>
<p>Support Available</p>
</div>
</div>
</div>
</div>
</div>
</div>
<!--End statistics-->
<!--why choose-->
<section class="why-choos-lg pad-tb">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="common-heading text-l">
<span>Why Choose Us</span>
<h2 class="mb20">Why The 3one  <span class="text-second text-bold">Ranked Top</span> Among The Leading Web &amp; App Development Companies</h2>
<p>We've been working with the web since 2006 and have a few grey hairs to prove it! Our experience, along with our creative thinking, will help make sure your project is a success.</p>
<div class="itm-media-object mt40">
<div class="media">
<img alt="icon" src="images/icons/computers.svg"/>
<div class="media-body">
<h4>Streamlined Project Management</h4>
<p align="justify">projects have become more complex as technologies rapidly change and end-users demand greater ease-of-use and flexibility. For an IT project manager to achieve their objectives, it is imperative that these initiatives are completed on time and on budget</p>
</div>
</div>
<div class="media mt40">
<img alt="icon" src="images/icons/worker.svg"/>
<div class="media-body">
<h4>A Dedicated Team of Experts</h4>
<p align="justify">We have a web developer team of expert professionals with handsome experience in their relative fields. Our developers are a motivated lot who work towards achieving their goals within the specified timeframe and giving you complete worth of the money spent.</p>
</div>
</div>
<div class="media mt40">
<img alt="icon" src="images/icons/deal.svg"/>
<div class="media-body">
<h4>We're Passionate &amp; Friendly People</h4>
<p>We love a challenge. We enjoy complex web development projects and tricky third party software integrations . You'll find us honest, reliable and a safe pair of hands.</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="single-image bg-shape-dez">
<img alt="image" class="img-fluid" src="images/about/about-company.jpg"/></div>
<p class="text-center mt30">We can only deliver results if we know what your success looks like. We listen, find out what you want to achieve and plan how we get you there. Things work better when we do it together.</p>
<div class="cta-card mt60 text-center">
<h3 class="mb20">Let's Start a  <span class="text-second text-bold">New Project</span> Together</h3>
<p>We'll keep you updated with the progress of the site..</p>
<a class="btn-outline lnk mt30" href="#">Request A Quote    <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</div>
</section>
<!--End why choose-->
<!--Start Portfolio-->
<section class="portfolio-section pad-tb">
<div class="container">
<div class="row justify-content-center ">
<div class="col-lg-8">
<div class="common-heading">
<span>Our Work</span>
<h2 class="mb0">Our Latest Creative Work</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-8 col-sm-8 mt60">
<div class="isotope_item hover-scale">
<div class="item-image">
<a href="#"><img alt="image" class="img-fluid" src="images/portfolio/image-d.jpg"/> </a>
</div>
<div class="item-info">
<h4><a href="#">Ecommerce Development</a></h4>
<p>Web Application</p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-4 mt60">
<div class="isotope_item hover-scale">
<div class="item-image">
<a href="#"><img alt="image" class="img-fluid" src="images/portfolio/image-1.jpg"/> </a>
</div>
<div class="item-info">
<h4><a href="#">Creative App</a></h4>
<p>iOs, Android</p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-4 mt60">
<div class="isotope_item hover-scale">
<div class="item-image">
<a href="#"><img alt="image" class="img-fluid" src="images/portfolio/image-6.jpg"/> </a>
</div>
<div class="item-info">
<h4><a href="#">Brochure Design</a></h4>
<p>Graphic, Print</p>
</div>
</div>
</div>
<div class="col-lg-8 col-sm-8 mt60">
<div class="isotope_item hover-scale">
<div class="item-image">
<a href="#"><img alt="image" class="img-fluid" src="images/portfolio/image-c.jpg"/> </a>
</div>
<div class="item-info">
<h4><a href="#">Icon Pack</a></h4>
<p>iOs, Android</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End Portfolio-->
<!--Start Clients-->
<section class="clients-section- bg-gradient15 pad-tb">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-8">
<div class="common-heading">
<span>Our happy customers</span>
<h2 class="mb30">Some of our Clients</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="clients-logos text-center col-12">
<ul class="row text-center clearfix">
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-1.png"/></div>
<p>Cyber Forza, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-2.png"/></div>
<p>Iraye Group, UK</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-3.png"/></div>
<p>samskrita bharati, Singapore</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-4.png"/></div>
<p>Sai Technology, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-5.png"/></div>
<p>Gr Group, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-6.png"/></div>
<p>Biryani House , Ireland</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-7.png"/></div>
<p>My New Bazar, India</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-8.png"/></div>
<p>Dhana varsha Bank, India</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-9.png"/></div>
<p>My Business Central, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-10.png"/></div>
<p>Sri Samskrutika Kalasaradhi, Singapore </p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-11.png"/></div>
<p>Arbitrator murthy, India</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-12.png"/></div>
<p>sunrise schools Group, India</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-13.png"/></div>
<p>Comic Buzz, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-14.png"/></div>
<p>Heitta, South Africa</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-15.png"/></div>
<p>SMART NET SOLUTIONS, India</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-16.png"/></div>
<p>Sankalp Technologies, USA</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-17.png"/></div>
<p>Mana Telugu Mana Velugu, Singapore</p>
</li>
<li class="col-lg-2 col-md-3 col-sm-4 col-6">
<div class="brand-logo"><img alt="clients" class="img-fluid" src="images/client/clients-2.png"/></div>
<p>RP School, India</p>
</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<!--End Clients-->
<!--Start work-category-->
<section class="work-category pad-tb">
<div class="container">
<div class="row">
<div class="col-lg-4 v-center">
<div class="common-heading text-l">
<span>Industries we work for</span>
<h2>Helping Businesses in All Domains</h2>
<p>We offer our clients technology solutions that add real value to their business. Itâs simple â we understand that our success is measured by the success of our clients</p>
</div>
</div>
<div class="col-lg-8">
<div class="work-card-set">
<div class="icon-set">
<div class="work-card cd1">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-1.png"/></div>
<p>Social Networking</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd2">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-2.png"/></div>
<p>Digital Marketing</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd3">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-3.png"/></div>
<p>Ecommerce Development</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd4">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-4.png"/></div>
<p>Video Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd5">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-5.png"/></div>
<p>Banking Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd6">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-6.png"/></div>
<p>Enterprise Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd7">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-7.png"/></div>
<p>Education Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd8">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-8.png"/></div>
<p>Tour and Travels</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd9">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-9.png"/></div>
<p>Health Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd10">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-10.png"/></div>
<p>Event &amp; Ticket</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd11">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-11.png"/></div>
<p>Restaurant Service</p>
</div>
</div>
<div class="icon-set">
<div class="work-card cd12">
<div class="icon-bg"><img alt="Industries" src="images/icons/icon-12.png"/></div>
<p>Business Consultant</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End  work-category-->
<!--Start Testinomial-->
<section class="testinomial-section bg-none pad-tb">
<div class="container">
<div class="row justify-content-center ">
<div class="col-lg-8">
<div class="common-heading">
<span>What our clients say about Niwax.</span>
<h2>Over 1200+ Satisfied Clients and Growing</h2>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="video-testimonials owl-carousel">
<div class="video-review">
<a class="video-link" href="#"><img alt="client" class="img-fluid" src="images/client/client-pic.jpg"/>
<div class="review-vid-details">
<div class="-vid-ico"><span class="triangle-play2"></span></div>
<p>Cina Cleaves</p>
</div></a>
</div>
<div class="video-review">
<a class="video-link" href="#"><img alt="client" class="img-fluid" src="images/client/client-pic-a.jpg"/>
<div class="review-vid-details">
<div class="-vid-ico"><span class="triangle-play2"></span></div>
<p>Jokvch Marlin</p>
</div></a>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="owl-carousel testimonial-card-a pl25">
<div class="testimonial-card">
<div class="t-text">
<p>Great to work with. EXCELLENT results. I had a concept in mind and they really delivered with exceptional quality  </p>
</div>
<div class="client-thumbs mt30">
<div class="media v-center">
<div class="user-image bdr-radius"><img alt="girl" class="img-fluid" src="images/user-thumb/girl.jpg"/></div>
<div class="media-body user-info v-center">
<h5>Ginger Rockey-Johnson</h5>
<p>Spice Girl of Tampa Bay , <small>Tampa Bay , USA</small></p>
</div>
</div>
</div>
</div>
<div class="testimonial-card">
<div class="t-text">
<p>Very efficient and well developed concepts. A great pleasure to work with.  Very co-operative and worked hard to ensure our client was satisfied with the result.  </p>
</div>
<div class="client-thumbs mt30">
<div class="media v-center">
<div class="user-image bdr-radius"><img alt="girl" class="img-fluid" src="images/user-thumb/user3.jpg"/></div>
<div class="media-body user-info">
<h5>Simth  </h5>
<p>CEO of Gorilla Innovations, <small>New York, US State</small></p>
</div>
</div>
</div>
</div>
<div class="testimonial-card">
<div class="t-text">
<p>I was mostly happy with the high level of experience and professionalism of the various teams that worked on my project. Not only they clearly understood my exact technical requirements but even suggested better ways in doing them by using the presence of mind. The Communication and integration tools that were used were excellent and easy</p>
</div>
<div class="client-thumbs mt30">
<div class="media v-center">
<div class="user-image bdr-radius"><img alt="girl" class="img-fluid" src="images/user-thumb/arun.jpg"/></div>
<div class="media-body user-info">
<h5>Arun Voleti </h5>
<p>Director of Engineering - CyberFoza inc. <small>CA, USA</small></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-lg-12">
<div class="review-ref mt100">
<div class="review-title-ref">
<h4>Read More Reviews</h4>
<p>Read our reviews from all over world.</p>
</div>
<div class="review-icons">
<a href="#" target="blank"><img alt="review" class="img-fluid" src="images/about/reviews-icon-1.png"/></a>
<a href="#" target="blank"><img alt="review" class="img-fluid" src="images/about/reviews-icon-2.png"/></a>
<a href="#" target="blank"><img alt="review" class="img-fluid" src="images/about/reviews-icon-3.png"/></a>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End Testinomial-->
<!--Start CTA-->
<section class="cta-area pad-tb">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-8">
<div class="common-heading">
<span>Let's work together</span>
<h2>We Love to Listen to Your Requirements</h2>
<a class="btn-outline" href="javascript:void(0)">Estimate Project <i class="fas fa-chevron-right fa-icon"></i></a>
<p class="cta-call">Or call us now <a href="tel:+9163637333"><i class="fas fa-phone-alt"></i> (+91)90 63 63 73 33</a></p>
</div>
</div>
</div>
</div>
<div class="shape shape-a1"><img alt="shape" src="images/shape/shape-3.svg"/></div>
<div class="shape shape-a2"><img alt="shape" src="images/shape/shape-4.svg"/></div>
<div class="shape shape-a3"><img alt="shape" src="images/shape/shape-13.svg"/></div>
<div class="shape shape-a4"><img alt="shape" src="images/shape/shape-11.svg"/></div>
</section>
<!--End CTA-->
<!--Start Footer-->
<footer>
<div class="footer-row2">
<div class="container">
<div class="row justify-content-between">
<div class="col-lg-3 col-sm-6 ftr-brand-pp">
<a class="navbar-brand mt30 mb25" href="#"> <img alt="Logo" src="images/logo.png" width="100"/></a>
<p>3one Technologies is one of the best experienced website development  company Hyderabad, Kakinada &amp; Amaravati in Andhra Pradesh. We offer the complete web solutions all over the World including domain registration, hosting, website maintenance Company.</p>
<a class="btn-main bg-btn3 lnk mt20" href="#">Become Partner <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
<div class="col-lg-3 col-sm-6">
<h5>Contact Us</h5>
<ul class="footer-address-list ftr-details">
<li>
<span><i class="fas fa-envelope"></i></span>
<p>Email <span> <a href="mailto:info@3one.in">info@3one.in</a></span></p>
</li>
<li>
<span><i class="fas fa-phone-alt"></i></span>
<p>Phone <span> <a href="tel:+919063637333">+91 90 63 63 73 33</a></span></p>
</li>
<li>
<span><i class="fas fa-map-marker-alt"></i></span> <p>Telangana <span> # 6-3-1111/22,Behind Kirtilal Jewellers,Nishat Baght, Begumpet,Hyderabad â 500 016.</span></p></li>
<li> <p>Andhra Pradesh <span># 2-99 , Madhavapatnam kakinada-533005.</span></p>
</li>
</ul>
</div>
<div class="col-lg-2 col-sm-6">
<h5>Company</h5>
<ul class="footer-address-list link-hover">
<li><a href="get-quote.html">Contact</a></li>
<li><a href="javascript:void(0)">Customer's FAQ</a></li>
<li><a href="javascript:void(0)">Refund Policy</a></li>
<li><a href="javascript:void(0)">Privacy Policy</a></li>
<li><a href="javascript:void(0)">Terms and Conditions</a></li>
<li><a href="javascript:void(0)">License &amp; Copyright</a></li>
</ul>
</div>
<div class="col-lg-4 col-sm-6 footer-blog-">
<h5>Latest Blogs</h5>
<div class="single-blog-">
<div class="post-thumb"><a href="#"><img alt="blog" src="images/blog/blog-small.jpg"/></a></div>
<div class="content">
<p class="post-meta"><span class="post-date"><i class="far fa-clock"></i>April 15, 2020</span></p>
<h4 class="title"><a href="https://rajeshdoot.com/demo/niwax/blog-sngle.html">We Provide you Best &amp; Creative Consulting Service</a></h4>
</div>
</div>
<div class="single-blog-">
<div class="post-thumb"><a href="#"><img alt="blog" src="images/blog/blog-small.jpg"/></a></div>
<div class="content">
<p class="post-meta"><span class="post-date"><i class="far fa-clock"></i>April 15, 2020</span></p>
<h4 class="title"><a href="https://rajeshdoot.com/demo/niwax/blog-sngle.html">We Provide you Best &amp; Creative Consulting Service</a></h4>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-brands">
<div class="container">
<div class="row">
<div class="col-lg-4 v-center">
<h5 class="mb10">Top App Development Companies</h5>
<p>computing solutions, are counted as the most distinguish digital marketing firm. With the true expertise in current edge technology, it is not difficult to bring forth the true business expansion capacity before targeted readership and audience.</p>
</div>
<div class="col-lg-8 v-center">
<ul class="footer-badges-">
<li><a href="#"><img alt="badges" src="images/about/badges-a.png"/></a></li>
<li><a href="#"><img alt="badges" src="images/about/badges-b.png"/></a></li>
<li><a href="#"><img alt="badges" src="images/about/badges-c.png"/></a></li>
<li><a href="#"><img alt="badges" src="images/about/badges-d.png"/></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer-row3">
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="footer-social-media-icons">
<a href="javascript:void(0)" target="blank"><i class="fab fa-facebook"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-twitter"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-instagram"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-linkedin"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-youtube"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-pinterest-p"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-vimeo-v"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-dribbble"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-behance"></i></a>
</div>
<div class="footer-">
<p>Copyright © 2020 3one Technologies. All rights reserved.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<!--End Footer-->
<!--scroll to top-->
<a href="#top" id="scrollUp"></a>
<!-- js placed at the end of the document so the pages load faster -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/isotope-min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/hc-nav.js"></script>
<!--common script file-->
<script src="js/main.js"></script>
</body>
</html>