<!DOCTYPE html>
<html lang="nl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.boerderijhooghei.nl/xmlrpc.php" rel="pingback"/>
<title>Pagina niet gevonden</title>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.boerderijhooghei.nl/feed/" rel="alternate" title=" » Feed" type="application/rss+xml"/>
<link href="https://www.boerderijhooghei.nl/comments/feed/" rel="alternate" title=" » Reactiesfeed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.boerderijhooghei.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.boerderijhooghei.nl/wp-content/plugins/instagram-feed/css/sbi-styles.min.css?ver=2.6.2" id="sb_instagram_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.boerderijhooghei.nl/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.boerderijhooghei.nl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic|Oswald:300,300italic,regular,italic,600,600italic&amp;subset=latin" id="pinboard-web-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/style.css" id="pinboard-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/styles/colorbox.css" id="colorbox-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
	<script src="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/scripts/html5.js" type="text/javascript"></script>
	<![endif]-->
<script src="https://www.boerderijhooghei.nl/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/scripts/ios-orientationchange-fix.js" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/scripts/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/scripts/jquery.fitvids.js" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-content/themes/pinboard/scripts/jquery.colorbox-min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var icwp_wpsf_vars_lpantibot = {"form_selectors":"","uniq":"600037a4aef9e","cbname":"icwp-wpsf-58c6767e20d28","strings":{"label":"Ik ben een persoon.","alert":"Vink a.u.b. het vakje aan zodat we weten dat je geen bot bent.","loading":"Laden"},"flags":{"gasp":true,"captcha":true}};
/* ]]> */
</script>
<script src="https://www.boerderijhooghei.nl/wp-content/plugins/wp-simple-firewall/resources/js/shield-antibot.js?ver=10.1.5&amp;mtime=1607381407" type="text/javascript"></script>
<link href="https://www.boerderijhooghei.nl/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.boerderijhooghei.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.boerderijhooghei.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<script>
/* <![CDATA[ */
	jQuery(window).load(function() {
			});
	jQuery(document).ready(function($) {
		$('#access .menu > li > a').each(function() {
			var title = $(this).attr('title');
			if(typeof title !== 'undefined' && title !== false) {
				$(this).append('<br /> <span>'+title+'</span>');
				$(this).removeAttr('title');
			}
		});
		function pinboard_move_elements(container) {
			if( container.hasClass('onecol') ) {
				var thumb = $('.entry-thumbnail', container);
				if('undefined' !== typeof thumb)
					$('.entry-container', container).before(thumb);
				var video = $('.entry-attachment', container);
				if('undefined' !== typeof video)
					$('.entry-container', container).before(video);
				var gallery = $('.post-gallery', container);
				if('undefined' !== typeof gallery)
					$('.entry-container', container).before(gallery);
				var meta = $('.entry-meta', container);
				if('undefined' !== typeof meta)
					$('.entry-container', container).after(meta);
			}
		}
		function pinboard_restore_elements(container) {
			if( container.hasClass('onecol') ) {
				var thumb = $('.entry-thumbnail', container);
				if('undefined' !== typeof thumb)
					$('.entry-header', container).after(thumb);
				var video = $('.entry-attachment', container);
				if('undefined' !== typeof video)
					$('.entry-header', container).after(video);
				var gallery = $('.post-gallery', container);
				if('undefined' !== typeof gallery)
					$('.entry-header', container).after(gallery);
				var meta = $('.entry-meta', container);
				if('undefined' !== typeof meta)
					$('.entry-header', container).append(meta);
				else
					$('.entry-header', container).html(meta.html());
			}
		}
		if( ($(window).width() > 960) || ($(document).width() > 960) ) {
			// Viewport is greater than tablet: portrait
		} else {
			$('#content .hentry').each(function() {
				pinboard_move_elements($(this));
			});
		}
		$(window).resize(function() {
			if( ($(window).width() > 960) || ($(document).width() > 960) ) {
									$('.page-template-template-full-width-php #content .hentry, .page-template-template-blog-full-width-php #content .hentry, .page-template-template-blog-four-col-php #content .hentry').each(function() {
						pinboard_restore_elements($(this));
					});
							} else {
				$('#content .hentry').each(function() {
					pinboard_move_elements($(this));
				});
			}
			if( ($(window).width() > 760) || ($(document).width() > 760) ) {
				var maxh = 0;
				$('#access .menu > li > a').each(function() {
					if(parseInt($(this).css('height'))>maxh) {
						maxh = parseInt($(this).css('height'));
					}
				});
				$('#access .menu > li > a').css('height', maxh);
			} else {
				$('#access .menu > li > a').css('height', 'auto');
			}
		});
		if( ($(window).width() > 760) || ($(document).width() > 760) ) {
			var maxh = 0;
			$('#access .menu > li > a').each(function() {
				var title = $(this).attr('title');
				if(typeof title !== 'undefined' && title !== false) {
					$(this).append('<br /> <span>'+title+'</span>');
					$(this).removeAttr('title');
				}
				if(parseInt($(this).css('height'))>maxh) {
					maxh = parseInt($(this).css('height'));
				}
			});
			$('#access .menu > li > a').css('height', maxh);
							$('#access li').mouseenter(function() {
					$(this).children('ul').css('display', 'none').stop(true, true).fadeIn(250).css('display', 'block').children('ul').css('display', 'none');
				});
				$('#access li').mouseleave(function() {
					$(this).children('ul').stop(true, true).fadeOut(250).css('display', 'block');
				});
					} else {
			$('#access li').each(function() {
				if($(this).children('ul').length)
					$(this).append('<span class="drop-down-toggle"><span class="drop-down-arrow"></span></span>');
			});
			$('.drop-down-toggle').click(function() {
				$(this).parent().children('ul').slideToggle(250);
			});
		}
					var $content = $('.entries');
			$content.imagesLoaded(function() {
				$content.masonry({
					itemSelector : '.hentry, #infscr-loading',
					columnWidth : container.querySelector('.threecol'),
				});
			});
														$('.entry-attachment audio, .entry-attachment video').mediaelementplayer({
			videoWidth: '100%',
			videoHeight: '100%',
			audioWidth: '100%',
			alwaysShowControls: true,
			features: ['playpause','progress','tracks','volume'],
			videoVolume: 'horizontal'
		});
		$(".entry-attachment, .entry-content").fitVids({ customSelector: "iframe[src*='wordpress.tv'], iframe[src*='www.dailymotion.com'], iframe[src*='blip.tv'], iframe[src*='www.viddler.com']"});
	});
	jQuery(window).load(function() {
					jQuery('.entry-content a[href$=".jpg"],.entry-content a[href$=".jpeg"],.entry-content a[href$=".png"],.entry-content a[href$=".gif"],a.colorbox').colorbox({
				maxWidth: '100%',
				maxHeight: '100%',
			});
			});
/* ]]> */
</script>
<style type="text/css">
			#header input#s {
			width:168px;
			box-shadow:inset 1px 1px 5px 1px rgba(0, 0, 0, .1);
			text-indent: 0;
		}
						@media screen and (max-width: 760px) {
			#footer-area {
				display: none;
			}
		}
					#header {
			border-color: #5b5b5b;
		}
		#access {
			background: #5b5b5b;
		}
		@media screen and (max-width: 760px) {
			#access {
				background: none;
			}
		}
				#access li li {
			background: #919191;
		}
					.entry,
		#comments,
		#respond,
		#posts-nav {
			background: #ffffff;
		}
					#footer-area {
			background: #ffffff;
		}
				#copyright {
			background: #5b5b5b;
		}
																										#access a:hover,
		#access li.current_page_item > a,
		#access li.current-menu-item > a {
			color:#f0f0f0;
		}
				#sidebar,
		#sidebar-left,
		#sidebar-right {
			color:#666666;
		}
				.widget-title {
			color:#666666;
		}
				.widget-area a {
			color:#666666;
		}
				#footer-area {
			color:#666666;
		}
				#footer-area .widget-title {
			color:#666666;
		}
				#copyright {
			color:#cccccc;
		}
				#copyright a {
			color:#7597b9;
		}
		</style>
<style type="text/css">
	#site-title .home,
	#site-description {
		position:absolute !important;
		clip:rect(1px, 1px, 1px, 1px);
	}
</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #bfbfbf; background-image: url("https://www.boerderijhooghei.nl/wp-content/uploads/2014/10/Clipboard022.jpg"); background-position: left top; background-size: auto; background-repeat: repeat; background-attachment: fixed; }
</style>
</head>
<body class="error404 custom-background">
<div id="wrapper">
<header id="header">
<div id="site-title">
<a href="https://www.boerderijhooghei.nl/" rel="home">
<img alt="" height="648" src="https://www.boerderijhooghei.nl/wp-content/uploads/2020/05/cropped-IMG_4730-2.jpg" width="1500"/>
</a>
<a class="home" href="https://www.boerderijhooghei.nl/" rel="home"></a>
</div>
<div id="site-description"></div>
<div class="clear"></div>
<nav id="access">
<a class="nav-show" href="#access">Show Navigation</a>
<a class="nav-hide" href="#nogo">Hide Navigation</a>
<div class="menu-menu1-container"><ul class="menu" id="menu-menu1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-16" id="menu-item-16"><a href="https://www.boerderijhooghei.nl/">Welkom</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="https://www.boerderijhooghei.nl/wie-wij-zijn/">Over ons</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24" id="menu-item-24"><a href="https://www.boerderijhooghei.nl/algemeen/">Dagbesteding</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-785" id="menu-item-785"><a href="https://www.boerderijhooghei.nl/algemeen/">Dagbesteding algemeen</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-176" id="menu-item-176"><a href="https://www.boerderijhooghei.nl/dagbesteding-jongeren/">Voor wie?</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://www.boerderijhooghei.nl/pro-stadskanaal/">PrO Stadskanaal</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2460" id="menu-item-2460"><a href="https://www.boerderijhooghei.nl/stage/">Stage</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2411" id="menu-item-2411"><a href="https://www.boerderijhooghei.nl/fotos/">Foto’s</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-361" id="menu-item-361"><a href="https://www.boerderijhooghei.nl/contact/">Contact</a></li>
</ul></div> <div class="clear"></div>
</nav><!-- #access -->
</header><!-- #header --> <div id="container">
<section class="column threefourthcol" id="content">
<div class="entry">
<article class="post hentry column onecol" id="post-0">
<h2 class="entry-title">Content not found</h2>
<div class="entry-content">
				The content you are looking for could not be found.
											</div><!-- .entry-content -->
</article><!-- .post -->
<div class="clear"></div>
</div><!-- .entry -->
</section><!-- #content -->
<div class="column fourcol" id="sidebar">
<div class="widget-area" id="sidebar-top" role="complementary">
<div class="column onecol"><aside class="widget widget_instagram-feed-widget" id="instagram-feed-widget-2"><h3 class="widget-title">Instagram</h3>
<div class="sbi sbi_col_4 sbi_width_resp" data-cols="4" data-feedid="sbi_17841431005609623#20" data-num="20" data-res="auto" data-shortcode-atts="{}" id="sb_instagram" style="padding-bottom: 10px;width: 100%;">
<div class="sb_instagram_header sbi_no_avatar" style="padding: 5px; margin-bottom: 10px;padding-bottom: 0;">
<a class="sbi_header_link" href="https://www.instagram.com/boerderij_hooghei/" rel="noopener nofollow" target="_blank" title="@boerderij_hooghei">
<div class="sbi_header_text sbi_no_bio">
<h3>boerderij_hooghei</h3>
</div>
<div class="sbi_header_img">
<div class="sbi_header_hashtag_icon"><svg aria-hidden="true" aria-label="Instagram" class="sbi_new_logo fa-instagram fa-w-14" data-fa-processed="" data-icon="instagram" data-prefix="fab" role="img" viewbox="0 0 448 512">
<path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" fill="currentColor"></path>
</svg></div>
</div>
</a>
</div>
<div id="sbi_images" style="padding: 5px;">
<div class="sbi_item sbi_type_carousel sbi_new sbi_transition" data-date="1594758185" id="sbi_18043268812249022">
<div class="sbi_photo_wrap">
<a class="sbi_photo" data-full-res="https://scontent-amt2-1.cdninstagram.com/v/t51.29350-15/109132349_146929180344889_3654288394011287857_n.jpg?_nc_cat=101&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=kQYqHegMJvcAX97moks&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=f21b4b78d693e00d4c5a471190cab962&amp;oe=6024C918" data-img-src-set='{"d":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.29350-15\/109132349_146929180344889_3654288394011287857_n.jpg?_nc_cat=101&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=kQYqHegMJvcAX97moks&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=f21b4b78d693e00d4c5a471190cab962&amp;oe=6024C918","150":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.29350-15\/109132349_146929180344889_3654288394011287857_n.jpg?_nc_cat=101&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=kQYqHegMJvcAX97moks&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=f21b4b78d693e00d4c5a471190cab962&amp;oe=6024C918","320":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.29350-15\/109132349_146929180344889_3654288394011287857_n.jpg?_nc_cat=101&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=kQYqHegMJvcAX97moks&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=f21b4b78d693e00d4c5a471190cab962&amp;oe=6024C918","640":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.29350-15\/109132349_146929180344889_3654288394011287857_n.jpg?_nc_cat=101&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=kQYqHegMJvcAX97moks&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=f21b4b78d693e00d4c5a471190cab962&amp;oe=6024C918"}' href="https://www.instagram.com/p/CCosFUXJz6b/" rel="noopener nofollow" target="_blank">
<span class="sbi-screenreader">Afgelopen vrijdag hebben we weer genoten van onze </span>
<svg aria-hidden="true" aria-label="Clone" class="svg-inline--fa fa-clone fa-w-16 sbi_lightbox_carousel_icon" data-fa-pro="" data-icon="clone" data-prefix="far" role="img" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
<path d="M464 0H144c-26.51 0-48 21.49-48 48v48H48c-26.51 0-48 21.49-48 48v320c0 26.51 21.49 48 48 48h320c26.51 0 48-21.49 48-48v-48h48c26.51 0 48-21.49 48-48V48c0-26.51-21.49-48-48-48zM362 464H54a6 6 0 0 1-6-6V150a6 6 0 0 1 6-6h42v224c0 26.51 21.49 48 48 48h224v42a6 6 0 0 1-6 6zm96-96H150a6 6 0 0 1-6-6V54a6 6 0 0 1 6-6h308a6 6 0 0 1 6 6v308a6 6 0 0 1-6 6z" fill="currentColor"></path>
</svg> <img alt="Afgelopen vrijdag hebben we weer genoten van onze jaarlijkse bbq. We hebben deze dag ook afscheid genomen van een aantal stagiaires." src="https://www.boerderijhooghei.nl/wp-content/plugins/instagram-feed/img/placeholder.png"/>
</a>
</div>
</div><div class="sbi_item sbi_type_carousel sbi_new sbi_transition" data-date="1591470011" id="sbi_17881796944625432">
<div class="sbi_photo_wrap">
<a class="sbi_photo" data-full-res="https://scontent-ams4-1.cdninstagram.com/v/t51.2885-15/101689336_713713639426866_5724451339492124771_n.jpg?_nc_cat=107&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KdUPjE-b-SEAX8xxsyS&amp;_nc_ht=scontent-ams4-1.cdninstagram.com&amp;oh=d23782b6e4ff2b679b0b818c09ca7910&amp;oe=60265159" data-img-src-set='{"d":"https:\/\/scontent-ams4-1.cdninstagram.com\/v\/t51.2885-15\/101689336_713713639426866_5724451339492124771_n.jpg?_nc_cat=107&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KdUPjE-b-SEAX8xxsyS&amp;_nc_ht=scontent-ams4-1.cdninstagram.com&amp;oh=d23782b6e4ff2b679b0b818c09ca7910&amp;oe=60265159","150":"https:\/\/scontent-ams4-1.cdninstagram.com\/v\/t51.2885-15\/101689336_713713639426866_5724451339492124771_n.jpg?_nc_cat=107&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KdUPjE-b-SEAX8xxsyS&amp;_nc_ht=scontent-ams4-1.cdninstagram.com&amp;oh=d23782b6e4ff2b679b0b818c09ca7910&amp;oe=60265159","320":"https:\/\/scontent-ams4-1.cdninstagram.com\/v\/t51.2885-15\/101689336_713713639426866_5724451339492124771_n.jpg?_nc_cat=107&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KdUPjE-b-SEAX8xxsyS&amp;_nc_ht=scontent-ams4-1.cdninstagram.com&amp;oh=d23782b6e4ff2b679b0b818c09ca7910&amp;oe=60265159","640":"https:\/\/scontent-ams4-1.cdninstagram.com\/v\/t51.2885-15\/101689336_713713639426866_5724451339492124771_n.jpg?_nc_cat=107&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KdUPjE-b-SEAX8xxsyS&amp;_nc_ht=scontent-ams4-1.cdninstagram.com&amp;oh=d23782b6e4ff2b679b0b818c09ca7910&amp;oe=60265159"}' href="https://www.instagram.com/p/CBGsY5ypPiC/" rel="noopener nofollow" target="_blank">
<span class="sbi-screenreader">Gestart met de bouw van een nieuwe schuur, en een </span>
<svg aria-hidden="true" aria-label="Clone" class="svg-inline--fa fa-clone fa-w-16 sbi_lightbox_carousel_icon" data-fa-pro="" data-icon="clone" data-prefix="far" role="img" viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
<path d="M464 0H144c-26.51 0-48 21.49-48 48v48H48c-26.51 0-48 21.49-48 48v320c0 26.51 21.49 48 48 48h320c26.51 0 48-21.49 48-48v-48h48c26.51 0 48-21.49 48-48V48c0-26.51-21.49-48-48-48zM362 464H54a6 6 0 0 1-6-6V150a6 6 0 0 1 6-6h42v224c0 26.51 21.49 48 48 48h224v42a6 6 0 0 1-6 6zm96-96H150a6 6 0 0 1-6-6V54a6 6 0 0 1 6-6h308a6 6 0 0 1 6 6v308a6 6 0 0 1-6 6z" fill="currentColor"></path>
</svg> <img alt="Gestart met de bouw van een nieuwe schuur, en een overkapping in de groentetuin." src="https://www.boerderijhooghei.nl/wp-content/plugins/instagram-feed/img/placeholder.png"/>
</a>
</div>
</div><div class="sbi_item sbi_type_image sbi_new sbi_transition" data-date="1589181305" id="sbi_17860529851875371">
<div class="sbi_photo_wrap">
<a class="sbi_photo" data-full-res="https://scontent-amt2-1.cdninstagram.com/v/t51.2885-15/96405028_1629309367232555_755834783091308237_n.jpg?_nc_cat=109&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=BPaPHFHVEtwAX9gNmvW&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=fff1ced82bc8dd4811dea60fb5e215f0&amp;oe=6026DBB3" data-img-src-set='{"d":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.2885-15\/96405028_1629309367232555_755834783091308237_n.jpg?_nc_cat=109&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=BPaPHFHVEtwAX9gNmvW&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=fff1ced82bc8dd4811dea60fb5e215f0&amp;oe=6026DBB3","150":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.2885-15\/96405028_1629309367232555_755834783091308237_n.jpg?_nc_cat=109&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=BPaPHFHVEtwAX9gNmvW&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=fff1ced82bc8dd4811dea60fb5e215f0&amp;oe=6026DBB3","320":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.2885-15\/96405028_1629309367232555_755834783091308237_n.jpg?_nc_cat=109&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=BPaPHFHVEtwAX9gNmvW&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=fff1ced82bc8dd4811dea60fb5e215f0&amp;oe=6026DBB3","640":"https:\/\/scontent-amt2-1.cdninstagram.com\/v\/t51.2885-15\/96405028_1629309367232555_755834783091308237_n.jpg?_nc_cat=109&amp;ccb=2&amp;_nc_sid=8ae9d6&amp;_nc_ohc=BPaPHFHVEtwAX9gNmvW&amp;_nc_ht=scontent-amt2-1.cdninstagram.com&amp;oh=fff1ced82bc8dd4811dea60fb5e215f0&amp;oe=6026DBB3"}' href="https://www.instagram.com/p/CACfB4PJ0fz/" rel="noopener nofollow" target="_blank">
<span class="sbi-screenreader">We gaan weer beginnen! 🎉</span>
<img alt="We gaan weer beginnen! 🎉" src="https://www.boerderijhooghei.nl/wp-content/plugins/instagram-feed/img/placeholder.png"/>
</a>
</div>
</div> </div>
<div id="sbi_load">
<span class="sbi_follow_btn">
<a href="https://www.instagram.com/boerderij_hooghei/" rel="noopener nofollow" target="_blank"><svg aria-hidden="true" aria-label="Instagram" class="svg-inline--fa fa-instagram fa-w-14" data-fa-processed="" data-icon="instagram" data-prefix="fab" role="img" viewbox="0 0 448 512">
<path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" fill="currentColor"></path>
</svg>Volgen op Instagram</a>
</span>
</div>
<span class="sbi_resized_image_data" data-feed-id="sbi_17841431005609623#20" data-resized='{"17860529851875371":{"id":"96405028_1629309367232555_755834783091308237_n","ratio":"1.00","sizes":{"full":640,"low":320}},"17881796944625432":{"id":"101689336_713713639426866_5724451339492124771_n","ratio":"1.00","sizes":{"full":640,"low":320}},"18043268812249022":{"id":"109132349_146929180344889_3654288394011287857_n","ratio":"1.00","sizes":{"full":640,"low":320}}}'>
</span>
</div>
</aside><!-- .widget --></div> </div><!-- #sidebar-top -->
<div class="column twocol">
<div class="widget-area" id="sidebar-right" role="complementary">
<div class="column onecol"><aside class="widget widget_text" id="text-11"> <div class="textwidget"></div>
</aside><!-- .widget --></div> </div><!-- #sidebar-right -->
</div><!-- .twocol -->
</div><!-- #sidebar --> </div><!-- #container -->
<div id="footer">
<div id="copyright">
<p class="copyright twocol">© Boerderij Hooghei 2021 </p>
<div class="clear"></div>
</div><!-- #copyright -->
</div><!-- #footer -->
</div><!-- #wrapper -->
<script type="text/javascript">

	var iCWP_WPSF_Recaptcha = new function () {

		var bInvisible = false;

		this.setupForm = function ( oForm ) {

			var recaptchaContainer = oForm.querySelector( '.icwpg-recaptcha' );

			if ( recaptchaContainer !== null ) {

				var recaptchaContainerSpec = grecaptcha.render(
					recaptchaContainer,
					{
						'sitekey': '6Le8JCkUAAAAAOpp2HPL3aZlmVxV8sHFC4qrUQwg',
						'size': '',
						'theme': 'light',
						'badge': 'bottomright',
						'callback': function ( reCaptchaToken ) {
													},
						'expired-callback': function () {
							grecaptcha.reset( recaptchaContainerSpec );
						}
					}
				);

							}
		};

		this.initialise = function () {
			if ( grecaptcha !== undefined ) {
				for ( var i = 0; i < document.forms.length; i++ ) {
					this.setupForm( document.forms[ i ] );
				}
				/**
				 * For some crazy reason invisible recaptcha badge attaches to div with this class.
				 * Fortunately removing the class at this stage doesn't interrupt normal behaviour.
				 */
				if ( bInvisible ) {
					document.querySelector( 'form' ).classList.remove( 'shake' );
				}
			}
		};
	}();

	var onLoadIcwpRecaptchaCallback = function () {
		iCWP_WPSF_Recaptcha.initialise();
	};
</script><!-- Instagram Feed JS -->
<script type="text/javascript">
var sbiajaxurl = "https://www.boerderijhooghei.nl/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.boerderijhooghei.nl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.boerderijhooghei.nl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"nl","strings":{"mejs.download-file":"Bestand downloaden","mejs.install-flash":"Je gebruikt een browser die geen Flash Player heeft ingeschakeld of ge\u00efnstalleerd. Zet de Flash Player-plugin aan of download de nieuwste versie van https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Volledig scherm","mejs.play":"Afspelen","mejs.pause":"Pauzeren","mejs.time-slider":"Tijdschuifbalk","mejs.time-help-text":"Gebruik de Links\/Rechts-pijltoetsen om \u00e9\u00e9n seconde vooruit te spoelen, Omhoog\/Omlaag-pijltoetsen om tien seconden vooruit te spoelen.","mejs.live-broadcast":"Live uitzending","mejs.volume-help-text":"Gebruik Omhoog\/Omlaag-pijltoetsen om het volume te verhogen of te verlagen.","mejs.unmute":"Geluid aan","mejs.mute":"Afbreken","mejs.volume-slider":"Volumeschuifbalk","mejs.video-player":"Videospeler","mejs.audio-player":"Audiospeler","mejs.captions-subtitles":"Ondertitels","mejs.captions-chapters":"Hoofdstukken","mejs.none":"Geen","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanees","mejs.arabic":"Arabisch","mejs.belarusian":"Wit-Russisch","mejs.bulgarian":"Bulgaars","mejs.catalan":"Catalaans","mejs.chinese":"Chinees","mejs.chinese-simplified":"Chinees (Versimpeld)","mejs.chinese-traditional":"Chinees (Traditioneel)","mejs.croatian":"Kroatisch","mejs.czech":"Tsjechisch","mejs.danish":"Deens","mejs.dutch":"Nederlands","mejs.english":"Engels","mejs.estonian":"Estlands","mejs.filipino":"Filipijns","mejs.finnish":"Fins","mejs.french":"Frans","mejs.galician":"Galicisch","mejs.german":"Duits","mejs.greek":"Grieks","mejs.haitian-creole":"Ha\u00eftiaans Creools","mejs.hebrew":"Hebreeuws","mejs.hindi":"Hindi","mejs.hungarian":"Hongaars","mejs.icelandic":"IJslands","mejs.indonesian":"Indonesisch","mejs.irish":"Iers","mejs.italian":"Italiaans","mejs.japanese":"Japans","mejs.korean":"Koreaans","mejs.latvian":"Lets","mejs.lithuanian":"Litouws","mejs.macedonian":"Macedonisch","mejs.malay":"Maleis","mejs.maltese":"Maltees","mejs.norwegian":"Noors","mejs.persian":"Perzisch","mejs.polish":"Pools","mejs.portuguese":"Portugees","mejs.romanian":"Roemeens","mejs.russian":"Russisch","mejs.serbian":"Servisch","mejs.slovak":"Slovaaks","mejs.slovenian":"Sloveens","mejs.spanish":"Spaans","mejs.swahili":"Swahili","mejs.swedish":"Zweeds","mejs.tagalog":"Tagalog","mejs.thai":"Thais","mejs.turkish":"Turks","mejs.ukrainian":"Oekra\u00efens","mejs.vietnamese":"Vietnamees","mejs.welsh":"Wels","mejs.yiddish":"Jiddisch"}};
</script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/imagesloaded.min.js?ver=3.2.0" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/masonry.min.js?ver=3.3.2" type="text/javascript"></script>
<script async="async" defer="defer" src="https://www.google.com/recaptcha/api.js?hl=nl-NL&amp;onload=onLoadIcwpRecaptchaCallback&amp;render=explicit&amp;ver=5.4.4" type="text/javascript"></script>
<script src="https://www.boerderijhooghei.nl/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var sb_instagram_js_options = {"font_method":"svg","resized_url":"https:\/\/www.boerderijhooghei.nl\/wp-content\/uploads\/sb-instagram-feed-images\/","placeholder":"https:\/\/www.boerderijhooghei.nl\/wp-content\/plugins\/instagram-feed\/img\/placeholder.png"};
/* ]]> */
</script>
<script src="https://www.boerderijhooghei.nl/wp-content/plugins/instagram-feed/js/sbi-scripts.min.js?ver=2.6.2" type="text/javascript"></script>
</body>
</html>