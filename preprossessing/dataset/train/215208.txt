<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "54035",
      cRay: "6110df59df9824d3",
      cHash: "265affe4676b234",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmctd2lraS5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "WjqKoQItwRKuatUou8+62C9g6mFNOdF6BZ8u9k0TAXr9LJi76UZSPF3mI54XJC6LTzHwqls955wlPr+Wte9WhkZf5crG8HBMcDt76esFJEZZ6QxzD0fTwfuBw+3zftPqyob2is43Dz4Sy79zq/WULU+eJB0yC7jLLgPAYlXf2FKnM8xkHNplfWUkNcstk+NRZ70McPdAh2LnYzxkaP2NEIDYR2j5eUw4k4VfNhCqwdgztqNFDT+8zbkIxVL8Gj5Uk3ieQyyGAO/dfOe/ocHcNx66wgxJeGlfU6mYyns/KKCGhG76W7yXFzrj2NSiq2T7ow7m9HWE1g4Wk8F66GrZ9vXGEJaNafKex7XDxgHZzZyquWLQcTSM82zBewcWahTr25MN4WimKZQi43AaaEIPHV2tiX5hF24NxJHG9n6c7HQxROQR/NQD9VuBH+VXoqowyihMtIqYY383b8WnCS88QKYzWAIgqSzAHAvFZYHNzCDxImaYbBYR7u6pq0A1SpQE7+tRlDZ9jGRhS+BzVPNPPdQ1/J9S52AANH8rIciJDAJEp7/kZeIDJU2ufG2fOCjYUDnQB40e821aagEQ4ndDn+2LAJoeNlrUFDFd3OIwCt7BbMq0+mxYvdH5eEF5wcGlWRFhgt96DVFjn/gvXugmhrMJNRenT6r+Pebu72+O3ifXnTdpXBH1CtP/G5vwYtKEclg6Bahy3mdhWrQo2gKCO7Qf+L65IyZd/PTkw5au789HAVSGp+QBs/4D4bjjAL3Q",
        t: "MTYxMDU1OTMxMi45MzYwMDA=",
        m: "T1nDvea+8EWNZ/ckLxM1WJ3Vu5ZGUvL+OoL1wDJsqyM=",
        i1: "p5/gU2UDZ+rsP0nXEroy7g==",
        i2: "PWQHQPuEpsbkQoQjviOWyw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "BjLdLXUFhnAUIfS6k4L1vvwQyIcgyzfiChK+tJnKi2c=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6110df59df9824d3");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bg-wiki.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=a0a7a95966779b73ae1163b9a2be28af95b7a745-1610559312-0-AWO5EzNYKmvEL5dqqleQjgNVo4dYiuM8FakM8wotDI5y8n65pIDbVcrVIPv7KSUPkBK025GAlBlBWNSlK3szCT8DaEkWEvJojaFa8ztiZByZlkFQ9UpdHrXJNuqD6vTrpOUkMALlhgT9GZtybEMC5aZQe0xv15ZF1aWI-p0bDaTgUsattMqpj83fgXnBTe8firsdp9PHflbwjrarxQbWM3F0ykctd2KrD0u1xKxzP53Aq99SLysEZRhgb8hd4o2nUnX0SwdVJAXy9VjlaVBREwYES9f1SVeDp6OS51QgG44feyVc3WVQrV7zGzc7MbabLg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="f8efe57633a5dc820a7ecd2d075c93ebd2edc908-1610559312-0-ASfxWnjiCLMdQBzfbrecP0Gq0ldrEEJu73kzbwHHc7hA5cKfmMAHBfvhuHEfwxZDWCezuq4RnxKa8h0bUNr2/EMLw1kAcAz+MxCmISTTtDGZ0IDmTz355Ndtt+j4IyVymdqv36/OI6pWQtYGVqnKCJRG0rx4HFYwPPPz0CUgVVwcp2TxRSD0B/WQpHJKKYZAT7Lv7yluN+PHjJ0Jq89bWwRVkXIjz5ajgV304EjaHgi2D+Pj3sWuhc7MeBnp4AAowTom972JybXBLWw3pUCPt/D4MZvJgjpyn6AT3rSJTehjetNHfva9DIeK2VhKJ6Zt1aOXNKEmY57rKd8dA0he1Zly09BvUjUIvijG69HQfmPd2gZbYCs9qLoQwMVOdnBQ37SqVhGAbN/wdNJ18KFiluWekaBqQYN3cGlPbbZdvde9tMDQPKIvcR8zswZaaq/uQE6X0cNicnzVINbDZZ5lUntRzRCvfr+Ej3yaYvD2vy2tXJ0YDBzHSWLU5thyAFSd2oaLtPIaITS3o24CY5Bb9aUwAusER4IC8MG+550i+BIV+Ya1FmFhOrDrgSrOYo6qwL9Ll1EosQZoil3D7AqT4Z9hZHW6qCvCGXWPRoLERcyvEmxwAt7cBiXBEAmM/QIYfqr8YtvwiLMPOZXpG4ecU/Y5Ko6cBH0aAJUTX7rRKi9r4x8pB7ove/5vklsvdDlfmoDw5BTXt20m1XoRYc0tvUquG2BCy3bnqOjrHkgfK0w8yPohAj+ZYlDvhVjjK2LvJzCIJBw8GAqQMHXIFYmkbALcNds8kbeQ6DgckLqt7mbCAqm1rsItrTK1xg/xn3D+h850oqFt5iGUo94W3yx3nbNgMb5foMScryskjyZ6R8oTAJH3HiUw89xWc1r+Yz5rWv4MWyBxL/KrKpmapoLpePLDcQLGtd+A6XQ057wt2l/TjHfs2sZraTQl5qzKy4jaOyBsV+7jlQE/Y1czT9sPpfpXucAoIvzOldeDSXY8+oqtgVrvqwVDKSjCK33HbgoCTEwHbJEpjNS3c7BpjEvaYUGj67Pn3iqL1BTo9/qgGJb6Met+kVPHBZNVxl+oytrLNQph2FqpYY6tXugTydxRoi6LynlvjYTmVaYPOjuYY19fxSdgH4IB6skt7UacmeWAdlvCCxPnyYrPCiiEnO3rQMDRODofjEtTybXxEKuSi5PjSh+SakzJCgidwoSeHNtSk6q4ocOrykJIETtVB1cDJFbtJaHOV7fmF1Ql6GaGmZztbuYmKy5ts/MpPKumBM4eHuqnqBKcfIiXIEdkYSdFOMG0bUGkLtGECrWz1JdjRIueN4k156c2D/8Gc/y1he3H5w=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="cc7252d072473692bb33dc783a14a91e"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610559316.936-sdEyQWPOcW"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6110df59df9824d3')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6110df59df9824d3</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
