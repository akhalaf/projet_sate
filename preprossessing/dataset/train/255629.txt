<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Yog Sadhana</title>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery.min.js"></script>
<!-- slider start -->
<link href="css/style1.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="js/jquery.min.js" type="text/javascript"></script>
<script language="javascript" src="js/jquery.easing.min.js" type="text/javascript"></script>
<script src="js/jquery.easy-ticker.js" type="text/javascript"></script>
<!-- slider end -->
<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css" rel="stylesheet"/>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="wrapper1">
<article>
<div class="wrapper">
<nav>
<?php include('include/top_nav.php')?>
</nav>
<div class="clear"></div>
<header>
<style>
        
        #slideshow {
  margin: 80px auto;
  position: relative;
  
  height: 380px;
  padding: 10px;
  
}

#slideshow > div {
  position: absolute;
  top: 10px;
  left: 10px;
  right: 10px;
  bottom: 10px;
  border-radius: 15px;
}
    </style>
<script>
            $("#slideshow > div:gt(0)").hide();

setInterval(function() {
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
}, 3000);
        </script>
<div id="slideshow">
<div>
<img src="images/header1.jpg"/>
</div>
<div>
<img src="images/header2.jpg"/>
</div>
<div>
<img src="images/header3.jpg"/>
</div>
</div>
</header>
<img src="images/shadow.png"/>
<div class="clear"></div>
<article>
<div class="mid_con" style="margin-top:0px !important">
<div class="tesmonial">
<div class="tesmonial_text">
<p class="tesmonial_text1">We are what our thoughts have made us; so take care about what you think. Words are secondary. Thoughts live; they travel far. <strong>Swami Vivekananda</strong></p>
<p class="tesmonial_text1">Talk to yourself once in a day .Otherwise you may miss meeting an excellent person in this world <strong>Swami Vivekananda</strong></p>
<p class="tesmonial_text1">Soldier may fight a thousand battle and win a thousand war , but he  who conquers himself is the real victor <strong>Shri Budha</strong> </p>
<p class="tesmonial_text1">You should reform yourself but do not degrade yourself . You are your best friend and you can become your own enemy too .. <strong>Bhagavat Gita</strong> </p>
</div>
<script type="text/javascript">
				(function() {
				
					var quotes = $(".tesmonial_text1");
					var quoteIndex = -1;
					
					function showNextQuote() {
						++quoteIndex;
						quotes.eq(quoteIndex % quotes.length)
							.fadeIn(2000)
							.delay(2000)
							.fadeOut(2000, showNextQuote);
					}
					
					showNextQuote();
					
				})();
			</script>
</div>
<div class="clear"></div>
<div class="container">
<div class="what_new">
<div class="title" style="background-color:#28143e; color:#FFF">Testimonials</div>
<p><img src="images/img2_1.jpg"/></p>
<div class="left what_new_con" style="width:88%; margin-left:5px">
<p class="justify">I had diabetes. So healing of wound was the biggest problem. After taking medicines for a long time it started showing its side effects. I was so depressed in life, my wife, who is a follower of bramhavidya always used to suggest me to join bramhavidya, but I was ignoring it. Once i had gone to Bangalore with my family, while returning I kept vomiting  throughout my journey. Later i visited doctor and had</p>
</div>
<p> </p>
<p><a class="read_more" href="testimonial.php" style="padding:10px; margin-bottom:20px">Read More</a></p>
</div>
<div class="what_new">
<div class="title" style="background-color:#04475d; color:#FFF">Latest News</div>
<p><img src="images/img2.jpg"/></p>
<div class="left what_new_con">
<!--<script src="js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>-->
<script src="js/jquery.easy-ticker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	var dd = $('.vticker').easyTicker({
		direction: 'up',
		easing: 'easeInOutBack',
		speed: 'slow',
		interval: 5000,
		height: 'auto',
		visible: 1,
		mousePause: 0,
		controls: {
			up: '.up',
			down: '.down',
			toggle: '.toggle',
			stopText: 'Stop !!!'
		}
	}).data('easyTicker');
	
	cc = 1;
	$('.aa').click(function(){
		$('.vticker ul').append('<li>' + cc + ' Triangles can be made easily using CSS also without any images. This trick requires only div tags and some</li>');
		cc++;
	});
	
	$('.vis').click(function(){
		dd.options['visible'] = 1;
		
	});
	
	$('.visall').click(function(){
		dd.stop();
		dd.options['visible'] = 0 ;
		dd.start();
	});
	
});
  </script>
<style>
.vticker{
  }
.vticker ul{ 
	padding: 0;
}
.vticker li{
	list-style: none;
 	padding:10px  0 0 0 ; font-family:'OpenSansRegular'; color: #666; font-size:13px;
}
.et-run{
	background: red;
}
  </style>
<div class="what_new_text2 justify" style="width:99%; height:150px; margin-left:-10px">
<button class="up submit_small">UP</button>
<button class="down submit_small">DOWN</button>
<div class="vticker">
<ul>
<li>Short term Basic course at Kudal kelbai Mandir on May 24/25 ,May 31st/June 1st, June 14th/15th , June 22nd. Saturday 2-6 pm,Sunday 8-12 noon. Sanskar classes for children (Age 8-16 ) will be at same place , same timing ,except June 22nd. Contact - 09422116521</li>
<?php #$obj->getMediaListing('','front_listing'); /*<li>Short term Basic course at Kudal kelbai Mandir on May 24/25 ,May 31st/June 1st, June 14th/15th , June 22nd. Saturday 2-6 pm,Sunday 8-12 noon. Sanskar classes for children (Age 8-16 ) will be at same place , same timing ,except June 22nd. Contact - 09422116521</li>*/?&gt;
                  </ul>
</div>
</div>
<p class="what_new_text2 justify" style="width:95%; margin-left:-3px">
<?php # echo $obj->getTickerListing('','front_listing');
						/* Short term Basic course at Kudal kelbai Mandir on May 24/25 ,May 31st/June 1st, June 14th/15th , June 22nd. Saturday 2-6 pm,Sunday 8-12 noon. Sanskar classes for children (Age 8-16 ) will be at same place , same timing ,except June 22nd. Contact - 09422116521*/ ?&gt;
              </p>
<p> </p>
<p> </p>
<p> </p>
</div>
<!--<p><a href="about_guruji.php" class="read_more" style="margin-left:15px; padding:10px; margin-bottom:20px">Read More</a></p>-->
</div>
<div class="quick_enquiry">
<?php include('include/courses_enquiry.php')?>
</div>
</div>
</div>
</article>
</div>
</article>
<footer>
<?php include('include/footer.php')?>
</footer>
</div>
</body>
</html>
