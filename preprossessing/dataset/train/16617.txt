<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "66507",
      cRay: "610ae8a2bba1ddc7",
      cHash: "08748028bf6f1d4",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuMWEubHYv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "7FIa3Rdn1YWITsCZkCSzNF6DDPSAmS2jwDqeUCxB3sBNIuup6a/wIZAkvRB0twHk1CqxAEpdPIBwiKKUnAtseiKNFUeKnFxlhGCsc3CjGG1EwvNewrRrWxuyIeN9ek8K6mfGN82cXU5PBUziOblOX/DcE3Ch2nToN9gDWz3H3rFFSAhBQ7ZPDgo+vgAdZtKtm4HSjIPYj9+CvuLHI5LjAceSxNt2Zk8q+1yEdBdE+8N7XEIQM3Oj3CHkPvppOpbISfwGWOi0rm3v6kWSAvTerKRSUc75RuGLfhA1u1YM+tOAOFkZ1zPNVY4xkEL/AWNBaAouJH4ttIIbKjJuoaKAXhvHPZjKcsPj1ldaBxHukKaxFeKyGTG46IAlW/omu26UrYSKCHd+hmAcd5Id9xAYLOOgLXJn4vYTv4wQ6z+bB2NGkXabPd/F+/ZnLD4Ue9TYVW5HYKFGHoYxDKzum4PrjCiMG2y/IrV6q1d/FLkvIKq5b0MVGnJymhb3/ZoYvpTMIj/3csmqpJbio4i/4tIMot8gG6pUWz2k0TzZ7wOhtgPy08pLKy/J9cjouePAMbIHa5pmxUeuEhV6hRHer1V6B5mF7JV2Bfm5YEk0Mw+Fwx9jdk8HjffR/JLU8zNFqNyxXdozCVbVCfrXHXFD/gNHff0fpngGB1iOrBW98DNo16b/LmzjkAERlDdH9DDtT8cBQpwbcq1abDF7BASWBGcJIiBPpB4v2oYifHjcru7W2lqrG1bZHVeqC3pCYitX+cQ0",
        t: "MTYxMDQ5Njc3OC42ODUwMDA=",
        m: "EM1AgLinRh1hgq11HohFjgGnMmqfvjwrvBjuUzi6Xy8=",
        i1: "NPK2/vLRv9db9AcWTqB8zw==",
        i2: "vSRgg+lto3UEJGJDK9LfLQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "A5WTuIPXyKSNWMKQbsv5y2H3D9XIb29OyNNYfW5FOdI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.1a.lv</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=3f39bb4dbfdf67e27494848fd59c80e98b9b1c63-1610496778-0-AbdJ_7hhQijBrvwyP_fa-uloFb7txMm1ZXuolCuN76QaaxGkx-Sl2oCzhwhotD92Sltok4lOsWeN5M0PCWIKauBNm5107Yj7T-2evPuAD5jGxD-sfhnIkt-QTRiRZAF6sQEjxPMvRsNVbz8Ii1ehcn3YpFyZoIEY-4puZC9YhQDgVyIa39jRld7f0Nt0Lo-qHSjqlzAVFNBDzZqth72umY_7trqocRyiPQD_HiaNPDHpzBCxjtO0eaEcHQMRrTqVd5cJ6fy-HoxouQ-8XL1eSX0UKZYpessDiimVJOqL71RKNmqPDLfHi-myYJw4vHacQwb18bYxXzY95EJ4RJ1kggOuX4qqCLeM2pN7ArgeBXEQ0cHteSURtcfiDPxFSnqcTnra0mxz_J1vDyIEZjQjHhZ6R-EGCznY1M1x4QFisv-TYWjaQPErEOxiscjjGQhK1R-45XY1pddcgB9rBzMn4rc9JpRFPLHSyWpLa4dPB1vdMdBtKPDXJ_9LQTeUNYzptYrMMmr6xcOSfgm7Ixkr6AI" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="6ba21775e687b24a2adc6297c31634f78922a1bc-1610496778-0-AUL8VUGb62PJu91PZ3G3qwMG6HJwFFYE1jJnJzTMMNJ6PxUhAeIi3iTsjZv3FDeN68amCPIZ56d8yFDLeQhoKxuPZUNrlvjRlvI2Awp9pJRR9yAf37tRLDn5tZhaIGnNz+438Uk0+WcxFMHgHfsBO5WGXGmYTjxwBMKN/wSQMs+sraekD/Q9yyLaZVsc2OJF+M6VV/fE9y4avnNQcdnpigOl80mizberW17qLMYKGJUXh7ORaPEzohZoobOAoGzCaIU+2E0ykH4TAUPyGrZw1bMFvGON1x/axxMPFJHDTNBqZAvew9Z4Pu1QTxMHaVZ06CRgV4ruSPc+Bs8Za6leK11cGfAWe8VZWMLJz5zGGCIFYwZTR9O/CIx8TjrCHK3MN5UNDMsSTYiiifck429ip+M6mtkQ3TifJvOSxJbSRb86Dz9HVgEDa8p5z1YsPEbITJfaYma7dKhDa9487kWoXj7T/4hKfdiI6w6xFlfqC87gdHDMXyZFh2WPrvC6K1ryccSo/i8ArTE7lZ2mqymxsayPk3RQsVsNQv11qzhsx7PEpI2JYRWDhQ+mklwtHWPmzs8EW0E02yIfBs+3jjy4gbbACRlQKZwYms2V6yosRaO4Yk0N1Lto5fNmkL8AJdYFrDu0Ai7V2THU8N6vYx4SPvfh01lJM13TIHp/xUYn0BO737/J4fORIu1YgGiUtcbsWe7qg7BNz4DMZYC6Zq9dBk99A3GtJ7iMT3nKdtA2tXm4r6mrd/Ttxk+Ah/nTQHP5sou2zDlnLj6HQDPKaYZfKJ+jBVLq+vuF+6OCbB11B4VU0A8SpS1JnI+QCDpivdUxEuZCa0mlBmGub0jjhpKOijy0KNo5iNI2CxCsCFwiZQz6+iuEqC2v/53uUWMAN5wqFAUHc6LoXHD1mioFe6jUflNELFtbau4GC8fcVUcAB0zOuhoNv7Fk3m0WlWTp/tmUsgTaoKNvz2cHBwq+4AanD+AHrclO7v1Y7My8FtIVQKzHolWlbNZhDVuYxXqB8S+Pa4sRjwoq+a32uo/kJ4qEEdj8R5/p7f7dUtils74lAIbQe9zxLNN84+NuiXLz+dSodIF3shdEcMCrx17EKKsJ3jgWLhcXJqxRtmJVq/CHOFDJZq5oKWPMwxKMoTP/uTcpcxa0CxmI2EPNVB1t7vSHDJxtrQEb8D5eITXG2RezLh/jEgI+3eRGwrig6rfGhKE++3PNCCZjVyKGfMrnzFvwu1EAx8TxJ6N/UOSNYokpzt0opEOUAzORlF10tZ6NdjejgwwUCWxBSEmhpuEWCWIp3G3oBu0fWfQ/voov+23BZSzw"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="2a0d34b070becdb53b389c923f90ef99"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ae8a2bba1ddc7')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ae8a2bba1ddc7</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
