<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Advads - Реклама и заработок</title>
<meta content="Реклама и заработок" name="description"/>
<meta content="" name="keywords"/>
<link href="/css/font-awesome.css" rel="stylesheet"/>
<meta content="width=1175, maximum-scale=3" name="viewport"/>
<link href="/css/grid.css" rel="stylesheet"/>
<link href="/css/font-awesome.css" rel="stylesheet"/>
<link href="/css/style.css?v7" rel="stylesheet"/>
<script type="text/javascript">function DocumentReady(callback){window.addEventListener('DOMContentLoaded', callback);};</script>
<script type="text/javascript">
	var uid = 0;
	var vk_api_id = '6651289';
	</script>
<script src="/js/jquery-2.2.1.min.js"></script>
<script src="/js/core.js?vn1"></script>
<script src="/js/script.js?vn4"></script>
</head>
<body>
<header>
<div class="row flex jcsb aic">
<a class="logo" href="/"></a>
<div class="menu flex jcsb xl_50 hed">
<a href="/regestrator">Регистрация</a>
<a href="/index#recl_bl" onclick="$('html, body').animate({ scrollTop: $('#recl_bl').offset().top }, 700); return false;">Рекламодателям</a>
<a href="/index#ispls_bl" onclick="$('html, body').animate({ scrollTop: $('#ispls_bl').offset().top }, 700); return false;">Исполнителям</a>
<a href="/index#allstats_bl" onclick="$('html, body').animate({ scrollTop: $('#allstats_bl').offset().top }, 700); return false;">Статистика</a>
</div>
<div class="inline_right hif ">
<a class="btn btn_one" href="/avtoriz">Авторизация</a>
</div>
</div>
</header>
<script src="/js/awesomechart.js" type="text/javascript"></script>
<div id="back-top"><a class="arrow_01" href="#top" title="Вверх"><span></span></a></div>
<div id="hero">
<div class="row">
<div class="regs_bl" id="regs_bl">
<h1 class="h1">Рекламный сервис с трендовым будущим</h1>
<p class="text">Рекламный сервис <span class="color_otr">ADVADS.net</span> предлагает всем предпринимателям и частным лицам доступ к уникальному живому трафику. Благодаря ему, в кратчайшее время можно найти множество клиентов для своего бизнеса и получить достойный заработок без значительных вложений. Реклама – двигатель любого бизнеса, будь то раскрутка сайта, арбитраж трафика или разные виды продаж. На нашем сервисе Вы найдете ряд рекламных форматов, заказав которые, существенно увеличите число своих клиентов и размер чистого дохода. Мы регулярно проводим отбор и настройку внутренних условий сервиса, поэтому в распоряжении наших клиентов находится исключительно качественный трафик. А это – занимающиеся интернет-серфингом люди, потенциальные потребители Ваших товаров, работ и услуги.
Ваше рекламное объявление ожидают <span class="color_otr" id="">225484</span> заинтересованных клиентов</p>
</div>
<div class="recl_bl" id="recl_bl">
<p class="text">Сервис <span class="color_otr">ADVADS.net</span> предоставляет рекламодателям ряд форматов онлайн-рекламы: переход после просмотра рекламных объявлений, просмотр рекламы во фрейме (frame), с установленным временем, защитой от автокликеров и иными функциями. Используя форматы нашего сервиса, Вы сможете быстро раскрутить свой сайт, повысить продажи товаров и услуг, значительно продвинуть бизнес. Вы наверняка получите конкурентные преимущества, новых клиентов и заработок без особых вложений, практически не прилагая никаких собственных усилий. На ADVADS для рекламодателей имеется четыре тарифа со своими настройками. Для заказа рекламы бизнеса следует выбрать подходящий тариф и заполнить пустующие поля. Всё – просто. А мы подстроимся под Вас на любых условиях.</p>
<div class="tariff_bl flex fww jcc">
<div class="item t_mini flex fdc">
<p class="heading">Тариф mini</p>
<ul class="list">
<li>Время просмотра <span class="color_otr">15 сек.</span></li>
<li>Переходы после просмотра.</li>
<li>Защита от автокликеров.</li>
</ul>
<div class="stats_f mta">
<p><span class="color_otr">30 руб.</span> за 1000</p>
<p>просмотров</p>
</div>
</div>
<div class="item t_standart flex fdc">
<p class="heading">Тариф standart</p>
<ul class="list">
<li>Время просмотра <span class="color_otr">20 сек.</span></li>
<li>Переходы после просмотра.</li>
<li>Защита от автокликеров.</li>
<li>Выделение в списке.</li>
</ul>
<div class="stats_f mta">
<p><span class="color_otr">50 руб.</span> за 1000</p>
<p>просмотров</p>
</div>
</div>
<div class="item t_vip flex fdc">
<p class="heading">Тариф vip</p>
<ul class="list">
<li>Время просмотра <span class="color_otr">40 сек.</span></li>
<li>Просмотр в активном окне.</li>
<li>Переходы после просмотра.</li>
<li>Защита от автокликеров.</li>
<li>Выделение в списке.</li>
</ul>
<div class="stats_f mta">
<p><span class="color_otr">80 руб.</span> за 1000</p>
<p>просмотров</p>
</div>
</div>
<div class="item t_max flex fdc">
<p class="heading">Тариф max</p>
<ul class="list">
<li>Время просмотра <span class="color_otr">60 сек.</span></li>
<li>Просмотр в активном окне.</li>
<li>Переходы после просмотра.</li>
<li>Защита от автокликеров.</li>
<li>Максимальный эффект.</li>
<li>Выделение в списке.</li>
</ul>
<div class="stats_f mta">
<p><span class="color_otr">120 руб.</span> за 1000</p>
<p>просмотров</p>
</div>
</div>
</div>
</div>
<script>

				function recalc()
				{
					var rubl = $("#calc_read").val();
					var results = rubl * 33;
					$("#calc_result").val(results);
				}

				$(document).ready(function(){

					$("#calc_read").keyup (function ()
					{
						recalc()
					});

					$("#calc_read").click (function ()
					{
						recalc()
					});

				});
			</script>
<div class="ispls_bl" id="ispls_bl">
<p class="text">Рекламный сервис <span class="color_otr">ADVADS.net</span> предлагает исполнителям хороший заработок, а также простую и удобную систему оплаты труда. Эта система предполагает фиксированные цены и гарантирует немедленный перевод денег на счет после исполнения задания. Чтобы начать зарабатывать, следует начать с простых заданий, постепенно переходя к более сложным. Полученные деньги будут автоматически зачисляться на Ваш счет, а затем их можно выводить с сервиса на интернет-кошельки и кредитные карты. ADVADS предлагает исполнителям серфинг сайтов, чтение писем, просмотры роликов в YouTube, лайки, репосты, набор подписчиков и различные виды заданий. Это отличный современный способ заработка без всяких вложений в сфере рекламы, который можно получать, просто не выходя из дома.</p>
<div class="h2">Количество доступных заданий для исполнителей</div>
<div class="aic flex stat_spi_00">
<canvas class="photo_stat" height="600" id="chartCanvas1" width="800"></canvas>
<div class="inf_bl_st vam">
<div class="item youtube_l">YouTube просмотры.</div>
<div class="item serf_l">Серфинг сайтов.</div>
<div class="item read_l">Чтение писем.</div>
<div class="item zadan_001">Задания.</div>
<div class="item laik_001">Лайки (Youtube).</div>
<div class="item laik_002">Лайки (Vk).</div>
<div class="item repost_001">Репост.</div>
<div class="item podp_001">Подписчики (Youtube).</div>
<div class="item podp_002">Подписчики (Vk).</div>
</div>
<script type="text/javascript">
						var chart1 = new AwesomeChart('chartCanvas1');
						chart1.chartType = "ring";
						chart1.title = "";
						chart1.data = [9,0,5,31,1,8,9,21,126];
						chart1.labels = [9,0,5,31,1,8,9,21,126];
						chart1.colors = ['#37c53d', '#ff8432', '#599fff', '#e23535', '#945b6f', '#75725a', '#5865a7', '#37a3c5', '#abb51f'];
						chart1.titleFillStyle = '#E4CE0D';
						chart1.labelFontHeight = 30;
						chart1.animate = true;
						chart1.draw();
					</script>
</div>
</div>
<div class="allstats_bl" id="allstats_bl">
<div class="h1">Общая статистика рекламного сервиса</div>
<div class="flex jcsb photo_stat2">
<canvas class="" height="400" id="chartCanvas2" width="400"></canvas>
<canvas class="" height="400" id="chartCanvas3" width="400"></canvas>
<div class="inf_bl_st vam">
<div class="item youtube_l">Размещено видео.</div>
<div class="item serf_l">Размещено ссылок.</div>
<div class="item read_l">Размещено писем.</div>
<div class="item zadan_001">Размещено заданий.</div>
<div class="item laik_001">Размещено лайков (Youtube)</div>
<div class="item laik_002">Размещено лайков (Vk)</div>
<div class="item repost_001">Размещено репостов.</div>
<div class="item podp_001">Размещено заданий подписки (Youtube)</div>
<div class="item podp_002">Размещено заданий подписки (Vk)</div>
<div class="item podp_003">Размещено баннеров</div>
</div>
</div>
<div class="blockSt jcsb flex">
<div class="bl1">
<div class="df flex">
<div class="bar11"></div>
<div class="n1 ">Новых (1 месяц)</div>
</div>
<div class="df flex">
<div class="bar22"></div>
<div class="n2">Новых (1 день)</div>
</div>
<div class="df flex">
<div class="bar33"></div>
<div class="n3">Online пользователей</div>
</div>
</div>
<script type="text/javascript">
						var chart2 = new AwesomeChart('chartCanvas2');
						chart2.chartType = "ring";
						chart2.title = "";
						chart2.data = [757,47,284];
						chart2.labels = [757,47,284];
						chart2.colors = ['#8BC34A', '#bcad16', '#d2d4d7'];
						chart2.titleFillStyle = '#E4CE0D';
						chart2.labelFontHeight = 30;
						chart2.animate = true;
						chart2.draw();

						var chart3 = new AwesomeChart('chartCanvas3');
						chart3.chartType = "ring";
						chart3.title = "";
						chart3.data = [3802,175,802,200,910,378,357,127,321,334];
						chart3.labels = [3802,175,802,200,910,378,357,127,321,334];
						chart3.colors = ['#37c53d', '#ff8432', '#599fff', '#cc70c8f7', '#e23535', '#945b6f', '#75725a', '#5865a7', '#37a3c5', '#abb51f'];
						chart3.titleFillStyle = '#E4CE0D';
						chart3.labelFontHeight = 30;
						chart3.animate = true;
						chart3.draw();
					</script>
<div class="bl1">
<div class="n4">СЕРВИС ВЫПЛАТИЛ</div>
<p class="stat_bacs">947,998.1700 руб.</p>
</div>
</div>
</div>
</div>
</div>
<script src="js/script.js"></script>
<footer class="mta_foot">
<div class="row flex jcsb aic">
<span class="Copyright">Copyright ©2016 - 2017 advads.net - Сервис активной рекламы</span>
<div class="xl_35 menu_foot flex jcsb">
<a href="/pravila">Правила</a>
<a href="/pravila">Инструкции</a>
<a href="/pravila">Помощь</a>
<a href="/pravila">Контакты</a>
</div>
</div>
</footer>
</body>
<div>
<a class="bgpopup" href="javascript:void(0);" id="editModal_0"></a>
<div class="popup err">
<div class="con_er">
<div class="toast-item toast-type-error" style="">
<div class="flex aic">
<div class="toast-item-image toast-item-1"></div>
<p class="help_vspliv_01 msg_text">Жалоба отправлена</p>
</div>
</div>
</div>
</div>
</div>
<div class="pager-body-tpl" style="display: none;">
<div class="pagination pager-class" data-form="{tpl.form}" data-pager-class="{tpl.ext_class}">{tpl.pages}</div>
</div>
<div class="pager-page-tpl" style="display: none;">
<a class="{tpl.active}" data-page="{tpl.page}" href="javascript:void(0)">{tpl.page}</a>
</div>
<div class="for-forms" style="display: none;"></div>
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50530402 = new Ya.Metrika2({
                    id:50530402,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/50530402" style="position:absolute; left:-9999px;"/></div></noscript>
</html>
