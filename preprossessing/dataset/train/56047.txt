<html><body><p>﻿<!DOCTYPE html>

</p>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<base href="https://www.ab-plus.com"/>
<title>Ab Plus – Serveur Vocal Interactif - SVI</title>
<meta content="Ab Plus opérateur telecom propose des solutions SVI, serveur vocal et numéros spéciaux." name="description"/>
<meta content="" name="Keywords"/>
<meta content="VN WEB" name="Author"/>
<meta content="index,follow,all" name="robots"/>
<meta content="noarchive" name="robots"/>
<meta content="general" name="rating"/>
<meta content="VYyKuu6SeoBLDQ6qd7910IDKGzTPZNSCaEudQfp3uic" name="google-site-verification"/>
<meta content="https://www.ab-plus.com" name="identifier-url"/>
<meta content="France" http-equiv="location"/>
<meta content="no-cache, must-revalidate" http-equiv="cache-control"/>
<meta content="no-cache" http-equiv="pragma"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="VYyKuu6SeoBLDQ6qd7910IDKGzTPZNSCaEudQfp3uic" name="google-site-verification"/>
<meta class="netreviewsWidget" id="netreviewsWidgetNum14555"/>
<script src="//cl.avis-verifies.com/fr/widget4/widget04.min.js"></script>
<link href="img/favicon.png" rel="icon" type="image/png"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Raleway:400,700" rel="stylesheet" type="text/css"/>
<link href="css/owl.carousel.css" rel="stylesheet"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/style_generale.css" rel="stylesheet"/>
<link href="css/style_responsive.css" rel="stylesheet"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43606775-1', 'auto');
  ga('send', 'pageview');

</script>
<header>
<nav>
<div class="container_fluid">
<div class="row">
<div class="col-md-12">
<div class="hamb">
<div class="burger"></div>
</div>
<div class="row">
<div class="container">
<div class="col-md-2 col-sm-3 col-xs-12 logo">
<a href="https://www.ab-plus.com" title="Serveur vocal interactif sur mesure (SVI)">
<img alt="Serveur vocal interactif sur mesure (SVI)" src="img/header/abplus-logo.svg" title="AB PLUS SVI"/>
</a>
</div>
<div class="col-md-10 col-sm-9 col-xs-12 menu_connexion">
<div class="row">
<div class="col-md-12 service_connexion">
<div class="col-md-6">
<span class="service" style="float: right;"><a href="tel:+33147456050" title="Téléphone AB PLUS SVI"><img alt="Appelez-nous" src="img/serveur-vocal/serveur-vocal-telephone.png" style="width: 20px; height: 20px;position: relative; top: 2px;" title="Appelez-nous"/> <strong style="font-size: 18px; margin-left: 15px; color: #253f6c;">01.47.45.60.50</strong> </a></span>
</div>
<div class="col-md-6">
<span class="connexion"><span class="inscription_connexion"><a href="espace-client.html">Espace Client</a></span></span>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row menu_principale">
<div class="container">
<div class="col-md-12">
<ul class="nav navbar-nav">
<li><a class="active" href="https://www.ab-plus.com" title="Serveur vocal interactif sur mesure (SVI)">Accueil</a></li>
<li><a href="abplus-hebergeur-SVI.html" title="AB Plus opérateur et serveurs vocaux">Qui sommes-nous</a></li>
<li><a href="numeros-speciaux-0800/" title="Numéros de téléphone sva, cristal, vert, géographique">Numéros spéciaux</a></li>
<li><a href="serveur-vocal.html" title="Fournisseur Serveur Vocal Interactif (SVI)">Serveur vocal</a></li>
<li><a href="web-services.html" title="Numéros Dynamiques &amp; web service">Web et Telecom</a></li>
<li class="pointage"><a href="pointage-telephonique/pointage-par-telephone.html" title="Télépointage par téléphone">Pointage téléphonique</a>
<ul class="nav_secondary">
<li class="ss_menu"><a href="pointage-telephonique/pointage-par-telephone.html" title="Pointage par téléphone">Télépointage</a></li>
<li class="ss_menu"><a href="pointage-telephonique/pointeuse-mobile.html" title="Pointage par téléphone mobile">Comment ça marche ?</a></li>
<li class="ss_menu"><a href="pointage-telephonique/pointage-telephonique.html" title="Pointage téléphonique">Exemples</a></li>
<li class="ss_menu"><a href="pointage-telephonique/telepointage.html" title="Télépointage par téléphone">FAQ</a></li>
</ul>
</li>
<li><a href="contact-svi.html" title="Téléphone Opérateur SVI">Contact</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</nav>
</header>

﻿<section class="container_fluid">
<div class="row">
<div class="col-md-12 bloc_question" id="slider_header">
<div class="contact_question">
<img alt="Une question" src="img/img_question_07.png"/>
</div>
<div class="popup_validation">
<div class="popup_form">
<div class="fermeture_popup">X</div>
<p>Message Envoyé</p>
</div>
</div>
<div class="formulaire_question">
<form autocomplete="off" class="for_ask" id="contact_form" method="post">
<div class="row">
<div class="col-md-6">
<label>Nom*</label>
<input id="nom" name="nom" tabindex="1" type="text"/>
<span class="error-message"></span>
</div>
<div class="col-md-6">
<label>Téléphone</label>
<input class="tel" id="tel" maxlength="10" name="tel" tabindex="4" type="text"/>
<span class="error-message"></span>
</div>
</div>
<div class="row">
<div class="col-md-6">
<label>Prénom*</label>
<input id="prenom" name="prenom" tabindex="2" type="text"/>
<span class="error-message"></span>
</div>
<div class="col-md-6">
<label>E-mail*</label>
<input id="mail" name="mail" tabindex="5" type="text"/>
<span class="error-message"></span> </div>
</div>
<div class="row">
<div class="col-md-6">
<label>Société</label>
<input id="societe" name="societe" tabindex="3" type="text"/>
<span class="error-message"></span> </div>
<div class="col-md-6">
<label>Contactez-moi</label>
<div class="select_bloc">
<select class="contact" id="contact" name="contact" tabindex="6">
<option value="Par Téléphone">Par Téléphone</option>
<option value="Par E-mail">Par E-mail</option>
</select>
</div>
</div>
</div>
<div class="row">
<div class="col-md-9 demande_contact">
<label>Quelle est votre demande ?</label>
<textarea cols="28" id="comments" name="comments" rows="2" tabindex="7"></textarea>
<p>*Renseignements obligatoires</p>
</div>
<div class="col-md-10 text-right bt_envoyer">
<input id="envoyer" name="envoi" type="submit" value="ENVOYEZ"/>
</div>
</div>
<div class="close_windows"><span>X</span>FERMER LA FENÊTRE</div>
</form>
</div> <ul class="bxslider">
<li class="slide1">
<div class="row">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-11 col-sm-offset-1 col-xs-11 col-xs-offset-1">
<div class="col-md-6">
<p class="titre_slide"> Votre<br/>
<strong>Serveur Vocal</strong><br/>
	                  sur mesure</p>
<p class="texte_slide">Pour traiter les appels téléphoniques<br/>
	                  Diffuser des messages vocaux <br/>
	                  Et transférer les appels vers le bon interlocuteur !</p>
<a class="lien_slide" href="serveur-vocal.html" title="Serveur Vocal Interactif">Serveur Vocal</a>
</div>
<div class="col-md-6">
<span style="margin-right: 90px;">
<p class="titre_slide"> Votre<br/>
<strong>Numéro 0800 </strong><br/>
	                    Professionnel</p>
<p class="texte_slide">Numéro vert<br/>
	                    Numéro géographique <br/>
	                    Choisissez le bon tarif !</p>
<a class="lien_slide" href="numeros-speciaux-0800/" title="Souscrire Numéro">Souscription Numéro</a>
</span>
</div>
</div>
</div>
</div>
</div>
</li>
</ul>
</div>
<div class="col-md-12" id="solutions">
<div class="row">
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>Nos <span class="rouge">Solutions</span> SVI</h1>
</div>
<div class="col-md-12">
<div class="row col_solution">
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/svi-saisi-code-postal.html" title="Serveur Vocal Interactif Code Postal">
<img alt="Code postal svi" src="img/serveur-vocal/svi-saisi-code-postal.jpg" title="Serveur Vocal Interactif Code Postal"/>
</a>
<h3><a href="serveur-vocal/svi-saisi-code-postal.html" title="Serveur Vocal Interactif Code Postal">POINTS DE VENTE</a></h3>
<p>Numéro client permettant de transférer les appels vers le point de vente le plus proche.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/astreinte-telephonique.html" title="Serveur Vocal Astreinte Par Téléphone">
<img alt="Astreinte téléphonique par svi" src="img/serveur-vocal/astreinte-telephonique.jpg" style="height: 141px" title="Serveur Vocal Astreinte Par Téléphone"/>
</a>
<h3><a href="serveur-vocal/astreinte-telephonique.html" title="Serveur Vocal Astreinte Par Téléphone">NUMÉRO D'ASTREINTE TÉLÉPHONIQUE</a></h3>
<p>Un numéro unique automatiquement transféré grâce à un planning.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="pointage-telephonique/pointage-par-telephone.html" title="Service Pointage Par Téléphone">
<img alt="Service pointage téléphonique" src="img/pointage-telephonique/pointage-par-telephone.jpg" title="Service Pointage Par Téléphone"/>
</a>
<h3><a href="pointage-telephonique/pointage-par-telephone.html" title="Service Pointage Par Téléphone">POINTAGE TÉLÉPHONIQUE</a></h3>
<p>Télépointage simple et rapide de votre personnel en mission extérieure.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/centre-appels-teletravail.html" title="Serveur Vocal Centre d'appels">
<img alt="Centre appels" src="img/serveur-vocal/centre-appels.jpg" style="height: 141px;" title="Centre d'appels télétravail"/>
</a>
<h3><a href="serveur-vocal/centre-appels-teletravail.html" title="Serveur Vocal Centre d'appels">CENTRE D'APPELS</a></h3>
<p>Un centre appels transféré automatiquement vers des mobiles.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="numeros-tournants.html" title="Numéros de Téléphone Dynamiques">
<img alt="Numéros téléphone tournants" src="img/numeros-dynamiques/numeros-telephone-tournants.jpg" title="Numéros de Téléphone Dynamiques"/>
</a>
<h3><a href="numeros-tournants.html" title="Numéros de Téléphone Dynamiques">NUMÉROS DYNAMIQUES</a></h3>
<p>Rentabiliser un annuaire avec les numéros 0899 tournants.</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/numero-de-crise.html" title="Serveur Vocal gestion de crise">
<img alt="gestion de crise" src="img/serveur-vocal/gestion de crise.jpg" title="Serveur Vocal gestion de crise"/>
</a>
<h3><a href="serveur-vocal/numero-de-crise.html" title="Serveur Vocal gestion de crise">NUMERO DE CRISE</a></h3>
<p>Nos solutions téléphoniques pour la gestion de crise</p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/pti.html" title="Serveur Vocal PTI">
<img alt="pti" src="img/serveur-vocal/PTI.jpg" title="Serveur Vocal PTI"/>
</a>
<h3><a href="serveur-vocal/pti.html" title="Serveur Vocal Protection Travailleur Isolé">PROTECTION TRAVAILLEUR ISOLÉ</a></h3>
<p>Système d’alarme PTI par appel Téléphonique </p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<a href="serveur-vocal/automate-appel.html" title="Serveur Vocal Automate Appel">
<img alt="Automate appel téléphonique svi" src="img/serveur-vocal/automate-appel.jpg" title="Serveur Vocal Automate Appel"/>
</a>
<h3><a href="serveur-vocal/automate-appel.html" title="Serveur Vocal Automate Appel">AUTOMATE D’APPELS</a></h3>
<p>Pour alerter ou informer les membres d’une entreprise ou d’une communauté.</p>
</div>
</div>
<p class="mais-aussi">Mais aussi : 
					<a href="serveur-vocal/standard-automatique.html" title="Standard automatique"> Standard Automatique</a> , 
					<a href="serveur-vocal/svi-carte-fidelite.html" title="Numéro carte fidélité">Carte de fidélité</a> , 
					<a href="web-services/monitoring-service-client.html" title="Moniteur service client">Moniteur pour service client</a> , <br/> <br/>
<a href="web-services.html" title="Web service">Web services</a>, 
					<a href="serveur-vocal/numero-travaux.html" title="Numéro d'information chantier">Numéro d'information chantier</a> , 
					<a href="serveur-vocal/alerte-telephonique.html" title="Alerte produit de consommation">Alerte produit de consommation</a> , 
					
					
				etc ....</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-12" id="service">
<div class="row">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Nos experts <span class="rouge">SVI<span> à votre service</span></span></h2>
</div>
<div class="col-md-12">
<div class="row col_service">
<div class="col-md-3 col-sm-3 col-xs-6">
<img alt="+ de 20ans dans la Téléphonie" src="img/accueil/experts-telephone.jpg" title="Experts en Téléphonie"/>
<p class="bleu"><b>BÉNÉFICIEZ</b><br/>
<i>de 20 ans d'expérience</i></p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<img alt="Relation Client RC par Téléphone" src="img/accueil/telephonie-relation-client.jpg" title="Service Téléphonique Relation Client"/>
<p class="gris"><b>AMÉLIOREZ</b><br/>
<i>votre relation client</i></p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<img alt="AB PLUS serveur vocal sur mesure" src="img/accueil/serveur-vocal-sur-mesure.jpg" title="Serveur Vocal Sur Mesure"/>
<p class="bleu"><b>CONSTRUISEZ</b><br/>
<i>votre serveur vocal sur mesure</i></p>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
<img alt="Prestation et Serveur Vocal" src="img/accueil/tarif-serveur-vocal.jpg" title="Coût d'un Serveur Vocal"/>
<p class="gris"><b>RÉDUISEZ</b><br/>
<i>vos coûts téléphoniques</i></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-12" id="actualites">
<div class="row">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Nos <span class="rouge">Actualités</span> SVI</h2>
</div>
<div class="col-md-12">
<div class="row content_actualites">
<div class="col-md-4 col-sm-4 col-sm-12">
<h3 class="rouge">Centre d'Appels<br/>
                    et télétravail</h3>
<p>La solution Planitel facilite la distribution des appels avec le transfert d'appels automatique.</p>
</div>
<div class="col-md-4 col-sm-4 col-sm-12">
<h3>Numéro 0800<br/>
                    Gratuits</h3>
<p>De nouvelles conditions sur nos numéro 0800 gratuits vous permettent de réduire considérablement votre facture telecom.</p>
</div>
<div class="col-md-4 col-sm-4 col-sm-12">
<h3 class="rouge">Portabilité<br/>
                    de votre numéro vert</h3>
<p>Portez gratuitement votre <strong>numéro vert</strong> chez AB PLUS pour bénéficier de nos meilleures offres.</p>
</div>
<a class="bt_plus" href="actualite-numeros-sva.html" title="Numéros SVA">EN SAVOIR PLUS</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-12" id="confiance">
<div class="row">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Ils nous font confiance</h2>
<ul class="owl-carousel">
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Afnil - AB PLUS" src="img/accueil/partenaires/Afnil.jpg" title="Afnil - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Air-caraibes - AB PLUS" src="img/accueil/partenaires/Air-caraibes.jpg" title="Air-caraibes - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI AirfranceKlm - AB PLUS" src="img/accueil/partenaires/AirfranceKlm.jpg" title="AirfranceKlm - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI CAF - AB PLUS" src="img/accueil/partenaires/CAF.jpg" title="CAF - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Dalkia - AB PLUS" src="img/accueil/partenaires/Dalkia.jpg" title="Dalkia - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Eiffage - AB PLUS" src="img/accueil/partenaires/Eiffage.jpg" title="Eiffage - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Elengy - AB PLUS" src="img/accueil/partenaires/Elengy.jpg" title="Elengy - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Honeywell - AB PLUS" src="img/accueil/partenaires/Honeywell.jpg" title="Honeywell - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Isor - AB PLUS" src="img/accueil/partenaires/Isor.jpg" title="Isor - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Jardiland - AB PLUS" src="img/accueil/partenaires/Jardiland.jpg" title="Jardiland - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Keolis - AB PLUS" src="img/accueil/partenaires/Keolis.jpg" title="Keolis - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI LVMH - AB PLUS" src="img/accueil/partenaires/LVMH.jpg" title="LVMH - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Ratp - AB PLUS" src="img/accueil/partenaires/Ratp.jpg" title="Ratp - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Safran - AB PLUS" src="img/accueil/partenaires/Safran.jpg" title="Safran - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI TBWA-corpo - AB PLUS" src="img/accueil/partenaires/TBWA-corpo.jpg" title="TBWA-corpo - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Urssaf - AB PLUS" src="img/accueil/partenaires/Urssaf.jpg" title="Urssaf - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Vinci - AB PLUS" src="img/accueil/partenaires/Vinci.jpg" title="Vinci - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI Zodiac - AB PLUS" src="img/accueil/partenaires/Zodiac.jpg" title="Zodiac - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI adapei - AB PLUS" src="img/accueil/partenaires/adapei.jpg" title="adapei - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI eurovia - AB PLUS" src="img/accueil/partenaires/eurovia.jpg" title="eurovia - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI fichet - AB PLUS" src="img/accueil/partenaires/fichet.jpg" title="fichet - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI frenchbee - AB PLUS" src="img/accueil/partenaires/frenchbee.jpg" title="frenchbee - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI loreal - AB PLUS" src="img/accueil/partenaires/loreal.jpg" title="loreal - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI nexity - AB PLUS" src="img/accueil/partenaires/nexity.jpg" title="nexity - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI printemps - AB PLUS" src="img/accueil/partenaires/printemps.jpg" title="printemps - AB PLUS"/></li>
<li class="item" style="margin: 0 10px 0 10px"><img alt="SVI securitas - AB PLUS" src="img/accueil/partenaires/securitas.jpg" title="securitas - AB PLUS"/></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<footer>
<div class="container">
<div class="row">
<div class="col-md-12">
<p>Copyright © 1995 - 2021 - AB Plus est une société 100% française - Réalisation par <a href="http://www.vnweb.fr" target="_blank" title="Agence de Communication Alpes Maritimes">Agence de Communication VN WEB</a> - <a href="mentions-legales.html">Mentions légales</a> - Tous droits réservés</p>
</div>
</div>
</div>
</footer>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/confirm_form2.js" type="text/javascript"></script>
<script src="js/confirm_form.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scrollReveal.min.js"></script>
<script src="js/main.js"></script>
<script>function cookie_expiry_callback(){return 31536000;}</script>
<script data-bg="#457291" data-expires="cookie_expiry_callback" data-link="#000000" data-linkmsg="En savoir plus" data-message="Nous utilisons les cookies pour améliorer votre expérience. en continuant à visiter ce site, vous acceptez notre utilisation des cookies." data-moreinfo="http://www.cnil.fr/vos-droits/vos-traces/les-cookies/" id="cookiebanner" src="/js/cookiebanner.min.js" type="text/javascript"></script>
</body></html>