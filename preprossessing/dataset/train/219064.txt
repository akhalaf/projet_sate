<!DOCTYPE html>
<!--[if IE]> <html lang="en-GB" class="no-js ie"> <![endif]--><!--[if !IE]><!--><html class="no-js" lang="en-GB"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="True" name="HandheldFriendly"/>
<title>Togetherall | A safe community to support your mental health, 24/7</title>
<meta content="https://togetherall.com/en-gb/" name="url"/>
<link href="https://togetherall.com/en-gb/" rel="canonical"/>
<link href="https://togetherall.com/en-gb/" hreflang="en-gb" rel="alternate"/>
<link href="https://togetherall.com/en-ca/" hreflang="en-ca" rel="alternate"/>
<link href="https://togetherall.com/en-us/" hreflang="en-us" rel="alternate"/>
<!-- Google Tag Manager for WordPress by gtm4wp.com -->
<script data-cfasync="false" data-pagespeed-no-defer="" type="text/javascript">//<![CDATA[
	var gtm4wp_datalayer_name = "dataLayer";
	var dataLayer = dataLayer || [];
//]]>
</script>
<!-- End Google Tag Manager for WordPress by gtm4wp.com -->
<!-- This site is optimized with the Yoast SEO plugin v15.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="Togetherall is a 24/7, safe online community for people who are stressed, anxious or feeling low, with self-guided courses and resources." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://togetherall.com/en-gb/" rel="canonical"/>
<meta content="en_GB" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Home - Togetherall" property="og:title"/>
<meta content="Togetherall is a 24/7, safe online community for people who are stressed, anxious or feeling low, with self-guided courses and resources." property="og:description"/>
<meta content="https://togetherall.com/en-gb/" property="og:url"/>
<meta content="Togetherall" property="og:site_name"/>
<meta content="2020-11-30T10:41:24+00:00" property="article:modified_time"/>
<meta content="https://togetherall.com/wp-content/uploads/2020/10/FB-Instagram.png" property="og:image"/>
<meta content="1080" property="og:image:width"/>
<meta content="1080" property="og:image:height"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="https://togetherall.com/wp-content/uploads/2020/10/Twitter.png" name="twitter:image"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://togetherall.com/en-gb/#website","url":"https://togetherall.com/en-gb/","name":"Togetherall","description":"A safe community to support your mental health, 24/7","potentialAction":[{"@type":"SearchAction","target":"https://togetherall.com/en-gb/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-GB"},{"@type":"ImageObject","@id":"https://togetherall.com/en-gb/#primaryimage","inLanguage":"en-GB","url":"https://togetherall.com/wp-content/uploads/2020/10/FB-Instagram.png","width":1080,"height":1080},{"@type":"WebPage","@id":"https://togetherall.com/en-gb/#webpage","url":"https://togetherall.com/en-gb/","name":"Home - Togetherall","isPartOf":{"@id":"https://togetherall.com/en-gb/#website"},"primaryImageOfPage":{"@id":"https://togetherall.com/en-gb/#primaryimage"},"datePublished":"2019-11-12T16:10:12+00:00","dateModified":"2020-11-30T10:41:24+00:00","description":"Togetherall is a 24/7, safe online community for people who are stressed, anxious or feeling low, with self-guided courses and resources.","inLanguage":"en-GB","potentialAction":[{"@type":"ReadAction","target":["https://togetherall.com/en-gb/"]}]}]}</script>
<meta content="01Tx8Bn-OSNfKhKvLi5TbwLLCBf59FO0ih0Afppqp7A" name="google-site-verification"/>
<!-- / Yoast SEO plugin. -->
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//use.typekit.net" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/togetherall.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.1"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://togetherall.com/wp-content/plugins/instagram-feed-pro/css/sb-instagram.min.css?ver=5.3.3" id="sb_instagram_styles-css" media="all" rel="stylesheet"/>
<link href="https://togetherall.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.1" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://togetherall.com/wp-content/plugins/wpml-translation-management/res/css/admin-bar-style.css?ver=2.9.5" id="wpml-tm-admin-bar-css" media="all" rel="stylesheet"/>
<link href="https://togetherall.com/wp-content/themes/bww/assets/css/app.css?ver=1604940745" id="bww_theme-css" media="screen" rel="stylesheet"/>
<link href="https://use.typekit.net/ogv4ppm.css?ver=1.0" id="bww_fonts-css" media="all" rel="stylesheet"/>
<link href="https://use.typekit.net/guf2yic.css?ver=1.0" id="bww_fonts_new-css" media="all" rel="stylesheet"/>
<link href="https://togetherall.com/wp-content/themes/bww/style.css?ver=1591626561" id="bww_theme_base-css" media="screen" rel="stylesheet"/>
<script id="jquery-core-js" src="https://togetherall.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script id="gtm4wp-form-move-tracker-js" src="https://togetherall.com/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-form-move-tracker.js?ver=1.11.5"></script>
<link href="https://togetherall.com/wp-json/" rel="https://api.w.org/"/><link href="https://togetherall.com/wp-json/wp/v2/pages/6" rel="alternate" type="application/json"/><link href="https://togetherall.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://togetherall.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.1" name="generator"/>
<link href="https://togetherall.com/en-gb/" rel="shortlink"/>
<link href="https://togetherall.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftogetherall.com%2Fen-gb%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://togetherall.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftogetherall.com%2Fen-gb%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<meta content="WPML ver:4.3.11 stt:1,65,66;" name="generator"/>
<!-- Google Tag Manager for WordPress by gtm4wp.com -->
<script data-cfasync="false" data-pagespeed-no-defer="" type="text/javascript">//<![CDATA[
	var dataLayer_content = {"pagePostType":"frontpage","pagePostType2":"single-page","pagePostAuthor":"Big White Wall"};
	dataLayer.push( dataLayer_content );//]]>
</script>
<script data-cfasync="false">//<![CDATA[
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.'+'js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PKPQ4T6');//]]>
</script>
<!-- End Google Tag Manager -->
<!-- End Google Tag Manager for WordPress by gtm4wp.com --><link href="https://togetherall.com/wp-content/uploads/2020/09/cropped-ms-icon-310x310-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://togetherall.com/wp-content/uploads/2020/09/cropped-ms-icon-310x310-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://togetherall.com/wp-content/uploads/2020/09/cropped-ms-icon-310x310-1-180x180.png" rel="apple-touch-icon"/>
<meta content="https://togetherall.com/wp-content/uploads/2020/09/cropped-ms-icon-310x310-1-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="home">
<div class="cover cover--top"></div>
<header class="header fixed-top">
<div class="cover cover--height"></div>
<div class="container">
<div class="d-none d-lg-block">
<div class="row align-items-center">
<div class="col col-12 col-md-3">
<a href="https://togetherall.com/en-gb/">
<img alt="Togetherall logo. Togetherall name with overlapping speech bubble icons above the last two letters" class="img-fluid" src="https://togetherall.com/wp-content/themes/bww/assets/svg/logo.svg" width="150"/>
</a>
</div>
<div class="col col-12 col-md-9">
<nav class="navigation">
<ul class="navigation_menu">
<li>
<a class="nav-link " href="https://togetherall.com/en-gb/about-us/">About Us</a>
</li>
<li>
<a class="nav-link " href="https://togetherall.com/en-gb/our-work/">Our Work</a>
</li>
<li class="nav-item navigation_menu--3">
<a aria-expanded="false" aria-haspopup="true" class="nav-link navigation_menu-toggle " data-toggle="dropdown--3" href="https://togetherall.com/en-gb/our-latest/" role="button">Our Latest +</a>
<div class="navigation_menu_hover navigation_menu_hover--3 dropdown-menu">
<a class="dropdown-item " href="https://togetherall.com/en-gb/news/">News</a>
<a class="dropdown-item " href="https://togetherall.com/en-gb/press-coverage/">Press Coverage</a>
<a class="dropdown-item " href="https://togetherall.com/en-gb/research/">Research</a>
<a class="dropdown-item " href="https://togetherall.com/en-gb/videos/">Videos</a>
</div>
</li>
</ul>
<a class="button button--space-20 button--normal button--light-blue" href="https://account.v2.togetherall.com/log-in?region=Europe" target="_blank">Login</a>
<a class="button button--space-30 button--normal button--blue-reverse" href="https://account.v2.togetherall.com/register?region=Europe" target="_blank">Register</a>
<ul class="navigation_lang">
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link lang-toggle" data-toggle="dropdown" href="https://togetherall.com/en-gb/" id="lang-toggle" role="button"><img alt="British English" src="https://togetherall.com/wp-content/uploads/flags/UK.png" width="23"/> EN</a>
<div class="dropdown-menu">
<a class="dropdown-item" data-lang="en-ca" href="https://togetherall.com/en-ca/"><img alt="Canadian English" src="https://togetherall.com/wp-content/uploads/flags/CA.png" width="23"/> EN</a>
<a class="dropdown-item" data-lang="en-us" href="https://togetherall.com/en-us/"><img alt="American English" src="https://togetherall.com/wp-content/uploads/flags/US.png" width="23"/> EN</a>
</div>
</li>
</ul>
</nav>
</div>
</div>
</div>
<div class="header_mobile d-lg-none">
<div class="row align-items-center justify-content-between">
<div class="col col-4">
<a href="https://togetherall.com/en-gb/"><img alt="Togetherall logo. Togetherall name with overlapping speech bubble icons above the last two letters" class="header_mobile_logo img-fluid" src="https://togetherall.com/wp-content/themes/bww/assets/svg/logo.svg" width="150"/></a>
<ul class="navigation_lang">
<li class="nav-item dropdown">
<a aria-expanded="false" aria-haspopup="true" class="nav-link lang-toggle" data-toggle="dropdown" href="https://togetherall.com/en-gb/" id="lang-toggle2" role="button"><img alt="British English" src="https://togetherall.com/wp-content/uploads/flags/UK.png" width="23"/> EN</a>
<div class="dropdown-menu">
<a class="dropdown-item" data-lang="en-gb" href="https://togetherall.com/en-gb/"><img alt="British English" src="https://togetherall.com/wp-content/uploads/flags/UK.png" width="23"/> EN</a>
<a class="dropdown-item" data-lang="en-ca" href="https://togetherall.com/en-ca/"><img alt="Canadian English" src="https://togetherall.com/wp-content/uploads/flags/CA.png" width="23"/> EN</a>
<a class="dropdown-item" data-lang="en-us" href="https://togetherall.com/en-us/"><img alt="American English" src="https://togetherall.com/wp-content/uploads/flags/US.png" width="23"/> EN</a>
</div>
</li>
</ul>
</div>
<div class="col col-2">
<button aria-controls="navigation" aria-expanded="false" aria-label="Menu" class="hamburger hamburger--elastic js-menu" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
</div>
</div>
</div>
</header>
<div class="navigation_mobile" id="navigation">
<nav class="navigation_mobile_menu">
<a class=" mobile-only" href="https://togetherall.com/en-gb/">Home</a>
<a class=" " href="https://togetherall.com/en-gb/about-us/">About Us</a>
<a class=" " href="https://togetherall.com/en-gb/our-work/">Our Work</a>
<a class="js-collapse " href="https://togetherall.com/en-gb/our-latest/">Our Latest<span>+</span></a>
<div class="collapse">
<div class="card card-body">
<ul class="collapse_menu">
<li><a class="dropdown-item " href="https://togetherall.com/en-gb/news/">News</a></li>
<li><a class="dropdown-item " href="https://togetherall.com/en-gb/press-coverage/">Press Coverage</a></li>
<li><a class="dropdown-item " href="https://togetherall.com/en-gb/research/">Research</a></li>
<li><a class="dropdown-item " href="https://togetherall.com/en-gb/videos/">Videos</a></li>
</ul>
</div>
</div>
</nav>
<div class="navigation_mobile_footer d-flex align-items-center justify-content-between">
<div class="social">
<a href="https://www.facebook.com/wearetogetherall/" target="_blank"><img alt="Facebook" src="https://togetherall.com/wp-content/themes/bww/assets/svg/facebook.svg" width="36"/></a>
<a href="https://twitter.com/togetheralluk" target="_blank"><img alt="Twitter" src="https://togetherall.com/wp-content/themes/bww/assets/svg/twitter.svg" width="36"/></a>
<a href="https://www.linkedin.com/company/together-all" target="_blank"><img alt="Linked In" src="https://togetherall.com/wp-content/themes/bww/assets/svg/linkedin.svg" width="36"/></a>
<a href="https://www.instagram.com/wearetogetherall/" target="_blank"><img alt="Instagram" src="https://togetherall.com/wp-content/themes/bww/assets/svg/instagram.svg" width="36"/></a>
</div>
<img alt="TogetherAll" class="footer_logo" src="https://togetherall.com/wp-content/themes/bww/assets/svg/logo_footer.svg" width="100"/>
</div>
<span class="navigation_mobile_copyright copyright">© Copyright 2018-2021, Togetherall Limited. All rights reserved. <span>Built by <a class="bold" href="https://www.bravand.com">Bravand.</a></span></span>
<div class="navigation_mobile_button">
<a class="button button--large button--red-reverse" href="/in-crisis/" target="_blank">In Crisis?</a>
</div>
</div>
<main class="main main--green main_home">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12 col-md-12 col-lg-12">
<div class="main_container main_container--home">
<h1 class="main_title">Get support. Take control. Feel better.</h1>
<h2 class="main_subtitle"><span>A safe community to support your mental health, 24/7</span></h2>
<a class="main_button button button--medium button--blue" href="https://account.v2.togetherall.com/register?region=Europe" target="_blank">Join us</a>
</div>
</div>
</div>
</div>
<a class="side_sticky" href="/in-crisis/" target="_blank">In Crisis?</a>
<span class="helper_scroll">scroll</span>
</main>
<section class="section hp_video pt90">
<div class="container">
<div class="row justify-content-center">
<div class="col col-9 col-md-8" data-aos="fade-in" data-aos-delay="200">
<h2 class="hp_video_title title title--center">The community helps me express how I’m feeling and get support</h2>
</div>
</div>
<div class="row justify-content-center">
<div class="justify-content-center d-flex col col-11 col-md-12" data-aos="fade-in" data-aos-delay="200">
<figure>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/08/laptop-and-mobile@2x-1.png"/>
</figure>
<button class="video-btn" data-src="https://youtu.be/5Gdz0OXnVLM" data-target="#myModal" data-toggle="modal" type="button">
<img alt="Play" class="img-fluid" src="https://togetherall.com/wp-content/themes/bww/assets/images/play@2x.png" width="90"/>
</button>
</div>
</div>
</div>
</section>
<section class="section hp_benefits pt90 pb90">
<div class="container">
<div class="row">
<div class="col col-12 col-md-6">
<div class="cloud cloud_blue" data-aos="fade-in" data-aos-delay="400">
<h3 class="cloud_title cloud_title--normal cloud_title--white">A safe place to talk,<br/>
share &amp; support<br/>
others like you</h3>
</div>
<div class="cloud cloud_white" data-aos="fade-in" data-aos-delay="400">
<h3 class="cloud_title cloud_title--large cloud_title--blue">What is <br/>
Togetherall?</h3>
</div>
</div>
<div class="col col-12 col-md-6">
<ul class="hp_benefits_list">
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2020/02/2-2.png" width="42"/> A vibrant online community where members can support each other</li>
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2019/11/l_ico_2.png" width="42"/> Access 24 hours a day, 365 days a year</li>
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2020/02/2-copy.png" width="42"/> Trained professionals available 24/7 to keep the community safe</li>
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2020/02/3-2.png" width="42"/> Self-assessments &amp; recommended resources</li>
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2020/02/1.png" width="42"/> Creative tools to help express how you’re feeling</li>
<li><img alt="" src="https://togetherall.com/wp-content/uploads/2020/02/6-2-1.png" width="42"/> Wide range of self-guided courses to do at your own pace</li>
</ul>
</div>
</div>
</div>
</section>
<section class="section hp_steps pt90 pb90">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12">
<h2 class="hp_steps_title title title--center">Join us in 3 simple steps…</h2>
<ol class="row row-steps row-steps-home justify-content-center">
<li class="col col-12 col-md-4 nopadding hp_steps_item__home">
<div class="hp_steps_item hp_steps_item--first hp_steps_item--normal h-100" data-aos="fade-in" data-aos-delay="0">
<span class="hp_steps_item_number hp_steps_item_number--pink">1</span>
<h2 class="hp_steps_item_title hp_steps_item_title--verveine hp_steps_item_title--decoration hp_steps_item_title--pink">Register</h2>
<p class="hp_steps_item_text">Get started by completing the registration form with a few basic details. You will be sent an email to verify your account.</p>
</div>
</li>
<li class="col col-12 col-md-4 nopadding hp_steps_item__home">
<div class="hp_steps_item hp_steps_item--normal h-100" data-aos="fade-in" data-aos-delay="400">
<span class="hp_steps_item_number hp_steps_item_number--yellow">2</span>
<h2 class="hp_steps_item_title hp_steps_item_title--verveine hp_steps_item_title--decoration hp_steps_item_title--yellow">Activate</h2>
<p class="hp_steps_item_text">Click on the button within our activation email to confirm your account.</p>
</div>
</li>
<li class="col col-12 col-md-4 nopadding hp_steps_item__home">
<div class="hp_steps_item hp_steps_item--normal h-100" data-aos="fade-in" data-aos-delay="800">
<span class="hp_steps_item_number hp_steps_item_number--green">3</span>
<h2 class="hp_steps_item_title hp_steps_item_title--verveine hp_steps_item_title--decoration hp_steps_item_title--green">Participate</h2>
<p class="hp_steps_item_text">We’ll ask a few more questions about your current situation &amp; take you on a tour of Togetherall. Welcome to the community!</p>
</div>
</li>
</ol>
<div class="row justify-content-center">
<div class="col col-12 col-md-8 col-lg-6">
<div class="hp_steps_footer bold justify-content-center">
<span>Ready to join us?</span>
<a class="button button--space-20 button--normal button--light-blue" href="https://account.v2.togetherall.com/register?region=Europe">register</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section hp_quotes">
<div class="container">
<div class="row">
<div class="col">
<div class="quotes_container">
<img alt='"' class="quotes_container--decoration quotes_container--decoration-l" src="https://togetherall.com/wp-content/themes/bww/assets/svg/quote_blue_l.svg" width="114"/>
<img alt='"' class="quotes_container--decoration quotes_container--decoration-r" src="https://togetherall.com/wp-content/themes/bww/assets/svg/quote_blue_r.svg" width="114"/>
<div class="quotes_slider js-quotes-sider">
<div class="quotes_slider_item">
<div class="quotes_slider_item_inner">
<p>The members on here are amazing… I’ve realised I’m not alone and actually helping others in my position makes me feel like I have something to give</p>
</div>
<span>- Togetherall member</span>
</div>
<div class="quotes_slider_item">
<div class="quotes_slider_item_inner">
<p>Getting support from the community and Wall Guides helps me understand I’m not the only one...</p>
</div>
<span>- Togetherall member</span>
</div>
<div class="quotes_slider_item">
<div class="quotes_slider_item_inner">
<p>Being anonymous is a godsend. I feel like I can truly write down what I’m thinking without judgement or stigma</p>
</div>
<span>- Togetherall member</span>
</div>
<div class="quotes_slider_item">
<div class="quotes_slider_item_inner">
<p>Working through one of the courses really opened my eyes to how many negative thinking patterns I had gotten into…</p>
</div>
<span>- Togetherall member</span>
</div>
</div>
<div class="slider_dots"><button class="ppause"><img alt="pause" src="https://togetherall.com/wp-content/themes/bww/assets/svg/ppause.svg" width="21"/></button><button class="pplay"><img alt="play" src="https://togetherall.com/wp-content/themes/bww/assets/svg/pplay.svg" width="23"/></button><div class="quotes_dots"></div></div>
</div>
</div>
</div>
</div>
</section>
<section class="section hp_about pt90 pb165">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12 col-md-12">
<h2 class="hp_about_title title title--center">What’s being talked about in the community?</h2>
<span class="hp_about_subtitle hp_about_subtitle--space subtitle subtitle--center" data-aos="fade-in" data-aos-delay="400">Take a look at these posts inspired by key topics in discussion with our members…</span>
<div class="hp_about_slider" data-aos="fade-in" data-aos-delay="400">
<button class="slick-arrow slick-prev" type="button"> <img alt="Show previous items" class="img-fluid" src="https://togetherall.com/wp-content/themes/bww/assets/images/arrow_l.png" srcset="https://togetherall.com/wp-content/themes/bww/assets/images/arrow_l@2x.png 2x" width="19"/></button>
<div class="hp_about_slider_container js-about-sider">
<div class="hp_about_slider_item">
<p>I’ve got deadlines coming out of my ears and exams looming for a course I’m not even sure I want to do anymore. Can anyone else relate?</p>
<span>Stress whilst studying</span>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/02/Profile-image-1-1.png"/>
</div>
<div class="hp_about_slider_item">
<p>I guess it takes as long as it takes, but it’s having an effect on every part of my life… they say time is a healer – I really hope that’s true. </p>
<span>Coping with loss</span>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/02/Profile-image-2.png"/>
</div>
<div class="hp_about_slider_item">
<p> I’ve been on a particular medication for a few weeks and struggling with some of the side effects. Just want to know what working for some of you.</p>
<span>Side effects of medication</span>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/02/Profile-image-3.png"/>
</div>
<div class="hp_about_slider_item">
<p>Reading everyone’s stories of coping with illness has given me a bit of inspiration to get out the house for a walk today. Keep going everyone, it’s the little things that help!</p>
<span>Dealing with chronic illness</span>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/02/Profile-image-4.png"/>
</div>
<div class="hp_about_slider_item">
<p>My friend told me today that she quite often feels completely alone. I try so hard to keep connected with her and hate to think of her feeling isolated. How can I make her feel better without breaking down myself?</p>
<span>Loneliness</span>
<img alt="" class="img-fluid" src="https://togetherall.com/wp-content/uploads/2020/02/Profile-image-5.png"/>
</div>
</div>
<div class="hp_about_slider_dots slider_dots"></div>
<button class="slick-arrow slick-next" type="button"> <img alt="Show next items" class="img-fluid" src="https://togetherall.com/wp-content/themes/bww/assets/images/arrow_r.png" srcset="https://togetherall.com/wp-content/themes/bww/assets/images/arrow_r@2x.png 2x" width="19"/></button>
</div>
</div>
</div>
</div>
</section>
<section class="section hp_about hp_about--special pb120">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12">
<h2 class="hp_about_title title title--center">Can I access Togetherall?</h2>
<span class="hp_about_subtitle subtitle subtitle--center" data-aos="fade-in" data-aos-delay="400">We’re funded in lots of different ways, so you may have free<br/>
access to our community using one of the routes below:</span>
<ul class="stones" data-aos="fade-in" data-aos-delay="400">
<li class="stones_item bold">
<span>via the NHS<br/>
in your area<em class="star">*</em></span>
</li>
<li class="stones_item bold">
<span>as a student<em class="star">*</em></span>
</li>
<li class="stones_item bold">
<span>as an<br/>
employee</span>
</li>
<li class="stones_item bold">
<span>as a UK veteran</span>
</li>
<li class="stones_item bold">
<span>as a member of<br/>
a UK military <br/>
family<em class="star">*</em></span>
</li>
<li class="stones_item bold">
<span>via your health<br/>
insurance<em class="star">*</em></span>
</li>
<li class="stones_item bold">
<span>as a resident in<br/>
Auckland, NZ<em class="star">*</em></span>
</li>
</ul>
<a class="hp_about_details bold" data-aos="fade-in" data-aos-delay="400" href="https://account.v2.togetherall.com/register?region=Europe"><em class="star modify">*</em> Check availability <em class="arrow modify">&gt;</em></a>
</div>
</div>
</div>
</section>
<section class="section hp_stats pt120a pb110">
<div class="container">
<ul class="row">
<li class="col col-6 col-md-3">
<div class="hp_stats_container">
<span class="hp_stats_title">70%</span>
<p class="hp_stats_text">joined Togetherall for immediate access</p>
</div>
</li>
<li class="col col-6 col-md-3">
<div class="hp_stats_container">
<span class="hp_stats_title">250,000+</span>
<p class="hp_stats_text">people supported so far</p>
</div>
</li>
<li class="col col-6 col-md-3">
<div class="hp_stats_container">
<span class="hp_stats_title">64%</span>
<p class="hp_stats_text">access Togetherall outside of 9-5 working hours</p>
</div>
</li>
<li class="col col-6 col-md-3">
<div class="hp_stats_container">
<span class="hp_stats_title">67%</span>
<p class="hp_stats_text">share on Togetherall because it's anonymous</p>
</div>
</li>
</ul>
</div>
</section>
<section class="section hp_instagram pt110 pb145">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12">
<div class="row justify-content-center">
<div class="col col-12 col-md-6">
<div class="moodal moodal--mt65 moodal--mb10 hp_moodal">
<h2 class="moodal_title bold">Ready to join us?</h2>
<p class="moodal_text">Join our online mental health community.<br/>
Accessible any time, anywhere.</p>
<a class="button button--space--top-25 button--normal button--light-blue" href="https://account.v2.togetherall.com/register?region=Europe" target="_blank">Register</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Modal -->
<div aria-hidden="true" aria-label="video overlay" class="modal fade" id="myModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body">
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
<!-- 16:9 aspect ratio -->
<div class="embed-responsive embed-responsive-16by9">
<div class="embed-responsive-item" id="player"></div>
<script src="//www.youtube.com/player_api"></script>
</div>
</div>
</div>
</div>
</div>
<div class="description description--border-top">
<div class="container">
<div class="row justify-content-center">
<div class="col col-12 col-md-10">
<p class="bold">This site is not intended for individuals in an emergency. If you are in a life-threatening situation in the UK, please immediately dial 999 or go to your nearest Accident and Emergency department (A&amp;E). If you are in a life-threatening situation in New Zealand, please immediately dial 111 or go to your nearest hospital emergency department (ED). Further crisis help can be found <a class="bold" href="https://togetherall.com/en-gb/in-crisis/">here</a>.</p>
</div>
</div>
</div>
</div>
<footer class="footer">
<div class="container">
<div class="row">
<div class="col col-12 col-md-4">
<h3 class="footer_title bold">Togetherall </h3>
<span class="footer_address">36-38 Whitefriars Street<br/>
London<br/>
EC4Y 8BQ</span>
</div>
<div class="col col-12 col-md-4">
<h3 class="footer_title bold footer_title--space">Links</h3>
<div class="footer_links">
<ul class="footer_list">
<li>
<a class="" href="https://togetherall.com/en-gb/complaints/">Complaints</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/house-rules/">House rules</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/privacy-policy/">Privacy</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/accessibility/">Accessibility Statement</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/faqs/">FAQ’s</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/careers/">Careers</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/member-terms/">Member terms</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/press-kit/">Press kit</a>
</li>
<li>
<a class="" href="https://togetherall.com/en-gb/big-white-wall/">Big White Wall</a>
</li>
<li>
<a class="footer_list_extra" href="https://togetherall.com/en-gb/in-crisis/" target="_blank">Crisis support</a>
</li>
</ul>
</div>
</div>
<div class="col col-12 col-md-4">
<h3 class="footer_title bold footer_title--space">Contact us </h3>
<a class="footer_button button button--full button--white-reverse" href="https://togetherall.com/contact">Send us a message</a>
<div class="d-flex align-items-center justify-content-between">
<div class="social">
<a href="https://www.facebook.com/wearetogetherall/" target="_blank"><img alt="Facebook" src="https://togetherall.com/wp-content/themes/bww/assets/svg/facebook.svg" width="26"/></a>
<a href="https://twitter.com/togetheralluk" target="_blank"><img alt="Twitter" src="https://togetherall.com/wp-content/themes/bww/assets/svg/twitter.svg" width="26"/></a>
<a href="https://www.linkedin.com/company/together-all" target="_blank"><img alt="Linked In" src="https://togetherall.com/wp-content/themes/bww/assets/svg/linkedin.svg" width="26"/></a>
<a href="https://www.instagram.com/wearetogetherall/" target="_blank"><img alt="Instagram" src="https://togetherall.com/wp-content/themes/bww/assets/svg/instagram.svg" width="26"/></a>
</div>
<a href="https://togetherall.com/en-gb/"><img alt="TogetherAll" class="footer_logo" src="https://togetherall.com/wp-content/themes/bww/assets/svg/logo_footer.svg" width="100"/></a>
</div>
</div>
</div>
<div class="row">
<div class="col">
<span class="copyright">© Copyright 2018-2021, Togetherall Limited. All rights reserved. <span>Built by <a class="bold" href="https://www.bravand.com">Bravand.</a></span></span>
</div>
</div>
</div>
</footer>
<div class="ie11info">
<div class="container">
<div class="row align-items-center justify-content-center">
<div class="col col-12 col-md-9">
<p>Our Website has detected that you are using an outdated browser that will prevent you from accessing our service. We recommended that you update your browser to use Togetherall.</p>
</div>
<div class="col col-12 col-md-3 d-flex justify-content-end">
<button class="ie11_button button button--small button--transparent">OK</button>
</div>
</div>
</div>
</div>
<div class="cookie">
<div class="container">
<div class="row align-items-center justify-content-center">
<div class="col col-12 col-md-9">
<p>This website uses cookies to ensure you get the best experience. <a class="bold" href="https://togetherall.com/cookies">Learn more</a></p>
</div>
<div class="col col-12 col-md-3 d-flex justify-content-end">
<button class="cookie_button button button--small button--transparent">OK</button>
</div>
</div>
</div>
</div>
<div class="navigation_mobile_button navigation_mobile_button-bottom js-navigation_mobile_button">
<a class="button button--space-20 button--half button--light-blue" href="https://account.v2.togetherall.com/log-in?region=Europe" target="_blank">Login</a>
<a class="button button--half button--blue-reverse" href="https://account.v2.togetherall.com/register?region=Europe" target="_blank">Register</a>
</div>
<!--
The IP2Location Redirection is using IP2Location LITE geolocation database. Please visit https://lite.ip2location.com for more information.
fbb86093b26eb8b165665cc2da3ccddf60cd4d4f
-->
<!-- Custom Feeds for Instagram JS -->
<script type="text/javascript">
var sbiajaxurl = "https://togetherall.com/wp-admin/admin-ajax.php";
</script>
<script id="bww_popper-js" src="https://togetherall.com/wp-content/themes/bww/assets/js/vendor/popper.min.js?ver=1.0"></script>
<script id="bww_bootstrap-js" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js?ver=1.0"></script>
<script id="bww_aos-js" src="https://togetherall.com/wp-content/themes/bww/assets/js/vendor/aos/aos.js?ver=1.0"></script>
<script id="bww_core-js-extra">
var settings = {"ajaxurl":"https:\/\/togetherall.com\/wp-admin\/admin-ajax.php"};
</script>
<script id="bww_core-js-before">
var $ = jQuery.noConflict();
</script>
<script id="bww_core-js" src="https://togetherall.com/wp-content/themes/bww/assets/js/min/core.min.js?ver=20200421"></script>
<script id="wp-embed-js" src="https://togetherall.com/wp-includes/js/wp-embed.min.js?ver=5.5.1"></script>
<script>
            $( document ).ready(function() {
					$('.swal-button.swal-button--confirm').text('CLOSE');
                    $('.swal-modal').scrollTop(0);
                    $('.swal-modal').scrollTop(1);

                    setTimeout(() => {
                        $('.swal-modal').scrollTop(0);
                        $('.swal-modal').scrollTop(1);
                    }, 100);

                    setTimeout(() => {
                        $('.swal-modal').scrollTop(0);
                        $('.swal-modal').scrollTop(1);
                    }, 500);
				});
        </script>
</body>
</html>
<!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/


Served from: togetherall.com @ 2021-01-14 20:54:33 by W3 Total Cache
-->