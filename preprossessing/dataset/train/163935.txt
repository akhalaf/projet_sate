<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, width=device-width, minimal-ui" name="viewport"/>
<meta content="33AFD2F4B578EE6763800D976DC2ABE6" name="msvalidate.01"/>
<meta content="utggGi7XXsq3btftWI5W1jj3mXUvKat6LoBG881f" name="csrf-token"/>
<meta content="BKSyLAnohu4X_DeU58guHPEPQVvxJIhPeAVncUh2us0" name="google-site-verification"/>
<meta content="SAMEORIGIN" http-equiv="X-Frame-Options"/>
<meta content="Astromart.com is the leading portal for astronomy news, classified ads and telescope reviews. Place your astronomy ads, read the latest news articles and check out the current reviews of newest telescopes and equipment." name="description"/>
<meta content="astronomy news, astronomy classified, astronomy classifieds, astronomy ads, astronomy news article, astronomy news articles, astronomy telescope reviews " name="keywords"/>
<title>Astromart</title>
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css" rel="stylesheet"/>
<link href="http://www.astromart.com/rss/news" rel="alternate" title="Astromart - News" type="application/rss+xml"/>
<link href="http://www.astromart.com/rss/classifieds" rel="alternate" title="Astromart - Classifieds" type="application/rss+xml"/>
<link href="http://www.astromart.com/rss/auctions" rel="alternate" title="Astromart - Auctions" type="application/rss+xml"/>
<link href="http://www.astromart.com/rss/reviews_and_articles" rel="alternate" title="Astromart - Articles" type="application/rss+xml"/>
<link href="http://www.astromart.com/rss/events" rel="alternate" title="Astromart - Events Calendar" type="application/rss+xml"/>
<link href="http://www.astromart.com/rss/forums" rel="alternate" title="Astromart - Forums" type="application/rss+xml"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/astromart/stylesheets/application.css?v=16" rel="stylesheet"/>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script crossorigin="anonymous" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
</head>
<body>
<main>
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#menu" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<a class="navbar-brand" href="/">
<img alt="" src="/astromart/images/logo.png"/>
</a>
<div class="collapse navbar-collapse" id="menu">
<ul class="navbar-nav mr-auto">
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/news/show">News</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="/classifieds">Classifieds</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/auctions">Auctions</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/members">Members</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/reviews-and-articles">Reviews &amp; Articles</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/forums">Forums</a>
</li>
<li class="nav-item ">
<a class="nav-link" href="https://astromart.com/calendar">Calendar</a>
</li>
</ul>
<ul class="navbar-nav mr-auto navbar-nav--top">
<li class="nav-item">
<a class="nav-link" href="/support-options">Support Astromart</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://astromart.com/login">Log In</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/forums/astromart-groups/astromart-faq">Help</a>
</li>
</ul>
</div>
</nav>
<article>
<div class="container-fluid">
<div class="row">
<button class="btn opener opener-left" data-toggle-div=".col-second"><i class="fa fa-angle-right"></i></button>
<button class="btn opener opener-right" data-toggle-div=".col-third"><i class="fa fa-angle-left"></i></button>
<div class="col-12 col-md-3 col-xl-2 col-second">
<div class="boxes">
<div class="box box--image">
<header>
<h3>Image of the day</h3>
</header>
<div class="content text-center">
<div style="padding: 0 30px">
From the<br/><a href="//www.buytelescopes.com/gallery" target="_blank">ATWB Customer Gallery</a><br/>
<a href="//www.buytelescopes.com/gallery/photo/67632" target="_blank">
<img border="0" src="//www.buytelescopes.com/content/images/thumbs/2ebf23cad60dced4212c1d71fd1f2df7ec74479a_ngc253-sculptor-galaxy_300.jpg" style="max-width: 100%; height: auto; border-radius: 3px"/>
</a>
<br/>
<b>NGC253 Sculptor Galaxy</b><br/>
<br/>
<!--<a target="_blank" href="//www.buytelescopes.com/gallery">View the Anacortes Telescope &amp; Wild Bird Customer Gallery</a>-->
</div>
</div>
</div>
<div class="box box--list">
<header>
<h3>About Astromart</h3>
</header>
<div class="content">
<ul>
<li><a href="/about/terms-of-service">Terms of Service</a></li>
<li><a href="/about/privacy-policy">Privacy Policy</a></li>
<li><a href="/forums/astromart-groups/astromart-faq">Help &amp; FAQ</a></li>
<li><a href="/links">Astronomy Links</a></li>
<li><a href="/users">User Profiles</a></li>
<li><a href="/users/top-users">Top Users List</a></li>
<li><a href="/about/sponsors">Sponsors</a></li>
<li><a href="/about/supporters/">Supporters</a></li>
<li><a href="/rss">RSS Feeds</a></li>
</ul>
</div>
</div>
<div class="box box--form">
<header>
<h3>My Account</h3>
</header>
<div class="content">
<form action="/login" method="post">
<input name="_token" type="hidden" value="utggGi7XXsq3btftWI5W1jj3mXUvKat6LoBG881f"/>
<div class="form-group">
<input class="form-control" id="name" name="username" placeholder="Email or Username" type="text"/>
</div>
<div class="form-group">
<input class="form-control" id="password" name="password" placeholder="Password" type="password"/>
</div>
<button class="btn btn-primary" type="submit">Log In</button>
</form>
<p>New to Astromart?</p>
<a href="https://astromart.com/register">Register an account...</a>
</div>
</div>
<div class="box box--support">
<header>
<h3>Need Help?</h3>
</header>
<div class="content">
<p style="padding: 30px">
<!-- Embedded Click and Chat Start -->
<script src="//server10.clickandchat.com/include.js?domain=www.astromart.com" type="text/javascript"></script>
<script type="text/javascript">
                if(sWOTrackPage)sWOTrackPage();
            </script>
<!-- Embedded Click and Chat End -->
<!-- If you have any questions or experiencing an issue please report to
              <a href="mailto:support@astromart.com">support@astromart.com</a>-->
<style>
                #wo_online_image, #wo_offline_image {
                    display: none !important;
                }
            </style>
</p></div>
</div> </div>
</div>
<div class="col-12 col-md-6 col-xl-8 col-first">
<div class="news">
<h2>
<a href="/news/show/roman-space-telescope-the-resolution-of-hubble-with-a-100-times-larger-field-of-view">
                        Roman Space Telescope – The Resolution of Hubble with a 100 Times Larger Field of View
                    </a>
</h2>
<h4>
                                        Posted by
                    <a href="/users/58944">
                        Guy Pirro
                    </a>
                    |
                    
                    01/12/2021 07:59PM
                    | <a href="/news/show/roman-space-telescope-the-resolution-of-hubble-with-a-100-times-larger-field-of-view/comments">
                        3 Comments
                    </a>
</h4>
<p>In 1995, the Hubble Space Telescope stared at a blank patch of the sky for 10 straight days. The resulting Deep Field image captured thousands of previously unseen, distant galaxies. Similar observations have followed since then, including the longest and deepest exposures, the Hubble Ultra Deep Field and the eXtreme Deep Field. Now, astronomers are looking ahead to the future, and the possibilities enabled by NASA’s upcoming Nancy Grace Roman Space Telescope, scheduled for launch in 2025. The Roman Space Telescope will be able to photograph an area of sky 100 times larger than Hubble with the same exquisite sharpness. As a result, a Roman Ultra Deep Field would collect millions of galaxies, including hundreds that date back to just a few hundred million years after the big bang. Such an observation would fuel new investigations into multiple science areas, from the structure and evolution of the universe to star formation over cosmic time.</p>
<a class="btn btn-primary" href="/news/show/roman-space-telescope-the-resolution-of-hubble-with-a-100-times-larger-field-of-view">
                    Read More
                </a>
</div>
<div class="latest">
<div class="row">
<div class="col-12 col-lg-6">
<div class="box box--list box--latest">
<header>
<h3>Latest Astronomy News</h3>
</header>
<div class="content">
<ul>
<li>
<a href="/news/show/kiss-the-sky-tonight-month-of-january-2021">
                                            Kiss the Sky Tonight -- Month of January 2021
                                        </a>
</li>
<li>
<a href="/news/show/years-end-sentiment-from-the-crew-at-astromart-and-atwb">
                                            Year's End Sentiment from the crew at Astromart and ATWB
                                        </a>
</li>
<li>
<a href="/news/show/from-the-atwb-team">
                                            From the ATWB Team
                                        </a>
</li>
<li>
<a href="/news/show/chinas-change-5-spacecraft-brings-back-moon-samples">
                                            China’s Chang’e-5 Spacecraft Brings Back Moon Samples
                                        </a>
</li>
<li>
<a href="/news/show/kiss-the-sky-tonight-month-of-december-2020">
                                            Kiss the Sky Tonight -- Month of December 2020
                                        </a>
</li>
</ul>
</div>
</div>
</div>
<div class="col-12 col-lg-6">
<div class="box box--list box--latest">
<header>
<h3>Latest Articles &amp; Telescope Reviews</h3>
</header>
<div class="content">
<ul>
<li>
<a href="/articles/show/astroart">
                                            Astroart
                                        </a>
</li>
<li>
<a href="/reviews/telescopes/cats/show/gso-8-inch-true-cassegrainian">
                                            GSO 8-inch TRUE CASSEGRAINIAN
                                        </a>
</li>
<li>
<a href="/reviews/telescopes/show/the-ioptron-urban-mc90-goto-telescope-system">
                                            The iOptron Urban MC90 "GoTo" telescope system
                                        </a>
</li>
<li>
<a href="/articles/how-to/show/a-retake-on-the-porter-springfield-mount">
                                            A retake on the Porter Springfield mount
                                        </a>
</li>
<li>
<a href="/articles/how-to/show/meade-ls68-adapter-for-meade-field-tripod">
                                            Meade LS6/8 adapter for Meade field tripod
                                        </a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="featured">
<h2>Featured Classifieds</h2>
<div class="row">
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/binoculars/show/orion-giantview-25x100-astronomy-binoculars-local-pickup-minneapolisst-paul-area">Orion GiantView 25x100 Astronomy Binoculars - Local Pickup Minneapolis/St. Paul Area</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Eden Prairie, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/binoculars/show/orion-giantview-25x100-astronomy-binoculars-local-pickup-minneapolisst-paul-area">
<img alt="Orion GiantView 25x100 Astronomy Binoculars - Local Pickup Minneapolis/St. Paul Area" src="/images/classifieds/101339/thumb_a04264b91a956e2e7478b188fd54de77-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$325.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/eyepieces/show/for-sale-125-morpheus-499613">For sale 12.5 Morpheus</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Biglerville, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/eyepieces/show/for-sale-125-morpheus-499613">
<img alt="For sale 12.5 Morpheus" src="/images/classifieds/9096/thumb_b34aaedf357727b64126efcccb2fb906-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$185.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/binoculars/show/orion-monster-binocular-mount-extension-pier-local-pickup-minneapolisst-paul-area">Orion Monster Binocular Mount &amp; Extension Pier - Local Pickup Minneapolis/St. Paul Area</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Eden Prairie, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/binoculars/show/orion-monster-binocular-mount-extension-pier-local-pickup-minneapolisst-paul-area">
<img alt="Orion Monster Binocular Mount &amp; Extension Pier - Local Pickup Minneapolis/St. Paul Area" src="/images/classifieds/101339/thumb_c3c0eb2c1799c32eddfb89283b2941dd-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$450.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/ccd-cameras-astro/show/altair-astro-26c-new-unused">Altair Astro 26C - New Unused</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Santa Ana, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/ccd-cameras-astro/show/altair-astro-26c-new-unused">
<img alt="Altair Astro 26C - New Unused" src="/images/classifieds/107811/thumb_3ad4b567fbf692ad04e710696ab7299c-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$1,500.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/telescope-refractor/show/wanted-tmbastrotech-114mm-mounting-rings">Wanted TMB/Astrotech 114mm mounting rings</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Georgetown, US</p>
</header>
<div class="content">
<span class="sticker">Wanted</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/telescope-refractor/show/wanted-tmbastrotech-114mm-mounting-rings">
<img alt="Wanted TMB/Astrotech 114mm mounting rings" src="/images/classifieds/59740/thumb_0d309c1027415c9fecc8ce5bda1a5b8a-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$1.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/filters/show/astrodon-tru-balance-125-mounted-lrgb-ha-oiii-and-sii">Astrodon Tru-Balance 1.25" Mounted LRGB + Ha, Oiii and Sii</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Cranford, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/filters/show/astrodon-tru-balance-125-mounted-lrgb-ha-oiii-and-sii">
<img alt='Astrodon Tru-Balance 1.25" Mounted LRGB + Ha, Oiii and Sii' src="/images/classifieds/78740/thumb_23d6b061dab76085bfe3c14c2465b13f-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$1,200.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/ccd-cameras-astro/show/starlight-xpress-trius-814-ccd-camera">Starlight Xpress Trius 814 Mono CCD Camera.</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Cranford, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/ccd-cameras-astro/show/starlight-xpress-trius-814-ccd-camera">
<img alt="Starlight Xpress Trius 814 Mono CCD Camera." src="/images/classifieds/78740/thumb_f6f5180f25580e17f6fb07c2419cebd0-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$1,500.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/mounts-equatorial/show/pier-tech-2-height-adjustable-pier-new-design-2021-499605">Pier-Tech 2 Height Adjustable Pier. New Design 2021</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Bartlett, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/mounts-equatorial/show/pier-tech-2-height-adjustable-pier-new-design-2021-499605">
<img alt="Pier-Tech 2 Height Adjustable Pier. New Design 2021" src="/images/classifieds/101624/thumb_785d73da52e8dd55a7bffba95e052cdc-img.png"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$3,500.00</span>
</div>
</div>
</footer>
</div>
</div>
<div class="col-12 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="box box--featured">
<header>
<h3><a href="/classifieds/astromart-classifieds/eyepieces/show/meade-uwa-88-smoothie-made-in-japan">Meade UWA 8.8 Smoothie (made in Japan)</a></h3>
<p><i aria-hidden="true" class="fa fa-map-marker"></i> Tooele, US</p>
</header>
<div class="content">
<span class="sticker">For Sale</span>
<div class="content-image" style="max-height: 250px; overflow: hidden">
<a href="/classifieds/astromart-classifieds/eyepieces/show/meade-uwa-88-smoothie-made-in-japan">
<img alt="Meade UWA 8.8 Smoothie (made in Japan)" src="/images/classifieds/119957/thumb_1611032d89c0e7aa802c51178535ea4f-img.jpg"/>
</a>
</div>
</div>
<footer>
<div class="row">
<div class="col-9">
<span class="content-price">$180.00</span>
</div>
</div>
</footer>
</div>
</div>
</div>
</div>
<div class="auction">
<h2>Auction ending soon</h2>
<div class="flex-table flex-table-striped flex-table-auctions">
<div class="flex-table-row flex-table-row--header">
<div class="flex-table-col flex-table-col--photo">Photo</div>
<div class="flex-table-col flex-table-col--title">Title</div>
<div class="flex-table-col flex-table-col--current">Current</div>
<div class="flex-table-col flex-table-col--bids">Bids</div>
<div class="flex-table-col flex-table-col--expires">Expires On</div>
</div>
<div class="flex-table-row flex-table-row--body">
<div class="flex-table-col flex-table-col--photo" data-heading="Photo:">
<a href="/auctions/astromart-auctions/mounts-drives-tripods/show/bushnell-or-focal-mini-steel-tabletop-yoke-mount">
<img alt="" src="/images/auctions/5780/e2fa7f9e0ba012dae9311580f628d0ca-img.jpg" style="max-height: 100px;"/>
</a>
</div>
<div class="flex-table-col flex-table-col--title" data-heading="Title:">
<a href="/auctions/astromart-auctions/mounts-drives-tripods/show/bushnell-or-focal-mini-steel-tabletop-yoke-mount">
                Bushnell or Focal mini steel tabletop yoke mount
            </a>
</div>
<div class="flex-table-col flex-table-col--current" data-heading="Current bid:">
            
                            -
                    </div>
<div class="flex-table-col flex-table-col--bids" data-heading="Bids:">0</div>
<div class="flex-table-col flex-table-col--expires" data-heading="Expires on:">
                            01/22/2021 09:15AM
                    </div>
</div>
</div>
</div>
</div>
<div class="col-12 col-md-3 col-xl-2 col-third">
<div class="boxes">
<div class="box box--image box--image-right">
<header>
<h3>Funding Member</h3>
</header>
<div class="content text-center">
<img alt="" src="/astromart/images/img-atw-logo.gif"/>
<a href="https://www.buytelescopes.com" target="_blank">Telescopes, Astronomy, Binoculars</a>
</div>
</div>
<div class="box box--image box--image-right">
<div class="content text-center">
<a href="http://www.buytelescopes.com/baader-planetarium"><img src="/images/banners/92869/92869.jpg"/></a>
<a href="http://www.buytelescopes.com/takahashi"><img src="/images/banners/92440/92440.jpg"/></a>
<a href="https://www.luxxoptica.com/product-gallery"><img src="/images/banners/95407/46389257489adadf7ed9af73a38b12cd-img.jpg"/></a>
</div>
</div>
<div class="box box--list">
<header>
<h3>Sponsors</h3>
</header>
<div class="content">
<ul>
<li><a href="http://www.astromart.com">AstroMart LLC</a></li>
<li><a href="http://www.luxxoptica.com">Luxxoptica</a></li>
<li><a href="">AstroMart</a></li>
<li><a href="http://skywatch.brainiac.com/astroland">Rod Mollise</a></li>
<li><a href="https://www.telescopengineering.com">T.E.C</a></li>
<li><a href="http://www.agoptical.com">AG Optical Systems</a></li>
<li><a href="http://www.BuyTelescopes.com">SellTelescopes.com</a></li>
<li><a href="http://www.BuyTelescopes.com">Anacortes Telescope</a></li>
<li><a href="http://www.clearline-tech.com">Clearline Technology Corp.</a></li>
<li><a href="http://www.buytelescopes.com/takahashi">Takahashi</a></li>
<li><a href="https://koji-matsumoto.com">Matsumoto Company</a></li>
<li><a href="http://www.buytelescopes.com/takahashi">Takahashi</a></li>
<li><a href="http://www.astromart.com">Astromart Customer Service</a></li>
<li><a href="http://www.optique-unterlinden.com/">Optique Unterlinden (Europe)</a></li>
<li><a href="http://www.piertechinc.com">Pier-Tech Inc.</a></li>
<li><a href="http://www.focusknobs.com">FocusKnobs</a></li>
<li><a href="http://www.facebook.com/Leafcustomcases">Leaf Custom Cases</a></li>
<li><a href="http://www.apm-telescopes.de">APM-Telescopes</a></li>
<li><a href="https://www.getleadsfast.com">GetLeadsFast, LLC</a></li>
<li><a href="https://www.getleadsfast.com">GetLeadsFast, LLC</a></li>
</ul>
<br/>
<a class="btn btn-primary btn-xs" href="/about/sponsors">View all sponsors</a>
</div>
</div>
<div class="box box--form">
<header>
<h3>Search</h3>
</header>
<div class="content">
<form action="/search" method="post">
<input name="_token" type="hidden" value="utggGi7XXsq3btftWI5W1jj3mXUvKat6LoBG881f"/>
<div class="form-group">
<input class="form-control" id="q" name="q" placeholder="Search..." type="text"/>
</div>
<div class="form-group">
<select class="form-control" id="in" name="in">
<option value="news">News</option>
<option value="classifieds">Classifieds</option>
<option value="auctions">Auctions</option>
<option value="review_and_articles">Reviews &amp; Articles</option>
<option value="members">Members Reviews &amp; Articles</option>
<option value="forums">Forums</option>
<option value="events">Calendar Events</option>
</select>
</div>
<button class="btn btn-primary" type="submit">Go</button>
</form>
</div>
</div> </div>
</div>
</div>
</div>
</article>
<footer>
<nav class="footer-links">
<ul>
<li><a href="/news/show">Astronomy News</a></li>
<li><a href="/classifieds">Telecope Classifieds</a></li>
<li><a href="/auctions">Telescope Auctions</a></li>
<li><a href="/reviews-and-articles">Telescope Reviews</a></li>
<li><a href="http://www.buytelescopes.com">Telescopes</a></li>
<li><a href="/forums">Telescope and Astronomy Forums</a></li>
<li><a href="/account">My Account</a></li>
<li><a href="/forums/astromart-groups/astromart-faq">Help</a></li>
<li><a href="/rss">RSS</a></li>
</ul>
</nav>
<div class="copyright">
<p>© 2021 Astromart.com. All rights reserved.</p>
</div>
</footer></main>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-120673500-1', 'auto');


</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120673500-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120673500-1');
    </script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/astromart/javascripts/site.js?v=5"></script>
<script>
        ga('send', 'pageview');
        $(function(){
            $('.datepicker').datepicker();
        })

    </script>
</body>
</html>