<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="" name="description"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>æä¸­äº - ååç²¾åå°è¯´åéå¹³å°</title>
<meta content="æä¸­äº,æä¸­äºå°è¯´,å°è¯´åé,å°è¯´åéå¹³å°" name="keywords"/>
<link href="https://qcdn-legacy.zhangzhongyun.com/favicon.ico" rel="shortcut icon"/>
<link href="https://qcdn.zhangzhongyun.com/vendor/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="style.css?v=8" rel="stylesheet"/>
<link href="css/responsive.css?v=8" rel="stylesheet"/>
<!--[if IE]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="https://qcdn-legacy.zhangzhongyun.com/vendor/mobile-detect/1.3.7/mobile-detect.min.js"></script>
</head>
<body>
<div id="preloader">
<div id="load">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</div>
<!-- ::::::::::::::::::::::::::: Header Start ::::::::::::::::::::::::::: -->
<header class="header_area">
<!-- Main Header Area Start -->
<div class="main_header_area">
<div class="container">
<div class="row">
<div class="col-sm-2 col-xs-9">
<!-- Logo Area:: For better view in all device please use logo image max-width 70px -->
<div class="logo_area">
<a href="/"><img alt="" src="img/core-img/logo.v1.png?v=3"/></a>
</div>
</div>
<div class="col-sm-10 col-xs-12">
<!-- Menu Area Start -->
<div class="main_menu_area">
<div class="mainmenu">
<nav>
<ul id="nav">
<li class="current_page_item"><a href="#home">é¦é¡µ</a></li>
<li><a href="#advantages">æä»¬çä¼å¿</a></li>
<li><a href="javascript:goDownload();">ä¸è½½</a></li>
<li><a href="#contact">èç³»æä»¬</a></li>
</ul>
</nav>
</div>
</div>
<!-- Menu Area End -->
</div>
</div>
</div>
</div>
<!-- Main Header Area End -->
</header>
<!-- ::::::::::::::::::::::::::: Header End ::::::::::::::::::::::::::: -->
<!-- ::::::::::::::::::::::::::: Welcome Area Start ::::::::::::::::::::::::::: -->
<section class="welcome_slider" id="home">
<div class="welcome_slides">
<div class="single_slide text-center" style="background-image: url(img/bg-pattern/slider-1.jpg);"></div>
<div class="single_slide text-center" style="background-image: url(img/bg-pattern/slider-2.jpg);"></div>
<div class="single_slide text-center" style="background-image: url(img/bg-pattern/slider-3.jpg);"></div>
</div>
</section>
<!-- ::::::::::::::::::::::::::: Welcome Area End ::::::::::::::::::::::::::: -->
<!-- ::::::::::::::::::::::::::: Features Area Start ::::::::::::::::::::::::::: -->
<div class="more_features_area section_padding_100_70" id="advantages">
<div class="container">
<div class="row">
<div class="col-xs-12">
<!-- Section Heading Start -->
<div class="section_heading">
<img src="img/advantage/heading.png"/>
</div>
<!-- Section Heading End -->
</div>
</div>
<div class="row">
<div class="col-sm-6 col-md-3">
<div class="single_benifits single_benifits1 wow fadeInUp item active" data-wow-delay="0.2s">
<div class="icon_box">
<img src="img/advantage/icon1.png"/>
</div>
<h4>é¶ææ¬æå¥</h4>
<p>0ææ¬æå¥ï¼ä»éæä¾è®¤è¯æå¡å·å³å¯å¿«éåå»ºèªå·±çå°è¯´ç½ç«</p>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="single_benifits single_benifits2 wow fadeInUp item" data-wow-delay="0.4s">
<div class="icon_box">
<img src="img/advantage/icon2.png"/>
</div>
<h4>å°è¯´æ­£çææ</h4>
<p>ç­¾çº¦å½åå¤§åä¸­æå°è¯´ç½ï¼æ­£çææ</p>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="single_benifits single_benifits3 wow fadeInUp item" data-wow-delay="0.6s">
<div class="icon_box">
<img src="img/advantage/icon3.png"/>
</div>
<h4>æ¶¨ç²å¿«</h4>
<p>ä»è´¹éè¯»ï¼ç²ä¸é»æ§é«ï¼æ¥æ¶¨åç²å¾è½»æ¾</p>
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="single_benifits single_benifits4 wow fadeInUp item" data-wow-delay="0.8s">
<div class="icon_box">
<img src="img/advantage/icon4.png"/>
</div>
<h4>ç»ç®å¿«</h4>
<p>å½å¤©ç»ç®ï¼æ¬¡æ¥ææ¬¾</p>
</div>
</div>
</div>
</div>
<div class="area_footer"></div>
</div>
<!-- ::::::::::::::::::::::::::: Features Area End ::::::::::::::::::::::::::: -->
<!-- ::::::::::::::::::::::::::: Download App Area Start ::::::::::::::::::::::::::: -->
<div class="contact_area" id="contact">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="section_heading">
<img src="img/contact/heading.png"/>
</div>
</div>
</div>
<div class="row" style="font-size:14px;">
<div class="col-xs-12 col-md-4">
<i class="fa fa-map-marker"></i><span class="contact_item">ç¦å·å¸é¼æ¥¼åºè½¯ä»¶å¤§é89å·ç¦å·è½¯ä»¶å­Fåº4å·æ¥¼</span>
</div>
<div class="col-xs-12 col-md-4 col-wechat">
<i class="fa fa-wechat"></i><span class="contact_item contact_item_wechat">åéåä½</span>
<img alt="" class="contact-qrcode" src="./img/wechat-code/qingtian11td.png"/>
</div>
<div class="col-xs-12 col-md-4">
<i class="fa fa-envelope-o"></i><span class="contact_item">é®ç®±: 1474668036@qq.com</span>
</div>
</div>
</div>
</div>
<!-- ::::::::::::::::::::::::::: Download App Area End ::::::::::::::::::::::::::: -->
<!-- ::::::::::::::::::::::::::: Footer Area Start ::::::::::::::::::::::::::: -->
<footer class="footer_area">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="friend_link">
<span>åæé¾æ¥ï¼</span>
<a href="http://www.weiyingxiao.com" target="_blank">å¾®è¥é</a>
<a href="http://www.xunmang.com" target="_blank">è¿èèªåªä½å©æ</a>
</div>
</div>
<div class="col-xs-12" style="margin-top:10px">
<!-- Footer Copwrite Area -->
<div class="copyright" style="text-align:center;line-height:1.8em;">
                        © <script>document.write(new Date().getFullYear())</script> çæææ.
                        <a href="http://beian.miit.gov.cn" target="_blank">
<script>document.write(location.hostname.indexOf('zhangzhongyun.com') >= 0 ? 'é½ICPå¤09047605å·-7' : 'é½ICPå¤09047605å·-2')</script>
</a>
<br/>
                        åºçç»è¥è®¸å¯è¯: æ°åºåé¼æåºå­ç¬¬266å· |
                        å¢å¼çµä¿¡ä¸å¡ç»è¥è®¸å¯è¯: é½B2-20180370 |
                        é½ç½æ(2017)8592-226å· 
                    </div>
</div>
</div>
</div>
</footer>
<!-- ::::::::::::::::::::::::::: Footer Area End ::::::::::::::::::::::::::: -->
<!-- ::::::::::::::::::::::::::: All jQuery Plugins ::::::::::::::::::::::::::: -->
<!-- jQuery (necessary for all JavaScript plugins) -->
<script src="https://qcdn.zhangzhongyun.com/vendor/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap js -->
<script src="https://qcdn.zhangzhongyun.com/vendor/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Owl-carousel js -->
<script src="js/owl.carousel.min.js"></script>
<script src="js/meanmenu.js"></script>
<!-- Onepage Nav js -->
<script src="js/jquery.nav.js"></script>
<!-- jQuery easing js -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Back to top js -->
<script src="js/jquery.scrollUp.js"></script>
<!-- MatchHeight js -->
<script src="js/jquery.matchHeight-min.js"></script>
<!-- Waypoints js -->
<script src="js/waypoints.js"></script>
<!-- Wow JS -->
<script src="js/wow.min.js"></script>
<!-- Active js -->
<script src="js/custom.js?v=5"></script>
</body>
</html>
