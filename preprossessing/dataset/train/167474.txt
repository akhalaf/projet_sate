<!DOCTYPE html>
<html lang="de">
<head>
<title>Ungarische Übersetzungen - ATS Übersetzungsbüro - Ungarisch Deutsch Übersetzer</title>
<meta content="Sie benötigen professionelle ungarische Übersetzungen für Ihre Geschäftsberichte, Vertröge oder technische Handböcher durch Fachübersetzer. Ihr Fachübersetzungsdienst bietet Übersetzungen ungarisch, deutsch, english, französisch und spanisch professionell, schnell und zuverlässig." name="description"/>
<meta content="LQvY1o4LKQI1CB-9P2USpYpk-HE9aF0a1SBCvERhUpA" name="google-site-verification"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="de" http-equiv="content-language"/>
<meta http-equiv="archive.org_bot content=" noindex=""/>
<meta content="index,follow" name="robots"/>
<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<link href="css/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ats-group.net/index-2.html" rel="canonical"/>
<meta content="service" property="og:type"/>
<meta content="German Hungarian Translation Services" property="og:title"/>
<meta content="https://www.ats-group.net/index-2.html" property="og:url"/>
<meta content="ATS-Translation" property="og:site_name"/>
<meta content="Sie benötigen professionelle ungarische Übersetzungen für Ihre Geschäftsberichte, Vertröge oder technische Handböcher? durch Fachübersetzer. Ihr Fachübersetzungsdienst bietet Übersetzungen ungarisch, deutsch, english, französisch und spanisch professionell, schnell und zuverlässig." property="og:description"/>
<script async="" src="js/jquery-1.8.0.min.js"></script>
<script async="" src="js/jquery.touchwipe.1.1.1.js"></script>
<script async="" src="js/jquery.carouFredSel-5.5.0-packed.js"></script>
<!--[if lt IE 9]><script async src="js/modernizr.custom.js"></script><![endif]-->
<script async="" src="js/functions.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-0611072400049090",
    enable_page_level_ads: true
  });
</script>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"Durch Nutzung dieser Website stimmen Sie der Verwendung von Cookies f&#252;r Analysen, personalisierte Inhalte und Werbung zu. Weitere Informationen &#252;ber Cookies finden Sie in unserer Datenschutzerkl&auml;rung.","dismiss":"Na gut!","learnMore":"Mehr Infos","link":"https://www.ats-group.net/deutsch/datenschutz.html","theme":"dark-bottom"};
</script>
<script async="" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js" type="text/javascript"></script>
<!-- End Cookie Consent plugin -->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <script> (adsbygoogle = window.adsbygoogle || []).push({ google_ad_client: "ca-pub-0611072400049090", enable_page_level_ads: true }); </script>
</head>
<body>
<!-- wrapper -->
<div class="wrapper">
<!-- header -->
<header class="header">
<div class="shell">
<div class="header-top">
<div itemscope="" itemtype="http://schema.org/Organization"><h1 id="logo"><a href="https://www.ats-group.net/" itemprop="url" title="ATS Translation"><span itemprop="name">ATS Translation</span></a></h1></div>
<nav id="navigation"> <a class="nav-btn" href="https://www.ats-group.net/index.html">Sprachendienstleister<span></span></a>
<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
<li class="active home" itemprop="name"><a href="https://www.ats-group.net/" itemprop="url"><span itempop="">Home</span></a><meta content="1" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/deutsch/leistungen.html" itemprop="url" title="Leistungen">Leistungen</a><meta content="2" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/deutsch/preis.html" itemprop="url" title="Preise">Preise</a><meta content="3" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/sprache-ressourcen.html" itemprop="url" title="Sprachen">Sprachen</a><meta content="4" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/deutsch/uebersetzung-ressourcen.html" itemprop="url" title="Übersetzungen">Übersetzungen</a><meta content="5" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/woerterbuecher-deutsch.html" itemprop="url" title="Wörterbücher">Wörterbücher</a><meta content="6" itemprop="position"/></li>
<li itemprop="name"><a href="https://www.ats-group.net/deutsch/kontakt.html" itemprop="url" title="Kontakt">Kontakt</a><meta content="7" itemprop="position"/></li>
</ul>
</nav>
<div class="cl"> </div>
</div>
<div class="slider2">
<div id="bg"></div>
</div>
</div>
<!-- <div class="pagination"></div>
        <a id="prev" href="#"></a> <a id="next" href="#"></a> </div>
    </div> -->
</header>
<!-- end of header -->
<!-- shell -->
<div class="shell">
<!-- main -->
<div class="main">
<div class="socials">
<ol itemscope="" itemtype="http://www.schema.org/BreadcrumbList">
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/philosophie.html" itemprop="item"><span itemprop="name">Sprachendienst und Übersetzungsbüro</span></a><meta content="1" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/referenzen.html" itemprop="item"><span itemprop="name">Referenzen</span></a><meta content="2" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/preis.html" itemprop="item"><span itemprop="name">Preise</span></a><meta content="3" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/leistungen.html" itemprop="item"><span itemprop="name">Leistungen</span></a><meta content="4" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/fachgebiete.html" itemprop="item"><span itemprop="name">Fachgebiete</span></a><meta content="5" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/dolmetschen.html" itemprop="item"><span itemprop="name">Dolmetschen</span></a><meta content="6" itemprop="position"/></li><li>&gt;</li>
<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="https://www.ats-group.net/deutsch/express-uebersetzungen.html" itemprop="item"><span itemprop="name">Express</span></a><meta content="7" itemprop="position"/></li>
</ol>
</div>
<section class="post"> <img alt="" src="css/images/science_and_technology.jpg"/>
<div class="post-cnt">
<h2>Professionelle Ungarisch-Übersetzungen mit Schwerpunkt in Deutsch, Spanisch, Französisch und Englisch.</h2>
<div class="before"></div>
<h4>Unser Übersetzungsbüro bietet professionelle Übersetzungen Ungarisch-Deutsch, Ungarisch-Spanisch, Englisch-Ungarisch oder Französisch-Ungarisch an.</h4>
<div class="before"></div>
<p>Im Fokus des Unternehmens stehen Fachübersetzungen und Übersetzungen aus dem Ungarischen ins Deutsche und aus dem Deutschen ins Ungarische in allen Bereichen und Branchen für Firmen und Privatpersonen.</p>
<h3>Über uns</h3>
<ul>
<li>Für Übersetzungen vom und ins Ungarisch, dann sind Sie bei uns richtig!</li>
<li>Hoch qualifizierte und erfahrene Übersetzer (ausschließlich Muttersprachler) für Ungarisch, Deutsch, Englisch, Spanisch und Französich.</li>
<li>Unsere Auftraggeber setzen sich aus dem öffentlichen, industriellen und privaten Bereich zusammen.</li>
<li>Auch das Spektrum der von uns bearbeiteten Projekte reicht von IT, Informatik/Lokalisierung, Industrie, Handel, Wirtschaft, Medizin, Naturwissenschaften und Mathematik bis hin zu technischen Dokumentationen. Hierbei sind  für uns Fachkenntnisse und Branchenwissen selbstverständlich.</li>
<li><a href="https://www.ats-group.net/deutsch/referenzen.html">Hier finden Sie einige Referenzen zufriedener Kunden</a></li>
</ul>
<div class="before"></div>
<h3>Warum wir?</h3>
<ul>
<li>Wir bieten unseren Kunden einen kompletten Service rund um das Thema Übersetzen und Dolmetschen.</li>
<li>Wir arbeiten schnell, zuverlässig, diskret und auf hohem Niveau zu sehr fairen Preisen!</li>
<li>Kurze Reaktionszeiten und unkomplizierter Ablauf von der Anfrage bis hin zur Auftragserfüllung.</li>
<li>Desktop Publishing, Lektorat und Transkription</li>
<li>Ihre Übersetzer und Dolmetscher von morgen in den Sprachen <b>Ungarisch, Englisch, Französisch, Spanisch und Deutsch!</b></li>
<li><b>ATS-Translation</b>, Ihr Fachpartner für ungarische Übersetzungen.</li>
<li>Ungarisch-Übersetzer nach Norm DIN-15038  und und ISO-9001.</li>
</ul>
<h4>Unser Übersetzungsbüro übernimmt für Sie ungarische Übersetzungen von muttersprachlichen Ungarisch-Übersetzern:</h4>
<div class="before"></div>
<ul>
<li>Übersetzungen <b>Ungarisch ⇔ Deutsch</b></li>
<li>Übersetzungen <b>Ungarisch ⇔ Englisch</b></li>
<li>Übersetzungen <b>Ungarisch ⇔ Spanisch</b></li>
<li>Übersetzungen <b>Ungarisch ⇔ Französisch</b></li>
</ul>
<div class="before"></div>
<ul>
<li>Übersetzungen <b>Deutsch ⇔ Ungarisch</b></li>
<li>Übersetzungen <b>Deutsch ⇔ Englisch</b></li>
<li>Übersetzungen <b>Deutsch ⇔ Spanisch</b></li>
<li>Übersetzungen <b>Deutsch ⇔ Französisch</b></li>
</ul>
<h3>Unsere Fachgebiete</h3>
<p><b>IT/ Informatik/ Lokalisierung:</b> EDV, Telekommunikation, Websites, Onlinehilfen, E-Content, Netzwerke, LAN, WLAN, Hardware und Software.</p>
<p><b>Industrie:</b> Automobilindustrie, Bauindustrie, Fernmeldetechnik, Mess- und Steuerungstechnik, Elektronik, Mikroelektronik, Datentechnik, Automatisierung, Energie und Maschinenbau.<br/>
</p><p><b>Technische Texte</b>: Handbücher, Bedienungsanleitungen, Pflichtenhefte, Maschinenbeschreibungen und Kataloge. <a href="https://www.ats-group.net/deutsch/technische-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
<p><b>Wirtschaft:</b> Finanzwesen, Bankwesen, Marketing, PR, Werbung, Naturschutz, Forstwirtschaft, Weinbau, Önologie und Volks- und Betriebswirtschaft. wirtschaftliche Texte: Bilanzen, Präsentationen, Pressemitteilungen, Geschäftsberichten und Vorträgen. <a href="https://www.ats-group.net/deutsch/wirtschaft-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
<p><b>Medizin (Medizinische Texte):</b> Medizintechnik, Pharmazeutik, Pharma und Pharmaindustrie. <a href="https://www.ats-group.net/deutsch/medizin-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
<p><b>Handel:</b> Import, Export, Zoll, Wareneinfuhr, Logistik, Ausstellungen und Messen. <a href="https://www.ats-group.net/deutsch/handel-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
<p><b>Dokumentat/Texte:</b> Handbücher, Textredaktion, Artikel, Briefe, Manuals, Pressetexte, Vertragstexte, Betriebsanleitungen, Verpackungstexte, Korrespondenz, Firmenprospekte, Bücher, Magazine, Korrekturlesen und Lektorat. <a href="https://www.ats-group.net/deutsch/text-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
<p><b>Naturwissenschaften und Mathematik:</b> Chemie ,Geografie, Mathematik und Physik. <a href="https://www.ats-group.net/deutsch/naturwissenschaften-uebersetzung.html" style="text-decoration: none;">&gt;&gt;&gt;</a></p>
</div>
<div class="cl"> </div>
</section>
<!-- cols -->
<section class="cols">
<div class="col"> <img alt="" src="css/images/col-img1.png"/>
<div class="col-cnt">
<h2>Technisch</h2>
<div class="before"></div>
<a class="more" href="https://www.ats-group.net/deutsch/technische-uebersetzung.html">mehr...</a> </div>
</div>
<div class="col"> <img alt="" src="css/images/col-img2.png"/>
<div class="col-cnt">
<h2>Lokalisierung</h2>
<div class="before"></div>
<a class="more" href="https://www.ats-group.net/deutsch/lokalisierung-uebersetzung.html">mehr...</a> </div>
</div>
<div class="col"> <img alt="" src="css/images/col-img3.png"/>
<div class="col-cnt">
<h2>Medizin</h2>
<div class="before"></div>
<a class="more" href="https://www.ats-group.net/deutsch/medizin-uebersetzung.html">mehr...</a> </div>
</div>
<div class="cl"> </div>
</section>
<!-- end of cols -->
<section class="content">
<h2>Deutsch Ungarisch Fachübersetzungen</h2>
<p>Professionelle Fachübersetzungen Ungarisch Deutsch und Ungarisch Deutsch für die Bereiche Wirtschaft, Recht, Technik, Geschäftsbericht, Bilanz, Marketing, Wissenschaft und Medizin. <br/>
<a class="more" href="https://www.ats-group.net/deutsch/fachuebersetzungen.html">mehr...</a></p>
<div class="before"></div>
<p>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- respv -->
<ins class="adsbygoogle" data-ad-client="ca-pub-0611072400049090" data-ad-format="auto" data-ad-slot="3115073653" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>
<!--
<div class="before"></div>
<p>
<style type="text/css">
@import url(http://www.google.com/cse/api/branding.css);
</style>
<div class="cse-branding-right" style="background-color:#FFFFFF;color:#000000">
  <div class="cse-branding-form">
    <form action="http://www.google.com" id="cse-search-box" target="_blank">
      <div>
        <input type="hidden" name="cx" value="partner-pub-0611072400049090:rdcm83-t2u6" />
        <input type="hidden" name="ie" value="ISO-8859-1" />
        <input type="text" name="q" size="31" />
        <input type="submit" name="sa" value="Suche" />
      </div>
    </form>
  </div>
  <div class="cse-branding-logo">
    <img src="http://www.google.com/images/poweredby_transparent/poweredby_FFFFFF.gif" alt="Google" />
  </div>
  <div class="cse-branding-text">
    Benutzerdefinierte Suche
  </div>
</div> -->
<div class="before"></div>
</section>
<section class="partners">
<div id="share-buttons">
<!-- Facebook -->
<a href="https://www.facebook.com/sharer.php?u=https://www.ats-group.net" target="_blank">
<img alt="Facebook" src="https://www.ats-group.net/images/facebook.png"/></a>
<!-- Google+ -->
<a href="https://plus.google.com/share?url=https://www.ats-group.net" target="_blank">
<img alt="Google" src="https://www.ats-group.net/images/google.png"/></a>
<!-- Twitter -->
<a href="https://twitter.com/share?url=https://www.ats-group.net&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons" target="_blank">
<img alt="Twitter" src="https://www.ats-group.net/images/twitter.png"/></a>
<!-- Buffer -->
<a href="https://bufferapp.com/add?url=https://www.ats-group.net&amp;text=Simple Share Buttons" target="_blank">
<img alt="Buffer" src="https://www.ats-group.net/images/buffer.png"/></a>
<!-- Digg -->
<a href="https://www.digg.com/submit?url=https://www.ats-group.net" target="_blank">
<img alt="Digg" src="https://www.ats-group.net/images/diggit.png"/></a>
<!-- Email -->
<a href="mailto:?Subject=Simple Share Buttons&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 https://www.ats-group.net">
<img alt="Email" src="https://www.ats-group.net/images/email.png"/></a>
<!-- LinkedIn -->
<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.ats-group.net" target="_blank">
<img alt="LinkedIn" src="https://www.ats-group.net/images/linkedin.png"/></a>
<!-- Pinterest -->
<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
<img alt="Pinterest" src="https://www.ats-group.net/images/pinterest.png"/></a>
<!-- Print -->
<a href="javascript:;" onclick="window.print()">
<img alt="Print" src="https://www.ats-group.net/images/print.png"/></a>
<!-- Reddit -->
<a href="https://reddit.com/submit?url=https://www.ats-group.net&amp;title=Simple Share Buttons" target="_blank">
<img alt="Reddit" src="https://www.ats-group.net/images/reddit.png"/></a>
<!-- StumbleUpon-->
<a href="https://www.stumbleupon.com/submit?url=https://www.ats-group.net&amp;title=Simple Share Buttons" target="_blank">
<img alt="StumbleUpon" src="https://www.ats-group.net/images/stumbleupon.png"/></a>
<!-- Tumblr-->
<a href="http://www.tumblr.com/share/link?url=https://www.ats-group.net&amp;title=Simple Share Buttons" target="_blank">
<img alt="Tumblr" src="https://www.ats-group.net/images/tumblr.png"/></a>
<!-- VK -->
<a href="http://vkontakte.ru/share.php?url=https://www.ats-group.net" target="_blank">
<img alt="VK" src="https://www.ats-group.net/images/vk.png"/></a>
<!-- Yummly -->
<a href="https://www.yummly.com/urb/verify?url=https://www.ats-group.net&amp;title=Simple Share Buttons" target="_blank">
<img alt="Yummly" src="https://www.ats-group.net/images/yummly.png"/></a>
<div class="socials">
<p>Fragen Sie uns am besten per Email <a href="mailto:ats@ats-group.net">ats [at] ats-group [punkt] net</a></p>
<ul>
<li><a class="german-ico" href="https://www.ats-group.net/index.html">german-ico</a></li>
<li><a class="english-ico" href="https://www.ats-group.net/index-2.html">english-ico</a></li>
<li><a class="hungarian-ico" href="https://www.ats-group.net/index-1.html">hungarian-ico</a></li>
<li><a class="french-ico" href="https://www.ats-group.net/index-3.html">french-ico</a></li>
</ul>
</div>
</div>
<!-- end of main -->
</section></div>
<!-- end of shell -->
<!-- footer -->
<div id="footer">
<!-- shell -->
<div class="shell">
<!-- footer-cols -->
<div class="footer-cols">
<div class="col">
<h2>Übersetzer</h2>
<ul>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/philosophie.html">Philosophie</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/referenzen.html">Referenzen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/leistungen.html">Leistungen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/fachgebiete.html">Fachgebiete</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/dolmetschen.html">Dolmetschen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/arbeitsmittel.html">Arbeitsmittel</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/lieferungen-uebersetzungen.html">Lieferung</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/lieferzeiten-uebersetzungen.html">Lieferzeiten</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/publishing.html">Dateiformat</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/lektorieren-uebersetzungen.html">Qualität</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/team.html">Team</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/andere_leistungen.html">Service</a></li>
</ul>
</div>
<div class="col">
<h2>Preise</h2>
<ul>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/preis.html">Übersetzungen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/preis_dolmetschen.html">Dolmetschen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/preis-lektorieren.html">Lektorat und Korrektorat</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/angebot.html">Angebote zur Übersetzung</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/preis-uebersetzungen.html">Preiskalkulation Übersetzung</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/express-uebersetzungen.html">Express-Übersetzung</a></li>
</ul>
</div>
<div class="col">
<h2>Sprachen</h2>
<ul>
<li><a class="navunten" href="https://www.ats-group.net/sprachen/dialekte.html">Dialekt Online</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/deutsch-dialekte.html">Dialekt Deutsch</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/deutsch-dialekte.html">Bücher</a></li>
<li><a class="navunten" href="https://www.ats-group.net/glossar-lexikon.html">Glossar</a> ∼ <a class="navunten" href="https://www.ats-group.net/buecher/glossar-lexikon.html">Glossar (Bücher)</a> ∼ <a class="navunten" href="https://www.ats-group.net/medizin/glossar-gesundheit-medizin.html">Medizin</a></li>
<li><a class="navunten" hrerf="https://www.ats-group.net/sprachen/fremdsprachen.html">Sprachen</a> - <a class="navunten" hrerf="https://www.ats-group.net/buecher/fremd-sprachen.html">Bücher</a> ∼ <a class="navunten" href="https://www.ats-group.net/deutsch/fremdsprachen-lernen.html">Sprachen Lernen</a> ∼ <a class="navunten" hrerf="https://www.ats-group.net/grammatik/index.html">Grammatik</a> - <a class="navunten" hrerf="https://www.ats-group.net/buecher/grammatik-sprachen.html">Grammatik (Bücher)</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprache-ressourcen.html">Sprachen Ressourcen</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/podcasts.html">Podcasts</a> ∼ <a class="navunten" href="https://www.ats-group.net/buecher/index.html">Sprachbücher</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/deutsch.html">Deutsch</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/englisch.html">Englisch</a></li>
<li><a class="navunten" href="https://www.ats-group.net/sprachschule/index.html">Sprachkurse</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachreisen/index.html">Sprachreisen</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/schule.html">Sprachschule</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/sprachzertifikate.html">Sprachzertifikate</a> ∼ <a class="navunten" href="https://www.ats-group.net/sprachen/unterricht.html">Unterricht</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/ungarisch.html">Ungarisch</a> ∼  <a class="navunten" href="https://www.ats-group.net/sprachen/uebersetzer-tools.html">Übersetzer Tools</a> ∼ <a class="navunten" href="https://www.ats-group.net/woerterbuecher-deutsch.html">Wörterbuch</a></li>
</ul>
</div>
<div class="col">
<h2>Unternehmen</h2>
<ul>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/kontakt.html">Kontakt</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/formular.html">Online-Anfrage</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/fachuebersetzungen.html">Fachübersetzungen</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/jobs.html">Fachübersetzer Jobs</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/agb.html">AGB</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/datenschutz.html">Datenschutz</a></li>
<li><a class="navunten" href="https://www.ats-group.net/deutsch/impressum.html">Impressum</a></li>
</ul>
</div>
<div class="cl"> </div>
</div>
<!-- end of footer-cols -->
<div class="footer-bottom">
<div class="footer-nav">
<ul>
<li><a class="navunten" href="https://www.ats-group.net/index.html">Home</a></li>
<li><a href="https://www.ats-group.net/deutsch/sitemap.html" title="Sitemap">Sitemap</a></li>
<li><a class="navunten" href="https://www.ats-group.net/index-2.html">Englisch</a></li>
<li><a class="navunten" href="https://www.ats-group.net/index-3.html">Französisch</a></li>
<li><a class="navunten" href="https://www.ats-group.net/index-1.html">Ungarisch</a></li>
<li><a class="navunten" href="https://www.ats-group.net/zeitungen/index.html">Zeitungen</a></li>
</ul>
</div>
<footer class="site-footer" itemscope="" itemtype="https://schema.org/WPFooter" role="contentinfo"><p class="copy"><span itemprop="creator">Übersetzungsbüro-Dolmetscherdienst</span> | <span content="2001-2020" itemprop="copyrightYear">© 2001-2020</span> | <span itemprop="copyrightHolder">ATS Translation</span></p></footer>
<div class="cl"> </div>
</div>
</div>
<!-- end of shell -->
</div>
<!-- footer -->
</div>
<!-- end of wrapper -->
<script> 
    var gaProperty = 'UA-105322803-1'; 
    var disableStr = 'ga-disable-' + gaProperty; 
    if (document.cookie.indexOf(disableStr + '=true') > -1) { 
        window[disableStr] = true;
    } 
    function gaOptout() { 
        document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/'; 
        window[disableStr] = true; 
        alert('Das Tracking ist jetzt deaktiviert'); 
    } 
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); 

    ga('create', 'UA-105322803-1', 'auto'); 
    ga('set', 'anonymizeIp', true); 
    ga('send', 'pageview'); 
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b4f2178f463c467" type="text/javascript"></script>
</div></body>
</html>