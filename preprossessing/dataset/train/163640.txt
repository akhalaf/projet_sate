<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Sign Up Astral Media | Astral Group Partner</title>
<link href="layout/favicon.ico" rel="icon" type="image/png"/>
<meta content="Astral Media developes and designs websites for your company, host your website and do online advertising" name="description"/>
<meta content="Astral Media, Astral Group, Web Design, Web Development, Online Advertising, Advertising, Hosting, Web Hosting, Host, Durban, South Africa" name="keywords"/>
<meta content="English" name="language"/>
<meta content="never" name="expires"/>
<meta content="Copyright 2011" name="copyright"/>
<meta content="Astral Media" name="publisher"/>
<link href="css/custom.css" media="screen" rel="stylesheet"/>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> </td>
<td bgcolor="#FBFBFB" width="990">
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="10"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"> </td>
<td width="205"><a class="opaque" href="http://www.astralmedia.co.za/" target="_self"><img alt="Astral Media" border="0" height="92" src="layout/logo.jpg" width="205"/></a></td>
<td width="469"> </td>
<td><img alt="031 301 5555" height="62" src="layout/call.jpg" width="267"/></td>
<td width="24"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="3"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td><img alt="" height="12" src="layout/top-bottom.jpg" width="990"/></td>
</tr>
</table>
</td>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> </td>
<td width="990">
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="30"> </td>
</tr>
</table>
</td>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> </td>
<td width="843">
<table border="0" cellpadding="0" cellspacing="0" width="843">
<tr>
<td width="5"><img alt="" height="50" src="layout/nav-left.jpg" width="5"/></td>
<td align="center" width="165"><a class="nav" href="index.html">HOMEPAGE</a></td>
<td bgcolor="#AE0939" width="1"></td>
<td bgcolor="#BC345B" width="1"></td>
<td align="center" width="165"><a class="nav" href="products.html">PRODUCTS</a></td>
<td bgcolor="#AE0939" width="1"></td>
<td bgcolor="#BC345B" width="1"></td>
<td align="center" width="165"><span class="active">SIGN UP</span></td>
<td bgcolor="#AE0939" width="1"></td>
<td bgcolor="#BC345B" width="1"></td>
<td align="center" width="165"><a class="nav" href="portfolio.html">PORTFOLIO</a></td>
<td bgcolor="#AE0939" width="1"></td>
<td bgcolor="#BC345B" width="1"></td>
<td align="center" width="165"><a class="nav" href="contact.html">CONTACTS</a></td>
<td width="5"><img alt="" height="50" src="layout/nav-right.jpg" width="5"/></td>
</tr>
</table>
</td>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> </td>
<td height="30" width="990"> </td>
<td> </td>
</tr>
</table>
<form action="signup.php" id="user_registration" method="post" name="user_registration">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> </td>
<td bgcolor="#FFFFFF" width="990">
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td><img alt="" height="20" src="layout/main-top.jpg" width="990"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"> </td>
<td width="80"><img alt="" height="59" src="layout/personal-details.jpg" width="68"/></td>
<td width="311"><h1>PERSONAL DETAILS</h1></td>
<td width="54"> </td>
<td bgcolor="#E7E7E7" width="1"></td>
<td width="37"> </td>
<td width="100"><img alt="" height="67" src="layout/debit-order.jpg" width="95"/></td>
<td width="357"><h1>DEBIT ORDER DETAILS</h1></td>
<td width="25"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"> </td>
<td align="right" style="color:#FF0000;font-size:10px;" width="391">* Denotes a required field</td>
<td width="54"> </td>
<td bgcolor="#E7E7E7" width="1"></td>
<td width="37"> </td>
<td width="457"> </td>
<td width="25"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"> </td>
<td valign="top" width="391">
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="25">Select Product <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20"><strong>Offer Inclusive of VAT and the following:</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20" style="color:#FF0000;">•   Domain Registration (www.yourbusinessname.co.za)</td>
</tr>
<tr>
<td height="20" style="color:#FF0000;">•   4 Page Website Design</td>
</tr>
<tr>
<td height="20" style="color:#FF0000;">•   100 Email Accounts (yourname@yourbusinessname.co.za)</td>
</tr>
<tr>
<td height="20" style="color:#FF0000;">•   Flash or Slideshow Image Presentation</td>
</tr>
<tr>
<td height="20" style="color:#FF0000;">•   Search Engine Optimisation (google, yahoo etc.) SEO Analytics</td>
</tr>
<tr>
<td height="20" style="color:#FF0000;">•   Social Networking Links (facebook, twitter etc)</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">Customer name <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><input class="signup" maxlength="80" name="customername" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">Trading name <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><input class="signup" maxlength="80" name="tradingname" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">ID Number or Business Registration Number <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><input class="signup" maxlength="80" name="idnumber" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">Email address <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><input class="signup" maxlength="80" name="email" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">Postal address <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><textarea class="signup" cols="20" name="postal" rows="20" style="height:80px; font-family:Arial, Helvetica, sans-serif; overflow:auto;"></textarea></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20">Physical address</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td><textarea class="signup" cols="20" name="physical" rows="20" style="height:80px; font-family:Arial, Helvetica, sans-serif; overflow:auto;"></textarea></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20" width="190">Suburb</td>
<td width="11"> </td>
<td width="190">Postal code</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td width="190"><input class="signup2" maxlength="80" name="suburb" type="text"/></td>
<td width="11"> </td>
<td width="190"><input class="signup2" maxlength="80" name="code" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td height="20" width="190">Telephone <strong style="color:#FF0000;">*</strong></td>
<td width="11"> </td>
<td width="190">Fax</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="391">
<tr>
<td width="190"><input class="signup2" maxlength="80" name="telephone" type="text"/></td>
<td width="11"> </td>
<td width="190"><input class="signup2" maxlength="80" name="fax" type="text"/></td>
</tr>
</table>
</td>
<td width="54"> </td>
<td bgcolor="#E7E7E7" width="1"></td>
<td width="37"> </td>
<td valign="top" width="457">
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td width="40"><img alt="Information" height="27" src="layout/information.jpg" width="27"/></td>
<td style="color:#FF0000;" width="417">By completing this and accepting the terms and conditions, the customer/its representative hereby authorises Astral Media to debit from the account specified below:</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td width="40"><img alt="Number 1" height="36" src="layout/number-1.jpg" width="35"/></td>
<td width="417">Monthly Service Fees (including any pro-rata amounts or any Service Fees charged monthly) or any such overdue Monthly Service Fees (including relevant interest and / or charges in terms of the Agreement) on the specified debit date of each calendar month; or</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td width="40"><img alt="Number 2" height="36" src="layout/number-2.jpg" width="35"/></td>
<td width="417">Annual Service Fees or any such overdue Annual Services Fees (including relevant interest and / or charges in terms of the Agreement) on the specified debit date of each calendar month following the commencement of the Agreement or the Service and annually thereafter. This authority will remain in full force for the duration of the Agreement and any extensions thereof or whilst any amounts, including Service Fees remain outstanding.</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20">Bank name <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td><input class="signup" maxlength="80" name="bankname" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20">Account holder <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td><input class="signup" maxlength="80" name="accountholder" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20">Branch code <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td><input class="signup" maxlength="80" name="branchcode" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20">Branch name <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td><input class="signup" maxlength="80" name="branchname" type="text"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20">Account number <strong style="color:#FF0000;">*</strong></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td width="190"><input class="signup2" maxlength="80" name="accountnumber" type="text"/></td>
<td width="11"></td>
<td><label><input checked="checked" name="debitdate" type="radio" value="Twenty-fifth"/> 25<sup>th</sup></label> <label><input name="debitdate" type="radio" value="Last working day"/> Last working day</label></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="8"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="20"><label><input checked="checked" id="checkbox" name="checkbox" type="checkbox" value="Yes"/> I agree that I have read and understand the <a href="privacy.html">terms and conditions</a></label></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td height="50"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="457">
<tr>
<td><input class="button" name="submit" type="submit" value="Submit Form"/></td>
</tr>
</table>
</td>
<td width="25"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="30"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"></td>
<td bgcolor="#EEEEEE" height="1" width="940"></td>
<td width="25"></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="30"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td height="30"> </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td width="25"> </td>
<td>© 2012 Astral Media  |  <a href="privacy.html">Privacy Policy</a></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="990">
<tr>
<td><img alt="" height="20" src="layout/main-bottom.jpg" width="990"/></td>
</tr>
</table>
</td>
<td> </td>
</tr>
</table>
</form>
<br/><br/><br/>
<script src="js/validate.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Franklin_Gothic_Book_400-Franklin_Gothic_Book_400.font.js" type="text/javascript"></script>
<script type="text/javascript">
    Cufon.replace('h1, h2', { fontFamily: 'TitilliumMaps26L', hover: true });
	function handleError() { return true; }
	window.onerror = handleError;
	Cufon.replace('h1');
	Cufon.replace('h2');
	Cufon.replace('.red');
</script>
<script>
error_color = '#d7d7d7';
function validateuser_registration(formname) {

if (!minLength(formname.customername,3,"Please enter a Customer Name !")) return false;else
if (!IsEmpty(formname.tradingname,"Please enter a Trading Name !")) return false;else
if (!IsPhone(formname.idnumber,"Please enter an ID number !")) return false;else
if (!isEmail(formname.email,"You must enter a valid email !")) return false;else
if (!IsEmpty(formname.postal,"Please enter a Postal Address !")) return false;else
if (!IsPhone(formname.telephone,"Please enter your Telephone Number !")) return false;else
if (!IsEmpty(formname.bankname,"Please enter your Bank !")) return false;else
if (!IsEmpty(formname.accountholder,"Please type in the Account Holder !")) return false;else
if (!IsPhone(formname.branchcode,"Please type in your Branch Code !")) return false;else
if (!IsEmpty(formname.branchname,"Please type in your Branch Name !")) return false;else
if (!IsEmpty(formname.accountnumber,"Please enter your Account Number !")) return false;else
;
return true;};
function user_registrationinit(e)
{
	if ( validateuser_registration(document.user_registration) == false ) 
	{
		if (!e) var e = window.event;
		if (e.preventDefault) {
		      e.preventDefault();
		      e.stopPropagation();
		} else {
		      e.returnValue = false;
		      e.cancelBubble = true;
		}		
		return false;
	} else
	{
		return true; 
	}
	//cancel submit if validate returns false;
}	

if (document.getElementById('user_registration').addEventListener)
{
  document.getElementById('user_registration').addEventListener('submit', user_registrationinit, false); 
} else if (document.getElementById('user_registration').attachEvent)
{
  document.getElementById('user_registration').attachEvent('onsubmit', user_registrationinit);
} else
{
	document.getElementById('user_registration').onclick = user_registrationinit;
}
</script>
</body>
</html>