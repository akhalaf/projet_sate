<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "12003",
      cRay: "610dd93bee61d96a",
      cHash: "4d4e5b66d5c87c5",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly9hbnN3ZWFyLmh1Lw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "Zb8aAJJ0WIY0N9AFZzRmHtdesG7JEy+hDXk+8iwC8sGXJq6Xw0XGjsJrx2cgtkdceRUNOJ4OL9VPv26MamQWZnA3eIz6eRjiw59wloKiJsgoGRvSnUytABXJ1jbNuCUC99kYM5ZZTNz9yY/bl9vMvLcrtREbzSGQd/XLXZDmcyAre+L27LSs0Yb/urfAqJOgKLKT6JPmmoGbt1LkfjegsAnM93MrEgrHFEflbkQvkYoSC3/tSHQj60KbephxC6gMgwqplXJqhR2B7m8sxeFw9hiBqtia9gKp+55ZblPVHYZKwsOuvdo4NEL8hI5g1cqZxwseyBxkuY+ejPgMta7eVzNUyo+iz/9uPsNE56OCKOzfa8LDyQRfzlwzncU41DHC94XQ5UDEEAMR9yIbNr6Q90t+jG7KjEdZwG527dYeL8nGb9/+GHiDHJddzeSUEnA1U6fCTQqki/fCIl2IYiSdRvSPZo4PvN1VNCxBxvsGr5ed4VHLoNMp8vAHXSzgdBXJ8Ik4aJ4syAye30nLAgSwKSDEbX6lWD9HPz1syjGvz9Y7+P3o+VVe7o9r5oAxXYN7uyUkfPuqrKhx3/nIJlR92UDfa37J02YXoPdp6sQSmoz4APCGjFvIxygkXQO51E2tlWMxg54Jl4rAQvr9zPL0lDfDup5lQJLw4h6y0th4mGi3m2OB/z/B6HNHqJ9TC6hPsVYZQzP/voNYzFvsf/64OQ2hjJNnn3qzIuKr+d7nB/U5AgPZ6h9JWU0dvEsKh8IN",
        t: "MTYxMDUyNzYwNS4xMTAwMDA=",
        m: "lacrnhIeD7HdOCW8+fvfcbS0Qk5f+HD/tTMc4OQBWqQ=",
        i1: "GczZmuSHMEZADH9D/ENGKg==",
        i2: "bH6BYRsw7rK11IRo4J762w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "GIrtTTBIKxJC9Er2bjqZe5eGID4fVZVxLlXX2NTc7oI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610dd93bee61d96a");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> answear.hu.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=c9b065bc803908efd40c339ae5e55095e251f00b-1610527605-0-AW5i-rT0A1o8Xu0EXCE9sY8cZHYbLEpxsS0-YjVACcXUwmTd3BhiU109bjug6CHlSY-k0fal9l03x8hI7_c33UTe3RJMdrTM2ZdMcwYKA7MQlPvki7TDOuXM5xRjSbSUM36FJj9vWgLiEWmsKcsWP3qVWz7B8vrfQ5aeV0ql_WlvGKrNqezhe_uk733cOFB8gxs1p8Q_N49YD7ddXoH3zt_AjSOt8NawtBNotvYX7LVG3P1ir-AMtYBuF5NLw8LdpY8IUTMX_GWgCirsmBSNpq56KVybBHxVvmZnMNjWPxSLUrGvC1JQxOthIOJHcYYEzw" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="0568f29fecd65bf69474bd6e81af3898f83a1b61-1610527605-0-Abip7ROJQaqtCRsAjsxBhZoMalCZO/+17wKNBQjtJeQMMyCxWFQCUseBXPEXgKPS2alv+QpGBmXLYAogGDX7YErd88iPfOr4UnhvFT9i88PyP7WNbansumAv/n0LQjTtVjPq+MvE9S+pQbWX9T4d6CKAIRFMfm0Z9aADy+hViE8yqw76Q3AyrDFkko22VntzBawOBoYv5url+fIzlydZafvXKTQSwTsvmVyHD+cTNqhZyo97uHGkC5/v13ehMV+7nggiStTviImHcT3mxtCVsdDD97u9tot77aR9P+sSa0fCt0XzdG21hOwaH4Xly1q3t3gQ850OjtgrgCNxDaZW5ONd0WDVb7ZTpNG9LpPcwk7qyIKpQJRZLy9GUmpG4RKPiuu+TpzUvtf4dGhdQ1uxes/Bt2n3PZgWT2HHNjlQqrXYpA1anWcPRrSl9BsmzPEKQqbC8Jwks0bdVgU2SA/mz3JFgtin/fMb++gXsUL7rnuM8yC8t1WdPosvvGvDuAxtLI0rBgAwa3gXQ4s1unNnlHG4CFhEVFv4CEIXdF2iQ95Qe0Qd3rPdOMqD17GtahEIIIeRukoqcRXp8W5OWmnK2+WXz9R8lw2mPym1BYY0/spcFUnsETspo32AGY0mvxQf8aN7dQiEuSApGt/G52R3WEuQNqKXsPlZhcxfcKTAHTzZsz7CyF1ftkE+kQ3RBfSsOPKh6EXGOv2sgNW3Lpd6PL6nlA9lyH7l1y+gszZFuViwgWDE5oJQNUT0pls1fD1x5lT6h4k4aE830eCPFUTRYWctowa2jX+bdB/6KPk0vAutnHMKrHA4++FuWLoUBsqIlD5aZLLdFnN8qTF/i5ST5HLpRrt0opuc8hm/P3ljux/lJOV5qgK0kg9xgTn3AAMKoQ3BpBV0YgaBOR2Kln26VjWhZ+eXj4/M3gqLgz9FyoNZbzPxTcMZOFwN6xYYeeACVW5m5fxxWk5HHTt9BNK1UVglPPitnvIjwxMufjyZSdmIKsxSEQ5frvPm+9FYC/tRhgOhVdbxaYDJGmESyd5QWmMJS3g/mKq+mCPuk8+lkdIahTR6U2lgYGUJ+5eawEDLrTkHmjyoJc5RJn7zi7Vo7sdsmvTXHFxlDRBaLhkNPsOzYVzfe7IH3WpjfCjkxE7gBG147Na6gSxDCdhJoZoFjKal+vSAz/evxlIR8B6Jt5EpuincE9nDfdWjt+0eEz2De0q3bFh/5LS8wwD8J7eptPns2Rfu5L+C8J9V+S63vyME2Gq3h45bxoekCiPzTL7YNCWE+6dw4t7DXQPxlHRFCqnqRX1nk2IxUA1fknr6FkstoGleMzv0eyBf3jdHpu5RmQ=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="5bec7e122d05cf9005e4d254404712ac"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610527609.11-0pfxMnxOIu"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610dd93bee61d96a')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610dd93bee61d96a</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
