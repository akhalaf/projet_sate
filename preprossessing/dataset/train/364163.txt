<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<title>Page not found</title><meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="noindex,follow" name="robots"/>
<style type="text/css">
			.slide-excerpt { width: 35%; }
			.slide-excerpt { bottom: 0; }
			.slide-excerpt { left: 0; }
			.flexslider { max-width: 1140px; max-height: 460px; }
			.slide-image { max-height: 460px; }
		</style>
<style type="text/css">
			@media only screen
			and (min-device-width : 320px)
			and (max-device-width : 480px) {
				.slide-excerpt { display: none !important; }
			}
		</style> <link href="//counselling-manchester.co.uk" rel="dns-prefetch"/>
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://counselling-manchester.co.uk/feed/" rel="alternate" title="Counselling Manchester » Feed" type="application/rss+xml"/>
<link href="https://counselling-manchester.co.uk/comments/feed/" rel="alternate" title="Counselling Manchester » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/counselling-manchester.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=c2fcd566a749f7c324a6d57bed55b3c0"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://counselling-manchester.co.uk/wp-content/themes/outreach-pro/style.css?ver=3.1" id="outreach-pro-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://counselling-manchester.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" id="font-awesome-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://counselling-manchester.co.uk/wp-content/plugins/table-of-contents-plus/screen.min.css?ver=1509" id="toc-screen-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://counselling-manchester.co.uk/wp-includes/css/dashicons.min.css?ver=c2fcd566a749f7c324a6d57bed55b3c0" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato%3A400%2C700&amp;ver=3.1" id="google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://counselling-manchester.co.uk/wp-content/plugins/simple-social-icons/css/style.css?ver=2.0.1" id="simple-social-icons-font-css" media="all" rel="stylesheet" type="text/css"/>
<style id="simple-social-icons-font-inline-css" type="text/css">
#simple-social-icons-2 ul li a,#simple-social-icons-2 ul li a:hover {font-size: 18px;padding: 9px;color: #333333;background-color: #ffffff;border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius:3px;}#simple-social-icons-2 ul li a:hover {color: #ffffff;background-color: #333333;}
</style>
<link href="https://counselling-manchester.co.uk/wp-content/plugins/genesis-responsive-slider/style.css?ver=0.9.5" id="slider_styles-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://counselling-manchester.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://counselling-manchester.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://counselling-manchester.co.uk/wp-content/themes/genesis/lib/js/html5shiv.min.js?ver=3.7.3'></script>
<![endif]-->
<script src="https://counselling-manchester.co.uk/wp-content/themes/outreach-pro/js/responsive-menu.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://counselling-manchester.co.uk/wp-content/plugins/simple-social-icons/svgxuse.js?ver=1.1.21" type="text/javascript"></script>
<link href="https://counselling-manchester.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://counselling-manchester.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<style>
.scroll-back-to-top-wrapper {
    position: fixed;
	opacity: 0;
	visibility: hidden;
	overflow: hidden;
	text-align: center;
	z-index: 99999999;
    background-color: #777777;
	color: #eeeeee;
	width: 50px;
	height: 48px;
	line-height: 48px;
	bottom: 30px;
	left: 30px;
	padding-top: 2px;
	border-top-left-radius: 10px;
	border-top-right-radius: 10px;
	border-bottom-right-radius: 10px;
	border-bottom-left-radius: 10px;
	-webkit-transition: all 0.5s ease-in-out;
	-moz-transition: all 0.5s ease-in-out;
	-ms-transition: all 0.5s ease-in-out;
	-o-transition: all 0.5s ease-in-out;
	transition: all 0.5s ease-in-out;
}
.scroll-back-to-top-wrapper:hover {
	background-color: #888888;
  color: #eeeeee;
}
.scroll-back-to-top-wrapper.show {
    visibility:visible;
    cursor:pointer;
	opacity: 1.0;
}
.scroll-back-to-top-wrapper i.fa {
	line-height: inherit;
}
.scroll-back-to-top-wrapper .fa-lg {
	vertical-align: 0;
}
</style><style type="text/css">div#toc_container {width: 300px;}div#toc_container ul li {font-size: 85%;}</style><script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//counselling-manchester.co.uk/?wordfence_logHuman=1&hid=C447C08AE162946A655F7AD43A6CDB81');
</script><link href="https://counselling-manchester.co.uk/wp-content/themes/outreach-pro/images/favicon.ico" rel="icon"/>
<link href="https://counselling-manchester.co.uk/xmlrpc.php" rel="pingback"/>
<link href="https://counselling-manchester.co.uk/appS651/6197d0203db4bd51df337067349ae3ac/4a1f09bc36c87a99e456c5d7e86450b9/Login/%09/page/2/" rel="next"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #10d4f2; }
</style>
</head>
<body class="error404 custom-background content-sidebar outreach-pro-blue" itemscope="" itemtype="http://schema.org/WebPage"><div class="site-container"><header class="site-header" itemscope="" itemtype="http://schema.org/WPHeader"><div class="wrap"><div class="title-area"><p class="site-title" itemprop="headline"><a href="https://counselling-manchester.co.uk/">Counselling Manchester</a></p><p class="site-description" itemprop="description">My subtitle</p></div><div class="widget-area header-widget-area"><section class="widget simple-social-icons" id="simple-social-icons-2"><div class="widget-wrap"><ul class="alignright"><li class="ssi-facebook"><a href="https://facebook.com"><svg aria-labelledby="social-facebook" class="social-facebook" role="img"><title id="social-facebook">Facebook</title><use xlink:href="https://counselling-manchester.co.uk/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-facebook"></use></svg></a></li><li class="ssi-twitter"><a href="https://twitter.com"><svg aria-labelledby="social-twitter" class="social-twitter" role="img"><title id="social-twitter">Twitter</title><use xlink:href="https://counselling-manchester.co.uk/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-twitter"></use></svg></a></li></ul></div></section>
</div></div></header><nav class="nav-primary" itemscope="" itemtype="http://schema.org/SiteNavigationElement"><div class="wrap"><ul class="menu genesis-nav-menu menu-primary" id="menu-main"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-6" id="menu-item-6"><a href="http://counselling-manchester.co.uk" itemprop="url"><span itemprop="name">Home</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-672" id="menu-item-672"><a href="https://counselling-manchester.co.uk/counselling/" itemprop="url"><span itemprop="name">Counselling</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-675" id="menu-item-675"><a href="https://counselling-manchester.co.uk/supervision/" itemprop="url"><span itemprop="name">Supervision</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-597" id="menu-item-597"><a href="https://counselling-manchester.co.uk/about-me/" itemprop="url"><span itemprop="name">About Me</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-599" id="menu-item-599"><a href="https://counselling-manchester.co.uk/fees/" itemprop="url"><span itemprop="name">Sessions &amp; Fees</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-600" id="menu-item-600"><a href="https://counselling-manchester.co.uk/frequently-asked-questions/" itemprop="url"><span itemprop="name">Frequently Asked Questions</span></a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-605" id="menu-item-605"><a href="https://counselling-manchester.co.uk/more-information/" itemprop="url"><span itemprop="name">More Information</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-598" id="menu-item-598"><a href="https://counselling-manchester.co.uk/contact-2/" itemprop="url"><span itemprop="name">Contact Me</span></a></li>
</ul></div></nav><div class="site-inner"><div class="wrap"><div class="content-sidebar-wrap"><main class="content"><article class="entry"><h1 class="entry-title">Not found, error 404</h1><div class="entry-content"><p>The page you are looking for no longer exists. Perhaps you can return back to the site's <a href="https://counselling-manchester.co.uk/">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.</p><form action="https://counselling-manchester.co.uk/" class="search-form" itemprop="potentialAction" itemscope="" itemtype="http://schema.org/SearchAction" method="get" role="search"><meta content="https://counselling-manchester.co.uk/?s={s}" itemprop="target"/><input itemprop="query-input" name="s" placeholder="Search this website …" type="search"/><input type="submit" value="Search"/></form><h4>Pages:</h4><ul><li class="page_item page-item-587"><a href="https://counselling-manchester.co.uk/about-me/">About Me</a></li>
<li class="page_item page-item-181"><a href="https://counselling-manchester.co.uk/columns/">Column Classes</a></li>
<li class="page_item page-item-583"><a href="https://counselling-manchester.co.uk/contact-2/">Contact Me</a></li>
<li class="page_item page-item-568"><a href="https://counselling-manchester.co.uk/frequently-asked-questions/">Frequently Asked Questions</a></li>
<li class="page_item page-item-520"><a href="https://counselling-manchester.co.uk/landing-page/">Landing Page</a></li>
<li class="page_item page-item-602"><a href="https://counselling-manchester.co.uk/more-information/">More Information</a></li>
<li class="page_item page-item-61 page_item_has_children"><a href="https://counselling-manchester.co.uk/layouts/">Page Layouts</a>
<ul class="children">
<li class="page_item page-item-63"><a href="https://counselling-manchester.co.uk/layouts/cs/">Content/Sidebar</a></li>
<li class="page_item page-item-67"><a href="https://counselling-manchester.co.uk/layouts/css/">Content/Sidebar/Sidebar</a></li>
<li class="page_item page-item-73"><a href="https://counselling-manchester.co.uk/layouts/fwc/">Full Width Content</a></li>
<li class="page_item page-item-65"><a href="https://counselling-manchester.co.uk/layouts/sc/">Sidebar/Content</a></li>
<li class="page_item page-item-71"><a href="https://counselling-manchester.co.uk/layouts/scs/">Sidebar/Content/Sidebar</a></li>
<li class="page_item page-item-69"><a href="https://counselling-manchester.co.uk/layouts/ssc/">Sidebar/Sidebar/Content</a></li>
</ul>
</li>
<li class="page_item page-item-75 page_item_has_children"><a href="https://counselling-manchester.co.uk/templates/">Page Templates</a>
<ul class="children">
<li class="page_item page-item-77"><a href="https://counselling-manchester.co.uk/templates/archive/">Archive Page</a></li>
<li class="page_item page-item-79"><a href="https://counselling-manchester.co.uk/templates/blog/">Blog Page</a></li>
</ul>
</li>
<li class="page_item page-item-2"><a href="https://counselling-manchester.co.uk/sample-page/">Sample Page</a></li>
<li class="page_item page-item-31 page_item_has_children"><a href="https://counselling-manchester.co.uk/sample/">Sample Page</a>
<ul class="children">
<li class="page_item page-item-33 page_item_has_children"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/">Sub Page 1.1</a>
<ul class="children">
<li class="page_item page-item-53 page_item_has_children"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-21/">Sub Page 2.1</a>
<ul class="children">
<li class="page_item page-item-56"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-21/sub-page-31/">Sub Page 3.1</a></li>
<li class="page_item page-item-57"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-21/sub-page-32/">Sub Page 3.2</a></li>
<li class="page_item page-item-58"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-21/sub-page-33/">Sub Page 3.3</a></li>
</ul>
</li>
<li class="page_item page-item-54"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-22/">Sub Page 2.2</a></li>
<li class="page_item page-item-55"><a href="https://counselling-manchester.co.uk/sample/sub-page-11/sub-page-23/">Sub Page 2.3</a></li>
</ul>
</li>
<li class="page_item page-item-35"><a href="https://counselling-manchester.co.uk/sample/sub-page-12/">Sub Page 1.2</a></li>
<li class="page_item page-item-52"><a href="https://counselling-manchester.co.uk/sample/sub-page-13/">Sub Page 1.3</a></li>
</ul>
</li>
<li class="page_item page-item-473"><a href="https://counselling-manchester.co.uk/service-times/">Service Times</a></li>
<li class="page_item page-item-581"><a href="https://counselling-manchester.co.uk/fees/">Sessions &amp; Fees</a></li>
<li class="page_item page-item-479"><a href="https://counselling-manchester.co.uk/upcoming-events/">Upcoming Events</a></li>
<li class="page_item page-item-469"><a href="https://counselling-manchester.co.uk/">Welcome to Outreach</a></li>
<li class="page_item page-item-83"><a href="https://counselling-manchester.co.uk/contact/">Contact Page</a></li>
</ul><h4>Categories:</h4><ul> <li class="cat-item cat-item-3"><a href="https://counselling-manchester.co.uk/category/category-1/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Category #1</a>
<ul class="children">
<li class="cat-item cat-item-10"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 1.1</a>
<ul class="children">
<li class="cat-item cat-item-13"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-21/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 2.1</a>
<ul class="children">
<li class="cat-item cat-item-16"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-21/sub-category-31/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 3.1</a>
</li>
<li class="cat-item cat-item-17"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-21/sub-category-32/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 3.2</a>
</li>
<li class="cat-item cat-item-18"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-21/sub-category-33/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 3.3</a>
</li>
</ul>
</li>
<li class="cat-item cat-item-14"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-22/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 2.2</a>
</li>
<li class="cat-item cat-item-15"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-11/sub-category-23/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 2.3</a>
</li>
</ul>
</li>
<li class="cat-item cat-item-11"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-12/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 1.2</a>
</li>
<li class="cat-item cat-item-12"><a href="https://counselling-manchester.co.uk/category/category-1/sub-category-13/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Sub Category 1.3</a>
</li>
</ul>
</li>
<li class="cat-item cat-item-4"><a href="https://counselling-manchester.co.uk/category/category-2/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Category #2</a>
</li>
<li class="cat-item cat-item-5"><a href="https://counselling-manchester.co.uk/category/category-3/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Category #3</a>
</li>
<li class="cat-item cat-item-6"><a href="https://counselling-manchester.co.uk/category/category-4/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Category #4</a>
</li>
<li class="cat-item cat-item-7"><a href="https://counselling-manchester.co.uk/category/category-5/" title="This is a sample category description, which can be used to boost SEO rankings.  Make sure you have enabled this from the Edit Category screen in your dashboard.">Category #5</a>
</li>
<li class="cat-item cat-item-8"><a href="https://counselling-manchester.co.uk/category/featured/">Featured</a>
</li>
<li class="cat-item cat-item-1"><a href="https://counselling-manchester.co.uk/category/uncategorized/">Uncategorized</a>
</li>
</ul><h4>Authors:</h4><ul><li><a href="https://counselling-manchester.co.uk/author/counsadmin/" title="Posts by counsadmin">counsadmin</a> (13)</li><li><a href="https://counselling-manchester.co.uk/author/ruth/" title="Posts by Ruth">Ruth</a> (4)</li></ul><h4>Monthly:</h4><ul> <li><a href="https://counselling-manchester.co.uk/2015/11/">November 2015</a></li>
<li><a href="https://counselling-manchester.co.uk/2015/09/">September 2015</a></li>
<li><a href="https://counselling-manchester.co.uk/2013/09/">September 2013</a></li>
<li><a href="https://counselling-manchester.co.uk/2013/08/">August 2013</a></li>
</ul><h4>Recent Posts:</h4><ul> <li><a href="https://counselling-manchester.co.uk/counselling/">Counselling Services in Manchester</a></li>
<li><a href="https://counselling-manchester.co.uk/supervision/">Supervision Services in Manchester</a></li>
<li><a href="https://counselling-manchester.co.uk/slider-3/">Slider 3</a></li>
<li><a href="https://counselling-manchester.co.uk/hello-world/">Hello world!</a></li>
<li><a href="https://counselling-manchester.co.uk/threaded-comments/">Sample Post With Threaded Comments</a></li>
<li><a href="https://counselling-manchester.co.uk/image-aligned-left/">Sample Post With Image Aligned Left</a></li>
<li><a href="https://counselling-manchester.co.uk/image-aligned-right/">Sample Post With Image Aligned Right</a></li>
<li><a href="https://counselling-manchester.co.uk/image-centered/">Sample Post With Image Centered</a></li>
<li><a href="https://counselling-manchester.co.uk/unordered-list/">Sample Post With an Unordered List</a></li>
<li><a href="https://counselling-manchester.co.uk/ordered-list/">Sample Post With an Ordered List</a></li>
<li><a href="https://counselling-manchester.co.uk/blockquote/">Sample Post With a Blockquote</a></li>
<li><a href="https://counselling-manchester.co.uk/sample-table/">Sample Post With a Table</a></li>
<li><a href="https://counselling-manchester.co.uk/headlines/">Sample Post With Headlines</a></li>
<li><a href="https://counselling-manchester.co.uk/sample-message-1/">Sample Message #1</a></li>
<li><a href="https://counselling-manchester.co.uk/sample-message-2/">Sample Message #2</a></li>
<li><a href="https://counselling-manchester.co.uk/sample-message-3/">Sample Message #3</a></li>
<li><a href="https://counselling-manchester.co.uk/sample-message-4/">Sample Message #4</a></li>
</ul></div></article></main><aside aria-label="Primary Sidebar" class="sidebar sidebar-primary widget-area" itemscope="" itemtype="http://schema.org/WPSideBar" role="complementary"><section class="widget widget_text" id="text-2"><div class="widget-wrap"><h4 class="widget-title widgettitle">About Me</h4>
<div class="textwidget"><img align="right" alt="Ruth Harrison MBACP Registered and Accredited Counsellor " class="size-thumbnail wp-image-585" height="150" src="http://counselling-manchester.co.uk/wp-content/uploads/2015/09/2015_08230054-e1443560612523-150x150.jpg" width="150"/>My name is Ruth Harrison and I am a qualified and experienced counsellor. <a href="http://counselling-manchester.co.uk/about-me">More about me...</a></div>
</div></section>
<section class="widget widget_text" id="text-3"><div class="widget-wrap"><h4 class="widget-title widgettitle">Quick Contact</h4>
<div class="textwidget"><ul>
<li>Call: 07982 750 069 or0161 215 0690</li>
<li>Email: ruth@counselling-manchester.co.uk</li>
<li><a href="http://counselling-manchester.co.uk/contact-2">All contact details</a></li>
</ul></div>
</div></section>
</aside></div></div></div><footer class="site-footer" itemscope="" itemtype="http://schema.org/WPFooter"><div class="wrap"><p></p></div></footer></div><div class="scroll-back-to-top-wrapper">
<span class="scroll-back-to-top-inner">
<i class="fa fa-3x fa-arrow-up"></i>
</span>
</div><script type="text/javascript">jQuery(document).ready(function($) {$(".flexslider").flexslider({controlsContainer: "#genesis-responsive-slider",animation: "fade",directionNav: 1,controlNav: 1,animationDuration: 1000,slideshowSpeed: 10000    });  });</script><script src="https://counselling-manchester.co.uk/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script src="https://counselling-manchester.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var scrollBackToTop = {"scrollDuration":"0","fadeDuration":"0.5"};
/* ]]> */
</script>
<script src="https://counselling-manchester.co.uk/wp-content/plugins/scroll-back-to-top/assets/js/scroll-back-to-top.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var tocplus = {"visibility_show":"show","visibility_hide":"hide","visibility_hide_by_default":"1","width":"300px"};
/* ]]> */
</script>
<script src="https://counselling-manchester.co.uk/wp-content/plugins/table-of-contents-plus/front.min.js?ver=1509" type="text/javascript"></script>
<script src="https://counselling-manchester.co.uk/wp-content/plugins/genesis-responsive-slider/js/jquery.flexslider.js?ver=0.9.5" type="text/javascript"></script>
<script src="https://counselling-manchester.co.uk/wp-includes/js/wp-embed.min.js?ver=c2fcd566a749f7c324a6d57bed55b3c0" type="text/javascript"></script>
</body></html>
