<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
    //<![CDATA[
    (function(){
      
      window._cf_chl_opt={
        cvId: "1",
        cType: "non-interactive",
        cNounce: "27730",
        cRay: "611a61e6ffefd1fb",
        cHash: "82bfb176aa08251",
        cFPWv: "b",
        cRq: {
          ru: "aHR0cHM6Ly93d3cuYmxvZ2RhbWFyaWFmZXJuYW5kYS5jb20v",
          ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
          rm: "R0VU",
          d: "DcHpuzl5zvCUgRPCNznd3LcPap2KZNRrfn17hBGbfZHCqCc+fW54//ZxUAIdKxwXZskaLom11aCKou5j/B50goFf8WqGwef6gfOL13vppxUAJlydeRU0g7uu963XXBQKkWivF0Gg4Ty1eLOqcXUu3oaxY5L/I1Q1CcJ35feR5fJ0SWwAorlsfKi04e7XrNB8x6jQ+9gJwapBNDBzEifffP1Q/qRkKHba6qPLOWWHOTTm+xaN3W9SUkpCKTgqGAy/imT0m51d8kaUne08QDDVfGbsNkCL2B5ZO9RxPJVLQjHZBHjLK4KPY2iTVGEIrreXT2kKPYRokz9B6976OqZDs6lLzQ4TVvviZLtUt7os9qik0WEagg48Xrfu3zlJSTdi4MVHR+vQgd1n/oRnJ9Uhmr++aC//YG9dtJUCYyQz2ENGq3SpZjtVO2dLb7xDvVFbHm5sO71FeEwfw9WblzVN6bGY4uJNicJnFOE6m0KZwZrfTqykatisKm0CyGacCraNxkOe5q1wd7x5jIB1kWDQZ5p7/Zsek3OpxRSyjEk5ZU99iH95+DWXB++uX1D9UaAeUrJdBOk43Q/kuFINzr6alWMFhRVIwLnAvENDcPXxUNy7/G5brmaRKwcSoM/3qAB0lbrs1jpug4uZFCq5VY9JgmdhSu16WsMiGzQfOGMjY6OvkDoQRzqeAQuBL0j83cJf8Y39GgvcFc0gen46mrmrgKmkGF2DDyy70ejrCaAWJVF85yNGfSFF73Z/BB14QMP3ty+u7uotME+LRq6z5U3XNM/OKtoyvtCSXyN1f++M42M=",
          t: "MTYxMDY1OTAzMi4xNzAwMDA=",
          m: "hZhTHpN4x6g1nW/wNZ8M/M9qk7t8L4yB8JrTZER4+aU=",
          i1: "y1CilLM11gM5ZtMR5RTSFw==",
          i2: "KiwPqj/vW7Z+rZzUCnlDlQ==",
          uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
          hh: "zOiWbwGr0Xxz0dNuhbYNpFw9R1cq3H45MTGy77TU80A=",
        }
      }
      window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
      
      var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
        var a = document.getElementById('cf-content');a.style.display = 'block';
        var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
        var trkjs = isIE ? new Image() : document.createElement('img');
        trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=611a61e6ffefd1fb");
        trkjs.id = "trk_jschal_js";
        trkjs.setAttribute("alt", "");
        document.body.appendChild(trkjs);
        
        var cpo = document.createElement('script');
        cpo.type = 'text/javascript';
        cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
        var done = false;
        cpo.onload = cpo.onreadystatechange = function() {
          if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
            done = true;
            cpo.onload = cpo.onreadystatechange = null;
            window._cf_chl_enter()
          }
        };
        document.getElementsByTagName('head')[0].appendChild(cpo);
      
      }, false);
    })();
    //]]>
  </script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> www.blogdamariafernanda.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=b7d9e21674a5c90228faa73b43e88cf6a0d21377-1610659032-0-Aftby56o_JPhfBcDVWfizzXgkkZXpJaAaMdEBqyYneDYqk7XnbkjxahVFfIDajrmjUT1MOYFWmLsLiNPwqFNAKpFs_TaQ3kkFt6Kd2OH11fzN4nGMe_L19xYdWPiEEPJ6DzRJbxdmnltfxanDF_qhHcDfxbPjBxjXRjdsn_DYhorMsv84lfIR9AAuVbpYhK8rIh17eQ5oIPqv-df14cCY6ZoceHboZ9AIElM1who0KmeEzRFFcmufz5brs_m1CTnrxciX9n_nKIEFyOmLiq4Rd3I45prAvolYUzhBpYQY2ie0YrWwvPYM0NbMsYE7AHFSurlasypH9HvRSi2tHcaS3RhPFSo2562PEXuas8gl5xLP3LjE1agpO7ArqJ-ffk25A" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="4b17bbbb18341e16695830072fb1c12b1e2cde7e-1610659032-0-AUr08d6mmBQTjqx25O73dN4B0gYIO6jWbcVZMz2910A2ruEjgVQsz4kdEpjtih3JMc2ESkM2Q9KsQwjPlsxsxs0jIhHa5nJZU/DsWdGE0P8FxkuVWKsVvd5ROGW/lhU/AU4FG4HvKeyEBbUi+umWYQYfMAhAqjaFZp2amDhK2oKXNoaBI86CzqLV4MlTn/4x17c7efnZ8Avr3lQ7nDPbKsZAe4aJbnA6QgokPS5xZ95BNhUsKBlMdm8rQuSVlLXZgPOw08Uyaq88J3wAS0tWpT7J0eRASw4E54ZHLHNbxVKTdq2gosqyo00C1T7CpNGghUs13Ho40SJJOYX5YwiinGP/l3J3hyTQIXyV3vKK1d95PV8tn9OyGyZXQuQ1cHizvMdotSM+p7/a1wEgYKyqxq4qCrwUfs7So0+EZqAzqLU80qTSI9dz+mQX0lOBScTWM2kF39tqmScUJctWr8p0Oatn4K6I8TiNh8lAYzhTXypC3RJhTL0GURu1wwTfOg7Azk0vEdCk89gGNLJPzLXH5PIEV2uLKSNbr6hMD7PvZg6rnSI3J656PcBk0GKUR9/TyRTtaTYL/Se6jfG3TsCsSj0xkX+cfLkPNlLzYxOHkDP6MlMGJsRRZkur3Y9TV+SE1nHE1jrfky3RkUH6oSOAEBn5IzrRM5arI245IiTfudtS2ajiQ2jTSKL9wt7/DxYfpgyXw4EjVpKE4tkWqa8iEFmTvaFlXOrqC2SMptXfwPcV19+QxRrga0fftSg5h+xokEEi56ZEQHX/4HQZPUWz5svOs8OFai3NRp/1tOCdpgGoezA9ZQmdaDduNFv+56Vr2I4yi4gB9LrwIxlvglpYTT7Vw0poVIjNRyPwezNQXnP0UilnNCzC1r5iNl9oRsjCGdWJsYvNKqnjkg6mEIu/2tLKt5rodwQxsqIEwzkqNOPJX/WsQDgXgSenVw8h4ErPTdJnbAg53gUJyRJCHWHYCaxWG/IeabisyIN+vd/Y3yNbF215u9AbuYgHfHUnjFOoINPUFkKl8JnesDau2ujDcJyOeHb4iUBhezwcA6WzdEkqhV8fgq2bywCjvr9L4psqGgZdwcClFV03Nbe83mI8epXZ6cjEwSPfPXGZD8KJ2J8LtNP8b4aMW8qaSFc54GjHmY7Eq+2/O49qopMw0vITVGx25X2RIx6YG1GnPe9LXeaHmtMT5RDeLNRO6wOTkfB8M2BUcqhsUoJBQrHQlcjB5nv/ql721Vn0XvnJ2vR9LNA1sUb+Mcvljyg7Yzbxf4RAFxzRPIa8Zl4PdMheJZQETgR4hBsz9z75w0qEq/2CyZ5Ze5rE2upE4VXlsC07deU1pb+vGkcywPj3y4VFxDGiCog="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="21281fba12c1c96f02ca69604fb07c55"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610659036.17-OJ3oREDaWT"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=611a61e6ffefd1fb')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>611a61e6ffefd1fb</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
