<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta content="iz8FZ4W3SsNzfZsWOcKDAJ8xGE6yssUqQDWGrHcNr1k" name="google-site-verification"/>
<title>PT BUMI PERTIWI PERSADA</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- Custom Fonts -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css"/>
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- Plugin CSS -->
<link href="css/animate.min.css" rel="stylesheet" type="text/css"/>
<!-- Custom CSS -->
<link href="css/creative.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top">
<nav class="navbar navbar-default navbar-fixed-top" id="mainNav">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand page-scroll" href="#page-top"><img src="img/bpp.jpg"/></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;"> Tentang Kami<b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a class="page-scroll" href="#visi">Visi &amp; Misi</a></li>
<li><a class="page-scroll" href="legalitas.php#legal">Legalitas</a></li>
<li><a class="page-scroll" href="core-feature.php#core">Core Feature</a></li>
<li><a class="page-scroll" href="key-person.php#key">Key Person</a></li>
</ul>
</li>
<li>
<a class="page-scroll" href="#sukses_measurment">Ukuran Sukses</a>
</li>
<li>
<a class="page-scroll" href="area-coverage.php#area">Cakupan Wilayah</a>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">Truk Kami<b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a class="page-scroll" href="#our_truck">Gambar Truk</a></li>
<li><a class="page-scroll" href="jenis-truck.php#jenis">Jenis Truk</a></li>
<li><a class="page-scroll" href="detail-truck.php#detail">Detail Truk</a></li>
</ul>
</li>
<li>
<a class="page-scroll" href="#contact">Kontak</a>
</li>
</ul>
</div>
<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>
<style type="text/css">
	.header-content a {
		font-size: 50px;
	}
	.header-content {
		margin-top: 150px;
	}
	.header-content h3 {
		margin-bottom: 300px;
		letter-spacing: 0.125em;
	}
	.animated {
		animation-duration: 1.5s;
	}
</style>
<header>
<div class="header-content">
<div class="header-content-inner">
<h1>PT BUMI PERTIWI PERSADA</h1>
<hr/>
<h3><i>Trucking / Transporter  Division</i></h3>
<div class="s animated infinite slideInDown">
<a class="page-scroll" href="#visi"><i class="fa fa-angle-down"></i></a>
</div>
</div>
</div>
</header>
<section class="bg-primary" id="visi">
<div class="container">
<div class="row">
<div class="col-lg-8 col-lg-offset-2 text-center">
<strong><h1><h2 class="section-heading">Visi &amp; Misi</h2></h1></strong>
<hr class="light"/>
<p> Visi : Menjadi perusahaan Logistic / Transportasi pilihan karena terpercaya &amp; inovatif serta selalu memberikan nilai tambah untuk para pelanggan, mitra kerja, pemegang saham dan karyawan.</p><div><div><br/></div><div>Misi : - Menciptakan kepuasan pelanggan dengan membentuk standarisasi truk &amp; infrastruktur lainnya serta kompetensi karyawan</div><div>- Menjaga key performancee index (KPI) / Success measurement sebagai standard perusahaan </div><div>- Melakukan efisiensi biaya sesuai anggaran dan efektifitas kerja sesuai dengan Standard Operational Procedure</div><div><font size="2"><span style="text-align: justify; text-indent: -0.38in; line-height: 1.7em;"><br/></span></font></div><div><br/></div></div>
</div>
</div>
</div>
</section>
<style type="text/css">
		#sukses_measurment {
			background: #f2f2f3;
		}
		.service-box .icon {
			background: #fff;
			width: 100px;
			height: 100px;
			border-radius: 99em;
			margin: 0px auto;
			padding-top: 20px;
		}
		.service-box {
			padding: 20px 10px;
		}
		.service-box h3 {
			margin: 40px 0;
		}
	</style>
<section id="sukses_measurment">
<div class="container">
<div class="row">
<div class="col-lg-12 text-center">
<h2 class="section-heading">Ukuran Sukses</h2>
<hr class="primary"/>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-6 text-center">
<div class="service-box">
<div class="icon">
<i class="fa fa-4x fa-truck wow bounceIn text-primary" data-wow-delay=".1s"></i>
</div>
<h3>Waktu Tempuh</h3>
<h1></h1><p class="text-muted">lamanya waktu perjalanan (hari) yang disepakati oleh client dan PT BPP untuk servis door to door  dalam pengangkutan produk dari titik pengambilan barang sampai ke titik pembongkaran akhir</p>
</div>
</div>
<div class="col-lg-3 col-md-6 text-center">
<div class="service-box">
<div class="icon">
<i class="fa fa-4x fa-lock wow bounceIn text-primary" data-wow-delay=".1s"></i>
</div>
<h3>Keamanan</h3>
<h1></h1><p class="text-muted">Jaminan keamanan produk selama perjalanan sesuai agreement. Kecukupan dan kebersihan produk yang diangkut</p>
</div>
</div>
<div class="col-lg-3 col-md-6 text-center">
<div class="service-box">
<div class="icon">
<i class="fa fa-4x fa-group wow bounceIn text-primary" data-wow-delay=".2s"></i>
</div>
<h3>Partnership Jangka Panjang</h3>
<h1></h1><p class="text-muted">Evaluasi secara berkala dengan agreed template KPI antara  management PT BPP dengan client   untuk business development.</p>
</div>
</div>
<div class="col-lg-3 col-md-6 text-center">
<div class="service-box">
<div class="icon">
<i class="fa fa-4x fa-money wow bounceIn text-primary" data-wow-delay=".3s"></i>
</div>
<h3>Harga</h3>
<h1></h1><p class="text-muted">Harga yang kompetitif dan inovasi yang diperlukan untuk  efisiensi  biaya dalam jangka pendek dan jangka panjang</p>
</div>
</div>
</div>
</div>
</section>
<section class="no-padding" id="our_truck">
<div class="container-fluid">
<div class="row no-gutter">
<div class="col-lg-4 col-sm-6">
<a class="portfolio-box" href="#popup">
<img alt="" class="img-responsive" src="img/portfolio/02.jpg"/>
<div class="portfolio-box-caption">
<div class="portfolio-box-caption-content">
<div class="project-category text-faded">
                                    PT BPP
                                </div>
<div class="project-name">
                                   Lihat Gambar
                                </div>
</div>
</div>
</a>
</div>
<!-- -->
<div id="popup">
<div class="window">
<a class="close-button" href="#our_truck" title="Close">X</a>
<h1>Sebagian Truk Kami</h1>
<p><img src="img/bpptruk.jpg"/></p>
</div>
</div>
<!-- -->
<div class="col-lg-4 col-sm-6">
<a class="portfolio-box" href="jenis-truck.php#jenis">
<img alt="" class="img-responsive" src="img/portfolio/04.jpg"/>
<div class="portfolio-box-caption">
<div class="portfolio-box-caption-content">
<div class="project-category text-faded">
                                    PT BPP
                                </div>
<div class="project-name">
                                    Jenis Truk
                                </div>
</div>
</div>
</a>
</div>
<div class="col-lg-4 col-sm-6">
<a class="portfolio-box" href="detail-truck.php">
<img alt="" class="img-responsive" src="img/portfolio/06.jpeg"/>
<div class="portfolio-box-caption">
<div class="portfolio-box-caption-content">
<div class="project-category text-faded">
                                    PT BPP
                                </div>
<div class="project-name">
                                    Detail Truk
                                </div>
</div>
</div>
</a>
</div>
<!--
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/4.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/5.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/6.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>-->
</div>
</div>
</section>
<style type="text/css">
		.a img {
			padding: 10px;
			margin-right: 50px;
		}
		#contact {
			background: #f2f2f3;
		}
	</style>
<aside class="a">
<div class="container text-center">
<div class="call-to-action">
<strong><h2>Clients</h2></strong>
<hr class="primary"/>
<marquee>
<img src="img/nestle.jpg"/>
<img src="img/traktor.gif"/>
<img src="img/argha.jpg"/>
<img src="img/hengan.jpg"/>
<img src="img/nabati.jpg"/>
<img src="img/godrej.jpg"/>
</marquee>
<strong><br/><h4><b>Layanan Kami</b></h4></strong>
<h1></h1><p>
				- Layanan Ritase door to door dari dan ke  tempat tujuan.<br/>
				- Layanan PP door to door pulang pergi dengan  harga yang lebih cost effective.<br/>
				- Layanan Sewa Truk Mendedikasikan 1 armada untuk  dipakai sesuai kebutuhan Anda. <br/>
				- Multidrop Ritase dengan lebih dari 1 kota tujuan
 				</p>
</div>
</div>
</aside>
<section id="contact">
<div class="container">
<div class="row">
<div class="col-lg-8 col-lg-offset-2 text-center">
<strong><h1><h2 class="section-heading">Kontak</h2></h1></strong>
<hr class="primary"/>
<h1><strong><p>Kami menawarkan kerja sama pengangkutan produk dalam jangka panjang dengan win-win spirit... Hubungi kami atau kirim email dan kami akan kembali kepada Anda sesegera mungkin !</p></strong></h1>
</div>
<div class="col-lg-4 col-lg-offset-2 text-center">
<i class="fa fa-phone fa-3x wow bounceIn"></i>
<strong><p>0812 1963 465/ 0812 1994 6292</p></strong>
</div>
<div class="col-lg-4 text-center">
<i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
<strong><p><a href="mailto:your-email@your-domain.com">bpp271067@gmail.com</a></p></strong>
</div>
</div>
</div>
</section>
<aside class="bg-dark">
<div class="container text-left">
<div class="call-to-action">
<h5>Copyright © PT BUMI PERTIWI PERSADA 2019</h5>
<p></p>
</div>
</div>
</aside>
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript -->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.fittext.js"></script>
<script src="js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/creative.js"></script>
</body>
</html>
