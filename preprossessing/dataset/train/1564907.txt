<!DOCTYPE html>
<!--[if IE 8]>    <html class="ie8" lang="es"> <![endif]--><!--[if IE 9]>    <html class="ie9" lang="es"> <![endif]--><!--[if gt IE 8]><!--><html lang="es"> <!--<![endif]-->
<head>
<title>No se encontró la página | El Periódico Digital | Noticias de Última Hora</title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://wazzpromo.com/xmlrpc.php" rel="pingback"/>
<meta content="noindex,follow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://wazzpromo.com/feed/" rel="alternate" title="El Periódico Digital | Noticias de Última Hora » Feed" type="application/rss+xml"/>
<link href="https://wazzpromo.com/comments/feed/" rel="alternate" title="El Periódico Digital | Noticias de Última Hora » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/wazzpromo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%7CRoboto+Condensed%3A300%2C300italic%2C400%2C400italic%2C700%2C700italic&amp;ver=4.5" id="google-fonts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://wazzpromo.com/wp-content/themes/Newsmag/style.css?ver=4.5" id="td-theme-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://wazzpromo.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://wazzpromo.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://wazzpromo.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://wazzpromo.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://wazzpromo.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
</head></html>