<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="default" name="apple-mobile-web-app-status-bar-style"/>
<link href="images/favicon.ico" rel="shortcut icon"/>
<link href="images/touch-icon-iphone.png" rel="apple-touch-icon"/>
<link href="images/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="images/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="images/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152"/>
<meta content="https://bondagefilesbest.com/templates/metal-curve-3/images/logo.png" property="og:image"/>
<link href="https://bondagefilesbest.com/templates/metal-curve-3/css/offline.css" rel="stylesheet" type="text/css"/>
<title>Сайт временно отключен</title>
</head>
<body>
<div class="offpage">
<div class="wrap">
<div class="logo"><img alt="" src="https://bondagefilesbest.com/templates/metal-curve-3/images/offline.png"/></div>
<div class="title">Сайт временно отключен</div>
<div class="reason">Сайт находится на текущей реконструкции, после завершения всех работ сайт будет открыт.<br/><br/>Приносим вам свои извинения за доставленные неудобства.</div>
<p class="copyright">Copyright © 2004–2016 <a href="http://dle-news.ru/" rel="nofollow" target="_blank">SoftNews Media Group</a> All Rights Reserved. Powered by DataLife Engine © 2016</p>
</div>
</div>
</body>
</html>