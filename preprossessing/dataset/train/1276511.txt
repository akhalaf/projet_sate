<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>
		Scientific Publishing Institute
					</title>
<meta content="Scientific Publishing Institute 3.1.0.0" name="generator"/>
<link href="https://www.scipg.com/index.php/index/$$$call$$$/page/page/css?name=stylesheet" rel="stylesheet" type="text/css"/><link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/><link href="//fonts.googleapis.com/css?family=Montserrat:400,700|Noto+Serif:400,400i,700,700i" rel="stylesheet" type="text/css"/><link href="https://www.scipg.com/plugins/generic/orcidProfile/css/orcidProfile.css" rel="stylesheet" type="text/css"/>
</head><body class="pkp_page_index pkp_op_index has_site_logo" dir="ltr">
<div class="cmp_skip_to_content">
<a href="#pkp_content_main">Skip to main content</a>
<a href="#pkp_content_nav">Skip to main navigation menu</a>
<a href="#pkp_content_footer">Skip to site footer</a>
</div>
<div class="pkp_structure_page">
<header class="pkp_structure_head" id="headerNavigationContainer" role="banner">
<div class="pkp_head_wrapper">
<div class="pkp_site_name_wrapper">
<h1 class="pkp_site_name">
<a class="is_img" href="https://www.scipg.com/index.php/index/index">
<img alt="Page Header Logo" height="130" src="https://www.scipg.com/public/site/pageHeaderTitleImage_en_US.png" width="1160"/>
</a>
</h1>
</div>
<nav aria-label="User Navigation" class="pkp_navigation_user_wrapper" id="navigationUserWrapper">
<ul class="pkp_navigation_user pkp_nav_list" id="navigationUser">
<li class="profile">
<a href="https://www.scipg.com/index.php/index/user/register">
					Register
				</a>
</li>
<li class="profile">
<a href="https://www.scipg.com/index.php/index/login">
					Login
				</a>
</li>
</ul>
</nav>
</div><!-- .pkp_head_wrapper -->
</header><!-- .pkp_structure_head -->
<div class="pkp_structure_content has_sidebar">
<div class="pkp_structure_main" id="pkp_content_main" role="main">
<div class="page_index_site">
<div class="about_site">
			Scientific Publishing Institute (SPI) is a platform for peer-reviewed and scientific open-access journals. The Journals published by SPI are fully open access.<br/>
<br/>
Paper Selection and Publication Process<br/>
<br/>
1. Upon receipt of paper submission, the Editor sends an E-mail of confirmation to the corresponding author within 1-3 working days. If you fail to receive this confirmation, your submission/e-mail may be missed. Please contact the Editor in time for that.<br/>
2. Peer review. We use double-blind system for peer-review; both reviewers and authors' identities remain anonymous. The paper will be peer-reviewed by three experts; two reviewers from outside and one editor from the journal typically involve in reviewing a submission. <br/>
3. Notification of the result of review by E-mail.<br/>
4. The publication charge is 100 USD (online publication only) or 200 USD (online publication + 1 hard copy)<br/>
5. E-journal in PDF is available on the journal’s webpage, free of charge for download.
		</div>
<div class="journals">
<h2>
			Journals
		</h2>
<ul>
<li>
<div class="body">
<h3>
<a href="https://www.scipg.com/index.php/101" rel="bookmark">
									International Journal of Educational Technology and Learning
								</a>
</h3>
<div class="description">
<p>The International Journal of Educational Technology and Learning (IJETL) is a peer-reviewed academic journal published by Scientific Publishing Institute.</p>
</div>
<ul class="links">
<li class="view">
<a href="https://www.scipg.com/index.php/101">
										View Journal
									</a>
</li>
<li class="current">
<a href="https://www.scipg.com/index.php/101/issue/current">
										Current Issue
									</a>
</li>
</ul>
</div>
</li>
<li>
<div class="body">
<h3>
<a href="https://www.scipg.com/index.php/102" rel="bookmark">
									Journal of Accounting, Business and Finance Research
								</a>
</h3>
<div class="description">
<p>Journal of Accounting, Business and Finance Research (JABFR) is a double-blind peer-reviewed journal, published by Scientific Publishing Institute.</p>
</div>
<ul class="links">
<li class="view">
<a href="https://www.scipg.com/index.php/102">
										View Journal
									</a>
</li>
<li class="current">
<a href="https://www.scipg.com/index.php/102/issue/current">
										Current Issue
									</a>
</li>
</ul>
</div>
</li>
<li>
<div class="body">
<h3>
<a href="https://www.scipg.com/index.php/103" rel="bookmark">
									International Journal of Emerging Trends in Social Sciences
								</a>
</h3>
<div class="description">
<p>International Journal of Emerging Trends in Social Sciences (IJETSS) is a double-blind peer-reviewed journal published by Scientific Publishing Institute.</p>
</div>
<ul class="links">
<li class="view">
<a href="https://www.scipg.com/index.php/103">
										View Journal
									</a>
</li>
<li class="current">
<a href="https://www.scipg.com/index.php/103/issue/current">
										Current Issue
									</a>
</li>
</ul>
</div>
</li>
</ul>
</div>
</div><!-- .page -->
</div><!-- pkp_structure_main -->
</div><!-- pkp_structure_content -->
<div class="pkp_structure_footer_wrapper" id="pkp_content_footer" role="contentinfo">
<div class="pkp_structure_footer">
<div class="pkp_footer_content">
<p>2967 Dundas St. W., Toronto, ON M6P 1Z2,<strong> Canada   Phone:  </strong>+16473139210<br/>Executive Suite Z - 5 ,Building Z ,Business Bay, DXB, <strong>United Arab Emirates</strong><br/><strong>E-mail:</strong> info@scipg.com; editor@scipg.com</p>
</div>
<!-- <div class="pkp_brand_footer" role="complementary" aria-label="About this Publishing System">
			<a href="https://www.scipg.com/index.php/index/about/aboutThisPublishingSystem">
				<img alt="Scientific Publishing Institute" src="https://www.scipg.com/templates/images/ojs_brand.png">
			</a>
			<a href="">
				<img alt="Public Knowledge Project" src="https://www.scipg.com/lib/pkp/templates/images/pkp_brand.png">
			</a>
		</div> -->
</div>
<div style="z-index: 999; position: fixed; bottom: 10px; right: 10px;"><a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=scipg.com','SiteLock','width=600,height=600,left=160,top=170');" style="float: left;margin-left: 428px;margin-top: -109px;"><img alt="SiteLock" class="img-responsive" src="//shield.sitelock.com/shield/scipg.com" title="SiteLock"/></a></div>
</div><!-- pkp_structure_footer_wrapper -->
</div><!-- pkp_structure_page -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script><script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js" type="text/javascript"></script><script src="https://www.scipg.com/lib/pkp/js/lib/jquery/plugins/jquery.tag-it.js" type="text/javascript"></script><script src="https://www.scipg.com/plugins/themes/default/js/lib/popper/popper.js" type="text/javascript"></script><script src="https://www.scipg.com/plugins/themes/default/js/lib/bootstrap/util.js" type="text/javascript"></script><script src="https://www.scipg.com/plugins/themes/default/js/lib/bootstrap/dropdown.js" type="text/javascript"></script><script src="https://www.scipg.com/plugins/themes/default/js/main.js" type="text/javascript"></script>
<script async="" charset="utf-8" src="https://badge.dimensions.ai/badge.js"></script>
</body>
</html>