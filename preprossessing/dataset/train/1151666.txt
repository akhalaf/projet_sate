<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>An Error Occurred: Not Found</title>
<style>
		<!--
		body {
			margin: 0 0 0 0;
			margin-top: 50px;
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 12px;
			color: #666;
			background: #f9f9f9;
		}

		ul {
			list-style-type: none;
			margin-left: -15px;
			margin-top: 25px;
		}

		li {
			margin-bottom: 5px;
		}

		a {
			color: #d74963;
			font-weight: bold;
			text-decoration: none;
			text-transform: uppercase;
		}

		a:hover {
			color: #d74963;
			text-decoration: underline;
		}

		.bg-error-code {
			color: #fff;
			font-size: 2500%;
			font-weight: bold;
			position: absolute;
			left: 5%;
			top: 1%;
		}

		-->
	</style>
</head>
<body>
<div class="bg-error-code">404</div>
<div style="width:720px; height:460px; margin:0 auto; background-repeat:no-repeat; position:relative; text-align: center">
<div>
<img src="/bundles/app/images/logo-planner.png?101" style="margin: 0 auto; display: block;"/>
<h1>Oops!</h1>
<h2>The server returned a "404: Not Found".</h2>
<p>Looks like there's a trouble with this page!</p>
<p> </p>
<ul>
<li><a href="/">Go to the HOMEPAGE</a></li>
<li><a href="javascript: history.back();">Go back where you came from</a></li>
</ul>
</div>
</div>
</body>
</html>
