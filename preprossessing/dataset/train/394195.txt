<!DOCTYPE html>
<html class="no-js" lang="fr-FR">
<head>
<meta charset="utf-8"/>
<script type="text/javascript">var ua = navigator.userAgent; var meta = document.createElement('meta');if((ua.toLowerCase().indexOf('android') > -1 && ua.toLowerCase().indexOf('mobile')) || ((ua.match(/iPhone/i)) || (ua.match(/iPad/i)))){ meta.name = 'viewport';	meta.content = 'target-densitydpi=device-dpi, width=device-width'; }var m = document.getElementsByTagName('meta')[0]; m.parentNode.insertBefore(meta,m);</script> <meta content="AitThemes.club, http://www.ait-themes.club" name="Author"/>
<title>David Leclerc Chiropraticien</title>
<!--[if lte IE 8]>
	<script src="/design/js/libs/modernizr-2.6.1-custom.js"></script>
	<![endif]-->
<!--[if lt IE 9]>
	<script src="//ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
<link href="/design/revslider/rs-plugin/css/settings.css?rev=4.6.0&amp;ver=4.5.3" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/colorbox.css?ver=4.5.3" id="CSS_comments_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/contact.css?ver=4.5.3" id="CSS_contact_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/hoverZoom.css?ver=4.5.3" id="CSS_hoverzoom_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/prettySociable.css?ver=4.5.3" id="CSS_sociable_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/jquery-ui-1.9.2.custom.css?ver=4.5.3" id="CSSjqui_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/css/fancybox/jquery.fancybox-1.3.4.css?ver=4.5.3" id="CSS_fancybox_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="/design/wp-flexible-map/css/styles.css?ver=1.12.0" id="flxmap-css" media="all" rel="stylesheet" type="text/css"/>
<script src="/design/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="/design/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="/design/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?rev=4.6.0&amp;ver=4.5.3" type="text/javascript"></script>
<script src="/design/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.6.0&amp;ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/script.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/gridgallery.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/testimonials.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/libs/jquery-plugins.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/libs/modernizr-2.6.1-custom.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/libs/jquery.infieldlabel.js?ver=4.5.3" type="text/javascript"></script>
<script src="/design/js/libs/jquery.ImageSwitch.yui.js?ver=4.5.3" type="text/javascript"></script>
<link href="/style.css?1471299432" id="ait-style" media="all" rel="stylesheet" type="text/css"/>
</head>
<body class="page page-id-2 page-template page-template-page-fullwidth page-template-page-fullwidth-php" data-themeurl="">
<header class="clearfix">
<div class="header-content defaultContentWidth clearfix">
<div class="logo left clearfix">
<a class="trademark" href="http://davidleclercchiropraticien.com/">
<img alt="logo" src="/images/logoDavidLeclerc.png"/>
</a>
<div class="table">
<div class="tagLineHolder">
<p class="left textshadow info">3290 de Normandie, Trois-Rivières<br/>Téléphone : (819) 373-2241</p>
</div>
</div>
</div>
<div class="right cliearfix">
</div>
</div>
<div class="menu-container">
<div class="menu-content defaultContentWidth">
<div id="mainmenu-dropdown-duration" style="display: none;">200</div>
<div id="mainmenu-dropdown-easing" style="display: none;">swing</div>
<div class="menu-wrap">
<span class="menubut bigbut">Main Menu</span>
<nav class="mainmenu"><ul class="menu" id="menu-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 current_page_item " id="menu-item-13"><a href="/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14 " id="menu-item-14"><a href="/services/">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34 " id="menu-item-34"><a href="/approche-attentive/">Approche</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45 " id="menu-item-45"><a href="/ce-qui-peut-etre-traite/">Ce qui peut être traité</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76 " id="menu-item-76"><a href="/qui-je-suis/">Qui je suis?</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49 " id="menu-item-49"><a href="/urgences/">Urgences</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46 " id="menu-item-46"><a href="/contactez-nous/">Contactez-Nous</a></li>
</ul></nav>
</div>
</div>
</div>
<div class="slider-container slider-disabled">
<div class="wrapper">
<!-- START REVOLUTION SLIDER 4.6.0 fixed mode -->
<div class="slider-content">
<div class="rev_slider_wrapper" id="rev_slider_1_1_wrapper" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;height:400px;width:1000px;">
<div class="rev_slider" id="rev_slider_1_1" style="display:none;height:400px;width:1000px;">
<ul> <!-- SLIDE  -->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="fade">
<!-- MAIN IMAGE -->
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="/images/image01.png"/>
<!-- LAYERS -->
<!-- LAYER NR. 1 -->
<div class="tp-caption tp-fade" data-easing="easeOutExpo" data-elementdelay="0" data-end="6700" data-endelementdelay="0" data-endspeed="300" data-speed="500" data-start="700" data-x="60" data-y="39" style="z-index: 2;"><img alt="" src="/images/image02.png"/>
</div>
</li>
<!-- SLIDE  -->
<li data-masterspeed="300" data-saveperformance="off" data-slotamount="7" data-transition="fade">
<!-- MAIN IMAGE -->
<img alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" src="/images/image01.png"/>
<!-- LAYERS -->
<!-- LAYER NR. 1 -->
<div class="tp-caption tp-fade" data-easing="easeOutExpo" data-elementdelay="0" data-end="6700" data-endelementdelay="0" data-endspeed="300" data-speed="500" data-start="700" data-x="60" data-y="39" style="z-index: 2;"><img alt="" src="/images/image02.png"/>
</div>
</li>
</ul>
</div>
<script type="text/javascript">

				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
				

				var setREVStartSize = function() {
					var	tpopt = new Object();
						tpopt.startwidth = 1000;
						tpopt.startheight = 400;
						tpopt.container = jQuery('#rev_slider_1_1');
						tpopt.fullScreen = "off";
						tpopt.forceFullWidth="off";

					tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
				};

				/* CALL PLACEHOLDER */
				setREVStartSize();


				var tpj=jQuery;
				tpj.noConflict();
				var revapi1;

				tpj(document).ready(function() {

				if(tpj('#rev_slider_1_1').revolution == undefined)
					revslider_showDoubleJqueryError('#rev_slider_1_1');
				else
				   revapi1 = tpj('#rev_slider_1_1').show().revolution(
					{
						dottedOverlay:"none",
						delay:7000,
						startwidth:1000,
						startheight:400,
						hideThumbs:200,

						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:1,
						
												
						simplifyAll:"off",

						navigationType:"none",
						navigationArrows:"nexttobullets",
						navigationStyle:"round",

						touchenabled:"on",
						onHoverStop:"on",
						nextSlideOnWindowFocus:"off",

						swipe_threshold: 0.7,
						swipe_min_touches: 1,
						drag_block_vertical: false,
						
												
												
						keyboardNavigation:"off",

						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						shadow:0,
						fullWidth:"off",
						fullScreen:"off",

						spinner:"spinner0",
						
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",

						
						
						
						
						
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,

												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0					});



					
				});	/*ready*/

			</script>
</div><!-- END REVOLUTION SLIDER -->
</div>
<div class="office-info">
<div class="office-hours">
<span class="office-info-icon"></span>
<span class="office-info-title">Heures d'ouvertures</span>
<div class="timetable">
<div class="timetable-entry">
<div class="timetable-day">Lun</div>
<div class="timetable-time">9:00-12:00 13:30-17:00</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Mar</div>
<div class="timetable-time">18:30-20:00</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Mer</div>
<div class="timetable-time">9:00-12:00 13:30-17:00 18:30-20:00</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Jeu</div>
<div class="timetable-time">Urgences sur appel</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Ven</div>
<div class="timetable-time">9:00-12:00 13:30-17:00 18:30-20:00</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Sam</div>
<div class="timetable-time">Urgences sur appel</div>
</div>
<div class="timetable-entry">
<div class="timetable-day">Dim</div>
<div class="timetable-time">Urgences sur appel</div>
</div>
</div>
</div>
<div class="office-telephone clearfix">
<span class="office-info-icon"></span>
<span class="office-info-title">Téléphone</span>
<span class="office-info-data">(819) 373-2241</span>
</div>
<div class="office-email clearfix">
<span class="office-info-icon"></span>
<span class="office-info-title">Courriel</span>
<span class="office-info-data"><a href="mailto:cliniquechirodl@gmail.com">cliniquechirodl@gmail.com</a></span>
</div>
</div>
</div>
</div>
</header>
<div id="sections">
<div class="subpage defaultContentWidth clear onecolumn" id="container">
<div class="mainbar entry-content clearfix" id="content">
<div id="content-wrapper">
</div>
</div>
</div>
</div>
<footer class="page-footer">
<aside class="footer-line clearfix">
<div class="wrapper clearfix">
<div class="footer-text left"><p>© 2016 Copyright Clinique Dr David Leclerc Chiropraticien.</p></div>
<div class="footer-menu right clearfix">
<nav class="footer-menu"><ul class="menu clear" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51" id="menu-item-51"><a href="/urgences/">Urgences</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="/contactez-nous/">Contactez-Nous</a></li>
</ul></nav> </div>
</div>
</aside>
</footer>
<script src="/design/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="/design/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="/design/js/jquery/ui/tabs.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="/design/js/jquery/ui/accordion.min.js?ver=1.11.4" type="text/javascript"></script>
<script type="text/javascript">
</script>
</body>
</html>