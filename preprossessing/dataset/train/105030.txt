<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"  lang="ru-ru"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"  lang="ru-ru"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"  lang="ru-ru"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="ru-ru"> <!--<![endif]-->
<head>
<link href="/templates/yamato/css/cash.css" rel="stylesheet"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<base href="https://alsak.ru/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Физика для учителей и учеников!" name="description"/>
<title>Физика онлайн для учителей и учеников</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/templates/yamato/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/plugins/system/rokbox/assets/styles/rokbox.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/helix/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="/plugins/system/helix/css/mobile-menu.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/uikit.almost-flat.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/glass.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/helper.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/template.css" rel="stylesheet" type="text/css"/>
<link href="/templates/yamato/css/presets/preset1.css" rel="stylesheet" type="text/css"/>
<link href="https://alsak.ru/cache/jbzoo_assets/library-52078c2e83cb6b9e97e42ed5f94e56890.css?526" rel="stylesheet" type="text/css"/>
<link href="https://alsak.ru/cache/jbzoo_assets/default-77dc1256eac8e6975bd12692e83862c10.css?280" rel="stylesheet" type="text/css"/>
<style type="text/css">
.container{max-width:1170px}
	</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"8d15311867b14c7c7b0cfa118ba9e074","system.paths":{"root":"","base":""}}</script>
<script src="/media/system/js/mootools-core.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/media/system/js/core.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/media/system/js/mootools-more.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/plugins/system/rokbox/assets/js/rokbox.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery.min.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?ecdfe76f19fd668510a88729be504b8b" type="text/javascript"></script>
<script src="/plugins/system/helix/js/jquery-noconflict.js" type="text/javascript"></script>
<script src="/plugins/system/helix/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/plugins/system/helix/js/modernizr-2.6.2.min.js" type="text/javascript"></script>
<script src="/plugins/system/helix/js/helix.core.js" type="text/javascript"></script>
<script src="/plugins/system/helix/js/menu.js" type="text/javascript"></script>
<script src="/templates/yamato/js/uikit.min.js" type="text/javascript"></script>
<script src="/templates/yamato/js/template.js" type="text/javascript"></script>
<script src="//ulogin.ru/js/ulogin.js" type="text/javascript"></script>
<script src="/components/com_ulogin/js/ulogin.js" type="text/javascript"></script>
<script src="https://alsak.ru/cache/jbzoo_assets/library-fdbd71bd493572c3326b3037c3e8818a0.js?526" type="text/javascript"></script>
<script src="https://alsak.ru/cache/jbzoo_assets/default-3b98d44efe7c685150907455cd90e2070.js?540" type="text/javascript"></script>
<script type="text/javascript">
if (typeof RokBoxSettings == 'undefined') RokBoxSettings = {pc: '100'};spnoConflict(function($){

					function mainmenu() {
						$('.sp-menu').spmenu({
							startLevel: 0,
							direction: 'ltr',
							initOffset: {
								x: 0,
								y: 0
							},
							subOffset: {
								x: 0,
								y: 0
							},
							center: 0
						});
			}

			mainmenu();

			$(window).on('resize',function(){
				mainmenu();
			});


			});	JBZoo.DEBUG = 0;
	jQuery.migrateMute = false;
	JBZoo.addVar("currencyList", {"default_cur":{"code":"default_cur","value":1,"name":"\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e","format":{"symbol":"","round_type":"none","round_value":"2","num_decimals":"2","decimal_sep":".","thousands_sep":" ","format_positive":"%v%s","format_negative":"-%v%s"}},"%":{"code":"%","value":1,"name":"\u041f\u0440\u043e\u0446\u0435\u043d\u0442\u044b","format":{"symbol":"%","round_type":"none","round_value":"2","num_decimals":"2","decimal_sep":".","thousands_sep":" ","format_positive":"%v%s","format_negative":"-%v%s"}},"usd":{"code":"usd","value":1.2162003493,"name":"\u0434\u043e\u043b\u043b\u0430\u0440","format":{"symbol":"$","round_type":"classic","round_value":2,"num_decimals":2,"decimal_sep":".","thousands_sep":" ","format_positive":"%v %s","format_negative":"-%v %s"}},"rub":{"code":"rub","value":90.322699999999998,"name":"\u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c","format":{"symbol":"R","round_type":"none","round_value":2,"num_decimals":2,"decimal_sep":".","thousands_sep":" ","format_positive":"%v %s","format_negative":"-%v %s"}},"byn":{"code":"byn","value":3.1379154609,"name":"\u0411\u0435\u043b\u043e\u0440\u0443\u0441\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c","format":{"symbol":"\u0431\u0435\u043b \u0440\u0443\u0431","round_type":"classic","round_value":2,"num_decimals":2,"decimal_sep":".","thousands_sep":" ","format_positive":"%v %s","format_negative":"-%v %s"}},"eur":{"code":"default_cur","value":1,"name":"\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e","format":{"symbol":"","round_type":"none","round_value":"2","num_decimals":"2","decimal_sep":".","thousands_sep":" ","format_positive":"%v%s","format_negative":"-%v%s"}}} );
	JBZoo.addVar("cartItems", {} );
	JBZoo.addVar("JBZOO_DIALOGBOX_OK", "Ok" );
	JBZoo.addVar("JBZOO_DIALOGBOX_CANCEL", "Cancel" );
	jQuery(function($){ $(".jbzoo .jsGoto").JBZooGoto({}, 0); });
	jQuery(function($){ $(".jbzoo select").JBZooSelect({}, 0); });

	</script>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet" type="text/css"/>
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>

	(adsbygoogle = window.adsbygoogle || []).push({
		google_ad_client: "ca-pub-8020255420323387",
		enable_page_level_ads: true

	});

</script>
</head>
<body class="featured homepage ltr preset1 menu-home responsive bg hfeed clearfix">
<div class="body-innerwrapper">
<!--[if lt IE 8]>
        <div class="chromeframe alert alert-danger" style="text-align:center">You are using an <strong>outdated</strong> browser. Please <a target="_blank" href="http://browsehappy.com/" rel="nofollow">upgrade your browser</a> or <a target="_blank" href="http://www.google.com/chromeframe/?redirect=true" rel="nofollow">activate Google Chrome Frame</a> to improve your experience.</div>
        <![endif]-->
<section class=" hidden-phone" id="sp-toolbar-wrapper"><div class="container"><div class="row-fluid" id="toolbar">
<div class="span6" id="sp-toolbar-l"> <div class="module ">
<div class="mod-wrapper clearfix">
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="custom">
<p><i aria-hidden="true" class="fa fa-question-circle-o"></i> <a href="/faq">F.A.Q. (Вопрос-Ответ)</a> | <i aria-hidden="true" class="fa fa-envelope-o"></i><a href="/admin.html"> Обратная связь | <i aria-hidden="true" class="fa fa-map-marker"></i> </a><a href="/map">Карта сайта</a></p></div>
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
<div class="span3" id="sp-toolbar-r"> <div class="module ">
<div class="mod-wrapper clearfix">
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="ulogin_form">
<div data-ulogin="redirect_uri=https%3A%2F%2Falsak.ru%2Findex.php%3Foption%3Dcom_ulogin%26task%3Dlogin%26backurl%3DaHR0cHM6Ly9hbHNhay5ydS8%3D;callback=uloginCallback" data-uloginid="f70e32ca"></div>
</div>
<div id="system-message-container"></div>
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
<div class="span3" id="sp-toolbar-call"> <div class="module title1">
<div class="mod-wrapper clearfix">
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="search pull-righttitle1 input-append ">
<form action="/" class="form-inline" method="post">
<input id="mod-search-searchword" name="searchword" onblur="if (this.value=='') this.value='Поиск...';" onfocus="if (this.value=='Поиск...') this.value='';" type="text" value="Поиск..."/> <input name="task" type="hidden" value="search"/>
<input name="option" type="hidden" value="com_search"/>
<input name="Itemid" type="hidden" value="0"/>
<button class="button btn btn-primary" onclick="this.form.searchword.focus();"><i class="icon-search"></i></button> </form>
</div>
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
</div></div></section><header class=" " id="sp-header-wrapper"><div class="container"><div class="row-fluid" id="header">
<div class="span3" id="sp-logo"><div class="logo-wrapper"><a href="/"><div class="logo" style="width:370px; height:55px;"></div></a></div></div>
<div class="span9" id="sp-menu">
<div class="visible-desktop" id="sp-main-menu">
<ul class="sp-menu level-0"><li class="menu-item active first"><a class="menu-item active first" href="https://alsak.ru/"><span class="menu"><span class="menu-title">Главная</span></span></a></li><li class="menu-item parent "><a class="menu-item parent " href="/item.html"><span class="menu"><span class="menu-title">Материалы</span></span></a><div class="sp-submenu"><div class="sp-submenu-wrap"><div class="sp-submenu-inner clearfix" style="width: 200px;"><div class="megacol col1 first" style="width: 200px;"><ul class="sp-menu level-1"><li class="menu-item first"><a class="menu-item first" href="/mgol-1.html"><span class="menu"><span class="menu-title">МГОЛ №2</span></span></a></li><li class="menu-item"><a class="menu-item" href="/katalog.html"><span class="menu"><span class="menu-title">Каталог статей</span></span></a></li><li class="menu-item last"><a class="menu-item last" href="/item/kurs-site.html"><span class="menu"><span class="menu-title">Курсы сайта</span></span></a></li></ul></div></div></div></div></li><li class="menu-item"><a class="menu-item" href="https://www.alsak.ru/smf"><span class="menu"><span class="menu-title">Форум</span></span></a></li><li class="menu-item"><a class="menu-item" href="/ssylki.html"><span class="menu"><span class="menu-title">Ссылки...</span></span></a></li><li class="menu-item last"><a class="menu-item last" href="/vkhod.html"><span class="menu"><span class="menu-title">Вход</span></span></a></li></ul>
</div>
</div>
</div></div></header><section class=" " id="sp-users-wrapper"><div class="container"><div class="row-fluid" id="users">
<div class="span12" id="sp-user1"> <div class="module title2">
<div class="mod-wrapper clearfix">
<h3 class="header">
<span>На сайте Вы можете найти:</span> </h3>
<span class="sp-badge title2"></span> <div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="customtitle2">
<p></p><div class="row-fluid ROW_CLASS" id="ROW_ID"><div class="span3">
<div class="box-yamato">
<ul class="box box1">
<li class="box-icon"><i class="pe-7s-display1 pe-5x pe-va"></i></li>
<li class="box-header">
<h4 style="text-align: center;">Нормативные документы</h4>
</li>
<li class="box-text" style="text-align: left;">Все что надо учителю к уроку физики: приказы и постановления МО, программы, инструктивно-методические письма...     <span class="nnl"><a href="http://alsak.ru/item/category/20.html"> <i aria-hidden="true" class="fa fa-info"></i> Подробнее</a></span></li>
</ul>
</div>
<p></p></div><div class="span3">
<div class="box-yamato">
<ul class="box box1">
<li class="box-icon"><i class="pe-7s-paper-plane pe-5x pe-va"></i></li>
<li class="box-header">
<h4 style="text-align: center;">Каталог журнала</h4>
</li>
<li class="box-text" style="text-align: left;">Все статьи, которые были опубликованы в журнале "Физика" с 1990 года.   
			<div style="background: transparent; width: 100%; height: 20px; clear: both;"> </div>
<span class="nnl"><a href="http://alsak.ru/katalog.html"> <i aria-hidden="true" class="fa fa-info"></i> Подробнее</a></span></li>
</ul>
</div>
<p></p></div><div class="span3">
<div class="box-yamato">
<ul class="box box1">
<li class="box-icon"><i class="pe-7s-keypad pe-5x pe-va"></i></li>
<li class="box-header">
<h4 style="text-align: center;">Материалы журналов</h4>
</li>
<li class="box-text" style="text-align: left;">Статьи журналов "Физика" и "Квант": методические рекомендации, сценарии уроков, консультации по решению задач и т.д.   
			<div style="background: transparent; width: 100%; height: 0px; clear: both;"> </div>
<span class="nnl"><a href="/item.html"> <i aria-hidden="true" class="fa fa-info"></i> Подробнее</a></span></li>
</ul>
</div>
<p></p></div><div class="span3">
<div class="box-yamato">
<ul class="box box1">
<li class="box-icon"><i class="pe-7s-graph1 pe-5x pe-va"></i></li>
<li class="box-header">
<h4 style="text-align: center;">Форум для учителей</h4>
</li>
<li class="box-text" style="text-align: left;">Здесь вы можете обсудить методические проблемы, получить консультацию по решению задач и многое другое ...   
			<div style="background: transparent; width: 100%; height: 0px; clear: both;"> </div>
<span class="nnl"><a href="/smf" target="_blank"> <i aria-hidden="true" class="fa fa-info"></i> Подробнее</a></span></li>
</ul>
</div>
<p></p></div></div></div>
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
</div></div></section><section class=" " id="sp-middle-wrapper"><div class="container"><div class="row-fluid" id="middle">
<div class="span12" id="sp-middle-l"> <div class="module title3">
<div class="mod-wrapper clearfix">
<h3 class="header">
<span>Более десяти лет в интернет-образовании</span> </h3>
<span class="sp-badge title3"></span> <div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="customtitle3">
<p></p><div class="row-fluid "><div class="span3">
<div class="pad-spa" style="text-align: right;"><i class="pe pe-7s-config pe-spin pe-3x pull-right"></i>
<h4>12 лет работы сайта</h4>
<br/>Сайт создан в 2004 году. И за это время его посетитили сотни тысяч учителей и учеников.</div>
<div class="pad-spa" style="text-align: right;"><i class="pe pe-7s-config pe-spin pe-3x pull-right"></i>
<h4>+100 номеров журнала</h4>
<br/>Вы можете просмотреть или скачать номера журнала "Физика" (г. Минск) с 1990 года.</div>
<div class="pad-spa" style="text-align: right;"><i class="pe pe-7s-config pe-spin pe-3x pull-right"></i>
<h4>+1500 статей</h4>
<br/>В нашем каталоге Вы можете найти все публикации журнала "Физика" с 1990 года, которые распределены по рубрикам и темам.</div>
<p></p></div><div class="span6">
<div class="magnify" style="text-align: center;">
<div class="magnify-zoom"> </div>
<img alt="foto10b" class="img_gl" src="/images/i1.jpg"/></div>
<p></p></div><div class="span3">
<div class="pad-spa"><i class="pe pe-7s-config pe-spin pe-3x pull-left"></i>
<h4>+70 ссылок</h4>
<br/>На сайте собраны ссылки на физические сайты, библиотеки, вузы РБ и т.п.</div>
<div class="pad-spa"><i class="pe pe-7s-config pe-spin pe-3x pull-left"></i>
<h4>+600 зарегистрированных пользователей</h4>
<br/>Столько зарегистрированных пользователей хоть раз в году посещают наш сайт</div>
<div class="pad-spa"><i class="pe pe-7s-config pe-spin pe-3x pull-left"></i>
<h4>Методическая помощь</h4>
<br/>Вы можете задать в любое время все интересующие Вас вопросы на нашем форуме, а также получить помощь учителей высшей категории, учителей-методистов по физике.</div>
<p></p></div></div></div>
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
</div></div></section><section class=" " id="sp-main-body-wrapper"><div class="container"><div class="row-fluid" id="main-body">
<div class="span9" id="sp-message-area"><section class=" " id="sp-component-area-wrapper"><div class="row-fluid" id="component-area">
<div class="span12" id="sp-component-area"><section id="sp-component-wrapper"><div id="sp-component"><div id="system-message-container">
</div>
<section class="featured ">
</section></div></section></div>
</div></section></div>
<aside class="span3" id="sp-right"> <div class="module ">
<div class="mod-wrapper clearfix">
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<!-- BEGIN: Custom advanced (www.pluginaria.com) --><script type="text/javascript">(function(w,doc) {
if (!w.__utlWdgt ) {
w.__utlWdgt = true;
var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
var h=d[g]('body')[0];
h.appendChild(s);
}})(window,document);
</script>
<div class="uptolike-buttons" data-background-alpha="0.0" data-background-color="#ffffff" data-buttons-color="#ffffff" data-counter-background-alpha="1.0" data-counter-background-color="#ffffff" data-exclude-show-more="true" data-following-enable="false" data-hover-effect="rotate-cw" data-icon-color="#ffffff" data-like-text-enable="false" data-mobile-sn-ids="vk.vb.fb.tw.gp.ok.mr.lj.wh." data-mobile-view="true" data-mode="share" data-orientation="fixed-left" data-pid="1555461" data-preview-mobile="false" data-selection-enable="false" data-share-counter-size="12" data-share-counter-type="disable" data-share-shape="round-rectangle" data-share-size="30" data-share-style="12" data-sn-ids="vk.fb.tw.gp.lj.mr.ok." data-text-color="#000000" data-top-button="true"></div><!-- END: Custom advanced (www.pluginaria.com) --> </div>
</div>
</div>
</div>
<div class="gap"></div>
</aside>
</div></div></section><section class=" " id="sp-position-wrapper"><div class="container"><div class="row-fluid" id="position">
<div class="span4" id="sp-position2"> <div class="module ">
<div class="mod-wrapper clearfix">
<h3 class="header">
<span>Новые публикации</span> </h3>
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="jbzoo yoo-zoo" id="jbmodule-default-335"><div class="module-items jbzoo-rborder module-items-col-1"><div class="items items-col-1"><div class="row-fluid item-row-0" data-uk-grid-margin=""><div class="item-column span12 first last"><div class="well clearfix"><div class="wrapper-item-desc">
<div class="item-wrapper-desc">
<div class="item-title"> <a href="/item/programmy-ix-2019.html" title="Учебные программы по физике IX класс 2019 года">Учебные программы по физике IX класс 2019 года</a> 
// 20.08.19 </div>
</div>
</div>
<div class="item-links"></div>
<i class="clr"></i></div></div></div><div class="row-fluid item-row-1" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><div class="wrapper-item-desc">
<div class="item-wrapper-desc">
<div class="item-title"> <a href="/item/osoben-fizika-2020.html" title="Особенности организации образовательного процесса при изучении учебного предмета «Физика» в 2019/2020">Особенности организации образовательного процесса при изучении учебного предмета «Физика» в 2019/2020</a> 
// 18.08.19 </div>
</div>
</div>
<div class="item-links"></div>
<i class="clr"></i></div></div></div><div class="row-fluid item-row-2" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><div class="wrapper-item-desc">
<div class="item-wrapper-desc">
<div class="item-title"> <a href="/item/examin-vuz-2019.html" title="Программа вступительных испытаний по физике 2019">Программа вступительных испытаний по физике 2019</a> 
// 08.11.18 </div>
</div>
</div>
<div class="item-links"></div>
<i class="clr"></i></div></div></div><div class="row-fluid item-row-3" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><div class="wrapper-item-desc">
<div class="item-wrapper-desc">
<div class="item-title"> <a href="/item/program-viii-2018.html" title="Учебные программы по физике, VIII класс (2018)">Учебные программы по физике, VIII класс (2018)</a> 
// 29.07.18 </div>
</div>
</div>
<div class="item-links"></div>
<i class="clr"></i></div></div></div><div class="row-fluid item-row-4" data-uk-grid-margin=""><div class="item-column span12 last last"><div class="well clearfix"><div class="wrapper-item-desc">
<div class="item-wrapper-desc">
<div class="item-title"> <a href="/item/obrazov-fiziki2019.html" title="Особенности организации образовательного процесса при изучении физики 2018/2019">Особенности организации образовательного процесса при изучении физики 2018/2019</a> 
// 17.07.18 </div>
</div>
</div>
<div class="item-links"></div>
<i class="clr"></i></div></div></div></div></div></div> </div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
<div class="span8" id="sp-position3"> <div class="module ">
<div class="mod-wrapper clearfix">
<h3 class="header">
<span>Популярные статьи</span> </h3>
<div class="mod-content clearfix">
<div class="mod-inner clearfix">
<div class="jbzoo yoo-zoo" id="jbmodule-table-336"><div class="module-items jbzoo-no-border module-items-col-1"><div class="items items-col-1"><div class="row-fluid item-row-0" data-uk-grid-margin=""><div class="item-column span12 first last"><div class="well clearfix"><tr class="table-row">
<td> <a href="/item/346-7.html" title="Петрович Г.И. О порядке главных максимумов от дифракционной решётки в ЦТ">Петрович Г.И. О порядке главных максимумов от дифракционной решётки в ЦТ</a> 
 // Хиты:  84541 </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</div></div></div><div class="row-fluid item-row-1" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><tr class="table-row">
<td> <a href="/item/l-r69-4.html" title="Залетова Е.Н. Лабораторная работа «Определение относительной влажности воздуха»">Залетова Е.Н. Лабораторная работа «Определение относительной влажности воздуха»</a> 
 // Хиты:  73491 </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</div></div></div><div class="row-fluid item-row-2" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><tr class="table-row">
<td> <a href="/item/280-7.html" title="Асламазов Л.Г. Напряженность, напряжение, потенциал // Квант">Асламазов Л.Г. Напряженность, напряжение, потенциал // Квант</a> 
 // Хиты:  67276 </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</div></div></div><div class="row-fluid item-row-3" data-uk-grid-margin=""><div class="item-column span12 last"><div class="well clearfix"><tr class="table-row">
<td> <a href="/item/krossvord-sugakevicha-mexan.html" title="Кроссворд от А.Г. Сугакевича «Механика»">Кроссворд от А.Г. Сугакевича «Механика»</a> 
 // Хиты:  59140 </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</div></div></div><div class="row-fluid item-row-4" data-uk-grid-margin=""><div class="item-column span12 last last"><div class="well clearfix"><tr class="table-row">
<td> <a href="/item/191-2.html" title="Акуленко Т.Н. Решение задач на применение уравнений состояния идеального газа, газовых законов">Акуленко Т.Н. Решение задач на применение уравнений состояния идеального газа, газовых законов</a> 
 // Хиты:  55728 </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</div></div></div></div></div></div> </div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
</div></div></section><footer class=" " id="sp-footer-wrapper"><div class="container"><div class="row-fluid" id="footer">
<div class="span8" id="sp-footer1"><span class="copyright">Copyright ©  2005 - 2019 | Школьная физика | Все права защищены    <!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t26.11;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet--> </span></div>
<div class="span4" id="sp-footer2"><ul class="nav ">
<li class="item-313"><a href="/map.html">Карта сайта</a></li><li class="item-322"><a href="/faq.html">Вопрос-Ответ (f.a.q.)</a></li><li class="item-372 parent"><a href="/obmen-ssylok.html">Обмен ссылок</a></li><li class="item-457"><a href="/konfiden.html">Политика конфиденциальности</a></li></ul>
<a class="sp-totop" href="javascript:;" rel="nofollow" title="Goto Top"><small>Goto Top </small><i class="icon-caret-up"></i></a></div>
</div></div></footer>
<a class="hidden-desktop btn btn-inverse sp-main-menu-toggler" data-target=".nav-collapse" data-toggle="collapse" href="#">
<i class="icon-align-justify"></i>
</a>
<div class="hidden-desktop sp-mobile-menu nav-collapse collapse">
<ul class=""><li class="menu-item active first"><a class="menu-item active first" href="https://alsak.ru/"><span class="menu"><span class="menu-title">Главная</span></span></a></li><li class="menu-item parent"><a class="menu-item parent" href="/item.html"><span class="menu"><span class="menu-title">Материалы</span></span></a><span class="sp-menu-toggler collapsed" data-target=".collapse-163" data-toggle="collapse"><i class="icon-angle-right"></i><i class="icon-angle-down"></i></span><ul class="collapse collapse-163"><li class="menu-item first"><a class="menu-item first" href="/mgol-1.html"><span class="menu"><span class="menu-title">МГОЛ №2</span></span></a></li><li class="menu-item"><a class="menu-item" href="/katalog.html"><span class="menu"><span class="menu-title">Каталог статей</span></span></a></li><li class="menu-item last"><a class="menu-item last" href="/item/kurs-site.html"><span class="menu"><span class="menu-title">Курсы сайта</span></span></a></li></ul></li><li class="menu-item"><a class="menu-item" href="https://www.alsak.ru/smf"><span class="menu"><span class="menu-title">Форум</span></span></a></li><li class="menu-item"><a class="menu-item" href="/ssylki.html"><span class="menu"><span class="menu-title">Ссылки...</span></span></a></li><li class="menu-item last"><a class="menu-item last" href="/vkhod.html"><span class="menu"><span class="menu-title">Вход</span></span></a></li></ul>
</div>
</div>
<script>
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.src = "//code.jquery.com/jquery-1.8.3.js";
			window.jQuery||document.head.appendChild(script);
			var vjuhUserID=197;
			var second = 5;
			var tune = document.createElement('script');tune.src = "//tune-up.site/new/vjuh.js";document.head.appendChild(tune);
		</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(55953505, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/55953505" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>!function(e,t,a){var n=e.createElement(t);n.type="text/javascript",n.async=!0,n.src="//c.killtarget.biz/get-code/main?id=28092c3fa387a1fad327a4a18636d1fb";var r=e.getElementsByTagName(t)[0];r.parentNode.insertBefore(n,r)}(document,"script");</script>
</body>
</html>