<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bachmann Trains - Model Trains</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<link href="https://www.bachmanntrains.com/home-usa/css/2010_styles.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/css/2012_styles.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/banner-rotator/themes/default/default.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/banner-rotator/themes/bar/bar.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/banner-rotator/nivo-slider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.bachmanntrains.com/home-usa/banner-rotator/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.5/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- <script type="text/javascript" src="https://www.bachmanntrains.com/home-usa/scripts/jquery-ui-1.8.1.custom.min.js"></script> 
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="https://www.bachmanntrains.com/home-usa/banner-rotator/jquery.nivo.slider.js" type="text/javascript"></script>
<!-- CAL -->
<script src="https://www.bachmanntrains.com/home-usa/phpcal/js/less.js" type="text/javascript"></script>
<script src="https://www.bachmanntrains.com/home-usa/phpcal/js/prism.js" type="text/javascript"></script>
<script src="https://www.bachmanntrains.com/home-usa/phpcal/js/underscore.js" type="text/javascript"></script>
<script src="https://www.bachmanntrains.com/home-usa/phpcal/js/moment.js" type="text/javascript"></script>
<script src="https://www.bachmanntrains.com/home-usa/phpcal/js/clndr.js" type="text/javascript"></script>
<style type="text/css">
<!--
.s22 {background-color: #A4CAE6;}
.s21 {background-color: #F2BFBF;}
.s23 {background-color: #CCFF00;}
.s24 {background-color: #FBF484;}
.s29999 {background-color: #FFC18A;}
#masterPage {height: 1040px !important;}
div#socialLogos {top:680px !important;}
#leftSide {height:661px !important;}
#rssFeed {top:710px !important;}
div#siteMap {top:630px !important;}
div#copyright {top:745px !important;}
/* div#BTrotator {top:130px !important;} */
div#aboutUs {top:475px !important;}
div#FeaturesDiv {height:392px !important;}
.nivoSlider {width:597px !important;}
-->
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-47226188-1']);
  _gaq.push(['_setDomainName', 'bachmanntrains.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<div id="masterPage">
<div id="pageHeader">
<a class="headerBtn" href="https://shop.bachmanntrains.com/" id="storeBtn">ONLINE<br/>STORE<br/><img src="https://www.bachmanntrains.com/home-usa/images/bach-man_products.png"/></a>';
	<a class="headerBtn" href="https://www.bachmanntrains.com/home-usa/board/index.php" id="askBachmanBtn">"ASK THE BACH MAN"<br/>FORUM<br/><img src="https://www.bachmanntrains.com/home-usa/images/bach-man_board_no-hyphen.png"/></a>
<a class="headerBtn" href="https://www.bachmanntrains.com/home-usa/prod_serv.php" id="partsBtn">PARTS, SERVICE,<br/>&amp; INFORMATION<br/><img src="https://www.bachmanntrains.com/home-usa/images/bach-man_parts.png"/></a>
<a class="headerBtn" href="https://www.bachmanntrains.com/home-usa/catalogs_brochures.php" id="catalogBtn">CATALOGS AND<br/>BROCHURES<br/><img src="https://www.bachmanntrains.com/home-usa/images/catalog_tn.png"/></a>
<!--
				<a id="whatsNewBtn" class="headerBtn">WHAT'S<br/>NEW?<br/><span style="font-size:70px; color: #FAC447;">?</span></a>
-->
</div>
<div id="leftSide">
<div class="leftBtn" id="bachmann2013Btn">
<a href="https://shop.bachmanntrains.com/"><img src="https://www.bachmanntrains.com/home-usa/images/bachmann_button.png" title="Click here to explore our complete line of model railroading products in HO, N, O, On30,and Large Scale."/></a>
</div>
<div class="leftBtn" id="bighauler2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=718"><img src="https://www.bachmanntrains.com/home-usa/images/big_hauler.png" title="1:22.5 large scale products from Bachmann's standard line."/></a>
</div>
<div class="leftBtn" id="plasticville2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=754"><img src="https://www.bachmanntrains.com/home-usa/images/plasticville.png" title="Classic kits and pre-assembled buildings in HO, N, and O scale."/></a>
</div>
<div class="leftBtn" id="willams2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=491"><img src="https://www.bachmanntrains.com/home-usa/images/williams_logo.png" title="Affordable, durable, and easy to operate O-gauge 3-rail trains."/></a>
</div>
<div class="leftBtn" id="spectrum2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=755"><img src="https://www.bachmanntrains.com/home-usa/images/spectrum.png" title="Premium HO, N, On30, and 1:20.3 Large Scale products."/></a>
</div>
<div class="leftBtn" id="eztrack2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=604"><img src="https://www.bachmanntrains.com/home-usa/images/ez-track3.png" title="The snap-fit track and roadbed system for HO and N scales."/></a>
</div>
<div class="leftBtn" id="thomas2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=756"><img src="https://www.bachmanntrains.com/home-usa/images/thomas_cloud_tm.png" title="Thomas &amp; Friends™ HO and Large Scale locomotives, train sets and accessories."/></a>
</div>
<div class="leftBtn" id="dynamis2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=274"><img src="https://www.bachmanntrains.com/home-usa/images/dcc_logo.png" title="Digital command control products for beginning and advanced modelers."/></a>
</div>
<div class="leftBtn" id="scenescapes2013Btn">
<a href="https://shop.bachmanntrains.com/index.php?main_page=index&amp;cPath=469"><img src="https://www.bachmanntrains.com/home-usa/images/scene_scapes_logo.png" title="Trees and landscaping materials suitable for all scales."/></a>
</div>
<div class="leftBtn" id="ctpatBtn">
<a href="https://www.cbp.gov/border-security/ports-entry/cargo-security/c-tpat-customs-trade-partnership-against-terrorism" target="_blank"><img src="https://www.bachmanntrains.com/home-usa/images/ctpat.gif" title="A Proud C-TPAT Memeber."/></a>
</div>
<div style="height: 32px;"></div>
</div>
<div id="pageContent">
<div id="topMenu">
<div style="position: absolute; top: 6 px;">
<a href="https://www.bachmanntrains.com/home-usa/whats_new.php" id="whatsNewBtn" style="border-left: 0px;">What's New</a>
<a href="https://www.bachmanntrains.com/home-usa/board/index.php#2" id="faqBtn">FAQs</a>
<a href="https://www.bachmanntrains.com/home-usa/newsletter.php" id="giftShopBtn">Newsletter</a>
<a href="https://www.bachmanntrains.com/home-usa/contactus.php" id="contactUsBtn">Contact</a>
<!--                    <a id="linksBtn">Links</a>-->
<a href="https://www.bachmanntrains.com/home-usa/about_us.php" id="aboutUsBtn">About Us</a>
<a href="https://www.bachmanntrains.com/home-usa/getting_started.php" id="aboutUsBtn">Getting Started</a>
<a href="https://www.bachmanntrains.com/home-usa/careers.php" id="careersBtn">Careers</a>
</div>
<!--<div style="position: absolute; top: 36px;">
                 <a id="mrcBtn" style="border-left: 0px;">Model Railroad Club</a>
                    <a href="https://www.bachmanntrains.com/home-usa/newsletter.php" id="newsletterBtn">Newsletter Sign-Up</a>
                </div> -->
</div><div id="FeaturesDiv">
<div id="BannerDiv">
<!-- *** ad banner start *** -->
<a href="https://www.bachmanntrains.com/home-usa/ez-app.php">
<img src="https://www.bachmanntrains.com/home-usa/images/ezapp/ezapp_home_banner.jpg" width="597"/>
</a>
<!-- *** ad banner end *** -->
<!--////// START of the slider //////-->
<div class="nivoSlider" id="slider">
<a href="https://www.bachmanntrains.com/bm/news/featuredproduct/1579"><img data-thumb="https://www.bachmanntrains.com/home-usa/banner-rotator/images/OFigures2.jpg" id="imgid1" src="https://www.bachmanntrains.com/home-usa/banner-rotator/images/OFigures2.jpg" title="#htmlcaption1"/></a> </div>
<div id="htmlcaption1" style="display:none">
<div class="nivo-caption1">O Scale Figures in Stock</div>
<div class="nivo-caption1a"> </div>
<div class="nivo-caption1ab">Click on the image for Info</div>
</div> <script type="text/javascript">
    $.noConflict();
    jQuery(window).load(function() {
        jQuery('#slider').nivoSlider({
            effect: 'fade',
            pauseTime: 6000,
            startSlide: 0,
            directionNav: true,
            controlNav: false
            });
    });
    </script>
<!--////// END of the slider ///////-->
</div>
</div>
<div id="bachCalender">
<!-- START CALENDAR -->
<div id="bmcalendar"></div>
<div id="bmcalendardiv"></div>
<!-- END CALENDAR -->
</div>
<div id="downloads">
<a href="https://www.bachmanntrains.com/home-usa/wallpaper.php">
<div id="downloadsBox">
</div></a>
<span id="boxTitle">Free Downloads</span>
</div>
<div id="retailers">
<div id="retailersBox">
<a href="https://www.bachmanntrains.com/bm/locator"><img src="https://www.bachmanntrains.com/home-usa/images/find_a_retailer.jpg"/></a>
</div>
<span id="boxTitle">Find a Retailer</span>
</div>
<div id="aboutUs">
<div id="BTrotator">
<ul>
<img height="170" id="rbrotateimg" src="https://www.bachmanntrains.com/home-usa/images/about_1.jpg" width="387"/>
<!-- 
<a href="https://www.bachmanntrains.com/home-usa/play-tape-contest.php">
  <img src="https://www.bachmanntrains.com/home-usa/images/play_tape_img.jpg" width="387" height="170" />
</a>
-->
</ul>
</div>
<!-- <div style="position:relative;top:130px"> -->
<map name="worldmap">
<area alt="Bachmann Europe Plc" coords="14,105,69,142" href="https://www.bachmann.co.uk/" shape="rect" target="_blank" title="Bachmann Europe Plc"/>
<area alt="Bachmann Liliput" coords="81,105,135,142" href="http://www.liliput.de/" shape="rect" target="_blank" title="Bachmann Liliput"/>
<area alt="Bachmann China" coords="144,105,199,142" href="https://www.bachmannchina.com.cn/" shape="rect" target="_blank" title="Bachmann China"/>
</map>
<img border="0" id="worldImg" src="https://www.bachmanntrains.com/home-usa/images/earth2d.gif" usemap="#worldmap"/>
<!-- </div> -->
</div>
<div id="socialLogos">
<a href="https://www.facebook.com/pages/Bachmann-Trains/182658255760?v=wall" target="_blank"><img class="socialIMGs" id="facebook" src="https://www.bachmanntrains.com/home-usa/images/logo_facebook.jpg"/></a>
<a href="https://twitter.com/bachmanntrains" target="_blank"><img class="socialIMGs" id="twitter" src="https://www.bachmanntrains.com/home-usa/images/logo_twitter.jpg"/></a>
<a href="https://www.youtube.com/user/bachmanntrains" target="_blank"><img class="socialIMGs" id="youtube" src="https://www.bachmanntrains.com/home-usa/images/logo_youtube.jpg"/></a>
</div>
<div id="rssFeed"><a href="http://feeds.feedburner.com/RssFeedForBachmannTrainsNews"><img alt="RSS Feed" border="0" src="https://www.bachmanntrains.com/home-usa/images/rss_icon.png" title="RSS Feed"/></a></div>
<!-- <div id="siteMap"><a>Sitemap</a></div> -->
<div id="copyright">Copyright 2021, Bachmann Industries, Inc.</div>
</div>
</div>
<!-- calendar init -->
<link href="https://www.bachmanntrains.com/home-usa/css/phpcalstyle.css" rel="stylesheet" type="text/css"/>
<script src="https://www.bachmanntrains.com/home-usa/js/phpcal.js"></script>
<script>
( function($) {
  
  var thisday = new Date();
  var thisdaymonth = thisday.getUTCMonth();   
  var thisdayyear = thisday.getFullYear();
  Bmcal.createcal(thisdayyear,thisdaymonth);
  Bmcal.clickevents(); 
  
})(jQuery);
</script>
<!-- calendar init END -->
<script>

  var RBimgRot=function(){var a=[],b=0,f=function(){var d=document.getElementById("rbrotateimg");d.src=a[b];b++;b>=a.length&&(b=0);e(d,8500,!0);setTimeout(f,8500)},e=function(a,c,b){!0===b?c--:c++;0<c&&100>c&&(a.style.opacity=c/100,setTimeout(function(){e(a,c,b)},15))};return{init:function(){a[0]="https://www.bachmanntrains.com/home-usa/images/about_1.jpg";a[1]="https://www.bachmanntrains.com/home-usa/images/about_2.jpg";a[2]="https://www.bachmanntrains.com/home-usa/images/about_3.jpg";a[3]="https://www.bachmanntrains.com/home-usa/images/about_4.jpg";
a[4]="https://www.bachmanntrains.com/home-usa/images/about_5.jpg";a[5]="https://www.bachmanntrains.com/home-usa/images/about_6.jpg";setTimeout(f,8500)}}}();RBimgRot.init();


    </script>
</body>
</html>