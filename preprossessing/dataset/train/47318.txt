<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="apple-touch-fullscreen"/>
<meta content="telephone=no,email=no" name="format-detection"/>
<meta content="fullscreen=yes,preventMove=no" name="ML-Config"/>
<meta content="black" name="apple-mobile-web-app-status-bar-style"/>
<title>404</title>
<meta content="" name="keywords"/>
<meta content="经验本,养生,保健" name="description"/>
<link href="/m/css/base2.css?v2" rel="stylesheet" type="text/css"/>
<link href="/css/other.css?v1" rel="stylesheet" type="text/css"/>
<script src="js/styleinterference.js" type="text/javascript"></script>
<style type="text/css">
.nofind_3{padding:8rem 10% 11rem;text-align:center}
.nofind_3 img{width:100%;vertical-align:top;margin-bottom:5.5rem}
.nofind_3 p{width:100%;height:2rem;font-size:1.4rem;color:#434343;line-height:2rem}
.nofind_3 .btn{margin-top:9rem;float:left;width:100%;border-radius:5px;height:4rem;padding:0;border:none 0;line-height:4rem;font-size:1.6rem}
</style>
</head>
<body>
<div class="loading up" id="loading"><i></i></div>
<div class="wrap bg_white">
<header><span><a aria-hidden="true" class="ico-back white" href="javascript:history.go(-1);"></a></span>404</header>
<div class="nofind_3"><img src="images/image_4.jpg"/><p>很抱歉，您访问的页面不在地球上...</p><a class="btn btn_red" href="http://www.jingyanben.com/">返回首页</a></div>
</div>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/slide.min.js" type="text/javascript"></script>
<script src="js/default.js" type="text/javascript"></script>
<script type="text/javascript">
df.ready(function(){
    df.hots(".hots_table"); //tab滑动;
    df.tab('.hots_table li'); //abmenu;
});
</script>
</body>
</html>