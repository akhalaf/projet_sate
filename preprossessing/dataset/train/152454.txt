<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "59794",
      cRay: "610eaf8b79a003a1",
      cHash: "4778449c438e56b",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJyZWRhdHV0dG8uY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "jr1OuEZegIy7Ex6bEiAzAKielQufT2XVSy+JXYp11A5OgoN71XS1EGDNU7MbJD/FTqKXw+B9EzGkp1VkNGhsac3WqGzjtbN2ICP6aRXGpqOYug4u6Q5cO5UgM5Dgo01X7APvUSiwmd14/Ye1c/HRSzHHqpEZBUGvguGMGEUqLQW66x3IKo22KtCTlQfnF3wcTVWPGaHE5k5iq72dKZPu4/6KdPyoZY7YIY0TS2Bi8fVlUHmZ55VnzyTH1oepMrs3jmYU+4YoP+MDIvINOjSCN4dfZcAUW0TrpGHNvhTitxX/Ncf/baZnOZkzXkM225/kOJLjOKnoKSlkbDzRqdTHTlnibD1Uc2q9NmMbmbVoLu3vqubCEBpE4YCJMZvirY8OuwdLYTliWu9TX33FnnynV6qtZUaG8WT4MBa48RlXJhcMX909o5hQra02CnhdU3LtaqqqPpYfAL8YpPlRnIqn6S8jZXLS9721sRGDifw3KLJ/WtoIapqQCpcbed8pUzZKKI52yv0NCqP+sTsQrLYxNNihYd0Q033x9S5I0JildLfU9NhtMLWm16riYwCJLH0b7A/vn2MbMNKSewzH48EDJq2aK7EG99hGhbEFjtvEcXx5gJ+Xd7JWwyCIT391ydZ3jWnCpg3fnOBBLSbAjqM8OmMIGxunHgjQGdOC4ro8Ud1MWJkc2rLsK+uCnEBy2a51aBTQBsoLIyWpmMOq6fwNcl6LTDnNMdQLHvWPKmP1sKGG5F4BbINpEQb313koCE7G",
        t: "MTYxMDUzNjM4My4yODgwMDA=",
        m: "yzS7I0DD7W9SZv53bUAZVfn3b1rv8j5qcl3Lun2erBs=",
        i1: "YCtOzUW+A/r09CIhjOEF+w==",
        i2: "uXY+s5/advs4Pfr38dpX4A==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "bFoQ6HB75Y8r7rvmNFTIpebDFAlpfDJA5cbQichI7/s=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.arredatutto.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=7844683eb97bd8ead5dd69b04efb8b2ebd49f343-1610536383-0-AWMkmOjp1N_neLXvwMzJfGKfAY-4Vtz0oFZkCbec03D3IZ8rpWl28828Gdx8ozgvRA9MmFBlEbxLQ_Jrx9PrnfA3G6fyir4KyqUu0-NKwlyyTF7W_PQ5ETaExZ__7W98oc7b6JViuqb9LAkFciDKn3yqesBuWfdKntlTn4gruKDnmDKwZog5Gfy9CPj5uvZfVIsbul3IgiCOyctitfivyfudinHtMbgcIqNES9j1dDtBGinauuuapI0eQoWMJUxMaD8ZvMTTIn5XdZzSKtPcmmCZWXHBw87zmXiQTryUQrchiMXUEKcSgZMUik0fq5i3fi_QU-o_YypFKxoJGLdxhZkutVosqyWjCeT6rs9aASkXSRIjDRa-hK7CGM-6E7xGtgC1IjC79ZSUYWmdEmHxMiHwrndr8MmPDSncebYiYBtnu6rkiKxDlFUHWgEP3CupZbqcHdt1uZYwP4L2D2t5UMczvQ6sxSxXq5N5fogErRkSLxML-lzo_-TfCah89qJywbzl7kWT8J_lfBrCTNxnLy4" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="8f70cc4095ee864fb0b03ca7b6053462c714e670-1610536383-0-ATCjWbk0C/KVFkamqoboqn2MeMh8YwU3j+ZUkESt29rAhy7G63dPtKtFO6EdoMkqjeDJgka7kuOS2SdTCgiqV8sdJUDVGzD/OEOQEp4vdVbOHHVZCqDAIUnwzsQZF1SnpBwIqXZzrbtLbqkyRu85ejDe2kl892S15+0zqX3VP9mHP4I/CU+0JWBmtZmV3KGrarSmGR6p77bc+ALGR7SSl9jjNGCYaAOcU8uKn4NEeZGQ5i9LglywYVglmra2AghSnvLpQ8O/6ckWStki+1yTQ+KLI/5F0Z3uoFWgLsELP2IquNmeBGb4NSOexk9qLc4C65jUokRebGXtxIoCfGr61yUx+5DCcsqRmxoebVEOE4lcckYusWSarPL03OkiXc2MaUAiR6aPKrHa0NGJt40QE+T7qB9OW6NH8uDJOmsru+b4kUPk+FE+6qJJu0lXkSIROUrHzWegTyb9eZORFXQ3X7Z3fuYWHUWsmLakE5sFhNmyIXHqDnHrHvP7nNNrD/hHzQe3LbR03CyYlvo0RwIKoXUAD07cZ5c5PaONHKGA1hVPHBQgSqYRj8mlFZUr5dW2e4jLyCu1gqkik4Fzjg07+n5Wyqo9QzqBoOhaSoBlcARBZQwKCZktDsyDLoHGAg02bEjLqNGXndkzYytXoVfPlc8H5ACmJ0A1nSnvK+m5q0JjPz9X/F9zlLVboa5yDGx1uxLoZIfRD01/VbvbXD/1g7EhwgjhOyNX0oLflYFdL2d312Tgbyxd5W3o40fplD0fI9bA+wTWSfNuq+WGpjwvcoZx7HZqB8E27EM0sqGMErmoe7kOramFkwvr9Qf7iTCj4+14ww8e/hItPX4ghFXbY/peM4FxKUKun1VmCVc3usaZMjOyb8DboyXnUTNGCs40kBbLjXJdgak/D96YZHyf9eo8CiOgq4saDctbaA4JhQfs3K8s1nlH7LcK5amNV2uyuJcBNuZPmTEiLoUW1OHcWksX2V040iKgYj6fzkqO9u38EQaXiCRzt05OXzn282q95AfmfGLsp27caAjd4MCR6naTIg0c4EdnCGf7TviogqGMSasS87uNhk4mgixfsR2X0oWXVnj+s0QvHS0R63L09JUJAqqkaEXbJhlvCtdKlGaKYT15pRrqCwtt/gt0Y5mpQqQP4TNZ8e9LznSPERNFpocHgYjxCSioLjkIgME5QVJ3muao40XCL/Z6MGNRPH/aZnAychKFlUnI10f+5EZRXtNiRwPiTBqsU9GW4OfZQbio+3A01Y5N1QZGKC88xnzBVCvPv97xv2f4meF5ugoKS93mQGI+DpQGNBeSWgGSfa1Y"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="05a1dde08d26ee5ebdf3f565027fc69d"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610eaf8b79a003a1')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610eaf8b79a003a1</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
