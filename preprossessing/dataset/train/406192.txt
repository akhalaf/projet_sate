<!DOCTYPE html>
<html>
<head>
<!-- Site made with Mobirise Website Builder v4.4.0, https://mobirise.com -->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="Mobirise v4.4.0, mobirise.com" name="generator"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="assets/images/logo4.png" rel="shortcut icon" type="image/x-icon"/>
<meta content="" name="description"/>
<title>Home</title>
<link href="assets/web/assets/mobirise-icons/mobirise-icons.css" rel="stylesheet"/>
<link href="assets/tether/tether.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet"/>
<link href="assets/theme/css/style.css" rel="stylesheet"/>
<link href="assets/mobirise/css/mbr-additional.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<section class="header10 cid-qB9i2HxVpR mbr-fullscreen mbr-parallax-background" data-rv-view="18" id="header10-2">
<div class="container">
<div class="media-container-column mbr-white col-lg-8 col-md-10 ml-auto">
<h1 class="mbr-section-title align-right mbr-bold pb-3 mbr-fonts-style display-2">
                SITIO EN CONSTRUCCIÃN</h1>
<h3 class="mbr-section-subtitle align-right mbr-light pb-3 mbr-fonts-style display-5">Desarrollo Corporativo tiene como misiÃ³n contribuir con cada empresa nacional en su desarrollo, proporcionÃ¡ndole herramientas para alcanzar su crecimiento constante, mediante el adecuado incentivo que permita alcanzar a la empresa a su punto efectivo de su organizaciÃ³n y gestiÃ³n que le permita alcanzar su Ã©xito y crecimiento continuo, impulsando el desarrollo del talento humano del personal de la empresa que permita liderazgo efectivos logrando alcanzar por ende el crecimiento de la empresa.</h3>
<p class="mbr-text align-right pb-3 mbr-fonts-style display-5">
<a href="mailto:eventos@desarrollocorp.com">eventos@desarrollocorp.com</a> â¢ Tel: <a href="tel:+50587817000">+505 8781-7000</a> </p>
</div>
</div>
</section>
<section class="engine"><a href="https://mobirise.co/e">website builder software</a></section><script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smooth-scroll/smooth-scroll.js"></script>
<script src="assets/jarallax/jarallax.min.js"></script>
<script src="assets/theme/js/script.js"></script>
</body>
</html>