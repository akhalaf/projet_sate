<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "99485",
      cRay: "610a178ab84c1a2a",
      cHash: "93a8012f8b90fab",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYnJvYm90aWNzLWdhbWVzLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "738pusQp9VYJz1Zx+ugFJC+9Th/p5UumAPfEX9WIX6F46Ip8eGc8mjcMF8aev5v5+Aq2f+iK8sAD0HnEgy5KPjU2D6eP3rnFrGicxtwTUevQApYF5HWqa8VgmJrpv/DPQ+CIDKpWStUwetA5JlW48wOkOdL4Gi4t4jpZu+iI2xF4UQCEr2vj6nVu1BtqHzZ1UhsVD1JZkBjtvpUEP/apT5XMfzR19iWyEdclXKSCqNDFoNafsMVKUKyo6VJ9tHdTtExfniOWPYqHAbbqQNQP5cxZElHpdu48dIgCnltzG82RV3Cx8/Jhsr70hqvN3KxUub5q/REqVkd88XHw60rmOd0gRbP7Opkhj8EHUqVXDSVSQ7dOk7WFzECt7RlN3agneXh/CO3xOfb0+75TvSw+/ISqWbZHdZJzMSuuJokl/WyfiV7JZh4P86nfkmN4St8HJTzr8dqhb81028LdNTZK/SB+xLHt6VB2NdM/OUrzH94TF4GThaL5TYg/Gk3v1lBoy0pmCuBwVKW243Rfn05wmWuaZtEsGr3gQ0oiLZUPK2+ORYHtZli29MOAbblXSPlR6PuWyI5vYndT5SE2Mwl29juT5xEDkbDetAqXLy/1ENZ9xpYA4IYVy6U7bseztuoRWxkYJdgSvTQojV09Y8McaTYjdjon6YKIZWFPvfUiniKoZP44aO5BCl+Mn+BUjSTYr6YHIu9g/JCbbVLCsXSw4mjSrmJJQ4/yaiYOZtce85lmEfBPaz6I6QanxjDaLvfu",
        t: "MTYxMDQ4ODIxNC4xOTkwMDA=",
        m: "ZLBaGj5Y96BvlRLv6+4jGJTF1j7EgnkYFic0jylix4M=",
        i1: "V1FNgVSa0p+DNgvgMJGsog==",
        i2: "8Kc0YjQTwRYw/NwbUvUXlA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "ILiprY5UXUwsL/+4AMBC1IOWFgu4kNoSGfxpEI4H6nA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.brobotics-games.com</h2>
</div><!-- /.header -->
<div style="display: none;"><a href="https://rkrmpg.com/americanactinoid.php?faqid=252">table</a></div>
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=518dc02b18104ed3859920057c7f1b80db7ad8a9-1610488214-0-AfmqkTEBjsJvDdMDJTQrxLRjEsWUq8dETiia6wLeUqSZCnckiXpTnxyci9i8USehpNAYqXAC_X6137g2kpDa655iwnC7efik2sJPK2qwXzGMdTbRlDNh3PWATzIl3PGahiwrU0RzHxJc_QQpjupOOLzWxZLEpTA-AY6Xb2_U3lCIAC7jXXVGY-KUvfa6dV_OMPqClg-2hW1tpotzLiV5sPKKuLByc6UHDDiF4DytxQJgm2UWzv0G_TKAP_vQGg8goQZYzGFFvsuyQj4pSeiK2piSJ3fxuullyQ2YpJMsdgGElDelwYYNMPb0h66bidnKTC8JoZgVGY8CjUiwFRx9QMuAv_B9skdio50idch0WlPYDGB4wnuR17ZFYjdvYAbL7ZUky5eVMBUgrWZ2oHDfA7I62HecGJHRhX0G7E-FLxqoIDlYhuWFq6NyUlP2-mi5hNKttvGBcR49CJQtAecbCl9ofe1ODR_BHeBuv3RwBGoIgyJiLJq-5___hy3JmkC8uLcGlgWzndNLysI8yHpgEEU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="ab97d17a21701007be435a0870cec2115e3b4974-1610488214-0-Af0+buoqK6+qWgOXnFM4PBOzyXPg+gEKjJNpiRmxiZhKT+SJgTWmC0MnYAw092NwHywxO2SoIPT+t12cUEDPUyrBwN5SFUUAYR57zFsLQBxPq2kPEVXaGsiaNx06vuKDDcm0JWhX6ku4KlcRgZg9ZacOA8Xof7mkOhMJXLKjk2yGCJwg/sebxvqHlHVjUc7TtBQu/tNKWTHmz3aaRQRlv8qiEAwcz+/xWujosm+gMZcZXZQhRgXRjP+bhJiXdfe+eSk4+bI02uC8E4kK+M/ugKIPwXlkZkQqcBsUoBISSqb3VpLahTfJmlVpEFCzy/tTgLeqBA14dQMM4b8vFUXHQIp2MczzjteN7L5MsNb2gb/Ay/uHONLjd6MPGudsYllI/w2AIFIEOjLLmIrJWTldwMR4lI7Jdzq8j3jnB8o10fq5hPUsJGKEV6Rsjxpo8G+T/sOq+u1XXfphKl/6HQ/dE/vUJ+D1aB/fELC2aieCZnGSY2u409eHNBSv3O5Z/BhuC+5GrA4KTZqjN7PirdJV+SOD9dfZ2dUKxrysH1p1CH189GM7dTFPGUYNt4eUG2hvK7hO5VBItlOE2fbfVM+O+y6eyA591zFVWGgELbzZhx72jaEoQmCp5+S4hNI2K9T19yVZa1E9o2NpTnvY26BovhlpZ+s1bQ615gJtXAw2Qg6bVEtWUBbMi94Vqr5AcOyNDAumg9uUe72vmJxKnzO60Z14OpVEmgZQelikbBRaHqvKI/+da1JfMCZ3P8N1VGdQXJv6Ps7Eu7e+O2+T1vJNHH32qa0NO5JsEzji9ACQtMgaPqsyMe5qOU64a7/1ktGltQqMR3fE5zVo7XCLL5l/ueGjlChtp3G/L5SwQuqxqwtZL5imWmhqfc17i8e71O7knzLQciPZqkLvCZMaz4WoTEBnaVqPX6tSkGFPfi65amqRJ1jgL98UgyQYGupLj7SWtD44EQPFh6RrMFzSe5oXmUHTJqRGhQNA5T8klkLTDICiM8v6cLfDZQJD75zpkYWs5e+D2/CaZK+Yfn5WrBaibeCe4ZYqetyiFR13XFplNVU1spmDjX377oinOoRG3/E0HK10VMyDWlBKi/3H/m5jCCt8tvZ0HNnvD2ujD4fVVxuLwXdixklnZIcSn5xipkBIxL7GECznFFRzPEf3cn/kCy41Zo4VRVQ5xTkBEFIdq48vp9ygfRSKf6ji7sXpVkbUwypANCrnMugtuuNR2PQyiBCOu+8Dm0S/fayoqdM+YteLsZ6qNcr23tYeEBExM3+4G9y9x5iGIsLx6CEl2j6lsVAy927b71fkavAWJGlQe7qfflZdjP1j7Fo5+5SY0cHMDQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="1f12ab08f9d61c897f8112474ce61cde"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610a178ab84c1a2a')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610a178ab84c1a2a</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
