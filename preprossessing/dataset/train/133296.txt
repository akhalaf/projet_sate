<!DOCTYPE html>
<html class="no-js no-svg" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<link href="https://apiaryfund.com/wp-content/uploads/2018/05/APIARY_FAVICON.png" rel="shortcut icon" type="image/x-icon"/> <link href="http://gmpg.org/xfn/11" rel="profile"/>
<script async="" src="https://f1a9c731519348f1bd62d71aeefe28ac.js.ubembed.com"></script>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Home - Apiary Fund</title>
<!-- HubSpot WordPress Plugin v7.5.5: embed JS disabled as a portalId has not yet been configured -->
<!-- This site is optimized with the Yoast SEO plugin v7.1 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="The primary purpose of the Apiary Fund is to help people learn how to invest successfully by joining in the active management of our investment fund. Apiary defines success as the ability to generate consistent profits, limit risk, and manage the emotions of investing. It is our goal to increase the number of successful traders and to create a friendly environment where the traders support the community and the community supports the traders." name="description"/>
<meta content="noindex,follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Home - Apiary Fund" property="og:title"/>
<meta content="The primary purpose of the Apiary Fund is to help people learn how to invest successfully by joining in the active management of our investment fund. Apiary defines success as the ability to generate consistent profits, limit risk, and manage the emotions of investing. It is our goal to increase the number of successful traders and to create a friendly environment where the traders support the community and the community supports the traders." property="og:description"/>
<meta content="https://apiaryfund.com/" property="og:url"/>
<meta content="Apiary Fund" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="The primary purpose of the Apiary Fund is to help people learn how to invest successfully by joining in the active management of our investment fund. Apiary defines success as the ability to generate consistent profits, limit risk, and manage the emotions of investing. It is our goal to increase the number of successful traders and to create a friendly environment where the traders support the community and the community supports the traders." name="twitter:description"/>
<meta content="Home - Apiary Fund" name="twitter:title"/>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/apiaryfund.com\/","name":"Apiary Fund","potentialAction":{"@type":"SearchAction","target":"https:\/\/apiaryfund.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://apiaryfund.com/feed/" rel="alternate" title="Apiary Fund » Feed" type="application/rss+xml"/>
<link href="https://apiaryfund.com/comments/feed/" rel="alternate" title="Apiary Fund » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/apiaryfund.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.5"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://apiaryfund.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/includes/ext/infusionsoft_infusionbar/css/style.css?ver=4.9.5" id="infusion-front-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" id="inf_infusionsoft-open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/css/style.css?ver=1.0.0" id="inf_infusionsoft-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/plugins/sitemap/css/page-list.css?ver=4.3" id="page-list-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/plugins/wordpress-popular-posts/public/css/wpp.css?ver=4.0.13" id="wordpress-popular-posts-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/style.css?ver=4.9.5" id="twentyseventeen-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentyseventeen-ie8-css'  href='https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/css/ie8.css?ver=1.0' type='text/css' media='all' />
<![endif]-->
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/css/responsive.css?ver=4.9.5" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/css/font-awesome.min.css?ver=4.9.5" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/fonts/fonts.css?ver=1.0" id="fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/css/slick.css?ver=1.0" id="slick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/fancybox/jquery.fancybox.css?ver=1.0" id="fancybox-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://apiaryfund.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/js/html5.js?ver=3.7.3'></script>
<![endif]-->
<link href="https://apiaryfund.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://apiaryfund.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://apiaryfund.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.5" name="generator"/>
<link href="https://apiaryfund.com/" rel="shortlink"/>
<link href="https://apiaryfund.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fapiaryfund.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://apiaryfund.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fapiaryfund.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script src="https://wq377.infusionsoft.com/app/webTracking/getTrackingCode"></script> <meta content="wordpress-plugin" name="onesignal"/>
<link href="https://apiaryfund.com/wp-content/plugins/onesignal-free-web-push-notifications/sdk_files/manifest.json.php?gcm_sender_id=" rel="manifest"/>
<script async="" src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script> <script>

      window.OneSignal = window.OneSignal || [];

      OneSignal.push( function() {
        OneSignal.SERVICE_WORKER_UPDATER_PATH = "OneSignalSDKUpdaterWorker.js.php";
        OneSignal.SERVICE_WORKER_PATH = "OneSignalSDKWorker.js.php";
        OneSignal.SERVICE_WORKER_PARAM = { scope: '/' };

        OneSignal.setDefaultNotificationUrl("https://apiaryfund.com");
        var oneSignal_options = {};
        window._oneSignalInitOptions = oneSignal_options;

        oneSignal_options['wordpress'] = true;
oneSignal_options['appId'] = '8b6f477a-e962-4fa6-8e08-84da26191498';
oneSignal_options['autoRegister'] = true;
oneSignal_options['welcomeNotification'] = { };
oneSignal_options['welcomeNotification']['title'] = "";
oneSignal_options['welcomeNotification']['message'] = "";
oneSignal_options['path'] = "https://apiaryfund.com/wp-content/plugins/onesignal-free-web-push-notifications/sdk_files/";
oneSignal_options['safari_web_id'] = "web.onesignal.auto.3f550615-46c0-4fa5-9ee8-42953ece3d19";
oneSignal_options['persistNotification'] = true;
oneSignal_options['promptOptions'] = { };
              OneSignal.init(window._oneSignalInitOptions);
                    });

      function documentInitOneSignal() {
        var oneSignal_elements = document.getElementsByClassName("OneSignal-prompt");

        var oneSignalLinkClickHandler = function(event) { OneSignal.push(['registerForPushNotifications']); event.preventDefault(); };        for(var i = 0; i < oneSignal_elements.length; i++)
          oneSignal_elements[i].addEventListener('click', oneSignalLinkClickHandler, false);
      }

      if (document.readyState === 'complete') {
           documentInitOneSignal();
      }
      else {
           window.addEventListener("load", function(event){
               documentInitOneSignal();
          });
      }
    </script>
<!-- Easy Columns 2.1.1 by Pat Friedl http://www.patrickfriedl.com -->
<link href="https://apiaryfund.com/wp-content/plugins/easy-columns/css/easy-columns.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<script>
            document.addEventListener( 'wpcf7mailsent', function( event ) {
                location = 'https://apiaryfund.com/thank-you/';
            }, false );
        </script>
</head>
<body class="home page-template-default page page-id-5 inf_infusionsoft">
<div class="mobile-Hamburger">
<a class="slider-switch" href="javascript:;">
<span class="lines"></span>
</a>
</div>
<div class="overlay cf"></div>
<div class="mobile-nav-block">
<div class="mobile-nav-block-inner">
<div class="top-bar-menu">
<div class="logo">
<a class="logo" href="https://apiaryfund.com/" rel="home">
<img alt="Apiary Fund" src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/images/logo.png"/>
</a>
</div>
<div class="close-menu slider-switch view-maxi"><span class="lines"></span></div>
</div>
<div class="nav-content">
<div class="menu-main-navigation-container"><ul class="menu" id="menu-main-navigation"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-544" id="menu-item-544"><a href="https://apiaryfund.com/about/">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-539" id="menu-item-539"><a href="https://apiaryfund.com/faq/">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56" id="menu-item-56"><a href="https://apiaryfund.com/beeline-to-funding/">Beeline</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54" id="menu-item-54"><a href="https://apiaryfund.com/reviews/">Reviews</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://apiaryfund.com/events/">Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://apiaryfund.com/contact/">Contact</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-512" id="menu-item-512"><a href="https://apiaryfund.com/">Resources</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-514" id="menu-item-514"><a href="https://mytradingsignals.com/">My Trading Signals</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-511" id="menu-item-511"><a href="http://www.traderonthestreet.com/">Trader On The Street</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-519" id="menu-item-519"><a href="http://blog.apiaryfund.com/">Articles</a></li>
</ul>
</li>
</ul></div> </div>
<div class="mobile-bottom-links">
<a href="https://apiaryfund.com/user?destination=user" target="_blank">Trader Login</a>
<a href="https://start.apiaryfund.com/application/" target="_blank">Apply Here!</a>
</div>
</div>
</div>
<div id="wrapper">
<header>
<div class="container">
<div class="htop-right">
<span class="tradlink"><a href="https://apiaryfund.com/user?destination=user" target="_blank">Trader Login</a></span>
<a class="cta-btn" href="https://start.apiaryfund.com/application/" target="_blank">Apply Here!</a>
</div>
<a class="logo" href="https://apiaryfund.com/" rel="home">
<img alt="Apiary Fund" src="https://apiaryfund.com/wp-content/uploads/2018/03/logo.png"/>
</a>
<div class="header-right">
<nav aria-label="Top Menu" class="main-navigation" id="site-navigation" role="navigation">
<div class="menu-main-navigation-container"><ul class="menu" id="top-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-544"><a href="https://apiaryfund.com/about/">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-539"><a href="https://apiaryfund.com/faq/">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="https://apiaryfund.com/beeline-to-funding/">Beeline</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="https://apiaryfund.com/reviews/">Reviews</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="https://apiaryfund.com/events/">Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52"><a href="https://apiaryfund.com/contact/">Contact</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-512"><a href="https://apiaryfund.com/">Resources</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-514"><a href="https://mytradingsignals.com/">My Trading Signals</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-511"><a href="http://www.traderonthestreet.com/">Trader On The Street</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-519"><a href="http://blog.apiaryfund.com/">Articles</a></li>
</ul>
</li>
</ul></div></nav><!-- #site-navigation -->
</div>
</div>
</header><!-- #masthead -->
<div class="banner-block">
<div class="home-slider">
<div class="slide-items">
<img alt="" class="desk" src="https://apiaryfund.com/wp-content/uploads/2018/03/home-banner.jpg"/>
<img alt="" class="mob" src="https://apiaryfund.com/wp-content/uploads/2018/03/home-xs-banner.png"/>
<div class="banner-text container cf">
<div class="">
<h2>Learn to Trade Online<br/> and Share in the Profits!</h2> <p>We’ve helped over 1,600 <u>new</u> traders make as much as <u>$2,532</u><br/> or more per month… Will you be next?</p> <a class="cta-btn" href="#welcome">Apply Here</a>
</div>
</div>
</div>
</div>
<div class="banner-logos">
<div class="logo-slider">
<div class="logo-items">
<a href="https://www.facebook.com/pg/apiaryfund/reviews/?ref=page_internal" target="_blank"> <img alt="" src="https://apiaryfund.com/wp-content/uploads/2018/04/FACEBOOK-REVIEWS.png"/>
</a> </div>
</div>
</div>
</div>
<div class="welcome-block cf" id="welcome">
<div class="container cf">
<div class="welcol-left">
<h2>
<span>Welcome to the</span> 					Leading Trading Institute in the World				</h2>
<p>We train our students to become consistently profitable. Once we’ve taught you everything that a 2.5 trillion-dollar hedge fund manager knows and you pass our tests, we’ll put our money where our mouth is and GIVE YOU OUR MONEY to trade. From there on, you’ll keep up to 70% of all the profits you make and NEVER risk losing your own personal money on a trade.</p>
</div>
</div>
<div class="welcol-right" style="background:url(https://apiaryfund.com/wp-content/uploads/2018/03/block-image.jpg) right center no-repeat;">
<div class="shape-tringle"></div>
<div class="">
<a href="#services"><span class="number">3</span>
<h5>Here Are 3 Ways You Can Get Started at Apiary Fund</h5></a>
</div>
</div>
</div>
<div class="service-block" id="services">
<div class="container cf">
<ul class="slides">
<li>
<a class="match-height" href="https://start.apiaryfund.com/application/">
<img alt="" src="https://apiaryfund.com/wp-content/uploads/2018/04/watch-icon-1.png"/>
</a>
<h5>REGISTER FOR YOUR 1 HOUR<br/> "GET STARTED" INTRODUCTORY WEBINAR</h5>
<a class="cta-btn" href="https://start.apiaryfund.com/application/" target="_blank">Apply Here</a>
</li>
</ul>
</div>
</div>
<div class="review-block" style="background:url(https://apiaryfund.com/wp-content/uploads/2018/03/review-bg.jpg) left center no-repeat;">
<div class="container cf">
<div class="review-section">
<div class="review-slider">
<div class="review-items">
<h3><a href="https://apiaryfund.com/review/how-jim-davis-got-funded-with-the-apiary-fund/">How Jim Davis Got Funded with The Apiary Fund</a></h3>
<p>Jim Davis recently stepped into the first funded level of Beeline to Funding and I had the pleasure of hearing his story--his struggles, triumphs, strategy, and more. Jim is a salesman that took some interest in trading stocks and options in the late 90's but kept most of these aspirations on the shelf until about the Spring of 2016. He was looking for a way to supplement his present income...</p>
</div>
<div class="review-items">
<h3><a href="https://apiaryfund.com/review/len-pavia-his-experience-and-path-to-funding-with-the-apiary-fund/">Len Pavia – His Experience and Path to Funding with The Apiary Fund</a></h3>
<p>Have you ever wanted to pick the brain of someone who has made it through Beeline and is managing a funded account so you could get some tips and expectations for your journey to funding? If yes, then you're not alone. That's why I sat down with Len Pavia and peppered him with questions about the beginnings of his trading journey, his general experience, and what his path to funding...</p>
</div>
</div>
<a class="learn-more" href="https://apiaryfund.com/reviews/"><span>More Reviews</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
</div>
<div class="program-block">
<div class="container cf">
<img alt="" class="program-head" src="https://apiaryfund.com/wp-content/uploads/2018/03/welcomeimg.png"/>
<div class="heading-section">
<span>FEATURED TRADER DEVELOPMENT SYSTEM</span> <h3>The “Beeline to Funding”</h3> </div>
<div class="pcontent"><p>The <em>Beeline to Funding</em> is an innovative Trader Development System which takes you step-by-step from a beginner trader to a professional trader in as little as 1 hour a day. In this system, you’ll gain “experience points” by watching training videos and by placing trades, because practice makes profit. As you gain points and increase your skills, you’ll rise through the Beeline hives from Copper, to Bronze, to Silver, and finally Gold! <strong>Upon completion of the Beeline system, you’ll be given your very own Apiary Fund trading account so you can trade for profits with our money!</strong></p>
</div>
<div class="funding-block">
<h4>Inside the “Beeline to Funding” you’ll get:</h4>
<ul><li><i class="fa fa-check-circle"></i>The 12-Part “From Nobody to Expert Trader” Course</li><li><i class="fa fa-check-circle"></i>Your Own Simulated Trading Account</li><li><i class="fa fa-check-circle"></i>Exclusive Access to Our “Funded Trader’s Community Forum”</li><li><i class="fa fa-check-circle"></i>The Complete “Funded Trader Mastery” Training Library</li><li><i class="fa fa-check-circle"></i>100% Personal Access to Support via Website, Webinar &amp; Email</li><li><i class="fa fa-check-circle"></i>Hands-on trading experience with real money in real markets in real-time</li><ul>
</ul></ul></div>
<div class="bottom-block">
<h3>Try “Beeline to funding” Risk Free!</h3>
<a class="cta-btn" href="https://start.apiaryfund.com/application/" target="_blank">Apply For Free</a>
</div>
</div>
</div>
<div class="trial-block" style="background:url(https://apiaryfund.com/wp-content/uploads/2018/03/trialblock-bg.jpg) center center no-repeat;">
<div class="container cf">
<h3>Apply Today</h3>
<p>We’ve helped over 1,600 new traders make as much as $2,532 <br/>
or more per month… Will you be next?<br/>
</p>
<a class="cta-btn" href="https://start.apiaryfund.com/application/" target="_blank">Apply Here</a>
</div>
</div>
<div class="recent-block">
<div class="container cf">
<h3>RECENT BLOG POSTS</h3>
<div class="post-slider">
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/best-trading-platform/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/04/pltfrm-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/best-trading-platform/">What Is the Best Forex Trading Platform?</a></h4>
<a class="learn-more" href="https://apiaryfund.com/best-trading-platform/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/currencies-trading/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/04/currheader2-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/currencies-trading/">How to Trade Currencies</a></h4>
<a class="learn-more" href="https://apiaryfund.com/currencies-trading/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/trading-hindsight/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/04/trading-hindsight-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/trading-hindsight/">Trading Hindsight</a></h4>
<a class="learn-more" href="https://apiaryfund.com/trading-hindsight/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/trading-experience/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/04/trading-experience-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/trading-experience/">Trading Experience</a></h4>
<a class="learn-more" href="https://apiaryfund.com/trading-experience/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/fear-trading/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/fear-trading/">Fear &amp; Trading</a></h4>
<a class="learn-more" href="https://apiaryfund.com/fear-trading/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/price-pattern-trading/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/price-pattern-trading/">Price Pattern Trading</a></h4>
<a class="learn-more" href="https://apiaryfund.com/price-pattern-trading/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/5-ways-to-place-a-trade/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/5-ways-to-place-a-trade/">5 Ways to Place a Trade</a></h4>
<a class="learn-more" href="https://apiaryfund.com/5-ways-to-place-a-trade/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/the-aroon-indicator/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/the-aroon-indicator/">The Aroon Indicator</a></h4>
<a class="learn-more" href="https://apiaryfund.com/the-aroon-indicator/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/the-failed-beeline-meeting/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/the-failed-beeline-meeting/">The Failed Beeline Meeting</a></h4>
<a class="learn-more" href="https://apiaryfund.com/the-failed-beeline-meeting/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/trader-spotlight-josh/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/trader-spotlight-josh/">Trader Spotlight | Josh</a></h4>
<a class="learn-more" href="https://apiaryfund.com/trader-spotlight-josh/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/the-sma/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/the-sma/">SMA</a></h4>
<a class="learn-more" href="https://apiaryfund.com/the-sma/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
<div class="slide-items">
<a class="match-height" href="https://apiaryfund.com/support-qa/"><img alt="" class="attachment-post-thumb size-post-thumb wp-post-image" height="380" sizes="100vw" src="https://apiaryfund.com/wp-content/uploads/2019/01/Screen-Shot-2019-01-07-at-10.20.19-AM-470x380.png" width="470"/></a>
<div class="post-detail">
<span class="date"></span>
<h4><a href="https://apiaryfund.com/support-qa/">Support Q &amp; A</a></h4>
<a class="learn-more" href="https://apiaryfund.com/support-qa/"><span>Learn More</span> <i class="fa fa-angle-right"></i></a>
</div>
</div>
</div>
</div>
</div>
</div><!-- #wrapper -->
<footer class="site-footer">
<div class="container cf">
<div class="footer-top cf">
<aside class="footer-logo-area">
<a class="footer-logo" href="https://apiaryfund.com/" rel="home">
<img alt="Apiary Fund" src="https://apiaryfund.com/wp-content/uploads/2018/03/footer-logo.png"/>
</a>
<span class="copyright-txt">© 2018 All Rights Reserved</span>
</aside>
<aside class="footer-menu-area">
<div class="menu-footer-navigation-container"><ul class="menu" id="footer-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72" id="menu-item-72"><a href="https://apiaryfund.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a href="https://apiaryfund.com/reviews/">Reviews</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74" id="menu-item-74"><a href="https://apiaryfund.com/terms-of-service/">Terms of Service</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75" id="menu-item-75"><a href="https://apiaryfund.com/beeline-to-funding/">Beeline</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://apiaryfund.com/events/">Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77" id="menu-item-77"><a href="https://apiaryfund.com/disclaimer/">Disclaimer</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78" id="menu-item-78"><a href="https://apiaryfund.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79" id="menu-item-79"><a href="https://apiaryfund.com/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80" id="menu-item-80"><a href="https://apiaryfund.com/privacy-policy/">Privacy Policy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-410" id="menu-item-410"><a href="https://apiaryfund.com/beeline-to-funding/">Products</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-276" id="menu-item-276"><a href="https://apiaryfund.com/category/trader-on-the-street/">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-505" id="menu-item-505"><a href="https://apiaryfund.com/copyright-policy/">Copyright Policy</a></li>
</ul></div>
</aside>
<aside class="footer-address-area">
<h5><a href="tel:1-801-701-1650">1-801-701-1650</a></h5>
<p><strong>Apiary Fund</strong>
383 W Lakeview Rd<br/>
Lindon, UT 84042</p>
<ul class="social-links">
<li><a href="https://www.facebook.com/apiaryfund" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="https://twitter.com/apiaryfund" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="http://instagram.com/apiaryfundtraders" target="_blank"><i class="fa fa-instagram"></i></a></li>
<li><a href="https://www.youtube.com/apiaryfund" target="_blank"><i class="fa fa-youtube"></i></a></li>
</ul>
</aside>
</div>
<div class="site-info">
<p><strong>FOREX DISCLAIMER<br/>
</strong>Before deciding to participate in the Forex market, you should carefully consider your investment objectives, level of experience and risk tolerance. Most importantly, do not invest money you cannot afford to lose. There is considerable exposure to risk in any off-exchange foreign exchange transaction, including, but not limited to, leverage, creditworthiness, limited regulatory protection and market volatility that may substantially affect the price, or liquidity of a currency or currency pair. The leveraged nature of Forex trading means that any market movement will have an equally proportional effect on your deposited funds. This may work against you as well as for you. The possibility exists that you could sustain a total loss of initial margin funds and be required to deposit additional funds to maintain your position. If you fail to meet any margin requirement, your position may be liquidated and you will be responsible for any resulting losses.</p>
<p><strong>SITE DISCLAIMER</strong><br/>
Before deciding to participate in the Forex market, you should carefully consider your investment objectives, level of experience and risk tolerance. Most importantly, do not invest money you cannot afford to lose. There is considerable exposure to risk in any off-exchange foreign exchange transaction, including, but not limited to, leverage, creditworthiness, limited regulatory protection and market volatility that may substantially affect the price, or liquidity of a currency or currency pair. The leveraged nature of Forex trading means that any market movement will have an equally proportional effect on your deposited funds. This may work against you as well as for you. The possibility exists that you could sustain a total loss of initial margin funds and be required to deposit additional funds to maintain your position. If you fail to meet any margin requirement, your position may be liquidated and you will be responsible for any resulting losses.</p>
<p><strong>SOFTWARE DISCLAIMER</strong><br/>
There are risks associated with utilizing an Internet-based trading system including, but not limited to, the failure of hardware, software, and Internet connection. Apiary is not responsible for communication failures or delays when trading via the Internet.</p>
</div><!-- .site-info -->
</div><!-- .wrap -->
</footer><!-- #colophon -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" id="et-gf-open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/apiaryfund.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script src="https://apiaryfund.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var infusion = {"admin_bar":""};
/* ]]> */
</script>
<script src="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/includes/ext/infusionsoft_infusionbar/js/infusion.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/js/jquery.uniform.min.js?ver=1.0.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var infusionsoftSettings = {"ajaxurl":"https:\/\/apiaryfund.com\/wp-admin\/admin-ajax.php","pageurl":"https:\/\/apiaryfund.com\/","stats_nonce":"14eabd1b8c","subscribe_nonce":"0ed5031024"};
/* ]]> */
</script>
<script src="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/js/custom.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/plugins/infusionsoft-official-opt-in-forms/js/idle-timer.min.js?ver=1.0.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/js/global.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/js/slick.min.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/js/jquery.matchHeight.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/fancybox/jquery.fancybox.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-content/themes/Apiary-fund/assets/js/script.js?ver=1.0" type="text/javascript"></script>
<script src="https://apiaryfund.com/wp-includes/js/wp-embed.min.js?ver=4.9.5" type="text/javascript"></script>
</body>
</html>
