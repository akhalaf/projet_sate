<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" prefix="og: https://ogp.me/ns#" xmlns="https://www.w3.org/1999/xhtml">
<head profile="https://gmpg.org/xfn/11">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://webframez.com/wp-content/themes/webframez/source/jquery.fancybox.css?v=2.1.5" media="screen" rel="stylesheet" type="text/css"/>
<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
	</style>
<meta content="IE=11,IE=10,IE=9,IE=8,IE=7" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
<meta content="index, follow" name="robots"/>
<meta content="h-QGcTL4yd9Mi9srMjZe4QFpUPQxXxbN6GAFknMrhCk" name="google-site-verification"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46030193-3', 'auto');
  ga('send', 'pageview');

</script>
<link href="https://webframez.com/wp-content/themes/webframez/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/css/styles.css" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/css/animations_controller.css" rel="stylesheet" type="text/css"/>
<!-- Slider CSS File -->
<link href="https://webframez.com/wp-content/themes/webframez/css/jquery.mSimpleSlidebox.css" rel="stylesheet" type="text/css"/>
<!-- Slider CSS File -->
<!-- Custom Scrollbar CSS File -->
<link href="https://webframez.com/wp-content/themes/webframez/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<!-- Custom Scrollbar CSS File -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>

      <script type="text/javascript"  src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script type="text/javascript"  src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	
      <![endif]-->
<!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css"  href="https://webframez.com/wp-content/themes/webframez/css/ie7.css">
    <link rel="stylesheet" type="text/css"  href="https://webframez.com/wp-content/themes/webframez/css/ie.css">

    <![endif]-->
<!--[if lt IE 10]>
 <link rel="stylesheet" type="text/css"  href="https://webframez.com/wp-content/themes/webframez/css/styles.css">
<style type="text/css">
.testimonials_info { visibility:visible !important; }
.testimonials_content { visibility:visible !important; }
</style>
<![endif]-->
<!-- External Font Files -->
<link href="https://fonts.googleapis.com/css?family=Oswald:400,300" rel="stylesheet" type="text/css"/>
<link href="" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/slider/engine1/style.css" rel="stylesheet" type="text/css"/>
<!-- All in One SEO 4.0.11 -->
<title></title>
<meta content="noindex" name="robots"/>
<link href="https://www.webframez.com/knby545/" rel="canonical"/>
<meta content="nositelinkssearchbox" name="google"/>
<script class="aioseo-schema" type="application/ld+json">
			{"@context":"https:\/\/schema.org","@graph":[{"@type":"WebSite","@id":"https:\/\/www.webframez.com\/#website","url":"https:\/\/www.webframez.com\/","description":"we frame your ideas","publisher":{"@id":"https:\/\/www.webframez.com\/#organization"}},{"@type":"Organization","@id":"https:\/\/www.webframez.com\/#organization","url":"https:\/\/www.webframez.com\/"},{"@type":"BreadcrumbList","@id":"https:\/\/www.webframez.com\/knby545\/#breadcrumblist","itemListElement":[{"@type":"ListItem","@id":"https:\/\/www.webframez.com\/#listItem","position":1,"item":{"@type":"WebPage","@id":"https:\/\/www.webframez.com\/#item","name":"Home","description":"Web framez offers custom website design, web development, apps development & SEO services. Our solutions are simply par excellence.","url":"https:\/\/www.webframez.com\/"},"nextItem":"https:\/\/www.webframez.com\/knby545\/#listItem"},{"@type":"ListItem","@id":"https:\/\/www.webframez.com\/knby545\/#listItem","position":2,"item":{"@id":"https:\/\/www.webframez.com\/knby545\/#item","url":"https:\/\/www.webframez.com\/knby545\/"},"previousItem":"https:\/\/www.webframez.com\/#listItem"}]}]}
		</script>
<!-- All in One SEO -->
<link href="//webframez.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/webframez.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/css/style.css?ver=5.6" id="testimonial_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-admin/css/color-picker.min.css?ver=5.6" id="wp-color-picker-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/css/owl.carousel.css?ver=5.6" id="owl.carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/css/owl.theme.css?ver=5.6" id="owl.theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/ParaAdmin/css/ParaAdmin.css?ver=5.6" id="ParaAdmin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/themes/flat/style.css?ver=5.6" id="testimonial-style-flat-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/testimonial_data/themes/rounded/style.css?ver=5.6" id="testimonial-style-rounded-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70" id="wp-pagenavi-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&amp;subset=latin%2Clatin-ext" id="twentythirteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/fonts/genericons.css?ver=2.09" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://webframez.com/wp-content/themes/webframez/style.css?ver=2013-07-18" id="twentythirteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentythirteen-ie-css'  href='https://webframez.com/wp-content/themes/webframez/css/ie.css?ver=2013-07-18' type='text/css' media='all' />
<![endif]-->
<script id="jquery-core-js" src="https://webframez.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://webframez.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="testimonial_js-js" src="https://webframez.com/wp-content/plugins/testimonial_data/js/scripts.js?ver=5.6" type="text/javascript"></script>
<script id="owl.carousel-js" src="https://webframez.com/wp-content/plugins/testimonial_data/js/owl.carousel.js?ver=5.6" type="text/javascript"></script>
<script id="ParaAdmin-js" src="https://webframez.com/wp-content/plugins/testimonial_data/ParaAdmin/js/ParaAdmin.js?ver=5.6" type="text/javascript"></script>
<link href="https://www.webframez.com/wp-json/" rel="https://api.w.org/"/><link href="https://webframez.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://webframez.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://www.webframez.com/wp-content/uploads/2015/05/favicon.png" rel="shortcut icon"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//www.webframez.com/?wordfence_lh=1&hid=4B750031874531758986A83AA798D24B');
</script>
<style id="twentythirteen-header-css" type="text/css">
	
		.site-header {
			background: url(https://webframez.com/wp-content/themes/webframez/images/headers/circle.png) no-repeat scroll top;
			background-size: 1600px auto;
		}
	
	</style>
<style id="wp-custom-css" type="text/css">
			.services_panels h1{
	font-size: 18px;
}		</style>
<link href="https://www.webframez.com/feed/" rel="alternate" title=" latest posts" type="application/rss+xml"/>
<link href="https://www.webframez.com/comments/feed/" rel="alternate" title=" latest comments" type="application/rss+xml"/>
<link href="https://webframez.com/xmlrpc.php" rel="pingback"/>
<script async="async" src="https://webframez.com/wp-content/themes/webframez/slider/engine1/jquery.js" type="text/javascript"></script>
</head>
<body>
<div class="hfeed" id="wrapper">
<!-- Header Area Starts -->
<div class="header_wrapper">
<div class="container large_container">
<div class="row remove_margin">
<div class="col-md-4 remove_padding">
<a class="home-link" href="https://www.webframez.com/" rel="home" title="">
<!--<h1 class="site-title"></h1>-->
<img alt="Webframez - We frame your ideas" class="img-responsive" src="https://webframez.com/wp-content/themes/webframez/images/logo.png" title="Webframez - We frame your ideas"/>
</a>
</div>
<div class="col-md-8 remove_padding">
<div class="col-md-4 col-md-offset-8 remove_padding">
<a class="quote_btn" href="https://webframez.com/request-a-quote">
<span class="btn_txt anim_tran_four">Request a quote</span>
<span class="btn_icon anim_tran_four"></span>
</a>
</div>
<div class="col-md-11 col-md-offset-1 remove_padding">
<div class="info_wrapper">
<div class="header_info call_icon">
<span>Call Us :</span> +91-98880-01269

                        </div>
<div class="header_info">
<span>Mail Us :</span> <a class="dark_links" href="mailto:webframez@gmail.com">webframez@gmail.com</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Header Area Ends -->
<!-- //////////////////// -->
<!-- Menu Start Here -->
<div class="nav_holder">
<div class="container">
<div class="menu_trigger">Navigations<span class="pure_round anim_tran_four"></span></div>
<ul class="animate_menu" id="main_menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-91" id="menu-item-91"><a href="https://www.webframez.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90" id="menu-item-90"><a href="https://www.webframez.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-402" id="menu-item-402"><a href="#">OUR SERVICES</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-433" id="menu-item-433"><a href="#">Website Solutions</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-400" id="menu-item-400"><a href="https://www.webframez.com/website-designing/">Web Designing</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-460" id="menu-item-460"><a href="https://www.webframez.com/website-development/">Website Development</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-458" id="menu-item-458"><a href="https://www.webframez.com/e-commerce-solutions/">E-Commerce Solutions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-457" id="menu-item-457"><a href="https://www.webframez.com/content-management-system/">Content Management System</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-909" id="menu-item-909"><a href="#">App Development</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-456" id="menu-item-456"><a href="https://www.webframez.com/iphone-applications-development/">iPhone Apps Development</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-455" id="menu-item-455"><a href="https://www.webframez.com/android-applications-development/">Android Apps Development</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-459" id="menu-item-459"><a href="https://www.webframez.com/website-hosting/">Website Hosting</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-403" id="menu-item-403"><a href="#">Internet Marketing</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-462" id="menu-item-462"><a href="https://www.webframez.com/seo-services/">SEO Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-461" id="menu-item-461"><a href="https://www.webframez.com/social-media-optimization/">Social Media Optimization</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-463" id="menu-item-463"><a href="#">Print Media</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-464" id="menu-item-464"><a href="https://www.webframez.com/logo-designing/">Logo Designing</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-24" id="menu-item-24"><a href="https://www.webframez.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87" id="menu-item-87"><a href="https://www.webframez.com/portfolio/">Portfolio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1136" id="menu-item-1136"><a href="https://www.webframez.com/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88" id="menu-item-88"><a href="https://www.webframez.com/contact-us/">Get In Touch</a></li>
</ul>
</div>
</div>
<!-- Menu Ends Here -->
<!-- //////////////////// -->
<div class="banner_holder anim_tran_four">
<div class="container large_container">
<div class="col-md-8">
<h1 class="inner_orange_title anim_tran_four"></h1>
<h2 class="inner_white_title anim_tran_four"></h2>
</div>
<div class="col-md-4">
<div class="banner_icons">
<div class="circle_border white_circle_border">
<div class="circle white_circle blog_icon"><img src="https://webframez.com/wp-content/themes/webframez/images/blog_icon.png"/></div>
</div>
</div>
</div>
</div>
</div>
<!-- Add jQuery library -->
<style type="text/css">
	
#pnf {
    border-top: 1px solid #E7E7E7;
    padding: 65px 0 65px 150px;
}
.fl {
    float: left;
}

#pnf .pnf_content {
    float: left;
    font-size: 20px;
    line-height: 25px;
    padding: 90px 0 0 30px;
}
#pnf .pnf_heading {
    font-size: 30px;
    font-weight: bold;
}

.spacer_8 {
    padding: 8px;
}

.align_c {
    text-align: center;
}

a:link, a:visited, a:focus {
    outline: 0 none;
    text-decoration: none;
}
</style>
<div class="inner_content_wrapper">
<div id="pnf">
<img alt="Sorry, the page you requested was not found." class="fl" height="290" src="https://webframez.com/wp-content/themes/webframez/images/page-not-found.gif" title="Sorry, the page you requested was not found." width="189"/>
<div class="pnf_content">
<span class="pnf_heading">Sorry, the page you requested was not found.</span>
<div class="spacer_8"></div>
<div class="align_c">
			Please check the URL. If you are having trouble<br/>
			locating a page, try visiting the <a class="light_blue" href="https://www.webframez.com">home page</a>
<!--<div class="bor_dash">
				You will be automatically redirected in <span id="timercnt" class="font_28 dark_gray" style="line-height:28px"></span> seconds.
			</div>-->
</div>
</div>
<div class="clear"></div>
</div>
</div>
<!-- Footer Area Starts Here -->
<div class="footer_holder">
<div class="container large_container">
<div class="col-md-6 links_section">
<h3 class="footer_titles">Services We Provide</h3>
<div class="menu-footer-container"><ul class="dark_links_anime"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-331" id="menu-item-331"><a href="https://www.webframez.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-330" id="menu-item-330"><a href="https://www.webframez.com/about-us/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-323" id="menu-item-323"><a href="https://www.webframez.com/website-designing/">Website Designing</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1194" id="menu-item-1194"><a href="https://www.webframez.com/website-development/">Website Development</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-325" id="menu-item-325"><a href="https://www.webframez.com/website-hosting/">Website Hosting</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-327" id="menu-item-327"><a href="https://www.webframez.com/portfolio/">Portfolio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1135" id="menu-item-1135"><a href="https://www.webframez.com/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-328" id="menu-item-328"><a href="https://www.webframez.com/contact-us/">Contact Us</a></li>
</ul></div>
</div>
<div class="col-md-6 contact_info">
<h3 class="footer_titles">Get In Touch With Us</h3>
<img alt="" src="https://webframez.com/wp-content/themes/webframez/images/map_icon.png" title=""/>
<ul class="footer_ul"><li class="widget-container widget_text" id="text-9"> <div class="textwidget"><p>Ludhiana ::  # Madhopuri-7, Ludhiana -141008, Punjab, INDIA
</p><p>Mohali :: # Phase 5, Industrail Area, Mohali - 160059 Punjab, INDIA<br/>
                Call Us : 01724-680779,  +91-98880-01269, <br/>
                Mail Us : <a class="dark_links" href="mailto:webframez@gmail.com">webframez@gmail.com</a></p>
<br/>
<a class="orange_btn_dark rounded_six anim_tran_four" href="http://www.webframez.com/contact-us/">Contact Us</a></div>
</li></ul>
</div>
</div>
</div>
<!-- Footer Area Ends Here -->
<!-- //////////////////// -->
<!-- Social Bar Area Starts Here -->
<div class="social_bar_holder">
<div class="social_icons widget_text" id="text-11"><h4>WE ARE ALSO HERE</h4> <div class="textwidget"> <ul>
<li><a class="icons_anime" href="https://www.facebook.com/webframez" rel="noopener" target="_blank"></a></li>
<li><a class="icons_anime twitter_icon" href="https://twitter.com/webframez" rel="noopener" target="_blank"></a></li>
<li><a class="icons_anime linkedIn_icon" href="http://in.linkedin.com/pub/pankaj-lehar/20/770/603" rel="noopener" target="_blank"></a></li>
</ul>
</div></div>
</div>
</div>
<!-- Social Bar Area Ends Here -->
<!-- //////////////////////////////////////////////////////////// -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://webframez.com/wp-content/themes/webframez/js1/jquery_code_min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://webframez.com/wp-content/themes/webframez/js1/bootstrap.min.js"></script>
<!-- Main Slider Scripts Starts Here -->
<link href="https://webframez.com/wp-content/themes/webframez/css/responsive-slider-paralax.css" rel="stylesheet" type="text/css"/>
<script src="https://webframez.com/wp-content/themes/webframez/js1/jquery.event.move.js"></script>
<script src="https://webframez.com/wp-content/themes/webframez/js1/responsive-slider.js"></script>
<!-- Main Slider Scripts End Here -->
<!-- Crousal Scripts Starts -->
<link href="https://webframez.com/wp-content/themes/webframez/css/crousal_styles.css" rel="stylesheet" type="text/css"/>
<script src="https://webframez.com/wp-content/themes/webframez/js1/google_ajax_min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="/js1/jquery.mousewheel.js"></script>-->
<script src="https://webframez.com/wp-content/themes/webframez/js1/jquery.contentcarousel.js" type="text/javascript"></script>
<script type="text/javascript">

	$('#ca-container').contentcarousel();

</script>
<script type="text/javascript">
	$(document).ready(function() {
	var pos = document.getElementById('apply_pos').value;
	//var pos = document.getElementById('vfb-44').value;

$('#vfb-44 option[value="' + pos + '"]').prop('selected', true);
	//alert(pos);
	//$('#vfb-44').val(pos);
	});
	  </script>
<!-- Crousal Scripts Ends -->
<!-- Dropdown Menu Function -->
<script src="https://webframez.com/wp-content/themes/webframez/js1/jquery.easing.1.3.js" type="text/javascript"></script>
<script type="text/javascript">

$(function(){

	//var jquery=$.noConflict();

$("li.open .dropdown-menu > li > a.dropdown-toggle").live("click",function(e){

		

		var current=$(this).next();

		var grandparent=$(this).parent().parent();

		if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))

			$(this).toggleClass('right-caret left-caret');

		grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');

		grandparent.find(".sub-menu:visible").not(current).hide();

		current.toggle();

		e.stopPropagation();

	});

	$(".dropdown-menu > li > a:not(.trigger)").on("click",function(){

		var root=$(this).closest('.dropdown');

		root.find('.left-caret').toggleClass('right-caret left-caret');

		root.find('.sub-menu:visible').hide();

	});

});

</script>
<script src="https://webframez.com/wp-content/themes/webframez/js/isotope/jquery-1.7.1.min.js"></script>
<script src="https://webframez.com/wp-content/themes/webframez/js/isotope/jquery.isotope.min.js"></script>
<!--------------------------Testimonial---Script------------->
<script src="https://webframez.com/wp-content/themes/webframez/js/jquery.innerfade.js" type="text/javascript"></script>
<script type="text/javascript">

	 			//var j=$.noConflict(); 

		   

					$('.testimonials_fade').innerfade({

						speed: 1500,

						timeout: 10000,

						type: 'random_start',

						containerheight: '1.5em'

					});

			

  	</script>
<!--------------------------Testimonial---Script End------------->
<!-- Lazy Load Plugin -->
<script src="https://webframez.com/wp-content/themes/webframez/js/isotope/jquery.lazyload.js" type="text/javascript"></script>
<!-- Lazy Load Plugin -->
<script>

    $(function(){

    

      var $container = $('#container');

    

      $container.isotope({

        masonry: {

          columnWidth: 20

        },

        

      });

    

      

      var $optionSets = $('#options .option-set'),

          $optionLinks = $optionSets.find('a');



      $optionLinks.click(function(){

        var $this = $(this);

        // don't proceed if already selected

        if ( $this.hasClass('selected') ) {

          return false;

        }

        var $optionSet = $this.parents('.option-set');

        $optionSet.find('.selected').removeClass('selected');

        $this.addClass('selected');

  

        // make option object dynamically, i.e. { filter: '.my-filter-class' }

        var options = {},

            key = $optionSet.attr('data-option-key'),

            value = $this.attr('data-option-value');

        // parse 'false' as false boolean

        value = value === 'false' ? false : value;

        options[ key ] = value;

        if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {

          // changes in layout modes need extra logic

          changeLayoutMode( $this, options )

        } else {

          // otherwise, apply new options

          $container.isotope( options );

        }

        

        return false;

      });





    

      // Sites using Isotope markup

      var $sites = $('#sites'),

          $sitesTitle = $('<h2 class="loading"><img src="https://webframez.com/wp-content/themes/webframez/images/qkKy8.gif" />Loading sites using Isotope</h2>'),

          $sitesList = $('<ul class="clearfix"></ul>');

        

      $sites.append( $sitesTitle ).append( $sitesList );



      $sitesList.isotope({

        layoutMode: 'cellsByRow',

        cellsByRow: {

          columnWidth: 290,

          rowHeight: 400

        }

      });

    

      var ajaxError = function(){

        $sitesTitle.removeClass('loading').addClass('error')

          .text('Could not load sites using Isotope :(');

      };

   

			var items = [],

            item, datum;



          var $items = $( items.join('') )

            .addClass('example');

      

          $items.imagesLoaded(function(){

            $sitesTitle.removeClass('loading').text('Sites using Isotope');

            $container.append( $items );

            $items.each(function(){

              var $this = $(this),

                  itemHeight = Math.ceil( $this.height() / 60 ) * 60 - 10;

              $this.height( itemHeight );

            });

            $container.isotope( 'insert', $items );

          });

		

    });

  </script>
<script type="text/javascript">

	$(document).ready(function() {

		$('.menu_trigger').click(function(){

			$('#main_menu').slideToggle('slow');
	
			$('#main_menu').addClass('level1');

			$("a[href$='#']").click(function() {
			

				var thiss = $(this).parent('.menu-item-has-children');

				$(thiss).children('ul').slideToggle('slow');

				$(thiss).children('ul').children('li.menu-item-has-children').addClass('level3');
			return false;

		});

		});

	
		<!--Intro Video Popup Starts Here-->


		$('.videoimage').click(function(){
			$('.duplicate_vimage').css("display","none");			$(this).addClass('videomain');			$('.videomain').css("display","block");						var video_src = $('.promo_video').attr('src');			$('.promo_video').attr('src', video_src+'?rel=0&autoplay=1');			})


<!--Intro Video Popup Ends Here-->
	
	
		



		jQuery("img.lazy").lazyload({

			 effect : "fadeIn",

			 failurelimit: 100

			});
	});

</script>
<!-- Add fancyBox main JS and CSS files -->
<script src="https://webframez.com/wp-content/themes/webframez/source/jquery.fancybox.js?v=2.1.5" type="text/javascript"></script>
<script>

	jQuery(document).ready(function() {
	//alert("hello");
			/*
			 *  Simple image gallery. Uses default settings
			 */
		jQuery('.fancy_box_gallery').each(function(){
			var get_id = jQuery(this).children('div').children('a:first').attr('id');
			//console.log(get_id);
			jQuery('.'+get_id).fancybox();
		}); 
			
		});
		</script>
<!-- Dropdown Menu Function -->
<!-- #main -->
<div id="footer">
<div id="colophon">
<div id="site-info">
</div><!-- #site-info -->
</div><!-- #colophon -->
</div><!-- #footer -->
<!-- #wrapper -->
<div id="aioseo-admin"></div><script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.webframez.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://webframez.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="twentythirteen-script-js" src="https://webframez.com/wp-content/themes/webframez/js/functions.js?ver=2013-07-18" type="text/javascript"></script>
<script id="wp-embed-js" src="https://webframez.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>