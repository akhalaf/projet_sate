<!DOCTYPE html>
<html class="root root_js_disabled" data-platform="desktop" lang="en-US">
<head>
<meta charset="utf-8"/>
<title>404 Not Found | 9news.com</title>
<link href="/themes/cool.css" rel="stylesheet"/>
<link href="/critical.min.css" rel="stylesheet"/>
<link href="/modules.min.css" rel="stylesheet"/>
<link href="/assets/favicons/KUSA.png" rel="icon"/>
<style>
        .page__grid {
            margin: 0 auto !important;
            width: 1170px !important;
        }

        .footer__inner {
        }

        .error__button {
            text-decoration: none;
            line-height: 82px;
            padding: 0 75px;
            margin-right: 30px;
        }

        #action-container {
            display: flex;
        }

        .search__search-form {
            flex-grow: 1;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<div class="page" data-module="page" data-platform="desktop">
<div class="page__header">
<header class="header" data-module="header">
<div class="header__main">
<div class="header__main-inner">
<a class="header__station-branding" href="/">
<img alt="Home" class="header__logo" loading="eager" src="/assets/shared-images/logos/kusa.png" width="112"/>
</a>
<nav class="header__not-logo">
<h2 class="header__nav-heading">Navigation</h2>
<ul class="header__nav">
<li class="header__nav-item">
<a class="header__nav-link" href="/news">
<span class="header__nav-label">News</span>
</a>
</li>
<li class="header__nav-item">
<a class="header__nav-link" href="/news">
<span class="header__nav-label">Weather</span>
</a>
</li>
<li class="header__nav-item">
<a class="header__nav-link" href="/news">
<span class="header__nav-label">Watch</span>
</a>
</li>
</ul>
</nav>
</div>
</div>
</header>
</div>
<main class="page__main" id="main">
<div class="page__grid">
<div class="error">
<h1 class="error__title">404</h1>
<div class="error__description"><p>The case of this missing page is still unsolved … but don't worry, we have our best investigative reporters tracking down leads.</p><p>Stay tuned…</p></div>
<div id="action-container">
<a class="error__button" href="/">Go Back Home</a>
<form action="/search" class="search__search-form" method="GET">
<label class="search__search-label" for="site_search">Search:</label>
<input class="search__search-box" id="site_search" name="q" placeholder="Enter Search Terms" type="search"/>
<button class="search__search-submit" type="submit">Search</button>
</form>
</div>
</div>
</div>
</main>
<div class="page__footer page__footer_theme_default">
<footer class="footer" data-module="footer">
<div class="footer__outer">
<div class="footer__inner">
<div class="footer__logo-wrap">
<img alt="KUSA-TV" class="footer__logo" src="/assets/shared-images/logos/kusa.png"/>
</div>
<div class="footer__menu">
<ul class="footer__list">
<li class="footer__item"><a class="footer__link" href="/terms" rel="" target="_self">Terms of Service</a></li>
<li class="footer__item"><a class="footer__link" href="/privacy" rel="" target="_self">Privacy Policy</a></li>
</ul>
</div>
<div class="footer__copyright">
                            © 2021 KUSA-TV. All Rights Reserved.
                        </div>
</div>
</div>
</footer>
</div>
</div>
<script>
        var utag_data = {
            subsection: '',
            subcategory: '',
            section: 'error',
            page_type: 'error',
            page_name: '404',
            division: 'broadcast',
            site_type: 'desktop',
            market: 'Denver, CO'
        }
    </script>
<script src="//tags.tiqcdn.com/utag/tegna/kusa-redesign-desktop/prod/utag.js"></script>
</body>
</html>