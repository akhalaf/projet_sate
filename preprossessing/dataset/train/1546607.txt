<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/favicon.ico" rel="icon"/>
<title>Visible Comunicaciones</title>
<!-- Bootstrap core CSS -->
<link href="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/style.css" rel="stylesheet"/>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/js/ie-emulation-modes-warning.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<meta content="noindex,follow" name="robots"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/visiblecomunicaciones.cl\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.19"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://visiblecomunicaciones.cl/wp-content/uploads/formidable/css/formidablepro.css?ver=525404" id="formidable-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://visiblecomunicaciones.cl/wp-content/plugins/ajax-page-load/jquery.js?ver=4.7.19" type="text/javascript"></script>
<link href="https://visiblecomunicaciones.cl/wp-json/" rel="https://api.w.org/"/>
<link href="https://visiblecomunicaciones.cl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://visiblecomunicaciones.cl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.7.19" name="generator"/>
<script type="text/javascript">

		checkjQuery = false;

		jQueryScriptOutputted = false;

		

		//Content ID

		var AAPL_content = 'content';

		

		//Search Class

		var AAPL_search_class = 'searchform';

		

		//Ignore List - this is for travisavery who likes my comments... hello

		var AAPL_ignore_string = new String('#, /wp-, .pdf, .zip, .rar'); 

		var AAPL_ignore = AAPL_ignore_string.split(', ');

		

		//Shall we take care of analytics?

		var AAPL_track_analytics = false
		

		//Various options and settings

		var AAPL_scroll_top = true
		

		//Maybe the script is being a tw**? With this you can find out why...

		var AAPL_warnings = false;

		

		//This is probably not even needed anymore, but lets keep for a fallback

		function initJQuery() {

			if (checkjQuery == true) {

				//if the jQuery object isn't available

				if (typeof(jQuery) == 'undefined') {

				

					if (! jQueryScriptOutputted) {

						//only output the script once..

						jQueryScriptOutputted = true;

						

						//output the jquery script

						//one day I will complain :/ double quotes inside singles.

						document.write('<scr' + 'ipt type="text/javascript" src="https://visiblecomunicaciones.cl/wp-content/plugins/ajax-page-load/jquery.js"></scr' + 'ipt>');

					}

					setTimeout('initJQuery()', 50);

				}

			}

		}



		initJQuery();



	</script>
<script src="https://visiblecomunicaciones.cl/wp-content/plugins/ajax-page-load/ajax-page-loader.js" type="text/javascript"></script>
<script src="https://visiblecomunicaciones.cl/wp-content/plugins/ajax-page-load/reload_code.js" type="text/javascript"></script>
<script type="text/javascript">

		//urls

		var AAPLsiteurl = "http://visiblecomunicaciones.cl";

		var AAPLhome = "http://visiblecomunicaciones.cl";

		

		//The old code here was RETARDED - Much like the rest of the code... Now I have replaced this with something better ;)

		//PRELOADING YEEEYYYYY!!

		var AAPLloadingIMG = jQuery('<img/>').attr('src', 'http://visiblecomunicaciones.cl/wp-content/uploads/AAPL/loaders/SMALL - Squares.gif');

		var AAPLloadingDIV = jQuery('<div/>').attr('style', 'display:none;').attr('id', 'ajaxLoadDivElement');

		AAPLloadingDIV.appendTo('body');

		AAPLloadingIMG.appendTo('#ajaxLoadDivElement');

		//My code can either be seen as sexy? Or just a terribly orchestrated hack? Really it's up to you...

		

		//Loading/Error Code

		//now using json_encode - two birds one bullet.

		var str = "<center>\r\n\t<p style=\"text-align: center !important;\">Cargando... Porfavor Espere...<\/p>\r\n\t<p style=\"text-align: center !important;\">\r\n\t\t<img src=\"{loader}\" border=\"0\" alt=\"Cargando\" title=\"Porfavor Espere...\" \/>\r\n\t<\/p>\r\n<\/center>";

		var AAPL_loading_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));

		str = "<center>\r\n\t<p style=\"text-align: center !important;\">Error!<\/p>\r\n\t<p style=\"text-align: center !important;\">\r\n\t\t<font color=\"red\">Hubo un problema, el contenido no fue cargado.<\/font>\r\n\t<\/p>\r\n<\/center>";

		var AAPL_loading_error_code = str.replace('{loader}', AAPLloadingIMG.attr('src'));

	</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//visiblecomunicaciones.cl/?wordfence_logHuman=1&hid=7FBED850DAABAACA005214CB9A2E9AA5');
</script> </head>
<body>
<div class="site-wrapper">
<div class="site-wrapper-inner">
<div class="cover-container">
<div class="masthead clearfix">
<div class="inner">
<h3 class="masthead-brand"></h3>
<nav>
<ul class="nav masthead-nav menu">
<li><a href="https://visiblecomunicaciones.cl">Inicio</a></li>
<li><a href="https://visiblecomunicaciones.cl/toda-una-vida">Somos</a></li>
<li><a href="https://visiblecomunicaciones.cl/lo-que-mejor-hacemos">Lo que mejor hacemos</a></li>
<li><a href="https://visiblecomunicaciones.cl/nuestro-sello">Nuestro Sello</a></li>
<li><a href="https://visiblecomunicaciones.cl/trabajemos-juntos">Trabajemos Juntos</a></li>
</ul>
</nav>
</div>
</div>
<div class="inner cover" id="content">
<p class="lead"><img src="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/img/logo-screen.png" style="max-width:100%;"/></p>
<p class="lead">
<a class="btn btn-lg btn-default" href="https://visiblecomunicaciones.cl/lo-que-mejor-hacemos">Conoce más</a>
</p>
</div>
<div class="mastfoot">
<div class="inner">
<p>Visible Comunicaciones 2015. Todos los derechos reservados.®</p>
</div>
</div>
</div>
</div>
</div><video autobuffer="" autoplay="" id="bg_vid" loop="" src="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/vids/v1.mp4"></video>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://visiblecomunicaciones.cl/wp-content/themes/visibletheme/js/ie10-viewport-bug-workaround.js"></script>
<script src="https://visiblecomunicaciones.cl/wp-includes/js/wp-embed.min.js?ver=4.7.19" type="text/javascript"></script>
</body>
</html>
