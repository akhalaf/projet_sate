<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://endofthingscomic.com/wp-content/themes/comicpress/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://endofthingscomic.com/xmlrpc.php" rel="pingback"/>
<meta content="4.4" name="ComicPress"/>
<title>Page not found – The End of Things</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://endofthingscomic.com/feed" rel="alternate" title="The End of Things » Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/endofthingscomic.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://endofthingscomic.com/wp-includes/css/dist/block-library/style.min.css?ver=5.1.8" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://endofthingscomic.com/wp-content/plugins/comic-easel/css/comiceasel.css?ver=5.1.8" id="comiceasel-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://endofthingscomic.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-content/themes/comicpress/js/ddsmoothmenu.js?ver=5.1.8" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-content/themes/comicpress/js/menubar.js?ver=5.1.8" type="text/javascript"></script>
<link href="https://endofthingscomic.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://endofthingscomic.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://endofthingscomic.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.1.8" name="generator"/>
<meta content="1.15" name="Comic-Easel"/>
<meta content="" name="Referrer"/>
<!--Customizer CSS-->
<style type="text/css">
	#page { width: 980px; max-width: 980px; }
	#add-width { width: 10px; }
	#content-column { width: 562px; max-width: 100%; }
	#sidebar-right { width: 204px; }
	#sidebar-left { width: 204px; }
	body { color: #ffffff!important; }
	a:link, a:visited { color: #57a5c6!important; }
	a:visited { color: #004aaa!important; }
	a:visited { color: #004aaa!important; }
	.comic-nav a:link, .comic-nav a:visited { color: #FFFFFF!important; }
	.comic-nav a:hover { color: #F00!important; }
</style>
<!--/Customizer CSS-->
<style type="text/css">
	#header { width: 575px; height: 161px; background: url('https://endofthingscomic.com/wp-content/uploads/2013/09/cropped-logotype2012_2.gif') top center no-repeat; overflow: hidden; }
	#header h1 { padding: 0; }
	#header h1 a { display: block; width: 575px; height: 161px; text-indent: -9999px; }
	.header-info, .header-info h1 a { padding: 0; }
</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #000000; }
</style>
</head>
<body class="error404 custom-background user-guest unknown pm day tue layout-3c scheme-none">
<div id="page-wrap">
<div id="page">
<header id="header">
<div class="header-info">
<h1><a href="https://endofthingscomic.com">The End of Things</a></h1>
<div class="description"></div>
</div>
<div class="clear"></div>
</header>
<div id="content-wrapper">
<div id="subcontent-wrapper">
<div id="sidebar-left">
<div class="sidebar">
<div class="widget ceo_comic_list_dropdown_widget" id="ceo_comic_list_dropdown_widget-2">
<div class="widget-content">
</div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="content-column">
<div class="narrowcolumn" id="content">
<div class="post uentry type-page">
<div class="post-content">
<div class="post-info">
<h2 class="page-title">Page Not Found</h2>
</div>
<div class="entry">
<p><a href="https://endofthingscomic.com">Click here to return to the home page</a> or try a search:</p>
<p></p><form action="https://endofthingscomic.com" class="searchform" method="get">
<input class="s-search" name="s" onblur="this.value=(this.value=='') ? 'Search...' : this.value;" onfocus="this.value=(this.value=='Search...') ? '' : this.value;" type="text" value="Search..."/>
<button type="submit">»</button>
</form>
<div class="clear"></div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div id="sidebar-right">
<div class="sidebar">
<div class="widget widget_calendar" id="calendar-2">
<div class="widget-content">
<div class="calendar_wrap" id="calendar_wrap"><table id="wp-calendar">
<caption>January 2021</caption>
<thead>
<tr>
<th scope="col" title="Monday">M</th>
<th scope="col" title="Tuesday">T</th>
<th scope="col" title="Wednesday">W</th>
<th scope="col" title="Thursday">T</th>
<th scope="col" title="Friday">F</th>
<th scope="col" title="Saturday">S</th>
<th scope="col" title="Sunday">S</th>
</tr>
</thead>
<tfoot>
<tr>
<td colspan="3" id="prev"><a href="https://endofthingscomic.com/archives/date/2016/10">« Oct</a></td>
<td class="pad"> </td>
<td class="pad" colspan="3" id="next"> </td>
</tr>
</tfoot>
<tbody>
<tr>
<td class="pad" colspan="4"> </td><td>1</td><td>2</td><td>3</td>
</tr>
<tr>
<td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
</tr>
<tr>
<td>11</td><td id="today">12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td>
</tr>
<tr>
<td>18</td><td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td>
</tr>
<tr>
<td>25</td><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
</tr>
</tbody>
</table></div></div>
<div class="clear"></div>
</div>
</div>
</div> <div class="clear"></div>
</div>
</div>
<footer id="footer">
<div id="footer-sidebar-wrapper">
</div>
<div class="clear"></div>
<div id="footer-menubar-wrapper">
<div class="clear"></div>
</div>
<p class="copyright-info">
©2006-2016 <a href="https://endofthingscomic.com">Evan</a> <span class="footer-pipe">|</span> Powered by <a href="http://wordpress.org/">WordPress</a> with <a href="http://frumph.net">ComicPress</a>
<span class="footer-subscribe"><span class="footer-pipe">|</span> Subscribe: <a href="https://endofthingscomic.com/feed">RSS</a>
</span>
<span class="footer-uptotop"><span class="footer-pipe">|</span> <a href="" onclick="scrollup(); return false;">Back to Top ↑</a></span>
</p>
</footer>
</div> <!-- // #page -->
</div> <!-- / #page-wrap -->
<script src="https://endofthingscomic.com/wp-content/plugins/comic-easel/js/keynav.js" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-content/themes/comicpress/js/scroll.js" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-content/themes/comicpress/js/cvi_text_lib.js" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-content/themes/comicpress/js/instant.js" type="text/javascript"></script>
<script src="https://endofthingscomic.com/wp-includes/js/wp-embed.min.js?ver=5.1.8" type="text/javascript"></script>
</body>
</html>