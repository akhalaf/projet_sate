<!DOCTYPE html>
<html lang="en-us">
<head>
<title>Kansas City Royals Attendance Records (1969 - 2020) by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="Kansas City Royals Attendance kansas city royals attendance" name="keywords"/>
<meta content="Kansas City Royals attendance records in a year-by-year format by Baseball Almanac." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<!-- BEGIN FREESTAR -->
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || {};
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || {};
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- END FREESTAR -->
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<div id="wrapper">
<div class="header-container">
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
</div>
<div class="container">
<div class="intro">
<h1>Kansas City Royals Attendance Data</h1>
<h2>Attendance Records for the Kansas City Royals</h2>
<p>Baseball Almanac presents the Kansas City Royals attendance records for every season and ballpark in their history including home game averages, season totals, a league average for comparison &amp; a link to a league total chart.</p>
</div>
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"We are playing hard, but we're not hitting like we could or pitching like we could. As a leader on this team I feel responsible." - Mike Sweeney</p>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="5">
<h2>Kansas City Royals Attendance</h2>
<p>1969 - 2020</p>
</td>
</tr>
<tr>
<td class="banner" rowspan="2">Year</td>
<td class="banner" rowspan="2">Ballpark Name</td>
<td class="banner" colspan="3">Attendance</td>
</tr>
<tr>
<td class="banner">Game Average</td>
<td class="banner">Season Total</td>
<td class="banner">A.L. Average</td>
</tr>
<tr>
<td class="datacolBox">
<table class="ba-sub">
<tr>
<td>1969</td>
</tr>
<tr>
<td>1970</td>
</tr>
<tr>
<td>1971</td>
</tr>
<tr>
<td>1972</td>
</tr>
<tr>
<td>1973</td>
</tr>
<tr>
<td>1974</td>
</tr>
<tr>
<td>1975</td>
</tr>
<tr>
<td>1976</td>
</tr>
<tr>
<td>1977</td>
</tr>
<tr>
<td>1978</td>
</tr>
<tr>
<td>1979</td>
</tr>
<tr>
<td>1980</td>
</tr>
<tr>
<td>1981</td>
</tr>
<tr>
<td>1982</td>
</tr>
<tr>
<td>1983</td>
</tr>
<tr>
<td>1984</td>
</tr>
<tr>
<td>1985</td>
</tr>
<tr>
<td>1986</td>
</tr>
<tr>
<td>1987</td>
</tr>
<tr>
<td>1988</td>
</tr>
<tr>
<td>1989</td>
</tr>
<tr>
<td>1990</td>
</tr>
<tr>
<td>1991</td>
</tr>
<tr>
<td>1992</td>
</tr>
<tr>
<td>1993</td>
</tr>
<tr>
<td>1994</td>
</tr>
<tr>
<td>1995</td>
</tr>
<tr>
<td>1996</td>
</tr>
<tr>
<td>1997</td>
</tr>
<tr>
<td>1998</td>
</tr>
<tr>
<td>1999</td>
</tr>
<tr>
<td>2000</td>
</tr>
<tr>
<td>2001</td>
</tr>
<tr>
<td>2002</td>
</tr>
<tr>
<td>2003</td>
</tr>
<tr>
<td>2004</td>
</tr>
<tr>
<td>2005</td>
</tr>
<tr>
<td>2006</td>
</tr>
<tr>
<td>2007</td>
</tr>
<tr>
<td>2008</td>
</tr>
<tr>
<td>2009</td>
</tr>
<tr>
<td>2010</td>
</tr>
<tr>
<td>2011</td>
</tr>
<tr>
<td>2012</td>
</tr>
<tr>
<td>2013</td>
</tr>
<tr>
<td>2014</td>
</tr>
<tr>
<td>2015</td>
</tr>
<tr>
<td>2016</td>
</tr>
<tr>
<td>2017</td>
</tr>
<tr>
<td>2018</td>
</tr>
<tr>
<td>2019</td>
</tr>
<tr>
<td>2020</td>
</tr>
</table>
</td>
<td class="datacolBox">
<table class="ba-sub">
<tr>
<td>Municipal Stadium (II)</td>
</tr>
<tr>
<td>Municipal Stadium (II)</td>
</tr>
<tr>
<td>Municipal Stadium (II)</td>
</tr>
<tr>
<td>Municipal Stadium (II)</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Royals Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
<tr>
<td>Kauffman Stadium</td>
</tr>
</table>
</td>
<td class="datacolBox">
<table class="ba-sub">
<tr>
<td>11,073</td>
</tr>
<tr>
<td>8,556</td>
</tr>
<tr>
<td>11,314</td>
</tr>
<tr>
<td>9,190</td>
</tr>
<tr>
<td>16,609</td>
</tr>
<tr>
<td>14,485</td>
</tr>
<tr>
<td>14,220</td>
</tr>
<tr>
<td>20,744</td>
</tr>
<tr>
<td>22,872</td>
</tr>
<tr>
<td>27,846</td>
</tr>
<tr>
<td>27,924</td>
</tr>
<tr>
<td>28,256</td>
</tr>
<tr>
<td>24,843</td>
</tr>
<tr>
<td>28,203</td>
</tr>
<tr>
<td>24,097</td>
</tr>
<tr>
<td>22,346</td>
</tr>
<tr>
<td>26,700</td>
</tr>
<tr>
<td>28,652</td>
</tr>
<tr>
<td>29,537</td>
</tr>
<tr>
<td>29,195</td>
</tr>
<tr>
<td>30,589</td>
</tr>
<tr>
<td>27,888</td>
</tr>
<tr>
<td>26,686</td>
</tr>
<tr>
<td>23,058</td>
</tr>
<tr>
<td>23,884</td>
</tr>
<tr>
<td>24,356</td>
</tr>
<tr>
<td>17,132</td>
</tr>
<tr>
<td>17,838</td>
</tr>
<tr>
<td>18,853</td>
</tr>
<tr>
<td>18,570</td>
</tr>
<tr>
<td>18,709</td>
</tr>
<tr>
<td>19,319</td>
</tr>
<tr>
<td>18,964</td>
</tr>
<tr>
<td>16,334</td>
</tr>
<tr>
<td>22,819</td>
</tr>
<tr>
<td>21,031</td>
</tr>
<tr>
<td>17,356</td>
</tr>
<tr>
<td>17,158</td>
</tr>
<tr>
<td>19,961</td>
</tr>
<tr>
<td>19,986</td>
</tr>
<tr>
<td>22,473</td>
</tr>
<tr>
<td>19,942</td>
</tr>
<tr>
<td>21,290</td>
</tr>
<tr>
<td>21,748</td>
</tr>
<tr>
<td>21,614</td>
</tr>
<tr>
<td>24,154</td>
</tr>
<tr>
<td>33,439</td>
</tr>
<tr>
<td>31,577</td>
</tr>
<tr>
<td>27,754</td>
</tr>
<tr>
<td>20,556</td>
</tr>
<tr>
<td>18,267</td>
</tr>
<tr>
<td>--</td>
</tr>
</table>
</td>
<td class="datacolBox">
<table class="ba-sub">
<tr>
<td>902,414</td>
</tr>
<tr>
<td>693,047</td>
</tr>
<tr>
<td>910,784</td>
</tr>
<tr>
<td>707,656</td>
</tr>
<tr>
<td>1,345,341</td>
</tr>
<tr>
<td>1,173,292</td>
</tr>
<tr>
<td>1,151,836</td>
</tr>
<tr>
<td>1,680,265</td>
</tr>
<tr>
<td>1,852,603</td>
</tr>
<tr>
<td>2,255,493</td>
</tr>
<tr>
<td>2,261,845</td>
</tr>
<tr>
<td>2,288,714</td>
</tr>
<tr>
<td>1,279,403</td>
</tr>
<tr>
<td>2,284,464</td>
</tr>
<tr>
<td>1,963,875</td>
</tr>
<tr>
<td>1,810,018</td>
</tr>
<tr>
<td>2,162,717</td>
</tr>
<tr>
<td>2,320,794</td>
</tr>
<tr>
<td>2,392,471</td>
</tr>
<tr>
<td>2,350,181</td>
</tr>
<tr>
<td>2,477,700</td>
</tr>
<tr>
<td>2,244,956</td>
</tr>
<tr>
<td>2,161,537</td>
</tr>
<tr>
<td>1,867,689</td>
</tr>
<tr>
<td>1,934,578</td>
</tr>
<tr>
<td>1,400,494</td>
</tr>
<tr>
<td>1,233,530</td>
</tr>
<tr>
<td>1,435,997</td>
</tr>
<tr>
<td>1,517,638</td>
</tr>
<tr>
<td>1,494,875</td>
</tr>
<tr>
<td>1,506,068</td>
</tr>
<tr>
<td>1,564,847</td>
</tr>
<tr>
<td>1,536,101</td>
</tr>
<tr>
<td>1,323,034</td>
</tr>
<tr>
<td>1,779,895</td>
</tr>
<tr>
<td>1,661,478</td>
</tr>
<tr>
<td>1,371,181</td>
</tr>
<tr>
<td>1,372,684</td>
</tr>
<tr>
<td>1,616,867</td>
</tr>
<tr>
<td>1,578,922</td>
</tr>
<tr>
<td>1,797,887</td>
</tr>
<tr>
<td>1,615,327</td>
</tr>
<tr>
<td>1,724,450</td>
</tr>
<tr>
<td>1,739,859</td>
</tr>
<tr>
<td>1,750,754</td>
</tr>
<tr>
<td>1,956,482</td>
</tr>
<tr>
<td>2,708,549</td>
</tr>
<tr>
<td>2,557,712</td>
</tr>
<tr>
<td>2,220,370</td>
</tr>
<tr>
<td>1,665,107</td>
</tr>
<tr>
<td>1,479,659</td>
</tr>
<tr>
<td>--</td>
</tr>
</table>
</td>
<td class="datacolBox">
<table class="ba-sub">
<tr>
<td>1,011,227</td>
</tr>
<tr>
<td>1,007,095</td>
</tr>
<tr>
<td>989,047</td>
</tr>
<tr>
<td>953,211</td>
</tr>
<tr>
<td>1,119,467</td>
</tr>
<tr>
<td>1,087,275</td>
</tr>
<tr>
<td>1,099,119</td>
</tr>
<tr>
<td>1,221,484</td>
</tr>
<tr>
<td>1,402,825</td>
</tr>
<tr>
<td>1,466,426</td>
</tr>
<tr>
<td>1,597,999</td>
</tr>
<tr>
<td>1,563,575</td>
</tr>
<tr>
<td>1,004,713</td>
</tr>
<tr>
<td>1,648,604</td>
</tr>
<tr>
<td>1,713,647</td>
</tr>
<tr>
<td>1,711,531</td>
</tr>
<tr>
<td>1,752,302</td>
</tr>
<tr>
<td>1,798,052</td>
</tr>
<tr>
<td>1,948,382</td>
</tr>
<tr>
<td>2,035,688</td>
</tr>
<tr>
<td>2,132,090</td>
</tr>
<tr>
<td>2,166,590</td>
</tr>
<tr>
<td>2,294,113</td>
</tr>
<tr>
<td>2,268,524</td>
</tr>
<tr>
<td>2,380,955</td>
</tr>
<tr>
<td>1,728,728</td>
</tr>
<tr>
<td>1,811,356</td>
</tr>
<tr>
<td>2,122,721</td>
</tr>
<tr>
<td>2,234,523</td>
</tr>
<tr>
<td>2,298,169</td>
</tr>
<tr>
<td>2,286,874</td>
</tr>
<tr>
<td>2,262,557</td>
</tr>
<tr>
<td>2,346,071</td>
</tr>
<tr>
<td>2,207,891</td>
</tr>
<tr>
<td>2,191,745</td>
</tr>
<tr>
<td>2,340,422</td>
</tr>
<tr>
<td>2,360,452</td>
</tr>
<tr>
<td>2,458,741</td>
</tr>
<tr>
<td><strong>2,527,968</strong></td>
</tr>
<tr>
<td>2,464,986</td>
</tr>
<tr>
<td>2,305,178</td>
</tr>
<tr>
<td>2,289,427</td>
</tr>
<tr>
<td>2,333,812</td>
</tr>
<tr>
<td>2,384,555</td>
</tr>
<tr>
<td>2,306,065</td>
</tr>
<tr>
<td>2,299,409</td>
</tr>
<tr>
<td>2,323,798</td>
</tr>
<tr>
<td>2,336,365</td>
</tr>
<tr>
<td>2,290,907</td>
</tr>
<tr>
<td>2,161,376</td>
</tr>
<tr>
<td>2,039,521</td>
</tr>
<tr>
<td>--</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="banner" rowspan="2">Year</td>
<td class="banner" rowspan="2">Ballpark Name</td>
<td class="banner">Game Average</td>
<td class="banner">Season Total</td>
<td class="banner">A.L. Average</td>
</tr>
<tr>
<td class="banner" colspan="3">Attendance</td>
</tr>
<tr>
<td class="header" colspan="5">Kansas City Royals Attendance Analysis | <a href="../baseball_attendance.shtml" title="Baseball Attendance">Baseball Attendance</a></td>
</tr>
</table>
</div>
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="../images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>The <a href="kcr.shtml">Kansas City Royals</a> enjoyed their first million fan season in 1973 when 1,345,341 fans attended games at the team's new Royals Stadium.</p>
<p>Are you a serious <a href="kcr.shtml">Kansas City Royals</a> fan? Join our <a href="https://www.baseball-fever.com" target="_blank">Royals discussion forum</a> and talk team history, trivia, records, current events, and much more.</p>
<p>Compare these attendance numbers to the <a href="../mgrtmkr.shtml">yearly results</a> of the franchise, the finishes, winning percent totals, and games behind the leader.</p>
</div>
</div>
<div class="footer">
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
				copyright=new Date();
				update=copyright.getFullYear();
				document.write(update);
				</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
</div>
</body>
</html>