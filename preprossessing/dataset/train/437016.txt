<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="it" xml:lang="it" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DP Service AUDIO</title>
<link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-type"/>
</head>
<body>
<div id="bodyhome">
<div id="header">
<p id="logo">DP <span class="white">SERVICE</span> AUDIO</p>
<p id="slogan">www.dpaudio.it</p>
</div>
<div id="page"> <!-- wraps and defines overall page width, centers it -->
<div id="content"> <!-- content area of page -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding"> <!-- content goes below this line -->
<p>DP SERVICE AUDIO mette a disposizione impianti audio professionali, il tutto con garanzia di serieta' e professionalita'</p>
<br/>
<img src="images/dpa.png"/>
<br/>
</div>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<h2><a href="/html/chi_sono.php">CHI SONO</a></h2>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding"> <!-- content goes below this line -->
<br/>
<!-- <img src="images/strisc.jpg" width="100%" height="100%" class="imagefloat" alt="" /> -->
<h5>In sviluppo il software</h5><img src="images/rsz_battiplayer.png"/><h5>per l'automazione degli spettacoli teatrali</h5><br/><br/>
<!--<a href="images/Player.png"><img src="images/rsz_player.jpg"/></a> -->
<img src="images/rsz_player.jpg"/>
</div>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding"> <!-- content goes below this line -->
<h4>Installazione impianto filodiffusione centro Cervignano del Friuli</h4>
<li>40 diffusori installati</li>
<li>1000Wrms distribuiti</li>
<li>2.5km di cavi stesi</li>
<li>gestione remota via internet e APP android</li>
<br/><br/>
<img height="80%" src="images/mappa.png" width="80%"/><br/><br/>
<img height="80%" src="images/filo.png" width="80%"/><br/><br/>
</div>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding"> <!-- content goes below this line -->
<h4>Sviluppo APP votazioni via SMS</h4>
<h5>Con possibilità di estrazione numeri per lotteria</h5>
<br/><br/>
<img height="20%" src="images/android.png" width="20%"/><br/><br/>
<img height="40%" src="images/vot2.png" width="40%"/>&amp;nbsp<img height="40%" src="images/vot1.png" width="40%"/><br/><br/>
</div>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<!-- code below will make a new grey box for content, cut and paste as needed -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding"> <!-- content goes below this line -->
<h5>Sviluppo automazione digitale per sistemi di campane</h5>
<h5>Campanile elettronico preamplificato</h5>
<br/><br/>
<img height="20%" src="images/android.png" width="20%"/><br/><br/>
<img height="80%" src="images/campane.png" width="80%"/>
</div>
</div>
<div class="box-bottom"></div><br/>
<!-- end of grey box code -->
<div class="box-top"></div>
<div class="box">
<div class="box-padding">
<!--<h2><a href="/php/foto.php">Foto</a>&nbsp&nbsp<a href="/php/video/video.php">Video</a></h2><br>



            <br>
			<div class="line2"></div><!-- dotted line -->
<br/>
<br/>
<h5>DP SERVICE AUDIO</h5>
<pc>di Emanuele Battistella</pc><br/>
<pc>Strada del Molino 13/A - 33050 - Carlino (UD)</pc><br/>
<pc>Tel. 3381625677 - emanuele@dpaudio.it</pc><br/>
<pc>P.IVA IT02727960300 - C.F. BTTMNL90L25E473N</pc><br/>
<br/>.<br/>
<h5>E-Mail:<a href="mailto:emanuele@dpaudio.it"><font color="red"> emanuele@dpaudio.it </font></a></h5>
</div>
</div>
<div class="box-bottom"></div>
</div> <!-- end content area of page -->
<div id="right-nav"> <!-- right side navigation, etc on page -->
<p class="nav-headline">Menu:</p>
<div id="main-menu">
<ul>
<li><a href="/home.php">HOME</a></li>
<li><a href="/html/news.php">NEWS</a></li>
<li><a href="/html/chi_sono.php">CHI SONO</a></li>
<li><a href="/html/dpaudio.php">DPAUDIO</a></li>
<li><a href="/html/contatti.php">CONTATTAMI</a></li>
</ul>
</div>
<br/>
<br/><br/>
<div class="right-nav-divider"></div>
<p class="nav-headline"></p>
<div id="posts">
</div>
</div> <!-- end right side navigation -->
<div id="footer">
<div id="footer-pad">
<div class="line"></div>
<p>© Copyright by DP Service Audio 2016, All rights reserved</p>
</div>
</div>
</div>
</div>
</body>
</html>