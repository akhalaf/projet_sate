<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
--><!DOCTYPE HTML>
<html>
<head>
<title>ELectro Rail - Mechanical Engineering</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700" rel="stylesheet" type="text/css"/>
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/>
<script src="js/jquery.min.js"></script>
<!--start slider -->
<link href="css/fwslider.css" media="all" rel="stylesheet"/>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/fwslider.js"></script>
<script type="text/javascript">
//<![CDATA[ 

  $(function() {
    // Stick the #nav to the top of the window
    var nav = $('#nav');
    var navHomeY = nav.offset().top;
    var isFixed = false;
    var $w = $(window);
    $w.scroll(function() {
        var scrollTop = $w.scrollTop();
        var shouldBeFixed = scrollTop > navHomeY;
        if (shouldBeFixed && !isFixed) {
            nav.css({
                position: 'fixed',
                top: 0,
                left: nav.offset().left,
                width: nav.width()
            });
            isFixed = true;
        }
        else if (!shouldBeFixed && isFixed)
        {
            nav.css({
                position: 'static'
            });
            isFixed = false;
        }
    });
});

//]]>  
</script>
<!--end slider -->
<!---strat-date-piker---->
<link href="css/jquery-ui.css" rel="stylesheet"/>
<script src="js/jquery-ui.js"></script>
<script>
				  $(function() {
				    $( "#datepicker,#datepicker1" ).datepicker();
				  });
		  </script>
<!---/End-date-piker---->
<link href="css/JFGrid.css" rel="stylesheet" type="text/css"/>
<link href="css/JFFormStyle-1.css" rel="stylesheet" type="text/css"/>
<script src="js/JFCore.js" type="text/javascript"></script>
<script src="js/JFForms.js" type="text/javascript"></script>
<!-- Set here the key for your domain in order to hide the watermark on the web server -->
<script type="text/javascript">
			(function() {
				JC.init({
					domainKey: ''
				});
				})();
		</script>
<!--nav-->
<script>
		$(function() {
			var pull 		= $('#pull');
				menu 		= $('nav ul');
				menuHeight	= menu.height();

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();
			});

			$(window).resize(function(){
        		var w = $(window).width();
        		if(w > 320 && menu.is(':hidden')) {
        			menu.removeAttr('style');
        		}
    		});
		});
</script>
</head>
<body>
<!-- start header -->
<div class="header_bg">
<div class="wrap">
<div class="header">
<div class="top-nav">
<nav class="clearfix">
<ul>
<li class="active"><a href="index.html">Home</a></li>
<li><a href="about.html">About Us</a></li>
<li><a href="activities.html">Catalogue</a></li>
<li><a href="contact.html">contact Us</a></li>
<li><a href="galery.html">Galery</a></li>
</ul>
<a href="#" id="pull">Menu</a>
</nav>
</div>
<div class="h_right">
<!--start menu -->
<ul class="menu">
<li class="active"><a href="index.html">home</a></li> |
				<li><a href="about.html">About Us</a></li> |
				<li><a href="activities.html">Catalogue</a></li> |
				<li><a href="contact.html">Contact Us</a></li> |
				<li><a href="galery.html">Galery</a></li> |

				 <div class="clear"></div>
</ul>
<!-- start profile_details -->
<form class="style-1 drp_dwn">
</form>
</div>
<div class="clear"></div>
</div>
</div>
</div>
<!----start-images-slider---->
<div class="images-slider">
<!-- start slider -->
<div id="fwslider">
<div class="slider_container">
<div class="slide">
<!-- Slide image -->
<img alt="" src="images/slider-bg.jpg"/>
<!-- /Slide image -->
<!-- Texts container -->
<div class="slide_content">
<div class="slide_content_wrap">
<!-- Text title -->
<!-- /Text title -->
</div>
</div>
<!-- /Texts container -->
</div>
<!-- /Duplicate to create more slides -->
<div class="slide">
<img alt="" src="images/slider-bg2.jpg"/>
<div class="slide_content">
<div class="slide_content_wrap">
<!-- Text title -->
<!-- /Text title -->
</div>
</div>
</div>
<!--/slide -->
</div>
<div class="timers"> </div>
<div class="slidePrev"><span> </span></div>
<div class="slideNext"><span> </span></div>
</div>
<!--/slider -->
</div>
<!--start main -->
<div class="main_bg">
<div class="wrap">
<div class="online_reservation">
<div class="b_room">
<div class="clear"></div>
</div>
</div>
<!--start grids_of_3 -->
<div class="grids_of_3">
<div class="grid1_of_3" style="width: 14%">
<p style="text-align:justify"> </p>
</div>
<div class="grid1_of_3">
<div class="grid1_of_3_img">
<video controls="" height="340px" width="100%">
<source src="Final Render 720 HD_photos.mp4.mp4" type="video/mp4">
<source src="Final Render 720 HD_photos.oggtheora.ogv" type="video/avi">
</source></source></video>
</div>
<h4> </h4>
</div>
<div class="grid1_of_3">
<div class="grid1_of_3_img">
<video controls="" height="340px" width="100%">
<source src="lastvideo_audio.mp4.mp4" type="video/mp4">
<source src="lastvideo_audio.oggtheora.ogv" type="video/avi">
</source></source></video>
</div>
<h4> </h4>
</div>
<div class="clear"></div>
<div class="clear"></div>
<div class="clear"></div>
<div class="clear"></div>
<div><div class="room">
<h4>Company Introduction</h4>
<p class="para" style="font-size: 100%">Electro-Rail was 
				established in 1997, and is a leader in the field of Electro 
				Mechanical Engineering. We are a supplier and manufacturer of 
				Electro Mechanical and general engineering components to the 
				rail and mining markets in South Africa and Sub-Saharan Africa.  
				Our main products and services include new and refurbished: 
				Brush Holders, new and re-tipped contacts, connectors, buss 
				bars, slip rings, insulators, electrical coach connectors, gear 
				cases and pole shoes as well as general machining and 
				fabrication of engineering related products.</p>
<p class="para"> </p>
<h4>Company History</h4>
<p class="para">Electro-Rail was established as a close corporation in 1997, and became a limited-liability company in 2001. The company was Certified to ISO 9001 in February 2006 and is currently accredited at a Level 2 BBBEE status.</p>
<p class="para"> </p>
<h4>About Electro-Rail</h4>
<p class="para">Our operation is based from our 2700mÂ² factory 
				which is owned by Protec Property Holding. Electro-Rail also 
				leases another 1000mÂ² of adjoining factory space. Our main 
				equipment in the factory includes 10 CNC Lathes and 10 CNC 
				machining centres. We also have numerous conventional lathes and 
				milling machines, mig and tig welding machines, hydraulic and 
				electric presses to name but a few.</p>
</div>
</div>
</div>
</div>
</div>
<!--start main -->
<div class="footer_bg">
<div class="wrap">
<div class="footer">
<div class="copy">
<p class="link"><span>All rights reserved | <a href="http://www.auroradesigns.co.za/"> Aurora Designs (Pty Ltd)</a></span></p>
</div>
<div class="f_nav">
<ul>
<li><a href="index.html">home</a></li>
<li><a href="activities.html">Catalogue</a></li>
<li><a href="contact.html">Contact</a></li>
</ul>
</div>
<div class="soc_icons">
<ul>
</ul>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</body>
</html>
