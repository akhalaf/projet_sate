<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="" name="google-site-verification"/>
<meta content="" name="Keywords"/>
<meta content="" name="description"/>
<meta content="Index, Follow" name="robots"/>
<meta content="3 days" name="revisit"/>
<meta content="NOODP" name="robots"/>
<meta content="Bangabasi Evening College" name="Copyright"/>
<meta content="global" name="distribution"/>
<title>Welcome to Bangabasi Evening College</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<link href="templatemo_style.css" rel="stylesheet" type="text/css"/>
<link charset="utf-8" href="css/MenuMatic.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="css/resetmin.css" rel="stylesheet" type="text/css"/>
<script charset="utf-8" src="js/MenuMatic_0.68.3.js" type="text/javascript"></script>
<link href="css/jquery.ennui.contentslider.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>
<style>
	img.event-thumbimage1 {    height:160px;
    width:140px;
    -webkit-box-shadow: 0px 0px 10px 0px #4d4d4d;
    -moz-box-shadow: 0px 0px 10px 0px #4d4d4d;
    box-shadow: 0px 0px 10px 0px #4d4d4d;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -khtml-border-radius: 2px;  
    border-radius: 2px;
    border:solid  #FFC 10px;
 
}
img.event-thumbimage11 {height:180px;
    width:190px;
    -webkit-box-shadow: 0px 0px 10px 0px #4d4d4d;
    -moz-box-shadow: 0px 0px 10px 0px #4d4d4d;
    box-shadow: 0px 0px 10px 0px #4d4d4d;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -khtml-border-radius: 2px;  
    border-radius: 2px;
    border:solid  #FFC 10px;
}
 </style>
</head>
<body>
<div id="templatemo_header_wrapper">
<div id="templatemo_header">
<table align="right" border="0" width="670">
<tr>
<td width="28"> </td>
<td width="233"> </td>
<td width="28"> </td>
<td width="148"> </td>
<td width="10"> </td>
<td width="99"> </td><td width="14"> </td>
<td width="34"> </td> <td width="38"> </td>
</tr>
</table>
<div id="site_logo">
<p> </p>
<p>
</p><div style="font-family: Georgia, 'Times New Roman', Times, serif;font-size:26px;font-weight: bold; color:#000; float:left; padding-left:30px; text-shadow:#BEBE7C 3px 2px 3px; margin-top:14px;font-size:30px;">Bangabasi Evening College</div><div style=" font-family:'Courier New', Courier, monospace;font-size:16px;font-weight: normal; color:#333; float:left; padding-left:30px;text-shadow:#000 0px 1px 1px; "><br/>
    	  Affiliated to the University of Calcutta</div>
</div>
</div> <!-- end of header -->
</div> <!-- end of header wrapper -->
<!-- end of menu -->
<!-- end of menu wrapper -->
<div id="tempatemo_content_wrapper">
<div id="templatemo_content">
<div id="content_panel">
<div id="column_w610">
<p align="justify" style="margin: 0px; padding: 0px 0px 16px 2px; line-height: 1.7em; font-family: verdana, arial, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; color:#333;font-weight: normal; letter-spacing: normal; orphans: auto; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; ">
<!--<table width="100%" border="0" style="margin: 0px; padding: 0px 0px 16px 2px; line-height: 1.7em; font-family: verdana, arial, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; color:#333;font-weight: normal; letter-spacing: normal; orphans: auto; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; "><div align="justify">
  <tr>
    <td width="37%" height="217" valign="top"><img src="img/teacher-in-charge.jpg"  class="event-thumbimage11"/></td>
    <td width="50%" valign="top">Rabin Mukherjee College, formerly Behala College of Commerce, came into existence in consonance with the well known proverb, âNature abhors a vacuum.â Yes, to cater to the long-felt need for Commerce education in the widespread area of Western Behala with a growing population Behala College of Commerce was founded in 1967 at Parnasree, Kolkata- 6.<br/> </td>
  </tr>
  
  <tr>
    <td colspan="2"> At the initial stage of this introductory statement we would like to add that there had been a change in the name of the College. Consequent upon the sad demise of Sri Rabin Mukherjee, a revered leader of democratic movements and a former Cabinet Minister of the Govt. of W.B. and also the former President of the Governning Body of our College, a popular demand was placed for renaming the College after the name of the dear departed leader. Consequently Behala College of Commerce was re-christened as Rabin Mukherjee College in 2005 with the consent and approval of the University of Calcutta and the Government of West Bengal.<br/><br/>

Primarily Behala College of Commerce was purely a Commerce College affiliated to the University of Calcutta. Now it is a Commerce as well as an Arts College.<br/><br/>

Time comes but it does not take long to pass. Thus we have stepped into the year 2012- 2013 session with full fervour and gusto, welcoming another new session with greater resolutions and greater faith in our own ability to accomplish. Time gives us a lot of things and then it also takes a lot more from us.<br/><br/>

With the passage of time we have felt that there is a growing demand among students for study in other disciplines. For that reason and the ethusiasm of the local people and the good offices of the Governing Body, the college introduced Arts with Honours in English and English, Bengali, Economics, Education and Political Science in the General Stream.<br/><br/>

Rabin Mukherjee College has undertaken the construction of the new building for its class rooms and administrative block. The basic objectives of the college are to extend education with variety of disciplines.<br/>
</td>
    </tr>
    </div>
</table>-->
</p>
<table border="0" width="100%">
<tr>
<td><!--<span class="services" style="margin:20px; 20px; 0 30px;"><a href="students_profile_mapping/"> <em> <img src="images/1page_img5.png" alt="" style="padding-right:50px; " /> <img src="images/1page_img5-hover.png" alt="" class="image-hover" style="padding-right:20px;" /></em> <span>Teaching-Aid for Teachers</span></a></span>--></td>
<td align="center"><div class="services last" style="margin:20px; 0 0 30px;"><a href="http://www.collegefeed.bangabasieveningcollege.in" target="_blank"> <em> <img alt="" src="images/1page_img3.png" style="padding-right:50px;"/> <img alt="" class="image-hover" src="images/1page_img3-hover.png" style="padding-right:20px;"/></em> <span>Feedback Mechanism </span></a></div>
<div class="services last" style="margin:20px; 0 0 30px;"><a href="https://bangabasieveningcollege.in/lms" target="_blank"> <em> <img alt="" src="images/1page_img3.png" style="padding-right:50px;"/> <img alt="" class="image-hover" src="images/1page_img3-hover.png" style="padding-right:20px;"/></em> <span>Learning Managemenrt System (LMS)</span></a></div>
<div class="services last" style="margin:20px; 0 0 30px;"><a href="https://bangabasieveningcollege.in/spm/" target="_blank"> <em> <img alt="" src="images/1page_img3.png" style="padding-right:50px;"/> <img alt="" class="image-hover" src="images/1page_img3-hover.png" style="padding-right:20px;"/></em> <span>Sudent Profile Mapping</span></a></div>
<div class="services last" style="margin:20px; 0 0 30px;"><a href="https://bangabasieveningcollege.in/teacher_appraisal/" target="_blank"> <em> <img alt="" src="images/1page_img3.png" style="padding-right:50px;"/> <img alt="" class="image-hover" src="images/1page_img3-hover.png" style="padding-right:20px;"/></em> <span>360° Teachers' Appraisal</span></a></div>
</td>
<td><!--<div class="services last" style="margin:20px; 0 0 30px;"><a href="SFS_TAS/"> <em> <img src="images/1page_img3.png" alt="" style="padding-right:50px;" /> <img src="images/1page_img3-hover.png" alt="" class="image-hover"style="padding-right:50px;" /></em> <span>Students' Profile Mapping  </span></a></div>--></td>
</tr>
</table>
</div>
<!-- end of column w610 --><!-- end of column 290 -->
<div class="cleaner"></div>
</div> <!-- end of content panel -->
<div class="cleaner"></div>
</div> <!-- end of content -->
</div> <!-- end of content wrapper -->
<div id="templatemo_footer_wrapper">
<div id="templatemo_footer">
<div class="section_w920">
    Copyright Â© 2014 <a href="#" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; color:#FFF;"> Bangabasi Evening College </a> | Powered by <a href="#" style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; color:#FFF;" target="_parent">Right Brains Technology</a></div>
<div class="cleaner"></div>
</div> <!-- end of footer -->
</div> <!-- end of footer -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script>
<script src="js/logging.js" type="text/javascript"></script>
</body>
</html>