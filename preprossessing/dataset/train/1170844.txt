<!DOCTYPE HTML>
<html lang="NL">
<head>
<title>Videogastenboek op uw feest | DE PRAATPAAL</title>
<meta content="no" http-equiv="imagetoolbar"/>
<meta content="praatpaal, feest, huwelijk, gastenboek" name="keywords"/>
<meta content="DE PRAATPAAL is hét videogastenboek in verschillende kleuren met eenvoudige touchscreen-bediening, waarbij de gasten zichzelf op beeld aan het werk zien." lang="nl" name="description"/>
<meta content="take2media - Paul Bouwman" lang="nl" name="author"/>
<meta content="© take2media 2010" lang="nl" name="copyright"/>
<meta content="https://praatpaaltouch.be" name="identifier-url"/>
<script src="/js/jquery-1.7.1.min.js"></script>
<script src="/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<link href="/fancybox/jquery.fancybox-1.3.4.css" media="screen" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
$(document).ready(function() {
// fade link-images
 	$('.link').hover(function () {
		var src = $("img", this).attr("src").match(/[^\.]+/) + "_hover.png";
			$("img", this).attr("src", src);
    	}, function () {
			var src = $("img", this).attr("src").replace("_hover", "");
			$("img", this).attr("src", src);
    });


	$("a[rel=vergroting]").fancybox({
		'transitionIn'		: 'elastic',
		'transitionOut'		: 'elastic',
		'speedIn'			: 600,
		'speedOut'			: 300,
		'titlePosition' 	: 'over',
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
		return '<span id="fancybox-title-over">Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		}
	});

});
</script>
<meta content="iLwnS_CEI8P-PClmDYh-Z-SaL9gC3pXSDrz_WgOE8W8" name="google-site-verification"/> <!-- http://praatpaaltouch.be/ -->
<meta content="ruq_vT4-NQjHD-4aSuNoWbDGqH3J9zxM3qp4McAbG7c" name="google-site-verification"/> <!-- http://depraatpaaltouch.be/ -->
<meta content="VFFo_fs7raz9l5FVVFI3Lzgsv9-5alImnP-KRcJlfyE" name="google-site-verification"/> <!-- http://praatpaal.be/ -->
<meta content="MyYwvF4TpUf7IVNCKCdLLI3u93paZVoNx40pyip6zOI" name="google-site-verification"/> <!-- http://depraatpaal.be/ -->
<link href="/stijl.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Glegoo" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="wrapper">
<script src="/js/bxslider.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#slider").bxSlider({
            auto: true,
            mode: 'vertical',
            pager: true,
            controls: false,
			speed: 1000,
			pause: 5500
        });
    });
    </script>
<ul id="menu">
<div class="logo"><img alt="" src="/images/logo_icon.png"/></div>
<li><a href="/">Home</a></li>
<li><a href="/gastenboek-huwelijk-uitleg">De Praatpaal</a></li><li><a href="/andere-ideeen">Andere ideeën</a></li><li><a href="/contact-gastenboek-huwelijk">Contact</a></li> </ul>
<ul id="slider">
<li><img alt="" src="/images/header1.png"/></li>
<li><img alt="" src="/images/header2.png"/></li>
</ul>
<div id="home"><div class="kolom ppt"><h2>De PraatPaal Touch</h2><p>Het videogastenboek in verschillende kleuren met eenvoudige touchscreen-bediening, waarbij jullie gasten zichzelf op beeld aan het werk zien.</p><p>'De praatpaal touch' is een video gastenboek waarin jullie gasten gedurende gans jullie trouwfeest een booschap kunnen inspreken.</p>
<p>'De praatpaal touch' beschikt over een touch-screen beeldscherm, een camera en een microfoon. Een persoonlijke babbelbox op je eigen feest dus! Op het scherm is live-video beeld te zien, je ziet jezelf dus op het beeldscherm! Zodra je op START drukt, begint de opname te lopen.</p><br/><br/><a class="btn_info" href="/gastenboek-huwelijk-uitleg"> meer informatie </a></div><div class="kolom2"><h2>links</h2><a class="links_marginr" href="http://babbelballon.be" id="links_home" target="blank"><img src="/images/babbel.png"/></a><a href="http://deflitspaal.be" id="links_home" target="blank"><img src="/images/flits.png"/></a><a class="links_marginr" href="http://defeestcameraadjes.be" id="links_home" target="blank"><img src="/images/feest.png"/></a><a href="http://hetfeestviltje.be/" id="links_home" target="blank"><img src="/images/viltje.png"/></a><a id="links_home"><img src="/images/tekst_link.png"/></a></div><div class="clear"></div></div>
<div id="tekst">
<h1 class="home">Videogastenboek voor uw feest_</h1>
</div><!-- EINDE TEKST -->
<div id="footer">
<div id="kaart"><img src="/images/kaart.png"/></div>
<div id="adres">
        BOEMPATAT!<br/>
        Spoorbermstraat 8<br/>
        8500 KORTRIJK<br/>
        België<br/>
<p>tel  0477 359 047<br/>
<a href="mailto:info@depraatpaal.be" id="email">info@depraatpaal.be</a>
</p></div>
<div id="copyright"><p>© 2021 DE PRAATPAAL</p></div>
</div>
</div><!-- EINDE WRAPPER -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15856697-1");
pageTracker._setDomainName("none");
pageTracker._setAllowLinker(true);
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
