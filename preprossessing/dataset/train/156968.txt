<!DOCTYPE html>
<html lang="de-DE">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://arztpraxis-drews.info/xmlrpc.php" rel="pingback"/>
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<title>404 Nicht gefunden | Arztpraxis Dr. Drews</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://arztpraxis-drews.info/feed/" rel="alternate" title="Arztpraxis Dr. Drews » Feed" type="application/rss+xml"/>
<link href="https://arztpraxis-drews.info/comments/feed/" rel="alternate" title="Arztpraxis Dr. Drews » Kommentar-Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/arztpraxis-drews.info\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Arztpraxis Drews v.1.0.0" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://arztpraxis-drews.info/wp-content/plugins/wp-media-folder/assets/css/divi-widgets.css?ver=5.3.7" id="wpmf_divi_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.6" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/uploads/wtfdivi/wp_head.css?ver=1506503194" id="wtfdivi-user-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/themes/Divi/style.dev.css?ver=5.6" id="divi-parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/themes/praxis_drews/font-awesome-4.7.0/css/font-awesome.min.css?ver=5.6" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/themes/praxis_drews/style.css?ver=4.7.5" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/plugins/wp-media-folder/class/divi-widgets/styles/style.min.css?ver=1.0.0" id="wpmf-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes_responsive.css?ver=4.7.5" id="et-shortcodes-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/styles/magnific_popup.css?ver=4.7.5" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://arztpraxis-drews.info/wp-includes/css/dashicons.min.css?ver=5.6" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://arztpraxis-drews.info/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://arztpraxis-drews.info/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="cookie-notice-front-js-extra" type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxUrl":"https:\/\/arztpraxis-drews.info\/wp-admin\/admin-ajax.php","nonce":"a48a11eca8","hideEffect":"fade","position":"bottom","onScroll":"0","onScrollOffset":"100","onClick":"0","cookieName":"cookie_notice_accepted","cookieTime":"2592000","cookieTimeRejected":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"0","cache":"0","refuse":"0","revokeCookies":"0","revokeCookiesOpt":"automatic","secure":"1","coronabarActive":"0"};
/* ]]> */
</script>
<script id="cookie-notice-front-js" src="https://arztpraxis-drews.info/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.2" type="text/javascript"></script>
<link href="https://arztpraxis-drews.info/wp-json/" rel="https://api.w.org/"/><link href="https://arztpraxis-drews.info/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://arztpraxis-drews.info/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style>
/* Display the team member icons */
.db_pb_team_member_website_icon:before{content:"\e0e3";}
.db_pb_team_member_email_icon:before{content:"\e010";}
.db_pb_team_member_instagram_icon:before{content:"\e09a";}

/* Fix email icon hidden by Email Address Encoder plugin */
ul.et_pb_member_social_links li > span { 
	display: inline-block !important; 
}
</style>
<style>
@media only screen and (min-width: 981px) {
    .et_pb_module.db_inline_form .et_pb_newsletter_fields > p { 
        flex: auto !important;
    }
    .et_pb_module.db_inline_form .et_pb_newsletter_fields p.et_pb_newsletter_field {
        margin-right: 2%; 
    }
}
</style>
<!-- Analytics by WP-Statistics v13.0.4 - https://wp-statistics.com/ -->
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link as="font" crossorigin="anonymous" href="https://arztpraxis-drews.info/wp-content/themes/Divi/core/admin/fonts/modules.ttf" rel="preload"/><link href="https://fonts.googleapis.com/css?family=Michroma" rel="stylesheet"/><link href="https://arztpraxis-drews.info/wp-content/uploads/2017/09/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://arztpraxis-drews.info/wp-content/uploads/2017/09/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://arztpraxis-drews.info/wp-content/uploads/2017/09/cropped-favicon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://arztpraxis-drews.info/wp-content/uploads/2017/09/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<style id="et-divi-customizer-global-cached-inline-styles">body,.et_pb_column_1_2 .et_quote_content blockquote cite,.et_pb_column_1_2 .et_link_content a.et_link_main_url,.et_pb_column_1_3 .et_quote_content blockquote cite,.et_pb_column_3_8 .et_quote_content blockquote cite,.et_pb_column_1_4 .et_quote_content blockquote cite,.et_pb_blog_grid .et_quote_content blockquote cite,.et_pb_column_1_3 .et_link_content a.et_link_main_url,.et_pb_column_3_8 .et_link_content a.et_link_main_url,.et_pb_column_1_4 .et_link_content a.et_link_main_url,.et_pb_blog_grid .et_link_content a.et_link_main_url,body .et_pb_bg_layout_light .et_pb_post p,body .et_pb_bg_layout_dark .et_pb_post p{font-size:15px}.et_pb_slide_content,.et_pb_best_value{font-size:17px}body{color:rgba(0,0,0,0.7)}h1,h2,h3,h4,h5,h6{color:#222222}body{line-height:1.9em}.woocommerce #respond input#submit,.woocommerce-page #respond input#submit,.woocommerce #content input.button,.woocommerce-page #content input.button,.woocommerce-message,.woocommerce-error,.woocommerce-info{background:#4b94cb!important}#et_search_icon:hover,.mobile_menu_bar:before,.mobile_menu_bar:after,.et_toggle_slide_menu:after,.et-social-icon a:hover,.et_pb_sum,.et_pb_pricing li a,.et_pb_pricing_table_button,.et_overlay:before,.entry-summary p.price ins,.woocommerce div.product span.price,.woocommerce-page div.product span.price,.woocommerce #content div.product span.price,.woocommerce-page #content div.product span.price,.woocommerce div.product p.price,.woocommerce-page div.product p.price,.woocommerce #content div.product p.price,.woocommerce-page #content div.product p.price,.et_pb_member_social_links a:hover,.woocommerce .star-rating span:before,.woocommerce-page .star-rating span:before,.et_pb_widget li a:hover,.et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active,.et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active,.et_pb_gallery .et_pb_gallery_pagination ul li a.active,.wp-pagenavi span.current,.wp-pagenavi a:hover,.nav-single a,.tagged_as a,.posted_in a{color:#4b94cb}.et_pb_contact_submit,.et_password_protected_form .et_submit_button,.et_pb_bg_layout_light .et_pb_newsletter_button,.comment-reply-link,.form-submit .et_pb_button,.et_pb_bg_layout_light .et_pb_promo_button,.et_pb_bg_layout_light .et_pb_more_button,.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce button.button.alt.disabled,.woocommerce-page button.button.alt,.woocommerce-page button.button.alt.disabled,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt,.woocommerce a.button,.woocommerce-page a.button,.woocommerce button.button,.woocommerce-page button.button,.woocommerce input.button,.woocommerce-page input.button,.et_pb_contact p input[type="checkbox"]:checked+label i:before,.et_pb_bg_layout_light.et_pb_module.et_pb_button{color:#4b94cb}.footer-widget h4{color:#4b94cb}.et-search-form,.nav li ul,.et_mobile_menu,.footer-widget li:before,.et_pb_pricing li:before,blockquote{border-color:#4b94cb}.et_pb_counter_amount,.et_pb_featured_table .et_pb_pricing_heading,.et_quote_content,.et_link_content,.et_audio_content,.et_pb_post_slider.et_pb_bg_layout_dark,.et_slide_in_menu_container,.et_pb_contact p input[type="radio"]:checked+label i:before{background-color:#4b94cb}a{color:#4b94cb}#top-header,#et-secondary-nav li ul{background-color:#4c95cc}#top-menu li a{font-size:17px}body.et_vertical_nav .container.et_search_form_container .et-search-form input{font-size:17px!important}#main-footer{background-color:#4b94cb}#footer-widgets .footer-widget a,#footer-widgets .footer-widget li a,#footer-widgets .footer-widget li a:hover{color:#ffffff}.footer-widget{color:#ffffff}#main-footer .footer-widget h4{color:#4b94cb}.footer-widget li:before{border-color:#4b94cb}#footer-widgets .footer-widget li:before{top:9.75px}#footer-bottom{background-color:rgba(255,255,255,0)}#footer-info,#footer-info a{color:#ffffff}.et_slide_in_menu_container,.et_slide_in_menu_container .et-search-field{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-moz-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-webkit-input-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field:-ms-input-placeholder{letter-spacing:px}@media only screen and (min-width:981px){.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container,.et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container{height:11.88px}.et-fixed-header#top-header,.et-fixed-header#top-header #et-secondary-nav li ul{background-color:#4c95cc}}@media only screen and (min-width:1350px){.et_pb_row{padding:27px 0}.et_pb_section{padding:54px 0}.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper{padding-top:81px}.et_pb_fullwidth_section{padding:0}}	body,input,textarea,select{font-family:'Nunito',Helvetica,Arial,Lucida,sans-serif}</style></head>
<body class="error404 cookies-not-set dbdb_divi_2_4_up desktop et_pb_button_helper_class et_fullwidth_nav et_fullwidth_secondary_nav et_fixed_nav et_show_nav et_secondary_nav_enabled et_secondary_nav_only_menu et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_smooth_scroll et_right_sidebar et_divi_theme et-db" data-rsssl="1">
<div id="page-container">
<div id="top-header">
<div class="container clearfix">
<div id="et-secondary-menu">
<ul class="menu" id="et-secondary-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-709"><a href="https://arztpraxis-drews.info/patienteninformation-zum-datenschutz/">Patienteninformation zum Datenschutz</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-201"><a href="https://arztpraxis-drews.info/datenschutzerklaerung/">Datenschutzerklärung</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209"><a href="https://arztpraxis-drews.info/impressum/">Impressum</a></li>
</ul> </div> <!-- #et-secondary-menu -->
</div> <!-- .container -->
</div> <!-- #top-header -->
<header data-height-onload="66" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://arztpraxis-drews.info/">
<img alt="Arztpraxis Dr. Drews" data-height-percentage="54" id="logo" src="https://arztpraxis-drews.info/wp-content/uploads/2017/09/favicon.png"/>
</a>
</div>
<div data-fixed-height="40" data-height="66" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-450" id="menu-item-450"><a href="https://arztpraxis-drews.info/">Startseite</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-206" id="menu-item-206"><a href="/#sprechzeiten">Sprechzeiten</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-456" id="menu-item-456"><a href="/#notdienstnummern">Notdienst</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-449" id="menu-item-449"><a href="https://arztpraxis-drews.info/praxisteam/">Praxisteam</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-462" id="menu-item-462"><a href="https://arztpraxis-drews.info/leistungen/">Leistungen</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-199" id="menu-item-199"><a href="/#anfahrt">Anfahrt</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Seite wählen</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://arztpraxis-drews.info/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Suchen …" title="Suchen nach:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<div class="container">
<div class="clearfix" id="content-area">
<div id="left-area">
<article class="et_pb_post not_found" id="post-0">
<div class="entry">
<!--If no results are found-->
<h1 class="not-found-title">Keine Ergebnisse gefunden</h1>
<p>Die angefragte Seite konnte nicht gefunden werden. Verfeinern Sie Ihre Suche oder verwenden Sie die Navigation oben, um den Beitrag zu finden.</p>
</div>
<!--End if no results are found--> </article> <!-- .et_pb_post -->
</div> <!-- #left-area -->
<div id="sidebar">
<div class="et_pb_widget widget_search" id="search-2"><form action="https://arztpraxis-drews.info/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Suche nach:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Suche"/>
</div>
</form></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_recent_comments" id="recent-comments-2"><h4 class="widgettitle">Neueste Kommentare</h4><ul id="recentcomments"></ul></div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_archive" id="archives-2"><h4 class="widgettitle">Archive</h4>
<ul>
</ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_categories" id="categories-2"><h4 class="widgettitle">Kategorien</h4>
<ul>
<li class="cat-item-none">Keine Kategorien</li> </ul>
</div> <!-- end .et_pb_widget --><div class="et_pb_widget widget_meta" id="meta-2"><h4 class="widgettitle">Meta</h4>
<ul>
<li><a href="https://arztpraxis-drews.info/wp-login.php">Anmelden</a></li>
<li><a href="https://arztpraxis-drews.info/feed/">Feed der Einträge</a></li>
<li><a href="https://arztpraxis-drews.info/comments/feed/">Kommentare-Feed</a></li>
<li><a href="https://de.wordpress.org/">WordPress.org</a></li>
</ul>
</div> <!-- end .et_pb_widget --> </div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->
</div> <!-- #main-content -->
<span class="et_pb_scroll_top et-pb-icon"></span>
<footer id="main-footer">
<div id="footer-bottom">
<div class="container clearfix">
<p id="footer-info">© 2021 Arztpraxis Dr. Drews | Designed by <a href="http://mindcopter.com" target="_blank" title="mindcopter">mindcopter</a></p>
</div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<style>
	.et_pb_slide.db_background_url:hover{
		cursor:pointer;
	}
	</style>
<script>
	jQuery(function($){
		$(".db_background_url").click(function(){
			var url = $(this).data('db_background_url');
			if (url.indexOf('#') == 0 || url.indexOf('.') == 0) {
				et_pb_smooth_scroll($(url), false, 800);
			} else {
				document.location=url;
			}
		});
	});
	</script>
<script>
//#uc-btn-accept-banner

window.onload = function() {
    document.getElementById('uc-btn-accept-banner').addEventListener('click', ausgabe, false);
	document.getElementById('uc-btn-deny-banner').addEventListener('click', ausgabe, false);
    function ausgabe() {
    	   	 window.location.reload(true);
    }
}
	
	

</script><link href="https://fonts.googleapis.com/css?family=Nunito:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic&amp;subset=latin,latin-ext&amp;display=swap" id="et-builder-googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="et-builder-modules-global-functions-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"};
/* ]]> */
</script>
<script id="et-builder-modules-global-functions-script-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-global-functions.js?ver=4.7.5" type="text/javascript"></script>
<script id="et-jquery-touch-mobile-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.mobile.custom.min.js?ver=4.7.5" type="text/javascript"></script>
<script id="divi-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
/* ]]> */
</script>
<script id="divi-custom-script-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/js/custom.js?ver=4.7.5" type="text/javascript"></script>
<script id="smooth-scroll-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/js/smoothscroll.js?ver=4.7.5" type="text/javascript"></script>
<script id="et-builder-modules-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_custom = {"ajaxurl":"https:\/\/arztpraxis-drews.info\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/arztpraxis-drews.info\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/arztpraxis-drews.info\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"7dfc4034b4","subscription_failed":"Bitte \u00fcberpr\u00fcfen Sie die Felder unten aus, um sicherzustellen, dass Sie die richtigen Informationen eingegeben.","et_ab_log_nonce":"c66d361be0","fill_message":"Bitte f\u00fcllen Sie die folgenden Felder aus:","contact_error_message":"Bitte folgende Fehler beheben:","invalid":"Ung\u00fcltige E-Mail","captcha":"Captcha","prev":"Vorherige","previous":"Vorherige","next":"Weiter","wrong_captcha":"Sie haben die falsche Zahl im Captcha eingegeben.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"","unique_test_id":"","ab_bounce_rate":"","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];
/* ]]> */
</script>
<script id="et-builder-modules-script-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-scripts.js?ver=4.7.5" type="text/javascript"></script>
<script id="wpmf-frontend-bundle-js" src="https://arztpraxis-drews.info/wp-content/plugins/wp-media-folder/class/divi-widgets/scripts/frontend-bundle.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="divi-fitvids-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.fitvids.js?ver=4.7.5" type="text/javascript"></script>
<script id="waypoints-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/scripts/ext/waypoints.min.js?ver=4.7.5" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.magnific-popup.js?ver=4.7.5" type="text/javascript"></script>
<script id="et-core-common-js" src="https://arztpraxis-drews.info/wp-content/themes/Divi/core/admin/js/common.js?ver=4.7.5" type="text/javascript"></script>
<script id="wtfdivi-user-js-js" src="https://arztpraxis-drews.info/wp-content/uploads/wtfdivi/wp_footer.js?ver=1506503194" type="text/javascript"></script>
<script id="wp-embed-js" src="https://arztpraxis-drews.info/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<!-- Cookie Notice plugin v1.3.2 by Digital Factory https://dfactory.eu/ -->
<div aria-label="Cookie Notice" class="cookie-notice-hidden cookie-revoke-hidden cn-position-bottom" id="cookie-notice" role="banner" style="background-color: rgba(76,149,204,1);"><div class="cookie-notice-container" style="color: #fff;"><span class="cn-text-container" id="cn-notice-text">Wir verwenden auf dieser Seite Cookies, um Ihnen den bestmöglichen Service zu gewährleisten. Wenn Sie diese Seite weiterverwenden, stimmen Sie der Cookie-Nutzung zu.</span><span class="cn-buttons-container" id="cn-notice-buttons"><a aria-label="OK" class="cn-set-cookie cn-button wp-default button" data-cookie-set="accept" href="#" id="cn-accept-cookie">OK</a><a aria-label="Mehr Informationen" class="cn-more-info cn-button wp-default button" href="https://arztpraxis-drews.info/datenschutzerklaerung/" id="cn-more-info" target="_blank">Mehr Informationen</a></span><a aria-label="OK" class="cn-close-icon" data-cookie-set="accept" href="javascript:void(0);" id="cn-close-notice"></a></div>
</div>
<!-- / Cookie Notice plugin -->
</body>
</html>