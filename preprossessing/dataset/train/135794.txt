<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="U1IT2F7Cul" name="baidu-site-verification"/>
<title>AppCan中国Hybrid混合应用开发、移动平台、移动云平台的领导者</title>
<meta content="企业移动应用管理平台,移动云服务平台,Hybrid混合移动应用开发,企业移动化，企业移动门户，跨平台开发" name="keywords"/>
<meta content="AppCan Hybrid混合应用开发、企业移动应用管理平台、移动服务云平台等企业移动门户的领导者，提供移动应用开发及移动跨平台开发技术等服务。服务热线400-040-1766。" name="description"/>
<link href="images/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<!-- <link rel="stylesheet" href="css/ui-base.css"> -->
<link href="css/indexBYvic.css" rel="stylesheet"/>
<link href="css/appcanmain.css" rel="stylesheet"/>
<link href="//cdn.bootcss.com/toastr.js/2.1.4/toastr.min.css" rel="stylesheet"/>
<link href="css/index-pc.css" media="(min-width:750px)" rel="stylesheet"/>
<link href="css/index-mobile.css" media="(min-width:320px) and (max-width:750px)" rel="stylesheet"/>
<link href="../css/newHeader-tomobi.css" media="(min-width:750px)" rel="stylesheet" type="text/css"/>
<link href="../css/newHeader-mobile.css" media="(min-width:320px) and (max-width:750px)" rel="stylesheet" type="text/css"/>
<link href="../css/footer-tomobi.css" media="(min-width:750px)" rel="stylesheet" type="text/css"/>
<link href="../css/footer-mobile.css" media="(min-width:320px) and (max-width:750px)" rel="stylesheet" type="text/css"/>
<script src="//hm.baidu.com/hm.js?fe5f371a643909544db6ac97e59b2d12"></script>
<script src="js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="js/header_20191220.js" type="text/javascript"></script>
<script src="js/sso.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.bootcss.com/toastr.js/2.1.4/toastr.min.js"></script>
<!-- <script src="js/jsized.snow.min.js"></script> -->
<script charset="utf-8" id="id_jsonp_bridge_1576465255910_24779137269262486" src="//p.qiao.baidu.com/cps2/site/poll?cb=jsonp_bridge_1576465255910_24779137269262486&amp;l=1&amp;sign=&amp;v=2253768505013226034&amp;s=13226034&amp;e=27574557&amp;dev=0&amp;auth=%7B%22anonym%22%3A0%2C%22key%22%3A%226625043401725287240wluy989852906%22%2C%22sn%22%3A%22705406631%22%2C%22id%22%3A%222253768505013226034%22%2C%22from%22%3A4%2C%22token%22%3A%22bridge%22%7D&amp;_time=1576465255910" type="text/javascript"></script>
<style>
    html,
    body,
    div,
    span,
    applet,
    object,
    iframe,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p,
    blockquote,
    pre,
    a,
    abbr,
    acronym,
    address,
    big,
    cite,
    code,
    del,
    dfn,
    em,
    img,
    ins,
    kbd,
    q,
    s,
    samp,
    small,
    strike,
    strong,
    sub,
    sup,
    tt,
    var,
    b,
    u,
    i,
    center,
    dl,
    dt,
    dd,
    ol,
    ul,
    li,
    fieldset,
    form,
    label,
    legend,
    table,
    caption,
    tbody,
    tfoot,
    thead,
    tr,
    th,
    td,
    article,
    aside,
    canvas,
    details,
    embed,
    figure,
    figcaption,
    footer,
    header,
    hgroup,
    menu,
    nav,
    output,
    ruby,
    section,
    summary,
    time,
    mark,
    audio,
    video {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      margin: 0;
      padding: 0;
      border: 0;
      font-size: 20px;
      font: inherit;
      font-weight: normal;
      vertical-align: baseline;
    }

    .menuBtn {
      margin-top: 4vw;
      width: 17px;
      height: 13px;
      background: #fff;
    }
  </style>
</head>
<body>
<div id="head">
<div class="header">
<div class="logo">
<a href="//www.appcan.cn">
<img alt="" class="blueLogo" src="//www.appcan.cn/images/icon/logo_blue.png"/>
<img alt="" class="whiteLogo" src="//www.appcan.cn/images/icon/logo_white.png"/>
</a>
</div>
<div class="tab">
<ul>
<li>
<!-- <a href="../index.html?type='index'">首页</a> -->
<a href="//www.appcan.cn">首页</a>
</li>
<li>
<!-- <a href="../new_kaifahezi/index.html">开发盒子</a> -->
<a href="//www.appcan.cn/new_kaifahezi/index.html">开发盒子</a>
</li>
<li>
<!-- <a href="../new_pingtaitaojian/index.html">移动平台套件</a> -->
<a href="//www.appcan.cn/new_pingtaitaojian/index.html">移动平台套件</a>
</li>
<li>
<!-- <a href="../new_zhengqitong/index.html">政企通</a> -->
<a href="//www.appcan.cn/new_zhengqitong/index.html">政企通</a>
</li>
<li class="openList">
<a href="javascript:void(0);">开发者</a>
</li>
<li>
<!-- <a href="../new_hezuohuoban/index.html">合作伙伴</a> -->
<a href="//www.appcan.cn/new_hezuohuoban/index.html">合作伙伴</a>
</li>
<li>
<a href="//www.zyhao.com" target="_blank">正益工场</a>
</li>
</ul>
</div>
<div class="loginAndRegister">
<div class="noLogin">
<a href="//newsso.appcan.cn/login?service=http://appcan.cn/" target="_blank">登录</a>
<a href="//dashboard.appcan.cn/register" target="_blank">注册</a>
</div>
<div class="login ul_display_none">
<span></span>
<a class="logout" href="//dashboard.appcan.cn/user/logout" target="_blank">退出</a>
</div>
</div>
<div class="menuBtn">
<img alt="" src="//www.appcan.cn/images/icon/sidebar.png"/>
</div>
</div>
<!-- 下拉菜单 -->
<div class="listBox">
<ul>
<li>
<a href="//dashboard.appcan.cn/app/manage">
<p class="listTitle">开发管理</p>
</a>
<div class="listItem">
<a href="//dashboard.appcan.cn/app">
<p>我的应用</p>
</a>
<a href="//dashboard.appcan.cn/plugin">
<p>引擎插件</p>
</a>
<a href="//dashboard.appcan.cn/transfer">
<p>应用转移</p>
</a>
<a href="//newdocx.appcan.cn/IDE/summary">
<p>开发工具</p>
</a>
<a href="//dashboard.appcan.cn/portal">
<p>我的门户</p>
</a>
</div>
</li>
<li>
<a href="//plugin.appcan.cn">
<p class="listTitle">插件中心</p>
</a>
<div class="listItem">
<a href="//plugin.appcan.cn/index.html#recommend_plugin1">
<p>推荐插件</p>
</a>
<a href="//plugin.appcan.cn/plugin_list.html#plugin_list1">
<p>插件列表</p>
</a>
<a href="//plugin.appcan.cn/create.html#create_plugin1">
<p>创建插件</p>
</a>
<a href="//plugin.appcan.cn/my_share.html">
<p>我的分享</p>
</a>
</div>
</li>
<li>
<a href="//newdocx.appcan.cn">
<p class="listTitle">文档中心</p>
</a>
<div class="listItem">
<a href="//newdocx.appcan.cn/AppCan">
<p>平台概述</p>
</a>
<a href="//newdocx.appcan.cn/IDE/summary">
<p>开发工具IDE</p>
</a>
<a href="//newdocx.appcan.cn/app-engine/summary">
<p>应用引擎</p>
</a>
<a href="//newdocx.appcan.cn/plugin-API/manual">
<p>应用插件</p>
</a>
<a href="//newdocx.appcan.cn/UI/source">
<p>UI基础框架</p>
</a>
<a href="//newdocx.appcan.cn/JSSDK/summary">
<p>JS SDK</p>
</a>
<a href="//newdocx.appcan.cn/quickstart/create-app">
<p>开发指导</p>
</a>
<a href="//newdocx.appcan.cn/FAQs">
<p>常见问题</p>
</a>
</div>
</li>
<li>
<a href="//edu.appcan.cn/video1.html">
<p class="listTitle">在线视频</p>
</a>
<div class="listItem">
<a href="//edu.appcan.cn/video1.html?big_type=%u524D%u7AEF%u5F00%u53D1&amp;small_all=allon">
<p>前端开发</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u63A5%u53E3%u5F00%u53D1">
<p>接口开发</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u4E91%u7AEF%u5F00%u53D1">
<p>云端开发</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u79FB%u52A8%u7BA1%u7406%u5E73%u53F0">
<p>移动管理平台</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u9879%u76EE%u5B9E%u6218&amp;small_all=allon&amp;level=">
<p>项目实战</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u6B63%u76CA%u5DE5%u4F5C">
<p>正益工作</p>
</a>
<a href="//edu.appcan.cn/video1.html?big_type=%u73B0%u573A%u6F14%u8BB2">
<p>现场演讲</p>
</a>
</div>
</li>
<li>
<a href="//enterprise.appcan.cn/solution/329">
<p class="listTitle">案例中心</p>
</a>
<div class="listItem">
<a href="//www.appcan.cn/appshow">
<p>APP案例</p>
</a>
<a href="//enterprise.appcan.cn/solution/329">
<p>解决方案</p>
</a>
<a href="//enterprise.appcan.cn/solution/solution">
<p>客户名录</p>
</a>
<a href="//enterprise.appcan.cn/solution/85">
<p>经典案例</p>
</a>
</div>
</li>
<li>
<a href="//bbs.appcan.cn">
<p class="listTitle">社区中心</p>
</a>
<div class="listItem">
<a href="//bbs.appcan.cn">
<!-- <img src="//www.appcan.cn/images/icon/shequ.png" alt="" style="width:24px;height:24px;"> -->
<p>论坛</p>
</a>
<a href="//bbs.appcan.cn/home.php?mod=space&amp;do=blog">
<p>博客</p>
</a>
<a href="//edu.appcan.cn/">
<p>学院</p>
</a>
<a href="//edu.appcan.cn/Wanted.html">
<p>招聘</p>
</a>
</div>
</li>
</ul>
</div>
<!-- 移动端菜单 -->
<div class="menu-mobile">
<div class="panel-group" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<!-- <a href="../index.html?type='index'">首页</a> -->
<a href="//www.appcan.cn">首页</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<!-- <a href="../new_kaifahezi/index.html">开发盒子</a> -->
<a href="//www.appcan.cn/new_kaifahezi/index.html">开发盒子</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<!-- <a href="../new_pingtaitaojian/index.html">移动平台套件</a> -->
<a href="//www.appcan.cn/new_pingtaitaojian/index.html">移动平台套件</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<!-- <a href="../new_zhengqitong/index.html">政企通</a> -->
<a href="//www.appcan.cn/new_zhengqitong/index.html">政企通</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">开发者</a>
</h4>
</div>
<div class="panel-collapse collapse" id="collapseTwo">
<div class="panel-body" id="kaifazhe">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a href="//dashboard.appcan.cn/app/manage">开发管理</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a href="//plugin.appcan.cn">插件中心</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a href="//newdocx.appcan.cn">文档中心</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a href="//edu.appcan.cn/video1.html">在线视频</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a data-parent="#kaifazhe" data-toggle="collapse" href="#anliCenter">案例中心</a>
</h4>
</div>
<div class="panel-collapse collapse" id="anliCenter">
<div class="panel-body">
<a href="//www.appcan.cn/appshow">APP案例</a>
<a href="//enterprise.appcan.cn/solution/329">解决方案</a>
<a href="//enterprise.appcan.cn/solution/solution">客户名录</a>
<a href="//enterprise.appcan.cn/solution/85">经典案例</a>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a data-parent="#kaifazhe" data-toggle="collapse" href="#shequCenter">社区中心</a>
</h4>
</div>
<div class="panel-collapse collapse" id="shequCenter">
<div class="panel-body">
<a href="//bbs.appcan.cn">论坛</a>
<a href="//bbs.appcan.cn/home.php?mod=space&amp;do=blog">博客</a>
<a href="//edu.appcan.cn/">学院</a>
<a href="//edu.appcan.cn/Wanted.html">招聘</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<!-- <a href="../new_hezuohuoban/index.html">合作伙伴</a> -->
<a href="//www.appcan.cn/new_hezuohuoban/index.html">合作伙伴</a>
</h4>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a href="//www.zyhao.com" target="_blank">正益工场</a>
</h4>
</div>
</div>
</div>
</div>
</div>
<!-- 轮播图 -->
<div id="firstPage">
<img alt="" src="./images/icon/banner_01.png"/>
</div>
<!-- part01 -->
<div id="partBox">
<div class="box_01">
<div class="part" id="part01">
<h1>开发盒子</h1>
<p class="text">赋能开发者及合作伙伴高效、跨平台的移动开发及管理能力</p>
<div>
<img alt="" src="./images/icon/part_01_image01.png"/>
</div>
<h2>安全、高效、独有</h2>
<p class="text" style="margin-top:22px;">基于业内领先的 Hybrid App 开发引擎，采用 HTML5 标准作为开发语言，一次开发，多平台适配。
          提供团队管理、开发流程管理、配置管理、版本管理、测试管理等功能，有效、有序的控制开发过程，提升开发效率。</p>
<div class="learnMore">
<a href="./new_kaifahezi/index.html">了解更多</a>
</div>
</div>
</div>
<div class="box_02">
<div class="part" id="part02">
<h1>移动平台套件</h1>
<p class="text">赋能开发者及合作伙伴为大中型企业移动化提供一站式开发及运营管理平台</p>
<div>
<div class="content_02" style="float:left;">
<img alt="" src="./images/icon/part_02_image01.png"/>
<p class="text">门户基座</p>
<p class="text-pc">为企业搭建信用户聚合、应用聚合、消息聚合、内容聚合、统一登录为一体的企业移动综合门户</p>
<p class="text-mobile">企业移动综合门户</p>
</div>
<div class="content_02">
<img alt="" src="./images/icon/part_02_image02.png"/>
<p class="text">打包编译</p>
<p class="text-pc">一次开发，Android、iOS全适配，帮助企业降低开发成本、提升开发效率</p>
<p class="text-mobile">Android、iOS全适配</p>
</div>
<div class="content_02" style="float:right;">
<img alt="" src="./images/icon/part_02_image03.png"/>
<p class="text">一体化管理</p>
<p class="text-pc">提供对用户、对应用、对内容、对消息的综合管理服务为企业打造完善全面的移动管理体系</p>
<p class="text-mobile">完善全面的移动管理体系</p>
</div>
</div>
<div class="learnMore" style="margin-top: 2.5vw;">
<a href="./new_pingtaitaojian/index.html">了解更多</a>
</div>
</div>
</div>
<div class="box_01">
<div class="part" id="part03">
<h1>政企通</h1>
<p class="text">赋能合作伙伴为大型企事业单位移动化提供系统性、行业性的解决方案平台</p>
<div class="part03-pc">
<div class="content_03">
<div class="left_text">
<h2>原生基座</h2>
<p class="line"></p>
<p class="text">一键生成原生门户基座，配置专属首页模版,分级运营管理</p>
</div>
<div class="right_img">
<img alt="" src="./images/icon/part_03_image01.png"/>
</div>
</div>
<div class="content_03 content_03_part02">
<div class="right_img" style="width: 41.72vw;">
<img alt="" src="./images/icon/part_03_image02.png"/>
</div>
<div class="left_text" style="width: 32.4vw;">
<h2 style="margin-top:5vw;">统一管理</h2>
<p class="line"></p>
<p class="text">用户、组织、运营、权限中心统一管控，服务接入逐级分发</p>
</div>
</div>
<div class="content_03" style="margin-top:-5.55vw;">
<div class="left_text" style="margin-right:5vw;">
<h2 style="margin-top: 6.57vw;">微应用开发体系</h2>
<p class="line"></p>
<p class="text">基于Visual Studio Code 集成AppCan UI插件，提供移动端 Vue 组件库，包括AppCan UI基础工程、WIFI真机同步应用调试器等功能
              </p>
<div>
<a href="//tongplatform.appcan.cn:6067/open/docs/index.html#/zh-CN/intro">了解更多</a>
</div>
</div>
<div class="right_img" style="width: 41.72vw;">
<img alt="" src="./images/icon/part_03_image03.png"/>
</div>
</div>
<div class="content_03">
<div class="right_img" style="width: 41.72vw;">
<img alt="" src="./images/icon/part_03_image04.png"/>
</div>
<div class="left_text">
<h2 style="margin-top:4.57vw;">开放平台</h2>
<p class="line"></p>
<p class="text">支持H5、微应用、服务号、原生组件、Web接入，集团统一管控分发，实现开发运营的统一、可控</p>
<div>
</div>
</div>
</div>
</div>
<div class="part03-mobile">
<div>
<h2>原生基座</h2>
<p>一键生成原生门户基座，配置专属首页模版,分级运营管理</p>
<img alt="" src="./images/icon/part_03_image01.png"/>
</div>
<div>
<h2>统一管理</h2>
<p>用户、组织、运营、权限中心统一管控，服务接入逐级分发</p>
<img alt="" src="./images/icon/part_03_image02.png" style="margin-left: 60px"/>
</div>
<div>
<h2>微应用开发体系</h2>
<p>基于Visual Studio Code 集成AppCan UI插件，提供移动端 Vue 组件库，包括AppCan UI基础工程、WIFI真机同步应用调试器等功能</p>
<img alt="" src="./images/icon/part_03_image03.png"/>
</div>
<div>
<h2>开放平台</h2>
<p>支持H5、微应用、服务号、原生组件、Web接入，集团统一管控分发，实现开发运营的统一、可控</p>
<img alt="" src="./images/icon/part_03_image04.png"/>
</div>
</div>
<div class="learnMore" style="margin-top: 2.77vw;">
<a href="./new_zhengqitong/index.html">了解更多</a>
</div>
</div>
</div>
<div class="box_01">
<div id="part04">
<div id="companyBox">
<ul>
<li><img alt="" src="./images/icon/logo/logo_01.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_02.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_03.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_04.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_05.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_06.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_07.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_08.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_09.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_10.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_11.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_12.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_13.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_14.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_15.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_16.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_17.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_18.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_19.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_20.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_21.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_22.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_23.png"/></li>
<li><img alt="" src="./images/icon/logo/logo_01.png"/></li>
</ul>
</div>
</div>
</div>
</div>
<!--footer-->
<div class="footer">
<div class="firstBox">
<div class="Box">
<div class="copyright">
<a href="//www.appcan.cn">
<img alt="" src="//www.appcan.cn/images//header_logo.png"/>
</a>
<p>正益移动互联科技股份有限公司版权所有<br/><a href="https://beian.miit.gov.cn" target="_blank"><span>京ICP备11006447号-8</span></a></p>
<p><a href="//www.beian.gov.cn/portal/registerSystemInfo?recordcode=11010502033806">京公网安备：11010502033806号</a>
</p>
<!-- <p class="copy">© Copyright 2016 Virgo. All Rights Reserved</p> -->
</div>
<div class="telephone">
<p><img src="//www.appcan.cn/images/icon/icon_telephone.png"/>400-040-1766</p>
<p><img src="//www.appcan.cn/images/icon/icon_email.png"/>服务邮箱：service@zymobi.com</p>
</div>
<div class="OR-code">
<div class="codeBox">
<img alt="" src="//www.appcan.cn/images/icon/QRCode1.png"/>
<p>AppCan官方微信</p>
</div>
<!-- <div class="codeBox">
          <img src="//www.appcan.cn/images/icon/QRCode2.png" alt="">
          <p>正益工场官方微信</p>
        </div> -->
</div>
</div>
</div>
<div class="secondBox">
<div class="Box">
<a href="//www.appcan.cn/terms_service/aboutus.html" target="_blank">关于我们</a>
<span>|</span>
<a href="//www.appcan.cn/news/gfxw" target="_blank">新闻活动</a>
<span>|</span>
<a href="//www.appcan.cn/terms_service/joinus.html" target="_blank">诚聘英才</a>
<span>|</span>
<a href="//www.appcan.cn/terms_service/linkus.html" target="_blank">联系我们</a>
<span>|</span>
<a href="//www.appcan.cn/terms_service/lawstatement.html" target="_blank">法律声明</a>
<span>|</span>
<a href="//www.appcan.cn/terms_service/links.html" target="_blank">合作伙伴</a>
<span>|</span>
<a href="//www.appcan.cn/websiteMap/map.html" target="_blank">网站地图</a>
</div>
</div>
</div>
<script type="text/javascript">document.write(unescape("%3Cspan id='cnzz_stat_icon_1279098936'%3E%3C/span%3E%3Cscript src='https://s4.cnzz.com/z_stat.php%3Fid%3D1279098936%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));</script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?223526ad8bc0f52d4ca6fa19fc65bf8b";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(hm, s);
})();
</script>
</body>
</html>
