<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>AllinHindi.com » Interesting Facts, Hindi Stories, Thoughts, Quotes, ...</title>
<meta content="Interesting Facts, Hindi Stories, Thoughts, Quotes, ..." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://www.allinhindi.com/" rel="canonical"/>
<link href="https://www.allinhindi.com/page/2/" rel="next"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="AllinHindi.com » Interesting Facts, Hindi Stories, Thoughts, Quotes, ..." property="og:title"/>
<meta content="Interesting Facts, Hindi Stories, Thoughts, Quotes, ..." property="og:description"/>
<meta content="https://www.allinhindi.com/" property="og:url"/>
<meta content="AllinHindi.com" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="@BccFalna" name="twitter:site"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.allinhindi.com/#website","url":"https://www.allinhindi.com/","name":"AllinHindi.com","description":"Interesting Facts, Hindi Stories, Thoughts, Quotes, ...","potentialAction":[{"@type":"SearchAction","target":"https://www.allinhindi.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"CollectionPage","@id":"https://www.allinhindi.com/#webpage","url":"https://www.allinhindi.com/","name":"AllinHindi.com &raquo; Interesting Facts, Hindi Stories, Thoughts, Quotes, ...","isPartOf":{"@id":"https://www.allinhindi.com/#website"},"description":"Interesting Facts, Hindi Stories, Thoughts, Quotes, ...","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://www.allinhindi.com/"]}]},{"@type":"BreadcrumbList","@id":"https://www.allinhindi.com/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://www.allinhindi.com/","url":"https://www.allinhindi.com/","name":"Home"}}]}]}</script>
<link href="//platform-api.sharethis.com" rel="dns-prefetch"/>
<link href="//www.googletagmanager.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://feeds.feedburner.com/AllinHindi" rel="alternate" title="AllinHindi.com » Feed" type="application/rss+xml"/>
<link href="https://www.allinhindi.com/comments/feed/" rel="alternate" title="AllinHindi.com » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.allinhindi.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<script>
						advanced_ads_ready=function(){var fns=[],listener,doc=typeof document==="object"&&document,hack=doc&&doc.documentElement.doScroll,domContentLoaded="DOMContentLoaded",loaded=doc&&(hack?/^loaded|^c/:/^loaded|^i|^c/).test(doc.readyState);if(!loaded&&doc){listener=function(){doc.removeEventListener(domContentLoaded,listener);window.removeEventListener("load",listener);loaded=1;while(listener=fns.shift())listener()};doc.addEventListener(domContentLoaded,listener);window.addEventListener("load",listener)}return function(fn){loaded?setTimeout(fn,0):fns.push(fn)}}();
						</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.allinhindi.com/wp-content/themes/blognews-master/style.css" id="blognews-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.allinhindi.com/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.allinhindi.com/wp-content/plugins/sharethis-share-buttons/css/mu-style.css" id="share-this-share-buttons-sticky-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Oswald%3A400%2C700%7COpen+Sans%3A400%2C700" id="google-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.allinhindi.com/wp-includes/css/dashicons.min.css" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.allinhindi.com/wp-content/themes/blognews-master/style.css" id="blognews-dashicons-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.allinhindi.com/wp-content/themes/blognews-master/lib/font/genericons.css" id="blognews-genericons-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="share-this-share-buttons-mu-js" src="//platform-api.sharethis.com/js/sharethis.js#property=5c3f3e448366dd0011c2fa0b&amp;product=inline-buttons" type="text/javascript"></script>
<script id="jquery-core-js" src="https://www.allinhindi.com/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<script async="" id="google_gtagjs-js" src="https://www.googletagmanager.com/gtag/js?id=UA-81642141-1" type="text/javascript"></script>
<script id="google_gtagjs-js-after" type="text/javascript">
window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('set', 'developer_id.dZTNiMT', true);
gtag('config', 'UA-81642141-1', {"anonymize_ip":true} );
</script>
<script id="ga-external-tracking-js" src="https://www.allinhindi.com/wp-content/plugins/google-analyticator/external-tracking.min.js" type="text/javascript"></script>
<link href="https://www.allinhindi.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.allinhindi.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.allinhindi.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<meta content="Site Kit by Google 1.22.0" name="generator"/><link href="https://www.allinhindi.com/xmlrpc.php" rel="pingback"/>
<link href="https://www.allinhindi.com/wp-content/themes/blognews-master/modifiedstyle.css?1.2.3" id="modifiedstyle" rel="stylesheet"/>
<link href="https://www.allinhindi.com/favicon.ico" rel="icon" sizes="16x16"/>
<script async="" custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
</script>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-8115413496993665",
          enable_page_level_ads: true
     });
</script><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ffffff; }
</style>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><script>(adsbygoogle = window.adsbygoogle || []).push({"google_ad_client":"ca-pub-8115413496993665","enable_page_level_ads":true,"tag_partner":"site_kit"});</script>
<script type="text/javascript">
    var analyticsFileTypes = [''];
    var analyticsSnippet = 'disabled';
    var analyticsEventTracking = 'enabled';
</script>
<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-81642141-1', 'auto');
	ga('require', 'displayfeatures');
 
	ga('send', 'pageview');
</script>
<style id="wp-custom-css" type="text/css">
			.archive-pagination li {
    display: inline;
    margin: 0 1px;
}

h1, h2, h3, h4, h5, h6, .archive-title, .entry-title a {
    margin-bottom: 0px;
}

.entry-header .entry-meta {
    margin-bottom: 14px;
}

.adjacent-entry-pagination{
	display:none;
}

.breadcrumb {
    border-radius: 0px;
    padding: 10px 10px 5px;
    margin-bottom: 0px;
    border-top: 1px solid #dedede;
    border-left: 1px solid #dedede;
    border-right: 1px solid #dedede;
}		</style>
</head>
<body class="home blog custom-background header-full-width sidebar-content genesis-breadcrumbs-visible genesis-footer-widgets-hidden aa-prefix-adsen-" data-rsssl="1"><div class="site-container"><nav aria-label="Main" class="nav-primary"><div class="wrap"><ul class="menu genesis-nav-menu menu-primary" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.allinhindi.com/" style="padding-left: 0;" title="allinhindi.com"><b style="font-size: 20px;">AllinHindi.com</b></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5693" id="menu-item-5693"><a href="https://www.allinhindi.com/amazing-interesting-facts/"><span>Interesting Facts</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5688" id="menu-item-5688"><a href="https://www.allinhindi.com/hindi-stories-%e0%a4%95%e0%a4%b9%e0%a4%be%e0%a4%a8%e0%a4%bf%e0%a4%af%e0%a4%be%e0%a4%82/"><span>Stories</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5687" id="menu-item-5687"><a href="https://www.allinhindi.com/hindi-quotes-%e0%a4%85%e0%a4%a8%e0%a4%ae%e0%a5%8b%e0%a4%b2-%e0%a4%b5%e0%a4%9a%e0%a4%a8/"><span>Quotes</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5684" id="menu-item-5684"><a href="https://www.allinhindi.com/dohe-sanskrit-slokas-%e0%a4%a6%e0%a5%8b%e0%a4%b9%e0%a5%87-%e0%a4%b8%e0%a4%82%e0%a4%b8%e0%a5%8d%e2%80%8d%e0%a4%95%e0%a5%83%e0%a4%a4-%e0%a4%b6%e0%a5%8d%e2%80%8d%e0%a4%b2%e0%a5%8b%e0%a4%95/"><span>Dohe</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5683" id="menu-item-5683"><a href="https://www.allinhindi.com/learn-blogging-in-hindi/"><span>Blogging</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5685" id="menu-item-5685"><a href="https://www.allinhindi.com/jyotish-%e0%a4%9c%e0%a5%8d%e0%a4%af%e0%a5%8b%e0%a4%a4%e0%a4%bf%e0%a4%b7/"><span>Jyotish</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5686" id="menu-item-5686"><a href="https://www.allinhindi.com/festivals-%e0%a4%a4%e0%a5%8d%e2%80%8d%e0%a4%af%e0%a5%8b%e0%a4%b9%e0%a4%be%e0%a4%b0/"><span>Festivals</span></a></li>
</ul></div></nav><header class="site-header"><div class="wrap"><div class="title-area"><h1 class="site-title"><a href="https://www.allinhindi.com/">AllinHindi.com</a></h1><p class="site-description">Interesting Facts, Hindi Stories, Thoughts, Quotes, ...</p></div></div></header><div class="site-inner"><div class="content-sidebar-wrap"><main class="content"><div class="breadcrumb"><span><span aria-current="page" class="breadcrumb_last">Home</span></span></div><article aria-label="Arnold Schwarzenegger – Motivational Thoughts in Hindi" class="post-11195 post type-post status-publish format-standard has-post-thumbnail category-inspirational-people entry"><header class="entry-header"><h2 class="entry-title"><a class="entry-title-link" href="https://www.allinhindi.com/arnold-schwarzenegger-motivational-thoughts-in-hindi/" rel="bookmark">Arnold Schwarzenegger – Motivational Thoughts in Hindi</a></h2>
<p class="entry-meta"><time class="entry-time">September 29, 2020</time> by <span class="entry-author"><a class="entry-author-link" href="https://www.allinhindi.com/author/allinhindi/" rel="author"><span class="entry-author-name">ADMIN</span></a></span> <span class="entry-comments-link"><a href="https://www.allinhindi.com/arnold-schwarzenegger-motivational-thoughts-in-hindi/#respond">Leave a Comment</a></span> </p></header><div class="entry-content"><a aria-hidden="true" class="entry-image-link" href="https://www.allinhindi.com/arnold-schwarzenegger-motivational-thoughts-in-hindi/" tabindex="-1"><img alt="Arnold Schwarzenegger - Motivational Thoughts in Hindi" class="alignright post-image entry-image" height="150" loading="lazy" sizes="(max-width: 150px) 100vw, 150px" src="https://www.allinhindi.com/wp-content/uploads/2020/09/Arnold-Schwarzenegger-Motivational-Thoughts-in-Hindi-150x150.jpg" srcset="https://www.allinhindi.com/wp-content/uploads/2020/09/Arnold-Schwarzenegger-Motivational-Thoughts-in-Hindi-150x150.jpg 150w, https://www.allinhindi.com/wp-content/uploads/2020/09/Arnold-Schwarzenegger-Motivational-Thoughts-in-Hindi-120x120.jpg 120w, https://www.allinhindi.com/wp-content/uploads/2020/09/Arnold-Schwarzenegger-Motivational-Thoughts-in-Hindi-80x80.jpg 80w" width="150"/></a><p style="text-align: justify;"><strong>Arnold Alois Schwarzenegger</strong> 30 July 1947 को जन्‍में एक Austrian-American Actor, Businessman, भूतपूर्व Politician और एक Professional Body-Builder हैं, जो कि California के 38<sup>th</sup> Governor थे।</p>
<p style="text-align: justify;">इन्‍होंने 15 साल की उम्र से ही Weight Lifting करना शुरू कर दिया था और 20 वर्ष की उम्र में Mr. Universe का खिताब जीतने के बाद 7 बार Mr. Olympia Contest जीता। इन्‍होंने Sports पर कई किताबें भी लिखीं। इन्‍हें पूरी दुनियॉं में Body-Building के क्षैत्र में बहुत ख्‍याति प्राप्‍त होने के साथ ही ये Hollywood की कई Action Films के Hero भी रहे हैं।</p>
<p style="text-align: justify;">एक Weight Lifter, Body-Builder, Actor, Businessman से लेकर Successful Politician बनने तक का लम्‍बा सफर तय करने वाले Arnold Schwarzenegger के जीवन के निम्‍नानुसार कुछ अमूल्‍य अनुभवों को जानना हमारे लिए भी निश्चित रूप से काफी उपयोगी साबित हो सकता है-</p>
<ul>
<li>वे ही लोग सफल होते हैं, जिनके जीवन में कोई Clear Vision, Clear Goal, Clear Purpose होता है।</li>
<li>जो लोग आपको अपने काम पर अपनी क्षमताओं पर अपनी इच्‍छाओं को Fulfill करने पर आपके Goal या Vision को Achieve करने की आपकी क्षमता पर Doubt करते हैं और आपमें भी Doubt पैदा करते हैं, उन लोगों को सुनना बन्‍द कीजिए। उन्‍हें अपने से दूर कर दीजिए।</li>
</ul>
<p style="padding-left: 40px;">क्‍योंकि लोग आपकी क्षमताओं के बारे में बात नहीं कर रहे हैं, वे अपने खुद की क्षमताओं के बारे में बात कर रहे हैं और इसीलिए वे वो हासिल नहीं कर पाऐ हैं, जो आप हासिल करना चाहते हैं और जो स्‍वयं सफल नहीं हैं, उनकी सलाह मानने की, उनकी बात सुनने की कोई जरूरत नहीं है। उनसे दूर हो जाना ही आपके लिए सबसे ज्‍यादा अच्‍छा है।</p>
<ul>
<li>दुनियॉं कोई Magic नहीं है जो आपके लिए सफलता को अपने आप बना दे। केवल आपका काम ही वह Magic है, जो आपको सफल बनाता है। इसलिए आप जो भी कुछ हासिल करना चाहते हैं, उसके लिए काम कीजिए और जब आप काम करेंगे, आप उसे हासिल कर लेंगे लेकिन अगर आप केवल उसके बारे में सोंचेंगे और कोई काम नहीं करेंगे, कोई Action नहीं लेंगे, तो आप उसे हासिल नहीं कर सकते क्‍योंक‍ि<strong> Action </strong><strong>ही</strong> <strong>Success </strong><strong>का</strong> <strong>रास्‍ता</strong> <strong>है</strong>। जो Action नहीं लेता, वो Success पर नहीं पहुंचता और Action लेना काफी Hard-Work होता है।</li>
<li>कोई बहाना नहीं चलेगा। जो करना है, उसे कीजिए और अगर नहीं करते, तो असफल होने का दोष दूसरों को मत दीजिए क्‍योंकि तब अगर आपको वो नहीं मिलता, जो आप हासिल करना चाहते हैं, तो वो केवल इसलिए क्‍योंकि आपने वो नहीं किया, जो आपको करना चाहिए था।</li>
<li>आप जो भी कुछ पाना चाहते हैं, उसके लिए जो भी कुछ करना है, उसके लिए पहले अपनी तरफ से जीतोड़ मेहनत कर लीजिए और फिर अपने Achievements की <strong>Advertising </strong><strong>कीजिए</strong> क्‍योंकि जरूरी नहीं है कि आप जो कर रहे हैं, लोग समझ ही जाऐं कि आप क्‍या कर रहे हैं। अक्‍सर हमें खुद अपनी तरफ से बताना जरूरी होता है कि हम क्‍या Special कर रहे हैं, ताकि दूसरे लोग Notice कर सकें।</li>
<li><strong>Backup Plans </strong><strong>मत</strong> <strong>रखिए।</strong> Deadly Successful लोग कभी कोई Backup Plan नहीं रखते, जिसकी वजह से सफल होना उनकी इच्‍छा नहीं मजबूरी बन जाता है और मजबूरी में इन्‍सान अपनी Limits से कहीं ज्‍यादा बाहर जाकर वो करता है, जिसके बारे में वह Normal Situations में सोंच तक नहीं सकता। इसलिए जब आपके पास कोई Second Option नहीं रहेगा, कोई Safety Plan नहीं रहेगा, तब आप अपनी वास्‍तविक क्षमता के साथ Perform कर सकेंगे।</li>
</ul>
<p style="padding-left: 40px;">जब तक आपके दिमाग में Plan B रहता है, तब तक आप अपने दिमाग को ये कह रहे होते हैं कि उसे अपने Plan A पर 100% Effort लगाने की जरूरत नहीं है और हमारा दिमाग कभी भी अपनी तरफ से कोई मेहनत करना नहीं चाहता। वो हमेंशा ही Comfort में रहना चाहता है, क्‍योंकि उसे केवल Survive करने के लिए Design किया गया है और वह Survive करने से आगे के बारे में सोंचता ही नहीं है।</p>
<p style="padding-left: 40px;">इसलिए सारे Plan B को हमेंशा के लिए समाप्‍त कर दीजिए, फिर जो बचेगा वो केवल Plan A होगा और तब आपका दिमाग Survival Mode में आ जाएगा। उसे लगेगा कि अब अगर Plan A सफल न हुआ, तो उसका Comfort समाप्‍त हो जाएगा और हमारा दिमाग जैसे ही ये मान लेता है कि अब Survival पर भी खतरा आ गया है, वह Plan A को अपने 100% Efforts के साथ सफल करने के Solutions देना शुरू कर देता है।</p>
<p style="padding-left: 40px;">जब Safety Net के रूप में हमारे पास कोई Plan B नहीं होता, तब हमारा दिमाग और ज्‍यादा सावधान हो जाता है क्‍योंकि उसे पता है कि कोई Safety Net है ही नहीं, इसलिए अगर Fail होकर गिरे, तो बचाने के लिए कोई Plan B नहीं है। और इस तरह की <strong>करो</strong> <strong>या</strong> <strong>मरो</strong> वाली स्थिति में हमारा दिमाग ऐसे Solutions देता है, जिसकी सामान्‍य परिस्थितियों में कल्‍पना भी नहीं की जा सकती।</p>
<ul>
<li>लोगों के पास हमेंशा Plan B केवल इसीलिए होता है क्‍योंकि वे Fail होने से डरते हैं और जो Fail होने से डरते हैं, वे कुछ नया Try करने से भी डरते हैं, जिसके परिणामस्‍वरूप वे हमेंशा <strong>कुंऐ</strong> <strong>के</strong> <strong>मेंढ़क</strong> बनकर अपने Comfort Zone से बाहर निकलने की हिम्‍मत ही नहीं कर पाते और इतने बड़े संसार से वंचित रह जाते हैं।</li>
<li>जब कोई Safety Net नहीं होता, तब हम ज्‍यादा Better Perform करते हैं।</li>
<li>Fail होना कोई गलत काम नहीं है। असफल होना Normal है। लेकिन असफल हो कर रूक जाना अपराध है, जिसकी हमेंशा असफलता के रूप में सजा भोगनी पड़ती है।</li>
<li>असफल होना आगे बढ़ने का सबसे पहला Step है। असफल होने से हमें ये पता चल जाता है कि हमने जो किया वो सही नहीं था, हमें कुछ और Try करने की जरूरत है।</li>
<li>Winners, Fail होने के बाद फिर से खड़े हो जाते हैं, जबकि जो Fail होने के बाद फिर से खड़े होने का प्रयास ही नहीं करते, वे Looser होते हैं। फिर से प्रयास करने वाला कभी Looser नहीं होता।</li>
<li>सफल लोग तब तक लड़ते रहते हैं, जब तक कि मर नहीं जाते या फिर जीत नहीं जाते।</li>
<li>असफल होने से मत डरिए क्‍योंकि आपको लगता है कि असफल होने से आप कुछ 100% खो देंगे। जबकि सत्‍य ये है कि हम कभी भी कुछ भी 100% नहीं खोते। हां, कुछ नया Try करने में 50% – 60% जरूर खो देते हैं, लेकिन पूरा 100% कभी नहीं खोते और यदि हम Fail हो भी जाऐं, तब भी जो बचा हुआ 30% – 40% होता है, उतने में भी Survive करना सम्‍भव होता है जिसे फिर से प्रयास करके बढ़ाया जा सकता है।</li>
</ul>
<p style="padding-left: 40px;">जब आप कुछ 100% खो जाने के डर से कोई नया प्रयास भी नहीं करते, तब आप सिकुड़ जाते हैं, सीमित हो जाते हैं, अपनी सम्‍भावनाओं को नकार देते हैं, एक कुंऐ के मेंढ़क बनकर रहे जाते हैं, पिंजरे में कैद हो जाते हैं। लेकिन यदि आप 100% खो जाने की सम्‍भावना के बावजूद भी कुछ नया प्रयास करने का जोखिम उठाते हैं, तो आप जीवन को उसकी परिपूर्णता में जीते हैं, उसे उसकी सम्‍भावनाओं में विकसित होने, खिलने, पोषित होने का Space देते हैं।</p>
<ul>
<li>जब तक लोग ये मानते हैं कि आप वो Achieve नहीं कर सकते, जो करना चाहते हैं, तब तक बहुत सम्‍भावना है कि आप वो Achieve कर लेंगे। लेकिन जब आप स्‍वयं ये मानते लगते हैं कि आप वो Achieve नहीं कर सकते, जो करना चाहते हैं, तब आप निश्चित कर देते हैं कि आप वो कभी Achieve नहीं कर पाऐंगे। इसलिए जो लोग आपको आपकी क्षमताओं पर शक करवाते हैं, उनसे तुरन्‍त दूर हो जाईए ताकि आप स्‍वयं अपनी क्षमताओं पर शक करने से बच सकें।</li>
</ul>
<p style="padding-left: 40px;">क्‍योंकि यदि आप बार-बार ये सुनेंगे कि आप वो नहीं कर सकते, जो करना चाहते हैं, तो एक दिन आप खुद भी मान लेंगे कि आप वो नहीं कर सकते, जो करना चाहते हैं और वही दिन आपकी असफलता वाला दिन होगा, क्‍योंकि आप उसी दिन Fail होंगे। जब तक युद्ध चालू है, तब तक हमारी हार नहीं होती, हार उसी दिन होती है, जिस दिन हम लड़ना बन्‍द करके समर्पण कर देते हैं।</p>
<ul>
<li>आप खुद को तभी Complete और संतुष्‍ट महसूस करते हैं, जब आपके पास जो भी कुछ है, आप वो किसी दूसरे को देते हैं या दूसरों को वही पाने में मदद करते हैं, जिन्‍हें उसकी जरूरत है।</li>
</ul>
<p><strong>Arnold Alois Schwarzenegger </strong>के ये Life Changing Thoughts मेरे लिए काफी Motivational रहे। उम्‍मीद है, इन में से किसी न किसी Thought ने आपको भी जरूर प्रभावित किया होगा। इसलिए यदि आपको भी ये Post अच्‍छा लगा हो, तो अपने Friends, Followers के साथ Like व Share जरूर कीजिए।</p>
<div class="sharethis-inline-share-buttons" style="margin-top: 0px; margin-bottom: 0px;"></div></div><footer class="entry-footer"><p class="entry-meta"><span class="entry-categories">Filed Under: <a href="https://www.allinhindi.com/category/inspirational-people/" rel="category tag">Inspirational People</a></span> </p></footer></article><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-8115413496993665" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="9161956883" style="display:block; text-align:center;"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script><div class="archive-pagination pagination"><ul><li class="active"><a aria-current="page" aria-label="Current page" href="https://www.allinhindi.com/">1</a></li>
<li><a href="https://www.allinhindi.com/page/2/">2</a></li>
<li><a href="https://www.allinhindi.com/page/3/">3</a></li>
<li class="pagination-omission">…</li>
<li><a href="https://www.allinhindi.com/page/440/">440</a></li>
<li class="pagination-next"><a href="https://www.allinhindi.com/page/2/">Next Page »</a></li>
</ul></div>
</main><aside aria-label="Primary Sidebar" class="sidebar sidebar-primary widget-area" role="complementary"><section class="widget adsen-widget" id="advads_ad_widget-4"><div class="widget-wrap"><div id="adsen-1955988690" style="margin-left: auto; margin-right: auto; text-align: center; margin-bottom: 20px; "><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-8115413496993665" data-ad-format="fluid" data-ad-layout="in-article" data-ad-slot="9161956883" style="display:block; text-align:center;"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script></div></div></section>
<section class="widget adsen-widget"><div class="widget-wrap"><div class="adsen-insidebar" id="adsen-1890869922" style="margin-left: auto; margin-right: auto; text-align: center; margin-top: 15px; margin-bottom: 15px; "><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-8115413496993665" data-ad-slot="6648949631" style="display:inline-block;width:336px;height:280px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div></div></section>
<section class="widget widget_text" id="text-31"><div class="widget-wrap"><h4 class="widget-title widgettitle">People like also…</h4>
<div class="textwidget"><div class="arpw-random-post ">
<ul class="arpw-ul">
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/financial-literacy-in-india/" rel="bookmark noopener noreferrer" target="_blank">ज्‍यादातर भारतीय पैसों के मामले में नासमझ हैं।</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/what-is-the-purpose-of-a-budget/" rel="bookmark noopener noreferrer" target="_blank">महंगाई से बचना है, तो Home Budget बनाईए।</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/regular-vs-direct-mutual-fund/" rel="bookmark noopener noreferrer" target="_blank">25% तक प्रभावित हो सकता है आपका Return</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/types-of-budget-%e0%a4%95%e0%a5%81%e0%a4%9b-%e0%a4%9c%e0%a4%b0%e0%a5%82%e0%a4%b0%e0%a5%80-%e0%a4%ac%e0%a4%be%e0%a4%a4%e0%a5%87%e0%a4%82/" rel="bookmark noopener noreferrer" target="_blank">सभी तरह के लोगों के लिए एक जैसा नहीं होता Home Budget.</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/importance-of-home-budget-%e0%a4%ac%e0%a4%9c%e0%a4%9f/" rel="bookmark noopener noreferrer" target="_blank">सपने साकार करने हैं, तो अपने घर का मासिक बजट बनाईए</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/what-is-budget-in-hindi/" rel="bookmark noopener noreferrer" target="_blank">क्‍या होता है बजट? जानिए सबकुछ, आसान उदाहरण द्वारा।</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/financial-planning-process/" rel="bookmark noopener noreferrer" target="_blank">Financial Goals Achieve होते हैं सही Investment से</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/what-is-debt-funds/" rel="bookmark noopener noreferrer" target="_blank">Invest करने से पहले समझें, क्‍या होते हैं Debt Mutual Funds?</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/types-of-bank-accounts/" rel="bookmark noopener noreferrer" target="_blank">क्‍यों होते हैं 4 अलग तरह के Bank Accounts?</a></li>
<li class="arpw-li arpw-clearfix"><a class="arpw-title" href="http://www.befinanceguru.com/difference-between-simple-interest-and-compound-interest/" rel="bookmark noopener noreferrer" target="_blank">कैसे Interest Charge करते हैं Banks?</a></li>
</ul>
</div></div>
</div></section>
<section class="widget widget_text" id="text-5"><div class="widget-wrap"><h4 class="widget-title widgettitle">Sponsored By</h4>
<div class="textwidget"><div>
<a href="http://www.bccfalna.com/ebooks-price/" rel="noopener noreferrer" target="_blank">
<img alt="BccFalna.com: TechTalks in Hindi. Be with Us, Be ahead" src="https://www.bccfalna.com/BccFalnaAD.png" style="width: 336px;" title="BccFalna.com: TechTalks in Hindi. Be with Us, Be ahead"/>
</a>
</div></div>
</div></section>
</aside></div></div>
<div style="text-align:center;margin:0 auto;padding:10px 0 0;    font-size: 13px;">
| <span><a href="https://www.allinhindi.com/about-us/" style="font-weight: normal;">About Us</a></span> |
<span><a href="https://www.allinhindi.com/contact-us/" style="font-weight: normal;">Contact Us</a></span> |
<span><a href="https://www.allinhindi.com/privacy-policy/" style="font-weight: normal;">Privacy Policy</a></span> |
<span><a href="https://www.allinhindi.com/terms-conditions/" style="font-weight: normal;">Terms &amp; Conditions and Disclaimer</a></span> |
<br/>
<span style="color: #999999;">Copyright © 2016 - 2019 to <a href="https://www.allinhindi.com/"><b>AllinHindi.com</b></a></span> </div>
</div><footer class="site-footer"><div class="wrap"><p>Copyright © 2021 · <a href="http://wpcanada.ca/our-themes/blognews/">BlogNews Theme</a> on <a href="https://www.studiopress.com/">Genesis Framework</a> · <a href="https://wordpress.org/">WordPress</a> · <a href="https://www.allinhindi.com/wp-login.php" rel="nofollow">Log in</a></p></div></footer>
<script type="text/javascript">
var sc_project=10550061; 
var sc_invisible=1; 
var sc_security="3b40c172"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript async type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a href="http://statcounter.com/shopify/" target="_blank" title="shopify
analytics"><img alt="shopify analytics" class="statcounter" src="https://c.statcounter.com/10550061/0/3b40c172/1/"/></a></div></noscript>
<script async="" src="https://wms-in.amazon-adsystem.com/20070822/IN/js/link-enhancer-common.js?tag=bccfalcom-21" type="text/javascript">
</script>
<noscript>
<img alt="" src="https://wms-in.amazon-adsystem.com/20070822/IN/img/noscript.gif?tag=bccfalcom-21"/>
</noscript><script type="text/javascript">
jQuery("document").ready(function(){
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 104) {
            jQuery('.nav-primary').addClass("f-nav");
            jQuery('#wpadminbar').css('display','none');
        } else {
            jQuery('.nav-primary').removeClass("f-nav");
            jQuery('#wpadminbar').css('display','block');
        }
    });
});
</script>
<script>
function adBlockFunction()
{
// Google Analytics Tracking 
setTimeout(function() { 
ga('send', 'event', 'Blocker', 'click','Blocker');
},5000);
// Google Analytics End

// Website Users
document.getElementById('content').innerHTML = 'Please disable your ad blocker or visit this site on Google Chrome Browser.';
}
</script>
<div class="box">
<div class="bet_time" id="test">
</div>
</div>
<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent) ) {
var currentURL = window.location.href;
var titleText = document.getElementsByTagName("title")[0].innerHTML;
document.getElementById("test").innerHTML = "<a href='whatsapp://send?text= " + currentURL + " %0A%0A " + titleText + "'><img src='https://www.allinhindi.com/wp-content/uploads/2017/01/Whatsapp-icon.png' /></a><a href='javascript:void(0);' id='dummy'></a>";
document.getElementById("dummy").click();
} else {
document.getElementById("test").innerHTML = "<a href='javascript:void(0);' id='dummy2'></a>";
document.getElementById("dummy2").click();
}
</script>
<style>
.bet_time { position:fixed; bottom:10px; left:10px; z-index: 100; }
</style>
<amp-auto-ads data-ad-client="ca-pub-8115413496993665" type="adsense">
</amp-auto-ads><script id="blognews-responsive-menu-js" src="https://www.allinhindi.com/wp-content/themes/blognews-master/lib/js/responsive-menu.js" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.allinhindi.com/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
</body></html>
