<!DOCTYPE html>
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Page not found | S&amp;T SIRI Voluntary Organisation</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://sirivo.org/wp-content/uploads/2016/11/fav-3.png" rel="shortcut icon"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://sirivo.org/xmlrpc.php" rel="pingback"/>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://sirivo.org/wp-content/themes/siri/js/ie10-viewport-bug-workaround.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://sirivo.org/wp-content/themes/siri/js/html5shiv.js"></script>
	<script src="https://sirivo.org/wp-content/themes/siri/js/respond.min.js"></script>
    <![endif]-->
<link href="https://sirivo.org/feed/" rel="alternate" title="S&amp;T SIRI Voluntary Organisation » Feed" type="application/rss+xml"/>
<link href="https://sirivo.org/comments/feed/" rel="alternate" title="S&amp;T SIRI Voluntary Organisation » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/sirivo.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.24"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;return g&&g.fillText?(g.textBaseline="top",g.font="600 32px Arial","flag"===a?(g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3):"diversity"===a?(g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e):("simple"===a?g.fillText(h(55357,56835),0,0):g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://sirivo.org/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.5.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/plugins/revslider/rs-plugin/css/settings.css?ver=4.6.5" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link href="https://sirivo.org/wp-content/themes/siri/bootstrap/css/bootstrap.min.css?ver=8.2" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/fontawesome/css/font-awesome.min.css?ver=8.2" id="fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/css/animate.min.css?ver=8.2" id="animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/js/flexslider/flexslider.css?ver=8.2" id="flexslider_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/js/fancyBox/jquery.fancybox.css?ver=8.2" id="fancyBox_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/js/fancyBox/helpers/jquery.fancybox-thumbs.css?ver=8.2" id="fancyBox_helper_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://sirivo.org/wp-content/uploads/alterna/alterna-styles.css?ver=30" id="alterna_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sirivo.org/wp-content/themes/siri/style.css?ver=8.2" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Oswald%3A400%7COpen+Sans%3A400%2C400italic%2C300%2C300italic%2C700%2C700italic&amp;ver=4.4.24" id="custom-theme-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://sirivo.org/wp-content/uploads/js_composer/custom.css?ver=4.3.4" id="js_composer_custom_css-css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://sirivo.org/wp-includes/js/jquery/jquery.js?ver=1.11.3" type="text/javascript"></script>
<script src="https://sirivo.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?ver=4.6.5" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?ver=4.6.5" type="text/javascript"></script>
<link href="https://sirivo.org/wp-json/" rel="https://api.w.org/"/>
<link href="https://sirivo.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://sirivo.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.4.24" name="generator"/>
<script type="text/javascript">
			jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '64a04cb94a';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"https://sirivo.org/wp-admin/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
		</script>
<meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="https://sirivo.org/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]--></head>
<body class="error404 boxed-layout wpb-js-composer js-comp-ver-4.3.4 vc_responsive">
<div class="wrapper">
<div class="header-wrap">
<div id="header-topbar">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6">
<div id="header-topbar-left-content">
<div class="topbar-element custom-content"><p>Phone: 9948170538 | sirisrinu.e@gmail.com</p></div> </div>
</div>
<div class="col-md-6 col-sm-6">
<div id="header-topbar-right-content">
<div class="topbar-element"><ul class="topbar-socials"> <li class="social"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li> <li class="social"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li> <li class="social"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li> <li class="social"><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li></ul></div> </div>
</div>
</div>
</div>
</div>
<header class="header-style-1">
<div class="header-fixed-enabled" id="alterna-header">
<div class="container">
<div class="logo">
<a href="https://sirivo.org" title="S&amp;T SIRI Voluntary Organisation"> <img alt="logo" class="logo-default" height="60" src="http://sirivo.org/wp-content/uploads/2016/11/logo.png" width="227"/>
<img alt="logo" class="logo-retina" height="60" src="http://sirivo.org/wp-content/uploads/2016/11/logo.png" width="227"/>
</a>
</div>
<div class="header-custom-container">
<marquee direction="left" onmousedown="4" scrollamount="4" style="text-align:center">
<h2 style="font-size:18px; color:red; font-weight:bold">Membership Form <a href="http://sirivo.org/wp-content/uploads/2018/07/membership-form.pdf" target="_blank">click Here for Download</a></h2>
</marquee> </div>
</div>
</div>
<!-- mobile show drop select menu -->
<div class="navbar" id="alterna-drop-nav">
<div class="navbar-inverse" id="alterna-nav-menu-select">
<button class="btn btn-navbar collapsed" data-target=".nav-collapse" data-toggle="collapse" type="button">
<span class="fa fa-bars"></span>
</button>
<div class="nav-collapse collapse"><ul class="nav"></ul></div>
</div>
<div class="alterna-nav-form-container container">
<div class="alterna-nav-form">
<form action="https://sirivo.org/" class="searchform" method="get" role="search">
<div>
<input class="sf-s" name="s" placeholder="Search" type="text"/>
<input class="sf-searchsubmit" type="submit" value=""/>
</div>
</form>
</div>
</div>
</div>
<!-- menu & search form -->
<nav id="alterna-nav">
<div class="container">
<div class="fixed-logo">
<a href="https://sirivo.org" title="S&amp;T SIRI Voluntary Organisation"></a>
</div>
<div class="alterna-nav-menu-container"><ul class="alterna-nav-menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11" id="menu-item-11"><a href="https://sirivo.org/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-122" id="menu-item-122"><a href="#">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123" id="menu-item-123"><a href="https://sirivo.org/about-us/about-siri-vo/">About SIRI VO</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-126" id="menu-item-126"><a href="https://sirivo.org/about-us/mission/">Mission</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127" id="menu-item-127"><a href="https://sirivo.org/about-us/vision/">Vision</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-124" id="menu-item-124"><a href="https://sirivo.org/about-us/managment/">Managment</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-125" id="menu-item-125"><a href="https://sirivo.org/about-us/members-of-our-board/">Members of Our Board</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-128" id="menu-item-128"><a href="https://sirivo.org/programs/">Programs</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-129" id="menu-item-129"><a href="https://sirivo.org/programs/farmer-clubs/">Farmer Clubs</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-132" id="menu-item-132"><a href="https://sirivo.org/programs/farmer-clubs/inaugurations/">Inaugurations</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://sirivo.org/programs/farmer-clubs/base-level-oriention-training-program-blotp/">Base Level Oriention Training Program (BLOTP)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-133" id="menu-item-133"><a href="https://sirivo.org/programs/farmer-clubs/meet-with-experts/">Meet With Experts</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-131" id="menu-item-131"><a href="https://sirivo.org/programs/farmer-clubs/cat-exposure-visits/">CAT Exposure Visits</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-134" id="menu-item-134"><a href="https://sirivo.org/programs/rural-hats/">Rural Hats</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135" id="menu-item-135"><a href="https://sirivo.org/programs/rural-hats/nalgonda/">Nalgonda</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136" id="menu-item-136"><a href="https://sirivo.org/programs/rural-hats/warangal/">Warangal</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-138" id="menu-item-138"><a href="https://sirivo.org/programs/science-exhibition/">Science Exhibition</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-139" id="menu-item-139"><a href="https://sirivo.org/programs/sdi/">SDI</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-137" id="menu-item-137"><a href="https://sirivo.org/programs/one-day-leadership-program/">One Day Leadership Program</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-140" id="menu-item-140"><a href="https://sirivo.org/programs/other-programs/">Other Programs</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143" id="menu-item-143"><a href="https://sirivo.org/programs/other-programs/water-soil-program/">Water &amp; Soil Program</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-141" id="menu-item-141"><a href="https://sirivo.org/programs/other-programs/health-camps/">Health Camps</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-142" id="menu-item-142"><a href="https://sirivo.org/programs/other-programs/residential-special-training-center-program-rstcp/">Residential Special Training Center Program (RSTCP)</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-144" id="menu-item-144"><a href="https://sirivo.org/gallery/">Gallery</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-698" id="menu-item-698"><a href="#">ARISE 2016</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-699" id="menu-item-699"><a href="https://sirivo.org/gallery/distinguished-scientists/">Distinguished Scientists</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-700" id="menu-item-700"><a href="https://sirivo.org/gallery/lifetine-ach-awarad/">Lifetine Ach. Awarad</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-701" id="menu-item-701"><a href="https://sirivo.org/gallery/young-scientists/">Young Scientists</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-706" id="menu-item-706"><a href="#">Farmer Club Program</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-704" id="menu-item-704"><a href="https://sirivo.org/gallery/innagarations/">Innagarations</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-702" id="menu-item-702"><a href="https://sirivo.org/gallery/blotp/">BLOTP</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-703" id="menu-item-703"><a href="https://sirivo.org/gallery/cat-exposure-visits/">CAT Exposure Visits</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-705" id="menu-item-705"><a href="https://sirivo.org/gallery/meet-with-experts/">Meet With Experts</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-707" id="menu-item-707"><a href="#">Rural Hats Program</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-708" id="menu-item-708"><a href="https://sirivo.org/gallery/nalgonda/">Nalgonda</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-709" id="menu-item-709"><a href="https://sirivo.org/gallery/warangal/">Warangal</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-711" id="menu-item-711"><a href="https://sirivo.org/gallery/science-exhibition/">Science Exhibition</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-712" id="menu-item-712"><a href="https://sirivo.org/gallery/sdi-program/">SDI Program</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-710" id="menu-item-710"><a href="https://sirivo.org/gallery/oneday-leadership/">Oneday Leadership</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-716" id="menu-item-716"><a href="#">Other Programs</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-713" id="menu-item-713"><a href="https://sirivo.org/gallery/health-camp/">Health Camp</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-714" id="menu-item-714"><a href="https://sirivo.org/gallery/rstc-program/">RSTC Program</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-715" id="menu-item-715"><a href="https://sirivo.org/gallery/water-soil/">Water &amp; Soil</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145" id="menu-item-145"><a href="https://sirivo.org/news-events/">News &amp; Events</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-148" id="menu-item-148"><a href="https://sirivo.org/contact-us/">Contact Us</a></li>
</ul></div> <div class="alterna-nav-form-container">
<div class="alterna-nav-form">
<form action="https://sirivo.org/" class="searchform" method="get" role="search">
<div>
<input class="sf-s" name="s" placeholder="Search" type="text"/>
<input class="sf-searchsubmit" type="submit" value=""/>
</div>
</form>
</div>
</div>
</div>
</nav>
</header> </div><!-- end header-wrap -->
<div class="content-wrap">
<div class="container" id="main">
<div class="row">
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
<div class="error-404">
<span class="error-icon"><i class="fa fa-exclamation-triangle"></i></span>
<h1 class="entry-title">404 - Page not found</h1>
<h5>OOPS!Looks like the page you are looking for isn't there.You may have mistyped the address or the page may have moved.</h5>
<div class="widget_search">
<form action="https://sirivo.org/" class="sidebar-searchform" method="get" role="search">
<div>
<input id="sidebar-s" name="s" placeholder="Search" type="text"/>
<input id="sidebar-type" name="post_type" type="hidden" value="post"/>
<input id="sidebar-searchsubmit" type="submit" value=""/>
</div>
</form></div>
</div>
</div>
</div>
</div>
</div><!-- end content-wrap -->
<div class="footer-wrap">
<footer class="footer-content">
<div class="footer-top-content">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3"><div class="widget widget_text" id="text-2"><h4 class="widget-title">Accredited By</h4><div class="line"></div><div class="clear"></div> <div class="textwidget"><p><img src="http://sirivo.org/wp-content/uploads/2016/11/nabard.png" style="background:#fff"/></p></div>
</div></div>
<div class="col-md-3 col-sm-3"><div class="widget widget_nav_menu" id="nav_menu-3"><h4 class="widget-title">About Us</h4><div class="line"></div><div class="clear"></div><div class="menu-about-us-container"><ul class="menu" id="menu-about-us"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110" id="menu-item-110"><a href="https://sirivo.org/about-us/about-siri-vo/">About SIRI VO</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113" id="menu-item-113"><a href="https://sirivo.org/about-us/mission/">Mission</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114" id="menu-item-114"><a href="https://sirivo.org/about-us/vision/">Vision</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111" id="menu-item-111"><a href="https://sirivo.org/about-us/managment/">Managment</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112" id="menu-item-112"><a href="https://sirivo.org/about-us/members-of-our-board/">Members of Our Board</a></li>
</ul></div></div></div>
<div class="col-md-3 col-sm-3"><div class="widget widget_nav_menu" id="nav_menu-4"><h4 class="widget-title">Quick Links</h4><div class="line"></div><div class="clear"></div><div class="menu-quick-links-container"><ul class="menu" id="menu-quick-links"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115" id="menu-item-115"><a href="https://sirivo.org/programs/">Programs</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118" id="menu-item-118"><a href="https://sirivo.org/gallery/">Gallery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119" id="menu-item-119"><a href="https://sirivo.org/news-events/">News &amp; Events</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-120" id="menu-item-120"><a href="#">Sucess Stories</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-121" id="menu-item-121"><a href="#">Annual Reports</a></li>
</ul></div></div></div>
<div class="col-md-3 col-sm-3"><div class="widget widget_text" id="text-3"><h4 class="widget-title">Contact Us</h4><div class="line"></div><div class="clear"></div> <div class="textwidget"><p>Annaram Road,<br/>
Thorrur,<br/>
Warangal Dist.,<br/>
Pin: 506 317.<br/>
Telangana, India<br/>
Phone: 9948170538<br/>
Email: sirisrinu.e@gmail.com</p>
</div>
</div></div>
</div>
</div>
</div>
<div class="footer-bottom-content">
<div class="container">
<div class="footer-copyright">Copyright © 2016 <a href="#">SIRI VO</a>. All rights reserved.</div>
<div class="footer-link">Powered by <a href="http://passionwebs.in" target="_blank">Passionwebs</a></div>
</div> </div>
</footer>
</div><!-- end footer-wrap -->
</div><!-- end wrapper -->
<script src="https://sirivo.org/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/sirivo.org\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"sending":"Sending ..."};
/* ]]> */
</script>
<script src="https://sirivo.org/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.5.1" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/bootstrap/js/bootstrap.min.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/plugins/js_composer/assets/lib/isotope/dist/isotope.pkgd.min.js?ver=4.3.4" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/js/fancyBox/jquery.mousewheel-3.0.6.pack.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/js/fancyBox/jquery.fancybox.pack.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/js/fancyBox/helpers/jquery.fancybox-thumbs.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/js/flexslider/jquery.flexslider-min.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-content/themes/siri/js/jquery.theme.js?ver=8.2" type="text/javascript"></script>
<script src="https://sirivo.org/wp-includes/js/wp-embed.min.js?ver=4.4.24" type="text/javascript"></script>
</body>
</html>