<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://beatbeatwhisperdotcom.wordpress.com/xmlrpc.php" rel="pingback"/>
<title>beatbeatwhisperdotcom</title>
<link href="//s2.wp.com" rel="dns-prefetch"/>
<link href="//s1.wp.com" rel="dns-prefetch"/>
<link href="//s0.wp.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.pubmine.com" rel="dns-prefetch"/>
<link href="//x.bidswitch.net" rel="dns-prefetch"/>
<link href="//static.criteo.net" rel="dns-prefetch"/>
<link href="//ib.adnxs.com" rel="dns-prefetch"/>
<link href="//aax.amazon-adsystem.com" rel="dns-prefetch"/>
<link href="//bidder.criteo.com" rel="dns-prefetch"/>
<link href="//cas.criteo.com" rel="dns-prefetch"/>
<link href="//gum.criteo.com" rel="dns-prefetch"/>
<link href="//ads.pubmatic.com" rel="dns-prefetch"/>
<link href="//gads.pubmatic.com" rel="dns-prefetch"/>
<link href="//tpc.googlesyndication.com" rel="dns-prefetch"/>
<link href="//ad.doubleclick.net" rel="dns-prefetch"/>
<link href="//googleads.g.doubleclick.net" rel="dns-prefetch"/>
<link href="//www.googletagservices.com" rel="dns-prefetch"/>
<link href="//cdn.switchadhub.com" rel="dns-prefetch"/>
<link href="//delivery.g.switchadhub.com" rel="dns-prefetch"/>
<link href="//delivery.swid.switchadhub.com" rel="dns-prefetch"/>
<link href="//a.teads.tv" rel="dns-prefetch"/>
<link href="//prebid.media.net" rel="dns-prefetch"/>
<link href="//adserver-us.adtech.advertising.com" rel="dns-prefetch"/>
<link href="//fastlane.rubiconproject.com" rel="dns-prefetch"/>
<link href="//prebid-server.rubiconproject.com" rel="dns-prefetch"/>
<link href="//hb-api.omnitagjs.com" rel="dns-prefetch"/>
<link href="//mtrx.go.sonobi.com" rel="dns-prefetch"/>
<link href="//apex.go.sonobi.com" rel="dns-prefetch"/>
<link href="//u.openx.net" rel="dns-prefetch"/>
<link href="https://beatbeatwhisperdotcom.wordpress.com/feed/" rel="alternate" title="beatbeatwhisperdotcom » Feed" type="application/rss+xml"/>
<link href="https://beatbeatwhisperdotcom.wordpress.com/comments/feed/" rel="alternate" title="beatbeatwhisperdotcom » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
		/* <![CDATA[ */
		function addLoadEvent(func) {
			var oldonload = window.onload;
			if (typeof window.onload != 'function') {
				window.onload = func;
			} else {
				window.onload = function () {
					oldonload();
					func();
				}
			}
		}
		/* ]]> */
	</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s0.wp.com\/wp-content\/mu-plugins\/wpcom-smileys\/twemoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s0.wp.com\/wp-content\/mu-plugins\/wpcom-smileys\/twemoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/s2.wp.com\/wp-includes\/js\/wp-emoji-release.min.js?m=1605528427h&ver=5.6-RC5-49737"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://s2.wp.com/_static/??-eJydkV1OAzEMhC9E4nYrFnhAnCU/VuoSJ1GcqNrbN0uBVhQB4sWSx/ONZBuORbmcGqYG3FWJPVASOBaXWQlTxOVLp53IHVxhNuZwAXP1xguEmK2JN94PW+ijtVjDmFSEJz3rCWyn6Nc496oi2WrqAtKWiH+IeaMEDtiKWXGz5N5UqOT/G1FNoxTkF9zld2zS93oHnqR9aup79OrM6/JD52La6mD0ZDAiD9tP2Pkf1paKImpUps6q7Qd4+56zDKVb2JvKOZG77PTCz9t5u9k9zg/T5nAC8nHEsQ==?cssminify=yes" id="all-css-0-1" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css">
.has-text-align-justify {
	text-align:justify;
}
</style>
<link crossorigin="anonymous" href="https://fonts.googleapis.com/css?family=Hind%3A300%2C400%2C500%2C600%2C700&amp;subset=latin%2Clatin-ext" id="harmonic-hind-css" media="all" rel="stylesheet"/>
<link href="https://s0.wp.com/_static/??-eJyFjsEOwjAMQ3+IEoY0NA6Ib+lK6AJpU7WpJv6ewoUhBNxs2c8JzMk4iYpRIVSTuHqKBTxGzNSCL3LtSlnBgtUJAxZIdYTJ5iCRHLROqzgJpuiN8YNZ3Jvp5FELYG2pXAkN2xkUQ2KrbffvgEcxLM4qSXwz5syW8i8048jim/TPj1/2AR3Doev3m27Xb4fhcgdF0nFa?cssminify=yes" id="all-css-2-1" media="all" rel="stylesheet" type="text/css"/>
<link href="https://s2.wp.com/wp-content/mu-plugins/global-print/global-print.css?m=1465851035h&amp;cssminify=yes" id="print-css-3-1" media="print" rel="stylesheet" type="text/css"/>
<style id="jetpack-global-styles-frontend-style-inline-css">
:root { --font-headings: unset; --font-base: unset; --font-headings-default: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif; --font-base-default: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;}
</style>
<link href="https://s0.wp.com/wp-content/themes/h4/global.css?m=1420737423h&amp;cssminify=yes" id="all-css-6-1" media="all" rel="stylesheet" type="text/css"/>
<script id="harmonic-backstretch-set-js-extra">
var BackStretchImg = {"src":""};
</script>
<script id="wpcom-actionbar-placeholder-js-extra">
var actionbardata = {"siteID":"110027455","siteName":"beatbeatwhisperdotcom","siteURL":"http:\/\/beatbeatwhisperdotcom.wordpress.com","icon":"<img alt='' src='https:\/\/s2.wp.com\/i\/logo\/wpcom-gray-white.png' class='avatar avatar-50' height='50' width='50' \/>","canManageOptions":"","canCustomizeSite":"","isFollowing":"","themeSlug":"pub\/harmonic","signupURL":"https:\/\/wordpress.com\/start\/","loginURL":"https:\/\/wordpress.com\/log-in?signup_flow=account","themeURL":"","xhrURL":"https:\/\/beatbeatwhisperdotcom.wordpress.com\/wp-admin\/admin-ajax.php","nonce":"789bd950e6","isSingular":"","isFolded":"","isLoggedIn":"","isMobile":"","subscribeNonce":"<input type=\"hidden\" id=\"_wpnonce\" name=\"_wpnonce\" value=\"f22c5eb0fe\" \/>","referer":"https:\/\/beatbeatwhisperdotcom.wordpress.com\/","canFollow":"1","feedID":"47121541","statusMessage":"","customizeLink":"https:\/\/beatbeatwhisperdotcom.wordpress.com\/wp-admin\/customize.php?url=https%3A%2F%2Fbeatbeatwhisperdotcom.wordpress.com%2F","i18n":{"view":"View site","follow":"Follow","following":"Following","edit":"Edit","login":"Log in","signup":"Sign up","customize":"Customize","report":"Report this content","themeInfo":"Get theme: Harmonic","shortlink":"Copy shortlink","copied":"Copied","followedText":"New posts from this site will now appear in your <a href=\"https:\/\/wordpress.com\/read\">Reader<\/a>","foldBar":"Collapse this bar","unfoldBar":"Expand this bar","editSubs":"Manage subscriptions","viewReader":"View site in Reader","viewReadPost":"View post in Reader","subscribe":"Sign me up","enterEmail":"Enter your email address","followers":"","alreadyUser":"Already have a WordPress.com account? <a href=\"https:\/\/wordpress.com\/log-in?signup_flow=account\">Log in now.<\/a>","stats":"Stats"}};
</script>
<script src="https://s2.wp.com/_static/??-eJyNjdEOwiAMRX9I1hmNkQfjtwB2Ao6CtGTx790SFzOffOpN7jm3MBXlMgmSQGQomSUhs7ljF3kH2zZlG0ZUjbHOAIkKNOSVC+TGdkNewPhsWF+f8zskHucXUJoFb2rKFNzX6axxD5aK4vyf4prVVr2my/7U6/NR9wcd3+GGVik=" type="text/javascript"></script>
<link href="https://beatbeatwhisperdotcom.wordpress.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://s1.wp.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress.com" name="generator"/>
<link href="https://wp.me/7rFan" rel="shortlink"/>
<!-- Jetpack Open Graph Tags -->
<meta content="website" property="og:type"/>
<meta content="beatbeatwhisperdotcom" property="og:title"/>
<meta content="https://beatbeatwhisperdotcom.wordpress.com/" property="og:url"/>
<meta content="beatbeatwhisperdotcom" property="og:site_name"/>
<meta content="https://s0.wp.com/i/blank.jpg" property="og:image"/>
<meta content="en_US" property="og:locale"/>
<meta content="@wordpressdotcom" name="twitter:site"/>
<meta content="249643311490" property="fb:app_id"/>
<!-- End Jetpack Open Graph Tags -->
<link href="https://s1.wp.com/i/favicon.ico" rel="shortcut icon" sizes="16x16 24x24 32x32 48x48" type="image/x-icon"/>
<link href="https://s1.wp.com/i/favicon.ico" rel="icon" sizes="16x16 24x24 32x32 48x48" type="image/x-icon"/>
<link href="https://s2.wp.com/i/webclip.png" rel="apple-touch-icon"/>
<link href="https://beatbeatwhisperdotcom.wordpress.com/osd.xml" rel="search" title="beatbeatwhisperdotcom" type="application/opensearchdescription+xml"/>
<link href="https://s1.wp.com/opensearch.xml" rel="search" title="WordPress.com" type="application/opensearchdescription+xml"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <style type="text/css">
			.recentcomments a {
				display: inline !important;
				padding: 0 !important;
				margin: 0 !important;
			}

			table.recentcommentsavatartop img.avatar, table.recentcommentsavatarend img.avatar {
				border: 0px;
				margin: 0;
			}

			table.recentcommentsavatartop a, table.recentcommentsavatarend a {
				border: 0px !important;
				background-color: transparent !important;
			}

			td.recentcommentsavatarend, td.recentcommentsavatartop {
				padding: 0px 0px 1px 0px;
				margin: 0px;
			}

			td.recentcommentstextend {
				border: none !important;
				padding: 0px 0px 2px 10px;
			}

			.rtl td.recentcommentstextend {
				padding: 0px 10px 2px 0px;
			}

			td.recentcommentstexttop {
				border: none;
				padding: 0px 0px 0px 10px;
			}

			.rtl td.recentcommentstexttop {
				padding: 0px 10px 0px 0px;
			}
		</style>
<meta content="beatbeatwhisperdotcom" name="application-name"/><meta content="width=device-width;height=device-height" name="msapplication-window"/><meta content="name=Subscribe;action-uri=https://beatbeatwhisperdotcom.wordpress.com/feed/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="name=Sign up for a free blog;action-uri=http://wordpress.com/signup/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="name=WordPress.com Support;action-uri=http://support.wordpress.com/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="name=WordPress.com Forums;action-uri=http://forums.wordpress.com/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/> <script type="text/javascript">

			window.doNotSellCallback = function() {

				var linkElements = [
					'a[href="https://wordpress.com/?ref=footer_blog"]',
					'a[href="https://wordpress.com/?ref=footer_website"]',
					'a[href="https://wordpress.com/?ref=vertical_footer"]',
					'a[href^="https://wordpress.com/?ref=footer_segment_"]',
				].join(',');

				var dnsLink = document.createElement( 'a' );
				dnsLink.href = 'https://wordpress.com/advertising-program-optout';
				dnsLink.classList.add( 'do-not-sell-link' );
				dnsLink.rel = 'nofollow';
				dnsLink.style.marginLeft = '0.5em';
				dnsLink.textContent = 'Do Not Sell My Personal Information';

				var creditLinks = document.querySelectorAll( linkElements );

				if ( 0 === creditLinks.length ) {
					return false;
				}

				Array.prototype.forEach.call( creditLinks, function( el ) {
					el.insertAdjacentElement( 'afterend', dnsLink );
				});

				return true;
			};

		</script>
<script type="text/javascript">
		function __ATA_CC() {var v = document.cookie.match('(^|;) ?personalized-ads-consent=([^;]*)(;|$)');return v ? 1 : 0;}
		var __ATA_PP = { pt: 0, ht: 0, tn: 'harmonic', amp: false, siteid: 8982, blogid: 110027455, consent: __ATA_CC()   };
		var __ATA = __ATA || {};
		__ATA.cmd = __ATA.cmd || [];
		__ATA.criteo = __ATA.criteo || {};
		__ATA.criteo.cmd = __ATA.criteo.cmd || [];
		
		</script>
<script type="text/javascript">
		(function(){var g=Date.now||function(){return+new Date};function h(a,b){a:{for(var c=a.length,d="string"==typeof a?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a)){b=e;break a}b=-1}return 0>b?null:"string"==typeof a?a.charAt(b):a[b]};function k(a,b,c){c=null!=c?"="+encodeURIComponent(String(c)):"";if(b+=c){c=a.indexOf("#");0>c&&(c=a.length);var d=a.indexOf("?");if(0>d||d>c){d=c;var e=""}else e=a.substring(d+1,c);a=[a.substr(0,d),e,a.substr(c)];c=a[1];a[1]=b?c?c+"&"+b:b:c;a=a[0]+(a[1]?"?"+a[1]:"")+a[2]}return a};var l=0;function m(a,b){var c=document.createElement("script");c.src=a;c.onload=function(){b&&b(void 0)};c.onerror=function(){b&&b("error")};a=document.getElementsByTagName("head");var d;a&&0!==a.length?d=a[0]:d=document.documentElement;d.appendChild(c)}function n(a){var b=void 0===b?document.cookie:b;return(b=h(b.split("; "),function(c){return-1!=c.indexOf(a+"=")}))?b.split("=")[1]:""}function p(a){return"string"==typeof a&&0<a.length}
		function r(a,b,c){b=void 0===b?"":b;c=void 0===c?".":c;var d=[];Object.keys(a).forEach(function(e){var f=a[e],q=typeof f;"object"==q&&null!=f||"function"==q?d.push(r(f,b+e+c)):null!==f&&void 0!==f&&(e=encodeURIComponent(b+e),d.push(e+"="+encodeURIComponent(f)))});return d.filter(p).join("&")}function t(a,b){a||((window.__ATA||{}).config=b.c,m(b.url))}var u=Math.floor(1E13*Math.random()),v=window.__ATA||{};window.__ATA=v;window.__ATA.cmd=v.cmd||[];v.rid=u;v.createdAt=g();var w=window.__ATA||{},x="s.pubmine.com";
		w&&w.serverDomain&&(x=w.serverDomain);var y="//"+x+"/conf",z=window.top===window,A=window.__ATA_PP&&window.__ATA_PP.gdpr_applies,B="boolean"===typeof A?Number(A):null,C=window.__ATA_PP||null,D=z?document.referrer?document.referrer:null:null,E=z?window.location.href:document.referrer?document.referrer:null,F,G=n("__ATA_tuuid");F=G?G:null;var H=window.innerWidth+"x"+window.innerHeight,I=n("usprivacy"),J=r({gdpr:B,pp:C,rid:u,src:D,ref:E,tuuid:F,vp:H,us_privacy:I?I:null},"",".");
		(function(a){var b=void 0===b?"cb":b;l++;var c="callback__"+g().toString(36)+"_"+l.toString(36);a=k(a,b,c);window[c]=function(d){t(void 0,d)};m(a,function(d){d&&t(d)})})(y+"?"+J);}).call(this);
		</script><script type="text/javascript">
	window.google_analytics_uacct = "UA-52447-2";
</script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-52447-2']);
	_gaq.push(['_gat._anonymizeIp']);
	_gaq.push(['_setDomainName', 'wordpress.com']);
	_gaq.push(['_initData']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
	})();
</script>
</head>
<body class="home blog customizer-styles-applied loading secondary-sidebar highlander-enabled highlander-light">
<div class="hfeed site" id="page">
<header id="masthead" role="banner">
<div id="mobile-panel">
<div id="mobile-link">
<span id="menu-title">Menu</span>
</div>
<h1 class="site-title"><a href="https://beatbeatwhisperdotcom.wordpress.com/" rel="home">beatbeatwhisperdotcom</a></h1>
</div>
<div id="mobile-block">
<nav class="main-navigation" id="site-navigation" role="navigation">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="menu"><ul>
<li class="current_page_item"><a href="https://beatbeatwhisperdotcom.wordpress.com/">Home</a></li><li class="page_item page-item-1"><a href="https://beatbeatwhisperdotcom.wordpress.com/about/">About</a></li>
</ul></div>
</nav><!-- #site-navigation .main-navigation -->
</div><!-- #menu-block-->
<div id="site-branding">
<h1 class="site-title"><a href="https://beatbeatwhisperdotcom.wordpress.com/" rel="home">beatbeatwhisperdotcom</a></h1>
</div>
<nav class="desktop-nav main-navigation site-wrapper" id="site-navigation" role="navigation">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="menu"><ul>
<li class="current_page_item"><a href="https://beatbeatwhisperdotcom.wordpress.com/">Home</a></li><li class="page_item page-item-1"><a href="https://beatbeatwhisperdotcom.wordpress.com/about/">About</a></li>
</ul></div>
</nav><!-- #site-navigation .main-navigation -->
</header>
<div id="content-wrapper">
<div class="site-wrapper" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="hentry no-results not-found">
<header class="page-header">
<h1 class="page-title">Nothing Found</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It seems we can’t find what you’re looking for. Perhaps searching can help.</p>
<form action="https://beatbeatwhisperdotcom.wordpress.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
</div><!-- .page-content -->
</section><!-- .hentry .no-results .not-found -->
</main><!-- #main .site-main -->
</div><!-- #primary .content-area -->
<div id="secondary" role="complementary">
<div class="widget-area">
<aside class="widget widget_search" id="search-2"><form action="https://beatbeatwhisperdotcom.wordpress.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></aside><aside class="widget widget_recent_comments" id="recent-comments-2"><h1 class="widget-title">Recent Comments</h1></aside><aside class="widget widget_archive" id="archives-2"><h1 class="widget-title">Archives</h1>
<ul>
</ul>
</aside><aside class="widget widget_categories" id="categories-2"><h1 class="widget-title">Categories</h1>
<ul>
<li class="cat-item-none">No categories</li> </ul>
</aside> <div id="atatags-286348-5fff20e85b312"></div>
<script>
				__ATA.cmd.push(function() {
					__ATA.initDynamicSlot({
						id: 'atatags-286348-5fff20e85b312',
						location: 140,
						formFactor: '003',
						label: {
							text: 'Advertisements',
						},
						creative: {
							reportAd: {
								text: 'Report this ad',
							},
							privacySettings: {
								text: 'Privacy',
								
							}
						}
					});
				});
			</script> </div><!-- .widget-area -->
</div><!-- #secondary -->
<div class="clear"></div>
</div><!-- #content .site-wrapper -->
</div><!-- #content-wrapper -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info">
<a href="https://wordpress.com/?ref=footer_website" rel="nofollow">Create a free website or blog at WordPress.com.</a>
</div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->
</div><!-- #page -->
<!-- -->
<script id="grofiles-cards-js" src="//0.gravatar.com/js/gprofiles.js?ver=202102y"></script>
<script id="wpgroho-js-extra">
var WPGroHo = {"my_hash":""};
</script>
<script src="https://s1.wp.com/wp-content/mu-plugins/gravatar-hovercards/wpgroho.js?m=1610363240h" type="text/javascript"></script>
<script>
		// Initialize and attach hovercards to all gravatars
		( function() {
			function init() {
				if ( typeof Gravatar === 'undefined' ) {
					return;
				}

				if ( typeof Gravatar.init !== 'function' ) {
					return;
				}

				Gravatar.profile_cb = function ( hash, id ) {
					WPGroHo.syncProfileData( hash, id );
				};

				Gravatar.my_hash = WPGroHo.my_hash;
				Gravatar.init( 'body', '#wp-admin-bar-my-account' );
			}

			if ( document.readyState !== 'loading' ) {
				init();
			} else {
				document.addEventListener( 'DOMContentLoaded', init );
			}
		} )();
	</script>
<div style="display:none">
</div>
<!-- CCPA [start] -->
<script type="text/javascript">
			( function () {

				var setupPrivacy = function() {

					document.addEventListener( 'DOMContentLoaded', function() {

						// Minimal Mozilla Cookie library
						// https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework
						var cookieLib = window.cookieLib = {getItem:function(e){return e&&decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*"+encodeURIComponent(e).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=\\s*([^;]*).*$)|^.*$"),"$1"))||null},setItem:function(e,o,n,t,r,i){if(!e||/^(?:expires|max\-age|path|domain|secure)$/i.test(e))return!1;var c="";if(n)switch(n.constructor){case Number:c=n===1/0?"; expires=Fri, 31 Dec 9999 23:59:59 GMT":"; max-age="+n;break;case String:c="; expires="+n;break;case Date:c="; expires="+n.toUTCString()}return"rootDomain"!==r&&".rootDomain"!==r||(r=(".rootDomain"===r?".":"")+document.location.hostname.split(".").slice(-2).join(".")),document.cookie=encodeURIComponent(e)+"="+encodeURIComponent(o)+c+(r?"; domain="+r:"")+(t?"; path="+t:"")+(i?"; secure":""),!0}};

						var setDefaultOptInCookie = function() {
							var value = '1YNN';
							var domain = '.wordpress.com' === location.hostname.slice( -14 ) ? '.rootDomain' : location.hostname;
							cookieLib.setItem( 'usprivacy', value, 365 * 24 * 60 * 60, '/', domain );
						};

						var setCcpaAppliesCookie = function( value ) {
							var domain = '.wordpress.com' === location.hostname.slice( -14 ) ? '.rootDomain' : location.hostname;
							cookieLib.setItem( 'ccpa_applies', value, 24 * 60 * 60, '/', domain );
						}

						var maybeCallDoNotSellCallback = function() {
							if ( 'function' === typeof window.doNotSellCallback ) {
								return window.doNotSellCallback();
							}

							return false;
						}

						var usprivacyCookie = cookieLib.getItem( 'usprivacy' );

						if ( null !== usprivacyCookie ) {
							maybeCallDoNotSellCallback();
							return;
						}

						var ccpaCookie = cookieLib.getItem( 'ccpa_applies' );

						if ( null === ccpaCookie ) {

							var request = new XMLHttpRequest();
							request.open( 'GET', 'https://public-api.wordpress.com/geo/', true );

							request.onreadystatechange = function () {
								if ( 4 === this.readyState ) {
									if ( 200 === this.status ) {

										var data = JSON.parse( this.response );
										var ccpa_applies = data['region'] && data['region'].toLowerCase() === 'california';

										setCcpaAppliesCookie( ccpa_applies );

										if ( ccpa_applies ) {
											if ( maybeCallDoNotSellCallback() ) {
												setDefaultOptInCookie();
											}
										}
									} else {
										setCcpaAppliesCookie( true );
										if ( maybeCallDoNotSellCallback() ) {
											setDefaultOptInCookie();
										}
									}
								}
							};

							request.send();
						} else {
							if ( ccpaCookie === 'true' ) {
								if ( maybeCallDoNotSellCallback() ) {
									setDefaultOptInCookie();
								}
							}
						}
					} );
				};

				if ( window.defQueue && defQueue.isLOHP && defQueue.isLOHP === 2020 ) {
					defQueue.items.push( setupPrivacy );
				} else {
					setupPrivacy();
				}

			} )();
		</script>
<!-- CCPA [end] -->
<script type="text/javascript">
			( function( $ ) {
				$( document.body ).on( 'post-load', function () {
					if ( typeof __ATA.insertInlineAds === 'function' ) {
						__ATA.insertInlineAds();
					}
				} );
			} )( jQuery );
			</script>
<script>
window.addEventListener( "load", function( event ) {
	var link = document.createElement( "link" );
	link.href = "https://s0.wp.com/wp-content/mu-plugins/actionbar/actionbar.css?v=20201002";
	link.type = "text/css";
	link.rel = "stylesheet";
	document.head.appendChild( link );

	var script = document.createElement( "script" );
	script.src = "https://s0.wp.com/wp-content/mu-plugins/actionbar/actionbar.js?v=20201002";
	script.defer = true;
	document.body.appendChild( script );
} );
</script>
<div class="widget widget_eu_cookie_law_widget">
<div class="hide-on-button ads-active" data-consent-expiration="180" data-hide-timeout="30" id="eu-cookie-law">
<form method="post">
<input class="accept" type="submit" value="Close and accept"/>

		Privacy &amp; Cookies: This site uses cookies. By continuing to use this website, you agree to their use. <br/>
To find out more, including how to control cookies, see here:
				<a href="https://automattic.com/cookies" rel="nofollow">
			Cookie Policy		</a>
</form>
</div>
</div><script src="https://s1.wp.com/_static/??-eJyNjEsOwjAMRC+EMR9RxAJxljR1WzdfxU7L8SkLJNQFYvc0M29wyWBTVIqKk2BHM1vKz/0kO/yqdKRAgrm2OJoSUmT7novjDJ6jgz7ZKtDzv+aHt/NQIfs6cBRcuBtIBamubXJM4M2CSiF7o7TJf/yYLnCE1hQMRpTKSqDFWCer9Aj3Y3M4X0/N7dJML5HHY6Y=" type="text/javascript"></script>
<script type="text/javascript">
// <![CDATA[
(function() {
try{
  if ( window.external &&'msIsSiteMode' in window.external) {
    if (window.external.msIsSiteMode()) {
      var jl = document.createElement('script');
      jl.type='text/javascript';
      jl.async=true;
      jl.src='/wp-content/plugins/ie-sitemode/custom-jumplist.php';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(jl, s);
    }
  }
}catch(e){}
})();
// ]]>
</script><script defer="" src="//stats.wp.com/w.js?61"></script> <script type="text/javascript">
_tkq = window._tkq || [];
_stq = window._stq || [];
_tkq.push(['storeContext', {'blog_id':'110027455','blog_tz':'0','user_lang':'en','blog_lang':'en','user_id':'0'}]);
_stq.push(['view', {'blog':'110027455','v':'wpcom','tz':'0','user_id':'0','subd':'beatbeatwhisperdotcom'}]);
_stq.push(['extra', {'crypt':'UE5XaGUuOTlwaD85flAmcm1mcmZsaDhkV11YdWtpP0NsWnVkPS9sL0ViLndld3BxWnRYfEk3ajJqT2dOSmVyMm05b3x5c3ZLNCZbNWhsVnlPVFU2MlJpMFctYUxKXWdEfGVvUHZyV20/RU5ueSZlK28wYVozTGI9Vn5zZ1ByUlglTW13MFVKSzlTUnA5Nz9Tam1kQlVdX0JDSXdSWTlyNGxCejhEazRBYi9weD0sT18rNTAsLEJTeklfb2M4YkJmbF9Jfnp5aUo4S3xCeEVSQXM4aXxpR2VDUFBjL0lEdTNsaytvcy9ueUUlLytVWC8lRjVEWVR1M0VubC03ZC8/Q3dhY3pCRzNPK3NwPUlF'}]);
_stq.push([ 'clickTrackerInit', '110027455', '0' ]);
	</script>
<noscript><img alt="" src="https://pixel.wp.com/b.gif?v=noscript" style="height:1px;width:1px;overflow:hidden;position:absolute;bottom:1px;"/></noscript>
<script>
if ( 'object' === typeof wpcom_mobile_user_agent_info ) {

	wpcom_mobile_user_agent_info.init();
	var mobileStatsQueryString = "";
	
	if( false !== wpcom_mobile_user_agent_info.matchedPlatformName )
		mobileStatsQueryString += "&x_" + 'mobile_platforms' + '=' + wpcom_mobile_user_agent_info.matchedPlatformName;
	
	if( false !== wpcom_mobile_user_agent_info.matchedUserAgentName )
		mobileStatsQueryString += "&x_" + 'mobile_devices' + '=' + wpcom_mobile_user_agent_info.matchedUserAgentName;
	
	if( wpcom_mobile_user_agent_info.isIPad() )
		mobileStatsQueryString += "&x_" + 'ipad_views' + '=' + 'views';

	if( "" != mobileStatsQueryString ) {
		new Image().src = document.location.protocol + '//pixel.wp.com/g.gif?v=wpcom-no-pv' + mobileStatsQueryString + '&baba=' + Math.random();
	}
	
}
</script> </body>
</html>