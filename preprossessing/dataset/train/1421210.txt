<!DOCTYPE html>
<!--
COPYRIGHT by HighHay/Mivfx
Before using this theme, you should accept themeforest licenses terms.
http://themeforest.net/licenses
--><html class="no-js" lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta charset="utf-8"/>
<!-- Page Title Here -->
<title>Teknobil HavacÄ±lÄ±k Savunma Enerji Teknolojileri</title>
<!-- Page Description Here -->
<meta content="A responsive coming soon template, un template HTML pour une page en cours de construction" name="description"/>
<!-- Disable screen scaling-->
<meta content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
<!-- Initializer -->
<link href="./css/normalize.css" rel="stylesheet"/>
<!-- Web fonts and Web Icons -->
<link href="./css/pageloader.css" rel="stylesheet"/>
<link href="./fonts/opensans/stylesheet.css" rel="stylesheet"/>
<link href="./fonts/asap/stylesheet.css" rel="stylesheet"/>
<link href="./css/ionicons.min.css" rel="stylesheet"/>
<!-- Vendor CSS style -->
<link href="./css/foundation.min.css" rel="stylesheet"/>
<link href="./js/vendor/jquery.fullPage.css" rel="stylesheet"/>
<link href="./js/vegas/vegas.min.css" rel="stylesheet"/>
<!-- Main CSS files -->
<link href="./css/main.css" rel="stylesheet"/>
<link href="./css/main_responsive.css" rel="stylesheet"/>
<link href="./css/style-font1.css" rel="stylesheet"/>
<script src="./js/vendor/modernizr-2.7.1.min.js"></script>
</head>
<body class="alt-bg" id="menu">
<!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<!-- Page Loader -->
<div class="page-loader" id="page-loader">
<div><i class="ion ion-loading-d"></i><p>loading</p></div>
</div>
<!-- BEGIN OF site header Menu -->
<!-- Add "material" class for a material design style -->
<header class="header-top">
<!--		<header class="header-top material">-->
<div class="logo">
<a href="#home">
<img alt="Logo Brand" src="img/logo_large.png"/>
</a>
</div>
<div class="menu clearfix">
<a href="#about-us">hakkÄ±mÄ±zda</a>
<!-- Add other menu similar to "about" here -->
<a href="#contact">iletiÅim</a>
</div>
</header>
<!-- END OF site header Menu-->
<!-- BEGIN OF Quick nav icons at left -->
<nav class="quick-link count-6 nav-left">
<ul id="qmenu">
<li data-menuanchor="home">
<a class="" href="#home"><i class="icon ion ion-home"></i>
</a>
<span class="title">Anasayfa</span>
</li>
<li data-menuanchor="when">
<a class="" href="#when"><i class="icon ion ion-android-alarm"></i>
</a>
<span class="title">Ne Zaman</span>
</li>
<li data-menuanchor="contact">
<a href="#contact"><i class="icon ion ion-android-call"></i>
</a>
<span class="title">Ä°letiÅim Bilgileri</span>
</li>
</ul>
</nav>
<!-- END OF Quick nav icons at left -->
<!-- BEGIN OF site cover -->
<div class="page-cover" id="home">
<!-- Cover Background -->
<div class="cover-bg pos-abs full-size bg-img" data-image-src="img/bg-default.jpg"></div>
<!-- BEGIN OF Slideshow Background -->
<!--<div class="cover-bg pos-abs full-size slide-show">
				<i class='img' data-src='./img/bg-slide1.jpg'></i>
				<i class='img' data-src='./img/bg-slide2.jpg'></i>
				<i class='img' data-src='./img/bg-slide3.jpg'></i>
				<i class='img' data-src='./img/bg-slide4.jpg'></i>
			</div>-->
<!-- END OF Slideshow Background -->
<!--BEGIN OF Static video bg  - uncomment below to use Video as Background-->
<!--<div id="container" class="video-container show-for-medium-up">
                <video autoplay="autoplay" loop="loop" autobuffer="autobuffer" muted="muted"
                       width="640" height="360">
                    <source src="vid/flower_loop.mp4" type="video/mp4">
                </video>
            </div>-->
<!--END OF Static video bg-->
<!-- Solid color as background -->
<!--            <div class="cover-bg pos-abs full-size bg-color" data-bgcolor="rgba(51, 2, 48, 0.12)"></div>-->
<!-- Solid color as filter -->
<!--            <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(62, 24, 219, 0.41)"></div>-->
</div>
<!--END OF site Cover -->
<!-- BEGIN OF site main content content here -->
<main class="page-main" id="mainpage">
<!-- Begin of home page -->
<div class="section page-home page page-cent" id="s-home">
<!-- Logo -->
<div class="logo-container">
<img alt="Logo" class="h-logo" src="img/logo_only.png"/>
</div>
<!-- Content -->
<section class="content">
<header class="header">
<div class="h-left">
<h2><strong>TEKNOBÄ°L</strong></h2>
</div>
<div class="h-right">
<h3>Teknobil HavacÄ±lÄ±k Savunma Enerji Teknolojileri San.Tic.Ltd.Åti.</h3>
</div>
</header>
</section>
</div>
<!-- End of home page -->
<!-- Begin of timer page -->
<div class="section page-when page page-cent" id="s-when">
<section class="content">
<div class="clock clock-countdown">
<div class="site-config" data-date="01/01/2018 23:00:00" data-date-timezone="+0"></div>
<header class="header">
<strong>YapÄ±m AÅamasÄ±nda</strong>
</header>
<div class="elem-left">
<div class="digit hours">00</div>
<div class="text">hrs</div>
</div>
<div class="elem-center">
<!-- Optional text at top or image logo -->
<!--<span class="text top">here in</span>-->
<!-- Optional logo at top -->
<span class="text top"><img alt="Logo" class="img" src="img/logo_large.png"/></span>
<div class="digit days">000</div>
<div class="text">days</div>
</div>
<div class="elem-right">
<div class="digit minutes">00</div>
<div class="text">min</div>
</div>
<!-- second knob here -->
<div class="second">
<input class="knob container" data-bgcolor="rgba(255,255,255,0)" data-displayinput="false" data-displayprevious="true" data-fgcolor="#fff" data-height="400" data-max="6000" data-thickness=".07" data-width="400" id="second-knob" value="0"/>
</div>
</div>
</section>
</div>
<!-- End of timer page -->
<!-- Begin of register page -->
<div class="section page-register page page-cent" id="s-register">
<section class="content">
<header class="p-title">
<h3>Register <i class="ion ion-compose"></i></h3>
</header>
<div>
<form action="ajaxserver/serverfile.php" class="form magic send_email_form" id="mail-subscription" method="get">
<p class="invite center">Please, write your email below to stay in touch with us :</p>
<div class="fields clearfix">
<div class="input">
<label for="reg-email">Email </label>
<input class="email_f" data-validation-type="email" id="reg-email" name="email" placeholder="your@email.address" required="" type="email"/></div>
<div class="buttons">
<button class="button email_b" id="submit-email" name="submit_email">Ok</button>
</div>
</div>
<p class="email-ok invisible"><strong>Thank you</strong> for your subscription. We will inform you.</p>
</form>
</div>
</section>
</div>
<!-- End of register page -->
<!-- Begin of about us page -->
<div class="section page-about page page-cent" id="s-about-us">
<section class="content">
<header class="p-title">
<h3>TEKNOBÄ°L<i class="ion ion-android-information">
</i>
</h3>
<h2>We <span class="bold">make</span> only <span class="bold">beautiful</span> things</h2>
</header>
<article class="text">
<p>TEKNOBIL HAVACILIK SAVUNMA ENERJI TEKNOLOJILERI SAN.TIC.LTD.ÅTI.</p>
</article>
</section>
</div>
<!-- End of about us page -->
<!-- Begin of Contact page   -->
<div class="section page-contact page page-cent bg-color" data-bgcolor="rgba(95, 25, 208, 0.88)s" id="s-contact">
<!-- Begin of contact information -->
<div class="slide" data-anchor="information" id="information">
<section class="content">
<header class="p-title">
<h3>Ä°letiÅim<i class="ion ion-location">
</i>
</h3>
<ul class="buttons">
<li class="show-for-medium-up">
<a href="#about-us" title="About"><i class="ion ion-android-information"></i></a>
</li>
<!--<li>
									<a title="Contact" href="#contact/information"><i class="ion ion-location"></i></a>
								</li>-->
<li>
<a href="#contact/message" title="Message"><i class="ion ion-email"></i></a>
</li>
</ul>
</header>
<!-- Begin Of Page SubSction -->
<div class="contact">
<div class="row">
<div class="medium-6 columns left">
<ul>
<li>
<h4>Email</h4>
<p><a href="mailto://info@teknobilsavunma.com">info@teknobilsavunma.com</a></p>
</li>
<li>
<h4>Adres</h4>
<p>OrhanlÄ± Mah. Karadeniz Cad. <br/>Turgut Reis San. Sit.
B4 Blok No:27<br/> Tuzla / Ä°stanbul

										</p></li>
<li>
<h4>Telefon</h4>
<p>+90 216 394 94 01</p>
</li>
<li>
<h4>Fax</h4>
<p>+90 216 394 94 02 </p>
</li>
<li>
<h4>Gsm</h4>
<p>+90 546 436 14 20</p>
</li>
</ul>
</div>
<div class="medium-6 columns social-links right">
<ul>
<!-- legal notice -->
<li class="show-for-medium-up">
<h4>Website</h4>
<p><a href="http://www.teknobilsavunma.com">www.teknobilsavunma.com</a></p>
</li>
<li class="show-for-medium-up">
<h4>Sosyal BaÄlantÄ±lar</h4>
<!-- Begin of Social links -->
<div class="socialnet">
<a href="#"><i class="ion ion-social-facebook"></i></a>
<a href="#"><i class="ion ion-social-instagram"></i></a>
<a href="#"><i class="ion ion-social-twitter"></i></a>
<a href="#"><i class="ion ion-social-pinterest"></i></a>
<a href="#"><i class="ion ion-social-tumblr"></i></a>
</div>
<!-- End of Social links -->
</li>
<li>
<p><img alt="Logo" class="logo" src="img/logo_large.png"/></p>
<p class="small">Teknobil HavacÄ±lÄ±k Savunma Enerji Teknolojileri San.Tic.Ltd.Åti.</p>
</li>
</ul>
</div>
</div>
</div>
<!-- End of page SubSection -->
</section>
</div>
<!-- end of contact information -->
<!-- begin of contact message -->
<div class="slide" data-anchor="message" id="message">
<section class="content">
<header class="p-title">
<h3>Bize YazÄ±n<i class="ion ion-email">
</i>
</h3>
<ul class="buttons">
<li class="show-for-medium-up">
<a href="#about-us" title="About"><i class="ion ion-android-information"></i></a>
</li>
<li>
<a href="#contact/information" title="Contact"><i class="ion ion-location"></i></a>
</li>
<!--<li>
									<a title="Message" href="#contact/message"><i class="ion ion-email"></i></a>
								</li>-->
</ul>
</header>
<!-- Begin Of Page SubSction -->
<div class="page-block c-right v-zoomIn">
<div class="wrapper">
<div>
<form action="ajaxserver/serverfile.php" class="message form send_message_form" method="get">
<div class="fields clearfix">
<div class="input">
<label for="mes-name">Name </label>
<input id="mes-name" name="name" placeholder="Your Name" required="" type="text"/></div>
<div class="buttons">
<button class="button email_b" id="submit-message" name="submit_message">Ok</button>
</div>
</div>
<div class="fields clearfix">
<div class="input">
<label for="mes-email">Email </label>
<input id="mes-email" name="email" placeholder="Email Address" required="" type="email"/>
</div>
</div>
<div class="fields clearfix no-border">
<label for="mes-text">Message </label>
<textarea id="mes-text" name="message" placeholder="Message ..." required=""></textarea>
<div>
<p class="message-ok invisible">Your message has been sent, thank you.</p>
</div>
</div>
</form>
</div>
</div>
</div>
<!-- End Of Page SubSction -->
</section>
</div>
<!-- End of contact message -->
</div>
<!-- End of Contact page  -->
</main>
<!-- END OF site main content content here -->
<!-- Begin of site footer -->
<footer class="page-footer">
<span>Sosyal BaÄlantÄ±lar
				<a href="http://www.facebook.com/" target="_blank"><i class="ion icon ion-social-facebook"></i></a>
<a href="http://www.instagram.com/" target="_blank"><i class="ion icon ion-social-instagram"></i></a>
<a href="http://twitter.com/" target="_blank"><i class="ion icon ion-social-twitter"></i></a>
</span>
</footer>
<!-- End of site footer -->
<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<!-- All Javascript plugins goes here -->
<!--		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>-->
<script src="./js/vendor/jquery-1.11.2.min.js"></script>
<!-- All vendor scripts -->
<script src="./js/vendor/all.js"></script>
<!-- Detailed vendor scripts -->
<!--<script src="./js/vendor/jquery.fullPage.min.js"></script>
        <script src="./js/vendor/jquery.slimscroll.min.js"></script>
        <script src="./js/vendor/jquery.knob.min.js"></script>
        <script src="./js/vegas/vegas.min.js"></script>
        <script src="./js/jquery.maximage.js"></script>
        <script src="./js/okvideo.min.js"></script>-->
<!-- Downcount JS -->
<script src="./js/jquery.downCount.js"></script>
<!-- Form script -->
<script src="./js/form_script.js"></script>
<!-- Javascript main files -->
<script src="./js/main.js"></script>
</body>
</html>
