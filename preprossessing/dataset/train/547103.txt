<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>FeelsGood Virtual Reality for Healthcare</title>
<link href="assets/images/favicon.png" rel="icon" sizes="16x16" type="image/png"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Root Multipurpose Landing Page Template" name="description"/>
<meta content="Root HTML Template, Root Landing Page, Landing Page Template" name="keywords"/>
<link href="assets/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Nunito:400,700%7CRoboto:300,400,500%7CMuli:300,400" rel="stylesheet"/>
<link href="assets/css/animate.css" rel="stylesheet"/> <!-- Resource style -->
<link href="assets/css/owl.carousel.css" rel="stylesheet"/>
<link href="assets/css/owl.theme.css" rel="stylesheet"/>
<link href="assets/MagnificPopup/magnific-popup.css" rel="stylesheet"/>
<link href="assets/css/ionicons.min.css" rel="stylesheet"/> <!-- Resource style -->
<link href="assets/css/style.css" media="all" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="wrapper">
<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header page-scroll">
<button aria-expanded="false" class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#"><img src="assets/images/logo.png" width="18%"/></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a class="page-scroll" href="#main">Home</a></li>
<li><a class="page-scroll" href="#buy">Our solution</a></li>
<li><a class="page-scroll" href="#specs">Advantage</a></li>
<li><a class="page-scroll" href="#usecases">Use cases</a></li>
<li><a class="page-scroll" href="#theytrustus">They trust us</a></li>
<li><a class="page-scroll" href="#comingsoon">Coming soon</a></li>
<li><a class="page-scroll" href="#signup">Sign up</a></li>
</ul>
</div>
</div>
</nav><!-- /.navbar-collapse -->
<div class="main main-2" id="main">
<div class="hero-2">
<div class="container">
<div class="row">
<div class="col-md-5 col-sm-6">
<div class="intro">
<h1 class="wow fadeInDown" data-wow-delay="0.1s">Virtual reality for the health care</h1>
<p class="wow fadeInDown" data-wow-delay="0.2s">FeelsGood is focused on reducing pain, stress, anxiety and physiotherapeutic rehabilitation.</p>
<a class="btn btn-action btn-fb wow fadeInDown popup-youlist" data-wow-delay="0.3s" href="https://www.youtube.com/embed/videoseries?list=PL-SwmM_SSrvBnyj_Q90kx0y1JU63fBiLH"><span>Ver video</span></a>
</div>
</div>
<div class="col-md-7 col-sm-6">
<img alt="Feature" class="img-responsive" src="assets/images/product.png"/>
</div>
</div>
</div>
</div><!-- Pi-Hero -->
<div class="cta mobile-trans-bg" id="buy">
<div class="container">
<div class="row">
<div class="col-md-5 col-md-offset-7">
<div class="cta-inner">
<h1>VR PAIN FEELSGOOD</h1>
<p>It is our product to reduce pain during venipuncture procedures, whose effectiveness has been demonstrated in more than 10,000 painless procedures.

FeelsGood has designed a technique that redirects patient care during venipuncture procedures, this technique is called "2-step synchronized approach" and is used in Peru, Colombia, Mexico, Brazil and soon in Ecuador.</p><br/><br/><br/><br/><br/><br/><br/>
<!-- <span><img src="assets/images/price.png"></span>  -->
</div>
</div>
</div>
</div>
</div>
<div class="features-boxed" id="specs">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="boxed-intro text-center wow fadeInDown">
<h4>Advantage</h4>
<h1>OUR PRODUCTS OFFER</h1>
</div>
</div>
<div class="col-md-3 col-sm-4 wow fadeInDown">
<div class="box-inner">
<div class="box-info">
<h1>Reduces pain</h1>
<p>We reduce the sensation of pain during venipuncture procedures such as poth-to-cath catheter opening, canalization, blood sampling or medication application.</p>
</div>
</div>
</div>
<div class="col-md-3 col-sm-4 wow fadeInDown">
<div class="box-inner">
<div class="box-info">
<h1>We reduce stress and anxiety</h1>
<p>We managed to reduce anxiety and stress during radiotherapy, chemotherapy procedures, among others.</p>
</div>
</div>
</div>
<div class="col-md-3 col-sm-4 wow fadeInDown">
<div class="box-inner">
<div class="box-info">
<h1>Costs reduction.</h1>
<p>Compared to local anesthetics like lidocaine that are consumed when applied, our product can be used many times.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="flex-split" id="usecases">
<div class="f-left">
<div class="left-content wow fadeInLeft" data-wow-delay="0s">
<h2 class="wow fadeInDown" data-wow-delay="0.1s"><span>Our product has been tested in more than 10,000 painless procedures.</span> </h2>
<p class="wow fadeInDown" data-wow-delay="0.2s">The most important institutions in Latin America and Brazil have already been using it.</p><br/><br/><br/><br/><br/>
</div>
</div>
<div class="f-right">
<div class="video-icon hidden-xs text-center">
<a class="popup-youlist wow fadeInUp" data-wow-delay="0.2s" href="https://www.youtube.com/embed/videoseries?list=PL-SwmM_SSrvBnyj_Q90kx0y1JU63fBiLH"><i class="ion-ios-play"></i></a>
</div>
</div>
</div>
<div class="review-section" id="theytrustus">
<div class="container">
<br/>
<div class="boxed-intro text-center wow fadeInDown">
<h4>In Peru, Colombia, Mexico, Ecuador, Brasil </h4>
<br/>
<h1 style="font-weight: bold; ">THEY TRUST US</h1>
</div>
<br/>
<div class="row">
<div class="col-sm-8 col-sm-offset-2">
<div class="reviews owl-carousel owl-theme">
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/Einstein.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Hospital Israelita Albert Einstein </h3>
<h3 style="font-weight: bold; ">Brasil</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/federicogomez.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Hospital Infantil Federico Gomez </h3>
<h3 style="font-weight: bold; ">MÃ©xico</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/cancerologia.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Instituto de CancerologÃ­a </h3>
<h3 style="font-weight: bold; ">Colombia</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/auna.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>ClÃ­nica Delgado - RED AUNA </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/inen.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Instituto Nacional de Enfermedades NeoplÃ¡sicas </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/roche.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Roche </h3>
<h3 style="font-weight: bold; ">Ecuador</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/niÃ±o.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Instituto Nacional del NiÃ±o - San Borja </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/sanna.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>SANNA</h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/precisa.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Laboratorios Precisa </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/roche.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>Roche </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
<div class="review-single"><img alt="Client Testimonoal" src="assets/icons/sanfelipe.jpg"/>
<div class="review-text wow fadeInDown" data-wow-delay="0.1s">
<h3>ClÃ­nca San Felipe </h3>
<h3 style="font-weight: bold; ">PerÃº</h3>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="download" id="comingsoon">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="dwnld-inner text-center">
<div class="app-info wow fadeInUp">
<h1 class="wow fadeInUp">Social platform for physiotherapeutic rehabilitation</h1>
<h4>We are building the first virtual reality physiotherapeutic rehabilitation social platform, it will be available in June 2021. <br class="hidden-xs"/> and you will be able to rehabilitate from home.</h4>
<i class="ion ion-star"></i>
<i class="ion ion-star"></i>
<i class="ion ion-star"></i>
<i class="ion ion-star"></i>
<i class="ion ion-star"></i>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="cta-2" id="signup">
<div class="container">
<div class="row text-center">
<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
<div class="cta-2-inner">
<h1 class="wow fadeInDown" data-wow-delay="0.1s">Subscribe now and get early access to our products.</h1>
<p class="wow fadeInDown" data-wow-delay="0.2s">If you want more information please provide us with your email address</p>
<div class="subform wow fadeInDown" data-wow-delay="0.3s">
<form action="assets/php/saveSuscribe.php" class="formee" id="signup" method="post">
<input id="emailToSubscribe" name="email" type="text"/>
<input class="right inputnew" id="SubscribeButton" title="Send" type="button" value="Subscribe"/>
</form>
<div id="response"></div>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row text-center">
<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
<h3>Our Social media</h3>
<a href="https://www.facebook.com/feelsgoodvr" target="_blank"><img class="mx-5" src="img/fb-circle.png" width="32px"/></a>
<a href="https://www.linkedin.com/company/feelsgoodvr/" target="_blank"><img class="mx-5" src="img/linkedin-circle.png" width="32px"/></a>
<a href="https://www.youtube.com/channel/UC8gb7zOGglkbPh7f2ToflIw" target="_blank"><img class="mx-5" src="img/youtube-circle.png" width="32px"/></a>
</div>
</div>
</div>
</div>
<div class="footer">
<div class="container">
<div class="row">
<div class="col-sm-5">
<div class="ft-left">
<div class="footer-img">
<a class="navbar-brand" href="#"><img src="assets/images/logo.png" width="30%"/></a>
<br/><br/>
</div>
<br/>
<p>FeelsGood is a startup focused on the health field, uses virtual reality, augmented reality and artificial intelligence technologies to generate a positive impact on people's lives and help health companies reduce costs and improve their services </p>
<p>© 2020 - FeelsGood SA. Made with love | <a class="__cf_email__" data-cfemail="ec85828a83ac8a8989809f8b838388c28f8d9e89" href="/cdn-cgi/l/email-protection">[email protected]</a> </p>
</div>
</div>
<div class="col-sm-4 col-sm-offset-3">
<div class="ft-right">
<ul>
<li><a class="page-scroll" href="#main">Home</a></li>
<li><a class="page-scroll" href="#buy">Our solution</a></li>
<li><a class="page-scroll" href="#specs">Advantage</a></li>
<li><a class="page-scroll" href="#usecases">Use cases</a></li>
<li><a class="page-scroll" href="#theytrustus">They trust us</a></li>
<li><a class="page-scroll" href="#comingsoon">Coming soon</a></li>
<li><a class="page-scroll" href="#signup">Sign up</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div> <!-- Main -->
</div><!-- Wrapper -->
<!-- Jquery and Js Plugins -->
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="assets/js/plugins.js" type="text/javascript"></script>
<script src="assets/js/validator.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
<script src="assets/js/popupmanager.js" type="text/javascript"></script>
<script src="assets/MagnificPopup/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=G-152HRH4923"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-152HRH4923');
</script>
</body>
</html>
