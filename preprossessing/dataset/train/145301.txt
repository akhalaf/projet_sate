<!DOCTYPE html>
<html lang="en">
<head>
<!-- meta data & title -->
<meta charset="utf-8"/>
<title>Araaya</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- Favicon and touch icons -->
<link href="assets/ico/favicon.ico" rel="shortcut icon"/>
<link href="assets/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="assets/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="assets/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="assets/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
<!-- CSS -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css"/>
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
<link href="assets/css/animate.min.css" rel="stylesheet"/>
<link href="assets/css/style.css" rel="stylesheet"/>
</head>
<body>
<!-- Header -->
<nav class="navbar navbar-default navbar-static-top navbar-sticky" id="navbar-section" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-responsive-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<!-- <a class="navbar-brand wow fadeInDownBig" href="index.html"><img class="office-logo" src="assets/img/slider/Office.png" alt="Office"></a>       -->
</div>
<div class="collapse navbar-collapse navbar-responsive-collapse" id="navbar-spy">
<img alt="logo" src="assets/img/Logo.png" style="width: 150px;"/>
<ul class="nav navbar-nav pull-right">
<li class="active">
<a href="index.html">Home</a>
</li>
<li>
<a href="about.html">About</a>
</li>
<li>
<a href="product.html">Products</a>
</li>
<li>
<a href="Project.html">Enterprise Solutions</a>
</li>
<!-- <li>
                        <a href="Project.html">Enterprise Solutions</a>
                    </li> -->
<li>
<a href="contact.html"><span>Contact</span></a>
</li>
</ul>
</div>
</div>
</nav>
<!-- End Header -->
<!-- Begin #carousel-section -->
<section class="section-global-wrapper" id="carousel-section">
<div class="container-fluid-kamn">
<div class="row">
<div class="carousel slide" data-ride="carousel" id="carousel-1">
<!-- Indicators -->
<ol class="carousel-indicators visible-lg">
<li class="active" data-slide-to="0" data-target="#carousel-1"></li>
<li data-slide-to="1" data-target="#carousel-1"></li>
<li data-slide-to="2" data-target="#carousel-1"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<!-- Begin Slide 1 -->
<div class="item active">
<img alt="Image of first carousel" height="400" src="assets/img/slider/image1.jpg"/>
<div class="carousel-caption">
<!-- <h3 class="carousel-title hidden-xs">Office BOOTSTRAP TEMPLATE</h3>
                                <p class="carousel-body">RESPONSIVE \ MULTI PAGE</p> -->
</div>
</div>
<!-- End Slide 1 -->
<!-- Begin Slide 2 -->
<div class="item">
<img alt="Image of second carousel" height="400" src="assets/img/slider/image2.jpg"/>
<div class="carousel-caption">
<!-- <h3 class="carousel-title hidden-xs">EASY TO CUSTOMIZE</h3>
                                <p class="carousel-body">BEAUTIFUL \ CLEAN \ MINIMAL</p> -->
</div>
</div>
<!-- End Slide 2 -->
<!-- Begin Slide 3 -->
<div class="item">
<img alt="Image of third carousel" height="400" src="assets/img/slider/image3.jpg"/>
<div class="carousel-caption">
<!-- <h3 class="carousel-title hidden-xs">MULTI-PURPOSE TEMPLATE</h3>
                                <p class="carousel-body">PORTFOLIO \ CORPORATE \ CREATIVE</p> -->
</div>
</div>
<!-- End Slide 3 -->
</div>
<!-- Controls -->
<a class="left carousel-control" data-slide="prev" href="#carousel-1">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" data-slide="next" href="#carousel-1">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>
</div>
</div>
</section>
<!-- End #carousel-section -->
<!-- Begin #services-section -->
<section class="services-section section-global-wrapper" id="services">
<div class="container">
<div class="row">
<div class="services-header">
<!-- <h3 class="services-header-title">Our Mission</h3>
                    <p class="services-header-body"><em> Things we provide in Office </em>  </p><hr> -->
</div>
</div>
<!-- Begin Services Row 1 -->
<div class="row services-row services-row-head services-row-1">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated fadeInLeft" data-wow-offset="40" style="height: 707px;">
<p class="services-icon"><span class="fa fa-desktop fa-5x"></span></p>
<h4 class="services-title">State Land Information Management System (eSlims)â</h4>
<p class="services-body">eSLIMS is a highly available and scalable system which processes and maintains
                        information of all state lands and land alienations. This system is designed to automate the land alienation
                        process which involves in application processing and selection of candidates in land alienation. All
                        government lands in Sri Lanka (80% of all lands in the country are government lands) will be managed via
                        this system eventually. All government lands are mapped and assigned a unique key (GIS parcel code) to
                        identify them across all systems. One of the main goals of the eSlims is to provide highly useful
                        information such as land usage information for national development planning and decision making.</p>
<p class="services-more"><a href="eSlims.html">More</a></p>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated zoomIn" data-wow-offset="40" style="height: 707px;">
<p class="services-icon"><i class="fa fa-desktop fa-5x"></i></p>
<h4 class="services-title">ePopulationâ â Register for â Registrar Generalâs Department</h4>
<p class="services-body">ePopulation is one of the most important government projects developed under eSriLanka
                        initiative. It is to be used as the main government data backbone for citizen information. ePopulation
                        creates and maintains a populations registry to store all citizen information, in addition to civil
                        registrations. ePopulation includes the registration of life events (Birth| Marriages| Adoption| Death),
                        certificate issuance in any language, assignment of PIN (Personal Identification Number) to citizens. This
                        is highly scalable, available and secure system with record levels expected to surpass 20 million. Searches
                        and queries had to be specifically improved for critical performance needs for big databases. ePopulation
                        data will be available to all other government (such as ministry of defense, department of immigration,
                        etc.) and trusted parties via eServices.</p>
<p class="services-more"><a href="ePopulation.html">More</a></p>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated fadeInRight" data-wow-offset="40">
<p class="services-icon"><i class="fa fa-thumbs-o-up fa-5x"></i></p>
<h4 class="services-title">Green Frontiers Charging Application</h4>
<p class="services-body">This project for an electric vehicle charging company which contains two android
                        applications, a REST webs ervice, a web application for other mobiles( for iOS, Windows
                        etc.) and a web application. One android application and mobile web application are
                        dedicated to clients of the company. Those apps provide features like locations of charging
                        stations, shortest path for nearest charging station, the current status of charging stations and
                        many others. The mobile web application is designed to be compatible with mobile browsers
                        considering responsiveness and User Experience to give the real feel of a mobile application.
                        Other android app is for operators who provide service at charging stations. It provides
                        features to update status of charging station, to see status of clients and etc. The REST web
                        service provides services to android applications. The web application is dedicated to admin
                        panel of the company who want to see statistics of charging stations,clients and income of the
                        company.</p>
<p class="services-more"><a href="eCharging.html">More</a></p>
</div>
</div>
</div>
<!-- End Serivces Row 1 -->
<!-- Begin Services Row 2 -->
<div class="row services-row services-row-tail services-row-2">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated fadeInLeft" data-wow-offset="40" style="height: 460px;">
<p class="services-icon"><span class="fa fa-desktop fa-5x"></span></p>
<h4 class="services-title">State Land Information Management System (eSlims) for â Mahaweli Authority of Sri Lanka
                        ( eMahaweli)</h4>
<p class="services-body">This is a customization of eSlims developed for Land Commissioner Department for the
                        purpose of replacing current manual system of Land registration in Railway Department. The Land registration
                        part from eSlims is used with some customization. Land leases and payment of arrears are introduced as new
                        modules under Railway department.</p>
<!-- <p class="services-more"><a href="#">Find Out More</a></p> -->
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated zoomIn" data-wow-offset="40" style="height: 460px;">
<p class="services-icon"><i class="fa fa-mobile fa-5x"></i></p>
<h4 class="services-title">MOBILE SERVICES</h4>
<p class="services-body">Araaya develops high quality mobile games for Android and iPhones. We have a range of
                            board and
                            puzzle games which
                            are very comprehensive, addictive and can be played in a short time. Our technology enables
                            multiplayer game play connecting across Android and iPhones.</p>
<!-- <p class="services-more"><a href="http://www.araaya.com/products.html">More</a></p> -->
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="services-group wow animated fadeInRight" data-wow-offset="40" style="height: 460px;">
<p class="services-icon"><i class="fa fa-mobile fa-5x"></i></p>
<h4 class="services-title">ONLINE GAMING PLATFORM</h4>
<p class="services-body">Araaya offers a free gaming platform where players can challenge friends to play single
                            player or multiplayer games
                            online, players can organize matches/ tournaments and view global leaderboards, players can have chat while
                            playing.
                            Simultaneous games play is also supported for training games. Each multiplayer game provides the facility to
                            play with
                            computer players at different difficulty levels.</p>
<!-- <p class="services-more"><a href="http://23.23.201.229/ai-games/">Play Now</a></p> -->
</div>
</div>
</div>
<!-- End Serivces Row 2 -->
</div>
</section>
<!-- End #services-section -->
<!-- Footer -->
<footer>
<div class="container">
<div class="row">
<div class="col-md-4">
<h3><i class="fa fa-map-marker"></i> Contact:</h3>
<p class="footer-contact">Araaya Business Solutions (Pvt) Ltd.</p>
<p class="footer-contact">41/A, Lili Aveneu,</p>
<p class="footer-contact">Robert Gunawardana Road,</p>
<p class="footer-contact">Baththaramulla,</p>
<p class="footer-contact">Sri Lanka.</p><br/>
<p class="footer-contact">Phone: +94 (0)11 288 4738</p>
<em class="block-author">Email: info@araaya.com </em> <br/>
</div>
<div class="col-md-4">
<h3><i class="fa fa-external-link"></i> Links</h3>
<p> <a href="#"> About ( Who we are )</a></p>
<p> <a href="#"> Services ( What we do )</a></p>
<p> <a href="#"> Contact ( Feel free to contact )</a></p>
<p> <a href="#"> Blog ( Write to us )</a></p>
<p> <a href="#"> Team ( Meet the Team )</a></p>
</div>
<div class="col-md-4">
<h3><i class="fa fa-heart"></i> Socialize</h3>
<div id="social-icons">
<a class="btn-group google-plus" href="#">
<i class="fa fa-google-plus"></i>
</a>
<a class="btn-group linkedin" href="#">
<i class="fa fa-linkedin-square"></i>
</a>
<a class="btn-group twitter" href="#">
<i class="fa fa-twitter"></i>
</a>
<a class="btn-group facebook" href="#">
<i class="fa fa-facebook"></i>
</a>
</div>
</div>
</div>
</div>
</footer>
<div class="copyright text center">
<p>© Copyright 2019, Araaya Business Solution (Pvt) Ltd </p>
</div>
<script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script>
      new WOW().init();
    </script>
</body>
</html>
