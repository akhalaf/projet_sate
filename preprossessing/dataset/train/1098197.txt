<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Builder Alert</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- Loading Bootstrap -->
<link href="https://builder.banaa.com/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<!-- Loading Flat UI -->
<link href="https://builder.banaa.com/build/sent.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
<div class="row">
<br/>
<div class="col-md-offset-3 col-md-6">
<div class="alert alert-info">
<button class="close fui-cross" type="button"></button>
<h4 class="text-center">404, Not Found!</h4>
<div id="content">
<p>Oops! You have requested the page that is no longer there.</p>
</div>
</div>
<p class="small text-center">Builder</p>
</div><!-- /.col-md-12 -->
</div>
</div>
<!-- /.container -->
</body>
</html>
