<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "96837",
      cRay: "6111229ced68dd2a",
      cHash: "50d5000970020ff",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYml0Y2x1Yi5iei8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "p+vy9dgza99h43GsalcCIGBaNFhpxA6SUmIuoA5ITn4Wfu077Bytya1jEGqhykp2J2Qc4ocU5xQalIUqEfEp8ndvT46qW6L35GA9l/+0dok54uHcjP/L9bko6DxTmMUqkKLtHo8ie5tb/Ila95vTmoxB3H5S+pQl92dKP+bwJIjUDSB8hXElCYm2s3U5va7jNYInYFv/krZjsBXjdzenh0mU0qCE9ypo3gOkUEXGf/huKw1/NIdNDZVj5Y2lEATybIXPet1CzHZ9/KyV/lTqIiULEoChWrGMaGjO1uwv+KaGuczB/kjYg9AuTjp9cNICS14v03b7KLFPAzd91y4IUJKq41HSc2it6UKkzrtJ2rRoPhyhuLapueECI1eNuWBWn2K45giTTdMnmk01piir7w+skeSeh4dVvIAkcXnS6FqFvLQcOxmAqm1v4xtJWol96dW9QfxY7dn28lTEc1emmVipckVHSSEJQV3DHUGQb9x75f4nEBS3jvzlssIybmrBm+KkGClVYvG4AadwSOKzktEt/Rd4WEHUTOHwyVy4eVJ4YyLvPbJqlEkXCnlQYu7PJ9S4lp/HSOf5xWLjZeXBv9rm3fr+nt7mvW8aOU+ClKV0yFvOHgvEpnRl22IjsJJJXJDd6yskDjYbpAb0aFI/RQONJ86+02TCzt44h9KIS31BlYwf950JFRR/YCtKB0PYasoNlwWvdgDi6vKrIny5AeKZG4FCwJPZMdmMHSc5jza9Az8ZAvPyRfFtW1YMlbOxOEtjL3l7jVvBUATsesmnDA==",
        t: "MTYxMDU2MjA2Ny45OTQwMDA=",
        m: "08GFZgYPoTbGhPL+luv+rarGRRhEepGgH/NE4oiOwJ8=",
        i1: "F0dqSOUw2i2BM8x90Yijkg==",
        i2: "tKR2R//LBDZDrJyGQlf2Dw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "qF+0XIGUB3CNBz0/QEHadEq7yG3p5E8xCgUCur05TE8=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6111229ced68dd2a");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> bitclub.bz.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=b5ad11fd99d4662f6400e2eb94e6af45b71fce06-1610562067-0-Aba-zM5Wm2ajeKFeSjCD-sgoFXsfJny_YXX97kgIjkxt1hPvhKs2hMf0AYmY75ne_9pHKOYgO4PhgIUF3jsXHG8tluvXHESIl6mqkl21eoEJ8RpGeIW8UT0mnbjneNbDacPP3V_6PQJIvqQ02dsNAJA9DOYHB8B22tSB5EonssQL-e9E9ozvMv_NQmClbmffr0XRjm1t457nE-8IHI8lfCDbTWsY2FUxssI0ISXzRSOE3Ub8wSqCA0FxQeMhr2zFOGvAna_SnUBtZ9k8gY85pDKhnTXJseCoeCHb7l0E-1aftQBe7VJYy-FTj5QXckVe3ZCdR5vAvo35MDHAHJHCWM8" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="659195bbedac6b75bc9892bad3488c31fda4024b-1610562067-0-AV6dWBDOctC8oEFf88b14dKuvDPRPDEbzZx4lYriVWrVoGoIKaBerx6BxF54HlCjAs3CFRtunhiT3TrydH9JlCEKAILq09sSwjDdd2p5Pj7noHUzNkOB5bQKkYatZKYrKbyrN268zid/uumx7erhfiSSg3WzTI3M0FRIPIFhUtCKXpQYVMMktqJF86azNP9ef/fkk0NtmnFDwhinN4DqMEh9NADrcKLJcXJMphIH1OPlN8AK+IoBuykWJCB0E3UpZXmH+6jBHbXH2Ln+DU5aRr2UBblgw+DbpLD3HfGTqEXA588/tDB1IbQ9e1066YKzAUE3SIUyWmV9SGfl4zG9rlYdUjHiFFVidPS1UYxqbjkwYMmurT2sv0toY4HqXZbXsIV1NTWD83SqQ69wuGR0m/TtGC/+2Wa3mmMQjtT3SLjRbl2QuewJXeVDPBUew/XNGso2/MtOi0rC1EVHUnURjD2c9ArYYbLuNuD336yTN/N8FV+u4J1zHzTTwfzKjcs1GXD1K8wdK9qKrCPH8uV0o85BIQr4qns/gSuEQvHfjSIF6lp7iJw3dfGloqC3/SsOsDYjc0N6HEg1eJYENY1mUNaaBfdnkFiji849FJDr4ZZWA+T31KbCS3LGb7MfOZglarCm9QOzlC3fusAIgr6cG8tMUZA4Rhy8FozVW/j5w2cWW1wBlcPuGCoEPH5uEoYdO5xoIcrZu5kwvxohdC/of9IyZw9aYVSipvAIoJbRH1AWfnx6k9lRze6U5LzBG7XPhsNVzrWNw/HC4xDYuR3EAZnnQiADoSBisZuJ8JQUuldFPFEtat2wyKGF5Vc5zIn1PjhCADIK9Y7gcg1CfgA1dm2gS65vbricMt/jckks3Dujh4ffzpoI38Hw4TjWns0MoaptF8dK3Z2KywzCp1sw1mbszEo5sN0GLbow8YRK1Xb89Dq+pDX5pOUvZZzN/Ywgclcx60yB3IBAgooFbFDZ5FdvUXN3MgBkXxGB6O+x+fVwXClRigjWPEev6ZuPjuQ3sue1lrQPRJjeprDKV0ADRyk0Oj2Q5FjBa2yRk6o2+za0TnbyGJCzWcp1IxPVcEto9b/JPYAZX8y5SsFFW5H6+tejx20+o6b/AD59O7XnJLKEAGYJ7KMFswsSgii2THHPlMosKOF/lIUqjqV0FUP9Lb1LbNll8bn/LxVoKQXNRvLCySLSRkJG3fmXErUfVgnsUow/Mwo0kWdgn1/MUYOT0O2KTgXBF6Guldlthzl+qRrC0OjM2prPwzmeciw9gdbnbuky+apmFoDh1IZsj3tpiKqEwWq1sXuBUhkuY8BNRAKmdKNI4mAW5wka44KpW2pG9g=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="59f7f7fa9e9ae70423b870b8671ed27c"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610562071.994-yEYAF+OtFR"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6111229ced68dd2a')"> </div>
</div>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://munkhey.com/ratpacket.php?s=5926">table</a></div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6111229ced68dd2a</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
