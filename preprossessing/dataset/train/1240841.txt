<!DOCTYPE html>
<html class="no-js" lang="fi">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>Sivua ei löydy – RNC Sport Club</title>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.rnc.fi/feed/" rel="alternate" title="RNC Sport Club » syöte" type="application/rss+xml"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-121854033-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-121854033-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.rnc.fi\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.rnc.fi/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/css/public.css?ver=4.3.9" id="easy-facebook-likebox-plugin-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/css/font-awesome.css?ver=4.3.9" id="easy-facebook-likebox-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/css/animate.css?ver=4.3.9" id="easy-facebook-likebox-animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/popup/magnific-popup.css?ver=4.3.9" id="easy-facebook-likebox-popup-styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,600" id="mh-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/style.css?ver=2.7.5" id="mh-magazine-lite-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/includes/font-awesome.min.css" id="mh-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rnc.fi/wp-content/plugins/tablepress/css/default.min.css?ver=1.9.1" id="tablepress-default-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[]","home_url":"https:\/\/www.rnc.fi","hash_tracking":"false"};
/* ]]> */
</script>
<script src="https://www.rnc.fi/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.0" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/popup/jquery.magnific-popup.min.js?ver=4.3.9" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/js/jquery.cookie.js?ver=4.3.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var public_ajax = {"ajax_url":"https:\/\/www.rnc.fi\/wp-admin\/admin-ajax.php"};
var public_ajax = {"ajax_url":"https:\/\/www.rnc.fi\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/js/public.js?ver=4.3.9" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/js/scripts.js?ver=2.7.5" type="text/javascript"></script>
<link href="https://www.rnc.fi/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.rnc.fi/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.rnc.fi/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
<!--[if lt IE 9]>
<script src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/js/css3-mediaqueries.js"></script>
<![endif]-->
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #fcfcfc; background-image: url("https://www.rnc.fi/wp-content/uploads/2018/01/622524_436698273041754_587884278_o.jpg"); background-position: center center; background-size: cover; background-repeat: repeat; background-attachment: fixed; }
</style>
<link href="https://www.rnc.fi/wp-content/uploads/2018/01/cropped-logo-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.rnc.fi/wp-content/uploads/2018/01/cropped-logo-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.rnc.fi/wp-content/uploads/2018/01/cropped-logo-1-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.rnc.fi/wp-content/uploads/2018/01/cropped-logo-1-270x270.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			
.mh-meta-comments {
    display: none;
}

.mh-meta-author {
    display: none;
}

.tablepress tfoot th, .tablepress thead th {
    background-color: #e64946;
 		color: #fff;
}

.mh-custom-header {
	background-color: #e1e1e1;
}		</style>
</head>
<body class="error404 custom-background wp-custom-logo mh-right-sb" id="mh-mobile" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div id="fb-root"></div>
<div class="mh-container mh-container-outer">
<div class="mh-header-mobile-nav mh-clearfix"></div>
<header class="mh-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="mh-container mh-container-inner mh-row mh-clearfix">
<div class="mh-custom-header mh-clearfix">
<div class="mh-site-identity">
<div class="mh-site-logo" itemscope="itemscope" itemtype="http://schema.org/Brand" role="banner">
<a class="custom-logo-link" href="https://www.rnc.fi/" itemprop="url" rel="home"><img alt="RNC Sport Club" class="custom-logo" height="181" itemprop="logo" src="https://www.rnc.fi/wp-content/uploads/2018/01/cropped-logo.png" width="283"/></a></div>
</div>
</div>
</div>
<div class="mh-main-nav-wrap">
<nav class="mh-navigation mh-main-nav mh-container mh-container-inner mh-clearfix" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="menu-paavalikko-container"><ul class="menu" id="menu-paavalikko"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://www.rnc.fi/etusivu/">RNC Sport Club ry</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-37" id="menu-item-37"><a href="https://www.rnc.fi/lajit/">Lajit</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40" id="menu-item-40"><a href="https://www.rnc.fi/lajit/thainyrkkeily/">Thainyrkkeily</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44" id="menu-item-44"><a href="https://www.rnc.fi/lajit/kuntothai/">Kuntothai</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://www.rnc.fi/lajit/mma/">MMA (Vapaaottelu)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58" id="menu-item-58"><a href="https://www.rnc.fi/lajit/bjj-brasilialainen-jujutsu/">BJJ (Brasilialainen jujutsu)</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59" id="menu-item-59"><a href="https://www.rnc.fi/lajit/lukkopaini/">Lukkopaini</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115" id="menu-item-115"><a href="https://www.rnc.fi/harjoitusajat/">Harjoitusajat</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a href="https://www.rnc.fi/harjoitusryhmat/">Harjoitusryhmät ja hinnasto</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84" id="menu-item-84"><a href="https://www.rnc.fi/ilmoittautuminen/">Ilmoittautuminen</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-74" id="menu-item-74"><a href="https://www.rnc.fi/yhteystiedot/">Yhteystiedot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296" id="menu-item-296"><a href="https://www.rnc.fi/media/">Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-346" id="menu-item-346"><a href="https://www.rnc.fi/valmentajat/">Valmentajat</a></li>
</ul></div> </nav>
</div>
</header><div class="mh-wrapper mh-clearfix">
<div class="mh-content" id="main-content" itemprop="mainContentOfPage" role="main">
<header class="page-header">
<h1 class="page-title">
				Sivua ei löydy			</h1>
</header>
<div class="entry-content mh-widget">
<div class="mh-box">
<p>Emme valitettavasti löytäneet etsimääsi. Ehkä hakutoiminnosta on apua.</p>
</div>
<h4 class="mh-widget-title mh-404-search">
<span class="mh-widget-title-inner">
					Etsi				</span>
</h4>
<form action="https://www.rnc.fi/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Haku:</span>
<input class="search-field" name="s" placeholder="Haku …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Hae"/>
</form> </div>
<div class="404-recent-articles mh-widget-col-2"><div class="mh-widget"><h4 class="mh-widget-title"><span class="mh-widget-title-inner">Recent Articles</span></h4> <ul class="mh-custom-posts-widget mh-clearfix"> <li class="post-554 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2020/12/30/kuntothain-starttikurssi-helmikuussa-2021/" title="Kuntothain starttikurssi helmikuussa 2021"><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2020/12/30/kuntothain-starttikurssi-helmikuussa-2021/" title="Kuntothain starttikurssi helmikuussa 2021">
									Kuntothain starttikurssi helmikuussa 2021								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>30.12.2020</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2020/12/30/kuntothain-starttikurssi-helmikuussa-2021/#mh-comments">0</a></span>
</div>
</div>
</li> <li class="post-548 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2020/12/30/peruskurssit-helmikuussa-2021/" title="Peruskurssit helmikuussa 2021"><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2020/12/30/peruskurssit-helmikuussa-2021/" title="Peruskurssit helmikuussa 2021">
									Peruskurssit helmikuussa 2021								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>30.12.2020</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2020/12/30/peruskurssit-helmikuussa-2021/#mh-comments">0</a></span>
</div>
</div>
</li> <li class="post-525 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2020/06/01/ohjatut-treenit-jatkuu-1-6/" title="Ohjatut treenit jatkuu 1.6."><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2020/06/01/ohjatut-treenit-jatkuu-1-6/" title="Ohjatut treenit jatkuu 1.6.">
									Ohjatut treenit jatkuu 1.6.								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>1.6.2020</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2020/06/01/ohjatut-treenit-jatkuu-1-6/#mh-comments">0</a></span>
</div>
</div>
</li> <li class="post-521 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2020/04/06/koronatiedote/" title="Koronatiedote"><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2020/04/06/koronatiedote/" title="Koronatiedote">
									Koronatiedote								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>6.4.2020</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2020/04/06/koronatiedote/#mh-comments">0</a></span>
</div>
</div>
</li> <li class="post-215 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2018/09/14/suomi-mma-cup-7/" title="Suomi MMA Cup 7"><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2018/09/14/suomi-mma-cup-7/" title="Suomi MMA Cup 7">
									Suomi MMA Cup 7								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>14.9.2018</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2018/09/14/suomi-mma-cup-7/#mh-comments">0</a></span>
</div>
</div>
</li> <li class="post-180 mh-custom-posts-item mh-custom-posts-small mh-clearfix">
<figure class="mh-custom-posts-thumb">
<a href="https://www.rnc.fi/2018/07/04/rnc-sport-club-ry-vuosikokouskutsu-11-7-2018/" title="RNC Sport Club Ry vuosikokouskutsu 11.7.2018"><img alt="Ei kuvaa" class="mh-image-placeholder" src="https://www.rnc.fi/wp-content/themes/mh-magazine-lite/images/placeholder-small.png"/> </a>
</figure>
<div class="mh-custom-posts-header">
<p class="mh-custom-posts-small-title">
<a href="https://www.rnc.fi/2018/07/04/rnc-sport-club-ry-vuosikokouskutsu-11-7-2018/" title="RNC Sport Club Ry vuosikokouskutsu 11.7.2018">
									RNC Sport Club Ry vuosikokouskutsu 11.7.2018								</a>
</p>
<div class="mh-meta mh-custom-posts-meta">
<span class="mh-meta-date updated"><i class="fa fa-clock-o"></i>4.7.2018</span>
<span class="mh-meta-comments"><i class="fa fa-comment-o"></i><a class="mh-comment-count-link" href="https://www.rnc.fi/2018/07/04/rnc-sport-club-ry-vuosikokouskutsu-11-7-2018/#mh-comments">0</a></span>
</div>
</div>
</li> </ul></div> </div>
</div>
<aside class="mh-widget-col-1 mh-sidebar" itemscope="itemscope" itemtype="http://schema.org/WPSideBar"> <div class="mh-widget widget_recent_entries" id="recent-posts-2"> <h4 class="mh-widget-title"><span class="mh-widget-title-inner">Ajankohtaista</span></h4> <ul>
<li>
<a href="https://www.rnc.fi/2020/12/30/kuntothain-starttikurssi-helmikuussa-2021/">Kuntothain starttikurssi helmikuussa 2021</a>
<span class="post-date">30.12.2020</span>
</li>
<li>
<a href="https://www.rnc.fi/2020/12/30/peruskurssit-helmikuussa-2021/">Peruskurssit helmikuussa 2021</a>
<span class="post-date">30.12.2020</span>
</li>
<li>
<a href="https://www.rnc.fi/2020/06/01/ohjatut-treenit-jatkuu-1-6/">Ohjatut treenit jatkuu 1.6.</a>
<span class="post-date">1.6.2020</span>
</li>
<li>
<a href="https://www.rnc.fi/2020/04/06/koronatiedote/">Koronatiedote</a>
<span class="post-date">6.4.2020</span>
</li>
<li>
<a href="https://www.rnc.fi/2018/09/14/suomi-mma-cup-7/">Suomi MMA Cup 7</a>
<span class="post-date">14.9.2018</span>
</li>
</ul>
</div><div class="mh-widget widget_easy_facebook_feed" id="easy_facebook_feed-2"><h4 class="mh-widget-title"><span class="mh-widget-title-inner">Facebook</span></h4><div class="efbl_feed_wraper" id="efbl_feed_7"><div class="efbl_fb_story fullwidth link efbl_no_image efbl_has_message " id="efblcf"><div class="efbl_post_content"><div class="efbl_author_info"><div class="efbl_name_date">
<p class="efbl_author_name"> <a href="https://facebook.com/155908554454062" target="_blank">RNC Sport Club ry</a></p>
<p class="efbl_story_time">2 years 6 months ago</p>
</div>
</div><div class="efbl_content_wraper"><p class="efbl_story_text">Seuran vuosikokous pidetään keskiviikkona 11.7.18 klo 19.15 salilla. Käy katsomassa…</p><div class="efbl_shared_story efbl_has_link_image "><a class="efbl_link_image" href="https://rnc.myclub.fi/flow/notifications" re="nofollow" target="_blank"><img alt="RNC Sport Club Ry (myClub)" src="https://external.xx.fbcdn.net/safe_image.php?d=AQBloCWHO1LlCVn7&amp;w=130&amp;h=130&amp;url=https%3A%2F%2Fd1kgctjlkvcjq1.cloudfront.net%2Fassets%2Fmyclub_orange_original-723761e75565c3180cd2b05cf2b6266cfd49fa5248b2c2a26be377d849adc949.png&amp;cfs=1&amp;_nc_hash=AQALWs4ec9hFRxZr"/></a><div class="efbl_link_text"><p class="efbl_title_link"><a href="https://rnc.myclub.fi/flow/notifications" target="_blank">RNC Sport Club Ry (myClub)</a></p><p class="efbl_link_caption">rnc.myclub.fi</p><p class="efbl_link_description">myClub is a management service for your sports- and free-time club. With myClub you can track your members' attendance, manage payments and fees, keep a member register and communicate with all your members.</p></div></div></div></div><div class="efbl_story_meta"><!--Readmore div started-->
<div class="efbl_read_more_link">
<a class="efbl_read_full_story" href="https://www.facebook.com/155908554454062_1892584090786491" target="_blank">Read full story</a>
<a class="efbl_share_links" href="javascript:void(0)">Share</a>
<span class="efbl_links_container">
<a class="efbl_facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/155908554454062_1892584090786491" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="efbl_twitter" href="https://twitter.com/intent/tweet?text=https://www.facebook.com/155908554454062_1892584090786491" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="efbl_linked_in" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.facebook.com/155908554454062_1892584090786491" target="_blank"><i class="fa fa-linkedin"></i></a>
<a class="efbl_google_plus" href="https://plus.google.com/share?url=https://www.facebook.com/155908554454062_1892584090786491" target="_blank"><i class="fa fa-google-plus"></i></a>
</span>
</div>
<!--Readmore div end--></div></div><div class="efbl_fb_story fullwidth photo efbl_no_image efbl_has_message " id="efblcf"><div class="efbl_post_content"><div class="efbl_author_info"><div class="efbl_name_date">
<p class="efbl_author_name"> <a href="https://facebook.com/155908554454062" target="_blank">RNC Sport Club ry</a></p>
<p class="efbl_story_time">2 years 6 months ago</p>
</div>
</div><div class="efbl_content_wraper"><p class="efbl_story_text">Tänään taas RNC:n taistelijat vauhdissa💪🏼👊🏻💥</p><p class="story_description">Hyvää huomenta kamppailuväki! Tänään moni onkin jo suuntaamassa juhannuksen viettoon. Kannattaakin ajoissa viritellä immaf.tv -palvelun tili kuntoon että pääset seuraamaan EM-kilpailuiden semifinaalipäivää.<br/>
<br/>
Bout 13 Flyweight 56.7 kg (125 lbs) Ghita Iulia Luiza (Romania) vs. Janika Antinmaa, WFC Warriors<br/>
<br/>
Bout 14 Bantamweight 61.2 kg (135 lbs) Karolina Hulkko vs.<br/>
Frida Vastamäki (MMA-landslaget)<br/>
<br/>
Bout 16 Featherweight 65.8 kg (145 lbs) Fabiana Giampà (Italy) vs. Jenni Kivioja, Sisu Gym Järvenpää<br/>
<br/>
Bout 17 Featherweight 65.8 kg (145 lbs) Julia Dorny (German Mixed Martial Arts Federation e.V. - GEMMAF vs. Anniina Ervasti, MMA Lappeenranta<br/>
<br/>
Bout 22 Featherweight 65.8 kg (145 lbs) Mici Saaristo, Finnfightersgym vs.Ciaran Breslin (Northern Ireland)<br/>
<br/>
Bout 23 Featherweight 65.8 kg (145 lbs) Eemil Kurhela, RNC Sport Club ry vs. Amir Malekpour (Sweden)<br/>
<br/>
Bout 29 Middleweight 83.9 kg (185 lbs) Dario Bellandi (FIGMMA - Federazione Italiana Grappling Mixed Martial Arts)<br/>
vs. Tommi Leinonen, RNC Sport Club ry<br/>
<br/>
Bout 33 Heavyweight 120.2 kg (265 lbs) Ryan Spillane (Ireland) VS Matias Anttila, Finnfightersgym<br/>
<br/>
Ottelut suorana immaf.tvn kautta. Ensimmäinen suomalainen nousee häkkiin arviolta klo 14 aikaan.</p></div></div><div class="efbl_story_meta"><!--Readmore div started-->
<div class="efbl_read_more_link">
<a class="efbl_read_full_story" href="https://www.facebook.com/SuomenVapaaotteluliitto/photos/a.555765514561466.1073741827.174135076057847/1170709186400426/?type=3" target="_blank">Read full story</a>
<a class="efbl_share_links" href="javascript:void(0)">Share</a>
<span class="efbl_links_container">
<a class="efbl_facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/SuomenVapaaotteluliitto/photos/a.555765514561466.1073741827.174135076057847/1170709186400426/?type=3" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="efbl_twitter" href="https://twitter.com/intent/tweet?text=https://www.facebook.com/SuomenVapaaotteluliitto/photos/a.555765514561466.1073741827.174135076057847/1170709186400426/?type=3" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="efbl_linked_in" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.facebook.com/SuomenVapaaotteluliitto/photos/a.555765514561466.1073741827.174135076057847/1170709186400426/?type=3" target="_blank"><i class="fa fa-linkedin"></i></a>
<a class="efbl_google_plus" href="https://plus.google.com/share?url=https://www.facebook.com/SuomenVapaaotteluliitto/photos/a.555765514561466.1073741827.174135076057847/1170709186400426/?type=3" target="_blank"><i class="fa fa-google-plus"></i></a>
</span>
</div>
<!--Readmore div end--></div></div><div class="efbl_fb_story fullwidth photo efbl_no_image efbl_has_message " id="efblcf"><div class="efbl_post_content"><div class="efbl_author_info"><div class="efbl_name_date">
<p class="efbl_author_name"> <a href="https://facebook.com/155908554454062" target="_blank">RNC Sport Club ry</a></p>
<p class="efbl_story_time">2 years 6 months ago</p>
</div>
</div><div class="efbl_content_wraper"><p class="efbl_story_text">Keskiviikon BJJ:t vedettiin todella kovalla edustuksella naapurikaupungista. Kiitos taas kerran…</p></div></div><div class="efbl_story_meta"><div class="efbl_info"><span class="efbl_comments">
<span class="efbl_comments_text"><i class="fa fa-comment-o"></i></span>
<span class="efbl_comments_counter"> 2 </span>
</span></div><!--Readmore div started-->
<div class="efbl_read_more_link">
<a class="efbl_read_full_story" href="https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" target="_blank">Read full story</a>
<a class="efbl_share_links" href="javascript:void(0)">Share</a>
<span class="efbl_links_container">
<a class="efbl_facebook" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" target="_blank"><i class="fa fa-facebook"></i></a>
<a class="efbl_twitter" href="https://twitter.com/intent/tweet?text=https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" target="_blank"><i class="fa fa-twitter"></i></a>
<a class="efbl_linked_in" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" target="_blank"><i class="fa fa-linkedin"></i></a>
<a class="efbl_google_plus" href="https://plus.google.com/share?url=https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" target="_blank"><i class="fa fa-google-plus"></i></a>
</span>
</div>
<!--Readmore div end--><div class="efbl_comments_wraper"><div class="efbl_comments"><div class="efbl_commenter_image"><a href="https://facebook.com/" rel="nofollow" target="_blank" title="Timeline Photos">
<img alt="Timeline Photos" height="32" src="https://graph.facebook.com//picture" width="32"/>
</a></div><div class="efbl_comment_text"><a class="efbl_comenter_name" href="https://facebook.com/" rel="nofollow" target="_blank" title="Timeline Photos">
</a><p class="efbl_comment_message">Totta töriset taas Markus Marque Frigård. Oikeasti mut nostettiin painien jälkeen roikkumaan noiden Riksulaisten kivilihojen väliin. Itse painimisen hoidin tietenkin vaan naisten kanssa, lyhyillä erillä ja pitkillä tauoilla. Silti löi ihan totaalisesti vanteelle.</p><p class="efbl_comment_time_n_likes"><span class="efbl_comment_like"><i class="fa fa-thumbs-o-up"></i> 2</span> - <span class="efbl_comment_time">2 years 6 months ago </span></p></div></div><div class="efbl_comments"><div class="efbl_commenter_image"><a href="https://facebook.com/" rel="nofollow" target="_blank" title="Timeline Photos">
<img alt="Timeline Photos" height="32" src="https://graph.facebook.com//picture" width="32"/>
</a></div><div class="efbl_comment_text"><a class="efbl_comenter_name" href="https://facebook.com/" rel="nofollow" target="_blank" title="Timeline Photos">
</a><p class="efbl_comment_message">Harri niin hapoilla että pitää ottaa ukoista tukee että pysyy jaloillaan, ollu kovia eriä!🤙🤙</p><p class="efbl_comment_time_n_likes"><span class="efbl_comment_like"><i class="fa fa-thumbs-o-up"></i> 3</span> - <span class="efbl_comment_time">2 years 6 months ago </span></p></div></div><div class="efbl_comments_footer">
<a href="https://www.facebook.com/RNCSportClub/photos/a.161435630568021.35636.155908554454062/1872589622785938/?type=3" rel="nofollow" target="_blank"><i class="fa fa-comment-o"></i> comment on facebook </a>
</div></div></div></div><div class="efbl_custom_likebox"><div id="fb-root"></div>
<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.async=true; 
					  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=395202813876688";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script> <div class="efbl-like-box 1">
<img class="efbl-loader" src="https://www.rnc.fi/wp-content/plugins/easy-facebook-likebox/public/assets/images/loader.gif"/>
<div class="fb-page" data-adapt-container-width="true" data-animclass=" " data-height="500" data-hide-cover="false" data-hide-cta="false" data-href="https://www.facebook.com/155908554454062" data-show-facepile="false" data-show-posts="false" data-small-header="false" data-width="">
</div>
</div>
</div><input id="item_number" type="hidden" value=""/></div></div></aside></div>
<div class="mh-copyright-wrap">
<div class="mh-container mh-container-inner mh-clearfix">
<p class="mh-copyright">© 2021 RNC Sport Club</p>
</div>
</div>
</div><!-- .mh-container-outer -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.rnc.fi\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.rnc.fi/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LfCwoUUAAAAAHqJqpmrmXtC5Kj4lT58IjFAVT-Y&amp;ver=3.0" type="text/javascript"></script>
<script src="https://www.rnc.fi/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
<script type="text/javascript">
( function( grecaptcha, sitekey ) {

	var wpcf7recaptcha = {
		execute: function() {
			grecaptcha.execute(
				sitekey,
				{ action: 'homepage' }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		}
	};

	grecaptcha.ready( wpcf7recaptcha.execute );

	document.addEventListener( 'wpcf7submit', wpcf7recaptcha.execute, false );

} )( grecaptcha, '6LfCwoUUAAAAAHqJqpmrmXtC5Kj4lT58IjFAVT-Y' );
</script>
</body>
</html>