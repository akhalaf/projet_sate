<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="fi-fi" xml:lang="fi-fi" xmlns="http://www.w3.org/1999/xhtml">
<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!--<base href="http://arjalaiho.fi/" />-->
<meta content="index, follow" name="robots"/>
<title>Etusivu - Arja Laiho</title>
<script src="media/system/js/core.js" type="text/javascript"></script>
<script src="media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="media/system/js/caption.js" type="text/javascript"></script>
<link href="templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="templates/system/css/general.css" rel="stylesheet" type="text/css"/>
<link href="templates/arjalaih2/css/template.css" media="screen" rel="stylesheet" type="text/css"/>
<!--[if IE 6]><link rel="stylesheet" href="/templates/arjalaih2/css/template.ie6.css" type="text/css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="/templates/arjalaih2/css/template.ie7.css" type="text/css" media="screen" /><![endif]-->
<script type="text/javascript">if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
<script src="templates/arjalaih2/jquery.js" type="text/javascript"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script src="templates/arjalaih2/script.js" type="text/javascript"></script>
<script type="text/javascript">if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12160972-34']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body class="nep-j16">
<div id="nep-main">
<div class="cleared reset-box"></div>
<div class="nep-nav">
<div class="nep-nav-l"></div>
<div class="nep-nav-r"></div>
<div class="nep-nav-outer">
<div class="nep-nav-wrapper">
<div class="nep-nav-inner">
<div class="nep-hmenu-extra1">
<div class="custom">
<p><a><img alt="" border="0" src="images/arjalaiho_logo_www250.png"/></a></p></div></div>
<ul class="nep-hmenu"><li class="active item102" id="current"><a class=" active" href="index.html"><span class="l"></span><span class="r"></span><span class="t">Etusivu</span></a></li><li class="item103"><a href="psykoterapia.html"><span class="l"></span><span class="r"></span><span class="t">Psykoterapia</span></a></li><li class="item104"><a href="tyoenohjaus.html"><span class="l"></span><span class="r"></span><span class="t">Työnohjaus</span></a></li><li class="item105"><a href="perheterapia.html"><span class="l"></span><span class="r"></span><span class="t">Perheterapia</span></a></li><li class="item114"><a href="mielenhuolto.html"><span class="l"></span><span class="r"></span><span class="t">Mielenhuolto</span></a></li></ul></div>
</div>
</div>
</div>
<div class="cleared reset-box"></div>
<div class="cleared reset-box"></div>
<div class="eskuva"> <div class="random-image-eskuva">
<img alt="polkukuva-etusivulle-1.jpg" height="400" src="images/etusivu/polkukuva-etusivulle-1.jpg" width="1051"/></div> </div>
<div class="nep-sheet">
<div class="nep-sheet-cc"></div>
<div class="nep-sheet-body">
<div class="nep-content-layout">
<div class="nep-content-layout-row">
<div class="nep-layout-cell nep-sidebar1">
<div class="nep-block">
<div class="nep-block-body">
<div class="nep-blockcontent">
<div class="nep-blockcontent-body">
<div class="custom">
<p><img alt="" border="0" src="images/arja-esittelykuva_200.jpg"/></p>
<p>puh. 040 8613 546<br/>Rauma<br/>Lähetä sähköpostia: 
 <script type="text/javascript">
 <!--
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy79902 = '&#97;rj&#97;' + '&#64;';
 addy79902 = addy79902 + '&#97;rj&#97;l&#97;&#105;h&#111;' + '&#46;' + 'f&#105;';
 document.write('<a ' + path + '\'' + prefix + ':' + addy79902 + '\'>');
 document.write(addy79902);
 document.write('<\/a>');
 //-->\n </script><script type="text/javascript">
 <!--
 document.write('<span style=\'display: none;\'>');
 //-->
 </script>TÃ¤mÃ¤ sÃ¤hkÃ¶postiosoite on suojattu spamboteilta. Tarvitset JavaScript-tuen nÃ¤hdÃ¤ksesi sen.
 <script type="text/javascript">
 <!--
 document.write('</');
 document.write('span>');
 //-->
 </script></p></div>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
</div>
</div>
<div class="nep-block">
<div class="nep-block-body">
<div class="nep-blockcontent">
<div class="nep-blockcontent-body">
<ul class="menu"><li class="item113"><a href="esittely-arja-laiho.html">Esittely Arja Laiho</a></li><li class="item112"><a href="koulutus.html">Koulutus</a></li><li class="item111"><a href="hinnasto.html">Hinnasto</a></li></ul>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
</div>
<div class="nep-layout-cell nep-content">
<div class="item-page"><div class="nep-post">
<div class="nep-post-body">
<div class="nep-post-inner">
<div class="nep-postcontent">
<div class="nep-article"><p>Mikä määritelmä kuvaisi elämää osuvasti, kertoisi siitä oleellisen tai määrittelisi sen sisällön? <br/><br/>Ajattelen usein elämää matkana. Eri aikoina kuljen erilaisilla teillä ja eri kulkuvälineillä. Pysähtelenkin välillä.<br/><br/>Elämänmatkalla tapahtuu joskus liikaa tai liian hämmentävää tai kipeästi koskettavaa. Toisinaan kulku ei vain suju niinkuin toivoisi tai se käy yksinäiseksi.<br/><br/>Tarjoan sinulle elämän solmukohtiin kanssakulkijan. Kuljemme kappaleen matkaa yhdessä asioita jakaen, pohtien ja ratkaisuja löytäen. Avaat solmuja ja matkasi jatkuu edelleen.<br/><br/>Miten voisin olla avuksi?</p> </div>
</div>
<div class="cleared"></div>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
<div class="cleared"></div>
<div class="nep-footer">
<div class="nep-footer-t"></div>
<div class="nep-footer-body">
<div class="nep-footer-text">
</div>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
</div>
</div>
<div class="cleared"></div>
<div class="cleared"></div>
</div>
</body>
</html>