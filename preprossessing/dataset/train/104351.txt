<!DOCTYPE html>
<html lang="en-GB">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2017, by DNN Corporation -->
<!--*********************************************-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-61261564-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61261564-1');
</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache, no-store, must-revalidate" http-equiv="Cache-Control"/>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="0" http-equiv="Expires"/>
<!-- iphone dialog -->
<script>
   function getCookie(cname) {
    var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
		}
	}
	return "";
	}
	
	function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

   function removeIPhoneDialog(){
      $("#iphone-dialog").remove();
	  setCookie("iPhoneApp", 1, 7);
   }
   window.addEventListener("load", function(){
     

	var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
	var cookie = getCookie("iPhoneApp");
	if(iOS && (!cookie || cookie == "")){
		$("body").append('<div id="iphone-dialog" role="dialog" aria-live="polite" aria-label="cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window cc-banner cc-type-info cc-theme-block cc-bottom cc-color-override-821008809 " style="color: rgb(123, 122, 127);background-color: rgb(234, 247, 247);font-size:1.5rem"><span id="cookieconsent:desc" class="cc-message">Thank you for visiting our site. We have a free mobile app that will help you get the news on the go. Please <a aria-label="download" role="button" tabindex="0" class="cc-link" href="https://itunes.apple.com/us/app/alphagalileo/id687076998?mt=8" rel="noopener noreferrer nofollow" target="_blank">Download</a> it from the apple store  </span><div class="cc-compliance"><a aria-label="dismiss cookie message" role="button" tabindex="0" class="cc-btn cc-dismiss" onclick="removeIPhoneDialog()">Ok</a></div></div>');
	}
   });


</script>
<script>
window.addEventListener("load", function() {
setTimeout(function(){
  window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#efeeec",
                "text": "#7b7a7f"
            },
            "button": {
                "background": "#7b7a7f",
                "text": "#ffffff"
            }
        },
        "content": {
            "message": "This website is GDPR compliant",
            "href": "/AlphaGalileo/Legal/Privacy-Policy"
        }
    });
});
}, 1000);

  
</script><title>
	AlphaGalileo &gt; AlphaGalileo
</title><meta content=",DotNetNuke,DNN" id="MetaKeywords" name="KEYWORDS"/><meta content="DotNetNuke " id="MetaGenerator" name="GENERATOR"/><meta content="INDEX, FOLLOW" id="MetaRobots" name="ROBOTS"/><link href="/DependencyHandler.axd/f97a579dcd931514ed3f57a747f80d09/536/css" rel="stylesheet" type="text/css"/><script src="/DependencyHandler.axd/f22c234a9ec82f339afa12f65dce3619/536/js" type="text/javascript"></script><link href="/DTControls/DocumentsUploaderControl/Scripts/lightbox.min.css" rel="stylesheet"/><link href="/Portals/0/Favicon_red_release.ico?ver=2020-11-04-185211-617" rel="SHORTCUT ICON" type="image/x-icon"/></head>
<body id="Body">
<form action="/Default.aspx?tabid=33&amp;error=An%20unexpected%20error%20has%20occurred&amp;content=0" enctype="multipart/form-data" id="Form" method="post">
<div class="aspNetHidden">
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="Z+AkIyjNXDEMd/p+mz7kRzzhf69xl3tsMsgQLxTgqvMnFtc6u7wonmIVEsvpPVROr+7C1X1U9Myd4IPACGNWKjdSLZEePp5d+NzRsvoPZxZxmn1xixz/HmB+ECzowKsvEJR7GGS79yad0U956PwBaS0IvAvpn89rPt2uXglv6B/PvBtjJ6/A6UDDncEVb0F88NaBqAe5F8RTay30mgf7ZSLgPcoe0fWzTGLRjzyXNgZysFSCDaWL0nZscf27RqCy5LIkRHPUpGuk6kPTpRvNRH70KSGCKAyO+1ypClmcHpjcKhgruYKGAiANN+qZmoUDatHHEIXHgrWNiHpZgoIZltCL5fNZrRJVMD29aA4v3IdHqwV/pmA4SB3XbAzdeKG35bl4dQ=="/>
</div>
<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"en-GB","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":".","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":",","CurrencySymbol":"£","NaNSymbol":"NaN","CurrencyNegativePattern":1,"NumberNegativePattern":1,"PercentPositivePattern":1,"PercentNegativePattern":1,"NegativeInfinitySymbol":"-∞","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":".","NumberGroupSeparator":",","CurrencyPositivePattern":0,"PositiveInfinitySymbol":"∞","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":".","PercentGroupSeparator":",","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"AM","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402300799999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":"/","FirstDayOfWeek":1,"CalendarWeekRule":2,"FullDateTimePattern":"dd MMMM yyyy HH:mm:ss","LongDatePattern":"dd MMMM yyyy","LongTimePattern":"HH:mm:ss","MonthDayPattern":"d MMMM","PMDesignator":"PM","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"dd/MM/yyyy","ShortTimePattern":"HH:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy","AbbreviatedDayNames":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"ShortestDayNames":["Su","Mo","Tu","We","Th","Fr","Sa"],"DayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"AbbreviatedMonthNames":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",""],"MonthNames":["January","February","March","April","May","June","July","August","September","October","November","December",""],"IsReadOnly":false,"NativeCalendarName":"Gregorian Calendar","AbbreviatedMonthGenitiveNames":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",""],"MonthGenitiveNames":["January","February","March","April","May","June","July","August","September","October","November","December",""]},"eras":[1,"A.D.",null,0]};//]]>
</script>
<script src="/ScriptResource.axd?d=NJmAwtEo3IptHsdz3-jERcHlHAbI60TOxZlVbL8YCP72WSH7HDdR23A5VvXneoDufsgIbYHLn6Tm6j7ImU2nwAZHKpTxhd06UguYEn9LueyrVALrbhiZd1V2Mc-0piaD_LOpyQ2&amp;t=ffffffffdfc97409" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=dwY9oWetJoJMBMWSl4hTMJ7Zlo4U8VzEtCfTfzBt8gBls6m_5pP5soaUNtDDhGCGYLtWa3xnQJq-iYyKlhQUXwzyn3qagQ0jT5kktjupV1aIW_kzsXK9FAihAXNrRdSIZ9QBGaJg6XnyDCXc0&amp;t=ffffffffdfc97409" type="text/javascript"></script>
<div class="aspNetHidden">
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="CA0B0334"/>
<input id="__VIEWSTATEENCRYPTED" name="__VIEWSTATEENCRYPTED" type="hidden" value=""/>
</div><script src="/DependencyHandler.axd/9e755f1620d2ac5116d6003c6f7cca17/536/js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', ['tdnn$ctr404$_UP','dnn_ctr404__UP'], [], [], 120, '');
//]]>
</script>
<!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<div class="skin ">
<div id="siteWrapper" style="">
<!-- UserControlPanel  -->
<!--<div id="topHeader">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div id="search-top" class="pull-right small-screens hidden-sm hidden-md hidden-lg">
                        <span id="dnn_dnnSearch2_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSearch2$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSearch2_txtSearch" class="NormalTextBox" aria-label="Search" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText" title="Clear search text"></a>
    </span>
    <a id="dnn_dnnSearch2_cmdSearch" class="SearchButton" href="javascript:__doPostBack(&#39;dnn$dnnSearch2$cmdSearch&#39;,&#39;&#39;)">Search</a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'en-GB',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSearch2_SiteRadioButton');
            var webBtn = $('#dnn_dnnSearch2_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="search hidden-xs">
                        <span id="dnn_dnnSearch_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSearch$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSearch_txtSearch" class="NormalTextBox" aria-label="Search" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText" title="Clear search text"></a>
    </span>
    <a id="dnn_dnnSearch_cmdSearch" class="SearchButton" href="javascript:__doPostBack(&#39;dnn$dnnSearch$cmdSearch&#39;,&#39;&#39;)">Search</a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'en-GB',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSearch_SiteRadioButton');
            var webBtn = $('#dnn_dnnSearch_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                    </div>
                    
                    <a href="#" id="search-action">
                    </a>
                    <div id="login" class="pull-right">
                        
<div id="dnn_dnnLogin_loginGroup" class="loginGroup">
    <a id="dnn_dnnLogin_enhancedLoginLink" title="Login" class="LoginLink" rel="nofollow" onclick="this.disabled=true;" href="https://www.alphagalileo.org/en-gb/Login-details?returnurl=%2fDefault.aspx%3ftabid%3d33%26error%3dAn%2520unexpected%2520error%2520has%2520occurred%26content%3d0">Login</a>
</div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<!--Header -->
<div class="aroundHead">
<header class="container headerwr" role="banner">
<div class="row dnnpane">
<div class="col-md-12 bannerInner">
<div class="MainHeadInn" id="mainHeader-inner">
<div class="clearfix"></div>
<div class="row dnnpane">
<div class="col-md-9">
<div class="navbar navbar-default" role="navigation">
<div id="Logo-top-wrapper">
<div id="logo">
<span class="brand">
<a href="https://www.alphagalileo.org/en-gb/" id="dnn_dnnLOGO_hypLogo" title="AlphaGalileo"><img alt="AlphaGalileo" id="dnn_dnnLOGO_imgLogo" src="/Portals/0/2017-header-sharp4.png?ver=2020-11-04-185211-847"/></a>
</span>
</div>
<div class="text-underlogo col-md-12" id="dnn_header_100_1"><div class="DnnModule DnnModule-DNN_HTML DnnModule-399"><a name="399"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr399_ContentPane"><div class="dnnFormMessage dnnFormValidationSummary" id="dnn_ctr399_ctl01_dnnSkinMessage">
<span class="dnnModMessageHeading" id="dnn_ctr399_ctl01_lblHeading">A critical error has occurred. Please check the Event Viewer for further details.</span>
<span id="dnn_ctr399_ctl01_lblMessage"> </span>
</div>
<!-- Start_Module_399 --><div class="DNNModuleContent ModDNNHTMLC" id="dnn_ctr399_ModuleContent">
</div><!-- End_Module_399 --></div>
<div class="clear"></div>
</div>
</div></div>
</div>
</div>
</div>
<div class="col-md-3">
<div class="for-the-login" id="dnn_FORTHELOGIN"><div class="DnnModule DnnModule-Authentication DnnModule-404"><a name="404"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr404_ContentPane"><!-- Start_Module_404 --><div class="DNNModuleContent ModAuthenticationC" id="dnn_ctr404_ModuleContent">
<div id="dnn_ctr404__UP">
</div><div id="dnn_ctr404__UP_Prog" style="display:none;">
<div class="dnnLoading dnnPanelLoading"></div>
</div>
</div><!-- End_Module_404 --></div>
<div class="clear"></div>
</div>
</div></div>
<div class="userTopRight"><a class="SkinObject" href="https://www.alphagalileo.org/en-gb/UserRegistration?returnurl=https%3a%2f%2fwww.alphagalileo.org%2fen-gb%2f" id="dnn_dnnUser_registerLink" rel="nofollow" title="Register">Register</a>
<a class="showwhenloggedin" href="/AlphaGalileo/ctl/Logoff" id="dnn_dnnLogin2_enhancedLoginLink" rel="nofollow" title="Logout">Logout</a></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="language">
</div>
</div>
</div>
</header>
</div>
<!-- Page Content -->
<main role="main">
<div class="container">
<div class="row dnnpane">
<div class="col-md-12 headerPane" id="dnn_HeaderPane"><div class="DnnModule DnnModule-UserSelectedPreferencesEditor DnnModule-1679"><a name="1679"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1679_ContentPane"><!-- Start_Module_1679 --><div class="DNNModuleContent ModUserSelectedPreferencesEditorC" id="dnn_ctr1679_ModuleContent">
</div><!-- End_Module_1679 --></div>
<div class="clear"></div>
</div>
</div><div class="DnnModule DnnModule-UserSelectedPreferencesEditor DnnModule-1678"><a name="1678"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1678_ContentPane"><!-- Start_Module_1678 --><div class="DNNModuleContent ModUserSelectedPreferencesEditorC" id="dnn_ctr1678_ModuleContent">
</div><!-- End_Module_1678 --></div>
<div class="clear"></div>
</div>
</div></div>
</div>
<div id="mainContent-inner">
<div class="row dnnpane">
<div class="col-md-12 contentPane DNNEmptyPane" id="dnn_ContentPane"></div>
</div>
<div class="row dnnpane">
<div class="col-md-8 spacingTop DNNEmptyPane" id="dnn_P1_75_1"></div>
<div class="col-md-4 spacingTop DNNEmptyPane" id="dnn_P1_25_2"></div>
</div>
<div class="row dnnpane">
<div class="col-md-4 spacingTop DNNEmptyPane" id="dnn_P2_25_1"></div>
<div class="col-md-8 spacingTop DNNEmptyPane" id="dnn_P2_75_2"></div>
</div>
<div class="row dnnpane">
<div class="col-md-4 spacingTop DNNEmptyPane" id="dnn_P3_33_1"></div>
<div class="col-md-4 spacingTop DNNEmptyPane" id="dnn_P3_33_2"></div>
<div class="col-md-4 spacingTop DNNEmptyPane" id="dnn_P3_33_3"></div>
</div>
<div class="row dnnpane">
<div class="col-md-2 spacingTop padding-left5" id="P5_33_1">
<div class="LinksTop-Left" id="dnn_P5_33_1_122"><div class="DnnModule DnnModule-UserSelectedPreferencesEditor DnnModule-1680"><a name="1680"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1680_ContentPane"><!-- Start_Module_1680 --><div class="DNNModuleContent ModUserSelectedPreferencesEditorC" id="dnn_ctr1680_ModuleContent">
</div><!-- End_Module_1680 --></div>
<div class="clear"></div>
</div>
</div></div>
<div class="navbar navbar-default " role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="nav-style">
<div class="AroundNavigation" role="navigation">
<div class="nav pull-right" id="navbar">
<div class="RegistrationLogButtonsM"><a class="dnnPrimaryActionn LOGB" href="/Login-details" id="dnn_ctr404_Login_Login_DNN_cmdLogin" title="Login">Login</a> <a class="dnnSecondaryActionn REGB" href="/en-gb/UserRegistration?returnurl=http%3a%2f%2falphag.digitaltradingco.co.uk%2fen-gb%2f" id="dnn_ctr404_Login_Login_DNN_registerLink">Register</a>
</div>
<div class="HomeLink"> <a href="">Home</a></div>
<div class="AroundLanguage">
<div class="languageLabel">
<h3>Language</h3>
</div>
<div class="l"><div class="language-object">
<select class="NormalTextBox" id="dnn_LANGUAGE1_selectCulture" name="dnn$LANGUAGE1$selectCulture">
<option selected="selected" value="en-GB">English (United Kingdom)</option>
<option value="fr-FR">Français (France)</option>
<option value="de-DE">Deutsch (Deutschland)</option>
<option value="es-ES">Español (España, Alfabetización Internacional)</option>
</select>
</div>
</div>
</div>
<ul>
<li class="dropdown active">
<a href="https://www.alphagalileo.org/en-gb/" target="">AlphaGalileo</a>
<ul>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/AlphaGalileo-News" target="">AlphaGalileo News</a>
</li>
<li class="dropdown ">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us" target="">About us</a>
<ul>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/Who-we-are" target="">Who we are</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/Jobs-at-AlphaGalileo" target="">Jobs at AlphaGalileo</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/How-the-service-works" target="">How the service works</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/Global-reach" target="">Global reach</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/Services-to-the-media" target="">Services to the media</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/What-our-users-say" target="">What our users say</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/About-us/Who-uses-us" target="">Who uses us</a>
</li>
</ul>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Prices" target="">Prices</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Advertising" target="">Advertising</a>
</li>
<li class="dropdown ">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help" target="">Help</a>
<ul>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/Posting-policy" target="">Posting policy</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/Keywords-guide" target="">Keywords guide</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/AlphaGalileo-knowledge-base" target="">AlphaGalileo knowledge base</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/FAQs" target="">FAQs</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/Your-content-statistics" target="">Your content statistics</a>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Help/Cookies-and-AlphaGalileo" target="">Cookies and AlphaGalileo</a>
</li>
</ul>
</li>
<li class="">
<a href="https://www.alphagalileo.org/en-gb/AlphaGalileo/Contact-us" target="">Contact us</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="addverts-left" id="dnn_P5_33_1_2"><div class="DnnModule DnnModule-AdvertFrontend DnnModule-1640"><a name="1640"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1640_ContentPane"><!-- Start_Module_1640 --><div class="DNNModuleContent ModAdvertFrontendC" id="dnn_ctr1640_ModuleContent">
</div><!-- End_Module_1640 --></div>
<div class="clear"></div>
</div>
</div></div>
</div>
<div class="col-md-7 spacingTop" id="dnn_P5_33_2" style="width: 601px;"><div class="DnnModule DnnModule-AdvertFrontend DnnModule-1753"><a name="1753"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1753_ContentPane"><!-- Start_Module_1753 --><div class="DNNModuleContent ModAdvertFrontendC" id="dnn_ctr1753_ModuleContent">
</div><!-- End_Module_1753 --></div>
<div class="clear"></div>
</div>
</div></div>
<div class="col-md-3 spacingTop" id="dnn_P5_33_3"><div class="DnnModule DnnModule-DNN_HTML DnnModule-1676"><a name="1676"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1676_ContentPane"><!-- Start_Module_1676 --><div class="DNNModuleContent ModDNNHTMLC" id="dnn_ctr1676_ModuleContent">
</div><!-- End_Module_1676 --></div>
<div class="clear"></div>
</div>
</div><div class="DnnModule DnnModule-AdvertFrontend DnnModule-1642"><a name="1642"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1642_ContentPane"><!-- Start_Module_1642 --><div class="DNNModuleContent ModAdvertFrontendC" id="dnn_ctr1642_ModuleContent">
</div><!-- End_Module_1642 --></div>
<div class="clear"></div>
</div>
</div></div>
</div>
<div class="row dnnpane">
<div class="col-md-3 spacingTop DNNEmptyPane" id="dnn_P5_25_1"></div>
<div class="col-md-6 spacingTop DNNEmptyPane" id="dnn_P5_50_2"></div>
<div class="col-md-3 spacingTop DNNEmptyPane" id="dnn_P5_25_3"></div>
</div>
<div class="row dnnpane">
<div class="col-md-12 contentPane spacingTop DNNEmptyPane" id="dnn_ContentPaneLower"></div>
</div>
</div><!-- /.mainContent-inner -->
</div><!-- /.container -->
</main><!-- /.mainContent -->
<!-- Footer -->
<footer role="contentinfo">
<div class="footer-above">
<div class="container">
<div class="row dnnpane addrel">
<div class="bottom-footer footer-col col-md-12" id="dnn_footer_25_1"><div class="DnnModule DnnModule-DNN_HTML DnnModule-1653"><a name="1653"></a>
<div class="DNNContainer_noTitle">
<div id="dnn_ctr1653_ContentPane"><!-- Start_Module_1653 --><div class="DNNModuleContent ModDNNHTMLC" id="dnn_ctr1653_ModuleContent">
</div><!-- End_Module_1653 --></div>
<div class="clear"></div>
</div>
</div></div>
<!--
<div id="dnn_dnnLogin2_loginGroup" class="loginGroup">
    <a id="dnn_dnnLogin2_enhancedLoginLink" title="Login" class="LoginLink" rel="nofollow" onclick="this.disabled=true;" href="https://www.alphagalileo.org/en-gb/Login-details?returnurl=%2fDefault.aspx%3ftabid%3d33%26error%3dAn%2520unexpected%2520error%2520has%2520occurred%26content%3d0">Login</a>
</div>-->
<!--
                    
                    <div id="dnn_footer_25_2" class="footer-col col-md-3 col-sm-6 DNNEmptyPane"></div>
                    <div class="clearfix visible-sm"></div>
                    <div id="dnn_footer_25_3" class="footer-col col-md-3 col-sm-6 DNNEmptyPane"></div>
                    <div id="dnn_footer_25_4" class="footer-col col-md-3 col-sm-6 DNNEmptyPane"></div>
					-->
</div>
</div>
</div>
<div class="footer-below">
<div class="container">
<div class="row dnnpane">
<div class="col-md-12">
<div class="copyright">
<span class="SkinObject" id="dnn_dnnCopyright_lblCopyright">Copyright 2021 by DNN Corp</span>
</div>
<!--<div class="terms-priv">
                            
                            <a id="dnn_dnnTerms_hypTerms" class="SkinObject" rel="nofollow" href="https://www.alphagalileo.org/en-gb/Terms">Terms Of Use</a>
                            |
					        <a id="dnn_dnnPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="https://www.alphagalileo.org/en-gb/Privacy">Privacy Statement</a>
                        </div>-->
</div>
</div>
</div>
</div>
</footer>
</div>
</div>
<!-- /.SiteWrapper -->
<!--CDF(Css|/Portals/_default/skins/alphagalileo-from-xci/bootstrap/css/bootstrap.min.css)-->
<!--CDF(Css|/Portals/_default/skins/alphagalileo-from-xci/css/jquery.smartmenus.bootstrap.css)-->
<!--CDF(Css|/Portals/_default/skins/alphagalileo-from-xci/Menus/MainMenu/MainMenu.css)-->
<!--CDF(Css|/Portals/_default/skins/alphagalileo-from-xci/skin.css)-->
<!--CDF(Javascript|/Portals/_default/skins/alphagalileo-from-xci/bootstrap/js/bootstrap.min.js)-->
<!--CDF(Javascript|/Portals/_default/skins/alphagalileo-from-xci/js/jquery.smartmenus.js)-->
<!--CDF(Javascript|/Portals/_default/skins/alphagalileo-from-xci/js/jquery.smartmenus.bootstrap.js)-->
<!--CDF(Javascript|/Portals/_default/skins/alphagalileo-from-xci/js/scripts.js)-->
<script type="text/javascript">
$( ".language-object option" ).each(function( index ) {$( this ).text($( this ).text().replace(/\s*\(.*?\)\s*/g, ''));});
if(window["__cultureInfo"] != null && window["__cultureInfo"].name != null && window["__cultureInfo"].name.indexOf("fr") > -1){
		var replaced = $(".language-object").html().replace('value="en-GB">English</option>','value="en-GB">Anglais</option>');
		replaced = replaced.replace('value="fr-FR">Français</option>','value="fr-FR">Français</option>');
		replaced = replaced.replace('value="de-DE">Deutsch</option>','value="de-DE">Allemand</option>');
		replaced = replaced.replace('value="es-ES">Español</option>','value="es-ES">Espagnol</option>');
		$(".language-object").html(replaced);
		$(".AroundNavigation ul").html($(".AroundNavigation ul").html().replace('AlphaGalileo (fr-FR)','AlphaGalileo'));
}
	else if(window["__cultureInfo"] != null && window["__cultureInfo"].name != null && window["__cultureInfo"].name.indexOf("de") > -1){
		var replaced = $(".language-object").html().replace('value="en-GB">English</option>','value="en-GB">Englisch</option>');
		replaced = replaced.replace('value="fr-FR">Français</option>','value="fr-FR">Französisch</option>');
		replaced = replaced.replace('value="de-DE">Deutsch</option>','value="de-DE">Deutsch</option>');
		replaced = replaced.replace('value="es-ES">Español</option>','value="es-ES">Spanisch</option>');
		$(".language-object").html(replaced);
		$(".AroundNavigation ul").html($(".AroundNavigation ul").html().replace('AlphaGalileo (de-DE)','AlphaGalileo'));
	}else if(window["__cultureInfo"] != null && window["__cultureInfo"].name != null && window["__cultureInfo"].name.indexOf("es") > -1){
		var replaced = $(".language-object").html().replace('value="en-GB">English</option>','value="en-GB">Inglés</option>');
		replaced = replaced.replace('value="fr-FR">Français</option>','value="fr-FR">Francés</option>');
		replaced = replaced.replace('value="de-DE">Deutsch</option>','value="de-DE">Alemán</option>');
		replaced = replaced.replace('value="es-ES">Español</option>','value="es-ES">Español</option>');
		$(".language-object").html(replaced);
		$(".AroundNavigation ul").html($(".AroundNavigation ul").html().replace('AlphaGalileo (es-ES)','AlphaGalileo'));
	}



</script>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script async="" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<!-- Albacross -->
<script type="text/javascript">
              (function(a,l,b,c,r,s){_nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];r.async=1;
              r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;s.parentNode.insertBefore(r,s);
              })(document,"script","serve.albacross.com/track.js","89473054");
            </script>
<!-- END: Albacross -->
<input id="ScrollTop" name="ScrollTop" type="hidden"/>
<input autocomplete="off" id="__dnnVariable" name="__dnnVariable" type="hidden" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`33`}"/>
<input name="__RequestVerificationToken" type="hidden" value="VVaEZ6LjcR_t2TJBVv2Hf_4XBJP_5P3nHIb_81TqyK9w_xuyXaB4nY7tyHD26xND2nLcFw2"/>
<script type="text/javascript">
//<![CDATA[
Sys.Application.add_init(function() {
    $create(Sys.UI._UpdateProgress, {"associatedUpdatePanelId":"dnn_ctr404__UP","displayAfter":500,"dynamicLayout":true}, null, null, $get("dnn_ctr404__UP_Prog"));
});
//]]>
</script>
</form>
<!--CDF(Javascript|/js/dnncore.js)--><!--CDF(Javascript|/js/dnn.modalpopup.js)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css)--><!--CDF(Css|/Portals/_default/skins/alphagalileo-from-xci/skin.css)--><!--CDF(Css|/DesktopModules/Admin/Authentication/module.css)--><!--CDF(Css|/DesktopModules/Admin/Authentication/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/UserSelectedPreferencesEditor/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/DesktopModules/AdvertFrontend/module.css)--><!--CDF(Css|/Resources/Search/SearchSkinObjectPreview.css)--><!--CDF(Javascript|/Resources/Search/SearchSkinObjectPreview.js)--><!--CDF(Css|/Resources/Search/SearchSkinObjectPreview.css)--><!--CDF(Javascript|/Resources/Search/SearchSkinObjectPreview.js)--><!--CDF(Javascript|/js/dnn.js)--><!--CDF(Javascript|/js/dnn.servicesframework.js)--><!--CDF(Javascript|/Portals/_default/skins/alphagalileo-from-xci/ResponsiveNav/responsive.js)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js)-->
</body>
</html>