<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://bookretriever.com/"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Book Retriever is an application that allows teachers to manage and organize their classroom libraries and checkout books to students. There are also companion apps for scanning books available." name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>Classroom Library Organization &amp; Checkout System - Book Retriever</title>
<link href="/?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/assets/sass/build/bookretriever-1563551544077348859-min.css" rel="stylesheet" type="text/css"/>
<script src="/assets/js/build/bookretriever-1563551544077348859-min.js" type="text/javascript"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet"/>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '196474567556735');
    fbq('track', 'PageView');
    </script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=196474567556735&amp;ev=PageView
    &amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-73331060-1', 'bookretriever.com');
  ga('send', 'pageview');
</script>
<!-- Universal Google Analytics Plugin by PB Web Development -->
</head>
<body id="home">
<div id="main-banner">
<div id="navbar-desk">
<div class="container">
<div class="row">
<div class="col-sm-3 col-xs-4">
<img alt="Book Retriever" id="main-logo" src="/images/book-retriever-logo.png"/>
</div>
<div class="col-sm-9 col-xs-8">
<ul class="nav menu">
<li class="item-127"><a href="/features">Features</a></li><li class="item-128"><a href="/pricing">Pricing</a></li><li class="item-129"><a href="https://app.bookretriever.com/auth/register">Sign-Up</a></li><li class="item-116"><a href="http://support.bookretriever.com/support/#/docs/366/book-retriever">Support</a></li><li class="item-126"><a href="/contact-us">Contact Us</a></li><li class="item-101 default current active"><a href="/">Home</a></li><li class="item-130"><a class="login" href="https://app.bookretriever.com">Sign In</a></li><li class="item-113"><a class="facebook" href="http://www.facebook.com/bookretriever/" rel="noopener noreferrer" target="_blank">Facebook</a></li><li class="item-114"><a class="twitter" href="http://www.twitter.com/mybookretriever" rel="noopener noreferrer" target="_blank">Twitter</a></li><li class="item-160"><a class="pinterest" href="http://www.pinterest.com/bookretriever/" rel="noopener noreferrer" target="_blank">Pinterest</a></li></ul>
<div class="callbtn" id="mobile-toggle">
<a href="#">
<div class="menu-icon">
<span></span>
<span></span>
<span></span>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="main-banner-content">
<h1>Your students are unique.<br/> Find the perfect book for each one.</h1>
<p>
                  Level | Track | Match
                </p>
<div class="callbtn">
<a href="https://app.bookretriever.com/auth/register">Start your 30 Day Free Trial Today!</a>
</div>
<p class="nocc">
                  No credit card required!
                </p>
</div>
</div>
<div class="container">
<div class="roi-callout">
<h3>
                See how much money you save when you use Book Retriever.
            </h3>
<a href="/cost-savings-calculator">Use Our Cost Savings Calculator</a>
</div>
</div>
<div id="intro-section">
<div class="container">
<div class="flex">
<div class="copy">
<h2>Match Students to the Right Book Using Our Classroom Library Tools</h2>
<p>By quickly leveling your books and tracking student reading history, you can make sure every student is taking a home book that aligns with their reading skills and interests. <br/><br/>
                        And our checkout system will give you the confidence that your students will return your books.  The average classroom library loses about 10-20% every year. Cutting down the loss rate to just a few percent which saves you money on replacement cost.
</p>
</div>
<div class="image">
<img alt="Book Retriever" src="/images/bookretriever-lead-screen.jpg"/>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12">
<blockquote>
                  “Book Retriever not only helps me manage my growing classroom library, but it also helps me find just the right books for students' reading level and interest. I decided to assign "locations" by genre, so I could easily find books to match a student's interest. Sometimes, though, I need a book on a particular level; it's so easy to search my collection by level online to find the perfect match for a student.”
                  <div>Annette K., 6th Grade Teacher</div>
</blockquote>
</div>
</div>
</div>
<div id="benefits-section">
<div class="container">
<div class="flex">
<div class="feature">
<div class="image">
<img alt="Leveled Reading Grades" src="/images/leveled-icon.svg"/>
</div>
<div class="copy">
<h3>The Most Extensive Leveling Data Available</h3>
<p>Book Retriever features the largest database of leveled data for books of any app like it. Simply scan in your books to get leveling information saving you time from searching elsewhere.</p>
</div>
</div>
<div class="feature">
<div class="image">
<img alt="Studnet Progress" src="/images/student-progress.png"/>
</div>
<div class="copy">
<h3>Student Profiles Provides Insight</h3>
<p>Every book a student checks out is logged, making it easy to see if a student is reading at grade level. This valuable information lets a teacher guide a student to a new book that will improve reading skills.</p>
</div>
</div>
<div class="feature">
<div class="image">
<img alt="Kids using Book Retriever in Student Mode" class="circle-img" src="/images/kids-on-tablets.jpg"/>
</div>
<div class="copy">
<h3>Our Scanning App Has a Student Mode</h3>
<p>Give your students the ability to check in and out books without risking important information and settings.</p>
<p>
                      Get the app for your favorite device.
                    </p>
<ul class="badge-list">
<li>
<a href="https://itunes.apple.com/us/app/book-retriever-scanning-app/id1086127313?ls=1&amp;mt=8" target="_blank">
<img alt="Book Retriever App Available on The App Store" src="/images/badge-apple.svg"/>
</a>
</li><!--
--><li>
<a href="https://play.google.com/store/apps/details?id=com.bookretriever.BookRetrieverScanningApp&amp;hl=en" target="_blank">
<img alt="Book Retriever App Available on Google Play" src="/images/badge-google.svg"/>
</a>
</li><!--
--><li>
<a href="https://www.amazon.com/Book-Retriever-Scanning-App/dp/B01H41WYEC/ref=sr_1_1?s=mobile-apps&amp;ie=UTF8&amp;qid=1466180298&amp;sr=1-1&amp;keywords=Book+Retriever" target="_blank">
<img alt="Book Retriever App Available at Amazon" src="/images/badge-amazon.svg"/>
</a>
</li>
</ul>
</div>
</div>
<div class="feature">
<div class="image">
<img alt="Barcode labels for your books" src="/images/barcode-labels.png"/>
</div>
<div class="copy">
<h3>Print Barcodes and Labels for Your Books</h3>
<p>Print leveling labels for your entire library quickly. Barcode labels allow for tracking on multiple copies of the same book.</p>
</div>
</div>
<div class="feature">
<div class="image">
<img alt="Calendar icon" src="/images/calendar.png"/>
</div>
<div class="copy">
<h3>Track Book Checkouts</h3>
<p>Know which books are overdue and who checked them out. And books can be flagged as missing, which is great for end of the school year reconciliation.</p>
</div>
</div>
<div class="feature">
<div class="image">
<img alt="Clever" class="clever" src="/images/clever-logo.svg"/>
</div>
<div class="copy">
<h3>Clever Integration for Schools and Rosters</h3>
<p>Clever enabled districts can now integrate seamlessly with Book Retriever. Teacher logins are simplified, class rosters are automatically imported, and more! <a href="https://clever.com/" target="_blank">Click here to learn more about Clever.</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="container" id="get-started">
<div class="row">
<div class="col-sm-12">
<h2>Get Started with Book Retriever Today!</h2>
</div>
<div class="col-sm-6">
<h3>For Teachers</h3>
<ul>
<li>
                Manage classroom libraries
              </li>
<li>
                Book asset management
              </li>
<li>
                Extensive leveling data
              </li>
<li>
                Teacher checks books in and out
              </li>
<li>
                Free scanning app for iOS, Android and Kindle
              </li>
<li>
                Track overdue books
              </li>
<li>
                Leveled label printing
              </li>
</ul>
</div>
<div class="col-sm-6 price-col">
<div class="price-circle">
<div class="price-circle-inner01">
<div class="price-circle-inner02">
<div class="price-circle-inner03">
<div class="price-content">
<div class="price-number">
                        3
                      </div>
<div class="price-label">
                        Per Month
                      </div>
<div class="price-fine-print">
                        $36 Billed Annually
                      </div>
</div>
</div>
</div>
</div>
</div>
<div class="callbtn">
<a href="https://app.bookretriever.com/auth/register">Start Your Free 30-Day Trial Today!</a>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<h3>For Schools and Districts</h3>
<ul>
<li>Multi-Level administration, with district and school administrators</li>
<li>Administrators can manage schools, teachers and libraries</li>
<li>Advanced analytics and reporting</li>
<li>Import &amp; export data</li>
<li>Libraries can have multiple teachers – great for bookrooms</li>
<li>Integration with Clever</li>
<li>And much more!</li>
</ul>
</div>
<div class="col-sm-6 price-col">
<div class="price-circle district">
<div class="price-circle-inner01">
<div class="price-circle-inner02">
<div class="price-circle-inner03">
<div class="price-content">
<div class="price-label">
                       Call for Price
                      </div>
</div>
</div>
</div>
</div>
</div>
<div class="callbtn">
<a href="/contact-us">Call Us for Group Pricing</a>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12" style="text-align:center">
<div class="roi-callout" style="text-align:center">
<h3>
                See how much money you save when you use Book Retriever.
              </h3>
<a href="/cost-savings-calculator">Use Our Cost Savings Calculator</a>
</div>
</div>
</div>
</div>
<footer>
<div class="container">
<div class="row">
<div class="col-sm-12">  
            </div>
</div>
<div class="row">
<div class="col-sm-12 badge-list-container">
<h3>Get Our Scanning App for Your Favorite Device</h3>
<ul class="badge-list">
<li>
<a href="https://itunes.apple.com/us/app/book-retriever-scanning-app/id1086127313?ls=1&amp;mt=8" target="_blank">
<img alt="Book Retriever App Available on The App Store" src="/images/badge-apple.svg"/>
</a>
</li><!--
--><li>
<a href="https://play.google.com/store/apps/details?id=com.bookretriever.BookRetrieverScanningApp&amp;hl=en" target="_blank">
<img alt="Book Retriever App Available on Google Play" src="/images/badge-google.svg"/>
</a>
</li><!--
--><li>
<a href="https://www.amazon.com/Book-Retriever-Scanning-App/dp/B01H41WYEC/ref=sr_1_1?s=mobile-apps&amp;ie=UTF8&amp;qid=1466180298&amp;sr=1-1&amp;keywords=Book+Retriever" target="_blank">
<img alt="Book Retriever App Available at Amazon" src="/images/badge-amazon.svg"/>
</a>
</li>
</ul>
</div>
</div>
<div class="row">
<!-- <div id="footer-menu" class="col-sm-12">
          <ul class="nav menu">
<li class="item-127"><a href="/features" >Features</a></li><li class="item-128"><a href="/pricing" >Pricing</a></li><li class="item-129"><a href="https://app.bookretriever.com/auth/register" >Sign-Up</a></li><li class="item-116"><a href="http://support.bookretriever.com/support/#/docs/366/book-retriever" >Support</a></li><li class="item-126"><a href="/contact-us" >Contact Us</a></li><li class="item-101 default current active"><a href="/" >Home</a></li><li class="item-130"><a href="https://app.bookretriever.com" class="login">Sign In</a></li><li class="item-113"><a href="http://www.facebook.com/bookretriever/" class="facebook" target="_blank" rel="noopener noreferrer">Facebook</a></li><li class="item-114"><a href="http://www.twitter.com/mybookretriever" class="twitter" target="_blank" rel="noopener noreferrer">Twitter</a></li><li class="item-160"><a href="http://www.pinterest.com/bookretriever/" class="pinterest" target="_blank" rel="noopener noreferrer">Pinterest</a></li></ul>

        </div> -->
<div class="col-sm-12">
<div class="foot-logo-wrap">
<img alt="Book Retriever" class="foot-logo" src="/images/book-retriever-logo-clrtxt.png"/>
</div>
<div class="foot-contact">
<div class="custom">
<p>Ã‚Â© 2016 Book Retriever | 206 W. Argonne, Lower Level | St. Louis, MO 63122 | 314.677.2125</p></div>
</div>
<div class="foot-contact">
<div class="custom">
<ul>
<li><a href="/become-a-partner">Become a Partner</a></li>
<li><a href="/contact-us">Contact Us</a></li>
<li><a href="/privacy-policy">Privacy Policy</a></li>
<li><a href="/terms-of-service">Terms of Service</a></li>
</ul></div>
</div>
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="//bookretriever.us12.list-manage.com/subscribe/post?u=91b5570f8e584b691689c880c&amp;id=a6ef00a2ed" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
<div id="mc_embed_signup_scroll">
<div class="mc-field-group">
<input class="required email" id="mce-EMAIL" name="EMAIL" placeholder="Sign-up for Updates" type="email" value=""/>
</div>
<div id="mce-responses">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div aria-hidden="true" style="position: absolute; left: -5000px;"><input name="b_91b5570f8e584b691689c880c_a6ef00a2ed" tabindex="-1" type="text" value=""/></div>
<input class="button" id="mc-embedded-subscribe" name="subscribe" type="submit" value="&gt;"/>
</div>
</form>
<div class="clear"></div>
<p></p>
</div>
<!--End mc_embed_signup-->
</div>
</div>
</div>
</footer>
</body>
</html>
