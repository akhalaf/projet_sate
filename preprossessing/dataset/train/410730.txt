<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<title>  Страница не найдена</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/img/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/css/reset.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/css/style_vn.css" rel="stylesheet" type="text/css"/>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/font/font.css" media="screen, projection" rel="stylesheet" type="text/css"/>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/hoverIntent.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/superfish.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/prettyCheckboxes.js" type="text/javascript"></script>
<script type="text/javascript">
		 jQuery(function($) {
			 $('ul.sf-menu').superfish();
		 });
	</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type=checkbox],input[type=radio]').prettyCheckboxes({
			checkboxWidth: 17, // The width of your custom checkbox
			checkboxHeight: 17, // The height of your custom checkbox
			className : 'prettyCheckbox', // The classname of your custom checkbox
			display: 'list' // The style you want it to be display (inline or list)
		});
	});
	</script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/fancybox/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/fancybox/jquery.fancybox-conf.js" type="text/javascript"></script>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/js/fancybox/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/fancy.js" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-content/themes/dezprofil/js/jcarousellite.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function($){
	$(".gall").jCarouselLite({
			btnNext: ".next",
			btnPrev: ".prev",
			mouseWheel: false,
			visible: 4
		});
	});
	</script>
<script type="text/javascript">
		function fontSize() {
		var width = 1000; // ширина, от которой идет отсчет
		var fontSize = 10; // минимальный размер шрифта
		var bodyWidth = $('.news_ob').width();
		var multiplier = bodyWidth / width;
		if ($('html').width() >= width) fontSize = Math.floor(fontSize * multiplier);
		  $('.od_news').css({fontSize: fontSize+'px'});
		}
		$(function() { fontSize(); });
		$(window).resize(function() { fontSize(); });
	</script>
<link href="https://dezprofil.ru/wp-content/themes/dezprofil/style.css" rel="stylesheet" type="text/css"/>
<link href="https://dezprofil.ru/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://dezprofil.ru/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
</head>
<body style="background: #d6bb80 url(https://dezprofil.ru/wp-content/themes/dezprofil/img_bg/bg_vn_komary.jpg) top center no-repeat;">
<div class="page">
<div class="menu_ob">
<div class="menu">
<div class="menu_left">
<ul class="sf-menu">
<li class="usl" onmouseover="this.className='hov usl'" style="width:140px"><a href="/service"><span>Услуги</span></a>
<ul class="usl">
<li class="disable"><a href="https://dezprofil.ru/service/bloxi/" title="Блохи">Блохи</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/dezinsektsiya-1/" title="Дезинсекция">Дезинсекция</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/dezinfekciya/" title="Дезинфекция">Дезинфекция</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/deratizatsiya/" title="Дератизация">Дератизация</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/iksodovye-kleshhi/" title="Иксодовые клещи">Иксодовые клещи</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/klopy/" title="Клопы">Клопы</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/komary/" title="Комары">Комары</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/krysinye-kleshhi/" title="Крысиные клещи">Крысиные клещи</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/krysy/" title="Крысы">Крысы</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/muravi/" title="Муравьи">Муравьи</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/muxi/" title="Мухи">Мухи</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/myshi/" title="Мыши">Мыши</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/ozonirovanie/" title="Озонирование">Озонирование</a></li>
<li class="disable"><a href="https://dezprofil.ru/service/osy/" title="Осы">Осы</a></li>
<li class="last"><a href="https://dezprofil.ru/service/tarakany/" title="Тараканы">Тараканы</a></li>
</ul>
</li>
<li style="width:209px; border-left:1px  #349653 solid"><a href="/catalog"><span>Каталог продукции</span></a>
<ul>
<li class="disable"><a href="https://dezprofil.ru/type/sredstva-dlya-unichtozheniya-i-otlova-nasekomyx/" title="Средства для уничтожения и отлова насекомых">Средства для уничтожения и отлова насекомых</a></li><li class="disable"><a href="https://dezprofil.ru/type/sredstva-dlya-unichtozheniya-i-otlova-gryzunov/" title="Средства для уничтожения и отлова грызунов">Средства для уничтожения и отлова грызунов</a></li><li class="disable"><a href="https://dezprofil.ru/type/dezinficiruyushhie-sredstva/" title="Дезинфицирующие средства">Дезинфицирующие средства</a></li><li class="last"><a href="https://dezprofil.ru/type/prochee/" title="Прочее">Прочее</a></li> </ul>
</li>
</ul>
</div>
<div class="menu_right">
<div onmouseout="document.home.src='https://dezprofil.ru/wp-content/themes/dezprofil/img/home_n.png'" onmouseover="document.home.src='https://dezprofil.ru/wp-content/themes/dezprofil/img/home.png'" style=" float:left; width:75px; margin-top:27px; text-align:center">
<a href="https://dezprofil.ru">
<img height="12" name="home" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/home_n.png" width="14"/>
</a>
</div>
<ul class="dezprofil-top-menu" id="top-menu"><li class="first menu-item menu-item-type-post_type menu-item-object-page" id="nav-menu-item-17"><a class="menu-link main-menu-link" href="https://dezprofil.ru/about/">О компании</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category" id="nav-menu-item-142"><a class="menu-link main-menu-link" href="https://dezprofil.ru/category/news/">Новости</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="nav-menu-item-19"><a class="menu-link main-menu-link" href="https://dezprofil.ru/education/">Обучение</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="nav-menu-item-83"><a class="menu-link main-menu-link" href="https://dezprofil.ru/nashi-klienty/">Наши клиенты</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="nav-menu-item-21"><a class="last menu-link main-menu-link" href="https://dezprofil.ru/contact/">Контакты</a></li>
<li class="helper"></li></ul> </div>
</div>
</div>
<div class="clear"></div><div class="header header-inner">
<ul>
<li style="margin-left:30px"><a href="https://dezprofil.ru"><img height="128" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/logo.png" width="339"/></a></li>
<li class="header-inner__li"><img height="14" src="https://dezprofil.ru/wp-content/uploads/2020/07/tel.png" style="margin-bottom:2px" width="212"/><br/>г. Новосибирск, ул. Кузьмы Минина, 9/2<span>, 1этаж, оф. 905</span></li>
</ul>
<div class="lng">
<ul>
<li><img height="63" name="rus" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/rus_a.png" width="62"/></li>
<li onmouseout="document.eng.src='https://dezprofil.ru/wp-content/themes/dezprofil/img/eng.png'" onmouseover="document.eng.src='https://dezprofil.ru/wp-content/themes/dezprofil/img/eng_a.png'"><a href="https://dezprofil.ru/about-the-company/"><img height="63" name="eng" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/eng.png" width="62"/></a></li>
</ul>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="obol">
<div class="sidebar">
<div class="shapka">
<div class="cherta"></div>
<h2>404</h2>
</div>
</div>
<div class="mainbar">
<div class="bread"><ul class="breadcrumb"><li><a href="https://dezprofil.ru" rel="nofollow">Главная</a><span> → </span></li><li class="breadcrumb__active">404</li></ul></div>
<div class="text">
<div class="obsch">извините, по вашему запросу ничего не найдено</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="footer_guarantor"></div>
</div>
<div class="footer">
<div class="left">
<a href="http://www.seomen.ru/" target="_blank">Создание сайта</a> - <a href="https://www.seomen.ru/" target="_blank"><img height="24" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/seomen.png" width="59"/></a>
</div>
<div class="right">
<div style="float:right; margin:7px 0 20px 5px"><img height="31" src="https://dezprofil.ru/wp-content/themes/dezprofil/img/medal.gif" width="31"/></div>
<div style="float:right; margin:7px 0 20px 10px">
<!--LiveInternet counter-->
<script type="text/javascript">
                <!--
                document.write("<a href='https://www.liveinternet.ru/click' " +
                    "target=_blank><img src='https://counter.yadro.ru/hit?t14.4;r" +
                    escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                        ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                            screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                    ";" + Math.random() +
                    "' alt='' title='LiveInternet: показано число просмотров за 24" +
                    " часа, посетителей за 24 часа и за сегодня' " +
                    "border=0 width=88 height=31><\/a>") //
                -->
            </script>
<!--/LiveInternet-->
</div>


        © 2001—2021 «Дезпрофиль» <br/>
        г. Новосибирск, ул. Кузьмы Минина, 9/2<span>, 1этаж, оф. 905</span><br/>
        (383) 347-50-20, 310-15-17, 349-90-95
    </div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/dezprofil.ru\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="https://dezprofil.ru/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Lft3a8ZAAAAAM4xj1M5Af5jbyGdsx6kVEXz19P7&amp;ver=3.0" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6Lft3a8ZAAAAAM4xj1M5Af5jbyGdsx6kVEXz19P7","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script src="https://dezprofil.ru/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?ver=5.2" type="text/javascript"></script>
<script src="https://dezprofil.ru/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>