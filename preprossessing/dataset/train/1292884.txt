<!DOCTYPE html>
<html lang="en-US">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- Site Crafted Using DMS v2.2.4 - WordPress - HTML5 - www.PageLines.com -->
<!-- Start >> Meta Tags and Inline Scripts -->
<title>Page not found – Self-Talk Plus Classrooms of the Mind</title>
<link href="//a.omappapi.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/selftalkplus.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<!-- Styles -->
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://selftalkplus.com/wp-content/plugins/sidebar-manager-light/css/otw_sbm.css?ver=5.4.4" id="otw_sbm.css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://selftalkplus.com/wp-content/uploads/pagelines/compiled-css-core-1606964789.css" id="pagelines-less-core-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://selftalkplus.com/wp-content/uploads/pagelines/compiled-css-sections-1606964789.css" id="pagelines-less-sections-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://selftalkplus.com/wp-content/plugins/popover/inc/external/wpmu-lib/css/wpmu-ui.3.min.css?ver=5.4.4" id="wpmu-wpmu-ui-3-min-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://selftalkplus.com/wp-content/plugins/popover/inc/external/wpmu-lib/css/animate.3.min.css?ver=5.4.4" id="wpmu-animate-3-min-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C600%2C800&amp;ver=0f1f20" id="master_font_import-css" media="all" rel="stylesheet" type="text/css"/>
<!-- Scripts -->
<script src="https://selftalkplus.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://selftalkplus.com/wp-content/uploads/2016/10/fav2.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://selftalkplus.com/wp-content/uploads/2016/10/touch2.png" rel="apple-touch-icon"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="" property="pl-share-title"/>
<meta content="" property="pl-share-url"/>
<meta content="" property="pl-share-desc"/>
<meta content="" property="pl-share-img"/>
<link href="https://selftalkplus.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://selftalkplus.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://selftalkplus.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<script type="text/javascript">var ajaxurl = "https://selftalkplus.com/wp-admin/admin-ajax.php"</script><meta content="Memberium v2.177 for WordPress" name="generator"/><link href="https://Memberium.com" hreflang="en" rel="help" title="Membership site system for WordPress and Infusionsoft" type="text/html"/>
<!-- On Ready -->
<script> /* <![CDATA[ */
!function ($) {
jQuery(document).ready(function() {
})
}(window.jQuery);
/* ]]> */
</script>
<link href="https://selftalkplus.com/wp-content/plugins/pagelines-customize/style.css?ver=221-0907121947" rel="stylesheet"/>
<!--[if lte IE 9]>
<script type='text/javascript' src='https://selftalkplus.com/wp-content/plugins/dms-plugin-pro/libs/js/html5.min.js'></script>
<![endif]-->
<!--[if lte IE 9]>
<script type='text/javascript' src='https://selftalkplus.com/wp-content/plugins/dms-plugin-pro/libs/js/respond.min.js'></script>
<![endif]-->
<!--[if lte IE 9]>
<script type='text/javascript' src='https://selftalkplus.com/wp-content/plugins/dms-plugin-pro/libs/js/selectivizr-min.js'></script>
<![endif]-->
<!--[if lte IE 9]>
<link rel='stylesheet' href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css' />
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87394570-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="https://yk272.infusionsoft.com/app/webTracking/getTrackingCode" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-46506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-46506603-1');
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '329842611028220');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=329842611028220&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
<style id="pagelines-custom" type="text/css">
 @media all and (max-width:767px){.nooby{display:none !important} } .socmed{padding:0 !important} .post-comments.sc a{display:none !important} .ninety.section-mediabox img{width:90% !important;max-width:90% !important;box-shadow:2px 2px 5px 0px #4f4f4f} .ninetynoshadow.section-mediabox img{width:90% !important;max-width:90%} .ninetyone.section-mediabox img{width:90% !important;max-width:90% !important} @media (max-width:750px){.ninetyone.section-mediabox img{width:50% !important;max-width:50% !important} } .ninetyone{width:90% !important;max-width:90% !important} @media (max-width:750px){.ninetyone{width:50% !important;max-width:50% !important} .optinimg{max-width:90%;height:auto} } .ninetyplus.section-mediabox img{width:90% !important;max-width:90% !important;-webkit-box-shadow:15px 15px 15px -11px rgba(0,0,0,0.32);-moz-box-shadow:15px 15px 15px -11px rgba(0,0,0,0.32);box-shadow:15px 15px 15px -11px rgba(0,0,0,0.32)} body .pl-fixed-top{border-bottom:1px solid #eee;background-color:#ffffff;box-shadow:0 1px 0 rgba(0,0,0,0)} .texty{font-size:17px;font-weight:300;line-height:1.3} .texty .hentry{width:90% !important;max-width:90% !important;margin:-30px auto 100px} .texty h6{font-size:34px;font-weight:400;margin:0px} .margy{margin-left:5% !important} strong{font-weight:600 !important} .hilitey.section-highlight .highlight-area{width:90% !important;max-width:90% !important;font-size:24px !important} .section-highlight .highlight-subhead{line-height:1.2;font-size:22px;opacity:1} .section-highlight .highlight-subhead .gform_wrapper .description,.section-highlight .highlight-subhead .gform_wrapper .gfield_description,.section-highlight .highlight-subhead .gform_wrapper .gsection_description,.section-highlight .highlight-subhead .gform_wrapper .instruction{display:none !important} .texty2{font-size:40px;width:90% !important;max-width:90% !important;line-height:1;margin-left:5%} .section-navi .pl-nav > li > a{opacity:1.0;font-size:14px;font-weight:400} iframe{height:900px !important} .shadly iframe{width:550px !important;height:450px !important} .shadly{margin-left:auto !important;margin-right:auto !important;float:none !important;margin-bottom:30px !important} @media (max-width:750px){.shadly{padding-bottom:250px !important} } @media (max-width:750px){.shadly iframe{margin-bottom:250px !important} } @media (max-width:750px){.shadly2{margin-top:180px !important} } @media (max-width:750px){.pushdown{margin-top:180px !important} } ol{margin-left:45px !important;margin-top:-35px !important} .gform_wrapper .description,.gform_wrapper .gfield_description,.gform_wrapper .gsection_description,.gform_wrapper .instruction{display:none !important} 
</style>
</head>
<!-- Start >> HTML Body -->
<body class="error404 dms pl-pro-version responsive full_width pl-save-map-on-load template-none display-full desktop">
<div class="pl-mobile-menu">
<form action="https://selftalkplus.com/" class="mm-search pl-searcher" method="get" onsubmit="this.submit();return false;"><fieldset><span class="btn-search"><i class="icon icon-search"></i></span><input class="searchfield" name="s" placeholder="Search" type="text" value=""/></fieldset></form>
<div class="mm-holder">
<ul class="mobile-menu primary-menu" id="menu-topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-223" id="menu-item-223"><a href="https://selftalkplus.com/">HOME</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-232" id="menu-item-232"><a href="https://selftalkplus.com/registration-form/">SIGN UP</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-234" id="menu-item-234"><a href="https://selftalkplus.com/login/">MEMBER LOGIN</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-238" id="menu-item-238"><a href="https://selftalkplus.com/listening-tips/">LISTENING TIPS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355" id="menu-item-355"><a href="https://selftalkplus.com/faq/">FAQ</a></li>
<li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-756" id="menu-item-756"><a href="https://www.facebook.com/drshadhelmstetter/" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
<li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-757" id="menu-item-757"><a href="https://instagram.com/drshadhelmstetter/" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
<li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-755" id="menu-item-755"><a href="https://twitter.com/ShadHelmstetter" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
</ul> <div class="social-menu">
</div>
</div>
</div>
<div class="site-wrap" id="site">
<div class="boxed-wrap site-translate">
<div class="pl-fixed-top is-not-fixed" data-region="fixed-top" id="fixed-top">
<div class="pl-fixed-region pl-region" data-region="fixed">
<div class="outline pl-area-container">
<!-- Canvas Area | Section Template -->
<section class="pl-area pl-area-sortable area-tag lp-head pl-bg-cover section-pl_area" data-clone="uye65q5" data-object="PLSectionArea" data-sid="pl_area" id="pl_areauye65q5" style="  "><div class="pl-area-pad fix"> <div class="pl-area-wrap " style="padding-top: 0px; padding-bottom: 0px;">
<div class="pl-content ">
<div class="pl-inner area-region pl-sortable-area editor-row" style="">
<div class="row grid-row">
<!-- TextBox | Section Template -->
<section class="pl-section span3 offset0 pl-bg-cover section-textbox" data-clone="uzzim3l" data-object="PageLinesTextBox" data-sid="textbox" id="textboxuzzim3l" style="  "><div class="pl-section-pad fix"><div class="textbox-wrap pl-animation textleft " style=""><div class="hentry" data-sync="textbox_content"><div class="pl-editor-only">Textbox Section</div>
</div></div></div></section>
<!-- Navi | Section Template -->
<section class="pl-section span9 offset0 pl-bg-cover section-navi" data-clone="udva16i" data-object="PLNavi" data-sid="navi" id="naviudva16i" style="  "><div class="pl-section-pad fix"> <div class="navi-wrap fix">
<div class="navi-left ">
</div>
<div class="navi-right">
<ul class="inline-list pl-nav sf-menu respond dd-theme-dark dd-toggle-hover" id="menu-topnav-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-223"><a href="https://selftalkplus.com/">HOME</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-232"><a href="https://selftalkplus.com/registration-form/">SIGN UP</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-234"><a href="https://selftalkplus.com/login/">MEMBER LOGIN</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-238"><a href="https://selftalkplus.com/listening-tips/">LISTENING TIPS</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355"><a href="https://selftalkplus.com/faq/">FAQ</a></li><li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="https://www.facebook.com/drshadhelmstetter/" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li><li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-757"><a href="https://instagram.com/drshadhelmstetter/" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li><li class="socmed menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="https://twitter.com/ShadHelmstetter" rel="noopener noreferrer" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li><li class="popup-nav"><a class="menu-toggle mm-toggle respond"><i class="icon icon-reorder"></i></a></li></ul>
</div>
<div class="navi-left navi-search">
</div>
</div>
</div></section></div> </div>
</div>
</div>
</div></section>
</div>
</div>
</div>
<div class="fixed-top-pusher"></div>
<script> jQuery('.fixed-top-pusher').height( jQuery('.pl-fixed-top').height() ) </script>
<div class="pl-region-wrap">
<div class="thepage page-wrap" id="page">
<div class="page-canvas">
<header class="header pl-region" data-region="header" id="header">
<div class="outline pl-area-container">
<!-- Canvas Area | Section Template -->
<section class="pl-area pl-area-sortable area-tag pl-bg-cover section-pl_area" data-clone="u71c39" data-object="PLSectionArea" data-sid="pl_area" id="pl_areau71c39" style="  "><div class="pl-area-pad fix"> <div class="pl-area-wrap " style="">
<div class="pl-content ">
<div class="pl-inner area-region pl-sortable-area editor-row" style="">
</div>
</div>
</div>
</div></section> </div>
</header>
<div class="pl-region" data-region="template" id="page-main">
<div class="outline template-region-wrap pl-area-container" id="dynamic-content">
<!-- Canvas Area | Section Template -->
<section class="pl-area pl-area-sortable area-tag pl-bg-cover section-pl_area" data-clone="ub3d9f" data-object="PLSectionArea" data-sid="pl_area" id="pl_areaub3d9f" style="  "><div class="pl-area-pad fix"> <div class="pl-area-wrap " style="padding-top: 20px; padding-bottom: 20px;">
<div class="pl-content nested-section-area">
<div class="pl-inner area-region pl-sortable-area editor-row" style="">
<div class="row grid-row">
<!-- 404 Error | Section Template -->
<section class="pl-section span10 offset1 pl-bg-cover section-noposts" data-clone="ub3da5" data-object="PageLinesNoPosts" data-sid="noposts" id="nopostsub3da5" style="  "><div class="pl-section-pad fix"> <div class="notfound boomboard">
<h2 class="hugetext center">404!</h2>
<p class="subhead center">Sorry, This Page Does not exist.<br/>Go <a href="https://selftalkplus.com">home</a> or try a search?</p>
<div class="center fix"><form action="https://selftalkplus.com/" class="searchform pl-searcher" method="get" onsubmit="this.submit();return false;"><fieldset><span class="btn-search"><i class="icon icon-search"></i></span><input class="searchfield" name="s" placeholder="Search" type="text" value=""/></fieldset></form> </div>
</div>
</div></section></div> </div>
</div>
</div>
</div></section> </div>
<div class="clear"></div>
</div>
</div>
</div>
<footer class="footer pl-region" data-region="footer" id="footer">
<div class="page-area outline pl-area-container fix">
<!-- Canvas Area | Section Template -->
<section class="pl-area pl-area-sortable area-tag lp-foot pl-black pl-bg-cover section-pl_area" data-clone="u7206d" data-object="PLSectionArea" data-sid="pl_area" id="pl_areau7206d" style="  "><div class="pl-area-pad fix"> <div class="pl-area-wrap " style="padding-top: 20px; padding-bottom: 20px;">
<div class="pl-content nested-section-area">
<div class="pl-inner area-region pl-sortable-area editor-row" style="">
<div class="row grid-row">
<!-- NavBar | Section Template -->
<section class="pl-section span5 offset0 pl-bg-cover section-navbar" data-clone="u7mqgpo" data-object="PLNavBar" data-sid="navbar" id="navbaru7mqgpo" style="  "><div class="pl-section-pad fix"> <div class="navbar fix navbar-content-width pl-color-black-trans">
<div class="navbar-inner ">
<div class="navbar-content-pad fix">
<a class="nav-btn nav-btn-navbar mm-toggle" href="javascript:void(0)"> MENU <i class="icon icon-reorder"></i> </a>
<div class="nav-collapse collapse">
<form action="https://selftalkplus.com/" class="searchform pl-searcher" method="get" onsubmit="this.submit();return false;"><fieldset><span class="btn-search"><i class="icon icon-search"></i></span><input class="searchfield" name="s" placeholder="Search" type="text" value=""/></fieldset></form><ul class="font-sub navline pldrop pull-left" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-409" id="menu-item-409"><a href="https://selftalkplus.com/customer-support/">Customer Support</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-407" id="menu-item-407"><a href="https://selftalkplus.com/terms-of-service/">Terms of Service</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-408" id="menu-item-408"><a href="https://selftalkplus.com/privacy-policy/">Privacy Policy</a></li>
</ul> </div>
<div class="clear"></div>
</div>
</div>
</div>
</div></section>
<!-- Socialinks | Section Template -->
<section class="pl-section span7 offset0 pl-bg-cover section-socialinks" data-clone="u72319" data-object="PLSocialinks" data-sid="socialinks" id="socialinksu72319" style="  "><div class="pl-section-pad fix"> <div class="socialinks-wrap fix sl-links-right">
<div class="sl-text"><span class="sl-copy"><script type="text/javascript">copyright=new Date(); update=copyright.getFullYear();  document.write("© "+ update + " The Self-Talk Institute. All rights reserved.");</script><a href="http://21thirteen.com" target="_blank"> Website by 21Thirteen Design, Inc.</a></span> </div> <div class="sl-links">
<a class="sl-link" href="https://www.facebook.com/drshadhelmstetter/" target="_blank"><i class="icon icon-facebook"></i></a><a class="sl-link" href="https://instagram.com/drshadhelmstetter/" target="_blank"><i class="icon icon-instagram"></i></a><a class="sl-link" href="https://twitter.com/ShadHelmstetter" target="_blank"><i class="icon icon-twitter"></i></a> </div>
</div>
</div></section></div> </div>
</div>
</div>
</div></section> </div>
</footer>
</div>
</div>
</div>
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/c8705d89-5df0-41b8-8d08-8bc9c4333cfb.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script><!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com :: Campaign Title: Self-Talk Plus --><div id="om-yqlhp4uqrj7lrtuz-holder"></div><script>var yqlhp4uqrj7lrtuz,yqlhp4uqrj7lrtuz_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){yqlhp4uqrj7lrtuz_poll(function(){if(window['om_loaded']){if(!yqlhp4uqrj7lrtuz){yqlhp4uqrj7lrtuz=new OptinMonsterApp();return yqlhp4uqrj7lrtuz.init({"u":"9645.226835","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="https://a.optmnstr.com/app/js/api.min.js",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;yqlhp4uqrj7lrtuz=new OptinMonsterApp();yqlhp4uqrj7lrtuz.init({"u":"9645.226835","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster --><!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com :: Campaign Title: selftalk --><div id="om-ftdy9ztkocdxetpy-holder"></div><script>var ftdy9ztkocdxetpy,ftdy9ztkocdxetpy_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){ftdy9ztkocdxetpy_poll(function(){if(window['om_loaded']){if(!ftdy9ztkocdxetpy){ftdy9ztkocdxetpy=new OptinMonsterApp();return ftdy9ztkocdxetpy.init({"u":"9645.460631","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="https://a.optmnstr.com/app/js/api.min.js",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;ftdy9ztkocdxetpy=new OptinMonsterApp();ftdy9ztkocdxetpy.init({"u":"9645.460631","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster --><script>window._popup_data = {"ajaxurl":"","do":"get_data","ajax_data":[],"popup":[{"html_id":"ac76eaf6819cfb42d41a35d6965a358ec","popup_id":534,"close_hide":true,"expiry":365,"custom_size":false,"width":"","height":"","overlay_close":true,"display":"click","display_data":{"delay":30,"delay_type":"s","scroll":0,"scroll_type":"%","anchor":"","click":".popup","click_multi":"on"},"scroll_body":false,"form_submit":"close","animation_in":"","animation_out":"","inline":false,"html":"<div id=\"ac76eaf6819cfb42d41a35d6965a358ec\" class=\"wpmui-popup wdpu-container wdpu-background no-img buttons style-minimal rounded wdpu-534 no-title no-subtitle\" style=\"\"> <div class=\"resize popup wdpu-msg move\" style=\"\"> <a href=\"#\" class=\"wdpu-close\" title=\"Close this box\"><\/a> <div class=\"wdpu-msg-inner resize\"> <div class=\"wdpu-head\"> <div class=\"wdpu-title\"><\/div> <div class=\"wdpu-subtitle\"><\/div> <\/div> <div class=\"wdpu-middle\"> <div class=\"wdpu-text\"> <div class=\"wdpu-inner \"> <div class=\"wdpu-content\"><h1>Self-Talk Plus+\u2122<\/h1>\n<p>Listen now!<br \/>\nListen instantly to all self-talk sessions,<br \/>\nand have access to all member benefits.<\/p>\n<p>Sign up now, free for 30 days.<\/p>\n<p><a href=\"\/registration-form\/\">sign up now<\/a><\/p>\n<\/div> <\/div> <\/div> <\/div> <div class=\"wdpu-buttons\"> <a href=\"#\" class=\"wdpu-hide-forever\">Never see this message again.<\/a> <a href=\"https:\/\/selftalkplus.com\/registration-form\/\" class=\"wdpu-cta\" target=\"_self\">Sign me up!<\/a> <\/div> <\/div> <\/div> <\/div>","styles":"\/**\r\n * Style: Minimal\r\n *\/\r\nhtml.no-scroll {\r\n  overflow: hidden;\r\n}\r\nhtml.no-scroll body {\r\n  overflow: hidden;\r\n}\r\n\r\n.wdpu-loading {\r\n  position: relative;\r\n}\r\n\r\n.wdpu-loading:after {\r\n  content: '';\r\n  position: absolute;\r\n  left: 0;\r\n  top: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background: rgba(255, 255, 255, 0.8) url(https:\/\/selftalkplus.com\/wp-content\/plugins\/popover\/css\/tpl\/minimal\/..\/..\/..\/inc\/external\/wpmu-lib\/img\/spinner.gif) center no-repeat;\r\n  z-index: 1000;\r\n  cursor: default;\r\n}\r\n\r\n.wdpu-534 {\r\n  position: fixed;\r\n  z-index: 100000;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  overflow: auto;\r\n  background: rgba(0, 0, 0, 0.5);\r\n  padding: 0;\r\n  \/* Default: Image on right side *\/\r\n}\r\n.wdpu-534 *, .wdpu-534 *:before, .wdpu-534 *:after {\r\n  box-sizing: content-box;\r\n}\r\n.wdpu-534.custom-size {\r\n  padding: 0;\r\n}\r\n.wdpu-534.inline {\r\n  position: relative;\r\n  overflow: visible;\r\n  background: transparent;\r\n  padding: 0;\r\n}\r\n.wdpu-534.inline .wdpu-msg {\r\n  max-width: 100%;\r\n  margin: 0;\r\n}\r\n.wdpu-534.custom-pos .wdpu-msg {\r\n  position: absolute;\r\n  display: inline-block;\r\n  margin: 0 0 30px;\r\n}\r\n.wdpu-534.no-title.no-subtitle .wdpu-head {\r\n  display: none;\r\n}\r\n.wdpu-534.no-title .wdpu-title {\r\n  display: none;\r\n}\r\n.wdpu-534.no-subtitle .wdpu-subtitle {\r\n  display: none;\r\n}\r\n.wdpu-534 .wdpu-msg {\r\n  position: relative;\r\n  display: block;\r\n  margin: 0;\r\n  text-align: left;\r\n  box-shadow: 0 2px 40px rgba(0, 0, 0, 0.3);\r\n  font-size: 15px;\r\n  \/* Size \/ responsiveness *\/\r\n  max-width: 800px;\r\n  min-width: 100px;\r\n}\r\n.wdpu-534 .wdpu-msg p {\r\n  font-size: 15px;\r\n}\r\n.wdpu-534 .wdpu-msg p:first-child {\r\n  margin-top: 0;\r\n}\r\n.wdpu-534 .wdpu-msg p:last-child {\r\n  margin-bottom: 0;\r\n}\r\n.wdpu-534 .wdpu-close {\r\n  position: absolute;\r\n  top: 0;\r\n  right: 0;\r\n  width: 50px;\r\n  height: 49px;\r\n  line-height: 50px;\r\n  text-align: center;\r\n  color: #AAA;\r\n  z-index: 10;\r\n  transition: color .3s, background .3s;\r\n  text-decoration: none;\r\n  background: rgba(255, 255, 255, 0.1);\r\n}\r\n.wdpu-534 .wdpu-close.no-title {\r\n  width: 30px;\r\n  height: 30px;\r\n  line-height: 30px;\r\n}\r\n.wdpu-534 .wdpu-close:before {\r\n  content: '\\D7';\r\n  margin: 0 -2px 0 0;\r\n  position: relative;\r\n  font-weight: bold;\r\n  font-size: 24px;\r\n  font-family: helvetica, sans-serif !important;\r\n}\r\n.wdpu-534 .wdpu-close:hover {\r\n  color: #555;\r\n  background: rgba(255, 255, 255, 0.6);\r\n}\r\n.wdpu-534 .wdpu-msg-inner {\r\n  margin: 0px auto;\r\n  background: #FFF;\r\n  position: relative;\r\n  z-index: 1;\r\n  overflow: hidden;\r\n  max-height: 100%;\r\n}\r\n.wdpu-534 .wdpu-msg-inner.custom-size {\r\n  max-width: none;\r\n  min-width: 0;\r\n}\r\n.wdpu-534 .wdpu-text,\r\n.wdpu-534 .wdpu-image {\r\n  vertical-align: middle;\r\n  display: inline-block;\r\n  box-sizing: border-box;\r\n}\r\n.wdpu-534 .wdpu-image {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  overflow: hidden;\r\n}\r\n.wdpu-534 .wdpu-image img {\r\n  height: 100%;\r\n  width: auto;\r\n  position: relative;\r\n  max-width: none;\r\n}\r\n.wdpu-534 .wdpu-middle {\r\n  position: relative;\r\n  min-height: 300px;\r\n}\r\n.wdpu-534 .wdpu-inner {\r\n  overflow: auto;\r\n}\r\n.wdpu-534 .wdpu-inner.no-bm {\r\n  margin: 0;\r\n}\r\n.wdpu-534 .wdpu-content {\r\n  padding: 20px;\r\n}\r\n.wdpu-534 .wdpu-buttons {\r\n  position: absolute;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  padding: 20px;\r\n  height: 35px;\r\n  line-height: 35px;\r\n  background: #E8E8E8;\r\n  border-top: 1px solid #ccc;\r\n  box-shadow: 0 1px 0 0px #FFF inset;\r\n  text-align: right;\r\n}\r\n.wdpu-534 a {\r\n  color: #488CFD;\r\n  text-decoration: underline;\r\n  opacity: .9;\r\n}\r\n.wdpu-534 a:visited {\r\n  color: #488CFD;\r\n}\r\n.wdpu-534 a:hover, .wdpu-534 a:active, .wdpu-534 a:focus {\r\n  color: #488CFD;\r\n  opacity: 1;\r\n}\r\n.wdpu-534 .wdpu-head {\r\n  border-bottom: 1px solid #DDD;\r\n  padding: 14px 50px 10px 20px;\r\n  min-height: 25px;\r\n  margin: 0;\r\n}\r\n.wdpu-534 .wdpu-title,\r\n.wdpu-534 .wdpu-subtitle {\r\n  font-size: 20.25px;\r\n  line-height: 24px;\r\n  padding: 0;\r\n  color: #488CFD;\r\n  display: inline-block;\r\n}\r\n.wdpu-534 .wdpu-title {\r\n  font-weight: bold;\r\n}\r\n.wdpu-534 .wdpu-subtitle {\r\n  font-weight: 100;\r\n}\r\n.wdpu-534 .wdpu-cta {\r\n  line-height: 1.2em;\r\n  padding: 8px 24px;\r\n  opacity: .9;\r\n  margin: 0 0 0 20px;\r\n  text-decoration: none;\r\n  font-weight: 100;\r\n  background: #488CFD;\r\n  color: #FFFFFF;\r\n}\r\n.wdpu-534 .wdpu-cta:visited {\r\n  color: #FFFFFF;\r\n}\r\n.wdpu-534 .wdpu-cta:hover, .wdpu-534 .wdpu-cta:active, .wdpu-534 .wdpu-cta:focus {\r\n  color: #FFFFFF;\r\n  opacity: 1;\r\n}\r\n.wdpu-534 .wdpu-text {\r\n  margin: 0 40% 0 0;\r\n}\r\n.wdpu-534 .wdpu-image {\r\n  text-align: right;\r\n  right: 0;\r\n  width: 40%;\r\n}\r\n.wdpu-534.img-left .wdpu-text {\r\n  margin: 0 0 0 40%;\r\n  margin-right: 0;\r\n}\r\n.wdpu-534.img-left .wdpu-image {\r\n  text-align: left;\r\n  right: auto;\r\n  left: 0;\r\n}\r\n.wdpu-534.no-img .wdpu-text {\r\n  margin: 0;\r\n}\r\n.wdpu-534.no-img .wdpu-image {\r\n  display: none;\r\n}\r\n.wdpu-534.rounded .wdpu-cta {\r\n  border-radius: 5px;\r\n}\r\n.wdpu-534.rounded .wdpu-msg {\r\n  border-radius: 11px;\r\n}\r\n.wdpu-534.rounded .wdpu-msg-inner {\r\n  border-radius: 10px;\r\n}\r\n.wdpu-534.rounded .wdpu-close {\r\n  border-radius: 0 10px 0 0;\r\n}\r\n.wdpu-534.buttons .wdpu-inner {\r\n  margin-bottom: 75px;\r\n}\r\n.wdpu-534.buttons .wdpu-image {\r\n  margin-bottom: 75px;\r\n}\r\n\r\n@media screen and (max-width: 770px) and (min-width: 480px) {\r\n  .wdpu-534 {\r\n    padding: 0;\r\n  }\r\n  .wdpu-534 .wdpu-image, .wdpu-534.img-right .wdpu-image, .wdpu-534.img-left .wdpu-image, .wdpu-534.no-img .wdpu-image {\r\n    width: 100%;\r\n    position: relative;\r\n    display: block;\r\n    max-height: 250px;\r\n    margin: 0;\r\n  }\r\n  .wdpu-534 .wdpu-image img, .wdpu-534.img-right .wdpu-image img, .wdpu-534.img-left .wdpu-image img, .wdpu-534.no-img .wdpu-image img {\r\n    height: auto;\r\n    width: 100%;\r\n  }\r\n  .wdpu-534 .wdpu-text, .wdpu-534.img-right .wdpu-text, .wdpu-534.img-left .wdpu-text, .wdpu-534.no-img .wdpu-text {\r\n    margin: 0;\r\n  }\r\n  .wdpu-534 .wdpu-buttons, .wdpu-534.img-right .wdpu-buttons, .wdpu-534.img-left .wdpu-buttons, .wdpu-534.no-img .wdpu-buttons {\r\n    padding-top: 15px;\r\n    padding-bottom: 15px;\r\n  }\r\n  .wdpu-534 .wdpu-content, .wdpu-534.img-right .wdpu-content, .wdpu-534.img-left .wdpu-content, .wdpu-534.no-img .wdpu-content {\r\n    padding: 15px 20px;\r\n  }\r\n  .wdpu-534 .wdpu-head, .wdpu-534.img-right .wdpu-head, .wdpu-534.img-left .wdpu-head, .wdpu-534.no-img .wdpu-head {\r\n    padding-top: 8px;\r\n    padding-bottom: 6px;\r\n  }\r\n  .wdpu-534 .wdpu-close, .wdpu-534.img-right .wdpu-close, .wdpu-534.img-left .wdpu-close, .wdpu-534.no-img .wdpu-close {\r\n    height: 40px;\r\n    line-height: 40px;\r\n  }\r\n  .wdpu-534 p, .wdpu-534.img-right p, .wdpu-534.img-left p, .wdpu-534.no-img p {\r\n    margin-bottom: 16px;\r\n  }\r\n  .wdpu-534.img-right .wdpu-inner {\r\n    margin: 0;\r\n  }\r\n  .wdpu-534.img-right .wdpu-image {\r\n    margin-bottom: 75px;\r\n  }\r\n  .wdpu-534.mobile-no-img .wdpu-image {\r\n    display: none;\r\n  }\r\n}\r\n@media screen and (max-width: 480px) {\r\n  .wdpu-534 {\r\n    padding: 0;\r\n  }\r\n  .wdpu-534 .wdpu-image, .wdpu-534.img-right .wdpu-image, .wdpu-534.img-left .wdpu-image, .wdpu-534.no-img .wdpu-image {\r\n    width: 100%;\r\n    position: relative;\r\n    display: block;\r\n    max-height: 150px;\r\n    margin: 0;\r\n  }\r\n  .wdpu-534 .wdpu-image img, .wdpu-534.img-right .wdpu-image img, .wdpu-534.img-left .wdpu-image img, .wdpu-534.no-img .wdpu-image img {\r\n    height: auto;\r\n    width: 100%;\r\n  }\r\n  .wdpu-534 .wdpu-text, .wdpu-534.img-right .wdpu-text, .wdpu-534.img-left .wdpu-text, .wdpu-534.no-img .wdpu-text {\r\n    margin: 0;\r\n  }\r\n  .wdpu-534 .wdpu-buttons, .wdpu-534.img-right .wdpu-buttons, .wdpu-534.img-left .wdpu-buttons, .wdpu-534.no-img .wdpu-buttons {\r\n    padding-top: 10px;\r\n    padding-bottom: 10px;\r\n  }\r\n  .wdpu-534 .wdpu-content, .wdpu-534.img-right .wdpu-content, .wdpu-534.img-left .wdpu-content, .wdpu-534.no-img .wdpu-content {\r\n    padding: 10px 20px;\r\n  }\r\n  .wdpu-534 .wdpu-head, .wdpu-534.img-right .wdpu-head, .wdpu-534.img-left .wdpu-head, .wdpu-534.no-img .wdpu-head {\r\n    padding-top: 8px;\r\n    padding-bottom: 6px;\r\n  }\r\n  .wdpu-534 .wdpu-close, .wdpu-534.img-right .wdpu-close, .wdpu-534.img-left .wdpu-close, .wdpu-534.no-img .wdpu-close {\r\n    height: 40px;\r\n    line-height: 40px;\r\n  }\r\n  .wdpu-534 p, .wdpu-534.img-right p, .wdpu-534.img-left p, .wdpu-534.no-img p {\r\n    margin-bottom: 8px;\r\n  }\r\n  .wdpu-534.img-right .wdpu-inner {\r\n    margin: 0;\r\n  }\r\n  .wdpu-534.img-right .wdpu-image {\r\n    margin-bottom: 75px;\r\n  }\r\n  .wdpu-534.mobile-no-img .wdpu-image {\r\n    display: none;\r\n  }\r\n}\r\n\r\n\r\n\/* 390339-1522950918-au *\/","script":"me.custom_handler = \t\tfunction( me ) {\r\n\t\t\tif ( me.data.display_data['click_multi'] ) {\r\n\t\t\t\tjQuery(document).on( 'click', me.data.display_data['click'], me.show_popup );\r\n\t\t\t} else {\r\n\t\t\t\tjQuery(document).one( 'click', me.data.display_data['click'], me.show_popup );\t \t\t\t\t\t \t \t  \t\t\t\t\t \r\n\t\t\t}\r\n\t\t}\r\n\t\t"}]};</script> <script type="text/javascript">
		var yqlhp4uqrj7lrtuz_shortcode = true;var ftdy9ztkocdxetpy_shortcode = true;		</script>
<!-- Footer Scripts -->
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/js/script.bootstrap.min.js?ver=2.2.2" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/js/pl.helpers.js?ver=0f1f20" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/js/script.fitvids.js?ver=0f1f20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var plKarma = {"ajaxurl":"https:\/\/selftalkplus.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/js/pl.common.js?ver=0f1f20" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/js/script.flexslider.js?ver=0f1f20" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/themes/dms/dms/sections/navbar/navbar.js?ver=0f1f20" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/plugins/dms-plugin-pro/libs/js/jquery.sonar.min.js?ver=0.2" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/plugins/dms-plugin-pro/libs/js/lazy-load.js?ver=0.2" type="text/javascript"></script>
<script>(function(d){var s=d.createElement("script");s.type="text/javascript";s.src="https://a.omappapi.com/app/js/api.min.js";s.async=true;s.id="omapi-script";d.getElementsByTagName("head")[0].appendChild(s);})(document);</script><script src="https://selftalkplus.com/wp-content/plugins/popover/inc/external/wpmu-lib/js/wpmu-ui.3.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/plugins/popover/js/public.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://selftalkplus.com/wp-content/plugins/optinmonster/assets/js/helper.js?ver=1.9.11" type="text/javascript"></script>
<script type="text/javascript">var omapi_localized = { ajax: 'https://selftalkplus.com/wp-admin/admin-ajax.php?optin-monster-ajax-route=1', nonce: '271a1a4c07', slugs: {"yqlhp4uqrj7lrtuz":{"slug":"yqlhp4uqrj7lrtuz","mailpoet":false},"ftdy9ztkocdxetpy":{"slug":"ftdy9ztkocdxetpy","mailpoet":false}} };</script>
<script type="text/javascript">var omapi_data = {"wc_cart":[],"object_id":"0","object_key":"","object_type":"","term_ids":[]};</script>
</body>
</html>