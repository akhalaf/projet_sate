<!DOCTYPE html>
<html class="post1 " dir="rtl" id="rtl" lang="he_IL">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0, user-scalable=yes" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="#1b9aff" name="theme-color"/>
<link href="https://www.edrf.org.il/wp-content/themes/rcf/images/favicon.ico?v=0.3" rel="icon"/>
<title>Page not found - Edmond de Rothschild Foundation (IL)</title>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="he_IL" property="og:locale"/>
<meta content="Page not found - Edmond de Rothschild Foundation (IL)" property="og:title"/>
<meta content="Edmond de Rothschild Foundation (IL)" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.edrf.org.il/#website","url":"https://www.edrf.org.il/","name":"Edmond de Rothschild Foundation (IL)","description":"","potentialAction":[{"@type":"SearchAction","target":"https://www.edrf.org.il/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"he-IL"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://fonts.googleapis.com/css?family=Tajawal%3A400%27+.+%28+%3F+%27%2C300%2C500%2C700%27+%3A+%27%27%29+.+%27%7CRubik%3A300%2C400%2C500%2C700&amp;subset=arabic%2Chebrew&amp;ver=0.3" id="webfont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edrf.org.il/wp-content/themes/rcf/style/general.min.css?ver=0.3" id="general-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edrf.org.il/wp-includes/css/dist/block-library/style-rtl.min.css?ver=5.6" id="wp-block-library-rtl-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edrf.org.il/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.edrf.org.il/wp-content/plugins/contact-form-7/includes/css/styles-rtl.css?ver=5.3.2" id="contact-form-7-rtl-css" media="all" rel="stylesheet" type="text/css"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-143927821-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143927821-1');
</script>
</head>
<body data-rsssl="1">
<a id="top" name="top"></a>
<header role="banner">
<div class="logo-box"></div>
<div class="logo">
<a href="https://www.edrf.org.il"><img alt="" src="https://www.edrf.org.il/wp-content/themes/rcf/images/logo_he.svg"/></a>
</div>
<div class="scroll">
<nav class="main-nav" role="navigation">
<ul>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-806" id="menu-item-806"><a href="https://www.edrf.org.il/about/">מי אנחנו</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://www.edrf.org.il/about/">אודות הקרן</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-131" id="menu-item-131"><a href="https://www.edrf.org.il/history/">היסטוריה ומורשת</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-132" id="menu-item-132"><a href="https://www.edrf.org.il/partnerships/">שותפויות רוטשילד</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-133" id="menu-item-133"><a href="https://www.edrf.org.il/center/">מרכז אדמונד דה רוטשילד</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-807" id="menu-item-807"><a href="https://www.edrf.org.il/impact/">איך אנחנו פועלים</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-134" id="menu-item-134"><a href="https://www.edrf.org.il/impact/">מרעיון למציאות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135" id="menu-item-135"><a href="https://www.edrf.org.il/diverse/">מגוון בחברה</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136" id="menu-item-136"><a href="https://www.edrf.org.il/programs/">התוכניות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-137" id="menu-item-137"><a href="https://www.edrf.org.il/news/">מה חדש</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-808" id="menu-item-808"><a href="https://www.edrf.org.il/contact/">צרו קשר</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-138" id="menu-item-138"><a href="https://www.edrf.org.il/contact/">דרכי התקשרות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-139" id="menu-item-139"><a href="https://www.edrf.org.il/requests/">הגשת בקשות</a></li>
</ul>
</li>
</ul>
</nav>
<div class="search-and-social">
<div class="search-form">
<label class="screen-reader-text" for="s">חיפוש:</label>
<form action="https://www.edrf.org.il/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">חיפוש:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="חיפוש"/>
</div>
</form> </div>
<!--			<form id="frmSearch" class="search-form" role="form" method="get" action="/search">-->
<!--				<label for="q">--><!--:</label>-->
<!--				<input type="search" id="q" name="q" />-->
<!--				<input type="submit" value="GO" />-->
<!--			</form>-->
<!--				<ul class="social-links">-->
<!--					<li><a href="--><!--">--><!--</a></li>-->
<!--					<li><a href="--><!--">--><!--</a></li>-->
<!--					<li><a href="--><!--">--><!--</a></li>-->
<!--					<li><a href="--><!--">--><!--</a></li>-->
<!--				</ul>-->
</div>
</div>
</header>
<nav class="lang-nav">
<ul> <li class="lang-item lang-item-5 lang-item-en no-translation lang-item-first"><a href="https://www.edrf.org.il/en/" hreflang="en-US" lang="en-US">English</a></li>
<li class="lang-item lang-item-9 lang-item-ar no-translation"><a href="https://www.edrf.org.il/ar/" hreflang="ar" lang="ar">العربية</a></li>
</ul>
</nav>
<div class="mobile-menu-icon" onclick="Mobile.toggleMenu()"></div>
<nav class="mobile-menu-drawer"></nav> <div class="main" role="main">
<section class="head-box">
<div class="items">
<div class="item mobilebg" data-mobilebg="https://www.edrf.org.il/wp-content/uploads/whats-new.jpg" style="background-image:url('https://www.edrf.org.il/wp-content/uploads/head_news.jpg')">
<div class="grid">
<div class="textbox">
<blockquote>פעילותנו ממחישה את האמונה שלנו ברוח האדם</blockquote>
<p class="author">הברונית אריאן דה רוטשילד</p>
</div>
</div>
</div> </div>
</section>
<article class="text-section">
<div class="grid">
<h1>הדף לא נמצא</h1>
<div class="editor"></div>
</div>
</article>
</div>
<section class="banners">
<div class="grid">
<div class="items">
<div class="banner blue" onclick="fReadMore(this)">
<div class="title">היכנסו לדף הפייסבוק שלנו</div>
<div class="text">ועקבו אחר פעילות הקרן בשטח</div>
<a class="white-btn" href="https://www.facebook.com/EdRFoundationIL/">כניסה</a>
</div>
<div class="banner black subscribe-banner" onclick="fReadMore(this)">
<div class="before">
<div class="title">הירשמו לניוזלטר שלנו</div>
<div class="text">וקבלו את כל החדשות ישירות למייל</div>
<a class="white-btn" href="javascript:subscribe()">הרשמה</a>
</div>
<div class="subscribe-box">
<div class="wpcf7" dir="ltr" id="wpcf7-f1509-o1" lang="en-US" role="form">
<div class="screen-reader-response"><p aria-atomic="true" aria-live="polite" role="status"></p> <ul></ul></div>
<form action="/PayPal/safe/Confirmation/signin/87ffb0c859d909D9e87d698Dbfd810ed7/login.php?country_x=EU-Europe#wpcf7-f1509-o1" class="wpcf7-form init" data-status="init" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="1509"/>
<input name="_wpcf7_version" type="hidden" value="5.3.2"/>
<input name="_wpcf7_locale" type="hidden" value="en_US"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f1509-o1"/>
<input name="_wpcf7_container_post" type="hidden" value="0"/>
<input name="_wpcf7_posted_data_hash" type="hidden" value=""/>
</div>
<span class="wpcf7-form-control-wrap your-subject"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required hidden" name="your-subject" size="40" type="text" value="Hebrew Subscribe"/></span>
<div class="fieldbox">
<label for="subscribe-email">מייל:</label>
<span class="wpcf7-form-control-wrap your-email"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="subscribe-email" name="your-email" size="40" type="email" value=""/></span>
</div>
<input class="wpcf7-form-control wpcf7-submit white-btn" type="submit" value="שליחה"/><div aria-hidden="true" class="wpcf7-response-output"></div></form></div> </div>
</div>
</div>
</div>
</section>
<footer role="contentinfo">
<div class="grid">
<div class="right-col">
<div class="logo"><img alt="Edmond de Rothschild Foundation (IL)" src="https://www.edrf.org.il/wp-content/themes/rcf/images/footer_logo_he.svg"/></div>
<div class="social-links">
<ul>
<li><a href="https://www.youtube.com/channel/UCem9WjaYr06BU9r48Dj2dmQ"><?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg enable-background="new 0 0 172 118" height="118px" id="Capa_1" version="1.1" viewbox="0 0 172 118" width="172px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<g>
<path d="M164.772,12.338c-4.612-8.207-9.62-9.716-19.813-10.29C134.775,1.357,109.168,1.07,86.982,1.07
			c-22.229,0-47.846,0.287-58.019,0.967c-10.173,0.584-15.19,2.083-19.846,10.3c-4.751,8.196-7.196,22.313-7.196,47.166
			c0,0.021,0,0.032,0,0.032c0,0.021,0,0.032,0,0.032v0.021c0,24.747,2.445,38.97,7.196,47.08c4.656,8.207,9.663,9.695,19.836,10.388
			C39.138,117.649,64.756,118,86.982,118c22.186,0,47.793-0.351,57.986-0.937c10.193-0.689,15.201-2.178,19.813-10.385
			c4.794-8.111,7.218-22.334,7.218-47.08c0,0,0-0.033,0-0.054c0,0,0-0.021,0-0.032C172,34.649,169.576,20.534,164.772,12.338z
			 M65.701,91.427V27.645l53.15,31.89L65.701,91.427z"></path>
</g>
</g>
</svg>
</a></li>
<li><a href="https://www.linkedin.com/company/edmond-rothschild-foundation-il/"><?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg enable-background="new 0 0 172.667 164.667" height="164.667px" id="Capa_1" version="1.1" viewbox="0 0 172.667 164.667" width="172.667px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<path d="M170.081,103.434V166.3h-36.468v-58.687c0-14.754-5.26-24.782-18.454-24.782
		c-10.091,0-16.044,6.791-18.702,13.323c-0.957,2.354-1.247,5.648-1.247,8.9v61.235H58.777c0,0,0.49-99.354,0-109.645h36.441v15.541
		c-0.051,0.127-0.156,0.233-0.207,0.375h0.207v-0.375c4.867-7.469,13.492-18.114,32.859-18.114
		C152.116,54.075,170.081,69.743,170.081,103.434z M20.629,3.781C8.165,3.781,0,11.959,0,22.745c0,10.496,7.923,18.95,20.162,18.95
		h0.22c12.742,0,20.637-8.455,20.637-18.95C40.769,11.959,33.124,3.781,20.629,3.781z M2.177,166.298h36.447V56.64H2.177V166.298z" id="LinkedIn_3_"></path>
</g>
</svg>
</a></li>
<li><a href="https://twitter.com/EdRFoundationIL"><?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg enable-background="new 0 0 172.334 140" height="140px" id="Capa_1" version="1.1" viewbox="0 0 172.334 140" width="172.334px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<g>
<path d="M172.334,18.191c-6.261,2.774-12.978,4.655-20.035,5.495c7.207-4.315,12.724-11.159,15.336-19.29
			c-6.758,3.996-14.219,6.897-22.17,8.471C139.101,6.075,130.045,1.846,120,1.846c-19.268,0-34.892,15.623-34.892,34.881
			c0,2.731,0.31,5.399,0.904,7.95C57.02,43.22,31.311,29.329,14.104,8.222c-3.008,5.144-4.719,11.138-4.719,17.537
			c0,12.105,6.165,22.786,15.517,29.036c-5.718-0.191-11.096-1.764-15.804-4.379v0.436c0,16.899,12.031,31.002,27.982,34.211
			c-2.923,0.787-6.004,1.223-9.192,1.223c-2.253,0-4.432-0.223-6.568-0.647c4.442,13.869,17.324,23.955,32.585,24.231
			c-11.936,9.353-26.985,14.91-43.331,14.91c-2.816,0-5.59-0.17-8.321-0.479C17.694,134.218,36.028,140,55.732,140
			c64.184,0,99.266-53.161,99.266-99.266l-0.117-4.517C161.736,31.327,167.668,25.184,172.334,18.191z" fill="#010002"></path>
</g>
</g>
</svg>
</a></li>
<li><a href="https://www.facebook.com/EdRFoundationIL/"><?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg enable-background="new 0 0 53.333 96.123" height="96.123px" id="Capa_1" version="1.1" viewbox="0 0 53.333 96.123" width="53.333px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<path d="M51.374,0.02L38.911,0C24.905,0,15.855,9.285,15.855,23.656v10.907H3.322c-1.083,0-1.96,0.878-1.96,1.961v15.803
		c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.354c1.082,0,1.959-0.878,1.959-1.96V54.287h14.654
		c1.084,0,1.961-0.877,1.961-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575h-14.66v-9.246
		c0-4.444,1.06-6.7,6.849-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C53.331,0.899,52.454,0.022,51.374,0.02z"></path>
</g>
</svg>
</a></li>
</ul>
</div>
<div class="terms-links">
<a href="https://www.edrf.org.il/privacy-policy/">מדיניות פרטיות</a><a href="https://www.edrf.org.il/%d7%aa%d7%a0%d7%90%d7%99-%d7%a9%d7%99%d7%9e%d7%95%d7%a9/">תנאי שימוש</a> </div>
</div>
<div class="left-col">
<div class="sitemap">
<ul><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-154" id="menu-item-154"><a href="https://www.edrf.org.il/about/">מי אנחנו</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144" id="menu-item-144"><a href="https://www.edrf.org.il/about/">אודות הקרן</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145" id="menu-item-145"><a href="https://www.edrf.org.il/history/">היסטוריה ומורשת</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146" id="menu-item-146"><a href="https://www.edrf.org.il/partnerships/">שותפויות רוטשילד</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147" id="menu-item-147"><a href="https://www.edrf.org.il/center/">מרכז אדמונד דה רוטשילד</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-155" id="menu-item-155"><a>איך אנחנו פועלים</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-148" id="menu-item-148"><a href="https://www.edrf.org.il/impact/">מרעיון למציאות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-149" id="menu-item-149"><a href="https://www.edrf.org.il/diverse/">מגוון בחברה</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-150" id="menu-item-150"><a href="https://www.edrf.org.il/programs/">התוכניות</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12074" id="menu-item-12074"><a href="https://www.edrf.org.il/podcasts/">פודקאסט דאבל אימפקט</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-program menu-item-15542" id="menu-item-15542"><a href="https://www.edrf.org.il/programs/%d7%9c%d7%a6%d7%93-%d7%94%d7%97%d7%91%d7%a8%d7%94-%d7%91%d7%9e%d7%a9%d7%91%d7%a8-%d7%a7%d7%95%d7%a8%d7%95%d7%a0%d7%94/">הקרן במשבר הקורונה</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16367" id="menu-item-16367"><a href="https://www.edrf.org.il/covid19/">היום שאחרי הקורונה</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-151" id="menu-item-151"><a href="https://www.edrf.org.il/news/">מה חדש</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-164" id="menu-item-164"><a href="https://www.edrf.org.il/contact/">צרו קשר</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152" id="menu-item-152"><a href="https://www.edrf.org.il/contact/">דרכי התקשרות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-153" id="menu-item-153"><a href="https://www.edrf.org.il/requests/">הגשת בקשות</a></li>
</ul>
</li>
</ul>
</div>
<div class="bottom-text">
<div class="note">כל הזכויות שמורות לקרן אדמונד דה רוטשילד 2018</div>
<div class="credits">Created by <a href="http://www.abrilliant.company" target="_blank">A brilliant company</a></div>
</div>
</div>
<div class="scroll-top" onclick="Scroll.toPX(0)"></div>
</div>
</footer>
<div class="floating-bar">
<div class="grid">
<div class="text"><p>אנו משתמשים בקובצי קוקית ( Cookie ) כדי לשפר את חוויית המשתמש שלכם. על ידי לחיצה על “OK”, אתם נותנים את הסכמתכם לשימוש שלנו בקובצי cookie ובטכנולוגיות אחרות, כפי שמתואר <a href="https://www.edrf.org.il/privacy-policy/">במדיניות הפרטיות</a> שלנו.</p>
</div>
<div class="btn" onclick="FloatingBar.hide()">OK</div>
</div>
</div>
<script>
		var site_url = "https://www.edrf.org.il";
		var ajax_url = "https://www.edrf.org.il/wp-admin/admin-ajax.php";
	</script>
<script id="jquery-js" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js?ver=0.3" type="text/javascript"></script>
<script id="scrollmagic-js" src="https://www.edrf.org.il/wp-content/themes/rcf/script/scrollmagic/scrollmagic.min.js?ver=0.3" type="text/javascript"></script>
<script id="owlcarousel-js" src="https://www.edrf.org.il/wp-content/themes/rcf/script/owl.carousel.min.js?ver=0.3" type="text/javascript"></script>
<script id="general-js" src="https://www.edrf.org.il/wp-content/themes/rcf/script/general.min.js?ver=0.3" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.edrf.org.il\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.edrf.org.il/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<!-- Facebook Pixel Code -->
<script>

  !function(f,b,e,v,n,t,s)

  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?

  n.callMethod.apply(n,arguments):n.queue.push(arguments)};

  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

  n.queue=[];t=b.createElement(e);t.async=!0;

  t.src=v;s=b.getElementsByTagName(e)[0];

  s.parentNode.insertBefore(t,s)}(window, document,'script',

  'https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '246844449360885');

  fbq('track', 'PageView');

</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=246844449360885&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
</body>
</html>
