<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>Biocare Coco Substrates</title>
<meta content="Biocare Coco Substrates" name="keywords"/>
<meta content="Biocare Coco Substrates" name="description"/>
<!-- Favicon -->
<link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="img/apple-touch-icon.png" rel="apple-touch-icon"/>
<!-- Mobile Metas -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css"/>
<!-- Vendor CSS -->
<link href="vendor/bootstrap/bootstrap.css" rel="stylesheet"/>
<link href="vendor/fontawesome/css/font-awesome.css" rel="stylesheet"/>
<link href="vendor/owlcarousel/owl.carousel.min.css" media="screen" rel="stylesheet"/>
<link href="vendor/owlcarousel/owl.theme.default.min.css" media="screen" rel="stylesheet"/>
<link href="vendor/magnific-popup/magnific-popup.css" media="screen" rel="stylesheet"/>
<!-- Theme CSS -->
<link href="css/theme.css" rel="stylesheet"/>
<link href="css/theme-elements.css" rel="stylesheet"/>
<link href="css/theme-blog.css" rel="stylesheet"/>
<link href="css/theme-shop.css" rel="stylesheet"/>
<link href="css/theme-animate.css" rel="stylesheet"/>
<!-- Current Page CSS -->
<link href="vendor/rs-plugin/css/settings.css" media="screen" rel="stylesheet"/>
<link href="vendor/circle-flip-slideshow/css/component.css" media="screen" rel="stylesheet"/>
<!-- Skin CSS -->
<link href="css/skins/default.css" rel="stylesheet"/>
<!-- Theme Custom CSS -->
<link href="css/custom.css" rel="stylesheet"/>
<!-- Head Libs -->
<script src="vendor/modernizr/modernizr.js"></script>
<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->
<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->
</head>
<body>
<div class="body">
<header id="header">
<div class="container">
<div class="logo">
<a href="index.html">
<img alt="Biocare Coco Substrates" data-sticky-height="90" data-sticky-width="100" height="96" src="img/logo.png" width="100"/> </a> </div>
<nav class="nav-top">
<ul class="nav nav-pills nav-top">
<li>
<span><i class="fa fa-envelope"></i>info@biocarecocos.com</span> </li>
<li class="phone">
<span><i class="fa fa-phone"></i>0091 â 9366644882 / 9487037402</span> </li>
</ul>
</nav>
<button class="btn btn-responsive-nav btn-inverse" data-target=".nav-main-collapse" data-toggle="collapse">
<i class="fa fa-bars"></i> </button>
</div>
<div class="navbar-collapse nav-main-collapse collapse">
<div class="container">
<ul class="social-icons">
<li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
<li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin">Linkedin</a></li>
</ul>
<nav class="nav-main mega-menu">
<ul class="nav nav-pills nav-main" id="mainMenu">
<li class="active"><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li><a href="coco-peat.html">Coco Peat</a></li>
<li><a href="products.html">Our Products</a></li>
<li><a href="quality-standard.html">Quality Standard</a></li>
<li><a href="contact-us.html">Contact Us</a></li>
</ul>
</nav>
</div>
</div>
</header>
<div class="main" role="main">
<div class="slider-container">
<div class="slider" data-plugin-revolution-slider="" id="revolutionSlider">
<ul>
<li data-masterspeed="300" data-slotamount="13" data-transition="fade">
<img alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" src="img/custom-header-bg.jpg"/>
<div class="tp-caption top-label lfl stl" data-easing="easeOutExpo" data-speed="300" data-start="500" data-x="180" data-y="180">Manufacturers of Export Quality Coir Products</div>
<div class="tp-caption main-label sft stb" data-easing="easeOutExpo" data-speed="300" data-start="1500" data-x="135" data-y="210">Biocare Coco Substrates</div>
<div class="tp-caption customin customout" data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" data-endspeed="500" data-hoffset="0" data-speed="800" data-start="700" data-voffset="0" data-x="right" data-y="bottom" style="z-index: 3"><img alt="" src="img/slides/zion-accounting.png"/>
</div>
</li>
<li data-masterspeed="300" data-slotamount="13" data-transition="fade">
<img alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" src="img/custom-header-bg.jpg"/>
<div class="tp-caption top-label lfl stl" data-easing="easeOutExpo" data-speed="300" data-start="500" data-x="190" data-y="180">Manufacturers of Export Quality Coir Products</div>
<div class="tp-caption main-label sft stb" data-easing="easeOutExpo" data-speed="300" data-start="1500" data-x="135" data-y="210">Biocare Coco Substrates</div>
<div class="tp-caption customin customout" data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" data-endspeed="500" data-hoffset="0" data-speed="800" data-start="700" data-voffset="0" data-x="right" data-y="bottom" style="z-index: 3"><img alt="" src="img/slides/zion-accounting-slide.png"/>
</div>
</li>
</ul>
</div>
</div>
<div class="home-intro">
<div class="container">
<div class="row">
<div class="col-md-10">
<p>
									We are one of the leading producer and exporter of Coco peat substrates or growing medium in South India for about 10 years.We are producing the following. 
								</p>
</div>
<div class="col-md-2">
<div class="get-started">
<a class="btn btn-lg btn-primary" href="#">Contact Us</a>
</div>
</div>
</div>
</div>
</div>
<div class="container">
<div class="col-md-12">
<h2 class="mobilealign">Welcome to <strong>Biocare Coco Substrates</strong></h2>
<p>Our production unit is located at Singampunari Industrial Estate, which is 6 kms. from the NH 45 which connects Chennai and Kanyakumari. Around 15 kms. radius with Singampunari as center, it is notable coconut growing area. The mesocarp of coconut is being processed into coir fibre and the coir pith is available in this area throughout the year. With this advantage, we have been consistently in coco peat export market for the last 10 years.</p>
<p>From 1997 to 2005, we exported the products through merchandiser and from 2005, we have been exporting in the label of âAsian Coir Industriesâ which is the sister concern of our Biocare Coco Substrates.</p>
<p>These are mainly exported to Europe,Canada, Spain,Netherlands,United Kingdom and Korea.</p>
<p>Our Company is well known for its innovative technology adopted products, the quality standards and customer oriented specifications fit to the particular need.</p>
<hr class="tall"/>
</div>
<div class="row">
<div class="col-md-12">
<h2 class="mobilealign">Our <strong>Products</strong></h2>
<div class="popup-inline-content" id="taxationservices">
<h2 class="mt-lg">Coco Chips</h2>
<div class="row">
<div class="col-md-5">
<img alt="" class="img-thumbnail img-responsive mb-lg" src="img/products/chips-block.jpg"/>
</div>
<div class="col-md-7">
<p>The coconut husk is cut in to random sized pieces, which are called coco chips. They are segregated into three sizes â Small, medium and large. The coco chips can be used to replace the bark based growth medium for Orchids and in cut flower production facilities. Small size chips range from 6mm to 13mm, the medium range from 13mm to 25mm and the large range from 25mm to 50mm. By mixing this, the air filled porosity can be increased in the substrate.</p>
<table border="0" cellpadding="0" cellspacing="0" class="tab-wrap" width="100%">
<tbody><tr>
<td class="green-bg" width="20%">Size of Chips</td>
<td bgcolor="#f3fbe5"> 6 to 20 mm</td>
</tr>
<tr>
<td class="green-bg">Bale Size</td>
<td bgcolor="#f3fbe5">30 x 30 x 15cm</td>
</tr>
<tr>
<td class="green-bg">E.C.</td>
<td bgcolor="#f3fbe5"> &lt;1 Mhos/cm</td>
</tr>
<tr>
<td class="green-bg">Packing</td>
<td bgcolor="#f3fbe5">Individually Polythene wrapped and palletized</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="popup-inline-content" id="briquettes">
<h2 class="mt-lg">COCO Briquettes</h2>
<div class="row">
<div class="col-md-5">
<img alt="" class="img-thumbnail img-responsive mb-lg" src="img/products/briquettes.jpg"/>
</div>
<div class="col-md-7">
<p>Briquettes are small sized compressed coir pith mainly made for home garden markets. This is also used by bulk consumers after reconstituting with fertilizers and for mixing with other growing medium.</p>
<table border="0" cellpadding="0" cellspacing="0" class="tab-wrap" width="100%">
<tbody><tr>
<td class="green-bg" width="20%">Weight</td>
<td bgcolor="#f3fbe5"> 650 gm Â± 30 gm</td>
</tr>
<tr>
<td class="green-bg">Size</td>
<td bgcolor="#f3fbe5">20 x 10 x 5 Cm</td>
</tr>
<tr>
<td class="green-bg">Compression Ratio</td>
<td bgcolor="#f3fbe5">8 : 1</td>
</tr>
<tr>
<td class="green-bg">Rehydrated yield</td>
<td bgcolor="#f3fbe5"> 8 to 9 lits</td>
</tr>
<tr>
<td class="green-bg">Wrapping</td>
<td bgcolor="#f3fbe5">Un-wrapped(individually Shrink wrap with label)</td>
</tr>
<tr>
<td class="green-bg">Packaging</td>
<td bgcolor="#f3fbe5">2000 Nos per pallet</td>
</tr>
<tr>
<td class="green-bg">Loadability</td>
<td bgcolor="#f3fbe5">20 Pallets / 40 FCL 
   (26 MTs approx.)</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="popup-inline-content" id="peatblocks">
<h2 class="mt-lg">COCO BLOCKS</h2>
<div class="row">
<div class="col-md-5">
<img alt="" class="img-thumbnail img-responsive mb-lg" src="img/products/peatblocks.jpg"/>
</div>
<div class="col-md-7">
<p>Blocks are made mainly for commercial use. It is reconstituted and used as a growing medium by green house cultivators and for potting mixes. After re-constitution it is mixed with fertilizer and other additives and packed as retail pack by manufacturers of growing medium.</p>
<table border="0" cellpadding="0" cellspacing="0" class="tab-wrap" width="100%">
<tbody><tr>
<td class="green-bg" width="20%">Weight</td>
<td bgcolor="#f3fbe5"> 5 Kg Â± 0.3 Kg</td>
</tr>
<tr>
<td class="green-bg">Size</td>
<td bgcolor="#f3fbe5">30 x 30 x 13 / 15 Cm</td>
</tr>
<tr>
<td class="green-bg">Compression Ratio</td>
<td bgcolor="#f3fbe5">5 : 1</td>
</tr>
<tr>
<td class="green-bg">Rehydrated yield</td>
<td bgcolor="#f3fbe5">13 to 14 lit / Kg</td>
</tr>
<tr>
<td class="green-bg">Wrapping</td>
<td bgcolor="#f3fbe5">Un-wrapped(individually Polythene wrapped)</td>
</tr>
<tr>
<td class="green-bg">Packaging</td>
<td bgcolor="#f3fbe5">210 Nos. per pallet</td>
</tr>
<tr>
<td class="green-bg">Loadability</td>
<td bgcolor="#f3fbe5">20 Pallets / 40 FCL (21 MTs approx.)</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="popup-inline-content" id="growbag">
<h2 class="mt-lg">Grow Bag</h2>
<div class="row">
<div class="col-md-5">
<img alt="" class="img-thumbnail img-responsive mb-lg" src="img/products/growbag.jpg"/>
</div>
<div class="col-md-7">
<p>In the grow bags, the plants can be directly planted and grown. The plant holes can be made on the top surface of the bags according to the crops to be raised. The drainage holes may be provided to drain excess water irrigated.</p>
<table border="0" cellpadding="0" cellspacing="0" class="tab-wrap" width="100%">
<tbody><tr>
<td class="green-bg" width="20%">Dimensions</td>
<td bgcolor="#f3fbe5"> 100 x 20 x 4 cm</td>
</tr>
<tr>
<td class="green-bg">Expanded size</td>
<td bgcolor="#f3fbe5">100 x 20 x 16 cm</td>
</tr>
<tr>
<td class="green-bg">Packaging</td>
<td bgcolor="#f3fbe5">Individually Packed in 
   UV treated bags</td>
</tr>
<tr>
<td class="green-bg">Loadability</td>
<td bgcolor="#f3fbe5">20 Pallets / 5460 
   Nos. per 40 FCL</td>
</tr>
</tbody></table>
</div>
</div>
</div>
<div class="owl-carousel" data-plugin-options='{"items": 4, "margin": 20, "loop": false}'>
<div>
<a data-plugin-lightbox="" data-plugin-options='{"type": "inline", preloader: false}' href="#taxationservices">
<span class="thumb-info">
<span class="thumb-info-wrapper">
<img alt="" class="img-responsive" src="img/products/chips-block.jpg"/>
<span class="thumb-info-title">
<span class="thumb-info-type">Coco Chips</span>
</span>
<span class="thumb-info-action">
<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
</span>
</span>
</span>
</a>
</div>
<div>
<a data-plugin-lightbox="" data-plugin-options='{"type": "inline", preloader: false}' href="#briquettes">
<span class="thumb-info">
<span class="thumb-info-wrapper">
<img alt="" class="img-responsive" src="img/products/briquettes.jpg"/>
<span class="thumb-info-title">
<span class="thumb-info-type">Briquettes</span>
</span>
<span class="thumb-info-action">
<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
</span>
</span>
</span>
</a>
</div>
<div>
<a data-plugin-lightbox="" data-plugin-options='{"type": "inline", preloader: false}' href="#peatblocks">
<span class="thumb-info">
<span class="thumb-info-wrapper">
<img alt="" class="img-responsive" src="img/products/peatblocks.jpg"/>
<span class="thumb-info-title">
<span class="thumb-info-type">Coco Blocks</span>
</span>
<span class="thumb-info-action">
<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
</span>
</span>
</span>
</a>
</div>
<div>
<a data-plugin-lightbox="" data-plugin-options='{"type": "inline", preloader: false}' href="#growbag">
<span class="thumb-info">
<span class="thumb-info-wrapper">
<img alt="" class="img-responsive" src="img/products/growbag.jpg"/>
<span class="thumb-info-title">
<span class="thumb-info-type">Grow Bags</span>
</span>
<span class="thumb-info-action">
<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
</span>
</span>
</span>
</a>
</div>
</div>
<hr class="tall"/>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="feature-box feature-box-style-2">
<div class="feature-box-icon">
<i class="fa fa-group"></i>
</div>
<div class="feature-box-info">
<h4 class="mb-xs">Our Infrastructure</h4>
<p>Our production unit is located at Singampunari Sivagangai district, which is 6 kms. From the NH 45 which connects Chennai and Tuticorin. Around 25 kilometers radius with Singampunari as center, it is a notable coconut growing area in India.</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="feature-box feature-box-style-2">
<div class="feature-box-icon">
<i class="fa fa-film"></i>
</div>
<div class="feature-box-info">
<h4 class="mb-xs">Our Team</h4>
<p>The C.E.O. of our company is Mr.Maharajan Prakash, who has 18 years experience in coco peat industry. He started his career in this industry from 1995, i.e. from the very first day of introduction of the industry in India.</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="feature-box feature-box-style-2">
<div class="feature-box-icon">
<i class="fa fa-bars"></i>
</div>
<div class="feature-box-info">
<h4 class="mb-xs">Our Quality Assurance</h4>
<p>The values of pH and E.C. is calculated by both 1:1.5 method which is otherwise called as Dutch method and 1:5 method which is otherwise called as Korean method.</p>
</div>
</div>
</div>
</div>
</div>
<hr class="tall"/>
<section class="section section-primary mb-none">
<div class="container">
<div class="row">
<div class="counters counters-text-light">
<div class="col-md-2 col-sm-6">
<div class="counter">
<strong data-append="%" data-to="90">0</strong>
<label>Raw Material</label>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="counter">
<strong data-append="%" data-to="20">0</strong>
<label>Moisture Content</label>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="counter">
<strong data-append="%" data-to="100">0</strong>
<label>Organic and Bio degradable </label>
</div>
</div>
<div class="col-md-4 col-sm-6">
<div class="counter">
<strong data-append="%" data-to="100">0</strong>
<label>Organic and Bio decomposable</label>
</div>
</div>
</div>
</div>
</div>
</section></div>
<section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer call-to-action-in-footer-margin-top">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="call-to-action-content">
<h3><strong>Biocare Coco Substrates</strong> goal is to âBuild enduring relationships with our clients that transcend debits and creditsâ</h3>
<p>Manufacturers of Export Quality Coir Products.</p>
</div>
<div class="call-to-action-btn">
<a class="btn btn-lg btn-primary" href="#" target="_blank">Contact Us</a><span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -12px;"></span> </div>
</div>
</div>
</div>
</section>
</div>
<footer id="footer">
<div class="container">
<div class="row">
<div class="footer-ribbon">
<span>Get in Touch</span> </div>
<div class="col-md-6">
<div class="newsletter">
<h4 class="heading-primary">About Us</h4>
<p>Our production unit is located at Singampunari Sivagangai district, which is 6 kms. From the NH 45 which connects Chennai and Tuticorin. Around 25 kilometers radius with Singampunari as center, it is a notable coconut growing area in India. The mesocarp of coconut is being processed into coir fiber and the coir pith is available in ample quantity ...</p>
<a href="#"><i class="fa fa-angle-double-right"></i> Read More...</a>
</div>
</div>
<div class="col-md-4">
<div class="contact-details">
<h4 class="heading-primary">Contact Us</h4>
<ul class="contact">
<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong>#15 / 1, 164 â Industrial Estate,Singampunari â 630 502,Sivagangai district,Tamilnadu, India.</p>
</li>
<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> 0091 â 4577 â 243917.,  0091 â 9366644882 / 9487037402.</p>
</li>
<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@biocarecocos.com">info@biocarecocos.com</a></p>
</li>
</ul>
</div>
</div>
<div class="col-md-2">
<h4 class="heading-primary">Follow Us</h4>
<div class="social-icons">
<ul class="social-icons">
<li class="facebook"><a data-placement="bottom" data-tooltip="" href="#" target="_blank" title="Facebook">Facebook</a></li>
<li class="twitter"><a data-placement="bottom" data-tooltip="" href="#" target="_blank" title="Twitter">Twitter</a></li>
<li class="linkedin"><a data-placement="bottom" data-tooltip="" href="#" target="_blank" title="Linkedin">Linkedin</a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer-copyright">
<div class="container">
<div class="row">
<div class="col-md-7">
<p>Biocare Coco Substrates © Copyright <script>document.write(new Date().getFullYear());</script>. All Rights Reserved.</p>
</div>
<div class="col-md-5">
<nav id="sub-menu">
<ul>
<li><a href="#">Privacy Policy</a></li>
<li><a href="#">Contact Us</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</footer>
<!-- Vendor -->
<!--[if lt IE 9]>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<![endif]-->
<!--[if gte IE 9]><!-->
<script src="vendor/jquery/jquery.js"></script>
<!--<![endif]-->
<script src="vendor/jquery.appear/jquery.appear.js"></script>
<script src="vendor/jquery.easing/jquery.easing.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.js"></script>
<script src="master/style-switcher/style.switcher.js"></script>
<script src="vendor/bootstrap/bootstrap.js"></script>
<script src="vendor/common/common.js"></script>
<script src="vendor/jquery.validation/jquery.validation.js"></script>
<script src="vendor/jquery.stellar/jquery.stellar.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.js"></script>
<script src="vendor/isotope/jquery.isotope.js"></script>
<script src="vendor/owlcarousel/owl.carousel.js"></script>
<script src="vendor/jflickrfeed/jflickrfeed.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="vendor/vide/vide.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>
<!-- Specific Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
<script src="js/views/view.home.js"></script>
<!-- Theme Custom -->
<script src="js/custom.js"></script>
<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>
</body>
</html>
