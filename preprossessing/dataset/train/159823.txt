<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Welcome</title>
<link href="css/normalize.css" media="all" rel="stylesheet" type="text/css"/>
<link href="css/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="css/grid.css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Anton" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css"/>
<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="js/superfish.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
<script type="text/javascript">
  	// initialise plugins
		jQuery(function(){
			// main navigation init
			jQuery('ul.sf-menu').superfish({
				delay:       1000, 		// one second delay on mouseout 
				animation:   {opacity:'show',height:'show'}, // fade-in and slide-down animation
				speed:       'normal',  // faster animation speed 
				autoArrows:  false,   // generation of arrow mark-up (for submenu) 
				dropShadows: false   // drop shadows (for submenu)
			});
			
		
			
		});
		
		// Init for audiojs
		audiojs.events.ready(function() {
			var as = audiojs.createAll();
		});
  </script>
<script type="text/javascript">
		jQuery(window).load(function() {
			// nivoslider init
			jQuery('#slider').nivoSlider({
				effect: 'sliceUpDown',
				slices:15,
				boxCols:8,
				boxRows:8,
				animSpeed:500,
				pauseTime:5000,
				directionNav:false,
				directionNavHide:false,
				controlNav:true,
				captionOpacity:1			});
		});
	</script>
<!-- Custom CSS -->
</head>
<body>
<div id="main"><!-- this encompasses the entire Web site -->
<header id="header">
<div class="container_12 clearfix">
<div class="grid_12">
<div class="logo" style="float:left;">
<a href="#" id="logo"><img src="images/logo.png"/></a>
</div>
<nav class="primary">
<ul class="sf-menu" id="topnav">
<li><a href="index.html">Home</a></li>
<li><a href="aboutus.html">About Us</a></li>
<li><a href="#">Products</a>
<ul class="sub-menu">
<li><a href="pet-acce.html">Pet Accessories Collection</a></li>
<li><a href="saddle.html">Saddle</a></li>
<li><a href="bridle.html">Bridle</a></li>
<li><a href="fancybelt.html">Fancy Belt</a></li>
<li><a href="halter.html">Halter</a></li>
<li><a href="polo-belts.html">Polo Belts</a></li>
<li><a href="girth.html">Girth</a></li>
<li><a href="breastplate.html">Breast Plate</a></li>
<li><a href="reins.hthml">Reins</a></li>
<li><a href="martingle.html">Martingle</a></li>
<li><a href="browband.html">Brow Band</a></li>
<li><a href="dogcollar.html">Dog Collar</a></li>
<li><a href="doglead.html">Dog Lead</a></li>
<li><a href="leads.html">Leads</a></li>
<li><a href="rugs.html">Rugs</a></li>
</ul>
</li>
<li><a href="feedback.php">Feedback</a></li>
<li><a href="contactus.html">Contact Us</a></li>
</ul> </nav><!--.primary-->
</div>
</div><!--.container_12-->
</header>
<section id="slider-wrapper">
<div class="container_12 clearfix">
<div class="grid_12">
<span class="slider-wrap"></span>
<div class="nivoSlider" id="slider">
<a href="#"><img src="images/slide04.jpg"/></a>
<a href="#"><img src="images/slide03.jpg"/></a>
<a href="#"><img src="images/slide02.jpg"/></a>
<a href="#"><img src="images/slide01.jpg"/></a>
</div>
</div>
</div>
</section><!--#slider-->
<div class="container_12 primary_content_wrap clearfix">
<div class="clearfix vr">
<div class="grid_8">
<div class="column-left column-inner">
<div class="widget-content" id="my_postwidget-3">
<ul class="latestpost">
<li class="clearfix">
<figure class="featured-thumbnail">
<a href="#"><img height="208" src="images/img1.jpg" width="218"/></a> </figure>
<h4 style="font-weight:normal;">Welcome To AS International</h4>
<div class="excerpt">
                  A. S. International is the house of quality leather saddlery, harnesses and all equestrian equipments. We wish to introduce ourselves as a reliable manufacturer and exporter of all type of leather equestrian equipments. We are exporting to number of leading and well-established customers all over the world. It will not be out of place to mention here that we not only have the capacity but are always willing to adopt new technology for improvement of the products we manufacture. 									</div>
<a class="button" href="aboutus.html">more</a> </li>
</ul>
<!-- Link under post cycle -->
</div>
</div>
</div>
<div class="grid_4">
<div class="column-right column-inner">
<ul class="recentcomments">
<li><img src="images/arrow.png"/> <a href="pet-acce.html">Pet Accessories Collection</a></li>
<li><img src="images/arrow.png"/> <a href="saddle.html">Saddle</a></li>
<li><img src="images/arrow.png"/> <a href="bridle.html">Bridle</a></li>
<li><img src="images/arrow.png"/> <a href="fancybelt.html">Fancy Belt</a></li>
<li><img src="images/arrow.png"/> <a href="halter.html">Halter</a></li>
<li><img src="images/arrow.png"/> <a href="polo-belts.html">Polo Belts</a></li>
<li><img src="images/arrow.png"/> <a href="girth.html">Girth</a></li>
<li><img src="images/arrow.png"/> <a href="breastplate.html">Breast Plate</a></li>
<li><img src="images/arrow.png"/> <a href="reins.hthml">Reins</a></li>
<li><img src="images/arrow.png"/> <a href="martingle.html">Martingle</a></li>
<li><img src="images/arrow.png"/> <a href="browband.html">Brow Band</a></li>
<li><img src="images/arrow.png"/> <a href="dogcollar.html">Dog Collar</a></li>
<li><img src="images/arrow.png"/> <a href="doglead.html">Dog Lead</a></li>
<li><img src="images/arrow.png"/> <a href="leads.html">LEADS</a></li>
<li><img src="images/arrow.png"/> <a href="rugs.html"> Rugs</a></li>
</ul>
</div>
</div>
</div>
</div>
<!--.container-->
</div><!--#main-->
<footer id="footer">
<div class="container_12 clearfix">
<div class="clearfix" id="copyright">
<div class="grid_12">
<div id="footer-text" style="color:#fff;">
<a class="site-name" href="#" style="color:#fff;">AS International</a> © 2018
												
												<!-- {%FOOTER_LINK} -->
</div>
</div>
</div>
</div><!--.container-->
</footer>
<!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>
