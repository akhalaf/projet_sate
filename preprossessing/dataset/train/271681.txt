<html><body><p>ï»¿<!DOCTYPE html>

</p>
<meta charset="utf-8"/>
<link href="/static/assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/static/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/static/assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="/static/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/static/css/style.css" rel="stylesheet" type="text/css"/>
<title>Byloth's Website</title>
<header class="jumbotron">
<div class="container">
<div class="page-header">
<h1>Byloth's Website</h1>
</div>
<h1>
<small>Coming soon...</small>
</h1>
</div>
</header>
<main id="main-container">
<div class="spacer"></div>
<div class="modal-content window ui-draggable ui-resizable" id="contact-window">
<div class="navbar navbar-default title-bar ui-draggable-handle">
<span class="navbar-brand">Contact form</span>
<button aria-label="Close" class="close" data-dismiss="window" type="button">
<span class="fa fa-times-circle"></span>
</button>
</div>
<div class="well">
<div class="panel panel-default">
<div class="panel-body">
<div id="contact-form">
<div class="form-group form-inline">
<div class="input-group">
<label class="input-group-addon" for="contact-form_name" id="contact-form_name-label">Name:</label>
<input aria-describedby="contact-form_name-label" class="form-control" id="contact-form_name" placeholder="John Doe" required="required" type="text"/>
</div>
<div class="input-group">
<label class="input-group-addon" for="contact-form_email" id="contact-form_email-label">e-mail address:</label>
<input aria-describedby="contact-form_email-label" class="form-control" id="contact-form_email" placeholder="john.doe@email.com" required="required" type="email"/>
</div>
</div>
<div class="form-group">
<label for="contact-form_subject">Object:</label>
<input class="form-control" id="contact-form_subject" placeholder="Contact request" required="required" type="text"/>
</div>
<div class="form-group">
<label for="contact-form_message">Message:</label>
<textarea class="form-control" id="contact-form_message" placeholder="Write here your message..." required="required" rows="5"></textarea>
</div>
<div class="form-group text-right">
<button class="btn btn-primary" id="contact-form_submit" type="submit">
<span class="fa fa-paper-plane-o"></span>
<span class="text">Send</span>
</button>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal-content window ui-draggable ui-resizable" id="code-window">
<div class="navbar navbar-default title-bar ui-draggable-handle">
<span class="navbar-brand">New project</span>
<button aria-label="Close" class="close" data-dismiss="window" type="button">
<span class="fa fa-times-circle"></span>
</button>
</div>
<div class="well">
<div class="panel-body">
<pre id="code"><span></span></pre>
</div>
</div>
</div>
<div class="modal-content window ui-draggable ui-resizable" id="console-window">
<div class="navbar navbar-default title-bar ui-draggable-handle">
<span class="navbar-brand">Byloth's Website Console</span>
<button aria-label="Close" class="close" data-dismiss="window" type="button">
<span class="fa fa-times-circle"></span>
</button>
</div>
<div class="well">
<div class="panel panel-default" id="console">
<div class="panel-body">
                            Byloth's WEB_SRV 3.0-RELEASE (STABLE): <span id="full-date"></span>
<br/>
<br/>
                            Welcome to Byloth's Website!<br/>
<br/>
                            Login: <span class="cursor">_</span>
<!-- guest@byloth_srv:~$ <span class="cursor">_</span> -->
</div>
</div>
</div>
</div>
</main>
<footer id="page-footer">
<div class="container">
<div class="row">
<div class="col-md-7 col-sm-7 col-xs-7 col">
                        Copyright Â© 2014-<span id="date-year"></span>, Byloth's Website. All rights reserved.
                    </div>
<div class="col-md-5 col-sm-5 col-xs-5 text-right">
                        Designed &amp; Developed with <span class="fa fa-heart"></span> by
                        <a href="https://github.com/Byloth" target="_blank" title="Byloth on GitHub"><span class="fa fa-github"></span> Byloth</a>.
                    </div>
</div>
</div>
</footer>
<script src="/static/assets/js/jquery.min.js" type="application/javascript"></script>
<script src="/static/assets/js/bootstrap.min.js" type="application/javascript"></script>
<script src="/static/assets/js/jquery-ui.min.js" type="application/javascript"></script>
<script src="/static/assets/js/typed.min.js" type="application/javascript"></script>
<script src="/static/js/script.js" type="application/javascript"></script>
<script src="/static/js/typed.js" type="application/javascript"></script>
</body></html>