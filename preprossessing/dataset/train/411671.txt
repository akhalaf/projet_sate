<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
<head>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- normalize and html5 boilerplate resets -->
<link href="templates/Skyline_v2/resources/css/reset.css" rel="stylesheet"/>
<link href="templates/Skyline_v2/resources/css/less.build.css" rel="stylesheet"/>
<!--[if lte IE 9]>
        <script src="templates/Skyline_v2/resources/js/html5shiv.js"></script>
        <script src="templates/Skyline_v2/resources/js/html5shiv-printshiv.js"></script>

        <![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>DHANDS Electrical</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<link href="resources/XJ-HBL.jpg" rel="shortcut icon" type="image/x-icon"/>
<link href="resources/XJ-HBL.jpg" rel="icon" type="image/x-icon"/>
<!-- Start of user defined header tracking codes -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118757979-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118757979-1');
</script>
<!-- End of user defined header tracking codes -->
<style id="styleCSS" type="text/css">
    /*
    Some Style Themes enhanced with background textures provided by http://subtlepatterns.com/
*/
body {
    
    
    background-repeat: repeat;
    background-attachment: fixed;
    background-position: top left;
    background-size: auto;
}

/* IOS devices 'bgd-att: fixed' solution */
@media only screen and (max-device-width: 1366px) {
    .bgd-attachment-fixed {
        background-image: none;
    }
    .bgd-attachment-fixed:after {
        content: '';
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        
        background-repeat: repeat;
        background-position: top left;
        background-size: auto;
        z-index: -2;
    }
}

.Text_2_Default,
.yola_heading_container {
  word-wrap: break-word;
}

.yola_bg_overlay {
    display:table;
    table-layout: fixed;
    position:absolute;
    min-height: 100%;
    min-width: 100%;
    width:100%;
    height:100%;
}
.yola_outer_content_wrapper {
    
    padding-right: 0px;
    
    padding-left: 0px;
}
.yola_inner_bg_overlay {
    width:100%;
    min-height: 100vh;
    display: table-cell;
    
    vertical-align: top;
}
.yola_outer_heading_wrap {
    width:100%;
    text-align: center;
}
.yola_heading_container {
    margin: 0 auto;
    
    	background-color: #000000;

}
.yola_inner_heading_wrap {
    margin: 0 auto;
    
}
.yola_innermost_heading_wrap {
    padding-left:0;
    padding-right:0;
    margin: 0 auto;
    padding-top: 40px;
    padding-bottom: 40px;
}
.yola_inner_heading_wrap.top nav,
.yola_inner_heading_wrap.top div#yola_heading_block,
.yola_inner_heading_wrap.bottom nav,
.yola_inner_heading_wrap.bottom div#yola_heading_block {
    padding-left: 60px;
    padding-right: 60px;
}
.yola_inner_heading_wrap.left .yola_innermost_heading_wrap,
.yola_inner_heading_wrap.right .yola_innermost_heading_wrap {
    padding-left: 60px;
    padding-right: 60px;
}
.yola_inner_heading_wrap h1 {
    margin: 0;
}
#yola_nav_block {
    height: 100%;
}
#yola_nav_block nav {
    text-align: center;
    
}
#yola_nav_block nav ul{
    display:inline;
}
.yola_inner_heading_wrap.left #yola_heading_block {
    float:left;
}
.yola_inner_heading_wrap.right #yola_heading_block {
    float:right;
}
.yola_inner_heading_wrap.top #yola_nav_block {
    padding:1rem 0 0 0;
}
.yola_inner_heading_wrap.right #yola_nav_block {
    float:left;
    padding:1rem 0 0 0;
}
.yola_inner_heading_wrap.bottom #yola_nav_block {
    padding:0 0 1rem 0;
}
.yola_inner_heading_wrap.left #yola_nav_block {
    float:right;
    padding:1rem 0 0 0;
}
.yola_banner_wrap {
    background-attachment: scroll;
    text-align: center;
    margin: 0 auto;
    
    display: none;
    background-position: top left;
    background-size: auto;
    background-repeat: repeat-x;
    
    
}
.yola_inner_banner_wrap {
    padding-left:0;
    padding-right:0;
    padding-top: 0px;
    padding-bottom: 0px;
    
}
.yola_innermost_banner_wrap {
    margin: 0 auto;
    
}
.yola_inner_nav_wrap {
    margin: 0 auto;
    
}
.yola_banner_wrap nav ul.sys_navigation {
    text-align: center;
    padding-top:1rem;
    padding-bottom:1rem;
}
.yola_banner_wrap h1 {
    margin:0;
    text-align: center;
}
.yola_site_tagline {
    padding-top:0;
    padding-bottom:0;
    font-family: 'Open Sans';
    font-size: 3rem;
    color: #ffffff;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
    text-align: left;
    padding-right: 0px;
    padding-left: 0px;

}
.yola_site_tagline span {
    display: inline-block;
    
    
    
    
    
}
ul.sys_navigation {
    margin: 0;
    padding: 0;
    text-align: center;
}
ul.sys_navigation li {
    display: inline;
    list-style: none;
    margin:0 1rem 0 0;
}
.yola_inner_heading_wrap ul.sys_navigation li:last-child {
    margin:0;
}
ul.sys_navigation li a{
    text-decoration: none;
}

div.ys_submenu {
    margin-top: 8px;
}

.yola_content_wrap {
    margin:0 auto;
    
    
}
.yola_content_column {
    margin:0 auto;
    
}

.yola_inner_content_column {
    margin:0 auto;

    
    
    
    
}
.yola_inner_footer_wrap {
    padding: 0 20px;
}
div[id*='sys_region_'] {
    padding-left: 0 ! important;
    padding-right: 0 ! important;
}
.yola_site_logo {
    
    max-width:100%;
}
#sys_heading.yola_hide_logo img {
    display:none;
}
#sys_heading.yola_hide_logo span {
    display:inline;
}
a#sys_heading.yola_show_logo {
    font-size:14px;
}
#sys_heading.yola_show_logo img {
    display:inline;
}
#sys_heading.yola_show_logo span {
    display:none;
}
.yola_footer_wrap {
    margin:0 auto;
    
    	background-color: #000000;

}
.yola_footer_column {
    margin:0 auto;
    
    display: block;
}
footer {
    padding-top: 5px;
    padding-right: 60px;
    padding-bottom: 5px;
    padding-left: 60px;
    font-family: 'Gentium Book Basic';
    font-size: 1rem;
    color: #ffffff;
    line-height: 1.5em;
    
    text-transform: none;

}
span.yola_footer_socialbuttons {
    display:inline-block;
    line-height:0;
    margin:0;
    padding:0;
    display:inline-block;
    position:static;
    float:left;
    width:146px;
    height:20px;
    display: none;
}
.sys_yola_form .submit,
.sys_yola_form input.text,
.sys_yola_form input.email,
.sys_yola_form input.tel,
.sys_yola_form input.url,
.sys_yola_form textarea {
    font-family: 'Gentium Book Basic';
    font-size: 1rem;
    line-height: 1.5em;
    
    text-transform: none;
}
div.sys_yola_form {
    padding:0 !important;
}
div.sys_yola_form form {
    margin:0 !important;
    padding:0 !important;
}
.sys_layout h2, .sys_txt h2, .sys_layout h3, .sys_txt h3, .sys_layout h4, .sys_txt h4, .sys_layout h5, .sys_txt h5, .sys_layout h6, .sys_txt h6, .sys_layout p, .sys_txt p {
    margin-top:0;
}
div[id*='sys_region_'] {
    padding:0 !important;
}
.sys_layout blockquote {
  margin-top: 10px;
  margin-bottom: 10px;
  margin-left: 50px;
  padding-left: 15px;
  border-left: 3px solid #000000;;
  font-size: 1.5rem;
  font-style: italic;
  color: #000000;
  
  
  
  
}
.sys_layout blockquote,
.sys_layout blockquote h1,
.sys_layout blockquote h2,
.sys_layout blockquote h3,
.sys_layout blockquote h4,
.sys_layout blockquote h5,
.sys_layout blockquote h6,
.sys_layout blockquote p {
    font-family: 'Titillium Web';
}
.sys_layout p,.sys_layout pre {margin:0 0 1.5em 0}
.sys_layout h2,.sys_layout h3,.sys_layout h4,.sys_layout h5,.sys_layout h6 { margin:0 0 0.5em 0 }
.sys_layout dl, .sys_layout menu,.sys_layout ol,.sys_layout ul{margin:0 0 1.5em 0}

.mob_menu {
    display: none;
}

.new-text-widget img, .old_text_widget img {
    max-width: 100%;
}


@media only screen and (max-width : 736px) {
    html {
        font-size: 80%;
    }

    body .m_inherit_width {
        width: inherit;
    }

    .small_device_hide {
        opacity: 0;
    }

    /* Remove display table so that fixefox can understand max-width */
    .yola_bg_overlay, .yola_inner_bg_overlay {
       display: block;
    }

    /* Zero out padding of the heading wrapper */
    .yola_inner_heading_wrap.top .yola_innermost_heading_wrap,
    .yola_inner_heading_wrap.bottom .yola_innermost_heading_wrap,
    .yola_inner_heading_wrap.left .yola_innermost_heading_wrap,
    .yola_inner_heading_wrap.right .yola_innermost_heading_wrap {
        padding-left: 0;
        padding-right: 0;
    }

    /* Make all image widgets center aligned */
    .Image_Default img {
        display: block;
        margin: 0 auto;
    }

    /* Center button widgets in column dividers */
    .column_divider .sys_button {
        text-align: center;
    }

    /* Make column dividers snap to one over another */
    .yola_inner_heading_wrap.left #yola_heading_block, .yola_inner_heading_wrap.right #yola_heading_block {
        float: none;
    }

    #sys_heading {
        word-wrap: break-word;
        word-break: break-word;
    }

    body .column_divider .left, body .column_divider .right {
        width: 100%;
        padding-left: 0;
        padding-right: 0;
    }

    .mob_menu a:visited {
        color: #fff;
    }

    .mob_menu {
        display: block;
        z-index: 1;
        ;
        background: rgba(0,0,0,1.00);;
        ;
    }

    .mob_menu.menu_open {
        position: absolute;
        min-height: 100%;
        padding: 1rem 0 0 0;
        margin: 0;
        top: 0;
        left: 0;
        right: 0;
    }

    .yola_outer_content_wrapper {
        display: block;
        padding-top: 0;
    }

    .mob_menu_overlay {
        display: none;
        text-transform: none;
    }

    .menu_open .mob_menu_overlay  {
        display: block;
    }

    .mob_menu_toggle {
        display: block;
        padding-top: 5%;
        padding-bottom: 6%;
        text-align: center;
        color: #666;
        cursor: pointer;
    }
    .mob_submenu_toggle {
        list-style: none;
        text-align: center;
        padding: 0;
        margin: 0;
    }

    .new-text-widget img, .old_text_widget img {
        height: auto;
    }

    #sys_heading span {
        font-size: 35px;
        font-weight: 500;
    }
    .sys_navigation {
        display: none;
    }

    .mobile_ham {
        stroke: #ffffff;
    }

    .mobile_quit {
        display: none;
    }

    .menu_open .mobile_ham {
        display: none;
    }

    .menu_open .mobile_quit {
        display: inline;
        stroke: #ffffff;
    }

    .mob_menu_list {
        font-family: 'Open Sans';
        font-weight: lighter;
        margin: 0;
        font-size: 2.2em;
        line-height: 2;
        letter-spacing: 0.1em;
        list-style: none;
        text-align: center;
        padding: 0;
        -webkit-animation-duration: .2s;
        -webkit-animation-fill-mode: both;
        -webkit-animation-name: fadeInUp;
        -moz-animation-duration: .2s;
        -moz-animation-fill-mode: both;
        -moz-animation-name: fadeInUp;
        -o-animation-duration: .2s;
        -o-animation-fill-mode: both;
        -o-animation-name: fadeInUp;
        animation-duration: .2s;
        animation-fill-mode: both;
        animation-name: fadeInUp;
    }

    .mob_menu_overlay .mob_menu_list a {
        color: #ffffff;
    }

    .mob_more_toggle {
        display: inline-block;
        cursor: pointer;
        background: none;
        border: none;
        outline: none;
        margin-left: 8px;
        stroke: #ffffff;
    }

    .up_arrow {
        display: none;
    }

    .sub_menu_open svg .down_arrow {
        display: none;
    }

    .sub_menu_open .up_arrow {
        display: inline;
    }

    .mob_menu_overlay .mob_menu_list .selected a {
        color: #ffffff;
    }

    .sub_menu_open a {
        color: #ffffff;
    }

    .mob_menu_list .sub_menu_open a {
        color: #ffffff;
    }

    .sub_menu_open .mob_more_toggle {
        stroke: #ffffff;
    }

    .mob_submenu_list {
        font-family: 'Open Sans';
        font-weight: lighter;
        list-style: none;
        text-align: center;
        padding: 0 0 5% 0;
        margin: 0;
        line-height: 1.6;
        display: none;
        -webkit-animation-duration: .2s;
        -webkit-animation-fill-mode: both;
        -webkit-animation-name: fadeInUp;
        -moz-animation-duration: .2s;
        -moz-animation-fill-mode: both;
        -moz-animation-name: fadeInUp;
        -o-animation-duration: .2s;
        -o-animation-fill-mode: both;
        -o-animation-name: fadeInUp;
        animation-duration: .2s;
        animation-fill-mode: both
        animation-name: fadeInUp;
    }

    .sub_menu_open .mob_submenu_list{
        display: block;
    }

    .mob_submenu_items {
        font-size: 0.75em;
    }
    .mob_menu_list .mob_nav_selected {
        color: #ffffff;
    }

    .menu_open ~ .yola_outer_content_wrapper {
        display: none;
    }

    @-webkit-keyframes fadeInUp {
      0% {
        opacity: 0;
        -webkit-transform: translate3d(0, 100%, 0);
        transform: translate3d(0, 100%, 0);
      }
      100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
      }
    }

    @-moz-keyframes fadeInUp {
      0% {
        opacity: 0;
        -moz-transform: translate3d(0, 100%, 0);
        transform: translate3d(0, 100%, 0);
      }
      100% {
        opacity: 1;
        -moz-transform: none;
        transform: none;
      }
    }

    @-o-keyframes fadeInUp {
      0% {
        opacity: 0;
        -o-transform: translate3d(0, 100%, 0);
        transform: translate3d(0, 100%, 0);
      }
      100% {
        opacity: 1;
        -o-transform: none;
        transform: none;
      }
    }

    @keyframes fadeInUp {
      0% {
        opacity: 0;
        transform: translate3d(0, 100%, 0);
      }
      100% {
        opacity: 1;
        transform: none;
      }
    }
}


  </style>
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.2/webfont.js" type="text/javascript"></script>
<style type="text/css">
      @import url("//fonts.googleapis.com/css?family=Titillium+Web%3Aregular%2C400|Gentium+Book+Basic%3Aregular|Open+Sans%3Aregular&subset=latin,latin-ext");
    </style>
<style id="styleOverrides" type="text/css">
    /* ======================
*
*  Site Style Settings
*
=========================*/
/* Paragraph text (p) */

.content p, #content p, .HTML_Default p, .Text_Default p, .sys_txt p, .sys_txt a, .sys_layout p, .sys_txt, .sys_layout  {
    font-family: 'Gentium Book Basic';
    
    font-size: 1rem;
    color: #000000;
    line-height: 1.5em;
    
    text-transform: none;
}

/* Navigation */
.sys_navigation a, .ys_menu_2, div#menu ul, div#menu ul li a, ul.sys_navigation li a, div.sys_navigation ul li.selected a, div.sys_navigation ul li a, #navigation li a, div.ys_menu ul a:link, div.ys_menu ul a:visited, div.ys_nav ul li a, #sys_banner ul li a {
    font-family: 'Open Sans';
    
    font-size: 1rem;
    color: #ffffff;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}


/* Navigation:selected */
div.sys_navigation ul li.selected a, div#menu ul li.selected a, #navigation li.selected a, div.ys_menu ul li.selected a:link, div.ys_menu ul li.selected a:visited, div.ys_nav ul li.selected a, #sys_banner ul li.selected a {
    color: #ffffff;
}

/* Navigation:hover */
div.sys_navigation ul li a:hover, div#menu ul li a:hover, #navigation li a:hover, div.ys_menu ul a:hover, div.ys_nav ul li a:hover, div.ys_menu ul li a:hover, #sys_banner ul li a:hover {
    color: #808080;
}

/* Site Title */
#sys_heading, a#sys_heading, #sys_banner h1 a, #header h1 a, div#heading h1 a {
    font-family: 'Open Sans';
    
    font-size: 2rem;
    color: #ffffff;
    text-decoration: none;
    letter-spacing: 1px;
    line-height: 1.5em;
    text-transform: uppercase;
}

/* Hyperlinks (a, a:hover, a:visited) */

a, .sys_txt a:link, .sys_layout a:link {text-decoration: none;}


a:hover, .sys_txt a:hover, .sys_layout a:hover {text-decoration: underline;}

/* Headings (h2, h3, h4, h5, h6) */
.sys_layout h2, .sys_txt h2 {
    font-family: 'Open Sans';
    
    font-size: 3.5rem;
    color: #000000;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.2em;
    text-transform: none;
}

.sys_layout h2 a, .sys_layout h2 a:link, .sys_layout h2 a:hover, .sys_layout h2 a:visited {
    font-family: 'Open Sans';
    
    font-size: 3.5rem;
    color: #000000;
    letter-spacing: 0px;
    line-height: 1.2em;
    text-transform: none;
}

.sys_layout h3, .sys_txt h3 {
    font-family: 'Open Sans';
    
    font-size: 2.3rem;
    color: #e0a400;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h3 a, .sys_layout h3 a:link, .sys_layout h3 a:hover, .sys_layout h3 a:visited {
    font-family: 'Open Sans';
    
    font-size: 2.3rem;
    color: #e0a400;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h4, .sys_txt h4 {
    font-family: 'Open Sans';
    
    font-size: 1.6rem;
    color: #000000;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h4 a, .sys_layout h4 a:link, .sys_layout h4 a:hover, .sys_layout h4 a:visited {
    font-family: 'Open Sans';
    
    font-size: 1.6rem;
    color: #000000;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h5, .sys_txt h5 {
    font-family: 'Open Sans';
    
    font-size: 1rem;
    color: #ccde00;
    text-decoration: none;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h5 a, .sys_layout h5 a:link, .sys_layout h5 a:hover, .sys_layout h5 a:visited {
    font-family: 'Open Sans';
    
    font-size: 1rem;
    color: #ccde00;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h6, .sys_txt h6 {
    font-family: 'Open Sans';
    
    font-size: 0.8rem;
    color: #de001a;
    
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

.sys_layout h6 a, .sys_layout h6 a:link, .sys_layout h6 a:hover, .sys_layout h6 a:visited {
    font-family: 'Open Sans';
    
    font-size: 0.8rem;
    color: #de001a;
    letter-spacing: 0px;
    line-height: 1.5em;
    text-transform: none;
}

/*button widget*/
.sys_layout .sys_button a, .sys_layout .sys_button a:link, .sys_layout .sys_button a:visited {
    display:inline-block;
    text-decoration: none;
}
.sys_layout .sys_button a:link, .sys_layout .sys_button a:visited {
    cursor:pointer;
}
.sys_layout .sys_button a {
    cursor:default;
}

.sys_layout .sys_button.square a, .sys_layout .sys_button.square a:link {
    border-radius:0px;
}
.sys_layout .sys_button.rounded a, .sys_layout .sys_button.rounded a:link {
    border-radius:3px;
}
.sys_layout .sys_button.pill a, .sys_layout .sys_button.pill a:link {
    border-radius:90px;
}

/*button sizes*/
.sys_layout .sys_button.small a, .sys_layout .sys_button.small a:link, .sys_layout .sys_button.small a:visited {font-family: 'Gentium Book Basic';font-size: 1rem;padding-top:0.5rem;padding-bottom:0.5rem;padding-left:1rem;padding-right:1rem;}
.sys_layout .sys_button.medium a, .sys_layout .sys_button.medium a:link, .sys_layout .sys_button.medium a:visited {font-family: 'Gentium Book Basic';font-size: 1rem;line-height: 1.5em;letter-spacing: 0px;text-transform: none;padding-top:1rem;padding-bottom:1rem;padding-left:3rem;padding-right:3rem;}
.sys_layout .sys_button.large a, .sys_layout .sys_button.large a:link, .sys_layout .sys_button.large a:visited {font-family: 'Gentium Book Basic';font-size: 1rem;padding-top:1.5rem;padding-bottom:1.5rem;padding-left:2rem;padding-right:2rem;}

/*button styles:small*/
.sys_layout .sys_button.small.outline a, .sys_layout .sys_button.small.outline a:link {
    border-color:rgba(100,68,94,1.00);
    color: rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.small.outline a:visited {
    color: rgba(100,68,94,1.00);
}
.sys_layout .sys_button.small.solid a, .sys_layout .sys_button.small.solid a:link {
    	background-color: #64445e;

    color: #ffffff;
    border-color:rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.small.solid a:visited {
    color: #ffffff;
}
.sys_layout .sys_button.small.outline a:hover {
    background-color: rgba(100,68,94,1.00);
    color: #ffffff;
    text-decoration: none;
}

/*button styles:medium*/
.sys_layout .sys_button.medium.outline a, .sys_layout .sys_button.medium.outline a:link {
    border-color:rgba(100,68,94,1.00);
    color: rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.medium.outline a:visited {
    color: rgba(100,68,94,1.00);
}
.sys_layout .sys_button.medium.solid a, .sys_layout .sys_button.medium.solid a:link {
    	background-color: #64445e;

    color: #ffffff;
    border-color:rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.medium.solid a:visited {
    color: #ffffff;
}
.sys_layout .sys_button.medium.outline a:hover {
    background-color: rgba(100,68,94,1.00);
    color: #ffffff;
    text-decoration: none;
}
/*button styles:large*/
.sys_layout .sys_button.large.outline a, .sys_layout .sys_button.large.outline a:link {
    border-color:rgba(100,68,94,1.00);
    color: rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.large.outline a:visited {
    color: rgba(100,68,94,1.00);
}
.sys_layout .sys_button.large.solid a, .sys_layout .sys_button.large.solid a:link {
    	background-color: #64445e;

    color: #ffffff;
    border-color:rgba(100,68,94,1.00);
    border-style: solid;
    border-width: 2px;
}
.sys_layout .sys_button.large.solid a:visited {
    color: #ffffff;
}
.sys_layout .sys_button.large.outline a:hover {
    background-color: rgba(100,68,94,1.00);
    color: #ffffff;
    text-decoration: none;
}

.sys_layout .sys_button.solid a:hover {
    text-decoration: none;
    opacity: .8;
}  </style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">window.jQuery || document.write('<script src="/components/bower_components/jquery/dist/jquery.js"><\/script>')</script>
<link href="classes/commons/resources/flyoutmenu/flyoutmenu.css?1001073" rel="stylesheet" type="text/css"/>
<script src="classes/commons/resources/flyoutmenu/flyoutmenu.js?1001073" type="text/javascript"></script>
<link href="classes/commons/resources/global/global.css?1001073" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
  var swRegisterManager = {
    goals: [],
    add: function(swGoalRegister) {
      this.goals.push(swGoalRegister);
    },
    registerGoals: function() {
      while(this.goals.length) {
        this.goals.shift().call();
      }
    }
  };

  window.swPostRegister = swRegisterManager.registerGoals.bind(swRegisterManager);
</script>
<link href="classes/components/Image/layouts/Default/Default.css?1001073" rel="stylesheet" type="text/css"/>
</head>
<body class="bgd-attachment-fixed" lang="en">
<script data-embed-key="AIzaSyARNP6PBrYdKNmsYf09tU8jRnffQb-sFQg" id="locationData" type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "LocalBusiness",
            "telephone": "0439 078 503",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Ashfield ",
                "addressLocality": "",
                "addressRegion": "NSW",
                "addressCountry": "Australia",
                "postalCode": "2131"
            },
            "openingHours": ["Mo 07:00-19:00", "Tu 07:00-19:00", "We 07:00-19:00", "Th 07:00-19:00", "Fr 07:00-19:00", "Sa 08:00-17:00"]
        }
    </script>
<div class="yola_bg_overlay" id="sys_background">
<div class="yola_inner_bg_overlay">
<div class="yola_outer_content_wrapper">
<header role="header">
<div class="yola_outer_heading_wrap">
<div class="yola_heading_container">
<div class="yola_inner_heading_wrap left">
<div class="yola_innermost_heading_wrap">
<nav class="mob_menu">
<div class="mob_menu_toggle"><!--Mobile Nav Toggle-->
<svg class="mobile_ham" height="25" width="40">
<line stroke-width="2" x1="0" x2="40" y1="3" y2="3"></line>
<line stroke-width="2" x1="0" x2="40" y1="13" y2="13"></line>
<line stroke-width="2" x1="0" x2="40" y1="23" y2="23"></line>
</svg>
<svg class="mobile_quit" height="50" width="26">
<line stroke-width="2" x1="0" x2="26" y1="1" y2="25"></line>
<line stroke-width="2" x1="0" x2="26" y1="25" y2="1"></line>
</svg>
</div>
<div class="mob_menu_overlay"> <!--Mobile Nav Overlay-->
<ul class="mob_menu_list">
<li class="selected">
<a href="./" title="Home">Home</a>
</li>
</ul>
</div>
</nav>
<div id="yola_heading_block"> <!--Title / Logo-->
<h1>
<a class="yola_show_logo" href="./" id="sys_heading">
<img alt="" class="yola_site_logo" src="resources/large-white.png"/>
<span></span>
</a>
</h1>
</div>
<div id="yola_nav_block"> <!--Nav-->
<nav role="navigation">
<div class="sys_navigation">
<ul class="sys_navigation">
<li class="selected" id="ys_menu_0">
<a href="./" title="Home">Home</a>
</li>
</ul>
<script>
/* jshint ignore:start */
$(document).ready(function() {
    flyoutMenu.initFlyoutMenu(
        [{"name":"Home","title":"Home","href":".\/","children":[]}]
    , 'flyover');
});
/* jshint ignore:end */
</script>
</div>
</nav>
</div>
<div style="padding:0; height:0; clear:both;"> </div>
</div>
</div>
</div>
<div class="yola_banner_wrap" id="sys_banner">
<div class="yola_inner_banner_wrap">
<div class="yola_innermost_banner_wrap">
<h2 class="yola_site_tagline" style="display:none"><span></span></h2>
</div>
</div>
</div>
</div>
</header>
<main class="yola_content_wrap" role="main">
<div class="yola_content_column">
<div class="yola_inner_content_column clearFix">
<style media="screen">
  .layout_1-column {
    width: 100%;
    padding: 0;
    margin: 0;
  }

  .layout_1-column:after {
    content: "";
    display: table;
    clear: both;
  }

  .zone_top {
    margin: 0;
    padding: 5px;
    vertical-align: top;
    line-height: normal;
    min-width: 100px;
  }
</style>
<div class="layout_1-column sys_layout">
<div id="layout_row1">
<div class="zone_top" id="sys_region_1"><div class="Image_Default" id="Iec65a045c4f345959fe24e246215ef76" style="display:block;clear: both;text-align:center;"><a href="https://sh188.isrefer.com/go/lp01/a54/" target="_blank"><style>#Iec65a045c4f345959fe24e246215ef76_img {-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;width: 563px;border:none;max-width: 100%;height: auto;}</style><img id="Iec65a045c4f345959fe24e246215ef76_img" src="resources/FIBARO_HOME_INTELLIGENCE2.JPG"/></a></div><div class="Text_2_Default" id="I719fcf506a2b4b4bb97416ecf03d8bfc" style="display:block;clear: both;margin:1rem 1rem 1rem 1rem;"><style type="text/css">
    div.sys_text_widget img.float-left{float:left;margin:10px 15px 10px 0;}
    div.sys_text_widget img.float-right{position:relative;margin:10px 0 10px 15px;}
    div.sys_text_widget img{margin:4px;}
    div.sys_text_widget {
        overflow: hidden;
        margin: 0;
        padding: 0;
        color: ;
        font: ;
        background-color: ;
    }
</style>
<div class="sys_txt sys_text_widget new-text-widget" id="I719fcf506a2b4b4bb97416ecf03d8bfc_sys_txt" systemelement="true"><h4 class="over_outline" style="color: #000000; text-align: center;">for all your home and office electrical services and automation requirements contact Daniel on 0439 078 503 or email <a class="over_outline" href="mailto:daniel@dhands.com.au" style="color: #000000;" title="Offering Solutions">daniel@dhands.com.au</a></h4></div></div><div class="Layout1_Default" id="I8a818b29954f45c38d83fd594acbcbb9" style="display:block;clear: both;margin:1rem 1rem 1rem 1rem;"><style>.column_I8a818b29954f45c38d83fd594acbcbb9 {width: 100%;-moz-box-sizing:border-box;-webkit-box-sizing: border-box;box-sizing:border-box;}.column_I8a818b29954f45c38d83fd594acbcbb9:after {content: "";display: table;clear: both;}.column_I8a818b29954f45c38d83fd594acbcbb9 .left {text-align: left;vertical-align: top;width: 59.336543606206526%;padding: 0 15px 0 0;float: left;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing:border-box;}.column_I8a818b29954f45c38d83fd594acbcbb9 .right {vertical-align: top;width: 40.663456393793474%;padding: 0 0 0 15px;float: left;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;}</style><div class="column_I8a818b29954f45c38d83fd594acbcbb9 column_divider"><div class="left" id="Left_I8a818b29954f45c38d83fd594acbcbb9"><div class="Layout1_Default" id="I04b8cfc7cb0a4dae8c1436908cae16bc" style="display:block;clear: both;"><style>.column_I04b8cfc7cb0a4dae8c1436908cae16bc {width: 100%;-moz-box-sizing:border-box;-webkit-box-sizing: border-box;box-sizing:border-box;}.column_I04b8cfc7cb0a4dae8c1436908cae16bc:after {content: "";display: table;clear: both;}.column_I04b8cfc7cb0a4dae8c1436908cae16bc .left {text-align: left;vertical-align: top;width: 50%;padding: 0 15px 0 0;float: left;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing:border-box;}.column_I04b8cfc7cb0a4dae8c1436908cae16bc .right {vertical-align: top;width: 50%;padding: 0 0 0 15px;float: left;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;}</style><div class="column_I04b8cfc7cb0a4dae8c1436908cae16bc column_divider"><div class="left" id="Left_I04b8cfc7cb0a4dae8c1436908cae16bc"><div class="Image_Default" id="If330232e15ff493990841f9399d15886" style="display:block;clear: both;text-align:center;"><style>#If330232e15ff493990841f9399d15886_img {-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;width: 240px;border:none;max-width: 100%;height: auto;}</style><img id="If330232e15ff493990841f9399d15886_img" src="resources/KNXENNorm_RGB.jpg"/></div></div><div class="right" id="Right_I04b8cfc7cb0a4dae8c1436908cae16bc"><div class="Image_Default" id="I08800d89f25645ec9e679a7b05fd28b6" style="display:block;clear: both;text-align:center;margin:1rem 0 0 0;"><style>#I08800d89f25645ec9e679a7b05fd28b6_img {-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;width: 370px;border:none;max-width: 100%;height: auto;}</style><img id="I08800d89f25645ec9e679a7b05fd28b6_img" src="resources/dynalite.png"/></div></div></div></div></div><div class="right" id="Right_I8a818b29954f45c38d83fd594acbcbb9"><div class="Image_Default" id="Id97abfff20124f73b3abd609fce41d80" style="display:block;clear: both;text-align:center;"><style>#Id97abfff20124f73b3abd609fce41d80_img {-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;width: 509px;border:none;max-width: 100%;height: auto;}</style><img id="Id97abfff20124f73b3abd609fce41d80_img" src="resources/KeyFob%201.png.opt509x201o0%2C0s509x201.png"/></div></div></div></div></div>
</div>
</div>
</div>
</div>
</main>
<div class="yola_footer_wrap">
<div class="yola_footer_column">
<footer id="yola_style_footer">
<p style="float:right; margin:0;">Ashfield, NSW 2131, Australia | 0439 078 503</p><div style="clear:both; height:0;"></div>
</footer>
</div>
</div>
<script data-id="8a4986c849763a4301497e78088f7694" data-partner="WL_NETREGISTRY" data-url="//analytics.yolacdn.net/tracking.js" data-user="3dcb968c86ba4272bff92d1752199429" id="site_analytics_tracking" type="text/javascript">
  var _yts = _yts || [];
  var tracking_tag = document.getElementById('site_analytics_tracking');
  _yts.push(["_siteId", tracking_tag.getAttribute('data-id')]);
  _yts.push(["_userId", tracking_tag.getAttribute('data-user')]);
  _yts.push(["_partnerId", tracking_tag.getAttribute('data-partner')]);
  _yts.push(["_trackPageview"]);
  (function() {
    var yts = document.createElement("script");
    yts.type = "text/javascript";
    yts.async = true;
    yts.src = document.getElementById('site_analytics_tracking').getAttribute('data-url');
    (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(yts);
  })();
</script>
<!-- Start of Google Analytics code configured in the Site Settings dialog -->
<script data-id="UA-118757979-1" id="ga_tracking" type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', document.getElementById('ga_tracking').getAttribute('data-id'), 'auto');  // Replace with your property ID.
  ga('send', 'pageview');
</script>
<!-- End of Google Analytics code configured in the Site Settings dialog -->
<!-- template: Skyline_v2 963d9aa5-111c-42ed-8aa6-86880ac6d143 -->
</div>
</div> <!-- .inner_bg_overlay -->
</div> <!-- #sys_background / .bg_overlay -->
<script src="templates/Skyline_v2/resources/js/browserify.build.js"></script>
</body>
</html>