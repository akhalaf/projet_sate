<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Advertile Mobile | Advertile Mobile</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:700,300,400" rel="stylesheet" type="text/css"/>
<link href="https://fontastic.s3.amazonaws.com/6LCNdWk3R3EgXL4kRnMP44/icons.css" rel="stylesheet"/>
<link href="css/app.min.css" rel="stylesheet"/>
<script src="js/vendor/modernizr.min.js"></script>
</head>
<body>
<!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
<div class="off-canvas-wrap" data-offcanvas="">
<div class="inner-wrap">
<a class="right-off-canvas-toggle menu-icon" href="#"><span></span></a>
<header class="header image-home full-image margin--bottom">
<div class="row fluid">
<a class="logo left" href="/">ADVERTILE MOBILE</a>
</div>
<div class="table-center">
<div class="row fluid">
<div class="large-8 large-offset-2 medium-10 medium-offset-1 columns">
<h1 class="headline">Meaningful rewards you love</h1>
<p class="lead">We enable people to get the things they love by completing small tasks in their spare time</p>
</div>
</div>
</div>
</header>
<!-- We are Advertile Mobile -->
<section>
<div class="row fluid">
<div class="margin--bottom large-8 large-offset-2 medium-10 medium-offset-1 columns">
<h2>We are Advertile Mobile</h2>
<p class="text--block">
					We create innovative apps to engage users around the world helping them get the things they love with their mobile device. Everyone has the opportunity to participate by completing small tasks and missions - everyday, everywhere.
				</p>
</div>
</div>
</section>
<!-- Building Products -->
<section class="margin--bottom bg-gray">
<div class="row fluid">
<div class="large-5 hide-for-small-only hide-for-medium-only columns">
<img alt="app" class="app" src="images/app.png"/>
</div>
<div class="build large-5 large-offset-0 medium-10 medium-offset-1 columns end">
<h2>The AppBounty App and all related services have been shut down.</h2>
<p class="text--block">
<em>
						If you are a data subject who would like to exercise your rights of data access or data deletion under Article 15 or 17 of the GDPR legislation, please be advised that all company data associated with the AppBounty product and services has already been deleted.
					</em>
</p>
</div>
</div>
</section>
<!-- Footer -->
<footer class="footer">
<div class="row fluid">
<div class="medium-3 columns hide-for-small-only">
<a class="logo left" href="/">ADVERTILE MOBILE</a>
</div>
<div class="medium-6 columns">
<ul class="footer-nav">
<li><a data-reveal-id="impressumModal" href="javascript:void(0)">Impressum</a></li>
</ul>
</div>
<div class="medium-3 columns socials">
</div>
</div>
<div class="reveal-modal large" data-reveal="" id="impressumModal">
<a class="close-reveal-modal">×</a>
<div class="wrapper">
<h3>Imprint</h3>
<h4>Fyber GmbH</h4>
<p>
					Office Address:<br/>
					Wallstraße 9-13<br/>
					10179 Berlin<br/>
					Germany<br/>
					Phone: +49 30-6098555-0<br/>
					Fax: +49 30-6098555-35<br/>
					Email: info@fyber.com
				</p>
<p>Managing Directors: Ziv Elul, Daniel Sztern, Yaron Zaltsman</p>
<p>
					Statutory seat: Amsterdam, The Netherlands,<br/>
					Kamer van Koophandel KvK-Nr. 54747805<br/>
					German Branch Office: Amtsgericht Charlottenburg, HRB 166541
				</p>
<p>
					VAT ID No.: DE289234185<br/>
					LEI: 894500D5B6A8E1W0VL50
				</p>
<p>
					Editorial responsibility for content under § 55 II RStV: Eva Sayre, Wallstraße 9-13, 10179 Berlin
				</p>
<h4>Disclaimer</h4>
<p>
					Fyber is not responsible for any content of third-party websites which can be accessed via links from Fyber’s website. Fyber does not control any of these sites and expressly dissociates itself from their content.
				</p>
<h4>Copyright</h4>
<p>
					All rights reserved. Reproduction of any content on Fyber’s website as well as storage and usage of such content on optical or electronic data carriers only upon prior written consent of Fyber. Unwarranted utilization of such content in whole or in part by third parties is strictly prohibited.
				</p>
</div>
</div>
</footer>
<a class="exit-off-canvas"></a>
</div>
</div>
<script src="js/vendor/libraries.min.js"></script>
<script src="js/vendor/foundation.min.js"></script>
<script src="js/app.min.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!-- <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script> -->
</body>
</html>
