<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-21414261-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-21414261-1');
</script>
<script src="https://www.googleoptimize.com/optimize.js?id=GTM-PS9MV5P"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title> BodBot: Personal Trainer, Nutritionist, and Coach. Free. </title>
<meta content="personal training, training, fitness, exercise, strength, weight lifting, strength training, diet, nutrition, personalized, health, cardiovascular" name="keywords"/>
<meta content="Free personalized training and nutrition plans to help you accomplish any fitness, athletic or health goal." name="description"/>
<link href="favicon.ico" rel="shortcut icon"/>
<!-- FACEBOOK METADATA -->
<meta content="BodBot" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.bodbot.com/Images/marketing/bbapplogo_100.png" property="og:image"/>
<meta content="http://www.bodbot.com" property="og:url"/>
<meta content="BodBot" property="og:site_name"/>
<meta content="Free personalized training and nutrition plans to help you accomplish any fitness, athletic or health goal." property="og:description"/>
<meta content="21063" property="fb:admins"/>
<link href="style20130810.css" rel="stylesheet" type="text/css"/>
<script src="https://static.bodbot.com/Scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
<meta content="JFR8womzlJvCAQBo9gFU-4K40dgLmVvrMEJQX3VsEXU" name="google-site-verification"/>
<link href="https://chrome.google.com/webstore/detail/ppnkdiaelidjhcebhmgemlpnghbdgjhk" rel="chrome-webstore-item"/>
</head>
<script type="text/javascript">



$(document).ready(function(){
	var sign_in_toggle = $('#sign-in-toggle');
	
	// Displaying the sign-in form
	$('#login-button').toggle(function() {
		$(sign_in_toggle).fadeIn('fast')
		$('#login_up_arrow').fadeIn('fast')
	},
	function(){
		$(sign_in_toggle).fadeOut('fast');
		$('#login_up_arrow').fadeOut('fast');
	})
	
	// Removing the sign-in-form
	$('#remove_exercise').click(function() {
		$(sign_in_toggle).fadeOut('fast');
		$('#login_up_arrow').fadeOut('fast');
	})
})

</script>
<style>
.feature_description{
	font-family:Segoe UI Light, Helvetica-Light, Myriad Pro, "Myriad Pro", Myriad, Univers, Calibri, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
</style>
<body id="front_page" style="background-color:#ffffff">
<!--FACEBOOK CONNECT -->
<div align="center" class="portal_menucontainer">
<div class="portal-primary-header">
<img height="36" src="Images/Logo_Large.png" style="position:absolute;left:5px;bottom:0px;" width="141"/> <div style="position:absolute;left:198px;bottom:-10px;">
<div class="primary-menu-button">
<a class="primary-menu-button-a" id="training_nav" onclick="scrollTo2('#training_features_block', '#training_nav')"> training</a>
</div>
<div class="primary-menu-button">
<a class="primary-menu-button-a" id="nutrition_nav" onclick="scrollTo2('#nutrition_features_block', '#nutrition_nav')"> nutrition</a>
</div>
<div class="primary-menu-button">
<a class="primary-menu-button-a" id="quotes_nav" onclick="scrollTo2('#quotes', '#quotes_nav')"> testimonials</a>
</div>
</div>
<div id="sign-in">
<div id="login-button"><button class="main_button light_button" id="portal-sign-in-button" name="login_submit">login</button></div>
<div id="sign-in-toggle">
<!--<div class="float-left"><a id="remove_exercise">&nbsp;</a></div>-->
<form action="https://www.bodbot.com/Scripts/login.php" id="login_form" method="post" name="login_form">
<table class="registration_rule_giver">
<tbody>
<div id="message_holder">
</div>
<tr class="label_cell">
<td>
<label class="sign-in" for="form_user_name"> Email</label><input id="time_zone_offset_holder" name="time_zone_offset_holder" type="hidden" value="0"/>
</td>
</tr>
<tr class="input_cell">
<td>
<input class="Registration_Input" id="login_email" name="login_email" type="text"/>
</td>
</tr>
<tr class="label_cell">
<td>
<label class="sign-in" for="form_user_name"> Password</label>
</td>
</tr>
<tr class="input_cell">
<td>
<input class="Registration_Input" id="password" name="login_password" type="password"/>
</td>
</tr>
<tr class="login-ancillaries" style="margin-top: -10px;">
<td>
<div class="float-left" style="margin-bottom: 10px; font-size: .7em;.em;.; margin-top: -10px; position:absolute\9;">
<a href="http://www.bodbot.com/Forgot_Password.html">Forgot my password</a>
</div>
<div class="float-right" style="font-size: .7em;.em;.; margin-top: -10px;">
<input id="Remember_Me" name="Remember_Me" type="checkbox"/>
<label> Remember Me </label>
</div>
<div class="float-right" style="margin-top: 10px;float:left\9;right:0\9;"><button class="main_button" id="portal-sign-in-button" name="login_submit" type="submit">login</button></div>
</td>
</tr>
</tbody>
</table>
</form>
<script type="text/javascript">
        $(function() {
			//get the user's timezone
			var d = new Date()
			var gmtHours = -d.getTimezoneOffset()/60;
			$('#time_zone_offset_holder').val(gmtHours);
			//alert("The local time zone is: GMT " + gmtHours);
        });
    </script>
</div>
<button class="main_button" id="sign_up_button" onclick="location.href='Register.html?s=front_page'">get bodbot</button> <div class="up_arrow" id="login_up_arrow"></div>
</div>
</div>
</div>
<div align="center" class="mainborder" style="overflow:hidden;">
<div>
<div id="feature_main_header_container" style="padding-top:120px;text-align:center">
<span id="feature_main_header" style="font-size:40px">
					AI Workouts <br/>
<span style="font-size:25px;display:inline-block;padding-top:10px">
						Step-by-Step Guidance - at Home or at the Gym<br/>
</span>
</span>
</div>
</div>
<div class="exercise_image" data-speed="4" data-type="background" style="background:url('https://static.bodbot.com/Images/Exercise_Visualizations_Banner201807.png') 50% 210px no-repeat fixed; min-height: 950px;background-size:60%;">
</div>
<div class="features_block" data-speed="10" data-type="background" id="training_features_block" style="height:auto;">
<div class="features_block_content">
<h1 style="color:white">Your Own Digital Personal Trainer</h1>
<div class="header_separator"></div>
<h2 style="color:white;text-align:left">Get results with workouts adapted to you, your goals, and your progress.</h2>
<div class="feature_images">
<div class="feature_block left_floated_text" id="goal_feature">
<div class="feature_description">
<h3>Achieve Your Goals</h3>
Whether you simply want to lose fat, gain muscle or be healthy – or you want to work on a specific sport or combination of goals – BodBot will tailor your workouts to your needs. BodBot will work with you to find an appropriate training volume, intensity, and frequency, and will select exercises and progressions to get you where you’re going efficiently. Why drive half the speed limit? 
						</div>
<img alt="custom workouts for different fitness goals" class="screenshot_img" height="322px" src="https://static.bodbot.com/Images/frontpage/goal_screenshot.png" width="425px"/>
<img alt="workout goals icon" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/goal.png" width="62px"/>
</div>
<div class="feature_block right_floated_text">
<div class="feature_description">
<h3>Adaptation with every rep</h3>
							There are no cookie-cutter plans here. If you blow-away the average rate of progress, or hit some hiccups in your training, BodBot's algorithms will adapt your workouts to your pace. Whether your body finds a pristine highway or goes 'off-road' - let BodBot be its navigator.  
						</div>
<img alt="workouts adapt based on your progress" class="screenshot_img" height="262px" src="https://static.bodbot.com/Images/frontpage/adaptation_screenshot.png" width="425px"/>
<img alt="custom workouts adaptation icon" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/adapt.png" width="62px"/>
</div>
<div class="feature_block left_floated_text">
<div class="feature_description">
<h3>Intelligent Workout Planning</h3>
							Whether you have unlimited time or just a few 20-30 minute blocks around the week, BodBot can fit your workouts into any combination. Busy weeks, but free weekends? Done. Half-hour on Monday, but an hour on Wednesday and Friday? No Problem. Miss a workout? Have a new 30 minute window? No problem, we'll adjust as necessary. 
						</div>
<img alt="flexible workout scheduling at the gym or at home" class="screenshot_img" height="425px" src="https://static.bodbot.com/Images/frontpage/schedule_screenshot.png" width="425px"/>
<img alt="workout schedule icon" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/schedule.png" width="62px"/>
</div>
<div class="feature_block right_floated_text">
<div class="feature_description">
<h3>Make Every Rep Count</h3>
							Use BodBot’s fitness tests to identify areas of opportunity and to reduce injury risk. From strength imbalances, to mobility and stability, to posture and flexibility, BodBot can provide immediate feedback and assistance. Just as you should periodically check the oil in your car, BodBot can help check your body's systems.  
						</div>
<img alt="personal fitness plans based on your strengths and weaknesses" class="screenshot_img" height="335px" src="https://static.bodbot.com/Images/frontpage/strengths_screenshot.png" style="margin-bottom:0px;" width="425px"/>
<img alt="body icon for tailored fitness plans" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/man.png" style="margin-bottom:0px;" width="62px"/>
</div>
<div class="feature_block"></div><!--Needed so the div extends to the full height of the child elements; weirdness with floating-->
</div>
</div>
</div>
<div class="exercise_image" data-speed="4" data-type="background" style="background:url('https://static.bodbot.com/Images/frontpage/exercise1.jpg') 50% 0 no-repeat fixed; min-height: 700px;top:600px;background-size:100%;">
</div>
<div class="features_block" data-speed="10" data-type="background" id="nutrition_features_block" style="height:auto;">
<div class="features_block_content">
<h1 style="color:white">Beyond Macronutrients</h1>
<div class="header_separator"></div>
<h2 style="color:white;text-align:left">The most advanced nutrition tools on the market.</h2>
<div class="feature_images">
<div class="feature_block left_floated_text" id="goal_feature">
<div class="feature_description">
<h3>Infinitely Customizable</h3>
							All of your nutrition targets are tailored to your specific goals and body. The nutrition requirements of gaining muscle, improving health, or optimizing athletic performance are all very different, and BodBot will recommend both macro and micronutrient targets accordingly. 
						</div>
<img alt="tailored macronutrient and micronutrient recommendations" class="screenshot_img" height="171px" src="https://static.bodbot.com/Images/frontpage/eat_smarter_screenshot.png" width="425px"/>
<img alt="custom nutrition recommendations based on your goal" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/goal.png" width="62px"/>
</div>
<div class="feature_block right_floated_text">
<div class="feature_description">
<h3>Your targets update with your workouts</h3>
							Complete a great workout? Need to refuel for next time? We'll help you consume enough carbohydrate to replenish muscle glycogen. Missed your planned session? No problem, we'll adjust your calories and other targets as necessary.
						</div>
<img alt="diet recommendations based on your workouts " class="screenshot_img" height="279px" src="https://static.bodbot.com/Images/frontpage/training_plus_nutrition_screenshot.png" width="425px"/>
<img alt="nutrition integrated with exercise" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/training_plus_nutrition.png" width="62px"/>
</div>
<div class="feature_block left_floated_text">
<div class="feature_description">
<h3>Track your meals seamlessly</h3>
							BodBot instantly remembers your tracked foods and meals. Record a frequently eaten meal with a single click, and reap the rewards of nutrition tracking faster and easier. 
						</div>
<img alt="meal tracking with a single click" class="screenshot_img" height="292px" src="https://static.bodbot.com/Images/frontpage/meals_screenshot.png" width="425px"/>
<img alt="faster calorie and nutrient tracking" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/meals.png" width="62px"/>
</div>
<div class="feature_block right_floated_text">
<div class="feature_description">
<h3>Deep Nutrition Insights</h3>
Based on your progress towards your nutrient targets, BodBot will recommend foods to help you fill deficiencies. Need to hit your protein and omega-3 fatty acid targets? Consider salmon or scallops. Looking for healthy fats and fiber? Give walnuts or almonds a look. No guesswork required, we’ll surface a list of foods that best meet your needs. 
						</div>
<img alt="foods by nutrients to help you reach your nutrition targets" class="screenshot_img" height="324px" src="https://static.bodbot.com/Images/frontpage/recommendations_screenshot.png" style="margin-bottom:0px;" width="425px"/>
<img alt="custom food recommendations" class="feature_img" height="62px" src="https://static.bodbot.com/Images/frontpage/icons/recommendations.png" style="margin-bottom:0px;" width="62px"/>
</div>
<div class="feature_block"></div><!--Needed so the div extends to the full height of the child elements; weirdness with floating-->
</div>
</div>
</div>
<div class="exercise_image" data-speed="4" data-type="background" style="background:url('https://static.bodbot.com/Images/frontpage/exercise3.jpg') 50% 0 no-repeat fixed; min-height: 700px;top:600px;background-size:100%;">
</div>
<div class="features_block" data-speed="10" data-type="background">
<div class="features_block_content">
<h1>Take BodBot with You</h1>
<div class="header_separator"></div>
<div class="feature_images" id="mobile_app_icons">
<div class="mobile_app_icon">
<img alt="BodBot Android 4.5 Star Rating" height="230px" src="https://static.bodbot.com/Images/frontpage/apps_optimized/android.png" width="150px"/>
</div>
<div class="mobile_app_icon">
<img alt="BodBot iPhone 5 Star Rating" height="230px" src="https://static.bodbot.com/Images/frontpage/apps_optimized/iphone.png" width="150px"/>
</div>
<div class="mobile_app_icon">
<img alt="BodBot Windows Phone 5 Star Rating " height="230px" src="https://static.bodbot.com/Images/frontpage/apps_optimized/windowsphone8.png" width="150px"/>
</div>
</div>
</div>
</div>
<div class="exercise_image" data-speed="4" data-type="background" style="background:url('https://static.bodbot.com/Images/frontpage/exercise4.jpg') 50% 67px no-repeat fixed; min-height: 700px;top:600px;background-size:100%;">
</div>
<div class="features_block" data-speed="10" data-type="background" id="quotes">
<div id="background_quotes" style="width:1300px">
<div class="background_quote quote_importance_3" style="margin-left:27px;margin-top:49px">“This website is brilliant...<br/>I’m so glad you made this,<br/>and I’m glad I found it.”</div>
<div class="background_quote quote_importance_1" style="margin-left:300px;margin-top:55px">“Love (and I mean LOVE)<br/>your website”</div>
<div class="background_quote quote_importance_1" style="margin-left:714px;margin-top:14px">“I love it.”</div>
<div class="background_quote quote_importance_1" style="margin-left:638px;margin-top:63px">“THIS IS GREAT!”</div>
<div class="background_quote quote_importance_3" style="margin-left:847px;margin-top:55px">“THANK YOU THANK YOU THANK YOU<br/>for creating this.<br/>It has saved my workouts.”</div>
<div class="background_quote quote_importance_3" style="margin-left:857px;margin-top:149px">“your website is the perfect thing for me.”</div>
<div class="background_quote quote_importance_3" style="margin-left:54px;margin-top:191px">“This is amazing.”</div>
<div class="background_quote quote_importance_1" style="margin-left:549px;margin-top:234px">“I love this app.”</div>
<div class="background_quote quote_importance_1" style="margin-left:831px;margin-top:222px">“i love it”</div>
<div class="background_quote quote_importance_2" style="margin-left:800px;margin-top:265px">“Ingenius.<br/>Whoever designed this app was brilliant.<br/>It’s intuitive, sleek, and easy on the battery.<br/>The creator of the site did a wonderful job as well.<br/>The information is soundwith a great way to customize your workout<br/>for any level of skill. A+ in my book, sirs or ma’am.”</div>
<div class="background_quote quote_importance_1" style="margin-left:0px;margin-top:250px">“This is unbelievable.<br/>This is exactly what I was looking for<br/>and I love the user friendliness<br/>and simple nature.”</div>
<div class="background_quote quote_importance_1" style="margin-left:402px;margin-top:301px">“Love it”</div>
<div class="background_quote quote_importance_1" style="margin-left:108px;margin-top:393px">“i love this program”</div>
<div class="background_quote quote_importance_3" style="margin-left:57px;margin-top:491px">“this site ROCKS.”</div>
<div class="background_quote quote_importance_2" style="margin-left:415px;margin-top:375px">“This application is<br/>literally a marvel”</div>
<div class="background_quote quote_importance_2" style="margin-left:314px;margin-top:502px">“this is seriously the best thing ever...<br/>I will show everyone I know this -<br/>seriously this is EXACTLY what I needed.”</div>
<div class="background_quote quote_importance_2" style="margin-left:831px;margin-top:490px">“This site is awesome!<br/>It is exactly what I have been looking for.<br/>i would consider it genius for the guys who created it.”</div>
</div>
<div class="features_block_content">
<div class="quote_container" style="margin-top:70px;">
<div class="quote">“THANK YOU<br/>THANK YOU<br/>THANK YOU”</div>
<div class="quote_name">- Ryan15</div>
</div>
<div class="quote_container" style="width:400px;margin-top:100px;margin-left:70px;">
<div class="quote">“This application tops any other workout app and not only beats them but totally blows them out of the water.”</div>
<div class="quote_name">- fizzy518</div>
</div>
<div class="quote_container" style="margin-top:150px;margin-left:90px;margin-bottom:50px;">
<div class="quote">“Genius.”</div>
<div class="quote_name">- D. Harris</div>
</div>
<div class="quote_container" style="margin-top:100px;margin-left:70px;">
<div class="quote">“This is a<br/>godsend.”</div>
<div class="quote_name">- Brian L.</div>
</div>
<div class="quote_container" style="width:500px;margin-top:30px;margin-left:230px;">
<div class="quote">“THANK YOU!<br/>This is what I have needed for so long. I’ve always been willing to work out, but have floundered with trying to plan a workout. This is perfect!!<br/>THANK YOU!!”</div>
<div class="quote_name">- H_Blackstone</div>
</div>
</div>
</div>
<div class="features_block" data-speed="10" data-type="background" id="features_sign_up">
<div class="features_block_content">
<button class="main_button" onclick="location.href='Register.html?s=front_page'">get bodbot</button>
				(it's free!)
			</div>
</div>
<div style="background-color:#999;width:100%;height:80px;"><div id="footer">
<div style="width:960px;text-align:right;padding-bottom:15px;margin-top:15px;margin-bottom:0px;font-size:12px;position:relative">
<a class="primary_link_style" href="https://www.bodbot.com/About.html"> About</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Testimonials.html"> Testimonials</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Blog.html"> Blog</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Help.html"> Help</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Privacy.html"> Privacy</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Terms.html"> Terms</a> -
		<a class="primary_link_style" href="mailto:information@bodbot.com"> Contact</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Exercise_Library.html"> Exercise Library</a> - 
		<a class="primary_link_style" href="https://www.bodbot.com/Workout_Generator.html"> Workout Generator</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Food_Database.html"> Food Database</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Food_Explorer.html"> Food Explorer</a> -
		<a class="primary_link_style" href="https://www.bodbot.com/Personalize_Your_Workouts.html"> Workout Planner</a> - 
		
		<span> 
			© 2020 BodBot, LLC
		</span>
</div>
</div>
</div></div>
<script type="text/javascript">

function scrollTo2(location, nav_item){
	console.log('scrollTo location: '+location+', nav item: '+nav_item)
	$("#nav-main-current-page").removeAttr('id');
	$(nav_item).parent().attr('id', 'nav-main-current-page');
	$('html, body').animate({ scrollTop: $(location).offset().top - 67 }, 1000);
}
</script>
</body>
</html>
