<!DOCTYPE html>
<html lang="sv-SE">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<script type="text/javascript"> var epic_ad_ajax_url = "https://roliga.nu/?ajax-request=epic_ad";</script>
<!-- This site is optimized with the Yoast SEO plugin v15.3 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Sidan finns inte - Roliga.nu</title>
<meta content="noindex, follow" name="robots"/>
<meta content="sv_SE" property="og:locale"/>
<meta content="Sidan finns inte - Roliga.nu" property="og:title"/>
<meta content="Roliga.nu" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://roliga.nu/#website","url":"https://roliga.nu/","name":"Roliga.nu","description":"Ditt skratt i vardagen","potentialAction":[{"@type":"SearchAction","target":"https://roliga.nu/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"sv-SE"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="https://roliga.nu/feed/" rel="alternate" title="Roliga.nu » flöde" type="application/rss+xml"/>
<link href="https://roliga.nu/comments/feed/" rel="alternate" title="Roliga.nu » kommentarsflöde" type="application/rss+xml"/>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://roliga.nu/wp-content/themes/contentberg/style.css?ver=1.8.2" id="contentberg-core-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.5.3" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-content/plugins/epic-ad/assets/css/style.css?ver=1.0.0" id="epic-ad-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto%3A400%2C500%2C700%7CPT+Serif%3A400%2C400i%2C600%7CIBM+Plex+Serif%3A500" id="contentberg-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-content/themes/contentberg/css/lightbox.css?ver=1.8.2" id="contentberg-lightbox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-content/themes/contentberg/css/fontawesome/css/font-awesome.min.css?ver=1.8.2" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://roliga.nu/wp-content/themes/contentberg-child/style.css?ver=5.5.3" id="contentberg-child-css" media="all" rel="stylesheet" type="text/css"/>
<script id="cookie-notice-front-js-extra" type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxUrl":"https:\/\/roliga.nu\/wp-admin\/admin-ajax.php","nonce":"d5f165a330","hideEffect":"fade","position":"bottom","onScroll":"0","onScrollOffset":"100","onClick":"0","cookieName":"cookie_notice_accepted","cookieTime":"2592000","cookieTimeRejected":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"0","cache":"1","refuse":"0","revokeCookies":"0","revokeCookiesOpt":"automatic","secure":"1","coronabarActive":"0"};
/* ]]> */
</script>
<script id="cookie-notice-front-js" src="https://roliga.nu/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.2" type="text/javascript"></script>
<script id="jquery-core-js" src="https://roliga.nu/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="lazysizes-js" src="https://roliga.nu/wp-content/themes/contentberg/js/lazysizes.js?ver=1.8.2" type="text/javascript"></script>
<link href="https://roliga.nu/wp-json/" rel="https://api.w.org/"/><link href="https://roliga.nu/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://roliga.nu/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<script>var Sphere_Plugin = {"ajaxurl":"https:\/\/roliga.nu\/wp-admin\/admin-ajax.php"};</script><meta content="_IGtHPWc_oU1dNXobbLzQdagXJF1x_JUvWeGy9m4_SY" name="google-site-verification"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-112268247-30"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112268247-30');
</script><style data-type="jeg_custom-css" id="jeg_dynamic_css" type="text/css"></style>
</head>
<body class="error404 cookies-not-set right-sidebar lazy-normal has-lb">
<div class="main-wrap">
<header class="main-head head-nav-below has-search-modal simple simple-boxed" id="main-head">
<div class="inner inner-head" data-sticky-bar="0">
<div class="wrap cf wrap-head">
<div class="left-contain">
<span class="mobile-nav"><i class="fa fa-bars"></i></span>
<div class="title">
<a href="https://roliga.nu/" rel="home" title="Roliga.nu">
<img alt="Roliga.nu" class="logo-image" src="https://roliga.nu/wp-content/uploads/2019/02/roliga-logo-wide-r1.png"/>
</a>
</div>
</div>
<div class="navigation-wrap inline">
<nav class="navigation inline simple light" data-sticky-bar="0">
<div class="menu-sidhuvud-container"><ul class="menu" id="menu-sidhuvud"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-2 menu-item-11" id="menu-item-11"><a href="https://roliga.nu/bilder/"><span>Bilder</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-15 menu-item-33" id="menu-item-33"><a href="https://roliga.nu/djur/"><span>Djur</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-14 menu-item-26" id="menu-item-26"><a href="https://roliga.nu/klipp/"><span>Klipp</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-4 menu-item-192" id="menu-item-192"><a href="https://roliga.nu/skamt/"><span>Skämt</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-10 menu-item-193" id="menu-item-193"><a href="https://roliga.nu/citat/"><span>Texter</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-7 menu-item-191" id="menu-item-191"><a href="https://roliga.nu/historier/"><span>Historier</span></a></li>
</ul></div> </nav>
</div>
<div class="actions">
<a class="search-link" href="#" title="Search"><i class="fa fa-search"></i></a>
</div>
</div>
</div>
</header> <!-- .main-head -->
<div class="main wrap">
<div class="ts-row cf">
<div class="col-12 main-content cf">
<div class="the-post the-page page-404 cf">
<header class="post-title-alt">
<h1 class="main-heading">Page Not Found!</h1>
</header>
<div class="post-content error-page row">
<div class="col-3 text-404 main-color">404</div>
<div class="col-8 post-content">
<p>
					We're sorry, but we can't find the page you were looking for. It's probably some thing we've done wrong but now we know about it and we'll try to fix it. In the meantime, try one of these options:					</p>
<ul class="links fa-ul">
<li> <a class="go-back" href="#">Föregående</a></li>
<li> <a href="https://roliga.nu">Go to Homepage</a></li>
</ul>
<form action="https://roliga.nu/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Type and hit enter..." title="Search for:" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><i class="fa fa-search"></i></button>
</form>
</div>
</div>
</div>
</div> <!-- .main-content -->
</div> <!-- .ts-row -->
</div> <!-- .main -->
<footer class="main-footer bold bold-light">
<section class="upper-footer">
<div class="wrap">
<ul class="widgets ts-row cf">
<li class="widget column col-4 widget_nav_menu" id="nav_menu-3"><div class="menu-sidfot-container"><ul class="menu" id="menu-sidfot"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-482" id="menu-item-482"><a href="https://roliga.nu/integritetspolicy/">Integritetspolicy</a></li>
</ul></div></li> </ul>
</div>
</section>
<section class="lower-footer cf">
<div class="wrap">
<ul class="social-icons">
</ul>
<p class="copyright">© 2019 Roliga.nu				</p>
<div class="to-top">
<a class="back-to-top" href="#"><i class="fa fa-angle-up"></i> Tillbaka till toppen</a>
</div>
</div>
</section>
</footer>
</div> <!-- .main-wrap -->
<div class="mobile-menu-container off-canvas" id="mobile-menu">
<a class="close" href="#"><i class="fa fa-times"></i></a>
<div class="logo">
</div>
<ul class="mobile-menu"></ul>
</div>
<div class="search-modal-wrap">
<div aria-modal="true" class="search-modal-box" role="dialog">
<form action="https://roliga.nu/" class="search-form" method="get">
<input class="search-field" name="s" placeholder="Search..." required="" type="search" value=""/>
<button class="search-submit visuallyhidden" type="submit">Submit</button>
<p class="message">
			Type above and press <em>Enter</em> to search. Press <em>Esc</em> to cancel.		</p>
</form>
</div>
</div>
<script id="epic-ad-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var epicAdConfig = [];
/* ]]> */
</script>
<script id="epic-ad-script-js" src="https://roliga.nu/wp-content/plugins/epic-ad/assets/js/script.js?ver=1.0.0" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://roliga.nu/wp-content/themes/contentberg/js/magnific-popup.js?ver=1.8.2" type="text/javascript"></script>
<script id="jquery-fitvids-js" src="https://roliga.nu/wp-content/themes/contentberg/js/jquery.fitvids.js?ver=1.8.2" type="text/javascript"></script>
<script id="imagesloaded-js" src="https://roliga.nu/wp-includes/js/imagesloaded.min.js?ver=4.1.4" type="text/javascript"></script>
<script id="object-fit-images-js" src="https://roliga.nu/wp-content/themes/contentberg/js/object-fit-images.js?ver=1.8.2" type="text/javascript"></script>
<script id="contentberg-theme-js-extra" type="text/javascript">
/* <![CDATA[ */
var Bunyad = {"custom_ajax_url":"\/wp-content\/themes\/.a\/%09"};
/* ]]> */
</script>
<script id="contentberg-theme-js" src="https://roliga.nu/wp-content/themes/contentberg/js/theme.js?ver=1.8.2" type="text/javascript"></script>
<script id="theia-sticky-sidebar-js" src="https://roliga.nu/wp-content/themes/contentberg/js/theia-sticky-sidebar.js?ver=1.8.2" type="text/javascript"></script>
<script id="jquery-slick-js" src="https://roliga.nu/wp-content/themes/contentberg/js/jquery.slick.js?ver=1.8.2" type="text/javascript"></script>
<script id="jarallax-js" src="https://roliga.nu/wp-content/themes/contentberg/js/jarallax.js?ver=1.8.2" type="text/javascript"></script>
<script id="masonry-js" src="https://roliga.nu/wp-includes/js/masonry.min.js?ver=4.2.2" type="text/javascript"></script>
<script id="jquery-masonry-js" src="https://roliga.nu/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2b" type="text/javascript"></script>
<script id="wp-embed-js" src="https://roliga.nu/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<!-- Cookie Notice plugin v1.3.2 by Digital Factory https://dfactory.eu/ -->
<div aria-label="Cookie Notice" class="cookie-notice-hidden cookie-revoke-hidden cn-position-bottom" id="cookie-notice" role="banner" style="background-color: rgba(0,0,0,1);"><div class="cookie-notice-container" style="color: #fff;"><span class="cn-text-container" id="cn-notice-text">Vi använder cookies för att se till att vi ger dig den bästa upplevelsen på vår webbplats. Om du fortsätter att använda denna webbplats kommer vi att anta att du godkänner detta.</span><span class="cn-buttons-container" id="cn-notice-buttons"><a aria-label="GODKÄNN" class="cn-set-cookie cn-button bootstrap" data-cookie-set="accept" href="#" id="cn-accept-cookie">GODKÄNN</a><a aria-label="Integritetspolicy" class="cn-more-info cn-button bootstrap" href="https://roliga.nu/integritetspolicy/" id="cn-more-info" target="_blank">Integritetspolicy</a></span><a aria-label="GODKÄNN" class="cn-close-icon" data-cookie-set="accept" href="javascript:void(0);" id="cn-close-notice"></a></div>
</div>
<!-- / Cookie Notice plugin -->
</body>
</html>