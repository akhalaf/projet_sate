<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="it-IT">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="it-IT">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="it-IT">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="it-IT">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Pagina non trovata - Ruotando</title>
<link href="https://www.ruotando.info/xmlrpc.php" rel="pingback"/>
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
	<script src="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/js/html5.js" type="text/javascript"></script>
	<![endif]-->
<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
<!-- This site is optimized with the Yoast SEO plugin v15.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex, follow" name="robots"/>
<meta content="it_IT" property="og:locale"/>
<meta content="Pagina non trovata - Ruotando" property="og:title"/>
<meta content="Ruotando" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.ruotando.info/#website","url":"https://www.ruotando.info/","name":"Ruotando","description":"Art&amp;Show","potentialAction":[{"@type":"SearchAction","target":"https://www.ruotando.info/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"it-IT"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.ruotando.info/feed/" rel="alternate" title="Ruotando » Feed" type="application/rss+xml"/>
<link href="https://www.ruotando.info/comments/feed/" rel="alternate" title="Ruotando » Feed dei commenti" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.ruotando.info\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<meta content="Ruotando v.1.6" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.ruotando.info/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.5.3" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,700,800|Raleway:400,200,100,500,700,800,900&amp;subset=latin,latin-ext" id="nexus-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/style.css?ver=5.5.3" id="nexus-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/epanel/shortcodes/css/shortcodes.css?ver=3.0" id="et-shortcodes-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/epanel/shortcodes/css/shortcodes_responsive.css?ver=3.0" id="et-shortcodes-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/epanel/page_templates/js/fancybox/jquery.fancybox-1.3.4.css?ver=1.3.4" id="fancybox-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/epanel/page_templates/page_templates.css?ver=1.8" id="et_page_templates-css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.ruotando.info/wp-content/plugins/lightbox-plus/css/shadowed/colorbox.min.css?ver=2.7.2" id="lightboxStyle-css" media="screen" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.ruotando.info/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="cookie-notice-front-js-extra" type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxUrl":"https:\/\/www.ruotando.info\/wp-admin\/admin-ajax.php","nonce":"7e7ceafe92","hideEffect":"fade","position":"bottom","onScroll":"0","onScrollOffset":"100","onClick":"0","cookieName":"cookie_notice_accepted","cookieTime":"2592000","cookieTimeRejected":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"0","cache":"0","refuse":"0","revokeCookies":"0","revokeCookiesOpt":"automatic","secure":"1","coronabarActive":"0"};
/* ]]> */
</script>
<script id="cookie-notice-front-js" src="https://www.ruotando.info/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.3.2" type="text/javascript"></script>
<link href="https://www.ruotando.info/wp-json/" rel="https://api.w.org/"/><link href="https://www.ruotando.info/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.ruotando.info/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/> <style>
		a { color: #4bb6f5; }

		body { color: #333333; }

		#top-menu li.current-menu-item > a, #top-menu > li > a:hover, .meta-info, .et-description .post-meta span, .categories-tabs:after, .home-tab-active, .home-tab-active:before, a.read-more, .comment-reply-link, h1.post-heading, .form-submit input, .home-tab-active:before, .et-recent-videos-wrap li:before, .nav li ul, .et_mobile_menu, #top-menu > .sfHover > a { background-color: #a8b400; }

		.featured-comments span, #author-info strong, #footer-bottom .current-menu-item a, .featured-comments span { color: #a8b400; }
		.entry-content blockquote, .widget li:before, .footer-widget li:before, .et-popular-mobile-arrow-next { border-left-color: #a8b400; }

		.et-popular-mobile-arrow-previous { border-right-color: #a8b400; }

		#top-menu > li > a { color: #333333; }

		#top-menu > li.current-menu-item > a, #top-menu li li a, .et_mobile_menu li a { color: #ffffff; }

		</style>
</head>
<body class="error404 cookies-not-set unknown et_includes_sidebar">
<header id="main-header">
<div class="container">
<div class="clearfix" id="top-info">
<a href="https://www.ruotando.info/">
<img alt="Ruotando" id="logo" src="https://www.ruotando.info/wp-content/themes/Ruotando%20Art%26Show/images/logo.png"/>
</a>
<div style="float:right; margin-bottom: -1px; padding-top: 80px; text-align:center;">
</div>
</div>
<div class="clearfix" id="top-navigation">
<div id="et_mobile_nav_menu"><a class="mobile_nav closed" href="#">Navigation Menu<span class="et_mobile_arrow"></span></a></div>
<nav>
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3582" id="menu-item-3582"><a href="https://www.ruotando.info/chi-siamo/">Chi siamo</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3585" id="menu-item-3585"><a href="http://www.ruotando.info/il-progetto/">Il progetto</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3571" id="menu-item-3571"><a href="https://www.ruotando.info/category/eventi/">Eventi</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3553" id="menu-item-3553"><a href="http://www.ruotando.info/artisti">Artisti</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3588" id="menu-item-3588"><a href="https://www.ruotando.info/cosa-offriamo/">Cosa offriamo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3589" id="menu-item-3589"><a href="https://www.ruotando.info/nel-mondo/">Nel mondo</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3551" id="menu-item-3551"><a href="https://www.ruotando.info/category/news/">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3693" id="menu-item-3693"><a href="http://www.ruotando.info/vuoi-partecipare/">Partecipa</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4043" id="menu-item-4043"><a href="https://www.ruotando.info/ruotando-e-le-aziende/">Ruotando e le Aziende</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3581" id="menu-item-3581"><a href="https://www.ruotando.info/contatti/">Contatti</a></li>
</ul> </nav>
<ul id="et-social-icons">
<li class="twitter">
<a href="#">
<span class="et-social-normal">Follow us on Twitter</span>
<span class="et-social-hover"></span>
</a>
</li>
<li class="facebook">
<a href="#">
<span class="et-social-normal">Follow us on Facebook</span>
<span class="et-social-hover"></span>
</a>
</li>
</ul>
</div> <!-- #top-navigation -->
</div> <!-- .container -->
</header> <!-- #main-header -->
<div class="page-wrap container">
<div id="main-content">
<div class="main-content-wrap clearfix">
<div id="content">
<div id="breadcrumbs">
<span class="et_breadcrumbs_content">
<a class="breadcrumbs_home" href="https://www.ruotando.info">Home</a> <span class="raquo">»</span>
</span> <!-- .et_breadcrumbs_content --> </div> <!-- #breadcrumbs -->
<div id="left-area">
<div class="recent-post">
<div class="entry">
<!--If no results are found-->
<h1>No Results Found</h1>
<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
</div>
<!--End if no results are found--> </div> <!-- end .recent-post -->
</div> <!-- end #left-area -->
</div> <!-- #content -->
<div id="sidebar">
<div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-2"><h4 class="widgettitle">Partners</h4> <div class="et-centered-ad">
<a href="http://linear4ciak.it/"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2015/10/linear4ciak.png" title="Linear4Ciak"/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-10"> <div class="et-centered-ad">
<a href="http://www.spaziocinema.info/" target="_blank"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2016/11/Anteo-spazioCinema.png" title=""/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-3"> <div class="et-centered-ad">
<a href="http://www.youfitpalestre.it/"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2015/09/youfit.png" title="YouFit Palestre"/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-4"> <div class="et-centered-ad">
<a href="http://www.laverdi.org/italian/auditorium.php" target="_blank"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2015/11/laverdi-logo2.png" title="Auditorium Milano Fondazione Cariplo Largo Mahler"/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-5"> <div class="et-centered-ad">
<a href="http://enricorizzi.com/" target="_blank"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2016/01/Enrico-Rizzi.png" title=""/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-6"><h4 class="widgettitle">Con il patrocinio di:</h4> <div class="et-centered-ad">
<a href="http://it.expoincitta.com/Calendario/Appuntamenti/Vibrazioni.kl" target="_blank"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2016/05/MilanoExpo-orizz.png" title=""/></a>
</div>
</div> <!-- end .widget --><div class="widget widget_etcenteredadwidget" id="etcenteredadwidget-9"> <div class="et-centered-ad">
<a href="" target="_blank"><img alt="" src="http://www.ruotando.info/wp-content/uploads/2016/01/patrocinio-regione-lombardia-1.png" title=""/></a>
</div>
</div> <!-- end .widget --> </div> <!-- end #sidebar -->
</div> <!-- .main-content-wrap -->
</div> <!-- #main-content -->
</div></body></html>