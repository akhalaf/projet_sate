<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/> <title>
		
		 DOCTOR KSA : 
		Medical vacancy | Medical events | Doctors jobs - Doctor KSA	</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="mLa-u-MlZvsBaS6DU7D9K2R5gSEpiLnuIUz3OKFEGW0" name="google-site-verification"/>
<title>:: DOCTOR KSA ::</title>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/><link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/><link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/><link href="/css/style.css" rel="stylesheet" type="text/css"/><link href="/css/datepicker.css" rel="stylesheet" type="text/css"/><link href="/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://www.doctorksa.com/js/bootstrap.min.js"></script>
</head>
<body>
<!-- Header Section Start -->
<header>
<div class="container prelative">
<div class="row">
<div class="top_signinsec clearfix">
<ul class="clearfix">
<li><a class="login" href="https://www.doctorksa.com/users/login">Login</a></li>
<li><a class="reg" href="https://www.doctorksa.com/users/signup">Registration</a></li>
</ul>
</div>
<div class="logo"><a href="https://www.doctorksa.com"><img src="https://www.doctorksa.com/img/logo.png"/></a></div>
<div class="header-right clearfix">
<div class="toplink_menu clearfix">
<ul class="top_menu clearfix">
<li><a href="https://www.doctorksa.com/Pages/display/about-doctorksa-the-medical-resource-provider-in-saudi-arabia-5">About Us</a></li>
<li><a href="https://www.doctorksa.com/Pages/display/contact-doctorksa">Contact Us</a></li>
<li><a href="https://www.doctorksa.com/surveys/">Surveys</a></li>
<li><a href="https://www.doctorksa.com/Pages/faq">FAQ's</a></li>
<li><a href="https://www.doctorksa.com/Pages/links/78/anesthesia">Links</a></li>
</ul>
<ul class="header_social clearfix">
<li><a href="http://www.facebook.com/pages/Doctorksa/128422235982" target="_blank"><img src="https://www.doctorksa.com/img/icon_fb.png"/></a></li>
<li><a href="http://www.twitter.com/doctorksa" target="_blank"><img src="https://www.doctorksa.com/img/icon_twitter.png"/></a></li>
<li><a href="https://www.linkedin.com/company/doctorksa/notifications?goback=&amp;trk=hb_ntf_AGGREGATED_COMPANY" target="_blank"><img src="https://www.doctorksa.com/img/icon_linkedin.png"/></a></li>
<li><a href="https://www.pinterest.com/doctorksa0467/" target="_blank"><img src="https://www.doctorksa.com/img/icon_pinintrest.png"/></a></li>
<li><a href="https://www.instagram.com/doctorksa_medical/" target="_blank"><img src="https://www.doctorksa.com/img/icon_instagram.png"/></a></li>
<li><a href="https://plus.google.com/b/104936602605927078191/104936602605927078191/posts" target="_blank"><img src="https://www.doctorksa.com/img/icon_googleplus.png"/></a></li>
</ul>
</div>
</div>
</div>
</div>
</header><!-- Header Section End -->
<!-- Navigation Section Start -->
<section class="nav_sec">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 nopadding">
<nav class="navbar navbar-inverse right_nav">
<div class="navbar-header">
<button class="navbar-toggle " data-target="#myNavbar" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="collapse navbar-collapse " id="myNavbar">
<ul class="nav navbar-nav">
<li><a href="https://www.doctorksa.com">Home</a></li>
<li><a href="https://www.doctorksa.com/Pages/news">News</a></li>
<li><a href="https://www.doctorksa.com/Downloads/slides">Medical Slides</a></li>
<li><a href="https://www.doctorksa.com/Pages/articles">Articles</a></li>
<li><a href="https://www.doctorksa.com/events/event_list">Medical Events</a></li>
<li><a href="https://www.doctorksa.com/jobs/jobslist">Medical Jobs</a></li>
<li><a href="https://www.doctorksa.com/jobs/doctorslist">Doctor Profile</a></li>
</ul>
<form accept-charset="utf-8" class="form-horizontal" data-toggle="validator" id="form_serc" method="post" role="form"><div style="display:none;"><input name="_method" type="hidden" value="POST"/></div>
<div class="searchbox_area">
<input id="search_keyword" name="keyword" placeholder="Search" required="" type="text"/>
<div class="search_icon">
<input class="deepsearch" onclick="jsfunction()" src="https://www.doctorksa.com/img/icon_search.png" type="button"/>
</div>
</div>
</form>
<script type="text/javascript">
	function jsfunction(){

		var newurl;
		var value1= $('#search_keyword').val();



		newurl = 'https://www.doctorksa.com'+'/Pages/search/key:'+value1;

		window.location=newurl;
	}
</script>
<style>
.deepsearch{ background: rgba(0, 0, 0, 0) url("https://www.doctorksa.com/img/icon_search.png") no-repeat scroll 0 0;
    border: medium none;
    width: 20px;}
</style> </div>
</nav>
</div>
</div>
</div>
</section>
<!-- Navigation Section End -->
<!-- Banner Section Start -->
<div class="banner_section">
<div class="container clearfix " style="margin-bottom:-53px;overflow: hidden;">
<div class="top_slider">
<ul class="bxslider clearfix">
<li>
<div class="banner_doct"><a border="0" href="javascript:void(0);" target="_blank"><img src="https://www.doctorksa.com/img/banners/1464088438_homepage banner2A.png"/></a></div>
<div class="banner_about">
<h2>DoctorKSA</h2>
<p>is a web based medical advertising service that is becoming increasingly popular for providing medically specialized recruitment and medical event management services. 
</p>
<div class="aboutimgsec clearfix">
<a href="#"> </a>
<a href="#"> </a>
</div>
<div class="aboutregsec clearfix">
<div class="left_regtext">
<span class="datetxt">Join us for recent Medical jobs, Medical events, Medical Directory Service Medical News &amp; Educational Material Service,  and Advertising &amp; Medical Surveys</span> </div>
</div>
</div>
<div class="clearfix"></div>
<div class="web_strip" style="position:relative; z-index:9;">
<div class="container ">
<div class="wstrip_left"><img height="34px;" src="https://www.doctorksa.com/img/banners/1464028617_DoctorKSA_1.jpg"/></div>
<div class="website_addresslink"></div>
</div>
</div><!-- /.web_strip --> </li>
</ul>
</div>
</div>
<div class="web_strip" style="height:53px;"></div> <!-- /.web_strip -->
</div>
<!-- Banner Section end -->
<!-- Services Section Start -->
<section class="services">
<div class="container ">
<div class="ourservices clearfix">
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="serv_box">
<div class="serv_img"><a href="https://www.doctorksa.com/jobs/jobslist"><img src="img/serv_img1.jpg"/></a></div>
<div class="serv_greybox">
<h2 class="servtitle">Latest Medical Jobs</h2>
<div class="service_contentbox scroll-pane">
<p><a href="https://www.doctorksa.com/jobs/jobsdetail/5337">Neuropathology Consultant				<span>01-12-2021</span> </a>
</p>
<p><a href="https://www.doctorksa.com/jobs/jobsdetail/5336">Nuclear Medicine Asst. Sub-Specialty Consultant				<span>01-12-2021</span> </a>
</p>
<p><a href="https://www.doctorksa.com/jobs/jobsdetail/5335">Spine Surgery Assistant Consultant				<span>01-12-2021</span> </a>
</p>
<p><a href="https://www.doctorksa.com/jobs/jobsdetail/5334">Neurology Assistant Consultant &amp;  Consultant (Adult and Pediatric)				<span>01-12-2021</span> </a>
</p>
<p><a href="https://www.doctorksa.com/jobs/jobsdetail/5333">Neurophysiology Consultant				<span>01-12-2021</span> </a>
</p>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="serv_box">
<div class="serv_img"><a href="https://www.doctorksa.com/Pages/our_clients"><img src="img/serv_img2.jpg"/></a></div>
<div class="serv_greybox">
<h2 class="servtitle">Our partners/clients</h2>
<div class="service_contentbox align_cen">
<ul class="bxslider">
<li><a href="https://mco.ae/" onclick="return tarc('10')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1552631116_MCO Logo-Transparent-01.png" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('11')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472985038_king fahad.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('12')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472985434_Saad Hospital.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('23')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472985572_National Guard.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('24')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472986071_Suleman Al Habib.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('25')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472994196_download.png" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('26')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472988449_National Medical Care.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('27')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472989308_Royal Commission Jubail Hospital.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('28')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1473085915_Health Oasis Hospital.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('29')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1473086349_New mowasat hospital.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('30')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472991208_Al moosa Hospital.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('31')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472991765_Al Ahsa Hospital.jpg" width="198"/></a></li>
<li><a href="http://kr.net.sa/v1/" onclick="return tarc('32')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1569269042_logoF.jpg" width="198"/></a></li>
<li><a href="http://www.doctorksa.com/Pages/our_clients" onclick="return tarc('33')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1472996731_samir abbas hospital.jpg" width="198"/></a></li>
<li><a href="https://www.doctorksa.com/Pages/our_clients" onclick="return tarc('65')" target="_blank"><img height="191" src="https://www.doctorksa.com/img/banners/1599072386_EVENT TROOP logo (1).jpg" width="198"/></a></li>
<!--<li><a href="#"><img src="img/ssgmo_logo2.jpg"></a></li>
						<li><a href="#"><img src="img/ssgmo_logo3.jpg"></a></li>-->
</ul>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="serv_box">
<div class="serv_img"><a href="#"><img src="img/serv_img3.jpg"/></a></div>
<div class="serv_greybox">
<h2 class="servtitle">Post a Job/Event</h2>
<div class="service_contentbox scroll-pane" style="width:235px;">
<p style="width:235px;">To post a job:
				<span>email: <a href="mailto:recruitment@doctorksa.com">recruitment@doctorksa.com</a></span>
<span>or call: 00966547204620</span>
</p>
<p style="width:235px;">To post an event:
				<span>email: <a href="mailto:amjad.khan@doctorksa.com">amjad.khan@doctorksa.com</a></span>
<span>or call: 00966547204620</span>
</p>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="serv_box">
<div class="serv_img"><a href="https://www.doctorksa.com/events/event_list"><img src="img/serv_img4.jpg"/></a></div>
<div class="serv_greybox">
<h2 class="servtitle">Latest Medical Events</h2>
<div class="service_contentbox scroll-pane">
<p><a href="https://www.doctorksa.com/events/event_detail/3449/اون لاين كورس اول برنامج تجميل متكامل">اون لاين كورس اول برنامج تج�..
				<span>09-24-2020</span></a>
</p>
<p><a href="https://www.doctorksa.com/events/event_detail/1510/SASEM 6th Scientific Assembly  2020">SASEM 6th Scientific Assembly  2020..
				<span>02-07-2021</span></a>
</p>
<p><a href="https://www.doctorksa.com/events/event_detail/3310/The Arab League of Associations for Rheumatology Congress for year 2021  ">The Arab League of Associations for Rheumatology C..
				<span>03-03-2021</span></a>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Services Section End -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Footer Section Start -->
<script type="text/javascript">
function docapt()
{
	jQuery.ajax({
		type: 'POST',
		url:  'https://www.doctorksa.com/Pages/capta',
		dataType: 'json',
		success: function(data){ 				
		$(".abc_url_cap").attr('src', 'https://www.doctorksa.com'+data.captcha_url);
		$("#cap_cd").val(data.captcha.code);		
		}			
	});
	return false;
}
</script>
<footer>
<div class="container">
<div class="row clearfix">
<div class="col-xs-12 col-sm-3 col-md-3">
<div class="footer_list">
<ul>
<li><a href="https://www.doctorksa.com">Home</a></li>
<li><a href="https://www.doctorksa.com/Pages/news">News</a></li>
<li><a href="https://www.doctorksa.com/Downloads/slides">Medical Slides</a></li>
<li><a href="https://www.doctorksa.com/Pages/articles">Articles</a></li>
<li><a href="https://www.doctorksa.com/events/event_list">Medical Events</a></li>
<li><a href="https://www.doctorksa.com/jobs/jobslist">Medical Jobs</a></li>
<li><a href="https://www.doctorksa.com/jobs/doctorslist">Doctor Profile</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-2 col-md-2">
<div class="footer_list">
<ul>
<li><a href="https://www.doctorksa.com/Pages/display/about-doctorksa-the-medical-resource-provider-in-saudi-arabia-5">About Us </a></li>
<li><a href="https://www.doctorksa.com/Pages/display/contact-doctorksa">Contact Us</a></li>
<li><a href="https://www.doctorksa.com/surveys">Surveys</a></li>
<li><a href="https://www.doctorksa.com/Pages/faq">FAQ's</a></li>
<li><a href="https://www.doctorksa.com/Pages/links">Links</a></li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<form action="https://www.doctorksa.com/users/newsletter" class="form-horizontal" data-toggle="validator" method="post" role="form">
<input id="spec_add" name="spec_add" type="hidden" value=""/>
<div class="newsletterbox">
<h3>Subscribe to Our Newsletter</h3>
<ul>
<li><input class="form-control" name="news_name" placeholder="Name" required="" type="text"/></li>
<li><input class="form-control" name="news_email" placeholder="Email" required="" type="email"/></li>
<li><select class="form-control" id="spec_gg_aa" name="news_specialty" onchange="addspectof(this.value)" required="">
<option value="">Select Speciality</option>
<option id="cbf_2" value="2">Anesthesia</option>
<option id="cbf_3" value="3">Aviation Medicine</option>
<option id="cbf_4" value="4">Administration</option>
<option id="cbf_5" value="5">Biostatistics</option>
<option id="cbf_6" value="6">Cardiac Surgery</option>
<option id="cbf_7" value="7">Cardiology</option>
<option id="cbf_8" value="8">Critical Care</option>
<option id="cbf_9" value="9">Dental</option>
<option id="cbf_10" value="10">Dermatology</option>
<option id="cbf_11" value="11">Emergency Medicine</option>
<option id="cbf_12" value="12">Endocrine</option>
<option id="cbf_13" value="13">ENT</option>
<option id="cbf_14" value="14">Epidemiology</option>
<option id="cbf_15" value="15">Family Medicine</option>
<option id="cbf_16" value="16">Gastroenterology</option>
<option id="cbf_17" value="17">Genetics</option>
<option id="cbf_18" value="18">Geriatric Medicine</option>
<option id="cbf_19" value="19">Hematology</option>
<option id="cbf_20" value="20">Hepatology</option>
<option id="cbf_21" value="21">Infectious Diseases</option>
<option id="cbf_22" value="22">Internal Medicine</option>
<option id="cbf_23" value="23">Laboratory</option>
<option id="cbf_24" value="24">Medical Physics</option>
<option id="cbf_25" value="25">Nephrology</option>
<option id="cbf_26" value="26">Neurology</option>
<option id="cbf_27" value="27">Neurosurgery</option>
<option id="cbf_28" value="28">Nuclear Medicine</option>
<option id="cbf_29" value="29">Obstetric/Gynecology</option>
<option id="cbf_30" value="30">Ophthalmology</option>
<option id="cbf_31" value="31">Oncology</option>
<option id="cbf_32" value="32">Organ Transplantation</option>
<option id="cbf_33" value="33">Orthopedics</option>
<option id="cbf_34" value="34">Palliative Medicine</option>
<option id="cbf_35" value="35">Pathology</option>
<option id="cbf_36" value="36">Pediatrics</option>
<option id="cbf_37" value="37">Pharmacology</option>
<option id="cbf_38" value="38">Plastic Surgery</option>
<option id="cbf_39" value="39">Physiotherapy</option>
<option id="cbf_40" value="40">Psychiatry</option>
<option id="cbf_41" value="41">Pulmonary</option>
<option id="cbf_42" value="42">Radiology</option>
<option id="cbf_43" value="43">Rheumatology</option>
<option id="cbf_44" value="44">Surgery</option>
<option id="cbf_45" value="45">Urology</option>
<option id="cbf_46" value="46">Vascular Surgery</option>
</select></li>
</ul>
<div class="captcha_sec clearfix">
<ul>
<li>
<span class="sec_code">Security Code<span class="mendetory_field">*</span></span>
</li>
<li class="cap_img"><img class="abc_url_cap" src="https://www.doctorksa.com//img/captcha/64d8dcbe570351c1974f33cf790e16ec.png"/></li>
<li><input class="form-control ip_input" name="news_ip_input" required="" type="text"/></li>
<li><img onclick="javascript:docapt();" src="https://www.doctorksa.com/img/captcha_reset.png" style="cursor:pointer;"/></li>
<ul>
</ul></ul></div>
<input id="cap_cd" name="captchava" type="hidden" value="B2dt5"/>
<div class="subscribe_box">
<input class="btn_subs" type="submit" value="subscribe"/>
</div>
</div>
</form>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<div class="upload_boxbtn">
<button class="btn-primary1" data-target="#exampleModal" data-toggle="modal" data-whatever="@mdo" type="button">
<a class="btn btn-primary " href="#"> <i class="fa fa-upload"></i>
 UPLOAD YOUR CV </a></button>
<div aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
<h4 class="modal-title" id="exampleModalLabel">Please submit your information</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
<div class="form_box">
<form action="https://www.doctorksa.com/users/submitresume" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data" id="form1" method="post" role="form">
<div class="form-group">
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><label>Name:</label></div>
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
<input name="username" placeholder="Name" required="required" type="text"/>
</div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-12"><label>Specialty:</label></div>
<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12" style="margin-bottom: 11px; margin-left: -6px;">
<select class="form-control" name="news_specialty" required="">
<option value="">-- Select --</option>
<option id="acbf_2" value="Anesthesia">Anesthesia</option>
<option id="acbf_3" value="Aviation Medicine">Aviation Medicine</option>
<option id="acbf_4" value="Administration">Administration</option>
<option id="acbf_5" value="Biostatistics">Biostatistics</option>
<option id="acbf_6" value="Cardiac Surgery">Cardiac Surgery</option>
<option id="acbf_7" value="Cardiology">Cardiology</option>
<option id="acbf_8" value="Critical Care">Critical Care</option>
<option id="acbf_9" value="Dental">Dental</option>
<option id="acbf_10" value="Dermatology">Dermatology</option>
<option id="acbf_11" value="Emergency Medicine">Emergency Medicine</option>
<option id="acbf_12" value="Endocrine">Endocrine</option>
<option id="acbf_13" value="ENT">ENT</option>
<option id="acbf_14" value="Epidemiology">Epidemiology</option>
<option id="acbf_15" value="Family Medicine">Family Medicine</option>
<option id="acbf_16" value="Gastroenterology">Gastroenterology</option>
<option id="acbf_17" value="Genetics">Genetics</option>
<option id="acbf_18" value="Geriatric Medicine">Geriatric Medicine</option>
<option id="acbf_19" value="Hematology">Hematology</option>
<option id="acbf_20" value="Hepatology">Hepatology</option>
<option id="acbf_21" value="Infectious Diseases">Infectious Diseases</option>
<option id="acbf_22" value="Internal Medicine">Internal Medicine</option>
<option id="acbf_23" value="Laboratory">Laboratory</option>
<option id="acbf_24" value="Medical Physics">Medical Physics</option>
<option id="acbf_25" value="Nephrology">Nephrology</option>
<option id="acbf_26" value="Neurology">Neurology</option>
<option id="acbf_27" value="Neurosurgery">Neurosurgery</option>
<option id="acbf_28" value="Nuclear Medicine">Nuclear Medicine</option>
<option id="acbf_29" value="Obstetric/Gynecology">Obstetric/Gynecology</option>
<option id="acbf_30" value="Ophthalmology">Ophthalmology</option>
<option id="acbf_31" value="Oncology">Oncology</option>
<option id="acbf_32" value="Organ Transplantation">Organ Transplantation</option>
<option id="acbf_33" value="Orthopedics">Orthopedics</option>
<option id="acbf_34" value="Palliative Medicine">Palliative Medicine</option>
<option id="acbf_35" value="Pathology">Pathology</option>
<option id="acbf_36" value="Pediatrics">Pediatrics</option>
<option id="acbf_37" value="Pharmacology">Pharmacology</option>
<option id="acbf_38" value="Plastic Surgery">Plastic Surgery</option>
<option id="acbf_39" value="Physiotherapy">Physiotherapy</option>
<option id="acbf_40" value="Psychiatry">Psychiatry</option>
<option id="acbf_41" value="Pulmonary">Pulmonary</option>
<option id="acbf_42" value="Radiology">Radiology</option>
<option id="acbf_43" value="Rheumatology">Rheumatology</option>
<option id="acbf_44" value="Surgery">Surgery</option>
<option id="acbf_45" value="Urology">Urology</option>
<option id="acbf_46" value="Vascular Surgery">Vascular Surgery</option>
</select>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><label>Email:</label></div>
<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"><input name="email" placeholder="Email" required="required" type="text"/></div>
<div class="col-lg-3 col-md-2 col-sm-2 col-xs-12"><label>CV:</label></div>
<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12" style="margin-bottom: 11px; margin-left: -6px;margin-top:9px;"> 
							<div class="upload-pic">
<input accept=".doc,.pdf, .docx" id="pdffile" name="resumefile" required="required" type="file"/>
<script type="text/javascript">
								var onloadCallback = function() {
							        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
							        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
							        widgetId1 = grecaptcha.render('example1', {
							          'sitekey' : '6Lc1oRoUAAAAABjeyTf93tgiiplzxx4t6usyShwz',
							          'callback' : 'YourOnSubmitFn',
							        });
							      };
							      function YourOnSubmitFn(response){
								  	$.ajax({
								  		url: "/users/validate_captcha",
								  		data: {
									  		response : response,
									  		secret : '6Lc1oRoUAAAAACECQSpbboVvGwiVKeAQfmi2k4OB'
									  	},
								  		method : 'POST',
								  		type: 'json',
								  		success : function (res){
								  			console.log(res);
								  			data = JSON.parse(res);
								  			console.log(data.success);
								  			if(data.success === true){
								  				sessionStorage.setItem('captchaValidation', '1');	
								  			}else{
								  				sessionStorage.setItem('captchaValidation', '0');
								  			}
								  		},
								  		error : function(res){
									  		console.log(res);
									  		alert('captcha validation failed!\n Please try again');
									  		grecaptcha.reset(widgetId1);
									  	}
								  	});
								  }
								  $(document).ready( function() {
								  		$('#form1').submit(function(event){
									  	status = sessionStorage.getItem('captchaValidation');
									  	if(status == 1){
									  		$(this.form).submit();
									  	}else{
									  		event.preventDefault();
									  		alert('Please verify the captcha!');
									  		grecaptcha.reset(widgetId1);
									  	}
									  	sessionStorage.removeItem('captchaValidation');
									  });
								  });
							</script>
<br/>
<div class="g-recaptcha" data-callback="YourOnSubmitFn" id="example1">
</div>
<script async="" defer="" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit"></script>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="form_box">
<input class="btn btn-danger" id="btn-submit" type="submit" value="Submit"/>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
</div>
</div>
</div>
</div>
</div>
<div class="footer_list">
<ul>
<li>Follow us on</li>
<div class="social_icons clearfix">
<ul>
<li><a href="http://www.facebook.com/pages/Doctorksa/128422235982" target="_blank"><img src="https://www.doctorksa.com/img/icon_fb.png"/></a></li>
<li><a href="http://www.twitter.com/doctorksa" target="_blank"><img src="https://www.doctorksa.com/img/icon_twitter.png"/></a></li>
<li><a href="https://www.linkedin.com/company/doctorksa/notifications?goback=&amp;trk=hb_ntf_AGGREGATED_COMPANY" target="_blank"><img src="https://www.doctorksa.com/img/icon_linkedin.png"/></a></li>
<li><a href="https://www.pinterest.com/doctorksa0467/" target="_blank"><img src="https://www.doctorksa.com/img/icon_pinintrest.png"/></a></li>
<li><a href="https://www.instagram.com/doctorksa_medical/" target="_blank"><img src="https://www.doctorksa.com/img/icon_instagram.png"/></a></li>
<li><a href="https://plus.google.com/b/104936602605927078191/104936602605927078191/posts" target="_blank"><img src="https://www.doctorksa.com/img/icon_googleplus.png"/></a></li>
</ul>
</div>
</ul>
<ul><li style="margin-top: 25px;">Download App</li></ul>
<ul><li><a href="https://play.google.com/store/apps/details?id=com.doctorksa.doctorksaeventsapp&amp;hl=ar" target="_blank"><img class="" height="inherit" src="https://www.doctorksa.com/img/googleplay.png" style="margin-right:8px;" title="" width="inherit"/></a> <a href="https://itunes.apple.com/us/app/doctorksa-events/id1116364854?mt=8" target="_blank"><img class="" height="inherit" src="https://www.doctorksa.com/img/iphone.png" style="height: 45px; width: 133px;" title="" width="inherit"/></a>
</li></ul>
</div>
</div>
</div>
</div>
</footer>
<div class="container">
<div class="copyright">
© Copyright 2015, all rights reserved with DoctorKSA.com 
</div>
</div>
<script type="text/javascript">
function addspectof(a)
{
	var b = $("#spec_gg_aa option:selected").text();
	$("#spec_add").val(b);	
}
</script>
<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-8669509-1']);
	  _gaq.push(['_setDomainName', 'doctorksa.com']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	  
		$(window).load(function() {
		
		$('.jspPane').bind('mousewheel DOMMouseScroll', function(e) {
		var scrollTo = null;

		if (e.type == 'mousewheel') {
			scrollTo = (e.originalEvent.wheelDelta * -1);
		}
		else if (e.type == 'DOMMouseScroll') {
			scrollTo = 40 * e.originalEvent.detail;
		}

		if (scrollTo) {
			e.preventDefault();
			$(this).scrollTop(scrollTo + $(this).scrollTop());
		}
		//$(this).jScrollPane({wheelSpeed: 100});
		//$(this).jScrollPane({ showArrows: true, wheelSpeed: 60 });
		//myJScrollPane.getVerticalScrollBar().setUnitIncrement(16);
		//$(this).getVerticalScrollBar().setUnitIncrement(20);
		//$(this).jScrollPane({mouseWheelSpeed: 200});
	});	
			
		});
	</script>
<!-- Footer Section End -->
<link href="css/jquery.jscrollpane.css" media="all" rel="stylesheet" type="text/css"/>
<!-- latest jQuery direct from google's CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<!-- the mousewheel plugin -->
<script src="js/jquery.mousewheel.js" type="text/javascript"></script>
<!-- the jScrollPane script -->
<script src="js/jquery.jscrollpane.min.js" type="text/javascript"></script>
<!-- scripts specific to this demo site -->
<style id="page-css" type="text/css">
			/* Styles specific to this particular page */
			.scroll-pane
			{
				width: 100%;
				height: 200px;
				overflow: auto;
			}
			.horizontal-only
			{
				height: auto;
				max-height: 200px;
			}
			.jspHorizontalBar
			{
				height:0 !important;
			}
		</style>
<script id="sourcecode" type="text/javascript">
			$(function()
			{
				//$('.scroll-pane').jScrollPane();
			});
		</script>
<!-- jQuery library (served from Google) -->
<!-- bxSlider Javascript file -->
<script src="js/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet"/>
<script type="text/javascript">
$('.bxslider').bxSlider({
  auto: true,
  mode:'fade',
  controls:false,
  pager:false,
  autoControls: false
});

function tarc(id)
{
	jQuery.ajax({
		type: 'POST',
		url:  'https://www.doctorksa.com/users/clicktrac/'+id,
		dataType: 'json',
		success: function(data){ 				
			
		}			
	});

}
</script>
</body>
</html>
