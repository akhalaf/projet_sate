<!DOCTYPE html>
<html lang="pt-br">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-9788933-1"></script>
<script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-9788933-1');
    </script>
<title>Before - Tecnologia da Informação</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Before - Tecnologia da Informação" name="description"/>
<meta content="before,vuejs,startup" name="keywords"/>
<meta content="Before - Tecnologia da Informação" name="author"/>
<meta content="#FF6633" name="apple-mobile-web-app-status-bar-style"/>
<meta content="#FF6633" name="msapplication-navbutton-color"/>
<link href="/img/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/img/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/img/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/site.webmanifest" rel="manifest"/>
<link color="#f37043" href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#f37043" name="msapplication-TileColor"/>
<meta content="#f37043" name="theme-color"/>
<link href="/css/main.cbd2c0fddfb5b5735bff.css?cbd2c0fddfb5b5735bff" rel="stylesheet"/></head>
<body>
<div id="app"></div>
<script src="/js/main.bundle.js?cbd2c0fddfb5b5735bff" type="text/javascript"></script></body>
</html>
