<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="BIOCert adalah lembaga inspeksi dan sertifikasi organik dan ecososial (produksi ramah lingkungan dan bertanggungjawab secara sosial). BIOCert merupakan anggota dari Cert All [Certification Alliance], yaitu aliansi lembaga sertifikasi pangan organik di Asia Pasifik untuk memberikan layanan inspeksi dan sertifikasi organik untuk pasar nasional, ASEAN, Uni Eropa, Swiss, Amerika Serikat dan Kanada. PT BIOCert Indonesia (selanjutnya disebut dengan BIOCert) merupakan lembaga sertifikasi organik yang telah terakreditasi ISO 17065 oleh Komite Akreditasi Nasional dan oleh IOAS untuk lingkup sertifikasi organik Uni Eropa dan Kanada. BIOCert memiliki komitmen untuk terus mendorong setiap organisasi, perusahaan atau kelompok mitra untuk meningkatkan produktivitas dan kinerja dengan menerapkan prinsip-prinsip keberlanjutan. Kami percaya praktik kerja yang memperhatikan nilai-nilai lingkungan, sosial dan ekonomi merupakan pilar penting yang menentukan keberlanjutan suatu usaha.Dengan visi untuk menjadi mitra terpercaya untuk mewujudkan kinerja keberlanjutan dan beretika dalam produksi pertanian dan pangan, BIOCert terus berusaha untuk menjadi perusahaan yang kredibel dan mitra terpercaya untuk industri pertanian dan makanan di Asia Pasifik. Untuk memastikan kemudahan dan kepuasan klien dalam memperoleh layanan sertifikasi, BIOCert menyediakan ragam skema layanan sertifikasi diantaranya Sertifikasi Organik SNI, Sertifikasi Organik Ekspor (Standar EU, NOP, COR), Sertifikasi UTZ, Sertifikasi Rainforest Alliance, Sertifikasi C.A.F.E Practice dan Sertifikasi 4C. " name="description"/>
<meta content="Organic Certification Services, Layanan Sertifikasi Organik, organik,organic,sertifikasi,certification,sertifikasi Organik, Organic certification, organik indonesia, sertifikasi organik ekspor, organik ekspor, sertifikasi EU, sertifikasi NOP, sertifikasi COR, sertifikasi RA, sertifikasi SNI, verifikasi UTZ, verifikasi 4C, sertifikasi organik indonesia, indonesia  organic certification, sertifikasi SNI organik, organic SNI certification, NOP certification, COR certification, RA certification, UTZ verification, 4C verification, NOP certification indonesia, COR certification indonesia, RA certification indonesia, UTZ verification indonesia, 4C verification indonesia, UTZ Certified, layanan audit dan sertifikasi Code of Conduct dan Chain of Custody Program UTZ Certified, lembaga verifikasi C.A.F.E Practices, verification agency for verification of C.A.F.E Practices, organic certification body, lembaga sertifikasi organik, certified according to Canada Organic Regime, How can be certified as organic for Canada and U.S. market, partnership with INDOCERT, INDOCERT’s country representative, lspo, utz certification indonesia, 4c certification indonesia, cafe practices certification indonesia" name="keywords"/>
<meta content="BIOCert" name="author"/>
<!-- Stylesheets
	============================================= -->
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css"/>
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" rel="stylesheet"/>
<link href="https://www.biocert.co.id/assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/swiper.css" rel="stylesheet" type="text/css"/>
<!-- Construction Demo Specific Stylesheet -->
<link href="https://www.biocert.co.id/assets/themes/construction/construction.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/components/bs-datatable.css" rel="stylesheet" type="text/css"/>
<!-- / -->
<link href="https://www.biocert.co.id/assets/css/dark.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/font-icons.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/animate.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/magnific-popup.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/themes/construction/css/fonts.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.biocert.co.id/assets/themes/construction/css/colors.css" rel="stylesheet" type="text/css"/>
<link href="https://www.biocert.co.id/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- Document Title
	============================================= -->
<title>
            BIOCert
        </title>
</head>
<body class="stretched">
<div id="fb-root"></div>
<!-- Document Wrapper
	============================================= -->
<div class="clearfix" id="wrapper">
<!-- Top Bar
		============================================= -->
<div id="top-bar">
<div class="container clearfix">
<div class="col_half nobottommargin clearfix">
<!-- Top Social
					============================================= -->
<div id="top-social">
<ul>
<li>
<a class="si-facebook" href="#" id="linkFb" target="_blank">
<span class="ts-icon">
<i class="icon-facebook">
</i>
</span>
<span class="ts-text">
                                            Facebook
                                        </span>
</a>
</li>
<li>
<a class="si-twitter" href="#" id="linkTwitter" target="_blank">
<span class="ts-icon">
<i class="icon-twitter">
</i>
</span>
<span class="ts-text">
                                            Twitter
                                        </span>
</a>
</li>
<li>
<a class="si-instagram" href="#" id="linkIg" target="_blank">
<span class="ts-icon">
<i class="icon-instagram2">
</i>
</span>
<span class="ts-text">
                                            Instagram
                                        </span>
</a>
</li>
</ul>
</div>
<!-- #top-social end -->
</div>
<div class="col_half fright col_last clearfix nobottommargin">
<!-- Top Links
					============================================= -->
<div class="top-links">
<ul>
<li>
<a href="#">
                                        Language                                    </a>
<ul>
<li>
<a href="https://www.biocert.co.id/LangSwitch/switchLanguage/id">
                                                IDN
                                            </a>
</li>
<li>
<a href="https://www.biocert.co.id/LangSwitch/switchLanguage/en">
                                                ENG
                                            </a>
</li>
</ul>
</li>
</ul>
</div>
<!-- .top-links end -->
</div>
</div>
</div>
<!-- #top-bar end -->
<!-- Header
		============================================= -->
<header class="sticky-style-2" id="header">
<div class="container clearfix">
<!-- Logo
				============================================= -->
<div id="logo">
<a class="standard-logo" href="https://www.biocert.co.id/">
<img alt="Canvas Logo" src="https://www.biocert.co.id/assets/themes/construction/images/logo_website.png"/>
</a>
<a class="retina-logo" href="https://www.biocert.co.id/">
<img alt="Canvas Logo" src="https://www.biocert.co.id/assets/themes/construction/images/logo_website.png"/>
</a>
</div>
<!-- #logo end -->
<ul class="header-extras">
<li>
<i class="i-plain icon-call nomargin">
</i>
<div class="he-text" id="divCallUs">
                                Call Us                                <span>
</span>
</div>
</li>
<!--13/9/2018 add Whatsapp-->
<li>
<i class="i-plain fab fa-whatsapp-square nomargin">
</i>
<div class="he-text" id="divWA">
                                Whatsapp (click below)
                                <span>
<a href="https://wa.me/6288809001187" target="_blank">+62-888-0900-1187</a>
</span>
</div>
</li>
<!--xx-->
<li>
<i class="i-plain icon-line2-envelope nomargin">
</i>
<div class="he-text" id="divEmailUs">
                                Email Us                                <span>
</span>
</div>
</li>
</ul>
</div>
<div id="header-wrap">
<!-- Primary Navigation
				============================================= -->
<nav class="with-arrows style-2" id="primary-menu">
<div class="container clearfix">
<div id="primary-menu-trigger">
<span><i class="icon-reorder">
</i> Menu</span>
</div>
<ul>
<li class="mega-menu" style="display:none">
<a href="#">
            What Can We Do For You
        </a>
<div class="mega-menu-content style-2 clearfix" id="megaMenu">
</div>
</li>
</ul>
<ul class="pull-right" id="ulMenu">
</ul> </div>
</nav>
<!-- #primary-menu end -->
</div>
</header>
<!-- #header end -->
<section class="slider-parallax swiper_wrapper clearfix" data-loop="true" id="slider" style="height: 550px;">
<div class="swiper-container swiper-parent">
<div class="swiper-wrapper">
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/Banner_20210108.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/motto biocert_20200623.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/SNI organic_20200623.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/starbucks_20200623.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/export organic_20200623.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
<div class="swiper-slide" style="background-position: center bottom; background-image:url('http://www.biocert.co.id/uploads/biocert/home/training_20200623.png'); ">
<div class="container clearfix">
<!--<div class="slider-caption" style="left:30px;">
                                <p>
                                                                    </p>
                            </div> -->
</div>
</div>
</div>
<div id="slider-arrow-left">
<i class="icon-angle-left">
</i>
</div>
<div id="slider-arrow-right">
<i class="icon-angle-right">
</i>
</div>
</div>
</section>
<!-- Content
        ============================================= -->
<section id="content">
<div class="content-wrap">
<div class="promo promo-light promo-full uppercase header-stick" id="bannerService">
<div class="container clearfix">
<h3 style="letter-spacing: 2px;">
</h3>
<span>
</span>
<a class="button button-large button-border button-rounded" href="#">
                    Check Our Service                </a>
</div>
</div>
<div class="section parallax dark" data-stellar-background-ratio="0.4" id="overviewSection" padding:="" style="margin-top:0px;">
<div class="testimonial testimonial-full" data-arrows="false" style="z-index: 2;">
<div class="testi-content" id="pOverview">
</div>
</div>
<div class="video-wrap" style="z-index: 1;">
<div class="video-overlay" style="background: rgba(56,142,60,0.9);">
</div>
</div>
</div>
<div class="container clearfix" id="newsSection">
<div class="col_three_third">
<h4>
                    Recent News                </h4>
<div class="owl-carousel posts-carousel carousel-widget" id="oc-posts">
</div>t
            </div>
</div>
</div>
</section>
<!-- #content end -->
<!-- Footer
		============================================= -->
<footer class="dark" id="footer">
<div class="container">
<!-- Footer Widgets
				============================================= -->
<div class="footer-widgets-wrap clearfix">
<div class="col_two_third">
<div class="widget clearfix">
<div class="row">
<div class="col-md-3 col-xs-6 bottommargin-sm widget_links">
</div>
<div class="col-md-3 col-xs-6 bottommargin-sm widget_links">
</div>
<div class="col-md-3 col-xs-6 bottommargin-sm widget_links">
</div>
<div class="col-md-3 col-xs-6 bottommargin-sm widget_links">
</div>
</div>
</div>
</div>
<div class="col_one_third col_last">
<div class="widget clear-bottommargin-sm clearfix">
<div class="row">
<div class="col-md-12 bottommargin-sm">
<div class="footer-big-contacts" id="divCallUsFooter">
<span>
                                                Call Us                                            </span>
</div>
</div>
<div class="col-md-12 bottommargin-sm">
<div class="footer-big-contacts" id="divEmailUsFooter">
<span>
                                                Email Us                                            </span>
</div>
</div>
</div>
</div>
<div class="widget subscribe-widget clearfix">
<div class="row">
<div class="col-md-6 clearfix bottommargin-sm">
<a class="social-icon si-dark si-colored si-facebook nobottommargin" href="#" style="margin-right: 10px;">
<i class="icon-facebook">
</i>
<i class="icon-facebook">
</i>
</a>
<a href="#">
<small style="display: block; margin-top: 3px;">
<strong>
                                                    Like us
                                                </strong>
<br/>
                                                    on Facebook
                                                
                                            </small>
</a>
</div>
<div class="col-md-6 clearfix">
<a class="social-icon si-dark si-colored si-rss nobottommargin" href="#" style="margin-right: 10px;">
<i class="icon-rss">
</i>
<i class="icon-rss">
</i>
</a>
<a href="#">
<small style="display: block; margin-top: 3px;">
<strong>
                                                    Subscribe
                                                </strong>
<br/>
                                                    to RSS Feeds
                                                
                                            </small>
</a>
</div>
</div>
</div>
</div>
</div>
<!-- .footer-widgets-wrap end -->
</div>
<!-- Copyrights
			============================================= -->
<div id="copyrights">
<div class="container clearfix">
<div class="col_half">
                            Copyrights © 2017 All Rights Reserved by Biocert.
                            <br/>
<div class="copyright-links">
<a href="#">
                                        Terms of Use
                                    </a>
                                    /
                                    <a href="#">
                                        Privacy Policy
                                    </a>
</div>
</div>
<div class="col_half col_last tright">
<div class="copyrights-menu copyright-links clearfix" id="menuFooter">
</div>
</div>
</div>
</div>
<!-- #copyrights end -->
</footer>
<!-- #footer end -->
</div>
<!-- #wrapper end -->
<!-- Go To Top
	============================================= -->
<div class="icon-angle-up" id="gotoTop">
</div>
<!-- External JavaScripts
	============================================= -->
<script src="https://www.biocert.co.id/assets/js/jquery.js" type="text/javascript">
</script>
<script src="https://www.biocert.co.id/assets/js/plugins.js" type="text/javascript">
</script>
<!-- Footer Scripts
	============================================= -->
<script src="https://www.biocert.co.id/assets/js/functions.js" type="text/javascript">
</script>
<script src="https://www.biocert.co.id/assets/js/common/common.js" type="text/javascript">
</script>
<script src="https://www.biocert.co.id/assets/js/pages/template/template.js" type="text/javascript">
</script>
<script>
    var base_url = 'https://www.biocert.co.id/';
    var success_alert = '';
    var uriSegment3 = '';
    var uriSegment1 = '';
    var lang = 'en';
    var pageSession = 'NOT_SERVICE';
</script>
<script src="https://www.biocert.co.id/assets/js/pages/bse_home_page/home_page.js">
</script>
<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-101539212-1', 'auto');
          ga('send', 'pageview');
        
        </script>
</body>
</html>