<!DOCTYPE html>
<html lang="en">
<head>
<title>ArtistADay - Online Contemporary Fine Arts Gallery. New Emerging Artists Each Day - Artistaday.com</title><meta content="The mission of Artist A Day is to raise awareness of fine art globally through establishing personal connections between professional Contemporary Artists and people who love Art." name="description"/><meta content="ArtistADay.com: New contemporary fine art and emerging artists every day" property="og:title"/>
<meta content="http://www.artistaday.com/wp-content/uploads/aad_logo.jpg" property="og:image"/>
<meta content="The mission of ArtistADay is to raise awareness of contemporary fine art globally through establishing personal connections between professional emerging Artists and people who love Art." property="og:description"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Le styles -->
<link href="/wp-content/themes/bootstrap3/A.bootstrap,,_css,,_bootstrap.css+style.css,Mcc.EQW8EQrCW0.css.pagespeed.cf.mdf9ggGJfW.css" rel="stylesheet"/>
<!-- Google Fonts -->
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600,400&amp;display=swap" rel="preload"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600,400&amp;display=swap" media="print" onload="this.media='all'" rel="stylesheet"/>
<!-- Le fav and touch icons -->
<link href="/wp-content/uploads/xfavicon.png.pagespeed.ic.-7kczy-ZaP.png" rel="shortcut icon"/>
<link href="/wp-content/uploads/xapple-57x57.png.pagespeed.ic.M6Fx3lcSv4.png" rel="apple-touch-icon"/>
<link href="/wp-content/uploads/xapple-72x72.png.pagespeed.ic.Hzw8wgKJGf.jpg" rel="apple-touch-icon" sizes="72x72"/>
<link href="/wp-content/uploads/xapple-114x114.png.pagespeed.ic.jQRaWSHrzH.jpg" rel="apple-touch-icon" sizes="114x114"/>
<link href="/wp-content/uploads/xapple-144x144.png.pagespeed.ic.hc7I3CV0RC.png" rel="apple-touch-icon" sizes="144x144"/>
<script src="/wp-content/themes/bootstrap3/js/common_bootstrap3_unobfuscated.js.pagespeed.jm.N6nlMgdIiB.js" type="text/javascript"></script>
<script type="application/ld+json">
     {
  "@context": "https://schema.org",
  "@graph":
  [
	  {
	  "@type": "Organization",
	  "url": "https://www.artistaday.com",
	  "logo": "https://www.artistaday.com/wp-content/uploads/aad_logo_white_200x80.jpg",
	  "name": "ArtistADay",
	  "sameAs": [
	  	"https://www.twitter.com/artistaday",
	  	"https://www.instagram.com/artist.a.day",
	  	"https://www.facebook.com/pages/Artistadaycom/139368439410327",
	  	"https://www.pinterest.com/artistaday"
	  	]   
	  },
	  {
	  "@type": "WebSite",
      "url": "https://www.artistaday.com/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.artistaday.com/?s={search_term_string}",
        "query-input": "required name=search_term_string"
		}
	  }
  ]
  
  
}
</script>
<script type="text/javascript">var new_theme=1;</script>
<!-- END Google Ads -->
<meta content="Artistaday.com" property="og:site_name"/>
<meta content="article" property="og:type"/>
<!-- <meta property="og:url" content="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']"" /> -->
<!--[if gte IE 9]>
	  <style type="text/css">
	    .gradient, .gradient.over {
	       filter: none;
	    }
	    img {
	    	width: inherit;
	    }
	  </style>
	<![endif]-->
</head>
<body>
<div class="hidden-xs hidden-sm text-center" id="top_share" style="background-color: #ff69b4; height: 50px;">
<div class="container-fluid">
<div class="row">
<div class="col-md-12 col-lg-12">
<h4 id="shares_total" style="color: #ffffff; margin-top: 12px;">The Artistaday.com project has been retired but these archives will remain as a source of inspiration. Thanks for all that contributed!</h4>
</div>
</div>
</div>
</div>
<div class="container-fluid" style="max-width: 100%">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-top: 5px;">
<div class="visible-xs visible-sm" style="text-align: center; padding: 10px 0px 15px 15px;">
<a href="/"><img alt="Artistaday.com - Find new contemporary art, create your own gallery of art" border="0" src="/wp-content/uploads/xaad_logo_mobile_black-1.png.pagespeed.ic.jI3688KsHH.png" style="padding: 10px 0px;"/></a>
</div>
<div class="visible-md visible-lg" style="padding: 10px 0px 15px 15px;">
<a href="/"><img alt="Artistaday.com - Find new contemporary art, create your own gallery of art" border="0" src="/wp-content/uploads/logo_200x80.png.pagespeed.ce.T9VMvwvKUH.png"/></a>
</div>
</div>
<div class="hidden-xs hidden-sm col-md-9 col-lg-9">
<div class="text-right" style="text-align: right; margin-bottom: 5px;">
<!-- header -->
<!--
				<ins class="adsbygoogle"
				     style="display:inline-block;width:728px;height:90px"
				     data-ad-client="ca-pub-7963244442939670"
				     data-ad-slot="3154990578"></ins>
-->
<script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
</div>
</div>
</div>
</div>
<div class="navbar">
<div class="container-fluid" style="background-color: #666; max-width: 100%;">
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed btn-lg" data-target="#aad-navbar" data-toggle="collapse" style="margin-top: 0px; margin-bottom: 0px;" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="glyphicon glyphicon-align-justify"></span>
</button>
</div>
<div class="collapse navbar-collapse" id="aad-navbar">
<ul class="nav navbar-nav navbar-left">
<li class="btn-info"><a href="?pagename=login&amp;instance=tml-page">Login</a></li>
<li class="visible-xs visible-sm" id="navHome"><a href="/">Home</a></li>
<li class="dropdown" id="navArchives">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
              		Archives
              		<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="/?page_id=31141">By Recent</a></li>
<li><a href="/?page_id=9901">By Rating</a></li>
<li><a href="/?page_id=9342">By Type</a></li>
<li><a href="/?page_id=17683">By Most Shared</a></li>
</ul>
</li>
<li id="nacGuestCurators"><a href="/?page_id=14307">Guest Curators</a></li>
<li id="navTop10s"><a href="/?page_id=17903">Top 10s</a></li>
<li class="hidden-sm hidden-xs" id="navContact"><a href="/?page_id=209">Contact Us</a></li>
<li class="btn-info" id="navResidency"><a href="/residency">AAD Residency</a></li>
<!--  <li class="hidden-sm hidden-xs" id="navSubmissions"><a href="/?page_id=10">Submissions</a></li> -->
</ul>
<ul class="nav navbar-nav navbar-right" style="margin-left: 0px;">
<li>
<form action="/" class="navbar-form navbar-right" method="get">
<div class="form-group">
<span class="glyphicon glyphicon-search"></span>
<input class="search-query" id="q" name="s" placeholder="Name, location or keyword" style="width: 250px; height: 30px;" type="text"/>
</div>
</form>
</li>
</ul>
</div><!--/.nav-collapse -->
</div>
</div><div class="container-fluid">
<div class="row hidden-xs hidden-sm">
<div class="col-md-7 col-lg-7">
<h1><span style="color: #666;">Featured: </span> Jim Darling  <span class="index-header-location">Los Angeles, CA</span></h1>
</div>
<div class="col-md-5 col-lg-5" style="text-align: right;">
<!-- <a href="https://www.facebook.com/Artistaday-139368439410327/" id="facebook_share" title="Become a fan of our Facebook page"><img src="/wp-content/uploads/share_facebook.png" alt="artistaday facebook" width="" height="" /></a> -->
<a href="https://chrome.google.com/webstore/detail/artistadaycom-daily-conte/kcjofedkoeeommjlfegnjbgegnkgnjgk" id="chrome_share" title="Get the Google Chrome Extension"><img alt="artistaday chrome" height="" src="/wp-content/uploads/share_chrome.png.pagespeed.ce.1ERkIYT1NU.png" width=""/></a>
<a href="https://www.instagram.com/artist.a.day" id="instagram_share" title="Follow us on Instagram"><img alt="artistaday instagram" height="40" src="/wp-content/uploads/xshare_instagram.png.pagespeed.ic.YpXe19QUYH.png" width="40"/></a>
<!-- <a href="http://www.twitter.com/artistaday" id="twitter_share" title="Follow us on Twitter"><img src="/wp-content/uploads/share_twitter.png" alt="artistaday twitter" width="" height="" /></a> -->
<!-- 				<a href="/?feed=rss2" id="rss_share" title="Subscribe to our feed"><img src="/wp-content/uploads/share_feed.png" alt="artistaday rss" width="" height="" /></a> -->
<a href="http://www.pinterest.com/artistaday" id="pinterest_share" title="Follow us on Pinterest"><img alt="artistaday pinterest" height="" src="/wp-content/uploads/share_pinterest.png.pagespeed.ce.6FSvqobJnX.png" width=""/></a>
<!-- <a href="/?page_id=19369" id="mailing_share" title="Sign up for our mailing list"><img src="/wp-content/uploads/share_email.png" alt="artistaday mailing list" width="" height="" /></a> -->
</div>
</div>
<hr class="hidden-xs hidden-sm"/>
<div class="row visible-xs visible-sm">
<div class="col-xs-12 col-sm-12">
<h1 style="margin-bottom: 10px;">Jim Darling <br style="hidden-md hidden-lg"/><span class="index-header-location">Los Angeles, CA</span></h1>
</div>
</div>
<div class="row" style="margin-left: 0px; margin-right: 0px;">
<div class="hidden-xs hidden-sm col-md-12 col-lg-12 tag_header loading" id="featured_bg" style="height: 500px;">
<div class="hidden-xs hidden-sm row">
<div class="col-md-4 col-lg-4 col-md-offset-7 col-lg-offset-7 homepage_content_box"> <h2 style="margin-bottom: 10px;">Jim Darling</h2>
<p>Jim Darling is a Los Angeles, CA based artist who’s work mimics the views you see from an airplane window. Jim explains the work by saying, “Everyone is fascinated by flight, and for now airplanes are how we get to experience it. At some point on each flight I’ve been on, I think about sitting in a chair in the sky, and it seems crazy every time. These expansive views can be very humbling.”</p>
<p style="padding-top: 10px;"><a class="btn btn-sm btn-info" href="https://www.artistaday.com/?p=25647"><span class="glyphicon glyphicon-share"></span> See more, rate and comment on this artist</a></p>
</div>
</div>
</div>
<div class="visible-xs visible-sm">
<a href="https://www.artistaday.com/?p=25647"><img src="https://www.artistaday.com/wp-content/uploads/jimdarling_324423243_large.jpg"/></a>
</div>
</div>
<hr class="visible-xs visible-sm"/>
<div class="row visible-xs visible-sm">
<div class="col-xs-12 col-sm-12">
<p>Jim Darling is a Los Angeles, CA based artist who’s work mimics the views you see from an airplane window. Jim explains the work by saying, “Everyone is fascinated by flight, and for now airplanes are how we get to experience it. At some point on each flight I’ve been on, I think about sitting in a chair in the sky, and it seems crazy every time. These expansive views can be very humbling.”</p>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"><hr/><div class="row"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h3>AAD Residency</h3></div></div><hr/><a href="/residency"><div class="row" style="margin-left: 0px; margin-right: 0px;"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="promo_1" style="-webkit-filter:grayscale(100%);filter:grayscale(100%);position:relative;height:250px;-webkit-background-size:cover;background-image:url(/wp-content/uploads/aad_residency.jpg.pagespeed.ce.2XlcWC0-Df.jpg);background-size:cover;background-position:50% 50%;background-repeat:no-repeat"><div class="row hidden-xs hidden-sm"><div class="homepage_content_box col-md-6 col-lg-6 text-right" style="position: absolute; right: 35px; bottom: 20px;"><h2 style="margin-bottom: 10px;">Transform Your Workspace With World Class Original Art</h2></div></div></div><div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><div class="row text-center" style="padding-top: 15px;"><h2>Transform Your Workspace With World Class Original Art</h2><p style="padding-top: 20px;"><a class="btn btn-sm btn-inverse" href="/residency"><span class="glyphicon glyphicon-share"></span> Learn More</a></p></div></div></div></a></div><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"><hr/><div class="row"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h3>Best of AAD</h3></div></div><hr/><a href="/?tag=celebrity"><div class="row" style="margin-left: 0px; margin-right: 0px;"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="promo_2" style="-webkit-filter:grayscale(100%);filter:grayscale(100%);position:relative;height:250px;-webkit-background-size:cover;background-image:url(/wp-content/uploads/xnew_tag_celebrity_header.jpg.pagespeed.ic.gCkwP1Bz9Y.jpg);background-size:cover;background-position:50% 50%;background-repeat:no-repeat"><div class="row hidden-xs hidden-sm"><div class="homepage_content_box col-md-6 col-lg-6 text-right" style="position: absolute; right: 35px; bottom: 20px;"><p>Top 10:</p><h2 style="margin-bottom: 10px;">Celebrity</h2></div></div></div><div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><div class="row text-center" style="padding-top: 15px;"><h2>Top 10: Celebrity</h2><p style="padding-top: 20px;"><a class="btn btn-sm btn-inverse" href="/?tag=celebrity"><span class="glyphicon glyphicon-share"></span> View all</a></p></div></div></div></a></div> </div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<hr/>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
<h3>Guest curator Erin Nathanson on the work of Danny Ferrell</h3>
</div>
<div class="hidden-xs hidden-sm col-md-4 col-lg-4">
<a class="btn btn-sm btn-inverse pull-right" href="/?page_id=14307"><span class="glyphicon glyphicon-share"></span> View all guest curators</a>
</div>
</div>
<hr/>
<div class="row" style="margin-left: 0px; margin-right: 0px;">
<a href="/?page_id=35131">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="">
<div style="margin: 30px 30px;">
<h1 class="text-center hpquotemark">"</h1>
<h2 class="text-center hpquotes">I hate to admit that the word "beauty" comes to mind before anything else while viewing Danny's work, but in times like these I want to be captivated. Danny offers his viewer a fantastical look at his experiences as a queer male, raised in a society where identifying as gay was something to "fear" and not to "behold" - this is why I find beauty in his work, it is not just the colors, or technical skill, it is the presence of love and vulnerability he shares through his subjects that pull me in.</h2>
<h1 class="text-center hpquotemark" style="margin-top: 30px;">"</h1>
</div>
</div>
</a> <div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
<div class="row text-center" style="padding-top: 15px;">
<p style="padding-top: 20px;"><a class="btn btn-sm btn-inverse" href="/?page_id=14307"><span class="glyphicon glyphicon-share"></span> View all guest curators</a></p>
</div>
</div>
</div>
</div> </div>
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<hr/>
<div class="row">
<div class="col-md-8 col-lg-8">
<h3>Most Recent</h3>
</div>
<div class="col-md-4 col-lg-4">
<div>
<a class="btn btn-inverse pull-right" href="/?page_id=31141"><span class="glyphicon glyphicon-share"></span></a>
</div>
</div>
</div>
<hr/>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="https://www.artistaday.com/?p=143894"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/laddiejohndill_234423234_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xladdiejohndill_234423234_large.jpg.pagespeed.ic.WG-2nA9oya.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-md-12 col-lg-12">
<div style="padding: 10px 0px 10px 10px;">
<h3>Laddie John Dill</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Los+Angeles%2C+CA" style="font-weight: normal;">Los Angeles, CA</a>
</h5>
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="https://www.artistaday.com/?p=143886"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/victoriawagner_234234234_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xvictoriawagner_234234234_large.jpg.pagespeed.ic.PzB31UjspZ.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-md-12 col-lg-12">
<div style="padding: 10px 0px 10px 10px;">
<h3>Victoria Wagner</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Oakland%2C+CA" style="font-weight: normal;">Oakland, CA</a>
</h5>
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="https://www.artistaday.com/?p=143879"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/draganbibin_234432234_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xdraganbibin_234432234_large.jpg.pagespeed.ic.cDi3DrzqH4.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-md-12 col-lg-12">
<div style="padding: 10px 0px 10px 10px;">
<h3>Dragan Bibin</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Novi+Sad%2C+Serbia" style="font-weight: normal;">Novi Sad, Serbia</a>
</h5>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<hr/>
<div class="row">
<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
<h3>Highest ranked</h3>
</div>
<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
<a class="btn btn-inverse btn-sm pull-right" href="/?page_id=9901"><span class="glyphicon glyphicon-share"></span></a>
</div>
</div>
<hr/>
<div id="top3">
<div class="alert alert-info fade in" id="top3_alert"><strong>Loading artists...</strong></div>
</div>
</div>
<div class="hidden-xs hidden-sm col-md-3 col-lg-3">
<hr/>
<div class="row">
<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
<h3>Most Shared</h3>
</div>
<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
<a class="btn btn-inverse btn-sm pull-right" href="/?page_id=17683"><span class="glyphicon glyphicon-share"></span></a>
</div>
</div>
<hr/>
<div class="content_box_border" style="margin-bottom: 20px;">
<div class="">
<a href="https://www.artistaday.com/?p=13913"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/jacobsutton_234423423_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xjacobsutton_234423423_large.jpg.pagespeed.ic.pgEzOIRJU5.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>Jacob Sutton</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=London%2C+UK" style="font-weight: normal;">London, UK</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: right;">
<div style="padding-top: 10px; padding-right: 8px;">
<h2>169k</h2>
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div class="">
<a href="https://www.artistaday.com/?p=17668"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/scottnaismith_456654465645_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xscottnaismith_456654465645_large.jpg.pagespeed.ic.SGUnSU5DAS.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>Scott Naismith</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Glasgow%2C+Scotland" style="font-weight: normal;">Glasgow, Scotland</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: right;">
<div style="padding-top: 10px; padding-right: 8px;">
<h2>137k</h2>
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div class="">
<a href="https://www.artistaday.com/?p=13105"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/davidkracov_432243243_large.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/davidkracov_432243243_large.jpg.pagespeed.ce.6sO63Jqzx-.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>David Kracov</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Los+Angeles%2C+CA" style="font-weight: normal;">Los Angeles, CA</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: right;">
<div style="padding-top: 10px; padding-right: 8px;">
<h2>95k</h2>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<hr/>
<div class="row">
<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
<h3>AAD Staff Picks</h3>
</div>
</div>
<hr/>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="/?p=6767"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/valerilarko_324234234.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xvalerilarko_324234234.jpg.pagespeed.ic.K3PK-BEbGo.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>Valeri Larko</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=New+Rochelle%2C+NY" style="font-weight: normal;">New Rochelle, NY</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: center;">
<div style="padding-top: 10px;">
<!-- <h2>0</h2> -->
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="/?p=11876"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/zorannova_234234324.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xzorannova_234234324.jpg.pagespeed.ic.nVMzTrkdBF.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>Zoran Nova</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Sydney%2C+Australia" style="font-weight: normal;">Sydney, Australia</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: center;">
<div style="padding-top: 10px;">
<!-- <h2>0</h2> -->
</div>
</div>
</div>
</div>
<div class="content_box_border" style="margin-bottom: 20px;">
<div>
<a href="/?p=7864"><img class="lazy" data-original="https://www.artistaday.com/wp-content/uploads/sonnykay_243243234324.jpg" loading="lazy" src="/wp-content/uploads/ximg_loading.png.pagespeed.ic.2Uf4fyzXAx.png"/><noscript><img loading="lazy" src="https://www.artistaday.com/wp-content/uploads/xsonnykay_243243234324.jpg.pagespeed.ic.TBELCGl6Le.jpg"/></noscript></a>
</div>
<div class="row">
<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8">
<div style="padding: 10px 0px 10px 10px;">
<h3>Sonny Kay</h3>
<h5>
<a class="location_link" href="?page_id=10029&amp;l=Los+Angeles%2C+CA" style="font-weight: normal;">Los Angeles, CA</a>
</h5>
</div>
</div>
<div class="col-xs-5 col-sm-5 col-md-4 col-lg-4" style="margin-right: -2px; text-align: center;">
<div style="padding-top: 10px;">
<!-- <h2>0</h2> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div> <!-- /container -->
<div class="container-fluid" style="max-width: 100%">
<div class="row visible-xs visible-sm">
<div class="col-xs-12 col-sm-12 text-center">
<a href="https://www.instagram.com/artist.a.day" id="instagram_share" title="Follow us on Instagram"><img alt="artistaday instagram" height="40" src="/wp-content/uploads/xshare_instagram.png.pagespeed.ic.YpXe19QUYH.png" width="40"/></a>
<a href="https://www.pinterest.com/artistaday" id="pinterest_share" title="Follow us on Pinterest"><img alt="artistaday pinterest" height="" src="/wp-content/uploads/share_pinterest.png.pagespeed.ce.6FSvqobJnX.png" width=""/></a>
</div>
</div>
<hr/>
<footer>
<div class="row">
<div class="hidden-xs hidden-sm col-md-6 col-lg-6">Follow us: <a href="https://www.pinterest.com/artistaday/">Pinterest</a> | <a href="https://www.instagram.com/artist.a.day/">Instagram</a></div>
<div class="hidden-xs hidden-sm col-md-6 col-lg-6"><p class="pull-right">© 2007-2021 ArtistADay | <a href="/?page_id=3564">Terms</a> | <a href="/?page_id=6786">Privacy Policy</a></p></div>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><p class="text-center">© 2007-2021 ArtistADay | <a href="/?page_id=3564">Terms</a> | <a href="/?page_id=6786">Privacy Policy</a></p></div>
</div>
</footer>
<div class="container-fluid hidden-xs hidden-sm">
<div class="row">
<div class="col-md-12 col-lg-12" style="text-align: center;">
<!-- footer -->
<!--
				<ins class="adsbygoogle"
				     style="display:block"
				     data-ad-client="ca-pub-7963244442939670"
				     data-ad-slot="3406627238"
				     data-ad-format="auto">
				</ins>
-->
<script>(adsbygoogle=window.adsbygoogle||[]).push({});</script>
</div>
</div>
</div> </div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-31089-28"></script>
<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}gtag('js',new Date());gtag('config','UA-31089-28');</script> <!-- Placed at the end of the document so the pages load faster -->
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script src="/wp-content/themes/bootstrap3/js/jquery.js.pagespeed.jm.ycCwN_K0CI.js"></script>
<script src="/wp-content/themes/bootstrap3/js/bootstrap-collapse.js+bootstrap-transition.js+bootstrap-alert.js+bootstrap-dropdown.js+bootstrap-tooltip.js+jquery.autocomplete.min.js+cookie.js.pagespeed.jc.6T7SzCtiE4.js"></script><script>eval(mod_pagespeed_u8zJbqflb$);</script>
<script>eval(mod_pagespeed_VKM_KkYEbz);</script>
<script>eval(mod_pagespeed_FmeJRwr7YJ);</script>
<script>eval(mod_pagespeed_$x1NLXHLwt);</script>
<script>eval(mod_pagespeed_PpvPGF4IHS);</script>
<script>eval(mod_pagespeed_0cbQ6xNWPo);</script>
<script>eval(mod_pagespeed_fKng5TPFBu);</script>
<script>$('#navHome, #navAbout,#navArchives, #navSubmissions, #navContact, #navGallery').hover(function(){$(this).addClass("active");},function(){$(this).removeClass("active");});$('#chrome_share, #gplus_share, #instagram, #facebook_share, #twitter_share, #rss_share, #email_share, #pinterest_share, #mailing_share, #theme_select').tooltip({placement:'bottom'});jQuery(window).bind("load",function(){jQuery("#q").autocomplete('/wp-content/themes/bootstrap3/autocomplete.php',{max:10,scroll:false,width:228}).result(function(event,item){if(item[2]=="artist"){location.href="/?p="+item[1];}else if(item[2]=="page"){location.href="/?page_id="+item[1];}else{location.href="/?page_id=10029&l="+item[0];}});jQuery.cookie("greeting",null);if(jQuery.cookie("greeting")!="off"){var showGreeting=0;var referrer=document.referrer.toLowerCase();if(referrer.indexOf("pinterest.com")!==-1){jQuery('body').append("<div id=\"greeting\" style=\"background-color: #cb2027; color: #ffffff;\"><div class=\"close-button\">Dismiss X</div><img src=\"/wp-content/uploads/pinterest-icon-white.png\" alt=\"Pinterest\" /><p>Looks like you found us through Pinterest. <a href=\"https://www.pinterest.com/artistaday/\" title=\"Pinterest Referrer\" onclick=\"_gaq.push(['_trackEvent', 'Outbound', 'Pinterest referral']);\">Follow our Pinterest account for more great art!</a>.</p></div>");showGreeting=1;}if(referrer.indexOf("facebook.com")!==-1){jQuery('body').append("<div id=\"greeting\" style=\"background-color: #3b5998; color: #ffffff;\"><div class=\"close-button\">Dismiss X</div><img src=\"/wp-content/uploads/facebook-icon-white.png\" alt=\"Facebook\" /><p>Looks like you found us through Facebook. <a href=\"https://www.facebook.com/pages/Artistadaycom/139368439410327\" title=\"Facebook Referrer\" onclick=\"_gaq.push(['_trackEvent', 'Outbound', 'Facebook referral']);\" style=\"text-decoration: underline:\">Like our Facebook page for more great art!</a>.</p></div>");showGreeting=1;}if(referrer.indexOf("usercontent.com")!==-1){jQuery('body').append("<div id=\"greeting\" style=\"background-color: #dd4b39; color: #ffffff;\"><div class=\"close-button\">Dismiss X</div><img src=\"/wp-content/uploads/google-plus-icon-white.png\" alt=\"Google Plus\" /><p>Looks like you found us through iGoogle. <a href=\"https://plus.google.com/108559763812269968349/\" title=\Google Plus Referrer\" onclick=\"_gaq.push(['_trackEvent', 'Outbound', 'Google plus referral']);\">Like our Google+ page for more great art!</a>.</p></div>");showGreeting=1;}if(referrer.indexOf("//t.co")!==-1){jQuery('body').append("<div id=\"greeting\" style=\"background-color: #00aced; color: #ffffff;\"><div class=\"close-button\">Dismiss X</div><img src=\"/wp-content/uploads/twitter-icon-white.png\" alt=\"Twitter\" /><p>Looks like you found us through Twitter. <a href=\"https://twitter.com/artistaday\" title=\Twitter Referrer\" onclick=\"_gaq.push(['_trackEvent', 'Outbound', 'Twitter referral']);\">Follow us @artistaday for more great art!</a>.</p></div>");showGreeting=1;}if(showGreeting==1){jQuery('#greeting').show();jQuery('#greeting').animate({top:'+=60'},1000);jQuery.cookie("greeting","off",{expires:30});}}jQuery(".close-button").click(function(){jQuery("#greeting").hide();jQuery.cookie("greeting","off",{expires:30});});jQuery("#theme_select, #theme_select_mobile").on('click touchstart',function(){var data={u:0,t:new_theme};jQuery.post('./wp-content/themes/bootstrap3/scripts/set_theme.php',data,function(){location.reload();});});});</script>
<script src="/wp-content/themes/bootstrap3/js/jquery.lazyload.unminified.js.pagespeed.jm.jPEKaJqj69.js" type="text/javascript"></script>
<script type="text/javascript">jQuery(document).ready(function(){var img=new Image();img.onload=function(){jQuery('#featured_bg').removeClass("loading").css({"background-image":"url('https://www.artistaday.com/wp-content/uploads/jimdarling_homepage.jpg')","background-repeat":"no-repeat","background-position":"center center","background-size":"cover","-webkit-background-size":"cover","-moz-background-size":"cover","-o-background-size":"cover"});};img.src='https://www.artistaday.com/wp-content/uploads/jimdarling_homepage.jpg';jQuery("#featured_bg").click(function(){window.location.href='https://www.artistaday.com/?p=25647';});jQuery.ajax({type:"POST",url:"/wp-content/themes/bootstrap3/scripts/ranking_list_aad2017.php?n=3",success:function(data){jQuery('#top3').html(data);jQuery("img.lazy").lazyload({effect:"fadeIn",threshold:200,failure_limit:100});}});jQuery("#promo_1, #promo_2").hover(function(){$(this).css("-webkit-filter","");$(this).css("filter","");},function(){$(this).css("-webkit-filter","grayscale(100%)");$(this).css("filter","grayscale(100%)");});});</script>
</body>
</html>
