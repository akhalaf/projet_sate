<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="http://beemartialarts.com/wp1/xmlrpc.php" rel="pingback"/>
<title>About</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="http://beemartialarts.com/wp1/feed/" rel="alternate" title=" » Feed" type="application/rss+xml"/>
<link href="http://beemartialarts.com/wp1/comments/feed/" rel="alternate" title=" » Comments Feed" type="application/rss+xml"/>
<link href="http://beemartialarts.com/wp1/index/feed/" rel="alternate" title=" » About Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/beemartialarts.com\/wp1\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="http://beemartialarts.com/wp1/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C600%2C700%2C800%2C900%7COswald%3A300%2C400%2C600%2C700%7CScada%3A300%2C400%2C600%2C700&amp;ver=5.2.9" id="gravida-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/themes/gravida/style.css?ver=5.2.9" id="gravida-basic-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/themes/gravida/editor-style.css?ver=5.2.9" id="gravida-editor-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/themes/gravida/css/nivo-slider.css?ver=5.2.9" id="gravida-nivoslider-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/themes/gravida/css/main.css?ver=5.2.9" id="gravida-main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/themes/gravida/css/style_base.css?ver=5.2.9" id="gravida-base-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-content/plugins/easy-table/themes/default/style.css?ver=1.8" id="easy_table_style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="http://beemartialarts.com/wp1/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-content/themes/gravida/js/jquery.nivo.slider.js?ver=5.2.9" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-content/themes/gravida/js/custom.js?ver=5.2.9" type="text/javascript"></script>
<link href="http://beemartialarts.com/wp1/wp-json/" rel="https://api.w.org/"/>
<link href="http://beemartialarts.com/wp1/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="http://beemartialarts.com/wp1/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.9" name="generator"/>
<link href="http://beemartialarts.com/wp1/index/" rel="canonical"/>
<link href="http://beemartialarts.com/wp1/?p=2" rel="shortlink"/>
<link href="http://beemartialarts.com/wp1/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fbeemartialarts.com%2Fwp1%2Findex%2F" rel="alternate" type="application/json+oembed"/>
<link href="http://beemartialarts.com/wp1/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fbeemartialarts.com%2Fwp1%2Findex%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">
					.logo h2::first-letter, 
					#content h1.entry-title::first-letter,
					.feature-box a,
					.latest-blog span a,
					.postmeta a:hover, 
					a, 
					#footer .widget-column a:hover, 
					#copyright a:hover,
					.blog-post-repeat .entry-summary a, 
					.entry-content a,
					#sidebar aside h3.widget-title,
					.blog-post-repeat .blog-title a{
						color:#eeee22;
					}
					.site-nav li:hover a, 
					.site-nav li.current_page_item a,
					.nivo-caption h1 a,
					.site-nav li:hover ul li:hover, 
					.site-nav li:hover ul li.current-page-item,
					.site-nav li:hover ul li,
					p.form-submit input[type="submit"],
					#sidebar aside.widget_search input[type="submit"], 
					.wpcf7 input[type="submit"], 
					.add-icon, 
					.phone-icon, 
					.mail-icon,
					.pagination ul li .current, .pagination ul li a:hover{
						background-color:#eeee22;
					}
			</style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style type="text/css">
</style>
<link href="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/cropped-logo-1-2-32x32.png" rel="icon" sizes="32x32"/>
<link href="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/cropped-logo-1-2-192x192.png" rel="icon" sizes="192x192"/>
<link href="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/cropped-logo-1-2-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/cropped-logo-1-2-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="page-template-default page page-id-2 wp-custom-logo">
<div id="wrapper">
<div class="header">
<div class="site-aligner">
<div class="logo">
<a class="custom-logo-link" href="http://beemartialarts.com/wp1/" rel="home"><img alt="" class="custom-logo" height="128" src="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/cropped-beeMartialArts_black-1.jpg" width="250"/></a> <div class="clear"></div>
<h2><a href="http://beemartialarts.com/wp1/"></a></h2>
<p></p>
</div><!-- logo -->
<div class="mobile_nav"><a href="#">Go To...</a></div>
<div class="site-nav">
<div class="menu-menu-1-container"><ul class="menu" id="menu-menu-1"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-43" id="menu-item-43"><a href="http://beemartialarts.com/wp1/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-44" id="menu-item-44"><a aria-current="page" href="http://beemartialarts.com/wp1/index/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192" id="menu-item-192"><a href="http://beemartialarts.com/wp1/calendar/">Classes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="http://beemartialarts.com/wp1/contact/">Contact</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-491" id="menu-item-491"><a href="http://beemartialarts.com/wp1/bee-martial-arts-staff/">Staff</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-532" id="menu-item-532"><a href="http://beemartialarts.com/wp1/sagarmatha-meditation/">BHRAMARA YOGA</a></li>
</ul></div> </div><!-- site-nav --><div class="clear"></div>
</div><!-- site-aligner -->
</div><!-- header -->
<div id="content">
<div class="site-aligner">
<h1 class="entry-title">About</h1>
<div class="entry-content">
<p> </p>
<p><img alt="" class="wp-image-322 alignleft" height="200" src="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/uiu-e1528017962447.jpg" style="background-color: transparent; color: #333333; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; height: 292px; letter-spacing: normal; max-width: 1604.21px; outline: 1px solid #72777c; text-align: left; text-decoration: none; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;" width="219"/></p>
<p><i></i><u></u><strong>ABOUT GRANDMASTER</strong></p>
<p>Grand Master <span style="color: #3366ff;"><em>Shuny Bee</em></span> is a world-renowned martial artist, born and raised in Nepal. He has been training in martial arts since childhood. Technically, martial arts training in Nepal was illegal while he was growing up. Despite this, his dedication, creativity, hard work, determination and skill paid off when his martial arts demonstration was greatly honored and praised by then <em>Royal Prince of Nepal</em>, <strong>Prince Dhirendra Bikram Saha</strong>. And he became an international sensation in movies and onstage, performing, competing and demonstrating his art talent around the world.</p>
<p>Over the years <a href="https://en.wikipedia.org/wiki/Shifu"><span style="color: #333333;"><strong>Shifu</strong></span></a> <span style="color: #3366ff;"><em>Shuny Bee</em> </span>has been featured in numerous issues of <span style="color: #333399;"><a href="https://blackbeltmag.com/?s=bee&amp;submit-search=Search" style="color: #333399;"><strong>Black Belt Magazine</strong></a></span>. His techniques in martial arts has greatly honored by several masters and grand masters from the different arts from different countries.</p>
<p>As Guru Bee continued his journey in the martial arts, he has promoted the martial arts in numerous forms of mass media. He has had leading and supporting roles in movies and television. He has written and produced books and instructional DVDs promoting his art as well.</p>
<h5></h5>
<p> </p>
<p><span style="color: #ff0000;"><a href="https://en.wikipedia.org/wiki/Ted_Wong" style="color: #ff0000;">Ted</a> <a href="https://en.wikipedia.org/wiki/Ted_Wong" style="color: #ff0000;">Wong</a> </span>praising <span style="color: #3366ff;"><a href="http://beemartialarts.com/wp1/index/" style="color: #3366ff;"><em>Shuny</em></a><em> <a href="http://beemartialarts.com/wp1/index/" style="color: #3366ff;">Bee</a></em></span> for his excellence in martial arts and Keeping the good work in the Martial Arts world. Below <a href="https://en.wikipedia.org/wiki/Grandmaster_(martial_arts)"><span style="color: #339966;"><span style="color: #ffcc00;"><span style="color: #e6b919;"><strong>Grand Master</strong></span></span></span> </a><span style="color: #3366ff;"><em><a href="http://beemartialarts.com/wp1/index/" style="color: #3366ff;">Shuny</a> </em><a href="http://beemartialarts.com/wp1/index/" style="color: #3366ff;"><em>Bee</em></a></span> is demonstrating his martial arts style in Long Beach.</p>
<div class="wp-video" style="width: 480px;"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
<video class="wp-video-shortcode" controls="controls" height="320" id="video-2-1" preload="auto" width="480"><source src="http://beemartialarts.com/wp1/wp-content/uploads/2018/07/Shuny-Bee-Demo-Long-Beach-1.mp4?_=1" type="video/mp4"/><a href="http://beemartialarts.com/wp1/wp-content/uploads/2018/07/Shuny-Bee-Demo-Long-Beach-1.mp4">http://beemartialarts.com/wp1/wp-content/uploads/2018/07/Shuny-Bee-Demo-Long-Beach-1.mp4</a></video></div>
<p><b>Accomplishments</b></p>
<p><img alt="" class="alignnone size-full wp-image-458" height="1077" sizes="(max-width: 1586px) 100vw, 1586px" src="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/GrandMaster-ShunnyBee.png" srcset="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/GrandMaster-ShunnyBee.png 1586w, http://beemartialarts.com/wp1/wp-content/uploads/2018/06/GrandMaster-ShunnyBee-300x204.png 300w, http://beemartialarts.com/wp1/wp-content/uploads/2018/06/GrandMaster-ShunnyBee-768x522.png 768w, http://beemartialarts.com/wp1/wp-content/uploads/2018/06/GrandMaster-ShunnyBee-1024x695.png 1024w" width="1586"/></p>
<p> </p>
<p><strong>ABOUT ACADEMY</strong></p>
<p><img alt="" class="alignright wp-image-75" height="153" sizes="(max-width: 481px) 100vw, 481px" src="http://beemartialarts.com/wp1/wp-content/uploads/2016/09/ClassesBanner2.jpg" srcset="http://beemartialarts.com/wp1/wp-content/uploads/2016/09/ClassesBanner2.jpg 1400w, http://beemartialarts.com/wp1/wp-content/uploads/2016/09/ClassesBanner2-300x96.jpg 300w, http://beemartialarts.com/wp1/wp-content/uploads/2016/09/ClassesBanner2-768x245.jpg 768w, http://beemartialarts.com/wp1/wp-content/uploads/2016/09/ClassesBanner2-1024x326.jpg 1024w" width="481"/></p>
<p>Bee Martial Arts has been serving the country, USA since 1998. Since the Academy opened we have produced many national and international gold medalists, amateurs and professional fighters. In addition, some of the black belts from the Academy serve the country in their own fields such as the marines, army, law enforcement, and various professions. We welcome all skill levels and a variety of martial arts backgrounds; from people starting a weight loss regimen, getting in shape, or training to fight in championship as well as to protect them-selves.  BMA has a program for you. Stop by and check out our academy fully equipped with heavy bags, speed bags, grappling dummy, ground and pound bag, throwing mat, weight machines and many more features.</p>
<p> </p>
<p><img alt="" class="wp-image-460 alignleft" height="125" sizes="(max-width: 305px) 100vw, 305px" src="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/Certs-1.jpg" srcset="http://beemartialarts.com/wp1/wp-content/uploads/2018/06/Certs-1.jpg 926w, http://beemartialarts.com/wp1/wp-content/uploads/2018/06/Certs-1-300x123.jpg 300w, http://beemartialarts.com/wp1/wp-content/uploads/2018/06/Certs-1-768x314.jpg 768w" width="305"/>BMA offers <span style="color: #008000;"><strong>Taekwondo</strong></span>, <span style="color: #008000;"><strong>Jeetkunedo</strong></span>, <span style="color: #008000;"><strong>Kali</strong> </span>and <span style="color: #008000;"><strong>weaponry</strong></span>. All the classes are friendly and family oriented where each person feels welcomed and respected. The main goal of Academy is to help each student realize their full potential and train body, mind and spirit to overcome any obstacles they may face in life.</p>
<p>BMA is not only a part of community but the part of school district too. The key concepts of being safe, kind and respectful that is a core philosophy of the local school districts is taught consistently at Academy. Students of the BMA as they graduate through the belt ranking system must also have an approval form for each level signed by both the parents and teachers to move on and this promotes better behavior on the student’s part both at home and school.</p>
<p>BMA also has served from time to time to help out at the different schools in a verity of ways. For example, seminars on <span style="color: #ff0000;"><strong>self defense</strong></span>, reality martial arts for <span style="color: #ff6600;"><strong>street survival</strong></span>, culture awareness, different demonstrations, <strong><span style="color: #ff0000;">physical training</span></strong> for students or teachers and other resources. All our classes are taught under the supervision of Grand Master Shuny Bee. Safety gear and clothing requirements vary by class.</p>
</div><!-- entry-content -->
</div><!-- site-aligner -->
</div><!-- content -->
<footer id="footer">
<div class="site-aligner">
<div class="widget-column">
<div class="cols">
<h2>Bee Martial Arts</h2>
<div class="add-icon"></div><!-- add-icon --><div class="add-content">1139 S. Fair Oaks Ave.
Pasadena, CA 91105</div><!-- add-content --><div class="clear"></div>
<div class="phone-icon"></div><!-- phone-icon --><div class="phone-content">(626) 662-4795</div><!-- phone-content --><div class="clear"></div>
<div class="mail-icon"></div><!-- mail-icon --><div class="mail-content"><a href="mailto:beemartialarts.com@gmail.com">beemartialarts.com@gmail.com</a></div><!-- mail-content --><div class="clear"></div>
</div><!-- cols -->
</div><!-- widget-column -->
<div class="widget-column">
<div class="cols"><h2>Recent Tweets</h2>
<p>Use twitter widget for twitter feed.</p>
</div><!-- cols -->
</div><!-- widget-column -->
<div class="widget-column">
<div class="cols"><h2>Recent Posts</h2>
<ul>
<li><a href="http://beemartialarts.com/wp1/2018/06/03/group-class/">Group Class</a></li>
</ul>
</div><!-- cols -->
</div><!-- widget-column -->
<div class="widget-column last">
<div class="cols">
<h2>Join Us @ Facebook</h2>
<div class="social">
<a href="https://www.facebook.com/beemartialarts/?fref=ts" target="_blank" title="Facebook"><div class="fb icon"></div><span>Facebook</span></a>
</div><!-- social -->
</div><!-- cols -->
</div><!-- widget-column --><div class="clear"></div>
</div><!-- site-aligner -->
</footer>
<div id="copyright">
<div class="site-aligner">
<div class="left"><a href="https://www.sktthemes.net" rel="nofollow" target="_blank">Gravida</a></div>
<div class="right">© Shunny Bee, Bee Martial Arts</div>
<div class="clear"></div>
</div>
</div><!-- copyright -->
</div><!-- wrapper -->
<link href="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1" id="mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.2.9" id="wp-mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/beemartialarts.com\/wp1\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="http://beemartialarts.com/wp1/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
</script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp1\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.2.9" type="text/javascript"></script>
<script src="http://beemartialarts.com/wp1/wp-includes/js/mediaelement/renderers/vimeo.min.js?ver=4.2.6-78496d1" type="text/javascript"></script>
</body>
</html>
<!-- Page generated by LiteSpeed Cache 2.9.8.5 on 2021-01-14 03:38:43 -->