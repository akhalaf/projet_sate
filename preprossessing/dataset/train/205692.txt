<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-51128427-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-51128427-2');
</script>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="BeeDictionary.com" name="author"/>
<title>Definition of image_scanner | Bee Dictionary of English</title>
<meta content="Find the dictionary definition of image_scanner from Bee English Dictionary along with phonetics, audio, usages and articles related to image_scanner" name="description"/>
<meta content="image_scanner definition, meaning of image_scanner, dictionary meaning of image_scanner,definition of image_scanner" name="keywords"/>
<link href="/static/icon/favicon.ico" rel="icon"/>
<!-- Bootstrap core CSS -->
<!--link rel="stylesheet" href="/static/plugins/bootstrap-3.3.4/css/bootstrap.min.css" type='text/css'-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/static/css/style.css" rel="stylesheet" type="text/css"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>

          <script src="/static/js/html5shiv.min.js"></script>

          <script src="/static/js/respond.min.js"></script>

        <![endif]-->
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7157555391357793",
          enable_page_level_ads: true
     });
</script>
</head>
<body>
<header>
<!-- Preloader >

  <div id="preloader">
    <div id="status">
      <div class='spinner-wrap'>
        <div class='leftside'></div>
        <div class='rightside'></div>
        <div class='spinner'>
          <div class='bounce1'></div>
          <div class='bounce2'></div>
          <div class='bounce3'></div>
        </div>
      </div>
    </div>
  </div-->
<!-- Fixed navbar -->
<nav class="navbar main-menu navbar-default navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
<a class="navbar-brand" href="/" title="logo"><img alt="logo" src="/static/images/logo.png"/></a> </div>
<div class="navbar-collapse collapse pull-left">
<ul class="nav navbar-nav menu" id="menu">
<li><a href="/">Home</a></li>
<li><a href="/idioms">Idioms</a></li>
<li><a href="/common-errors">Common Errors</a></li>
<li><a href="https://blog.beedictionary.com">Blog</a></li>
<li class="dropdown"><a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">About<span class="caret"></span></a>
<ul class="dropdown-menu" role="menu">
<li><a href="/about/faq">FAQ</a></li>
<li><a href="/about/privacy">Privacy Policy</a></li>
<li><a href="/about/terms">Terms of Use</a></li>
<li><a href="/about/contact">Contact</a></li>
</ul>
</li>
</ul>
</div>
<ul class="nav navbar-nav navbar-right menu social-icons">
<li class="dropdown mega-dropdown search"> <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button"><i class="fa fa-search"></i></a>
<div class="dropdown-menu">
<div class="container">
<div class="row">
<div class="col-md-6 pull-right">
<div class="mega-dropdown-menu">
<form action="search" class="search-form" method="get">
<div class="form-group">
<div class="col-lg-12">
<div class="input-group">
<input autocapitalize="off" autocomplete="off" autocorrect="off" class="form-control" name="q" placeholder="Search for..." spellcheck="false" type="text"/>
<span class="input-group-btn">
<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
</span> </div>
<!-- /input-group -->
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</li>
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="https://twitter.com/wordofthisday" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-user"></i></a></li>
</ul>
<!--/.nav-collapse -->
</div>
</nav>
</header>
<section class="content">
<div class="container">
<h2 class="heading">image scanner :<span class="hidden-xs hidden-sm"> Definition, Usages, News and More</span></h2>
<div class="row">
<div class="col-md-8 col-lg-8">
<div class="panel panel-default theme-panel">
<div class="panel-heading">Search Words</div>
<div class="panel-body">
<form action="https://www.beedictionary.com/search" class="search-form" method="post">
<input name="csrfmiddlewaretoken" type="hidden" value="73vHZ4eihZ6SzoxV8XDJxTaus0GkW64PqhOoOaMT40szR5DT8HIA4LNBU2fxcRp5"/>
<div class="input-group">
<input autocapitalize="off" autocomplete="off" autocorrect="off" class="form-control" id="search-input" name="query" placeholder="Search for..." required="" spellcheck="false" type="text"/>
<span class="input-group-btn">
<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
</span> </div>
<div class="btn-group" data-toggle="buttons" style="margin-top: 5px">
<label class="btn btn-primary active">
<input autocomplete="off" checked="" id="search" name="options" type="radio" value="search"/> Search
                    </label>
<label class="btn btn-primary">
<input autocomplete="off" id="browse" name="options" type="radio" value="browse"/> Browse
                    </label>
</div>
<span>You can search or browse for words</span>
</form>
</div>
</div>
<article class="post">
<!-- Responsive_display -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7157555391357793" data-ad-format="auto" data-ad-slot="1962569192" style="display:block"></ins>
<script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
<div class="caption">
<div style="clear: both">
<h3 style="float: left;display:inline">image scanner <audio id="player" src="https://s3.amazonaws.com/vd.beedictionarycom/audio/image_scanner-1339e.mp3"></audio><i class="fa fa-volume-up" onclick="document.getElementById('player').play()"></i></h3>
</div>
<hr style="clear:both;"/>
<ul class="list-unstyled">
<li><span class="badge badge-info">n</span>  an electronic device that generates a digital representation of an image for data input to a computer
                    
                    </li>
</ul>
<div class="post-category"> <a href="#"><span> </span> Synonym(s)</a> </div>
<ul class="list-inline tags">
<li><a href="https://www.beedictionary.com/definition/scanner">scanner</a></li>
<li><a href="https://www.beedictionary.com/definition/digital_scanner">digital scanner</a></li>
</ul>
</div>
<!-- Responsive_display -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7157555391357793" data-ad-format="auto" data-ad-slot="1962569192" style="display:block"></ins>
<script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
</article>
<article class="post">
<div class="caption">
<h5>News &amp; Articles</h5>
<hr/>
<div class="post-category"> <a href="#"><span> </span> image scanner</a> </div>
<ul class="list-unstyled">
<li><b><a href="http://electronista.feedsportal.com/c/34342/f/625515/s/322c193d/sc/4/l/0L0Smacnn0N0Carticles0C130C10A0C0A70Cnew0Bunit0Bprovides0Bsoftware0Bcorrection0Bof0Bbinding0Bimage0Bdistortion0C/story01.htm" target="_blank">New Fujitsu scanner digitizes spine-bound books, magazines</a></b><br/> Image scanner manufacturer Fujitsu today introduced the newest addition to its ScanSnap product lineup, the ScanSnap SV600, featuring a new technique to quickly and easily digitize bound, fragile and large documents . While still maintaining the ScanSnap goal of providing one-button scanning and advanced imaging technology, the SV600 utilizes its compact scanning design to turn hard-to-scan ...
                            <br/><i style="margin-left: 1em;font-size: 12px">Oct. 8, 2013 - MacNN</i>
</li>
</ul>
</div>
</article>
<div class="pagination-wrap">
<ul class="pagination">
<li><a href="https://www.beedictionary.com/definition/a">A</a></li>
<li><a href="https://www.beedictionary.com/definition/b">B</a></li>
<li><a href="https://www.beedictionary.com/definition/c">C</a></li>
<li><a href="https://www.beedictionary.com/definition/d">D</a></li>
<li><a href="https://www.beedictionary.com/definition/e">E</a></li>
<li><a href="https://www.beedictionary.com/definition/f">F</a></li>
<li><a href="https://www.beedictionary.com/definition/g">G</a></li>
<li><a href="https://www.beedictionary.com/definition/h">H</a></li>
<li><a href="https://www.beedictionary.com/definition/i">I</a></li>
<li><a href="https://www.beedictionary.com/definition/j">J</a></li>
<li><a href="https://www.beedictionary.com/definition/k">K</a></li>
<li><a href="https://www.beedictionary.com/definition/l">L</a></li>
<li><a href="https://www.beedictionary.com/definition/m">M</a></li>
<li><a href="https://www.beedictionary.com/definition/n">N</a></li>
<li><a href="https://www.beedictionary.com/definition/o">O</a></li>
<li><a href="https://www.beedictionary.com/definition/p">P</a></li>
<li><a href="https://www.beedictionary.com/definition/q">Q</a></li>
<li><a href="https://www.beedictionary.com/definition/r">R</a></li>
<li><a href="https://www.beedictionary.com/definition/s">S</a></li>
<li><a href="https://www.beedictionary.com/definition/t">T</a></li>
<li><a href="https://www.beedictionary.com/definition/u">U</a></li>
<li><a href="https://www.beedictionary.com/definition/v">V</a></li>
<li><a href="https://www.beedictionary.com/definition/w">W</a></li>
<li><a href="https://www.beedictionary.com/definition/x">X</a></li>
<li><a href="https://www.beedictionary.com/definition/y">Y</a></li>
<li><a href="https://www.beedictionary.com/definition/z">Z</a></li>
</ul>
</div>
<div class="clearfix"></div>
</div>
<aside class="col-md-4 col-lg-4">
<div class="row">
<div class="col-sm-6 col-md-12 col-lg-12">
<!-- Large_Rectange_336x280 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-7157555391357793" data-ad-slot="8428628289" style="display:inline-block;width:336px;height:280px"></ins>
<script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
</div>
<div class="col-sm-6 col-md-12 col-lg-12">
<div class="panel panel-default theme-panel">
<div class="panel-heading">From the Blog</div>
<div class="panel-body nopadding">
<div class="media">
<div class="media-left"> <a href="#"> <img alt="author" src="/static/images/post/rp1.jpg"/> </a> </div>
<div class="media-body">
<h4 class="media-heading"><a href="#">Interesting words in the English language</a></h4>
<p><a href="#">English</a> • July 11</p>
</div>
</div>
<div class="media">
<div class="media-left"> <a href="#"> <img alt="author" src="/static/images/post/rp2.jpg"/> </a> </div>
<div class="media-body">
<h4 class="media-heading"><a href="#">For a Great Vocabulary, have a Healthy Heart!</a></h4>
<p><a href="#">Vocabulary</a> • June 27</p>
</div>
</div>
<div class="media">
<div class="media-left"> <a href="#"> <img alt="author" src="/static/images/post/rp3.jpg"/> </a> </div>
<div class="media-body">
<h4 class="media-heading"><a href="#">Meaning of existential and its usage</a></h4>
<p><a href="#">Meanings</a> • 11 March</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6 col-md-12 col-lg-12">
<div class="panel panel-default theme-panel">
<div class="panel-heading">Nearby Definitions</div>
<div class="panel-body nopadding">
<div class="list-group">
<a class="list-group-item" href="https://www.beedictionary.com/definition/imagery">imagery</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_being">imaginary being</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_creature">imaginary creature</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_number">imaginary number</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_part">imaginary part</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_part_of_a_complex_number">imaginary part of a complex number</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginary_place">imaginary place</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imagination">imagination</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imagination_image">imagination image</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginative_comparison">imaginative comparison</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaginativeness">imaginativeness</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaging">imaging</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imagism">imagism</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imago">imago</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imam">imam</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imamu_amiri_baraka">imamu amiri baraka</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaret">imaret</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imaum">imaum</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imavate">imavate</a>
<a class="list-group-item" href="https://www.beedictionary.com/definition/imbalance">imbalance</a>
</div>
</div>
</div>
</div>
</div>
</aside>
</div>
<div class="row">
<div class="col-md-12">
<div class="bgParallax" data-speed="15" id="parallax-1">
<div class="color-overlay black">
<h3><span>Bee Dictionary:</span> Search, browse, look at common errors, idioms and more.</h3>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Footer -->
<footer>
<div class="container">
<div class="row footer-top">
<div class="col-sm-4 footer-box">
<h4>About us</h4>
<div class="footer-box-text">
<p>BeeDictionary.com is an Online American English Dictionary with intuitive browsing interface. The dictionary has very useful other features like, full definition, audio, IPA and spelled phonetics...</p>
<p><a href="/about/faq">Read more...</a></p>
</div>
</div>
<div class="col-sm-4 footer-box">
<h4>Links</h4>
<div class="footer-box-text">
<p><a href="/common-errors">Common Errors</a></p>
<p><a href="/idioms">Idioms</a></p>
<p><a href="#">Crossword</a></p>
</div>
</div>
<div class="col-sm-4 footer-box">
<h4>Legal</h4>
<div class="footer-box-text">
<p><a href="/about/privacy">Privacy Policy</a></p>
<p><a href="/about/terms">Terms of Use</a></p>
<p><a href="/about/faq">FAQ</a></p>
<p><a href="/about/contact">Contact</a></p>
</div>
</div>
</div>
<div class="row footer-bottom">
<div class="col-sm-7 footer-copyright">
<p>© 2018 <a href="https://www.beedictionary.com">BeeDictionary.com</a></p>
</div>
<div class="col-sm-5">
<ul class="list-inline footer-social">
<li><a class="facebook" href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a class="twitter" href="https://twitter.com/wordofthisday" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a class="google-plus" href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a class="pinterest" href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
</ul>
</div>
</div>
</div>
</footer>
<a class="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
<!-- Bootstrap core JavaScript

        ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--script src="/static/js/jquery-1.11.2.min.js"></script-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- bootstrap -->
<!--script src="/static/plugins/bootstrap-3.3.4/js/bootstrap.min.js"></script-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="/static/js/modernizr.js"></script>
<!-- owl-carousel -->
<script src="/static/plugins/owl-carousel/owl.carousel.js"></script>
<!-- masonry -->
<script src="/static/plugins/masonry/masonry.min.js"></script>
<!-- Form Validation -->
<script src="/static/plugins/jquery-validate/jquery.validate.min.js"></script>
<!-- custom -->
<script src="/static/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
  // use this hash to cache search results
  window.query_cache = {};
  $('#search-input').typeahead({
      source:function(query,process){
          // if in cache use cached value, if don't wanto use cache remove this if statement
          two_chr = query.substring(0,2)
          if(query_cache[two_chr]){
              process(query_cache[two_chr]);
              return;
          }
          if( typeof searching != "undefined") {
              clearTimeout(searching);
              process([]);
          }
          searching = setTimeout(function() {
          two_chr = query.substring(0,2);
              return $.getJSON(
                  "https://www.beedictionary.com/static/auto/"+two_chr+".txt",
                  { q:query },
                  function(data){
                      // save result to cache, remove next line if you don't want to use cache
                      query_cache[query] = data;
                      // only search if stop typing for 100ms aka very fast typers
                      return process(data);
                  }
              );
          }, 100); // 100 ms
      }
  });
});
</script>
</body>
</html>