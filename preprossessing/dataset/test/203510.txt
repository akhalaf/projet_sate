<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "25684",
      cRay: "61107fff7f6baa90",
      cHash: "9a89e1c70dec4f3",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2FsaWZvcm5pYWJlYWNoZXMuY29tL2F0dHJhY3Rpb25zLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "iv2pKKjty9ucll+JYDb7KeAG60E47cBRqFYulYrFHCCaEC9i7JVYCkgPUDrOLyvwrIjXUxJMlvr2OS12IkFzIDpbZSzcZwUH8jSuZ3RfoJCyqUAmDMRijPcZWs5MfLLdpGL+xxu2xQn+Z5XTK4gkMwQPvFifda2EKC090V8l0EVjOGyJnSATRkXh4KO+lUgLhDJQQWW8x9UfywO+qzjZPga2RbQV7uzusY0JM+gjLZME1LQRHL7vSqYNb5Yq64MWbGQbUqDF0iKzbH+kaQZFf13g8j/akfG3sNm7IjqlwcU0isAnZbfpaaR+UU80+jrGS4K5wz+SvJG7U8H67QRlUm/h7kV0Z+yo/vl3gdEzhAgrjxYrTY5aESE1W5bsFzMOD6CblzsKm28aa8SYy1OvZoXgNziSvcrD/Ro40Ek6OrQmBkJPgWetUnABFKab/Upkdp1H1bbHPxTrtocWQCT5H3jBj21xBs1oDYjs47JflpB8yzAb04KvYoYwZe1cnoVlOOvOX3xvuhvEK/WLSHCZpni0TCC0YT/xxvmrKquhvdO+akGBsmtx5KIiafvy3y57yMCYmfCBVLGCC1fHMofrAkyo5KR34zPiO7k9eCHqT4M3OHUHsAhGRa5kfoXQOtWOhZZ3d/Wcrpw+/yV2ZLUVUXpNPQhhzmmYZBBueCVERAbrz6UvDXrfV/PXonfcURbW8pZckdthFl+GokyhxgM/LttEhoPgV1c7GRLldi+/MPj0FoGX4uOGWXZu/H9WcHsp",
        t: "MTYxMDU1NTQwNy4yNzUwMDA=",
        m: "Wg3KwtiB+GQtCf+0n/hJzlFQb9uySoziXXZE951YNYw=",
        i1: "UsFVfw9rPd0PQO6+lch6tQ==",
        i2: "tWSOEN3zXslQDoOF80jtWw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "MO0dV88updyJRe38MLNudPbYbUmrnR2k1jGkljkVG24=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=61107fff7f6baa90");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> californiabeaches.com.</h1>
<a href="https://tornado-networks.com/lupineabbey.php?goto=465" style="position: absolute; top: -250px; left: -250px;"></a>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/attractions/?__cf_chl_jschl_tk__=9f0bd4f94fb7cc09f563991990c12fff2be0ffff-1610555407-0-AaBdl4-sX2iVnvkF9xx7ZcbiGmpSx3bfVd9VXYgle8BHcVprokKWGHdQRls6h7NZBSVbFsFnA73HZt1qAIMXKxkLeyh298G9ig0EDcFQZ8TiOyWkrOjlzKrYnfUT4cJ7DX6iKhkYy10s4hytZxBF3trb8tTvRQaJe7Q_fE9PKlNmtE0vfrEO7NgDEEdeEnL3k6nP_0dMF7IRQjHZpkuHprbr1cIWNY0TxAP68vZgx9B0EHDzYgAxm0zffoCw-JfNs_Nx9oT1OncMrKAbdHjuM2OUfHmsLgT0pWVAl9g6qoLdGb9b8dUcQUvWeZqanOwRJA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="db64537fda76b460e56e137ef2c5be6366937367-1610555407-0-AUzphDOZTFlxu/YAi8+56yNRpKzm2WshPARR5FBzXBhai3O5kCxfhTZ9aDXVh/2vWXWo1InhMJSJ2pI6kDeDnkuZzIlrZLyvn4kF0HMvEYsSTkv5LPuDhMoBOpBYjsnbRtSze/ECPPdAKNA3nYvwJKfzBpUD2u+J3+eRHTzMeyEL78yw8nfekkJzmMQKTHhywkJz2hzH5UUJNyZHe6ZbZIBrQ3vboIiPGpuoWKlIO5HO83Z/O/Gb8+yjFU3uhWUaHBd5yzKt3SZTtj3TzVu/85H+0q+pu0hGSw20JmLRhUueW17SgiPopvLEt6uPCnmURfQbzJCbZqXK/J96HIX5iqsPpRkU+tG1xb+IbVI/h1RSSW3vbcMq4Cco6dgozbSy+KJld9bTW20T8Y1RfdAOiPObI9KdHKBmQ91A4S0gunuhqtBzrC3vCwwErDbAbjMZeWOcHdmhvnSm6Tx4TyeIYb8dlRvs7PXF28xmfpN/pp6oc0Q8NmWOXcS5geNIAdIlUJ9eohuU5dUgMrQHrz6fk4YCDfk7k108e9ZEDPldE48bTFH+e1Fg39mBD5AJNfqDZ975wmA9DyDfpHWeYUX/5oJEXXtgVFrdoUsQ0By5vtuBbcgAAkjFT1a054YBII7mLs2m+qokd8eXjxAn8s625sx7D7FpFljTZbSrhhU67Yg6ynoteOL6kp3i+RT2W8i+PXuv8ohpiNh66XnhpxuylnAdeDgCNShRlQAQb+vOp7rr/bmzBD3o8Gf2Xe6RZsDTMIU1qXVVXTOls7uPHtn7f6wSDUDKs7c7Gh28IrGx9rkqaxgNOuWJB539s5JDdXsG87EARBe32UedD2fr7kuG6FoqW/1PpxpXcqTVDquD6xmjUQpa2tPEjMy5TFWDhi7RugU6ZJ5xWazY6vQczecoYkvF8ExGMPbWWkJmucHUC6h1N03AjjSpHztzsqvft2JkX5DZDn7EzQcUqnwz36Abd2wZDoVmqwTFjyq9qDp72LEvrzwvvYamiLcIEh8LVL0K74++rHvT4bmkmjtB+o1ehOHxx1ejh7+p1wDWVqFIdET9nxPovb0R8sq/iW+H2hWlceQAmsNoZtRNd1E1SWLo5YAkK/WEpqZLXzZUFcSIFvzDMlDR8vN5UTSGAWzlGiFywxLjiS1l7RPdlQlOc+MBEmv/J4aru4fsY6f1ucLnqjRUwTh5WfLeL11SwKFaRR2R3FyHRRHxh5rq8/DtF4H2nCcGux9vis3FqJ+BG+RMT/sm0dNSUmxCZWELS6ePyCHQNcC3B9lMYpfdfCs+NXLuJtc7m6RND3m1nY22dzHFadTkZZFgKfAVRbLLxIgr8gVCBX87KNpC4a1GkJXlRxGYrEw="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="5dcb98bf5174332be554ba1efb3575ea"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610555411.275-u311XNS1bB"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=61107fff7f6baa90')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>61107fff7f6baa90</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
