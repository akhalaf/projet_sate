<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="АНО Центр экспертных исследований рынка" name="author"/>
<meta content="Изучение потребительской активности, интеграция общественного и экспертного мнений рамками web-опросов, B2B" name="description"/>
<meta content="подработка на дому, дистанционная работа, платные опросы, опросы за деньги, анкетер, анкетер.орг, маркетинговые исследования, потребительская активность, соцопросы, web-опросы, online-опросы, заказ опроса, заказ исследования, online panel" name="keywords"/>
<title>Вы отвечаете — мы платим :: anketer.org :: подработка на опросах</title>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/custom.css?v=1" rel="stylesheet"/>
<style type="text/css">
      body {
        padding-top: 60px;
      }
</style>
<link href="css/bootstrap-responsive.css" rel="stylesheet"/>
<link href="favicon.ico?v=1" rel="shortcut icon"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.placeholder.min.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script type="text/javascript">

Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    odnoklassniki: function(purl, text) {
        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
        url += '&st.comments=' + encodeURIComponent(text);
        url += '&st._surl='    + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    mailru: function(purl, ptitle, pimg, text) {
        url  = 'http://connect.mail.ru/share?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&imageurl='    + encodeURIComponent(pimg);
        Share.popup(url)
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};

function add_err(cls)
{
   del_suc(cls);
   $(cls).addClass("error");
}

function del_err(cls)
{
   $(cls).removeClass("error");
}

function add_suc(cls)
{
   del_err(cls);
   $(cls).addClass("success");
}

function add_norm(cls)
{
   del_err(cls);
   del_suc(cls);
}


function del_suc(cls)
{
   $(cls).removeClass("success");
}


function add_alert_block1(msg, alrt)
{
   $("#alrt_block1").removeClass("alert-success").removeClass("alert-error").addClass(alrt);
   $("#alert1").html(msg);
   $("#alrt_block1").show();
}

function add_alert_block2(msg, alrt)
{
   $("#alrt_block2").removeClass("alert-success").removeClass("alert-error").addClass(alrt);
   $("#alert2").html(msg);
   $("#alrt_block2").show();
}

function repaint_(c)
{
   var d = new Date();
   $(c).attr("src", "captcha0?"+d.getTime());
}

function scrl_(dd)
{
        $("html,body").animate({scrollTop: $(dd).offset().top-60},'slow');
}

function add_about() 
{
        $("#faqdiv").show();
        $("#foo").hide();
        scrl_("#faqdiv");
}

function add_agree() 
{
        $("#agree").show();
        $("#foo").hide();
        scrl_("#agree");
}

function add_recov()
{
       repaint_("#cpt2");
       $("#recovform").show();
       $("#regform").hide();  
       $("#alrt_block1").hide();
       scrl_("#ttop");
}

function del_recov()
{
       $("#recovform").hide();
       repaint_("#cpt");
       $("#regform").show();
       $("#alrt_block1").hide();
}

function add_send()
{
        var v1 = $("#u_email").val();
        var v2 = $("#u_eml").val();
        var v3 = $("#u_fam").val();
        var v4 = $("#u_nam").val();
        var em = v1;
        if (jQuery.trim(em) == "") {em = v2;}
        $("#textsend").val('Ваше имя: '+v4+' '+v3+'\n'+'Ваш Email: '+em+'\nСообщение: ');
        
        $("#sendform").show();
        $("#alrt_block2").hide();
        scrl_("#sendform");
        $("#textsend").focus();
}

function del_send()
{

       $("#alrt_block2").hide();
       $("#sendform").hide();
       scrl_("#ttop");
}

function del_agree()
{
       $("#agree").hide();
       $("#foo").show();
       scrl_("#ttop");
}

function del_about()
{
       $("#faqdiv").hide();
       $("#foo").show();
       scrl_("#ttop");
}


jQuery.preloadImages = function () 
{
    var images = (typeof arguments[0] == "object") ? arguments[0] : arguments;
    for (var i = 0; i < images.length; i++) 
    {
        jQuery("<img>").attr("src", images[i]);
    }
}


$(document).ready(function () 
{
 $.preloadImages('img/load.gif');

 $("#regform").submit(function(e)
 {
  e.preventDefault();
  var fam_ = $("#u_fam").val();
  var nam_ = $("#u_nam").val(); 
  var eml_ = $("#u_eml").val();
  var rep_ = $("#u_rep").val();
  var cod_ = $("#u_cod").val();
  $("#load1").show();
  
  $.post("reg_", { fam: fam_, nam: nam_, eml: eml_, rep: rep_, cod: cod_ },
  function(data) {
   if (data[5]=="1")
   {
       add_norm("#d_fam"); $("#u_fam").val("");  
       add_norm("#d_nam"); $("#u_nam").val("");  
       add_norm("#d_eml"); $("#u_eml").val("");  
       add_norm("#d_rep"); $("#u_rep").val("");  
       add_norm("#d_cod"); $("#u_cod").val("");  
   }
   else
   {
       if (data[0]=="2") {add_err("#d_fam");} else {add_suc("#d_fam");}
       if (data[1]=="2") {add_err("#d_nam");} else {add_suc("#d_nam");}
       if (data[2]=="2") {add_err("#d_eml");} else {add_suc("#d_eml");}
       if (data[3]=="2") {add_err("#d_rep");} else {add_norm("#d_rep");}
       if (data[4]=="2") {add_err("#d_cod");} else {add_norm("#d_cod");}
   }
   switch (data[5])
   {
      case '1': add_alert_block1("<strong>Поздравляем!</strong> Вы успешно зарегистрированы в базе ANKETER.ORG. Пароль от Вашего личного кабинета будет вскоре выслан на электронный ящик", "alert-success"); 
                repaint_("#cpt"); 
                
                break;
      case '2': add_alert_block1("Ошибка ввода регистрационных данных", "alert-error"); break;
      case '4': add_alert_block1("Респондент c таким Email уже зарегистрирован", "alert-error"); break;
      case '5': add_alert_block1("Ошибка сервера", "alert-error");
                $("#u_cod").val("");
                break;
   }
  }).fail(function() { add_alert_block1("Ошибка канала Интернет", "alert-error"); }).always(function() { $("#load1").hide(); });

  scrl_("#ttop");

 });


 $("#recovform").submit(function(e)
 {
  e.preventDefault();   
  var eml_ = $("#u_eml_r").val();
  var cod_ = $("#u_cod_r").val();
  $("#load2").show();

  $.post("recov_", { eml: eml_, cod: cod_ },
  function(data) { 
 
   if (data[2] == '1')
   {
       add_norm("#d_eml_r"); $("#u_eml_r").val("");  
       add_norm("#d_cod_r"); $("#u_cod_r").val("");  
   }
   else
   {
      if (data[0]=="2") {add_err("#d_eml_r");} else {add_suc("#d_eml_r");}
      if (data[1]=="2") {add_err("#d_cod_r");} else {add_suc("#d_cod_r");}
   }
   switch (data[2])
   {
      case '1': add_alert_block1("Пароль от Вашего личного кабинета будет вскоре выслан на электронный ящик", "alert-success"); 
                repaint_("#cpt2");
                break;
      case '2': add_alert_block1("Ошибка ввода данных для восстановления пароля", "alert-error"); break;
      case '4': add_alert_block1("Респондент c таким Email не зарегистрирован", "alert-error"); break;
      case '5': add_alert_block1("Ошибка сервера", "alert-error"); 
                $("#u_cod_r").val("");
                break;
      case '8': add_alert_block1("Email не включен в базу ANKETER.ORG или заблокирован", "alert-error"); break;
      case '9': add_alert_block1("Суточный лимит числа попыток восстановления пароля для указанного Email исчерпан", "alert-error"); break;
   }
  }).fail(function() { add_alert_block1("Ошибка канала Интернет", "alert-error"); }).always(function() { $("#load2").hide(); });

  scrl_("#ttop");

 });


 $("#sendform").submit(function(e){
 
  e.preventDefault();   
  var msg_ = $("#textsend").val();
  $("#load3").show();
  $.post("send_", { msg: msg_ },
  function(data) {
   switch (data)
   {
      case '1': add_alert_block2("Сообщение доставлено администратору", "alert-success"); break;
      default : add_alert_block2("Сообщение не доставлено", "alert-error"); break;
   }
  }).fail(function() { add_alert_block2("Ошибка канала Интернет", "alert-error"); }).always(function() { $("#load3").hide(); });

  scrl_("#alert2");

 });

 
 $('#u_email,#u_pass').placeholder();

});
 

</script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<!-- Fav and touch icons -->
<link href="img/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="img/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="img/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="img/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="brand" href="#"><img alt="anketer.org" src="img/logo.png" style="max-width:100px; margin-top: -4px;"/></a>
<div class="nav-collapse collapse">
<ul class="nav">
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Навигация <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="javascript:add_about();">Вопрос — ответ</a></li>
<li class="divider"></li>
<li><a href="javascript:add_agree();">Cоглашение на участие в опросах</a></li>
<li><a href="payment_order#bottom">Выплаты</a></li>
<li><a href="blog_1">Наш блог</a></li>
<li><a href="http://twitter.com/#!/anketer_org">Новости</a></li>
<li class="divider"></li>
<li><a href="order">Заказ опроса</a></li>
<li class="divider"></li>
<li><a href="javascript:add_send();">Обратная связь</a></li>
</ul>
</li>
<li></li>
</ul>
<!--noindex-->
<form action="go_dom" class="navbar-form pull-right" id="loginform" method="post" name="loginform">
<input autocomplete="off" class="span2" id="u_email" name="u_email" placeholder="Email" style="background:white;width:150px;" tabindex="7" type="text"/>
<input autocomplete="off" class="span2" id="u_pass" name="u_pass" placeholder="Пароль" style="background:white;width:150px;" tabindex="8" type="password"/>
<button class="btn btn-primary" tabindex="9" type="submit">Кабинет <i class="icon-white icon-user"></i></button>
<a href="javascript:void(0);" onclick="add_recov();">
<button class="btn btn-inverse" type="button">Забыли пароль? <i class="icon-white icon-user"></i></button>
</a>
</form>
<!--/noindex-->
</div>
</div>
</div>
</div>
<br/>
<div class="container">
<div class="row">
<div class="span6">
<h2>Вы отвечаете — мы платим</h2>
<br/>
<img alt=" " src="img/anketer.org.this_picture_painted_by_lemon5ky.jpg" width="428"/>
<br/>
<br/>
<br/>
<blockquote>  <p>Нас  <span class="site-counter-value"><span class="site-counter-number number-2"></span><span class="site-counter-number number-5"></span><span class="site-counter-number number-8"></span><span class="site-counter-number number-8"></span><span class="site-counter-number number-4"></span><span class="site-counter-number number-4"></span></span>. Мы ценим Вашу уникальность.</p></blockquote>
<br/>
</div>
<div class="span6" id="ttop">
<div class="alert " id="alrt_block1" style="display: none;">
<span id="alert1"></span>
</div>
<div class="well clearfix">
<form class="form-horizontal" id="regform" name="regform">
<fieldset>
<legend>Ваша регистрация в качестве респондента</legend>
<!--noindex-->
<div class="control-group" id="d_fam">
<label class="control-label" for="u_fam">Фамилия</label>
<div class="controls">
<input class="" id="u_fam" name="u_fam" placeholder="Иванов" tabindex="1" type="text"/>
</div>
</div>
<div class="control-group" id="d_nam">
<label class="control-label" for="u_nam">Имя, отчество</label>
<div class="controls">
<input class="" id="u_nam" name="u_nam" placeholder="Иван Иванович" tabindex="2" type="text"/>
</div>
</div>
<div class="control-group" id="d_eml">
<label class="control-label" for="u_eml">Email</label>
<div class="controls">
<input class="" id="u_eml" name="u_eml" placeholder="ivanov@gmail.com" tabindex="3" type="text"/>
</div>
</div>
<div class="control-group" id="d_rep">
<label class="control-label" for="u_rep">Повторите Email</label>
<div class="controls">
<input autocomplete="off" class="" id="u_rep" name="u_rep" placeholder="ivanov@gmail.com" tabindex="4" type="text"/>
</div>
</div>
<div class="control-group" id="d_cod">
<label class="control-label" for="u_cod">Код <img alt=" " id="cpt" src="captcha0"/>
<a href="javascript:void(0);" onclick="repaint_('#cpt');">
<button class="btn btn-mini" type="button"><i class="icon-black icon-refresh"></i></button>
</a>
</label>
<div class="controls">
<input class="" id="u_cod" name="u_cod" tabindex="5" type="text"/>
</div>
</div>
<div class="control-group">
<label class="control-label"></label>
<div class="controls">
<a href="javascript:add_agree();">Соглашение на участие в опросах</a>
</div>
</div>
<div id="m1"></div>
<div class="form-actions">
<button class="btn btn-large btn-success" tabindex="6" type="submit">Регистрация <i class="icon-white icon-user"></i></button>
<img alt="" id="load1" src="img/load.gif" style="display: none;"/>
</div>
<!--/noindex-->
</fieldset>
</form>
<form class="form-horizontal" id="recovform" name="recovform" style="display: none;">
<fieldset>
<legend>Восстановление пароля <a href="javascript:del_recov();">« свернуть</a></legend>
<!--noindex-->
<div class="control-group" id="d_eml_r">
<label class="control-label" for="u_eml_r">Email</label>
<div class="controls">
<input autocomplete="off" class="" id="u_eml_r" name="u_eml_r" placeholder="ivanov@gmail.com" tabindex="10" type="text"/>
</div>
</div>
<div class="control-group" id="d_cod_r">
<label class="control-label" for="u_cod_r">Код <img alt=" " id="cpt2" src="captcha0"/>
<a href="javascript:void(0);" onclick="repaint_('#cpt2');">
<button class="btn btn-mini" type="button"><i class="icon-black icon-refresh"></i></button>
</a>
</label>
<div class="controls">
<input class="" id="u_cod_r" name="u_cod_r" tabindex="11" type="text"/>
</div>
</div>
<div class="form-actions">
<button class="btn btn-large btn-success" tabindex="12" type="submit"><i class="icon-white icon-envelope"></i> Выслать пароль</button>
<img alt="" id="load2" src="img/load.gif" style="display: none;"/>
</div>
<!--/noindex-->
</fieldset>
</form>
</div>
</div>
</div>
<br/>
<h3>Как это работает?</h3>
<hr/>
<span class="badge badge-warning">1</span>
 Вы регистрируетесь в качестве респондента <i class="icon-black icon-user"></i> Респондентами могут выступать граждане России, проживающие на территории РФ. <br/><br/>
<span class="badge badge-warning">2</span>
 Вы получаете email-письмо с паролем от личного кабинета и ссылкой на первую оплачиваемую анкету. Теперь Вы будете более или менее регулярно получать письма со ссылками на новые опросы. Получение писем на email можно в любой момент отключить. Новые приглашения публикуются также в личном кабинете. <br/><br/>
<span class="badge badge-warning">3</span>
 Сразу после заполнения анкеты Ваш кошелек (в личном кабинете) будет пополнен на указанную в сообщении сумму. Накопление (размером от 5500 руб.) можно вывести из кошелька, пополнив баланс Вашего сотового телефона.<br/>
<hr/>
Данный проект не обещает быстрого, стабильного, большого заработка. Получение "быстрых денег на опросах" невозможно вследствие причин экономического характера. 
Частые опросы одних и тех же респондентов — это грубое нарушение важного принципа социологического исследования. Приглашения к участию в опросах высылаются только тогда, когда Вы нужны 
нашим заказчикам, соблюдены определенные нормы и условия социологической выборки. Информация о выплатах нашим респондентам имеет открытый характер. Она опубликована <a href="payment_order#bottom">здесь</a>. 
Денежное вознаграждение рассматривается в качестве простого материального стимула. Если Вы преследуете единственную меркантильную цель — "заработать деньги", пожалуйста, не регистрируйтесь на нашем сайте. Нам нужны только респонденты, искренне заинтересованные тематикой наших исследований. 
<br/>
<br/>
Мы долго думали над тем, каким образом усилить убедительность наших финансовых отчетов. Начиная с марта 2013 года, сразу после выплаты заработка респонденту будет предложена возможность указать координаты в социальных сетях с тем, чтобы заинтересованные лица смогли проверить наши данные. Надеемся, что положительные отзывы будут зароком деловой добропорядочности ANKETER.ORG 
<br/>
<br/>
<div id="faqdiv" style="display: none;">
<h3>Вопрос — ответ <a href="javascript:del_about();">« свернуть</a></h3>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Кто является инициатором проводимых исследований?</span></p>
<p>Инициатором выступает Автономная некоммерческая организация «Центр экспертных исследований рынка». Это обстоятельство обусловливает независимый характер нашей научно-исследовательской деятельности.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Какую цель преследует настоящий проект?</span></p>
<p>Целью настоящего проекта является интеграция экспертных мнений в различных областях социально-экономического знания. В качестве инструмента интеграции экспертных мнений выбрана электронная анкета. Интегрируемые экспертные мнения имеют социальный заказ и предназначены (в обобщеном виде) для распространения среди аналитиков и представителей бизнес-сообщества.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Как определяется сумма выплат?</span></p>
<p>Участие респондентов в настоящем проекте оплачивается каждый раз пропорционально личному опыту и затратам времени для ответов на вопросы той или иной анкеты.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Что необходимо для моего участия в проекте?</span></p>
<p>Чтобы присоединится к сообществу респондентов, Вам необходимо зарегистрироваться в проекте. После чего по электронной почте Вы будете получать приглашения для участия в опросах. Для участия в том или ином опросе Вам следует перейти по ссылке, которая будет всякий раз указана в тексте приглашения.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Могу ли я получать ссылки на электронные анкеты чаще?</span></p>
<p>Ваше участие в том или ином опросе определяется случайной выборкой с учетом социально-демографических характеристик и личностных компетенций. Повлиять на случайный отбор невозможно. Если число респондентов, соответствующих условиям выборки, превышает ее проектируемый размер, опрос может завершиться досрочно сразу после того, как будет получено необходимое число ответов. <br/>
Следует отметить, что Ваше мнение ценно (востребовано заказчиками) тогда, когда оно свежо и непредвзято. Лицо, регулярно заполняющее анкеты, именуется "профессиональным респондентом". Такой человек дает шаблонизированные ответы, его мнение замылено, он не способен удовлетворять запросы наших заказчиков. Мы заботимся о том, чтобы не случилось подобной перегрузки.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Я проживаю на территории Украины. Могу ли я участвовать в Ваших опросах?</span></p>
<p>К сожалению, нет. Участвовать в опросах могут только граждане России, проживающие на территории РФ.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Я не желаю сообщать Вам номер своего сотового телефона. Что делать?</span></p>
<p>В этом случае при вводе номера телефона можно ввести номер-заглушку из повторяющихся цифр, например, +7-977-777-77-77. Однако в этом случае мы не сможем перечислять Вам заработок. Однако заработок можно использовать в благотворительных целях. Об этом необходимо написать нам в письме по адресу info@anketer.org Внимание! Мы не рассылаем НИКАКИХ SMS-сообщений. Номер используется лишь для перечисления заработка.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Как изменить номер сотового телефона?</span></p>
<p>Это можно сделать в личном кабинете.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Пароль от моего почтового ящика безвозвратно утерян. Могу ли я сменить электронный адрес у своего аккаунта?</span></p>
<p>Вообще говоря, нет. Поскольку для подобной замены нам требуются убедительные доказательства того, что старый ящик принадлежал Вам и никому другому.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Как получить заработок?</span></p>
<p>Сумма из кошелька по запросу пользователя (не автоматически) поступает на баланс мобильного телефона. Номер мобильного телефона указывается респондентов в самой первой анкете, ссылка на которую высылается письмом вместе с паролем от личного кабинета. Необходимо зайти на страницу личного кабинета, нажать на ссылку "Вывести заработок". Далее нажать кнопку "Вывести". Важно! Выводить деньги можно начиная с суммы 5500 рублей. Для накопления суммы отвечайте, пожалуйста, на вопросы анкет, ссылки на которые Вы будете получать на почтовый ящик.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Почему ответ на письмо, отправленное с сайта anketer.org, задерживается?</span></p>
<p>На нашем ресурсе зарегистрировано такое количество респондентов, что мы физически не справляемся с обработкой почтовой корреспонденции и не в состоянии ответить на каждое письмо. В целях получения быстрого ответа пишите, пожалуйста, на электронный ящик info@anketer.org Корреспонденцию, полученную, через сервис Feedback мы рассматриваем во вторую очередь. Наивысший приоритет имеют письма по вопросам вывода заработка.</p>
<hr/>
<p><span class="label label-info" style="font-size: 13px;">Где на сайте можно найти информацию о текущих выплатах?</span></p>
<p>Общий список самых последних выплат публикуется <a href="payment_order#bottom">здесь</a>. Поскольку респонденты обычно выводят деньги сразу после очередного опроса, список обновляется через день или два после очередного опроса.</p>
<br/>
</div> <!--faqdiv -->
<div id="agree" style="display: none;">
<h3>Соглашение на участие в социологических Интернет-опросах <a href="javascript:del_agree();">« свернуть</a></h3>
<hr/>
<p>Автономная некоммерческая организация «Центр экспертных исследований рынка», (далее — «ANKETER», «Администрация сайта») и Респондент, вместе именуемые Стороны заключили настоящее Соглашение на участие в социологических опросах (далее по тексту — Соглашение) представляющее собой предложение АНО «Центр экспертных исследований рынка» приглашенному респонденту (далее — «Пользователь», «Респондент») использовать ресурсы настоящего сайта на нижеизложенных условиях.</p>
<p class="text-center"><strong>1. Предмет Соглашения</strong></p>
<p>1.1. Респондент принимает участие в web-опросах проводимых ANKETER, а ANKETER принимает на себя обязательства выплачивать вознаграждение по итогам проведения опроса, стоимость которых будет оговорена каждый раз заранее в тексте приглашения к участию в том или ином интернет-опросе.</p>
<p class="text-center"><strong>2. Права и обязанности Сторон</strong></p>
<p><strong>2.0. Права и обязанности Респондента</strong><br/>
2.1. Респондент обязуется ознакомиться с условиями участия в социологических опросах, размещённых на сайте <a href="http://anketer.org">http://anketer.org</a> самостоятельно и в полном объеме.<br/>
2.2. Пользователь не вправе регистрироваться в качестве Респондента, если он не достиг совершеннолетия и (или) не обладает полной дееспособностью.<br/>
2.3. Пользователь, регистрируясь на сайте в качестве Респондента, выражает тем самым полное признание условий использования данного сайта и условий проведения социологических опросов, без каких-либо изъятий и оговорок.<br/>
2.4. Нажатие кнопки регистрации  свидетельствует о том, что Пользователь, в полной мере ознакомился  с условиями настоящего Соглашения и условиями работы сайта и полностью принимает их.<br/>
2.5. Пользователь при регистрации сообщает необходимые данные, и, заключая Соглашение, даёт своё согласие обработку этих данных согласно ст.9 Федерального закона от 27.07.2006 N 152-ФЗ (ред. от 21.07.2014) "О персональных данных", а также на передачу этих данных третьим лицам в целях обусловленных проведением опроса.<br/>
2.6. Респондент дает согласие на обработку сообщённой в ходе проведения опросов информации и её дальнейшее использование в коммерческих целях.<br/>
2.7. Респондент выступает ответственным лицом за поддержание конфиденциальности своего пароля и другой личной информации. Респондент берет на себя обязательства незамедлительно сообщать администрации сайта о любом несанкционированном использовании своего пароля или личного кабинета, выявленных ошибках в работе предоставляемых ресурсов.<br/>
2.8. Респондент  несёт ответственность за всю сообщенную информацию.<br/>
2.9. Респондент имеет право на получение вознаграждения в размере и в порядке установленным ANKETER в условиях пользования сайтом.<br/>
2.10. Респондент согласен, что ANKETER не несет никакой ответственности ни перед ним, ни перед третьими лицами за изменение, отмену или прекращение какой-либо услуги.<br/>
2.11. Респондент признает, что ANKETER не несет ответственности за сохранность сообщенной им информации в базах данных ANKETER.<br/>
2.12. Респондент соглашается  получать от Администрации на указанный в своём профиле на сайте электронный адрес информационные электронные сообщения о важных событиях, происходящих в рамках Сайта или в связи с ним.<br/>
2.13. В случае, если выплачиваемое Респонденту вознаграждение подлежит налогообложению согласно ст. 217 НК РФ, ANKETER может взять на себя обязательства по своевременному оформлению всех необходимых документов и уплате налогов. Для этого Респондент должен заключить с ANKETER договор, предоставляющий право на выполнение таких обязательств.<br/>
2.14. Получение ЭКСПЕРТом денежного вознаграждения не может быть осуществлено без ознакомления с условиями проведения онлайн-платежей <a href="https://money.yandex.ru/pay/doc.xml?offerid=default">https://money.yandex.ru/pay/doc.xml?offerid=default</a><br/>
<br/>
<strong>3.0. Права и обязанности ANKETER</strong><br/>
3.1. ANKETER обязуется выплачивать Респонденту вознаграждение в порядке и в размере установленным условиями пользования сайта.<br/>
3.2. ANKETER обязуется предпринимать все необходимые меры по охране персональных данных Респондента. ANKETER вправе с согласия Респондента передавать информацию содержащую персональные данные Респондента третьим лицам для целей, обусловленных проведением опроса, при наличии согласия Респондента.<br/>
3.3. ANKETER оставляет за собой право отказать Респонденту в регистрации, выплате денежного вознаграждения, совершении иного обязательства, если на то будут веские причины. В качестве таковых могут выступить следующие:<br/>
— Сообщение ложных, крайне сомнительных сведений, сведений низкой ценности (на что должны указывать конкретные факты).<br/>
— Повторная (множественная) регистрация на сайте одного лица с разными электронными адресами.<br/>
— Утрата связи с Респондентом (Респондент не принимает приглашения к участию в интернет-опросах подряд в течение трех месяцев).<br/>
— Нанесение умышленного или неумышленного вреда ресурсам сайта (в том числе посредством DоS-атаки).<br/>
— Нецелевое использование Респондентом ресурсов сайта.<br/>
— Применение Респондентом оскорбительных выражений, ненормативной лексики, сообщение информации непристойного содержания; информации, противоречащей законодательству РФ, в том числе попадающей под определение, предусмотренное статьями УК РФ.<br/>
3.4. ANKETER сохраняет за собой право изменять или прекращать оказание какой-либо услуги, обновлять контент ресурсов сайта в целях повышения их качества без предварительного уведомления о том Респондента.</p>
<br/>
<p class="text-center"><strong>4. Действие Соглашения</strong></p>
<p>4.1. Соглашение (в том числе любая из его частей) может быть изменено ANKETER без какого-либо специального уведомления Респондента. Новая редакция Соглашения вступает в силу с момента ее размещения на сайте либо доведения до сведения Пользователя в иной удобной форме.<br/>
4.2. Соглашение может быть расторгнуто любой из Сторон в любое время путём направления уведомления об этом другой Стороне по электронной почте сообщённой при регистрации и (или) размещённой на сайте.<br/>
4.3. Соглашение также считается расторгнутым при наступлении событий, указанных в п. 3.3. Соглашения.</p>
<br/>
<p class="text-center"><strong>5. Заключительные положения</strong></p>
<p>5.1. Действующая редакция настоящего Соглашения размещена и доступна в сети Интернет по адресу: <a href="https://anketer.org">https://anketer.org</a><br/>
5.2. Ко всем вопросам прямо не урегулированным настоящим Соглашением применяются положения ст.ст. 426, 428, 1055 ГК РФ.<br/>
5.3. Дата принятия Респондентом условий Соглашения является датой его заключения.</p>
<br/>
<p class="text-center"><strong>6. Адреса и реквизиты сторон</strong></p>
<address>АНО «Центр экспертных исследований рынка»<br/>
ИНН 7709471972<br/>
КПП 770901001<br/>
Адрес 101000, Москва г., ул. Покровка, дом 14/2 стр.1<br/>
р/сч 40703810538000001755</address>
<address>ген.дир. Г.П. Посыпай</address>
<a href="convention_anketer.org.pdf">Настоящее соглашение в pdf-формате</a>
<hr/>
</div> <!-- agree -->
<form class="form-horizontal" id="sendform" name="sendform" style="display: none;">
<div class="alert" id="alrt_block2" style="display: none;">
<span id="alert2"></span>
</div>
<fieldset>
<legend>Письмо в службу поддержки <a href="javascript:del_send();">« свернуть</a></legend>
<div class="control-group" id="d_textsend">
<label class="control-label" for="textsend">Вопросы, комментарии, пожелания</label>
<div class="controls">
<textarea class="input-xxlarge" id="textsend" name="textsend" rows="5"></textarea>
</div>
</div>
<div class="form-actions">
<button class="btn btn-large btn-success" type="submit"><i class="icon-white icon-envelope"></i> Отправить</button>
<img alt="" id="load3" src="img/load.gif" style="display: none;"/>
</div>
</fieldset>
<hr/>
</form>
<div id="foo">
</div>
<h3>Новости</h3>
<a class="twitter-timeline" data-chrome="noheader nofooter noborders noscrollbar transparent" data-tweet-limit="10" data-widget-id="346355513786761216" href="https://twitter.com/anketer_org">Новости</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
<footer class="footer">
<div class="container">
<p class="muted credit" style="margin-left: 7px;">Пожалуйста, сообщите о нас Вашим друзьям, коллегам, знакомым:<br/>
<a href="javascript:void(0);" onclick="Share.vkontakte('http://anketer.org','Вы отвечаете — мы платим','http://anketer.org/pool/anketer_100x100_b.png','Я зарабатываю здесь')"><span class="label label-info">ВКонтакте</span></a>
<a href="javascript:void(0);" onclick="Share.facebook('http://anketer.org','Вы отвечаете — мы платим','http://anketer.org/pool/anketer_100x100_b.png','Я зарабатываю здесь')"><span class="label label-info">Facebook</span></a>
<a href="javascript:void(0);" onclick="Share.mailru('http://anketer.org','Вы отвечаете — мы платим','http://anketer.org/pool/anketer_100x100_b.png','Я зарабатываю здесь')"><span class="label label-info">Mail.Ru</span></a>
<a href="javascript:void(0);" onclick="Share.odnoklassniki('http://anketer.org','Я зарабатываю здесь')"><span class="label label-info">Одноклассники.ru</span></a>
<a href="javascript:void(0);" onclick="Share.twitter('http://anketer.org','Вы отвечаете — мы платим @anketer_org')"><span class="label label-info">Twitter</span></a>
<br/><br/>Пишите нам: <a href="mailto:info@anketer.org">info@anketer.org</a><br/>Новости: <a href="http://twitter.com/#!/anketer_org" rel="nofollow">@anketer_org</a>  |  <a href="blog_1">Наш блог</a>  |  <a href="order">Заказ опроса</a>
<br/><br/>Согласно закону о локализации персональных данных россиян на территории РФ клон базы данных anketer.org находится на защищенном сервере, размещенном на территории РФ по адресу 185.62.101.132 (компания rentacloud.su, договор № 1512)
    <br/><br/>Юридический адрес: 101000, Москва г., ул. Покровка, дом 14/2 стр.1
    <br/><br/>© АНО «Центр экспертных исследований рынка», 2012–2021</p>
</div>
</footer>
<!--noindex-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21534673 = new Ya.Metrika({id:21534673,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt=" " src="//mc.yandex.ru/watch/21534673" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-45397162-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<div style="display: none;">
<!-- begin of Top100 code -->
<div id="Rambler-counter">
<noscript>
<a href="http://top100.rambler.ru/navi/2961002/">
<img alt="Rambler's Top100" border="0" src="http://counter.rambler.ru/top100.cnt?2961002"/>
</a>
</noscript>
</div>
<script type="text/javascript">
var _top100q = _top100q || [];
_top100q.push(['setAccount', '2961002']);
_top100q.push(['trackPageviewByLogo', document.getElementById('Rambler-counter')]);

(function(){
  var pa = document.createElement("script"); 
  pa.type = "text/javascript"; 
  pa.async = true;
  pa.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/top100/top100.js";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(pa, s);
})();
</script>
<!-- end of Top100 code -->
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t26.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: "2421935", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
//]]></script><noscript><div style="position:absolute;left:-10000px;">
<img alt="Рейтинг@Mail.ru" height="1" src="//top-fwz1.mail.ru/counter?id=2421935;js=na" style="border:0;" width="1"/>
</div></noscript>
<!-- //Rating@Mail.ru counter -->
<!--/noindex-->
</body>
</html>
