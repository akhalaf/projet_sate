<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"e0849ccd11",applicationID:"2183487"};window.NREUM||(NREUM={}),__nr_require=function(n,e,t){function r(t){if(!e[t]){var i=e[t]={exports:{}};n[t][0].call(i.exports,function(e){var i=n[t][1][e];return r(i||e)},i,i.exports)}return e[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<t.length;i++)r(t[i]);return r}({1:[function(n,e,t){function r(){}function i(n,e,t){return function(){return o(n,[u.now()].concat(f(arguments)),e?null:this,t),e?void 0:this}}var o=n("handle"),a=n(4),f=n(5),c=n("ee").get("tracer"),u=n("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(n,e){s[e]=i(d+e,!0,"api")}),s.addPageAction=i(d+"addPageAction",!0),s.setCurrentRouteName=i(d+"routeName",!0),e.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(n,e){var t={},r=this,i="function"==typeof e;return o(l+"tracer",[u.now(),n,t],r),function(){if(c.emit((i?"":"no-")+"fn-start",[u.now(),r,i],t),i)try{return e.apply(this,arguments)}catch(n){throw c.emit("fn-err",[arguments,this,n],t),n}finally{c.emit("fn-end",[u.now()],t)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(n,e){m[e]=i(l+e)}),newrelic.noticeError=function(n,e){"string"==typeof n&&(n=new Error(n)),o("err",[n,u.now(),!1,e])}},{}],2:[function(n,e,t){function r(n,e){var t=n.getEntries();t.forEach(function(n){"first-paint"===n.name?a("timing",["fp",Math.floor(n.startTime)]):"first-contentful-paint"===n.name&&a("timing",["fcp",Math.floor(n.startTime)])})}function i(n){if(n instanceof c&&!s){var e,t=Math.round(n.timeStamp);e=t>1e12?Date.now()-t:f.now()-t,s=!0,a("timing",["fi",t,{type:n.type,fid:e}])}}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var o,a=n("handle"),f=n("loader"),c=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){o=new PerformanceObserver(r);try{o.observe({entryTypes:["paint"]})}catch(u){}}if("addEventListener"in document){var s=!1,p=["click","keydown","mousedown","pointerdown","touchstart"];p.forEach(function(n){document.addEventListener(n,i,!1)})}}},{}],3:[function(n,e,t){function r(n,e){if(!i)return!1;if(n!==i)return!1;if(!e)return!0;if(!o)return!1;for(var t=o.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==t[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var f=navigator.userAgent,c=f.match(a);c&&f.indexOf("Chrome")===-1&&f.indexOf("Chromium")===-1&&(i="Safari",o=c[1])}e.exports={agent:i,version:o,match:r}},{}],4:[function(n,e,t){function r(n,e){var t=[],r="",o=0;for(r in n)i.call(n,r)&&(t[o]=e(r,n[r]),o+=1);return t}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],5:[function(n,e,t){function r(n,e,t){e||(e=0),"undefined"==typeof t&&(t=n?n.length:0);for(var r=-1,i=t-e||0,o=Array(i<0?0:i);++r<i;)o[r]=n[e+r];return o}e.exports=r},{}],6:[function(n,e,t){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(n,e,t){function r(){}function i(n){function e(n){return n&&n instanceof r?n:n?c(n,f,o):o()}function t(t,r,i,o){if(!d.aborted||o){n&&n(t,r,i);for(var a=e(i),f=v(t),c=f.length,u=0;u<c;u++)f[u].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(n,e){h[n]=v(n).concat(e)}function m(n,e){var t=h[n];if(t)for(var r=0;r<t.length;r++)t[r]===e&&t.splice(r,1)}function v(n){return h[n]||[]}function g(n){return p[n]=p[n]||i(t)}function w(n,e){u(n,function(n,t){e=e||"feature",y[t]=e,e in s||(s[e]=[])})}var h={},y={},b={on:l,addEventListener:l,removeEventListener:m,emit:t,get:g,listeners:v,context:e,buffer:w,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var f="nr@context",c=n("gos"),u=n(4),s={},p={},d=e.exports=i();d.backlog=s},{}],gos:[function(n,e,t){function r(n,e,t){if(i.call(n,e))return n[e];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,e,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return n[e]=r,r}var i=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(n,e,t){function r(n,e,t,r){i.buffer([n],r),i.emit(n,e,t)}var i=n("ee").get("handle");e.exports=r,r.ee=i},{}],id:[function(n,e,t){function r(n){var e=typeof n;return!n||"object"!==e&&"function"!==e?-1:n===window?0:a(n,o,function(){return i++})}var i=1,o="nr@id",a=n("gos");e.exports=r},{}],loader:[function(n,e,t){function r(){if(!x++){var n=E.info=NREUM.info,e=l.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(n&&n.licenseKey&&n.applicationID&&e))return s.abort();u(y,function(e,t){n[e]||(n[e]=t)}),c("mark",["onload",a()+E.offset],null,"api");var t=l.createElement("script");t.src="https://"+n.agent,e.parentNode.insertBefore(t,e)}}function i(){"complete"===l.readyState&&o()}function o(){c("mark",["domContent",a()+E.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(f=Math.max((new Date).getTime(),f))-E.offset}var f=(new Date).getTime(),c=n("handle"),u=n(4),s=n("ee"),p=n(3),d=window,l=d.document,m="addEventListener",v="attachEvent",g=d.XMLHttpRequest,w=g&&g.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:g,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var h=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1158.min.js"},b=g&&w&&w[m]&&!/CriOS/.test(navigator.userAgent),E=e.exports={offset:f,now:a,origin:h,features:{},xhrWrappable:b,userAgent:p};n(1),n(2),l[m]?(l[m]("DOMContentLoaded",o,!1),d[m]("load",r,!1)):(l[v]("onreadystatechange",i),d[v]("onload",r)),c("mark",["firstbyte",f],null,"api");var x=0,O=n(6)},{}],"wrap-function":[function(n,e,t){function r(n){return!(n&&n instanceof Function&&n.apply&&!n[a])}var i=n("ee"),o=n(5),a="nr@original",f=Object.prototype.hasOwnProperty,c=!1;e.exports=function(n,e){function t(n,e,t,i){function nrWrapper(){var r,a,f,c;try{a=this,r=o(arguments),f="function"==typeof t?t(r,a):t||{}}catch(u){d([u,"",[r,a,i],f])}s(e+"start",[r,a,i],f);try{return c=n.apply(a,r)}catch(p){throw s(e+"err",[r,a,p],f),p}finally{s(e+"end",[r,a,c],f)}}return r(n)?n:(e||(e=""),nrWrapper[a]=n,p(n,nrWrapper),nrWrapper)}function u(n,e,i,o){i||(i="");var a,f,c,u="-"===i.charAt(0);for(c=0;c<e.length;c++)f=e[c],a=n[f],r(a)||(n[f]=t(a,u?f+i:i,o,f))}function s(t,r,i){if(!c||e){var o=c;c=!0;try{n.emit(t,r,i,e)}catch(a){d([a,t,r,i])}c=o}}function p(n,e){if(Object.defineProperty&&Object.keys)try{var t=Object.keys(n);return t.forEach(function(t){Object.defineProperty(e,t,{get:function(){return n[t]},set:function(e){return n[t]=e,e}})}),e}catch(r){d([r])}for(var i in n)f.call(n,i)&&(e[i]=n[i]);return e}function d(e){try{n.emit("internal-error",e)}catch(t){}}return n||(n=i),t.inPlace=u,t.flag=a,t}},{}]},{},["loader"]);</script>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<title>ï¼Ãï¼æ ªå¼ä¼ç¤¾ï½Webã·ã¹ãã ã®æè¡ãã¼ããã¼</title>
<meta content="ï¼Ãï¼ã¯Webã·ã¹ãã ã®æè¡ãã¼ããã¼ã§ãã" name="description"/>
<meta content="ã·ã¹ãã éçº,ãã¼ã ãã¼ã¸,å¶ä½ä¼ç¤¾,1Ã1,ã¯ã³ãã¤ã¯ã³,onebyone,å¤§éª,é¢è¥¿,PHP,CakePHP" name="keywords"/>
<link href="/styles/top/css/css.css" rel="stylesheet" type="text/css"/>
<link href="favicon.ico" rel="shortcut icon" type="img/x-icon"/>
<link href="/contents/feed" rel="alternate" title="ï¼Ãï¼æ ªå¼ä¼ç¤¾" type="application/rss+xml"/>
<script src="/styles/common/js/navi.js" type="text/javascript"></script>
<!--[if IE 6]>
	<script src="/styles/common/js/DD_belatedPNG.js"></script>
	<script>
		DD_belatedPNG.fix('img, .png_bg');
	</script>
<![endif]-->
</head>
<body>
<div class="png_bg" id="wrapper">
<div id="header">
<h1>ï¼Ãï¼ã¯Webã·ã¹ãã ã®æè¡ãã¼ããã¼ã§ãã</h1>
<p class="logo"><img alt="1Ã1-onebyone-" src="/styles/common/img/logo.jpg"/></p>
<p class="btn"><a href="/contact" title="ã¡ã¼ã«ã§ã®ãåãåãã"><img alt="ã¡ã¼ã«ã§ã®ãåãåãã" border="0" class="imgover" src="/styles/common/img/header_btn.jpg"/></a></p>
</div>
<div id="mainimg">
<img alt="æè¡ãã©ãä½¿ãã¹ããï¼ï¼Ãï¼ã§ã¯æä¾ãããµã¼ãã¹ã«å¿ãããæè¡ããä½¿ãã¾ãã" src="/styles/top/img/topmain.jpg"/>
</div>
<ul class="clearfix" id="menubtn">
<li><a href="/vision" title="ä¼æ¥­çå¿µ"><img alt="ä¼æ¥­çå¿µ" class="imgover" src="/styles/common/img/menu1.jpg"/></a></li>
<li><a href="/company" title="ä¼ç¤¾æ¦è¦"><img alt="ä¼ç¤¾æ¦è¦" class="imgover" src="/styles/common/img/menu2.jpg"/></a></li>
<li><a href="/service" title="äºæ¥­åå®¹"><img alt="äºæ¥­åå®¹" class="imgover" src="/styles/common/img/menu3.jpg"/></a></li>
<li><a href="/works" title="å¶ä½å®ç¸¾"><img alt="å¶ä½å®ç¸¾" class="imgover" src="/styles/common/img/menu4.jpg"/></a></li>
<li><a href="/recruit" title="æ¡ç¨æå ±"><img alt="æ¡ç¨æå ±" class="imgover" src="/styles/common/img/menu5.jpg"/></a></li>
<li><a href="/contact" title="ãåãåãã"><img alt="ãåãåãã" class="imgover" src="/styles/common/img/menu6.jpg"/></a></li>
</ul>
<div class="clearfix" id="contents">
<div id="main">
<div id="box1">
<h2><img alt="Webã·ã¹ãã ã§ãå°ãã§ã¯ããã¾ãããï¼" src="styles/top/img/toptxt.jpg"/></h2>
<p>
ãã¾ãWebã¯åãªããã¼ã ãã¼ã¸ã ãã§ã¯ãªããæ§ããªç¨éã«æ´»ç¨ããã¦ãã¾ãã<br/>
ECãµã¤ããæ¥­åã·ã¹ãã ãã°ã«ã¼ãã¦ã§ã¢ãä¼å¡ç®¡çç­ãæ°ãä¸ããã¨<br/>ã­ãªãããã¾ããã<br/>
ããã¦ãããããæ§ç¯ããããã®æ¹æ³ãæè¡ã¯å®ã«å¤å²ã«æ¸¡ã£ã¦ãã¾ãã<br/>
</p>
<p>
ï¼Ãï¼ã§ã¯ãããã¾ã§å¤ãã®Webã·ã¹ãã æ§ç¯ãéãã¦ã<br/>
ãã©ããã£ãæè¡ãé¸æãã¹ããã<br/>
ãã©ã®ããã«æ§ç¯ããã¹ããã<br/>
ãã©ã®ããã«éå¶ãã¦ããã¹ããã<br/>
ã¨ãããã¦ãã¦ããå®¢æ§ã¨å±ã«æ¥ãç©ã¿éã­ã¦ãã¾ãã
</p>
<p>
Webã·ã¹ãã ãæ§ç¯ããããWebã·ã¹ãã æè¡ã«ã¤ãã¦ç¸è«ãããã<br/>
ãµã¤ããéãããæ¹åãããç­ããWebã·ã¹ãã ã§ãå°ãã®ãã¨ãããã°ãæ°è»½ã«ãé£çµ¡ä¸ããã
</p>
</div>
</div>
<div id="side">
<div id="company">
<h3><img alt="1Ã1æ ªå¼ä¼ç¤¾" src="/styles/common/img/companyname.jpg"/></h3>
<p>ã531-0072<br/>å¤§éªå¸ååºè±å´3-4-14<br/>ã·ã§ã¼ã¬ã¤ãã« 3F</p>
<p>ãåãåããã¯<a href="/contact">ãã¡ã</a></p>
</div>
</div>
</div>
</div>
<div class="png_bg" id="footer">
<div id="footercontents">
<p><img alt="1Ã1æ ªå¼ä¼ç¤¾ãCopyright (C)  1x1 Inc. All Rights Reserved. " src="/styles/common/img/footer_logo.jpg"/></p>
<ul>
<li><a href="/sitemap" title="ãµã¤ãããã">ãµã¤ãããã</a></li>
<li><a href="/contact" title="ã¡ã¼ã«ã§ã®ãååã">ã¡ã¼ã«ã§ã®ãååã</a></li>
</ul>
</div>
</div>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"e0849ccd11","applicationID":"2183487","transactionName":"NQMHY0tXWRcCURZfCgxJJFRNX1gKTEYNRkoLCAFSQQ==","queueTime":0,"applicationTime":8,"atts":"GUQEFQNNShk=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
