<!DOCTYPE html>
<html lang="de">
<head>
<title>Webhosting, Homepage, Server, Onlinespeicher, Webbaukasten &amp; Domains: 1blu</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=0.75" name="viewport"/>
<meta content="Webhosting von 1blu: Homepage-Pakete, Server und Domains für professionelle Websites &amp; Internet-Anwendungen" name="description"/>
<meta content="1blu, 1blu AG, Business Support, dedicated, dedizierte server, domain, domainname, Domain-Umleitung, Email, E-Mail, email, eshops, eshop, eshop Power, eshop Professional, eshop Unlimited, eShop Lösung, Fernzugriff, FTP, FTP-Zugang, homepage, Homepage, Homepage Komplettpaket, Homepage Power, Homepage Professional, Homepage Unlimited, Homepage UnlimitedXXL, hosting, Hosting, IMAP, kaufmännische Software, MySQL, online-shop, online shop, Onlineshop, Online Shop Lösung, provider, Provider, providerwechsel, Providerwechsel, PHP, Perl, POP3, root server, rootserver, Root Server, DedicatedServer, DedicatedServer Power, DedicatedServer Unlimited, DedicatedServer UnlimitedXXL, DedicatedServer UnlimitedXXL+, server, Server traffic unbegrenzt, server virtuell, Service günstig, shops, software, Speicherplatz, speicher, Support günstig, support günstig, Technik Support, Traffic, traffic unbegrenzt, virtueller server, vServer, Vserver, vServer Power, vServer Unlimited, vServer UnlimitedXXL, Warenwirtschaft, webhosting, Web Hosting, website, webseite, webspace, Zusatzdomains günstig, Zusatzdomains billig" name="keywords"/>
<link href="/styles/1blu.de/favicon.ico" rel="shortcut icon" type="text/css"/>
<link href="/styles/1blu.de/apple.png" rel="apple-touch-icon"/>
<link href="/styles/1blu.de/theme.min.css" rel="stylesheet" type="text/css"/>
<link href="/styles/1blu.de/screen.css" rel="stylesheet" type="text/css"/>
<link href="/css/4cd43cf.css" rel="stylesheet"/>
<script src="/js/1e233c8.js" type="text/javascript"></script>
<script src="/js/4a573ce.js" type="text/javascript"></script>
<script src="/js/eb6710b.js" type="text/javascript"></script>
</head>
<body>
<div class="container main">
<div class="row head">
<div class="col-xs-4 col-sm-2 logo">
<img alt="1blu Logo" class="img-responsive logoimg" onclick="window.location.href = '/';" src="/styles/1blu.de/logo.png" style="cursor: pointer;"/>
</div>
<div class="col-xs-8 col-sm-10">
<div class="row subhead">
<div class="col-xs-12 hidden-xs">
<a href="/kontakt/">Kontakt</a>  | 
<a href="https://faq.1blu.de" target="_blank">FAQ</a>  | 
<a href="https://ksb.1blu.de" target="_blank">Kundenlogin</a>  | 
<a href="https://webmail.1blu.de" target="_blank">Webmailer</a>
</div>
</div>
<div class="row menu" style="margin: 0px;">
<div class="col-xs-12 pull-right" style="padding: 0px;">
<nav class="navbar navbar-default pull-right">
<div class="container-fluid" style="padding: 0px;">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="navbar-collapse-1" style="padding: 0px;">
<ul class="nav navbar-nav">
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-webhosting" data-toggle="dropdown" href="#" role="button"><span class="">Webhosting</span> <span class="caret"></span></a>
<ul class="dropdown-menu">
<li class="menuhover" onclick="window.location.href = '/webhosting/';"><h2><a class="" href="/webhosting/">Webhosting</a></h2></li>
<li class="menuhover" onclick="window.location.href = '/webhosting/homepagepakete/';"><h3><a class="" href="/webhosting/homepagepakete/">Homepagepakete</a></h3><span class="hidden-xs">Komplett-Hosting für Einsteiger und Fortgeschrittene</span></li>
<li class="menuhover" onclick="window.location.href = '/webhosting/performancepakete/';"><h3><a class="" href="/webhosting/performancepakete/">Performancepakete</a></h3><span class="hidden-xs">Höchste Performance durch High-End NVMe-SSD-Festplatten</span></li>
<li class="menuhover" onclick="window.location.href = '/webhosting/managedhosting/';"><h3><a class="" href="/webhosting/managedhosting/">ManagedHosting</a></h3><span class="hidden-xs">Eigene Serverumgebung mit komfortabler Verwaltung</span></li>
</ul>
</li>
<li><a class="nav-webbaukasten " href="/webbaukasten/">Webbaukasten</a>
</li>
<li><a class="nav-onlinespeicher " href="/online-speicher/">Online-Speicher</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-server" data-toggle="dropdown" href="#" role="button"><span class="">Server</span> <span class="caret"></span></a>
<ul class="dropdown-menu">
<li class="menuhover" onclick="window.location.href = '/server/';"><h2><a class="" href="/server/">Server</a></h2></li>
<li class="menuhover" onclick="window.location.href = '/server/vserver/';"><h3><a class="" href="/server/vserver/">vServer</a></h3><span class="hidden-xs">Performante Linux VPS mit SSD-Ausstattung</span></li>
<li class="menuhover" onclick="window.location.href = '/server/rootserver/';"><h3><a class="" href="/server/rootserver/">RootServer</a></h3><span class="hidden-xs">Virtuelle RootServer auf KVM-Basis mit festen Ressourcen</span></li>
<li class="menuhover" onclick="window.location.href = '/server/dedicatedserver/';"><h3><a class="" href="/server/dedicatedserver/">DedicatedServer</a></h3><span class="hidden-xs">Performance dedizierte Server für Business-Anforderungen</span></li>
<li class="menuhover" onclick="window.location.href = '/server/ssl/';"><h3><a class="" href="/server/ssl/">SSL-Zertifikate</a></h3><span class="hidden-xs">Für eine sichere und vertrauenswürdige Datenübertragung</span></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-domains" data-toggle="dropdown" href="#" role="button"><span class="">Domains</span> <span class="caret"></span></a>
<ul class="dropdown-menu">
<li class="menuhover" onclick="window.location.href = '/domains/';"><h3><a class="" href="/domains/">Domainpakete</a></h3><span class="hidden-xs">Sichern Sie sich Ihre persönliche Wunschadressse im Internet - zu supergünstigen Konditionen!</span></li>
<li class="menuhover" onclick="window.location.href = '/zusatzdomains/';"><h3><a class="" href="/zusatzdomains/">Zusatzdomains</a></h3><span class="hidden-xs">Weitere Domains für Ihren Internetauftritt</span></li>
</ul>
</li>
<li><a class="nav-business-hosting " href="/business-hosting/">Business-Hosting</a></li>
<li class="nav-divider visible-xs-block" role="separator"></li>
<li class="visible-xs-block menuhover"><a class="" href="/kontakt/">Kontakt</a></li>
<li class="visible-xs-block menuhover"><a href="https://faq.1blu.de" target="_blank">FAQ</a></li>
<li class="visible-xs-block menuhover"><a href="https://login.1blu.de" target="_blank">Kundenlogin</a></li>
<li class="visible-xs-block menuhover"><a href="https://webmail.1blu.de/" target="_blank">Webmailer</a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
</div>
</div>
</div>
</div>
<noscript>
<div class="row noscript">
<div class="col-xs-12">
<div class="alert alert-danger" role="alert">
<span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>
<span class="sr-only">Error:</span>
<strong>JavaScript ist nicht aktiviert!</strong> Um die Seite nutzen zu können, aktivieren Sie
          bitte JavaScript in Ihrem Browser.
        </div>
</div>
</div>
</noscript>
<div class="content_main">
<div class="row picture">
<div class="col-xs-12" style="padding: 0px;">
<img alt="Startseiten Bild" class="img-responsive startpageheadimage" src="/styles/1blu.de/startpagehead1.jpg"/>
<div class="startpageheadtext"><h2>Webhosting,<br/>Server &amp; Domains</h2>
<h3>Profi-Lösungen von 1blu</h3></div>
</div>
</div>
<div class="row" id="domainchecker" style="">
<div class="col-xs-12" style="padding: 0px;">
<div class="domaincheckbox" style="background-image: url('/styles/1blu.de/startpagehead2.jpg');">
<div class="row">
<div class="col-lg-6 visible-lg-inline">
<h3 style="margin-top: 5px;"><b>Wunschdomain noch frei?</b> Jetzt schnell prüfen:</h3>
</div>
<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-offset-0 col-lg-6">
<form action="/domaincheck/" id="domainCheckForm" method="post" name="form">
<div class="input-group">
<input class="form-control form-control" id="form_domain" name="form[domain]" placeholder="Ihre Wunschdomain" required="required" type="text"/>
<span class="input-group-btn">
<button class="btn btn-default btn" id="form_save" name="form[save]" type="submit">Domain prüfen</button>
</span>
</div>
<input id="form__token" name="form[_token]" type="hidden" value="t1DwOV2sp2Yz8ZmJSUFicSv6RIUL1-mH_sLWYRiYwJE"/>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="alert alert-danger" id="form_error" role="alert" style="margin-bottom: 0px;margin-top: 15px; display: none;">
<span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>
<span id="form_error_text"></span>
</div>
<div id="domain_search">
</div>
<div class="row index_content">
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="panel panel-default panel-hover panel-blue" onclick="window.location.href = 'webhosting';">
<div class="panel-body panel-blue indexHeadBoxes">
<div>
<img alt="Panel Bild" src="/styles/1blu.de/webhost-header.jpg" style="width: 100%; border-radius: 3px 3px 0px 0px;"/>
<div style="padding: 0px 15px;">
<h2>Webhosting</h2>
        Homepage-Pakete mit Komplett-Ausstattung und mehr Speed durch HTTP/2. Performance-Pakete: Turbo-Hosting mit NVMe-Technologie. ManagedHosting für maximalen Komfort. Günstige Domainpakete.
      </div>
</div>
</div>
<div class="panel-footer text-center">
<div class="startprice">
            ab 1,- €/Monat<sup><small>1</small></sup> </div>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="panel panel-default panel-hover panel-blue" onclick="window.location.href = 'server';">
<div class="panel-body panel-blue indexHeadBoxes">
<div>
<img alt="Panel Bild" src="/styles/1blu.de/server-header.jpg" style="width: 100%; border-radius: 3px 3px 0px 0px;"/>
<div style="padding: 0px 15px;">
<h2>Server</h2>
        Leistungsstarke Server-Lösungen zu Spitzenpreisen. vServer mit SSD-Performance, RootServer für maximale Leistungsstabilität, performante DedicatedServer.
      </div>
</div>
</div>
<div class="panel-footer text-center">
<div class="startprice">
            ab 1,- €/Monat<sup><small>2</small></sup> </div>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="panel panel-default panel-hover panel-blue" onclick="window.location.href = 'business-hosting';">
<div class="panel-body panel-blue indexHeadBoxes">
<div>
<img alt="Panel Bild" src="/styles/1blu.de/business-header.jpg" style="width: 100%; border-radius: 3px 3px 0px 0px;"/>
<div style="padding: 0px 15px;">
<h2>Business-Hosting</h2>
        Individuelle Hosting-Lösungen für anspruchsvolle Business-Anwendungen: Private Cloud Lösungen, dedizierte Server auf Wunsch inklusive Managed-Service, White-Label-fähiges Domain-Management.
      </div>
</div>
</div>
<div class="panel-footer text-center">
<div class="startprice">
            ab 199,- €/Monat<sup><small>3</small></sup> </div>
</div>
</div>
</div>
</div>
<div class="row featurette cursor_pointer vServerGroup" onclick="window.location.href = '/server/vserver/';">
<div class="col-sm-offset-7 col-sm-5 col-md-offset-6 col-md-6 col-xs-12">
<div class="pull-right" style="margin: 30px 20px 20px 30px;}">
<div style="max-width: 85px;max-height: 85px;margin:0 auto;margin-bottom: 20px;">
<div class="price">
<div class="price_top" style="">
<div style="float:left;">
           ab

                  </div>
</div>
<div style="clear: both; float: left; vertical-align: top; width: 85px; margin-top: -3px;">
<div class="price_price" style="width: 65px;">
                  1,-
              </div>
</div>
<div class="price_footer">
              € / Monat<sup>2</sup>
</div>
</div>
<div class="price_disturber" style="width: 100px;">
      12 Monate lang!
    </div>
</div>
</div>
<h2>1blu-vServer</h2>
<h4>VPS Linux</h4>
<p>Leistungsstarke virtuelle Server zu attraktiven Konditionen</p>
<ul>
<li>Bis zu 48 GB RAM und 1500 GB SSD</li>
<li>KVM | Virtuozzo</li>
<li>Freie Betriebssystemauswahl, per Klick wählbar</li>
<li>Eigene ISO-Images, eigenes DVD-Laufwerk</li>
</ul>
<a class="btn btn-default" href="/server/vserver/" role="button">Mehr »</a><br/>
<br/>
</div>
</div>
<div class="row featurette cursor_pointer webbuilderGroup" onclick="window.location.href = '/webbaukasten/';">
<div class="col-sm-offset-7 col-sm-5 col-md-offset-6 col-md-6 col-xs-12">
<div class="pull-right" style="margin: 30px 20px 20px 30px;}">
<div style="max-width: 85px;max-height: 85px;margin:0 auto;margin-bottom: 20px;">
<div class="price">
<div class="price_top" style="">
<div style="float:left;">
           ab

                  </div>
</div>
<div style="clear: both; float: left; vertical-align: top; width: 85px; margin-top: -3px;">
<div class="price_price" style="width: 65px;">
                  0,-
              </div>
</div>
<div class="price_footer">
              € / Monat<sup>4</sup>
</div>
</div>
<div class="price_disturber" style="width: 100px;">
      Bis 31.01.2021!
    </div>
</div>
</div>
<h2>1blu-Webbaukasten</h2>
<h4>Websites im Handumdrehen selbst erstellen!</h4>
<p>Mit dem 1blu-Webbaukasten erstellen Sie über eine intuitive bedienbare Weboberfläche Ihre eigene Website – natürlich optimiert für die Darstellung auf Mobilgeräten.</p>
<p>Über 190 Designvorlagen, kostenlose Bilder und Grafiken, unzählige Widgets wie Shop, Paypal, Blocks, Social Media stehen Ihnen für eine individuelle Gestaltung zur Verfügung.*</p>
<a class="btn btn-default" href="/webbaukasten/" role="button">Mehr »</a><br/>
<br/>
</div>
</div>
<div class="row featurette cursor_pointer performancepaketeGroup" onclick="window.location.href = '/webhosting/performancepakete/';">
<div class="col-sm-offset-7 col-sm-5 col-md-offset-6 col-md-6 col-xs-12">
<div class="pull-right" style="margin: 30px 20px 20px 30px;}">
<div style="max-width: 85px;max-height: 85px;margin:0 auto;margin-bottom: 20px;">
<div class="price">
<div class="price_top" style="">
<div style="float:left;">
           ab

                  </div>
</div>
<div style="clear: both; float: left; vertical-align: top; width: 85px; margin-top: -3px;">
<div class="price_price" style="width: 65px;">
                  1,-
              </div>
</div>
<div class="price_footer">
              € / Monat<sup>1</sup>
</div>
</div>
<div class="price_disturber" style="width: 100px;">
      Bis 31.01.2021!
    </div>
</div>
</div>
<h2>Performancepakete</h2>
<h4>Hosting mit NVMe-Technologie – Der Turbo für Ihre Website!</h4>
<ul style="color: whitesmoke;">
<li>High-End NVMe-SSD Festplatten sorgen für ultraschnelle Zugriffe auf Ihre Datenbanken und Dateien.</li>
<li>Höchste Performance für Ihre Webseite ist garantiert - Ideal für Wordpress, Joomla &amp; eCommerce.</li>
<li>Kostenlose SSL-Verschlüsselung mit Let´s Encrypt für alle Domains</li>
</ul>
<a class="btn btn-default" href="/webhosting/performancepakete/" role="button">Mehr »</a><br/>
<br/>
</div>
</div>
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<a class="panellink" href="/kontakt">
<div class="panel panel-primary panel-hover">
<div class="panel-heading">
<h3 class="panel-title">Kontakt &amp; Support</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/support.jpg" style="max-width:40%;margin-left: 10px; float: right;"/>
                        Sie haben Fragen zu unseren Produkten und Services oder benötigen Hilfe? Wir sind für Sie da.
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</a>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="panel panel-primary panel-hover" onclick="$(this).magnificPopup({items: {src: '/popups/hoechste_sicherheit', type: 'ajax'}}).magnificPopup('open');">
<div class="panel-heading">
<h3 class="panel-title">Server-Standort Deutschland</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/serverstandort_deutschland.jpg" style="max-width:55%;margin-left: 10px; float: right;"/>
                        Sämtliche 1blu- Serversysteme befinden sich in Deutschland - in unserem Rechenzentrum in Frankfurt/Main.
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="panel panel-primary panel-hover" onclick="$(this).magnificPopup({items: {src: '/popups/ct-test-100prozent-erreichbarkeit', type: 'ajax'}}).magnificPopup('open');">
<div class="panel-heading">
<h3 class="panel-title">1blu mit Erreichbarkeit von 100%</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/ct-logo.jpg" style="max-width:100%;margin-left: 10px; float: right;"/>
                        Im Test (Ausgabe 20/16) Webhosting-Pakete für dynamische Inhalte von acht großen Providern. 
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<a class="panellink" href="/webbaukasten/">
<div class="panel panel-primary panel-hover">
<div class="panel-heading">
<h3 class="panel-title">Webbaukasten</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/hbk.jpg" style="max-width:40%;margin-left: 10px; float: right;"/>
                        Mit dem 1blu-Webbaukasten erstellen Sie über eine intuitive bedienbare Weboberfläche Ihre eigene Website – natürlich optimiert für die Darstellung auf Mobilgeräten.
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</a>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<div class="panel panel-primary panel-hover" onclick="$(this).magnificPopup({items: {src: '/popups/webdesign-service', type: 'ajax'}}).magnificPopup('open');">
<div class="panel-heading">
<h3 class="panel-title">Webdesign-Service</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/seitenl_screenwebdesign.gif" style="max-width:50%;margin-left: 10px; float: right;"/>
                        Wir erstellen für SIe ein individuell angepasstes Website-Design.
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
<a class="panellink" href="/easyapps">
<div class="panel panel-primary panel-hover">
<div class="panel-heading">
<h3 class="panel-title">1-Klick-Installationen</h3>
</div>
<div class="panel-body indexBoxes" style="padding-bottom: 0px;">
<div>
<img alt="Panel Bild" class="img-responsive" src="/styles/1blu.de/easyapps.png" style="max-width:50%;margin-left: 10px; float: right;"/>
                        Über 100 topaktuelle Apps per Mausklick: Für Community-Software, CMS, eCommerce, Statistiken, Bilder und Dateien.
          </div>
</div>
<div class="panel-footer " style="background-color: #ffffff; padding-top: 0px;">
<span class="btn btn-default cursor_pointer " role="button">Mehr »</span>
</div>
</div>
</a>
</div>
</div>
</div>
<div class="row footer">
<div class="col-xs-12">
<hr/>
<div style="float:left;">
<a href="https://de-de.facebook.com/1bluAG" target="_blank" title="Facebook"><img alt="Facebook Logo" src="/styles/1blu.de/fb_logo.png"/></a>
<a href="https://twitter.com/1blu_ag" target="_blank" title="Twitter"><img alt="Twitter Logo" src="/styles/1blu.de/twitter_logo.png"/></a>
</div>
<div style="float:right;padding-top:5px;">
<a href="/agb/">AGB</a> |
  <a href="/datenschutz/">Datenschutz</a> |
  <a href="/impressum/">Impressum</a> |
  <a href="/karriere/">Karriere</a> |
  <a href="/partnerprogramm/">Partnerprogramm</a> |
  <a href="/business-hosting/">Großkunden/Reseller</a> |
  <a href="/unternehmen/">Unternehmen</a> |
  <a href="/presse/">Presse</a> |
  <a href="javascript:myCookieOpen();">Cookie-Einstellung</a>
</div>
</div>
</div>
<div class="row smalltext">
<div class="col-xs-12">
<div class="row footnotetoggle" id="footnotetoggle" onclick='$("#footnotes").toggle();$("#footnotetoggle_open").toggle();$("#footnotetoggle_close").toggle();'>
<div class="col-xs-12" id="footnotetoggle_open">
<button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-chevron-right"></span></button> Weiterführende Preisinformationen (*, Ziffer 1-4) einblenden
  </div>
<div class="col-xs-12 collapse" id="footnotetoggle_close">
<button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-chevron-down"></span></button> Weiterführende Preisinformationen (*, Ziffer 1-4) ausblenden
  </div>
</div>
<div class="collapse" id="footnotes">
<sup>1</sup> Alle Preise pro Monat inkl. 19% MwSt. Einmalige Einrichtungsgebühr für Homepage-Pakete ab 1blu-Homepage und Performance-Pakete jeweils 6,90 €,
    diese entfällt für 1blu-Homepage, 1blu-Homepage Power, 1blu-Homepage Professional und 1blu-Performance-Paket M bei Bestellung bis 31.01.2021. Einmalige
    Einrichtungsgebühr für 1blu-Homepage A, 1blu-Homepage X und 1blu-Web-Domain jeweils 4,90 €, diese entfällt für 1blu-Homepage A bei Bestellung bis
    31.01.2021. Keine einmalige Einrichtungsgebühr für Domain-Pakete. Verträge jeweils jederzeit kündbar mit einem Monat Frist zum Ende der Vertragslaufzeit.
    <br/><br/>
    Angebot für 1,- €/Monat: Bei Bestellung bis 31.01.2021 1blu-Homepage A für 1,- €/Monat für die jeweils erste Vertragslaufzeit von 12 Monaten, danach
    regulärer Preis von 4,29 €/Monat.
    <br/><br/>
    Angebot für 1,- €/Monat: Bei Bestellung bis 31.01.2021 1blu-Performance-Paket M für 1,- €/Monat für die jeweils erste Vertragslaufzeit von 3 Monaten, danach
    regulärer Preis von 8,90 €/Monat.
    <br/><br/>
<sup>2</sup> Alle Preise pro Monat inkl. 19% MwSt. Einmalige Einrichtungsgebühr für 1blu-VPS und 1blu-RootServer jeweils 9,90 € bei einer Vertragslaufzeit
    von einem Monat; keine Einrichtungsgebühr bei einer Vertragslaufzeit von 12 Monaten. Einmalige Einrichtungsgebühr für 1blu-DedicatedServer jeweils 89,- €
    bei einer Vertragslaufzeit von einem Monat, keine einmalige Einrichtungsgebühr bei einer Laufzeit von 6 Monaten. Verträge jeweils jederzeit kündbar mit
    einem Monat Frist zum Ende der Vertragslaufzeit.
    <br/><br/>
    Angebot für 1,- €/Monat: Bei Bestellung bis 31.01.2021 1blu-VPS R8 mit einer Vertragslaufzeit von 12 Monaten jeweils für 1,- €/Monat in den ersten 12
    Monaten; danach jeweils regulärer Preis von 4,90 €/Monat.
    <br/><br/>
    Angebote für 1,- €/Monat: Bei Bestellung bis 31.01.2021 1blu-RootServer mit einer Vertragslaufzeit von 12 Monaten jeweils für 1,- €/Monat in den ersten 6
    Monaten; danach jeweils regulärer Preis/Monat (z.B. 1blu-RootServer X 18,90 €, 1blu-RootServer 6X 59,90 €).
    <br/><br/>
<sup>3</sup> Preis/Monat, alle Preise verstehen sich zuzüglich der gesetzlichen Mehrwertsteuer. Vertragslaufzeit Dedizierter Server für 199,- €/Monat
    jeweils 12 Monate, die Kündigungsfrist beträgt einen Monat zum Vertragsende.
    <br/><br/>
<sup>4</sup> Preis/Monat inkl. 19% MwSt. Einrichtungsgebühr jeweils einmalig 4,90 €. Aktion bis 31.01.2021: 1blu-Webbaukasten „Business“ für 0,- €/Monat in
    der ersten Vertragslaufzeit von 12 Monaten, danach jeweils regulärer Preis von 3,90 €/Monat. Vertragslaufzeit jeweils wahlweise 1 Monat oder 12 Monate.
    Verträge jeweils jederzeit kündbar mit einem Monat Frist zum Ende der Vertragslaufzeit. Widgets (z.B. Shop, Paypal, Blocks, Social Media) inklusive ab
    Webbaukasten „Business“.
  </div>
</div>
</div>
<div class="row wordcloud">
<div class="col-xs-12">
<hr/>
<h3><a href="/" style="white-space: nowrap;">Webhosting</a></h3>
<h5><a href="/webhosting/homepagepakete/" style="white-space: nowrap;">eigene Homepage</a></h5>
<h5><a href="/webhosting/performancepakete/" style="white-space: nowrap;">leistungsstarkes
              Webhosting</a></h5>
<h4><a href="/online-speicher/" style="white-space: nowrap;">Online-Speicher</a></h4>
<h5><a href="/server/vserver/" style="white-space: nowrap;">vServer</a></h5>
<h5><a href="/webhosting/managedhosting/" style="white-space: nowrap;">ManagedServer</a></h5>
<h4><a href="/" style="white-space: nowrap;">Hosting</a></h4>
<h5><a href="/webhosting/managedhosting/" style="white-space: nowrap;">gemanagte Server</a></h5>
<h5><a href="/server/vserver/" style="white-space: nowrap;">Virtuelle Server</a></h5>
<h5><a href="/ecommerce/eshops/" style="white-space: nowrap;">Online-Shops</a></h5>
<h5><a href="/seo-master/" style="white-space: nowrap;">SEO-Master</a></h5>
</div>
</div>
<div id="mycookie-box1" style="display: none;">
<div id="mycookie-logo"></div>
<span class="mycookie-heading">Cookie-Einstellungen für 1blu.de</span>
<p style="clear: left;">Wir verwenden Cookies, um unsere Webseite und Angebote für Sie optimal und benutzerfreundlich zu gestalten. Hierbei entscheiden Sie,
    welche Cookies Sie
    zulassen möchten. Durch Bestätigen des Buttons "Allen zustimmen" stimmen Sie der Verwendung aller Cookies zu. Über den Button "Einstellungen" können Sie
    auswählen, welche Cookies Sie zulassen wollen. Die Übertragung Ihrer Daten erfolgt selbstverständlich ausschließlich verschlüsselt.</p>
<p>Ihre Auswahl können Sie später jederzeit am Seitenende über den Link "Cookie-Einstellungen" wieder ändern.</p>
<a class="mycookie-btn mycookie-config-btn" href="#" onclick="myCookieBtnNext();" style="float: left" type="button">Einstellungen</a>
<a class="mycookie-btn mycookie-ok-btn" href="#" onclick="myCookieBtnAllowAll();" style="float:right;" type="button">Allen zustimmen</a>
</div>
<div id="mycookie-box2" style="display: none;">
<div id="mycookie-logo"></div>
<span class="mycookie-heading">Cookies auf 1blu.de</span>
<p style="clear: left;"></p>
<span class="mycookie-heading2">Notwendige Cookies</span>
<label class="mycookie-switch">
<input checked="" class="mycookieele" disabled="" type="checkbox" value="1"/>
<span class="mycookie-slider round"></span>
</label>
<p style="clear: left;"></p>
<br/>
  Technisch erforderliche Cookies sind für die Navigation auf unserer Website zwingend notwendig. Die Auswahl und Bestellung von Produkten oder die Nutzung des
  Kundenlogins sind ohne sie nicht möglich.<br/>
<br/>
<hr class="cookie-hr"/>
<br/>
<span class="mycookie-heading2">Marketing / Partnerschaften</span>
<label class="mycookie-switch">
<input class="mycookieele" type="checkbox" value="2"/>
<span class="mycookie-slider round"></span>
</label>
<p style="clear: left;"></p>
<br/>
  Um unsere Webinhalte für Sie komfortabel zu gestalten, erfassen wir beispielsweise Informationen zu Nutzernavigation und Fehlermeldungen. Darüber hinaus
  existieren Partnerschaften mit externen Unternehmen (z.B. Affilinet-Anbieter), die über auf unserer Webseite eingebundene Cookies personenbezogene Daten
  erhalten und verarbeiten.<br/>
<br/>
<a class="mycookie-btn mycookie-config-btn" href="#" onclick="myCookieBtnBack();" style="float:left;" type="button">Zurück</a>
<a class="mycookie-btn mycookie-ok-btn" href="#" onclick="myCookieBtnAllowAll();" style="float:right; margin-left: 10px;" type="button">Allen zustimmen &amp;
    speichern</a>
<a class="mycookie-btn mycookie-ok-btn" href="#" onclick="myCookieBtnSaveAllowed();" style="float:right;" type="button">Ausgewählte speichern</a>
</div>
<link href="/css/1cc3368.css" rel="stylesheet"/>
<script src="/js/5972672.js" type="text/javascript"></script>
<script src="/js/fe56ad5.js" type="text/javascript"></script>
</div>
<script src="/js/215113c.js" type="text/javascript"></script>
</body>
</html>