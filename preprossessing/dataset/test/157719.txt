<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en-CA"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="en-CA"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="en-CA"> <![endif]--><!--[if IE 9 ]><html class="ie ie9" lang="en-CA"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html lang="en-CA"> <!--<![endif]-->
<head>
<title>Error 404 Not Found | Ambulatory Surgical Center of Stevens Point</title>
<meta content=" » Page not found | Outpatient Surgery | Ambulatory Surgical Center of Stevens Point" name="description"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="//gmpg.org/xfn/11" rel="profile"/>
<link href="https://ascstevenspoint.com/wp-content/themes/theme51357/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://ascstevenspoint.com/xmlrpc.php" rel="pingback"/>
<link href="https://ascstevenspoint.com/feed/" rel="alternate" title="Ambulatory Surgical Center of Stevens Point" type="application/rss+xml"/>
<link href="https://ascstevenspoint.com/feed/atom/" rel="alternate" title="Ambulatory Surgical Center of Stevens Point" type="application/atom+xml"/>
<link href="https://ascstevenspoint.com/wp-content/themes/theme51357/bootstrap/css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/themes/theme51357/bootstrap/css/responsive.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/themes/CherryFramework/css/camera.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/themes/theme51357/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//netdna.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://ascstevenspoint.com/feed/" rel="alternate" title="Ambulatory Surgical Center of Stevens Point » Feed" type="application/rss+xml"/>
<link href="https://ascstevenspoint.com/comments/feed/" rel="alternate" title="Ambulatory Surgical Center of Stevens Point » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ascstevenspoint.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/flexslider.css?ver=2.2.0" id="flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.carousel.css?ver=1.24" id="owl-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/owl-carousel/owl.theme.css?ver=1.24" id="owl-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=3.2.1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/includes/css/cherry-plugin.css?ver=1.2.7" id="cherry-plugin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/themes/theme51357/main-style.css" id="theme51357-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/themes/CherryFramework/css/magnific-popup.css?ver=0.9.3" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/motopress-content-editor/includes/css/theme.css?ver=1.5.8" id="mpce-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://ascstevenspoint.com/wp-content/plugins/motopress-content-editor/bootstrap/bootstrap-grid.min.css?ver=1.5.8" id="mpce-bootstrap-grid-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery-1.7.2.min.js?ver=1.7.2" type="text/javascript"></script>
<script id="easing-js" src="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/jquery.easing.1.3.js?ver=1.3" type="text/javascript"></script>
<script id="elastislide-js" src="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/elasti-carousel/jquery.elastislide.js?ver=1.2.7" type="text/javascript"></script>
<script id="googlemapapis-js" src="//maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;ver=5.6" type="text/javascript"></script>
<script id="parallaxSlider-js" src="https://ascstevenspoint.com/wp-content/themes/theme51357/js/parallaxSlider.js?ver=1.0" type="text/javascript"></script>
<script id="migrate-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery-migrate-1.2.1.min.js?ver=1.2.1" type="text/javascript"></script>
<script id="swfobject-js" src="https://ascstevenspoint.com/wp-includes/js/swfobject.js?ver=2.2-20120417" type="text/javascript"></script>
<script id="modernizr-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/modernizr.js?ver=2.0.6" type="text/javascript"></script>
<script id="jflickrfeed-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jflickrfeed.js?ver=1.0" type="text/javascript"></script>
<script id="custom-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/custom.js?ver=1.0" type="text/javascript"></script>
<script id="bootstrap-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/bootstrap/js/bootstrap.min.js?ver=2.3.0" type="text/javascript"></script>
<link href="https://ascstevenspoint.com/wp-json/" rel="https://api.w.org/"/><link href="https://ascstevenspoint.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://ascstevenspoint.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<!-- Qeryz code for ascstevenspoint.com -->
<!-- Paste this code right before the end of your <body> tag on every page of your site -->
<!-- Don't forget to activate your survey -->
<script src="https://qeryz-seohacker.netdna-ssl.com/survey/js/qryz_v3.2.js" type="text/javascript"></script>
<script type="text/javascript">
   var qRz = qRz || [];
   (function() {
      setTimeout(function(){
        var qryz_plks = document.createElement('div');
        qryz_plks.id = 'qryz_plks';
        qryz_plks.className = 'qryz_plks';
        document.body.appendChild(qryz_plks);
        qryzInit2('3493');
      },0);
   })();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52832914-1', 'auto');
  ga('send', 'pageview');

</script><script>
 var system_folder = 'https://ascstevenspoint.com/wp-content/themes/CherryFramework/admin/data_management/',
	 CHILD_URL ='https://ascstevenspoint.com/wp-content/themes/theme51357',
	 PARENT_URL = 'https://ascstevenspoint.com/wp-content/themes/CherryFramework', 
	 CURRENT_THEME = 'theme51357'</script>
<style type="text/css">
</style>
<style type="text/css">
h1 { font: normal 30px/35px Arial, Helvetica, sans-serif;  color:#333333; }
h2 { font: normal 37px/42px Roboto, sans-serif;  color:#2d2d30; }
h3 { font: normal 17px/20px Roboto, sans-serif;  color:#039684; }
h4 { font: normal 17px/24px Roboto, sans-serif;  color:#039684; }
h5 { font: normal 20px/30px Roboto, sans-serif;  color:#039684; }
h6 { font: normal 12px/18px Arial, Helvetica, sans-serif;  color:#333333; }
body { font-weight: normal;}
.logo_h__txt, .logo_link { font: normal 54px/60px Roboto, sans-serif;  color:#2d2d30; }
.sf-menu > li > a { font: normal 20px/24px Roboto, sans-serif;  color:#ffffff; }
.nav.footer-nav a { font: normal 20px/30px Roboto, sans-serif;  color:#2d2d30; }
</style>
<link href="https://ascstevenspoint.com/wp-content/uploads/2020/04/cropped-favicon-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://ascstevenspoint.com/wp-content/uploads/2020/04/cropped-favicon-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://ascstevenspoint.com/wp-content/uploads/2020/04/cropped-favicon-180x180.png" rel="apple-touch-icon"/>
<meta content="https://ascstevenspoint.com/wp-content/uploads/2020/04/cropped-favicon-270x270.png" name="msapplication-TileImage"/>
<!--[if lt IE 9]>
		<div id="ie7-alert" style="width: 100%; text-align:center;">
			<img src="http://tmbhtest.com/images/ie7.jpg" alt="Upgrade IE 8" width="640" height="344" border="0" usemap="#Map" />
			<map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" /><area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />
			</map>
		</div>
	<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery.mobile.customized.min.js" type="text/javascript"></script>
<script type="text/javascript">
			jQuery(function(){
				jQuery('.sf-menu').mobileMenu({defaultText: "Navigate to..."});
			});
		</script>
<!--<![endif]-->
<script type="text/javascript">
		// Init navigation menu
		jQuery(function(){
		// main navigation init
			jQuery('ul.sf-menu').superfish({
				delay: 1000, // the delay in milliseconds that the mouse can remain outside a sub-menu without it closing
				animation: {
					opacity: "show",
					height: "show"
				}, // used to animate the sub-menu open
				speed: "normal", // animation speed
				autoArrows: false, // generation of arrow mark-up (for submenu)
				disableHI: true // to disable hoverIntent detection
			});

		//Zoom fix
		//IPad/IPhone
			var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
				ua = navigator.userAgent,
				gestureStart = function () {
					viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
				},
				scaleFix = function () {
					if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
						viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
						document.addEventListener("gesturestart", gestureStart, false);
					}
				};
			scaleFix();
		})
	</script>
<!-- stick up menu -->
<script type="text/javascript">
		jQuery(document).ready(function(){
			if(!device.mobile() && !device.tablet()){
				jQuery('header .custom_poz').tmStickUp({
					correctionSelector: jQuery('#wpadminbar')
				,	listenSelector: jQuery('.listenSelector')
				,	active: true				,	pseudo: true				});
			}
		})
	</script>
</head>
<body class="error404">
<div class="main-holder" id="motopress-main">
<!--Begin #motopress-main-->
<header class="motopress-wrapper header">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5ffe0ca654b68" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
<div class="custom_poz0">
<div class="container">
<div class="row block_head_info">
<div class="span4" data-motopress-static-file="static/static-logo.php" data-motopress-type="static">
<!-- BEGIN LOGO -->
<div class="logo pull-left">
<a class="logo_h logo_h__img" href="https://ascstevenspoint.com/"><img alt="Ambulatory Surgical Center of Stevens Point" src="https://ascstevenspoint.com/wp-content/themes/theme51357/images/logo.png" title="Outpatient Surgery | Ambulatory Surgical Center of Stevens Point"/></a>
<p class="logo_tagline">Outpatient Surgery | Ambulatory Surgical Center of Stevens Point</p><!-- Site Tagline -->
</div>
<!-- END LOGO --> </div>
<div class="span8 header_widgets">
<div data-motopress-sidebar-id="header-sidebar-1" data-motopress-type="dynamic-sidebar">
<div class="visible-all-devices footer_txt " id="text-8"> <div class="textwidget"><a href="https://secure.epayhealthcare.com/assp560epay" rel="noopener" target="_new"><img border="0" src="https://ascstevenspoint.com/wp-content/uploads/2015/01/payonlineicon.png"/></a></div>
</div><div class="visible-all-devices header_info " id="text-6"> <div class="textwidget"><font size="+2">"Skill, Care, and Comfort <span class="color_1">Beyond Your Expectations</span>"</font> <br/>
500 Vincent Street Suite A, Stevens Point, WI - 715-345-0500 - fax 715-345-0400</div>
</div><div id="text-10"> <div class="textwidget"><div class="search-form search-form__h hidden-phone clearfix">
<form accept-charset="utf-8" action="" class="navbar-form pull-right" id="search-header" method="get">
<input class="search-form_it" name="s" placeholder="search your question here" type="text"/>
<input class="search-form_is btn btn-primary" id="search-form_is" type="submit" value="search"/>
</form>
</div></div>
</div> </div>
</div>
</div>
</div>
</div>
<div class="custom_poz">
<div class="container">
<div class="row block_menu">
<div class="span12 cont">
<div class="row">
<div class="span12">
<div data-motopress-static-file="static/static-nav.php" data-motopress-type="static">
<!-- BEGIN MAIN NAVIGATION -->
<nav class="nav nav__primary clearfix">
<ul class="sf-menu" id="topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home" id="menu-item-1914"><a href="https://ascstevenspoint.com/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2726"><a>About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-taxonomy menu-item-object-category" id="menu-item-2988"><a href="https://ascstevenspoint.com/category/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2435"><a href="https://ascstevenspoint.com/about/faqs/">FAQs</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2728"><a>Meet Our Surgeons</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2437"><a href="https://ascstevenspoint.com/portfolio/anesthesiapain-management/">Anesthesia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2438"><a href="https://ascstevenspoint.com/portfolio/ophthalmology/">Ophthalmology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2439"><a href="https://ascstevenspoint.com/portfolio/orthopedics/">Orthopedics</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2441"><a href="https://ascstevenspoint.com/portfolio/podiatry/">Podiatry</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2442"><a href="https://ascstevenspoint.com/portfolio/urology-2/">Urology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2443"><a href="https://ascstevenspoint.com/portfolio/spine/">Spine</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent" id="menu-item-2444"><a href="https://ascstevenspoint.com/news/">News</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom" id="menu-item-2908"><a href="https://ascstevenspoint.com/wp-content/uploads/2016/08/ADM-DISCRIMINATIONNOTICEACAREQUIREDJULY2016.pdf">Non-discrimination Notice</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2913"><a href="https://ascstevenspoint.com/billing-information/pricing-transparency/">Procedure Cost – Pricing Transparency</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2434"><a href="https://ascstevenspoint.com/about/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2814"><a href="https://ascstevenspoint.com/about/careers/">Careers</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2727"><a>Patient &amp; Billing Info</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2882"><a href="https://ascstevenspoint.com/billing-information/cash-based-procedures-2/">Cash Based Procedures</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom" id="menu-item-2479"><a href="https://ascstevenspoint.com/wp-content/uploads/2019/03/ASCBooklet14-2018.pdf">Patient Information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2429"><a href="https://ascstevenspoint.com/billing-information/pricing-transparency/">Procedure Cost – Pricing Transparency</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2430"><a href="https://ascstevenspoint.com/billing-information/insurances-we-accept/">Insurances We Accept</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2431"><a href="https://ascstevenspoint.com/billing-information/information-about-your-bill/">Information About Your Bill</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2432"><a href="https://ascstevenspoint.com/billing-information/billing-terminology/">Billing Terminology</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom" id="menu-item-2773"><a href="https://ascstevenspoint.simpleadmit.com/">On line Registration</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2746"><a>Procedures</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2748"><a href="https://ascstevenspoint.com/portfolio/anesthesiapain-management/">Anesthesia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2749"><a href="https://ascstevenspoint.com/portfolio/ophthalmology/">Ophthalmology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2750"><a href="https://ascstevenspoint.com/portfolio/orthopedics/">Orthopedics</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2752"><a href="https://ascstevenspoint.com/portfolio/podiatry/">Podiatry</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2753"><a href="https://ascstevenspoint.com/portfolio/urology-2/">Urology</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2754"><a href="https://ascstevenspoint.com/portfolio/spine/">Spine</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" id="menu-item-2743"><a>Joint Replacements</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2639"><a href="https://ascstevenspoint.com/total-joint-replacements/our-joint-program/">Outpatient Joint Replacement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2663"><a href="https://ascstevenspoint.com/total-joint-replacements/joint-replacement-faq/">FAQ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2642"><a href="https://ascstevenspoint.com/total-joint-replacements/total-knee-replacement/">Total Knee Replacement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2640"><a href="https://ascstevenspoint.com/total-joint-replacements/uni-compartmental-knee-replacement/">Partial Knee Replacement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2641"><a href="https://ascstevenspoint.com/total-joint-replacements/total-shoulder-replacement/">Total Shoulder Replacement</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2643"><a href="https://ascstevenspoint.com/total-joint-replacements/total-hip-replacement/">Total Hip Replacement</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-2445"><a href="https://ascstevenspoint.com/contacts/">Contacts</a></li>
</ul></nav><!-- END MAIN NAVIGATION --> </div>
</div>
<div class="span4">
<div class="hidden-phone" data-motopress-static-file="static/static-search.php" data-motopress-type="static">
<!-- BEGIN SEARCH FORM -->
<!-- END SEARCH FORM --> </div>
</div>
</div>
</div>
</div>
</div>
</div> </div>
</div>
</div>
</header>
<div class="motopress-wrapper content-holder clearfix">
<div class="container">
<div class="row">
<div class="span12" data-motopress-wrapper-file="404.php" data-motopress-wrapper-type="content">
<div class="row error404-holder">
<div class="span7 error404-holder_num" data-motopress-static-file="static/static-404.php" data-motopress-type="static">
						404					</div>
<div class="span5" data-motopress-static-file="static/static-not-found.php" data-motopress-type="static">
<div class="hgroup_404">
<h1>Sorry!</h1> <h2>Page Not Found</h2></div>
<h4>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</h4><p>Please try using our search box below to look for information on the internet.</p>
<div class="search-form">
<form accept-charset="utf-8" action="https://ascstevenspoint.com" id="searchform" method="get">
<input class="search-form_it" id="s" name="s" type="text" value=""/>
<input class="search-form_is btn btn-primary" id="search-submit" type="submit" value="search"/>
</form>
</div> </div>
</div>
</div>
</div>
</div>
</div>
<footer class="motopress-wrapper footer">
<div class="container">
<div class="row">
<div class="span12" data-motopress-id="5ffe0ca65be3d" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
<div class="row footer-widgets">
<div class="span4">
<div data-motopress-sidebar-id="footer-sidebar-2" data-motopress-type="dynamic-sidebar">
</div>
</div>
<div class="span4">
<div data-motopress-sidebar-id="footer-sidebar-3" data-motopress-type="dynamic-sidebar">
</div>
</div>
<div class="span4">
<div data-motopress-sidebar-id="footer-sidebar-4" data-motopress-type="dynamic-sidebar">
</div>
</div>
</div>
<div class="row">
<div class="span8">
<div data-motopress-static-file="static/static-footer-nav.php" data-motopress-type="static">
<nav class="nav footer-nav">
<ul class="menu" id="menu-footer-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2781" id="menu-item-2781"><a>Employee Access:</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2780" id="menu-item-2780"><a href="http://mail.host5.wiwebhost.com/">WebMail</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3182" id="menu-item-3182"><a href="https://ascstevenspoint.com/wp-content/uploads/2017/11/CURRENT_NoticeofPrivacyPracticesSeptember2013.pdf">Notice of Privacy Practices</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2783" id="menu-item-2783"><a>Your Retirement Plan</a></li>
</ul> </nav>
</div>
</div>
<div class="span4">
<div data-motopress-sidebar-id="footer-sidebar-1" data-motopress-type="dynamic-sidebar">
<div class="visible-all-devices footer_txt " id="text-5"> <div class="textwidget">500 Vincent Street Suite A, Stevens Point, WI - 715-345-0500 - fax-715-345-0400</div>
</div> </div>
</div>
</div>
<div class="row">
<div class="span12">
<div data-motopress-static-file="static/static-footer-text.php" data-motopress-type="static">
<div class="footer-text" id="footer-text">
	
			Ambulatory Surgical Center of Stevens Point | <a href="https://ascstevenspoint.com/wp-content/uploads/2016/08/NoticeofPrivacyPractices-September2013.pdf">Notice of Privacy Practices</a> </div> </div>
</div>
</div> </div>
</div>
</div>
</footer>
<!--End #motopress-main-->
</div>
<div class="visible-desktop" id="back-top-wrapper">
<p id="back-top">
<a href="#top"><span></span></a> </p>
</div>
<script type="text/javascript">
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-52832914-1', 'ascstevenspoint.com');
ga('require', 'displayfeatures');
ga('send', 'pageview');
		</script>
<!-- Show Google Analytics -->
<script type="text/javascript">
	var GETGA_settings = {"first_delay":"2","second_delay":"3","debug":"none"};
	var GETGA_events = [{"selector":".gtrack","description":"Generic Event Tracker","category":"Default","action_type":"click","action_label":"Default Item Clicked","label":"Default_{ITEM_TITLE}_{PAGE_URL}","status":"active"},{"selector":"a[href$=\\&quot;.pdf\\&quot;], a[href$=\\&quot;.doc\\&quot;], a[href$=\\&quot;.docx\\&quot;], a[href$=\\&quot;.ods\\&quot;], a[href$=\\&quot;.odt\\&quot;], a[href$=\\&quot;.xls\\&quot;], a[href$=\\&quot;.xlsx\\&quot;], a[href$=\\&quot;.txt\\&quot;], a[href$=\\&quot;.zip\\&quot;], a[href$=\\&quot;.csv\\&quot;]","description":"Downloads - pdf, doc(x), xls(x), txt, zip, csv","category":"Downloads","action_type":"click","action_label":"Downloaded","label":"Download_{ITEM_TITLE}_{PAGE_RELATIVE_URL}_{LINK_RELATIVE_URL}","status":"active"},{"selector":"input[type=submit]","description":"All Submit Buttons","category":"Form Submits","action_type":"click","action_label":"Form Submitted","label":"Form_Submitted_{TAG_HTML}_{PAGE_RELATIVE_URL}","status":"active"},{"selector":"form","description":"All Form Submissions","category":"Form Submits","action_type":"submit","action_label":"Form Submitted","label":"Form_Submitted_{TAG_HTML}_{PAGE_RELATIVE_URL}","status":"active"},{"selector":".gtrackexternal","description":"All External Links","category":"Links","action_type":"click","action_label":"External Links","label":"External_Link_Clicked_{TAG_HTML}_{PAGE_RELATIVE_URL}","status":"active"},{"selector":"window","description":"Resized","category":"Resized","action_type":"resize","action_label":"Resized","label":"Resized_{PAGE_RELATIVE_URL}","status":"active"},{"selector":"window","description":"Scrolled","category":"Scrolled","action_type":"scroll","action_label":"Scrolled","label":"Scrolled_{PAGE_RELATIVE_URL}","status":"active"},{"selector":"window","description":"Scrolled Depth","category":"Scrolled","action_type":"scrolldepth","action_label":"Scrolled Depth","label":"Scrolled_{SCROLL_PERCENTAGE}_{PAGE_RELATIVE_URL}","status":"active"}];
	</script>
<script defer="defer" src="https://ascstevenspoint.com/wp-content/plugins/gravitate-event-tracking/gravitate_event_tracking.js?v=1.5.3" type="text/javascript"></script>
<script id="flexslider-js" src="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/lib/js/FlexSlider/jquery.flexslider-min.js?ver=2.2.2" type="text/javascript"></script>
<script id="cherry-plugin-js-extra" type="text/javascript">
/* <![CDATA[ */
var items_custom = [[0,1],[480,2],[768,3],[980,4],[1170,5]];
/* ]]> */
</script>
<script id="cherry-plugin-js" src="https://ascstevenspoint.com/wp-content/plugins/cherry-plugin/includes/js/cherry-plugin.js?ver=1.2.7" type="text/javascript"></script>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/ascstevenspoint.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://ascstevenspoint.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3" type="text/javascript"></script>
<script id="chrome-smoothing-scroll-js" src="https://ascstevenspoint.com/wp-content/themes/theme51357/js/smoothing-scroll.js?ver=1.0" type="text/javascript"></script>
<script id="superfish-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/superfish.js?ver=1.5.3" type="text/javascript"></script>
<script id="mobilemenu-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery.mobilemenu.js?ver=1.0" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery.magnific-popup.min.js?ver=0.9.3" type="text/javascript"></script>
<script id="playlist-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jplayer.playlist.min.js?ver=2.3.0" type="text/javascript"></script>
<script id="jplayer-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery.jplayer.min.js?ver=2.6.0" type="text/javascript"></script>
<script id="tmstickup-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/tmstickup.js?ver=1.0.0" type="text/javascript"></script>
<script id="device-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/device.min.js?ver=1.0.0" type="text/javascript"></script>
<script id="zaccordion-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/jquery.zaccordion.min.js?ver=2.1.0" type="text/javascript"></script>
<script id="camera-js" src="https://ascstevenspoint.com/wp-content/themes/CherryFramework/js/camera.min.js?ver=1.3.4" type="text/javascript"></script>
<script id="wp-embed-js" src="https://ascstevenspoint.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script type="text/javascript">
				deleteCookie('cf-cookie-banner');
			</script>
<!-- this is used by many Wordpress features and for plugins to work properly -->
</body>
</html>