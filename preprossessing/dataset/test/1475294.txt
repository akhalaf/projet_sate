<!DOCTYPE html>
<html lang="en">
<head>
<title>Treetops Soft Play - Ely</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if lt IE 9]>
<script type="text/javascript" src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script type="text/javascript" src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lte IE 8]>
<script type="text/javascript">
  if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, start) {
      for (var i = (start || 0), j = this.length; i < j; i++) {
        if (this[i] === obj) {
          return i;
        }
      }
      return -1;
    }
  }
  
  if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
      return this.replace(/^\s+|\s+$/g, '');
    };
  };
</script>
<![endif]-->
<link href="https://www.treetopsely.co.uk/" rel="canonical"/>
<link href="//fonts.googleapis.com/css?family=Merienda+One" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Amatic+SC:400,700" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/minify_css.php" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico?ver=4" rel="shortcut icon"/>
<script type="text/javascript">
  (function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)
</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12&appId=191391444800375&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="eupopup eupopup-bottom"></div>
<style type="text/css">
<!--

.navbar {
	background: #FFFFFF;
	/*height: 150px;*/
}

.navbar-nav {
	margin-top: 50px;
}

.navbar-default .navbar-nav > li > a {
	font-family: "Amatic SC", cursive;
	font-size: 32px;
	font-weight: bold;
	color: #000000;
}

.navbar-default .navbar-nav > li > a:hover {
	color: #FD393B;
}

.nav > li > a {
	padding: 10px 7px 10px 7px;
}

@media screen and (min-width: 991px) {
	
	.navbar-brand-centered {
		background-color: transparent;
		display: block;
		left: 50%;
		max-width: 300px;
		position: absolute;
		text-align: center;
	}
	
	.navbar > .container .navbar-brand-centered {
		margin-left: -150px;
	}
	
	nav.navbar.shrink {
		height: 125px;
		-webkit-transition: all 0.35s;
		-moz-transition: all 0.35s;
		transition: all 0.35s;
	}
	
	nav.navbar.shrink .navbar-brand-centered {
		max-width: 170px;
	}
	
	nav.navbar.shrink .navbar-brand-centered {
		margin-left: -85px;
	}
	
	nav.navbar.shrink #main-menu,
	nav.navbar.shrink #main-menu-right {
		margin-top: 19px;
	}
	
}

-->
</style>
<script>
	$(document).ready(function() {
		$(window).scroll(function() {
			if ($(document).scrollTop() > 50) {
				$('nav').addClass('shrink');
			} else {
				$('nav').removeClass('shrink');
			}
		});
	});
</script>
<nav class="navbar navbar-default navbar-fixed-top">
<div class="top-menu">
<div class="container">
<div class="col-md-4 col-xs-12">
<i aria-hidden="true" class="fa fa-phone"></i> Call us on 01353 665111 </div>
<div class="col-md-8 hidden-sm hidden-xs text-right">
<a href="/customer-login"><i aria-hidden="true" class="fa fa-lock"></i> My Account</a><a href="/customer-register"><i aria-hidden="true" class="fa fa-user"></i> Register</a>
<a href="/basket"><i aria-hidden="true" class="fa fa-shopping-basket"></i> Basket (0)</a> </div>
</div>
</div>
<div class="container">
<div class="navbar-header">
<button aria-controls="navbar2" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar2" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand navbar-brand-centered" href="/"><img alt="" class="logo img-responsive" src="/images/logo.png"/></a>
</div>
<div class="navbar-collapse collapse" id="navbar2">
<ul class="sm sm-blue" id="main-menu">
<li><a href="/">Home</a></li><li class="hidden-md hidden-lg"><a href="/basket">Basket</a></li><li class="hidden-md hidden-lg"><a href="/customer-login">My Account</a></li><li class="hidden-md hidden-lg"><a href="/customer-register">Register</a></li><li><a href="/play">Play</a></li><li><a href="/events">Events</a></li><li><a href="/parties">Parties</a></li><li><a href="/food">Food</a></li>
</ul>
<ul class="sm sm-blue" id="main-menu-right">
<li><a href="/bookings/booking/">Book Online</a></li><li><a href="/about">About</a></li><li><a href="/find-us">Find Us</a></li><li><a href="/contact">Contact Us</a></li>
</ul>
</div>
</div>
</nav>
<style>
<!--

#home-page-carousel .home-page-carousel-overlay {
	position: absolute;
	top: 0px;
}

-->
</style>
<div class="carousel-wrapper">
<div class="carousel slide animatedParent animateOnce" data-ride="carousel" id="home-page-carousel">
<ol class="carousel-indicators">
</ol>
<div class="carousel-inner" role="listbox">
<div class="parallax item active animated fadeIn" data-height="626" data-image="uploads/homepagebanners/cd0c1b05f7fa9e7a50860737c652979b.jpg" data-width="2000"><div class="carousel-caption animated growIn delay-2000 slow"><div class="carousel-caption-inner"><p>Treetops Soft Play - the year round indoor fun from 0-10 years</p></div></div></div> </div>
<a class="left carousel-control" data-slide="prev" href="#myCarousel" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#myCarousel" role="button">
<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
<div class="carousel-overlay animated fadeIn delay-500 slowest"></div>
</div>
</div>
<div id="page-wrapper-home-page">
<div class="container" id="home-page-content-container"><div class="row"><h1>Welcome to Ely Treetops Soft Play</h1></div><div class="row"><div class="col-md-12">
<p>Treetops Soft Play is a family run soft play facility offering a 3 storey play frame design in a safe, relaxed environment, situated on the spacious Lancaster Way Business Park, Witchford near Ely.<br/>
A fun exciting place where the parent/carer and children alike can enjoy quality food and drinks to match.<br/>
**New air conditioning installed for Summer 2019 - enjoy safe, family fun come rain or shine**</p>
</div>
<div class="clearfix"></div><div class="book-now-btn"><a class="btn btn-md btn-success" href="/bookings/booking/"><i aria-hidden="true" class="fa fa-calendar"></i> BOOK ONLINE</a></div></div></div><style type="text/css">
<!--

.home_page_events_container {
	margin-top: 30px;
	margin-bottom: 60px;
}

.home_page_events {
}

.home_page_events h2 {
	color: #F7941D;
	font-family: "Merienda One", cursive;
	font-size: 40px;
	margin-top: 0px;
	margin-bottom: 20px;
}

a.home_page_events_content_wrapper {
	background-color: #F7941D;
	display: block;
	height: 530px;
	position: relative;
	text-decoration: none;
	z-index: 1;
}

.home_page_events_content {
	background-color: #F7941D;
	bottom: 30px;
	left: 30px;
	position: absolute;
	right: 30px;
	top: 30px;
	z-index: 2;
}

.home_page_events_content p {
	color: #FFFFFF;
	font-size: 16px;
	line-height: 1.25em;
	margin-top: 20px;
	margin-bottom: 0px;
}

.home_page_events_content img {
	height: auto;
	width: 100%;
}

.home_page_events_content .btn-primary {
	background-color: #FFFFFF;
	border: 2px solid #FFFFFF !important;
	bottom: 0px;
	color: #F7941D !important;
	left: 0px;
	margin-top: 0px;
	margin-bottom: 0px;
	position: absolute;
	z-index: 3;
}

.home_page_events_content .btn-primary:hover, .home_page_events_content .btn-primary:focus, .home_page_events_content .btn-primary:active {
	background-color: #FD393B;
	background-image: none;
	border: 2px solid #FD393B !important;
	color: #FFFFFF !important;
	text-decoration: none !important;
}

-->
</style>
<div class="clearfix"></div>
<div class="home_page_events_container animatedParent animateOnce">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 home_page_events animated growIn" data-appear-top-offset="-300">
<h2>Events</h2>
<a class="home_page_events_content_wrapper" href="/events">
<div class="home_page_events_content">
<img alt="" class="zoom img-responsive center-block" src="/uploads/homepagesections/6b3c02dda96ee80e9ca34a41b61ab927.jpg"/>
<p>At Treetops we believe in offering more that soft play. We offer a variety of regular classes and one off events in addition to our fantastic play area. Please click link to see details of our regular events and check our Facebook page for special events.</p>
<button class="btn btn-primary">Events</button>
</div>
</a>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 home_page_events animated growIn" data-appear-top-offset="-300">
<h2>Parties</h2>
<a class="home_page_events_content_wrapper" href="/parties">
<div class="home_page_events_content">
<img alt="" class="zoom img-responsive center-block" src="/uploads/homepagesections/56a240dcde858435d9b63cf74301f6c5.jpg"/>
<p>We offer a range of different party packages. All prices are inclusive of use of party chute and party room.</p>
<button class="btn btn-primary">Parties</button>
</div>
</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<style type="text/css">
<!--
	.home-page-section2 .btn-primary {
	background-color: #FFFFFF;
	border: 2px solid #FFFFFF !important;
	color: #6A9634 !important;
	margin-top: 15px;
	margin-bottom: 15px;
}

.home-page-section2 .btn-primary:hover, .home-page-section2 .btn-primary:focus, .home-page-section2 .btn-primary:active {
	background-color: #F7941D;
	background-image: none;
	border: 2px solid #F7941D !important;
	color: #FFFFFF !important;
	text-decoration: none !important;
}

-->
</style>
<div class="animatedParent animateOnce">
<div class="home-page-section2 animated fadeIn slow" data-appear-top-offset="-100">
<div class="container-fluid">
<div class="row">
<div class="parallax" data-height="540" data-image="uploads/treetops_ely_play_area.jpg" data-width="2000">
<div class="col-xs-12 col-sm-6 col-md-6 col-sm-offset-6 col-md-offset-6 section-right no-padding animated fadeIn">
<div class="section-inner col-lg-8">
<img alt="" src="images/chameleon.gif"/>
<span class="section-title">What we're about</span>
<p class="section-text">A family run independent soft play centre close to Ely with the focus of a great time for the whole family. Open 362 days of the year whatever the weather offering more than just soft play for the local community.</p>
<a class="btn btn-primary" href="about">Read More</a>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
<!--

.home_page_information_container {
	margin-top: 60px;
	margin-bottom: 60px;
}

.home_page_information {
	position: relative;
	z-index: 1;
}

.home_page_information img {
	height: auto;
	outline: 4px solid #FFFFFF;
	outline-offset: -25px;
	width: 100%;
}

a.home_page_information_content_wrapper {
	bottom: 0px;
	display: block;
	left: 15px;
	position: absolute;
	right: 15px;
	text-decoration: none;
	top: 0px;
	z-index: 2;
}

.home_page_information_content {
	left: 0px;
	position: absolute;
	right: 0px;
	text-align: center;
	top: 50%;
	-webkit-transform: translateY(-50%);
	-moz-transform: translateY(-50%);
	-ms-transform: translateY(-50%);
	transform: translateY(-50%);
}

.home_page_information_content h2 {
	color: #FD393B;
	font-family: "Amatic SC", cursive;
	font-size: 80px;
	line-height: 80px;
	margin-top: 0px;
	margin-bottom: 0px;
	
	-webkit-transition: color 0.35s;
	-moz-transition: color 0.35s;
	transition: color 0.35s;
}

a.home_page_information_content_wrapper:hover .home_page_information_content h2 {
	color: #FFFFFF;
}

@media only screen and (min-width: 992px) {
	
	
	
}

@media only screen and (min-width: 768px) {
	
	
	
}

@media only screen and (max-width: 767px) {
	
	.home_page_information_container {
		margin-top: 30px;
		margin-bottom: 15px;
	}
	
	.home_page_information {
		margin-bottom: 15px;
	}
	
}

@media only screen and (max-width: 479px) {
	
	.home_page_information_content h2 {
		font-size: 70px;
		line-height: 70px;
	}
	
}

-->
</style>
<div class="home_page_information_container">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 home_page_information">
<img alt="" src="/uploads/74084104811031f766b87199b1961e9d_section3a.jpg"/>
<a class="home_page_information_content_wrapper" href="play">
<div class="home_page_information_content">
<h2>Play</h2>
</div>
</a>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 home_page_information">
<img alt="" src="/uploads/cheese-toastie.jpg"/>
<a class="home_page_information_content_wrapper" href="food">
<div class="home_page_information_content">
<h2>Food</h2>
</div>
</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<div class="animatedParent animateOnce">
<div class="home-page-section4 animated fadeIn slow">
<div class="container-fluid">
<div class="row home-page-sections4">
<div class="parallax" data-height="540" data-image="uploads/treetops_ely_play_area2.jpg" data-width="2000">
<div class="col-xs-12 col-sm-6 col-md-6 section-left no-padding animated fadeIn" data-appear-top-offset="-100">
<div class="section-inner col-lg-offset-3">
<img alt="" height="170" src="images/bird.gif"/>
<span class="section-title">Fantastic Facilities</span>
<p class="section-text">With a 3 storey main play frame, separate under 4's and baby sections ' there really is something for everyone. 5* Hygiene rated kitchen serving freshly prepared food every day. As well as a fantastic choice of teas, coffees and cakes including gluten and dairy free options.</p>
</div>
</div>
<div class="hidden-xs col-sm-6 col-md-6"></div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div><style type="text/css">
<!--

.home_page_contact_container {
	margin-top: 60px;
	margin-bottom: 30px;
}

.home_page_contact {
}

.home_page_contact h2 {
	color: #F7941D;
	font-family: "Merienda One", cursive;
	font-size: 40px;
	margin-top: 0px;
	margin-bottom: 20px;
}

.home_page_contact h3 {
	color: #000000;
	font-size: 60px;
}

.home_page_contact h3 a {
	color: #000000;
	text-decoration: none !important;
}
	
.home_page_contact .button-findus {
  margin-top:30px;
  margin-bottom:30px;
  border-radius:30px;
  background:#FD393B;
  color:#fff;
  padding:10px;
  font-size:28px;
  font-family: 'Merienda One', cursive;
  width:200px;
  display:block;
  text-align:center;
  -webkit-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
	padding-top:15px;
	padding-bottom:15px;
	transition:0.5s;
} 

.home_page_contact .button-findus:hover {
  background:#F7941D;
  color:#fff;
  text-decoration:none;
}

.home_page_contact .button-facebook {
  margin-top:30px;
  margin-bottom:30px;
  border-radius:30px;
  background:#3B5999;
  color:#fff;
  padding:10px;
  padding-top:5px;
  padding-bottom:15px;
  font-size:28px;
  font-family: 'arial', sans-serif;
  width:200px;
  display:block;
  text-align:center;
  -webkit-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.75);
	padding-top:15px;
	padding-bottom:15px;
	transition:0.5s;
} 

.home_page_contact .button-facebook:hover {
  background:#F7941D;
  color:#fff;
  text-decoration:none;
}

.iframe-container{
  position: relative;
  width: 100%;
  padding-bottom: 56.25%; /* Ratio 16:9 ( 100%/16*9 = 56.25% ) */
  border:1px solid #6A9634;
}

.iframe-container > *{
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: 0;
  padding: 0;
  height: 100%;
  width: 100%;
}

-->
</style>
<div class="clearfix"></div>
<div class="home_page_contact_container">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 home_page_contact">
<h2>Get in touch</h2>
<p>Lancaster Way Business Park, Ely, Cambs, CB6 3NW</p>
<h3>Tel 01353 665111 </h3>
<p><a href="/cdn-cgi/l/email-protection#7f161119103f0b0d1a1a0b100f0c1a1306511c10510a14"><span class="__cf_email__" data-cfemail="1871767e77586c6a7d7d6c77686b7d7461367b77366d73">[email protected]</span></a></p>
<h3>Opening Hours</h3>
<p>Monday - 10am - 12.30pm and 1pm to 3.30pm<br/>
Tuesday - 10am - 12.30pm and 1pm to 3.30pm<br/>
Wednesday - 10am - 12.30pm and 1pm to 3.30pm<br/>
Thursday - 10am - 12.30pm and 1pm to 3.30pm<br/>
Friday - 10am until 12.30pm, 1pm to 3.30pm and 3.45 to 5.45pm<br/>
Saturday - 10am to 12.30pm and 1pm to 3.30pm<br/>
Sunday - 10am to 12.30pm and 1pm to 3.30pm</p>
<p>
<a class="button-findus" href="find-us">Find Us</a>
</p><div class="book-now-btn"><a class="btn btn-md btn-success" href="/bookings/booking/"><i aria-hidden="true" class="fa fa-calendar"></i> BOOK ONLINE</a></div>
<p>To find out all our day to day activity</p>
<p class="email">Follow us on Facebook</p>
<p><a class="button-facebook" href="https://www.facebook.com/TreetopsEly/" target="_blank"><img alt="" src="images/facebook-btn.png" width="140"/></a></p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 home_page_contact">
<div class="fb-page" data-adapt-container-width="true" data-height="840" data-hide-cover="false" data-href="https://www.facebook.com/treetopsely/" data-show-facepile="true" data-small-header="false" data-tabs="timeline" data-width="500"><blockquote cite="https://www.facebook.com/treetopsely/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/treetopsely/">Treetops Soft Play</a></blockquote></div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<div class="animatedParent animateOnce" data-appear-top-offset="-100">
<div class="animated fadeIn" id="footer_mascots"></div>
</div>
<div id="footer_wrapper">
<div class="container">
<div class="row">
<div id="footer">
<div class="col-xs-12 col-sm-3 col-md-3 footer_section"><p class="footer_title">Information</p><a href="/">Home</a><br/><a href="/treetops-play-pay-play-tcs">Treetops Play Pay &amp; Play T&amp;Cs</a><br/><a href="/food">Food</a><br/><a href="https://www.facebook.com/pg/TreetopsEly/reviews/?ref=page_internal" target="_blank">Testimonials</a><br/><a href="/cookies-policy">Cookie Policy</a><br/></div><div class="col-xs-12 col-sm-3 col-md-3 footer_section"><p class="footer_title">Customer Services</p><a href="/play">Play</a><br/><a href="/events">Events</a><br/><a href="/parties">Parties</a><br/><a href="/bookings/booking/">Book Now</a></div><div class="col-xs-12 col-sm-3 col-md-3 footer_section"><p class="footer_title">Company Details</p><a href="/about">About</a><br/><a href="/find-us">Find Us</a><br/><a href="/contact">Contact</a><br/><a class="social_icon" href="https://www.facebook.com/treetopsely/" target="_blank"><i aria-hidden="true" class="fa fa-facebook-square"></i></a></div>
<div class="col-xs-12 col-sm-3 col-md-3" id="footer_logo">
<a href="/"><img alt="Treetops Soft Play" src="/images/logo.png"/></a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<div id="final_wrapper">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6" id="copyright">
Copyright © 2021 Bunten Pickup Limited (Treetops Soft Play, Ely)<br/>Registered in United Kingdom No. 05947649<br/>Registered address: 266 Kings Avenue, Ely, Cambs, CB7 4PJ<br/>VAT Reg No. 896385066 </div>
<div class="col-xs-12 col-sm-6 col-md-6" id="upshot">
<a href="https://www.upshotmedia.co.uk">Website design</a> by <a href="https://www.upshotmedia.co.uk">Upshot Media Ltd</a>
</div>
</div>
</div>
</div>
<a class="cd-top hidden-xs" href="#0">Top</a>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js"></script>
<script>
	(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)
</script>
<script src="/js/minify_js.php"></script>
<script defer="" src="/js/minify_js.php?mode=DEFER"></script>
<script>
  $(document).ready(function () {
    $('.parallax').parallax({
      scroll_factor: 0.5,
      image_attr: 'image'
	  });
  });
</script>
</body>
</html>