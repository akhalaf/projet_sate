<html>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED.  -->
<!-- DO NOT COPY THIS CODE WITHOUT PERMISSION.     -->
<!-- DO NOT MAKE DERIVATIVE WORKS.                 -->
<!-- If you are thinking of copying this code      -->
<!-- email contact@airnav.com for authorization.   -->
<head>
<title>AirNav: MO8 - North Central Missouri Regional Airport</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Complete aeronautical information about North Central Missouri Regional Airport (Brookfield, MO, USA), including location, runways, taxiways, navaids, radio frequencies, FBO information, fuel prices, sunrise and sunset times, aerial photo, airport diagram." name="description"/>
<meta content="Brookfield" name="keywords"/>
<script type="text/javascript"><!--//--><![CDATA[//><!--

sfHover = function() {
        var sfEls = document.getElementById("navres").getElementsByTagName("LI");
        for (var i=0; i<sfEls.length; i++) {
                sfEls[i].onmouseover=function() {
                        this.className+=" sfhover";
                }
                sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
                }
        }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

//--><!]]></script>
<link href="//img.airnav.com/css/css2.css?v=0" rel="StyleSheet" type="text/css"/>
<link href="//img.airnav.com/css/airport.css?v=0" rel="StyleSheet" type="text/css"/>
<base target="_top"/>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" width="100%">
<tr valign="bottom">
<td align="left" height="60" valign="top">
<a href="/"><img alt="AirNav" border="0" height="60" src="//img.airnav.com/logo/header.gif?v=HTWA8Q" width="280"/></a>
</td>
<td> </td>
<td align="right" height="60" width="468">
<a href="/ad/click/SYWlyd2lzY29uc2luMj.xOS0z" onclick="_gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-3', 'Click']);" target="_new"><img alt="Air Wisconsin Up To $57,000 Bonus" border="0" height="60" src="//img.airnav.com/banners/airwisconsin2019-3.gif?v=QHE2CR" width="468"/></a>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td width="20"> </td><td align="left">
<table border="0" cellpadding="0" cellspacing="0"><tr><td>
<div id="mainmenu">
<ul>
<li><a href="/airports/" id="airports">Airports</a>
</li>
<li><a href="/navaids/" id="navaids">Navaids</a>
</li>
<li><a href="/airspace/fix/" id="fix">Airspace Fixes</a>
</li>
<li><a href="/fuel/" id="fuel">Aviation Fuel</a>
</li>
<li><a href="/hotels/" id="hotels">Hotels</a>
</li>
<li><a href="/airboss/" id="airboss">AIRBOSS</a>
</li>
<li><a href="/iphoneapp/" id="iphoneapp">iPhone App</a>
</li>
<li><a href="https://www.airnav.com/members/login?return=//my.airnav.com/my" id="my">My AirNav</a>
</li>
</ul>
</div>
</td></tr></table>
</td></tr>
<tr><td bgcolor="#9999FF" width="20"> </td><td align="right" bgcolor="#9999FF" width="100%"><font size="-1">924 users online
 <a href="/members/login"><img alt="" border="0" height="10" src="//img.airnav.com/btn/mini/login.gif?v=JZBHPB" width="45"/></a></font> </td></tr>
</table>
<hr/>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr bgcolor="#CCCCFF" valign="middle">
<td align="center" nowrap=""><font face="Verdana, Arial, Helvetica, sans-serif"><font size="5"><b>MO8</b></font></font></td>
<td bgcolor="#FFFFFF" width="99%"> <font face="Verdana, Arial, Helvetica, sans-serif" size="+1"><b>North Central Missouri Regional Airport</b><br/>Brookfield, Missouri, USA</font> </td>
<td bgcolor="#FFFFFF" nowrap=""><img alt="" border="0" height="36" src="//img.airnav.com/flag/s/us?v=HTWA6S" width="60"/></td>
</tr>
</table>
<hr/>
<table border="1" cellpadding="7" cellspacing="0" width="100%">
<tr><td bgcolor="#ccccff" nowrap="" width="20%">
<h3>GOING TO BROOKFIELD?</h3>
</td><td>
<ul id="navres">
<li> <table><tr><td align="center">
<a href="/hotels/selecthotel?airport=MO8">
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/94e48416-dcb6-4ca5-9bab-6e89cab71212-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/955e4d79-2cb1-482d-9f5e-b5146b69d191-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/5e2e1fc9-cee5-4f17-b43f-b848f546a45f-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/b88aadae-2d21-4a88-af0c-65bdf0d1ec08-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/6692d352-f4d7-409d-b689-12ae1708a57d-120x60.gif?v=PYUHZZ" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/e5700a21-b5a2-4f23-a4f3-0fb69c753ec4-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/019ac327-f992-4837-9631-b7ce96404b5f-120x60.gif?v=PBE2PF" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/f1572df8-c7e1-48b4-967d-f9d154e4558e-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/374c4614-a416-4047-af11-28151c6c1d86-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/b24e4089-3767-459e-857a-1e86b1946475-120x60.gif" width="60"/>
</a><a></a></td></tr><tr><td align="center">
<font size="-1"><a href="/hotels/selecthotel?airport=MO8">Reserve a <b>Hotel Room</b></a></font>
</td></tr></table>
<script>
var slideIndex = 0;
carousel("hotelbrands");

function carousel() {
    var c ='hotelbrands';
    var i;
    var x = document.getElementsByClassName(c);
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1} 
    x[slideIndex-1].style.display = "block"; 
    setTimeout(carousel, 500); // Change image every 2 seconds
}
</script>
</li>
</ul>
</td></tr></table>
<br/>
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td width="99%">
<h3>FAA INFORMATION EFFECTIVE 31 DECEMBER 2020</h3>
<a name="loc"></a><h3>Location</h3><table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" nowrap="">FAA Identifier: </td><td>MO8</td></tr>
<tr><td align="right" valign="top">Lat/Long: </td><td valign="top">39-46-16.9000N 093-00-46.0000W<br/>39-46.281667N 093-00.766667W<br/>39.7713611,-93.0127778<br/>(estimated)</td></tr>
<tr><td align="right" valign="top">Elevation: </td><td valign="top">844.5 ft. / 257.4 m (surveyed)</td></tr>
<tr><td align="right" valign="top">Variation: </td><td valign="top">00W (2020)</td></tr>
<tr><td align="right" nowrap="" valign="top">From city: </td><td valign="top">2 miles E of BROOKFIELD, MO</td></tr>
<tr><td align="right" nowrap="" valign="top">Time zone: </td><td valign="top">UTC -6 (UTC -5 during Daylight Saving Time)</td></tr>
<tr><td align="right" nowrap="">Zip code: </td><td>64628</td></tr>
</table>
<a name="ops"></a><h3>Airport Operations</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" nowrap="" valign="top">Airport use: </td><td valign="top">Open to the public</td></tr>
<tr><td align="right" nowrap="">Activation date: </td><td>06/2002</td></tr>
<tr><td align="right" nowrap="">Control tower: </td><td>no</td></tr>
<tr><td align="right" valign="top">ARTCC: </td><td valign="top">KANSAS CITY CENTER</td></tr>
<tr><td align="right" valign="top">FSS: </td><td valign="top">COLUMBIA FLIGHT SERVICE STATION</td></tr>
<tr><td align="right" nowrap="" valign="top">NOTAMs facility: </td><td valign="top">COU (NOTAM-D service available)</td></tr>
<tr><td align="right">Attendance: </td><td>IREG</td></tr>
<tr><td align="right" nowrap="">Wind indicator: </td><td>lighted</td></tr>
<tr><td align="right" nowrap="">Segmented circle: </td><td>yes</td></tr>
<tr><td align="right" valign="top">Lights: </td><td valign="top">ACTVT MIRL RY 18/36; PAPI RYS 18 &amp; 36; REIL RYS 18 &amp; 36 - CTAF.</td></tr>
<tr><td align="right" valign="top">Beacon: </td><td valign="top">white-green (lighted land airport)<br/>Operates sunset to sunrise.</td></tr>
</table>
<a name="com"></a><h3>Airport Communications</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right">CTAF: </td><td>122.9</td></tr>
<tr><td align="right" nowrap="" valign="top">WX AWOS-AV: </td><td valign="top">118.425 (660-258-2151)</td></tr>
</table>
<ul>
<li class="rmk">APCH/DEP SVC PRVDD BY KANSAS CITY ARTCC ON FREQS 125.25/235.975 (CHILLICOTHE RCAG).
</li></ul>
<a name="nav"></a><h3>Nearby radio navigation aids</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left">VOR radial/distance</th><th>  </th><th align="right">VOR name</th><th>  </th><th>Freq</th><th>  </th><th>Var</th></tr>
<tr><td><a href="/cgi-bin/navaid-info?id=MCM&amp;type=VOR.DME&amp;name=MACON">MCM</a>r280/25.5</td><td></td><td align="right">MACON VOR/DME</td><td></td><td>112.90</td><td></td><td>06E</td></tr>
<tr><td><a href="/cgi-bin/navaid-info?id=IRK&amp;type=VORTAC&amp;name=KIRKSVILLE">IRK</a>r216/29.2</td><td></td><td align="right">KIRKSVILLE VORTAC</td><td></td><td>114.60</td><td></td><td>06E</td></tr>
</table>
<a name="svcs"></a><h3>Airport Services</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" nowrap="" valign="top">Fuel available: </td><td valign="top">100LL JET-A<br/>SELF-SERVICE FUEL AVAILABLE 24 HRS.</td></tr>
<tr><td align="right">Parking: </td><td>tiedowns</td></tr>
</table>
<a name="rwys"></a><h3>Runway Information</h3>
<h4>Runway 18/36</h4>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" valign="top">Dimensions: </td><td colspan="3" valign="top">5002 x 75 ft. / 1525 x 23 m</td></tr>
<tr><td align="right" valign="top">Surface: </td><td colspan="3" valign="top">concrete, in excellent condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Weight bearing capacity: </td><td colspan="3" valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr><td colspan="2">PCN 10 /R/C/W/U</td></tr><tr><td nowrap="" valign="top">Single wheel: </td><td valign="top">30.0</td></tr></table></td></tr>
<tr><td align="right" nowrap="" valign="top">Runway edge lights: </td><td colspan="3" valign="top">medium intensity</td></tr>
<tr><td valign="top"></td><td valign="top"><b>RUNWAY 18</b></td><td>  </td><td valign="top"><b>RUNWAY 36</b></td></tr>
<tr><td align="right">Latitude: </td><td>39-46.693852N</td><td></td><td>39-45.869985N</td></tr>
<tr><td align="right">Longitude: </td><td>093-00.756740W</td><td></td><td>093-00.775288W</td></tr>
<tr><td align="right" valign="top">Elevation: </td><td valign="top">844.3 ft.</td><td></td><td valign="top">825.6 ft.</td></tr>
<tr><td align="right" nowrap="">Traffic pattern: </td><td>left</td><td></td><td>left</td></tr>
<tr><td align="right" nowrap="" valign="top">Runway heading: </td><td valign="top">181 magnetic, 181 true</td><td></td><td valign="top">001 magnetic, 001 true</td></tr>
<tr><td align="right" valign="top">Markings: </td><td valign="top">nonprecision, in good condition</td><td></td><td valign="top">nonprecision, in good condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Visual slope indicator: </td><td valign="top">4-light PAPI on left (3.00 degrees glide path)</td><td></td><td valign="top">4-light PAPI on left (3.00 degrees glide path)</td></tr>
<tr><td align="right" nowrap="">Runway end identifier lights: </td><td>yes</td><td></td><td>yes</td></tr>
<tr><td align="right" nowrap="" valign="top">Touchdown point: </td><td valign="top">yes, no lights</td><td></td><td valign="top">yes, no lights</td></tr>
</table>
<h3>Airport Ownership and Management from official FAA records</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right">Ownership: </td><td>Publicly-owned</td></tr>
<tr><td align="right" valign="top">Owner: </td><td valign="top">CITIES OF BROOKFIELD &amp; MARCELINE<br/>116 WEST BROOKS<br/>BROOKFIELD, MO 64628<br/>Phone 660-258-3377</td></tr>
<tr><td align="right" valign="top">Manager: </td><td valign="top">TED &amp; RANDY STOCKWELL<br/>28540 JET WAY DRIVE, PO BOX 283<br/>BROOKFIELD, MO 64628<br/>Phone 660-258-7317<br/>ALTERNATE PHONE NBRS 660-258-5700, 660-734-2505; 660-258-3969; 660-375-3616.</td></tr>
</table>
<a name="stats"></a><h3>Airport Operational Statistics</h3>
<table border="0" cellpadding="0" cellspacing="0"><tr>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr valign="bottom"><td align="right" valign="bottom">Aircraft based on the field:</td><td> </td><td align="right" valign="bottom">12</td></tr><tr valign="bottom"><td align="right" valign="bottom">Single engine airplanes:</td><td> </td><td align="right" valign="bottom">11</td></tr><tr valign="bottom"><td align="right" valign="bottom">Multi engine airplanes:</td><td> </td><td align="right" valign="bottom">1</td></tr></table></td><td>  </td><td bgcolor="#999999" width="1"><img height="1" src="//img.airnav.com/1dot.gif" width="1"/></td><td>  </td><td valign="top"><table border="0" cellpadding="0" cellspacing="0">
<tr valign="bottom"><td align="center" colspan="2" valign="bottom">Aircraft operations: avg 57/week<font size="-1"> *</font></td></tr><tr><td align="right" valign="top">74% </td><td valign="top">transient general aviation</td></tr><tr><td align="right" valign="top">24% </td><td valign="top">local general aviation</td></tr><tr><td align="right" valign="top">1% </td><td valign="top">military</td></tr><tr><td colspan="2"><font size="-1">* for 12-month period ending 31 December 2019</font></td></tr></table></td></tr></table>
<a name="notes"></a><h3>Additional Remarks</h3>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">FOR SERVICE AFTER HRS, CALL PHONE NUMBERS POSTED ON AIRPORT MGMT OFFICE DOOR.</td></tr>
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">NUMEROUS WATERFOWL &amp; DEER ON &amp; INVOF ARPT.</td></tr>
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">FOR CD CTC KANSAS CITY ARTCC AT 913-254-8508.</td></tr>
</table>
<a name="ifr"></a><h3>Instrument Procedures</h3>
<font size="-1">NOTE: All procedures below are presented as PDF files. If you need a reader for these files, you should <a href="/depart?http://www.adobe.com/products/acrobat/readstep2.html" target="adobereader">download</a> the free Adobe Reader.<p><strong>NOT FOR NAVIGATION</strong>. Please procure official charts for flight.<br/>FAA instrument procedures published for use from 31 December 2020 at 0901Z to 28 January 2021 at 0900Z.</p></font>
<table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left" colspan="3"> <br/>IAPs - Instrument Approach Procedures</th></tr>
<tr>
<td>RNAV (GPS) RWY 18</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/10261R18.PDF" target="ifrproc">download</a> <font size="-1">(244KB)</font></td></tr><tr>
<td>RNAV (GPS) RWY 36</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/10261R36.PDF" target="ifrproc">download</a> <font size="-1">(247KB)</font></td></tr>
<tr>
<td>NOTE: Special Take-Off Minimums/Departure Procedures apply</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/NC3TO.PDF" target="ifrproc">download</a> <font size="-1">(239KB)</font></td></tr>
</table>
<p>Other nearby airports with instrument procedures:</p>
<a href="/airport/KCHT">KCHT</a> - Chillicothe Municipal Airport (22 nm W)<br/>
<a href="/airport/K89">K89</a> - Macon-Fower Memorial Airport (26 nm E)<br/>
<a href="/airport/KIRK">KIRK</a> - Kirksville Regional Airport (29 nm NE)<br/>
<a href="/airport/KTRX">KTRX</a> - Trenton Municipal Airport (33 nm NW)<br/>
<a href="/airport/KMBY">KMBY</a> - Omar N Bradley Airport (33 nm SE)<br/>
</td>
<td> </td>
<td width="240">
<table border="0" cellpadding="0" cellspacing="0" width="240"><tr><td align="center">
<font size="-2">
<a href="#loc">Loc</a> | <a href="#ops">Ops</a> | <a href="#rwys">Rwys</a> | <a href="#ifr">IFR</a> | <a href="#biz">FBO</a> | <a href="#links">Links</a><br/>
<a href="#com">Com</a> | <a href="#nav">Nav</a> | <a href="#svcs">Svcs</a> | <a href="#stats">Stats</a> | <a href="#notes">Notes</a>
</font>
</td></tr>
</table>
<img alt="Area around MO8 (North Central Missouri Regional Airport)" border="0" height="200" src="//img.airnav.com/sm/29080?v=JPOX48" width="240"/><br/> <br/>
<font size="-1">
Road maps at:
<a href="http://www.mapquest.com/maps/map.adp?latlongtype=decimal&amp;zoom=6&amp;latitude=39.771361&amp;longitude=-93.012778&amp;name=MO8" target="airport_maps">MapQuest</a>
<a href="http://www.bing.com/maps/?sp=aN.39.771361_-93.012778_MO8&amp;lvl=14" target="airport_maps">Bing</a>
<a href="http://maps.google.com/maps?ll=39.771361%2C-93.012778&amp;spn=0.0193,0.0193" target="airport_maps">Google</a>
</font>
<br/> <br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Aerial photo</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<font size="-2">
<b>WARNING:</b> Photo may not be current or correct<br/>
<img alt="Aerial photo of MO8 (North Central Missouri Regional Airport)" border="0" height="180" src="//img.airnav.com/ap/29080.jpg?v=N5OBUV" width="240"/>
</font><center>Photo courtesy of AirNav, LLC</center>
<center>Photo taken 02-May-2014</center>
<center>looking west.</center>
<hr/>
Do you have a better or more recent aerial photo of North Central Missouri Regional Airport that you would like to share?  If so, please <b><a href="/airports/submitphoto.html?id=MO8">send us your photo</a></b>.<br/>
<br/> <br/>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Sectional chart</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<a href="http://www.airnav.com/depart?http://www.vfrmap.com/?type=vfrc&amp;lat=39.771&amp;lon=-93.013&amp;zoom=10&amp;api_key=763xxE1MJHyhr48DlAP2qQ" target="vfrmap"><img src="http://vfrmap.com/api?req=map&amp;type=sectc&amp;lat=39.771361&amp;lon=-93.012778&amp;zoom=10&amp;width=240&amp;height=240&amp;api_key=763xxE1MJHyhr48DlAP2qQ"/></a>
<br/> <br/>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Airport distance calculator</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<form action="/airport/MO8" method="POST">
<font size="-1">Flying to North Central Missouri Regional Airport?
Find the distance to fly.</font>
<p align="center">From <input maxlength="4" name="distance_from" size="5"/>
to MO8<br/>
<input alt="Calculate Distance" border="0" id="calculatedistance" name="calculatedistance" src="//img.airnav.com/btn/calculate-distance.gif?v=HTW9TQ" type="image" value=""/></p>
</form>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Sunrise and sunset</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><i>Times for 12-Jan-2021</i></font></div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><th></th><th> </th><th><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Local<br/>(UTC-6)</font></th><th> </th><th><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Zulu<br/>(UTC)</font></th></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Morning civil twilight</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">07:04</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13:04</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Sunrise</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">07:34</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13:34</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Sunset</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">17:07</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">23:07</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Evening civil twilight</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">17:37</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">23:37</font></td></tr>
</table>
</td></tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Current date and time</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top"><th align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Zulu (UTC)  </font></th><td align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13-Jan-2021 03:49:36</font></td></tr>
<tr valign="top"><th align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Local (UTC-6)  </font></th><td align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">12-Jan-2021 21:49:36</font></td></tr>
</table>
</td></tr>
</table>

    <br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">METAR</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellspacing="0">
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KCDJ </b><br/>26nm W </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130252Z AUTO 18004KT 02/M02 A3006 RMK AO1 SLP191 T00221022 58007
</font></td></tr>
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b><a href="/airport/KIRK">KIRK</a> </b><br/>29nm NE </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130255Z AUTO 22007KT 10SM CLR 04/M01 A3005 RMK AO2 SLP186 T00441011 56009
</font></td></tr>
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b><a href="/airport/KMBY">KMBY</a> </b><br/>33nm SE </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130335Z AUTO 21009KT 10SM CLR 05/M01 A3008 RMK AO2
</font></td></tr>
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b><a href="/airport/KMHL">KMHL</a> </b><br/>41nm S </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130335Z AUTO 21007KT 10SM CLR 03/00 A3009 RMK AO2
</font></td></tr>
</table>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">TAF</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellspacing="0">
</table>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">NOTAMs</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td align="left" width="20"><a href="https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&amp;reportType=RAW&amp;formatType=DOMESTIC&amp;retrieveLocId=MO8&amp;actionType=notamRetrievalByICAOs" target="NOTAMS"><img alt="" border="0" height="16" src="//img.airnav.com/wing.gif?v=HTWTJ4" width="16"/></a></td>
<td align="left"><a class="wl" href="https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&amp;reportType=RAW&amp;formatType=DOMESTIC&amp;retrieveLocId=MO8&amp;actionType=notamRetrievalByICAOs" target="NOTAMS">Click for the latest <b>NOTAMs</b></a></td>
</tr>
</table>
<font size="-1">NOTAMs are issued by the DoD/FAA and will open in a separate window not controlled by AirNav.</font>
 <br/>
</td></tr>
</table>
<br/> 
  </td>
</tr>
</table>
 <br/>
<a name="biz"></a>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr bgcolor="#ffffff" valign="middle"><th align="left" bgcolor="#9999ff" colspan="12" width="100%"><h3>FBO, Fuel Providers, and Aircraft Ground Support</h3></th></tr>
<tr><td rowspan="4">  </td><th bgcolor="#ccccff" width="240"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="240"/><br/> Business Name </th><th> </th><th bgcolor="#ccccff"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/><br/> Contact </th><th> </th>
<th bgcolor="#ccccff" nowrap="" width="90%"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/><br/> Services / Description </th><th> </th><th bgcolor="#ccccff" width="10%"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/><br/>Fuel Prices</th><th> </th><th bgcolor="#ccccff" colspan="2" width="1"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/><br/>Comments</th><td rowspan="4">  </td></tr>
<tr valign="middle">
<td width="240"><a href="/airport/MO8/A">North Central Missouri Regional Airport</a></td>
<td></td>
<td align="left" nowrap=""><font size="-1">660-258-7317<br/>660-734-2505<br/>[<a href="/airport/MO8/A/link" onmouseover="window.status='http://ncmrairport.com/'; return true" target="ext_apt_MO8">web site</a>]<br/>[<a href="mailto:ncmra@shighway.com?subject=Message from AirNav.com user to North Central Missouri Regional Airport (MO8)" onclick="_gaq.push(['_trackEvent', 'EmailListing', 'MO8-A', 'Click']);">email</a>]</font></td>
<td></td>
<td colspan="1"><font size="-1">Airport management, Aviation fuel, Aircraft parking (ramp or tiedown), Hangar leasing / sales, Passenger terminal and lounge, Rental cars, Courtesy transportation, ...<br/>
<br/><table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td align="left" width="20"><a href="/airport/MO8/A"><img alt="" border="0" height="16" src="//img.airnav.com/wing.gif?v=HTWTJ4" width="16"/></a></td>
<td align="left"><a class="wl" href="/airport/MO8/A"><font size="-1">More info about North Central Missouri Regional Airport</font></a></td>
</tr>
</table>
</font></td>
<td></td>
<td width="94">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" colspan="5">Phillips 66</td></tr><tr valign="top"><td></td><td align="right" colspan="2" nowrap="" valign="top" width="50%">100LL</td> <td align="right" colspan="2" nowrap="" valign="top" width="50%">Jet A</td></tr><tr valign="top"><td>SS </td><td align="right" nowrap=""></td><td align="right" nowrap="">$3.59 </td><td align="right" nowrap=""></td><td align="right" nowrap="">$3.05 </td></tr><tr><td align="center" colspan="5"><font size="-2"> Updated 12-Jan-2021</font></td></tr></table>
</td>
<td></td>
<td align="center" nowrap=""> 
<a href="/popup/ratings.html" onclick="javascript:window.open('/popup/ratings.html','airnavpopup','width=250,height=250,scrollbars=yes');return false;" target="_blank"><img alt="" border="0" height="12" src="//img.airnav.com/rating/aptpage/0.gif?v=L3KL3S" width="60"/></a><br/> <font size="-1">
 1 <a href="/airport/MO8/A#c">read</a> <a href="/airport/MO8/A/comment">write</a></font>
</td>
</tr>
<tr height="1"><td bgcolor="#cccccc" colspan="10" height="1"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/></td></tr>
<tr><td align="right" colspan="7"><table border="0" cellpadding="0" cellspacing="0"><tr><td>SS=<a href="/popup/service-explain.html?K=SS" onclick="javascript:window.open('/popup/service-explain.html?K=SS','airnavpopup','width=350,height=500,scrollbars=yes');return false;" target="_blank">Self service</a><br/></td></tr></table>
<a href="/airport/MO8/update-fuel"><img alt="Update Fuel Prices" border="0" height="20" src="//img.airnav.com/btn/update-prices.gif?v=HTW9TQ" width="125"/></a>
</td></tr>
<tr><td> </td></tr>
<tr bgcolor="#ffffff" valign="middle"><th align="left" bgcolor="#9999ff" colspan="12" width="100%"><h3>Would you like to see your business listed on this page?</h3></th></tr>
<tr><td> </td><td colspan="10"><font size="-1">If your business provides an interesting product or service to pilots, flight crews, aircraft, or users of the North Central Missouri Regional Airport, you should consider listing it here.  To start the listing process, click on the button below</font><br/> <br/>
<a href="/listings/subscribe/MO8"><img alt="Add a business" border="0" height="20" src="//img.airnav.com/btn/add-listing.gif?v=HTW9TQ" width="240"/></a>
</td></tr>
</table>
 <br/>
<a name="links"></a><h3>Other Pages about North Central Missouri Regional Airport</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td align="left" width="20"><a href="/airportlink?5YYHK" target="ext_apt29080"><img alt="" border="0" height="16" src="//img.airnav.com/wing.gif?v=HTWTJ4" width="16"/></a></td>
<td align="left"><a class="wl" href="/airportlink?5YYHK" target="ext_apt29080">ncmrairport.com</a></td>
</tr>
</table>

 <br/>
<a href="/airport/MO8/reportlinks"><img alt="Update a link" border="0" height="20" src="//img.airnav.com/btn/update-remove-add-a-link.gif?v=J8WXHS" width="212"/></a>
<br/> <br/>
<table bgcolor="#CCCCFF" border="0" cellpadding="0" cellspacing="5" width="100%">
<tr><td bgcolor="#333399" colspan="2"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/></td></tr>
<tr valign="bottom">
<td><font color="#404040" size="-2">
Copyright © AirNav, LLC. All rights reserved.
</font>
</td>
<td align="right"><font size="-2"><a href="/info/privacy.html">Privacy Policy</a> 
<a href="/info/contact.html">Contact</a></font></td>
</tr>
</table>
<script type="text/javascript">
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-467723-1']); 
 _gaq.push(['_trackPageview']);
 _gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-3', 'Impression']);
 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
</script>
</body>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED. -->
</html>
