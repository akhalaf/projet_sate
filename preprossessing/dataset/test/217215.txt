<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La webradio de l'improbable et de l'inouï : accueil - Bide et Musique</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Bide&amp;Musique" name="copyright"/>
<meta content="musique chanson webradio années 80 vintage jukebox variété chanson populaire divertissement technologie" name="keywords"/>
<meta content="music, radio, internet, streaming, shoutcast" name="classification"/>
<meta content="media webradio" name="type"/>
<meta content="La radio de l'improbable et de l'inouï" name="description"/>
<meta content="noodp" name="robots"/>
<meta content="width=device-width,initial-scale=0.8,user-scalable=1" name="viewport"/>
<link href="/style.css" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/news.rss" rel="alternate" title="(RSS) Les news de la radio" type="application/rss+xml"/>
<link href="/new_song.rss" rel="alternate" title="(RSS) Les dernières rentrées dans la base" type="application/rss+xml"/>
<link href="/recherche.html" rel="search" title="Recherche"/>
<!-- script type="text/javascript" src="/png.js" ></script -->
<script src="/bide.js?j=1" type="text/javascript"></script>
<script src="/prototype.js" type="text/javascript"></script>
<link href="/mediaplayer/mediaelementplayer.css" rel="stylesheet"/>
<script async="true" type="text/javascript">
    var elem = document.createElement('script');
    elem.src = 'https://quantcast.mgr.consensu.org/cmp.js';
    elem.async = true;
    elem.type = "text/javascript";
    var scpt = document.getElementsByTagName('script')[0];
    scpt.parentNode.insertBefore(elem, scpt);
    (function() {
    var gdprAppliesGlobally = false;
    function addFrame() {
        if (!window.frames['__cmpLocator']) {
        if (document.body) {
            var body = document.body,
                iframe = document.createElement('iframe');
            iframe.style = 'display:none';
            iframe.name = '__cmpLocator';
            body.appendChild(iframe);
        } else {
            setTimeout(addFrame, 5);
        }
        }
    }
    addFrame();
    function cmpMsgHandler(event) {
        var msgIsString = typeof event.data === "string";
        var json;
        if(msgIsString) {
        json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};
        } else {
        json = event.data;
        }
        if (json.__cmpCall) {
        var i = json.__cmpCall;
        window.__cmp(i.command, i.parameter, function(retValue, success) {
            var returnMsg = {"__cmpReturn": {
            "returnValue": retValue,
            "success": success,
            "callId": i.callId
            }};
            event.source.postMessage(msgIsString ?
            JSON.stringify(returnMsg) : returnMsg, '*');
        });
        }
    }
    window.__cmp = function (c) {
        var b = arguments;
        if (!b.length) {
        return __cmp.a;
        }
        else if (b[0] === 'ping') {
        b[2]({"gdprAppliesGlobally": gdprAppliesGlobally,
            "cmpLoaded": false}, true);
        } else if (c == '__cmp')
        return false;
        else {
        if (typeof __cmp.a === 'undefined') {
            __cmp.a = [];
        }
        __cmp.a.push([].slice.apply(b));
        }
    }
    window.__cmp.gdprAppliesGlobally = gdprAppliesGlobally;
    window.__cmp.msgHandler = cmpMsgHandler;
    if (window.addEventListener) {
        window.addEventListener('message', cmpMsgHandler, false);
    }
    else {
        window.attachEvent('onmessage', cmpMsgHandler);
    }
    })();
    window.__cmp('init', {
    		'Language': 'fr',
		'Initial Screen Body Text Option': 1,
		'Publisher Name': 'Moneytizer',
		'Default Value for Toggles': 'on',
		'UI Layout': 'banner',
		'No Option': false,
    });
</script>
<style>
        .qc-cmp-button,
        .qc-cmp-button.qc-cmp-secondary-button:hover {
            background-color: #000000 !important;
            border-color: #000000 !important;
        }
        .qc-cmp-button:hover,
        .qc-cmp-button.qc-cmp-secondary-button {
            background-color: transparent !important;
            border-color: #000000 !important;
        }
        .qc-cmp-alt-action,
        .qc-cmp-link {
            color: #000000 !important;
        }
        .qc-cmp-button,
        .qc-cmp-button.qc-cmp-secondary-button:hover {
            color: #ffffff !important;
        }
        .qc-cmp-button:hover,
        .qc-cmp-button.qc-cmp-secondary-button {
            color: #000000 !important;
        }
        .qc-cmp-small-toggle,
        .qc-cmp-toggle {
            background-color: #000000 !important;
            border-color: #000000 !important;
        }
        .qc-cmp-main-messaging,
		.qc-cmp-messaging,
		.qc-cmp-sub-title,
		.qc-cmp-privacy-settings-title,
		.qc-cmp-purpose-list,
		.qc-cmp-tab,
		.qc-cmp-title,
		.qc-cmp-vendor-list,
		.qc-cmp-vendor-list-title,
		.qc-cmp-enabled-cell,
		.qc-cmp-toggle-status,
		.qc-cmp-table,
		.qc-cmp-table-header {
    		color: #000000 !important;
		}
       	
        .qc-cmp-ui {
  			background-color: #ffffff !important;
		}

		.qc-cmp-table,
		.qc-cmp-table-row {
			  border: 1px solid !important;
			  border-color: #000000 !important;
		} 
    #qcCmpButtons a {
            text-decoration: none !important;

    }
    
    #qcCmpButtons button {
        margin-top: 65px;
    }
    
    
  @media screen and (min-width: 851px) {
    #qcCmpButtons a {
            position: absolute;
            bottom: 10%;
            left: 60px;
    }
  }
  .qc-cmp-qc-link-container{
    display:none;
  }
    </style>
</head>
<body>
<!--cookie-->
<div id="cookieOK">
		Les cookies nous permettent de personnaliser le contenu du site, les annonces publicitaires et d'analyser notre trafic. 
		Nous partageons également des informations avec nos partenaires, de publicité ou d'analyse
		mais aucune de vos données personnelles (e-mail, login).
		<input onclick="HideElement('cookieOK');saveCookie();" type="button" value="OK"/>
</div>
<script type="text/javascript">
		checkCookie();
	</script><!--Fin du cookie-->
<!--[DÉBUT DU CONTENEUR]-->
<table border="0" cellpadding="0" cellspacing="0" id="main_table" width="100%">
<tr>
<td id="gauche"> </td>
<td id="maquette">
<div class="bgbando1" id="bando">
<div id="nav-droite">
<div id="menu">
<a class="focus1" href="/index.html"><span class="txthidden">Accueil</span></a>
<a class="titre2" href="/nos-disques.html"><span class="txthidden">Nos disques</span></a>
<a class="titre3" href="/forum.html"><span class="txthidden">Forum</span></a>
<a class="titre4" href="/evenements.html"><span class="txthidden">Evénements</span></a>
<a class="titre5" href="/goodies.html"><span class="txthidden">Goodies</span></a>
<a class="titre6" href="/infos.html"><span class="txthidden">Infos</span></a>
<a class="titre7" href="/contact.html"><span class="txthidden">Contact</span></a>
</div>
<div class="txtclear"></div>
<div id="recherche">
<form action="/recherche.html#resultat" method="get">
<div class="recherche-titre"><img height="21" src="/images/titre-recherche.png" width="173"/></div>
<p style="line-height:10px;">dans la collection de B&amp;M</p>
<div class="rechercheint1">
<input name="kw" onclick="this.value='';" title="Recherche rapide dans la base des chansons" type="text" value="recherche"/>
</div>
<div class="rechercheint2"><input alt="OK" id="recherche-ok" src="/images/bt-ok-compte.jpg" title="Recherche rapide dans la base des chansons" type="image"/></div>
<input name="st" type="hidden" value="1"/>
</form>
<div class="txtclear"></div>
</div>
<!-- -->
<!-- Pub dans la partie droite -->
<div id="nav_ad2">
<div class="ads_333732345f363137325f3234393838">
<script type="text/javascript">
					var rdads=new String(Math.random()).substring (2, 11);
					document.write('<sc'+'ript type="text/javascript" src="//server1.affiz.net/tracking/ads_display.php?n=333732345f363137325f3234393838_3c3261eec4&rdads='+rdads+'"></sc'+'ript>');
				</script>
</div>
</div>
<div id="radiocote" style="border-top:none;">
<div class="radiocote-titre"><img src="/images/titre-radiocote.jpg"/></div>
</div>
<div id="rss4" style="border-bottom:none;">
<ul>
<li><a href="/grille.html">La grille des programmes</a></li>
<li><a href="/programme-webradio.html">Les titres à venir</a></li>
<li><a href="/requetes.html">Les requêtes</a></li>
</ul>
</div>
<div class="txtclear"></div>
<div class="txtclear"></div>
<div class="txtclear"></div>
<div id="discussion" style="border-bottom:none">
<div class="discussion-titre"><img alt="[Quoi de neuf ?]" height="22" src="/images/titre-discussion.png" width="173"/></div>
<p style="line-height:10px; padding:0 0 8px 0;">Pour pouvoir discuter ici, vous devez avoir <a href="/account.html">un compte</a> et être identifié.</p>
<ul id="wall-side">
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/9671." style="float:left; padding:0 3px 0 0;"/>
<a href="/account/9671.html">teddy33</a> à 18h09</span>
                        pendant <a href="/song/12788.html" title="Émission Ils ont osé ! - Saison 6 - Numéro 37">Saison 6 - Numéro 37</a>
</p>
<p>
                        Bethsabée, c'est un prénom féminin !                    </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/46327.jpg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/46327.html">Bethsabée Mouchot</a> à 17h44</span>
                        pendant <a href="/song/1579.html" title="Marie-Michèle Desrosiers - Petit Papa Noël">Petit Papa Noël</a>
</p>
<p>
                            Notre tranche d'évasion du jour, la résidence la Sourde à Deuil-la-Barre. Rien que le nom fait rêver (sans parler du nom du patelin). Les connaisseurs auront apprécié la présence sur le parking d'une Citroën Ami 8 et d'une Ford Capri, toutes deux d'une ravissante nuance moutarde (mal refermée pour la première) si tendance dans les joyeuses années 70.<br/>
<br/>
<a href="https://64.media.tumblr.com/813c22b0b440717d72e248b3e31d5b77/212797c8843aa66c-5f/s1280x1920/d46576ce99d46628e7af8e3885b4c2148b7b7cba.jpg"><img src="/favicon.ico"/></a> </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/46327.jpg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/46327.html">Bethsabée Mouchot</a> à 17h10</span>
                        pendant <a href="/song/5997.html" title="Benjamin - Le poisson et l'oiseau">Le poisson et l'oiseau</a>
</p>
<p>
                            C'est vrai que je suis un sacré comique de profiter de ce qu'un innocent internaute* me donne les clefs de son compte pour poster des bêtises en son nom ! Je suis presque au niveau de mon ami Maëri Andriamaëfassoa  qui une fois a poussé dans l'escalier un camarade qui s'y engageait lourdement chargé en partant d'un grand éclat de rire devant le cocasse de la blague. Sa victime ne s'est heureusement pas fait trop mal, mais j'en ai gardé l'idée que l'humour malgache est sans doute trop subtil pour qu'aux yeux d'un profane comme moi ça passe pour autre chose que pour une méchanceté aussi dangereuse que débile.<br/>
<br/>
<br/>
* Enfin « innocent » faut pas exagérer non plus, c'est Flam', quand même !<br/>
<small>je n'aurais pas fait ça à Thérèse, par exemple.</small> </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/125002.jpeg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/125002.html">Hasni</a> à 16h03</span>
                        pendant <a href="/song/2197.html" title="David Marouani - Envie de pleurer">Envie de pleurer</a>
</p>
<p>
                        Moi aussi                    </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/125002.jpeg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/125002.html">Hasni</a> à 15h36</span>
                        pendant <a href="/song/21937.html" title="Claude François - Je tiens un tigre par la queue">Je tiens un tigre par la queue</a>
</p>
<p>
                        (J'vais pas faire le fou)                    </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/125002.jpeg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/125002.html">Hasni</a> à 14h46</span>
                        pendant <a href="/song/7126.html" title="Woody Wood Pecker - Woody Wood le pivert">Woody Wood le pivert</a>
</p>
<p>
                        Ah ben pardon 😾                    </p>
<div class="txtclear"></div>
</li>
<li>
<p>
<span class="redtitre">
<img src="/images/avatars/84370.jpg" style="float:left; padding:0 3px 0 0;"/>
<a href="/account/84370.html">Flaming Youth</a> à 14h44</span>
                        pendant <a href="/song/7018.html" title="7 Zark 7 - La chanson de 7 Zark 7">La chanson de 7 Zark 7</a>
</p>
<p>
                        Si tu voulais dire des vannes à propos de «Super Kiki», tu vois le mal là où il n'a pas lieu d'être. J'applaudissais la farce que m'a faite Bethsabée. ^^                    </p>
<div class="txtclear"></div>
</li>
<div style="margin:8px 0 0 0;"><a href="#" onclick="new Ajax.Updater ('wall-side', '/mur-side.php?lim=15'); return false;"><img alt="[+]" height="20" src="/images/bt-puce-plus.png" style="float:left; margin:0 3px 0 0;" width="20"/></a>
<p style="padding:3px 0 0 0;"> Voir tous les commentaires</p>
<div class="txtclear"></div>
</div>
</ul>
<div class="txtclear"></div>
<script type="text/javascript">
              new Ajax.PeriodicalUpdater ('wall-side', '/mur-side.php', {asynchronous: true, frequency: 300});
              </script>
</div>
<div id="rss1" style="border-bottom:none;border-top:none;">
<ul>
<li><a href="/mur-des-messages.html">Le mur des messages</a></li>
</ul>
<br/>Nous suivre sur:
				<ul>
<li><a href="https://twitter.com/bideetmusique">twitter <img alt="Logo twitter" src="/images/logo_twitter.png" style="vertical-align:middle;"/></a></li>
<li><a href="https://www.facebook.com/BideEtMusique">facebook <img alt="Logo facebook" src="/images/logo_facebook.png" style="vertical-align:middle;"/></a></li>
</ul>
</div>
<div id="newletter">
<div class="newsletter-titre"><img height="21" src="/images/titre-newsletter.png" width="173"/></div>
<p>Vous pouvez vous abonner à la lettre d'information directement sur votre<a href="/account.html"> page compte</a>, ou en vous <a href="/inscription-newsletter.html">inscrivant ici</a>.</p>
</div>
<div id="rss2" style="border-bottom:none;">
<div class="rss-titre"><img height="21" src="/images/titre-rss.png" width="173"/></div>
<ul>
<li><a href="/news.rss"> RSS Les news </a></li>
<li><a href="/new_song.rss"> RSS Les nouvelles entrées</a></li>
<li><a href="/playlist.rss"> RSS La playlist</a></li>
</ul>
</div>
<div id="rss3" style="border-bottom:none;">
<div class="rss-titre"><img height="21" src="/images/titre-bidonautes.png" width="173"/></div>
<ul>
<li><a href="/recherche-bidonaute.html"> Rechercher un bidonaute </a></li>
<li><a href="/trombidoscope.html"> Le trombidoscope</a></li>
<li><a href="/gallery/"> La galerie photo</a></li>
</ul>
</div>
</div>
<div id="compte">
<form action="/ident.html" method="post">
<div class="compteint1">
<div class="compte-titre"><img height="26" src="/images/titre-moncompte.png" width="164"/></div>
<input id="login" name="LOGIN" onclick="this.value='';" type="text" value="login"/>
<input id="mot passe" name="PASSWORD" onclick="this.value='';" type="password" value="mot de passe"/>
</div>
<div class="compteint2"><input alt="OK" id="mon-compte-ok" name="OK" src="/images/bt-ok-compte.jpg" type="image" value=""/></div>
</form>
<div class="txtclear"></div>
<p style="line-height:10px;"><a href="/create_account.html">S'inscrire</a> | <a href="ident.html">Mot de passe perdu</a></p>
<p style="padding-top: 5px;">
<a href="/playlist-lq.m3u" title="écoutez en basse qualité"><img alt="Stream LQ" src="/images/ico_lq.png" style="padding:0px 5px 0px 5px; width: 24px;"/></a>
<a href="/playlist-hq.m3u" title="écoutez en haute qualité"><img alt="Stream HQ" src="/images/ico_hq.png" style="padding:0px 5px 0px 5px; width: 24px;"/></a>
</p>
</div>
<!-- Pub en en-tête -->
<div class="ads_333732345f363137325f3234393837">
<script type="text/javascript">
    				var rdads=new String(Math.random()).substring (2, 11);
        			document.write('<sc'+'ript type="text/javascript" src="//server1.affiz.net/tracking/ads_display.php?n=333732345f363137325f3234393837_9c77753eeb&rdads='+rdads+'"></sc'+'ript>');
        		</script>
</div>
<div id="logo">
<a href="/index.html">
<img alt="Bide&amp;Musique" src="/images/logo.png"/>
</a>
</div>
<div id="slogan">
<img alt="“" src="/images/gouttesgauche.png" style="float:left"/>
	        		Bide&amp;Musique, on se calme !    	    		<img alt="”" src="/images/gouttesdroite.png" style="float:right"/>
</div>
<div id="radio">
<script>
    	    		function Popup_BM_Flash(){open("http://www.bide-et-musique.com/player2/bideplayertest.html","Bide & Musique","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=580px,height=340px");}        		</script>
<a href="javascript:Popup_BM_Flash()">
<img src="/images/bt-radio.png"/>
</a>
</div>
</div>
<table border="0" cellpadding="0" cellspacing="0" style="background:url(/images/bg-middle.jpg) repeat-x top left;" width="100%">
<tr>
<td align="left" id="topint" valign="top"><img src="/images/bg-haut.png"/></td>
<td id="centre" rowspan="2" valign="top">
<div id="mobileButtons">
<center><table>
<tr>
<td><a class="bouton" onclick="mainMobileView(['menu']);"><span>Menu </span> </a></td>
<td><a class="bouton" onclick="hideAll();showConteneurContent();"><span>La page </span> </a></td>
<td><a class="bouton" onclick="mainMobileView(['rss1', 'rss2', 'rss3', 'rss4'])"><span>Le programme</span> </a></td>
</tr><tr>
<td><a class="bouton" onclick="mainMobileView(['recherche']);"><span>Recherche</span> </a></td>
<td>
<a class="bouton" onclick="mainMobileView(['discussion']);"><span>Le mur</span> </a>
</td>
<td><a class="bouton" onclick="mainMobileView(['newletter']);"><span>Newsletter</span> </a></td>
</tr><tr>
<td></td>
<td>
</td>
<td></td>
</tr>
</table></center>
</div>
<div id="conteneur">
<!--[DÉBUT DU FEEDBACK]-->
<!--[FIN DU FEEDBACK]-->
<!--[DEBUT DU BIDEBOX]-->
<!--[FIN DU BIDEBOX]-->
<div id="gd-encartblcplayer">
<div class="titre-bloc3">
<h2>En ce moment vous écoutez :</h2>
</div>
<div id="player-top">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td id="pochette" rowspan="2">
<a href="#" onclick="openImage('/images/pochettes/5374.jpg', 'Carlos+-+Big+bisou', 500, 500);return false;"><img alt="Carlos - Big bisou" src="/images/thumb150/5374.jpg"/></a>
</td>
<td id="player">
<p class="titre-song">
<a href="/song/5374.html" title="Fiche de « Big bisou »">Big bisou</a>
                  					(1977)
				            </p>
<p class="titre-song2">
<a href="/artist/55.html" title="Fiche de « Carlos »">Carlos</a>
</p>
<p class="piste">
<img alt="[93%]" height="18" src="/images/curseur-lecture.jpg" style="margin: 0 0 0 325px;" title="93%" width="18"/>
</p>
</td>
</tr>
<tr>
<td id="requete">
    Programmation générale
<div style="float: right;">
<a href="/playlist-lq.m3u" title="écoutez en basse qualité"><img alt="Stream LQ" src="/images/ico_lq.png" style="padding:0px 5px 0px 5px"/></a>
<a href="/playlist-hq.m3u" title="écoutez en haute qualité"><img alt="Stream HQ" src="/images/ico_hq.png" style="padding:0px 5px 0px 5px"/></a>
</div>
</td>
</tr>
</table>
</div>
<script type="text/javascript">
                			new Ajax.PeriodicalUpdater ('player-top', '/now-top.php', {asynchronous: true, frequency: 300});
							</script>
<div id="basgd-encartblc"></div>
</div>
<div class="txtclear"></div>
<!--[Evénement]-->
<!--[Fin Evénement]-->
<br/>
<div id="gd-encartblc">
<div class="titre-bloc"><h2>Morceau du moment</h2></div>
<table class="bmtable">
<tr>
<td class="vignette25" style="padding:5px;"> <a href="/show-image.html?I=/images/pochettes/3150.jpg&amp;T=G%E9n%E9rique+S%E9rie+-+K+2000+%28le+speech+du+d%E9but%29" onclick="openImage('/images/pochettes/3150.jpg', 'G%E9n%E9rique+S%E9rie+-+K+2000+%28le+speech+du+d%E9but%29', 500, 500); return false;"><img alt="Vignette de Générique Série - K 2000 (le speech du début)" src="/images/thumb30/3150.jpg" title="Cliquez pour agrandir"/></a>
</td>
<td class="baseitem"><a href="/artist/595.html" title="Générique Série">Générique Série</a></td>
<td class="baseitem"><a href="/song/3150.html" title="K 2000 (le speech du début)"><em>K 2000 (le speech du début)</em></a></td>
</tr>
<tr>
<td colspan="3"><p style="padding-left:8px;padding-right:8px;text-align:left"><a href="https://www.turbo.fr/actualite-automobile/kitt-de-k2000-est-vendre-et-david-hasselhoff-vous-la-livrera-lui-meme-172404">KITT de K2000 est à vendre et David Hasselhoff vous la livrera lui-même</a><br/>
<br/>
<img src="https://sm3.photorapide.com/membres/1862/photos/sq04kd.jpg"/></p>
<p style="text-align: right; font-style:italic ;"><a href="/morceaux-du-moment.html" title="L'historique des morceaux du moment">Précédents morceaux du moment</a><br/>
</p></td>
</tr>
</table>
<div id="basgd-encartblc"></div>
</div>
<div id="gd-encartblc">
<div class="titre-bloc"><h2>Bide&amp;Musique sur France Inter !</h2></div>
<div class="accnews">
Quand France Inter fait appel à Bide&amp;Musique pour une chronique spéciale hymne de foot, cela donne ceci : <br/>
<br/>
<a href="https://www.franceinter.fr/amp/emissions/la-victoire-en-chantant/la-victoire-en-chantant-03-janvier-2021?__twitter_impression=true">https://www.franceinter.fr/amp/emissions/la-victoi…</a><br/>
<br/>
Bonne écoute !</div>
<table style="width:100%;"> <tr>
<td><p><a class="infobulle" href="/news.rss"><img alt="[ Les news ]" border="0" src="/images/ico_rss.png"/><span>Recevoir<br/>les news de B&amp;M<br/>par RSS</span></a></p></td>
<td><p style="text-align: right; font-weight: bold;"><br/><a href="/forumgeneral_26165_1.html" style="color:#999999">En savoir plus / Réagir</a></p></td>
</tr></table>
<div id="basgd-encartblc"></div>
</div>
<div class="txtclear"></div>
<div id="gd-encartblc">
<div class="titre-programmation"></div>
<div id="BM_next_progs">
<div class="titre-bloc"><h2>Demandez le programme</h2></div>
<div class="sstitre-programmation">
mercredi 13 janvier - 18:44</div>
<table class="bmtable" width="100%">
<tr>
<td><strong><em>En cours</em></strong></td>
<td class="tcentered" colspan="2">Programmation générale (jusqu'à 19h00)</td>
</tr>
<tr>
<td>19h00 - 19h45</td>
<td><a href="/account/93488.html">Favoris de gudule3</a></td>
<td>Durée :  45 minutes 
</td>
</tr>
<tr>
<td>19h45 - 21h00</td>
<td class="tcentered" colspan="2">Programmation générale</td>
</tr>
<tr>
<td>21h00 - 22h30</td>
<td><a href="/program/12.html">B.O.F. : Bides Originaux de Films</a></td>
<td>Durée : 1 heure 30 minutes 
</td>
</tr>
<tr>
<td>22h30 - 02h30</td>
<td class="tcentered" colspan="2">Programmation générale</td>
</tr>
<tr>
<td>02h30 - 03h00</td>
<td><a href="/program/95.html">Radio Bide</a></td>
<td>Durée :  30 minutes 
</td>
</tr>
</table>
</div>
<div id="BM_next_songs">
<div class="sstitre-programmation">
À venir sur la platine
</div>
<table class="bmtable">
<tr>
<td>
<a href="/program/19.html">
            Spécial Noël            </a>
</td>
<td class="vignette25">
<a href="/show-image.html?I=/images/pochettes/1640.jpg&amp;T=Corou+de+Berra+-+Petit+papa+No%EBl" onclick="openImage('/images/pochettes/1640.jpg', 'Corou+de+Berra+-+Petit+papa+No%EBl', 600, 600); return false;"><img alt="Vignette de Corou de Berra - Petit papa Noël" src="/images/thumb25/1640.jpg" title="Cliquez pour agrandir"/></a>
</td>
<td class="baseitem">
<a href="/artist/7745.html" title="Visiter la fiche de « Corou de Berra »
                        ">
            Corou de Berra            </a>
</td>
<td class="baseitem">
<a href="/song/1640.html" title="Visiter la fiche de « Petit papa Noël »">
            Petit papa Noël            </a>
</td>
</tr>
<tr>
<td>
</td>
<td class="vignette25">
<a href="/show-image.html?I=/images/pochettes/1419.jpg&amp;T=Daniel+Auteuil+-+Que+la+vie+me+pardonne" onclick="openImage('/images/pochettes/1419.jpg', 'Daniel+Auteuil+-+Que+la+vie+me+pardonne', 507, 507); return false;"><img alt="Vignette de Daniel Auteuil - Que la vie me pardonne" src="/images/thumb25/1419.jpg" title="Cliquez pour agrandir"/></a>
</td>
<td class="baseitem">
<a href="/artist/638.html" title="Visiter la fiche de « Daniel Auteuil »
                        ">
            Daniel Auteuil            </a>
</td>
<td class="baseitem">
<a href="/song/1419.html" title="Visiter la fiche de « Que la vie me pardonne »">
            Que la vie me pardonne            </a>
</td>
</tr>
</table>
</div>
<div id="BM_past_songs">
<div class="sstitre-programmation">
     De retour dans leur bac    
    </div>
<table class="bmtable">
<tr>
<td class="date" style="width: 45px;"> 
            18:37        </td>
<td class="vignette25">
<a href="/show-image.html?I=/images/pochettes/4436.jpg&amp;T=The+Cure+-+The+Love+Cats" onclick="openImage('/images/pochettes/4436.jpg', 'The+Cure+-+The+Love+Cats', 500, 501); return false;"><img alt="Vignette de The Cure - The Love Cats" src="/images/thumb25/4436.jpg" title="Cliquez pour agrandir"/></a>
</td>
<td class="baseitem">
<a href="/artist/729.html" title="Visiter la fiche de « The Cure »
                        ">
            The Cure            </a>
</td>
<td class="baseitem">
<a href="/song/4436.html" title="Visiter la fiche de « The Love Cats »">
            The Love Cats            </a>
</td>
</tr>
<tr>
<td class="date" style="width: 45px;"> 
            18:36        </td>
<td class="vignette25">
<a href="/show-image.html?I=/images/pochettes/3150.jpg&amp;T=G%E9n%E9rique+S%E9rie+-+K+2000+%28le+speech+du+d%E9but%29" onclick="openImage('/images/pochettes/3150.jpg', 'G%E9n%E9rique+S%E9rie+-+K+2000+%28le+speech+du+d%E9but%29', 500, 500); return false;"><img alt="Vignette de Générique Série - K 2000 (le speech du début)" src="/images/thumb25/3150.jpg" title="Cliquez pour agrandir"/></a>
</td>
<td class="baseitem">
<a href="/artist/595.html" title="Visiter la fiche de « Générique Série »
                        ">
            Générique Série            </a>
</td>
<td class="baseitem">
<a href="/song/3150.html" title="Visiter la fiche de « K 2000 (le speech du début) »">
            K 2000 (le speech du début)            </a>
</td>
</tr>
<tr>
<td colspan="4" valign="middle"><div style="text-align:right; padding:0px 8px 5px 8px;">
<a href="#" onclick="new Ajax.Updater ('BM_past_songs', '/prog-past-accueil.php?l=1');return false;"><img alt="[+]" src="images/bt-puce-plus.png" style="float:right; margin:0 0 0 3px;"/></a>
<div style="padding:3px 0 0 0;">Voir toutes les archives</div>
</div></td></tr>
<!--
<tr class="entete">
<td colspan="4" class="morestuff"
    title="Cliquez pour allonger la liste"
    onclick="new Ajax.Updater ('BM_past_songs', '/prog-past-accueil.php?l=1');return false;">
[<a href="#" onclick="new Ajax.Updater ('BM_past_songs', '/prog-past-accueil.php?l=1');return false;">+</a>]
</td>
</tr>
-->
</table>
</div>
<div id="basgd-encartblc"></div>
</div>
<div class="txtclear"></div>
<div id="pt-encartblc1">
<div class="titre-bloc2"><h2>Présentation de Bide&amp;Musique</h2></div>
<p>
Depuis plus de dix ans, Bide&amp;Musique fait revivre sur Internet des morceaux qui ne sont pas ou plus diffusés sur les média traditionnels. Radio associative, projet collectif de passionnés de la musique, elle dispose d'une base discographique inégalée par sa richesse et sa diversité : 22738 titres et 10343 artistes différents. <br/>
<br/>
Bide&amp;Musique vous propose, à travers sa programmation, mais aussi des émissions thématiques, une plongée dans la chanson française et la variété internationale, des années 50 à nos jours. Laissez vous surprendre, séduire, amuser par une sélection rigoureusement établie sur un seul critère : le plaisir de la découverte, de la redécouverte, de musiques qui ont bercé, bercent ou auraient pu bercer notre quotidien. <br/>
<br/>
Bide&amp;Musique, c'est aussi une communauté de mélomanes aux goûts décalés qui peuvent partager et parfaire leur culture musicale grâce aux fonctions interactives du site. C'est grâce à vous aussi, les bidonautes, qu'elle est devenue une source d'information reconnue, consultée et sollicitée par de nombreux professionnels.
</p><div id="baspt-encartblc"></div>
</div>
<div id="pt-encartblc2">
<div class="titre-bloc2"><h2>Les liens utiles</h2></div>
<script>
     function Popup_BM_Flash(){open("http://www.bide-et-musique.com/player2/bideplayertest.html","Bide & Musique","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=580px,height=340px");}     </script>
<div id="liensutiles">
<a class="link1" href="/grille.html" title="Radio"></a>
<a class="link2" href="/nos-disques.html" title="Archives"></a><a class="link3" href="/faq.html" title="Questions"></a><a class="link4" href="/evenements.html" title="Evènements"></a><a class="link5" href="/forum.html" title="Forum"></a><a class="link6" href="https://www.comboutique.com/bide_et_musique" title="Le Shop"></a><a class="link7" href="/quizz-bide.html" title="Quizz"></a><a class="link8" href="/inscription-newsletter.html" title="Newsletter"></a><a class="link9" href="/goodies.html" title="Goodies"></a><a class="link10" href="/news.rss" title="Rss"></a><div class="txtclear"></div></div>
<div id="baspt-encartblc"></div>
</div>
<div class="txtclear"></div>
</div>
</td>
</tr>
<tr>
<td align="left" id="basint" valign="bottom"><img src="/images/bg-bas.png"/></td>
</tr>
</table>
<div id="footer">
<div id="bullefooter"><a href="#"><img src="/images/bulle-footer.png"/></a></div>
<div id="logofooter"><a href="/index.html"><img src="/images/logo-bide-footer.png"/></a></div>
<p><img height="7" src="/images/bg-dotted.jpg" width="96"/> <a href="/index.html">Accueil</a> | <a href="/nos-disques.html">Nos disques</a> | <a href="/forum.html">Forum</a> | <a href="/evenements.html">Evénements</a> | <a href="/goodies.html">Goodies</a> | <a href="/infos.html">Infos</a> | <a href="/contact.html">Contact</a> | <a href="/partenaires-liens.html">Partenaires et liens</a> <img height="7" src="/images/bg-dotted.jpg" width="96"/></p>
<p>Bide &amp; Musique ©2021 - <a href="mailto:contact@bide-et-musique.com">contact@bide-et-musique.com</a></p>
<br/><br/>
<script src="//www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
				_uacct = "UA-9280927-1";
				urchinTracker();
				</script>
</div></td>
<td> </td>
</tr>
</table>
</body>
</html>
