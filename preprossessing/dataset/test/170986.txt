<!DOCTYPE html>
<html class="no-js" lang="de-DE">
<head id="Head">
<script data-blockingmode="auto" data-cbid="75316ba8-4873-4fea-aea8-8ce696d926f2" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script>
<title>
	Deine Ausbildung in der M+E-Industrie - ausbildung-me.de
</title><meta charset="utf-8"/><meta content="Du möchtest eine Ausbildung machen oder dich über Berufe informieren? Erfahre hier mehr über die Ausbildung in der Metall- und Elektro-Industrie." id="MetaDescription" name="DESCRIPTION"/><meta content="INDEX, FOLLOW" id="MetaRobots" name="ROBOTS"/><meta content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link href="/Portals/_default/Skins/schueler/css/normalize.css" rel="stylesheet"/><link href="/Portals/_default/Skins/schueler/css/slicknav.css" rel="stylesheet"/><link href="/Portals/_default/Skins/schueler/css/font-meb.css" rel="stylesheet"/><link href="/Portals/_default/Skins/schueler/css/owl.carousel.css" rel="stylesheet"/><link href="/Portals/_default/Skins/schueler/css/owl.transitions.css" rel="stylesheet"/><link href="/Portals/_default/Skins/schueler/css/jquery.fancybox.min.css" rel="stylesheet"/><link href="apple-touch-icon-57.png" rel="apple-touch-icon"/><link href="apple-touch-icon-72.png" rel="apple-touch-icon" sizes="72x72"/><link href="apple-touch-icon-76.png" rel="apple-touch-icon" sizes="76x76"/><link href="apple-touch-icon-114.png" rel="apple-touch-icon" sizes="114x114"/><link href="apple-touch-icon-120.png" rel="apple-touch-icon" sizes="120x120"/><link href="apple-touch-icon-144.png" rel="apple-touch-icon" sizes="144x144"/><link href="apple-touch-icon-152.png" rel="apple-touch-icon" sizes="152x152"/><link href="/Portals/_default/default.css?cdv=75" media="all" rel="stylesheet" type="text/css"/><link href="/Portals/_default/Skins/schueler/skin.css?cdv=75" media="all" rel="stylesheet" type="text/css"/><link href="/Portals/0/portal.css?cdv=75" media="all" rel="stylesheet" type="text/css"/><link href="/DesktopModules/EasyDNNNews/static/rateit/css/rateit.css?cdv=75" media="all" rel="stylesheet" type="text/css"/><link href="/DesktopModules/EasyDNNNews/static/common/common.css?cdv=75" media="all" rel="stylesheet" type="text/css"/><script src="/DesktopModules/EasyDNNNews/static/eds_jquery/eds_jq.js?cdv=75" type="text/javascript"></script><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=75" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=75" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_10_03/jquery-ui.js?cdv=75" type="text/javascript"></script><!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1089493467735179');
fbq('track', "PageView");</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1089493467735179&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
<link href="/Portals/0/favicon.ico?ver=2015-11-03-093052-530" rel="SHORTCUT ICON" type="image/x-icon"/><meta content="Deine Ausbildung in der M+E-Industrie - ausbildung-me.de" property="og:title"/><meta content="website" property="og:type"/><meta content="https://www.ausbildung-me.de/" property="og:url"/><meta content="http://www.ausbildung-me.de/portals/0/was ist me352.jpg" property="og:image"/><meta content="de_DE" property="og:locale"/><meta content="Du möchtest eine Ausbildung machen oder dich über Berufe informieren? Erfahre hier mehr über die Ausbildung in der Metall- und Elektro-Industrie." property="og:description"/><meta content="ausbildung-me.de" property="og:site_name"/><meta content='{"instanceGlobal":"EasyDnnSolutions1_1_instance","google":{"maps":{"api":{"key":"","libraries":["geometry","places"]}}}}' name="eds__GmapsInitData"/></head>
<body id="Body">
<form action="/" enctype="multipart/form-data" id="Form" method="post">
<div class="aspNetHidden">
<input id="StylesheetManager_TSSM" name="StylesheetManager_TSSM" type="hidden" value=""/>
<input id="ScriptManager_TSM" name="ScriptManager_TSM" type="hidden" value=""/>
<input id="__VIEWSTATE" name="__VIEWSTATE" type="hidden" value="Opl/PSYJlQqV6W9tIDBNNx72p1T6IFncsAcHH0g/GVGszUuUM4DwbRr7xrQE+Z4I32vvTRl+xxkdp4Hlke7IqcAWDgeTzYOZAvakHLT7gR8ngQkIoesRe0A8I0yiDAkaIYsKSBx93aYYuMbgZYnnrmJP5UQsxnBT2TSvF3BMXK4s0jP08mzSq4oG1TID50iR9UqJXSqgRbnnR4ko3cUIBPmMlVTls9G6aNoJUlVKujMN8jYzPi2TCkdmCI/mscYGkZdq2w50/LE9htxTZYuAQSYyS6nzw46MnthHrfk6g/ynapzejdNMcTED8c+9j/SHTjPDtFW03cMPl7WyrlzrW6nL2JV7lw1CTeMSTzk8TTh849bvz4XMhre8FrY4/MKbHd20GBbzhxdzee4GvJZhU6D+OS0ua56hFq6UGjg/CvZJ108IU8cN1p3FcxfKMFUcVnL9m6Op9JHaCh8JcLOQwmsTWmAd4DrvAZChY7Ff5xeRDELi4AuozF8ct5ORfAbZa7uXq5Y68jS8gpa9FgE/YpAqi4jNBElBfiJL/8bwbCkYXJgr6cGWwrXvWFzh30dUS1gZoavxrqIwIRxCNiSSTWLZd69a0FmZylvNsuQd0sJzA+yWlzLDMBXMMIaBRyqOo+5nBYW4CTkh4mHr8QNnjwZduZ4HJYNm1zdJfktOLYv0FNpMZtHhnF1R0+mFaBGMrIrEW8E//9p+CSG9cu4LYJp1agmhUSqvi7D9t/R/Woq+BbrpRRnrw3HEYveB7JMVlfwLNx+cgkVn/fqqMH0bSVvjmXGJ5gK8is5gMLtBxQN7sejOMZhplDqmRy7Rv4AHBrwNDNQ401pTl0Gx76ESFMT4VelOn+pbvT+zhqFBsnytkTDmleCSygmrq/lnScOkZzvxsp70oYHRTwE/8mv2nQbuoFN+QnuLDDFSGZEhjOtrFDL2rqfSC2PBPOT6TSsFp6f3kkPOzvtTSbarH/qIpTgpWkpGt52vi/ktaxXasX6Ff/uQYqfei9iDgLM4KW9iMtVYPQDHnGVSgZkLc3W/omwQWHC3AVbMX7vHAmq9XFAudlDRD+jDc3zkIXNbZG5VUSj5MVSCjpn4OW+or3zj5J5QvLXOhhrQBJqbU542ZhSqUyIMazp3OnLzyr98Xz7MVsiCHlUjBOeY9hNOF3aOkmViGwftbmyhH1HgJaVLx15DKl3PLsBb6NiEmoZMcVrH6nBShrQcrfWM5e6yo4NTijeoAVt5Z7y5j9+F6ne+Db7QClB/Cbn6oRu1/F1eRw/w0vJ9ZwrHTLdV4UqzK0vsHzHllCm3KlrB8xvk4SThXqVnXbhcZqClHwhPYGdLnNHSXjPd8SUa5zXxE0swtUfrIt8KkFtr+ancTA/vOiInBcDK23nsuw8AVAJ8ylUF+Do2G0G30rtJ5CxoVd/f0caKaAEchTkFVknNaX5xuwNusuu6IJrMl2fnxq3TRYgq7YRzdhHAjX2JvGn5uAJc2ZOvn7txHb2ddc8GH05ftGqE01i/C9oBLuq2wuPTVwABN0DkDfWH4qhivwP1bMVMXxxvsLt6v9fUQ0TIjKAARGYmcddpo8eBk5/3GOB+rNXQEen8kdjRRrQHqRCS3V8Hl2NX5FjqcdldYyh5Ww6fxolbom94fVlfe1MQlUsaIYwxCIhkHXCazcp0HGf7U9w5ZO70JDedfThVAwcKcWeEy5DnxPB6QqXdA8f2tnnRqVupkwJwwsuQ1dZXQDpAhzXtcJGlelTDP3Fs10IlothbIb9YChFJheGi3UAFvWFjoTNMzZ459x2IZtGJzzl5RQ7lAi74l1HOU0sw1JKYLbFaE1EceIecHMHMfaj7r+l9Y0sLJ0zY+I8NZvvlMg0+6zE7cjQJVoo8nfPXpZZlAoJHFlP3uLgBqONkuTo0SgaIubDAVMAldy/xiSzNyVH2NG/ufKwkjkxU0z/2bEE8Ar+PNxyFIqltxhWNbM46boixM1CGZBAGn2px1Sv8mfh5yRW3/9fSIAs2/Ubm76+kuNoQO1N8385Mqb4qTyEKNsHWdFPCWV4a/6cTJi6zW1+SQrWtyTS+8hi5Xk7o5IHvKIu3yw0ymAMuO6flzLzxu25fybZQY2gnrKlhoUyAsPTFEXKYYNbgnqulTZeljMRhIhRi6fzF+tUK3m1TDjIDPd884WUP6ZfPMUPDAqllOK8tv4t/zkjT0RSBxs8yZXXxQWaLliz4Jjagln67GyijXAOu+Qc+7uK8X+kEzikLVhgFhZz2Zry3brnCw5mrYCkAFC4PVCe4JNuPAWrJnVogtD8BBguH40P3mLvwHlLsJ4ZUFqvUFvGilI5aMtzCfOSTOU/WVvzPNGY7+NXZjEMKcs57WxsprOhg+igZSjYUjbjNjuJO3XltvyWGQgnTh6Ak11YPuLpp0rxfN80w3i7W82LpYbTriimb4ZizXiaaPAnDfiUUCGf7bp1JvyhEwO890xP0mndnu2ln9rndyd9bc+tM1mx2bYklXZyMREX+9apNhKKz6J4Dp6ly+IcvfKCgmhviMJs8x7brUcWZ4qxKd6yxJnCMtO7350qBXt1HWQ6A2riKm25voek3x34vzveFG/VcawvUO42oa6aSVgiVg2WIBG2qLOPfuEfbxm8A+oA8FAhOPvzeuhkpcWbbCWfL9WI8KiapHfTAfPQr6oDafxB0iqkKIZGkeF5Rq0V0PowMiGXvDxmloHQ81UGZ1PlpM4Omf80rRLXDR5Tay61Psure3QAU61NoTayd0iZmhjB02dS0hwzh0e4yuA+LwD5NtUGYI2flcTJ4UY3lN4nq+x7Heo2m+fGtf3NlHScokjq9TawOKSYjLlUsMtiGQTCS2H31zDh0rBkWX8vgbtjLZQrsBbVxGwKWq/zJxUEdBMzl3LomiM3XOvkiFyHtFWdluhQOa+DbnqCsbED4F8zIocL/4YT6gz7rCn/h9wbFrNZTaOA0MD9EMSVcbtgHdwtgxQuTKvTYX8KZref4n9mKOyO6STEKTju/wtGDzDXtVlcPCa7DCqODvjPZNHEyt2OvoLBOLWBdtf0AdCHqMlkCDUkcKk93C/8iPTv+syJxGTBBZLi4cLVSD0zUruQgDcQ4Zzc1KcFfk8s2r6WSYqZHiF5/DH+fGIBOgzJ0jtQL6BUn5jPkurh0GEW6D6zlhNd0HCTk8ArRipOHxdIFmzkRyqzaoQERBPySi1Ir/OYEyiTdR2fwMavwWNsM0AuK5d/mm45dntqyGj7TAsZAdGzUzSK+xdhaz/zCWIgE0WxnTqFwsxbjc4Wcgylf5jV8k1LoI/A4Z+5jJlMn8IfO1MW4hO//88VWIbQiXjCK/Bq735zb38BlbynHIOmoDyHzvlWpYfx3kJXd4A9RwcbWhbGC9rT2blFl3J7++7b/DdBLYFxrvTw3LN3cnTMr4FWtvhSUdZaZ04+wgOkrDa7tFUyGW9lSyUV5P5r0gU4ereBZCUiDc+kx4BKorXlRIKrYnIkO6P8CC07KAuYRq8AijzDkNOsdncJUGGHaXZ09LisEsE21jOw7PUNR9Qs+S1965ysN7+XaFmb8y5jIpB0l2sCUxN+hf0AqiTBEtjbCEUhOHkzJ9svwNqqjJ3tgNhUQrf71tJU2HXNYXL/tWvV4ajL247avs4Fni9ELaU2d2mv7/d8jPCTx4qzB6KjX3xgAchZnPR8uyalT37+qyBs+4Tk+THX9NFsWSu8X8d+ERTkodZuscvTiuJtMDJdMWY1hpSKBo6LttweVmpA/tbdCIHxH+AIxOM8RQAe0hSFAVxAEUk1Jaz595oAvCLLb0px1twpSoT1rhQ5JaST0p45ZFhXQA8O+g5S9NZ7PTEoVJoGy2Z/5QY+4mqTbAeRV2I2fVmLVgt1QAnRgexmOHqEns4+o4xan+fPPLYMhT7nmt+9E3jTdUpSucO8okDWP7+4OqFB6fu5OhpFYu963vXqWOb0prPeP4Avx55LhZ4txaSjBufGApCB9ikMH9jkXwSqbySsujuZoPQleedzDQk248bixZl7JStMZ0Xe8k7HTjWeqOC4U/PWB0c8+n8Kwbz4m15NdbS9AuFOsXLoXEGx67xTK0THoMwW5B0ynrBsf3I9v6RFCwzFlgO1u/rghT29/UsTE1qA6OuPYpgTyGD8MqWK5fozN4pawOcpj90r7L4wwdJtHOf1Rrcwyj2sIcdnvzUatBHy//S8V3k0ysi/DMkP6Ov4S7WtGUDJ9T9LHA/LnOtVug1lK66haRrJJwhNwmY+vB0+wHCYiiq72K0z8ju0KLiVbZEZTga7PBbefK7hTFkXdY2cNJrTOtlBJzP1+almK8TznAHuEoBNnxSmTWKLFRWTmrEFXwNxIn5fPXNZ7ywCq0ws67UPRzeMR26DvTeR8QxhbziUXBmsAD6u3C9DD34MUzCeWfS1mVJTOIGY6dSLz/0mjVMGM4a96+h3zV/B+WIxnU0uHQNESByRKax/gBfZVLcJiBf+MWKDdTGCdqwKpitQECkNG2lXteIC7ZK5FF03vYZVHwLzuLylYak+Z5MQ3cP25GPbOslMdTdBL8hp18gP93+O3deO5R8S2BnFmyrqygAvZZA85Ci4qbAOPvebYm12p2DLAG70UzIvfEHmZXThSTrHToQPwYrujpH1PnOlG2kJ6Jm94777Hx0R3f+uzqS7JTftCv8KD1hglKlqlRU72kPNf/kyNyDrqgMkApAZSVKB5D3jL0UUyXekSlHUntDv9IQ+bFGqkqVfJyS2Hzs/lRcqzCi+YkbVm6oOFOQMP/cS1Aw6HqqRiB/Jdwlmz/F3jDP0M0WcuBpAQg/7Z+C9VBAVzR8zSES5C7UWHm9Hmquwz+acZ4o624lU3z1L0P4fSkkAsmSOdMmXmUsJkHK7tTNaEOlVgVoMKrt84SkGgHIQOLyVVroLjXfOWeE0ey6ejYKXwCZYvZa+elcNWKZyLnDU6NIsZJ0q0Q98hlRarI9BD8W/ALC067t39PzvAH+0up+JPETtKtuXZ1tTlj2Ut7g1bK4DhUbNjJvcOLEIdQEPUyVwResaWDDpvq9mqtTpByDvlCreHfAjJRmgmhx02XjboDiuYoTP/XTk6U/ih+5CRsLMhVU3vDNnYR1qfs0kggW2zdDL6phoUKJFZRzQewydHSJreFxPGiJhpkuYDMwAk7e968gy9/jhRvLAlSNrvGZR9rUfMVmRxSSzXg2gke585/6ogclyA0/wG5ePFmp5XnABX56KngC6qNB8IrWuPZWWjxwj8THsydbZ7XIpEO1WzCOwZibMdQlEUK5NxNxU5IbBTtCHTsVMCsb61rvI/ZiGcgicsnFX6gsb49VVM4DAFoI3odR9Aq6OgRDkEKOuj17bXZecypkvriaot/3Z5hY9g/yr2oNvLmFLaP+FMeXgpzYQE1PSI4fUfoPcQpzDCK1ZFAyAGyUCg2QfhBatEeHxa1U4shnIKRNQODNNOiAE/UoHRjO79nxATAfjPXjvTBEQdK+TlyWBpRpH1DjcoLyTWuQTCsYmByFj73vsy"/>
</div>
<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"de-DE","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":",","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":".","CurrencySymbol":"€","NaNSymbol":"NaN","CurrencyNegativePattern":8,"NumberNegativePattern":1,"PercentPositivePattern":0,"PercentNegativePattern":0,"NegativeInfinitySymbol":"-∞","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":",","NumberGroupSeparator":".","CurrencyPositivePattern":3,"PositiveInfinitySymbol":"∞","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":",","PercentGroupSeparator":".","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402297199999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":".","FirstDayOfWeek":1,"CalendarWeekRule":2,"FullDateTimePattern":"dddd, d. MMMM yyyy HH:mm:ss","LongDatePattern":"dddd, d. MMMM yyyy","LongTimePattern":"HH:mm:ss","MonthDayPattern":"d. MMMM","PMDesignator":"","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"dd.MM.yyyy","ShortTimePattern":"HH:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy","AbbreviatedDayNames":["So","Mo","Di","Mi","Do","Fr","Sa"],"ShortestDayNames":["So","Mo","Di","Mi","Do","Fr","Sa"],"DayNames":["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],"AbbreviatedMonthNames":["Jan","Feb","Mrz","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez",""],"MonthNames":["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember",""],"IsReadOnly":false,"NativeCalendarName":"Gregorianischer Kalender","AbbreviatedMonthGenitiveNames":["Jan","Feb","Mrz","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez",""],"MonthGenitiveNames":["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember",""]},"eras":[1,"n. Chr.",null,0]};//]]>
</script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3ade-DE%3a1b322a7c-dfaa-439f-aa80-5f3d155ef91d%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<div class="aspNetHidden">
<input id="__VIEWSTATEGENERATOR" name="__VIEWSTATEGENERATOR" type="hidden" value="CA0B0334"/>
<input id="__VIEWSTATEENCRYPTED" name="__VIEWSTATEENCRYPTED" type="hidden" value=""/>
<input id="__EVENTVALIDATION" name="__EVENTVALIDATION" type="hidden" value="dfaLg0nqqelxK4/E6kfnH5I/ozcNGx9k+YbhJ5ID+lP/S9yi+1tnS9X/6MFrso4ojdvYwCzLh8cI/zw+cmGFdj2mQOnryWwuzfxuZj8+WQMeIMXaYspalqKDMI3IDyNDnDeUNlnVDifbLQV6"/>
</div><script src="/js/dnn.modalpopup.js?cdv=75" type="text/javascript"></script><script src="/DesktopModules/EasyDNNNews/static/rateit/js/jquery.rateit_2.2.js?cdv=75" type="text/javascript"></script><script src="/js/dnncore.js?cdv=75" type="text/javascript"></script><script src="/DesktopModules/EasyDNNNews/static/EasyDnnSolutions/EasyDnnSolutions_1.1_2.2.js?cdv=75" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>
<div class="wrapper-site">
<nav class="mainnav">
<div class="wrapper wrapper-nav">
<a class="logo" href="/" title="Zur Startseite"><img alt="Logo M+E-Berufe" src="/Portals/_default/Skins/schueler/img/logo.png"/></a>
<ul id="menu">
<span class="search-global">
<li class="search-global-item">
<input id="searchterm" placeholder="Suchbegriff" type="text"/>
<a id="searchcmd">
<span class="search-global-button">
<i class="icon-magnifying_glass"></i>
</span>
</a>
<script>
                        $('#searchterm').keyup(function(event) {
                            if (event.keyCode == 13) {
                                document.location="/suchergebnisse?q='" + $('#searchterm').val() + "'";
                            }
                        });
                    </script>
</li>
</span>
<li class="hover"><a href="/metall-und-elektro-industrie" title="">Metall- und Elektro-Industrie</a>
<ul class="submenu">
<li class="hide-mobile"><a href="/metall-und-elektro-industrie/m-e-live-erleben" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/me_industrie_me_live.png"/><i class="icon-chevron_right"></i><span>M+E live erleben</span></a></li>
<li><a href="/metall-und-elektro-industrie/was-ist-die-m-e-industrie" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/me_industrie_was_ist_me.png"/><i class="icon-chevron_right"></i><span>Was ist die M+E-Industrie?</span></a></li>
<li><a href="/metall-und-elektro-industrie/ausbildung-in-der-m-e-industrie" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/me_industrie_ausbildung.png"/><i class="icon-chevron_right"></i><span>Ausbildung in der M+E-Industrie</span></a></li>
<li><a href="/metall-und-elektro-industrie/girlspower" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/me_industrie_girlspower.png"/><i class="icon-chevron_right"></i><span>Girlspower</span></a></li>
</ul>
</li>
<li class="hover"><a href="/berufe" title="">Berufe</a>
<ul class="submenu">
<li><a href="/berufe/berufs-check" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/berufe_berufscheck.png"/><i class="icon-chevron_right"></i><span>Berufs-Check</span></a></li>
<li><a href="/berufe/berufe-nach-taetigkeitsfeldern" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/berufe_taetigkeitsfelder.png"/><i class="icon-chevron_right"></i><span>Berufe nach Tätigkeitsfeldern</span></a></li>
<li><a href="/berufe/berufe-von-a-z" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/berufe_az.png"/><i class="icon-chevron_right"></i><span>Berufe von A-Z</span></a></li>
<li><a href="/berufe/ausbildungsplaetze" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/berufe_ausbildungsplaetze.png"/><i class="icon-chevron_right"></i><span>Ausbildungsplätze</span></a></li>
<li><a href="/berufe/ausbildung-corona" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/berufe_corona.png"/><i class="icon-chevron_right"></i><span>Ausbildung &amp; Corona</span></a></li>
<li><a href="/berufe/me-berufe-app" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/me-berufe-app-141.png"/><i class="icon-chevron_right"></i><span>ME-Berufe-App</span></a></li>
</ul>
</li>
<li class="hover"><a href="/bewerbung" title="">Bewerbung</a>
<ul class="submenu">
<li><a href="/bewerbung/anschreiben" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_anschreiben.png"/><i class="icon-chevron_right"></i><span>Anschreiben</span></a></li>
<li><a href="/bewerbung/lebenslauf" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_lebenslauf.png"/><i class="icon-chevron_right"></i><span>Lebenslauf</span></a></li>
<li><a href="/bewerbung/e-mail-bewerbung" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_email_bewerbung.png"/><i class="icon-chevron_right"></i><span>E-Mail-Bewerbung</span></a></li>
<li><a href="/bewerbung/vorstellungsgespraech" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_vorstellungsgespraech.png"/><i class="icon-chevron_right"></i><span>Vorstellungs­gespräch</span></a></li>
<li><a href="/bewerbung/einstellungstest" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_einstellungstest.png"/><i class="icon-chevron_right"></i><span>Einstellungstest</span></a></li>
<li><a href="/bewerbung/eignungstests" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/bewerbung_eignungstests.png"/><i class="icon-chevron_right"></i><span>Eignungstests</span></a></li>
</ul>
</li>
<li class="hover"><a href="/karriere" title="">Karriere</a>
<ul class="submenu">
<li><a href="/karriere/in-6-schritten-zur-ausbildung" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/karriere_6_schritte.png"/><i class="icon-chevron_right"></i><span>In 6 Schritten zur Ausbildung</span></a></li>
<li><a href="/karriere/gehalt" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/karriere_gehalt.png"/><i class="icon-chevron_right"></i><span>Gehalt</span></a></li>
<li><a href="/karriere/weiterbildung" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/karriere_weiterbildung.png"/><i class="icon-chevron_right"></i><span>Weiterbildung</span></a></li>
<li><a href="/karriere/studium" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/karriere_duales_studium.png"/><i class="icon-chevron_right"></i><span>Studium</span></a></li>
</ul>
</li>
<li><a href="/blog" title="">Blog</a></li>
<li class="hover"><a href="/und-action" title="">... und Action</a>
<ul class="submenu">
<li><a href="/und-action/videos" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/action_videos.png"/><i class="icon-chevron_right"></i><span>Videos</span></a></li>
<li class="hide-mobile"><a href="/und-action/technik-tests" title=""><img alt="" src="/Portals/_default/Skins/schueler/img/navi/action_technik_tests.png"/><i class="icon-chevron_right"></i><span>Technik-Tests</span></a></li>
</ul>
</li>
</ul>
</div>
<div class="dropdown-bg" id="dropdown"></div>
</nav>
<header class="header-home">
<div class="wrapper wrapper-hgroup">
<h1>Deine Ausbildung</h1>
<p>Du willst dich über die Metall- und Elektro-Industrie informieren?<br/>Hier bekommst du alles, was du brauchst:<br/>Berufsorientierung, Wissenswertes zu Beschäftigungsmöglichkeiten, spannende Fakten über die Branche, praktische Tipps für Bewerbung und Ausbildung und vieles mehr.</p>
</div>
</header>
<article class="home clearfix">
<div class="wrapper">
<div id="dnn_ContentPane"><div class="DnnModule DnnModule-VDWWD_OpenGraph DnnModule-699"><a name="699"></a><div id="dnn_ctr699_ContentPane"><!-- Start_Module_699 --><div id="dnn_ctr699_ModuleContent">
</div><!-- End_Module_699 --></div>
</div><div class="DnnModule DnnModule-WebContent DnnModule-589"><a name="589"></a><div id="dnn_ctr589_ContentPane"><!-- Start_Module_589 --><div id="dnn_ctr589_ModuleContent">
<div class="row"><section class="s2 m3 l4 column teaser"><a href="/berufe/ausbildung-corona" target="" title="Ausbildung Corona"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>Ausbildung &amp; Corona</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">FAQ rund um die Ausbildung während der Corona-Pandemie.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt="Ausbildung Corona" src="/Portals/0/bilder/ausbildung-corona-352.jpg" title="Ausbildung Corona"/></div></a></section><section class="s2 m3 l4 column teaser"><a href="/berufe/berufs-check" target="" title=" Berufs-Check"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>Berufs-Check</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">Finde heraus, welcher Beruf zu dir passt.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt=" Berufs-Check" src="/Portals/0/bilder/teaser-berufscheck.jpg" title=" Berufs-Check"/></div></a></section><section class="s2 m3 l4 column teaser"><a href="/karriere/gehalt" target="" title="Gehalt"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>Gehalt</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">So viel verdienst du in der M+E-Industrie.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt="Gehalt" src="/Portals/0/bilder/teaser-gehalt.jpg" title="Gehalt in der M+E-Industrie"/></div></a></section><section class="s2 m3 l4 column teaser"><a href="https://www.youtube.com/results?search_query=%23followfalk" target="" title="#followfalk"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>#followfalk</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">Schau mit Falk in die Unternehmen.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt="#followfalk" src="/Portals/0/bilder/follow-falk.jpg" title="#followfalk"/></div></a></section><section class="s2 m3 l4 column teaser"><a href="/berufe/ausbildungsplaetze" target="" title="Freie Ausbildungsplätze finden"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>Ausbildungsplatz finden</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">Hier findest du Unternehmen mit freien Stellen.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt="Freie Ausbildungsplätze finden" src="/Portals/0/bilder/teaser-ausbildungsplaetze.jpg" title="Freie Ausbildungsplätze finden"/></div></a></section><section class="s2 m3 l4 column teaser"><a href="/bewerbung" target="" title="Bewerbungstipps"><div class="teaser-header">
<div class="teaser-header-wrapper">
<h2>Bewerbungstipps</h2>
<div class="shadow"></div>
</div>
<div class="teaser-subheader"><span class="text">Vorlagen für Anschreiben und Lebenslauf.</span><span class="icon"><i class="icon-chevron_right"></i></span></div>
</div>
<div class="teaser-image-wrapper"><img alt="Bewerbungstipps" src="/Portals/0/bilder/bewerbung/vorstellungsgespraech352.jpg" title="Bewerbungstipps"/></div></a></section></div>
</div><!-- End_Module_589 --></div>
</div></div>
</div>
<div class="row blog-row">
<div class="blog-header-home">Unser Blog</div>
<div id="dnn_BlogPane"><div class="DnnModule DnnModule-EasyDNNnews DnnModule-646"><a name="646"></a><div id="dnn_ctr646_ContentPane"><!-- Start_Module_646 --><div id="dnn_ctr646_ModuleContent">
<script type="text/javascript">
	/*<![CDATA[*/
	
	
	
	
	
	
	
	
	
	
	

	eds3_5_jq(function ($) {
		if (typeof edn_fluidvids != 'undefined')
			edn_fluidvids.init({
				selector: ['.edn_fluidVideo iframe'],
				players: ['www.youtube.com', 'player.vimeo.com']
			});
		
		
		

	});
	/*]]>*/
</script>
<div class="eds_news_module_646 news eds_subCollection_news eds_news_Schueler eds_template_List_Article_Homepage eds_templateGroup_newsListArticleDefault eds_styleSwitchCriteria_module-646">
<div id="dnn_ctr646_ViewEasyDNNNewsMain_ctl00_pnlListArticles">
<!--ArticleRepeat:Before:-->
<div class="edn_646_article_list_wrapper"><!--ArticleTemplate-->
<div class="blog-item even item_0">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/das-perfekte-outfit-fuers-vorstellungsgespraech" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Das perfekte Outfit fürs Vorstellungsgespräch" src="/Portals/0/EasyDNNNews/2/250250p646EDNthumb2outfit-vorstellungsgespraech.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Das perfekte Outfit fürs Vorstellungsgespräch</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item odd item_1">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/corona-wie-geht-es-2021-an-den-schulen-weiter-1" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Corona - wie geht es 2021 an den Schulen weiter?" src="/Portals/0/EasyDNNNews/231/250250p646EDNthumbimg-lockdown-2021.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Corona - wie geht es 2021 an den Schulen weiter?</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item even item_2">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/top-motiviert-ins-neue-jahr-starten-jetzt-fuer-ausbildung-2021-bewerben-1" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Top motiviert ins neue Jahr starten: Jetzt für Ausbildung 2021 bewerben!" src="/Portals/0/EasyDNNNews/229/250250p646EDNthumbimg-bewerbung-2021.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Top motiviert ins neue Jahr starten: Jetzt für Ausbildung 2021 bewerben!</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item odd item_3">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/followfalk-bei-thyssenkrupp-marine-systems" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="#followfalk bei Thyssenkrupp Marine Systems" src="/Portals/0/EasyDNNNews/228/250250p646EDNthumbfalk-thyssenkrupp-454.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>#followfalk bei Thyssenkrupp Marine Systems</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item even item_4">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/muss-ich-nach-der-berufsschule-noch-in-den-betrieb" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Muss ich nach der Berufsschule noch in den Betrieb?" src="/Portals/0/EasyDNNNews/15/250250p646EDNthumbberufsschule.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Muss ich nach der Berufsschule noch in den Betrieb?</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item odd item_5">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/fuer-technische-berufe-begeistern-der-me-infotruck" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Für technische Berufe begeistern - der M+E-InfoTruck" src="/Portals/0/EasyDNNNews/227/250250p646EDNthumb227infotruck-imagefilm.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Für technische Berufe begeistern - der M+E-InfoTruck</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item even item_6">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/trotz-corona-pandemie-fuer-eine-ausbildung-bewerben" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="Trotz Corona-Pandemie für eine Ausbildung bewerben" src="/Portals/0/EasyDNNNews/226/250250p646EDNthumb226bewerbung-454.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>Trotz Corona-Pandemie für eine Ausbildung bewerben</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item odd item_7">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/4-technologietrends-fuer-die-ausbildung-2021" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="4 Technologietrends für die Ausbildung 2021" src="/Portals/0/EasyDNNNews/225/250250p646EDNthumb225cobot-454.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>4 Technologietrends für die Ausbildung 2021</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item even item_8">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/innovativetechnology-6-beispiele-aus-der-bionik" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="#innovativetechnology: 6 Beispiele aus der Bionik" src="/Portals/0/EasyDNNNews/224/250250p646EDNthumb224Bionik.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>#innovativetechnology: 6 Beispiele aus der Bionik</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
<!--ArticleTemplate-->
<div class="blog-item odd item_9">
<div class="blog-item-image-wrapper">
<div class="blog-item-image">
<a href="https://www.ausbildung-me.de/blog/blog-detail/innovativetechnology-wie-funktioniert-3d-druck" target="_self">
<div class="blog-item-image-bgcolor">
<img alt="#innovativetechnology: Wie funktioniert 3D-Druck?" src="/Portals/0/EasyDNNNews/192/250250p646EDNthumb192printer-4348150_1920.jpg"/>
</div>
<div class="blog-item-image-header">
<h2>#innovativetechnology: Wie funktioniert 3D-Druck?</h2>
<i class="icon-chevron_right icon2x"></i>
</div>
</a>
</div>
</div>
</div>
</div><!--ArticleRepeat:After:-->
</div>
</div>
</div><!-- End_Module_646 --></div>
</div></div>
</div>
</article>
<div class="push"></div>
</div>
<div class="wrapper-footer">
<footer>
<div class="wrapper clearfix">
<div class="footer-item-a">
<div class="footer-header"></div>
<span class="footer-header-a">Folge uns auf:</span>
<nav>
<ul class="clearfix">
<li><a href="https://www.facebook.com/IchhabPower" target="_blank" title="Facebook"><i class="icon-facebook icon2x icon-footer icon-footer-facebook"></i></a></li>
<li><a href="https://www.instagram.com/ausbildung_me" target="_blank" title="Instagram"><i class="icon-instagram icon2x icon-footer icon-footer-instagram"></i></a></li>
<li><a href="https://www.youtube.com/user/MEBerufe" target="_blank" title="Youtube"><i class="icon-youtube_play icon2x icon-footer icon-footer-youtube"></i></a></li>
<li><a href="https://twitter.com/meberufe_info" target="_blank" title="Twitter"><i class="icon-twitter icon2x icon-footer icon-footer-twitter"></i></a></li>
</ul>
</nav>
</div>
<div class="footer-item-b">
<span class="footer-header-b">Mehr Infos für dich</span>
<nav>
<ul>
<li><a href="/umfrage-infotruck" title="Zur Umfrage"><i class="icon-chevron_right"></i> Dein Feedback zum M+E-InfoTruck</a></li>
<li><a href="/linktipps" title="Zu den Linktipps"><i class="icon-chevron_right"></i> Linktipps</a></li>
<li><a href="/kontakt" title="Zur Kontakt-Seite"><i class="icon-chevron_right"></i> Kontakt zur Redaktion</a></li>
<li><a href="/_sitemap" title="Zur Sitemap"><i class="icon-chevron_right"></i> Sitemap</a></li>
<li><a href="/leitbild"><i class="icon-chevron_right"></i> Leitbild</a></li>
<li><a href="/impressum" title="Zum Impressum"><i class="icon-chevron_right"></i> Impressum</a></li>
<li><a href="/datenschutz" title="Zur Datenschutz-Seite"><i class="icon-chevron_right"></i> Datenschutz</a></li>
<li><a href="/cookie-einstellungen" title="Zu den Cookie-Einstellungen"><i class="icon-chevron_right"></i> Cookie-Einstellungen</a></li>
</ul>
</nav>
</div>
</div>
<div class="search-appr clearfix">
<div class="search-appr-header">
                Freie Plätze
                <span></span>
<div class="search-toggle-desktop"><i class=" icon-chevron_left icon2x"></i></div>
<div class="search-toggle"><i class=" icon-chevron_up"></i></div>
</div>
<div class="search-appr-body search-appr-body-home">
<div class="search-appr-standard">
<div class="search-appr-input-wrapper">
<span class="search-appr-input"><label>Was?</label><input class="typeahead-berufe-unternehmen" name="suche" placeholder="Beruf/Unternehmen eingeben" type="text"/></span>
<span class="search-appr-input"><label>Wo?</label><input class="typeahead-ort-plz" name="ort" placeholder="Ort oder PLZ eingeben" type="text"/></span>
</div>
</div>
<div class="search-appr-button">
<input type="button" value="Plätze suchen"/>
</div>
</div>
</div>
<div class="comenius"><img alt="Comenius Edu Med Siegel 2016" src="/Portals/_default/Skins/schueler/img/comenius-siegel-2016-150.png"/></div>
</footer>
<a class="" href="#" id="atop"><i class="icon-chevron_up"></i></a>
<div class="bg-bottom-right"></div>
</div>
<div class="bg-top-left"></div>
<script src="/Portals/_default/Skins/schueler/js/all.min.js"></script>
<script src="/Portals/_default/Skins/schueler/js/modernizr-custom.js"></script>
<script>
    jQuery(document).ready(function($){
        $(window).scroll(function(){
            if ($(this).scrollTop() > 450) {
                $('#atop').fadeIn('fast');
            } else {
                $('#atop').fadeOut('fast');
            }
        });
        $('#atop').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
    });
</script>
<script>
    $(function(){
        var val = $('#range').val();
            output  = $('#output');
     
        output.html(val);
     
        $('#range').change(function(){
            output.html(this.value);
        });

        $(".blog-row").blogslider();

        $('#menu li:has(ul)').doubleTapToGo();
        
        $('#menu').slicknav({
            allowParentLinks: true,
            closedSymbol: '<i class="icon-chevron_down icon2x"></i>',
            openedSymbol: '<i class="icon-chevron_up icon2x"></i>'
        });

        $('#menu').slicknav();

        var height = $(window).height();

        $('.slicknav_nav').css({'max-height': height, 'overflow-y':'scroll'});       

    });
    
</script>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.rheinsitemedia.net/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 3]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img alt="" src="//piwik.rheinsitemedia.net/piwik.php?idsite=3" style="border:0;"/></p></noscript>
<!-- End Piwik Code -->
<!-- Twitter single-event website tag code -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('l5qn7', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img alt="" height="1" src="https://analytics.twitter.com/i/adsct?txn_id=l5qn7&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" style="display:none;" width="1"/>
<img alt="" height="1" src="//t.co/i/adsct?txn_id=l5qn7&amp;p_id=Twitter&amp;tw_sale_amount=0&amp;tw_order_quantity=0" style="display:none;" width="1"/>
</noscript>
<!-- End Twitter single-event website tag code -->
<input id="ScrollTop" name="ScrollTop" type="hidden"/>
<input autocomplete="off" id="__dnnVariable" name="__dnnVariable" type="hidden"/>
<script src="/Resources/Shared/scripts/initWidgets.js" type="text/javascript"></script></form>
</body>
</html>