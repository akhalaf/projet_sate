<!DOCTYPE html>
<html>
<head>
<title>Pemerintah Provinsi Bali</title>
<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/landing.css" rel="stylesheet" type="text/css"/>
<script src="assets/js/jquery.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
<div class="container-fluid">
<!-- LATAR BELAKANG -->
<div class="row">
<div class="latar"></div>
</div>
<!-- HEADER -->
<div class="row header">
<div class="col-lg-12 text-center">
<img src="assets/img/pemprov.png" style="width: 12%"/>
<img src="assets/img/pemprovbali.png" style="width: 60%"/>
<!-- <img src="assets/img/headputih1.png" style="width: 60%"> -->
</div>
</div>
<!-- ISI -->
<div class="row">
<div class="col-lg-6 text-center">
<img src="assets/img/gubwagub.png" style="width: 80%; margin-top: 20px"/>
</div>
<div class="col-lg-6 text-center text-white" style="margin-top: 0px">
<div style="height: 50px"></div>
<img src="assets/img/nangunsatkerthi.png" style="width: 100%"/>
<!--<h3 class="nangun">NANGUN SAT KERTHI LOKA BALI</h3>
				<p style="color: black">Melalui Pola Pembangunan Semesta Berencana Menuju Bali Era Baru</p>-->
<a href="https://www.baliprov.go.id/web">
<button class="btn btn-danger text-white" style="width: 40%">Masuk Situs</button>
</a>
</div>
</div>
<div class="fixed-bottom footer">
<div class="row">
<div class="col-lg-12 text-center text-white">
<p>Pemerintah Provinsi Bali &amp;copy 2019</p>
</div>
</div>
</div>
</div>
</body>
</html>
