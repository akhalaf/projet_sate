<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="fr-FR">
<![endif]--><!--[if IE 7]>
<html id="ie7" lang="fr-FR">
<![endif]--><!--[if IE 8]>
<html id="ie8" lang="fr-FR">
<![endif]--><!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!--><html lang="fr-FR" xmlns:fb="http://ogp.me/ns/fb#">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=9" http-equiv="X-UA-Compatible"/>
<title>
		Page non trouvée | Association du Cercle National	</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://cercle-national.ch/xmlrpc.php" rel="pingback"/>
<meta content="noindex,nofollow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="//c0.wp.com" rel="dns-prefetch"/>
<link href="//i0.wp.com" rel="dns-prefetch"/>
<link href="//i1.wp.com" rel="dns-prefetch"/>
<link href="//i2.wp.com" rel="dns-prefetch"/>
<link href="https://cercle-national.ch/feed/" rel="alternate" title="Association du Cercle National » Flux" type="application/rss+xml"/>
<link href="https://cercle-national.ch/comments/feed/" rel="alternate" title="Association du Cercle National » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/cercle-national.ch\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/aqua/assets/stylesheets/aqpb-view.css?ver=1610629219" id="aqpb-view-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/c/5.6/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css" type="text/css">
.has-text-align-justify{text-align:justify;}
</style>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/wp-pricing-table/css/base.css?ver=5.6" id="pricing-base-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/wp-pricing-table/css/layout.css?ver=5.6" id="pricing-layout-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/wp-pricing-table/css/fluid_skeleton.css?ver=5.6" id="pricing-fluid-skeleton-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/wp-pricing-table/css/pricing_table.css?ver=5.6" id="pricing-table-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C700italic%2C800italic%2C400%2C800%2C700%2C600%2C300&amp;ver=5.6" id="opensans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/bootstrap/css/bootstrap.min.css?ver=5.6" id="asalah_bootstrap_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/fontello/css/fontello.css?ver=5.6" id="asalah_fontello_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/framework/fontello/css/animation.css?ver=5.6" id="asalah_fontelloanimation_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/flexslider/flexslider.css?ver=6.600" id="asalah_flexslider_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/flexslider/galleryslider.css?ver=6.600" id="asalah_galleryslider_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/inc/shortcodes/style.css?ver=5.6" id="asalah_shortcodes_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/prettyphoto/css/prettyPhoto.css?ver=5.6" id="asalah_prettyphoto_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/isotope/style.css?ver=1" id="asalah_isotope_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/owl-carousel/owl.carousel.css?ver=6.600" id="asalah_owl_carousel_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/js/owl-carousel/owl.theme.css?ver=6.600" id="asalah_owl_theme_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/style.css?ver=6.600" id="asalah_main_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cercle-national.ch/wp-content/themes/bostan/responsive.css?ver=6.600" id="asalah_responsive_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://c0.wp.com/p/jetpack/9.3/css/jetpack.css" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://c0.wp.com/c/5.6/wp-includes/js/jquery/jquery.min.js" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://c0.wp.com/c/5.6/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script id="asalah_modernizer-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/modernizr.js?ver=5.6" type="text/javascript"></script>
<link href="https://cercle-national.ch/wp-json/" rel="https://api.w.org/"/><link href="https://cercle-national.ch/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://cercle-national.ch/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<!-- <meta name="NextGEN" version="3.5.0" /> -->
<style type="text/css">img#wpstats{display:none}</style> <style type="text/css">
				/* If html does not have either class, do not show lazy loaded images. */
				html:not( .jetpack-lazy-images-js-enabled ):not( .js ) .jetpack-lazy-image {
					display: none;
				}
			</style>
<script>
				document.documentElement.classList.add(
					'jetpack-lazy-images-js-enabled'
				);
			</script>
<!--[if lt IE 9]>
<script src="https://cercle-national.ch/wp-content/themes/bostan/js/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="https://cercle-national.ch/wp-content/themes/bostan/framework/fontawesome/css/font-awesome-ie7.min.css">
<![endif]-->
<meta content="Association du Cercle National" property="og:site_name"/>
<meta content="Depuis 1848" property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://cercle-national.ch/wp-content/themes/bostan/img/default.jpg" property="og:image"/>
<meta content="https://cercle-national.ch" property="og:url"/>
<style>.main_content.page_builder_content .blog_post > .new_section:first-child, .main_content.page_builder_content .blog_post .aq-template-wrapper > .new_section:first-child  { margin-top:0;}body{} a{} h1{} h2{} h3{} h4{} h5{} h6{} .contact_info_item{} .below_header .navbar .nav > li > a{} .page-header a{} .services_info h3{} .portfolio_info h5{} .blog_title h4{} .widget_container h3{} .site_footer{} .site_footer a{} .site_secondary_footer{} .site_secondary_footer a{} .page_title_holder h1{} .page_title_holder .breadcrumb, .page_title_holder .breadcrumb a{} .logo {margin-top:30px;}.main_navbar {margin-top:22px;}.logo img {width:237px;height:87px;}</style></head>
<body class="error404 body_width">
<!-- Load facebook SDK -->
<div id="fb-root"></div>
<script>
	jQuery(window).on('load', function() {
		window.fbAsyncInit = function() {
			FB.init({
								status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				xfbml: true // parse XFBML
			});
		};
		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk';
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement('script');
			js.id = id;
			js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
	});
	</script>
<!-- End Load facebook SDK -->
<script type="text/javascript">
		jQuery(document).bind("mobileinit", function () {
			jQuery.extend(jQuery.mobile, {
				autoInitializePage: false
			});
		});
	</script>
<!-- start site header -->
<header class="header_container body_width">
<!-- start top header -->
<div class="container-fluid top_header">
<div class="container">
<div class="row-fluid">
<div class="span12">
<!-- start contact info span -->
<!-- end contact info span -->
</div>
</div>
</div>
</div>
<!-- end top header -->
<!-- start below header -->
<div class="container-fluid body_width below_header headerissticky" id="below_header">
<div class="container">
<div class="row-fluid">
<div class="span12" id="below_header_span">
<div class="row-fluid">
<div class="span12">
<div class="logo no_retina">
<a href="https://cercle-national.ch" title="Association du Cercle National">
<img alt="Association du Cercle National" class="default_logo" height="87" src="http://cercle-national.ch/wp-content/uploads/2019/05/Cercle_National_gris.png" width="237"/>
<h1><strong class="hidden">
												Association du Cercle National</strong></h1>
</a>
</div>
<div class="gototop pull-right" id="gototop" title="Scroll To Top">
<i class="icon-up-open"></i>
</div>
<div class="mobile_menu_button">
<i class="icon-menu"></i>
<p class="mobile_menu_text">
										Menu									</p>
</div>
<nav class="span navbar main_navbar pull-right desktop_menu">
<div class="main_nav"><ul class="nav" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1201" id="menu-item-1201"><a href="https://cercle-national.ch/">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1192" id="menu-item-1192"><a href="https://cercle-national.ch/qui-sommes-nous/">Qui sommes-nous</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1193" id="menu-item-1193"><a href="https://cercle-national.ch/histoire/">Histoire</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1208" id="menu-item-1208"><a href="https://cercle-national.ch/contact/">Contact</a></li>
</ul></div> </nav>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end below header -->
<!-- start below header -->
<div class="container-fluid body_width below_header hidden_header">
</div>
<!-- end below header -->
<div class="header_shadow_separator"></div>
</header>
<!-- end site header -->
<div class="body_width site_middle_content"> <div class="page_title_holder container-fluid">
<div class="container">
<div class="page_info">
<h1>
						404 Error					</h1>
</div>
</div>
</div>
<!-- end post title holder -->
<section class="main_content">
<!-- start single portfolio container -->
<div class="container blog_style_1 blog_page blog_posts new_section">
<div class="row-fluid">
<div class="span12">
<article class="blog_post">
<div class="blog_banner clearfix">
<h2>
							Sorry						</h2>
<h3>
							It seems we can't find what you're looking for.						</h3>
</div>
</article>
</div>
</div>
</div>
<!-- end single portfolio container -->
</section><!-- #content -->
</div> <!-- end body width from header -->
<footer class="container-fluid site_footer body_width">
<div class="container">
<div class="row-fluid">
</div>
</div>
</footer>
<div class="container-fluid site_secondary_footer body_width">
<div class="container secondary_footer_container">
<div class="row-fluid">
<div class="span12 pull-left">
								Association du Cercle National							</div>
</div>
</div>
</div>
<!-- ngg_resource_manager_marker --><script id="aqpb-view-js-js" src="https://cercle-national.ch/wp-content/themes/bostan/framework/aqua/assets/javascripts/aqpb-view.js?ver=1610629219" type="text/javascript"></script>
<script id="jetpack-photon-js" src="https://c0.wp.com/p/jetpack/9.3/_inc/build/photon/photon.min.js" type="text/javascript"></script>
<script id="asalah_bootstrap-js" src="https://cercle-national.ch/wp-content/themes/bostan/framework/bootstrap/js/bootstrap.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_eslider-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/eslider/jquery.eislideshow.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_fitvids-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.fitvids.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_newsticker-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.ticker.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_flexslider-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/flexslider/jquery.flexslider-min.js?ver=6.600" type="text/javascript"></script>
<script id="asalah_easing-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.easing.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_owl_carousel-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/owl-carousel/owl.carousel.min.js?ver=6.600" type="text/javascript"></script>
<script id="asalah_prettyphoto-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/prettyphoto/js/jquery.prettyPhoto.js?ver=3.1.6" type="text/javascript"></script>
<script id="asalah_mousewheel-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.mousewheel.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_jq_mobile-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.mobile-1.4.5.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_transmit-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.transit.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_throttle-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/jquery.ba-throttle-debounce.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_component-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/component.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_isotope-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/isotope/jquery.isotope.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_imagesloaded-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/imagesloaded.pkgd.min.js?ver=5.6" type="text/javascript"></script>
<script id="asalah_scripts-js-extra" type="text/javascript">
/* <![CDATA[ */
var bostan_core_vars = {"ajax_load":"https:\/\/cercle-national.ch\/wp-content\/themes\/bostan\/inc\/ajax-load.php","ajax_load_pagination":"https:\/\/cercle-national.ch\/wp-content\/themes\/bostan\/inc\/ajax-load-pagination.php"};
/* ]]> */
</script>
<script id="asalah_scripts-js" src="https://cercle-national.ch/wp-content/themes/bostan/js/asalah.js?ver=6.600" type="text/javascript"></script>
<script id="jetpack-lazy-images-polyfill-intersectionobserver-js" src="https://cercle-national.ch/wp-content/plugins/jetpack/vendor/automattic/jetpack-lazy-images/src/js/intersectionobserver-polyfill.min.js?ver=1.1.2" type="text/javascript"></script>
<script id="jetpack-lazy-images-js-extra" type="text/javascript">
/* <![CDATA[ */
var jetpackLazyImagesL10n = {"loading_warning":"Images are still loading. Please cancel your print and try again."};
/* ]]> */
</script>
<script id="jetpack-lazy-images-js" src="https://cercle-national.ch/wp-content/plugins/jetpack/vendor/automattic/jetpack-lazy-images/src/js/lazy-images.min.js?ver=1.1.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://c0.wp.com/c/5.6/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script defer="" src="https://stats.wp.com/e-202102.js"></script>
<script>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:9.3',blog:'162541130',post:'0',tz:'1',srv:'cercle-national.ch'} ]);
	_stq.push([ 'clickTrackerInit', '162541130', '0' ]);
</script>
<script type="text/javascript">
		jQuery("a.prettyPhoto").prettyPhoto();

	jQuery(".prettyPhotolink").click(function() {
		var thisgal = jQuery(this).attr('rel');
		jQuery("a.prettyPhoto." + thisgal + ":first").click();
	});
	
	/* Mobile Menu */

	var custom_click_event = jQuery.support.touch ? "tap" : "click";
	jQuery(".main_menu .dropdown > a, .main_menu .dropdown-submenu > a").each(function() {
		if (jQuery(this).next().length > 0) {
			jQuery(this).addClass("mobile_menu_parent");
		};
	});

	jQuery('<span class="mobile_dropdown_arrow"><i class="fa fa-angle-down"></i></span>').appendTo('.main_menu li a.mobile_menu_parent');

	jQuery('.mobile_menu_button').on(custom_click_event, function(event) {
		event.stopImmediatePropagation();
		event.preventDefault();
		jQuery('.main_navbar.desktop_menu').removeClass('desktop_menu').addClass('mobile_menu');
		if (jQuery('#below_header_span').hasClass('mobile_menu_opened')) {
			jQuery('.main_navbar').slideUp();
			jQuery('#below_header_span').toggleClass('mobile_menu_opened');
		} else {
			jQuery('#below_header_span').toggleClass('mobile_menu_opened');
			jQuery('.main_navbar').slideDown();
		}
	});

	jQuery(".main_navbar a.dropdown-toggle i.icon-down-open").on(custom_click_event, function(event) {
		event.preventDefault();
		event.stopPropagation();
		var parent = jQuery(this).parent().parent();
		if (parent.hasClass('opened')) {
			jQuery(this).parent().next('ul.dropdown-menu').slideUp(100);
			parent.removeClass('opened');
			jQuery(this).removeClass('icon-up-open');
			jQuery(this).addClass('icon-down-open');
		} else {
			parent.addClass('opened');
			jQuery(this).parent().next('ul.dropdown-menu').slideDown(100);
			jQuery(this).removeClass('icon-down-open');
			jQuery(this).addClass('icon-up-open');
		}
	});
	jQuery(".main_navbar .dropdown-submenu a i.icon-down-open").on(custom_click_event, function(event) {
		event.preventDefault();
		event.stopPropagation();
		var parent = jQuery(this).parent().parent();
		if (parent.hasClass('opened')) {
			jQuery(this).parent().next('ul.dropdown-menu').slideUp(100);
			parent.removeClass('opened');
			jQuery(this).removeClass('icon-up-open');
			jQuery(this).addClass('icon-down-open');
		} else {
			parent.addClass('opened');
			jQuery(this).parent().next('ul.dropdown-menu').slideDown(100);
			jQuery(this).removeClass('icon-down-open');
			jQuery(this).addClass('icon-up-open');
		}
	});
</script>
</body>
</html>