<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]--><!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]--><!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>About – Andrey Kurenkov's Web World</title>
<meta content="about" name="keywords"/>
<!-- Twitter Cards -->
<meta content="About" name="twitter:title"/>
<meta content="summary" name="twitter:card"/>
<meta content="/images/thumb.png" name="twitter:image"/>
<!-- Open Graph -->
<meta content="en_US" property="og:locale"/>
<meta content="article" property="og:type"/>
<meta content="About" property="og:title"/>
<meta content="/" property="og:url"/>
<meta content="Andrey Kurenkov's Web World" property="og:site_name"/>
<meta content="/images/thumb.png" property="og:image"/>
<link href="/" rel="canonical"/>
<link href="/feed.xml" rel="alternate" title="Andrey Kurenkov's Web World Feed" type="application/atom+xml"/>
<!-- http://t.co/dKP3o1e -->
<meta content="True" name="HandheldFriendly"/>
<meta content="320" name="MobileOptimized"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- For all browsers -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/css/main.css" rel="stylesheet"/>
<meta content="on" http-equiv="cleartype"/>
<!-- HTML5 Shiv and Media Query Support -->
<!--[if lt IE 9]>
	<script src="/assets/js/vendor/html5shiv.min.js"></script>
	<script src="/assets/js/vendor/respond.min.js"></script>
<meta http-equiv="Cache-control" content="public" max-age="3600">
<![endif]-->
<!-- Modernizr -->
<script src="/assets/js/vendor/modernizr-2.7.1.custom.min.js"></script>
<link href="//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700%7CPT+Serif:400,700,400italic" rel="stylesheet" type="text/css"/>
<link href="/images/favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/images/favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="/images/favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/images/favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/images/favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/images/favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/images/favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/images/favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="/images/favicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/images/favicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="/images/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/images/favicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="/images/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
</head>
<body class="page">
<div id="wrap">
<!--[if lt IE 9]><div class="browser-upgrade alert alert-info">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div><![endif]-->
<div class="navigation-wrapper">
<div class="top-navigation">
<nav class="nav" id="site-nav" role="navigation">
<a href="/"><img alt="Me" id="logo" src="/images/Logo.png"/></a>
<ul>
<li><a class="navlink" href="/">About</a></li>
<li><a class="navlink" href="/publications/">Publications</a></li>
<li><a class="navlink" href="/projects/">Projects</a></li>
<li><a class="navlink" href="/writing/">Writings</a></li>
<li><a class="navlink" href="/photography/">Photographs</a></li>
<li><a class="navlink" href="/videos/">Videos</a></li>
</ul>
</nav>
</div>
</div>
<div id="main" role="main">
<div class="article-author-side">
<div itemscope="" itemtype="http://schema.org/Person">
<img alt="Andrey Kurenkov bio photo" class="bio-photo" src="/images/Andrey.png"/>
<!--<h3 itemprop="name">Andrey Kurenkov</h3>-->
<p>An eclectic researcher and engineer endlessly interested in AI, art, side-projects, philosophy, and many more things.</p>
<a class="author-social" href="mailto:contact@andreykurenkov.com" target="_blank"><i class="fa fa-fw fa-envelope-square"></i> Email</a>
<a class="author-social" href="http://linkedin.com/in/andreykurenkov" target="_blank"><i class="fa fa-fw fa-linkedin-square"></i> LinkedIn</a>
<a class="author-social" href="http://github.com/andreykurenkov" target="_blank"><i class="fa fa-fw fa-github"></i> Github</a>
<a class="author-social" href="/files/resume.pdf" target="_blank"><i class="fa fa-fw fa-file"></i> Resume</a>
<a class="author-social" href="/writing/feed.xml/" target="_blank"><i class="fa fa-fw fa-rss"></i> RSS</a>
</div>
</div>
<article class="page">
<h1>About</h1>
<div class="article-wrap">
<p>Hi there. I am Andrey Kurenkov, a grad student at Stanford that likes do research about AI and robotics, write code, appreciate art, and ponder about life.</p>
<p>Currently, I am a PhD student with the <a href="http://svl.stanford.edu/home">Stanford Vision and Learning Lab</a> working at the intersection of robotics and computer vision, and am advised by Silvio Savarese.</p>
<p>This site documents my various projects, my writing (technical and not), my photography, and my YouTube videos. I am also quite active on <a href="https://twitter.com/andrey_kurenkov">Twitter</a>.</p>
<blockquote>
<p>“I love writing. I get a kick out of sharing my thoughts with others. The act of transforming ideas into words is an amazingly efficient way to solidify and refine your thoughts about a given topic. “ -Tom Preston (creator of Jekyll), <a href="http://tom.preston-werner.com/2008/11/17/blogging-like-a-hacker.html">Blogging Like a Hacker</a></p>
</blockquote>
<p>If you are so curious, I am more specifically interested in:</p>
<ul>
<li>AI/machine learning (increasingly, the  deep kind)</li>
<li>cinema (Tarkovsky, Aronofsky, Kaufman, Kurosawa, Bergman, Carruth, Kon, Miyazaki, Zvyagintsev, etc)</li>
<li>robotics (particularly, intelligent robotics that can interact with humans)</li>
<li>video games (mostly narrative-driven, all the indie stuff, and the Mass Effect-type stuff, and so on)</li>
<li>energy/climate change (this is what led me to my EE degree)</li>
<li>books (hard science fiction, modernist, some post-modernist, some pre-modernist)</li>
<li>programming (especially embedded programming, but also scripting and hackathon types things as well)</li>
<li>photography (doing it, as you can see on this site, but also appreciating)</li>
</ul>
<p>Or, in visual terms:</p>
<p><img alt="about" src="images/starter-pack.jpg"/></p>
</div><!-- /.article-wrap -->
</article>
</div><!-- /#wrap -->
</div><!-- /#index -->
<footer>
<span>© 2021 Andrey Kurenkov. Powered by <a href="http://jekyllrb.com" rel="nofollow">Jekyll</a> using the <a href="https://github.com/mmistakes/minimal-mistakes" rel="nofollow">Minimal Mistakes</a> and <a href="https://github.com/mmistakes/skinny-bones-jekyll">Skinny Bones</a> themes.</span>
</footer>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="/assets/js/scripts.min.js"></script>
<!--Asynchronous Google Analytics snippet -->
<script>
  var _gaq = _gaq || [];
  var pluginUrl = 
 '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
  _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  _gaq.push(['_setAccount', 'UA-54402578-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
