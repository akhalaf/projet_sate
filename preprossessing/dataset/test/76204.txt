<!DOCTYPE html>
<html>
<head>
<title>AdminBox</title>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<link href="/assets/adminbox_favicon-d1ab71ffa75b69f5abfec7ed79fd5d0594c8442235b9e8dc19ba2a2df06814fd.png" rel="apple-touch-icon shortcut icon" type="image/png"/>
<link data-turbolinks-track="true" href="/assets/application-471db5eb2cdaf09fc3b09733ef30a2b2169247cdc2a7ae95a47e32b261d2dbd8.css" media="all" rel="stylesheet"/>
<script data-turbolinks-track="true" src="/assets/application-79627bc0b8ce4c275daeb35ceb08b09bb54844fa2c87bea62ddc8a386260b7d0.js"></script>
<meta content="authenticity_token" name="csrf-param"/>
<meta content="vl8xqvlBybeL2KuFLi6Op2hyMQ0rGRbWYhmPCDeSw8dmFChusYC9j8Ut/6r7exLQnbXB/C2r+Y44FN3kq7AnwQ==" name="csrf-token"/>
<link href="/assets/favicon/apple-touch-icon-60x60-d7346abd3a5963de91c657514cd85dd05691d48bf6f5347db3aa6362cf592d71.png" rel="apple-touch-icon" sizes="60x60" type="image/png"/>
<link href="/assets/favicon/apple-touch-icon-76x76-5da5379b07b3f2e59c21d4a9b1ce2f15388fcf15f8dc488015bb1058ea556933.png" rel="apple-touch-icon" sizes="76x76" type="image/png"/>
<link href="/assets/favicon/apple-touch-icon-120x120-b35839e9777348606c4f2dfe12534dc5fcdf08cc67bc7014e0d49682cdbf3dc9.png" rel="apple-touch-icon" sizes="120x120" type="image/png"/>
<link href="/assets/favicon/apple-touch-icon-152x152-8f091c646eac21b64ce63365c0d99019ebe0c5a8a1b6789616efec8bbabc8d1b.png" rel="apple-touch-icon" sizes="152x152" type="image/png"/>
<link href="/assets/favicon/apple-touch-icon-180x180-c9e042128e41cf6e743512432aaab38bcda81a2ed339879fe0287e1ff61b2c04.png" rel="apple-touch-icon" sizes="180x180" type="image/png"/>
<link href="/assets/favicon/favicon-16x16-3d19515ac5a3269d6f9ea0953bc865e608647290d05655e266b048efa60918f1.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/assets/favicon/favicon-32x32-890b13a78adce669fe71e334a64627cb39258c267abf38b066501bb8b4d24f18.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/assets/favicon/manifest-e21757a5f3f65b1fe146ac570c693d208647278d06bd474226bb312bf9e2e574.json" rel="manifest"/>
<link color="#5bbad5" href="/assets/favicon/safari-pinned-tab-c1b10a03dac6023c746f0104ff9eeca38a2a7af7936744963999b785a287d3d5.svg" rel="mask-icon"/>
<link href="/assets/favicon/favicon-d87215de15a5ddca32a76fe984210a1e5015d70ba07009e0289c83c29dd25162.ico" rel="shortcut icon"/>
<meta content="/assets/favicon/browserconfig-31d0c34d2f6bbe842cacb52a24808fdbfbb5e43b4053f32358992a11dd5d55f3.xml" name="msapplication-config"/>
<meta content="#ffffff" name="theme-color"/>
</head>
<body data-locale="en">
<div class="container devise"><div class="row"><div class="col-xs-1 col-sm-2 col-md-3"></div><div class="col-xs-10 col-sm-8 col-md-6"><div class="row text-right langbar-with-padding">en <a href="/users/sign_in?locale=nl">nl</a> <a href="/users/sign_in?locale=fr">fr</a> <a href="/users/sign_in?locale=de">de</a> <a href="/users/sign_in?locale=cs">cz</a> <a href="/users/sign_in?locale=sk">sk</a> </div><div class="row"><div class="alert alert-warning alert-dismissible" role="alert"><button class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>Je dient je aan te melden of in te schrijven om door te gaan.</div></div><div class="row"><div class="well"><h2>Log in</h2>
<form accept-charset="UTF-8" action="/users/sign_in?locale=en" class="new_user" id="new_user" method="post"><input name="authenticity_token" type="hidden" value="UCJJe5kc8i47BkakUDxncahw6r5q9lv+hIUHxMSzSlHtRUu3FAxT0JFlsGtajuY8jDzb93AnAVnn6EzLDVQfBw=="/>
<div class="form-group">
<label for="user_login">Login</label>
<input autofocus="autofocus" class="form-control" id="user_login" name="user[login]" placeholder="e-mail or mobile (0032477777777)" type="text"/>
</div>
<div class="form-group">
<label for="user_password">Password</label>
<input autocomplete="off" class="form-control" id="user_password" name="user[password]" type="password"/>
</div>
<input class="btn btn-primary" data-disable-with="Log in" name="commit" type="submit" value="Log in"/>
</form>
<br/>
<p>
<em>
<a href="/users/password/new?locale=en">Forgot your password?</a><br/>
<a href="/users/confirmation/new?locale=en">Didn't receive confirmation instructions?</a><br/>
</em>
</p>
</div></div></div><div class="col-xs-1 col-sm-2 col-md-3"></div></div></div>
</body>
</html>
