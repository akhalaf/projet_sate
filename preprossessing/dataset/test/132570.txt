<!DOCTYPE html>
<html dir="ltr" lang="ru-ru" xml:lang="ru-ru" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="ru-ru" name="language"/>
<title>404 - Ошибка: 404</title>
<link href="/templates/id_apbp/css/error.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/id_apbp/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/id_apbp/favicon.ico" rel="Shortcut Icon" type="image/x-icon"/>
</head>
<body>
<div id="bg-1">
<div id="bg-2">
<div id="error-info">
<div class="pull-left header header-logo">
<a href="/">
<img src="/images/logo-apbp-h80.png"/>
</a>
</div>
<div class="pull-right header-contact">
				Телефон: +7 (495) 221-22-43<br/>
				Адрес: г. Москва, Шмитовский проезд, 12<br/>
				Email: <a href="mailto: info@apbp.ru">info@apbp.ru</a>
</div>
<div class="clear"></div>
<div id="error-info-inner">
<h1>Ошибка: 404</h1>
<p>
</p>
<p>Категория не найдена</p>
<p><strong>Вы не можете посетить текущую страницу по одной из причин:</strong></p>
<ol>
<li><strong>просроченная закладка/избранное</strong></li>
<li>кэш поисковой системы ссылается на <strong>несуществующий документ</strong></li>
<li><strong>неправильный адрес</strong></li>
<li>у вас <strong>нет права доступа</strong> на эту страницу</li>
<li>Запрашиваемый ресурс не найден.</li>
<li>В процессе обработки вашего запроса произошла ошибка.</li>
</ol>
<div class="clear"></div>
<ul class="error-menu">
<li><a href="/">Главная</a></li>
<li>/</li>
<li><a href="/obecty">Объекты</a></li>
<li>/</li>
<li><a href="/rekomendatsii">Рекомендации</a></li>
<li>/</li>
<li><a href="/o-kompanii">О компании</a></li>
<li>/</li>
<li><a href="/nadezhnost">Надежность</a></li>
<li>/</li>
<li><a href="/dopuski">Допуски</a></li>
<li>/</li>
<li><a href="/kontakty">Контакты</a></li>
</ul>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</body>
</html>