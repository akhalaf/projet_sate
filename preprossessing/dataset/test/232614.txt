<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "90227",
      cRay: "611126752f283b22",
      cHash: "70ccd304a7bcfa4",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYml0b2ZlcnRhcy5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "rrQBblF0iUkPPPysX7jRtzm+lo5PyAuouklg1Qsnw+bYP8HQ2B7kfPfo7pdCRDY+7jwQFgdot/4Z6ghDELdYcLPuQmIRdvypRmskQOnNNJ4Cjn+M/zi2DWGe7gaoJEY3iR1oJYcKjzaToUsUVyI5B6j3xj9lNJ8rLJIOben3IrLhtNIJUwynq1JBImE3yatxN/d+Ybf4JO+lUU4X+CX7XVpWw1bx/E3Fx4Y31TT8jmLYV6MzMFBxboUJmOht9oChG6CwDomYO+X7E4gEaGY2EHjXGx8AOku2IKwS/QNA3ZoyedBYmHLYXAmB21E89SVwhSrowRzETJtjcTXs4q5473iJFN6pO/wNedU9011aaov44ISjTi47iiJH39eyB3D4B+VcLz07NlnnPpyA3twdDDDVO0wCVGDZkrfr6pxBI7UgkOt5V6m0B0TQbW+O1CCzXB42KrXwSR+aB68VTw450SpEkSrbql9R6Rl00ry+ASFK0pMGglU2tR8TS3voKBMxSVTzT2ZdbPo/5cUNLUDgIpNZvq2RQk3oBdkXmrdgsZHGxcEeFZKhALsyzT47ZM1orzXM/7cYVzTyjzewUB5Knh+kBCJ4rI3Cf9yekj862zBM/+WkWkm7hOajWKKCTxejCn5Awrk4lk/6z1mdDgi72BEiWWDQTRmr4zgjxFnYVAppumz/ecvNjGBzQGnDdoGlqnYAaicatxac+heQgeDlqG+qhT1UjhTH7mY9HGmhvA/OrdJJ6GM6opSKsShqqU7o",
        t: "MTYxMDU2MjIyNS40NjcwMDA=",
        m: "oNKDtZhm0LT9Q+nPhzI2LxXd8R6UmIge9bjoqU6F540=",
        i1: "1LCp0FN54DSiMMhiIsWO+w==",
        i2: "Lt5T4Yy/vT3jnDFCET4UsA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "FKB2ZGUg9CedeupgD3MK3GHojGIx2y7meYHYV9dg40A=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bitofertas.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=f70e1bd0b65035a4568568a249ba8f38d50e8549-1610562225-0-AU4UqrYLzNzvmRONVAT8D2E0cbGb_VL2wn8tGOETraXU1j79Kf4wsZ76ZohizZeY30n_tdKzwIDya6VcUMjN53ZsACPVPzStA927yrRe-Q7QQ957KG-M2cCZb5MD8Gv0lBi4n2dsmCEyUvSHZ6P5iQRzTD8N4zI0JC_t8KPPdsWWQqMyp5niBOYGiwwwhNXx-qH0F34bFfFLCq3tYsnXexGWmx69w8oEeXkJm8r0jf97xTdP9cRiMZOyYen3N6p-bfAsyvT0yL5IoPdg7zEWY-Pbu5a2Iu6770f4ayio4-q2PUDLupflhQKY3OrurxZij-D9mfLCGHZ12uf2vu9vmxMphc-PPkWB_bzcBFVCPbTyksLsfO5Gp2JcthXGMn27wc4OkB_SoVi7ycBCLB-L3y9Y0bNT3OPm08Ibwi-lN_WTpqPcAm-_07_CZ48JiUMHUIqCcHlH8MTwKmcNJCZLQz7cPPcnTciaD3iTb6RRm2uXzqzi74JSnh9W3tOslKq7wSfx24U_d2TBCxmn5EsWyxc" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="3e893415938cff31d8eed20d235baf46bacbdcb3-1610562225-0-Ae246ObL0iOB32XT+iA4AP2jnkwEM55xyIvuamtSTkpR0nk1eZrctSujHdzdwy2ChImLeNTb/l31TRlLRohZVcM3k6Di2p2auhHeIm5OJ5AYlaC+bVs+EmPU+jlmOSeyS03xD9t+shXBX8lEKSIIWNWIXExiX2ott2f6WUX3OQMxMIaoOwrfA2kjXpzwPVy+li5+QLfCWSZB1de3BDtwMwa2Kt4u7cko0tQ6W/U29fyhf2ved4aG2w/qVycD1+LdUINYD7GlVCnLRiSECmuA3yA5h9VneW2Zqgdlpybg5Jm5y9VR9InN467nDgMf/egaziWiLvkRU68e8ykoOY9aCKZ1M8MUvTCnymAcE00wPRffkOwilCQs3RlzzJypTeUaytOrYv7neN7RgZFbA0sQGy9W0qwsXFyIxzQjuSQN6aBcZq2wE7xjj/VOlPMo2biDCBhPYmPgH+S1BdEIJEdSCsmwRlqGUMOSgTVbFT0C/50hAxk9DtuphRmdhFN/ucFE4XQV2WrokTA73rrh3CUvIbejvWiCOHuac0/+DT7a4qXADZpXNfNH1mURrXZwq9fU6TkrjOrCl4/IXwrdzzfdKPsl2dQKHy95gp8H25KHMcHtzcO8ooaoLpKn+grq0/YGrMcUyoJP1gqHFbxMXZ8zqiCZoU4KWD18EXOpuMFCSG2PZY/7xQVvVycxpfVkS6MiSQYL441m0MlbCn8kQYSBQaDr1TB9nhO6NEqMKCLmOC3SBXYwIRHnoabM3lDls4Zk1p5T5oSJDtbA9QzdXPipYgtizUsGjplVIg2G+snYtnrrhOMVDqI/XZ95d1sEOjEqhj1ZkXH+auXm756DT1YuWzV2itehePBbQnweFZUpBzeoEp75Bn9kYGLoPcZHu7QH1RRKmqJYXT6Di2r4peKgqD3W3L8N0oHlL9u2pS2pAVH71fPHHfgU+YCrlikAHWt0Z9UG1XxDYkA/VdmSwvrUi4G99jE3rFX7HKwWrRuaSNORSeS3PcDtKNNJO424BBH2GPKzrNNcCLziiwqF7Ra11lge/gMJKP6+tMfEwfwjGqyz+n4DQytinRJKf+XKE0U3IK7r1JW3sZQw7teXa/lx3/1lrNpzKr3GokVsdw9YmK/Npnj31NMKpUg2Tk3hvtJoeVzUQaMMNL5Ha6U3R41pCUN9AqI67pajnC1GGBmFj9vhE6cIZWr8bUY1v1bvqtTtpadRsuYeDS5Z1dVdQHUy0SOEBdSR6SoZPlu68p1AB/fOUymmR3c+v9qE8xxYmDfTVcTonjIR7TuijJNH9wxaqUQxsYgnpS2MRR6FqY8GTdIn"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="99456be8417005a1765cca110096c69a"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=611126752f283b22')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<!-- <a href="https://yorke-peninsula.net/wincreamy.php?threadid=78">table</a> -->
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">611126752f283b22</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
