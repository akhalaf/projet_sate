<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Page introuvable — Erreur 404</title>
</head>
<body>
<h1>Page introuvable — Erreur 404</h1>
<p>La page demandée n'a pas été trouvée sur le serveur.</p>
<p><a href="/">Retour à l'accueil du site</a></p>
</body>
</html>