<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script type="text/javascript">
 /* <![CDATA[ */
 if(top.frames.length > 0) top.location.href=self.location;
 /* ]]> */
</script>
<link href="/sites/default/files/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="Red social accesible para personas con discapacidad visual u otras afecciones funcionales. Orientada a la usabilidad en la que puede participar cualquier persona, una red para todos, de verdad." name="description"/>
<meta content="red social,ciegos,invidentes,accesible" name="keywords"/>
<link href="https://www.blindworlds.com/" rel="canonical"/>
<meta content="1 day" name="revisit-after"/>
<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" name="viewport"/>
<title>BlindWorlds | La red social accesible, para todos, de verdad</title>
<link href="/sites/default/files/ctools/css/32b872f83f634db19c34d228296318ef.css?l" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/default/files/css/css_c4f52af1ed3f229a527880e22a844ce5.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/themes/blindworldscom/front-page.css?l" media="all" rel="stylesheet" type="text/css"/>
<script src="/sites/default/files/js/js_ddbfa82481dedbcbd2a35f8c498285bd.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "audiorecorderfield_path": "sites/all/modules/audiorecorderfield", "dhtmlMenu": { "slide": "slide", "siblings": "siblings", "relativity": "relativity", "children": "children", "doubleclick": "doubleclick", "clone": 0 }, "lightbox2": { "rtl": "0", "file_path": "/(\\w\\w/)sites/default/files", "default_image": "/sites/all/modules/lightbox2/images/brokenimage.jpg", "border_size": 10, "font_color": "000", "box_color": "fff", "top_position": "", "overlay_opacity": "0.8", "overlay_color": "000", "disable_close_click": 1, "resize_sequence": 0, "resize_speed": 400, "fade_in_speed": 400, "slide_down_speed": 600, "use_alt_layout": 0, "disable_resize": 0, "disable_zoom": 0, "force_show_nav": 0, "show_caption": 1, "loop_items": 1, "node_link_text": "View Image Details", "node_link_target": 0, "image_count": "Image !current of !total", "video_count": "Video !current of !total", "page_count": "Page !current of !total", "lite_press_x_close": "press \x3ca href=\"#\" onclick=\"hideLightbox(); return FALSE;\"\x3e\x3ckbd\x3ex\x3c/kbd\x3e\x3c/a\x3e to close", "download_link_text": "", "enable_login": false, "enable_contact": false, "keys_close": "c x 27", "keys_previous": "p 37", "keys_next": "n 39", "keys_zoom": "z", "keys_play_pause": "32", "display_image_size": "original", "image_node_sizes": "()", "trigger_lightbox_classes": "", "trigger_lightbox_group_classes": "", "trigger_slideshow_classes": "", "trigger_lightframe_classes": "", "trigger_lightframe_group_classes": "", "custom_class_handler": 0, "custom_trigger_classes": "", "disable_for_gallery_lists": true, "disable_for_acidfree_gallery_lists": true, "enable_acidfree_videos": true, "slideshow_interval": 5000, "slideshow_automatic_start": true, "slideshow_automatic_exit": true, "show_play_pause": true, "pause_on_next_click": false, "pause_on_previous_click": true, "loop_slides": false, "iframe_width": 600, "iframe_height": 400, "iframe_border": 1, "enable_video": 0 }, "shortcuts": [ { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "73", "func_name": "shortcut_call_internal_path", "param": "inicio" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "77", "func_name": "shortcut_call_internal_path", "param": "muro" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "65", "func_name": "shortcut_call_internal_path", "param": "amigos" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "67", "func_name": "shortcut_call_internal_path", "param": "user/me" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "66", "func_name": "shortcut_call_internal_path", "param": "buscador" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "80", "func_name": "shortcut_call_internal_path", "param": "publicar" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "83", "func_name": "shortcut_call_internal_path", "param": "logout" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "68", "func_name": "shortcut_call_internal_path", "param": "accesibilidad" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "69", "func_name": "shortcut_call_internal_path", "param": "mensajes" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "70", "func_name": "shortcut_call_internal_path", "param": "favoritos" }, { "alt": "true", "ctrl": "false", "shift": "true", "win": "false", "char_code": "79", "func_name": "sitecustoms_goto_menu", "param": "sitecustoms_goto_menu" } ], "spamspan": { "m": "spamspan", "u": "u", "d": "d", "h": "h", "t": "t" }, "extlink": { "extTarget": "_blank", "extClass": "ext", "extSubdomains": 1, "extExclude": "", "extInclude": "", "extCssExclude": "", "extCssExplicit": "", "extAlert": 0, "extAlertText": "This link will take you to an external web site. We are not responsible for their content.", "mailtoClass": 0 }, "facebook_status": { "autofocus": false, "noautoclear": false, "lang_prefix": "", "maxlength": "140", "refreshLink": false, "ttype": "textfield" }, "CToolsUrlIsAjaxTrusted": { "/user?destination=inicio": true, "/": true } });
//--><!]]>
</script>
<script type="text/javascript"> </script>
</head>
<body class="front not-logged-in page-inicio no-sidebars" id="body">
<div id="page">
<div class="column sidebar" id="sidebar-left">
</div>
<div class="column" id="main">
<div id="header">
<img alt="Blindworlds" src="/sites/default/files/blindworlds.png"/>
</div>
<div id="content">
<h1 class="title" id="page-title">La red social accesible, para todos, de verdad</h1>
<div class="clear-block" id="content-content">
<div class="panel-flexible panels-flexible-portada clear-block" id="homepage">
<div class="panel-flexible-inside panels-flexible-portada-inside">
<div class="panels-flexible-row panels-flexible-row-portada-main-row panels-flexible-row-first clear-block row-intro">
<div class="inside panels-flexible-row-inside panels-flexible-row-portada-main-row-inside panels-flexible-row-inside-first clear-block">
<div class="panels-flexible-region panels-flexible-region-portada-center panels-flexible-region-first panels-flexible-region-last ">
<div class="inside panels-flexible-region-inside panels-flexible-region-portada-center-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-2">
<div class="pane-content">
<p>Para empezar, puedes establecer la presentación en las <a accesskey="d" href="/accesibilidad">opciones de accesibili<em>d</em>ad</a>.</p> </div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-portada-1 clear-block row-active">
<div class="inside panels-flexible-row-inside panels-flexible-row-portada-1-inside clear-block">
<div class="panels-flexible-region panels-flexible-region-portada-middle panels-flexible-region-first panels-flexible-region-last ">
<div class="inside panels-flexible-region-inside panels-flexible-region-portada-middle-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-block pane-user-0">
<h2 class="pane-title">Acceso usuarios</h2>
<div class="pane-content">
<form accept-charset="UTF-8" action="/user?destination=inicio" id="user-login-form" method="post">
<div><div class="form-item" id="edit-name-wrapper">
<label for="edit-name">Seudónimo o correo electrónico: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
<input class="form-text required" id="edit-name" maxlength="60" name="name" size="15" type="text" value=""/>
</div>
<div class="form-item" id="edit-pass-wrapper">
<label for="edit-pass">Contraseña: <span class="form-required" title="Este campo es obligatorio.">*</span></label>
<input class="form-text required" id="edit-pass" maxlength="60" name="pass" size="15" type="password"/>
</div>
<input class="form-submit" id="edit-submit" name="op" type="submit" value="Entrar"/>
<div class="item-list"><ul><li class="first last"><a href="/usuario/recuperar-clave" title="Solicitar una contraseña nueva por correo electrónico.">Recuperar contraseña</a></li>
</ul></div><input id="form-Xzij61RF-I_kvdKrsi8AGkOk_croG9gGk8Q33JWn8b0" name="form_build_id" type="hidden" value="form-Xzij61RF-I_kvdKrsi8AGkOk_croG9gGk8Q33JWn8b0"/>
<input id="edit-user-login-block" name="form_id" type="hidden" value="user_login_block"/>
</div></form>
</div>
</div>
<div class="panel-region-separator"></div><div class="panel-pane pane-custom pane-4">
<div class="pane-content">
<a href="/usuario/alta/persona">crear una cuenta</a> </div>
</div>
<div class="panel-region-separator"></div><div class="panel-pane pane-views-panes pane-publicaciones-panel-pane-2">
<h2 class="pane-title">Publicaciones recientes</h2>
<div class="pane-content">
<div class="view view-publicaciones view-id-publicaciones view-display-id-panel_pane_2 view-dom-id-1">
<div class="view-content">
<div class="item-list">
<ul>
<li class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field-nothing">
<span class="field-content"><a href="/publicacion/133552">Texto del día martes 12 de enero del año 2021.</a>, publicado por <a href="https://www.blindworlds.com/usuario/ver/diav" title="Ver perfil de usuario.">Daniel Ayala, El testigo</a>.</span>
</div>
</li>
<li class="views-row views-row-2 views-row-even">
<div class="views-field-nothing">
<span class="field-content"><a href="/publicacion/133551">"Sin prejuicios"  autora: Remedios Calderón Guirado</a>, publicado por <a href="https://www.blindworlds.com/usuario/ver/remerecalgui" title="Ver perfil de usuario.">Remedios Calderón</a>.</span>
</div>
</li>
<li class="views-row views-row-3 views-row-odd views-row-last">
<div class="views-field-nothing">
<span class="field-content"><a href="/publicacion/133550">muere una enfermera de 41 años a los 2 dias despues de ponerse la vacuna contra el coronavirus de Pfizer en #Portugal</a>, publicado por <a href="https://www.blindworlds.com/usuario/ver/berdasco" title="Ver perfil de usuario.">Urria Gorria</a>.</span>
</div>
</li>
</ul>
</div> </div>
</div> </div>
</div>
<div class="panel-region-separator"></div><div class="panel-pane pane-views-panes pane-buscador-personas-panel-pane-3">
<h2 class="pane-title">Damos la bienvenida a:</h2>
<div class="pane-content">
<div class="view view-buscador-personas view-id-buscador_personas view-display-id-panel_pane_3 view-dom-id-2">
<div class="view-content">
<table class="views-view-grid col-3">
<tbody>
<tr class="row-1 row-first">
<td class="col-1 col-first">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/rodriguezsanchezmiguel3626" title="Ver perfil de usuario.">Miguel Ángel Rodríguez Sánchez</a></span>
</div>
</td>
<td class="col-2">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/reynavalente" title="Ver perfil de usuario.">reyna valente</a></span>
</div>
</td>
<td class="col-3 col-last">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/aldanamarroquin44" title="Ver perfil de usuario.">paola aldana</a></span>
</div>
</td>
</tr>
<tr class="row-2">
<td class="col-1 col-first">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/monavalenciabrahian" title="Ver perfil de usuario.">brahian</a></span>
</div>
</td>
<td class="col-2">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/thiagoariel" title="Ver perfil de usuario.">Alexis soto</a></span>
</div>
</td>
<td class="col-3 col-last">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/mariaeugenialarreina" title="Ver perfil de usuario.">María Eugenia</a></span>
</div>
</td>
</tr>
<tr class="row-3 row-last">
<td class="col-1 col-first">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/yuse" title="Ver perfil de usuario.">Yusmany Risquet gil</a></span>
</div>
</td>
<td class="col-2">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/musician-shadow" title="Ver perfil de usuario.">Rosalba Vázquez García</a></span>
</div>
</td>
<td class="col-3 col-last">
<div class="views-field-name">
<span class="field-content"><a href="https://www.blindworlds.com/usuario/ver/raulmunozmetalmec" title="Ver perfil de usuario.">Raul Muñoz Ramirez</a></span>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div class="view-footer">
<p><a href="/directorio/personas">buscar otras personas</a></p> </div>
</div> </div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-portada-2 clear-block row-additional">
<div class="inside panels-flexible-row-inside panels-flexible-row-portada-2-inside clear-block">
<div class="panels-flexible-region panels-flexible-region-portada-bottom panels-flexible-region-first panels-flexible-region-last ">
<div class="inside panels-flexible-region-inside panels-flexible-region-portada-bottom-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-1">
<div class="pane-content">
<p><a href="/contenido/que-es-blindworlds">conocer blindworlds</a></p> </div>
</div>
</div>
</div>
</div>
</div>
<div class="panels-flexible-row panels-flexible-row-portada-3 panels-flexible-row-last clear-block row-footer">
<div class="inside panels-flexible-row-inside panels-flexible-row-portada-3-inside panels-flexible-row-inside-last clear-block">
<div class="panels-flexible-region panels-flexible-region-portada-footer panels-flexible-region-first panels-flexible-region-last ">
<div class="inside panels-flexible-region-inside panels-flexible-region-portada-footer-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-3">
<div class="pane-content">
<hr/><p style="margin:0; padding:0">Y también...</p><ul style="margin:0; padding:0"><li><a href="/directorio/entidades">directorio de entidades</a></li><li><a accesskey="d" href="/accesibilidad">Accesibili<em>d</em>ad</a></li><li><a href="/contenido/condiciones-de-uso">normas de uso</a></li><li><a href="/node/37">contacto</a></li></ul> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div> <!-- /content-content -->
</div> <!-- /content -->
</div>
<script src="/sites/default/files/js/js_1dba7a4d71ad8c96e7d7eee0f8191c0c.js" type="text/javascript"></script>
</div> <!-- /page -->
</body>
</html>
