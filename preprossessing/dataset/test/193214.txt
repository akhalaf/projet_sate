<html><body><p>ï»¿<!DOCTYPE html>

<!--[if IE 7 ]><html class="ie ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<!--<![endif]-->
</p>
<!-- un-comment and delete 2nd meta below to disable zoom (not cool)
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"> -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script src="jsx/jquery.js" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Barcode Software - Barcode Resource</title>
<meta content="en-us" http-equiv="Content-Language"/>
<meta content="Barcode Resource develops and markets barcode software, barcode fonts, barcode software sdk, label printing app and MICR fonts that adheres strictly to industry specifications." name="description"/>
<meta content="barcode software, barcode font, barcode software sdk, barcode fonts, barcode, code 128 barcode, ean13 barcode, gs1-128 barcode, micr e13b fonts" name="keywords"/>
<!-- RSS -->
<link href="#" rel="alternate" title="#" type="application/rss+xml"/>
<!-- Favicon -->
<link href="images/favicon.ico" rel="shortcut icon"/>
<!-- Google Font -->
<link href="//fonts.googleapis.com/css?family=Open+Sans|Lato" rel="stylesheet" type="text/css"/>
<!-- Primary CSS -->
<link href="style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="cssx/_mobile.css" media="all" rel="stylesheet" type="text/css"/>
<!-- Color Scheme CSS -->
<link href="cssx/karma-dark.css" media="all" rel="stylesheet" type="text/css"/>
<link href="cssx/secondary-dark.css" media="all" rel="stylesheet" type="text/css"/>
<link href="cssx/_font-awesome.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE 9]>
<style media="screen">
#footer,
.header-holder
{
behavior: url(jsx/PIE/PIE.htc);
}
</style>
<![endif]-->
<!--[if lte IE 8]>
<script type='text/javascript' src='jsx/html5shiv.js'></script>
<style media="screen">
/* uncomment for IE8 rounded corners
#menu-main-nav .drop ul a,
#menu-main-nav .drop,
#menu-main-nav ul.sub-menu,
#menu-main-nav .drop .c, 
#menu-main-nav li.parent, */
#footer,
.header-holder,
#horizontal_nav ul li,
#horizontal_nav ul a,
#tt-gallery-nav li,
#tt-gallery-nav a,
ul.tabset li,
ul.tabset a,
.karma-pages a,
.karma-pages span,
.wp-pagenavi a,
.wp-pagenavi span,
.post_date,
.post_comments,
.ka_button,
.flex-control-paging li a,
.colored_box,
.tools,
.karma_notify
.opener,
.callout_button,
.testimonials {
behavior: url(jsx/PIE/PIE.htc);
}
</style>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" href="cssx/_internet_explorer.css" media="screen"/>
<![endif]-->
<div class="content-style-default" id="tt-wide-layout">
<div id="wrapper">
<header id="header" role="banner">
<div class="top-block">
<div class="top-holder">
<!-- ***************** - Top Toolbar Left Side - ***************** -->
<div class="toolbar-left">
<ul>
<li><a href="//www.barcoderesource.com">Home</a></li>
<li><a href="contact.shtml">About &amp; Contact</a></li><li><a href="privacy_policy.shtml">Privacy Policy</a></li>
</ul>
</div><!-- END toolbar-left -->
<!-- ***************** - Top Toolbar Right Side - ***************** -->
<div class="toolbar-right">
<ul class="social_icons tt_vector_social_icons tt_no_social_title tt_image_social_icons">
<li><a class="twitter" href="ConnectCodeTwitter.shtml" title="Twitter">Twitter</a></li>
<li><a class="email" href="mailto:info@barcoderesource.com" title="Email">Email</a></li>
</ul>
</div><!-- END toolbar-right -->
</div><!-- END top-holder -->
</div><!-- END top-block -->
<div class="header-holder tt-logo-center">
<div class="header-overlay">
<div class="header-area">
<a class="logo" href="//www.barcoderesource.com"><img alt="BarcodeResource" class="tt-retina-logo" height="75" src="images/logo.png" width="301"/></a>
<!-- ***************** - Main Menu - ***************** -->
<nav role="navigation">
<ul id="menu-main-nav">
<li>
<a href="barcodefont.shtml"><span><strong>Product</strong><span class="navi-description">Barcode Fonts</span></span></a>
<ul class="sub-menu">
<li>
<a href="barcodefont.shtml"><span>Barcode Fonts</span></a>
<ul class="sub-menu">
<li><a href="code39_barcodefont.html"><span>Code 39 Barcode</span></a></li>
<li><a href="code128_barcodefont.html"><span>Code 128 Barcode</span></a></li>
<li><a href="uccean_barcodefont.html"><span>GS1 128 / UCCEAN Barcode</span></a></li>
<li><a href="itf14_barcodefont.html"><span>ITF14 Barcode</span></a></li>
<li><a href="i2of5_barcodefont.html"><span>I2of5 Barcode</span></a></li>
<li><a href="upcean_barcodefont.html"><span>UPCA / EAN13 Barcode</span></a></li>
<li><a href="gs1databar14_barcodefont.shtml"><span>GS1 Databar 14 Barcode</span></a></li>
<li><a href="download.html"><span>Download ConnectCode</span></a></li><br/>
</ul>
</li>
<li>
<a href="barcodefont.shtml"><span>Using the Fonts</span></a>
<ul class="sub-menu">
<li><a href="freebarcodesoftware.shtml"><span>With the Font Encoder</span></a></li>
<li><a href="excelbarcodefont.shtml"><span>With Excel</span></a></li>
<li><a href="accessbarcodefont.shtml"><span>With Access</span></a></li>
<li><a href="officebarcodefont.shtml"><span>With Word</span></a></li>
<li><a href="crystalreportsbarcode.shtml"><span>With Crystal Reports</span></a></li>
<li><a href="dll.shtml"><span>C#, VB, C++, WPF Integration</span></a></li><br/>
</ul>
</li>
<li>
<a href="pdf417barcode.shtml"><span>2D Barcode Fonts</span></a>
<ul class="sub-menu">
<li><a href="pdf417barcode.shtml"><span>PDF417 Barcode</span></a></li>
<li><a href="qrcodebarcode.shtml"><span>QR Code Barcode</span></a></li>
<li><a href="datamatrixbarcode.shtml"><span>DataMatrix Barcode</span></a></li><br/>
</ul>
</li>
<li>
<a href="micrfont.shtml"><span>MICR Fonts</span></a>
<ul class="sub-menu">
<li><a href="micrfont.shtml"><span>MICR E13B Font</span></a></li>
<li><a href="micre13bgauge.shtml"><span>MICR E13B Gauge</span></a></li>
<li><a href="micr_cmc7_font.shtml"><span>MICR CMC7 Font</span></a></li>
<li><a href="downloadmicrfont.shtml"><span>Download MICR</span></a></li><br/>
</ul>
</li>
<li>
<a href="dotNetBarcodeSDK.shtml"><span>SDK Products</span></a>
<ul class="sub-menu">
<li><a href="dotNetBarcodeSDK.shtml"><span>.Net Barcode SDK</span></a></li>
<li><a href="downloadSDK.shtml"><span>Download SDK</span></a></li><br/>
</ul>
</li>
<li>
<a href="barcodesoftware.shtml"><span>Images Products</span></a>
<ul class="sub-menu">
<li><a href="barcodesoftware.shtml"><span>Barcode Imager</span></a></li>
<li><a href="downloadimager.shtml"><span>Download Imager</span></a></li><br/>
</ul>
</li>
<li>
<a href="barcodelabelapp.shtml"><span>Windows Store</span></a>
<ul class="sub-menu">
<li><a href="barcodelabelapp.shtml"><span>Barcode &amp; Label app</span></a></li>
<li><a href="posterflyerapp.shtml"><span>Poster &amp; Flyer app</span></a></li>
<li><a href="scrapbookphotocollageapp.shtml"><span>Scrapbook &amp; Photo Collage</span></a></li>
<li><a href="envelopeaddressprintapp.shtml"><span>Envelope &amp; Address Print</span></a></li>
<li><a href="greetingcardsapp.shtml"><span>Greeting Cards</span></a></li>
</ul>
</li>
<li><a href="labelsoftware.shtml"><span>Label Software</span></a></li><br/>
</ul>
</li>
<li><a href="buy.html"><span><strong>Buy</strong><span class="navi-description">Purchase</span></span></a></li>
<li><a href="press.shtml"><span><strong>Press</strong><span class="navi-description">Press Releases</span></span></a></li>
<li><a href="contact.shtml"><span><strong>About</strong><span class="navi-description">About &amp; Contact</span></span></a></li>
</ul>
</nav>
</div><!-- END header-area -->
</div><!-- END header-overlay -->
</div><!-- END header-holder -->
</header><!-- END header -->
<!-- ***************** - Main Content Area - ***************** -->
<div id="main">
<div class="main-area">
<!-- ////////////////////////////////////////////////////////// -->
<!-- ***************** - Content Start Here - ***************** -->
<!-- ////////////////////////////////////////////////////////// -->
<!-- ***************** - Breadcrumbs Start Here - ***************** -->
<nav id="sub_nav" role="navigation">
<ul class="sub-menu">
<li>  <strong>HOME</strong></li>
<li class="current_page_item"><a href="index.shtml"><span>Barcode Software</span></a></li><br/>
<li>  <strong>BARCODE FONTS</strong></li>
<li><a href="barcodefont.shtml"><span>Barcode Fonts</span></a></li>
<li><a href="code39_barcodefont.html"><span>Code 39 Barcode</span></a></li>
<li><a href="code128_barcodefont.html"><span>Code 128 Barcode</span></a></li>
<li><a href="uccean_barcodefont.html"><span>GS1 128 / UCCEAN Barcode</span></a></li>
<li><a href="itf14_barcodefont.html"><span>ITF14 Barcode</span></a></li>
<li><a href="i2of5_barcodefont.html"><span>I2of5 Barcode</span></a></li>
<li><a href="upcean_barcodefont.html"><span>UPCA / EAN13 Barcode</span></a></li>
<li><a href="gs1databar14_barcodefont.shtml"><span>GS1 Databar 14 Barcode</span></a></li>
<li><a href="download.html"><span>Download ConnectCode</span></a></li><br/>
<li><strong>Using the Fonts</strong></li>
<li><a href="freebarcodesoftware.shtml"><span>With the Font Encoder</span></a></li>
<li><a href="excelbarcodefont.shtml"><span>With Excel</span></a></li>
<li><a href="accessbarcodefont.shtml"><span>With Access</span></a></li>
<li><a href="officebarcodefont.shtml"><span>With Word</span></a></li>
<li><a href="crystalreportsbarcode.shtml"><span>With Crystal Reports</span></a></li>
<li><a href="dll.shtml"><span>C#, VB, C++, WPF Integration</span></a></li><br/>
<li><strong>2D BARCODE FONTS</strong></li>
<li><a href="pdf417barcode.shtml"><span>PDF417 Barcode</span></a></li>
<li><a href="qrcodebarcode.shtml"><span>QR Code Barcode</span></a></li>
<li><a href="datamatrixbarcode.shtml"><span>DataMatrix Barcode</span></a></li><br/>
<li><strong>MICR FONTS</strong></li>
<li><a href="micrfont.shtml"><span>MICR E13B Font</span></a></li>
<li><a href="micre13bgauge.shtml"><span>MICR E13B Gauge</span></a></li>
<li><a href="micr_cmc7_font.shtml"><span>MICR CMC7 Font</span></a></li>
<li><a href="downloadmicrfont.shtml"><span>Download MICR</span></a></li><br/>
<li><strong>SDK PRODUCTS</strong></li>
<li><a href="dotNetBarcodeSDK.shtml"><span>.Net Barcode SDK</span></a></li>
<li><a href="downloadSDK.shtml"><span>Download SDK</span></a></li><br/>
<li><strong>IMAGE PRODUCTS</strong></li>
<li><a href="barcodesoftware.shtml"><span>Barcode Imager</span></a></li>
<li><a href="downloadimager.shtml"><span>Download Imager</span></a></li><br/>
<li><strong>WINDOWS STORE</strong></li>
<li><a href="barcodelabelapp.shtml"><span>Barcode &amp; Label app</span></a></li>
<li><a href="posterflyerapp.shtml"><span>Poster &amp; Flyer app</span></a></li>
<li><a href="scrapbookphotocollageapp.shtml"><span>Scrapbook &amp; Photo Collage</span></a></li>
<li><a href="envelopeaddressprintapp.shtml"><span>Envelope &amp; Address Print</span></a></li><li><a href="greetingcardsapp.shtml"><span>Greeting Cards app</span></a><br/></li>
<li><strong>Label Software</strong></li>
<li><a href="labelsoftware.shtml"><span>Label Software</span></a></li><br/>
<li><strong>BARCODE RESOURCES</strong></li>
<li><a href="barcodesymbology.shtml"><span>About Barcodes</span></a></li>
<li><a href="rfidarticles.shtml"><span>RFID</span></a></li>
<li><a href="thermalprinters.shtml"><span>Thermal  Printers</span></a></li>
<li><a href="barcodescanner.shtml"><span>Barcode  Scanners</span></a></li>
<li><a href="ocr.shtml"><span>OCR &amp; OCR Fonts</span></a></li>
<li><a href="micr.shtml"><span>MICR &amp; MICR Fonts</span></a></li>
<li><a href="barcodemagazines.shtml"><span>Other Web Resources</span></a></li>
<br/>
</ul>
</nav><!-- END sub_nav -->
<main class="content-left-nav" id="content" role="main">
<br/>
														Are you looking to print high quality <b>EAN 13, Code 39, I2of5 
															, Code 128, UCCEAN (GS1-128), UPCA, I2of5, GS1 Databar 14, PDF417, QR Code or DataMatrix barcodes</b>
														for your business?														If so, you have come to the right place. We offer a complete and <b>professional 
															barcode software and fonts</b> package, ConnectCode, to help you with your design and 
														printing.<!-- quickly and easily.-->
<br/>
<br/>
														ConnectCode can be used independently with our software Encoder or with other 
														softwares such as your text editor, graphics software, Excel, Access, Word, Crystal Reports, Reporting Services, Oracle, App for Office and Visual Studio projects. The fonts are field tested and has proven to be extremely flexible in meeting the changing demands of your 
														business.
														<br/>
<br/>
														Our products are designed with simplicity in mind and does not require you to 
														invest significant amount of time and effort. We also hope to provide you with 
														good support by treating your satisfaction as our paramount goal.
														<br/>
<br/><br/>
<h3>Barcode Software and Fonts</h3>
<div class="one_half">
<br/>
<img alt="barcodefonts" src="images/barcodefonts.png"/>
</div>
<div class="one_half_last">
<br/>
<h4>Product Details</h4>
<ul class="list">
<li>
<b><a href="barcodefont.shtml">Barcode Fonts</a></b> - New v11.5
																		</li><li>
<b><a href="download.html">Download</a></b>
<!--
																		<li>
																			<A href="buy.html">Purchase</A>
																		</li>
-->
</li></ul>
<h4>Barcode supported</h4>
<ul class="list">
<li>
																		Code 39/93</li>
<li>
																		Code 128</li>
<li>
																		UCCEAN/GS1-128</li>
<li>
																		UPCA/EAN13</li>
<li>
																		ISBN/ISSN</li>
<li>
																		Industrial 2 of 5</li>
<li>
																		I2of5/ITF14</li>
<li>
																		Modified Plessy</li>
<li>
																		GS1 Databar (14/Stacked/Expanded)</li>
<li>
																			POSTNET</li>
<li>
																			Codabar
																			and more....
																		</li>
</ul>
</div>
<br class="clear"/>
<div class="tt-testimonial-wrapper">
<div class="testimonials flexslider">
<ul class="slides">
<li>
<blockquote><p>
I have used several bar code font applications over the past 20 years and found that ConnectCode is the cleanest solution I have found. It works perfectly with Crystal Reports and doesn't have any issues embedding the bar code fonts into a PDF.
<cite>–T.M. (BrassCraft)</cite></p></blockquote>
</li>
<li>
<blockquote><p>
ResMed is a signatory to GS1 so the support of these barcode formats is very important. I showed this to one of our IT staff and he commented that it was better than anything they had.
<cite>–Dr P.S. (ResMed Ltd)</cite></p></blockquote>
</li>
<li>
<blockquote><p>
We chose to use your product because it installed flawlessly into excel and we were able to document the creation process so that staff can produce the barcodes in 4 simple steps. 
<cite>–Edgar (CTO - Record Guardian Inc.)</cite></p></blockquote>
</li>
<li>
<blockquote><p>
The font allows for the barcode to be consistantly sized and placed regardless of what data the initial page of the document uses to generate its code. 
<cite>–Glyn Rowling (Amethyst Mailing)</cite></p></blockquote>
</li>
</ul>
</div>
</div>
<!--
 <div class="testimonials">
 <blockquote><p>
I have used several bar code font applications over the past 20 years and found that ConnectCode is the cleanest solution I have found. It works perfectly with Crystal Reports and doesn't have any issues embedding the bar code fonts into a PDF.
<cite>&ndash;T.M. (BrassCraft)</cite></p></blockquote>
 
 <blockquote><p>
ResMed is a signatory to GS1 so the support of these barcode formats is very important. I showed this to one of our IT staff and he commented that it was better than anything they had.
<cite>&ndash;Dr P.S. (ResMed Ltd)</cite></p></blockquote>

 <blockquote><p>
We chose to use your product because it installed flawlessly into excel and we were able to document the creation process so that staff can produce the barcodes in 4 simple steps. 
<cite>&ndash;Edgar (CTO - Record Guardian Inc.)</cite></p></blockquote>


 <blockquote><p>
The font allows for the barcode to be consistantly sized and placed regardless of what data the initial page of the document uses to generate its code. 
<cite>&ndash;Glyn Rowling (Amethyst Mailing)</cite></p></blockquote>

 </div>
-->
<!-- END testimonials -->
<br/><br/>
<center>
<b>
v11.5 - New Version supports .NET Core 3.0, T-SQL, Blazor, Angular, Web Open Font Format 2 (WOFF2), PowerBuilder, COM, Crystal Reports, .NET Standard 2.0, React, WebAssembly, Polymer Web Components, Barcode &amp; Label app, Oracle PL/SQL, Report Builder, TypeScript, Reporting Services, SQL Server Data Tools (SSDT), Javascript, Excel, Word, Office 365 and Linux.
</b><br/><br/>
</center>


The versatility of this fonts package makes it easy for you to enable barcodes in office applications, third party software, databases and reporting software that support text display.

														ConnectCode comes bundled with an advanced software Encoder that helps you 
															validate data input, generate check digits and add start/stop characters. 

 A special feature creates images out of the barcode fonts, while giving you flexibility in setting the resolution, dimensions and boundaries of the resulting bitmaps. 


There are also Word/Excel Macros, Oracle and Crystal Reports formulas included to carry out the tasks of the Encoder mentioned above. Sample source code also provides 
															reference implementation of the Encoder if you are thinking of bundling our 
															fonts with your application.
															<br/>
<br/>
															Our products comes in various licenses so that you can choose to 
															use them in a single computer, in unlimited computers in your organization or 
															for bundling in your own applications. Our products come with free 
															technical support from our experienced staff.
															<br/>
<br/>
<a href="freebarcodefont.shtml">Free barcode fonts</a> (v5.0) are also available for 
															download on our website.
<br/><br/>
<h3>2D Barcode Software and Fonts</h3>
<br/>
<a href="pdf417barcode.shtml"><b>PDF417</b></a>, <a href="qrcodebarcode.shtml"><b>QR Code</b></a> and <a href="datamatrixbarcode.shtml"><b>DataMatrix</b></a>
<br/><br/>PDF417 is one of the earliest and most widely used 2-dimensional barcode. It is a stacked barcode composed of rows of linear barcode. Being a 2-dimensional barcode allows it to carry more information than the 1-dimensional barcodes. 
<br/><br/>
QR Code (Quick Response) is a 2-dimensional barcode consisting of black square patterns on a white background. The barcode is capable of storing more information than a conventional barcode. It is developed by Denso-Wave and is one of the more popular 2-dimensional barcodes.
<br/><br/>
DataMatrix is a two-dimensional matrix symbology made up of square modules arranged in a square or rectangle shaped pattern. This barcode is capable of packing large amount of data and has the capability of recovering the original data encoded even when it is partially damaged through an error correction technique. 										
<br/>
<h3>.NET Barcode SDK</h3>
<br/>
<div class="one_half">
<img alt="barcodesdk" border="1" src="images/barcode_software.png"/>
</div>
<div class="one_half_last">
<h4>Product Details</h4>
<ul class="list">
<li>
<b><a href="dotNetBarcodeSDK.shtml">.Net Barcode SDK</a> - New v3.6</b></li>
<li>
<b><a href="downloadSDK.shtml">Download</a></b></li>
</ul>
</div>
<br class="clear"/>

ConnectCode SDK is an advance and modern .Net compliant barcode generation Software Development Kit (SDK). It supports all commonly used linear barcodes in the industry including the modern GS1 Databar and is one of the most flexible barcode SDK available.
<br/><br/>
This Barcode SDK is uniquely designed to achieve maximum scalability and flexibility for your project. Due to this unique design, it is so powerful that you can use it to create barcodes for almost any scenarios in the Windows Operating System. 


<br/>
<h3>MICR E13B and CMC7 Fonts</h3>
<br/>
<div class="one_half">
<img alt="micr font" border="1" src="images/micre13bfont.png"/>
</div>
<div class="one_half_last">
<h4>Product Details</h4>
<ul class="list">
<li>
<b><a href="micrfont.shtml">MICR E13B Font</a></b>
</li>
<li>
<b><a href="micr_cmc7_font.shtml">MICR CMC7 Font</a></b> </li>
<li><b>
<a href="downloadmicrfont.shtml">Download</a></b>
</li>
</ul>
</div>
<br class="clear"/>
<b>MICR E13B Font</b> <br/><br/>This is a Magnetic Ink Character Recognition font based on the E13-B (ISO 1004) industry standard for check processing. E13-B is widely accepted in the US, Canada, Australia and many other countries. The character set of this standard comprises of ten numbers (0..9) and four special symbols(Amount, Domestic, BSB and Dash).  The font package includes TrueType fonts, Calibration fonts, PostScript fonts and OpenType fonts.

														<br/><br/>
<b>MICR CMC7 Font</b> <br/><br/> This is a Magnetic Ink Character Recognition font based on the CMC-7 (ISO 1004) industry standard. CMC-7 is a widely accepted standard throughout Europe, South America and many other countries.  The character set of this standard comprises of ten numbers (0..9), five special symbols and twenty-six letters.
														<br/><br/>
<a href="freesecurityfont.shtml">Free Security Fonts</a> <br/><br/> This is a free security font used to print text and amounts on secure documents e.g. a bank check. The secure documents require capabilities to prevent forgery and easy alteration.<br/>
<br/>
<h3>Barcode &amp; Label App - Windows Store</h3>
<br/>
<b><a href="barcodelabelapp.shtml">Barcode &amp; Label</a></b> is a label design and barcode printing app for Windows. It subscribes to the principles of elegantly simple user interface design and enables users to produce Address Labels, Inventory Tags, Price Labels and Business Name Cards quickly and easily. 
<br/><br/>
The app is bundled with over 900 industry label stock templates and 150 clip arts, and generates commonly used barcodes via font technology. It also supports vector shapes such as rectangle, ellipse, line, and both static and dynamic text/barcode fields. Users will be able to retrieve address and contact information from the Microsoft Excel and People's app with just a few taps and have the information printed out in a snap.
<br/><br/><br/>
<h3>Label Software - Windows Dekstop</h3>
<br/>
ConnectCode <b><a href="labelsoftware.shtml">Label Software</a></b> is an industrial strength label printing application with a What-You-See-Is-What-You-Get (WYSIWYG) user interface for the Windows desktop. It is equipped with over 1000 industry label templates, supports all commonly-used 1-dimensional barcodes which can be printed using inkjet, laser, multi-function, thermal and thermal-transfer printers. It is one of the most innovative label printing application in the market with advanced capabilities such as International Text, Duplex printing, TWAIN image acquisition and label templates management. 
<br/><br/><br/>
<h3>HTML Barcode SDK - Open Source</h3>
<br/>
The <a href="htmlBarcode.shtml"><b>HTML Barcode SDK</b></a> is an Open Source HTML and Javascript barcode generation Software Development Kit. It creates commonly-used 1 dimensional barcodes like Code 128, Code39, UPCA, EAN13 and others using pure Hypertext Markup Language (HTML) and can be used in a wide variety of browsers without additional browser plug-ins. 
<br/><br/><br/>
<h3>Barcode Software Image Generator</h3>
<!--
<br>
<div class="one_half">

																	<IMG alt="barcodefonts" src="images/barcodesimager2.jpg" border="1">
</div>
<div class="one_half_last">
<h4>Product Details</h4>
																		<UL class="list">
																			<LI>
																				<B><A href="barcodesoftware.shtml">Barcode Imager</A></B></li>
									

							<LI>
																				<B><a href="downloadimager.shtml">Download</A></B></li>
									

							<LI>
										
																		</UL>
</div>
-->
<br class="clear"/>

														ConnectCode <b><a href="barcodesoftware.shtml">Barcode Imager</a></b> allows you to generate professional barcode images that are 
															highly scannable. These images can be opened in other applications for futher 
															layout and printing. ConnectCode Imager supports popular image formats 
															including Portable Network Graphics, Graphics Interchange, Windows Bitmap, 
															Tagged Image File and Jpeg. The tool is very flexible. It lets you to generate 
															images of various resolutions to cater to different printers and applications.
<br/><br/>
<!--
Among the barcodes supported include Code39, Code 39 Extended, Code93, Code 128 Auto, Code128A, Code128B, Code128C, I2of5 (Interleaved 2 of 5), EAN 13 (European Article Numbering), EAN 8, UPCA (Universal Product Code), UPCE, UCCEAN, ISBN, ISSN, MSI, Codabar, UPC/EAN Extension 2 and Extension 5. 
<br><br>
														

														<P>The ConnectCode Engine have been created and tested vigourously by a team with 
															many years of experience in the Auto-ID and IT industry. It takes into 
															consideration many subtle properties to ensure the highest scannability and 
															compactness of the barcode.</P>

-->
<!--
				<br class="clear" />
				<div class="one_half tt-column">
				   <h4>1/2</h4>
				   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis sem nibh. Proin fringilla, lectus vitae vestibulum facilisis, est justo tempus orci, vel laoreet diam eros placerat elit. Pellentesque facilisis tempus velit sit amet porttitor. Proin nibh magna, porttitor accumsan malesuada non, auctor id tortor.</p>
				</div>
				<div class="one_half_last tt-column">
				   <h4>1/2</h4>
				   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis sem nibh. Proin fringilla, lectus vitae vestibulum facilisis, est justo tempus orci, vel laoreet diam eros placerat elit. Pellentesque facilisis tempus velit sit amet porttitor. Proin nibh magna, porttitor accumsan malesuada non, auctor id tortor.</p>
				</div>
				<br class="clear" />
                  
-->
<!-- ////////////////////////////////////////////////////////// -->
<!-- ***************** - Content Ends Here - ****************** -->
<!-- ////////////////////////////////////////////////////////// -->
</main><!-- END main #content -->
</div><!-- END main-area -->
<div id="footer-top"> </div>
</div><!-- END main -->
<!-- ***************** - Footer Starts Here - ***************** -->
<footer id="footer" role="contentinfo">
<div class="footer-overlay">
<div class="footer-content">
<div class="one_third tt-column">
<h3>News</h3>
<div class="textwidget">
<div class="blog-posts-shortcode-outer-wrap">
<ul class="tt-recent-posts">
<li>
<h4><a href="press.shtml">11-Feb-2020</a></h4>
<p><a href="press.shtml">Leading Barcode Software creates barcodes using fonts on SQL Server T-SQL, .NET Core 3.0, and Blazor</a></p>
</li>
</ul>
</div>
<br class="clear"/>
</div>
</div>
<div class="one_third tt-column">
<h3>Social Networks</h3>
<ul class="social_icons tt_vector_social_icons tt_no_social_title tt_image_social_icons">
<li><a class="twitter" href="ConnectCodeTwitter.shtml" title="Twitter">Twitter</a></li>
<li><a class="email" href="mailto:info@barcoderesource.com" title="Email">Email</a></li>
</ul>
<h3>Talk to us</h3>
<div class="textwidget">
<ul class="tt-business-contact">
<li><a class="tt-biz-phone" href="tel:6565236908">+65 6523 6908</a></li>
<li><a class="tt-biz-email" href="mailto:info@barcoderesource.com">info@barcoderesource.com</a></li>
</ul>
</div>
</div>
<div class="one_third_last tt-column">
<h3>Join our Mailing List</h3>
<div id="mc_signup">
<form action="//www.barcoderesource.com/cgi-bin/addemailbsbinx.pl">
<p><input id="mc_signup_form" name="mc_submit_type" type="hidden" value="html"/>
</p><div class="mc_merge_var">
<label class="mc_var_label mc_header mc_header_email" for="mc_mv_EMAIL">Email Address<span class="mc_required">*</span></label>
<br/>
<input class="mc_input" id="mc_mv_EMAIL" name="email" size="18" type="text" value=""/>
</div><!-- /mc_merge_var -->
<p><br/><input class="button" id="mc_signup_submit" name="B1" type="submit" value="Submit"/></p>
</form>
</div><!-- end mc_signup -->
</div><!-- end fourth one_fourth_column -->
</div><!-- END footer-content -->
</div><!-- END footer-overlay -->
<!-- ***************** - Footer Bottom Starts Here - ***************** -->
<div id="footer_bottom">
<div class="info">
<div id="foot_left"><p>Copyright © 2004-2020 barcoderesource.com. All rights reserved.</p>
</div><!-- end foot_left -->
<div id="foot_right">
<div class="top-footer"><a class="link-top" href="#">top</a></div>
<ul>
<li><a href="index.shtml">Home</a></li>
<li><a href="contact.shtml">About &amp; Contact</a></li><li><a href="privacy_policy.shtml">Privacy Policy</a></li>
</ul>
</div><!-- end foot_right -->
</div><!-- end info -->
</div><!-- end footer_bottom -->
</footer><!-- END footer -->
</div><!-- END wrapper -->
</div><!-- END tt-layout -->
<!-- ***************** - JavaScript Starts Here - ***************** -->
<script src="jsx/custom-main.js" type="text/javascript"></script>
<script src="jsx/superfish.js" type="text/javascript"></script>
<script src="jsx/jquery.flexslider.js" type="text/javascript"></script>
<script src="jsx/jquery.fitvids.js" type="text/javascript"></script>
<script src="jsx/scrollWatch.js" type="text/javascript"></script>
<script src="jsx/jquery.isotope.js" type="text/javascript"></script>
<script src="jsx/jquery.ui.core.min.js" type="text/javascript"></script>
<script src="jsx/jquery.ui.widget.min.js" type="text/javascript"></script>
<script src="jsx/jquery.ui.tabs.min.js" type="text/javascript"></script>
<script src="jsx/jquery.ui.accordion.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="jsx/jquery.prettyPhoto.js"></script>-->
<script>
jQuery(document).ready(function () {
    jQuery('.tt-parallax-text').fadeIn(1000); //delete this to remove fading content

    var $window = jQuery(window);
    jQuery('section[data-type="background"]').each(function () {
        var $bgobj = jQuery(this);

        jQuery(window).scroll(function () {
            var yPos = -($window.scrollTop() / $bgobj.data('speed'));
            var coords = '50% ' + yPos + 'px';
            $bgobj.css({
                backgroundPosition: coords
            });
        });
    });
});
</script>
<!--[if !IE]><!--><script>
  if (/*@cc_on!@*/false) {
	  document.documentElement.className+=' ie10';
  }
</script><!--<![endif]-->
</body></html>