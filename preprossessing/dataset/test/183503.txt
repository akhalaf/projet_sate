<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="https://cdn.sucuri.net/css/whitelabel/typography.css?611e1e2" rel="stylesheet"/>
<link href="https://cdn.sucuri.net/css/whitelabel.css?611e1e2" rel="stylesheet"/>
<link href="https://cdn.sucuri.net/css/whitelabel/buttons.css?611e1e2" rel="stylesheet"/>
<link href="https://cdn.sucuri.net/css/whitelabel/footer.css?611e1e2" rel="stylesheet"/>
<link href="https://cdn.sucuri.net/css/whitelabel/header.css?611e1e2" rel="stylesheet"/>
<link href="https://cdn.sucuri.net/css/fonts.css" rel="stylesheet"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>GoDaddy Security - Access Denied</title>
</head>
<body>
<header class="app-header clearfix">
<div class="wrap"><a class="logo" href="https://www.godaddy.com/"></a>
<span class="logo-neartext">Security</span>
</div>
</header>
<section class="center clearfix"></section>
<section class="app-content access-denied clearfix"><div class="box center width-max-940"><h1 class="brand-font font-size-xtra no-margin"><i class="icon-circle-red"></i>Access Denied - GoDaddy Website Firewall</h1>
<p class="medium-text code-snippet">If you are the site owner (or you manage this site), please whitelist your IP or if you think this block is an error please <a class="color-green underline" href="https://supportx.sucuri.net/">open a support ticket</a> and make sure to include the block details (displayed in the box below), so we can assist you in troubleshooting the issue. </p><h2>Block details:
</h2><table class="property-table overflow-break-all line-height-16">
<tr>
<td>Your IP:</td>
<td><span>210.75.253.169</span></td>
</tr>
<tr><td>URL:</td>
<td><span>www.backofficellc.com/~webapps/validation%09%0A</span></td>
</tr>
<tr>
<td>Your Browser: </td>
<td><span>python-requests/2.22.0</span></td>
</tr>
<tr><td>Block ID:</td>
<td><span>EVA079</span></td>
</tr>
<tr>
<td>Block reason:</td>
<td><span>An attempt to evade and bypass security filters was detected.</span></td>
</tr>
<tr>
<td>Time:</td>
<td><span>2021-01-14 19:19:47</span></td>
</tr>
<tr>
<td>Server ID:</td>
<td><span>18010</span></td></tr>
</table>
</div>
</section>
<footer class="app-footer clearfix"><span>Copyright © 1999 – 2019 GoDaddy Operating Company, LLC. All rights reserved.</span>
<span class="padding-left-25"><a class="underline" href="https://www.godaddy.com/Agreements/Privacy.aspx" rel="nofollow noopener" target="_blank">Privacy Policy</a></span>
</footer>
</body>
</html>
