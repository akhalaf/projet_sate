<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="text/html;charset=utf-8" http-equiv="Content-type"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<title>Awakin.org: Awakening with Kin</title>
<meta content="Awakin.org is a portal that delivers weekly wisdom from varied spiritual and religious disciplines." name="description"/>
<meta content="service, nonprofit, technology, spirituality, gift-economy, volunteerism, reading, inspiration, reflections, contemplations, meditation, spirit, reflection, uplifting, hopeful" name="keywords"/>
<base href="https://www.awakin.org/"/>
<!-- SET: Favicon -->
<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!-- SET: Stylesheet -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://awakin.org/inc/2019/css/bootstrap4hack.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://awakin.org/inc/2019/css/style.css?8b" rel="stylesheet" type="text/css"/>
<link href="https://awakin.org/inc/2019/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="https://servicespace.org/rss/tow.php" rel="alternate" title="Awakin: RSS Feed" type="application/rss+xml"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> <!-- Need this upfront for Audio Player -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" nonce="ssp271655">
  nav ul li a {fo}
</script>
<style>
</style>
<![endif]-->
</head>
<body>
<!-- wrapper starts -->
<div class="wrapper">
<!--Header Starts Here-->
<!--<div class="overlay"></div>-->
<header>
<div class="container">
<div class="top-header d-md-flex">
<ul class="d-md-flex ml-md-auto">
<li class="d-none d-md-inline-flex"><a class="transition" href="index.php">About Us</a></li>
<li class="d-none d-md-inline-flex"><a class="transition" href="index.php?op=subscribe">Subscribe</a></li>
<li class="d-block d-md-none login_link"><a class="transition d-inline-flex" href="index.php?op=subscribe" target="_blank">Subscribe</a></li>
<!--
                    <li class="d-block d-md-none login_link"><a href="https://servicespace.org/login" target="_blank" class="transition d-inline-flex">Login</a></li>
-->
<li class="d-block d-md-none contact_link"><a class="transition" href="https://www.awakin.org/index.php?op=contact">Contact Us</a></li>
</ul>
</div>
<div class="logo_block">
<ul class="d-flex">
<li>
<div class="logo_icon">
<a href="">
<figure>
<img alt="logo" src="images/ss_logo_dark.png" width="120"/>
</figure>
</a>
</div>
</li>
<li>
<div class="logo_heading">
<h1>
<a href="">Awa<span>kin</span>.org</a>
</h1>
<h6 class="text-right"><b style="font-family: 'Segoe UI'">Waking up to Wisdom In Stillness and Community</b></h6>
</div>
</li>
</ul>
</div>
<div class="header_links d-md-flex">
<ul class="d-md-flex ml-md-auto text-uppercase">
<li>
<a class="transition toplevel " href="read">Awakin Readings</a>
<ul class="dropdown text-capitalize">
<li><a class="sub_menu_item transition" href="read">Latest</a></li>
<li><a class="sub_menu_item transition" href="read/index.php?op=year">Archives</a></li>
<li><a class="sub_menu_item transition" href="read/view.php?op=search&amp;t=popular">Popular</a></li>
</ul>
</li>
<li>
<a class="transition toplevel " href="local">Awakin Circles</a>
<ul class="dropdown text-capitalize">
<li><a class="sub_menu_item transition" href="local">Locations</a></li>
<li><a class="sub_menu_item transition" href="local/about/start">Start A Circle!</a></li>
<li><a class="sub_menu_item transition" href="local/about/history">History</a></li>
</ul>
</li>
<li>
<a class="transition toplevel " href="calls">Awakin Calls</a>
<ul class="dropdown text-capitalize">
<li><a class="sub_menu_item transition" href="calls">Upcoming Call</a></li>
<li><a class="sub_menu_item transition" href="talks">Awakin Talks</a></li>
<li><a class="sub_menu_item transition" href="calls/?pg=archives">Archives</a></li>
<li><a class="sub_menu_item transition" href="calls/?pg=popular">Popular</a></li>
</ul>
</li>
</ul>
</div>
<div class="nav_icon d-block d-md-none">
<span></span>
<span></span>
<span></span>
</div>
</div>
</header>
<!--Header Ends Here--><!--Main Content Starts Here-->
<div class="main_content">
<div class="content_block">
<div class="container">
<div class="main_block d-flex flex-wrap">
<div class="left_block d-none d-md-flex align-items-center justify-content-center col-md-4 px-1">
<figure>
<img alt="plant" height="500" src="images/plant.jpg" width="400"/>
</figure>
</div>
<div class="right_block col-md-8 p-0">
<div class="right_content">
<p>Awakin.org is about deepening our self-awareness, in a community of kindred spirits. By changing ourselves, we change the world.</p>
<p>We offer three ways to do this:</p>
</div>
<ul class="d-flex flex-wrap">
<li class="col-sm-4 p-0">
<div class="gallery_content content_1 text-center">
<a href="read"><figure><img alt="gallery_1" height="170" src="images/gowithin.jpg" width="170"/></figure></a>
<a href="read"><b>Go Within</b></a>
<p>Read or comment on an inspiring passage of the week, or our vast archive that spans all wisdom traditions. This week:</p>
<a class="btn border-rounded transition" href="https://www.awakin.org/read/view.php?tid=2482">Bryan Stevenson</a>
</div>
</li>
<li class="col-sm-4 p-0">
<div class="gallery_content content_2 text-center">
<a href="local"><figure><img alt="gallery_1" height="170" src="images/meditation_hall.jpg" width="170"/></figure></a>
<a href="local"><b>Go Local</b></a>
<p>Join local groups in 60+ cities around the globe, where folks convene for an hour of group meditation, followed by a circle of sharing and a meal.</p>
</div>
</li>
<li class="col-sm-4 p-0">
<div class="gallery_content content_3 text-center">
<a href="calls"><figure><img alt="gallery_1" height="170" src="images/conference_call_.jpeg" width="170"/></figure></a>
<a href="calls"><b>Go Online</b></a>
<p>Every Saturday, dial into our Awakin Calls from wherever you are -- and engage with an inspiring guest speaker.  This week:</p>
<a class="btn border-rounded transition" href="https://www.awakin.org/calls/?pg=guest&amp;cid=534">Peniel Joseph</a>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!--Main Content Ends Here--><!--Footer Starts Here-->
<footer>
<div class="container">
<div class="footer_block">
<p><a class="btn btn-warning" href="index.php?op=subscribe" role="button" style="padding: 4px">Subscribe</a></p>
<p><a class="transition" href="https://www.servicespace.org/rss/tow.php">RSS Feed</a>, Podcasts (<a class="transition" href="https://www.servicespace.org/rss/tow-podcast.php">Readings</a>, <a class="transition" href="https://www.servicespace.org/rss/forest-podcast.php">Calls</a>, <a href="https://itunes.apple.com/us/podcast/servicespace-forest-call/id554916314">iTunes</a>)</p>
<p>Any questions? <a class="transition" href="https://www.awakin.org/index.php?op=contact">Contact us</a>.</p>
<p>Awakin.org is a project of <a class="transition" href="https://www.servicespace.org" target="_blank">ServiceSpace</a>.</p>
</div>
</div>
</footer>
<!--Footer Ends Here-->
</div>
<!-- wrapper ends -->
<!-- SET: SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $('.nav_icon').click(function(){
            $('body').toggleClass('open_menu');
            $(".header_links").slideToggle();
            $(this).toggleClass('open');
            });
        $('.top-header').clone().appendTo('.header_links');
        $('.overlay').click(function(){
            $('body').removeClass('open_menu');
            $('.nav_icon').removeClass('open');
            $('.header_links').slideUp();
        });
        $('.header_links ul li a').click(function(){
            if( $(window).width() < 768 ){
               $(this).parent().find('.dropdown').slideToggle();
               $(this).toggleClass('active');
               $(this).parent().siblings().find('.dropdown').slideUp();
               $(this).parent().siblings().find('>a').removeClass('active');       
            }
        });
        
    }); 
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-472633-16', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
</body>
</html>
