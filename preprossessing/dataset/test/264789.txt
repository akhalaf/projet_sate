<!DOCTYPE html>
<!--[if IE]>
<![if IE 5]><html class="ie ie5 lie9 lie8 lie7 lie6"><![endif]>
	<![if IE 6]><html class="ie ie6 lie9 lie8 lie7"><![endif]>
	<![if IE 7]><html class="ie ie7 lie9 lie8"><![endif]>
	<![if IE 8]><html class="ie ie8 lie9"><![endif]>
	<![if IE 9]><html class="ie ie9"><![endif]>
	<![if !((IE 6)|(IE 7)|(IE 8)|(IE 9))]><html class="ie"><![endif]>
<![endif]--><html class="notie" lang="zh-CN">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<title>菠萝设备有限公司</title>
<meta content="菠萝设备有限公司" name="description"/>
<meta content="菠萝设备有限公司" name="keywords"/>
<link href="/template/company/shebeijixie/skin/css/78002.css" rel="stylesheet" type="text/css"/>
<script charset="utf-8" src="/template/company/shebeijixie/skin/js/jquery-1.9.1.min.js" type="text/javascript"></script>
</head>
<body>
<div class="header">
<div class="container ">
<div class="fl logo"> <a href="https://www.buddylourdes.com/"><img src="/uploads/images/logo.png?n=5ch2b2eqtxuk5pxfusd6nhej5gmzbzmfvtsy7oa&amp;w=400"/></a> </div>
<div class="fr">
<div class="search c">
<form action="/plus/search.php" name="formsearch">
<input name="kwtype" type="hidden" value="0"/>
<button type="submit" value="">搜 索</button>
<input class="text" id="q" name="q" type="text"/>
</form>
</div>
</div>
</div>
</div>
<div class="nav c">
<div class="container">
<ul class="navul">
<li><a href="/">网站首页</a> </li><li><a href="https://www.buddylourdes.com/product/index_553/">卫星天线</a>
<div class="inul ">
<div class="inul-in c">
<ul>
<li><a href="https://www.buddylourdes.com/product/list_159/">等离子焊机</a>
</li><li><a href="https://www.buddylourdes.com/product/index_835/">氟化物</a>
</li><li><a href="https://www.buddylourdes.com/product/index_917/">传真纸</a>
</li><li><a href="https://www.buddylourdes.com/product/index_356/">化工实验设备</a>
</li><li><a href="https://www.buddylourdes.com/product/index_975.html">套圈</a>
</li></ul>
</div>
</div>
</li>
<li><a href="https://www.buddylourdes.com/product/list_786.html">踏步机</a>
<div class="inul ">
<div class="inul-in c">
<ul>
<li><a href="https://www.buddylourdes.com/product/index_964.html">压合机</a>
</li><li><a href="https://www.buddylourdes.com/product/list_361.html">燃料电池</a>
</li><li><a href="https://www.buddylourdes.com/product/index_488/">硅酸盐</a>
</li><li><a href="https://www.buddylourdes.com/product/list_364/">女式羽绒服</a>
</li><li><a href="https://www.buddylourdes.com/product/list_923.html">保健品代理</a>
</li></ul>
</div>
</div>
</li>
<li><a href="https://www.buddylourdes.com/product/index_737.html">图文处理软件</a>
<div class="inul ">
<div class="inul-in c">
<ul>
<li><a href="https://www.buddylourdes.com/product/index_191/">汽车天线</a>
</li><li><a href="https://www.buddylourdes.com/product/list_797/">挂钟</a>
</li><li><a href="https://www.buddylourdes.com/product/index_656/">驱虫灭害化学品</a>
</li><li><a href="https://www.buddylourdes.com/product/index_827/">内页</a>
</li><li><a href="https://www.buddylourdes.com/product/list_877/">太阳帽</a>
</li></ul>
</div>
</div>
</li>
<li><a href="https://www.buddylourdes.com/product/list_392/">电热膜</a>
<div class="inul ">
<div class="inul-in c">
<ul>
<li><a href="https://www.buddylourdes.com/product/list_916/">挂镜线</a>
</li><li><a href="https://www.buddylourdes.com/product/list_139/">工程施工</a>
</li><li><a href="https://www.buddylourdes.com/product/index_754/">非金属矿物制品</a>
</li><li><a href="https://www.buddylourdes.com/product/list_234/">男式凉拖鞋</a>
</li><li><a href="https://www.buddylourdes.com/product/index_865/">萤石</a>
</li></ul>
</div>
</div>
</li>
<li><a href="https://www.buddylourdes.com/newslist/808/">新闻中心</a></li>
<li><a href="https://www.buddylourdes.com/about.html">关于我们</a></li>
<li><a href="https://www.buddylourdes.com/lianxi.html">联系我们</a></li>
</ul>
</div>
</div>
<div class="banner">
<ul id="slides">
<li style="background:url('/temp/data/common/banner/1-1F411112115416.jpg') no-repeat center top;z-index:1;"> <a href="javascript:"><span>自行车轮胎</span></a> </li>
<li style="background:url('/temp/data/common/banner/1-1F4111121300-L.jpg') no-repeat center top;z-index:1;"> <a href="javascript:"><span>其他电热设备</span></a> </li>
<li style="background:url('/temp/data/common/banner/1-1F4111121300-L.jpg') no-repeat center top;z-index:1;"> <a href="javascript:"><span>笔类</span></a> </li>
</ul>
</div>
<div class="main1">
<div class="container">
<div class="column-title c">
<div class="fl title">产品展示</div>
<div class="fr"> <a class="m1-cpzs-prev" href="#"></a>
<ul class="m1-cpzs-pages">
</ul>
<a class="m1-cpzs-next" href="#"></a> </div>
</div>
<div class="m1-cpzs c">
<ul>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show_6767.html" title="装潢设计343-34328934"> <img alt="装潢设计343-34328934" src="https://www.buddylourdes.com/uploads/images/240689.jpg"/></a> </div>
<div class="title">装潢设计343-34328934</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show_6767.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show_9494.html" title="弯头D7305-735464"> <img alt="弯头D7305-735464" src="https://www.buddylourdes.com/uploads/images/107561.jpg"/></a> </div>
<div class="title">弯头D7305-735464</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show_9494.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show_8989.html" title="手套8B2A-824266"> <img alt="手套8B2A-824266" src="https://www.buddylourdes.com/uploads/images/134717.jpg"/></a> </div>
<div class="title">手套8B2A-824266</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show_8989.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show_7171.html" title="调速转把97B-971"> <img alt="调速转把97B-971" src="https://www.buddylourdes.com/uploads/images/211237.jpg"/></a> </div>
<div class="title">调速转把97B-971</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show_7171.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show_1515.html" title="其他智能卡AAD-273"> <img alt="其他智能卡AAD-273" src="https://www.buddylourdes.com/uploads/images/214556.jpg"/></a> </div>
<div class="title">其他智能卡AAD-273</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show_1515.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/5757.html" title="丰胸化学品D88-883662"> <img alt="丰胸化学品D88-883662" src="https://www.buddylourdes.com/uploads/images/949774.jpg"/></a> </div>
<div class="title">丰胸化学品D88-883662</div>
<div class="view"><a href="https://www.buddylourdes.com/product/5757.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/7373.html" title="光盘A69D06-696"> <img alt="光盘A69D06-696" src="https://www.buddylourdes.com/uploads/images/200660.jpg"/></a> </div>
<div class="title">光盘A69D06-696</div>
<div class="view"><a href="https://www.buddylourdes.com/product/7373.html" title="查看详情">查看详情</a></div>
</li>
<li>
<div class="img"><a href="https://www.buddylourdes.com/product/show8787/" title="防护鞋1EBCAE-12615"> <img alt="防护鞋1EBCAE-12615" src="https://www.buddylourdes.com/uploads/images/632328.jpg"/></a> </div>
<div class="title">防护鞋1EBCAE-12615</div>
<div class="view"><a href="https://www.buddylourdes.com/product/show8787/" title="查看详情">查看详情</a></div>
</li>
</ul>
</div>
</div>
</div>
<div class="main2">
<div class="container">
<div class="m2-2">
<div class="title">菠萝设备有限公司</div>
<p>菠萝设备有限公司10、水泥强度检验用ISO标准砂、灌砂专用砂。4、公路、铁路、桥梁、隧道、路基路面检测仪器5、沥青及沥青混合料检测仪器你不舍得花钱，我给你做实惠大气经典型模具，欢迎您来厂订购模具小霞模具专业生产汽车模具:汽配模具，汽车塑料件模具，汽车注塑模具，电动汽车模具，摩托车模具，电瓶车模具，电动车模具，玩具车模具，儿童车模具。日用品模具：快餐盒模具，保鲜盒模具，托盘模具，周转箱模具，水果筐模具，塑料盆模具，</p>
<div> <a class="m2-more" href=" http://www.buddylourdes.com/about/">MORE&gt;&gt;</a></div>
</div>
<div class="m2-1"> <img alt="" src="/template/company/shebeijixie/skin/images/img2.jpg"/> </div>
<div class="m2-3">
<div class="title"> 联系我们 </div>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon1.png"/><a href="Mailto:121534971@143.com" style="color:#fff"><span class="__cf_email__" data-cfemail="536261626660676a6462136267607d303c3e">[email protected]</span></a></p>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon2.png"/>089-61684350</p>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon3.png"/>089-61684350</p>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon4.png"/>2886853555 </p>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon6.png"/>alibaban</p>
<p><img alt="菠萝设备有限公司" src="/template/company/shebeijixie/skin/images/icon5.png"/>河南郑州荥阳市邓府圩143号</p>
</div>
</div>
</div>
<div class="main3">
<div class="container">
<div class="m3-in c">
<div class="m3-fl fl">
<div class="column-title c">
<div class="fl"><img alt="案例" src="/template/company/shebeijixie/skin/images/title_anli.jpg"/></div>
<div class="fr" style="padding-right: 40px;"> <a href="https://www.buddylourdes.com/product/index_726.html"><img alt="更多" src="/template/company/shebeijixie/skin/images/more.jpg"/></a></div>
</div>
<ul class="m3-anli">
<li>
<div class="fl"><a href="https://www.buddylourdes.com/product/show_1818.html" title="色谱仪803-832891639"><img alt="色谱仪803-832891639" src="https://www.buddylourdes.com/uploads/images/627791.jpg"/></a></div>
<div class="fr">
<div class="title"><a href="https://www.buddylourdes.com/product/show_1818.html">色谱仪803-832891639</a></div>
<p>一个人逛超市，一个人看电影，一个人吃火锅，这些21世纪的现代孤独体验我算是一件不落。...</p>
<a class="more" href="https://www.buddylourdes.com/product/show_1818.html" title="more">[MORE&gt;&gt;]</a> </div>
</li>
<li>
<div class="fl"><a href="https://www.buddylourdes.com/product/2828.html" title="垃圾车451-451767"><img alt="垃圾车451-451767" src="https://www.buddylourdes.com/uploads/images/817508.jpg"/></a></div>
<div class="fr">
<div class="title"><a href="https://www.buddylourdes.com/product/2828.html">垃圾车451-451767</a></div>
<p>具有时代性又不花哨，观念惊世骇俗但绝不是个简单的反对者”，应该说评价很高了。...</p>
<a class="more" href="https://www.buddylourdes.com/product/2828.html" title="more">[MORE&gt;&gt;]</a> </div>
</li>
<li>
<div class="fl"><a href="https://www.buddylourdes.com/product/show3535/" title="滚焊机9BD-981"><img alt="滚焊机9BD-981" src="https://www.buddylourdes.com/uploads/images/690326.jpg"/></a></div>
<div class="fr">
<div class="title"><a href="https://www.buddylourdes.com/product/show3535/">滚焊机9BD-981</a></div>
<p>网易新闻首发，未经授权不得转载。...</p>
<a class="more" href="https://www.buddylourdes.com/product/show3535/" title="more">[MORE&gt;&gt;]</a> </div>
</li>
</ul>
</div>
<div class="m3-fr fr">
<div class="column-title c">
<div class="fl"><img alt="新闻" src="/template/company/shebeijixie/skin/images/title_xinwen.jpg"/></div>
<div class="fr"><a href="https://www.buddylourdes.com/newslist/202/"><img alt="更多" src="/template/company/shebeijixie/skin/images/more.jpg"/></a></div>
</div>
<ul class="m3-xinwen">
<li><a href="https://www.buddylourdes.com/show/61648.html" title="单薄柔弱的陈都灵，竟然把气场全开的佟丽娅碾压成渣渣…">单薄柔弱的陈都灵，竟然把气场全开的佟丽娅碾压成渣渣…</a>2021-01-14 20:15</li>
<li><a href="https://www.buddylourdes.com/show/41663.html" title="探秘华为新模式客户服务中心：不止有智能机器人，还有高标准服务">探秘华为新模式客户服务中心：不止有智能机器人，还有高标准服务</a>2021-01-14 19:51</li>
<li><a href="https://www.buddylourdes.com/html/20210114/81629.html" title="2020广州车展：东风悦达起亚智跑Ace...">2020广州车展：东风悦达起亚智跑Ace...</a>2021-01-14 19:13</li>
<li><a href="https://www.buddylourdes.com/show/41631.html" title="中甲保级附加赛-黑龙江点球大战5-3新疆总比分9-7保级成功">中甲保级附加赛-黑龙江点球大战5-3新疆总比分9-7保级成功</a>2021-01-14 18:44</li>
<li><a href="https://www.buddylourdes.com/html/20210114/91631.html" title="据悉德国拟将防疫封锁措施延长到圣诞节前">据悉德国拟将防疫封锁措施延长到圣诞节前</a>2021-01-14 18:35</li>
<li><a href="https://www.buddylourdes.com/news/20210114/31655.html" title="【每周汇市调查】加元迎来一波爆发美元易守难攻下周重磅数据来袭">【每周汇市调查】加元迎来一波爆发美元易守难攻下周重磅数据来袭</a>2021-01-14 18:32</li>
<li><a href="https://www.buddylourdes.com/show/11654.html" title="日本空自F-15J战机使用尾钩着陆现场火花四溅">日本空自F-15J战机使用尾钩着陆现场火花四溅</a>2021-01-14 18:30</li>
<li><a href="https://www.buddylourdes.com/news/20210114/11683.html" title="中国移动：明年将基本实现全国市、县城区及部分重点乡镇5G良好覆盖">中国移动：明年将基本实现全国市、县城区及部分重点乡镇5G良好覆盖</a>2021-01-14 18:03</li>
<li><a href="https://www.buddylourdes.com/show/91652.html" title="台军一架F-16夜训时失联飞行员目前仍未找到">台军一架F-16夜训时失联飞行员目前仍未找到</a>2021-01-14 17:33</li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer" style="height:auto;">
<div class="container">
<div class="friendlink">
<div> <a href="https://www.buddylourdes.com/product/index_491/">结晶设备</a>    <a href="https://www.buddylourdes.com/product/index_765.html">其他煤制品</a>    <a href="https://www.buddylourdes.com/product/index_288/">橡皮擦</a>    <a href="https://www.buddylourdes.com/product/index_937/">阻燃面料</a>    <a href="https://www.buddylourdes.com/product/list_658/">其他橡胶机械</a>    <a href="https://www.buddylourdes.com/product/index_697/">磁卡</a>    <a href="https://www.buddylourdes.com/product/index_183/">配电屏</a>    <a href="https://www.buddylourdes.com/product/list_175.html">前端设备</a>    <a href="https://www.buddylourdes.com/product/index_687.html">色标</a>    <a href="https://www.buddylourdes.com/product/list_265.html">洗面奶</a>   </div>
<div></div>
<div></div>
</div>
<div class="fl">
<div> Copyright © 2017 Powered by 菠萝设备有限公司</div>
</div>
<div class="fr">
</div>
</div>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script charset="utf-8" src="/template/company/shebeijixie/skin/js/jquery.jslides.js" type="text/javascript"></script>
<script charset="utf-8" src="/template/company/shebeijixie/skin/js/plugins.js" type="text/javascript"></script>
<script charset="utf-8" src="/template/company/shebeijixie/skin/js/sly.min.js" type="text/javascript"></script>
<script charset="utf-8" src="/template/company/shebeijixie/skin/js/webwidget_slideshow_dot.js" type="text/javascript"></script>
<script>
    /*首页产品拖拽*/
    jQuery(function($){
        'use strict';
        (function () {
            var $frame = $('.m1-cpzs');
            $frame.sly({
                activateMiddle: 1,
                horizontal: 1,
                itemNav: 'basic',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBy: 1,
                pagesBar: $('.m1-cpzs-pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                prevPage: $('.m1-cpzs-prev'),
                nextPage: $('.m1-cpzs-next')
            });
        }());
    });
    /*发展历程拖拽*/
    jQuery(function($){
        'use strict';
        (function () {
            var $frame = $('.course-nav');
            $frame.sly({
                activateMiddle: 1,
                horizontal: 1,
                itemNav: 'basic',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: $('.course-nav li').index($('.course-nav li.hover')),//获取当前的位置
                scrollBy: 1,
                speed: 300,
                elasticBounds: 1,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                prevPage: $('.course-nav-l'),
                nextPage: $('.course-nav-r')
            });
        }());
    });
    
        /*荣誉资质拖拽*/
    jQuery(function($){
        'use strict';
        (function () {
            var $frame = $('.ry_1');
            $frame.sly({
                activateMiddle: 1,
                horizontal: 1,
                itemNav: 'basic',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,//获取当前的位置
                scrollBy: 1,
                speed: 300,
                elasticBounds: 1,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                prevPage: $('.ry_1-l'),
                nextPage: $('.ry_1-r')
            });
            
          $('.ry_1 li').click(function(){
              var $li = $('.ry_1 li');
              var index = $li.index(this);
              $li.removeClass('hover');
              $(this).addClass('hover');
              var $listul = $('.ry_2 li');
              $listul.removeClass('show');
              $listul.eq(index).addClass('show');
          })
          $('.ry_1 li').eq(0).click();
            
        }());
    });
    
    
    /*左侧产品焦点图*/
    $(function() {
        $(".main .main-left .left-cp").webwidget_slideshow_dot({
            slideshow_time_interval: '5000',
            slideshow_window_width: '220',
            slideshow_window_height: '170',
            slideshow_title_color: '#666',
            soldeshow_foreColor: '#fff',
            directory: 'images/'
        });
    });
    /*导航条ie6hover兼容*/
    $(function(){
        $('.nav .navul>li').hover(function(){
            $(this).addClass('hover');
        },function(){
            $(this).removeClass('hover');
        });
    });
    /*友情链接tab切换*/
    $(function(){
        $('.link-nav li').click(function(){
            var $li = $('.link-nav ul li');
            var index = $li.index(this);
            $li.removeClass('hover');
            $(this).addClass('hover');
            var $listul = $('.link-list ul');
            $listul.removeClass('show');
            $listul.eq(index).addClass('show');
        })
    })
</script></body>
</html>