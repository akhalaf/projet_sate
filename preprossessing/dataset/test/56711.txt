<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Kentucky  School for the Deaf - Provides services from elementary through high school.</title>
<meta content="Reference, Education, Special Education, Schools - Kentucky  School for the Deaf. Provides services from elementary through high school." name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/resources/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/resources/A.style.css+ui.css,Mcc.jEkV5vj9E0.css.pagespeed.cf.czmHp5pWPd.css" rel="stylesheet" type="text/css"/>
</head>
<script language="javascript" src="/resources/scripts.js"></script>
<body bgcolor="white">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="760">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" class="header" width="100%">
<tr>
<td rowspan="2"><a href="https://www.abc-directory.com/"><img border="0" height="84" src="/resources/logo1.gif" width="205"/></a></td>
<td align="right" width="100%"><table border="0" cellpadding="3" cellspacing="0" class="top-nav">
<tr align="center">
<td><a href="https://www.abc-directory.com/"><img border="0" height="10" src="/resources/ico_home.gif" width="11"/></a></td>
<td> </td>
<td><a href="javascript:setHomepage()"><img border="0" height="11" src="/resources/ico_homepage.gif" width="9"/></a></td>
<td> </td>
<td><a href="javascript:AddToFavorites('https://www.abc-directory.com/site/4517374', 'Kentucky  School for the Deaf - Provides services from elementary through high school.')"><img border="0" height="10" src="/resources/ico_favorite.gif" width="9"/></a></td>
<td> </td>
<td><a href="https://www.abc-directory.com/contactus/"><img border="0" height="8" src="/resources/ico_mail.gif" width="12"/></a></td>
<td> </td>
</tr>
<tr align="center">
<td><a class="topmenu" href="https://www.abc-directory.com/">Home</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="https://www.abc-directory.com/submiturl">Submit URL</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="javascript:AddToFavorites('https://www.abc-directory.com/site/4517374', 'Kentucky  School for the Deaf - Provides services from elementary through high school.')">Add to Favorite</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="https://www.abc-directory.com/contactus/">Contact</a></td>
<td></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="bottom">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="bottom" width="100%">
<tr>
<td width="50%">
</td>
<td>
<a class="titleondark" href="https://www.abc-directory.com/"><img border="0" src="/resources/catalogue_over.gif"/></a>
</td>
<td> </td>
<td>
<a class="titleondark" href="https://article.abc-directory.com/"><img border="0" src="/resources/articles.gif"/></a>
</td>
<td> </td>
<td>
<a class="titleondark" href="https://press.abc-directory.com/"><img border="0" src="/resources/press.gif"/></a>
</td>
<td>      
                </td>
<td align="left" nowrap="" valign="top" width="110">13 January, 2021</td>
</tr>
</table>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" class="search" width="100%">
<tr>
<td><img height="15" src="/resources/search_tl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_tr.gif" width="15"/></td>
</tr>
<tr>
<td colspan="2" width="100%">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<form id="frm_search" method="post" name="frm_search" onsubmit="return Search(true, 'http://search.abc-directory.com/', 'frm_search');">
<tr>
<td width="100%"></td>
<td align="right" class="titleondark" nowrap=""><i>search for</i></td>
<td align="right"><input name="edt_search_text" type="text" value=""/></td>
<td align="right"><a href="javascript:Search(false, 'http://search.abc-directory.com/', 'frm_search')"><img border="0" src="/resources/but_search.gif"/></a></td>
</tr>
</form>
</table>
</td>
<td> </td>
</tr>
<tr>
<td><img height="15" src="/resources/search_bl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_br.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> 
          </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="content" width="100%">
<tr>
<td class="sidebar" valign="top">
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i>Categories</i></td>
<td><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td2" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/2" id="td2_a">Arts</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/2">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td3" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/3" id="td3_a">Business</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/3">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td4" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/4" id="td4_a">Computers</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/4">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td5894662" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/5894662" id="td5894662_a">Education</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/5894662">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td5894661" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/5894661" id="td5894661_a">Entertainment</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/5894661">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td6" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/6" id="td6_a">Health</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/6">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td7" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/7" id="td7_a">Home</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/7">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td471237" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/471237" id="td471237_a">Kids and Teens</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/471237">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td8" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/8" id="td8_a">News</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/8">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td9" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/9" id="td9_a">Recreation</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/9">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td10" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/10" id="td10_a">Reference</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/10">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td12" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/12" id="td12_a">Science</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/12">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td13" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/13" id="td13_a">Shopping</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/13">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td14" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/14" id="td14_a">Society</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/14">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td15" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/15" id="td15_a">Sports</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/15">»</a></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/><br/> <table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i class="titlebox">Premium Listings</i></td>
<td><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689788" id="tds_a">Chris Adams Personal Training</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689786" id="tds_a">Spray Foam Insulation Greenville SC</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689784" id="tds_a">Berman, Sobin, Gross, Feldman &amp; Darby LLP</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689782" id="tds_a">Estes Services</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689780" id="tds_a">Ester Digital</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689778" id="tds_a">Biggles Removals</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689776" id="tds_a">Mobility Elevator &amp; Lift Co.</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689774" id="tds_a">Alcohol Free &amp; Non Alcoholic Beer, Cider, Wine &amp; Spirits</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689770" id="tds_a">United Roofing Inc.</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689766" id="tds_a">Gemstone Lights</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689772" id="tds_a">Best Organic Spices and Herbs | Organic Essential Oils and Teas</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689768" id="tds_a">Union Alarm</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689764" id="tds_a">Litwiller Renovations &amp; Custom Homes</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689762" id="tds_a">Agrinote Holdings</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689760" id="tds_a">Fracas Digital</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/><br/>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td valign="top"><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i>New Addition</i></td>
<td valign="top"><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689788">Chris Adams Personal Training</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689786">Spray Foam Insulation Greenville SC</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689784">Berman, Sobin, Gross, Feldman &amp; Darby LLP</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689782">Estes Services</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689780">Ester Digital</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689778">Biggles Removals</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689776">Mobility Elevator &amp; Lift Co.</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689774">Alcohol Free &amp; Non Alcoholic Beer, Cider, Wine &amp; Spirits</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689770">United Roofing Inc.</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689766">Gemstone Lights</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689772">Best Organic Spices and Herbs | Organic Essential Oils and Teas</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689768">Union Alarm</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689764">Litwiller Renovations &amp; Custom Homes</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689762">Agrinote Holdings</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689760">Fracas Digital</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689758">Fort, Holloway, &amp; Rogers</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689754">The Law Offices of Bryan R. Kazarian</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689752">Houston &amp; Alexander, PLLC</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689750">Hess Plastic Surgery: Christopher L. Hess, MD</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689748">Dennis and King</a></td>
</tr></table>
</td></tr>
</table></td></tr>
</table>
<br/><br/>
</td>
<td>    </td>
<td class="content-text" valign="top" width="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<div class="site">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="linkblue" href="/category/10">Reference</a> »                        <a class="linkblue" href="/category/243">Education</a> »                        <a class="linkblue" href="/category/9640">Special Education</a> »                        <a class="linkblue" href="/category/93725">Schools</a> <br/><br/>
</td>
<td align="right" nowrap="">
<a class="submenu" href="/update/4517374">UPDATE INFO</a> | <a class="submenu" href="/broken/4517374">REPORT BROKEN LINK</a>
</td>
</tr>
<tr>
<td width="100%">
<h1 class="titleonwhite"><i>Kentucky  School for the Deaf</i></h1>
</td>
<td nowrap="">
<span class="text">Popularity: <img src="/resources/pop1.gif"/>   Hit: 430</span>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><span><i>Details:</i></span></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" class="site-content" width="100%">
<tr>
<td bgcolor="#FFFFFF" valign="top">
</td>
<td bgcolor="#FFFFFF" valign="top" width="100%">Provides services from elementary through high school. Find information on academics, athletics, extracurricular and community activities. Located in Danville.
					</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">URL</span>:</td>
<td bgcolor="#FFFFFF" valign="top">
<a class="linkblue detdesc" href="http://www.ksd.k12.ky.us/" id="hrefSite" rel="nofollow" target="_blank">http://www.ksd.k12.ky.us/</a>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Title</span>:</td>
<td bgcolor="#FFFFFF" class="detdesc" valign="top">What's happening</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Image</span>:</td>
<td align="center" bgcolor="#FFFFFF" class="detdesc" valign="top"><img height="400" src="https://images.abc-directory.com/4517374-2.png" width="440"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Description</span>:</td>
<td bgcolor="#FFFFFF" class="detdesc" valign="top">Reference, Education, Special Education, Schools - Kentucky  School for the Deaf. Provides services from elementary through high school.</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="texttitle" valign="top">Similar: </td>
<td bgcolor="#FFFFFF" valign="top">
<a class="keywordlink" href="https://search.abc-directory.com/ri+school+for+the+deaf">ri school for the deaf</a>
                                 - 
                                                            <a class="keywordlink" href="https://search.abc-directory.com/deaf+school+sterk">deaf school sterk</a>
                                 - 
                                                            <a class="keywordlink" href="https://search.abc-directory.com/deaf+and+dumb+school">deaf and dumb school</a>
                                 - 
                                                            <a class="keywordlink" href="https://search.abc-directory.com/galludet+school+for+the+deaf">galludet school for the deaf</a>
                                 - 
                                                            <a class="keywordlink" href="https://search.abc-directory.com/deaf+maryland+school">deaf maryland school</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Related Sites </i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="/site/4517268">The Reading Foundation</a> (Popularity: <img src="/resources/pop1.gif"/>): Canadian learning clinics provide intensive, one-on-one remedial programs in reading, spelling, comprehension and math. Offers services for learning disabilities including  ...<br/>
<a class="linkblue" href="/site/4517269">Marburn Academy</a> (Popularity: <img src="/resources/pop1.gif"/>): Serving children of average to above average intellectual ability who are experiencing difficulty in school because of learning differences such  ...<br/>
<a class="linkblue" href="/site/4517272">Salisbury School</a> (Popularity: <img src="/resources/pop1.gif"/>): Residential school in Richmond, New Zealand providing special education for female students with academic, social and emotional needs.<br/>
<a class="linkblue" href="/site/4517273">Atlanta Speech School</a> (Popularity: <img src="/resources/pop1.gif"/>): Therapeutic  educational center  for children and adults with hearing, speech,  language, or learning disabilities.  Atlanta, GA.<br/>
<a class="linkblue" href="/site/4517274">Anne Carlsen Center for Children</a> (Popularity: <img src="/resources/pop1.gif"/>): Provides health care, education and support services for children with special needs and their families.  Jamestown, ND.<br/>
<a class="linkblue" href="/site/4517276">Calvin Academy</a> (Popularity: <img src="/resources/pop1.gif"/>): Provides day school, after school tutoring, and summer programs for children who are not successful in traditional k-12 classrooms including  ...<br/>
<a class="linkblue" href="/site/4517277">The Children's Annex</a> (Popularity: <img src="/resources/pop1.gif"/>): Specializes in autistic children's education.  New York.<br/>
<a class="linkblue" href="/site/4517280">Exceptional Schools for Exceptional Children</a> (Popularity: <img src="/resources/pop1.gif"/>): Directory of special schools approved by the Massachusetts Department of Education, grouped by the type of disability served.<br/>
<a class="linkblue" href="/site/4517281">Long Island's Vincent Smith School</a> (Popularity: <img src="/resources/pop1.gif"/>): A private school for grades 4-12,  known for its individualized program for reluctant learners, special education   students, and students who  ...<br/>
<a class="linkblue" href="/site/4517284">Green Tree School</a> (Popularity: <img src="/resources/pop1.gif"/>): A state-approved private school serving up to 150 students with emotional disturbance who are between the ages of six and  ...<br/>
<a class="linkblue" href="/site/4517372">Texas School for the Deaf</a> (Popularity: <img src="/resources/pop1.gif"/>): Our environment is designed to provide a community of role models where communication access and accommodations to succeed are available  ...<br/>
<a class="linkblue" href="/site/4517375">Rhode Island  School for the Deaf</a> (Popularity: <img src="/resources/pop1.gif"/>): RISD has been providing educational services to deaf children since 1876.<br/>
</td>
</tr>
</table>
<br/>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Popular Sites</i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="/site/4517306">Oconomowoc Developmental Training Center (ODTC)</a> (Popularity: <img src="/resources/pop1.gif"/>): Provides residential and school programming to children, adolescents and young adults with dually diagnosed developmental disabilities and emotional disturbances.  Oconomowoc,  ...<br/>
<a class="linkblue" href="/site/4517440">Moon Hall School for Dyslexic Children</a> (Popularity: <img src="/resources/pop1.gif"/>): Situated in rural Surrey, near Dorking. "School within a school" on the site of Belmont Preparatory - a relationship believed  ...<br/>
<a class="linkblue" href="/site/4517304">Priory Woods Special School</a> (Popularity: <img src="/resources/pop1.gif"/>): A special school catering to pupils aged 4 to 19 with severe and profound learning difficulties.<br/>
<a class="linkblue" href="/site/4517408">Tucker-Maxon Oral School</a> (Popularity: <img src="/resources/pop1.gif"/>): Learn about programs and educational technology. Features mission statement, news, and contacts. Located in Portland, Oregon.<br/>
<a class="linkblue" href="/site/4517396">Oral Deaf Education</a> (Popularity: <img src="/resources/pop1.gif"/>): The new technology available today through modern hearing aids and cochlear implants provides enough access to sound that, with appropriate  ...<br/>
</td>
</tr>
</table>
<br/>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Related Press</i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="https://press.abc-directory.com/press/5682">Taylor Bus Takes You Beyond School Bus Sales</a> (Popularity: <img src="/resources/pop1.gif"/>): MURRAY, KENTUCKY, Taylor Bus Sales, Inc. a family-owned business which has been around since 1961, announces that it is expanding  ...<br/>
<a class="linkblue" href="https://press.abc-directory.com/press/5909">Taylor Bus Sales Celebrates 49 Years in Business</a> (Popularity: <img src="/resources/pop1.gif"/>): MURRAY, KENTUCKY, Taylor Bus Sales is celebrating its 49th year in business. Why has Taylor been around, and kept on  ...<br/>
<a class="linkblue" href="https://press.abc-directory.com/press/8379">Worcester Polytechnic Institute to Help Develop Novel Sign Language Technology</a> (Popularity: <img src="/resources/pop1.gif"/>): WORCESTER, Mass. - January 08, 2020 -- A Worcester Polytechnic Institute (WPI) researcher has received a $1 million National Science  ...<br/>
<a class="linkblue" href="https://press.abc-directory.com/press/3003">Teen Girl Loses Hair But Gains Support</a> (Popularity: <img src="/resources/pop1.gif"/>): When Peggy Knight, of Peggy Knight Solutions, learned that a young woman was afraid to go to school because she  ...<br/>
<a class="linkblue" href="https://press.abc-directory.com/press/1135">UPS Offering Lucky Fan a Chance to Really Go "Off to the Races</a> (Popularity: <img src="/resources/pop1.gif"/>): UPS (NYSE:UPS) today announced a special sweepstakes program that combines the speed of UPS, NASCAR and the Kentucky Derby. The  ...<br/>
</td>
</tr>
</table>
<br/>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Related Articles</i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="https://article.abc-directory.com/article/4277">Is Kentucky Wine Country?</a> (Popularity: <img src="/resources/pop1.gif"/>): In the great state of Kentucky much of the tobacco farming is phasing out and wine grapes are moving in.  ...<br/>
<a class="linkblue" href="https://article.abc-directory.com/article/5371"> Bhai Parmanand Vidya Mandir Surya Niketan</a> (Popularity: <img src="/resources/pop1.gif"/>): BNPS , Best School in Delhi, Delhi School, CBSE School, Top School, India School, School In India, Senior Secondary School,  ...<br/>
<a class="linkblue" href="https://article.abc-directory.com/article/5384">Bhai Parmanand Vidya Mandir Surya Niketan</a> (Popularity: <img src="/resources/pop1.gif"/>): Best School in india, best school, school india, school in delhi, best school in delhi, delhi public schools, Best School  ...<br/>
<a class="linkblue" href="https://article.abc-directory.com/article/1239">British Sign Language: 6 Quick Facts for Beginners</a> (Popularity: <img src="/resources/pop1.gif"/>): In the last few years, there has been an increasing amount of interest in British Sign Language (BSL). Here are  ...<br/>
<a class="linkblue" href="https://article.abc-directory.com/article/2360">Against All Odds - The Miracle Worker</a> (Popularity: <img src="/resources/pop1.gif"/>): One of the greatest stories on victory and success of the 19th century must be that of Annie Mansfield Sullivan,  ...<br/>
</td>
</tr>
</table>
<br/>
<br/>
</div></td>
</tr>
</table>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" class="footer" width="100%">
<tr>
<td><img height="15" src="/resources/search_tl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_tr.gif" width="15"/></td>
</tr>
<tr>
<td> </td>
<td width="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr align="center">
<td class="copy">
<a class="bottommenu" href="https://www.abc-directory.com/">Home</a>
                  | <a class="bottommenu" href="#">Top</a>  | <a class="bottommenu" href="javascript:;" onclick="this.style.behavior='url(#default#homepage)'; this.setHomePage('https://www.abc-directory.com/');">Set
                  as Homepage</a> |
                  <a class="bottommenu" href="javascript:AddToFavorites('https://www.abc-directory.com/site/4517374', 'Kentucky  School for the Deaf - Provides services from elementary through high school.')">Bookmark this Page</a>
| <a class="bottommenu" href="https://www.abc-directory.com/privacypolicy/">Privacy</a>
 | <a class="bottommenu" href="https://www.abc-directory.com/banners/">Banners</a>
 | <a class="bottommenu" href="https://www.abc-directory.com/contactus/">Contact</a>

                  | <a class="bottommenu" href="https://www.abc-directory.com/submiturl">Submit Site</a>
<br/>
                  © 2003-2019, ABC-Directory.Com. All Rights Reserved
                </td>
</tr>
</table>
</td>
<td> </td>
</tr>
<tr>
<td><img height="15" src="/resources/search_bl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_br.gif" width="15"/></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>