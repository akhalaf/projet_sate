<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>File Not Found | Blockstatus.com</title>
<meta content="Blockstatus, MSN Status Checker, MSN Block Checker, MSN Delete Checker, MSN Nick Changer, MSN Display Pictures, Multi MSN, MSN Tools, MSN Messenger, MSN Messenger 8.5, MSN Messenger 9, NickNames, Yahoo Status Checker, AIM Status Checker, ICQ Status Checker, Anonymous Mailer, Fake Mailer, Proxy Checker, Headers Info, Proxy Info, Server Scanner, Proxy Detector, IP Detector, Headers Detector, MSN, Yahoo, ICQ, AOL, AIM, Tools, Funny Pictures" name="keywords"/>
<meta content="Best source for Messenger Tools, Block Checkers, Display Pictures, Anonymous Mailer, IP and Proxy Tools, Detectors and Downloads, and Tech Blog" name="description"/>
<meta content="en-us" http-equiv="Content-Language"/>
<meta content="index,follow" name="robots"/>
<meta content="2 days" name="revisit-after"/>
<meta content="Avonix.com" name="author"/>
<meta content="Copyright (c) Avonix.com" name="copyright"/>
<meta content="vFp4TIk-53jz8xGE2405-GLxAVXJAEZPc5g5CH4jMeQ" name="google-site-verification"/>
<link href="/style.css" media="screen" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="outer">
<div id="header">
<div id="logo"><a href="/"><img alt="Blockstatus.com" border="0" height="100" src="/images/logo.gif" width="289"/></a></div>
<div id="topLinks">
<ul>
<li><a class="linkHome" href="/">Home</a></li>
<li><a class="linkPrivacy" href="/policy">Privacy Policy</a></li>
<li><a class="linkContact" href="/contact">Contact</a></li>
</ul>
</div>
<div id="topNav">
<ul>
<li><a href="/msn/delete-checker">Delete Checker</a></li>
<li><a href="/msn/stchecker">Block Checker</a></li>
<li><a href="/blog">Blog</a></li>
<li><a href="/yahoo/status-checker">Yahoo Status</a></li>
</ul>
</div>
</div>
<div id="mainTop"> </div>
<div id="mainbody">
<div id="nav">
<div class="navStatus1" id="navHead"><h2 class="navh2h">Status Checkers</h2></div>
<div id="navLinks">
<ul>
<li><a href="/msn/stchecker">MSN Status Checker</a></li>
<li><a href="/yahoo/status-checker">Yahoo Status Checker</a></li>
<li><a href="/aim/status-checker">AIM Status Checker</a></li>
<li><a href="/icq/status-checker">ICQ Status Checker</a></li>
</ul>
</div>
<div id="navLinksB"> </div>
<div class="navmsn1" id="navHead"><h2 class="navh2h">MSN Tools</h2></div>
<div id="navLinks">
<ul>
<li><a href="/msn/delete-checker">MSN Delete Checker</a></li>
<li><a href="/msn/emoticons">MSN Emoticons</a></li>
<li><a href="/msn/display-pictures">MSN Display Pictures</a></li>
</ul>
</div>
<div id="navLinksB"> </div>
<div class="navWebmaster1" id="navHead"><h2 class="navh2h">Miscellaneous</h2></div>
<div id="navLinks">
<ul>
<li><a href="/anonymous-mailer">Anonymous Mailer</a></li>
<li><a href="/ip-proxy-detector">IP / Proxy Detector</a></li>
<li><a href="/headers-detector">Headers Detector</a></li>
<li><a href="/server-scanner">Server Scanner</a></li>
</ul>
</div>
<div id="navLinksB"> </div>
<div class="navFunnypics1" id="navHead"><a href="/funny-pictures"><h2 class="navh2h">Funny Pictures</h2></a></div>
<div id="navLinks">
<ul>
<li><a href="/funny-pictures/animals">Animals</a></li><li><a href="/funny-pictures/kids">Kids</a></li><li><a href="/funny-pictures/miscellaneous">Miscellaneous</a></li><li><a href="/funny-pictures/people">People</a></li><li><a href="/funny-pictures/politics">Politics</a></li><li><a href="/funny-pictures/sports">Sports</a></li><li><a href="/funny-pictures/vehicles">Vehicles</a></li> </ul>
</div>
<div id="navLinksB"> </div>
</div>
<div id="mContents">
<div id="adTop"><!-- BEGIN STANDARD TAG - popup or popunder - blockstatus.com: Run-of-site - DO NOT MODIFY -->
<script src="http://ad.xtendmedia.com/st?ad_type=pop&amp;ad_size=0x0&amp;section=657618&amp;banned_pop_types=28&amp;pop_times=1&amp;pop_frequency=21600" type="text/javascript"></script>
<!-- END TAG --><!-- ValueClick Media 728x90 LEADERBOARD CODE for blockstatus.com -->
<script language="javascript" src="http://media.fastclick.net/w/get.media?sid=21061&amp;tp=5&amp;d=j&amp;t=n"></script>
<noscript><a href="http://media.fastclick.net/w/click.here?sid=21061&amp;c=1" target="_top">
<img border="1" height="90" src="http://media.fastclick.net/w/get.media?sid=21061&amp;tp=5&amp;d=s&amp;c=1" width="728"/></a></noscript>
<!-- ValueClick Media 728x90 LEADERBOARD CODE for blockstatus.com --></div>
<div id="headTitle"><div id="headTitleL"></div><div id="headTitleM"><h1>File Not Found</h1>
<span></span></div><div id="headTitleR"></div></div>
<div id="adBTitle"></div> <div id="mContentsM">
<div id="adContentsLeft"><script type="text/javascript"><!--
google_ad_client = "pub-3327591199001531";
google_ad_slot = "8294277885";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script></div> <div id="mContentsMC">
<div id="mContentsMCBox">
<font size="+1">We're sorry!</font><br/>
<font size="3">The file you requested was not found. Please make sure that you have entered the correct URL.</font>
</div>
<div id="adMiddle"><script type="text/javascript"><!--
google_ad_client = "pub-3327591199001531";
google_ad_slot = "2344217565";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script></div> <div id="adMiddle"><script type="text/javascript"><!--
google_ad_client = "pub-3327591199001531";
google_ad_slot = "2344217565";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js" type="text/javascript">
</script></div>
</div>
</div>
<div id="relatedLinks"><b>Related Links</b><br/>
<a href="/msn/stchecker">MSN Status Checker</a> - <a href="/msn/delete-checker">MSN Delete Checker</a> - <a href="/msn/display-pictures">MSN Display Pictures</a> - <a href="/yahoo/status-checker">Yahoo Status Checker</a> - <a href="/aim/status-checker">AIM Status Checker</a> - <a href="/icq/status-checker">ICQ Status Checker</a> - <a href="/server-scanner">Server Scanner</a> - <a href="/anonymous-mailer">Anonymous Mailer</a>
<div id="relatedLinksBtm"> </div></div>
<div id="relatedLinks"><b>Recent Blog Posts</b><br/>
<a href="/blog/facebook-logos-helping-your-cause">Facebook Logos Helping Your Cause</a> - <a href="/blog/how-to-use-twitter-effectively">How to Use Twitter Effectively</a> - <a href="/blog/top-10-facebook-games-in-2010">Top 10 Facebook Games in 2010</a> - <a href="/blog/the-worst-facebook-status-you-have-ever-seen">The Worst Facebook Status You Have Ever Seen</a> - <a href="/blog/how-to-create-a-cool-facebook-status">How to Create a Cool Facebook Status</a> - 			<div id="relatedLinksBtm"> </div></div>
</div>
<div style="clear: both;"> </div>
</div>
<div id="mainBottom"> </div>
<div id="adFooter"><!-- ValueClick Media 728x90 LEADERBOARD CODE for blockstatus.com -->
<script language="javascript" src="http://media.fastclick.net/w/get.media?sid=21061&amp;tp=5&amp;d=j&amp;t=n"></script>
<noscript><a href="http://media.fastclick.net/w/click.here?sid=21061&amp;c=1" target="_top">
<img border="1" height="90" src="http://media.fastclick.net/w/get.media?sid=21061&amp;tp=5&amp;d=s&amp;c=1" width="728"/></a></noscript>
<!-- ValueClick Media 728x90 LEADERBOARD CODE for blockstatus.com --></div>
<div id="footer"><a href="/">Home</a>     <a href="/contact">Contact Us</a>      <a href="/policy">Terms of Services</a><br/>
This site is not Affiliated in any way with Microsoft corporation, American Online, Yahoo or ICQ.</div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2324352-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
