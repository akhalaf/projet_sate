<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 ltie8 ltie9" lang="en"><![endif]--><!--[if IE 8]><html class="ie ie8 ltie9" lang="en"><![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="initial-scale=1.0" name="viewport"/>
<link href="https://agruf.org/xmlrpc.php" rel="pingback"/>
<title>Alpha Gamma Rho – AGR at University of Florida</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://agruf.org/feed/" rel="alternate" title="Alpha Gamma Rho » Feed" type="application/rss+xml"/>
<link href="https://agruf.org/comments/feed/" rel="alternate" title="Alpha Gamma Rho » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/agruf.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://agruf.org/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/style.css?ver=5.5.3" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&amp;ver=5.5.3" id="Open-Sans-google-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/superfish/css/superfish.css?ver=5.5.3" id="superfish-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/dl-menu/component.css?ver=5.5.3" id="dlmenu-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/font-awesome-new/css/font-awesome.min.css?ver=5.5.3" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/fancybox/jquery.fancybox.css?ver=5.5.3" id="jquery-fancybox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/flexslider/flexslider.css?ver=5.5.3" id="flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/stylesheet/style-responsive.css?ver=5.5.3" id="style-responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/themes/megaproject-v1-05/stylesheet/style-custom.css?ver=5.5.3" id="style-custom-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/plugins/masterslider/public/assets/css/masterslider.main.css?ver=2.14.2" id="ms-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://agruf.org/wp-content/uploads/masterslider/custom.css?ver=1.1" id="ms-custom-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://agruf.org/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://agruf.org/wp-content/plugins/enable-jquery-migrate-helper/js/jquery-migrate-1.4.1-wp.js?ver=1.4.1-wp" type="text/javascript"></script>
<link href="https://agruf.org/wp-json/" rel="https://api.w.org/"/><link href="https://agruf.org/wp-json/wp/v2/pages/287" rel="alternate" type="application/json"/><link href="https://agruf.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://agruf.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<link href="https://agruf.org/" rel="canonical"/>
<link href="https://agruf.org/" rel="shortlink"/>
<link href="https://agruf.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fagruf.org%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://agruf.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fagruf.org%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script>var ms_grabbing_curosr='https://agruf.org/wp-content/plugins/masterslider/public/assets/css/common/grabbing.cur',ms_grab_curosr='https://agruf.org/wp-content/plugins/masterslider/public/assets/css/common/grab.cur';</script>
<meta content="MasterSlider 2.14.2 - Responsive Touch Image Slider" name="generator"/>
<!-- load the script for older ie version -->
<!--[if lt IE 9]>
<script src="https://agruf.org/wp-content/themes/megaproject-v1-05/javascript/html5.js" type="text/javascript"></script>
<script src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/easy-pie-chart/excanvas.js" type="text/javascript"></script>
<![endif]-->
</head>
<body class="home page-template-default page page-id-287 _masterslider _msp_version_2.14.2 header-style-1">
<div class="body-wrapper float-menu" data-home="https://agruf.org">
<!-- top navigation -->
<div class="top-navigation-wrapper">
<div class="top-navigation-container container">
<div class="top-navigation-left">
<div class="top-navigation-left-text"><a href="https://agruf.org" style="display: inline-block; margin-right: 20px;">Home</a><a href="https://agruf.org/resources/faq/" style="display: inline-block; margin-right: 20px;">FAQ</a><a href="https://agruf.org/contact/" style="display: inline-block; margin-right: 20px;">Contact</a></div> </div>
<div class="top-navigation-right">
<div class="top-navigation-right-text">Alpha Gamma Chapter...since 1924</div> <div class="top-social-wrapper">
<div class="social-icon">
<a href="https://www.facebook.com/pages/Alpha-Gamma-Rho-at-the-University-of-Florida/296723137062071?sk=timeline" target="_blank">
<img alt="Facebook" height="32" src="https://agruf.org/wp-content/themes/megaproject-v1-05/images/light/social-icon/facebook.png" width="32"/>
</a>
</div>
<div class="social-icon">
<a href="https://www.linkedin.com/groupInvitation?groupID=94867&amp;sharedKey=33A84325E74E" target="_blank">
<img alt="Linkedin" height="32" src="https://agruf.org/wp-content/themes/megaproject-v1-05/images/light/social-icon/linkedin.png" width="32"/>
</a>
</div>
<div class="social-icon">
<a href="https://twitter.com/AGR_UF" target="_blank">
<img alt="Twitter" height="32" src="https://agruf.org/wp-content/themes/megaproject-v1-05/images/light/social-icon/twitter.png" width="32"/>
</a>
</div>
<div class="clear"></div> </div>
</div>
<div class="clear"></div>
</div>
</div>
<header class="gdlr-header-wrapper">
<div class="gdlr-header-inner">
<div class="gdlr-logo-wrapper">
<div class="gdlr-logo-overlay"></div>
<div class="gdlr-logo-container container">
<!-- logo -->
<div class="gdlr-logo">
<a href="https://agruf.org">
<img alt="" height="125" src="https://agruf.org/wp-content/uploads/2015/08/logo.jpg" width="500"/> </a>
<div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation"><button class="dl-trigger">Open Menu</button><ul class="dl-menu gdlr-main-mobile-menu" id="menu-topnav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-480" id="menu-item-480"><a href="https://agruf.org/collegiate-members/">Collegiate Chapter</a>
<ul class="dl-submenu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-496" id="menu-item-496"><a href="https://agruf.org/collegiate-members/general-information/" title="General Information about AGR at UF">General Information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-499" id="menu-item-499"><a href="https://agruf.org/prospective-member/get-information/" title="Request Info">Get Information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-483" id="menu-item-483"><a href="https://agruf.org/resources/faq/" title="Frequently Asked Questions">F.A.Q.</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-476" id="menu-item-476"><a href="https://agruf.org/alumni/">Alumni</a>
<ul class="dl-submenu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51" id="menu-item-51"><a href="https://agruf.org/alumni/" title="AGR House Association">AGR House Association</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-576" id="menu-item-576"><a href="https://agruf.org/e-news/">E-News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-595" id="menu-item-595"><a href="https://agruf.org/alumni/pay-dues/">Pay Dues</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-484" id="menu-item-484"><a href="https://agruf.org/alumni/foundation/" title="Educational Foundation">Educational Foundation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-501" id="menu-item-501"><a href="https://agruf.org/alumni/doyle-conner-lifetime-achievement-award/">Doyle Conner Lifetime Achievement Award</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-486" id="menu-item-486"><a href="https://agruf.org/about-2/">History, Values &amp; More</a>
<ul class="dl-submenu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-485" id="menu-item-485"><a href="https://agruf.org/about-2/history-of-agr/" title="History of AGR">History of AGR</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-475" id="menu-item-475"><a href="https://agruf.org/about-2/agr-values/" title="AGR Values">AGR Values</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-487" id="menu-item-487"><a href="https://agruf.org/about-2/philanthropy/" title="Philanthropy">Philanthropy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-479" id="menu-item-479"><a href="https://agruf.org/about-2/career-development/" title="Career Development">Career Development</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-488" id="menu-item-488"><a href="https://agruf.org/resources/" title="Resources">Resources</a>
<ul class="dl-submenu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-500" id="menu-item-500"><a href="https://agruf.org/resources/important-links/" title="Links">Important Links</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-551" id="menu-item-551"><a href="https://www.alphagammarho.org/agrconnect" rel="noopener noreferrer" target="_blank" title="National AGR Portal">AGR Connect</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-615" id="menu-item-615"><a href="https://agruf.org/agr-news/">News</a>
<ul class="dl-submenu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-617" id="menu-item-617"><a href="https://agruf.org/agr-news/">AGR News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-580" id="menu-item-580"><a href="https://agruf.org/e-news/">E-News Signup</a></li>
</ul>
</li>
</ul></div> </div>
<div class="logo-right-area">
<div class="header-block-area-wrapper"><div class="header-block-area"><i class="header-block-icon fa "></i><div class="header-block-content"><div class="header-block-title"></div><div class="header-block-caption"></div></div></div><div class="header-block-area"><i class="header-block-icon fa "></i><div class="header-block-content"><div class="header-block-title"></div><div class="header-block-caption"></div></div></div><div class="header-block-area"><i class="header-block-icon fa fa-location-arrow"></i><div class="header-block-content"><div class="header-block-title">Museum Road</div><div class="header-block-caption">Gainesville, FL 32601</div></div></div><div class="clear"></div></div> <div class="clear"></div>
</div>
<div class="clear"></div>
</div>
</div>
<!-- navigation -->
<div id="gdlr-header-substitute"></div><div class="gdlr-navigation-wrapper"><div class="gdlr-navigation-container container"><nav class="gdlr-navigation" id="gdlr-main-navigation" role="navigation"><ul class="sf-menu gdlr-main-menu" id="menu-topnav-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-480menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-480 gdlr-normal-menu"><a class="sf-with-ul-pre" href="https://agruf.org/collegiate-members/">Collegiate Chapter</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-496"><a href="https://agruf.org/collegiate-members/general-information/" title="General Information about AGR at UF">General Information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-499"><a href="https://agruf.org/prospective-member/get-information/" title="Request Info">Get Information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-483"><a href="https://agruf.org/resources/faq/" title="Frequently Asked Questions">F.A.Q.</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-476menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-476 gdlr-normal-menu"><a class="sf-with-ul-pre" href="https://agruf.org/alumni/">Alumni</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51"><a href="https://agruf.org/alumni/" title="AGR House Association">AGR House Association</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-576"><a href="https://agruf.org/e-news/">E-News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-595"><a href="https://agruf.org/alumni/pay-dues/">Pay Dues</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-484"><a href="https://agruf.org/alumni/foundation/" title="Educational Foundation">Educational Foundation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-501"><a href="https://agruf.org/alumni/doyle-conner-lifetime-achievement-award/">Doyle Conner Lifetime Achievement Award</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-486menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-486 gdlr-normal-menu"><a class="sf-with-ul-pre" href="https://agruf.org/about-2/">History, Values &amp; More</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-485"><a href="https://agruf.org/about-2/history-of-agr/" title="History of AGR">History of AGR</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-475"><a href="https://agruf.org/about-2/agr-values/" title="AGR Values">AGR Values</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-487"><a href="https://agruf.org/about-2/philanthropy/" title="Philanthropy">Philanthropy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-479"><a href="https://agruf.org/about-2/career-development/" title="Career Development">Career Development</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-488menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-488 gdlr-normal-menu"><a class="sf-with-ul-pre" href="https://agruf.org/resources/" title="Resources">Resources</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-500"><a href="https://agruf.org/resources/important-links/" title="Links">Important Links</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-551menu-item menu-item-type-custom menu-item-object-custom menu-item-551 gdlr-normal-menu"><a href="https://www.alphagammarho.org/agrconnect" target="_blank" title="National AGR Portal">AGR Connect</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-615menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-615 gdlr-normal-menu"><a class="sf-with-ul-pre" href="https://agruf.org/agr-news/">News</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-617"><a href="https://agruf.org/agr-news/">AGR News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-580"><a href="https://agruf.org/e-news/">E-News Signup</a></li>
</ul>
</li>
</ul><i class="icon-search fa fa-search gdlr-menu-search-button" id="gdlr-menu-search-button"></i>
<div class="gdlr-menu-search" id="gdlr-menu-search">
<form action="https://agruf.org/" id="searchform" method="get">
<div class="search-text">
<input autocomplete="off" data-default="Type Keywords" name="s" type="text" value="Type Keywords"/>
</div>
<input type="submit" value=""/>
<div class="clear"></div>
</form>
</div>
</nav><div class="clear"></div></div></div> <div class="clear"></div>
</div>
</header>
<div class="gdlr-page-title-wrapper">
<div class="gdlr-page-title-overlay"></div>
<div class="gdlr-page-title-container container">
<h1 class="gdlr-page-title">Honoring our Past, Building our Future</h1>
</div>
</div>
<!-- is search --> <div class="content-wrapper">
<div class="gdlr-content">
<!-- Above Sidebar Section-->
<!-- Sidebar With Content Section-->
<div class="with-sidebar-wrapper"><section id="content-section-1"><div class="section-container container"><div class="twelve columns"><div class="gdlr-item gdlr-content-item"><p><img alt="" class="wp-image-810 aligncenter" height="461" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-22-at-2.49.54-PM-1024x433.png" width="1089"/></p>
</div></div><div class="clear"></div><div class="four columns"><div class="gdlr-item gdlr-content-item"><p><a href="https://contributions.omegafi.com/contributions/product/1524-alpha-gamma-rho-florida/"><img alt="Donate" class="alignnone size-full wp-image-811" height="278" src="https://agruf.org/wp-content/uploads/2020/09/1.png" width="600"/></a></p>
</div></div><div class="four columns"><div class="gdlr-item gdlr-content-item"><p><a href="https://givebox.com/1317"><img alt="Donate" class="alignnone size-full wp-image-812" height="278" src="https://agruf.org/wp-content/uploads/2020/09/2.png" width="600"/></a></p>
</div></div><div class="four columns"><div class="gdlr-item gdlr-content-item"><p><a href="https://agruf.org/wp-content/uploads/2019/01/AGR-FL-Campaign-Pledge-Form-FORM.pdf"><img alt="Donate" class="alignnone size-full wp-image-813" height="278" src="https://agruf.org/wp-content/uploads/2020/09/3.png" width="600"/></a></p>
</div></div><div class="clear"></div><div class="six columns"><div class="gdlr-item gdlr-content-item"><p><a href="https://contributions.omegafi.com/contributions/product/1524-alpha-gamma-rho-florida/"><img alt="Donate Today" class="alignnone size-full wp-image-758" height="479" src="https://agruf.org/wp-content/uploads/2020/09/count.png" width="900"/></a></p>
</div></div><div class="six columns"><div class="gdlr-item gdlr-content-item"><h2>Gifts of $2,500 or more will be recognized on a donor wall in the new house.</h2>
<hr/>
<p><a class="gdlr-button medium" href="https://agruf.org/wp-content/uploads/2020/12/agr-fl-donor-list-12.22.20.pdf" style="color:#ffffff; background-color:#666666; " target="_self">DONOR LIST</a>  <a class="gdlr-button medium" href="https://agruf.org/wp-content/uploads/2020/09/Available-Naming-Opportunities-8-17-20.pdf" style="color:#ffffff; background-color:#666666; " target="_self">NAMING OPPORTUNITIES</a></p>
</div></div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><p><img alt="" class="alignnone size-full wp-image-824" height="637" src="https://agruf.org/wp-content/uploads/2020/09/AGR-InfoGraphic.png" width="1601"/></p>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><h4><a href="https://scorpioco.com/agr/" rel="noopener" target="_blank">Click here to view the latest photos from construction site.</a></h4>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><p><strong>3-D Renderings Photo Album</strong></p>
<div class="gdlr-shortcode-wrapper"><div class="gdlr-gallery-item gdlr-item"><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.04.47-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.04.47-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.04.57-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.04.57-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.01.40-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.01.40-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.01-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.01-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.13-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.13-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-1" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.22-PM.png"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/Screen-Shot-2020-09-17-at-3.02.22-PM-150x150.png" width="150"/></a><span class="gallery-caption"></span></div></div><div class="clear"></div></div></div>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><p><strong>Groundbreaking Photo Album</strong></p>
<div class="gdlr-shortcode-wrapper"><div class="gdlr-gallery-item gdlr-item"><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_01.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_01-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_02.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_02-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_03.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_03-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_04.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_04-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_05.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_05-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-2" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_29.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/09/AGR_Groundbreaking_29-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="clear"></div></div></div>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><p><strong>Construction Begins</strong></p>
<div class="gdlr-shortcode-wrapper"><div class="gdlr-gallery-item gdlr-item"><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145738_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145738_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145743_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145743_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145801_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145801_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145804_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145804_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145809_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145809_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="gallery-column two columns"><div class="gallery-item"><a data-fancybox-group="gdlr-gal-3" data-rel="fancybox" href="https://agruf.org/wp-content/uploads/2020/10/20201005_145838_resized_1.jpg"><img alt="" height="150" src="https://agruf.org/wp-content/uploads/2020/10/20201005_145838_resized_1-150x150.jpg" width="150"/></a><span class="gallery-caption"></span></div></div><div class="clear"></div></div></div>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><p><strong>Have a question about the capital campaign?  </strong><br/><strong>Please contact one of these committee members:</strong></p>
<p>Bernie Lester ’58: <a href="mailto:wblester39@aol.com">wblester39@aol.com</a><br/>Ed Smoak ’69: <a href="mailto:esmoak2@gmail.com">esmoak2@gmail.com</a><br/>Tim Garman ’85: <a href="mailto:tgarman@topcropag.com">tgarman@topcropag.com</a><br/>Peter Chaires ’87: <a href="mailto:peter@thechaires.com">peter@thechaires.com</a><br/>Jeff Sumner ’90: <a href="mailto:jeff@sumnerengineering.com">jeff@sumnerengineering.com</a><br/>Angus Williams ’90: <a href="mailto:rwilliams@llw-law.com">rwilliams@llw-law.com</a><br/>Bernie LeFils ’07: <a href="mailto:bernie@lefilscpa.com">bernie@lefilscpa.com</a><br/>Mark Mangen ’12: <a href="mailto:markmangen@gmail.com">markmangen@gmail.com</a></p>
</div><div class="clear"></div><div class="clear"></div><div class="gdlr-item gdlr-content-item"><h1>Nurture. Grow. Give. Repeat.</h1>
</div><div class="clear"></div></div></section></div>
<!-- Below Sidebar Section-->
</div><!-- gdlr-content -->
<div class="clear"></div>
</div><!-- content wrapper -->
<footer class="footer-wrapper">
<div class="footer-container container">
<div class="footer-column twelve columns" id="footer-widget-1">
<div class="widget widget_search gdlr-item gdlr-widget" id="search-2"><div class="gdl-search-form">
<form action="https://agruf.org/" id="searchform" method="get">
<div class="search-text" id="search-text">
<input autocomplete="off" data-default="Type keywords..." id="s" name="s" type="text"/>
</div>
<input id="searchsubmit" type="submit" value=""/>
<div class="clear"></div>
</form>
</div></div> </div>
<div class="clear"></div>
</div>
<div class="copyright-wrapper">
<div class="copyright-container container">
<div class="copyright-left">
					Copyright 2004 - 2020 - Alpha Gamma Rho. All Right Reserved.				</div>
<div class="copyright-right">
					Web Services by <a href="https://www.tmpstyle.com">The Market Place</a> </div>
<div class="clear"></div>
</div>
</div>
</footer>
</div> <!-- body-wrapper -->
<script type="text/javascript"></script><script id="superfish-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/superfish/js/superfish.js?ver=1.0" type="text/javascript"></script>
<script id="hoverIntent-js" src="https://agruf.org/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script id="modernizr-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/dl-menu/modernizr.custom.js?ver=1.0" type="text/javascript"></script>
<script id="dlmenu-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/dl-menu/jquery.dlmenu.js?ver=1.0" type="text/javascript"></script>
<script id="jquery-easing-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/jquery.easing.js?ver=1.0" type="text/javascript"></script>
<script id="jquery-fancybox-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/fancybox/jquery.fancybox.pack.js?ver=1.0" type="text/javascript"></script>
<script id="jquery-fancybox-media-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/fancybox/helpers/jquery.fancybox-media.js?ver=1.0" type="text/javascript"></script>
<script id="jquery-fancybox-thumbs-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/fancybox/helpers/jquery.fancybox-thumbs.js?ver=1.0" type="text/javascript"></script>
<script id="flexslider-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/plugins/flexslider/jquery.flexslider.js?ver=1.0" type="text/javascript"></script>
<script id="gdlr-script-js" src="https://agruf.org/wp-content/themes/megaproject-v1-05/javascript/gdlr-script.js?ver=1.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://agruf.org/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>