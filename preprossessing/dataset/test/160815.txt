<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<title>ASSL Cagliari - Home page</title>
<meta content="fexYwocYa-sOH7mQpBusNvZwXzILRdA-vH7ejrHwZDc" name="google-site-verification"/>
<link href="https://fonts.googleapis.com/css?family=Lato:900" rel="stylesheet" type="text/css"/>
<link href="//www.aslcagliari.it/xsl/css/css_2.css" rel="stylesheet" title="principale" type="text/css"/>
<link href="//www.aslcagliari.it/xsl/css/css_51.css" rel="stylesheet" title="principale" type="text/css"/>
<link href="//www.aslcagliari.it/xsl/css/css_2.css" media="print" rel="stylesheet" type="text/css"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<script src="//code.jquery.com/jquery-3.3.1.min.js" type="text/javascript">var dummy=0;</script>
<script src="//code.jquery.com/jquery-migrate-3.0.1.js" type="text/javascript">var dummy=0;</script>
<script src="//www.regione.sardegna.it/js/jquery.cookiesdirective.js" type="text/javascript">var dummy=0;</script>
<script src="//www.aslcagliari.it/js/script.js" type="text/javascript">
            var dummy=0; // per IE (altrimenti non visualizza la pagina)
         </script>
<script type="text/javascript">
            $(document).ready(function(){
            if($.isFunction($.cookiesDirective)){
            $.cookiesDirective({
            privacyPolicyUri: 'http://www.regione.sardegna.it/privacy.html',// uri of your privacy policy
            privacyPolicyTitle: 'Informativa cookies',// uri of your privacy policy
            explicitConsent: false, // true // false allows implied consent
            position: 'bottom', // top or bottom of viewport
            duration: 100, // display time in seconds
            limit: 0, // limit disclosure appearances, 0 is forever
            message: "Questo sito utilizza i cookies per migliorare l'esperienza di navigazione. Continuando la navigazione accetti l'utilizzo dei cookies. ", // customise the disclosure message
            cookieScripts: null, // disclose cookie settings scripts
            fontFamily: 'helvetica', // font style for disclosure panel
            fontColor: '#FFFFFF', // font color for disclosure panel
            fontSize: '13px', // font size for disclosure panel
            backgroundColor: '#000000', // background color of disclosure panel
            backgroundOpacity: '85', // opacity of disclosure panel
            linkColor: '#ffffff' // link color in disclosure panel
            });
            }
            });
         </script>
<script type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
               ga('create', 'UA-1692395-8', 'auto');
            
            ga('set', 'anonymizeip', true);
            ga('send', 'pageview');
         </script>
</head>
<!--13/01/2021 12:40:21-->
<body>
<div class="container">
<!--SEZIONE HEADER : INIZIO-->
<div>
<a class="hidden" href="#content" tabindex="1" title="vai al contenuto della pagina">Vai al contenuto della pagina</a>
</div>
<div class="header">
<div class="header pos">
<div class="pos logo">
<img alt="ASSL Cagliari" src="/immagini/12_132_20161214123347.svg" style="width: 45%; margin-top:-8px; margin-bottom:8px;" title="ASSL Cagliari"/>
</div>
<div class="pos logo-reg" style="margin-top: 12px;">
<div>
<a href="http://www.regione.sardegna.it">
<img alt="Logo Regione Autonoma della Sardegna" src="//www.regione.sardegna.it/img/logo-reg.gif" title="Regione Autonoma della Sardegna"/>
</a>
</div>
<div class="sbds">
<a class="titolinotopbar" href="//www.sardegnasalute.it">SISTEMA SANITARIO DELLA SARDEGNA</a>
</div>
</div>
</div>
<hr class="hidden"/>
<a class="hidden" id="first">Salta al menu</a>
<ul id="navbar">
<li id="litbs8v1s0">
<a accesskey="1" href="/chisiamo/" title="Vai alla pagina: Chi siamo - (ALT+1)">Chi siamo</a>
</li>
<li id="litbs8v2s0">
<a accesskey="2" href="/aslinforma/" title="Vai alla pagina: Asl informa - (ALT+2)">Asl informa</a>
</li>
<li id="litbs8v3s0">
<a accesskey="3" href="//www.aslcagliari.it/index.php?xsl=12&amp;s=8&amp;v=9&amp;c=3239&amp;nodesc=2" title="Vai alla pagina: Albo pretorio - (ALT+3)">Albo pretorio</a>
</li>
<li id="litbs8v4s0">
<a accesskey="4" href="/argomenti/" title="Vai alla pagina: Argomenti - (ALT+4)">Argomenti</a>
</li>
<li id="litbs8v5s0">
<a accesskey="5" href="/servizicittadino/" title="Vai alla pagina: Servizi al cittadino - (ALT+5)">Servizi al cittadino</a>
</li>
<li id="litbs8v6s0">
<a accesskey="6" href="/servizisanitari/" title="Vai alla pagina: Servizi sanitari - (ALT+6)">Servizi sanitari</a>
</li>
<li id="litbs8v7s0">
<a accesskey="7" href="/dipartimenti/" title="Vai alla pagina: Dipartimenti - (ALT+7)">Dipartimenti</a>
</li>
<li id="litbs8v8s0">
<a accesskey="8" href="/distretti/" title="Vai alla pagina: Distretti - (ALT+8)">Distretti</a>
</li>
</ul>
<div class="navbar-bottom pos">
<ul class="time pos lrg">
<li class="pos lrg">  </li>
</ul>
</div>
</div>
<!--SEZIONE HEADER : FINE-->
<!--//www.aslcagliari.it/editoriale/visxml.php?xx=1&s=8&v=9&cn=Box+in+HP&na=1&n=20&t=1-->
<div class="menu-left ht-space r-space leftbar">
<ul>
<li class="column">
<div class="column ht-space">
<h2>
<a href="/chisiamo/">CHI SIAMO</a>
</h2>
</div>
<ul>
<li>
<a href="/chisiamo/azienda.html">Azienda</a>
</li>
<li>
<a href="/chisiamo/pianostrategico.html">Programmazione aziendale</a>
</li>
<li>
<a href="/chisiamo/cartadeiservizi.html">Carta dei servizi</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=3260&amp;esn=Primo+piano&amp;na=1&amp;n=10">Posta elettronica certificata</a>
</li>
<li class="transparenza sfondibackground">
<a href="/amministrazionetrasparente/">Amministrazione Trasparente</a>
</li>
</ul>
</li>
</ul>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="/aslinforma/">ASL INFORMA</a>
</h2>
</div>
<ul>
<li>
<a href="/aslinforma/notizie.html">Notizie</a>
</li>
<li>
<a href="/aslinforma/communicati.html">Ufficio stampa</a>
</li>
<li>
<a href="/aslinforma/galleriafotografica/">Galleria fotografica</a>
</li>
<li>
<a href="/aslinforma/formazione.html">Corsi e formazione</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=2820&amp;esn=Primo+piano&amp;na=1&amp;n=10">Relazioni sindacali</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=2821&amp;esn=Primo+piano&amp;na=1&amp;n=10">Il Comitato Consultivo Zonale di Cagliari - Carbonia - Sanluri</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4727&amp;esn=Primo+piano&amp;na=1&amp;n=10">Specialisti ambulatoriali, veterinari e altre professionalitÃ  sanitarie</a>
</li>
</ul>
</li>
</ul>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="//www.aslcagliari.it/index.php?xsl=12&amp;s=8&amp;v=9&amp;c=3239&amp;nodesc=2">ALBO PRETORIO</a>
</h2>
</div>
<ul>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4828">Determine Direttore ASSL</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=17&amp;s=8&amp;v=9&amp;c=3242&amp;n=10&amp;nodesc=1&amp;tb=13&amp;tipo=1">Delibere in pubblicazione ex Asl Cagliari</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=17&amp;s=8&amp;v=9&amp;c=2655&amp;n=10&amp;nodesc=1&amp;tipo=1">Determine in pubblicazione</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=4720&amp;n=10&amp;nodesc=1&amp;tipodoc=11">Indagini di mercato</a>
</li>
<li>
<a href="/aslinforma/bandi/">Bandi e gare</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=3328&amp;n=10&amp;nodesc=1&amp;tipodoc=4">Procedure negoziate </a>
</li>
<li>
<a href="/aslinforma/concorsi/">Concorsi e selezioni</a>
</li>
<li>
<a href="http://www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=4486&amp;n=10&amp;nodesc=1&amp;tipodoc=6">Selezioni a tempo determinato</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=3266&amp;esn=Primo+piano&amp;na=1&amp;n=10">Avvisi e comunicazioni</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=2826&amp;esn=Primo+piano&amp;na=1&amp;n=10">Regolamenti</a>
</li>
<li>
<a href="http://www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=4487&amp;n=10&amp;nodesc=1&amp;tipodoc=7">Avvisi di mobilitÃ </a>
</li>
<li>
<a href="http://www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=4488&amp;n=10&amp;nodesc=1&amp;tipodoc=8">Avvisi per collaborazioni e consulenze</a>
</li>
<li>
<a href="http://www.aslcagliari.it/index.php?xsl=16&amp;v=9&amp;s=8&amp;c=4489&amp;n=10&amp;nodesc=1&amp;tipodoc=9">Avvisi per l'attribuzione di incarichi</a>
</li>
</ul>
</li>
</ul>
<div class="column t-space">
<a href="http://www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=3260&amp;esn=Primo+piano&amp;na=1&amp;n=10" title="Posta elettronica certificata">
<img alt="posta elettronica certificata" class="home-img2" src="/immagini/8_130_20160512155043.png" title="posta elettronica certificata"/>
</a>
</div>
<div class="column t-space">
<a href="//www.aslcagliari.it/servizicittadino/modulistica.html" title="Modulistica">
<img alt="modulistica" class="home-img2" src="/immagini/8_151_20150604110209.gif" title="modulistica"/>
</a>
</div>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="/argomenti/">ARGOMENTI</a>
</h2>
</div>
<ul>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4695&amp;esn=Primo+piano&amp;na=1&amp;n=10">Tessera sanitaria</a>
</li>
<li>
<a href="/argomenti/vacinazioni/">Vaccinazioni</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=2973&amp;esn=Primo+piano&amp;na=1&amp;n=10">QualitÃ  e rischio clinico</a>
</li>
<li>
<a href="/argomenti/farmaci/">I farmaci</a>
</li>
<li>
<a href="/argomenti/screeningoncologici/">Screening oncologici</a>
</li>
<li>
<a href="/argomenti/diabete/">Diabete</a>
</li>
<li>
<a href="/argomenti/talassemia/">Talassemia</a>
</li>
<li>
<a href="/argomenti/sicurezzaalimentare/">Sicurezza alimentare</a>
</li>
<li>
<a href="/argomenti/sanitaanimale/">SanitÃ  animale</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=3300&amp;esn=Primo+piano&amp;na=1&amp;n=10">Progetti PASSI</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=5025&amp;esn=Primo+piano&amp;na=1&amp;n=10">Okkio alla  Salute</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=3303&amp;esn=Primo+piano&amp;na=1&amp;n=10">Accreditamento</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4791&amp;esn=Primo+piano&amp;na=1&amp;n=10">Piano di Emergenza per il Massiccio Afflusso di feriti </a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4973&amp;esn=Primo+piano&amp;na=1&amp;n=10">Pdta</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=5158&amp;esn=Primo+piano&amp;na=1&amp;n=10">Regolamenti Reach e Clp</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=5269&amp;esn=Primo+piano&amp;na=1&amp;n=10">Procreazione medicalmente assistita-PMA</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=5357&amp;esn=Primo+piano&amp;na=1&amp;n=10">Turni reperibilitÃ </a>
</li>
</ul>
</li>
</ul>
<div class="column t-space">
<a href="//www.aslcagliari.it/servizicittadino/urp.html" title="URP">
<img alt="banner urp" class="home-img2" src="/immagini/8_151_20181031092747.jpg" title="banner urp"/>
</a>
</div>
</div>
<div class="nrm-center t-space r-space">
<span id="content"></span>
<div class="nrm-center"><a href="//www.aslcagliari.ithttps://www.aslcagliari.it/index.php?xsl=7&amp;s=72113&amp;v=2&amp;c=288"><img alt="screening ogliastra" class="home-img" src="/immagini/8_136_20210111134330.jpg" title="screening ogliastra"/></a><hr class="hidden"/><a class="title8 block" href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72113&amp;v=2&amp;c=288">Covid, Temussi in Ogliastra: "Azione sinergica di screening sul territorio e vaccini per uscire dalla pandemia"</a>E' partita questa mattina la fase due dello screening di massa avviato da Ares ATS Sardegna nei 23 comuni dell'Ogliastra per la verifica dei cittadini positivi al covid-19.
<br/>"La popolazione sta rispondendo in maniera straordinaria anche oggi - spiega Massimo Temussi, coordinatore della task force - cosÃ¬ come l'organizzazione messa su dai sindaci dell'Ogliastra. Questa Ã¨ una fase molto importante perchÃ©, unitamente all'inizio del processo di vaccinazioni anti covid avviato in Sardegna, che in questo momento ci vede al quinto posto tra le regioni italiane per numero di vaccinati, cerca di raggiungere in maniera sinergica gli obiettivi di contrasto alla diffusione della pandemia"."L'obiettivo dello screening di massa, che risponde agli intenti del presidente Solinas - prosegue Temussi - Ã¨ quello di abbattere l'indice di contagio e bloccare le catene di trasmissione. Non dobbiamo abbassare la guardia, ma Ã¨ fondamentale spostare il monitoraggio sui territori, evitando cosÃ¬ che i cittadini positivi arrivino in ospedale quando ormai Ã¨ troppo tardi".
<br/></div>
<div class="nrm-center t-space">
<hr class="hidden"/>
<div class="column"><hr class="hidden"/><a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72041&amp;v=2&amp;c=288"><img alt="368ginecologia ss trinitÃ .jpg" class="thumbnail left" src="/immagini/8_136_20210104115826.jpg" title="368ginecologia ss trinitÃ .jpg"/></a><a class="title5 block" href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72041&amp;v=2&amp;c=288">Tre fiocchi al SS. TrinitÃ  di Cagliari: due maschietti e una femminuccia </a>Tre fiocchi, tutti con parto naturale, nel reparto di Ginecologia e Ostetricia dell'ospedale SS TrinitÃ  di Cagliari, diretto dalla Dott. ssa Eleonora Coccolone, per chiudere l'anno 2020 e aprire il 2021. L'ultimo nato, nel 2020, si chiama Michele ed Ã¨ nato alle 20.23. Tutti i bimbi stanno bene e sono insieme alle mamme nel rooming-in. Nella foto NicolÃ², con mamma Giulia.</div>
<div class="column l-space"><hr class="hidden"/><a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72043&amp;v=2&amp;c=288&amp;serial_session=YTowOnt9&amp;aclang=it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7&amp;httphst=www.aslcagliari.it"><img alt="vaccini covid" class="thumbnail left" src="/immagini/8_136_20210104123258.jpg" title="vaccini covid"/></a><a class="title5 block" href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72043&amp;v=2&amp;c=288&amp;serial_session=YTowOnt9&amp;aclang=it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7&amp;httphst=www.aslcagliari.it">Vaccine Day, al Binaghi di Cagliari i primi vaccinati</a>L'Ospedale Binaghi, negli ambulatori del SISP, il Capodanno simbolicamente con al speranza di liberare l'anno 2021 dalla panedmia. Patrizia Loi, infermiera del Servizio di Radiologia del PO SS. TrinitÃ , 53 anni, e Sergio Marracini, 64 anni, direttore sanitario, sono i primi due operatori che si sono sottoposti al vaccino in ATS Sardegna nel presidio ospedaliero appena trasformato in ospedale Covid.</div>
</div>
<div class="nrm-center t-space">
<hr class="hidden"/>
<hr class="hidden"/>
<a href="http://www.atssardegna.it/">
<img alt="ATS" class="home-img" src="/immagini/8_151_20170110123151.png" title="ATS"/>
</a>
</div>
<!--//www.aslcagliari.it/editoriale/visxml.php?xx=1&s=8&v=9&cn=Ultime+Notizie&na=1&n=9&nodesc=1-->
<div class="nrm-center t-space"><span class="azzurrino">ULTIME NOTIZIE</span><br/>11.01.21 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72114&amp;v=2&amp;c=288">Trasferimento Servizio Farmaceutico di Cagliari: numeri a cui rivolgersi per informazioni
<br/></a><br/>05.01.21 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=72050&amp;v=2&amp;c=288">Vaccinazioni anti covid19: modulo consenso e nota informativa</a><br/>29.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71983&amp;v=2&amp;c=288">"Sardi e sicuri": campagna sui media locali</a><br/>28.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71967&amp;v=2&amp;c=288">Specialistica ambulatoriale: on line le graduatorie valevoli per il 2021</a><br/>23.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71934&amp;v=2&amp;c=288">CPSI Infermieri dipendenti ASSL Cagliari: manifestazione di interesse</a><br/>22.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71920&amp;v=2&amp;c=288">Il Direttore della ASSL di Cagliari augura Buon Natale a tutto il personale sanitario, tecnico e amministrativo</a><br/>22.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71914&amp;v=2&amp;c=288">Convocazione assemblee elettorali Dipartimenti Area medica e chirurgica ASSL di Cagliari</a><br/>22.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71903&amp;v=2&amp;c=288">Controlli sulle autocertificazioni: avviso dei Distretti</a><br/>22.12.20 <a href="https://www.aslcagliari.it/index.php?xsl=7&amp;s=71899&amp;v=2&amp;c=288">Controlli autocertificazioni: avviso del Distretto di Quartu</a><br/></div>
</div>
<div class="menu-left ht-space">
<div class="column ht-space">
<a href="//www.aslcagliari.it/index.php?xsl=7&amp;s=69046&amp;v=2&amp;c=288" title="Conto corrente ATS Sardegna">
<img alt="banner iban" class="home-img2" src="/immagini/8_151_20200331165630.png" title="banner iban"/>
</a>
</div>
<ul>
<li class="column">
<div class="column ht-space">
<h2>
<a href="/servizicittadino/">SERVIZI AL CITTADINO</a>
</h2>
</div>
<ul>
<li class="ht-space">
<a href="/servizicittadino/cup.html">Cup - Centro unico di prenotazione</a>
</li>
<li>
<a href="/servizicittadino/118.html">118</a>
</li>
<li>
<a href="/servizicittadino/prontosoccorso.html">Pronto soccorso</a>
</li>
<li>
<a href="/servizicittadino/pua.html">Pua - Punto unico d'accesso</a>
</li>
<li>
<a href="/servizicittadino/urp.html">Urp - Ufficio relazioni con il pubblico</a>
</li>
<li>
<a href="/servizicittadino/ticket.html">Ticket</a>
</li>
<li>
<a href="/servizicittadino/guardiemediche.html">Guardie mediche</a>
</li>
<li>
<a href="/servizicittadino/guardieturistiche.html">Guardie turistiche</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=31&amp;s=8&amp;v=9&amp;c=453&amp;esn=Primo+piano&amp;na=1&amp;n=10&amp;spec=1">Medici di medicina generale</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=31&amp;s=8&amp;v=9&amp;c=454&amp;esn=Primo+piano&amp;na=1&amp;n=10&amp;spec=2">Pediatri di libera scelta</a>
</li>
<li>
<a href="/servizicittadino/farmacie.html">Farmacie</a>
</li>
<li>
<a href="/servizicittadino/modulistica.html">Modulistica</a>
</li>
<li>
<a href="/servizicittadino/struttureconvenzionate/">Strutture convenzionate</a>
</li>
<li>
<a href="/servizicittadino/comefareper/">Come fare per</a>
</li>
</ul>
</li>
</ul>
<div class="column t-space">
<a href="//www.aslcagliari.it/index.php?xsl=7&amp;s=69178&amp;v=2&amp;c=288" title="Supporto psicologico">
<img alt="banner supporto psicologico" class="home-img2" src="/immagini/8_151_20200403121238.png" title="banner supporto psicologico"/>
</a>
</div>
<div class="column">
<a href="https://cup.sardegnasalute.it/" rel="blank">
<img alt="cup web" class="home-img2" src="/immagini/8_130_20150203102053.png" title="cup web"/>
</a>
</div>
<div class="column t-space ric-form-margin-b">
<form action="/index.php" class="classeformhp" id="search3" method="get">
<div>
<input name="xsl" type="hidden" value="52"/>
<input name="v" type="hidden" value="7"/>
<input name="s" type="hidden" value="8"/>
<input name="ftt" type="hidden" value="1"/>
<input name="ftb" type="hidden" value="1"/>
<input name="fte" type="hidden" value="1"/>
<input name="na" type="hidden" value="1"/>
<input name="n" type="hidden" value="10"/>
<input name="es" type="hidden" value="271,272,273,274,275,276"/>
<label for="idkey">
<img alt="Cerca nel sito" class="g-dim block" src="/immagini/8_1_20071009173306.gif" title="Cerca nel sito"/>
</label>
<input class="ric-form-input" id="idkey" name="k" onfocus="if(this.value=='inserisci testo')this.value=''" title="Inserisci qui i termini da ricercare" type="text" value="inserisci testo"/>
<input class="ric-form-input2" type="submit" value="vai"/>
</div>
</form>
</div>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="/servizisanitari/">SERVIZI SANITARI</a>
</h2>
</div>
<ul>
<li>
<a href="/servizisanitari/ospedali/">Ospedali</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=18&amp;s=8&amp;v=9&amp;c=2688&amp;es=639,301&amp;&amp;esn=Primo+piano&amp;na=1&amp;n=10">Assistenza ambulatoriale</a>
</li>
<li>
<a href="/servizicittadino/hospice.html">Hospice</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=18&amp;s=8&amp;v=9&amp;c=2986&amp;es=639,301&amp;esn=Primo+piano&amp;na=1&amp;n=10">Centro donna Cagliari</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=7&amp;s=49623&amp;v=2&amp;c=621">Centro donna Isili</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=18&amp;s=8&amp;v=9&amp;c=3327&amp;es=639,301&amp;esn=Primo+piano&amp;na=1&amp;n=10">Centro TAO</a>
</li>
<li>
<a href="/servizisanitari/medicinadibasespecialistica.html">Cure primarie</a>
</li>
<li>
<a href="/servizisanitari/materno.html">Consultori</a>
</li>
<li>
<a href="/servizisanitari/serd.html">Dipendenze - SerD</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=4714&amp;esn=Primo+piano&amp;na=1&amp;n=10">Laboratorio di Tossicologia</a>
</li>
<li>
<a href="/servizisanitari/assistenzasociosanitaria.html">Rete socio-sanitaria</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=18&amp;s=8&amp;v=9&amp;c=3551&amp;es=639,301&amp;esn=Primo+piano&amp;na=1&amp;n=10">Cure renali e dialisi</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=89&amp;v=9&amp;s=8&amp;n=10&amp;na=1&amp;c=4704&amp;spec=&amp;intra=1">Libera Professione</a>
</li>
</ul>
</li>
</ul>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="/dipartimenti/">DIPARTIMENTI</a>
</h2>
</div>
<ul>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=21&amp;s=8&amp;v=9&amp;c=2660&amp;esn=Primo+piano&amp;na=1&amp;n=10">Salute mentale</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=21&amp;s=8&amp;v=9&amp;c=2661&amp;esn=Primo+piano&amp;na=1&amp;n=10">Prevenzione</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=15&amp;s=8&amp;v=9&amp;c=2687&amp;esn=Primo+piano&amp;na=1&amp;n=10">Acquisti servizi sanitari</a>
</li>
<li>
<a href="//www.aslcagliari.it/index.php?xsl=21&amp;s=8&amp;v=9&amp;c=2757&amp;esn=Primo+piano&amp;na=1&amp;n=10">Farmaco</a>
</li>
</ul>
</li>
</ul>
<ul>
<li class="column">
<div class="column t-space">
<h2>
<a href="/distretti/">DISTRETTI</a>
</h2>
</div>
<ul>
<li>
<a href="/distretti/cagliariareavasta.html">Cagliari - Area Vasta</a>
</li>
<li>
<a href="/distretti/cagliariovest.html">Area Ovest</a>
</li>
<li>
<a href="/distretti/quartu.html">Quartu-Parteolla</a>
</li>
<li>
<a href="/distretti/muravera.html">Sarrabus-Gerrei</a>
</li>
<li>
<a href="/distretti/sarcidano_barbagia.html">Sarcidano - Barbagia di Seulo e Trexenta</a>
</li>
</ul>
</li>
</ul>
<div class="column">
<a href="https://fse.sardegnasalute.it/" rel="blank">
<img alt="banner fascicolo sanitario elettronico" class="home-img2" src="/immagini/8_151_20160512150753.png" title="banner fascicolo sanitario elettronico"/>
</a>
</div>
</div>
<hr class="hidden"/>
<!--SEZIONE FOOTER : INIZIO-->
<div class="footer">
<div class="footer-div">
<span class="left">©</span>
<a class="left" href="/index.php?xsl=98&amp;s=8&amp;v=9&amp;c=260&amp;nodesc=3&amp;n=9&amp;bs1=4&amp;bs2=3&amp;bs3=3&amp;bd2=0&amp;bd1=1&amp;bd4=1&amp;bd3=5&amp;httphst=asl.regione.sardegna.it"> </a>
<span class="left">2021 Regione Autonoma della Sardegna</span>
</div>
</div>
<!--//www.aslcagliari.it/editoriale/visxml.php?xx=2&s=8&cn=footer&v=9&na=1-->
<div class="lower-footer">
<ul>
<li>
<a class="hidden" href="#first" id="nineth">Salta alla barra superiore</a>
</li>
<li>
<a class="t-link no-padding-left" href="//www.aslcagliari.it/index.php?xsl=8&amp;s=8&amp;v=9&amp;c=294&amp;nodesc=1" title="mappa">mappa</a>
</li>
<li>
<a class="t-link" href="//www.aslcagliari.it/index.php?xsl=9&amp;s=8&amp;v=9&amp;c=295&amp;na=1&amp;n=10" title="note legali">note legali</a>
</li>
<li>
<a class="t-link" href="https://form.agid.gov.it/view/906b9fe6-3380-45c2-abcf-0add6c7e1436" rel="blank" title="dichiarazione di accessibilitÃ ">dichiarazione di accessibilitÃ </a>
</li>
<li>
<a class="ultvocefoot" href="//www.aslcagliari.it/index.php?xsl=9&amp;s=8&amp;v=9&amp;c=296&amp;na=1&amp;n=10" title="contattaci">contattaci</a>
</li>
</ul>
</div>
<!--SEZIONE FOOTER : FINE-->
<hr class="hidden"/>
</div>
</body>
</html>
