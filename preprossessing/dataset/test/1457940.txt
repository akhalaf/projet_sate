<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Life in the rearview - Objects are closer than they appear" name="description"/>
<meta content="" name="keywords"/>
<link href="https://www.toddweiser.com/wp-content/themes/tiju-13/style.css" rel="stylesheet" type="text/css"/>
<link href="https://www.toddweiser.com/?feed=rss2" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="https://www.toddweiser.com/?feed=rss" rel="alternate" title="RSS .92" type="text/xml"/>
<link href="https://www.toddweiser.com/?feed=atom" rel="alternate" title="Atom 0.3" type="application/atom+xml"/>
<link href="https://www.toddweiser.com/xmlrpc.php" rel="pingback"/>
<title>Life in the rearview</title>
<meta content="0.64" name="NextGEN"/>
<style media="screen" type="text/css">@import "http://www.toddweiser.com/wp-content/plugins/nggallery/css/Black_Minimalism .css";</style>
<script type="text/javascript"> var tb_pathToImage = "http://www.toddweiser.com/wp-content/plugins/nggallery/thickbox/loadingAnimationv3.gif";</script>
<style media="screen" type="text/css">@import "http://www.toddweiser.com/wp-content/plugins/nggallery/thickbox/thickbox.css";</style>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.toddweiser.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<script src="https://www.toddweiser.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.toddweiser.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.toddweiser.com/wp-includes/js/swfobject.js?ver=2.2-20120417" type="text/javascript"></script>
<link href="https://www.toddweiser.com/index.php?rest_route=/" rel="https://api.w.org/"/>
<link href="https://www.toddweiser.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.toddweiser.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
</head>
<body class="waterbody">
<script type="text/javascript">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              </script>
<div align="center">
<div id="container">
<div id="containerbg">
<div id="outerleft">
<!-- start header text -->
<div id="logo">
<h1> <a href="https://www.toddweiser.com">Life in the rearview</a></h1> <p> # Objects are closer than they appear 
</p>
</div>
<!-- end header text -->
<!-- start image header -->
<div id="imgheader">
<img alt="header image" src="https://www.toddweiser.com/wp-content/themes/tiju-13/images/img_header.jpg"/>
</div>
<!-- end image header -->
<!-- start top menu -->
<div id="topmenu">
<ul id="mainlevel-nav">
<li class=""><a href="https://www.toddweiser.com">home</a></li>
<li class="page_item page-item-3"><a href="https://www.toddweiser.com/?page_id=3">resume</a></li>
<li class="page_item page-item-2"><a href="https://www.toddweiser.com/?page_id=2">about</a></li>
<li class="page_item page-item-4"><a href="https://www.toddweiser.com/?page_id=4">gallery</a></li>
</ul>
</div>
<!-- end top menu.  -->
<div id="container_inner">
<!-- start left column. -->
<div id="leftcol">
<!-- Photos Begin -->
<ul>
Under Progress
<!-- You may want to put your Flickr Stream here -->
<!--- To create a Flickr Badge please visit www.flickr.com --->
</ul>
<!-- Photos End -->
<!-- Archives Begin -->
<table cellpadding="0" cellspacing="0" class="moduletable">
<tbody><tr>
<th valign="top"> <img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> Archives </th></tr>
<tr><td> <ul>
</ul>
</td> </tr> </tbody></table>
<!-- archives End -->
<!-- Links Begin -->
<table cellpadding="0" cellspacing="0" class="moduletable">
<tbody><tr> <th valign="top"> <img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> Highlights </th></tr>
<tr><td>
<ul>
<a href=""><!--Highlighted articles of your choice --></a><br/>
<li><a href="">...</a></li>
<li><a href="">...</a></li>
<li><a href="">...</a></li>
<li><a href="">...</a></li>
<li><a href="">...</a></li>
</ul>
</td> </tr> </tbody></table>
<table cellpadding="0" cellspacing="0" class="moduletable">
<tbody><tr> <th valign="top"> <img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> More... </th></tr>
<tr><td>
<ul>
<!-- Your content here -->
</ul>
</td> </tr> </tbody></table>
<!-- Links End -->
</div>
<!-- end left column. -->
<div id="content_main">
<table border="0" cellpadding="0" cellspacing="0" width="519">
<tbody><tr><td>
<table cellpadding="0" cellspacing="0" class="blog">
<tbody><tr><td valign="top">
<div>
<table class="contentpaneopen">
<tbody>
<tr> <td>
<h2>Not found!</h2>
<p>Could not find the requested page. Use the navigation menu to find your target, or use the search box below:</p>
<div align="center"><form action="/index.php" id="searchform" method="get">
<div style="padding-left:4px;">
<br/>
<label for="s"></label>
<input id="s" name="s" size="17" style="background:#333333; color:#ffffff;" type="text" value=""/>
<input id="searchsubmit" type="hidden" value="Search"/>
</div></form></div>
</td></tr>
</tbody></table>
</div>
</td></tr></tbody></table>
</td></tr></tbody></table> </div>
<!-- end main body -->
</div></div>
<div id="outerright">
<!-- start right top header search box DO NOT EDIT.  -->
<div id="rightcol_top">
<table cellpadding="0" cellspacing="0" class="moduletable"><tbody><tr><td>
<div align="center"><form action="/index.php" id="searchform" method="get">
<div style="padding-left:4px;">
<br/>
<label for="s"></label>
<input id="s" name="s" size="17" style="background:#333333; color:#ffffff;" type="text" value=""/>
<input id="searchsubmit" type="hidden" value="Search"/>
</div></form></div>
</td></tr></tbody></table>
</div>
<!-- end right top header Search BOX.-->
<!-- start right column. -->
<div id="rightcol">
<table cellpadding="0" cellspacing="0" class="moduletable"><tbody><tr><th valign="top">
<img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> About
</th></tr><tr><td>

This page is currently a work in progress and should be available shortly.

</td></tr></tbody></table>
<table cellpadding="0" cellspacing="0" class="moduletable"><tbody><tr><th valign="top">
<img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> Recent Posts

</th></tr><tr><td>
<ul>
</ul>
</td></tr></tbody></table>
<table cellpadding="0" cellspacing="0" class="moduletable"><tbody><tr><th valign="top">
<img src="https://www.toddweiser.com/wp-content/themes/Tiju/images/right.gif"/> Recent Commentors

</th></tr><tr><td>
<ul></ul></td></tr></tbody></table></div></div></div></div></div></body></html>