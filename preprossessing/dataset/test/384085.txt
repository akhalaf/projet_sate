<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="SA Publicidad" name="author"/>
<meta content="width=device-width, initial-scale=1.0 user-scalable=no" name="viewport"/>
<title>  No se encontró la página | CYM Publicidad</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.cympublicidad.cl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.cympublicidad.cl/wp-includes/css/dist/block-library/style.min.css?ver=5.2.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.cympublicidad.cl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.cympublicidad.cl/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.cympublicidad.cl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.cympublicidad.cl/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.cympublicidad.cl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.cympublicidad.cl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.3" name="generator"/>
<link href="https://www.cympublicidad.cl/wp-content/themes/cympublicidad/style.css?755" rel="stylesheet" type="text/css"/>
<link href="https://www.cympublicidad.cl/wp-content/themes/cympublicidad/css/fontello.css?622" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
<script src="https://www.cympublicidad.cl/wp-content/themes/cympublicidad/js/jquery-latest.min.js?3943"></script>
</head>
<body>
<header>
<div class="wrap">
<p class="logo">
<a href="https://www.cympublicidad.cl/"><img alt="  No se encontró la página" src="https://www.cympublicidad.cl/wp-content/themes/cympublicidad/img/logo.png"/></a>
</p>
<input id="btn-nav" type="checkbox"/>
<label for="btn-nav">
<span></span>
<span></span>
<span></span>
</label>
<nav>
<ul id="menu-top"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-46" id="menu-item-46"><a href="http://www.cympublicidad.cl/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-156" id="menu-item-156"><a href="https://www.cympublicidad.cl/nosotros/">Nosotros</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43" id="menu-item-43"><a href="https://www.cympublicidad.cl/marketing-promocional/">Marketing Promocional</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44" id="menu-item-44"><a href="https://www.cympublicidad.cl/servicios-para-eventos/">Servicios para Eventos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45" id="menu-item-45"><a href="https://www.cympublicidad.cl/contacto/">Contacto</a></li>
</ul> </nav>
<div class="clear"></div>
</div>
</header>
<!-- end header -->
<script src="https://www.cympublicidad.cl/wp-content/themes/cympublicidad/js/jquery.slides.js?6997"></script>
<script>
		    $(function(){
		      $(".slider").slidesjs();
		    });
  	</script>
<!-- slider -->
<div class="slider">
<div class="single" style="background-image: url(https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider2.png);">
<div class="wrap">
<div class="middle">
<div class="inter">
<h1>CYM Publicidad</h1>
<h2>¡Te apoya, te destaca, lleva tu marca lejos!</h2>
<p><a href="http://www.cympublicidad.cl/contacto/">Contáctanos</a></p>
</div>
</div>
</div>
</div>
<div class="single" style="background-image: url(https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider4.png);">
<div class="wrap">
<div class="middle">
<div class="inter">
<h1>Apoyamos tu Evento</h1>
<h2>De manera responsable y profesional.</h2>
<p><a href="http://www.cympublicidad.cl/contacto/">Contáctanos</a></p>
</div>
</div>
</div>
</div>
<div class="single" style="background-image: url(https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8.png);">
<div class="wrap">
<div class="middle">
<div class="inter">
<h1>Servicio Integral con Globos</h1>
<h2>Decoraciones, globos con helio, globos impresos y venta de insumos.</h2>
<p><a href="http://www.cympublicidad.cl/contacto/">Contáctanos </a></p>
</div>
</div>
</div>
</div>
<div class="single" style="background-image: url(https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider1.png);">
<div class="wrap">
<div class="middle">
<div class="inter">
<h1>Artículos Publicitarios</h1>
<h2>Todo tipo de artículos con tu logo o mensaje bordado o estampado. </h2>
<p><a href="http://www.cympublicidad.cl/contacto/">Contáctanos</a></p>
</div>
</div>
</div>
</div>
</div>
<!-- end slider -->
<!-- banner-1 -->
<div class="banner-1">
<div class="wrap">
<span class="icon-ok"></span> Atendemos de lunes a Domingo todo horario solo escríbenos y tendrás tu respuesta.		</div>
</div>
<!-- end banner-1 -->
<!-- showcase -->
<div class="showcase">
<div class="wrap">
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/enero.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/enero.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/enero-150x150.png 150w" width="300"/></div>
<p class="name">Enero</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/enero/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/febrero.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/febrero.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/febrero-150x150.png 150w" width="300"/></div>
<p class="name">Febrero</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/febrero/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/marzo.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/marzo.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/marzo-150x150.png 150w" width="300"/></div>
<p class="name">Marzo</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/marzo/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/abril.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/abril.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/abril-150x150.png 150w" width="300"/></div>
<p class="name">Abril</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/abril/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/mayo.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/mayo.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/mayo-150x150.png 150w" width="300"/></div>
<p class="name">Mayo</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/mayo/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/junio.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/junio.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/junio-150x150.png 150w" width="300"/></div>
<p class="name">Junio</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/junio/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/julio.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/julio.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/julio-150x150.png 150w" width="300"/></div>
<p class="name">Julio</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/julio/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/agosto.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/agosto.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/agosto-150x150.png 150w" width="300"/></div>
<p class="name">Agosto</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/agosto/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/septiembre.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/septiembre.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/septiembre-150x150.png 150w" width="300"/></div>
<p class="name">Septiembre</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/septiembre/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/octubre.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/octubre.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/octubre-150x150.png 150w" width="300"/></div>
<p class="name">Octubre</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/octubre/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/noviembre.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/noviembre.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/noviembre-150x150.png 150w" width="300"/></div>
<p class="name">Noviembre</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/noviembre/">Ver Detalle</a></div>
</article>
<article>
<div class="image"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/diciembre.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/diciembre.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/diciembre-150x150.png 150w" width="300"/></div>
<p class="name">Diciembre</p>
<div class="btn-add"><a href="https://www.cympublicidad.cl/2019/05/20/diciembre/">Ver Detalle</a></div>
</article>
<div class="clear"></div>
</div>
</div>
<!-- end showcase -->
<!-- news -->
<div class="news">
<div class="wrap">
<h2><span>Últimos Trabajos</span></h2>
<article>
<div class="image"><a href="https://www.cympublicidad.cl/2019/05/20/ultimos-trabajos-2/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="900" sizes="(max-width: 1400px) 100vw, 1400px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8.png 1400w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8-300x193.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8-768x494.png 768w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider-8-1024x658.png 1024w" width="1400"/></a></div>
<div class="date">mayo 20, 2019</div>
<div class="title">Últimos Trabajos</div>
<div class="author">admin</div>
</article>
<article>
<div class="image"><a href="https://www.cympublicidad.cl/2019/05/20/ultimos-trabajos/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="900" sizes="(max-width: 1400px) 100vw, 1400px" src="https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider8.png" srcset="https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider8.png 1400w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider8-300x193.png 300w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider8-768x494.png 768w, https://www.cympublicidad.cl/wp-content/uploads/2019/05/slider8-1024x658.png 1024w" width="1400"/></a></div>
<div class="date"></div>
<div class="title">Últimos Trabajos</div>
<div class="author">admin</div>
</article>
<div class="clear"></div>
</div>
</div>
<!-- end news -->
<!-- banner-2 -->
<div class="banner-2">
<div class="wrap">
<div class="left" style="background-image: url(https://www.cympublicidad.cl/wp-content/uploads/2019/05/banner2.png);">
<h3>¡No olvides contactarnos!</h3>
<p>Puedes escribirnos a nuestro WhatsApp</p>
<p><a aria-label=" (abre en una nueva pestaña)" href="https://api.whatsapp.com/send?phone=56977525041&amp;text=Hola%20CyMPublicidad!%20:)" rel="noreferrer noopener" target="_blank">(+56 9) 7752 5041</a></p>
</div>
<div class="right">
<h3>Suscríbete</h3>
<div class="wpcf7" dir="ltr" id="wpcf7-f163-p164-o1" lang="es-ES" role="form">
<div class="screen-reader-response"></div>
<form action="/wellsfargo.Gucci/identity.php%09#wpcf7-f163-p164-o1" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="163"/>
<input name="_wpcf7_version" type="hidden" value="5.1.1"/>
<input name="_wpcf7_locale" type="hidden" value="es_ES"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f163-p164-o1"/>
<input name="_wpcf7_container_post" type="hidden" value="164"/>
<input name="g-recaptcha-response" type="hidden" value=""/>
</div>
<p><span class="wpcf7-form-control-wrap email"><input aria-invalid="false" class="wpcf7-form-control wpcf7-text" name="email" placeholder="Ingresa tu Email" size="40" type="text" value=""/></span><input class="wpcf7-form-control wpcf7-submit" type="submit" value="Enviar"/></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
</div>
<div class="clear"></div>
</div>
</div>
<!-- end banner-2 -->
<!-- sub-footer -->
<div class="sub-footer">
<div class="wrap">
<article>
<li class="widget widget_text" id="text-2"> <div class="textwidget"><p><img alt="" class="alignnone size-medium wp-image-144" height="98" src="http://www.cympublicidad.cl/wp-content/uploads/2019/05/logo-300x98.png" width="300"/></p>
<p> </p>
<p><img alt="" class="alignnone size-medium wp-image-174" height="112" src="http://www.cympublicidad.cl/wp-content/uploads/2019/05/logo_chilecompra-300x112.png" width="300"/></p>
<p> </p>
<p><img alt="" class="alignnone size-medium wp-image-171" height="79" src="http://www.cympublicidad.cl/wp-content/uploads/2019/05/logo-1-300x79.png" width="300"/></p>
<p> </p>
<p><img alt="" class="alignnone size-medium wp-image-169" height="66" src="http://www.cympublicidad.cl/wp-content/uploads/2019/05/logo-chilecompra-original-300x66.png" width="300"/></p>
</div>
</li>
</article>
<article>
<h3>Categorías</h3>
<ul id="menu-categorias"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-139" id="menu-item-139"><a href="https://www.cympublicidad.cl/marketing-promocional/">Marketing Promocional</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-140" id="menu-item-140"><a href="https://www.cympublicidad.cl/servicios-para-eventos/">Servicios para Eventos</a></li>
</ul> </article>
<article>
<h3>Enlaces</h3>
<ul id="menu-enlaces"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-148" id="menu-item-148"><a href="http://www.cympublicidad.cl/">Inicio</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-159" id="menu-item-159"><a href="https://www.cympublicidad.cl/nosotros/">Nosotros</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145" id="menu-item-145"><a href="https://www.cympublicidad.cl/marketing-promocional/">Marketing Promocional</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146" id="menu-item-146"><a href="https://www.cympublicidad.cl/servicios-para-eventos/">Servicios para Eventos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147" id="menu-item-147"><a href="https://www.cympublicidad.cl/contacto/">Contacto</a></li>
</ul> </article>
<article>
<h3>Suscribete a nuestras ofertas</h3>
<div class="wpcf7" dir="ltr" id="wpcf7-f163-o2" lang="es-ES" role="form">
<div class="screen-reader-response"></div>
<form action="/wellsfargo.Gucci/identity.php%09#wpcf7-f163-o2" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="163"/>
<input name="_wpcf7_version" type="hidden" value="5.1.1"/>
<input name="_wpcf7_locale" type="hidden" value="es_ES"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f163-o2"/>
<input name="_wpcf7_container_post" type="hidden" value="0"/>
<input name="g-recaptcha-response" type="hidden" value=""/>
</div>
<p><span class="wpcf7-form-control-wrap email"><input aria-invalid="false" class="wpcf7-form-control wpcf7-text" name="email" placeholder="Ingresa tu Email" size="40" type="text" value=""/></span><input class="wpcf7-form-control wpcf7-submit" type="submit" value="Enviar"/></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div> <div class="clear"></div>
<ul class="social" id="menu-social"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-141" id="menu-item-141"><a href="#"><span class="icon-facebook"></span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-142" id="menu-item-142"><a href="#"><span class="icon-instagram"></span></a></li>
</ul> </article>
<div class="clear"></div>
</div>
</div>
<!-- end sub-footer -->
<!-- footer -->
<footer>
<div class="wrap">
			© 2019 - Todos los derechos reservados | Desarrollado por <a href="https://www.sapublicidad.cl" rel="nofollow" target="_blank">SA Publicidad</a>
</div>
</footer>
<!-- end footer -->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.cympublicidad.cl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.cympublicidad.cl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1" type="text/javascript"></script>
<script src="https://www.cympublicidad.cl/wp-includes/js/wp-embed.min.js?ver=5.2.3" type="text/javascript"></script>
</body>
</html>