<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>Bay Area Hiker: Home page</title>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="bay area hikes, parks, preserves, open space, trails" name="keywords"/>
<meta content="Information about hiking trails in the San Francisco bay area, including photos of wildflowers, animals, trees, plants, and butterflies" name="description"/>
<!-- TODO HH - temporary hack for testing with included files -->
<!-- <meta http-equiv="Cache-Control" content="no-store" /> -->
<link href="apple-touch-icon.png" rel="apple-touch-icon"/>
<!-- Place favicon.ico in the root directory -->
<link href="/css/normalize.css" rel="stylesheet"/>
<link href="/css/main.css" rel="stylesheet"/>
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
<!-- HH BEGIN -->
<link href="/css/header.css" rel="stylesheet"/>
<link href="/css/dropmenu.css" rel="stylesheet"/>
<script src="/google/util.js" type="text/javascript"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBNjbvPt-1mzqG6hrjcUyOJt0-R_IWfb7A&amp;sensor=false" type="text/javascript"></script>
<script src="/google/geolocationmarker.js" type="text/javascript"></script>
<script src="/google/gmap.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<style>
          * {color:#404040;}
        </style>
<!-- HH END -->
</head>
<body>
<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser.
                Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<!-- Add your site or application content here -->
<div id="container">
<div class="shadow" id="header" style="position: fixed; top: 0px; width: 100%; z-index: 100;">
<div id="logo" style="float: left; height: inherit;">
<div style="float: left; margin: 8px 20px 10px;">
<a href="/">
<b>ba</b>hiker.com
                        </a>
</div>
</div>
<div id="link-menu"></div>
<!-- Patreon link -->
<div id="patreon-link" style="float: right; margin: 8px 3px 0 5px; font-size: .5em; width: 12em">
<a data-patreon-widget-type="become-patron-button" href="https://www.patreon.com/bePatron?u=3113560" title="Visit my Facebook page">Become a Patron!
                    </a>
<script async="" src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
</div>
<!-- FB page link -->
<div id="fb-link" style="float:right; margin: 4px 8px 3px 5px">
<a href="https://www.facebook.com/bayareahiker"></a>
<img src="/assets/FB-f-Logo__blue_72.png" style="height: .8em" title="Visit my Facebook page"/>
</div>
</div>
<div id="warn" style="width: 100%; height: 20px; position: fixed; top: 3em; float: left; clear:both; z-index:90; background: #f5ef42;">
                 <a href="/extras/covid.html">Hiking during COVID-19</a>
</div>
<div id="gmap" style="width: 100%; height: 100%; position: absolute;">
<div id="map_canvas" style="width: 100%; height: 100%; position: absolute"></div>
</div>
</div>
<script>
            $("#link-menu").load("/link-menu.html");
        </script>
<!-- google analytics -->
<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-68695020-1', 'auto');
          ga('send', 'pageview');
        </script>
</body>
</html>
