<html><body><p>ï»¿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

</p>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Untitled 1</title>
<style type="text/css">

 p.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: small;
}
.auto-style4 {
	font-family: Arial, Helvetica, sans-serif;
	color: #2F2F2F;
}
.auto-style5 {
	color: #2F2F2F;
}
</style>
<table align="center" cellpadding="0" cellspacing="0" style="width: 800px">
<tr>
<td>
<img alt="VanSant" class="auto-style2" height="54" src="logo-sm.png" width="187"/></td>
</tr>
<tr>
<td><br class="auto-style2"/><br/><br/><br/>
<span class="auto-style4">Dragi kupci, </span><br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">obveÅ¡Äamo vas, da je poslovni partner iz 
		tujine prekinil sodelovanje z naÅ¡im podjetjem. Tako oddaja novih naroÄil 
		Å¾al ni veÄ moÅ¾na. </span><br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">Vso naroÄeno blago vam bomo poskusili dobaviti 
		v najkrajÅ¡em Äasu. V primeru, da to ne bo moÅ¾no, vas bomo o tem 
		obvestili. </span><br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">VraÄilo blaga sprejemamo nemoteno v skladu s 
		SploÅ¡nimi pogoji poslovanja. Rok za vraÄilo poteÄe v 14 dneh od dneva 
		vaÅ¡ega prevzema blaga. DruÅ¾ba Pro Directa Labs d.o.o. vam zagotavlja 
		povraÄilo kupnine za vrnjeno blago.</span><br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style2"><span class="auto-style5">V primeru morebitnih 
		vpraÅ¡anj, vas prosimo, da se obrnete na naÅ¡e svetovalce na elektronski 
		naslov: </span><br class="auto-style5"/>
<a href="mailto:info@van-sant.com"><span class="auto-style5">
		info@van-sant.com</span></a></span><br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">NaÅ¡e podjetje je v stikih s potencialnimi 
		bodoÄimi partnerji. Iskreno upamo, da bomo v kratkem uspeli skleniti nov 
		dogovor o sodelovanju. V Å¾elji, da se vam kar najhitreje javimo z 
		odliÄno ponudbo, vas lepo pozdravljamo. </span>
<br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">Zahvaljujemo se vam za dosedanje zaupanje in 
		vam do naÅ¡ega naslednjega svidenja Å¾elimo vse dobro. </span>
<br class="auto-style4"/>
<br class="auto-style4"/>
<span class="auto-style4">Ekipa Pro Directa Labs</span><br class="auto-style2"/><br/><br/><br/>
</td>
</tr>
<tr>
<td><hr/></td>
</tr>
<tr>
<td>
<p class="auto-style1"><span style="font-size:11 px;">
<span class="auto-style3">Pro 
		Directa Labs d.o.o. -
		TrÅ¾aÅ¡ka cesta 65 -
		2000 Maribor</span><br class="auto-style3"/>
<span class="auto-style3">Tel: 02/320 99 20 - </span></span>
<span class="auto-style3" style="font-size: 10 px;">
		www.van-sant.com</span></p>
</td>
</tr>
</table>
</body></html>