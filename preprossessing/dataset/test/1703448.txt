<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Страница не найдена — 404</title>
<style type="text/css">
            * { font-family: Arial; }
            html, body { height:100%; margin:0px; }
            h2, p { margin:0px; }
            .ajaxlink{ text-decoration:none; border-bottom:dashed 1px #AAA; color:#AAA; }
            ul { list-style: none; margin: 10px; padding: 0; }
        </style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr>
<td align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="140">
<img src="/templates/adaptive/special/images/error404.png"/>
</td>
<td>
<h2>Страница не найдена — 404</h2>
<p>Возможно, она была удалена или перемещена.</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>