<!DOCTYPE html>
<html><head>
<meta charset="utf-8"/>
<title>BestAviation.net Error 404 - File not found</title>
<meta content="Sorry we could not find what you are looking for" name="description"/>
<link href="/inc/img/favicon.ico" rel="icon" type="image/png"/>
<meta content="BestAviation.net" property="og:site_name"/>
<meta content="109570295726827" property="fb:app_id"/>
<meta content="1200" property="og:image:width"/>
<meta content="630" property="og:image:height"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0" name="viewport"/> <!--320-->
<link href="https://plus.google.com/+bestaviation" rel="publisher"/>
<link href="//css.bestaviation.net/inc/css/style.min.css" rel="stylesheet" type="text/css"/>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Cantarell:400,700italic" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="mainwrapper"><!-- Main wrap for everything in view -->
<div class="topnav">
<div class="wrapper-top">
<div class="logo"><img alt="" id="bestaviationlogo" src="//img.bestaviation.net/inc/img/logo-top.png"/></div>
<div class="menubuttonsearch"><img alt="Search" src="//img.bestaviation.net/inc/img/icons/search-64px.png"/></div>
<div class="menubutton"><div class="stripe"> </div><div class="stripe"> </div><div class="stripe" style="margin-bottom:0;"> </div></div>
<nav id="topmenu">
<ul class="topmenu">
<li><span id="topmenu-avschool">Schools <i class="fi fi-angle-down"></i></span>
<ul class="drop" id="topmenu-avschool-list">
<li><a href="https://www.bestaviation.net/premier-schools/" title="Premier Aviation Schools"><i class="fi fi-star-o fi-fw"></i> Premier Aviation Schools</a></li>
<li><a href="https://www.bestaviation.net/flight_school/" title="Flight Schools"><i class="fi fi-little-plane fi-fw"></i> Flight Schools (airplane)</a></li>
<li><a href="https://www.bestaviation.net/helicopter_schools/" title="Helicopter Schools"><i class="fi fi-helicopter fi-fw"></i> Helicopter Schools</a></li>
<li><a href="https://www.bestaviation.net/college/" title="Aviation College Programs"><i class="fi fi-college-hat fi-fw"></i> Aviation Colleges</a></li>
<li><a href="https://www.bestaviation.net/aircraft_maintenance_schools/" title="Aircraft Maintenance Schools"><i class="fi fi-cogs fi-fw"></i> Aircraft Maintenance</a></li>
<li><a href="https://www.bestaviation.net/multi-engine-time-building/" title="Multi-Engine Time Building"><i class="fi fi-airplane-rotating fi-fw"></i> Multi-Engine Time Building</a></li>
<li><a href="https://www.bestaviation.net/type_rating/" title="Type Rating Courses"><i class="fi fi-pilot-hat fi-fw"></i> Type Rating Courses</a></li>
<li><a href="https://www.bestaviation.net/flight_attendant_school/" title="Flight Attendant Courses"><i class="fi fi-fasten-seatbelt fi-fw"></i> Flight Attendant</a></li>
<li><a href="https://www.bestaviation.net/aircraft_dispatcher_school/" title="Aircraft Dispatcher Courses"><i class="fi fi-airplane-dispatch fi-fw"></i> Aircraft Dispatcher</a></li>
<li><a href="https://www.bestaviation.net/air_traffic_controller_schools/" title="Air Traffic Controller Schools"><i class="fi fi-flight-radar fi-fw"></i> Air Traffic Controller (ATC)</a></li>
<li><a href="https://www.bestaviation.net/school-manager/join.asp" rel="nofollow" style="border-top:1px dotted #ccc; color:#d83c3c; font-weight:bold;" title="Submit a new school"><i class="fi fi-paper-plane-o fi-fw"></i> Add Your Aviation School</a></li>
</ul>
</li>
<li><a href="https://www.bestaviation.net/reviews/" id="nav-add-review" title="Review an Aviation School"><i class="fi fi-star"></i> Review a School</a></li>
</ul>
</nav>
<a class="logonamelink" href="https://www.bestaviation.net" title="Best Aviation Schools">BESTAVIATION<span class="net">.NET</span><br/><span class="tm"><i>Your Aviation Training Guide</i></span></a>
</div>
</div><div class="topimage topimage-narrow">
<div class="wrapper">
<div class="topimagefile-schooldir">
			  
		</div>
</div>
</div>
<div class="wrapper"> <!-- all main content inside this wrapper -->
<div class="content" style="margin-left:15px; margin-right:15px;min-height:500px;">
<h1 style="text-align:center;">File Not Found</h1>
<p style="text-align:center;">You appear to be lost. The resource you are looking for has either moved or does not exists.</p>
</div>
<!-- Banner at the bottom (also clears floating elements... --><div style="clear:both;"> </div>
</div> <!-- end, wrapper for main content --><div class="bottom-background">
<div class="wrapper">
<div class="bottom-content">
<div class="btmleftcol">
<h3>About</h3>
<a href="https://www.bestaviation.net/about.asp" rel="nofollow" title="About Best Aviation Schools">About Best Aviation</a><br/>
<a href="https://www.bestaviation.net/privacy.asp" rel="nofollow" title="Your Privacy on Best Aviation Schools">Your Privacy</a><br/>
<a href="https://www.bestaviation.net/contact.asp" rel="nofollow" title="Contact us">Contact Us</a>
</div>
<div class="btmleftcol">
<h3>Advertise</h3>
<a href="https://www.bestaviation.net/school-manager/join.asp" rel="nofollow" title="Add a new aviation school">Add a New School</a><br/>
<a href="https://www.bestaviation.net/school-manager/" rel="nofollow" title="Update a listed aviation school">Update a Listed School</a><br/>
<a href="https://www.bestaviation.net/contact.asp" title="Contact us to discuss paid advertising">Paid Advertising</a>
</div>
<div class="btmleftcol">
<h3>Connect With Us</h3>
<a class="sm-btm-href" href="https://www.facebook.com/bestaviation" id="sm-btm-a-fb" rel="nofollow" target="_blank" title="Follow us on Facebook"><span id="sm-btm-fb"> </span> Facebook</a>
<a class="sm-btm-href" href="https://twitter.com/bestaviationnet" id="sm-btm-a-tw" rel="nofollow" target="_blank" title="Follow us on Twitter"><span id="sm-btm-tw"> </span> Twitter</a>
<a class="sm-btm-href" href="https://plus.google.com/+bestaviation/posts" id="sm-btm-a-go" rel="nofollow" target="_blank" title="Follow us on Google+"><span id="sm-btm-go"> </span> Google+</a>
</div>
<div class="bottom-closer">
				2002 - 2021 © Best Aviation Schools
			</div>
</div>
</div>
</div>
</div><!-- Wrapper for everything in view -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1298236-1', 'auto');
  ga('send', 'pageview');
</script><div class="mobile-menu">
<div class="topmenuclose"><i class="fi fi-times-circle-o fi-2x"></i></div>
<a href="//www.bestaviation.net/reviews/" id="mob-menu-review">+ Review a School</a>
<h3>AVIATION SCHOOLS</h3>
<nav>
<ul>
<li><a href="//www.bestaviation.net/premier-schools/" rel="nofollow" title="Premier Aviation Schools"><i class="fi fi-star-o fi-fw"></i> Premier Aviation Schools</a></li>
<li><a href="//www.bestaviation.net/flight_school/" rel="nofollow" title="Flight Schools"><i class="fi fi-little-plane fi-fw"></i> Flight Schools (airplane)</a></li>
<li><a href="//www.bestaviation.net/helicopter_schools/" rel="nofollow" title="Helicopter Schools"><i class="fi fi-helicopter fi-fw"></i> Helicopter Schools</a></li>
<li><a href="//www.bestaviation.net/college/" rel="nofollow" title="Aviation College Programs"><i class="fi fi-college-hat fi-fw"></i> Aviation Colleges</a></li>
<li><a href="//www.bestaviation.net/aircraft_maintenance_schools/" rel="nofollow" title="Aircraft Maintenance Schools"><i class="fi fi-cogs fi-fw"></i> Aircraft Maintenance</a></li>
<li><a href="//www.bestaviation.net/multi-engine-time-building/" rel="nofollow" title="Multi-Engine Time Building"><i class="fi fi-airplane-rotating fi-fw"></i> Multi-Engine Time Building</a></li>
<li><a href="//www.bestaviation.net/type_rating/" rel="nofollow" title="Type Rating Courses"><i class="fi fi-pilot-hat fi-fw"></i> Type Rating Courses</a></li>
<li><a href="//www.bestaviation.net/flight_attendant_school/" rel="nofollow" title="Flight Attendant Courses"><i class="fi fi-fasten-seatbelt fi-fw"></i> Flight Attendant</a></li>
<li><a href="//www.bestaviation.net/aircraft_dispatcher_school/" rel="nofollow" title="Aircraft Dispatcher Courses"><i class="fi fi-airplane-dispatch fi-fw"></i> Aircraft Dispatcher</a></li>
<li><a href="//www.bestaviation.net/air_traffic_controller_schools/" rel="nofollow" title="Air Traffic Controller Schools"><i class="fi fi-flight-radar fi-fw"></i> Air Traffic Controller (ATC)</a></li>
</ul>
</nav>
</div>
<!-- Mobile search form -->
<form action="//www.bestaviation.net/search/" class="slidesearch" method="get">
<div class="topmenuclose"><i class="fi fi-times-circle-o fi-2x"></i></div>
<fieldset style="margin-top:20px;">
<legend>SEARCH FOR SCHOOLS</legend>
<input class="txt searchlocation" name="loc" placeholder="Enter a Location" required="" type="text"/>
</fieldset>
<fieldset>
<legend style="font-size:0.9em;">PROGRAM <span>(Optional)</span></legend>
<input class="rdo" id="srcmoball" name="tp" type="radio" value=""/> <label for="srcmoball">Show All Schools</label><br/>
<input class="rdo" id="srcmobfl" name="tp" type="radio" value="fl"/> <label for="srcmobfl">Flight Schools (Airplane)</label><br/>
<input class="rdo" id="srcmobhe" name="tp" type="radio" value="he"/> <label for="srcmobhe">Helicopter Schools</label><br/>
<input class="rdo" id="srcmobco" name="tp" type="radio" value="co"/> <label for="srcmobco">Aviation Degree Programs</label><br/>
<input class="rdo" id="srcmobmt" name="tp" type="radio" value="me"/> <label for="srcmobmt">Aircraft Maintenance Schools</label><br/>
<input class="rdo" id="srcmobatc" name="tp" type="radio" value="atc"/> <label for="srcmobatc">Air Traffic Control Schools</label><br/>
<input class="rdo" id="srcmobfa" name="tp" type="radio" value="fa"/> <label for="srcmobfa">Flight Attendant Courses</label><br/>
<input class="rdo" id="srcmobfd" name="tp" type="radio" value="fds"/> <label for="srcmobfd">Aircraft Dispatcher Courses</label>
</fieldset>
<button type="submit">Find Schools</button>
<div class="trmsg">To search for aircraft types visit <a href="//www.bestaviation.net/type_rating/" rel="nofollow" title="Aircraft Type Rating Courses">Type Rating Courses</a></div>
</form>
<img alt="Scroll to Top" class="scrolltotop" src="//img.bestaviation.net/inc/img/icons/scroll-top.png"/>
<script src="//use.fonticons.com/19e32b19.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//js.bestaviation.net/inc/js/universal-scripts.min.js" type="text/javascript"></script>
<script src="//js.bestaviation.net/inc/js/jquery.responsivetabs.min.js" type="text/javascript"></script>
<script>
  // LOAD PREMIER SCHOOL ADS //
  $(document).ready(function() {
    $.get( "/inc/ajax/ajax-premier-school-ads.asp?t=").done(function(data) {
        var dataSplit = data.split(']~[');
        if (dataSplit.length == 3) {
          $('#ps-dir-list').html(dataSplit[0]);
          $('#ps-profile-list').html(dataSplit[1]);
          $('#ps-right-col').html(dataSplit[2]);
        }
      });
      $('.ps-adtrack').on("click", "a" , function() {
        var psclickid = $(this).attr('id');
        if (psclickid) {
          var psclickidsplit = psclickid.split('-');
          $.get("/inc/ajax/ajax-premier-school-ads-click-tracker.asp?loc=" + psclickidsplit[0] + "&id=" + psclickidsplit[1]);
        } 
      });
    });
</script>
<script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-0969423959700009",
        enable_page_level_ads: true
      });
</script></body>
</html>