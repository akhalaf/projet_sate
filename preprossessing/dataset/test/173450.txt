<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="320" name="MobileOptimized"/>
<meta content="width=device-width, initial-scale=1, viewport-fit=cover" name="viewport"/>
<meta content="#4f96f6" name="msapplication-TileColor"/>
<meta content="#4f96f6" name="theme-color"/>
<link href="https://autofx-now.com/xmlrpc.php" rel="pingback"/>
<title>ただいまFX自動売買中</title>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//use.fontawesome.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://autofx-now.com/feed" rel="alternate" title="ただいまFX自動売買中 » フィード" type="application/rss+xml"/>
<link href="https://autofx-now.com/comments/feed" rel="alternate" title="ただいまFX自動売買中 » コメントフィード" type="application/rss+xml"/>
<link href="https://autofx-now.com/wp-content/themes/sango-theme/style.css?ver2_0_8" id="sng-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://autofx-now.com/wp-content/themes/sango-theme/entry-option.css?ver2_0_8" id="sng-option-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Quicksand%3A500%2C700&amp;display=swap" id="sng-googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" id="sng-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://autofx-now.com/wp-content/themes/sango-theme-child/style.css" id="child-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://autofx-now.com/wp-includes/js/jquery/jquery.min.js" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://autofx-now.com/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<link href="https://autofx-now.com/wp-json/" rel="https://api.w.org/"/><link href="https://autofx-now.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<meta content="FX自動売買って本当に儲かるの？MT4 EAを中心にトライオート、シストレ24、トラリピなどの運用実績を公開中。初心者の方向けにMT4の設定方法や使い方、運用ノウハウなども。" name="description"/><meta content="ただいまFX自動売買中" property="og:title"/>
<meta content="FX自動売買って本当に儲かるの？MT4 EAを中心にトライオート、シストレ24、トラリピなどの運用実績を公開中。初心者の方向けにMT4の設定方法や使い方、運用ノウハウなども。" property="og:description"/>
<meta content="website" property="og:type"/>
<meta content="https://autofx-now.com" property="og:url"/>
<meta content="https://autofx-now.com/wp-content/uploads/2019/01/default-thumbnail.png" property="og:image"/>
<meta content="https://autofx-now.com/wp-content/uploads/2019/01/default-thumbnail.png" name="thumbnail"/>
<meta content="ただいまFX自動売買中" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- gtag.js -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-52626265-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-52626265-8');
</script>
<style type="text/css">.broken_link, a.broken_link {
	text-decoration: line-through;
}</style><style id="custom-background-css" type="text/css">
body.custom-background { background-color: #f3f3f3; }
</style>
<link href="https://autofx-now.com/wp-content/uploads/2012/03/autofxnow-logo-144x144.jpg" rel="icon" sizes="32x32"/>
<link href="https://autofx-now.com/wp-content/uploads/2012/03/autofxnow-logo-300x300.jpg" rel="icon" sizes="192x192"/>
<link href="https://autofx-now.com/wp-content/uploads/2012/03/autofxnow-logo-300x300.jpg" rel="apple-touch-icon"/>
<meta content="https://autofx-now.com/wp-content/uploads/2012/03/autofxnow-logo-300x300.jpg" name="msapplication-TileImage"/>
<style> a{color:#4f96f6}.main-c, .has-sango-main-color{color:#4f96f6}.main-bc, .has-sango-main-background-color{background-color:#4f96f6}.main-bdr, #inner-content .main-bdr{border-color:#4f96f6}.pastel-c, .has-sango-pastel-color{color:#fffaf3}.pastel-bc, .has-sango-pastel-background-color, #inner-content .pastel-bc{background-color:#fffaf3}.accent-c, .has-sango-accent-color{color:#4f96f6}.accent-bc, .has-sango-accent-background-color{background-color:#4f96f6}.header, #footer-menu, .drawer__title{background-color:#ffffff}#logo a{color:#4b4b4b}.desktop-nav li a , .mobile-nav li a, #footer-menu a, #drawer__open, .header-search__open, .copyright, .drawer__title{color:#4b4b4b}.drawer__title .close span, .drawer__title .close span:before{background:#4b4b4b}.desktop-nav li:after{background:#4b4b4b}.mobile-nav .current-menu-item{border-bottom-color:#4b4b4b}.widgettitle{color:#4b4b4b;background-color:#e2d7c4}.footer{background-color:#dbd4ce}.footer, .footer a, .footer .widget ul li a{color:#3c3c3c}#toc_container .toc_title, .entry-content .ez-toc-title-container, #footer_menu .raised, .pagination a, .pagination span, #reply-title:before, .entry-content blockquote:before, .main-c-before li:before, .main-c-b:before{color:#4f96f6}.searchform__submit, #toc_container .toc_title:before, .ez-toc-title-container:before, .cat-name, .pre_tag > span, .pagination .current, .post-page-numbers.current, #submit, .withtag_list > span, .main-bc-before li:before{background-color:#4f96f6}#toc_container, #ez-toc-container, .entry-content h3, .li-mainbdr ul, .li-mainbdr ol{border-color:#4f96f6}.search-title i, .acc-bc-before li:before{background:#4f96f6}.li-accentbdr ul, .li-accentbdr ol{border-color:#4f96f6}.pagination a:hover, .li-pastelbc ul, .li-pastelbc ol{background:#fffaf3}body{font-size:100%}@media only screen and (min-width:481px){body{font-size:107%}}@media only screen and (min-width:1030px){body{font-size:107%}}.totop{background:#5ba9f7}.header-info a{color:#FFF;background:linear-gradient(95deg, #738bff, #85e3ec)}.fixed-menu ul{background:#FFF}.fixed-menu a{color:#a2a7ab}.fixed-menu .current-menu-item a, .fixed-menu ul li a.active{color:#4f96f6}.post-tab{background:#FFF}.post-tab > div{color:#a7a7a7}.post-tab > div.tab-active{background:linear-gradient(45deg, #bdb9ff, #67b8ff)}body{font-family:"Helvetica", "Arial", "Hiragino Kaku Gothic ProN", "Hiragino Sans", YuGothic, "Yu Gothic", "メイリオ", Meiryo, sans-serif;}.dfont{font-family:"Quicksand","Helvetica", "Arial", "Hiragino Kaku Gothic ProN", "Hiragino Sans", YuGothic, "Yu Gothic", "メイリオ", Meiryo, sans-serif;}.body_bc{background-color:f3f3f3}</style></head>
<body class="home blog custom-background fa5">
<div id="container">
<header class="header">
<div class="wrap cf" id="inner-header">
<h1 class="header-logo h1 dfont" id="logo">
<a class="header-logo__link" href="https://autofx-now.com">
<img alt="ただいまFX自動売買中" class="header-logo__img" src="https://autofx-now.com/wp-content/uploads/2019/01/fx-header.png"/>
</a>
</h1>
<div class="header-search">
<label class="header-search__open" for="header-search-input"><i class="fas fa-search"></i></label>
<input class="header-search__input" id="header-search-input" onclick="document.querySelector('.header-search__modal .searchform__input').focus()" type="checkbox"/>
<label class="header-search__close" for="header-search-input"></label>
<div class="header-search__modal">
<form action="https://autofx-now.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<input class="searchform__input" id="s" name="s" placeholder="検索" type="search" value=""/>
<button class="searchform__submit" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
</div>
</form> </div>
</div> <nav class="desktop-nav clearfix"><ul class="menu" id="menu-%e3%82%b0%e3%83%ad%e3%83%bc%e3%83%90%e3%83%ab%e3%83%a1%e3%83%8b%e3%83%a5%e3%83%bc"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10997" id="menu-item-10997"><a href="https://autofx-now.com/first-mt4">はじめてのMT4</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11001" id="menu-item-11001"><a href="https://autofx-now.com/myea">現在稼働中のEA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16991" id="menu-item-16991"><a href="https://autofx-now.com/mytraripi">トラリピ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10999" id="menu-item-10999"><a href="https://autofx-now.com/forex-tools">FXツール</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17833" id="menu-item-17833"><a href="https://autofx-now.com/reserve-crypto">暗号資産</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-17834" id="menu-item-17834"><a href="https://autofx-now.com/13204">ソーシャルレンディング</a></li>
</ul></nav></div>
</header>
<div class="divheader maximg" id="divheader" style="background: #ffffff;">
<div class="divheader__img">
<img src="https://autofx-now.com/wp-content/uploads/2019/01/header-icatch.png"/>
</div>
<div class="divheader__text">
<p class="divheader__descr" style="color: #4b4b4b;">FX自動売買って本当に儲かるの？

MT4 EAを中心にトライオート、シストレ24、トラリピなどの運用実績を公開中。

初心者の方向けにMT4の設定方法や使い方、運用ノウハウなども。</p>
</div>
</div>
<div id="content">
<div class="wrap cf" id="inner-content">
<main class="m-all t-2of3 d-5of7 cf" id="main">
<div class="widget_text home_top"><div class="textwidget custom-html-widget"><div class="widget-menu__title main-bc ct strong">
<i class="fas fa-map-marker-alt"></i> 運用メニュー
</div>
<ul class="widget-menu menu-three dfont cf">
<li><a href="https://autofx-now.com/myea"><i class="fas fa-desktop" style="color: #495057"></i>MT4 FX自動売買</a></li>
<li><a href="https://autofx-now.com/mytraripi"><i class="fas fa-paw" style="color: #fee101"></i>トラリピ</a></li>
<li><a href="https://autofx-now.com/reserve-crypto"><i class="fab fa-bitcoin" style="color: #FF9900"></i>暗号資産の積立</a></li>
<li><a href="https://autofx-now.com/13204"><i class="fas fa-donate" style="color: #92ceff"></i>ソーシャルレンディング</a></li>
<li><a href="https://autofx-now.com/triautofx"><i class="fas fa-location-arrow" style="color: #a67ecb"></i>トライオートFX</a></li>
<li><a href="https://autofx-now.com/st24"><i class="fas fa-sync-alt" style="color: #606cb6"></i>シストレ24</a></li>
</ul></div></div> <div class="cardtype cf">
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18301">
<p class="cardtype__img">
<img alt="2020年12月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2021-01-02" itemprop="datePublished">2021年1月2日</time> <h2>2020年12月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18281">
<p class="cardtype__img">
<img alt="2020年11月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-12-01" itemprop="datePublished">2020年12月1日</time> <h2>2020年11月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18266">
<p class="cardtype__img">
<img alt="2020年10月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-11-01" itemprop="datePublished">2020年11月1日</time> <h2>2020年10月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18248">
<p class="cardtype__img">
<img alt="2020年9月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/decline-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-10-01" itemprop="datePublished">2020年10月1日</time> <h2>2020年9月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18220">
<p class="cardtype__img">
<img alt="2020年8月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-09-01" itemprop="datePublished">2020年9月1日</time> <h2>2020年8月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18195">
<p class="cardtype__img">
<img alt="2020年7月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-08-01" itemprop="datePublished">2020年8月1日</time> <h2>2020年7月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18139">
<p class="cardtype__img">
<img alt="2020年6月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/decline-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-07-01" itemprop="datePublished">2020年7月1日</time> <h2>2020年6月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/18102">
<p class="cardtype__img">
<img alt="2020年5月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-06-01" itemprop="datePublished">2020年6月1日</time> <h2>2020年5月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/17986">
<p class="cardtype__img">
<img alt="2020年4月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-05-02" itemprop="datePublished">2020年5月2日</time> <h2>2020年4月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
<article class="cardtype__article">
<a class="cardtype__link" href="https://autofx-now.com/17942">
<p class="cardtype__img">
<img alt="2020年3月の運用成績" src="https://autofx-now.com/wp-content/uploads/2019/01/decline-520x300.jpg"/>
</p>
<div class="cardtype__article-info">
<time class="pubdate entry-time dfont" datetime="2020-04-01" itemprop="datePublished">2020年4月1日</time> <h2>2020年3月の運用成績</h2>
</div>
</a>
<a class="dfont cat-name catid36" href="https://autofx-now.com/category/report">運用レポート</a> </article>
</div>
<nav class="pagination dfont"><ul class="page-numbers">
<li><span aria-current="page" class="page-numbers current">1</span></li>
<li><a class="page-numbers" href="https://autofx-now.com/page/2">2</a></li>
<li><span class="page-numbers dots">…</span></li>
<li><a class="page-numbers" href="https://autofx-now.com/page/20">20</a></li>
<li><a class="next page-numbers" href="https://autofx-now.com/page/2"><i class="fa fa-chevron-right"></i></a></li>
</ul>
</nav> </main>
<div class="sidebar m-all t-1of3 d-2of7 last-col cf" id="sidebar1" role="complementary">
<aside class="insidesp">
<div class="normal-sidebar" id="notfix">
<div class="widget_text widget widget_custom_html" id="custom_html-23"><div class="textwidget custom-html-widget"><div style="text-align:center;padding:0;margin:0 auto;">
<!-- FX_サイドバー -->
<ins class="adsbygoogle" data-ad-client="ca-pub-2481749523228604" data-ad-format="auto" data-ad-slot="3547158312" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div></div><div class="widget widget_search" id="search-2"><form action="https://autofx-now.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<input class="searchform__input" id="s" name="s" placeholder="検索" type="search" value=""/>
<button class="searchform__submit" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
</div>
</form></div><div class="widget_text widget widget_custom_html" id="custom_html-11"><h4 class="widgettitle dfont has-fa-before">運用成績（2021年1月更新）</h4><div class="textwidget custom-html-widget"><div class="widget_text widget widget_custom_html" id="custom_html-5">
<div class="textwidget custom-html-widget"><div class="rate-box">
<div class="rateline"><div class="rate-thing"><i class="fas fa-desktop" style="color: #495057"></i> <a href="https://autofx-now.com/myea">MT4 FX自動売買</a></div><div class="rate-star dfont">+114,610円</div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-paw" style="color: #fee101"></i> <a href="https://autofx-now.com/mytraripi">トラリピ</a></div><div class="rate-star dfont">+347,380円</div></div>
<div class="rateline"><div class="rate-thing"><i class="fab fa-bitcoin" style="color: #FF9900"></i> <a href="https://autofx-now.com/reserve-crypto">暗号資産の積立</a></div><div class="rate-star dfont">+2,222,096円</div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-donate" style="color: #92ceff"></i> <a href="https://autofx-now.com/13204">ソーシャルレンディング</a></div><div class="rate-star dfont">+402,517円</div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-location-arrow" style="color: #a67ecb"></i> <a href="https://autofx-now.com/triautofx">トライオートFX(休止)</a></div><div class="rate-star dfont">+14,587円</div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-location-arrow" style="color: #c4575b"></i> <a href="https://autofx-now.com/etf">トライオートETF(休止)</a></div><div class="rate-star dfont"><span class="f-red">-30,573円</span></div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-sync-alt" style="color: #606cb6"></i> <a href="https://autofx-now.com/st24">シストレ24（休止）</a></div><div class="rate-star dfont"><span class="f-red">-370,710円</span></div></div>
<div class="rateline"><div class="rate-thing"><i class="fas fa-piggy-bank" style="color: #ffb776"></i> <a href="https://autofx-now.com/first-fx/reserve-fx">FXで外貨積立（休止）</a></div><div class="rate-star dfont">+11,044円</div></div>
</div></div>
</div></div></div><div class="widget_text widget widget_custom_html" id="custom_html-18"><h4 class="widgettitle dfont has-fa-before">運用中のMT4 FX自動売買</h4><div class="textwidget custom-html-widget"><div style="text-align:center; padding:10px;">
<a href="https://autofx-now.com/myea"><img border="0" src="https://widgets.myfxbook.com/custom-widget?id=3159670&amp;width=400&amp;height=250&amp;bart=1&amp;symbol=USDJPY&amp;linet=0&amp;bgColor=FFFFFF&amp;gridColor=BDBDBD&amp;lineColor=FF0505&amp;barColor=FFADAD&amp;bar1Color=FFADAD&amp;fontColor=525252&amp;title=OANDA%20JAPAN%20-%20TOKYO%20Server&amp;titles=12&amp;chartbgc=FFFFFF&amp;magic=-1&amp;equityColor=EFF45A"/></a>
</div></div></div><div class="widget widget_nav_menu" id="nav_menu-4"><h4 class="widgettitle dfont has-fa-before">MT4（メタトレーダー）特集</h4><div class="menu-mt4%e7%89%b9%e9%9b%86-container"><ul class="menu" id="menu-mt4%e7%89%b9%e9%9b%86"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16094" id="menu-item-16094"><a href="https://autofx-now.com/first-mt4">はじめてのMT4</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16091" id="menu-item-16091"><a href="https://autofx-now.com/first-mt4/open-account/fxbroker-hikaku">MT4対応の国内FX業者</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16093" id="menu-item-16093"><a href="https://autofx-now.com/first-mt4/vps">MT4の24時間稼働におすすめのVPS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18332" id="menu-item-18332"><a href="https://autofx-now.com/first-mt4/mt4-eaget">MT4 EA販売サイト</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16095" id="menu-item-16095"><a href="https://autofx-now.com/first-mt4/mt4-operation/myfxbook">Myfxbookの設定方法</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-16096" id="menu-item-16096"><a href="https://autofx-now.com/14786">MT4でビットコイン取引</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-16097" id="menu-item-16097"><a href="https://autofx-now.com/6895">MacでMT4を利用する方法</a></li>
</ul></div></div><div class="widget_text widget widget_custom_html" id="custom_html-21"><h4 class="widgettitle dfont has-fa-before">はじめてのFX自動売買</h4><div class="textwidget custom-html-widget"><ul class="my-widget" style="padding:10px;">
<li><a href="https://autofx-now.com/first-fx/auto-trade"><figure class="my-widget__img"><img alt="FX自動売買（システムトレード）" class="attachment-thumb-160 size-thumb-160 wp-post-image" height="160" sizes="(max-width: 160px) 100vw, 160px" src="https://autofx-now.com/wp-content/uploads/2015/06/autofx-now-160x160.jpg" srcset="https://autofx-now.com/wp-content/uploads/2015/06/autofx-now-160x160.jpg 160w, https://autofx-now.com/wp-content/uploads/2015/06/autofx-now-150x150.jpg 150w, https://autofx-now.com/wp-content/uploads/2015/06/autofx-now-125x125.jpg 125w" width="160"/></figure><div class="my-widget__text">FX自動売買のすすめ</div></a></li>
<li><a href="https://autofx-now.com/9310"><figure class="my-widget__img"><img alt="初心者でも簡単・無料で使えるFX自動売買（システムトレード）サービス" class="attachment-thumb-160 size-thumb-160 wp-post-image" height="160" sizes="(max-width: 160px) 100vw, 160px" src="https://autofx-now.com/wp-content/uploads/2014/10/system-trade-160x160.jpg" srcset="https://autofx-now.com/wp-content/uploads/2014/10/system-trade-160x160.jpg 160w, https://autofx-now.com/wp-content/uploads/2014/10/system-trade-150x150.jpg 150w, https://autofx-now.com/wp-content/uploads/2014/10/system-trade-125x125.jpg 125w" width="160"/></figure><div class="my-widget__text">初心者でも簡単・無料で使えるFX自動売買サービス</div></a></li>
</ul></div></div><div class="widget_text widget widget_custom_html" id="custom_html-26"><div class="textwidget custom-html-widget"></div></div><div class="widget_text widget widget_custom_html" id="custom_html-17"><div class="textwidget custom-html-widget"><div class="yourprofile">
<p class="profile-background"><img src="https://autofx-now.com/wp-content/uploads/2019/01/fx-sideintro.jpg"/></p>
<p class="profile-img"><a href="https://autofx-now.com/about"><img alt="ただいまFX自動売買中" height="100px" src="https://autofx-now.com/wp-content/uploads/2019/01/fx-logo-icon.jpg" width="100px"/></a></p>
<p class="yourname dfont">ただいまFX自動売買中</p>
</div>
<div class="profile-content">
<p>FX自動売買って本当に儲かるの？</p>
<p>MT4 EAを中心にトライオート、シストレ24、トラリピなどの運用実績を公開中。</p>
<p>初心者の方向けにMT4の設定方法や使い方、運用ノウハウなども。</p>
</div>
<ul class="profile-sns dfont">
<li><a href="https://twitter.com/autofxnow" rel="nofollow noopener" target="_blank"><i class="fab fa-twitter"></i></a></li>
<li><a href="https://www.facebook.com/autofxnow/" rel="nofollow noopener" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="https://feedly.com/i/subscription/feed/https://autofx-now.com/feed" rel="noopener" target="_blank"><i class="fas fa-rss"></i></a></li>
</ul></div></div><div class="widget widget_recent_entries" id="recent-posts-2"><h4 class="widgettitle dfont has-fa-before">最近の投稿</h4> <ul class="my-widget">
<li>
<a href="https://autofx-now.com/18301">
<figure class="my-widget__img">
<img alt="2020年12月の運用成績" height="160" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-160x160.jpg" width="160"/>
</figure>
<div class="my-widget__text">2020年12月の運用成績          </div>
</a>
</li>
<li>
<a href="https://autofx-now.com/18281">
<figure class="my-widget__img">
<img alt="2020年11月の運用成績" height="160" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-160x160.jpg" width="160"/>
</figure>
<div class="my-widget__text">2020年11月の運用成績          </div>
</a>
</li>
<li>
<a href="https://autofx-now.com/18266">
<figure class="my-widget__img">
<img alt="2020年10月の運用成績" height="160" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-160x160.jpg" width="160"/>
</figure>
<div class="my-widget__text">2020年10月の運用成績          </div>
</a>
</li>
<li>
<a href="https://autofx-now.com/18248">
<figure class="my-widget__img">
<img alt="2020年9月の運用成績" height="160" src="https://autofx-now.com/wp-content/uploads/2019/01/decline-160x160.jpg" width="160"/>
</figure>
<div class="my-widget__text">2020年9月の運用成績          </div>
</a>
</li>
<li>
<a href="https://autofx-now.com/18220">
<figure class="my-widget__img">
<img alt="2020年8月の運用成績" height="160" src="https://autofx-now.com/wp-content/uploads/2019/01/incline_2-160x160.jpg" width="160"/>
</figure>
<div class="my-widget__text">2020年8月の運用成績          </div>
</a>
</li>
</ul>
</div> <div class="widget_text widget widget_custom_html" id="custom_html-7"><div class="textwidget custom-html-widget"></div></div> </div>
</aside>
</div>
</div>
</div>
<footer class="footer">
<div class="inner-footer wrap cf" id="inner-footer">
<div class="fblock first">
<div class="ft_widget widget widget_search"><form action="https://autofx-now.com/" class="searchform" id="searchform" method="get" role="search">
<div>
<input class="searchform__input" id="s" name="s" placeholder="検索" type="search" value=""/>
<button class="searchform__submit" id="searchsubmit" type="submit"><i class="fas fa-search"></i></button>
</div>
</form></div><div class="ft_widget widget widget_nav_menu"><h4 class="ft_title">メニュー</h4><div class="menu-%e3%83%95%e3%83%83%e3%82%bf%e3%83%bc%e3%82%a6%e3%82%a3%e3%82%b8%e3%82%a7%e3%83%83%e3%83%88-container"><ul class="menu" id="menu-%e3%83%95%e3%83%83%e3%82%bf%e3%83%bc%e3%82%a6%e3%82%a3%e3%82%b8%e3%82%a7%e3%83%83%e3%83%88"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15934" id="menu-item-15934"><a href="https://autofx-now.com/first-fx">はじめてのFX</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15935" id="menu-item-15935"><a href="https://autofx-now.com/first-mt4">はじめてのMT4</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15937" id="menu-item-15937"><a href="https://autofx-now.com/myea">稼働中のMT4 EA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15936" id="menu-item-15936"><a href="https://autofx-now.com/forex-tools">FXの便利ツール</a></li>
</ul></div></div> </div>
<div class="fblock">
<div class="ft_widget widget widget_categories"><h4 class="ft_title">カテゴリー</h4>
<ul>
<li class="cat-item cat-item-118"><a href="https://autofx-now.com/category/invest">FX・その他投資 <span class="entry-count dfont">20</span></a>
</li>
<li class="cat-item cat-item-25"><a href="https://autofx-now.com/category/ea">MT4 EA <span class="entry-count dfont">32</span></a>
</li>
<li class="cat-item cat-item-20"><a href="https://autofx-now.com/category/mt4">MT4運用 <span class="entry-count dfont">21</span></a>
</li>
<li class="cat-item cat-item-6"><a href="https://autofx-now.com/category/toraripi">トラリピ <span class="entry-count dfont">16</span></a>
</li>
<li class="cat-item cat-item-36"><a href="https://autofx-now.com/category/report">運用レポート <span class="entry-count dfont">105</span></a>
</li>
</ul>
</div> </div>
<div class="fblock last">
<div class="ft_widget widget widget_nav_menu"><h4 class="ft_title">サイト情報</h4><div class="menu-%e3%82%b5%e3%82%a4%e3%83%88%e6%83%85%e5%a0%b1-container"><ul class="menu" id="menu-%e3%82%b5%e3%82%a4%e3%83%88%e6%83%85%e5%a0%b1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17887" id="menu-item-17887"><a href="https://autofx-now.com/about">このサイトについて</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9001" id="menu-item-9001"><a href="https://autofx-now.com/fx_sitemap">サイトマップ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11061" id="menu-item-11061"><a href="https://autofx-now.com/policy">サイトポリシー</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9804" id="menu-item-9804"><a href="https://docs.google.com/forms/d/e/1FAIpQLSfu8NPaEZF0nqWkh7E0hMte2DaRRwCRLc4Hn7bji_B9FhYDxQ/viewform?entry.1031661350=https://autofx-now.com">お問い合わせ</a></li>
</ul></div></div> </div>
</div>
<div id="footer-menu">
<div>
<a class="footer-menu__btn dfont" href="https://autofx-now.com/"><i class="fas fa-home"></i> HOME</a>
</div>
<nav>
</nav>
<p class="copyright dfont">
            © 2021            ただいまFX自動売買中            All rights reserved.
          </p>
</div>
</footer>
</div>
<script id="google-invisible-recaptcha-js-before" type="text/javascript">
var renderInvisibleReCaptcha = function() {

    for (var i = 0; i < document.forms.length; ++i) {
        var form = document.forms[i];
        var holder = form.querySelector('.inv-recaptcha-holder');

        if (null === holder) continue;
		holder.innerHTML = '';

         (function(frm){
			var cf7SubmitElm = frm.querySelector('.wpcf7-submit');
            var holderId = grecaptcha.render(holder,{
                'sitekey': '6LfgSTMUAAAAAP44mw1M6ajprz3CMickTxbYmhOZ', 'size': 'invisible', 'badge' : 'bottomright',
                'callback' : function (recaptchaToken) {
					if((null !== cf7SubmitElm) && (typeof jQuery != 'undefined')){jQuery(frm).submit();grecaptcha.reset(holderId);return;}
					 HTMLFormElement.prototype.submit.call(frm);
                },
                'expired-callback' : function(){grecaptcha.reset(holderId);}
            });

			if(null !== cf7SubmitElm && (typeof jQuery != 'undefined') ){
				jQuery(cf7SubmitElm).off('click').on('click', function(clickEvt){
					clickEvt.preventDefault();
					grecaptcha.execute(holderId);
				});
			}
			else
			{
				frm.onsubmit = function (evt){evt.preventDefault();grecaptcha.execute(holderId);};
			}


        })(form);
    }
};
</script>
<script async="" defer="" id="google-invisible-recaptcha-js" src="https://www.google.com/recaptcha/api.js?onload=renderInvisibleReCaptcha&amp;render=explicit&amp;hl=ja" type="text/javascript"></script>
<script id="wp-embed-js" src="https://autofx-now.com/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
</body>
</html>
