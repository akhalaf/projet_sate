<html xmlns="http://www.w3.org/TR/REC-html40" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="index_archivos/filelist.xml" rel="File-List"/>
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
b\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>SINELMECA</title>
<style>
<!--
 /* Definiciones de fuente */
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Arial;
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Californian FB";
	panose-1:2 7 4 3 6 8 11 3 2 4;}
@font-face
	{font-family:"Times New Roman";
	panose-1:2 2 6 3 5 4 5 2 3 4;}
@font-face
	{font-family:"Arial Narrow";
	panose-1:2 11 6 6 2 2 2 3 2 4;}
 /* Definiciones de estilo */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-right:0pt;
	text-indent:0pt;
	margin-top:0pt;
	margin-bottom:6.0pt;
	line-height:119%;
	text-align:left;
	font-family:Calibri;
	font-size:10.0pt;
	color:black;}
ol
	{margin-top:0in;
	margin-bottom:0in;
	margin-left:-2197in;}
ul
	{margin-top:0in;
	margin-bottom:0in;
	margin-left:-2197in;}
@page
	{size:8.0302in 11.0in;}
-->
</style>
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="3078" fillcolor="#03c [1]"
  strokecolor="black [0]">
  <v:fill color="#03c [1]" color2="white [7]"/>
  <v:stroke color="black [0]" color2="white [7]" weight="2pt">
   <o:left v:ext="view" color="black [0]" color2="white [7]"/>
   <o:top v:ext="view" color="black [0]" color2="white [7]"/>
   <o:right v:ext="view" color="black [0]" color2="white [7]"/>
   <o:bottom v:ext="view" color="black [0]" color2="white [7]"/>
   <o:column v:ext="view" color="black [0]" color2="white [7]"/>
  </v:stroke>
  <v:shadow color="black [0]"/>
  <o:extrusion v:ext="view" backdepth="0" viewpoint="0,0" viewpointorigin="0,0"/>
  <v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt"/>
  <o:colormru v:ext="edit" colors="#0f204f,#001672,#00204e,#f60"/>
 </o:shapedefaults><o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
  <o:regrouptable v:ext="edit">
   <o:entry new="1" old="0"/>
  </o:regrouptable>
 </o:shapelayout></xml><![endif]-->
</head>
<body bgcolor="#00204E" link="#660099" style="margin:0" vlink="teal">
<div style="position:absolute;width:14.0156in;height:8.0459in">
<!--[if gte vml 1]><v:shapetype id="_x0000_t202" coordsize="21600,21600" o:spt="202"
 path="m,l,21600r21600,l21600,xe">
 <v:stroke joinstyle="miter"/>
 <v:path gradientshapeok="t" o:connecttype="rect"/>
</v:shapetype><v:shape id="_x0000_s1025" type="#_x0000_t202" style='position:absolute;
 left:319.13pt;top:591.3pt;width:380.21pt;height:24pt;z-index:1;
 mso-wrap-distance-left:2.88pt;mso-wrap-distance-top:2.88pt;
 mso-wrap-distance-right:2.88pt;mso-wrap-distance-bottom:2.88pt' filled="f"
 fillcolor="#03c [1]" stroked="f" strokecolor="black [0]" strokeweight="2pt"
 o:cliptowrap="t">
 <v:fill color2="white [7]"/>
 <v:stroke color2="white [7]">
  <o:left v:ext="view" color="black [0]" color2="white [7]"/>
  <o:top v:ext="view" color="black [0]" color2="white [7]"/>
  <o:right v:ext="view" color="black [0]" color2="white [7]"/>
  <o:bottom v:ext="view" color="black [0]" color2="white [7]"/>
  <o:column v:ext="view" color="black [0]" color2="white [7]"/>
 </v:stroke>
 <v:shadow color="black [0]"/>
 <o:extrusion v:ext="view" backdepth="0" viewpoint="0,0" viewpointorigin="0,0"/>
 <v:textbox style='mso-column-margin:2mm' inset="2.88pt,2.88pt,2.88pt,2.88pt"/>
</v:shape><![endif]--><span style="position:absolute;z-index:1;
left:426px;top:788px;width:506px;height:32px">
<table cellpadding="0" cellspacing="0">
<tr>
<td height="32" style="vertical-align:top" valign="middle" width="506">
<div class="shape" style="padding:6.0pt 2.88pt 2.88pt 2.88pt" v:shape="_x0000_s1025">
<p class="MsoNormal" style="text-align:center;margin-bottom:0pt;text-align:
  center"><span lang="es-MX" style="font-size:8.0pt;line-height:119%;font-family:
  Verdana;color:white;font-style:italic;text-transform:uppercase;language:es-MX">©</span><span lang="es-MX" style="font-size:8.0pt;line-height:119%;font-family:Verdana;
  color:white;font-style:italic;text-transform:uppercase;language:es-MX"> Sinelmeca 2017.</span></p>
</div>
</td>
</tr>
</table>
</span><v:rect fillcolor="#03c [1]" filled="f" id="_x0000_s1026" o:cliptowrap="t" o:preferrelative="t" strokecolor="black [0]" stroked="f" strokeweight="2pt" style="position:absolute;
 left:.27pt;top:227.9pt;width:907.2pt;height:156.91pt;z-index:2;
 mso-wrap-distance-left:2.88pt;mso-wrap-distance-top:2.88pt;
 mso-wrap-distance-right:2.88pt;mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:imagedata o:title="index1" src="index_archivos/image310.png"></v:imagedata>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<o:lock aspectratio="t" v:ext="edit"></o:lock>
</v:rect><span style="position:absolute;z-index:2;
left:-2px;top:301px;width:1214px;height:215px"><img height="215" src="index_archivos/image310.png" v:shapes="_x0000_s1026" width="1214"/></span><v:rect fillcolor="#03c [1]" filled="f" id="_x0000_s1027" o:cliptowrap="t" o:preferrelative="t" strokecolor="black [0]" stroked="f" strokeweight="2pt" style="position:absolute;left:100.38pt;top:415.31pt;width:907.2pt;
 height:174.49pt;z-index:3;mso-wrap-distance-left:2.88pt;
 mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
 mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:imagedata o:title="index2" src="index_archivos/image312.png"></v:imagedata>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<o:lock aspectratio="t" v:ext="edit"></o:lock>
</v:rect><span style="position:absolute;z-index:3;
left:131px;top:551px;width:1215px;height:238px"><img height="238" src="index_archivos/image312.png" v:shapes="_x0000_s1027" width="1215"/></span><v:group coordorigin="1037082,1062974" coordsize="130873,26367" id="_x0000_s1082" o:cliptowrap="t" style="position:absolute;left:-6pt;top:.19pt;width:1030.5pt;
 height:207.62pt;z-index:4">
<v:roundrect arcsize="10923f" fillcolor="gray [7 darken(128)]" href="servicios.html" id="_x0000_s1083" o:cliptowrap="t" strokecolor="#ffc000" strokeweight="2pt" style="position:absolute;
  left:1125245;top:1082180;width:18783;height:6375;mso-wrap-distance-left:2.88pt;
  mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
  mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr" style="padding-top:7.74pt">
<p class="MsoNormal" style="text-align:center;text-align:center"><span lang="es-MX" style="font-size:18.0pt;line-height:119%;font-family:Arial;
   color:white;font-weight:bold;language:es-MX">SERVICIOS</span></p>
</div>
</v:textbox>
</v:roundrect><v:roundrect arcsize="10923f" fillcolor="gray [7 darken(128)]" href="contacto.html" id="_x0000_s1084" o:cliptowrap="t" strokecolor="#ffc000" strokeweight="2pt" style="position:absolute;
  left:1144637;top:1082180;width:18685;height:6375;mso-wrap-distance-left:2.88pt;
  mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
  mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr" style="padding-top:7.74pt">
<p class="MsoNormal" style="text-align:center;text-align:center"><span lang="es-MX" style="font-size:18.0pt;line-height:119%;font-family:Arial;
   color:white;font-weight:bold;language:es-MX">CONTACTOS</span></p>
</div>
</v:textbox>
</v:roundrect><v:roundrect arcsize="10923f" fillcolor="gray [7 darken(128)]" href="sinelmeca.html" id="_x0000_s1085" o:cliptowrap="t" strokecolor="#ffc000" strokeweight="2pt" style="position:absolute;
  left:1105878;top:1082180;width:18864;height:6375;mso-wrap-distance-left:2.88pt;
  mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
  mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr" style="padding-top:7.74pt">
<p class="MsoNormal" style="text-align:center;text-align:center"><span lang="es-MX" style="font-size:18.0pt;line-height:119%;font-family:Arial;
   color:white;font-weight:bold;language:es-MX">SINELMECA</span></p>
</div>
</v:textbox>
</v:roundrect><v:rect fillcolor="#ffc000" id="_x0000_s1086" o:cliptowrap="t" strokecolor="silver [7 darken(192)]" stroked="f" strokeweight="2pt" style="position:absolute;left:1037082;
  top:1069086;width:130873;height:14092;mso-wrap-distance-left:2.88pt;
  mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
  mso-wrap-distance-bottom:2.88pt">
<v:fill angle="180" color2="silver [7 darken(192)]" colors="10485f silver" focus="100%" method="none" rotate="t" type="gradient"></v:fill>
<v:stroke color2="white [7]">
<o:left color="silver [7 darken(192)]" color2="white [7]" joinstyle="miter" v:ext="view" weight="2pt"></o:left>
<o:top color="silver [7 darken(192)]" color2="white [7]" joinstyle="miter" v:ext="view" weight="2pt"></o:top>
<o:right color="silver [7 darken(192)]" color2="white [7]" joinstyle="miter" v:ext="view" weight="2pt"></o:right>
<o:bottom color="silver [7 darken(192)]" color2="white [7]" joinstyle="miter" v:ext="view" weight="2pt"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr" style="padding-top:80.49pt">
<p class="MsoNormal" style="text-align:center;margin-left:761.9103pt;
   margin-right:33.3134pt;text-align:center"><span lang="es-VE" style="font-size:
   12.0pt;line-height:119%;font-family:Arial;font-style:italic;font-weight:
   bold;language:es-VE"> </span></p>
<p class="MsoNormal" style="text-align:right;margin-left:571.7014pt;
   margin-right:33.3134pt;line-height:94%;text-align:right"><span lang="es-MX" style='font-size:14.0pt;line-height:94%;font-family:"Californian FB";
   color:#3F3F3F;font-weight:bold;text-transform:uppercase;language:es-MX'> </span></p>
</div>
</v:textbox>
</v:rect><v:shape fillcolor="#03c [1]" filled="f" id="_x0000_s1087" o:cliptowrap="t" strokecolor="black [0]" stroked="f" strokeweight="2pt" style="position:absolute;
  left:1121305;top:1079451;width:42974;height:3002;mso-wrap-distance-left:2.88pt;
  mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
  mso-wrap-distance-bottom:2.88pt" type="#_x0000_t202">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr">
<p class="MsoNormal" style="margin-bottom:4.0pt"><span lang="es-MX" style='font-size:14.0pt;line-height:119%;font-family:"Times New Roman";
   color:#00204E;font-weight:bold;language:es-MX'>SU ALIADO EN SERVICIOS Y MANTENIMIENTOS</span></p>
</div>
</v:textbox>
</v:shape><v:group coordorigin="1042119,1047987" coordsize="65708,26367" id="_x0000_s1088" o:cliptowrap="t" style="position:absolute;left:1042119;
  top:1062974;width:65709;height:26367">
<v:rect fillcolor="#03c [1]" filled="f" id="_x0000_s1089" o:cliptowrap="t" o:preferrelative="t" strokecolor="black [0]" stroked="f" strokeweight="2pt" style="position:absolute;left:1042119;top:1047987;
   width:65709;height:26367;mso-wrap-distance-left:2.88pt;
   mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
   mso-wrap-distance-bottom:2.88pt">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:imagedata cropbottom="17566f" croptop="10754f" o:title="logo-apaisado-sin-bordes" src="index_archivos/image303.png"></v:imagedata>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<o:lock aspectratio="t" v:ext="edit"></o:lock>
</v:rect><v:shape fillcolor="#03c [1]" filled="f" id="_x0000_s1090" o:cliptowrap="t" strokecolor="black [0]" stroked="f" strokeweight="2pt" style="position:absolute;
   left:1066317;top:1064791;width:14530;height:5732;mso-wrap-distance-left:2.88pt;
   mso-wrap-distance-top:2.88pt;mso-wrap-distance-right:2.88pt;
   mso-wrap-distance-bottom:2.88pt" type="#_x0000_t202">
<v:fill color2="white [7]"></v:fill>
<v:stroke color2="white [7]">
<o:left color="black [0]" color2="white [7]" v:ext="view"></o:left>
<o:top color="black [0]" color2="white [7]" v:ext="view"></o:top>
<o:right color="black [0]" color2="white [7]" v:ext="view"></o:right>
<o:bottom color="black [0]" color2="white [7]" v:ext="view"></o:bottom>
<o:column color="black [0]" color2="white [7]" v:ext="view"></o:column>
</v:stroke>
<v:shadow color="black [0]"></v:shadow>
<o:extrusion backdepth="0" v:ext="view" viewpoint="0,0" viewpointorigin="0,0"></o:extrusion>
<v:textbox inset="2.88pt,2.88pt,2.88pt,2.88pt" style="mso-column-margin:2mm">
<div dir="ltr">
<p class="MsoNormal"><span lang="es-MX" style='font-size:14.0pt;line-height:
    119%;font-family:"Arial Narrow";color:#00204E;font-weight:bold;language:
    es-MX'>J-40807905-4</span></p>
</div>
</v:textbox>
</v:shape></v:group></v:group><span style="position:
absolute;z-index:4;left:-10px;top:-2px;width:1379px;height:282px"><map name="MicrosoftOfficeMap0"><area coords="52, 0, 747, 282" nohref="" shape="Rect"/><area coords="0, 64, 1379, 217" nohref="" shape="Rect"/><area coords="736, 204, 728, 207, 725, 215, 725, 261, 728, 269, 736, 272, 912, 272, 920, 269, 923, 261, 923, 215, 920, 207, 912, 204, 736, 204" href="sinelmeca.html" shape="Polygon"/><area coords="1143, 204, 1134, 207, 1131, 216, 1131, 260, 1134, 268, 1143, 271, 1317, 271, 1325, 268, 1327, 260, 1327, 216, 1325, 207, 1317, 204, 1143, 204" href="contacto.html" shape="Polygon"/><area coords="939, 204, 931, 207, 928, 215, 928, 260, 931, 269, 939, 271, 1114, 271, 1122, 269, 1125, 260, 1125, 215, 1122, 207, 1114, 204, 939, 204" href="servicios.html" shape="Polygon"/></map><img border="0" height="282" src="index_archivos/image294.png" usemap="#MicrosoftOfficeMap0" v:shapes="_x0000_s1082 _x0000_s1083 _x0000_s1084 _x0000_s1085 _x0000_s1086 _x0000_s1087 _x0000_s1088 _x0000_s1089 _x0000_s1090" width="1379"/></span>
</div>
</body>
</html>
