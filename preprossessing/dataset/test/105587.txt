<!DOCTYPE html>
<html class="" lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, user-scallable=no" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta content="Altavista Wines - " property="og:title"/>
<meta content="https://altavistawines.com/" property="og:url"/>
<meta content="" property="og:description"/>
<meta content="https://altavistawines.com/wp-content/themes/altavista-2017/img/logofb.png" property="og:image"/> <!--  gal  -->
<title>Altavista Wines - </title>
<meta content="all" name="robots"/>
<link href="https://altavistawines.com/wp-content/themes/altavista-2017/css/foundation.css" rel="stylesheet"/>
<link href="https://altavistawines.com/wp-content/themes/altavista-2017/css/helpers.css" rel="stylesheet"/>
<link href="https://altavistawines.com/wp-content/themes/altavista-2017/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://altavistawines.com/wp-content/themes/altavista-2017/mq.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600" rel="stylesheet" type="text/css"/>
<link href="https://altavistawines.com/feed/" rel="alternate" title="Altavista Wines RSS Feed" type="application/rss+xml"/>
<link href="https://altavistawines.com/xmlrpc.php" rel="pingback"/>
<meta content="noindex,nofollow" name="robots"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.4 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
	var mi_version         = '7.10.4';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-143791118-1';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-143791118-1', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('send','pageview');
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Función desactivada __gaTracker(' + arguments[0] + " ....) porque no estás siendo rastreado. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/altavistawines.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=59d0f7fe4687789697a05d1a9f360029"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://altavistawines.com/wp-content/plugins/wp-colorbox/example5/colorbox.css?ver=59d0f7fe4687789697a05d1a9f360029" id="colorbox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://altavistawines.com/wp-includes/css/dist/block-library/style.min.css?ver=59d0f7fe4687789697a05d1a9f360029" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://altavistawines.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//altavistawines.com/wp-content/uploads/pum/pum-site-styles.css?generated=1592943464&amp;ver=1.9.1" id="popup-maker-site-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://altavistawines.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="colorbox-js" src="https://altavistawines.com/wp-content/plugins/wp-colorbox/jquery.colorbox-min.js?ver=1.1.2" type="text/javascript"></script>
<script id="wp-colorbox-js" src="https://altavistawines.com/wp-content/plugins/wp-colorbox/wp-colorbox.js?ver=1.1.2" type="text/javascript"></script>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/altavistawines.com","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://altavistawines.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.4" type="text/javascript"></script>
<link href="https://altavistawines.com/wp-json/" rel="https://api.w.org/"/><link href="https://altavistawines.com/wp-json/wp/v2/pages/12" rel="alternate" type="application/json"/><link href="https://altavistawines.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://altavistawines.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://altavistawines.com/" rel="canonical"/>
<link href="https://altavistawines.com/" rel="shortlink"/>
<link href="https://altavistawines.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Faltavistawines.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://altavistawines.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Faltavistawines.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		if (window.wfLogHumanRan) { return; }
		window.wfLogHumanRan = true;
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//altavistawines.com/?wordfence_lh=1&hid=2224028106DF51DF391B87B8F52F1C1C');
</script><style type="text/css">
.qtranxs_flag_en {background-image: url(https://altavistawines.com/wp-content/plugins/qtranslate-x/flags/gb.png); background-repeat: no-repeat;}
.qtranxs_flag_es {background-image: url(https://altavistawines.com/wp-content/plugins/qtranslate-x/flags/es.png); background-repeat: no-repeat;}
</style>
<link href="https://altavistawines.com/en/" hreflang="en" rel="alternate"/>
<link href="https://altavistawines.com/es/" hreflang="es" rel="alternate"/>
<link href="https://altavistawines.com/" hreflang="x-default" rel="alternate"/>
<meta content="qTranslate-X 3.4.6.8" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <style id="wp-custom-css" type="text/css">
			#popmake-1868 img{ margin:0;}		</style>
<script src="https://altavistawines.com/wp-content/themes/altavista-2017/js/modernizr.js"></script>
<script src="https://altavistawines.com/wp-content/themes/altavista-2017/js/jquery.js"></script>
<script src="https://altavistawines.com/wp-content/themes/altavista-2017/js/idangerous.swiper-2.1.min.js"></script>
<script src="https://altavistawines.com/wp-content/themes/altavista-2017/js/jquery.colorbox-min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="">
<div class="prehome" style='background-image: url("https://altavistawines.com/wp-content/uploads/2015/09/prehomebg.jpg")'>
<div class="row">
<div class="columns medium-12 text-center">
<h1></h1>
<span>¿Sos mayor de 18 años?</span>
<div class="selectpais">
<div class="selected">Argentina</div>
<ul>
<li><a data-pais="argentina" href="#">Argentina</a></li>
<li><a data-pais="usa" href="#">USA</a></li>
<li><a data-pais="francia" href="#">Francia</a></li>
<li><a data-pais="canada" href="#">Canadá</a></li>
<li><a data-pais="holanda" href="#">Holanda</a></li>
<li><a data-pais="singapur" href="#">Singapur</a></li>
<li><a data-pais="hong-kong" href="#">Hong Kong</a></li>
<li><a data-pais="vietnam" href="#">Vietnam</a></li>
<li><a data-pais="venezuela" href="#">Venezuela</a></li>
<li><a data-pais="mexico" href="#">Mexico</a></li>
<li><a data-pais="colombia" href="#">Colombia</a></li>
<li><a data-pais="brasil" href="#">Brasil</a></li>
<li><a data-pais="peru" href="#">Perú</a></li>
<li><a data-pais="bolivia" href="#">Bolivia</a></li>
<li><a data-pais="paraguay" href="#">Paraguay</a></li>
<li><a data-pais="costa-rica" href="#">Costa Rica</a></li>
<li><a data-pais="panama" href="#">Panamá</a></li>
<li><a data-pais="uruguay" href="#">Uruguay</a></li>
<li><a data-pais="suiza" href="#">Suiza</a></li>
<li><a data-pais="tailandia" href="#">Tailandia</a></li>
<li><a data-pais="ucrania" href="#">Ucrania</a></li>
<li><a data-pais="corea" href="#">Corea</a></li>
<li><a data-pais="rusia" href="#">Rusia</a></li>
<li><a data-pais="uk" href="#">UK</a></li>
<li><a data-pais="trinidad" href="#">Trinidad</a></li>
<li><a data-pais="nueva-zelanda" href="#">Nueva Zelanda</a></li>
<li><a data-pais="japon" href="#">Japón</a></li>
<li><a data-pais="alemania" href="#">Alemania</a></li>
<li><a data-pais="guatemala" href="#">Guatemala</a></li>
<li><a data-pais="filipinas" href="#">Filipinas</a></li>
<li><a data-pais="puerto-rico" href="#">Puerto Rico</a></li>
</ul>
</div>
<a class="sel" href="https://altavistawines.com/home/?pais=argentina" id="si">Si</a>
<a class="sel" href="#">No</a>
<div class="cons">
			Consumí con responsabilidad		</div>
<div class="rec">
			Recordarme		</div>
<p>
			Al entrar estás de acuerdo con los <br/>
<a href="https://altavistawines.com/terminos-y-condiciones/">Términos y Condiciones</a> y las <a href="https://altavistawines.com/politicas-de-privacidad/">Políticas de Privacidad</a>
</p>
</div>
</div>
</div>
<script>
$(document).ready(function () {
$( '.selected').click(function(e) {
  $('.selectpais').toggleClass('open');
  e.preventDefault();
});

$( '.selectpais ul a').click(function(e) {
  var pais = $(this).data('pais');
  $('#si').attr({
  	href: 'https://altavistawines.com/home/?pais='+pais,
  });
  $( '.selected').html( $(this).html() );
  $('.selectpais').removeClass('open');
  e.preventDefault();
});

});
</script>
<script src="https://altavistawines.com/wp-content/themes/altavista-2017/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
<div aria-hidden="true" class="pum pum-overlay pum-theme-1863 pum-theme-lightbox popmake-overlay auto_open click_open" data-popmake='{"id":1868,"slug":"coronavirus","theme_id":1863,"cookies":[{"event":"on_popup_close","settings":{"name":"pum-1868","time":"1 day","path":true}}],"triggers":[{"type":"auto_open","settings":{"cookie_name":["pum-1868"],"delay":"500"}},{"type":"click_open","settings":{"extra_selectors":"","cookie_name":null}}],"mobile_disabled":null,"tablet_disabled":null,"meta":{"display":{"stackable":false,"overlay_disabled":false,"scrollable_content":false,"disable_reposition":false,"size":"auto","responsive_min_width":"0%","responsive_min_width_unit":false,"responsive_max_width":"100%","responsive_max_width_unit":false,"custom_width":"640px","custom_width_unit":false,"custom_height":"380px","custom_height_unit":false,"custom_height_auto":false,"location":"center","position_from_trigger":false,"position_top":"100","position_left":"0","position_bottom":"0","position_right":"0","position_fixed":false,"animation_type":"fade","animation_speed":"350","animation_origin":"center top","overlay_zindex":false,"zindex":"1999999999"},"close":{"text":"","button_delay":"0","overlay_click":false,"esc_press":false,"f4_press":false},"click_open":[]}}' id="pum-1868" role="dialog">
<div class="pum-container popmake theme-1863" id="popmake-1868">
<div class="pum-content popmake-content">
<p><img alt="" class="aligncenter wp-image-1879 size-full" height="600" src="https://altavistawines.com/wp-content/uploads/2020/03/POP_UP2.jpg" width="600"/></p>
</div>
<button aria-label="Cerrar" class="pum-close popmake-close" type="button">
			×            </button>
</div>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/altavistawines.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://altavistawines.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7" type="text/javascript"></script>
<script id="jquery-ui-core-js" src="https://altavistawines.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script id="jquery-ui-position-js" src="https://altavistawines.com/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4" type="text/javascript"></script>
<script id="popup-maker-site-js-extra" type="text/javascript">
/* <![CDATA[ */
var pum_vars = {"version":"1.9.1","ajaxurl":"https:\/\/altavistawines.com\/wp-admin\/admin-ajax.php","restapi":"https:\/\/altavistawines.com\/wp-json\/pum\/v1","rest_nonce":null,"default_theme":"1862","debug_mode":"","disable_tracking":"","home_url":"\/","message_position":"top","core_sub_forms_enabled":"1","popups":[]};
var ajaxurl = "https:\/\/altavistawines.com\/wp-admin\/admin-ajax.php";
var pum_sub_vars = {"ajaxurl":"https:\/\/altavistawines.com\/wp-admin\/admin-ajax.php","message_position":"top"};
var pum_popups = {"pum-1868":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_form_submission":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"triggers":[{"type":"auto_open","settings":{"cookie_name":["pum-1868"],"delay":"500"}}],"cookies":[{"event":"on_popup_close","settings":{"name":"pum-1868","time":"1 day","path":true}}],"theme_id":"1863","size":"auto","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","close_on_form_submission_delay":"0","theme_slug":"lightbox","id":1868,"slug":"coronavirus"}};
/* ]]> */
</script>
<script id="popup-maker-site-js" src="//altavistawines.com/wp-content/uploads/pum/pum-site-scripts.js?defer&amp;generated=1592943464&amp;ver=1.9.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://altavistawines.com/wp-includes/js/wp-embed.min.js?ver=59d0f7fe4687789697a05d1a9f360029" type="text/javascript"></script>
</body>
</html>