<!DOCTYPE html>
<!--
           _    _  _____ _____   _____
     /\   | |  | |/ ____|  __ \ / ____|
    /  \  | |__| | |    | |  | | |
   / /\ \ |  __  | |    | |  | | |
  / ____ \| |  | | |____| |__| | |____
 /_/    \_\_|  |_|\_____|_____/ \_____|

--><html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>AHCDC</title>
<meta content="The Association of Hemophilia Clinic Directors of Canada(AHCDC) is an organization of Hemophlia Clinic Directors from across Canada" name="description"/>
<link href="https://www.ahcdc.ca/humans.txt" rel="author"/>
<meta content="True" name="HandheldFriendly"/>
<meta content="320" name="MobileOptimized"/>
<meta content="width=device-width, initial-scale=1 minimum-scale=1" name="viewport"/>
<meta content="bkCNBVm75BsVKEac0BeoyhVg11t5d825JsnIFicd" name="csrf-token"/>
<meta content="index,follow" name="robots"/>
<link href="https://www.ahcdc.ca/assets/images/favicon.png" rel="shortcut icon"/>
<link href="https://www.ahcdc.ca/assets/css/app.css" media="all" rel="stylesheet" type="text/css"/>
</head>
<body class="preload pages-home">
<header>
<div class="header-background">
<div class="header-gradient">
<div class="uk-container uk-container-center">
<div class="logo">
<a href="https://www.ahcdc.ca">
<img alt="Association of Hemophilia Clinic of Canada" src="https://www.ahcdc.ca/assets/images/logo_white.png"/>
<span class="logo-text">Association of Hemophilia Clinic Directors of Canada</span>
</a>
</div>
</div>
</div>
</div>
<div class="navbar">
<nav class="uk-container uk-container-center">
<a class="uk-navbar-brand" href="https://www.ahcdc.ca"><img class="logo-small" src="https://www.ahcdc.ca/assets/images/logo_black.png"/></a>
<div id="navicon"></div>
<div id="mainNav">
<ul class="uk-navbar-nav">
<li>
<a href="https://www.ahcdc.ca/about-ahcdc">
			About Us
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/about-ahcdc">
			About AHCDC
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/about-us">
			Contact Us
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/by-laws-policies">
			By-Laws &amp; Policies
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/members-list">
			Membership
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/board-of-directors">
			Board of Directors
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/members-list">
			Members list
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/committees">
			Committees
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/meetings-events">
			Meetings &amp; Events
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/calendar">
			Calendar
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/granting-agencies">
			Research
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/granting-agencies">
			Granting Agencies
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/ahcdc-publications">
			AHCDC Publications
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp">
			BCHERP
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/bcherp-grant-criteria">
			Grant Criteria
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp-funding-process">
			Funding Process
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp-application-form">
			Application Form
		</a>
</li>
</ul>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/links">
			Resources
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/plasma-protein-products">
			Plasma Protein Products
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/guides">
			Guides
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/national-genotyping-service">
			National Genotyping
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/rare-inherited-bleeding-disorders">
			Rare Inherited Bleeding Disorders
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/links">
			Links
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/canadian-hemophilia-registry">
			Registry
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/canadian-hemophilia-registry">
			Canadian Hemophilia Registry
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/cbdr">
			CBDR
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms">
			CHARMS
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/charms-adverse-drug-reaction-module">
			Adverse Drug Reaction Module
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms-how-to-submit-an-adr">
			How to submit an ADR
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms-how-to-review-an-adr">
			How to review an ADR
		</a>
</li>
</ul>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/chess">
			National Studies
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/chess">
			CHESS
		</a>
</li>
</ul>
</li>
<!-- Right Side Of Navbar -->
<li>
<a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                            Member<span class="uk-icon-caret-down uk-margin-small-left"></span>
</a>
<ul>
<!-- Authentication Links -->
<li><a href="https://www.ahcdc.ca/login"><i class="uk-icon uk-icon-sign-in uk-margin-small-right"></i> Login</a></li>
</ul>
</li>
</ul>
</div>
</nav>
</div>
</header>
<div class="uk-text-center" id="content">
<div class="section">
<div class="uk-container uk-container-center">
<h1>Who is the AHCDC ?</h1>
<p class="intro">The Association of Hemophilia Clinic Directors of Canada (AHCDC) is an organization of Hemophilia Clinic Directors from across Canada, incorporated under the laws of the Province of Ontario in 1994. We share a common interest in improving the treatment of people with hemophilia. The association meets once a year, often in conjunction with the Canadian Hemophilia Society (CHS).</p>
</div>
</div>
<div class="section">
<div class="uk-container uk-container-center">
<div class="uk-display-inline-block small-block">
<img src="https://www.ahcdc.ca/assets/images/target.png"/>
<h2 class="small">Our mission</h2>
<p>The goal of the AHCDC is to ensure excellent care for persons with congenital bleeding disorders in Canada through clinical services, research and education.</p>
</div>
<div class="uk-display-inline-block small-block">
<img src="https://www.ahcdc.ca/assets/images/executives.png"/>
<h2 class="small">Our executives</h2>
<ul class="uk-list">
<li>Dr. M. Sholzberg    President</li>
<li>Dr. R. Sinha   Vice President</li>
<li>Dr. Stephanie Cloutier     Treasurer</li>
<li>Dr. M.  Belletrutti     Secretary</li>
<li>Dr. R. J. Klaassen   Past President</li>
<li>Dr. N. Pardy   Privacy Officer</li>
</ul>
</div>
</div>
</div>
<div class="section">
<div class="uk-container uk-container-center">
<div class="article-feed">
<h2>The latest news</h2>
<article>
<h1>Thrombosis/Hemostasis Fellowship</h1>
<time datetime="2020-04-27">April 27, 2020</time>
<p>Last minute opening for application to the 2021-22 Baxter-Takeda Hemostasis/Thrombosis SickKids Fellowship.</p>
<a class="uk-button" href="news/thrombosishemostasis-fellowship">Read more</a>
</article>
<article>
<h1>Cancellation Notice - 2020 AHCDC Annual General Meeting</h1>
<time datetime="2020-03-13">March 13, 2020</time>
<p>Due to the rapidly evolving spread and on-going uncertainty of COVID-19 risk, the AHCDC regrets to inform you that 2020 AHCDC Annual General Meeting, which was scheduled for May 28 - 30. 2020, has been cancelled. </p>
<a class="uk-button" href="news/cancellation-notice-2020-ahcdc-annual-general-meeting">Read more</a>
</article>
<article>
<h1>2020 Combined CHS CodeRouge/AHCDC Annual General Meeting</h1>
<time datetime="2020-02-03">February 03, 2020</time>
<p>The combined 2020 CHS CodeRouge/AHCDC Annual General Meeting will take place in Edmonton, AB, May 29 - 30, 2020 at the Westin Edmonton. 
</p>
<a class="uk-button" href="news/2020-combined-chs-coderougeahcdc-annual-general-meeting">Read more</a>
</article>
<article>
<h1>THSNA 2020 Summit</h1>
<time datetime="2019-12-03">December 03, 2019</time>
<p>THSNA 2020 Summit will be in Chicago, April 23-25, 2020. Abstract submission deadline is December 13, 2019 and early bird registration deadline is December 15, 2019.</p>
<a class="uk-button" href="news/thsna-2020">Read more</a>
</article>
<a class="uk-button" href="https://www.ahcdc.ca/news">See all</a>
</div>
</div>
</div>
<div>
<div class="uk-container uk-container-center contact">
<h2>Contact us</h2>
<address>
<span><i class="fa fa-map-marker"></i>114 Cheyenne Way Ottawa, ON K2J 0E9</span>
<span>Rob Gallaher</span>
<span><i class="fa fa-phone"></i><a href="tel:1-877-212-5501,2">1-877-212-5501 ext.2</a></span>
<span><i class="fa fa-envelope-o"></i><a href="mailto:ahcdc@gallaher.ca">ahcdc@gallaher.ca</a></span>
</address>
</div>
</div>
</div>
<footer>
<div class="uk-container uk-container-center">
<nav>
<ul>
<li>
<a href="https://www.ahcdc.ca/about-ahcdc">
			About Us
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/about-ahcdc">
			About AHCDC
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/about-us">
			Contact Us
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/by-laws-policies">
			By-Laws &amp; Policies
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/members-list">
			Membership
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/board-of-directors">
			Board of Directors
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/members-list">
			Members list
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/committees">
			Committees
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/meetings-events">
			Meetings &amp; Events
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/calendar">
			Calendar
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/granting-agencies">
			Research
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/granting-agencies">
			Granting Agencies
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/ahcdc-publications">
			AHCDC Publications
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp">
			BCHERP
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/bcherp-grant-criteria">
			Grant Criteria
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp-funding-process">
			Funding Process
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/bcherp-application-form">
			Application Form
		</a>
</li>
</ul>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/links">
			Resources
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/plasma-protein-products">
			Plasma Protein Products
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/guides">
			Guides
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/national-genotyping-service">
			National Genotyping
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/rare-inherited-bleeding-disorders">
			Rare Inherited Bleeding Disorders
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/links">
			Links
		</a>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/canadian-hemophilia-registry">
			Registry
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/canadian-hemophilia-registry">
			Canadian Hemophilia Registry
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/cbdr">
			CBDR
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms">
			CHARMS
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/charms-adverse-drug-reaction-module">
			Adverse Drug Reaction Module
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms-how-to-submit-an-adr">
			How to submit an ADR
		</a>
</li>
<li>
<a href="https://www.ahcdc.ca/charms-how-to-review-an-adr">
			How to review an ADR
		</a>
</li>
</ul>
</li>
</ul>
</li>
<li>
<a href="https://www.ahcdc.ca/chess">
			National Studies
		</a>
<ul>
<li>
<a href="https://www.ahcdc.ca/chess">
			CHESS
		</a>
</li>
</ul>
</li>
</ul>
</nav>
</div>
<div class="bottom-footer">
<div class="uk-container uk-container-center">
<span>© AHCDC 2021</span>
</div>
</div>
</footer>
<script type="text/javascript">
            var baseUrl = 'https://www.ahcdc.ca';

                    </script>
<script src="https://www.ahcdc.ca/assets/js/components.js"></script>
<script src="https://www.ahcdc.ca/assets/js/app.js"></script>
</body>
</html>
