<!DOCTYPE html>
<!--[if IE 9 ]> <html lang="nl" class="ie9 loading-site no-js"> <![endif]--><!--[if IE 8 ]> <html lang="nl" class="ie8 loading-site no-js"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html class="loading-site no-js" lang="nl"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.praktijkanastasia.nl/xmlrpc.php" rel="pingback"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>404 - Pagina niet gevonden - Praktijk Anastasia in Hoevelaken</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/><meta content="follow, noindex" name="robots"/>
<script type="application/ld+json">{"@context" : "https://schema.org","@type" : "Organization","logo": "https:\/\/www.praktijkanastasia.nl\/wp-content\/uploads\/2018\/04\/logo.png","name" : "Praktijk Anastasia","url" : "https:\/\/www.praktijkanastasia.nl"}</script>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.praktijkanastasia.nl/feed/" rel="alternate" title="Praktijk Anastasia in Hoevelaken » Feed" type="application/rss+xml"/>
<link href="https://www.praktijkanastasia.nl/comments/feed/" rel="alternate" title="Praktijk Anastasia in Hoevelaken » Reactiesfeed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.praktijkanastasia.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.praktijkanastasia.nl/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.9.1" id="cookie-law-info-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.9.1" id="cookie-law-info-gdpr-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/assets/css/fl-icons.css?ver=3.12" id="flatsome-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/inc/integrations/ninjaforms/ninjaforms.css?ver=5.5.3" id="flatsome-ninjaforms-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/assets/css/flatsome.css?ver=3.12.2" id="flatsome-main-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.praktijkanastasia.nl/wp-content/themes/flatsome-child/style.css?ver=3.0" id="flatsome-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Poppins%3Aregular%2C700%2Cregular%2C700%2Cregular&amp;display=swap&amp;ver=3.9" id="flatsome-googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.praktijkanastasia.nl/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="cookie-law-info-js-extra" type="text/javascript">
/* <![CDATA[ */
var Cli_Data = {"nn_cookie_ids":[],"cookielist":[],"ccpaEnabled":"","ccpaRegionBased":"","ccpaBarEnabled":"","ccpaType":"gdpr","js_blocking":"","custom_integration":"","triggerDomRefresh":""};
var cli_cookiebar_settings = {"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":"","button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":"1","button_1_new_win":"","button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":"","button_2_hidebar":"","button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":"1","button_3_new_win":"","button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#62a329","button_4_as_button":"","font_family":"inherit","header_fix":"","notify_animate_hide":"1","notify_animate_show":"","notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":"1","scroll_close_reload":"","accept_close_reload":"","reject_close_reload":"","showagain_tab":"1","showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":"","show_once":"10000","logging_on":"","as_popup":"","popup_overlay":"1","bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"};
var log_object = {"ajax_url":"https:\/\/www.praktijkanastasia.nl\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="cookie-law-info-js" src="https://www.praktijkanastasia.nl/wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.9.1" type="text/javascript"></script>
<link href="https://www.praktijkanastasia.nl/wp-json/" rel="https://api.w.org/"/><link href="https://www.praktijkanastasia.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.praktijkanastasia.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style><!--[if IE]><link rel="stylesheet" type="text/css" href="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/assets/css/ie-fallback.css"><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><script src="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/assets/libs/ie-flexibility.js"></script><![endif]--><script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-55638243-1', 'auto');
ga('send', 'pageview');
</script><style id="custom-css" type="text/css">:root {--primary-color: #446084;}.header-main{height: 169px}#logo img{max-height: 169px}#logo{width:140px;}#logo img{padding:15px 0;}.header-bottom{min-height: 10px}.header-top{min-height: 29px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.header-bg-color, .header-wrapper {background-color: rgba(255,255,255,0.9)}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 16px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: 37px }@media (max-width: 549px) {.header-main{height: 118px}#logo img{max-height: 118px}}body{font-size: 100%;}body{font-family:"Poppins", sans-serif}body{font-weight: 0}.nav > li > a {font-family:"Poppins", sans-serif;}.mobile-sidebar-levels-2 .nav > li > ul > li > a {font-family:"Poppins", sans-serif;}.nav > li > a {font-weight: 700;}.mobile-sidebar-levels-2 .nav > li > ul > li > a {font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Poppins", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}.alt-font{font-family: "Poppins", sans-serif;}.alt-font{font-weight: 0!important;}.label-new.menu-item > a:after{content:"Nieuw";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Aanbieding";}.label-popular.menu-item > a:after{content:"Populair";}</style></head>
<body class="error404 lightbox nav-dropdown-has-arrow">
<a class="skip-link screen-reader-text" href="#main">Skip to content</a>
<div id="wrapper">
<header class="header has-sticky sticky-jump" id="header">
<div class="header-wrapper">
<div class="header-top hide-for-sticky nav-dark hide-for-medium" id="top-bar">
<div class="flex-row container">
<div class="flex-col hide-for-medium flex-left">
<ul class="nav nav-left medium-nav-center nav-small nav-divided">
</ul>
</div>
<div class="flex-col hide-for-medium flex-center">
<ul class="nav nav-center nav-small nav-divided">
</ul>
</div>
<div class="flex-col hide-for-medium flex-right">
<ul class="nav top-bar-nav nav-right nav-small nav-divided">
<li class="header-contact-wrapper">
<ul class="nav nav-divided nav-uppercase header-contact" id="header-contact">
<li class="">
<a class="tooltip" href="https://maps.google.com/?q=Praktijk Anastasia, Sportweg 20, Hoevelaken" rel="noopener noreferrer" target="_blank" title="Praktijk Anastasia, Sportweg 20, Hoevelaken">
<i class="icon-map-pin-fill" style="font-size:15px;"></i> <span>
			     	Ons adres			     </span>
</a>
</li>
<li class="">
<a class="tooltip" href="mailto:info@praktijkanastasia.nl" title="info@praktijkanastasia.nl">
<i class="icon-envelop" style="font-size:15px;"></i> <span>
			       	Contact			       </span>
</a>
</li>
<li class="">
<a class="tooltip" href="tel:06 52 12 61 64" title="06 52 12 61 64">
<i class="icon-phone" style="font-size:15px;"></i> <span>06 52 12 61 64</span>
</a>
</li>
</ul>
</li> </ul>
</div>
</div>
</div>
<div class="header-main " id="masthead">
<div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">
<!-- Logo -->
<div class="flex-col logo" id="logo">
<!-- Header logo -->
<a href="https://www.praktijkanastasia.nl/" rel="home" title="Praktijk Anastasia in Hoevelaken">
<img alt="Praktijk Anastasia in Hoevelaken" class="header_logo header-logo" height="169" src="https://www.praktijkanastasia.nl/wp-content/uploads/2018/04/logo.png" width="140"/><img alt="Praktijk Anastasia in Hoevelaken" class="header-logo-dark" height="169" src="https://www.praktijkanastasia.nl/wp-content/uploads/2018/04/logo.png" width="140"/></a>
</div>
<!-- Mobile Left Elements -->
<div class="flex-col show-for-medium flex-left">
<ul class="mobile-nav nav nav-left ">
<li class="nav-icon has-icon">
<a aria-controls="main-menu" aria-expanded="false" aria-label="Menu" class="is-small" data-bg="main-menu-overlay" data-color="" data-open="#main-menu" data-pos="center" href="#">
<i class="icon-menu"></i>
</a>
</li> </ul>
</div>
<!-- Left Elements -->
<div class="flex-col hide-for-medium flex-left flex-grow">
<ul class="header-nav header-nav-main nav nav-left nav-line-bottom nav-size-medium nav-uppercase">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-11" id="menu-item-11"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10" id="menu-item-10"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/?page_id=8">Energetische therapie</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47" id="menu-item-47"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/?page_id=33">Consult</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44" id="menu-item-44"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/?page_id=39">Over Anastasia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45" id="menu-item-45"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/?page_id=37">Tarieven</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43" id="menu-item-43"><a class="nav-top-link" href="https://www.praktijkanastasia.nl/?page_id=41">Contact</a></li>
</ul>
</div>
<!-- Right Elements -->
<div class="flex-col hide-for-medium flex-right">
<ul class="header-nav header-nav-main nav nav-right nav-line-bottom nav-size-medium nav-uppercase">
</ul>
</div>
<!-- Mobile Right Elements -->
<div class="flex-col show-for-medium flex-right">
<ul class="mobile-nav nav nav-right ">
<li class="header-search header-search-dropdown has-icon has-dropdown menu-item-has-children">
<a aria-label="Search" class="is-small" href="#"><i class="icon-search"></i></a>
<ul class="nav-dropdown nav-dropdown-default">
<li class="header-search-form search-form html relative has-icon">
<div class="header-search-form-wrapper">
<div class="searchform-wrapper ux-search-box relative is-normal"><form action="https://www.praktijkanastasia.nl/" class="searchform" method="get" role="search">
<div class="flex-row relative">
<div class="flex-col flex-grow">
<input class="search-field mb-0" id="s" name="s" placeholder="Zoeken" type="search" value=""/>
</div>
<div class="flex-col">
<button aria-label="Submit" class="ux-search-submit submit-button secondary button icon mb-0" type="submit">
<i class="icon-search"></i> </button>
</div>
</div>
<div class="live-search-results text-left z-top"></div>
</form>
</div> </div>
</li> </ul>
</li>
</ul>
</div>
</div>
<div class="container"><div class="top-divider full-width"></div></div>
</div>
<div class="header-bg-container fill"><div class="header-bg-image fill"></div><div class="header-bg-color fill"></div></div> </div>
</header>
<main class="" id="main">
<div class="content-area" id="primary">
<main class="site-main container pt" id="main" role="main">
<section class="error-404 not-found mt mb">
<div class="row">
<div class="col medium-3"><span class="header-font" style="font-size: 6em; font-weight: bold; opacity: .3">404</span></div>
<div class="col medium-9">
<header class="page-title">
<h1 class="page-title">Oops! Die pagina kan niet worden gevonden.</h1>
</header>
<div class="page-content">
<p>Het lijkt erop dat er niets is gevonden. Probeer een van onderstaande links of de zoekbalk.</p>
<form action="https://www.praktijkanastasia.nl/" class="searchform" method="get" role="search">
<div class="flex-row relative">
<div class="flex-col flex-grow">
<input class="search-field mb-0" id="s" name="s" placeholder="Zoeken" type="search" value=""/>
</div>
<div class="flex-col">
<button aria-label="Submit" class="ux-search-submit submit-button secondary button icon mb-0" type="submit">
<i class="icon-search"></i> </button>
</div>
</div>
<div class="live-search-results text-left z-top"></div>
</form>
</div>
</div>
</div>
</section>
</main>
</div>
</main>
<footer class="footer-wrapper" id="footer">
<!-- FOOTER 1 -->
<!-- FOOTER 2 -->
<div class="absolute-footer dark medium-text-center small-text-center">
<div class="container clearfix">
<div class="footer-primary pull-left">
<div class="copyright-footer">
        Copyright 2021 © <strong>Praktijk Anastasia</strong> - <a href="/final/privacy-statement">Privacy Statement</a> </div>
</div>
</div>
</div>
<a class="back-to-top button icon invert plain fixed bottom z-1 is-outline hide-for-medium circle" href="#top" id="top-link"><i class="icon-angle-up"></i></a>
</footer>
</div>
<div class="mobile-sidebar no-scrollbar mfp-hide" id="main-menu">
<div class="sidebar-menu no-scrollbar text-center">
<ul class="nav nav-sidebar nav-vertical nav-uppercase nav-anim">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-11"><a href="https://www.praktijkanastasia.nl/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10"><a href="https://www.praktijkanastasia.nl/?page_id=8">Energetische therapie</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="https://www.praktijkanastasia.nl/?page_id=33">Consult</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="https://www.praktijkanastasia.nl/?page_id=39">Over Anastasia</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="https://www.praktijkanastasia.nl/?page_id=37">Tarieven</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43"><a href="https://www.praktijkanastasia.nl/?page_id=41">Contact</a></li>
<li class="header-contact-wrapper">
<ul class="nav nav-divided nav-uppercase header-contact" id="header-contact">
<li class="">
<a class="tooltip" href="https://maps.google.com/?q=Praktijk Anastasia, Sportweg 20, Hoevelaken" rel="noopener noreferrer" target="_blank" title="Praktijk Anastasia, Sportweg 20, Hoevelaken">
<i class="icon-map-pin-fill" style="font-size:15px;"></i> <span>
			     	Ons adres			     </span>
</a>
</li>
<li class="">
<a class="tooltip" href="mailto:info@praktijkanastasia.nl" title="info@praktijkanastasia.nl">
<i class="icon-envelop" style="font-size:15px;"></i> <span>
			       	Contact			       </span>
</a>
</li>
<li class="">
<a class="tooltip" href="tel:06 52 12 61 64" title="06 52 12 61 64">
<i class="icon-phone" style="font-size:15px;"></i> <span>06 52 12 61 64</span>
</a>
</li>
</ul>
</li> </ul>
</div>
</div>
<!--googleoff: all--><div id="cookie-law-info-bar"><span>We gebruiken cookies voor een goed functionerende website. Door gebruik te maken van onze website, geeft u toestemming en gaat u akkoord met het gebruik van de cookies.  <a class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" data-cli_action="accept" id="cookie_action_close_header" role="button" style="display:inline-block;  margin:5px; " tabindex="0">Akkoord</a></span></div><div id="cookie-law-info-again" style="display:none;"><span id="cookie_hdr_showagain">Privacy &amp; Cookies Policy</span></div><div aria-hidden="true" aria-labelledby="cliSettingsPopup" class="cli-modal" id="cliSettingsPopup" role="dialog" tabindex="-1">
<div class="cli-modal-dialog" role="document">
<div class="cli-modal-content cli-bar-popup">
<button class="cli-modal-close" id="cliModalClose" type="button">
<svg class="" viewbox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
<span class="wt-cli-sr-only">Sluiten</span>
</button>
<div class="cli-modal-body">
<div class="cli-container-fluid cli-tab-container">
<div class="cli-row">
<div class="cli-col-12 cli-align-items-stretch cli-px-0">
<div class="cli-privacy-overview">
<h4>Privacy Overview</h4> <div class="cli-privacy-content">
<div class="cli-privacy-content-text">This website uses cookies to improve your experience while you navigate through the website. Out of these cookies, the cookies that are categorized as necessary are stored on your browser as they are essential for the working of basic functionalities of the website. We also use third-party cookies that help us analyze and understand how you use this website. These cookies will be stored in your browser only with your consent. You also have the option to opt-out of these cookies. But opting out of some of these cookies may have an effect on your browsing experience.</div>
</div>
<a class="cli-privacy-readmore" data-readless-text="Minder weergeven" data-readmore-text="Meer weergeven"></a> </div>
</div>
<div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">
<div class="cli-tab-section">
<div class="cli-tab-header">
<a class="cli-nav-link cli-settings-mobile" data-target="necessary" data-toggle="cli-toggle-tab" role="button" tabindex="0">
                                Noodzakelijk                            </a>
<div class="wt-cli-necessary-checkbox">
<input checked="checked" class="cli-user-preference-checkbox" data-id="checkbox-necessary" id="wt-cli-checkbox-necessary" type="checkbox"/>
<label class="form-check-label" for="wt-cli-checkbox-necessary">Noodzakelijk</label>
</div>
<span class="cli-necessary-caption">Altijd ingeschakeld</span> </div>
<div class="cli-tab-content">
<div class="cli-tab-pane cli-fade" data-id="necessary">
<p>Necessary cookies are absolutely essential for the website to function properly. This category only includes cookies that ensures basic functionalities and security features of the website. These cookies do not store any personal information.</p>
</div>
</div>
</div>
<div class="cli-tab-section">
<div class="cli-tab-header">
<a class="cli-nav-link cli-settings-mobile" data-target="non-necessary" data-toggle="cli-toggle-tab" role="button" tabindex="0">
                                Niet-noodzakelijk                            </a>
<div class="cli-switch">
<input checked="checked" class="cli-user-preference-checkbox" data-id="checkbox-non-necessary" id="wt-cli-checkbox-non-necessary" type="checkbox"/>
<label class="cli-slider" data-cli-disable="Uitgeschakeld" data-cli-enable="Ingeschakeld" for="wt-cli-checkbox-non-necessary"><span class="wt-cli-sr-only">Niet-noodzakelijk</span></label>
</div> </div>
<div class="cli-tab-content">
<div class="cli-tab-pane cli-fade" data-id="non-necessary">
<p>Any cookies that may not be particularly necessary for the website to function and is used specifically to collect user personal data via analytics, ads, other embedded contents are termed as non-necessary cookies. It is mandatory to procure user consent prior to running these cookies on your website.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<!--googleon: all--><script id="flatsome-live-search-js" src="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js?ver=3.12.2" type="text/javascript"></script>
<script id="hoverIntent-js" src="https://www.praktijkanastasia.nl/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script id="flatsome-js-js-extra" type="text/javascript">
/* <![CDATA[ */
var flatsomeVars = {"ajaxurl":"https:\/\/www.praktijkanastasia.nl\/wp-admin\/admin-ajax.php","rtl":"","sticky_height":"70","lightbox":{"close_markup":"<button title=\"%title%\" type=\"button\" class=\"mfp-close\"><svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"28\" height=\"28\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x\"><line x1=\"18\" y1=\"6\" x2=\"6\" y2=\"18\"><\/line><line x1=\"6\" y1=\"6\" x2=\"18\" y2=\"18\"><\/line><\/svg><\/button>","close_btn_inside":false},"user":{"can_edit_pages":false},"i18n":{"mainMenu":"Hoofdmenu"},"options":{"cookie_notice_version":"1"}};
/* ]]> */
</script>
<script id="flatsome-js-js" src="https://www.praktijkanastasia.nl/wp-content/themes/flatsome/assets/js/flatsome.js?ver=3.12.2" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.praktijkanastasia.nl/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
