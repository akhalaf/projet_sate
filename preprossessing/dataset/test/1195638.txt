<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>QD Network - Online Stores</title>
<meta content="QD Network Online Stores, phone cases, electronics" name="description"/>
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="styles/main.css" rel="stylesheet"/>
<!-- MailerLite Universal -->
<script>
(function(m,a,i,l,e,r){ m['MailerLiteObject']=e;function f(){
var c={ a:arguments,q:[]};var r=this.push(c);return "number"!=typeof r?r:f.bind(c.q);}
f.q=f.q||[];m[e]=m[e]||f.bind(f.q);m[e].q=m[e].q||f.q;r=a.createElement(i);
var _=a.getElementsByTagName(i)[0];r.async=1;r.src=l+'?v'+(~~(new Date().getTime()/1000000));
_.parentNode.insertBefore(r,_);})(window, document, 'script', 'https://static.mailerlite.com/js/universal.js', 'ml');

var ml_account = ml('accounts', '1634814', 'f3k8e6a7w7', 'load');
</script>
<!-- End MailerLite Universal -->
</head>
<body id="top"><div class="site-wrapper">
<div class="site-wrapper-inner">
<div class="cover-container">
<div class="masthead clearfix">
<div class="inner">
<h3 class="masthead-brand">QD Network LLC</h3>
</div>
</div><br/> <div class="inner cover">
<h1 class="cover-heading">QD Network</h1>
<p class="lead cover-copy"><a href="http://captivecase.com"> Captive Case </a></p>
<p class="lead cover-copy"><a href="http://soundtech.io"> SoundTech.io</a></p>
<p class="lead cover-copy"><a href="http://magnoliacases.com"> Magnolia Cases </a></p>
<br/>
<br/>
<div class="ml-form-embed" data-account="1634814:f3k8e6a7w7" data-form="1452992:f2r8h0">
</div>
</div>
<div class="mastfoot">
<div class="inner">
<p>info@qdnetwork.net</p>
</div>
</div>
<div aria-hidden="true" aria-labelledby="subscribeModalLabel" class="modal fade" id="subscribeModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="subscribeModalLabel">Get Notified on Launch:</h5>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<form>
<div class="form-group">
<label class="form-control-label" for="recipient-name">Enter you e-mail to get notified when we launch</label>
<input class="form-control" id="recipient-name" placeholder="your-name@example.com" type="text"/>
</div>
</form>
</div>
<div class="modal-footer">
<button class="btn btn-default" type="button">Subscribe</button>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="scripts/main.js"></script>
</div></body>
</html>