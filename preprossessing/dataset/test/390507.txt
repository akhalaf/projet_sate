<!DOCTYPE html>
<html lang="zh">
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.dannylodental.com/favv.jpg" rel="icon" sizes="16x16" type="image/png"/>
<meta content="" name="description"/>
<meta content="纽约牙医,纽约牙科,法拉盛牙医,法拉盛牙科,纽约华人牙科,法拉盛华人牙医，牙科治疗,牙齿美白,微整形，瘦脸针，玻尿酸" name="keywords"/>
<meta content="Colin Zhao" name="author"/>
<meta content="telephone=no" name="format-detection"/>
<!--Load bootstrap library and JQuery-->
<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!--<link rel="stylesheet" href="https://www.dannylodental.com/wp-content/themes/twentysixteen/css/style.css?v=4">-->
<!--<link rel="stylesheet" href="https://www.dannylodental.com/wp-content/themes/twentysixteen/css/mobile.css">-->
<link href="https://www.dannylodental.com/css/allm.css?2" rel="stylesheet"/>
</head>
<body style="background: #ffffff url(https://www.dannylodental.com/wp-content/themes/twentysixteen/images/background.png) repeat-x top;">
<header class="container">
<div class="row">
<div class="col-md-8 col-sm-8 col-xs-12 logo">
<img alt="纽约牙医" src="https://www.dannylodental.com/wp-content/themes/twentysixteen/images/logo.png"/>
<h1>纽约法拉盛专业牙科诊所<br/>
<i>罗立玮牙医教授</i></h1>
</div>
<div class="col-md-4 col-sm-4 col-xs-12 phone">
<p class="mobile-only" style="width:250px;border-radius:50px;">
<a href="tel:+1-718-961-0528" style="display:block;padding:2px;color:#000;text-decoration:none;font-size:16px;">718-961-0528<br/><font style="font-size:11px;">点击按钮可直接拨打</font></a>
</p>
<p class="mobile-hide">
					718-961-0528
				</p>
</div>
</div>
</header>
<div id="nav-wrap">
<nav class="container">
<ul class="row">
<li class=" col-md-2 col-sm-3 col-xs-12"><a href="https://www.dannylodental.com/index.php">诊所首页</a></li>
<li class=" col-md-2 col-sm-3 col-xs-12"><a href="https://www.dannylodental.com/about.php">罗医生简介</a></li>
<li class=" col-md-2 col-sm-3 col-xs-12"><a href="https://www.dannylodental.com/services.php">保险与服务</a></li>
<li class=" col-md-2 col-sm-3 col-xs-12"><a href="https://www.dannylodental.com/contact.php">联系我们</a></li>
</ul>
</nav>
</div>
<div class="clear"></div>
<main class="container">
<div class="row">
<section class="col-md-8 col-sm-7 pull-right col-xs-12">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">有点尴尬诶！该页无法显示。</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>这儿似乎什么都没有，试试搜索？</p>
<form action="https://www.dannylodental.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">搜索：</span>
<input class="search-field" name="s" placeholder="搜索…" title="搜索：" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><span class="screen-reader-text">搜索</span></button>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- .site-main -->
</div><!-- .content-area -->
<aside class="sidebar widget-area" id="secondary" role="complementary">
<section class="widget widget_recent_entries" id="recent-posts-2"> <h2 class="widget-title">博客最新文章</h2> <ul>
<li>
<a href="https://www.dannylodental.com/2019/01/13/%e5%9c%a8%e7%ba%bd%e7%ba%a6%e6%80%8e%e4%b9%88%e6%b4%97%e7%89%99%ef%bc%9f%e7%ba%bd%e7%ba%a6%e6%b4%97%e7%89%99%e4%b8%80%e6%ac%a1%e5%a4%9a%e5%b0%91%e9%92%b1%ef%bc%9f">在纽约怎么洗牙？纽约洗牙一次多少钱？</a>
</li>
<li>
<a href="https://www.dannylodental.com/2017/07/23/%e5%be%ae%e6%95%b4%e5%bd%a2%e8%a1%8c%e4%b8%9a%e4%b8%83%e5%a4%a7%e8%b5%b0%e5%90%91">微整形行业七大走向</a>
</li>
<li>
<a href="https://www.dannylodental.com/2017/07/09/%e4%b8%96%e7%95%8c%e6%af%8f%e5%b9%b4%e9%83%bd%e6%9c%89%e5%b9%be%e7%99%be%e8%90%ac%e4%ba%ba%e6%8e%a5%e5%8f%97%e6%95%b4%e5%bd%a2%e7%be%8e%e5%ae%b9%e6%89%8b%e8%a1%93">世界每年都有幾百萬人接受整形美容手術</a>
</li>
<li>
<a href="https://www.dannylodental.com/2017/07/05/%e4%bd%a0%e4%b8%8d%e8%ae%a9%e5%8c%96%e5%a6%86%ef%bc%8c%e6%88%91%e5%b0%b1%e5%be%ae%e6%95%b4%e5%bd%a2">你不让化妆，我就“微整形”</a>
</li>
</ul>
</section>
</aside><!-- .sidebar .widget-area -->
</section>
<div class="col-md-4 col-sm-5 col-xs-12" id="sidebar">
<aside>
<div class="head">
						诊所主诊医生
					</div>
<img alt="纽约牙医" border="0" src="https://www.dannylodental.com/wp-content/themes/twentysixteen/images/doc.jpg"/>
<div class="info">
<h2>法拉盛牙医</h2>
						罗立玮(Danny Lo)<br/>
						NYU牙科副教授<br/>
						口腔医学院主管<br/>
						哥伦比亚大学住院医总医师荣誉<br/>
<img alt="罗立玮NYU牙科副教授证书" src="https://www.dannylodental.com/wp-content/uploads/2016/06/IMG_7101-300x245.jpg"/>
<a class="btn btn-default" href="https://www.dannylodental.com/about.php" style="font-size:80%;margin:2px 0;">更多 ... </a>
<div style="both:clear;"></div>
</div>
</aside>
</div>
<div class="clear"></div>
</div>
</main>
<div class="container">
<footer class="row">
<div class="col-md-8 col-sm-6 pull-right">
<p class="sm-font">本站搜索关键字: 纽约牙医 , 法拉盛牙医 , 纽约牙科 , 法拉盛牙科 , 纽约华人牙科 , 法拉盛华人牙医, 法拉盛牙医诊所, 牙科治疗, 牙齿美白, <a href="https://www.dannylodental.com/法拉盛微整形/">法拉盛微整形</a>， 专业瘦脸针 ，玻尿酸</p>
<p>友情链接：<a href="//www.zhiwuxiumian.com" target="_blank">纽约袪痘</a> <a href="//www.deyutcm.com" target="_blank">纽约中医</a></p>
<p> © 2016 , <a href="https://www.dannylodental.com">纽约牙医</a> 罗立玮牙科诊所网站所由 369 <a href="//www.369usa.com" target="_blank">纽约网站设计</a> 设计与制作并及广告推广</p>
</div>
</footer>
</div>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-83133433-2', 'auto');
	ga('send', 'pageview');

	</script>
</body>
</html>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 