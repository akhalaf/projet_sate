<!DOCTYPE html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title> </title>
<meta content="index, follow" name="robots"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.riom.in/img/favicon.png" rel="icon" type="image/x-icon"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="https://www.riom.in/css/style.css" rel="stylesheet" type="text/css"/>
<link href="https://www.riom.in/css/lightGallery.css" rel="stylesheet"/>
<link href="https://www.riom.in/css/SpryTabbedPanels.css" rel="stylesheet" type="text/css"/>
<script src="https://www.riom.in/js/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="https://www.riom.in/css/reset.css" rel="stylesheet"/> <!-- CSS reset -->
<link href="https://www.riom.in/css/reset.css" rel="stylesheet"/> <!-- CSS reset -->
<link href="https://www.riom.in/css/responstyle.css" rel="stylesheet"/> <!-- Resource style -->
<script src="https://www.riom.in/js/modernizr.js"></script> <!-- Modernizr -->
<script src="https://www.riom.in/js/jquery-2.1.1.js"></script>
<script src="https://www.riom.in/js/jquery.mobile.custom.min.js"></script>
</head>
<body>
<header class="header">
<div class="top-header">
<div class="wraper">
<div class="logo-rit">
<a href="https://www.rawalinstitutions.com/"><img alt="Rawal Institution" src="https://www.riom.in/img/rawal-institution-logo.png"/></a>
</div><!--logo-->
<div class="social-icon" style="display:none;">
<a href="https://www.facebook.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
<a href="https://twitter.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
<a href="https://www.youtube.com/user/Rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-youtube"></i></a>
</div><!--social icon-->
<div class="logo-riom">
<a href="https://www.riom.in/"><img alt="Rawal Institution" src="https://www.riom.in/img/logo.png"/></a>
</div><!--logo-->
<nav>
<ul>
<li><a href="https://www.riom.in/career">Careers</a></li>
<li><a href="https://www.riom.in/feedback">Feedback &amp; Suggestions</a></li>
<li><a class="blinker" href="#modal">Admission Enquiry</a></li>
</ul>
</nav>
<div class="sidebtn-social">
<ul>
<li><a href="https://www.facebook.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
<li><a href="https://twitter.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
<li><a href="https://www.youtube.com/user/Rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-youtube"></i></a></li>
</ul>
</div>
</div><!--wraper-->
</div><!--top header-->
<div class="bottom-header">
<div class="wraper">
<!--<div class="search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</div>search-->
<nav>
<ul>
<li><a href="https://www.riom.in/">Home</a></li>
<li><a href="https://www.riom.in/about">About us</a>
<ul>
<li><a href="https://www.riom.in/chairman-message">Chairman Message</a>
</li>
<li><a href="https://www.riom.in/vice-chairman-message">Vice Chairman Message</a>
</li>
<li><a href="https://www.riom.in/president-message">President Message</a>
</li>
<li><a href="https://www.riom.in/vice-president-message">Vice President Message</a>
</li>
<li><a href="https://www.riom.in/director-message">Director Message</a>
</li>
</ul>
</li>
<li><a href="https://www.riom.in/#">Courses</a>
<ul>
<li><a href="https://www.riom.in/bachelor-of-business-administration">Bachelor of Business Administration</a>
</li>
<li><a href="https://www.riom.in/n-a">Mba (general)</a>
</li>
<li><a href="https://www.riom.in/mba-i">MBA (International)</a>
</li>
<li><a href="https://www.riom.in/bhmct">Bachelor of Hotel Management &amp; Catering Technology(BHMCT)</a>
</li>
<li><a href="https://www.riom.in/bachelor-of-computer-application-bca">Bachelor of Computer Application (BCA)</a>
</li>
</ul>
</li>
<li><a href="https://www.riom.in/#">Placement </a>
<ul>
<li><a href="https://www.riom.in/about-cric">About CRIC</a>
</li>
<li><a href="https://www.riom.in/pla-2016-17">Placement Record 2016-2017 Batch</a>
</li>
<li><a href="https://www.riom.in/sum-2016-17">Summer Internship Record 2016-17 Batch </a>
</li>
<li><a href="https://www.riom.in/visit">Industrial Visit</a>
</li>
<li><a href="https://www.riom.in/past">PasT Recruiters</a>
</li>
<li><a href="https://www.riom.in/con-cric">Contact CRIC</a>
</li>
</ul>
</li>
<li><a href="https://www.riom.in/media">Media</a></li>
<li><a href="https://www.riom.in/gallery">Gallery</a></li>
<li><a href="https://www.riom.in/contact">Contact Us</a></li>
</ul></nav>
</div><!--wraper-->
</div><!--top header-->
</header><!--header-->
<header class="cd-main-header">
<a class="cd-logo" href="https://www.riom.in/"><img alt="Rawal Institution" src="img/logo.png"/></a>
<ul class="cd-header-buttons">
<li><a class="cd-nav-trigger" href="#cd-primary-nav">Menu<span></span></a></li>
</ul> <!-- cd-header-buttons -->
</header>
<main class="cd-main-content">
<!-- your content here -->
</main>
<div class="cd-overlay"></div>
<div class="cd-nav">
<ul class="cd-primary-nav is-fixed" id="cd-primary-nav">
<li><a href="https://www.riom.in/">Home</a></li>
<li class="has-children">
<a href="https://www.riom.in/about">About us</a>
<ul class="cd-secondary-nav is-hidden">
<li class="go-back"><a href="#">About us</a></li>
<li><a href="https://www.riom.in/chairman-message">Chairman Message</a></li>
<li><a href="https://www.riom.in/vice-chairman-message">Vice Chairman Message</a></li>
<li><a href="https://www.riom.in/president-message">President Message</a></li>
<li><a href="https://www.riom.in/vice-president-message">Vice President Message</a></li>
<li><a href="https://www.riom.in/director-message">Director Message</a></li>
</ul>
</li>
<li class="has-children">
<a href="https://www.riom.in/#">Courses</a>
<ul class="cd-secondary-nav is-hidden">
<li class="go-back"><a href="#">Courses</a></li>
<li><a href="https://www.riom.in/bachelor-of-business-administration">Bachelor of Business Administration</a></li>
<li><a href="https://www.riom.in/n-a">Mba (general)</a></li>
<li><a href="https://www.riom.in/mba-i">MBA (International)</a></li>
<li><a href="https://www.riom.in/bhmct">Bachelor of Hotel Management &amp; Catering Technology(BHMCT)</a></li>
<li><a href="https://www.riom.in/bachelor-of-computer-application-bca">Bachelor of Computer Application (BCA)</a></li>
</ul>
</li>
<li class="has-children">
<a href="https://www.riom.in/#">Placement </a>
<ul class="cd-secondary-nav is-hidden">
<li class="go-back"><a href="#">Placement </a></li>
<li><a href="https://www.riom.in/about-cric">About CRIC</a></li>
<li><a href="https://www.riom.in/pla-2016-17">Placement Record 2016-2017 Batch</a></li>
<li><a href="https://www.riom.in/sum-2016-17">Summer Internship Record 2016-17 Batch </a></li>
<li><a href="https://www.riom.in/visit">Industrial Visit</a></li>
<li><a href="https://www.riom.in/past">PasT Recruiters</a></li>
<li><a href="https://www.riom.in/con-cric">Contact CRIC</a></li>
</ul>
</li>
<a href="https://www.riom.in/gallery">Gallery</a>
<a href="https://www.riom.in/media">Media</a>
<a href="https://www.riom.in/career">Careers</a>
<a href="https://www.riom.in/feedback">Feedback &amp; Suggestions</a>
<li><a href="https://www.riom.in/enquiry">Admission Equiry</a></li>
<li><a href="https://www.riom.in/scholarship">Scholarship</a></li>
<li><a href="#">Robotics</a></li>
<li><a href="#">Personality Enhancement</a></li>
<li><a href="#">Campus Tour</a></li>
<li><a href="#">Education Loan</a></li>
<li><a href="#">Quality Control</a></li>
<li><a href="#">Students Club</a></li>
<li><a href="#">Alumni Club</a></li>
<a href="https://www.riom.in/contact">contact</a>
<div class="social">
<li>
<a href="https://www.facebook.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a>
<a href="https://twitter.com/rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a>
<a href="https://www.youtube.com/user/Rawalinstitute" target="_blank"><i aria-hidden="true" class="fa fa-youtube"></i></a>
</li></div>
</ul> <!-- primary-nav -->
</div> <!-- cd-nav -->
<script src="https://www.riom.in/js/main2.js"></script>
<!-- Resource jQuery -->
<style>


.sidebtn{display:none;}

@-webkit-keyframes blinker {
  0% { color:#444a88; }
    100% { color: #fff;  }
}

@-moz-keyframes blinker {
  0% {  color:#444a88; }
    100% {  color: #fff;  }
}

@-o-keyframes blinker {
  0% { color:#444a88; }
    100% {  color: #fff;  }
}
.blink3{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 0.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}


</style>
<div class="inner-banner">
</div>
<div class="inner-content-section">
<div class="sidelist">
<div class="sidelist-menu">
</div><!--sidelist menu-->
<!--<div class="sidebar-highlight">
				    <div class="sidebar-highlight-container">
				        <h3>Find My Program</h3>
				        <p>Choose a program according to your interest or talent.</p>
				        <form>
							<div class="search1">
								<img src="https://www.riom.in/assets/frontend/img/search.png" alt="">
								<input type="text" class="search" placeholder="Keyword Search">
								<h4>or</h4>
								<select class="cs-select cs-skin-border">
									<option value="" disabled selected>Select your Interest</option>
									<option value="email">E-Mail</option>
									<option value="twitter">Twitter</option>
									<option value="linkedin">LinkedIn</option>
								</select>				
							</div>    
				                			
							<button type="submit" class="btn">SUBMIT</button>
				    	</form>        
				    </div>
				</div>-->
</div><!--sidelist-->
<div class="content-descriptin">
</div><!--content-->
</div><!--inner page-->
<a href="#modal"><div class="sidebtn">Admission Enquiry</div></a>
<footer class="footer">
<div class="footer-left">
<div class="footer-link">
<ul>
<li><a href="https://www.riom.in/enquiry">Admission Equiry</a></li>
<li><a href="https://www.riom.in/scholarship">Scholarship</a></li>
<li><a href="#">Robotics</a></li>
</ul>
</div><!--footer link-->
<div class="footer-link">
<ul>
<li><a href="#">Personality Enhancement</a></li>
<li><a href="#">Campus Tour</a></li>
<li><a href="#">Education Loan</a></li>
</ul>
</div><!--footer link-->
<div class="footer-link">
<ul>
<li><a href="#">Quality Control</a></li>
<li><a href="#">Students Club</a></li>
<li><a href="#">Alumni Club</a></li>
</ul>
</div><!--footer link-->
</div><!--footer left-->
<div class="footer-right">
<a class="btn" href="https://www.riom.in/contact">Contact Us</a>
<br clear="all"/>
<div class="addres">
<p>Sohna Road, Near Zakopur, Faridabad</p>
<p>Phone No. : 0129-2400400, 08860609801, 08860609802, 08860609803</p>
<p>Fax No. : 0129-2400404</p>
<p>Email : info@rawalinstitutions.com</p>
</div>
<br/>
<br/>
</div><!--footer right-->
<div id="back-top">
<a href="#top"><span></span></a></div>
</footer><!--footer-->
<div class="copyright">
<p>© 2021 by Rawal Institute of Management</p>
</div><!--copyright-->
<div aria-describedby="modal1Desc" aria-labelledby="modal1Title" class="remodal" data-remodal-id="modal" role="dialog">
<button aria-label="Close" class="remodal-close" data-remodal-action="close"></button>
<div class="popup-form-section">
<br clear="all"/>
<div class="new-form-add-left career-form">
<h1 style="margin-bottom: 25px;">Admission Enquiry</h1>
<form action="https://www.riom.in/process/insert_admission.php" method="post" onsubmit="return validation();">
<p align="center" class="err" id="errors1"></p>
<input id="names1" name="name" onkeypress="return  onlyAlphabets(event)" placeholder="Student's Name" type="text"/>
<input id="fnames1" name="fname" onkeypress="return  onlyAlphabets(event)" placeholder="Father's Name" type="text"/>
<input id="phones1" maxlength="10" name="phone" onkeypress="return isNumberKey(event,this)" placeholder="Phone" type="text"/>
<input id="emails1" name="email" placeholder="Email" type="text"/>
<select id="department" name="state" onchange="getCourse(this.value);">
<option value="Select State">Select State</option>
<option value="1">
Andaman and Nicobar Island (UT)</option>
<option value="2">
Andhra Pradesh</option>
<option value="3">
Arunachal Pradesh</option>
<option value="4">
Assam</option>
<option value="5">
Bihar</option>
<option value="6">
Chandigarh (UT)</option>
<option value="7">
Chhattisgarh</option>
<option value="8">
Dadra and Nagar Haveli (UT)</option>
<option value="9">
Daman and Diu (UT)</option>
<option value="10">
Delhi (NCT)</option>
<option value="11">
Goa</option>
<option value="12">
Gujarat</option>
<option value="13">
Haryana</option>
<option value="14">
Himachal Pradesh</option>
<option value="15">
Jammu and Kashmir</option>
<option value="16">
Jharkhand</option>
<option value="17">
Karnataka</option>
<option value="18">
Kerala</option>
<option value="19">
Lakshadweep (UT)</option>
<option value="20">
Madhya Pradesh</option>
<option value="21">
Maharashtra</option>
<option value="22">
Manipur</option>
<option value="23">
Meghalaya</option>
<option value="24">
Mizoram</option>
<option value="25">
Nagaland</option>
<option value="26">
Odisha</option>
<option value="27">
Puducherry (UT)</option>
<option value="28">
Punjab</option>
<option value="29">
Rajastha</option>
<option value="30">
Sikkim</option>
<option value="31">
Tamil Nadu</option>
<option value="32">
Telangana</option>
<option value="33">
Tripura</option>
<option value="35">
Uttar Pradesh</option>
<option value="34">
Uttarakhand</option>
<option value="36">
West Bengal</option>
</select>
<select id="course" name="district">
<option value="Select District">Select District</option>
</select>
<select id="cor" name="cor">
<option value="Select Cor">Select Course</option>
<option value="Master of Technology (M.Tech)">Master of Technology (M.Tech)</option>
<option value="Bachelor of Technology (B.Tech)">Bachelor of Technology (B.Tech)</option>
<option value="Diploma Engineering">Diploma Engineering</option>
<option value="MBA">MBA</option>
<option value="Bachelor of Business Administration(BBA)">Bachelor of Business Administration(BBA)</option>
<option value="Bachelor of Hotel Management &amp; Catering Technology(BHMCT)">Bachelor of Hotel Management &amp; Catering Technology(BHMCT)</option>
<option value="Bachelor of Education(B.Ed)">Bachelor of Education(B.Ed)</option>
</select>
<select id="branch1" name="branch">
<option value="Select Branch">Select Branch</option>
<option value="M.Tech-(Computer Science and Engineering)">M.Tech-(Computer Science and Engineering)</option>
<option value="M.Tech-(E.C.E -Signal Processing)">M.Tech-(E.C.E -Signal Processing)</option>
<option value="M.Tech-(M.E-Machine Design)">M.Tech-(M.E-Machine Design)</option>
<option value="Computer Science &amp; Engineering (CSE)">Computer Science &amp; Engineering (CSE)</option>
<option value="Electronics &amp; Communication Engineering (ECE)">Electronics &amp; Communication Engineering (ECE)</option>
<option value="Electrical Engineering (EE)"> Electrical Engineering (EE)</option>
<option value="Mechanical Engineering (ME)"> Mechanical Engineering (ME)</option>
<option value="Civil Engineering (CE)"> Civil Engineering (CE)</option>
<option value="Automobile Engineering (AE)">Automobile Engineering (AE)</option>
<option value="Diploma Mechanical Engineering">Diploma Mechanical Engineering</option>
<option value=" Diploma Civil Engineering"> Diploma Civil Engineering</option>
<option value="MBA (General)">MBA (General)</option>
<option value="MBA (International)">MBA (International)</option>
<option value="Bachelor of Business Administration(BBA)">Bachelor of Business Administration(BBA)</option>
<option value="Bachelor of Hotel Management &amp; Catering Technology(BHMCT)">Bachelor of Hotel Management &amp; Catering Technology(BHMCT)</option>
<option value="Bachelor of Education(B.Ed)">Bachelor of Education(B.Ed)</option>
</select>
<button class="btn" name="submit" style="cursor:pointer;" type="submit">SEND</button>
</form>
</div><!--new-form-add-left-->
</div><!--popup-form-section-->
</div><!--remodal-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});
	});

});
</script>
<script>
            
            
            $(document).ready(function () {
    var speed = 3000;    
    var run = setInterval(rotate, speed);
    var slides = $('.slide-test-faci');
    var container = $('#slides-test-faci ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev-faci'; //id of previous button
    var next = 'next-faci'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();                  
    $('#buttons-faci a').click(function (e) {                    
        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }        
        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }          
        return false;        
    });    
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });   
    function resetSlides() {
        container.css({
            'left': -1 * item_width
        });
    }    
});
function rotate() {
    $('#next-faci').click();
}
        </script>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://www.riom.in/js/jquery.mobile.touchsupport.js"></script>
<script src="https://www.riom.in/js/jquery.phoenix.js"></script>
<script src="https://www.riom.in/js/main.js"></script>
<script src="https://www.riom.in/popup/remodal.js"></script>
<link href="https://www.riom.in/popup/remodal.css" rel="stylesheet"/>
<link href="https://www.riom.in/popup/remodal-default-theme.css" rel="stylesheet"/>
<!-- Events -->
<script>

  $(document).on('opening', '.remodal', function () {

    console.log('opening');

  });



  $(document).on('opened', '.remodal', function () {

    console.log('opened');

  });



  $(document).on('closing', '.remodal', function (e) {

    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));

  });



  $(document).on('closed', '.remodal', function (e) {

    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));

  });



  $(document).on('confirmation', '.remodal', function () {

    console.log('confirmation');

  });



  $(document).on('cancellation', '.remodal', function () {

    console.log('cancellation');

  });

</script>
<script>
function getCourse(val) {
	$.ajax({
	type: "POST",
	url: "https://www.riom.in/process/ajaxData.php",
	data:'department='+val,
	success: function(data){
		$("#course").html(data);
	}
	});
}
</script>
<script>        
           function isNumberKey(event){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}    
        
</script>
<script type="text/javascript">
function validation() 
{ 
var names1= document.getElementById("names1").value.trim();
var pattern=  /^[A-Za-z ]{1,50}$/; 
if (!pattern.test(names1))
{ 
document.getElementById('errors1').innerHTML="*Please enter name*";
document.getElementById("names1").focus() ;
return false;
}  
var fnames1= document.getElementById("fnames1").value.trim();
var pattern=  /^[A-Za-z ]{1,50}$/; 
if (!pattern.test(fnames1))
{ 
document.getElementById('errors1').innerHTML="*Please enter father name*";
document.getElementById("fnames1").focus() ;
return false;
} 
var phones1= document.getElementById("phones1").value;
var pattern= /^\d{10}$/;
if (!pattern.test(phones1))
{ 
document.getElementById('errors1').innerHTML="*Please enter 10 digit phone number.*";
document.getElementById("phones1").focus() ;
return false;
}   
if(document.getElementById("emails1").value=="")
{
     document.getElementById('errors1').innerHTML="*Please enter email id.*";
     document.getElementById("emails1").focus() ;
     return false;
}
var emails1= document.getElementById("emails1").value;
   var pattern= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   if (!pattern.test(emails1))
 {
    document.getElementById('errors1').innerHTML="*Please enter valid email id.*";
     document.getElementById("emails1").focus() ;
     return false
 }
if(document.getElementById("department").value=="Select State")
{
document.getElementById('errors1').innerHTML="*Please select state*";
document.getElementById("department").focus() ;
return false;
}
if(document.getElementById("course").value=="Select District")
{
document.getElementById('errors1').innerHTML="*Please select district*";
document.getElementById("course").focus() ;
return false;
}
if(document.getElementById("cor").value=="Select Cor")
{
document.getElementById('errors1').innerHTML="*Please select course*";
document.getElementById("cor").focus() ;
return false;
}
if(document.getElementById("branch1").value=="Select Branch")
{
document.getElementById('errors1').innerHTML="*Please select branch*";
document.getElementById("branch1").focus() ;
return false;
}
document.getElementById("submit").disabled = true;
document.getElementById("submit").value = "Please wait...";
}
</script>
<script>
 function onlyAlphabets(evt)
    {
        var charCode;
        if (window.event)
            charCode = window.event.keyCode;  //for IE
        else
            charCode = evt.which;  //for firefox
        if (charCode == 32) //for &lt;space&gt; symbol
            return true;
        if (charCode > 31 && charCode < 65) //for characters before 'A' in ASCII Table
            return false;
        if (charCode > 90 && charCode < 97) //for characters between 'Z' and 'a' in ASCII Table
            return false;
        if (charCode > 122) //for characters beyond 'z' in ASCII Table
            return false;
        return true;
    }

function isNumberKey(evt, obj) 
        {
 
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
            if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
            return true;
        }

</script>
</body>
<link href="https://www.riom.in/css/responsive.css" rel="stylesheet" type="text/css"/>
<script defer="" src="https://www.riom.in/js/lightGallery.js" type="text/javascript"></script>
<script type="text/javascript">
         $(document).ready(function() {
            $("#light-gallery").lightGallery();
        });
    </script>
</html>