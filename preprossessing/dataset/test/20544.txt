<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>21 Roux Street</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="Luxury accommodation winelands, Franschhoek, South Africa, winelands village" name="description"/>
<meta content="luxury accommodation franschhoek luxury accommodation winelands boutique residence luxury guest house franschhoek winelands accommodation villas boutique hotels cape" name="keywords"/>
<meta content="index, follow" name="robots"/>
<meta content="14 days" name="revisit-after"/>
<link href="styles/21rouxstreet.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="container">
<div id="header">
<img alt="luxury accommodation franschhoek" src="images/header.jpg"/>
</div>
<div id="nav"><div class="nav_item" id="nav_home"><a class="nav_item" href="index.php"></a></div><div class="nav_item" id="nav_rooms"><a class="nav_item" href="rooms.php"></a></div><div class="nav_item" id="nav_bookings"><a class="nav_item" href="http://www.nightsbridge.co.za/bridge/book?bbid=21339" target="_blank"></a></div><div class="nav_item" id="nav_location"><a class="nav_item" href="location.php"></a></div><div class="nav_item" id="nav_tt"><a class="nav_item" href="http://jjtours.sobeit.co.za/" target="_blank"></a></div><div class="nav_item" id="nav_contact"><a class="nav_item" href="contact.php"></a></div></div> <div id="splash_main">
<img alt="luxury accommodation franschhoek" src="images/splash_main.jpg" title="Luxury Accommodation Franschhoek, 21 Roux Street"/>
</div>
<div id="splash_text">
<div alt="Experience the ultimate luxury at 21 Roux Steet" class="heading" src="images/splash_title.gif" style="margin-bottom: 10px;" title="Experience the ultimate luxury at 21 Roux Steet"></div>
<br/>
</div>
			A breathtaking statement in design and style, situated in Franschhoek, the Cape's most beautiful and prestigious winelands village.
			<br/><br/>
			Situated on the edge of the village and surrounded by magnificent mountain views, orchards and vineyards, 21 Roux Street is a haven of peace and tranquility. The three luxury designer suites, The Garden Suite, The Pool Suite and The Pool room, offer the ultimate in style and comfort, complimented by a manicured garden, sparkling swimming pool.
			<br/><br/>
			Whether exploring the picturesque village, browsing and indulging in the antique shops, galleries and boutiques of the main road, enjoying the fine cuisine of many of the award-winning restaurants or exploring one of the beautiful wine estates of our valley, your stay with us at 21 Roux Street will certainly be one of those special memories you take back home with you!
			<br/><br/>
			Franschhoek has the honour of being acknowledged as the food and wine capital of South Africa.
			<br/><br/>
<h2>We look forward to welcoming you to your 'Boutique Residence'.</h2>
</div>
<div id="rule">
<img alt="luxury accommodation franschhoek" src="images/hr.jpg"/>
</div>
<div id="footer">
			copyright 21 roux street 2021 | website by <a href="http://www.gravitymedia.co.za" target="_blank">Gravity Media</a>
</div>
</body>
</html>