<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Trucktronic Do Brasil Industria de Acessórios Limitada</title>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="Ellite Agência Digital" name="author"/>
<link href="images/favicon.ico" rel="shortcut icon"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<style>
.desenvolvimento h1{color:#fff !important;}
.telefone {color:rgba(0,0,0,0.8) !important;}
#dados{color: #fff;background-color: rgba(0,0,0,0.8);}
.slideshow:after {background-color: rgba(0,0,0,0.6) !important;}
.slideshow:after {background: url(pattern.png) repeat top left;}
.slideshow li:nth-child(1) span {background-image: url(images/banner1.jpg)}
.slideshow li:nth-child(2) span {background-image: url(images/banner2.jpg)}
.slideshow li:nth-child(3) span {background-image: url(images/banner3.jpg)}
.slideshow li:nth-child(4) span {background-image: url(images/banner4.jpg)}
.slideshow li:nth-child(5) span {background-image: url(images/banner5.jpg)}
.slideshow li:nth-child(6) span {background-image: url(images/banner6.jpg)}
a{color:#fff}
.box{color:#fff}
</style>
<body>
<ul class="slideshow">
<li><span>Image 01</span></li>
<li><span>Image 02</span></li>
<li><span>Image 03</span></li>
<li><span>Image 01</span></li>
<li><span>Image 02</span></li>
<li><span>Image 03</span></li>
</ul>
<div class="container">
<!-- logo -->
<div id="logo">
<img src="images/logo.png" style="max-width:500px"/>
</div>
<!-- mensagem -->
<div class="box">
<div class="desenvolvimento">
<h1>Acessórios Automotivos</h1>
</div>
						- CAPOTAS - AÇO COM ABS<br/>
						- TAMPÕES - TAMPA MARÍTIMA RIGIDA<br/>
						- BOX RETRATIL<br/>
						- ESTRIBO INTELIGENTE<br/><br/>
						
						MODELOS PARA TODAS AS PICAPES<br/>
						Solicite Maiores Informações
					</div> </div>
<footer>
<div id="dados">
<!-- telefone -->
<i class="fa fa-phone"></i>   54 98406.5960    |   				<!-- whatsaap-->
<i class="fa fa-whatsapp"></i>   54 98406.5960    |   				<!-- email -->
<i class="fa fa-envelope-o"></i>   txr@txr.ind.br    |   				<!-- facebook -->
<a href="https://www.facebook.com/TXR.Trucktronic/" target="_blank"><i class="fa fa-facebook"></i>   TXR.Trucktronic    |   				<!-- powered by -->
</a><a href="http://www.ellitedigital.com.br" target="_blank"><img class="logo-ellite" src="images/logo-ellite.png" width="80"/></a>
</div>
</footer>
</body>
</html>
