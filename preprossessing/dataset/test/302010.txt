<!DOCTYPE html>
<html lang="nl">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.centaurrecherche.nl/xmlrpc.php" rel="pingback"/>
<title>Pagina niet gevonden – Recherchebureau Centaur</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.centaurrecherche.nl/feed/" rel="alternate" title="Recherchebureau Centaur » Feed" type="application/rss+xml"/>
<link href="https://www.centaurrecherche.nl/comments/feed/" rel="alternate" title="Recherchebureau Centaur » Reactiesfeed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.centaurrecherche.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.centaurrecherche.nl/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.centaurrecherche.nl/wp-content/themes/anissa/style.css?ver=5.0.11" id="anissa-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display%3A400%2C700%7CMontserrat%3A400%2C700%7CMerriweather%3A400%2C300%2C700&amp;subset=latin%2Clatin-ext" id="anissa-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.centaurrecherche.nl/wp-content/themes/anissa/fonts/font-awesome.css?ver=4.3.0" id="anissa-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.centaurrecherche.nl/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.centaurrecherche.nl/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.centaurrecherche.nl/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.centaurrecherche.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.centaurrecherche.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
<style id="custom-header-image" type="text/css">
			.site-branding:before {
				background-image: url( https://www.centaurrecherche.nl/wp-content/uploads/2017/08/cropped-kompas-2.jpg);
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				content: "";
				display: block;
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				z-index:-1;
			}
		</style>
<style type="text/css">
			.site-title a,
		.site-description {
			color: #000000;
		}
		</style>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #e5df90; }
</style>
<link href="https://www.centaurrecherche.nl/wp-content/uploads/2015/06/cropped-logo-e1439556842305-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.centaurrecherche.nl/wp-content/uploads/2015/06/cropped-logo-e1439556842305-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.centaurrecherche.nl/wp-content/uploads/2015/06/cropped-logo-e1439556842305-180x180.png" rel="apple-touch-icon-precomposed"/>
<meta content="https://www.centaurrecherche.nl/wp-content/uploads/2015/06/cropped-logo-e1439556842305-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="error404 custom-background">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead" role="banner">
<nav class="main-navigation clear" id="site-navigation" role="navigation">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">Menu</button>
<div class="menu-centaur-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-197" id="menu-item-197"><a href="https://www.centaurrecherche.nl/">Vermist</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-783" id="menu-item-783"><a href="https://www.centaurrecherche.nl/opsporing-vermisten/">Opsporing vermisten</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-742" id="menu-item-742"><a href="https://www.centaurrecherche.nl/arne-hermsen/">Arne Hermsen</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-745" id="menu-item-745"><a href="https://www.centaurrecherche.nl/hans-en-piet/">Hans en Piet</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-743" id="menu-item-743"><a href="https://www.centaurrecherche.nl/herman-ploegstra/">Herman Ploegstra</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-740" id="menu-item-740"><a href="https://www.centaurrecherche.nl/milorad-mitrovic/">Milorad Mitrovic</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-744" id="menu-item-744"><a href="https://www.centaurrecherche.nl/rebecca-groenendijk/">Rebecca Groenendijk</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-750" id="menu-item-750"><a href="https://www.centaurrecherche.nl/ad-langenhuizen/">Ad Langenhuizen</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-782" id="menu-item-782"><a href="https://www.centaurrecherche.nl/oplichting-en-cybercrime/">Oplichting en cybercrime</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-910" id="menu-item-910"><a href="https://www.centaurrecherche.nl/affaire-en-alimentatie/">Affaire en alimentatie</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-829" id="menu-item-829"><a href="https://www.centaurrecherche.nl/in-de-media/">In de media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-698" id="menu-item-698"><a href="https://www.centaurrecherche.nl/contact/">Contact</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-866" id="menu-item-866"><a href="https://www.centaurrecherche.nl/juridisch-advies-en-ondersteuning/">Juridisch advies en ondersteuning</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-776" id="menu-item-776"><a href="https://www.centaurrecherche.nl/algemene-voorwaarden/">Algemene voorwaarden</a></li>
</ul>
</li>
</ul></div>
</nav><!-- #site-navigation -->
<div class="site-branding"> <h1 class="site-title"><a href="https://www.centaurrecherche.nl/" rel="home">Recherchebureau Centaur</a></h1>
<h2 class="site-description">opsporing &amp; juridisch advies</h2>
</div><!-- .site-branding -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="wrap clear">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">
          Oops! That page can’t be found.        </h1>
</header>
<!-- .page-header -->
<div class="page-content">
<p>
          It looks like nothing was found at this location. Maybe try a search?        </p>
<form action="https://www.centaurrecherche.nl/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Zoeken naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Zoeken"/>
</form> </div>
<!-- .page-content -->
</section>
<!-- .error-404 -->
</main>
<!-- #main -->
</div>
<!-- #primary -->
<div class="widget-area sidebar" id="secondary" role="complementary">
<aside class="widget widget_search" id="search-2"><form action="https://www.centaurrecherche.nl/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Zoeken naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Zoeken"/>
</form></aside></div>
<!-- #secondary --></div>
<!-- #content -->
</div>
<!-- .wrap  -->
<footer class="site-footer wrap" id="colophon" role="contentinfo">
<div class="site-info"> <a href="http://wordpress.org/">Proudly powered by WordPress</a> <span class="sep"> | </span> Theme: Anissa by <a href="https://alienwp.com/" rel="designer">AlienWP</a>. </div>
<!-- .site-info -->
</footer>
<!-- #colophon -->
</div>
<!-- #page -->
<script src="https://www.centaurrecherche.nl/wp-content/themes/anissa/js/navigation.js?ver=20120206" type="text/javascript"></script>
<script src="https://www.centaurrecherche.nl/wp-content/themes/anissa/js/skip-link-focus-fix.js?ver=20130115" type="text/javascript"></script>
<script src="https://www.centaurrecherche.nl/wp-content/themes/anissa/js/owl.carousel.js?ver=20120206" type="text/javascript"></script>
<script src="https://www.centaurrecherche.nl/wp-content/themes/anissa/js/effects.js?ver=20120206" type="text/javascript"></script>
<script src="https://www.centaurrecherche.nl/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
</body></html>