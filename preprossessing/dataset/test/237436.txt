<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="de" http-equiv="language"/>
<meta content="no" http-equiv="imagetoolbar"/>
<title>|| Blindtext-Generator | Lorem ipsum für Webdesigner ||</title>
<meta content="de" name="language"/>
<meta content="blindtext,lorem ipsum,platzhaltertext,fülltext,online-tool,generator,css-formatierung,webdesign" name="keywords"/>
<meta content="Online-Blindtextgenerator zur komfortablen Erstellung von Blindtexten und CSS-Formatierungen für Layoutaufgaben." name="description"/>
<meta content="index, follow" name="robots"/>
<link href="/res/css/btg_layout_2.css?d" media="screen" rel="stylesheet" type="text/css"/>
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" media="screen" href="/res/css/btg_fixie6.css?d" />
<![endif]-->
<script type="text/javascript">
var tt_theLanguage = 'de';
</script>
<script src="/res/js/jquery-1.4.1.min.js?d" type="text/javascript"></script>
<script src="/res/js/clipboard.min.js?d" type="text/javascript"></script>
<script src="/res/js/replacement.js?d" type="text/javascript"></script>
<script src="/res/js/tt_basics.js?d" type="text/javascript"></script>
<script src="/res/js/lorem-ipsum-generator-noflash.js?d" type="text/javascript"></script>
<script src="/res/js/jqdnr.js?d" type="text/javascript"></script>
</head>
<body onunload="generator6_unload();">
<div id="page">
<!-- header -->
<div id="header">
<div class="ie_headline"></div>
<a accesskey="H" class="img_logo" href="http://www.blindtextgenerator.de" title="Home [H]"><img alt="Blindtext-Generator Logo" src="/res/img/logo_blindtextgenerator.png"/></a>
<div class="multi_language">
<h2>Mehrsprachig - Übersetzung</h2>
<ul>
<li class="none"><a href="http://www.blindtextgenerator.com/ru">Russisch</a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/it">Italienisch</a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/es">Spanisch</a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/fr">Französisch</a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/lorem-ipsum">Englisch</a></li>
<li class="none"><a href="http://www.blindtextgenerator.de">Deutsch</a></li>
</ul>
</div>
<ul id="nav">
<li><a class="current" href="http://www.blindtextgenerator.de">Generator</a></li>
<li><a href="http://www.blindtextgenerator.de/snippets">HTML Snippets</a></li>
<li><a href="http://www.blindtextgenerator.de/ueber-blindtext">Über Blindtext</a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/ru"><img alt="Russisch" src="/res/img/flags/ru.png" title="Sprache auswählen: Russisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/cz"><img alt="Tschechisch" src="/res/img/flags/cz.png" title="Sprache auswählen: Tschechisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/it"><img alt="Italienisch" src="/res/img/flags/it.png" title="Sprache auswählen: Italienisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/es"><img alt="Spanisch" src="/res/img/flags/es.png" title="Sprache auswählen: Spanisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/fr"><img alt="Französisch" src="/res/img/flags/fr.png" title="Sprache auswählen: Französisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.com/lorem-ipsum"><img alt="Englisch" src="/res/img/flags/gb.png" title="Sprache auswählen: Englisch"/></a></li>
<li class="none"><a href="http://www.blindtextgenerator.de"><img alt="Deutsch" src="/res/img/flags/de.png" title="Sprache auswählen: Deutsch"/></a></li>
</ul>
</div><!-- eof header -->
<!-- content container -->
<div class="col_ri" id="wrapper">
<!-- sidebar left -->
<div class="sidebar sb_le clearfix"></div><!-- eof sidebar left -->
<!-- sidebar right -->
<div class="sidebar sb_ri clearfix">
<div class="block">
<h1 class="title">Willkommen</h1>
<h3>Willkommen beim Blindtextgenerator!</h3> <p>Hier könnt Ihr Euch komfortabel Blindtexte für Layoutaufgaben erzeugen.</p> <p>Wir werden Ihn nach und nach um Funktionalitäten erweitern und sind auf Euer Feedback gespannt.</p> <p>Ihr seid herzlich eingeladen, uns weitere Blindtexte zu schicken.</p> </div>
<div class="block">
<h1 class="title">Eigenwerbung</h1>
<!--<p style="text-align:center;">
                <a target="_blank" href="https://anost.net/Products/Barbara-Morgenstern-Beide/"><img src="/res/img/banner/banner-barbara-morgenstern-beide-ep.png" alt="Barbara Morgenstern - EP Beide" /></a>
            </p>-->
<p style="text-align:center;">
<a href="http://dummy-image-generator.com"><img alt="DIG" src="/res/img/banner/banner-tools-1.png"/></a>
</p>
</div>
<div class="block">
<h1 class="title">Credits</h1>
<p>Vielen Dank für Blindtexte an:<br/> <a href="http://textformer.de/">Nicolai von textformer</a><br/> <a href="http://richytype.de/">Eric von Richytype</a><br/> <a href="http://www.peewee.de/Blindtext.html">und Peewee</a></p> </div>
</div><!-- eof sidebar right -->
<!-- main ********************************************************************************************************* -->
<div class="clearfix" id="main">
<div class="modul sub_half_left">
<a href="#" id="idResetSettings" title="Einstellungen zurücksetzen"><img alt="Einstellungen zurücksetzen" class="icon_headline" src="/res/img/icon/icon_reset.png"/></a>
<h1 class="headline replacement">Einstellungen</h1>
<!-- standard settings -->
<form action="foo">
<fieldset>
<!-- list available blindtexts-->
<label class="left">Blindtext</label>
<fieldset id="idTextname">
<input checked="checked" class="js-textname iradio" id="idRadioTextname-ANY-lorem" name="radioTextname" type="radio" value="1"/><label for="idRadioTextname-ANY-lorem">Lorem ipsum
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-lorem" name="radioTextname" type="radio" value="2"/><label for="idRadioTextname-DE-lorem">Lorem ipsum auf Deutsch
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-ANY-li-europan" name="radioTextname" type="radio" value="3"/><label for="idRadioTextname-ANY-li-europan">Li Europan lingues
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-far-away-behind" name="radioTextname" type="radio" value="4"/><label for="idRadioTextname-DE-far-away-behind">Hinter den Wortbergen
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-werther" name="radioTextname" type="radio" value="5"/><label for="idRadioTextname-DE-werther">Werther
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-kafka" name="radioTextname" type="radio" value="6"/><label for="idRadioTextname-DE-kafka">Kafka
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-trapattoni" name="radioTextname" type="radio" value="7"/><label for="idRadioTextname-DE-trapattoni">Trapattoni '98
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-er-hoerte-leise" name="radioTextname" type="radio" value="8"/><label for="idRadioTextname-DE-er-hoerte-leise">Er hörte leise
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-typoblindtext" name="radioTextname" type="radio" value="9"/><label for="idRadioTextname-DE-typoblindtext">Typoblindtext
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-webstandards" name="radioTextname" type="radio" value="10"/><label for="idRadioTextname-DE-webstandards">Webstandards
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-pangram" name="radioTextname" type="radio" value="11"/><label for="idRadioTextname-DE-pangram">Pangramm
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
<input class="js-textname iradio" id="idRadioTextname-DE-a-z-1" name="radioTextname" type="radio" value="12"/><label for="idRadioTextname-DE-a-z-1">a-z A-Z 123 öüä
</label> <!--<a href="#"><strong>»</strong></a>--><br/>
</fieldset>
<fieldset>
<!-- number of words or chars-->
<label class="left">Anzahl</label>
<input class="itext_short mb_10" id="idInputWords" style="float:left;" type="text" value="200"/>
<div class="irocker"><a class="up" href="#" id="idWordsUp">mehr</a><a class="down" href="#" id="idWordsDown">weniger</a></div>
<input checked="checked" class="iradio" id="idRadioWords" name="radioWC" type="radio" value="1"/>
<label for="idRadioWords">Wörter</label>
<input class="iradio ml_5" id="idRadioChars" name="radioWC" type="radio" value="2"/>
<label for="idRadioChars">Zeichen</label><br/>
<!-- number of paragraphs-->
<input class="itext_shorter right" id="idInputParas" type="text" value="1"/>
<div class="irocker"><a class="up" href="#" id="idParasUp">mehr</a><a class="down" href="#" id="idParasDown">weniger</a></div>
<label for="idInputParas">Absätze</label><br/>
</fieldset>
<!-- output <p>-tags -->
<label class="left">Ausgabe</label>
<input class="icheck" id="idCheckPTags" type="checkbox"/>
<label for="idCheckPTags">&lt;p&gt; tags ausgeben</label>
</fieldset>
</form>
<!-- advanced settings -->
<p><a class="mt_10" href="#" id="go-advanced">» ERWEITERTE EINSTELLUNGEN</a></p>
<div id="advanced_toggle">
<form action="foo" class="twocol">
<fieldset>
<!--<p></p>-->
<!-- 1 -->
<label for="idFontFamily">Font-Family</label>
<select class="iselect mb_10" id="idFontFamily" name="Namen" size="1">
<optgroup label="Verdana">
<option label="Verdana, Geneva, sans-serif" selected="selected">Verdana, Geneva, sans-serif</option>
</optgroup>
<optgroup label="Arial">
<option label="Arial, Helvetica, sans-serif">Arial, Helvetica, sans-serif</option>
</optgroup>
<optgroup label="Courier">
<option label="Courier, Courier New, monospace">Courier, "Courier New", monospace</option>
</optgroup>
<optgroup label="Courier New">
<option label="Courier New, Courier, monospace">"Courier New", Courier, monospace</option>
</optgroup>
<optgroup label="Geneva">
<option label="Geneva, Verdana, sans-serif">Geneva, Verdana, sans-serif</option>
</optgroup>
<optgroup label="Georgia">
<option label="Georgia, Times New Roman, Times, serif">"Georgia", "Times New Roman", Times, serif</option>
</optgroup>
<optgroup label="Helvetica">
<option label="Helvetica, Arial, sans-serif">Helvetica, Arial, sans-serif</option>
</optgroup>
<optgroup label="Lucidia">
<option label="Lucida Grande, Lucida Sans Unicode, Arial, sans-serif">"Lucida Grande", "Lucida Sans Unicode", Arial, sans-serif</option>
</optgroup>
<optgroup label="Tahoma">
<option label="Tahoma, Verdana, sans-serif">Tahoma, Verdana, sans-serif</option>
</optgroup>
<optgroup label="Times New Roman">
<option label="Times New Roman, Times, serif">"Times New Roman", Times, serif</option>
</optgroup>
</select>
<label for="idFontStyle">Font-Style</label>
<select class="iselect mb_10" id="idFontStyle" name="Namen" size="1">
<option label="normal" selected="selected">normal</option>
<option label="italic">italic</option>
<option label="oblique">oblique</option>
</select><br/>
<!-- 2 -->
<label for="idFontWeight">Font-Weight</label>
<select class="iselect mb_10" id="idFontWeight" name="Namen" size="1">
<option label="normal" selected="selected">normal</option>
<option label="bold">bold</option>
<option label="bolder">bolder</option>
<option label="lighter">lighter</option>
</select>
<label for="idFontSize">Font-Size</label>
<select class="iselect mb_10" id="idFontSize" name="Namen" size="1">
<option label="8px">8px</option>
<option label="9px">9px</option>
<option label="10px" selected="selected">10px</option>
<option label="11px">11px</option>
<option label="12px">12px</option>
<option label="14px">14px</option>
<option label="16px">16px</option>
<option label="18px">18px</option>
<option label="20px">20px</option>
<option label="22px">22px</option>
<option label="24px">24px</option>
<option label="30px">30px</option>
<option label="40px">40px</option>
<option label="50px">50px</option>
<option label="60px">60px</option>
</select><br/>
<!-- 3 -->
<label for="idLetterSpacing">Letter-Spacing</label>
<select class="iselect mb_10" id="idLetterSpacing" name="Namen" size="1">
<option label="normal" selected="selected">normal</option>
<option label="1px">1px</option>
<option label="2px">2px</option>
<option label="3px">3px</option>
<option label="4px">4px</option>
<option label="5px">5px</option>
<option label="6px">6px</option>
<option label="7px">7px</option>
<option label="8px">8px</option>
<option label="9px">9px</option>
<option label="10px">10px</option>
</select>
<label for="idLineHeight">Line-Height</label>
<select class="iselect mb_10" id="idLineHeight" name="Namen" size="1">
<option label="normal" selected="selected">normal</option>
<option label="10px">10px</option>
<option label="11px">11px</option>
<option label="12px">12px</option>
<option label="13px">13px</option>
<option label="14px">14px</option>
<option label="15px">15px</option>
<option label="16px">16px</option>
<option label="17px">17px</option>
<option label="18px">18px</option>
<option label="19px">19px</option>
<option label="20px">20px</option>
<option label="22px">22px</option>
<option label="24px">24px</option>
<option label="26px">26px</option>
<option label="40px">44px</option>
<option label="50px">54px</option>
<option label="60px">64px</option>
</select><br/>
<!-- 4 -->
<label for="idTextTransform">Text-Transform</label>
<select class="iselect mb_10" id="idTextTransform" name="Namen" size="1">
<option label="none" selected="selected">none</option>
<option label="capitalize">capitalize</option>
<option label="uppercase">uppercase</option>
<option label="lowercase">lowercase</option>
</select>
<label for="idTextDecoration">Text-Decoration</label>
<select class="iselect mb_10" id="idTextDecoration" name="Namen" size="1">
<option label="none" selected="selected">none</option>
<option label="underline">underline</option>
<option label="overline">overline</option>
<option label="line-through">line-through</option>
</select><br/>
<!-- 5 -->
<label for="idTextAlign">Text-align</label>
<select class="iselect mb_10" id="idTextAlign" name="Namen" size="1">
<option label="left" selected="selected">left</option>
<option label="right">right</option>
<option label="center">center</option>
<option label="justify">justify</option>
</select><br/>
</fieldset>
</form>
<!--<a href="#" title="Kopieren"><img style="float:right;margin:5px 5px 5px 5px;" src="/res/img/icon/icon_copy_white.png" alt="Kopieren" /></a>-->
<div class="faux_css"><code><strong>p</strong>{<span id="idFauxCSS"><br/>font:normal 10px Verdana,Geneva,Arial,Helvetica,sans-serif;<br/></span>}<br/><br/></code></div>
</div><!-- eof toggle -->
</div>
<div class="modul sub_half_right">
<div class="icon_clipboard mr_10" id="idCopyToClipboardOuter">
<div id="idCopyToClipboardContainer">
<img alt="Alles markieren" height="18px" id="idCopyToClipboard" src="/res/img/icon/icon_clipboard.png" width="145px"/>
</div>
</div>
<a href="#" id="idSelectAllText" title="Alles markieren">
<img alt="Alles markieren" class="icon_headline mr_10" src="/res/img/icon/icon_copy.png"/>
</a>
<h1 class="headline ml_10 replacement">Blindtext</h1>
<div class="faux">
<div id="idClipboardOk">
<h1>Der Text wurde in die Zwischenablage kopiert</h1>
</div>
<div class="jqDnR" id="idTextPanel"><h1>Lorem ipsum</h1> dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</div>
<p class="textstore" id="idTextStore">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.
Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi.
Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis.
Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna.
In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem.
Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh.
Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus.
Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit.
Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus.
Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti.
Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.
Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio.
Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius.</p>
<div class="jqHandle jqResize" id="hands-on"></div>
</div>
<img alt="Ruler" class="ruler" src="/res/img/ruler.png"/>
</div>
<div id="ie_clearing"> </div>
</div><!-- eof main ******************************************************************************************* -->
</div><!-- eof content container -->
</div><!-- eof page -->
<div id="footer">
<div id="footer_inner">
<div class="anchor_top" style="padding-right:0;"><a href="#page" title="Top">[î] Top</a></div>
<!-- AddThis Bookmark Button BEGIN -->
<div id="addthis">
<span style="vertical-align:middle;font-size:10px;padding-right:20px;">Share: 
    <a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.blindtextgenerator.com&amp;t=Blindtextgenerator" rel="nofollow">
<img alt="Facebook" src="/res/img/social/fb.png" style="vertical-align:middle;"/>
</a>
<a href="http://twitter.com/home?status=http%3A%2F%2Fwww.blindtextgenerator.com" rel="nofollow" target="_blank">
<img alt="Twitter" src="/res/img/social/twitter.png" style="vertical-align:middle;"/>
</a>
<a href="https://plusone.google.com/_/+1/confirm?hl=en&amp;url=http%3A%2F%2Fwww.blindtextgenerator.com&amp;title=Blindtextgenerator" rel="nofollow" target="_blank">
<img alt="Google" src="/res/img/social/google.png" style="vertical-align:middle;"/>
</a>
<a href="http://www.diigo.com/post?url=http%3A%2F%2Fwww.blindtextgenerator.com&amp;title=Blindtextgenerator&amp;desc=A+handy+Lorem+Ipsum+Generator+that+helps+to+create+dummy+text+for+all+layout+needs" rel="nofollow" target="_blank">
<img alt="Diigo" src="/res/img/social/diigo.png" style="vertical-align:middle;"/>
</a>
<a href="http://delicious.com/post?url=http%3A%2F%2Fwww.blindtextgenerator.com&amp;title=Blindtextgenerator" rel="nofollow" target="_blank">
<img alt="Delicious" src="/res/img/social/delicious.png" style="vertical-align:middle;"/>
</a>
<!--     <script type="text/javascript">
        addthis_url    = location.href;   
        addthis_title  = document.title;  
        addthis_pub    = 'blind';     
      </script>
      <script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>-->
</span></div>
<!-- AddThis Bookmark Button END -->
<a class="icon" href="http://www.blindtextgenerator.de">Home</a> |
    <a class="icon" href="http://www.blindtextgenerator.de/impressum">Impressum</a> |
    <a class="icon" href="http://www.blindtextgenerator.de">v1.8(noflash)</a> |
    
    <a class="icon" href="http://www.blindtextgenerator.de/feedback">Anregungen &amp; Feedback</a> |
    
    <a href="http://www.diesachbearbeiter.de/web" title="dieSachbearbeiter"><img alt="Link dieSachbearbeiter" src="/res/img/diesachbearbeiter.png" style="vertical-align:middle;margin-left:-3px;"/></a>
<!-- Start of StatCounter Code -->
<!-- Start of StatCounter Code -->
<script type="text/javascript">
var sc_client_storage="disabled";
var sc_project=3315931; 
var sc_invisible=1; 
var sc_security="ec33b2bf"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a href="http://statcounter.com/" target="_blank" title="Web Analytics"><img alt="Web Analytics" class="statcounter" src="//c.statcounter.com/3315931/0/ec33b2bf/1/"/></a></div></noscript>
<!-- End of StatCounter Code -->
</div>
</div>
</body>
</html>
