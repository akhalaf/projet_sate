<!DOCTYPE html>
<html lang="sr-RS">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>Страница није пронађена – Tanja Marković</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://tm-lawoffice.com/feed/" rel="alternate" title="Tanja Marković » довод" type="application/rss+xml"/>
<link href="https://tm-lawoffice.com/comments/feed/" rel="alternate" title="Tanja Marković » довод коментара" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/tm-lawoffice.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://tm-lawoffice.com/wp-content/plugins/teammates/public/css/style-single.css?ver=1.0.0" id="teammates_style_single_member-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/css/bootstrap.min.css?ver=v3.3.6" id="boostrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/style.css?ver=4.9.16" id="lawyeriax-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="lawyeriax-style-inline-css" type="text/css">

                .lawyeriax-lite-big-title{
	                    background-image: url(http://tm-lawoffice.com/wp-content/uploads/2017/11/cropped-tanja-2.jpg);
	                    background-size:cover;
	                    background-repeat: no-repeat;
	                    background-position: center center;
	            }
</style>
<link href="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/css/font-awesome.min.css?ver=v4.5.0" id="font-awesome-css" media="" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display%3A400%2C700%7COpen+Sans%3A400%2C300%2C600%2C700&amp;subset=latin%2Clatin-ext" id="lawyeriax-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://tm-lawoffice.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://tm-lawoffice.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var pf = {"spam":{"label":"I'm human!","value":"0cc26ebaee"}};
/* ]]> */
</script>
<script src="https://tm-lawoffice.com/wp-content/plugins/pirate-forms/public/js/custom-spam.js?ver=4.9.16" type="text/javascript"></script>
<link href="https://tm-lawoffice.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://tm-lawoffice.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://tm-lawoffice.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<style type="text/css">
</style>
</head>
<body class="error404 hfeed">
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead" role="banner">
<div class="navbar navbar-main sticky-navigation navbar-fixed-top">
<div class="top-bar" id="top-bar">
<div class="container">
<div class="top-bar-left top-bar-social">
</div>
<div class="top-bar-right top-bar-jezik">
<div class="widget widget_polylang" id="polylang-4"><label class="screen-reader-text" for="lang_choice_polylang-4">Choose a language</label><select id="lang_choice_polylang-4" name="lang_choice_polylang-4">
<option selected="selected" value="sr">Српски језик</option>
<option value="en">English</option>
<option value="it">Italiano</option>
</select>
<script type="text/javascript">
					//<![CDATA[
					var urls_polylang4 = {"sr":"https:\/\/tm-lawoffice.com\/","en":"https:\/\/tm-lawoffice.com\/en\/homepage\/","it":"https:\/\/tm-lawoffice.com\/it\/start\/"};
					document.getElementById( "lang_choice_polylang-4" ).onchange = function() {
						location.href = urls_polylang4[this.value];
					}
					//]]>
				</script></div>
</div>
<div class="top-bar-right top-bar-contact">
<p>
<a class="lawyeriax-contact-phone" href="tel:+381-60-532-1440">
<span>+381-60-532-1440</span>
</a>
</p>
<p>
<a class="lawyeriax-contact-email" href="mailto:tanja@tm-lawoffice.com">
<span>tanja@tm-lawoffice.com</span>
</a>
</p>
</div>
</div> <!-- container -->
</div>
<div class="container container-header">
<div class="header-inner">
<div class="header-inner-site-branding">
<div class="site-branding-wrap">
<div class="site-branding">
<h1 class="site-title"><a href="https://tm-lawoffice.com/" rel="home">Tanja Marković</a></h1>
<p class="site-description">LAW OFFICE</p>
</div><!-- .site-branding -->
</div><!-- .site-branding-wrap -->
<div class="menu-toggle-button-wrap">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<i class="fa fa-bars"></i>
<span>Primary Menu</span>
</button>
</div>
</div>
<div class="main-navigation-wrap">
<nav class="main-navigation" id="site-navigation" role="navigation">
<div class="menu-glavni-meni-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-20" id="menu-item-20"><a href="https://tm-lawoffice.com/">Naslovna</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://tm-lawoffice.com/delatnost/">Delatnost</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://tm-lawoffice.com/cilj/">Cilj</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51" id="menu-item-51"><a href="https://tm-lawoffice.com/ljudi/">Ljudi</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50" id="menu-item-50"><a href="https://tm-lawoffice.com/pitanja/">Pitanja</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33"><a href="https://tm-lawoffice.com/en/kontakt/">Kontakt</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-333" id="menu-item-333"><a href="https://tm-lawoffice.com/blog/">Blog</a></li>
</ul></div> </nav><!-- #site-navigation -->
</div>
</div><!-- .header-inner -->
</div><!-- .container -->
</div>
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="container">
<div class="col-sm-12 content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://tm-lawoffice.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Претрага за:</span>
<input class="search-field" name="s" placeholder="Претрага …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Претражи"/>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .container -->
</div><!-- #content -->
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="container">
<div class="col-sm-4">
<li class="widget widget_sow-editor" id="sow-editor-3"><div class="so-widget-sow-editor so-widget-sow-editor-base"><h2 class="widget-title">Citati</h2>
<div class="siteorigin-widget-tinymce textwidget">
<p><strong>Iure naturae aequm est neminem cum alterius detrimento et iniuria fieri locupletiorem</strong> (Pomponius D. 51, 17, 206) – Opšti je princip pravičnosti da se niko ne obogati čineći drugome štetu i nepravdu.<br/>
<strong>Adiatur et altera pars</strong>. - Neka se sasluša i druga (protivnička) strana. Osnov pravičnosti u svakom postupku je da se saslušaju svi koji imaju neki interes pa i strana koja se protivi nečemu ili nekome.</p>
</div>
</div></li>
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<li class="widget widget_sow-editor" id="sow-editor-4"><div class="so-widget-sow-editor so-widget-sow-editor-base"><h2 class="widget-title">Kontakt</h2>
<div class="siteorigin-widget-tinymce textwidget">
<p>Adresa:Dr. Agostina Neta 66,<br/>
Belgrade, Serbia<br/>
Telefon: +38160/532-1440<br/>
E-mail:tanja@tm-lawoffice.com</p>
</div>
</div></li>
</div>
</div><!-- .container -->
<div class="container">
<div class="site-info">
<div class="col-sm-10 col-sm-offset-1 section-line section-line-footer"></div>
<div class="site-info-inner">
<a href="http://tm-lawoffice.com/" rel="nofollow" target="_blank">TM LAW OFFICE</a> powered by <a class="" href="https://digital2.rs/" rel="nofollow" target="_blank">Digital2</a>
</div><!-- .site-info-inner -->
</div><!-- .site-info -->
</div>
</footer><!-- #colophon -->
<div class="preloader">
<div class="status"> </div>
</div>
</div><!-- #page -->
<!-- Modal -->
<div aria-labelledby="myModalLabel" class="modal fade" id="siteModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<button aria-label="Close" class="close modal-close-button" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
<div class="modal-content">
<div class="modal-body">
<div class="pirate_forms_container widget-no" id="pirate_forms_container_default">
<!-- header -->
<!-- thank you -->
<div class="pirate_forms_wrap">
<!-- errors -->
<!-- form -->
<form class="pirate_forms form_honeypot-on wordpress-nonce-on pirate-forms-contact-name-on pirate-forms-contact-email-on pirate-forms-contact-subject-on pirate-forms-contact-message-on pirate-forms-contact-submit-on pirate_forms_from_form-on" enctype="application/x-www-form-urlencoded" method="post">
<div class="pirate_forms_three_inputs_wrap ">
<div class="col-xs-12 pirate_forms_three_inputs form_field_wrap contact_name_wrap col-xs-12 col-sm-6 contact_name_wrap pirate_forms_three_inputs form_field_wrap">
<input class="form-control" id="pirate-forms-contact-name" name="pirate-forms-contact-name" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter your name')" placeholder="Your Name" required="" type="text" value=""/>
</div>
<div class="col-xs-12 pirate_forms_three_inputs form_field_wrap contact_email_wrap col-xs-12 col-sm-6 contact_email_wrap pirate_forms_three_inputs form_field_wrap">
<input class="form-control" id="pirate-forms-contact-email" name="pirate-forms-contact-email" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter valid email')" placeholder="Your Email" required="" type="email" value=""/>
</div>
<div class="col-xs-12 pirate_forms_three_inputs form_field_wrap contact_subject_wrap col-xs-12 contact_subject_wrap pirate_forms_three_inputs form_field_wrap">
<input class="form-control" id="pirate-forms-contact-subject" name="pirate-forms-contact-subject" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Please enter a subject')" placeholder="Subject" required="" type="text" value=""/>
</div>
</div>
<div class="col-xs-12 form_field_wrap contact_message_wrap col-xs-12 contact_message_wrap pirate_forms_three_inputs form_field_wrap">
<textarea class="form-control" cols="30" id="pirate-forms-contact-message" name="pirate-forms-contact-message" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Enter your question or comment')" placeholder="Your message" required="" rows="5"></textarea>
</div>
<div class="col-xs-12 form_field_wrap contact_submit_wrap">
<button class="pirate-forms-submit-button btn btn-primary" id="pirate-forms-contact-submit" name="pirate-forms-contact-submit" placeholder="" type="submit">Send Message</button>
</div>
<input class="" id="pirate_forms_ajax" name="pirate_forms_ajax" placeholder="" type="hidden" value="0"/><div class="form_field_wrap hidden" style="display: none"><input class="" id="form_honeypot" name="honeypot" placeholder="" type="text" value=""/></div><input class="" id="pirate_forms_from_widget" name="pirate_forms_from_widget" placeholder="" type="hidden" value="0"/><input class="" id="wordpress-nonce" name="wordpress-nonce" placeholder="" type="hidden" value="df31b2b833"/><input class="" id="pirate_forms_from_form" name="pirate_forms_from_form" placeholder="" type="hidden" value="8189e637d5"/> </form>
<div class="pirate_forms_clearfix"></div>
</div>
<!-- footer -->
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
<link href="https://tm-lawoffice.com/wp-content/plugins/pirate-forms/public/css/front.css?ver=2.4.3" id="pirate_forms_front_styles-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/js/bootstrap.min.js?ver=20130115" type="text/javascript"></script>
<script src="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/js/functions.js?ver=20120206" type="text/javascript"></script>
<script src="https://tm-lawoffice.com/wp-content/themes/lawyeriax-lite/js/skip-link-focus-fix.js?ver=20130115" type="text/javascript"></script>
<script src="https://tm-lawoffice.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var pirateFormsObject = {"errors":"","rest":{"submit":{"url":"https:\/\/tm-lawoffice.com\/wp-json\/pirate-forms\/v1\/send_email\/"},"nonce":"35c45d69e7"}};
/* ]]> */
</script>
<script src="https://tm-lawoffice.com/wp-content/plugins/pirate-forms/public/js/scripts.js?ver=2.4.3" type="text/javascript"></script>
</body>
</html>
