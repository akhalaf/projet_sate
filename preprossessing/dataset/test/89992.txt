<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>爱笑聚 - 最新影视剧分享平台</title>
<meta content="chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="爱笑聚影视提供最新的电影、电视剧的迅雷下载、百度网盘、bt种子下载以及在线观看，是影视资源聚合分享平台。" name="description"/>
<meta content="" name="keywords"/>
<link href="/themes/site/default/css/build/core.css?v=20200804" rel="stylesheet"/>
<link href="/themes/site/default/css/build/style.css?v=20200226" rel="stylesheet"/>
<!-- <base id="headbase" href="http://www.aixiaoju.com/" /> -->
<script>
//全局变量 Global Variables
var GV = {
	JS_ROOT : '/res/js/dev/',										//js目录
	JS_VERSION : '20200925',											//js版本号(不能带空格)
	JS_EXTRES : '/themes/extres',
	TOKEN : '6b965b9b73d2d556',	//token $.ajaxSetup data
	U_CENTER : '/space-index-run',		//用户空间(参数 : uid)
	U_AVATAR_DEF : '/res/images/face/face_small.jpg',					//默认小头像
	U_ID : parseInt('0'),									//uid
	REGION_CONFIG : '',														//地区数据
	CREDIT_REWARD_JUDGE : '',			//是否积分奖励，空值:false, 1:true
	URL : {
		LOGIN : '/u-login-run',										//登录地址
		QUICK_LOGIN : '/u-login-fast',								//快速登录
		IMAGE_RES: '/res/images',										//图片目录
		CHECK_IMG : '/u-login-showverify',							//验证码图片url，global.js引用
		VARIFY : '/verify-index-get',									//验证码html
		VARIFY_CHECK : '/verify-index-check',							//验证码html
		HEAD_MSG : {
			LIST : '/message-notice-minilist'							//头部消息_列表
		},
		USER_CARD : '/space-card-run',								//小名片(参数 : uid)
		LIKE_FORWARDING : '/bbs-post-doreply',							//喜欢转发(参数 : fid)
		REGION : '/misc-webData-area',									//地区数据
		SCHOOL : '/misc-webData-school',								//学校数据
		EMOTIONS : "/emotion-index-run?type=bbs",					//表情数据
		CRON_AJAX : '',											//计划任务 后端输出执行
		FORUM_LIST : '/bbs-forum-list',								//版块列表数据
		CREDIT_REWARD_DATA : '/u-index-showcredit',					//积分奖励 数据
		AT_URL: '/bbs-remind-run',									//@好友列表接口
		TOPIC_TYPIC: '/bbs-forum-topictype'							//主题分类
	}
};
</script>
<script src="/res/js/dev/wind.js?v=20200925"></script>
</head>
<body>
<div class="wrap">
<header class="header_wrap">
<div class="header cc" id="J_header">
<div class="logo">
<a href="/" title="爱笑聚">爱笑聚</a>
</div>
<nav class="nav_wrap">
<div class="nav">
<ul>
<li><a href="/">首页</a></li>
<li><a href="/bbs-index-run">最新</a></li>
<li><a href="/thread-1">电影</a></li>
<li><a href="/thread-18">剧集</a></li>
<li><a href="/thread-28">综艺</a></li>
<li><a href="/tag-%E5%8A%A8%E7%94%BB">动漫</a></li>
<li><a href="/like-mylike-run">订阅</a></li>
<li><a href="/thread-15">交流</a></li>
</ul>
</div>
</nav>
<div class="header_search" role="search">
<form action="/search-s-run" method="post">
<input accesskey="s" aria-label="搜索关键词" id="s" name="keyword" placeholder="输入影片/IMDB编号/影人搜索" speech="" type="text" x-webkit-speech=""/>
<button aria-label="搜索" type="submit"><span>搜索</span></button>
<input name="csrf_token" type="hidden" value="6b965b9b73d2d556"/></form>
</div>
<div class="header_login">
<a href="/u-login-run" rel="nofollow">登录</a><a href="/u-register-run" rel="nofollow">注册</a>
</div>
</div>
</header>
<div class="main_wrap">
<div class="cc">
<div class="box_wrap design_layout_style J_mod_layout_none" data-lcm="100" id="btnbox" role="structure_btnbox"> <h2 class="design_layout_hd cc J_layout_hd" role="titlebar" style="display: none;"><span>100%</span></h2> <div class="design_layout_ct J_layout_item"><div class="mod_box J_mod_box" data-id="195" data-model="thread" id="J_mod_195" style="">
<div class="tags_hot" style="padding:7px 0 7px 11px;">
<ul class="cc">
<li class="newindexbt">电影</li>
<li><a href="/thread-1" target="_blank">全部</a></li>
<li><a href="/thread-1?tab=digest" target="_blank">精华</a></li>
<li><a href="/thread-5" target="_blank">欧美</a></li>
<li><a href="/thread-14" target="_blank">华语</a></li>
<li><a href="/thread-3" target="_blank">日本</a></li>
<li><a href="/thread-2" target="_blank">韩国</a></li>
<li><a href="/thread-4" target="_blank">动漫</a></li>
<li><a href="/thread-17" target="_blank">亚洲</a></li>
<li><a href="/thread-26" target="_blank">往期</a></li>
</ul>
</div>
<div class="tmode_imgGroupnew tmode_tagFront">
<ul class="cc" style="margin-left:7px;">
<li>
<a href="/read-41916" target="_blank"><img alt="同步" class="layeach" data-echo="/attachment/moviedetails/41916.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>同步</p><p class="newindexbt2">昨天20:04 订阅:11</p></a>
</li>
<li>
<a href="/read-41919" target="_blank"><img alt="所言所行" class="layeach" data-echo="/attachment/moviedetails/41919.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>所言所行</p><p class="newindexbt2">昨天21:18 订阅:3</p></a>
</li>
<li>
<a href="/read-41907" target="_blank"><img alt="隔绝之巢" class="layeach" data-echo="/attachment/moviedetails/41907.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>隔绝之巢</p><p class="newindexbt2">昨天16:31 订阅:4</p></a>
</li>
<li>
<a href="/read-41908" target="_blank"><img alt="棒！少年" class="layeach" data-echo="/attachment/moviedetails/41908.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>棒！少年</p><p class="newindexbt2">昨天16:37 订阅:9</p></a>
</li>
<li>
<a href="/read-41863" target="_blank"><img alt="穷途鼠的奶酪梦" class="layeach" data-echo="/attachment/moviedetails/41863.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>穷途鼠的奶酪梦</p><p class="newindexbt2">前天19:27 订阅:6</p></a>
</li>
<li>
<a href="/read-41852" target="_blank"><img alt="奇迹泽尼亚" class="layeach" data-echo="/attachment/moviedetails/41852.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>奇迹泽尼亚</p><p class="newindexbt2">前天13:32 订阅:5</p></a>
</li>
<li>
<a href="/read-41764" target="_blank"><img alt="斋戒时节遇见你" class="layeach" data-echo="/attachment/moviedetails/41764.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>斋戒时节遇见你</p><p class="newindexbt2">01-09 15:38 订阅:6</p></a>
</li>
<li>
<a href="/read-41659" target="_blank"><img alt="女人的碎片" class="layeach" data-echo="/attachment/moviedetails/41659.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>女人的碎片</p><p class="newindexbt2">01-07 17:04 订阅:18</p></a>
</li>
<li>
<a href="/read-41639" target="_blank"><img alt="我死之日" class="layeach" data-echo="/attachment/moviedetails/41639.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>我死之日</p><p class="newindexbt2">01-07 01:17 订阅:4</p></a>
</li>
<li>
<a href="/read-41148" target="_blank"><img alt="到达挚爱" class="layeach" data-echo="/attachment/moviedetails/41148.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>到达挚爱</p><p class="newindexbt2">2020-12-29 20:30 订阅:6</p></a>
</li>
<li>
<a href="/read-41594" target="_blank"><img alt="白草地" class="layeach" data-echo="/attachment/moviedetails/41594.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>白草地</p><p class="newindexbt2">01-06 00:31 订阅:12</p></a>
</li>
<li>
<a href="/read-41471" target="_blank"><img alt="除暴" class="layeach" data-echo="/attachment/moviedetails/41471.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>除暴</p><p class="newindexbt2">01-03 15:56 订阅:18</p></a>
</li>
<li>
<a href="/read-41325" target="_blank"><img alt="前程似锦的女孩" class="layeach" data-echo="/attachment/moviedetails/41325.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>前程似锦的女孩</p><p class="newindexbt2">2020-12-31 15:12 订阅:40</p></a>
</li>
<li>
<a href="/read-41326" target="_blank"><img alt="米纳里" class="layeach" data-echo="/attachment/moviedetails/41326.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>米纳里</p><p class="newindexbt2">2020-12-31 15:27 订阅:33</p></a>
</li>
</ul>
</div>
<div class="tags_hot" style="padding:27px 0 7px 11px;">
<ul class="cc">
<li class="newindexbt">剧集</li>
<li><a href="/thread-18" target="_blank">全部</a></li>
<li><a href="/thread-18?tab=digest" target="_blank">精华</a></li>
<li><a href="/thread-19" target="_blank">欧美剧</a></li>
<li><a href="/thread-20" target="_blank">大陆剧</a></li>
<li><a href="/thread-24" target="_blank">港台剧</a></li>
<li><a href="/thread-21" target="_blank">日剧</a></li>
<li><a href="/thread-22" target="_blank">韩剧</a></li>
<li><a href="/thread-23" target="_blank">动漫</a></li>
<li><a href="/thread-36" target="_blank">泰剧</a></li>
<li><a href="/thread-27" target="_blank">往期</a></li>
</ul>
</div>
<div class="tmode_imgGroupnew tmode_tagFront">
<ul class="cc" style="margin-left:7px;">
<li>
<a href="/read-41909" target="_blank"><img alt="黑色皮革手册SP～拐带行~" class="layeach" data-echo="/attachment/moviedetails/41909.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>黑色皮革手册SP～拐带行~</p><p class="newindexbt2">日剧 订阅:3</p></a>
</li>
<li>
<a href="/read-41891" target="_blank"><img alt="美国众神 第三季" class="layeach" data-echo="/attachment/moviedetails/41891.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>美国众神 第三季</p><p class="newindexbt2">欧美剧 订阅:12</p></a>
</li>
<li>
<a href="/read-40076" target="_blank"><img alt="进击的巨人 第四季" class="layeach" data-echo="/attachment/moviedetails/40076.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>进击的巨人 第四季</p><p class="newindexbt2">动漫 订阅:154</p></a>
</li>
<li>
<a href="/read-41843" target="_blank"><img alt="无职转生：到了异世界就拿出真本事" class="layeach" data-echo="/attachment/moviedetails/41843.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>无职转生：到了异世界就拿出真本事</p><p class="newindexbt2">动漫 订阅:16</p></a>
</li>
<li>
<a href="/read-41794" target="_blank"><img alt="堀与宫村" class="layeach" data-echo="/attachment/moviedetails/41794.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>堀与宫村</p><p class="newindexbt2">动漫 订阅:3</p></a>
</li>
<li>
<a href="/read-41793" target="_blank"><img alt="工作细胞black" class="layeach" data-echo="/attachment/moviedetails/41793.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>工作细胞black</p><p class="newindexbt2">动漫 订阅:17</p></a>
</li>
<li>
<a href="/read-41787" target="_blank"><img alt="工作细胞!! 第二季" class="layeach" data-echo="/attachment/moviedetails/41787.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>工作细胞!! 第二季</p><p class="newindexbt2">动漫 订阅:20</p></a>
</li>
<li>
<a href="/read-41781" target="_blank"><img alt="灵域" class="layeach" data-echo="/attachment/moviedetails/41781.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>灵域</p><p class="newindexbt2">大陆剧 订阅:8</p></a>
</li>
<li>
<a href="/read-41776" target="_blank"><img alt="发现女巫 第二季" class="layeach" data-echo="/attachment/moviedetails/41776.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>发现女巫 第二季</p><p class="newindexbt2">欧美剧 订阅:17</p></a>
</li>
<li>
<a href="/read-41770" target="_blank"><img alt="我们挚爱的故事" class="layeach" data-echo="/attachment/moviedetails/41770.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>我们挚爱的故事</p><p class="newindexbt2">港台剧 订阅:10</p></a>
</li>
<li>
<a href="/read-41753" target="_blank"><img alt="上阳赋" class="layeach" data-echo="/attachment/moviedetails/41753.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>上阳赋</p><p class="newindexbt2">大陆剧 订阅:25</p></a>
</li>
<li>
<a href="/read-41706" target="_blank"><img alt="弗兰·勒博维茨：假装我们在城市" class="layeach" data-echo="/attachment/moviedetails/41706.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>弗兰·勒博维茨：假装我们在城市</p><p class="newindexbt2">欧美剧 订阅:12</p></a>
</li>
<li>
<a href="/read-41704" target="_blank"><img alt="认识的妻子" class="layeach" data-echo="/attachment/moviedetails/41704.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>认识的妻子</p><p class="newindexbt2">日剧 订阅:14</p></a>
</li>
<li>
<a href="/read-41687" target="_blank"><img alt="狄金森 第二季" class="layeach" data-echo="/attachment/moviedetails/41687.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>狄金森 第二季</p><p class="newindexbt2">欧美剧 订阅:9</p></a>
</li>
</ul>
</div>
<div class="tags_hot" style="padding:27px 0 7px 11px;">
<ul class="cc">
<li class="newindexbt">综艺</li>
<li><a href="/thread-28" target="_blank">全部</a></li>
<li><a href="/thread-30" target="_blank">大陆</a></li>
<li><a href="/thread-29" target="_blank">日韩</a></li>
<li><a href="/thread-31" target="_blank">港台</a></li>
<li><a href="/thread-32" target="_blank">欧美</a></li>
</ul>
</div>
<div class="tmode_imgGroupnew tmode_tagFront" style="padding-bottom:27px;">
<ul class="cc" style="margin-left:7px;">
<li>
<a href="/read-40632" target="_blank"><img alt="奇葩说 第七季" class="layeach" data-echo="/attachment/moviedetails/40632.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>奇葩说 第七季</p><p class="newindexbt2">大陆 订阅:23</p></a>
</li>
<li>
<a href="/read-40293" target="_blank"><img alt="国家宝藏 第三季" class="layeach" data-echo="/attachment/moviedetails/40293.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>国家宝藏 第三季</p><p class="newindexbt2">大陆 订阅:18</p></a>
</li>
<li>
<a href="/read-39668" target="_blank"><img alt="我们离婚了" class="layeach" data-echo="/attachment/moviedetails/39668.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>我们离婚了</p><p class="newindexbt2">日韩 订阅:7</p></a>
</li>
<li>
<a href="/read-39506" target="_blank"><img alt="令人心动的offer 第二季" class="layeach" data-echo="/attachment/moviedetails/39506.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>令人心动的offer 第二季</p><p class="newindexbt2">大陆 订阅:6</p></a>
</li>
<li>
<a href="/read-39330" target="_blank"><img alt="第57届金马奖颁奖典礼‎" class="layeach" data-echo="/attachment/moviedetails/39330.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>第57届金马奖颁奖典礼‎</p><p class="newindexbt2">港台 订阅:4</p></a>
</li>
<li>
<a href="/read-39301" target="_blank"><img alt="哈哈哈哈哈" class="layeach" data-echo="/attachment/moviedetails/39301.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>哈哈哈哈哈</p><p class="newindexbt2">大陆 订阅:8</p></a>
</li>
<li>
<a href="/read-37109" target="_blank"><img alt="舞蹈风暴 第二季" class="layeach" data-echo="/attachment/moviedetails/37109.jpg" height="174" src="/res/lazyeach/blank.gif" width="125"/><p>舞蹈风暴 第二季</p><p class="newindexbt2">大陆 订阅:8</p></a>
</li>
</ul>
</div>
</div></div></div></div>
</div>
<!--.main-wrap,#main End-->
<div class="footer_wrap">
<div class="footer">
<div class="footerleft">© 2020 axjhd.com all rights reserved</div>
<div class="footerright">
<a href="?mobile=yes" rel="nofollow">手机访问</a><a href="/read-88" rel="nofollow">使用帮助</a><a href="/read-12258" rel="nofollow">发布资源</a><a href="/read-18436" rel="nofollow">选择线路</a>
<span style="display:none"><script src="https://s23.cnzz.com/z_stat.php?id=1276966312&amp;web_id=1276966312" type="text/javascript"></script></span>
</div>
</div>
</div>
<!--返回顶部-->
<a href="#" id="back_top" rel="nofollow" role="button" tabindex="-1">返回顶部</a>
</div>
<script>
Wind.use('jquery', 'global', function() {
	Wind.js('/res/lazyeach/lazyeach.js?v='+ GV.JS_VERSION, function() {
		Wind.css('/res/lazyeach/lazyeach.css?v='+ GV.JS_VERSION)
        lazyEcho.init({
            offset: 0,
            throttle: 0
        });
	});
});
</script>
</body>
</html>