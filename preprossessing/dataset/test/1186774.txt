<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Strony nie znaleziono</title>
<!-- Styles -->
<style>
        html, body {
            color: #636b6f;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: 100;
            height: 100%;
            margin: 0;
            background: linear-gradient(to bottom, rgba(238, 238, 238, 1) 0%, rgba(204, 204, 204, 1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */

        }

        .full-height {
            height: 100%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 36px;
            padding: 20px;
        }

        a, p {
            font-size: 24px;
        }

    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
<div class="content">
<h1>Bład 404</h1>
<p>
            Podana strona nie istnieje
        </p>
<a href="/">Strona główna</a>
</div>
</div>
</body>
</html>
