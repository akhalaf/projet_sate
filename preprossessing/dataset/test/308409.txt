<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Head Hunting Services India, HR Consultants India - Recruitment Consultants, Placement Consultancy, Executive Search</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Mr. E. Chandran's TRC is one of the pioneer HR consultants in Mumbai India offering Complete Solutions" name="abstract"/>
<meta content="The Right Connection offers Executive Head Hunting Services, Head Hunting Services in India, HR consultantancy, Placement Consultancy, Recruitment Consultancy, HR Consultants, " name="description"/>
<meta content="Head Hunting Services, Headhunting services india, HR Consultants india, Placement Consultancy, consultants, Executive Search, Executive Placements, recruitment, " name="keywords"/>
<meta content="7 days" name="revisit-after"/>
<meta content="sunsolutions@vsnl.net" name="author"/>
<!--

body {

	background-color: #531a03;

	margin-left: 0px;

	margin-top: 0px;

	margin-right: 0px;

	margin-bottom: 0px;

}

.table {

	font-family: Verdana, Arial, Helvetica, sans-serif;

	font-size: 11px;

	font-style: normal;

	line-height: 20px;

	font-weight: normal;

	font-variant: normal;

	text-transform: none;

	color: #000000;

}

-->
<link href="style_sheet.css" rel="stylesheet" type="text/css"/>
<style type="text/css">

<!--

body {

	background-color: #531a03;

	margin-left: 00px;

	margin-top: 0px;

	margin-right: 0px;

	margin-bottom: 0px;

}

.style2 {color: #FFFFFF}

.style3 {color: #531A03}

-->

</style>
<script language="JavaScript" type="text/JavaScript">

<!--

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}



function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v4.01

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && d.getElementById) x=d.getElementById(n); return x;

}



function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

//-->

</script>
</head>
<body onload="MM_preloadImages('images/data_bank_over.jpg','images/head_hunting_over.jpg','images/advertisement_over.jpg','images/post_over.jpg','images/apply_over.jpg')">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td></td>
<td align="left" valign="top" width="740"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" height="190" title="the right connection" width="740">
<param name="movie" value="images/trc.swf"/>
<param name="quality" value="high"/>
<embed height="190" pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="images/trc.swf" type="application/x-shockwave-flash" width="740"/>
</object></td>
<td align="left" valign="top"> </td>
</tr>
<tr>
<td></td>
<td align="left" valign="top" width="740"><table align="center" cellpadding="0" cellspacing="0" width="740">
<tr>
<td align="left" background="images/therightconnection.jpg" height="50" valign="top" width="742"> </td>
</tr>
<tr>
<td align="left" background="images/two.jpg" valign="top"><table cellpadding="0" cellspacing="0" height="160" width="740">
<tr align="left" valign="top">
<td width="400"><table cellpadding="0" cellspacing="0" height="150" width="400">
<tr align="left" valign="top">
<td width="16"> </td>
<td width="290"><div align="justify"><span class="text">The Right Connection (TRC) was incorporated  in April 1984, especially catering to the ever increasing demand for Middle and Senior Level Executives from all disciplines covering most of the Industries / Institutions. TRC is one of the pioneer HR consultants of Mumbai serving Top 'A' rated Clients for over two decades now.<br/>
</span></div></td>
<td width="85"><table align="right" cellpadding="0" cellspacing="0" width="100">
<tr>
<td align="right" valign="top"><img alt="Mr. E. Chandran" height="130" src="images/echandran.jpg" width="85"/></td>
</tr>
</table></td>
<td width="15"> </td>
</tr>
</table></td>
<td width="340"><table cellpadding="0" cellspacing="0" width="100%">
<tr align="left" valign="top">
<td width="20"> </td>
<td width="316"><a href="executive_search_services.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image8','','images/data_bank_over.jpg',1)"><img alt="Executive Search Services - Data Bank" border="0" height="40" id="Image8" name="Image8" src="images/data_bank.jpg" width="225"/></a></td>
</tr>
<tr align="left" valign="top">
<td height="15"></td>
<td height="15"></td>
</tr>
<tr align="left" valign="top">
<td> </td>
<td><a href="head_hunting_services_mumbai.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','images/head_hunting_over.jpg',1)"><img alt="Executive Search Services - Head Hunting" border="0" height="40" id="Image9" name="Image9" src="images/head_hunting.jpg" width="225"/></a></td>
</tr>
<tr align="left" valign="top">
<td height="15"></td>
<td height="15"></td>
</tr>
<tr align="left" valign="top">
<td> </td>
<td><a href="plcaement_conusultants_mumbai.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','images/advertisement_over.jpg',1)"><img alt="Executive Search Services - Advertisement" border="0" height="40" id="Image10" name="Image10" src="images/advertisement.jpg" width="225"/></a></td>
</tr>
</table></td>
</tr>
</table>
<table cellpadding="0" cellspacing="0" width="740">
<tr align="left" valign="top">
<td width="15"> </td>
<td width="360"><div align="justify"><span class="text">TRC's expertise lies in sourcing candidates from our own <br/>

                  database of Senior &amp; Middle level Executives across industries to fulfill our Client requirements apart from undertaking Value based Head Hunting assignments. TRC is based in Mumbai covering the entire country through various Associate offices in most metros.</span></div></td>
<td width="25"></td>
<td width="300"><p align="justify" class="text">ACHIEVEMENT.... Frankly one cannot excel without <br/>

the other’s commitment in reaching the above goal.

          This exactly is what THE RIGHT CONNECTION is all about. Understanding ones requirements, identifying similar organizations and creating a new atmosphere for both. See for yourself. </p></td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" background="images/two.jpg" valign="top"><table cellpadding="0" cellspacing="0" width="740">
<tr align="left" valign="top">
<td width="400"><table cellpadding="0" cellspacing="0" width="400">
<tr align="left" valign="top">
<td height="10" valign="middle"></td>
</tr>
<tr align="left" valign="top">
<td width="20"> </td>
<td background="images/menu_back.jpg" height="31" valign="middle"><div align="center"><span class="text_all style1"><a class="link" href="index.htm">Home</a>    </span><span class="link">| </span><span class="text_all style1">   <a class="link" href="the_right_connection.htm">About Us</a>  </span><span class="link">  | </span><span class="text_all style1">   <a class="link" href="executive_search_services.htm">Services</a>  </span><span class="link">  |  </span><span class="text_all style1">  <a class="link" href="contactus.htm">Contact Us </a></span></div></td>
<td width="20"></td>
</tr>
</table></td>
<td width="340"><table cellpadding="0" cellspacing="0" width="340">
<tr align="left" height="10" valign="top">
<td></td>
<td class="text2"></td>
<td></td>
<td class="text2"></td>
<td></td>
</tr>
<tr align="left" valign="top">
<td> </td>
<td class="text2"><div align="center">EMPLOYER</div></td>
<td> </td>
<td class="text2"><div align="center">CANDIDATE</div></td>
<td> </td>
</tr>
<tr align="left" valign="top">
<td width="20"> </td>
<td width="144"><a href="executive_placements.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image11','','images/post_over.jpg',1)"><img border="0" height="22" id="Image11" name="Image11" src="images/post_requirement.jpg" width="144"/></a></td>
<td width="20"> </td>
<td width="126"><a href="executive_jobs.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','images/apply_over.jpg',1)"><img alt="Apply Candidate" border="0" height="22" id="Image7" name="Image7" src="images/apply_candidate.jpg" width="126"/></a></td>
<td width="20"> </td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" valign="top"><img height="15" src="images/bottom_img.jpg" width="740"/></td>
</tr>
</table></td>
<td align="left" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td height="53"> </td>
</tr>
<tr>
<td height="41"><span class="table style3">Placement consultants india </span></td>
</tr>
<tr>
<td height="19"> </td>
</tr>
<tr>
<td><span class="table style3">head hunting services mumbai india </span></td>
</tr>
<tr>
<td> </td>
</tr>
<tr>
<td height="37"><span class="table style3">executive headhunting services</span></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" valign="top"><div align="center" class="table style3">HR Consultancy Executive Search Services </div></td>
<td align="left" valign="top" width="740"><div align="center"><span class="copyright_link style2">Copyright© The Right Connection 2008. All Rights Reserved.</span></div></td>
<td align="left" valign="top"> </td>
</tr>
</table>
</body>
</html>
