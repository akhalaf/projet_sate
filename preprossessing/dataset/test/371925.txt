<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<title>Crescent Tracking Pvt Ltd | Best Vehicle Tracking Service Provider</title>
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/animate.min.css" rel="stylesheet"/>
<link href="css/lightbox.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<link href="css/responsive.css" rel="stylesheet"/>
<!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="images/ico/favicon1.ico" rel="shortcut icon"/>
<link href="images/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="images/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="images/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="images/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed"/>
</head><!--/head-->
<body>
<header id="header">
<div class="container">
<div class="row">
<div class="col-sm-12 overflow">
<div class="social-icons pull-right">
<ul class="nav nav-pills">
<li><a href="https://www.facebook.com/pg/crescenttracking
"><i class="fa fa-facebook"></i></a></li>
<li><a href=""><i class="fa fa-twitter"></i></a></li>
<li><a href=""><i class="fa fa-google-plus"></i></a></li>
<li><a href=""><i class="fa fa-dribbble"></i></a></li>
<li><a href=""><i class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="navbar navbar-inverse" role="banner">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.html">
<h1><img alt="logo" src="images/logo.png"/></h1>
</a>
</div>
<div class="collapse navbar-collapse">
<ul class="nav navbar-nav navbar-right">
<li class="active"><a href="index.html">Home</a></li>
<li class="dropdown"><a href="#">About Us <i class="fa fa-angle-down"></i></a>
<ul class="sub-menu" role="menu">
<li><a href="aboutus2.html">Why Crescent?</a></li>
<li><a href="news.html">News &amp; Media</a></li>
<li><a href="faqs.html">FAQs</a></li>
<li><a href="careers.html">Careers</a></li>
<li><a href="ceomsg.html">CEO Message</a></li>
<li><a href="quicklinks.html">Quick Links</a></li>
</ul>
</li>
<li class="dropdown"><a href="">Products <i class="fa fa-angle-down"></i></a>
<ul class="sub-menu" role="menu">
<li><a href="vehicletracking.html">Vehicle Tracking</a></li>
<li><a href="assettracking.html">Assets Tracking</a></li>
<li><a href="biketracking.html">Bike Tracking</a></li>
<li><a href="fleettracking.html">Fleet Management System</a></li>
<li><a href="gensettracking.html">Gen-Set Tracking</a></li>
<li><a href="fuelsensor.html">Fuel Level Sensors</a></li>
<li><a href="loadsensor.html">Axle Load Sensors</a></li>
</ul>
</li>
<li class="dropdown"><a href="packages.html">Packages </a>
</li><li><a href="contactus.html ">Contact Us</a></li>
</ul>
</div>
<div class="search">
<form role="form">
<i class="fa fa-search"></i>
<div class="field-toggle">
<input autocomplete="off" class="search-form" placeholder="Search" type="text"/>
</div>
</form>
</div>
</div>
</div>
</header>
<!--/#header-->
<section id="home-slider">
<div class="container">
<div class="row">
<div class="main-slider">
<div class="slide-text">
<h2>Pakistan's No. 1 Tracking Mobile App</h2>
<p>We are a large business group and Crescent Tracking is a vehicle tracking company that has emerged into a competitive business environment where service providers are compelled to optimize resources, timely information has become a critical issue in achieving success.</p>
<h2>Our Mission</h2>
<p>To Protect 24/7 with Unbreakable Connectivity, To Present Value Added Technology, To Stop Unauthorized Assessability.</p> <h2>Our Vision</h2> <p> Just not Protecting Your Assets its Protecting You with Latest &amp; Most Advanced Vehicle Tracking Equipment. </p> <a class="btn btn-common" href="packages.html">Buy Now</a>
</div>
<img alt="slider image" class="slider-hill" src="images/home/slider/hill.png"/>
</div>
</div>
</div>
<div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
</section>
<!--/#home-slider-->
<section id="services">
<div class="container">
<div class="row">
<div class="col-sm-4 text-center padding wow fadeIn" data-wow-delay="300ms" data-wow-duration="1000ms">
<div class="single-service">
<div class="wow scaleIn" data-wow-delay="300ms" data-wow-duration="500ms">
<img alt="" src="images/home/icon1.png"/>
</div>
<h2>Vehicle Tracking</h2>
<p>The Best Vehicle Tracking Solution. Unbreakable Services. We provide complete solutions, including both Software and Hardware solutions for real-time GPS and satellite tracking and monitoring.</p>
</div>
</div>
<div class="col-sm-4 text-center padding wow fadeIn" data-wow-delay="600ms" data-wow-duration="1000ms">
<div class="single-service">
<div class="wow scaleIn" data-wow-delay="600ms" data-wow-duration="500ms">
<img alt="" src="images/home/icon2.png"/>
</div>
<h2>Bike Tracking</h2>
<p>Providing the Best Bike Tracking Solution in Pakistan with Latest and Most Advance Tracking Equipment. Location accuracy is less than 2.5 meters, the best accuracy allowed for consumer use.</p>
</div>
</div>
<div class="col-sm-4 text-center padding wow fadeIn" data-wow-delay="900ms" data-wow-duration="1000ms">
<div class="single-service">
<div class="wow scaleIn" data-wow-delay="900ms" data-wow-duration="500ms">
<img alt="" src="images/home/icon3.png"/>
</div>
<h2>Fleet Management</h2>
<p>Fleet Management Solution with Latest Mobile Applications and Web Tracking. GPS tracking and fleet management solutions that significantly help increase productivity, cut costs, improve safety and service</p>
</div>
</div>
</div>
</div>
</section>
<!--/#services-->
<section class="responsive" id="action">
<div class="vertical-center">
<div class="container">
<div class="row">
<div class="action take-tour">
<div class="col-sm-7 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="500ms">
<h1 class="title">Best Mobile Application.</h1>
<p>Smooth and Real Time Tracking on Mobile</p>
</div>
<div class="col-sm-5 text-center wow fadeInRight" data-wow-delay="300ms" data-wow-duration="500ms">
<div class="tour-button">
<a class="btn btn-common" href="https://play.google.com/store/apps/details?id=com.gosafesystem.gpsmonitor&amp;hl=en">Download Now</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--/#action-->
<section id="features">
<div class="container">
<div class="row">
<div class="single-features">
<div class="col-sm-5 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="500ms">
<img alt="" class="img-responsive" src="images/home/image1.jpg"/>
</div>
<div class="col-sm-6 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="500ms">
<h2>24/7 Control Room Operations</h2>
<p>24/7 Control Room and Call Center Facility to provide Best Quality of Services like Geo-Fence Limit Cross Call, Battery Temper Call. 24/7 Facility for Customer to get his Asset Details.</p>
</div>
</div>
<div class="single-features">
<div class="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="500ms">
<h2>Web Access for Self Tracking</h2>
<p>Smooth and Lite Web Access for Tracking your vehicle or Fleet. Get Daily, Weekly or Monthly Reports. Get an estimate of the fuel usage by vehicle for a specified period of time Current and historical performance. These reports allow you to compare current performance to past performance to determine how your vehicles are really performing.</p>
</div>
<div class="col-sm-5 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="500ms">
<img alt="" class="img-responsive" src="images/home/image2.jpg"/>
</div>
</div>
<div class="single-features">
<div class="col-sm-5 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="500ms">
<img alt="" class="img-responsive" src="images/home/image3.png"/>
</div>
<div class="col-sm-6 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="500ms">
<h2>Fuel, Weight and Temperature Sensors</h2>
<p>Fuel Level Sensors, Weight Sensors and Temperature Sensors. Crescen Fuel Level Sensor gives a general overview on fuel usage by vehicle or stationary unit. Axle load sensor passes the information on raising or lowering the load on axle to terminal .</p>
</div>
</div>
</div>
</div>
</section>
<!--/#features-->
<section id="clients">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="clients text-center wow fadeInUp" data-wow-delay="300ms" data-wow-duration="500ms">
<p><img alt="" class="img-responsive" src="images/home/clients.png"/></p>
<h1 class="title">Happy Clients</h1>
<p>Our Best Corporative Customers <br/></p>
</div>
<div class="clients-logo wow fadeIn" data-wow-delay="600ms" data-wow-duration="1000ms">
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client1.png
"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client2.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client3.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client4.png"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client5.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client6.jpg"/></a>
</div>
</div>
<div class="clients-logo wow fadeIn" data-wow-delay="600ms" data-wow-duration="1000ms">
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client7.jpg
"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client8.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client9.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client10.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client11.png"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client12.png"/></a>
</div>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client13.png"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client14.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client15.jpg"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client16.png"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client17.png"/></a>
</div>
<div class="col-xs-3 col-sm-2">
<a href="#"><img alt="" class="img-responsive" src="images/home/client18.png"/></a>
</div>
</div>
</div>
</div>
</section>
<!--/#clients-->
<footer id="footer">
<div class="container">
<div class="row">
<div class="col-sm-12 text-center bottom-separator">
<img alt="" class="img-responsive inline" src="images/home/under.png"/>
</div>
<div class="col-md-4 col-sm-6">
<div class="testimonial bottom">
<h2>News &amp; Updates</h2>
<div class="media">
<div class="pull-left">
<a href="#"><img alt="" src="images/home/profile2.png"/></a>
</div>
<div class="media-body">
<blockquote>Crescent Tracking Pvt Ltd won the Vehicle Tracking Tender of Allama Iqbal Open University, Islamabad</blockquote>
<h3><a href="#">-20-03-2020</a></h3>
</div>
</div>
<div class="media">
<div class="pull-left">
<a href="#"><img alt="" src="images/home/profile1.jpg"/></a>
</div>
<div class="media-body">
<blockquote>Crescent Tracking Pvt Ltd won the Vehicle Tracking Tender of Punjab Food Authority</blockquote>
<h3><a href="">-22-01-2020</a></h3>
</div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="contact-info bottom">
<h2>Contacts</h2>
<address>
                        E-mail: <a href="mailto:someone@example.com">info@crescenttrack.com</a> <br/> 
                        Phone: +92-81-2832920 <br/> 
                        Fax: +92-81-2837363 <br/>
</address>
<h2>Address</h2>
<address>
                        Office No. 4,5,6, 2nd Floor<br/> 
                        Gul Compelx, Opp MCB Circle Office <br/> 
                        M.A Jinnah Road Quetta <br/>
</address>
</div>
</div>
<div class="col-md-4 col-sm-12">
<div class="contact-form bottom">
<h2>Send a message</h2>
<form action="sendemail.php" id="main-contact-form" method="post" name="contact-form">
<div class="form-group">
<input class="form-control" name="name" placeholder="Name" required="required" type="text"/>
</div>
<div class="form-group">
<input class="form-control" name="email" placeholder="Email Id" required="required" type="email"/>
</div>
<div class="form-group">
<textarea class="form-control" id="message" name="message" placeholder="Your text here" required="required" rows="8"></textarea>
</div>
<div class="form-group">
<input class="btn btn-submit" name="submit" type="submit" value="Submit"/>
</div>
</form>
</div>
</div>
<div class="col-sm-12">
<div class="copyright-text text-center">
<p>© All Rights Reserved</p>
<p>Crafted by <a href="" target="_blank">Crescent IT Solution</a></p>
</div>
</div>
</div>
</div>
</footer>
<!--/#footer-->
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/lightbox.min.js" type="text/javascript"></script>
<script src="js/wow.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
</body>
</html>
