<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Welcome to Zoolit</title>
<link href="/stylesheets/base.css" media="screen" rel="Stylesheet" type="text/css"/>
<!--[if IE]>
<link href="/stylesheets/ie_hacks.css" media="screen" rel="Stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">
//<![[CDATA[
function set_year() {
	var date = new Date();
	var target = document.getElementById('year');

	target.innerHTML = date.getFullYear();
}
//]]>
</script>
</head>
<body onload="set_year()">
<div id="wrapper">
<div id="liquid_top"> </div>
<div id="liquid_bottom">
<!--		<div id="header">
			<a href="/user/login/" id="logout">Login or Register</a>
			<a href="/index.html" title="Zoolit.com">
				<img src="/images/logo.png" alt="Zoolit" id="logo" />
			</a>
		</div>

		<div id="main_title">
			<h1>My Stuff</h1>
		</div>
		<hr class="hr" />
-->
<div id="header">
<div class="header_panel">
<a href="/index.html" title="Zoolit">
<img alt="Zoolit" src="/images/logo.png" style="border:none;"/>
</a>
</div>
<div class="header_panel" style="text-align:center;">
<h1 class="main_title">My Links</h1>
</div>
<div class="header_panel" style="text-align:right;">
<a class="logout" href="/user/login/">Login or Register</a>
</div>
<div style="clear:both; height:10px;"> </div>
</div>
<div class="row" style="width:340px; margin:40px auto 20px auto;">
<a href="/user/login/" title="Login or Register">
<img alt="Getting Started" src="/images/get_started.png" style="float:left; width:119px; height:40px; border:none;"/>
</a>
<a href="/tour.html" title="Take a Tour">
<img alt="Take a Tour" src="/images/tour.png" style="width:119px; height:40px; float:right; border:none;"/>
</a>
<div class="row"> </div>
</div>
<p class="presentation">Zoolit is a shareable Web page that lists all your personal sites. Your Zoolit Landing Page is always current and up to date, providing the world with all of your personal Websites, Social Networks, Blogs, contact info, and Photo and Video Sharing sites.</p>
<p class="presentation">When you add, change or delete a Website address, simply make the change on your Zoolit Page and its updated everywhere you have your Zoolit Button or Link. Zoolit is super simple to set up and use, and it is completely <strong>FREE</strong>.</p>
<div class="row" style="width:380px; margin:20px auto;">
<span class="share">Share your zoolit on: </span><span class="sites">Myspace ~ Facebook ~ Linkedln ~ Flickr ~ Del.icio.us ~ Blogs ~ Forums ~ Message Boards ~ Email Signatures ~ Anywhere...</span>
</div>
<div class="row box" onclick="window.location = '/user/signup/'">
<table cellpadding="3" style="padding:10px 0;">
<tbody>
<tr>
<td><img alt="Register for FREE Zoolit Account. It's Quick and Simple!" class="teaser" src="/images/1.png"/></td>
<td class="teaser"><a href="/user/signup/" title="Register">Register</a> for FREE Zoolit Account. It's Quick and Simple!</td>
</tr>
<tr>
<td><img alt="Enter the Website address for all your sites." class="teaser" src="/images/2.png"/></td>
<td class="teaser">Enter the Website addresses for all your sites.</td>
</tr>
<tr>
<td><img alt="Copy and paste custom tag into all your sites and blogs. When someone clicks your Zoolit button, they are taken to your Zoolit Page." class="teaser" src="/images/3.png"/></td>
<td class="teaser">Copy and paste custom tag into all your sites and blogs. When someone clicks your Zoolit button, they are taken to your Zoolit Page.</td>
</tr>
</tbody>
</table>
</div>
</div>
<div id="footer">
<div style="float:left;">© <span id="year"></span> Zoolit</div>
<div style="float:right;">
<a href="/pp.html" title="Privacy Policy">Privacy Policy</a> |
			<a href="/contact/show/" title="Contact">Contact</a></div>
</div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1607418-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
