<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Asurekazani.com | A way to BroadCast yourself!" name="description"/>
<meta content="Kadir Ergün" name="author"/>
<meta content="Asurekazani.com" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="Asurekazani.com | A way to BroadCast yourself!" property="og:description"/>
<meta content="http://www.asurekazani.com/" property="og:url"/>
<meta content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE.png" property="og:image"/>
<meta content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x600.png" property="og:image"/>
<meta content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x314.png" property="og:image"/>
<meta content="www.asurekazani.com" property="og:sitename"/>
<meta content="http://www.asurekazani.com" property="og:url"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@acachawiki" name="twitter:site"/>
<meta content="@acacha1" name="twitter:creator"/>
<title>Asurekazani.com | A way to BroadCast yourself!</title>
<link href="https://www.asurekazani.com/css/bootstrap.css" rel="stylesheet"/>
<link href="https://www.asurekazani.com/css/main.css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Raleway:400,300,700" rel="stylesheet" type="text/css"/>
<script src="https://www.asurekazani.com/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="https://www.asurekazani.com/js/smoothscroll.js"></script>
</head>
<body data-offset="0" data-spy="scroll" data-target="#navigation">
<div class="navbar navbar-default navbar-fixed-top" id="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#"><b>Asurekazani</b></a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="active"><a class="smoothScroll" href="#home">Home</a></li>
<li><a class="smoothScroll" href="#desc">Description</a></li>
<li><a class="smoothScroll" href="#showcase">Showcase</a></li>
<li><a class="smoothScroll" href="#contact">Contact</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a href="https://www.asurekazani.com/login">Login</a></li>
<li><a href="https://www.asurekazani.com/register">Register</a></li>
</ul>
</div>
</div>
</div>
<section id="home" name="home"></section>
<div id="headerwrap">
<div class="container">
<div class="row centered">
<div class="col-lg-12">
<h1>Asurekazani <b><a href="https://github.com/lightfire">Video Online</a></b></h1>
<h3>Create your own custom Youtube Channel with <a href="https://www.asurekazani.com/">Asurekazani</a> to enjoy more!
<a href="https://www.asurekazani.com/">Asurekazani</a> allows you to create Awesome customization
It's free lets Join us!</h3>
<h3><a class="btn btn-lg btn-success" href="https://www.asurekazani.com/register">Get Started!</a></h3>
</div>
<div class="col-lg-2">
<h5>Amazing admin template</h5>
<p>Based on adminlte bootstrap theme</p>
<img class="hidden-xs hidden-sm hidden-md" src="https://www.asurekazani.com/img/arrow1.png"/>
</div>
<div class="col-lg-8">
<img alt="" class="img-responsive" src="https://www.asurekazani.com/img/app-bg.png"/>
</div>
<div class="col-lg-2">
<br/>
<img class="hidden-xs hidden-sm hidden-md" src="https://www.asurekazani.com/img/arrow2.png"/>
<h5>Awesome packaged...</h5>
<p>/p&gt;
</p></div>
</div>
</div>
</div>
<section id="desc" name="desc"></section>
<div id="intro">
<div class="container">
<div class="row centered">
<h1>Designed To Excel</h1>
<br/>
<br/>
<div class="col-lg-4">
<img alt="" src="https://www.asurekazani.com/img/intro01.png"/>
<h3>Community</h3>
<p>See <a href="#">Github project</a>, post <a href="#">issues</a> and <a href="#">Pull requests</a></p>
</div>
<div class="col-lg-4">
<img alt="" src="https://www.asurekazani.com/img/intro02.png"/>
<h3>Schedule</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div>
<div class="col-lg-4">
<img alt="" src="https://www.asurekazani.com/img/intro03.png"/>
<h3>Monitoring</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div>
</div>
<br/>
<hr/>
</div>
</div>
<div id="features">
<div class="container">
<div class="row">
<h1 class="centered">What's New?</h1>
<br/>
<br/>
<div class="col-lg-6 centered">
<img alt="" class="centered" src="https://www.asurekazani.com/img/mobile.png"/>
</div>
<div class="col-lg-6">
<h3>Some Features</h3>
<br/>
<div class="accordion ac" id="accordion2">
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapseOne">
First Class Design
</a>
</div>
<div class="accordion-body collapse in" id="collapseOne">
<div class="accordion-inner">
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
</div>
</div>
<br/>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapseTwo">
Retina Ready Theme
</a>
</div>
<div class="accordion-body collapse" id="collapseTwo">
<div class="accordion-inner">
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
</div>
</div>
<br/>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapseThree">
Awesome Support
</a>
</div>
<div class="accordion-body collapse" id="collapseThree">
<div class="accordion-inner">
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
</div>
</div>
<br/>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-parent="#accordion2" data-toggle="collapse" href="#collapseFour">
Responsive Design
</a>
</div>
<div class="accordion-body collapse" id="collapseFour">
<div class="accordion-inner">
<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
</div>
</div>
<br/>
</div>
</div>
</div>
</div>
</div>
<section id="showcase" name="showcase"></section>
<div id="showcase">
<div class="container">
<div class="row">
<h1 class="centered">Some Screenshots</h1>
<br/>
<div class="col-lg-8 col-lg-offset-2">
<div class="carousel slide" id="carousel-example-generic">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
<li data-slide-to="1" data-target="#carousel-example-generic"></li>
</ol>
<div class="carousel-inner">
<div class="item active">
<img alt="" src="https://www.asurekazani.com/img/item-01.png"/>
</div>
<div class="item">
<img alt="" src="https://www.asurekazani.com/img/item-02.png"/>
</div>
</div>
</div>
</div>
</div>
<br/>
<br/>
<br/>
</div>
</div>
<section id="contact" name="contact"></section>
<div id="footerwrap">
<div class="container">
</div>
</div>
<div id="c">
<div class="container">
<p>
<a href="/">Anasayfa</a>
<a href="/top10">En Çok Izlenenler</a>
<a href="/categories">Kategoriler</a>
<a href="/sitemap">Site Haritasi</a>
<a href="/contact">Iletisim</a>
<a href="/page/kullanici_sozlesmesi">Terms and Conditions</a>
<a href="/page/gizlilik">Privacy</a>
<br/><br/>
<a href="/"><strong>Asurekazani.com</strong></a> |
<a href="/"><strong>Video izle</strong></a> |
<a href="/"><strong>Fragman izle</strong></a> |
<a href="/"><strong>Dizi izle</strong></a> |
<a href="/"><strong>Komik Videolar</strong></a> |
<a href="/diziler/2/Kurtlar_Vadisi_Pusu"><strong>Kurtlar Vadisi Pusu</strong></a> |
<a href="/diziler/1/Karadayi"><strong>Karadayı</strong></a> |
<a href="/"><strong>Ezel izle</strong></a> |
<a href="/"><strong>Film izle</strong></a> |
<a href="/"><strong>Enteresan Videolar izle</strong></a>
</p>
<div id="footer_warning">Sitemizde yer alan videolar başka sitelerden derlenmiştir. Hiçbir video Asurekazani.com Serverlarında barınmamaktadır.
<div id="amoung"><span>ONLINE</span></div>
</div>
</div>
</div>
<script src="https://www.asurekazani.com/js/bootstrap.min.js" type="text/javascript"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>
