<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "79784",
      cRay: "610c407b7da3dcfa",
      cHash: "3316822dfcff903",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWt2aWRlby5zdHJlYW0v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "G3FgPnQKm4VYgqQOFZm/+sq1Qg6S32fuaNHGoFj4Nup6jK4WwQtH5Rwp7KKrPhwPyCrTUHZHAoP1dd2cboVPyDRCm7e+bMI73nWrpgkNmrbi2E1/NdmmptdqdfAA2k5V00h+i370vbzN8sIAy7laJrszgJSa1j8PH/SpI89KAgU6kcGuPSoWKANpFhd7wasMe3qFiX0Af95rhqMau9bXo4tfA1d5e188RElOhHX08o3QGL3Ge9iybiHDwFFGG6HKpaiCiMnWG+HpNnSd6b21Ir8j1VsDMcwbNueVkt6IGqSqP5iiQjuQ73mswzTXVnO/k0K83e8EA+3n4/xXeLSWul0RmCumXsaZ77AMpuYi5Yr0NyaQ4THXbh4I6WNRnFRH0o+OlqGdn4Cid4GbdejFfa3RyeIWBPLlP49aF7wcdvV/VjXa3fjN3c88Zr6ZyeRZDwrRFdzf7+3eEYTw9VlXlXlWHj649PNTw7aK70g+AdpwuMKTclat6ZKoblaYR1zdF+p+WdHp4agNy78UKP9KhonjG+3kosIhhzzgcakbAXTTFtm2FUdhyzMK24uFI7HWW3NpPVtyy5Q2lVHxQgz4EUjVl7DAChcQ6+P1e25zLeL6ZPQcnoHms3slfrKrLcj7KmhrxkE8mzU4SG+7nCuzH4zPJgXK35pymF+YlnS0zuqZbWwBHVB8IUvuhwkWr/aH3LB82avUO4M50SSxtULyn9gbBBJ3jeTJvQ+0Xw4OjwMOvxUMoAVIZJMUme8yx13d",
        t: "MTYxMDUxMDg2Mi42NDAwMDA=",
        m: "A4tdjCZ3/hBUfKGwxdU+w4vn/wf1ZG11DVODkdaTamU=",
        i1: "w9T7opd7eQnOP/sTtor/iQ==",
        i2: "cvf7yd55tp8Fw34aaVALdQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "BLyMxANlMIZ85MRHgh129MQstVTvCaIMcSLyW3n3KNI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.akvideo.stream</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=a1a490b259ab7ba945fb5c686895f1163098533e-1610510862-0-AaLsWU2DR66VWENnV7Xsm57RJpycvKlSJDBa1j4vsEQWSbI7zlyrfd0XHYdht0TyQkssEOOS2IYxM0oP9CvTf8gMJDr5f5xM1tYHzDTZ1_6ddU_cSPS4M02_TjZMZRPoTShF4Wtmmt7kddRu0tYn_uBEW3sIfx-QxBhu2yBaTHK52NYAO9yeeTz-FUA4NoD0LVcEIACjlXf_R5gomi_ZWdfKdvsmZGN27ZpAWbCzN3NGqb65GZ1SVpFE2bwyWV2H7AgCmwE0C4xdRpMq0S3RjynK78WIWIUWjGC4BQ6DKGkeoNYRcxpgGmD2Xst5AZYjpU7yvD8y3f17CR-HskUcMHzHXO1Z-oXTntnudUZRNrgNrYMbXuBqHLvVqIe-E0zqmuLzyKzsIM1AdG5vRZXRq70c6s5FyM_RZqmG4kodqhNzfjz9MAhbAEEc5w-ewc2g6SpsKOBXMpmi9gi3XwwxEBFNz5GPSnO2MW3edUf-koNU03PkRgHYgI7xixCm1R7nxPm6ytIF_6ndWEUoCIitfXg" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="85223306daaf922b3bdc96cccbddf9b90d23631a-1610510862-0-AQ5zxS7XLXsj0ZXPP/nZu0Jtcj1kYTe4DITaSyyUXU/3DQz8f80A6I9ah2elyMY6Uwt0pTvzQ2X2PzfZT/fGeGZdz4PMBUN38FqE+CQyNBZmCz9PO0fG8VAlY5NcHaNJ1O9aNLPNL7KfBa9AVi3neO3khpipPjC85SsKmHLJU5ZU9QGQv9ZHhBxZr2FqOCJnm9g5H0qKo9AhXPvqVYy2FtY2hNbX7qAe6bI7/tv19qACIMVBU2leYzJQ/+8wFv1hYXU36CWB17YoHInGFSj/DsxYZ7K2aDY5hmYo+sFN0RG1T4HC7dG6x10AYQYlPtGAZmgZ9aU2X9VNNixh01HsZsVB9csr4gsUNRMvu3DIjb8IuJ+FPfGPrueUv+Wlduk1850nGtEWaTOOfaKW5oIe/xp9+U/zz5koFArsfXR2jrbIcK5s9ezeLEHIaSDhJNgDaIugWywynC7iArRES0oV2tbrtGb2eRrqkyBJ9pz5TR43Vy8gXqDiO5cNx1Qa8s2pCbgZE8Ncfzloj7h3/7AHVe55UL9clbjFTPkEclGPHvrH9ZK+IblOFZ7mv+QAPppmyxqAeDab//UKsxMFdosrWWzOivr/tJ/toeqzBxyY+lTYffnF4pC0uB2ZkL+lS8URE9N42K9hBpiF7qJFzVx+UbiCuYWriGwK1MH2i7Lon519GkaZi5lMRNTzuS6iw35Lv108waClqUfZ51psXT5VUrXInE48e3rb9a0poQNhS3QvXIkl4DGdVGXR5vsMyr/EjZFTRBsus3dlbGImCcGTJLijTOGdVW3G76rpudS4o5NA4XHVPltMYxKl0Zsl/wgtESoJK9v8+nyfQ80oNOzUDwoIu1mr7UgoqqLCrkJSP0WTlidqs0SXDIhBvKYLV7+W1dyBqcfc9LoKKArzd3jbZA02QNe0lAmki8y+bkK2P9ibAsv7+Zzv105VQU97M1fVkLpNVTMWN/1eszhBjMWjNFQpNnRAQeihbLEsseDVm0aZGuyacOKeZ6JKXc7b9K/S9VE84byQEadjyQg4pcd6jESvbHICt1SimtIIKng16LaCNXFyrfYIlM96MGPNpgCMFKuzu/dKYU5fUFhV/YKzO9Pm9v8o98I4arYdKy33RMztN3x98JBA+GyYG5GqdwmXwIlN4adA82COwx7rsDS1xMoQgurIldLA2iNPpzeu2Wp4h5VkRr2B1/kPVLzW8eAkqGklVyDq6aFteSbeHdDz1ANfHT5fk+MAr/b79CVoGKuhtinRwfpG5ouWFeYUrlmAr8gVzy3Y8pg+vPh3EpZ3yEBT9FW987vVBy2p4krmu/HD"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="b87177c0d647a0a851e803ad5b4f92f2"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610c407b7da3dcfa')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<a href="https://derchris.net/fungoidintensity.php?lang=0"><!-- table --></a>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610c407b7da3dcfa</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
