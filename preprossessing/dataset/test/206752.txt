<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta content="IE=Edge" http-equiv="X-UA-Compatible"/>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
         </script>
<script src="script/js/timer.js" type="text/javascript"></script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="" name="Description"/>
<meta content="" name="Keywords"/>
<meta content="Zdeněk Hanzlíček, Jan Štěpař" name="author"/>
<meta content="index,follow" name="robots"/>
<meta content="index,follow,snippet,archive" name="googlebot"/>
<link href="../template/zlatastezka/styl.css" rel="stylesheet" type="text/css"/>
<script src="script/js/mootools.js" type="text/javascript"></script>
<script src="script/js/menumatic.js" type="text/javascript"></script>
<script src="script/js/MooDropMenu.js" type="text/javascript"></script>
<script src="script/js/domready.js" type="text/javascript"></script>
<link href="../template/zlatastezka/MooDropMenu.css" media="screen" rel="stylesheet" type="text/css"/>
<!-- Base -->
<script src="script/XtLightbox/Source/XtLightbox.js" type="text/javascript"></script>
<!-- Adaptors -->
<script src="script/XtLightbox/Source/Adaptor.js" type="text/javascript"></script>
<script src="script/XtLightbox/Source/Adaptor/Image.js" type="text/javascript"></script>
<!-- Renderers -->
<script src="script/XtLightbox/Source/Renderer.js" type="text/javascript"></script>
<script src="script/XtLightbox/Source/Renderer/Lightbox.js" type="text/javascript"></script>
<!-- Renderer styles -->
<link href="script/XtLightbox/Source/Renderer/Lightbox/style.css" rel="stylesheet" type="text/css"/>
<!--[if IE]>
     <link rel="stylesheet" href="template/zlatastezka/ie_styl.css" type="text/css"> 
  <![endif]-->
<!--[if lt IE 7]>
			<link rel="stylesheet" href="template/zlatastezka/MenuMatic-ie6.css" type="text/css" media="screen" charset="utf-8" />
	<![endif]-->
<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="favicon.ico" rel="bookmark icon" type="image/x-icon"/>
<title>Běh Zlatá stezka</title>
</head>
<script>
function testScroll(ev){
  var pozice = window.pageYOffset;
  //alert(pozice);
  if(pozice > 100){
    document.getElementById("topMenu").id = "topMenu_slide";
  }else{
    document.getElementById("topMenu_slide").id = "topMenu";
  }
  

}
window.onscroll=testScroll;
</script>
<body onload="startTimer() ">
<!-- <div id="login_lock_bg"> <a href="#" id="close_login_bg" class="login_lock_bg_a"></a> </div> -->
<div class="container">
<div id="top_logo">
<a href="https://behzlatastezka.cz"><img alt="logo" class="noborder" src="/template/zlatastezka/image/logo.png"/></a>
</div>
<div id="topMenu">
<ul id="nav2">
<li> <a href="https://behzlatastezka.cz/kontakt.html"> Kontakt</a></li> <li> <a href="https://behzlatastezka.cz/popis_trati.html"> POPIS TRATI / Streckenbeschreibung  </a></li><li><a href="#">Galerie</a><ul><li><a href="https://behzlatastezka.cz/15._rocnik_2019.html">15. Ročník 2019</a></li><li><a href="https://behzlatastezka.cz/14._rocnik_2018.html">14. Ročník 2018</a></li><li><a href="https://behzlatastezka.cz/13._rocnik_2017.html">13. Ročník 2017</a></li><li><a href="https://behzlatastezka.cz/fotky_z_12._rocniku.html">12.ročník 2016</a></li><li><a href="https://behzlatastezka.cz/11._rocnik_2015.html">11. Ročník 2015</a></li><li><a href="https://behzlatastezka.cz/stozec_2016.html">Stožec 2016</a></li></ul></li><li><a href="#">Informace / Informationen  </a><ul><li><a href="https://behzlatastezka.cz/zlata_stezka.html">Zlatá stezka</a></li><li><a href="https://behzlatastezka.cz/zprava_z_15._rocniku.html">Zpráva z 15. ročníku</a></li><li><a href="https://behzlatastezka.cz/14._rocnik_2018.html">Zpráva ze 14. ročníku</a></li><li><a href="https://behzlatastezka.cz/zprava_ze_13._rocniku.html">Zpráva ze 13. ročníku</a></li><li><a href="https://behzlatastezka.cz/zprava_ze_12._rocniku.html">Zpráva ze 12. ročníku</a></li></ul></li><li><a href="#">Výsledky / Ergebnisse </a><ul><li><a href="https://behzlatastezka.cz/vysledkova_listina_2019.html">Výsledková listina 2019</a></li><li><a href="https://behzlatastezka.cz/vysledkova_listina_2018.html">Výsledková listina 2018</a></li><li><a href="https://behzlatastezka.cz/vysledkova_listina_2017.html">Výsledková listina 2017</a></li><li><a href="https://behzlatastezka.cz/vysledkova_listina_2016.html">Výsledková listina 2016</a></li><li><a href="https://behzlatastezka.cz/vysledkova_listina_2015.html">Výsledková listina 2015</a></li><li><a href="https://behzlatastezka.cz/vysledkova_listina_2014.html">Výsledková listina 2014</a></li></ul></li>
<div class="clear"></div>
</ul>
</div>
<div class="clear"></div>
<div id="slider">
<div class="slide_logo_sm">
<img src="/template/zlatastezka/image/logo_gold.png"/>
</div>
<div class="slide_text_1_sm">
            BĚŽECKÝ ZÁVOD
          </div>
<div class="slide_text_2_sm">
            ZLATÁ STEZKA
          </div>
<img src="/template/zlatastezka/image/homepage_header_small.jpg"/>
</div>
<div class="page_content"></div>
<div class="propozice_zavodu">
</div>
<div id="footer">
<div class="footer_center">
<div id="paticka_obsah">
<div class="paticka_nadpis">Kontakt</div>
            Jan Tuláček<br/>
            +420 603 515 819 <br/>
            veteran@tulacek.cz <br/>
            obec Stožec, okres Prachatice<br/>
            48.8589236N, 13.8199772E<br/>
</div>
<div id="paticka_obsah">
<div class="paticka_nadpis">Napište Nám</div>
<form action="" method="POST">
<table border="0">
<tr>
<td>
<input class="contact_email" name="contact_email" placeholder="E-mail" type="text" value=""/>
</td>
<td>
<input class="contact_email" name="predmet_email" placeholder="Předmět" type="text" value=""/>
</td>
</tr>
<tr>
<td colspan="2">
<textarea class="contact_email" cols="45" name="dotaz_email" placeholder="Váše otázka..."></textarea>
</td>
</tr>
<tr>
<td>
                Kolik je:
                8 plus 8              </td>
<td>
<input class="contact_email" name="overeni_email" placeholder="Výsledek" type="text"/>
<input name="overeni_vysledek" type="hidden" value="16"/>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input class="email_btn" name="odeslat_dotaz" type="submit" value="Odeslat"/>
</td>
</tr>
</table>
</form>
</div>
<div class="clear"></div>
</div>
<div class="paticka_cop">Copyright © Servis 1001.cz 2021</div>
</div>
</div>
</body></html>