<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title>Alatassi-Group</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/accordian-slide.js" type="text/javascript"></script>
<style type="text/css">
.style3 {
	text-align: justify;
}
.style2 {
	list-style-type: square;
	direction: ltr;
}
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #2B2B2B;
}
</style>
</head>
<body style="background-color: #138CBB; background-image: url('')">
<div class="main">
<div class="header">
<div class="block_header">
<div class="logo">
<img alt="" height="95" src="images/logo1.jpg" width="290"/></div>
<div class="menu">
<ul class="style2" style="height: 0px; width: 456px; z-index: 1; position: absolute; top: 41px; left: 710px;">
<li class="style1" style="width: 54px">
<a class="FBG" href="index.html" style="width: 0px">Home</a></li>
<li style="width: 66px; height: 59px">
<a href="About.html" style="width: 49px">About us</a>     </li>
<li style="width: 57px"><a href="trading.html" style="width: 11px">
			Trading</a></li>
<li style="width: 92px"><a href="Manf.html" style="width: 72px">
			Manufacturing</a></li>
<li style="width: 67px"><a href="serv.html">Services</a></li>
<li style="width: 75px; height: 59px">
<a href="contact.html" style="width: 59px">Contact us</a>     </li>
</ul>
</div>
<div class="clr"></div>
<div class="header_text2">
<div class="header_text">
<div id="index_portfolio">
<div class="itemidx"> <a href="#"></a><img alt="screen 2" height="313" src="images/simple_img_1.jpg" width="680"/></div>
<div class="itemidx"> <a href="#"></a><img alt="screen 2" height="313" src="images/simple_img_2.jpg" width="680"/></div>
<div class="itemidx"> <a href="#"></a><img alt="screen 2" height="313" src="images/simple_img_3.jpg" width="680"/></div>
<div class="itemidx"> <a href="#"></a><img alt="screen 2" height="313" src="images/simple_img_4.jpg" width="680"/></div>
<div class="itemidx"> <a href="#"></a><img alt="screen 2" height="313" src="images/simple_img_5.jpg" width="680"/></div>
</div>
<div class="clr"></div>
</div>
</div>
<div class="clr"></div>
</div>
</div>
<div class="clr"></div>
<div class="body">
<div class="body_resize">
<div class="block_body">
<h2>Trading<br/>
</h2>
<p class="style3">
		Our company is specialized in the field  of air conditioning and 
		refrigeration part and equipment. Its main business is to supply the 
		Syrian market with commercial and industrials refrigeration equipments, 
		which is used in cold and freezer room for storing fruit, vegetable, 
		meat, chicken â¦ etc.
		<font face="Tahoma, Times New Roman, Simplified Arabic Fixed" size="2">
		 </font></p>
<a href="trading.html"><img alt="picture" border="0" height="31" src="images/read_more.gif" width="87"/></a>
<div class="clr"></div>
</div>
<div class="block_body">
<h2>Manufacturing<br/>
</h2>
<p class="style3" style="height: 131px">Alatassi manufactures heat exchanger coils for 
		commercial and industrial applications where heat transfer is required 
		between air and primary fluid such as : refrigreants-water-steam 
		....etc.                                                                </p>
<a href="#"><img alt="picture" border="0" height="31" src="images/read_more.gif" width="87"/></a>
<div class="clr"></div>
</div>
<div class="block_body">
<h2>Services<br/>
</h2>
<p class="style3" style="height: 150px">Another principle job is to 
		follow up customer and solve any problems that may affect their projects 
		(after sales service). This department is considered as the line in 
		customer support and is followed by the management and studying 
		departments that specialized in the technical fields.</p>
<a href="#"><img alt="picture" border="0" height="31" src="images/read_more.gif" width="87"/></a>
<div class="clr"></div>
</div>
<div class="clr"></div>
</div>
<div class="clr"></div>
</div>
<div class="FBG">
<div class="FBG_resize">
<div>
<div class="RecentIE">
<h2>News updates</h2>
<img alt="picture" class="news" height="87" src="images/g_317.jpg" width="90"/>
<p>Made in Syria Expo in Sudan   <strong>..</strong><br/>
            26-10-2010 - Khartoum  </p>
<p> <a href="#">Read More </a></p>
<div class="clr"></div>
<img alt="picture" class="news" height="94" src="images/thumb.jpg" width="92"/>
<p>The 3rd International Trade Exhibition for Construction Technology, 
			Building Materials and Equipment <strong>...</strong><br/>
            27-09-2010 - Erbil<a href="#">Read More </a></p>
<div class="clr"></div>
</div>
<div class="Recent">
<h2>What Are We Doing Now !!!</h2>
<p> </p>
<ul>
<li>Monoprix in Aleppo.</li>
<li>Frozen Frensh Fries factory in Adra</li>
<li></li>
<li></li>
<li></li>
</ul>
<div class="clr"></div>
</div>
<div class="Recent2">
<h2>Get in touch</h2>
<p><strong>Address</strong>: Damascus Syria                               <strong>Telephone</strong>: 
			+963 11 88 11 178<br/>
<strong>FAX</strong>: +963 11 88 88 925                                                        <br/>
<strong>Factory : +</strong>963 11 58 13 777<strong>                                              E-mail</strong>:           
			info@alatassi-group.com</p>
</div>
<div class="clr"></div>
</div>
</div>
<div class="clr"></div>
</div>
</div>
<div class="footer">
<div class="footer_resize">
<p class="leftt">Â© Copyright Alatassi-Group. All Rights Reserved<br/>
</p>
<div class="clr"></div>
</div>
</div>
</body>
</html>