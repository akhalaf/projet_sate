<!DOCTYPE html>
<html en-us="" lang="lang=" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="UTF-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="Axact Inc." name="Author"/>
<meta content="axact pakistan,axact karachi,axact company, axact it company, axact solutions, top it companies in pakistan, top companies in pakistan, top 50 companies in Pakistan" name="keywords"/>
<link href="https://www.axact.com/wp-content/themes/axact/assets/images/favicon.ico" rel="shortcut icon"/>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet"/>
<link href="https://www.axact.com/wp-content/themes/axact/assets/css/main-min-final.css" rel="stylesheet" type="text/css"/>
<link href="https://www.axact.com/wp-content/themes/axact/assets/css/main-careers.css" rel="stylesheet" type="text/css"/>
<link href="https://www.axact.com/wp-content/themes/axact/assets/css/home-trans-final.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.axact.com/wp-content/themes/axact/style.css" media="all" rel="stylesheet" type="text/css"/>
<title>Home - Axact - World's Leading IT Company</title>
<meta content="Axact is a mission-driven, fast-growing, technology-oriented organization with global operations and market leadership in associated markets" name="description"/>
<meta content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://www.axact.com/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Home - Axact - World's Leading IT Company" property="og:title"/>
<meta content="Axact is a mission-driven, fast-growing, technology-oriented organization with global operations and market leadership in associated markets" property="og:description"/>
<meta content="http://www.axact.com/" property="og:url"/>
<meta content="Axact" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Axact is a mission-driven, fast-growing, technology-oriented organization with global operations and market leadership in associated markets" name="twitter:description"/>
<meta content="Home - Axact - World's Leading IT Company" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"http://www.axact.com/#website","url":"http://www.axact.com/","name":"Axact","inLanguage":"en-US","description":"World&#039;s Leading IT Company","potentialAction":{"@type":"SearchAction","target":"http://www.axact.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"http://www.axact.com/#webpage","url":"http://www.axact.com/","name":"Home - Axact - World&#039;s Leading IT Company","isPartOf":{"@id":"http://www.axact.com/#website"},"inLanguage":"en-US","datePublished":"2020-02-19T11:52:29+00:00","dateModified":"2020-02-20T09:09:24+00:00","description":"Axact is a mission-driven, fast-growing, technology-oriented organization with global operations and market leadership in associated markets"}]}</script>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.axact.com/feed/" rel="alternate" title="Axact » Feed" type="application/rss+xml"/>
<link href="https://www.axact.com/comments/feed/" rel="alternate" title="Axact » Comments Feed" type="application/rss+xml"/>
<link href="https://www.axact.com/home/feed/" rel="alternate" title="Axact » Home Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.axact.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.axact.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.axact.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<style id="contact-form-7-inline-css" type="text/css">
.wpcf7 .wpcf7-recaptcha iframe {margin-bottom: 0;}.wpcf7 .wpcf7-recaptcha[data-align="center"] > div {margin: 0 auto;}.wpcf7 .wpcf7-recaptcha[data-align="right"] > div {margin: 0 0 0 auto;}
</style>
<link href="https://www.axact.com/wp-content/plugins/wp-job-openings/assets/css/general.min.css?ver=1.6.2" id="awsm-jobs-general-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.axact.com/wp-content/plugins/wp-job-openings/assets/css/style.min.css?ver=1.6.2" id="awsm-jobs-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.axact.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.axact.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.axact.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.axact.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.axact.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://www.axact.com/" rel="shortlink"/>
<link href="https://www.axact.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.axact.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.axact.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.axact.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<link href="https://www.axact.com/wp-content/uploads/2020/03/favicon.png" rel="icon" sizes="32x32"/>
<link href="https://www.axact.com/wp-content/uploads/2020/03/favicon.png" rel="icon" sizes="192x192"/>
<link href="https://www.axact.com/wp-content/uploads/2020/03/favicon.png" rel="apple-touch-icon-precomposed"/>
<meta content="http://www.axact.com/wp-content/uploads/2020/03/favicon.png" name="msapplication-TileImage"/>
</head>
<body class="bg-white homePg">
<noscript>
<div id="jqcheck"><img alt="Javascript is disabled" height="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB60lEQVQ4T2NkwAHePzrxf3ebL1jWp/0oA5egGiM2pVgFQQq31uj/N/ANZvj+8T3D7aNHGDwbTxNvwKtbO/9f3dLHYJ+axfDn5w+GI/NnMRhFtTEISJtjGIIh8Pv39/87ak0ZzCLiGMRUNMCufnLxDMOlrZsY3JtOMrCwsKPowTDg3tGZ/59f2sVgFRvPkO+bAzZgwsZJDEcXzWNQtIlikDGIwG3Az+9v/+9qsGOwTc1h4JeQhhswcfMUhrcP7zEcXzyXwb3xMAMbuwDcEBTTzi7P/s/M8IFB3zccbDPMBSADQODs2sUMzFwyDIah/ZgGfHt/7/+BvmAGm+RsBl4RMawGfHr5jOHowlkMjiUbGDj55MCGwE060Of1X0RZi0Hb2Q4e3eguAElc2X2A4e2DmwwOhVsRBnx6cfH/yXm5DFZxyQxcAoJ4Dfj24T3DsUVzGcwSJjLwSxkygk3ZVmv4X805gkHZRBNXwkQRv3/+NsP1nUsYvFvOMzI+PLXo/73DSxgsouIYOHj5UBRi8wJIwY8vnxlOLV/CIGcewsC4vkDhv01yLoOIoiqG7bgMACn88Owxw8HpvQyMGwqV/vs19TMwQnxDEthYW8DAeGCC3/9XN46TpBGmWEzDkoHx06dP/z9//kyWAby8vAwAcza2SBMOSCMAAAAASUVORK5CYII=" width="16"/> Javascript is disabled. Please enable it for better working experience.</div>
</noscript>
<div class="wrapper">
<div class="header row-fluid">
<header class="header-top">
<h1 class="logo span2"> <a href="/" title=""> </a></h1>
<div class="span10">
<div class="cta-top">
<ul>
<li class="no-brdr">Need Help, Call  &gt;  <span>(021) 37-132-928</span></li>
</ul>
</div>
<nav>
<div class="topsecnav">
<ul class="mainnav" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64" id="menu-item-64"><a href="https://www.axact.com/products/">Products</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63" id="menu-item-63"><a href="https://www.axact.com/business-units/">Business Units</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62" id="menu-item-62"><a href="https://www.axact.com/platforms/">Platforms</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61" id="menu-item-61"><a href="https://www.axact.com/aboutus/">About Us</a></li>
</ul> <ul class="secnav" id="menu-careers"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68" id="menu-item-68"><a href="https://www.axact.com/careers/">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67" id="menu-item-67"><a href="https://www.axact.com/partners/">Partners</a></li>
</ul> </div>
</nav>
</div>
</header></div>
<div class="wrapper posRel saniha">
<div class="story-slider clearfix" onclick="window.open('http://www.bolnetwork.com/shoaib-shaikh-speech-teammeet/','mywindow');">
<a href="http://www.bolnetwork.com/shoaib-shaikh-speech-teammeet/" target="_blank" title="">
<img alt="*" src="https://www.axact.com/wp-content/themes/axact/assets/images/ticker-ceo.jpg"/>
</a>
<span class="showvid">Learn how Shoaib Ahmed Shaikh will make Pakistan the #1 Country in the world by 2036</span>
</div>
</div>
<div class="sliderContainer fullWidth clearfix">
<div class="royalSlider heroSlider rsMinW" data-rsdelay="10000" id="full-width-slider">
<div class="rsContent">
<a href="http://www.aikallahkaafihay.org" target="_blank" title=""><img alt="*" class="rsImg" height="565" src="https://www.axact.com/wp-content/themes/axact/assets/images/banners/aakh-banner.jpg" width="1366"/></a>
</div>
<div class="rsContent">
<a href="javascript:;" title=""><img alt="*" class="rsImg" height="565" src="https://www.axact.com/wp-content/themes/axact/assets/images/banners/banner-ceo-message.jpg" width="1366"/></a>
</div>
<div class="rsContent">
<a class="openframe-new" href="https://player.vimeo.com/video/92325751" target="_blank" title=""><img alt="*" class="rsImg" height="615" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/banner.jpg" width="1351"/></a>
</div>
<div class="rsContent">
<a href="http://www.bolnetwork.com/" target="_blank" title=""><img alt="*" class="rsImg" height="659" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/media-enterprise.jpg" width="1450"/></a>
</div>
<div class="rsContent">
<a href="/products/" title=""><img alt="*" class="rsImg" height="660" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/08.jpg" width="1450"/></a>
</div>
<div class="rsContent">
<img alt="*" class="rsImg" height="659" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/01.jpg" width="1450"/>
</div>
<div class="rsContent">
<img alt="*" class="rsImg" height="659" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/02.jpg" width="1450"/>
</div>
<div class="rsContent">
<img alt="*" class="rsImg" height="659" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/03.jpg" width="1450"/>
</div>
<div class="rsContent">
<img alt="*" class="rsImg" height="659" src="https://www.axact.com/wp-content/themes/axact/assets/images/slider/04.jpg" width="1450"/>
</div>
</div>
</div>
<div class="row-fluid" style="margin-top: 40px">
<div class="wrapper outlines">
<article class="wrap-container axact-vision" data-img="url(/wp-content/themes/axact/assets/img/axact-vision.png)">
<section class="rightside">
<h3 class="hd2">Axact’s vision for the<br/> future of Pakistan</h3>
<p class="fa16">Mr. Shoaib Ahmed Shaikh revealed Axact’s<br/> Plan 2019 for a prosperous and a thriving Pakistan,<br/> and the role Axact, BOL, our government, other<br/> IT firms and each and every Pakistani will play in the<br/> realization of this dream. Watch Mr. Shoaib Ahmed<br/> Shaikh’s captivating speech to learn how Axact<br/> will make sectors of Food and Shelter,<br/> Healthcare, Judicial Assistance and Education<br/> accessible to every Pakistani by 2019.</p>
<p><a class="openframe" href="https://player.vimeo.com/video/92325751" title="">Watch the video</a></p>
</section>
</article>
<article class="wrap-container nobdr teammeetbg corporateEvent" data-img="url(/wp-content/themes/axact/assets/img/framesmerge.jpg)" id="corp-event">
<a class="scroll-to-top addmar" href="javascript:;" title=" ">Back to top</a>
<section class="leftside">
<h3 class="hd2">Pakistan’s Grandest Corporate Event</h3>
<p class="fa16"> <strong>20th Annual Team Meet</strong></p>
<p class="fa16">‘Team Meet’- the pinnacle of all recognitions, is the Grandest Corporate Event in Pakistan. It is a tradition that has rejoiced the achievements of Axactians &amp; Bolwalas. The Team Meet event is a hallmark of 20 years of meticulous planning, flawless execution and an aspiration to celebrate Axactians &amp; Bolwalas efforts and accomplishments. </p>
<p><a href="javascript:;" title="">Read more</a></p>
</section>
</article>
<br/><br/>
<div class="sep-x margin-auto"></div>
<article class="wrap-container nobdr freeinsaafbg" data-img="url(/wp-content/themes/axact/assets/img/freeinsaaf-img-trans.jpg)">
<span class="pak-one"></span>
<span class="pak-two"></span>
<span class="pak-three"></span>
<span class="pak-four"></span>
<span class="pak-five"></span>
<span class="pak-six"></span>
<a class="scroll-to-top addmar" href="javascript:;" title=" ">Back to top</a>
<section class="rightside">
<h3 class="hd2">Judicial Assistance<br/> Launched in Sindh</h3>
<p class="fa16"><strong>Providing Justice Free of Cost</strong> <br/> <br/> Judicial Assistance, a project of AAKH Trust is aimed to provide free justice to every individual free of cost. This initiative has been taken to make justice accessible to the common man and speed up the legal procedures through automated system. Judicial Assistance has launched its pilot project for Sindh region in Phase 1 and soon this initiative would be extended to entire Pakistan making justice accessible to all!</p>
<br/><a href="javascript:;" title="">Read more</a>
</section>
</article>
<div class="sep-x"></div>
<article class="wrap-container galaxact grandAxact clearfix" data-img="url(/wp-content/themes/axact/assets/img/galaxact.jpg)">
<a class="scroll-to-top addmar" href="javascript:;" title=" ">Back to top</a>
<section class="innercontent">
<span id="galfade"></span>
<h3 class="hd2">3.5 Million Sq ft. to accommodate 20,000<br/> Axactians in one shift</h3>
<p class="fa16"><strong>Axact is expanding grand with Galaxact</strong> <br/> <br/> Axact aims to increase Pakistan’s IT exports by $50 billion, by expanding its workforce to 100,000. To respond to this increasing number of workforce, Axact planned and initiated massive projects of infrastructure developments as well as expansions. Engineering an icon! Gal Axact. </p>
<br/><a class="readMore" href="javascript:;" title="">Read more</a>
</section>
</article>
<article class="wrap-container news-section"> <a class="scroll-to-top" href="javascript:;" title="">Back to top</a>
<section class="newssection">
<table class="tdTable870">
<tr>
<td class="AlignLeftV">
<div class="newsboxes bigone" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)">
<strong>Axact develops ERP for DHA free of cost</strong>
<p>The ERP system was formally handed over to DHA at a short ceremony held at DHA’s head office. Present for this momentous occasion …</p>
</div>
</td>
<td class="tdWitdh210">
<div class="newsboxes">
<div class="innbox topuni" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)"></div>
<strong>Top Universities Visited Axact</strong>
<p>Even this year, students from all the major institutions were given the opportunity to get to know Axact as part of Human Resource …</p>
</div>
</td>
<td class="tdWitdh210">
<div class="newsboxes">
<div class="innbox lifestyle" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)"></div>
<strong>Lifestyle at your fingertips</strong>
<p>Working at Axact is not the same as working at any other company in Pakistan. We are different and we do things differently – the Axact …</p>
</div>
</td>
</tr>
</table> <br/> <br/>
<table class="sectbl tdTable870">
<tr>
<td class="tdWitdh210">
<div class="newsboxes">
<div class="innbox x1000" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)"></div>
<strong>1000% growth – We are close to our target</strong>
<p>With excitement and a sense of accomplishment of getting closer to the mark, each month X1000 is held to recognize and …</p>
</div>
</td>
<td class="tdWitdh210">
<div class="newsboxes">
<div class="innbox activities" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)"></div>
<strong>Experience Axact’s Recreational Activities</strong>
<p>Work-life balance is at the core of Axact’s value system. Every quarter departmental picnics are organized. This provides team building …</p>
</div>
</td>
<td class="AlignLeftV">
<div class="newsboxes bigtwo" data-img="url(/wp-content/themes/axact/assets/img/sprites-boxes.jpg)">
<strong>Message from Chairman &amp; CEO</strong>
<p>Axact’s Chairman and CEO Mr. Shoaib Ahmed Shaikh held a huge audience mesmerized as he spoke about Axact’s dream for a prosperous and a thriving…</p>
</div>
</td>
</tr>
</table>
</section>
</article>
</div>
</div>
<div id="footer">
<div class="footer-bottom">
<div class="wrapper">
<div class="clearfix footerWrap">
<a class="back-to-top fttopimg" href="javascript:;" title=""></a>
<div class="deep-footer">
<ul class="" id="menu-footer"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="https://www.axact.com/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-70" id="menu-item-70"><a href="https://www.axact.com/privacy-policy/">Privacy Policy</a></li>
</ul>
<span class="copyright">Copyright © 1997-<span id="setYear"></span>, Axact, All rights reserved.</span>
</div>
<div class="social-area">
<a href="https://www.facebook.com/axactians/" target="_blank" title=""><span class="fbicon"></span></a>
<a href="https://twitter.com/axacttweets" target="_blank" title=""><span class="fbicon twitter"></span></a>
<a href="https://www.linkedin.com/company/axact/" target="_blank" title=""><span class="linkedicon"></span></a>
<a href="https://www.vimeo.com/axact" target="_blank" title=""><span class="fbicon vimeo"></span></a>
<a href="http://www.axactian.com/" target="_blank" title=""><span class="fbicon axactian"></span></a>
</div>
</div>
<div class="disclaimer">
<p><span>Disclaimer</span>The logo, name and graphics of Axact and its products and services are the trademarks of Axact. All other company names, brand names, trademarks and logos mentioned on this website are the property of their respective owners and do not constitute or imply endorsement, sponsorship or recommendation thereof by Axact and do not constitute or imply endorsement, sponsorship or recommendation of Axact by the respective trademark owner.</p>
</div>
</div>
</div>
</div>
<div class="footer-cracker"></div>
<div id="mask-saniha"></div>
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1333481860107601');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=1333481860107601&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','ny2rw');
twq('track','PageView');
</script>
<script src="https://www.axact.com/wp-content/themes/axact/assets/js/jquery.js"></script>
<script src="https://www.axact.com/wp-content/themes/axact/assets/js/allscripts.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
adroll_adv_id = "FPWFS7CS4FHHVAT7YE25UX";
adroll_pix_id = "2EEUGPZVMFA4NNLKGCIAEG";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
/* ]]> */
</script>
<script type="text/javascript">
/* <![CDATA[ */
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53237229-1', 'auto');
  ga('send', 'pageview');
/* ]]> */
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1017546593;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript">
</script>
<noscript>
<div class="displayInline">
<img alt="*" height="1" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1017546593/?value=0&amp;guid=ON&amp;script=0" width="1"/>
</div>
</noscript>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/www.axact.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.axact.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var awsmJobsPublic = {"ajaxurl":"http:\/\/www.axact.com\/wp-admin\/admin-ajax.php","is_tax_archive":"","is_search":"","job_id":"0","wp_max_upload_size":"838860800","i18n":{"loading_text":"Loading...","form_error_msg":{"general":"Error in submitting your application. Please try again later!","file_validation":"The file you have selected is too large."}}};
/* ]]> */
</script>
<script src="https://www.axact.com/wp-content/plugins/wp-job-openings/assets/js/script.min.js?ver=1.6.2" type="text/javascript"></script>
<script src="https://www.axact.com/wp-includes/js/comment-reply.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://www.axact.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</div></body></html>