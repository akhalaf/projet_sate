<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html dir="ltr" lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script src="/cdn-cgi/apps/head/Jq0nnS0vXM7N8LBHWLUh65M9EXo.js"></script><link href="/sites/default/files/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<meta content="Agir pour l'Environnement est une association nationale de mobilisation citoyenne en faveur de l'environnement." name="description"/>
<meta content="https://www.facebook.com/association.agirpourlenvironnement" property="article:publisher"/>
<meta content="http://www.agirpourlenvironnement.org/sites/default/files/logo.jpg" property="og:image"/>
<meta content="708341079, 1363794246" property="fb:admins"/>
<meta content="non_profit" property="og:type"/>
<meta content="Association de mobilisation citoyenne | Agir pour l'Environnement" property="og:title"/>
<meta content="http://www.agirpourlenvironnement.org/" property="og:url"/>
<meta content="Agir pour l'Environnement est une association nationale de mobilisation citoyenne en faveur de l'environnement." property="og:description"/>
<meta content="width=990" name="viewport"/>
<title>Agir pour l'Environnement | Association nationale de mobilisation citoyenne</title>
<link href="/modules/aggregator/aggregator.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/node/node.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/defaults.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/system/system-menus.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/modules/user/user.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/cck/theme/content-module.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/ctools/css/ctools.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/date/date.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/date/date_popup/themes/datepicker.1.7.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/date/date_popup/themes/jquery.timeentry.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/filefield/filefield.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/simplenews/simplenews.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/cck/modules/fieldgroup/fieldgroup.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/modules/contrib/views/css/views.css?w" media="all" rel="stylesheet" type="text/css"/>
<link href="/sites/all/themes/ape_theme/css/styler.css?w" media="all" rel="stylesheet" type="text/css"/>
<script src="/sites/all/modules/contrib/jquery_update/replace/jquery.min.js?w" type="text/javascript"></script>
<script src="/misc/drupal.js?w" type="text/javascript"></script>
<script src="/sites/default/files/languages/fr_0d01a77f59732d44415b9739a41dbe7a.js?w" type="text/javascript"></script>
<script src="/sites/all/modules/contrib/google_analytics/googleanalytics.js?w" type="text/javascript"></script>
<script src="/sites/all/modules/contrib/poormanscron/poormanscron.js?w" type="text/javascript"></script>
<script src="/sites/all/modules/ape_modules/ape/js/jquery.ape.sousMenuAgir.js?w" type="text/javascript"></script>
<script src="/sites/all/themes/ape_theme/lib/jquery.diaporama.min.js?w" type="text/javascript"></script>
<script src="/sites/all/themes/ape_theme/lib/modernizr-csstransition.js?w" type="text/javascript"></script>
<script src="/sites/all/themes/ape_theme/lib/jquery.barreDeProgression.min.js?w" type="text/javascript"></script>
<script src="/sites/all/themes/ape_theme/js/faireundon.barreDeProgression.js?w" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "googleanalytics": { "trackOutgoing": 1, "trackMailto": 1, "trackDownload": 1, "trackDownloadExtensions": "7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip|pdf" }, "cron": { "basePath": "/poormanscron", "runNext": 1610639896 } });
//--><!]]>
</script>
<script type="text/javascript"> </script>
<meta content="1512909781" property="twitter:account_id"/>
</head>
<body class="front not-logged-in page-accueil no-sidebars accueil">
<div id="fond">
<div class="background-interne" id="conteneur">
<div id="header">
<!-- Logo -->
<div id="logo"><a href="/" rel="home" title="Accueil"><img alt="Accueil" src="/sites/default/files/ape_theme_logo.png"/></a></div>
<!-- /logo -->
<!-- /description -->
<div id="desciption">
<!-- H1 -->
<h1 id="site-name">
<a href="/" rel="home" title="Accueil"><span>Agir pour l'Environnement</span></a>
</h1>
<!-- /H1 -->
<!-- Slogan -->
<div id="site-slogan">Une association de mobilisation citoyenne nationale en faveur de l'environnement.</div>
<!-- /slogan -->
<!-- Lien en dur : en savoir plus -->
<a href="association" id="video">L'association Agir pour l'Environnement</a>
<!-- /Lien en dur : en savoir plus -->
</div>
<!-- /description -->
<!-- header-top-right -->
<div class="clear-block block block-ape" id="don_progression_header">

  
  ﻿

  <div>
<p style="margin-top: 10px">
          Plus nous sommes nombreux, plus nous avons d’impact.
      </p>
<a class="button" href="https://bit.ly/adherer_header" style="border-bottom:0"><span>Adhérez maintenant</span></a>
</div>
<!--  <div id="objectif">-->
<!--    <div id="conteneur_barre" class="--><!--" style="background:none;">-->
<!--      <div class="barreDeProgression">-->
<!--        <div class="fond">&nbsp;</div>-->
<!--        <div class="barre">&nbsp;</div>-->
<!--        <div class="curseur">&nbsp;</div>-->
<!--      </div>-->
<!--      <p>--><!--</p>-->
<!--    </div>-->
<!--  </div>-->
<div class="clear"></div>
</div>
<!-- /header-top-right -->
</div>
<!-- navigation + searchbox -->
<div class="background-interne" id="navigation">
<ul class="links primary-links"><li class="menu-906 active-trail first active"><a class="active" href="/" title="Accueil du site d'Agir Pour l'Environnement">Accueil</a></li>
<li class="menu-3932"><a href="/presentation-de-l-association" title="Présentation de l'association">Association</a></li>
<li class="menu-10735"><a href="/blog/Campagnes" title="">Campagnes</a></li>
<li class="menu-5877"><a href="/actions" title="">Actions</a></li>
<li class="menu-909"><a href="/blog" title="Actualités d'Agir Pour l'Environnement">Blog</a></li>
<li class="menu-911"><a href="/communiques-presse" title="Vous êtes journaliste ? Venez lire nos communiqués de presse !">Presse</a></li>
<li class="menu-912 last"><a href="/contact" title="Des informations, des remarques... ? Contactez-nous !">Contact</a></li>
</ul>
<!-- Sous menu -->
<div class="clear-block block block-views" id="block-views-dernieres_campagnes-block_3">
<div aria-live="assertive" class="fleche_menu">
<div class="view view-dernieres-campagnes view-id-dernieres_campagnes view-display-id-block_3 view-dom-id-3">
<div class="view-header">
<a href="/blog/Campagnes">CAMPAGNES</a>
</div>
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="titre_sous_menu"><strong><a href="/blog/biodiversite-31173">BIODIVERSITE</a></strong></div>
</div>
<div class="views-row views-row-2 views-row-even">
<div class="titre_sous_menu"><strong><a href="/blog/energie-climat-31174">ENERGIE / CLIMAT</a></strong></div>
</div>
<div class="views-row views-row-3 views-row-odd">
<div class="titre_sous_menu"><strong><a href="/blog/nanos-et-dioxyde-de-titane-31111">NANOS ET DIOXYDE DE TITANE</a></strong></div>
</div>
<div class="views-row views-row-4 views-row-even">
<div class="titre_sous_menu"><strong><a href="/blog/pesticides-31175">PESTICIDES</a></strong></div>
</div>
<div class="views-row views-row-5 views-row-odd">
<div class="titre_sous_menu"><strong><a href="/blog/agriculture-bio-31162">AGRICULTURE / BIO</a></strong></div>
</div>
<div class="views-row views-row-6 views-row-even views-row-last">
<div class="titre_sous_menu"><strong><a href="/blog/plastique-31176">PLASTIQUE</a></strong></div>
</div>
</div>
<div class="view-footer">
<a class="button" href="/blog/Campagnes"><span>Toutes les campagnes</span></a> </div>
</div> </div>
</div>
<!-- /sous_menu -->
<div class="clear"></div><!-- /clear:both -->
</div>
<!-- /navigation -->
<div class="clear"></div><!-- /clear:both -->
<div id="contenu_1">
<h1 class="title" id="page-title">Actualité environnement en UNE</h1>
<div id="carroussel">
<div class="view view-FocusNews1 view-id-FocusNews1 view-display-id-page_2 view-dom-id-1">
<div class="view-content">
<div class="item-list">
<ol>
<li class="views-row views-row-1 views-row-odd views-row-first">
<h2 class="TitreUne"><a href="/communiques-presse/second-barometre-annuel-les-francais-et-l-environnement-31216">Second baromètre annuel « Les français et l'environnement »</a></h2>
<a class="imagecache imagecache-illustration_une_580_300 imagecache-linked imagecache-illustration_une_580_300_linked" href="/communiques-presse/second-barometre-annuel-les-francais-et-l-environnement-31216"><img alt="Visuel du second baromètre annuel &quot;Les français et l'environnement&quot;" class="imagecache imagecache-illustration_une_580_300" height="300" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/illustration_une_580_300/images/unes/facebook-02.jpg" title="" width="580"/></a></li>
<li class="views-row views-row-2 views-row-even">
<h2 class="TitreUne"><a href="/communiques-presse/zone-sans-pesticides-agir-pour-l-environnement-engage-un-recours-devant-le-31189">Zone sans pesticides : Agir pour l’Environnement engage un recours devant le Conseil d’Etat</a></h2>
<a class="imagecache imagecache-illustration_une_580_300 imagecache-linked imagecache-illustration_une_580_300_linked" href="/communiques-presse/zone-sans-pesticides-agir-pour-l-environnement-engage-un-recours-devant-le-31189"><img alt="Zone sans pesticides : Agir pour l’Environnement engage un recours devant le Con" class="imagecache imagecache-illustration_une_580_300" height="300" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/illustration_une_580_300/images/unes/collectifmaires-conseildetat.jpg" title="" width="580"/></a></li>
<li class="views-row views-row-3 views-row-odd views-row-last">
<h2 class="TitreUne"><a href="/communiques-presse/projet-de-ferme-usine-des-1069-vaches-dans-l-eure-avis-defavorable-du-comm-31179">Projet de ferme-usine des 1069 vaches dans l’Eure : AVIS DÉFAVORABLE DU COMMISSAIRE ENQUÊTEUR !</a></h2>
<a class="imagecache imagecache-illustration_une_580_300 imagecache-linked imagecache-illustration_une_580_300_linked" href="/communiques-presse/projet-de-ferme-usine-des-1069-vaches-dans-l-eure-avis-defavorable-du-comm-31179"><img alt="AVIS DÉFAVORABLE pour le Projet de ferme-usine des 1069 vaches dans l’Eure" class="imagecache imagecache-illustration_une_580_300" height="300" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/illustration_une_580_300/images/unes/victoire-1000vaches.jpg" title="" width="580"/></a></li>
</ol>
</div> </div>
</div> </div><!-- /carroussel -->
<div class="clear-block block block-ape" id="block-ape-ca_accueil">
<div id="courriel_action">
<ul>
<li id="courriel_action_1"><span><em>Se rassembler</em> pour agir efficacement</span></li>
<li id="courriel_action_2"><span><em>Participer</em> et soutenir des actions massives et coordonnées</span></li>
<li id="courriel_action_3"><span><em>Faire pression</em> sur les décideurs économiques et responsables politiques</span></li>
<li id="courriel_action_4"><span><em>Obtenir</em> des résultats rapides et concrêts.</span></li>
</ul>
</div>
<div id="recevoir_action">
<a href="courriel-action"><em>RECEVOIR</em> NOS ACTIONS</a>
</div>
</div>
</div>
<div class="clear"></div><!-- /clear:both -->
<div id="contenu_2">
<div class="clear-block block block-views" id="block-views-LastNews1-block_1">
<div class="view view-LastNews1 view-id-LastNews1 view-display-id-block_1 view-dom-id-4 derniere-actu">
<div class="view-header">
<div class="titre">
<h1>
<a href="/blog"><span class="cache">Blog </span>ACTUALITES ENVIRONNEMENT</a> </h1>
<ul>
<li>
<a class="icone_twitter_p" href="/suivre-actualites"><span class="offscreen">Twitter de l'association Agir poru l'Environnement</span></a> </li>
<li>
<a class="icone_facebook_p" href="/suivre-actualites"><span class="offscreen">Facebook de l'association Agir pour l'Environnement</span></a> </li>
<li>
<a class="icone_rss_p" href="/suivre-actualites"><span class="offscreen">Flux RSS de l'association Agir pour l'Environnement</span></a> </li>
<li>
<a class="icone_mail_p" href="/suivre-actualites"><span class="offscreen">Newsletter</span></a> </li>
</ul>
</div>
<div class="clear"></div> </div>
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field-field-illustration-fid">
<span class="field-content"><a class="imagecache imagecache-illustration_derniere_actu_280_160 imagecache-linked imagecache-illustration_derniere_actu_280_160_linked" href="/blog/stop-la-5g-31205"><img alt="" class="imagecache imagecache-illustration_derniere_actu_280_160" height="160" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/illustration_derniere_actu_280_160/images/actualites/stop5G_remisepetition_ministereeconomie_agirpourlenvironnement_20200626_ministere_ok.jpeg" title="" width="240"/></a></span>
</div>
<div class="views-field-title">
<span class="field-content"><h2><a href="/blog/stop-la-5g-31205">Stop à la 5G</a></h2></span>
</div>
<div class="views-field-created">
<span class="field-content">04/09/2020</span>
</div>
<div class="views-field-field-resume-value">
<div class="field-content">Fin 2019, le gouvernement a lancé les procédures d'attribution des fréquences 5G sans évaluation écologique et sanitaire et sans débat public préalable. Comme de mauvaises coutumes, les opérateurs de...</div>
</div>
</div>
</div>
<div class="view-footer">
<div class="clear"></div>
<div class="footer">
<a href="/blog">Toutes les actualités</a></div> </div>
</div>
</div>
<div class="clear-block block block-views" id="block-views-LastComPress1-block_1">
<div class="view view-LastComPress1 view-id-LastComPress1 view-display-id-block_1 view-dom-id-5">
<div class="view-header">
<div class="titre">
<h1>
<a href="/communiques-presse">COMMUNIQUÉS DE PRESSE <span class="cache"> Environnement </span></a>
</h1>
<ul>
<li><a class="icone_rss_p" href="https://feeds.feedburner.com/Agir_Pour_l_Environnement_Communiques_Presse"></a></li>
<li><a class="icone_mail_p" href="https://feedburner.google.com/fb/a/mailverify?uri=Agir_Pour_l_Environnement_Communiques_Presse&amp;loc=fr_FR"></a></li>
</ul>
</div> </div>
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field-title">
<span class="field-content"><h2><a href="/communiques-presse/5g-contre-l-avis-du-rapporteur-public-le-conseil-d-etat-rejette-le-recours-31229">5G : CONTRE L'AVIS DU RAPPORTEUR PUBLIC, LE CONSEIL D'ETAT REJETTE LE RECOURS DES ASSOCIATIONS</a></h2></span>
</div>
<div class="views-field-created">
<span class="field-content">05/01/2021</span>
</div>
</div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field-title">
<span class="field-content"><h2><a href="/communiques-presse/le-dioxyde-de-titane-restera-interdit-dans-nos-assiettes-mais-pas-dans-nos-31228">Le dioxyde de titane restera interdit dans nos assiettes mais pas dans nos dentifrices et médicaments : une demi-mesure inacceptable !</a></h2></span>
</div>
<div class="views-field-created">
<span class="field-content">23/12/2020</span>
</div>
</div>
<div class="views-row views-row-3 views-row-odd views-row-last">
<div class="views-field-title">
<span class="field-content"><h2><a href="/communiques-presse/solstice-d-hiver-sauvons-la-richesse-de-la-nuit-de-la-pollution-lumineuse-31227">Solstice d’hiver Sauvons la richesse de la Nuit de la pollution lumineuse</a></h2></span>
</div>
<div class="views-field-created">
<span class="field-content">21/12/2020</span>
</div>
</div>
</div>
<div class="view-footer">
<div class="footer">
<a href="communiques-presse">Tous les communiqués de presse</a>
</div> </div>
</div>
</div>
</div>
<div class="clear-block block block-ape" id="afin_dagir">
<ul>
<li>
<a href="courriel-action"><img alt="se rassembler" height="90" src="sites/all/themes/ape_theme/images/rassembler.png" width="115"/>Se rassembler</a>
<!--<div class="infobulle">
      <h4>Se rassembler</h4>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nec laoreet libero. Phasellus eros justo, rutrum sed ultricies aliquam, posuere eget leo. Mauris auctor orci ut eros accumsan eget aliquet tortor tristique.</p> 
    </div>-->
</li>
<li>
<a href="suivre-actualites"><img alt="suivre l'actualité" height="90" src="sites/all/themes/ape_theme/images/news.png" width="115"/>Suivre l'actualité</a></li>
<li>
<a href="actions"><img alt="Participer aux actions" height="90" src="sites/all/themes/ape_theme/images/action.png" width="115"/>Participer aux actions</a></li>
<li>
<a href="association"><img alt="Mieux nous connaitre" height="90" src="sites/all/themes/ape_theme/images/connaitre.png" width="115"/>Mieux nous connaitre</a></li>
<li>
<a href="http://faire-un-don.agirpourlenvironnement.org/"><img alt="Nous soutenir" height="90" src="sites/all/themes/ape_theme/images/soutenir.png" width="115"/>Nous soutenir</a></li>
</ul>
</div>
</div> <!-- /conteneur -->
<div class="clear"></div><!-- /clear:both -->
<div id="footer">
<div id="conteneur_footer">
<div class="col">
<div class="border" id="twitter">
<h4>Suivez-nous sur twitter</h4>
<p>
<a class="twitter-follow-button" data-lang="fr" data-show-count="false" href="https://twitter.com/APEnvironnement">Suivre @APEnvironnement</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</p>
</div>
<div class="border" id="facebook">
<h4>Rejoignez-nous sur Facebook</h4>
<!--
<p>
facebook<br />
<span>+ 30 000 fans</span>
</p>
<a href="http://www.facebook.com/association.agirpourlenvironnement">
</a>
-->
<div class="fond_facebook">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<fb:like-box border_color="#FFF" header="false" height="90" href="http://www.facebook.com/association.agirpourlenvironnement" show_faces="false" stream="false" width="300"></fb:like-box>
</div>
</div>
</div>
<div class="col">
<div class="clear-block block block-views" id="block-views-SitesAnnexe-block_1">
<div class="view view-SitesAnnexe view-id-SitesAnnexe view-display-id-block_1 view-dom-id-6">
<div class="view-header">
<h1 class="border">Agir pour l'environnement sur le web</h1>
</div>
<div class="view-content">
<div class="item-list">
<ul>
<li class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field-field-vignette-fid">
<span class="field-content"><a href="https://infonano.agirpourlenvironnement.org"><img alt="MAIS OÙ SE CACHENT LES NANOPARTICULES ?" class="imagecache imagecache-site_annexe_300_100 imagecache-default imagecache-site_annexe_300_100_default" height="100" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/site_annexe_300_100/sites_annexes/capture-d-ecran-2017-10-05-175700.png" title="" width="300"/></a></span>
</div>
<div class="views-field-title">
<span class="field-content"><h2><a href="https://infonano.agirpourlenvironnement.org">MAIS OÙ SE CACHENT LES NANOPARTICULES ?</a></h2></span>
</div>
<div class="views-field-body">
<div class="field-content"><p>La liste de plus de 300 produits contenant ou pouvant contenir des nanoparticules</p></div>
</div>
</li>
<li class="views-row views-row-2 views-row-even">
<div class="views-field-field-vignette-fid">
<span class="field-content"><a href="https://europacity-nonmerci.agirpourlenvironnement.org"><img alt="LES HYPERS, C’EST PAS SUPER !!!" class="imagecache imagecache-site_annexe_300_100 imagecache-default imagecache-site_annexe_300_100_default" height="100" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/site_annexe_300_100/sites_annexes/visuel-facebook1.jpg" title="" width="300"/></a></span>
</div>
<div class="views-field-title">
<span class="field-content"><h2><a href="https://europacity-nonmerci.agirpourlenvironnement.org">LES HYPERS, C’EST PAS SUPER !!!</a></h2></span>
</div>
<div class="views-field-body">
<div class="field-content"><p>NOS « TERRES RARES » SONT… AGRICOLES ! IL FAUT LES PROTÉGER !!!</p></div>
</div>
</li>
<li class="views-row views-row-3 views-row-odd views-row-last">
<div class="views-field-field-vignette-fid">
<span class="field-content"><a href="https://cantinesbio.agirpourlenvironnement.org"><img alt="Des cantines bio, j'en veux !" class="imagecache imagecache-site_annexe_300_100 imagecache-default imagecache-site_annexe_300_100_default" height="100" src="https://www.agirpourlenvironnement.org/sites/default/files/imagecache/site_annexe_300_100/sites_annexes/capture-d-ecran-2017-10-05-175405.png" title="" width="300"/></a></span>
</div>
<div class="views-field-title">
<span class="field-content"><h2><a href="https://cantinesbio.agirpourlenvironnement.org">Des cantines bio, j'en veux !</a></h2></span>
</div>
<div class="views-field-body">
<div class="field-content"><p>Interpellez plus de 50 000 établissements scolaires !</p></div>
</div>
</li>
</ul>
</div> </div>
</div>
</div>
</div>
<div class="col" id="menufooter">
<div class="clear-block block block-menu" id="block-menu-secondary-links">
<h2>Raccourcis</h2>
<ul class="menu"><li class="leaf first active-trail"><span><a class="active" href="/" title="">Accueil</a></span></li>
<li class="leaf"><span><a href="/courriel-action" title="">Recevoir nos actions</a></span></li>
<li class="leaf"><span><a href="/suivre-actualites" title="">Suivre nos actualités</a></span></li>
<li class="leaf"><span><a href="/node/133" title="">Recevoir les campagnes</a></span></li>
<li class="leaf last"><span><a href="http://faire-un-don.agirpourlenvironnement.org" title="">Faire un don</a></span></li>
</ul>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div id="mentions">
<div id="conteneur_mentions">
<div class="clear-block block block-ape" id="block-ape-menu_bas_page">
<p>
<a href="/plan-du-site">Plan du site</a> | <a href="/emplois">Emplois / Stages</a> | <a href="/missions-benevoles">Benevolat</a> | <a href="/mentions-legales">Mentions légales</a> | <a href="/credits">Crédits</a> | <a href="/contact">Contact</a></p>
</div>
<span>Agir Pour l'Environnement - 2 rue du Nord - 75018 Paris - Tel : 01.40.31.02.37</span> </div>
</div>
<div><!-- Fin id : #fond -->
<script type="text/javascript">
<!--//--><![CDATA[//><!--
/**
 * Initialisation du diaporama de la page d'accueil
 *
 * @author Benjamin MENANT job-contact@menant-benjamin.fr
 *
 */

Drupal.behaviors.ape_accueilDiaporama = function (context) {
  $('#carroussel ol').diaporama({
    'timeInterval'      : 4000,
    'transitionDuration': 500
  });
};

//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
/**
 * Initialisation du diaporama du footer
 *
 * @author Benjamin MENANT job-contact@menant-benjamin.fr
 *
 */

Drupal.behaviors.ape_footerDiaporama = function (context) {
  $('#block-views-SitesAnnexe-block_1 ul').diaporama({
    'timeInterval'      : 4000,
    'transitionDuration': 500
  });
};

//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-11782831-3"]);_gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");ga.type = "text/javascript";ga.async = true;ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
//--><!]]>
</script>
</div></div></body>
</html>
