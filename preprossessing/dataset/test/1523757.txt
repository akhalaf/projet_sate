<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="Hg2pPqND4Hpl6qQt4Mmd3ArVWv-bPVGLE9Kfq-S-Qy" name="globalsign-domain-verification"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Digital Agency Oman, Muscat -web design, development, marketing, SEO</title>
<meta content="UYG Oman - Highly Creative Digital Agency in Oman, Muscat offers affordable &amp; best web design, development, mobile application development, SEO, online &amp; internet marketing services" name="description"/>
<meta content="Digital agency Oman, internet marketing services Oman, website design Oman, website development Oman,internet marketing services, website design company, website design Oman" name="keywords"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<link href="css/animate.css" rel="stylesheet"/>
<style type="text/css">.wow:first-child {
	visibility: hidden;
}
	</style>
<!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    
    
  <![endif]-->
<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,700,900" rel="stylesheet" type="text/css"/><script src="js/jquery.parallax-1.1.3.js" type="text/javascript"></script><script src="js/jquery.localscroll-1.2.7-min.js" type="text/javascript"></script><script type="text/javascript">
$(document).ready(function(){
	$('#nav').localScroll(1200);
	
	//.parallax(xPosition, speedFactor, outerHeight) options:
	//xPosition - Horizontal position of the element
	//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
	//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
	$('#about').parallax("50%", 0.1);
	$('#para2').parallax("50%", 0.2);
	$('#contact').parallax("50%", 0.3);
	$('#banner').parallax("50%", 0.7);

})
</script><script>

$(document).ready(function() {
    $("#submit_btn").click(function() {
		//alert('hi');
		//exit;
       
        var proceed = true;
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields      
        $("#contact_form input[required=true], #contact_form textarea[required=true]").each(function(){
            $(this).css('border','none');
            if(!$.trim($(this).val())){ //if this field is empty
                $(this).css('border', '3px solid red'); //change border color to red  
                proceed = false; //set do not proceed flag
            }
            //check invalid email
            var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if($(this).attr("type")=="email" && !email_reg.test($.trim($(this).val()))){
                $(this).css('border', '3px solid red'); //change border color to red  
                proceed = false; //set do not proceed flag             
            }  
        });
       
        if(proceed) //everything looks good! proceed...
        {
            //get input field values data to be sent to server
            post_data = {
                'name'     : $('input[name=name]').val(),
                'email'    : $('input[name=email]').val(),
                'phone'  : $('input[name=phone]').val(),
                'message'           : $('textarea[name=message]').val()
            };
           
            //Ajax post data to server
            $.post('contact_me.php', post_data, function(response){  
                if(response.type == 'error'){ //load json data from server and output message    
                    output = '<div class="error">'+response.text+'</div>';
					  }else{
                    output = '<div class="success">'+response.text+'</div>';
                    //reset values in all input fields
                    $("#contact_form  input[required=true], #contact_form textarea[required=true]").val('');
                    $("#contact_form #contact_body").slideUp(); //hide form after success
                }
                $("#contact_form #contact_results").hide().html(output).slideDown();
				            }, 'json');
        }
    });
   
    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form  input[required=true], #contact_form textarea[required=true]").keyup(function() {
        $(this).css('border','none');
        $("#result").slideUp();
    });
});

</script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" id="main" role="navigation">
<div class="container-fluid"><!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header"><button class="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span></button><a class="navbar-brand" href="#banner"><img alt="Digital Agency Oman" src="images/logo.png"/></a></div>
<!-- Collect the nav links, forms, and other content for toggling -->
<ul class="language">
<!--<li><a href="arabic/index.html">Ø§ÙØ¹Ø±Ø¨ÙØ©</a></li>--->
</ul>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav pull-right"><!-- <li class="scoll active"><a href="#banner">Home</a></li>-->
<li><a class="scoll" href="#services">Services</a></li>
<li><a class="scoll" href="#about">About us</a></li>
<li><a class="scoll" href="#clients">clients</a></li>
<!--<li><a class="scoll" href="http://uygoman.com/project/UYG/en/our-works/">WORKS</a></li>-->
<li><a class="scoll" href="#contact">contact</a></li>
</ul>
</div>
<!-- /.navbar-collapse --></div>
<!-- /.container-fluid --></nav>
<section id="banner">
<h1><span>WE ARE CREATIVE</span><span>DIGITAL AGENCY IN OMAN</span></h1>
<a href="#services">See What We Do</a>
<div class="container-fluid">
<div class="row all_steps">
<ul class="steps_wrapper">
<li class="steps">
<div class="i_icon"><img alt="Marketing plans oman" src="images/plan_i.png"/>
<p>Plan</p>
</div>
<div class="i_arrow"><img alt="Marketing plans muscat" src="images/i_arrow.png"/></div>
</li>
<li class="steps">
<div class="i_icon"><img alt="Web design oman" src="images/design_i.png"/>
<p>Design</p>
</div>
<div class="i_arrow"><img alt="Web design muscat" src="images/i_arrow.png"/></div>
</li>
<li class="steps">
<div class="i_icon"><img alt="web development oman" src="images/dev_i.png"/>
<p>Development</p>
</div>
<div class="i_arrow"><img alt="web development muscat" src="images/i_arrow.png"/></div>
</li>
<li class="steps">
<div class="i_icon"><img alt="internet marketing oman" src="images/launch_i.png"/>
<p>Launch</p>
</div>
<div class="i_arrow"><img alt="internet marketing muscut" src="images/i_arrow.png"/></div>
</li>
<li class="steps">
<div class="i_icon"><img alt="Digital marketing oman" src="images/marketing_i.png"/>
<p>Marketing</p>
</div>
<div class="i_arrow"><img alt="Digital marketing muscat" src="images/i_arrow.png"/></div>
</li>
<li class="steps">
<div class="i_icon"><img alt="Digital agency muscat" src="images/result_i.png"/>
<p>Your Result</p>
</div>
</li>
</ul>
</div>
</div>
</section>
<section id="services">
<div class="container-fluid">
<h1>SERVICES WE OFFER</h1>
<div class="row wow fadeInDown">
<div class="col-md-4">
<div class="serivce_box">
<div class="service_icon"><img alt=" web designing in oman" src="images/designing.png"/></div>
<h3>Design Services</h3>
<p>We can assist you in developing convenient, easily accessed, attractive interfaces with documented user experience metrics for the technology that you work with every day. Whether you have a streamlined office decor you want to implement, a web site, or even a new software or graphics development project, our helpful team of artists, graphics specialists and creative designers can assist your UI, UX, Web Design, Graphics, Branding Designs, Logo Creation and related imagery needs.<br/>
 </p>
<ul class="services_list">
<li>Web Designing</li>
<li>Logo Designing</li>
<li>Graphic Designing</li>
</ul>
</div>
</div>
<div class="col-md-4">
<div class="serivce_box">
<div class="service_icon"><img alt="web development in oman" src="images/development.png"/></div>
<h3>Technology Services</h3>
<p>We also help companies roll out and implement the latest Mobile &amp; Web Apps, CMS websites, Custom Applications, and comprehensive E-Commerce Solutions on a large or small scale. Our consultants aid clients in maintaining state of the art Content Management Systems to invigorate their web pages. We can inform your employees about utilizing these capacities to the fullest extent in order to enhance organization-wide processes and optimize the flow of information and the management of data within the enterprise.</p>
<ul class="services_list">
<li>WordPress</li>
<li>Codeignitor</li>
<li>Magento</li>
</ul>
</div>
</div>
<div class="col-md-4">
<div class="serivce_box">
<div class="service_icon"><img alt="Digital marketing oaman" src="images/marketing.png"/></div>
<h3>Marketing Solutions</h3>
<p>Let us assist you with the challenging process of Search Engine Optimization, so that SEO solutions furnish you with a high ranking during online searches. We manage the creation, testing and placement of Paid Advertisements, as well as vital Social Media Marketing, to perform complete marketing Campaigns and Online Marketing functions that drive traffic to your site and develop long term customer relations for your company. Digital Services matter today. Contact us to explore how we can provide comprehensive digital solutions for you.</p>
<ul class="services_list">
<li>Search Engine Optimization</li>
<li>Pay Per Click</li>
<li>Online Marketing</li>
</ul>
</div>
</div>
</div>
<div class="row wow fadeInDown">
<div class="services_wrapper">
<div class="web_design"><a href="services.html#web_des"><img alt="Website design oman" src="images/web_design.png"/> </a>
<p><a href="services.html#web_des">Designing Services</a></p>
<a href="services.html#web_des"> </a></div>
<div class="web_dev"><a href="services.html#web_dev"><img alt="website development oman" src="images/web_development.png"/> </a>
<p><a href="services.html#web_dev">Development Services</a></p>
<a href="services.html#web_dev"> </a></div>
<div class="mobile_app"><a href="services.html#mob_app"><img alt="app development oman" src="images/mobile_app.png"/> </a>
<p><a href="services.html#mob_app">Mobile App Development</a></p>
<a href="services.html#mob_app"> </a></div>
</div>
</div>
<div class="row"><a class="view_all" href="services.html">View All</a></div>
</div>
</section>
<section id="about">
<div class="container-fluid">
<div class="row wow fadeInDown">
<div class="col-sm-12 about_top">
<h1>About us</h1>
<p>United Young Generation (UYG) emerging as one of the most reliable and successful Solution providers in Sultanate of Oman. We stepped in 2009, as the Branding specialize, in the creation and development of company branding; covering all aspects of visual communication, whether through traditional methods or through digital media.<br/>
<br/>
We are dedicated to bringing the best services and products to meet our client's objectives. Our customer service provides a full-range of pre- and post-sale support by phone, e-mail and in person all over the sultanate. Our services are backed by a team of carefully selected, highly qualified and experienced IT Professionals.<br/>
<br/>
With years of experience will allow an organization to grasp the cost savings and competitive advantage of modern information technology. Our prime interest is in our client’s satisfaction. We listen to the requirements and act accordingly with best possible solutions that fit in budget.</p>
</div>
</div>
<div class="row wow fadeInDown">
<div class="col-md-6 col-md-offset-1 col-md-4">
<div class="about_box">
<h2><img src="images/vision.png"/><span>OUR VISION</span></h2>
<p>To create new horizons in the local market and progress to higher levels, through innovative solutions, cost-effective services and continuous commitment towards clients.</p>
</div>
</div>
<div class="col-md-6 col-md-offset-2 col-md-4">
<div class="about_box">
<h2><img src="images/mission.png"/><span>OUR MISSION</span></h2>
<p>Working consistently to provide a unique client servicing experience that fits the local market and ensures effective advertising and strong strategic planning to build long-term relationships with clients.</p>
</div>
</div>
</div>
</div>
</section>
<section id="clients">
<div class="container-fluid">
<div class="row wow fadeInDown">
<div class="col-md-8 col-md-offset-2 col-sm-12">
<h1>We’re proud of who we work with</h1>
<p>We are proud of the work we do to our clients and we are following a strategy to satisfy our clients,below we listed some of our customers</p>
</div>
</div>
<div class="row wow fadeInDown">
<div class="col-sm-12 col-md-3"><img alt="Website design" src="images/c1.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="Website design muscat" src="images/c2.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="Website development" src="images/c3.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="Website development muscat" src="images/c4.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="app development" src="images/c5.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="app development muscat" src="images/c6.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="Internet marketing in oman" src="images/c7.png"/></div>
<div class="col-sm-12 col-md-3"><img alt="Internet marketing in muscat" src="images/c8.png"/></div>
</div>
</div>
</section>
<section id="works">
<div class="container-fluid">
<div class="row wow fadeInDown">
<h1>Our Works</h1>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="web designing oman" src="images/w1.png"/></div>
<div class="w_content">
<h3>Danamin</h3>
<p>Website Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="web designing muscat" src="images/w2.png"/></div>
<div class="w_content">
<h3>Happy Kid Clinic</h3>
<p>Website Design</p>
<a class="view_web" href="http://happykidclinic.com/" target="_blank"><img src="images/view.png"/></a></div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="logo designing oman" src="images/w3.png"/></div>
<div class="w_content">
<h3>Lorem ispum</h3>
<p>Website Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="logo designing muscat" src="images/w4.png"/></div>
<div class="w_content">
<h3>Across Continents Logistics</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img src="images/w5.png"/></div>
<div class="w_content">
<h3>Noor Al Assalah Trading Co.</h3>
<p>Website Design</p>
<a class="view_web" href="http://natcoman.com/" target="_blank"><img alt="graphic designing " src="images/view.png"/></a></div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img src="images/w7.jpg"/></div>
<div class="w_content">
<h3>Biyaq Laboratories</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="graphic designing oman " src="images/w8.jpg"/></div>
<div class="w_content">
<h3>Royal Touch</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="graphic designing muscat" src="images/w9.jpg"/></div>
<div class="w_content">
<h3>Royal Home</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="wordpress services " src="images/w10.jpg"/></div>
<div class="w_content">
<h3>Moorish</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="wordpress services oman" src="images/w11.jpg"/></div>
<div class="w_content">
<h3>Al.Saidiya L.L.C</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="wordpress services muscat" src="images/w12.jpg"/></div>
<div class="w_content">
<h3>MJ Botique</h3>
<p>Logo Design</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="w_box">
<div class="w_image"><img alt="wordpress services in oman " src="images/w13.jpg"/></div>
<div class="w_content">
<h3>International Golden Drop L.L.C</h3>
<p>Logo Design</p>
</div>
</div>
</div>
</div>
</div>
</section>
<section id="contact">
<div class="container-fluid">
<div class="row">
<h1>Start Your Project Today</h1>
<p>We are as excited as you are! Contact us now to start your project.</p>
</div>
<div class="row wow fadeInDown">
<div class="col-md-7">
<div id="contact_form"><input name="name" placeholder="Name" required="true" type="text"/> <input name="email" placeholder="Email" required="true" type="email"/> <input name="phone" placeholder="Phone" required="true" type="phone"/><textarea name="message" placeholder="Message" required="true"></textarea> <input id="submit_btn" name="send" type="button" value="send message"/>
<div id="contact_results"></div>
</div>
</div>
<div class="col-md-4 col-xs-offset-1">
<ul class="contact_info">
<li><span><img alt="UYG Oman contact" src="images/phone_icon.png"/></span> 00968 94025757</li>
<li><span><img alt="UYG Oman fax number" src="images/fax1.png"/></span> 00968 24397884</li>
<li><span><img alt="UYG Oman contact number" src="images/phone1.png"/></span> 00968 24397844</li>
<li><span><img alt="UYG Oman web" src="images/web.png"/></span> www.uygoman.com</li>
<li><span><img alt="UYG Oman contact address" src="images/home.png"/></span> P.O.Box. 404<br/>
	PC. 112 Ruwi Sultanate of Oman</li>
<li><span><img alt="UYG Oman location" src="images/location_icon.png"/></span> Located in Al-Khuwaire<br/>
	City Season Hotel, 5th Floor<br/>
	Office No. 501</li>
</ul>
</div>
</div>
</div>
</section>
<section id="social">
<div class="container-fluid">
<div class="row">
<h1>GET IN TOUCH WITH US</h1>
<ul>
<li><a href="https://instagram.com/uygoman/" target="_blank"><img alt="UYG Oman on instagram" src="images/instagram.png"/></a></li>
<li><a href="https://twitter.com/uygoman" target="_blank"><img alt="UYG Oman on twitter" src="images/twitter.png"/></a></li>
<li><a href="https://plus.google.com/110964081510539582358/about" target="_blank"><img alt="UYG Oman on google" src="images/google.png"/></a></li>
<li><a href="https://www.facebook.com/pages/United-Young-Generation/244622902227928?__mref=message_bubble" target="_blank"><img alt="UYG Oman on facebook" src="images/facebook.png"/></a></li>
</ul>
</div>
</div>
</section>
<section id="footer">
<div class="container-fluid">
<div class="row">
<ul class="footer_menu">
<li><a href="index.html#services">Services</a></li>
<li><a href="index.html#about">About us</a></li>
<li><a href="index.html#clients">Clients</a></li>
<li><a href="index.html#works">Works</a></li>
<li><a href="index.html#contact">Contact</a></li>
</ul>
<p class="footer_contact"><span><img alt="UYG Oman mail" src="images/mail2.png"/>info@uygoman.com</span> <span><img alt="UYG Oman on phone" src="images/call2.png"/>00968 24397884</span> <span><img alt="UYG Oman on fax" src="images/fax2.png"/>00968 24397844</span> <span><img alt="UYG Oman on call" src="images/phone2.png"/>00968 94025757</span></p>
<p><span>© 2015 United Young Generation.</span><span>All rights reserved.</span></p>
<div class="counter_cnt">
<p>Number of visitors</p>
<div class="counter_cnt_wrapper"><script src="http://counter10.freecounter.ovh/private/counter.js?c=4fc98c27c42106796d43b3739e9868c1" type="text/javascript"></script></div>
</div>
</div>
</div>
</section>
<script src="js/wow.js"></script><script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100
      }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
</script><script type="text/javascript">
$(document).ready(function(){
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
});
</script><script type="text/javascript">
$(document).ready(function(){
    
    $('.scroll').click(function(event) {
        event.preventDefault();
 
        var full_url = this.href;
        
        var parts = full_url.split('#');
        var trgt = parts[1];

        var target_offset = $('#'+trgt).offset();
        var target_top = target_offset.top;

        $('html, body').animate({scrollTop:target_top}, 500);


    });
    
    $('nav#main a').click(function(){
         $('nav#main a').removeClass('active');
         $(this).addClass('active');
    });
    
});
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-90176350-1', 'auto');
  ga('send', 'pageview')
</script>
<script src="js/bootstrap.js"></script><script src="js/bootstrap.min.js"></script></body>
</html>