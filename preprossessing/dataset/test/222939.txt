<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "88995",
      cRay: "6111193fa88624bf",
      cHash: "af08e44938e12bb",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmlvcnhpdi5vcmcv",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "Al0JAcaEaq8KcHNCN9foZigEO+Nrc0+3ubosDDEsrPyh8YngmszNAxPQ5widAXHkESuWRkM/n8pqzNBZUmoKqfCr+4InT4SCrNTrpuKICJ3oAmSEroAy0FXX/9Ikc3lqe/qzuTjGM6p7wVCGQyLTJdFd6M5yrC1kqPn1THVbr6EDuvi4E/NHRkAGx+hUHBIXkRuKGEG7R99J9+pKqwy/U8IHQP6X5C+pOEPNyxhjXTLGk2I7fvCRIi7MjjALVDjbzih9NYPKdGsU3A8cmxRLqmY24s4xMcLqFCg4fVdeljgTJOn7CLx90ZfPQWCnGlCMXNWCmOrN7fdKhfMyhjuYf8jE4LuX1PFkXVhTDsRkVujGtXPc7ooFMHTBCNYl1zNz2U/KWbt2S67XRxgbUcpg+ruQQ1bX94/YrrHxra+Nz1faSCXfCSVmM5C/imHkpr2HXPxSufgRvCj46E+g7ArStfX1Eqby4n+BWmbIuldQQTXxLd6yzz7p+jccAYEVbuL0039Q9EdOKg+MIcneM1/qGHeQb4pLvc06rjmJl3gLOFx1jUwQduJ68pMPMo7eUQ7Qml4bAM+tUfjc3N1Cid5qnod28Ow6k7NaBBqiuJVgIWlMAPtL0kGTryhNTJWl+m+72bCWRZMLoscCtPUo4pE3JvTcQ7qM9IjMxl1WJq/UQsPZW2bqzrAJgjLf1cKqghGXHRVjXRvUsGSdh5BEEgXrlZJqBOVZUhAxSUKoN4f2K3PBj4cWe4si/3WzfWZobowsfTZ5rpBClt9es3F32ydyVHMgFV1erbFlN8tYQeShU1k=",
        t: "MTYxMDU2MTY4NC40MjkwMDA=",
        m: "GGWzr2Wu034RJFYTq+uTpYtelrV56++csjnsovZ9h9U=",
        i1: "WlXD2bnacxMS0AnEeeCvaw==",
        i2: "eEXXDfx8Yf16yH24TC0NVQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "vi6Hih6OH8jmrRyGnbz5vHYhhE8KYe9yGronDbQ8ugM=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=6111193fa88624bf");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> biorxiv.org.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=20fee9f320ae2bb199713356fead6a23989a4c88-1610561684-0-ATOKERnNhGlt-mvI6Gdf8EoeNbANPNyt_jvnZzfna7jm-GDLQamHzkX3RlFzQCusplKB7udNKD_fV3wQSFP82gPTbFjIboX86ZP60-EliVY92GAuSRZ08aajVOeWbaMVXnjIB3Nb-Ft_f4o4rEQgwSHS47TX7MnhjntO713CgVbVKRshSloqDg3Kisv4usHPBscX_0JwlNrKzD9rFN03Prsskj37MhzpvEV1C30rpLTGPjcjoEEfVQUEm6zWfVxx5Rwso96DSFhNGkumdgLrNtk6IPuybHLlHGVGrdaiFkqf3jA0R0yRQV9xwT2E3phkshUx8xAErgrdG6zP7BUDV12VjMxckf6JMcMyvw8PlBGZtittZS9Na4aA998uOYT_6g" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="9b26f9d12a42103156e4ddf93a51f5ecc7f39626-1610561684-0-AZthLkjyKHdIrdECXe/tKLlnUtlwfhOf4lwfiqxbzcyAE3x8y+wYiVlTGwqRWz1JbqGaC0sBCOZG5rCNrKpIpk/re8MJkDDHqQd2hRRsu6g+vkgjyYJX4hh8In0Bx57G1k5wBXonsM1pMgrjF0RnqbWVCxufNBFutMdcMK1aWLQXFK3+VNH07YF98LZs7asf0wZScOUyg8An4Kh25fsE6Wc2vEnAAMfvV7Ya4HtiJjjI0GogDinQC58lX/wVirjEQCNwrrygFcooOjC7Vd/aVoPLYoln3Vk6hjjJLQzhsUZdkTw+i8Ck9ny9EtYHjI1CDmaIWPKYUzyWEEc8UeA5T3fEnFYxQN28rwzY8wBhYAU6EixRP84ybm1ClvzamShe9qELNUU7o5O7xdFGKPb90Rn/Q5u107ppB741R6FnS5+Ec3Fyuk7FZCEOjdoqU7ITUHFqddTGq+8iOyb/eGCpIUebVt9vkV6PoGtg8QomJpgp5LF7egQDdweDIrnIN805+CdXDkW2dbfxI0SmVjM5sITMO7jYLo8pX+rhSiNL6ZCXPtN9tGol1j5VfyOtGORhnJiy/zlrNOALyacDCQ2d87vcep6dT5EO2BzeT31NotaGywCgY2sm6nXInctiwr3wscIvqMXWM7o2y3NbAyncKAFbZPfo8+MF8RzoU7wmr9KNjGgpZFPMPVhsgwImEZk71+gwx1PspIUEUZPSSFXYBkKrzFG5jAjyUraPYEKgKrntb0o4daMw8NPzv2FDZAFjUJNytt3WXlWNAr1wwZwhx4p14IIMxH/1B2r3hzCTN542b985o7bM2hHS1GXw1K4cjHeCzxYZ3H+MVQq5eZ92/H8SChmpNxLcoO09OSAFH5L+ULl/A+U2/E+VjsYnSleP7wZrx87/4PPT0dW4YM5XztkqaF5Ta8VWoovXYuYg737df9DBeuFpHOIRLO/MlsxEv/Fw1pWrCjgdE9lpyHiGlLgEGrvk5VNyz9I0Yo500O4qlKb6Nk0LjyG3KXmgP5Qj3ooNz/5BfhDb7k21IPt8D89XR5h172H7N9lpZJzwahFopB3E4BvtoAAjKht/O+hNRrae4UPXazCVXGzbl1C3i6Z+4wtfCaIrCBzva63mo/8wpaZAbDgREu66le6uG6C1Z6gopyGCLKeYQbxCam58nUSJikTwePBtej5Nwh3I1FnvXwH2ev8H5AA1B1qcf6d/EjoJvfRSMZrEwreQfpW0+2zmUwEgxbcN7d4SGHCoASyDB0VC6Sy4BQzFmqBN7CJv9jYNCR2FfULxwEFj14YYhzrlF5i7mJ9ElOYsJfvCmQkA"/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="ebc91e3389f4ed6f9d996942711b63bc"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610561688.429-Qfq0/YJHx3"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=6111193fa88624bf')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>6111193fa88624bf</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
