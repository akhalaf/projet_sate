<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Aramark.net Login</title>
<script language="JavaScript1.1">
  
	function validateData()
	{
		if ( (document.forms["auth"]["PROXY_SG_USERNAME"].value == "") || (document.forms["auth"]["PROXY_SG_PASSWORD"].value== "") )
		{
			alert ("One or more fields are empty. Please fill in all the fields.");
			return false;
		} 
		else
		{
			if(validateUPN()) {
				return true;
			}
			alert ("User ID is invalid. It must be of the form 'lastname-firstname[[###]@[domain]]'");
			return false;
		}
	}

	function submitForm()
	{
		if (validateData())
		{
			document.forms["auth"]["PROXY_SG_USERNAME"].value = document.forms["auth"]["PROXY_SG_USERNAME"].value.toLowerCase();
			return true;
		}
			return false;
	}

	function setFocus()
	{
		document.forms["auth"].elements[0].focus()
	}

	function validateUPN()
	{
		validUPNRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		validANETRegEx = /^([a-zA-Z0-9_\.\-])+([0-9]{3,})+$/;

		if(document.forms["auth"]["PROXY_SG_USERNAME"].value.match(validUPNRegEx))
		 return(true);

		//no match for valid upn - check for valid 'old' ANET_ID
		if(document.forms["auth"]["PROXY_SG_USERNAME"].value.match(validANETRegEx))
		{
			document.forms["auth"]["PROXY_SG_USERNAME"].value+="@star.com";
			return(true);
		}
		//bad input
		return(false);
	}
        </script>
<style type="text/css">
<!--
.text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	}

.warnHead {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #EB002A;
	}

.warnMsg {font-size: 10px; color: #000000; font-family: Arial, Helvetica, sans-serif;}

a:link
{
	color: #EB002A;
	font-size: 14px;
	text-decoration: underline
}

a:hover
{
	color: #EB002A;
    text-decoration: none
}

a:visited
{
	color: #EB002A;
	font-size: 14px;
	text-decoration: underline
}

.welcomeHid {
	font-size: 6px;
	color: #FFFFFF;
}

.input {
    width:140px;
    border: 1px solid #CCC;
    background: #fff;
}
.input:hover {
    border: 1px solid #696969;
    background: #eee;
}
.notice {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	padding: 10px 15px;
	background-color: #a5d5ff;
	margin: 10px 0;
}
-->
</style>
</head>
<body onload="setFocus()">
<!--<H1>Enter Proxy Credentials for Realm star_upn</H1>
<P>Reason for challenge: Credentials are missing.-->
<form action="https://auth.aramark.net/?bcfru=aHR0cHM6Ly93d3cuYXJhbWFyay5uZXQv" method="POST" name="auth">
<table align="center" border="0" cellpadding="6" cellspacing="0" width="440">
<tr>
<td><div align="center"><img src="https://register.aramark.net/images/aramark_logo.png"/></div></td>
</tr>
<tr>
<td>
<div class="notice"><b>ACTION REQUIRED</b><br/><br/>
		Aramark.net has a new single sign-on experience coming soon. <a href="https://aramark365.sharepoint.com/sites/SSOPreview/SitePages/SuccessfulSignIn.aspx" target="_blank">Check NOW</a> to see if your account is compatible.<br/><br/>
		For instructions on how to set up your account <a href="https://aka.ms/securityinfoguide" target="_blank">click here</a> or <a href="https://www.youtube.com/watch?v=WKMeq2aU6Ck" target="_blank">watch this video.</a><br/><br/>
		Problems successfully signing in? Contact ServiceDesk.</div>
</td>
</tr>
<tr>
<td><table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td><table border="0" cellpadding="0" cellspacing="2" width="100%">
<tr>
<td style="width:150px"><div align="right"><span class="text">User ID: </span></div></td>
<td><input class="input" maxlength="64" name="PROXY_SG_USERNAME" value=""/></td>
</tr>
<tr>
<td style="width:150px"><div align="right"><span class="text">Password: </span></div></td>
<td><input class="input" maxlength="64" name="PROXY_SG_PASSWORD" type="PASSWORD"/>
<input name="PROXY_SG_REQUEST_ID" type="HIDDEN" value=""/></td>
</tr>
</table></td>
</tr>
<tr>
<td><div align="center"><input border="0" onclick="return submitForm()" src="https://register.aramark.net/images/signon_submit.gif" type="image"/>
</div></td>
</tr>
</table>
</td>
</tr>
<script language="javascript">
	$errmsg = 'Credentials are missing.';
	if ($errmsg == 'General authentication failure due to bad user ID or authentication token.'){
	window.location = 'https://register.aramark.net/bc_invalid.htm';
	}
	</script>
<tr>
<td><div align="center"><span class="text"><a href="https://register.aramark.net/PasswordReset.htm">Forgot/Reset
Password?</a></span></div></td>
</tr>
<tr>
<td><div align="center"><span class="text"><a href="https://register.aramark.net/ContactForAccountRequest.htm">Request
an account</a></span></div></td>
</tr>
<tr>
<td><div align="center"><span class="warnHead">Warning Notice</span><br/>
<span class="warnMsg">This site, each page and all content thereof are proprietary and confidential information
of Aramark. Access to and use of this information is subject to all Aramark policies including, but not limited to,
Aramark's Information Security policy, and other Internet and Intranet policies. These policies address, among other
things, the confidentiality of material, permitted access, permitted content and permitted use. (BC_20)</span></div></td>
<span class="welcomeHid">Welcome to aramark.net</span>
</tr>
</table>
</form>
</body>
</html>
