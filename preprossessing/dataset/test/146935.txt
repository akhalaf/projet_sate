<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "23580",
      cRay: "610e8e61dec0191d",
      cHash: "75534456e9f180f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJjaGlwcm9kdWN0cy5jb20v",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "OJYEbWmf6SqhEBAOpUoYzlP8cngoZtNMNdUvfWPHduQo8Y1nqJju8v/LKfMdzmyRX194FdT7/VqTCT2puUW3AkZ82TNYyqHaslfYRr8xMDFMLTQutyzgwYuOzMkRRtWBFnLRUEqKS98USm4MMmihWYPRbpHub7XKv4YEQap9opYv+kbHpjKNu+WqQvSxdxPsHS278B5HbeTgd+l8unLPIJpI692tM3YkkeJkwV2xCgcu6pM/64f0YC776mFPO0s+Z45Mx5epLJ/vsRTVe3JMXd0klhngG+GKh/roQGdexq00qx60/avtEZPtDVEAPc2a18aIr5XYcGiHeFfDjK+35S8QVoQyJClj4vmGWkwBz+oSUnFuNKUVYaMOyO8KXIo1Gb/FoxPl81TbH3nQYnHkJWLGNFTR35A/jol7enhcY0XbyZFNSI3rdDapksXyA2AqdJx5Vevvfq5S3JWT/XFRVyp1l7FdugcJjfLXPBScqIFF0f1QhkYxQFtTKRmzSQA0DpLhMWp8Vbd/drJpEzFUdAxx+NxA2iqOOlG3e+Af6wodIZY5BfPVXDmHWFeIngNR3TyhATg0deM7NOjOk3l6SoB3HGIz7Qp+3LnbswWRFPVIRIXmuuU24vsyWZG63uDpl+cTLqJvF6zMlrzd2S1FXr5j4BkKW3uOI7yk3h0kNIJ7ri3rdDE5B2jZkLkCs0abSKQpDJ4mqsNOe/miLelqcamA37Y//LiyVugrYpaAFd2XoIZV6V9/DCaLQhi7EDp+pYpOJ1gTqOlDLpIBPn5Nl9cv2OwhEtFpJLkIfWXC7O4=",
        t: "MTYxMDUzNTAyNC45NDkwMDA=",
        m: "0AVzcxJvy+QF4+gRVmsehtPMmZlNk+6DzT43YWSEly4=",
        i1: "YzRZOClyYKD1bKwqgk4D0w==",
        i2: "VufCgIDwrMZwf2cN9LpNOg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "OEAk01834NgZCXHvS4HMicXuIzxl0RSz+ApVyc+1n68=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610e8e61dec0191d");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> archiproducts.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=2d66833771db82bb7b559d7cfa164b15e90a4aa2-1610535024-0-Aapw-N0QAuqqdxEh_05KZGd9aDmXUNxqaMu2HhKQSnwpann8ZqejlibYaDfNSIHZdfxh0OjNDqxKeLHJAkaVU-kwVsO1X9ij5F6C55bzr_3e_Mpi1iBjxfa0RhqCyIbWlTjOQdtOh34qAV6DPLOYSOETsS3EmbOpRZuWUesIfYGTK0-fmteqpcHtlCsy9ZpMEA88HgznZNn3LvaiI8YFPdl1ociWRPfyUGv4x9OYzJfZpM8Ykwjl8tj6pzbEEbo3SLha8ubr7rpgIaIjbCK63aK-PqAxzvbExGwlP9kPqwhxzwocey02cE1nO0C7sikiDdJ8BRAy3DuZPdxsZPo-w0FeSR28z6PHSFsJqqMh8DzkPwVglmvGHMYpEs9km106-w" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="fa6bbdfde2f572b928baf2a0eb668eb9e8795c53-1610535024-0-ASV0Ul2JPdWPYUDUqLUXYbSZfDnWerPx4hGgOFxB4qCEpeOREFgSOsnWYBPGf8/RM8YUVyU9pwVjtGcwWFe8t7ZuQSuf9YTqGCsyhlrFTmO85PNDA62Rp3DsFjQPE3ceDaNV3CETAtuNxIgSDxKdcHJZifBkpMb8AGAu7cRq3yj+eDDQiOg83v8HZpftGNHzCvKpGu+1yL9VjGtIfzbaI+cvm6WsrdrG9AfCp8P3nGkvEwM4gNIOnE7wUUH97LwBPqFFNCORI7aaOQcnqtoTeQ15RFdAnF7SjLFHtIpjrx4LNDhUZ/66ljUEtQrl6FYiEySQar7qhd8ew3pJtJvq5lw8tKE6Kud5rRx4M7cb51Ixi1Qb454CosAAY6WYRLCF8yXLNK5Lqab6Kx9+yRpz0f1Yt4A5mo3M555+ZysF9yxa0yf/ZLQleLywGRv74Ay/O6f8QZjoDcP7xB64mx3RSSKblX2mg9Nu9D+7N1s3vZR4a7e2QTKaa+nHNdUoJAkL9+ir2OWqowWvW2Lus6fRGM4xR9u+z68ZTxmESM3KiIBUP4SR9EP4V3i/n3iBEzECFGv4aLEx0EOiMn12+HS3LJXXfgDZI+6m93NERfGmqjKVgnuYH4yUysgcf/u2G/Daua4eVqovIuQGQv/eO9XwIc4swHmaP/+7moPTmBHvyPxNIAmRBis1O7n4cLlkead+PNk1pGF1fL96O2kldu+ogBoyKS5ADYaPcjZicBtPQL95Fj8JdQhVQTReaa+IUzBf0hcCIoqsIOsvwJx50uKXk3mJ9meDiDNB1cUHe90qitxzv/YG0p73azDZWBoCIAo+RiTv6BPCBHqmp+uZqR7+9M7oi7RYJt3ke9NDUdvwCSHKkMeF/744Xa0naUCSym473DfX9Ua3uZHSg2l79BeRZFTvgpE1/Z27HVKEHoV/mJD8pLesB6XIQ8enqFtXD8KgffAJRkbvmk159tcPb5Au/4RnnnifcOZSeQTTbnDqr6iOREGOm0sbS7Tbmni5Yit/JZoEkChz6ClSPnRp4ztBDxWnSCe0GKEt4RZsCZretegcO2RGyAqfGDS8gR6bA6OCsaRpzkpG/ACR2KWKikhZxvWiDYsk6/wrza2ZYVHWB62nEHWoNe3OrJEKBthJZ8g92y2VXn6N5ddNFJqD31ZBm3tsCbMxFoXK3oFXDPUfCMpl3ROg6lQ99xynkVs5MgIDtS/hrn7Fv04OCGH/n8xxAls++pUJsN/FPMmppcOOsfwQQWp0d9WU8yVcbCeNkjPslrsKXfnprB8HnDhAp2NODDHnLM0YMTIsMIGu762u11FGYpuKgm1O10+IZpreKINhTw=="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="1890a0dd5658a2a0c853503c2b71a103"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610535028.949-QfTRe2VZKk"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610e8e61dec0191d')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610e8e61dec0191d</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
