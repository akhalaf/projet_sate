<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>VGMusic - 31,810 Game Music MIDI files</title>
<meta content="Video Game MIDI Music from NES, SNES, N64, GameCube, Gameboy, Genesis, Master System, Sega Dreamcast, Sega Saturn, Sony PlayStation, X-Box, Atari, TurboGrafx-16, and more!" name="description"/>
<meta content="MIDI,MID,videogame,game,video game,music,archive,nintendo,nes,snes,n64,gamecube,ds,gameboy,game boy,mario,zelda,final fantasy,sonic,sony,psx,ps2,psp,ps3,ps4,xbox,x-box,xbox360,xboxone,playstation,sega,genesis,saturn,commodore,master,master system,tg-16" name="keywords"/>
<meta content="index,follow" name="robots"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<style>
.menuhead {
    font-weight: bold;
    margin-top: 0px;
}
.menularge {
    font-weight: bold;
    margin-bottom: 0px;
}
.menu {
    margin-top: 0px;
}
</style>
<!-- Google Verify thing -->
<meta content="JdMp1AmLH+LEQCf3/zf3qgRL3ySVlWVLc3Jdbqd2Png=" name="verify-v1"/>
</head>
<body background="/images/background.jpg" onload="if (self != top) top.location = self.location">
<div align="center">
<table bgcolor="#DDDDDD" border="2" cellpadding="0" cellspacing="0" width="97%">
<tbody>
<tr>
<td align="center">
<table>
<tr>
<td align="center">
</td></tr><tr>
<td align="center" style="line-height:88%; margin-top: 0px;">
<a href="information/donate.php">Please contribute today</a>
            to help us maintain a banner-ad-free site. Thanks for the support!
           </td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" bgcolor="#DDDDDD" valign="middle">
<p style="margin-bottom: 0px;">
<img alt="Videogame Music Archive Logo made by Alexander Farris" height="299" src="./images/logos/vglogo4.jpg" width="493"/><br/>
<font size="1">Mario and Link © Nintendo. Sonic the Hedgehog and Miles "Tails" Prower © Sega.  Mega Man © Capcom.</font>
</p>
</td>
</tr>
<tr>
<td align="center" bgcolor="#DDDDDD" valign="bottom">
<div align="center" id="menu">
<!-- Google CSE Search Box Begins  -->
<form action="https://www.google.com/cse" id="searchbox_007557153183693834903:-vcc4ruk5dw">
<input name="cx" type="hidden" value="007557153183693834903:-vcc4ruk5dw"/>
<input name="cof" type="hidden" value="FORID:1"/>
<input name="q" size="40" type="text"/>
<input name="sa" type="submit" value="Search"/>
<img alt="Google Custom Search" src="https://www.google.com/coop/images/google_custom_search_smnar.gif"/>
</form>
<!-- Google CSE Search Box Ends -->
<form action="https://www.vgmusic.com/cgi/redirect.pl" method="post" target="_top">
<select name="info" onchange="newGo(this)" size="1">
<option selected="selected" value="https://www.vgmusic.com/">Section Menu</option>
<option value="https://www.vgmusic.com/">Main Page</option>
<option value="https://www.vgmusic.com/updates/">Recent Additions</option>
<option value="https://www.vgmusic.com/faq/">Frequently Asked Questions</option>
<!--  <option value="https://www.vgmusic.com/phpBB3/">VGMusic Forums &amp; Wishlist</option> -->
<option value="https://www.vgmusic.com/new-files/">New-Files</option>
<option value="https://www.vgmusic.com/new-files/upload/">Upload Files</option>
<!--   <option value="https://www.vgmusic.com/contests/">Contests</option> -->
<option value="https://www.vgmusic.com/information/links.php">Related Links</option>
<option value="https://www.vgmusic.com/information/milestones.php">Milestones</option>
</select>
<input alt="Go!" name="submit_info" src="/images/go.gif" style="margin-right: 1em; border: 0; border: 0; width: 25px; height: 18px; vertical-align: top;" type="image"/>
<select name="system" onchange="newGo(this)" size="1">
<option selected="selected" value="https://www.vgmusic.com/">Music Menu</option>
<option value="https://www.vgmusic.com/">Main Page</option>
<optgroup label="Nintendo Consoles">
<option value="http://www.vgmusic.com/music/console/nintendo/nes/">Nintendo</option>
<option value="http://www.vgmusic.com/music/console/nintendo/gameboy/">Game Boy</option>
<option value="http://www.vgmusic.com/music/console/nintendo/snes/">Super Nintendo</option>
<option value="http://www.vgmusic.com/music/console/nintendo/n64/">Nintendo 64</option>
<option value="http://www.vgmusic.com/music/console/nintendo/virtualboy/">VirtualBoy</option>
<option value="http://www.vgmusic.com/music/console/nintendo/gba/">Gameboy Advance</option>
<option value="http://www.vgmusic.com/music/console/nintendo/gamecube/">GameCube</option>
<option value="http://www.vgmusic.com/music/console/nintendo/ds/">Nintendo DS</option>
<option value="http://www.vgmusic.com/music/console/nintendo/3ds/">Nintendo 3DS</option>
<option value="http://www.vgmusic.com/music/console/nintendo/wii/">Nintendo Wii</option>
<option value="http://www.vgmusic.com/music/console/nintendo/wiiu/">Nintendo Wii U</option>
<option value="http://www.vgmusic.com/music/console/nintendo/switch/">Nintendo Switch</option>
</optgroup>
<optgroup label="Sega Consoles">
<option label="Master System" value="http://www.vgmusic.com/music/console/sega/master/">Sega Master System</option>
<option label="Game Gear" value="http://www.vgmusic.com/music/console/sega/gamegear/">Sega Game Gear</option>
<option label="Genesis" value="http://www.vgmusic.com/music/console/sega/genesis/">Sega Genesis</option>
<option value="http://www.vgmusic.com/music/console/sega/segacd/">Sega CD</option>
<option label="32X" value="http://www.vgmusic.com/music/console/sega/32x/">Sega 32x</option>
<option label="Saturn" value="http://www.vgmusic.com/music/console/sega/saturn/">Sega Saturn</option>
<option label="Dreamcast" value="http://www.vgmusic.com/music/console/sega/dreamcast/">Sega Dreamcast</option>
</optgroup>
<optgroup label="Sony Consoles">
<option label="PlayStation" value="http://www.vgmusic.com/music/console/sony/ps1/">Sony PlayStation</option>
<option label="PlayStation 2" value="http://www.vgmusic.com/music/console/sony/ps2/">Sony PlayStation 2</option>
<option label="PlayStation 3" value="http://www.vgmusic.com/music/console/sony/ps3/">Sony PlayStation 3</option>
<option label="PlayStation 4" value="http://www.vgmusic.com/music/console/sony/ps4/">Sony PlayStation 4</option>
<option label="PlayStation Portable" value="http://www.vgmusic.com/music/console/sony/psp/">Sony PlayStation Portable</option>
</optgroup>
<optgroup label="Microsoft Consoles">
<option value="http://www.vgmusic.com/music/console/microsoft/xbox/">Xbox</option>
<option value="http://www.vgmusic.com/music/console/microsoft/xbox360/">Xbox 360</option>
<option value="http://www.vgmusic.com/music/console/microsoft/xboxone/">Xbox One</option>
</optgroup>
<optgroup label="NEC Consoles">
<option value="http://www.vgmusic.com/music/console/nec/tg16/">TurboGrafx-16</option>
<option value="http://www.vgmusic.com/music/console/nec/tduo/">Turbo Duo</option>
<option value="http://www.vgmusic.com/music/console/nec/sgx/">SuperGrafx</option>
<option value="http://www.vgmusic.com/music/console/nec/pcfx/">PC-FX</option>
</optgroup>
<optgroup label="SNK Consoles">
<option label="Neo-Geo" value="http://www.vgmusic.com/music/console/snk/neogeo/">SNK Neo-Geo</option>
<option label="Neo-Geo Pocket" value="http://www.vgmusic.com/music/console/snk/neogeopocket/">SNK Neo-Geo Pocket</option>
</optgroup>
<optgroup label="Atari Consoles">
<option label="2600" value="http://www.vgmusic.com/music/console/atari/2600/">Atari 2600</option>
<option label="7800" value="http://www.vgmusic.com/music/console/atari/7800/">Atari 7800</option>
<option label="Lynx" value="http://www.vgmusic.com/music/console/atari/lynx/">Atari Lynx</option>
</optgroup>
<option value="http://www.vgmusic.com/music/console/coleco/colecovision/">Colecovision</option>
<option value="http://www.vgmusic.com/music/console/mattel/intellivision/">Mattel Intellivision</option>
<option value="http://www.vgmusic.com/music/console/magnavox/odyssey2/">Magnavox Odyssey2</option>
<option value="http://www.vgmusic.com/music/console/3do/3do/">3DO</option>
<option value="http://www.vgmusic.com/music/console/philips/cd-i/">Philips CD-i</option>
<optgroup label="Computer Systems">
<option value="http://www.vgmusic.com/music/computer/commodore/amiga/">Amiga Computer</option>
<option value="http://www.vgmusic.com/music/computer/amstrad/amstradcpc/">Amstrad CPC</option>
<option value="http://www.vgmusic.com/music/computer/apple/appleii/">Apple II</option>
<option value="http://www.vgmusic.com/music/computer/apple/macintosh/">Apple Macintosh</option>
<option value="http://www.vgmusic.com/music/computer/atari/atari/">Atari Computers</option>
<option value="http://www.vgmusic.com/music/computer/commodore/commodore/">Commodore 64/128</option>
<option value="http://www.vgmusic.com/music/computer/microsoft/windows/">Microsoft DOS/Windows</option>
<option value="http://www.vgmusic.com/music/computer/miscellaneous/msx/">MSX Computer</option>
<option value="http://www.vgmusic.com/music/computer/nec/pc-88/">NEC PC-88</option>
<option value="http://www.vgmusic.com/music/computer/nec/pc-98/">NEC PC-98</option>
<option value="http://www.vgmusic.com/music/computer/sharp/x68000/">Sharp X68000</option>
<option value="http://www.vgmusic.com/music/computer/sinclair/spectrum/">Sinclair Spectrum</option>
<option value="http://www.vgmusic.com/music/computer/tomy/tutor/">Tomy Tutor</option>
</optgroup>
<option value="http://www.vgmusic.com/music/other/miscellaneous/arcade/">Arcade</option>
<option value="http://www.vgmusic.com/music/other/miscellaneous/medley/">Medleys</option>
<option value="http://www.vgmusic.com/music/other/miscellaneous/piano/">Piano Only</option>
</select>
<input alt="Go!" name="submit_system" src="/images/go.gif" style="margin-right: 1em; border: 0; width: 25px; height: 18px; vertical-align: top;" type="image"/>
</form>
</div>
</td>
</tr>
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="0">
<tbody>
<tr>
<td align="center" bgcolor="#CCCCCC" valign="top" width="170">
<p style="line-height: 5px;">
<a href="http://www.ocremix.org/" target="_blank">
<img alt="OCRemix" border="1" height="37" src="images/sites/ocr4_logo_link01.jpg" width="120"/></a>
<br/>
<br/>
<a href="http://www.vgmdb.net/" target="_blank">
<img alt="VGMdb" border="1" height="50" src="images/sites/button_vgmdb_140x50.gif" width="140"/></a>
</p>
<hr/>
<p>
<strong>VGMUSIC™ MENU</strong><br/>
<a href="https://www.vgmusic.com/information/sitemap.php">(Site Map)</a>
</p>
<form action="https://www.vgmusic.com/cgi/random.cgi" id="random_button">
<input name="random_button" type="submit" value="Random Song"/>
</form>
<p style="margin-top:0; font-size:75%;">May not play new-files
	     </p>
<hr/>
<p class="menuhead">
            Information
           </p>
<p class="menu">
<a href="updates/">Archive Updates</a> <strong>11/30/2019
</strong><br/>
<a href="faq/">Freq. Asked Questions</a><br/>
<a href="new-files/">Newly Submitted Files</a><br/>
<a href="new-files/upload/">Upload Files</a><br/>
<a href="https://www.facebook.com/groups/4598068389">Facebook Group</a><br/>
<a href="https://discordapp.com/invite/Jc2FQ5g">Discord Chat</a><br/>
<a href="https://google.com/+vgmusic">Google+ Page</a><br/>
<a href="http://radio.emulationzone.org/chan9/">VGMusic Radio</a><br/>
<a href="information/links.php">Related Links</a><br/>
<a href="information/addlink.php">Linking to Our Site</a><br/>
<a href="information/milestones.php">VGMusic.com Milestones</a><br/>
</p>
<p class="menuhead">
<strong>Music </strong>(31,810)
           </p>
<p class="menularge">
            Nintendo
           </p>
<p class="menu">
<a href="./music/console/nintendo/nes/">NES</a>
            (4,204) 
            <br/>
<a href="./music/console/nintendo/gameboy/">Game Boy</a>
            (2,034) 
            <br/>
<a href="./music/console/nintendo/snes/">SNES</a>
            (6,709) 
            <br/>
<a href="./music/console/nintendo/n64/">N64</a>
            (1,900) 
            <br/>
<a href="./music/console/nintendo/virtualboy/">Virtual Boy</a>
            (8) 
            <br/>
<a href="./music/console/nintendo/gba/">GBA</a>
            (1,768) 
            <br/>
<a href="./music/console/nintendo/gamecube/">GameCube</a>
            (851) 
            <br/>
<a href="./music/console/nintendo/ds/">Nintendo DS</a>
            (733) 
            <br/>
<a href="./music/console/nintendo/3ds/">Nintendo 3DS</a>
            (53) 
            <br/>
<a href="./music/console/nintendo/wii/">Nintendo Wii</a>
            (505) 
            <br/>
<a href="./music/console/nintendo/wiiu/">Nintendo Wii U</a>
            (11) 
            <br/>
<a href="./music/console/nintendo/switch/">Nintendo Switch</a>
            (12) 
            <br/>
</p>
<p class="menularge">
            Sega
           </p>
<p class="menu">
<a href="./music/console/sega/master/">Master System</a>
            (1,585)
            <br/>
<a href="./music/console/sega/gamegear/">Sega Game Gear</a>
            (156)
            <br/>
<a href="./music/console/sega/genesis/">Sega Genesis</a>
            (2,413)
            <br/>
<a href="./music/console/sega/segacd/">Sega CD</a>
            (267)
            <br/>
<a href="./music/console/sega/32x/">Sega 32x</a>
            (51)
            <br/>
<a href="./music/console/sega/saturn/">Sega Saturn</a>
            (440)
            <br/>
<a href="./music/console/sega/dreamcast/">Sega Dreamcast</a>
            (300)
            <br/>
</p>
<p class="menularge">
            Sony
           </p>
<p class="menu">
<a href="./music/console/sony/ps1/">Sony PlayStation</a>
            (2,735)
            <br/>
<a href="./music/console/sony/ps2/">Sony PlayStation 2</a>
            (833)
            <br/>
<a href="./music/console/sony/ps3/">Sony PlayStation 3</a>
            (32)
            <br/>
<a href="./music/console/sony/ps4/">Sony PlayStation 4</a>
            (4)
            <br/>
<a href="./music/console/sony/psp/">Sony PSP</a>
            (37)
           </p>
<p class="menularge">
            Microsoft
           </p>
<p class="menu">
<a href="./music/console/microsoft/xbox/">Xbox</a>
            (58)
            <br/>
<a href="./music/console/microsoft/xbox360/">Xbox 360</a>
            (54)
            <br/>
<a href="./music/console/microsoft/xboxone/">Xbox One</a>
            (0)
           </p>
<p class="menularge">
            NEC
           </p>
<p class="menu">
<a href="./music/console/nec/tg16/">TurboGrafx-16</a>
            (98)
            <br/>
<a href="./music/console/nec/tduo/">Turbo Duo</a>
            (41)
            <br/>
<a href="./music/console/nec/sgx/">SuperGrafx</a>
            (1)
            <br/>
<a href="./music/console/nec/pcfx/">PC-FX</a>
            (1)
           </p>
<p class="menularge">
            SNK
           </p>
<p class="menu">
<a href="./music/console/snk/neogeo/">Neo-Geo</a>
            (233) 
            <br/>
<a href="./music/console/snk/neogeopocket/">Neo-Geo Pocket</a>
            (11) 
            <br/>
</p>
<p class="menularge">
            Atari
           </p>
<p class="menu">
<a href="./music/console/atari/2600/">Atari 2600</a>
            (313) 
            <br/>
<a href="./music/console/atari/7800/">Atari 7800</a>
            (2) 
            <br/>
<a href="./music/console/atari/lynx/">Atari Lynx</a>
            (1) 
            <br/>
</p>
<p class="menularge">
            Others 
           </p>
<p class="menu">
<a href="./music/console/mattel/intellivision/">Mattel Intellivision</a>
            (90)
            <br/>
<a href="./music/console/coleco/colecovision/">Coleco ColecoVision</a>
            (6) 
            <br/>
<a href="./music/console/magnavox/odyssey2/">Magnavox Odyssey2</a>
            (9)
            <br/>
<a href="./music/console/3do/3do/">3DO 3DO</a>
            (8) 
            <br/>
<a href="./music/console/philips/cd-i/">Philips CD-i</a>
            (9) 
           </p>
<p class="menuhead">
            Computer Systems
           </p>
<p class="menu">
<a href="./music/computer/microsoft/windows/">Dos &amp; Windows</a>
            (720) 
            <br/>
<a href="./music/computer/commodore/commodore/">Commodore 64</a>
            (317) 
            <br/>
<a href="./music/computer/miscellaneous/msx/">MSX Computer</a>
            (106) 
            <br/>
<a href="./music/computer/atari/atari/">Atari Computers</a>
            (101) 
            <br/>
<a href="./music/computer/commodore/amiga/">Amiga Computer</a>
            (153) 
            <br/>
<a href="./music/computer/amstrad/amstradcpc/">Amstrad CPC</a>
            (5) 
            <br/>
<a href="./music/computer/apple/appleii/">Apple II</a>
            (1) 
            <br/>
<a href="./music/computer/apple/macintosh/">Apple Macintosh</a>
            (16) 
            <br/>
<a href="./music/computer/nec/pc-88/">NEC PC-88</a>
            (13) 
            <br/>
<a href="./music/computer/nec/pc-98/">NEC PC-98</a>
            (22) 
            <br/>
<a href="./music/computer/tomy/tutor/">Tomy Tutor</a>
            (1) 
            <br/>
<a href="./music/computer/sharp/x68000/">Sharp X68000</a>
            (8) 
            <br/>
<a href="./music/computer/sinclair/spectrum/">Sinclair Spectrum</a>
            (5) 
           </p>
<p class="menuhead">
	    Miscellaneous
           </p>
<p class="menu">
<a href="./music/other/miscellaneous/arcade/">Arcade</a>
            (810) 
           <br/>
<a href="./music/other/miscellaneous/medley/">Medleys</a>
            (234) 
           <br/>
<a href="./music/other/miscellaneous/piano/">Piano Only</a>
            (721)
           <br/>
<a href="./music/other/miscellaneous/comedy/">Comedy &amp; Memes</a>
            (0)
           </p>
</td>
<!-- END OF LEFT COLUMN -->
<td align="left" valign="top">
<a class="twitter-timeline" data-chrome="nofooter transparent" data-dnt="true" data-widget-id="492493186472763392" href="https://twitter.com/vgmusic">Tweets by @vgmusic</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<hr/>
<h2>Contact Information</h2>
            Who are we? Find out more on our <a href="information/staff.php">Staff</a> page.
            <br/>
            Before emailing us, we would appreciate if you read our
            <a href="faq/">Frequently Asked Questions</a> page.
            <br/>
            Comments, compliments, problems, complaints, or messages 
            about broken links can be sent to the <a href="mailto:siteadmin@vgmusic.com?subject=VGMA%20Problem:">Webmasters</a>.
            <br/>
            Open staff <a href="information/positions.shtml">positions</a>.
           

          <table width="100%">
<col width="34%"/>
<col width="33%"/>
<col width="33%"/>
<tbody>
<tr>
<th>
<p class="smallpoint">
<strong>Position</strong>
</p>
</th>
<th>
<p class="smallpoint">
<strong>Name</strong>
</p>
</th>
<th>
<p class="smallpoint">
<strong>Email</strong>
</p>
</th>
</tr>
<tr id="staff_mike">
<td>
<p class="smallpoint">
               Founder, Finance/Advertising Administrator
              </p>
</td>
<td>
<p class="smallpoint">
               Mike Newman (Yaginuma)
              </p>
</td>
<td>
<p class="smallpoint">
<img alt="Mike's email address" src="images/mbn_email.png"/><br/><a href="https://www.vgmusic.com/~mike/an_analytical_look_at_spam.html">Spam be gone!</a>
</p>
</td>
</tr>
<tr id="staff_shane">
<td>
<p class="smallpoint">
               Webmaster, System Administrator
              </p>
</td>
<td>
<p class="smallpoint">
               Shane Evans (shan)
              </p>
</td>
<td>
<p class="smallpoint">
<img alt="shan's email address" src="images/shanmail.png"/>
</p>
</td>
</tr>
<tr id="staff_mark">
<td>
<p class="smallpoint">
            Updater
              </p>
</td>
<td>
<p class="smallpoint">
               Mark Carroll (TurquoiseStar17)
              </p>
</td>
<td>
</td>
</tr>
<tr id="staff_jace">
<td>
<p class="smallpoint">
            Updater
              </p>
</td>
<td>
<p class="smallpoint">
               Jace Hill
              </p>
</td>
<td>
</td>
</tr>
<tr id="staff_daylon">
<td>
<p class="smallpoint">
            Updater
              </p>
</td>
<td>
<p class="smallpoint">
							Daylon Camarena (Chimeratio)
              </p>
</td>
<td>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<hr/>
<p align="center" class="copyright">
Archive Established on: <em>Thu Dec 19 1996</em><br/>
Changes last made on: <em>August 26 2020 17:06:40.</em><br/>
Front Page Designed by <a href="mailto:mrs@post7.tele.dk">Morten "Sumez" Riis Svendsen</a><br/>
HTML Code Copyright © 2017 Video Game Music Archive<sup>SM</sup><br/>
VGMusic™ is an unregistered trademark of the Video Game Music Archive<sup>SM</sup><br/>
<a href="privacy.php">Privacy Policy</a><br/>
</p>
<p align="center" class="copyright">
All music files on this site, including those within zip files, are copyrighted by their respective authors.
If you wish to use some of the music files you find on this site in a project, on another website,
for some other purpose, you should contact the author of the files you wish to use. For most files,
this is listed in our archive in the information text file for each file.  Please do not link directly
to the music files on this site, but instead to the main page.
       </p>
<p align="center" class="copyright">
All other information on this website is copyrighted and should not be used on any other page without 
first obtaining our express permission.
More information is available on our <a href="information/legal.php">Terms of Service</a> page<a href="mailto:hmmmmm@vgmusic.com">.</a>
</p>
</td>
</tr>
</tbody>
</table>
</div>
</body>
</html>
