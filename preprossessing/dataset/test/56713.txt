<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Fednav - A full outline of this Canadian shipowner and operator's activities.</title>
<meta content="Business, Transportation and Logistics, Maritime, Ship Owners and Management - Fednav. A full outline of this Canadian shipowner and operator's activities." name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/resources/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/resources/A.style.css+ui.css,Mcc.jEkV5vj9E0.css.pagespeed.cf.czmHp5pWPd.css" rel="stylesheet" type="text/css"/>
</head>
<script language="javascript" src="/resources/scripts.js"></script>
<body bgcolor="white">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="760">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" class="header" width="100%">
<tr>
<td rowspan="2"><a href="https://www.abc-directory.com/"><img border="0" height="84" src="/resources/logo1.gif" width="205"/></a></td>
<td align="right" width="100%"><table border="0" cellpadding="3" cellspacing="0" class="top-nav">
<tr align="center">
<td><a href="https://www.abc-directory.com/"><img border="0" height="10" src="/resources/ico_home.gif" width="11"/></a></td>
<td> </td>
<td><a href="javascript:setHomepage()"><img border="0" height="11" src="/resources/ico_homepage.gif" width="9"/></a></td>
<td> </td>
<td><a href="javascript:AddToFavorites('https://www.abc-directory.com/site/810824', 'Fednav - A full outline of this Canadian shipowner and operator\'s activities.')"><img border="0" height="10" src="/resources/ico_favorite.gif" width="9"/></a></td>
<td> </td>
<td><a href="https://www.abc-directory.com/contactus/"><img border="0" height="8" src="/resources/ico_mail.gif" width="12"/></a></td>
<td> </td>
</tr>
<tr align="center">
<td><a class="topmenu" href="https://www.abc-directory.com/">Home</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="https://www.abc-directory.com/submiturl">Submit URL</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="javascript:AddToFavorites('https://www.abc-directory.com/site/810824', 'Fednav - A full outline of this Canadian shipowner and operator\'s activities.')">Add to Favorite</a></td>
<td><img height="10" src="/resources/blue.gif" width="1"/></td>
<td><a class="topmenu" href="https://www.abc-directory.com/contactus/">Contact</a></td>
<td></td>
</tr>
</table></td>
</tr>
<tr>
<td valign="bottom">
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="bottom" width="100%">
<tr>
<td width="50%">
</td>
<td>
<a class="titleondark" href="https://www.abc-directory.com/"><img border="0" src="/resources/catalogue_over.gif"/></a>
</td>
<td> </td>
<td>
<a class="titleondark" href="https://article.abc-directory.com/"><img border="0" src="/resources/articles.gif"/></a>
</td>
<td> </td>
<td>
<a class="titleondark" href="https://press.abc-directory.com/"><img border="0" src="/resources/press.gif"/></a>
</td>
<td>      
                </td>
<td align="left" nowrap="" valign="top" width="110">13 January, 2021</td>
</tr>
</table>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" class="search" width="100%">
<tr>
<td><img height="15" src="/resources/search_tl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_tr.gif" width="15"/></td>
</tr>
<tr>
<td colspan="2" width="100%">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<form id="frm_search" method="post" name="frm_search" onsubmit="return Search(true, 'http://search.abc-directory.com/', 'frm_search');">
<tr>
<td width="100%"></td>
<td align="right" class="titleondark" nowrap=""><i>search for</i></td>
<td align="right"><input name="edt_search_text" type="text" value=""/></td>
<td align="right"><a href="javascript:Search(false, 'http://search.abc-directory.com/', 'frm_search')"><img border="0" src="/resources/but_search.gif"/></a></td>
</tr>
</form>
</table>
</td>
<td> </td>
</tr>
<tr>
<td><img height="15" src="/resources/search_bl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_br.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td> 
          </td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="content" width="100%">
<tr>
<td class="sidebar" valign="top">
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i>Categories</i></td>
<td><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td2" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/2" id="td2_a">Arts</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/2">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td3" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/3" id="td3_a">Business</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/3">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td4" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/4" id="td4_a">Computers</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/4">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td5894662" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/5894662" id="td5894662_a">Education</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/5894662">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td5894661" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/5894661" id="td5894661_a">Entertainment</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/5894661">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td6" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/6" id="td6_a">Health</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/6">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td7" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/7" id="td7_a">Home</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/7">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td471237" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/471237" id="td471237_a">Kids and Teens</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/471237">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td8" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/8" id="td8_a">News</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/8">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td9" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/9" id="td9_a">Recreation</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/9">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td10" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/10" id="td10_a">Reference</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/10">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td12" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/12" id="td12_a">Science</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/12">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td13" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/13" id="td13_a">Shopping</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/13">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td14" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/14" id="td14_a">Society</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/14">»</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="td15" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><a class="directorieslink" href="/category/15" id="td15_a">Sports</a></td>
<td align="right" class="derictoriesmarks"><a class="directorieslink" href="/category/15">»</a></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/><br/> <table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i class="titlebox">Premium Listings</i></td>
<td><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689788" id="tds_a">Chris Adams Personal Training</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689786" id="tds_a">Spray Foam Insulation Greenville SC</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689784" id="tds_a">Berman, Sobin, Gross, Feldman &amp; Darby LLP</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689782" id="tds_a">Estes Services</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689780" id="tds_a">Ester Digital</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689778" id="tds_a">Biggles Removals</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689776" id="tds_a">Mobility Elevator &amp; Lift Co.</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689774" id="tds_a">Alcohol Free &amp; Non Alcoholic Beer, Cider, Wine &amp; Spirits</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689770" id="tds_a">United Roofing Inc.</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689766" id="tds_a">Gemstone Lights</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689772" id="tds_a">Best Organic Spices and Herbs | Organic Essential Oils and Teas</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689768" id="tds_a">Union Alarm</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689764" id="tds_a">Litwiller Renovations &amp; Custom Homes</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689762" id="tds_a">Agrinote Holdings</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="directorieslink" href="/site/4689760" id="tds_a">Fracas Digital</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/><br/>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="170">
<tr>
<td valign="top"><img height="27" src="/resources/green_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><i>New Addition</i></td>
<td valign="top"><img height="27" src="/resources/green_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#8EA076" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><table border="0" cellpadding="5" cellspacing="1" width="100%">
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689788">Chris Adams Personal Training</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689786">Spray Foam Insulation Greenville SC</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689784">Berman, Sobin, Gross, Feldman &amp; Darby LLP</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689782">Estes Services</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689780">Ester Digital</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689778">Biggles Removals</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689776">Mobility Elevator &amp; Lift Co.</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689774">Alcohol Free &amp; Non Alcoholic Beer, Cider, Wine &amp; Spirits</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689770">United Roofing Inc.</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689766">Gemstone Lights</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689772">Best Organic Spices and Herbs | Organic Essential Oils and Teas</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689768">Union Alarm</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689764">Litwiller Renovations &amp; Custom Homes</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689762">Agrinote Holdings</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689760">Fracas Digital</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689758">Fort, Holloway, &amp; Rogers</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689754">The Law Offices of Bryan R. Kazarian</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689752">Houston &amp; Alexander, PLLC</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689750">Hess Plastic Surgery: Christopher L. Hess, MD</a></td>
</tr></table>
</td></tr>
<tr><td bgcolor="#FFFFFF" class="topmenu" id="tds" onclick="do_click(this)" onmouseout="smenu_out(this)" onmouseover="smenu_hover(this)">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
<td><a class="directorieslink" href="/site/4689748">Dennis and King</a></td>
</tr></table>
</td></tr>
</table></td></tr>
</table>
<br/><br/>
</td>
<td>    </td>
<td class="content-text" valign="top" width="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<div class="site">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<a class="linkblue" href="/category/3">Business</a> »                        <a class="linkblue" href="/category/765130">Transportation and Logistics</a> »                        <a class="linkblue" href="/category/4688">Maritime</a> »                        <a class="linkblue" href="/category/4714">Ship Owners and Management</a> <br/><br/>
</td>
<td align="right" nowrap="">
<a class="submenu" href="/update/810824">UPDATE INFO</a> | <a class="submenu" href="/broken/810824">REPORT BROKEN LINK</a>
</td>
</tr>
<tr>
<td width="100%">
<h1 class="titleonwhite"><i>Fednav</i></h1>
</td>
<td nowrap="">
<span class="text">Popularity: <img src="/resources/pop1.gif"/>   Hit: 597</span>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><span><i>Details:</i></span></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
<table border="0" cellpadding="5" cellspacing="1" class="site-content" width="100%">
<tr>
<td bgcolor="#FFFFFF" valign="top">
</td>
<td bgcolor="#FFFFFF" valign="top" width="100%">A full outline of this Canadian shipowner and operator's activities. A high-tech site that rewards user patience.
					</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">URL</span>:</td>
<td bgcolor="#FFFFFF" valign="top">
<a class="linkblue detdesc" href="http://www.fednav.com" id="hrefSite" rel="nofollow" target="_blank">http://www.fednav.com</a>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Title</span>:</td>
<td bgcolor="#FFFFFF" class="detdesc" valign="top">Fednav Limited / Fednav Limitee</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Image</span>:</td>
<td align="center" bgcolor="#FFFFFF" class="detdesc" valign="top"><img height="400" src="https://images.abc-directory.com/810824-2.png" width="440"/></td>
</tr>
<tr>
<td bgcolor="#FFFFFF" valign="top"><span class="texttitle">Description</span>:</td>
<td bgcolor="#FFFFFF" class="detdesc" valign="top">Business, Transportation and Logistics, Maritime, Ship Owners and Management - Fednav. A full outline of this Canadian shipowner and operator's activities.</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" class="texttitle" valign="top">Similar: </td>
<td bgcolor="#FFFFFF" valign="top">
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Related Sites </i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="/site/810739">Ferry Companies of the Web</a> (Popularity: <img src="/resources/pop1.gif"/>): A whole site dedicated to ferry companies represented on the web.<br/>
<a class="linkblue" href="/site/810744">Sea Containers</a> (Popularity: <img src="/resources/pop1.gif"/>): Registered in Bermuda, the company provides passenger transport, leisure and marine container leasing.  We also engages in the associated businesses  ...<br/>
<a class="linkblue" href="/site/810746">Brittany Ferries</a> (Popularity: <img src="/resources/pop1.gif"/>): Ferry service between Ireland, UK, France and Spain.<br/>
<a class="linkblue" href="/site/810748">HH-Ferries</a> (Popularity: <img src="/resources/pop1.gif"/>): Swedish ferry service operating between Sweden and Denmark.<br/>
<a class="linkblue" href="/site/810749">Mols-Linien</a> (Popularity: <img src="/resources/pop1.gif"/>): Danish ferry service between Odden and Ebeltoft in Denmark.<br/>
<a class="linkblue" href="/site/810752">Smyril Line</a> (Popularity: <img src="/resources/pop1.gif"/>): Cruise the North Atlantic, visiting the Faroe Islands, Iceland and the Shetland Islands.<br/>
<a class="linkblue" href="/site/810753">Superfast Ferries</a> (Popularity: <img src="/resources/pop1.gif"/>): Maritime company offering passengers service in the Adriatic, Baltic, and North Sea regions.<br/>
<a class="linkblue" href="/site/810754">Anek Lines</a> (Popularity: <img src="/resources/pop1.gif"/>): Passenger ships that offers you services in Greek Domestic lines and International lines between Greece and Italy.<br/>
<a class="linkblue" href="/site/810757">Virtu Ferries</a> (Popularity: <img src="/resources/pop1.gif"/>): Operators of High Speed car and passenger ferries,  specialising on the  Malta - Sicily route.  Travel in comfort at up  ...<br/>
<a class="linkblue" href="/site/810761">Agoudimos Lines</a> (Popularity: <img src="/resources/pop1.gif"/>): Daily car and passenger ferries connecting Igoumenitsa to Brindisi.<br/>
<a class="linkblue" href="/site/810823">Samsun Corporation</a> (Popularity: <img src="/resources/pop1.gif"/>): Provides shipping (worldwide tramper), trading, logistics, ferro-alloys and stainless steel products.  Offices in numerous port cities.  [English, Korean]<br/>
<a class="linkblue" href="/site/810830">Herning Shipping A/S</a> (Popularity: <img src="/resources/pop1.gif"/>): Dealing in world-wide chartering of owned tonnage, management tonnage and time-chartered tonnage; LPG, chemical and product tankers.<br/>
</td>
</tr>
</table>
<br/>
<br/>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><img height="27" src="/resources/blue_left.gif" width="15"/></td>
<td class="titleondark" width="100%"><font class="sitetitle"><i>Popular Sites</i></font></td>
<td><img height="27" src="/resources/blue_right.gif" width="15"/></td>
</tr>
</table>
<table border="0" cellpadding="5" cellspacing="0" class="categoriesborder" width="100%">
<tr>
<td bgcolor="#FFFFFF">
<a class="linkblue" href="/site/810931">Reederei NORD Klaus E. Oldendorff</a> (Popularity: <img src="/resources/pop1.gif"/>): It is a family-owned international shipowning and shipmanagement company with offices in Hamburg, Germany and Limassol, Cyprus. It controls a  ...<br/>
<a class="linkblue" href="/site/810901">Niver Lines Shipping Company</a> (Popularity: <img src="/resources/pop1.gif"/>): Niver Lines is a Greek-based shipping company operating between East Coast South America and various Mediterranean ports.<br/>
<a class="linkblue" href="/site/810966">International Salvage Union</a> (Popularity: <img src="/resources/pop1.gif"/>): Provides information on the ISU and other member companies, including sections on salvage: an overview, casualty salvage, pollution defence, and  ...<br/>
<a class="linkblue" href="/site/810843">Qatar Shipping Company</a> (Popularity: <img src="/resources/pop1.gif"/>): Providing liquid and dry bulk shipping and transportation fleet worldwide.<br/>
<a class="linkblue" href="/site/810991">Hafar Marine (Singapore &amp; Indonesia)</a> (Popularity: <img src="/resources/pop1.gif"/>): Operating in Southeast Asia for charter towage or sale. Sea and inland waterway freight transport in Indonesia, Philippines, Thailand, and  ...<br/>
</td>
</tr>
</table>
<br/>
<br/>
<br/>
<br/>
</div></td>
</tr>
</table>
</td>
</tr>
</table>
<table bgcolor="#0071BC" border="0" cellpadding="0" cellspacing="0" class="footer" width="100%">
<tr>
<td><img height="15" src="/resources/search_tl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_tr.gif" width="15"/></td>
</tr>
<tr>
<td> </td>
<td width="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr align="center">
<td class="copy">
<a class="bottommenu" href="https://www.abc-directory.com/">Home</a>
                  | <a class="bottommenu" href="#">Top</a>  | <a class="bottommenu" href="javascript:;" onclick="this.style.behavior='url(#default#homepage)'; this.setHomePage('https://www.abc-directory.com/');">Set
                  as Homepage</a> |
                  <a class="bottommenu" href="javascript:AddToFavorites('https://www.abc-directory.com/site/810824', 'Fednav - A full outline of this Canadian shipowner and operator\'s activities.')">Bookmark this Page</a>
| <a class="bottommenu" href="https://www.abc-directory.com/privacypolicy/">Privacy</a>
 | <a class="bottommenu" href="https://www.abc-directory.com/banners/">Banners</a>
 | <a class="bottommenu" href="https://www.abc-directory.com/contactus/">Contact</a>

                  | <a class="bottommenu" href="https://www.abc-directory.com/submiturl">Submit Site</a>
<br/>
                  © 2003-2019, ABC-Directory.Com. All Rights Reserved
                </td>
</tr>
</table>
</td>
<td> </td>
</tr>
<tr>
<td><img height="15" src="/resources/search_bl.gif" width="15"/></td>
<td> </td>
<td><img height="15" src="/resources/search_br.gif" width="15"/></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>