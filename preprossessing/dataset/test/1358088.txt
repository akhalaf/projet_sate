<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "23727",
      cRay: "6108e9e49e2a1a2a",
      cHash: "009279f5162df37",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuc3BsaXR2encuYmUvY29tcG9uZW50cy9jb21fbWVkaWEvaW1hZ2VzL2Jtby5jb20vQk1PL0JNTy9CTU8vQk1PL0JNTy9CTU8vQk1PL0JNTy9ibW8vJTA5JTBB",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "/Mq+xvpFgyIJarykcfllwPD39qugR2b4UzPeSDdmRs9NpVvfXD9U06i8QYZGm3sH+4SvPElxXNlOCV7rKFIIHTraxKmjnexaZ6Kb/ruCPnKnlXHQTHHn8gmnVz1WWYG+40GaWtz636/HDqk2eSkpXrzy/VotZJOdBPaFrvt9J6j13GXQpEpVWRTwoCY1CsTK6YY+8bN20bQM3nRQ1YCj+rkTdYuO0WE1WGjMNiFLdxSV1m9lqBaV6yv38TUzKRc2J6wPgGqm2iX0vic7hlQW2HKglYfsoKib55N2B101YTBB/2IaK11it82oEO7KGt1HNqQzAwEYIZyAitqokW0Y1JtKeww2pwnYHZHaXSM0FBF8UBw6zdHWCg19JTBcxXvZlhMzmmORpwBpD99nxsGiEoIzaXOMxYhEHe/nQGpuEzBCuz22sJiSc4GUFUNO+P6KDmwHhJcuiIxgc47bVSvmHowFjqYbyyrlHHlSr+vA1J25XQrJJFgl9aEBCdm0vjCJntKsmCiR/DeG6tYhpvviue+sRqOus7552+KFlaKf4ra3Z+b7EYzP3VcIeobm0ZvMky4esUnvnpMbrzCp5fcfNMngqNFu3lMKeHgAQMMwGticGdDUWJyRLLzqqz1Zz/frBzB4wF0t1Kv5GyKul4E1ikLMI3WD4vmmTjAGJ8s0lRLlF0uIEqQN5HROBFAmRgP1NK4+hJ60tZ/RMKPVvFL5BdB2zqHvfiqS4B6I8LyYmDW1dDxbpgVqOeIXhAIv/bgYv571f4pt2SdqdcfIZIeJtw==",
        t: "MTYxMDQ3NTg1OC42NjUwMDA=",
        m: "m0gPx868FZxzU+xofAqQqFoH8OwZf0HFuX+PsTQr1KE=",
        i1: "uwwY99fEeMW2GFvSX74Idw==",
        i2: "mWWMLY51LcmFkGaAADs5Pg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "TYCQDwAicyaf9KqKWfKlUkPemELmNeZkS8OAdE1C4e0=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.splitvzw.be</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/components/com_media/images/bmo.com/BMO/BMO/BMO/BMO/BMO/BMO/BMO/BMO/bmo/%09%0A?__cf_chl_captcha_tk__=a4f751865e7537bdca3105477fccce0389a3d9e8-1610475858-0-ASauVIXSGe0mbi0pwia3fuNxIenxubtJqqNBj5H42FgGn8zStSC7EMhmt1WCezh1xN8vfLZxf2wMc6PBXX2FzyqrYsxGPQDL8Yi62x8hPfZsIjXLjv88AmQo8cCoZ1vK-9ik9n0ZXzbe5L_MeOxBl9kg6XS_2hYGlwIsG3_g-UwiK4OWrHBb3gqk31JL76tHo2gjB6tFqgN8U0Vu_Ex8y2zJ5FWik_Y4XL8_2N7fG70wYkdNrjJnzV6-P9mQRLSe5IhAxf5pBjyxIv70-Nvv2YO9dlYHUuiYWerpf665uJKws63-ESRRWJpLJbi1Gm3VgVkzLoq2YeNXtE439jRfc_cshilygmY0FiBdnOMb8Hl53lg5C_WIaO9XDn3GdwtgsZGplzrrsKASWkCi0ayZf5OKvNdgm6xm6Qkntb2HBKdaaAtJXvTxQlWkr5pfQbTmncBuISnijzuZcwaGvr8Dm1Gayr2HNcUAXNs1LCrWmKOaNeLxfBvUW51VPPtdOHEhexz-ZsxM93tR6yU-9KhYYUD2KhV4xcGmED5a4QpsJOcsZGu-kpZ6Uq71SKbqOEtBGHGHacU9KI62jKDZvUDCOCBFJXEQuXrkgysRYaFVF8U4NW_Yy5nySeXDqDT_VCB4fl1ixEUgCKAgihS4Y_XNbminIdZHyNP8JOhrug8O7YQA-ZYO6xRKwbDAx5JhLhfG0w" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="094dd855a59b9c219778c712e57838c9fcd8bc5c-1610475858-0-AWfMCSIVLOie87UcTjHao370ysT5LDjhRxPJ+LEFntHKpYaMU4BzVvKLgZ5ObI0ueG2U52E4PGz+MuJAbTXS6zaya1stIudx4MjS6mtla84UXCCpYwn8vZQ28LOS5nUL6jEkK62NXszJFX7c+75rcysU0wbJPU2SvQHqHesnoLgwBvki0kDqKLPnuuWF9uFx1f8RAh6bXeI24Oi4Yz2DNa9859IVC8yXJIRTBpE8VESCbsGVfhq5nVMaEf4f4imoec9VbqOxzZkuhDlrjaH66Ri7v0SM5zlvAtMJtXbGZHDnnKdlTAIPEHWog02iI/02+XdkL7zSTJKLr+7wmUB+4037v6XO/ekJOB2jyS3WCqWGjU3Nq4AyQ+sKsorLqLnLXFzO1FXaJKyrl52xosKy5QH6NVWWwOuTEm+D0fh94SgB9DjFDiayindOKprrednAChOIHTHMi5qNON+HIgls/V0uo0Z51cqc8IcIkFN3NFoAAOMRGX8apV8TfSn0VBX20DXKQhr4guPElJig3Rd1Wvj7Vbjglg99/LSnpM76TIQBNhTEpyRO/LQMCe9NIJ81rBkCZTNkmD9XsP9mt9ZA+j099LP0LEpkhJJxoFrOWJXlxEaAxAnYsiwWQ7shCSPh8WGAhf3ANwGlRkOMFroFfzPZKqkN5z0PiyJa5BpehYhhZE7LZV+Bpl1D8b75SDYsipLEd7/cFEOr0nPWE3qlFy3huV09ZsA05la1ey6cUDLBGFzhw+WyAbC7cWB6LERrPBmjf9HwBCUfKheBkcgLwo5ev2lCNVjAAlxYKGqVOLuBM5NDWcGEo+3NZxz6YNlGpzhKG0PfG0l+fb/DqxgZG84n9/6QfgEtCQx+xRgnoaCZ9+owewp4WtCLJ1jYWN9BPBql4QACVRT2yfn0/wDuqbIIip1LDUy7pgLWn8TwxLNBO2GR32qMso1xJLurh0IvrFzGc3zmAsbuMkirrl6JhE5jxw1TEclQIw60MDsYEuD6c8NwjlQxnx7oz3I93VYnr/sEBipPXKQiAvGmTyunQCDUV4WDEYCAzRDZpXBEr3Bh0mYf63reJhSzYF2o5g14ArlvBiiaKuiLaAot6mWpTV37T8SSZY7ecqL/fFt5dFQtWX2FlyLqzcQ3MYwnxr3F7hSeM/K7bqG1aD+Dk8z/WFCmUiogIUCxbj0JlBxFuTP+R4FIGTJ+vzg5NrtCrlaL+9OUjqCtC9OQixvvHTUewd6CfY/Y80acjqbMAEPz3hmm0pgn4lS4xXzgFAk27Wy7SWmhgS6DM/GOnjtiuIaWt5Wf6TEDoxMUq+Qzc7TFqscWlqHNqiUl/ZYPXOIGgIFI9g=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="6b202a0cec4dc4198fcff69389550a4b"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=6108e9e49e2a1a2a')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<!-- <a href="https://beatlemail.net/picture.php?blogid=0">table</a> -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">6108e9e49e2a1a2a</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
