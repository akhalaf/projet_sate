<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<title>ScreenshotMachine — Page not found</title>
<!-- Styles -->
<link href="/assets/css/page.min.css" rel="stylesheet"/>
<link href="/assets/css/style.css" rel="stylesheet"/>
<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon"/>
</head>
<body class="layout-centered bg-gray">
<!-- Main Content -->
<main class="main-content text-center pb-lg-8">
<div class="container">
<h1 class="display-1 text-muted mb-7">Page Not Found</h1>
<p class="lead">Seems you're looking for something that doesn't exist.</p>
<br/>
<button class="btn btn-secondary w-200 mr-2" onclick="window.history.back();" type="button">Go back</button>
<a class="btn btn-secondary w-200" href="/">Return Home</a>
</div>
</main><!-- /.main-content -->
<!-- Scripts -->
<script src="/assets/js/page.min.js"></script>
<script src="/assets/js/script.js"></script>
</body>
</html>