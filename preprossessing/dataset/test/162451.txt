<!DOCTYPE html>
<html lang="pt-br" xml:lang="pt-br" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="pt_BR" http-equiv="content-language"/>
<title>Assistir filmes online grátis - Ver filmes online em português</title>
<meta content="Assistir filmes online grátis em português (HD, BDRip, DvdRip)." name="description"/>
<meta content="filmes, online, gratis, portugues, hd, bdrip, dvdrip, streaming, youtube, completo, dublados, legenda, ver" name="keywords"/>
<link href="https://hdfilmes.co/engine/opensearch.php" rel="search" title="Assistir filmes online grátis - Ver filmes online em português" type="application/opensearchdescription+xml"/>
<link href="https://hdfilmes.co/page/2" rel="next"/>
<link href="https://hdfilmes.co/rss.xml" rel="alternate" title="Assistir filmes online grátis - Ver filmes online em português" type="application/rss+xml"/>
<link href="//ajax.googleapis.com" rel="dns-prefetch"/>
<link href="//www.googletagmanager.com" rel="dns-prefetch"/>
<link href="//cdn.sendpulse.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="/templates/assistirhdonline/images/favicon.ico" rel="shortcut icon"/>
<link href="/engine/classes/min/index.php?charset=utf-8&amp;f=/templates/assistirhdonline/css/styles.css,/templates/assistirhdonline/css/reset.css,/templates/assistirhdonline/css/slider.css,/templates/assistirhdonline/css/engine.css,/templates/assistirhdonline/css/popup.css,/templates/assistirhdonline/css/jquery-ui-1.8.7.custom.css&amp;7" rel="stylesheet"/>
<script async="" src="//yiefp.chfpgcbe.com/v/gHkFpzRNwC0FJxizasvFYn6kYHy3WA" type="text/javascript"></script>
<style>
.ttgb-top {
width: 100%;
left: calc(((100%) - 950px)/2);
z-index: 0;
display: block;
margin: 0 auto;
}

#click1 {
width: 100%;
height: 100%;
display: block;
margin: 0 auto;
}
.image-header {
    margin: 0 auto 0 auto;
    display: block;
}
.ttgb-img-lg, .ttgb-img-sm {
max-width: 100%;
height: auto;
border-bottom-color: rgb(76, 71, 67);
border-bottom-style: solid;
border-bottom-width: 1px;
}
</style>
</head>
<body>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-72891120-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-72891120-4');
</script>
<div id="header">
<div class="width">
<div class="logo-top">
<a href="/" title="Assistir filmes online gratuitamente em HD">
<img alt="hdfilmes.co - Assistir filmes online gratuitamente em HD" src="/templates/assistirhdonline/images/logo.png"/>
</a>
</div>
<div class="search-select">
<form action="" class="tophead-search" id="quicksearch" method="post" name="searchform">
<fieldset>
<span class="text quicksearch">
<input autocomplete="off" class="reset sinput tophead-search-txt" id="story" name="story" onblur="if(this.value=='') this.value='Pesquisa ...';" onfocus="if(this.value=='Pesquisa ...') this.value='';" type="text" value="Pesquisa ..."/>
</span>
<input class="go" title="encontrar" type="submit" value=""/>
<input name="do" type="hidden" value="search"/>
<input name="subaction" type="hidden" value="search"/>
</fieldset>
</form>
<div class="clear"></div>
<div class="primer">
Exemplo: <span onclick="document.getElementById('story').value='Avatar';  return false;">Avatar</span>
</div>
</div>
</div>
</div>
<div class="bg-site">
<div class="bg-wrapper">
<a href="https://webpuppweb.com/htjrpypz/?subId1=hdfilmesco" id="click1" rel="nofollow" target="_blank">
<div class="ttgb-top">
<img alt="adsslot-min" class="ttgb-img-lg image-header" src="/uploads/adsslot-min.png"/>
</div>
</a>
<div id="top-menu">
<div class="width">
<ul class="li-t-menu">
<li><a href="/">Pagina Inicial</a>
</li>
<li><a href="/policial/" rel="nofollow">Policial</a></li>
<li><a href="/faroeste/" rel="nofollow">Faroeste</a></li>
<li><a href="/guerra/" rel="nofollow">Guerra</a></li>
<li><a class="act-h-m" href="/series/" rel="nofollow">Séries</a></li>
<li><a href="/filmes-2017/" rel="nofollow">Filmes 2017</a> </li>
<li><a class="act-h-m" href="/xfsearch/Português/" rel="nofollow">Todos os filmes</a></li>
</ul>
</div>
</div>
<br/>
<div id="wrapper">
<div class="main">
</div>
</div>
<div class="allblockmainimg">
<div class="catblockmain">
<span class="allcatmain">409 filmes</span>
<a href="/filmes-2019/" title="Lançamentos - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/1-filmes-2019.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Lançamentos </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">709 filmes</span>
<a href="/filmes-2018/" title="Lançamentos 2018 - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/2-filmes-2018.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Lançamentos 2018 </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">100 filmes</span>
<a href="/filmes_populares/" title="Top filmes em 90 dias - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/3-filmes-populares.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Top filmes em 90 dias </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">2452 filmes</span>
<a href="/comedia/" title="Comédia - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/4-comedia.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Comédia </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">250 filmes</span>
<a href="/animao/" title="Animação - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/5-animao.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Animação </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">760 filmes</span>
<a href="/aventura/" title="Aventura - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/6-aventura.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Aventura </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">3464 filmes</span>
<a href="/drama/" title="Drama - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/7-drama.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Drama </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">1110 filmes</span>
<a href="/suspense/" title="Suspense - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/8-suspense.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Suspense </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">840 filmes</span>
<a href="/terror/" title="Terror - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/9-terror.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Terror </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">2237 filmes</span>
<a href="/agao/" title="Ação - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/10-agao.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Ação </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">340 filmes</span>
<a href="/crime/" title="Crime - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/11-crime.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Crime </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">321 filmes</span>
<a href="/fico-cientfica/" title="Ficção Científica - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/12-fico-cientfica.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Ficção Científica </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">250 filmes</span>
<a href="/romance/" title="Romance - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/13-romance.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Romance </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">117 filmes</span>
<a href="/biografia/" title="Biografia - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/14-biografia.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Biografia </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">622 filmes</span>
<a href="/documentrio/" title="Documentário - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/15-documentrio.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Documentário </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">146 filmes</span>
<a href="/policial/" title="Policial - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/16-policial.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Policial </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">98 filmes</span>
<a href="/faroeste/" title="Faroeste - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/17-faroeste.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Faroeste </div></div>
</a>
</div>
<div class="catblockmain">
<span class="allcatmain">74 filmes</span>
<a href="/guerra/" title="Guerra - Filmes Online">
<div style="background-color: darkkhaki;background-image: url('/templates/assistirhdonline/images/mainimg/18-guerra.jpg');width: 320px;height: 180px;"></div>
<div class="divtitleblocks">
<div class="titlecatnames">
Guerra </div></div>
</a>
</div>
</div>
<div class="clear"></div>
<div class="color-hr"></div>
<div class="top-txt">
<h1>Assistir filmes Online Gratis em Português</h1>
<div>
Como assistir filmes? Em nosso site você pode assistir online os melhores filmes de graça. Nosso portal cinema tem uma grande base de mega filmes em Português com legendas em Inglês e Português, e espanhol. Além disso, temos uma grande coleção de desenhos animados estrangeiros e filmes em qualidade HD. Nosso site é atualizado e cheio de mega filmes online frescos todos os dias! Só aqui você pode assistir on-line como HD, BDRip, DvdRip enorme coleção de filmes em Português.
</div>
</div>
<div class="clear"></div>
<div class="menumain" style="height: 205px; background-color: #fff; box-shadow: 0 0 20px 0 rgba(0,0,0,.1); padding: 10px;">
<ul class="catmainlink" style="width: 49.7%;float:left;border-right: 3px solid #ffd000;;">
<li><a href="/xfsearch/EUA/" rel="nofollow">EUA</a></li>
<li><a href="/xfsearch/Reino Unido/" rel="nofollow">Reino Unido</a></li>
<li><a href="/xfsearch/Franca/" rel="nofollow">Franca</a></li>
<li><a href="/xfsearch/Alemanha/" rel="nofollow">Alemanha</a></li>
<li><a href="/xfsearch/Brasil/" rel="nofollow">Brasil</a></li>
<li><a href="/xfsearch/Portugal/" rel="nofollow">Portugal</a></li>
<li><a href="/xfsearch/Argentina/" rel="nofollow">Argentina</a></li>
<li><a href="/xfsearch/Espanha/" rel="nofollow">Espanha</a></li>
<li><a href="/xfsearch/Italia/" rel="nofollow">Italia</a></li>
<li><a href="/xfsearch/Mexico/" rel="nofollow">Mexico</a></li>
<li><a href="/xfsearch/Coreia do Sul/" rel="nofollow">Coreia do Sul</a></li>
<li><a href="/xfsearch/Japao/" rel="nofollow">Japao</a></li>
<li><a href="/xfsearch/China/" rel="nofollow">China</a></li>
<li><a href="/xfsearch/Russia/" rel="nofollow">Russia</a></li>
<li><a href="/xfsearch/Russia/" rel="nofollow">Argentina</a></li>
</ul>
<ul class="catmainlinkright" style="width: 49.6%;float:right;border-left: 3px solid #ffd000;;">
<li><a href="/bdrip/" rel="nofollow">BDRip</a></li>
<li><a href="/hdrip/" rel="nofollow">WEBDL</a></li>
<li><a href="/hdrip/" rel="nofollow">HDRip</a></li>
<li><a href="/dvdrip/" rel="nofollow">DVDRip</a></li>
<li><a href="/dvdscr/" rel="nofollow">DVDScrs</a></li>
<li><a href="/hdts/" rel="nofollow">HDTS</a></li>
<li><a href="/hdcam/" rel="nofollow">HDCAM</a></li>
<li><a href="/telesync/" rel="nofollow">Telesync</a></li>
<li><a href="/trailers/" rel="nofollow">Trailers</a>
</li><li><a href="/filmes-2017/" rel="nofollow">Filmes 2017</a></li>
<li><a href="/filmes-2016/" rel="nofollow">Filmes 2016</a></li>
<li><a href="/filmes-2015/" rel="nofollow">Filmes 2015</a></li>
<li><a href="/filmes-2014/" rel="nofollow">Filmes 2014</a></li>
<li><a href="/filmes-2013/" rel="nofollow">Filmes 2013</a></li>
<li><a href="/filmes-2012/" rel="nofollow">Filmes 2012</a></li>
</ul>
</div>
<div id="footer">
<div class="logo-f">
<a href="https://www.fbdown.me/"><img alt="facebook video downloader" src="/templates/assistirhdonline/images/logo-footer.png"/> </a>
</div>
<div class="copyright">
<h3>Copyright © 2020, <a href="/" title="Assistir filmes Online Gratis em Português">Filmes Online</a> |
<a href="/dmca.html" rel="nofollow">DMCA</a> |
<a href="https://www.google.com/recaptcha/mailhide/d?k=017_qf140XDyMFWFcPItppxg==&amp;c=feDn_TyWkEU2pLbDHktQkkxkgJqCfYNt8Uo8EnyjX0E=" rel="nofollow noopener" target="_blank">Contact</a>
</h3> <center>
</center>
<br/>
<br/>
<p style="text-align:justify;color:#666;font: inherit;">Oferecemos filmes para você <a href="/"><b>assistir online</b></a> dublado Nós sempre atualizar o site e adicionar um monte de novos jogadores para <i><b><a href="https://hdfilmes.co/gofilmes/">gofilmes</a> online streaming</b></i>. Nossa portal filmes projetado para usuários que falam Português e você pode gratis assistir hd, bdrip, dvdrip. Nós não apenas trailers de mega filmes e filmes completo youtube e <i>online legendados</i>. Os jogadores também estão disponíveis a partir de vários locais ver <a href="https://hdfilmes.co/xilften/">xilften</a> filmes online. HD filmes, topflix, filmezando, xilften, overflix, <a href="https://hdfilmes.co/mmfilmes/">mm filmes</a>. Aviso. Todas as informações neste site é feita a partir de fontes públicas. A administração não é responsável. </p><h3>Filmes Online - Mega Filmes, Filmes Completos, Filmes Gratis</h3>
<br/>
<hr/>
</div>
<div class="couter"></div>
<div class="clear"></div>
</div>
</div>
</div>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:500" rel="stylesheet"/>
<script data-cfasync="false" src="/engine/classes/min/index.php?charset=utf-8&amp;g=general&amp;v=22"></script>
<div id="loading-layer" style="display:none">Processando ... Por favor aguarde...</div>
<script type="text/javascript">
if(top != self) {
  top.location = self.location;
}
<!--
var dle_root       = '/';
var dle_admin      = '';
var dle_login_hash = '';
var dle_group      = 5;
var dle_skin       = 'assistirhdonline';
var dle_wysiwyg    = '0';
var quick_wysiwyg  = '0';
var dle_act_lang   = ["Sim", "Não", "Entrar", "Cancelar", "Save", "Remove"];
var menu_short     = 'Quick Edit';
var menu_full      = 'Full Edit';
var menu_profile   = 'Perfil';
var menu_send      = 'Enviar mensagem';
var menu_uedit     = 'Administração de Usuários';
var dle_info       = 'Informação';
var dle_confirm    = 'Confirm';
var dle_prompt     = 'informação de entrada';
var dle_req_field  = 'Preencha todos os campos obrigatórios';
var dle_del_agree  = 'Você tem certeza que quer deletar esta mensagem?';
var dle_spam_agree = 'Você realmente quer marcar um usuário como um spammer? Isto irá apagar todos os seus / suas observações';
var dle_complaint  = 'Digite sua mensagem:';
var dle_big_text   = 'Selected area is too large text.';
var dle_orfo_title = 'Digite um comentário para relatório';
var dle_p_send     = 'Mandar';
var dle_p_send_ok  = 'Notification sent successfully';
var dle_save_ok    = 'Alterações salvou com sucesso.';
var dle_del_news   = 'Apagar filmes';
var allow_dle_delete_news   = false;
var dle_search_delay   = false;
var dle_search_value   = '';
$(function(){
	FastSearch();
});
//-->
</script>
<script>
[].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
  img.setAttribute('src', img.getAttribute('data-src'));
  img.onload = function() {
    img.removeAttribute('data-src');
  };
});
$(document).ready(function () {
document.getElementById('script_block').appendChild(document.getElementById('script_ad'));
document.getElementById('script_ad').style.display = 'block';
});
</script>
<script async="" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58de627bbe53e31d" type="text/javascript"></script>
<script async="" charset="UTF-8" src="//web.webpushs.com/js/push/b771c56350c1b4aa5f20a986a93cec6d_1.js"></script>
</body>
</html>
