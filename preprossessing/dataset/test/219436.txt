<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.bikesandmore.cl/xmlrpc.php" rel="pingback"/>
<title>No se encontró la página | Bikes And More</title>
<!-- wordpress head functions -->
<!-- end of wordpress head -->
<!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.bikesandmore.cl/feed/" rel="alternate" title="Bikes And More » Feed" type="application/rss+xml"/>
<link href="https://www.bikesandmore.cl/comments/feed/" rel="alternate" title="Bikes And More » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bikesandmore.cl\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.bikesandmore.cl/wp-content/plugins/contact-form-7/includes/css/styles.css" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/plugins/instagram-feed/css/sb-instagram.min.css" id="sb_instagram_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" id="sb-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css" id="woocommerce-layout-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css" id="woocommerce-smallscreen-css" media="only screen and (max-width: 768px)" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/css/woocommerce.css" id="woocommerce-general-css" media="all" rel="stylesheet" type="text/css"/>
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link href="https://www.bikesandmore.cl/wp-content/plugins/woocommerce-gateway-paypal-express-checkout/assets/css/wc-gateway-ppec-frontend-cart.css" id="wc-gateway-ppec-frontend-cart-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets_admin/css/jquery-ui-1.10.4.css" id="awe_ui_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets_admin/css/shortcode.css" id="awe_shortcode-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/bootstrap.css" id="boostrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/style.css" id="viska-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/font-awesome-4.2.0/css/font-awesome.min.css" id="viska-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/blog.css" id="viska-blog-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/animate.css" id="viska-animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/owl.carousel.css" id="viska-carousel-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/responsive.css" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/colors/viska-color-css.css" id="awe-color-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/assets/css/custom.css" id="viska-custom-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bikesandmore.cl/wp-content/themes/viska/style.css" id="viska-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/jquery.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/jflickrfeed.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/awe.shortcode.js" type="text/javascript"></script>
<link href="https://www.bikesandmore.cl/xmlrpc.php" rel="pingback"/>
<link href="https://www.bikesandmore.cl/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.bikesandmore.cl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.bikesandmore.cl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<meta content="WooCommerce 3.4.2" name="generator"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700,900" id="font-frame-css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" id="font-frame-css-2" rel="stylesheet" type="text/css"/>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
</head>
<body class="error404 woocommerce-no-js blog">
<!-- Page wrap -->
<div id="page-wrap"><!-- Preloader -->
<div id="preloader">
<div class="inner">
<div class="image">
<img alt="" class="img1" src="http://dev.lag.cl/bikesandmore/wp-content/uploads/2016/05/logo-bikes.png"/>
<img alt="" class="img2" src=""/>
</div>
<div class="circle-ef"></div>
</div>
</div>
<!-- End Preloader -->
<!-- End Navigation -->
<nav id="nav-menu">
<span id="close-menu"></span>
<ul class="menu-nav " id="main-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home" id="menu-item-131"><a href="https://www.bikesandmore.cl/#home">Home</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" id="menu-item-205"><a href="https://www.bikesandmore.cl/que-es-bikes-more/">Qué es Bikes &amp; More</a><ul class="sub-menu"> <li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-208"><a href="https://www.bikesandmore.cl/que-queremos-hacer/">Que queremos hacer</a></li> <li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-213"><a href="https://www.bikesandmore.cl/ventajas-de-elegirnos/">Ventajas de elegirnos</a></li></ul></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home" id="menu-item-132"><a href="https://www.bikesandmore.cl/#about">Nosotros</a></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children" id="menu-item-139"><a href="https://www.bikesandmore.cl/#services">Servicios</a><ul class="sub-menu"> <li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-318"><a href="https://www.bikesandmore.cl/programa-pmbi/">Programa PMBI</a></li></ul></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home" id="menu-item-135"><a href="https://www.bikesandmore.cl/#team">Team</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children" id="menu-item-153"><a href="https://www.bikesandmore.cl/blog/">Blog #1</a><ul class="sub-menu"> <li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-151"><a href="https://www.bikesandmore.cl/blog-masonry/">Blog #2</a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-247"><a href="https://www.bikesandmore.cl/tienda/">Tienda</a></li></ul> </nav>
<!-- HOME -->
<div class="blog-home video-place parallax" id="blog-banner" style="background-image: url(http://www.bikesandmore.cl/wp-content/uploads/2017/07/Rotwild-Gravity-electric-mountain-bike.jpg)">
<div class="head-ct">
<div class="home-content" id="home-content">
<!-- Slider content -->
</div>
<!-- Home media Image, Video, Slide -->
</div>
<div class="awe-overlay-bg" style="background-image: url(http://dev.lag.cl/bikesandmore/wp-content/themes/viska/assets/images/bg-pattern.png); background-color: rgba(0,0,0,.4)"></div> </div>
<!-- End Header -->
<header id="header">
<div class="container">
<!-- Logo -->
<h1 class="logo">
<a class="logo-image" href="https://www.bikesandmore.cl" style="width: 210px;height: 70px" title="Bikes And More"><img src="http://dev.lag.cl/bikesandmore/wp-content/uploads/2016/05/logo-bikes.png"/></a>
<a class="logo-image-sticky" href="https://www.bikesandmore.cl" style="width: 458px;height: 47px" title="Bikes And More"><img src="http://dev.lag.cl/bikesandmore/wp-content/uploads/2016/07/logo-rotwild-bco.png"/></a>
</h1>
<!-- Button Menu -->
<span id="button-menu"><i class="icon"></i></span>
<ul class="menu-top" id="menu-top"><li class="menu-item menu-item-type-custom menu-item-object-custom" id="menu-item-140"><a href="http://www.bikesandmore.cl/contacto/">Contacto</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page" id="menu-item-300"><a href="https://www.bikesandmore.cl/tienda/">Tienda</a></li></ul>
</div>
</header> <!--=============== wrapper ===============-->
<div id="main">
<!-- Blog Singer-->
<div id="content-blog">
<div class="container">
<div class="row">
<h3>Not Found</h3>
<p class="lead blog-description">Sorry, but the requested resource was not found on this site.</p>
<div class="separator"></div>
</div>
</div>
</div>
<!-- End Blog Singer-->
</div>
<!-- ENd Main -->
<!--=============== section blog end ===============-->
<!-- Footer -->
<footer class="footer" id="footer">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="footer-content">
<i class="awe-icon fa fa-angle-double-up" id="scroll-top"></i>
<div class="share"><a class="wow fadeInLeft" data-wow-delay="0.6" href="https://www.facebook.com/Bikes-more-421263301333732/"><i class="awe-icon fa fa-facebook"></i></a><a class="wow fadeInLeft" data-wow-delay="1" href="https://twitter.com/MoreBikes"><i class="awe-icon fa fa-twitter"></i></a><a class="wow fadeInLeft" data-wow-delay="1.4" href="https://www.instagram.com/bikesandmoresantiago/"><i class="awe-icon fa fa-instagram"></i></a></div> <p class="site-info">
                            Huérfanos 863, Of 815, Santiago, C.P. 8320176
Fono: 8899 8147 © Copyright 2014 Bikes and More | Aviso Legal                        </p>
</div>
</div>
</div>
</div>
</footer>
</div>
<!-- Instagram Feed JS -->
<script type="text/javascript">
var sbiajaxurl = "https://www.bikesandmore.cl/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.bikesandmore.cl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
/* ]]> */
</script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/contact-form-7/includes/js/scripts.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var sb_instagram_js_options = {"sb_instagram_at":"1246758279.M2E4MWE5Zg==.OTY3Y2RiYWRiOTdh.NDVmOTljZGMxOTVlOThiODA2ZTA=","font_method":"svg"};
/* ]]> */
</script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/instagram-feed/js/sb-instagram.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrito","cart_url":"https:\/\/www.bikesandmore.cl\/carro\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ab5d5cba866ac8973518cb1080d453ef","fragment_name":"wc_fragments_ab5d5cba866ac8973518cb1080d453ef"};
/* ]]> */
</script>
<script src="https://www.bikesandmore.cl/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/ui/core.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/ui/widget.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/ui/tabs.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/jquery/ui/accordion.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/jquery.sticky.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/jquery.owl.carousel.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/retina.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-content/themes/viska/assets/js/jquery.custom.min.js" type="text/javascript"></script>
<script src="https://www.bikesandmore.cl/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
</body>
</html>
