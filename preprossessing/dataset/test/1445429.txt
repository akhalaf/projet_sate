<!DOCTYPE html>
<html lang="en-US">
<head>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://thevenezuelareport.com/xmlrpc.php" rel="pingback"/>
<title>The Venezuela Report – Updates from Venezuela</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://thevenezuelareport.com/feed/" rel="alternate" title="The Venezuela Report » Feed" type="application/rss+xml"/>
<link href="https://thevenezuelareport.com/comments/feed/" rel="alternate" title="The Venezuela Report » Comments Feed" type="application/rss+xml"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/thevenezuelareport.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://thevenezuelareport.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://thevenezuelareport.com/wp-content/plugins/wp-easy-gallery-pro/css/prettyPhoto.css?ver=5.6" id="prettyPhoto_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="http://thevenezuelareport.com/wp-content/plugins/wp-easy-gallery-pro/css/default.css?ver=5.6" id="wpEasyGallery_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://thevenezuelareport.com/wp-content/themes/travelify/style.css?ver=5.6" id="travelify_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Ubuntu&amp;ver=5.6" id="travelify_google_font_ubuntu-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://thevenezuelareport.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://thevenezuelareport.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="prettyPhoto-js" src="http://thevenezuelareport.com/wp-content/plugins/wp-easy-gallery-pro/js/jquery.prettyPhoto.js?ver=5.6" type="text/javascript"></script>
<script id="easyGalleryTheme-js" src="http://thevenezuelareport.com/wp-content/plugins/wp-easy-gallery-pro/js/EasyGallery_Theme.js?ver=5.6" type="text/javascript"></script>
<script id="easyGalleryLoader-js" src="http://thevenezuelareport.com/wp-content/plugins/wp-easy-gallery-pro/js/EasyGalleryLoader.js?ver=5.6" type="text/javascript"></script>
<script id="travelify_functions-js" src="https://thevenezuelareport.com/wp-content/themes/travelify/library/js/functions.min.js?ver=5.6" type="text/javascript"></script>
<link href="https://thevenezuelareport.com/wp-json/" rel="https://api.w.org/"/><link href="https://thevenezuelareport.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://thevenezuelareport.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<!-- WP Easy Gallery Pro -->
<style>.wp-easy-gallery img {}</style><script>var wpegSettings = {gallery_theme: 'default'};</script> <style type="text/css">
        a { color: #1c1aa3; }
        #site-title a { color: #1c1aa3; }
        #site-title a:hover, #site-title a:focus  { color: #dd2c2c; }
        .wrapper { background: #F8F8F8; }
        .social-icons ul li a { color: #d0d0d0; }
		#main-nav a,
		#main-nav a:hover,
		#main-nav a:focus,
		#main-nav ul li.current-menu-item a,
		#main-nav ul li.current_page_ancestor a,
		#main-nav ul li.current-menu-ancestor a,
		#main-nav ul li.current_page_item a,
		#main-nav ul li:hover > a,
		#main-nav ul li:focus-within > a { color: #ffffff; }
        .widget, article { background: #fff; }
        .entry-title, .entry-title a, .entry-title a:focus, h1, h2, h3, h4, h5, h6, .widget-title  { color: #1b1e1f; }
		a:focus,
		a:active,
		a:hover,
		.tags a:hover,
		.tags a:focus,
		.custom-gallery-title a,
		.widget-title a,
		#content ul a:hover,
		#content ul a:focus,
		#content ol a:hover,
		#content ol a:focus,
		.widget ul li a:hover,
		.widget ul li a:focus,
		.entry-title a:hover,
		.entry-title a:focus,
		.entry-meta a:hover,
		.entry-meta a:focus,
		#site-generator .copyright a:hover,
		#site-generator .copyright a:focus { color: #dd2c2c; }
        #main-nav { background: #1c1aa3; border-color: #1c1aa3; }
        #main-nav ul li ul, body { border-color: #1c1aa3; }
		#main-nav a:hover,
		#main-nav a:focus,
		#main-nav ul li.current-menu-item a,
		#main-nav ul li.current_page_ancestor a,
		#main-nav ul li.current-menu-ancestor a,
		#main-nav ul li.current_page_item a,
		#main-nav ul li:hover > a,
		#main-nav ul li:focus-within > a,
		#main-nav li:hover > a,
		#main-nav li:focus-within > a,
		#main-nav ul ul :hover > a,
		#main-nav ul ul :focus-within > a,
		#main-nav a:focus { background: #dd2c2c; }
		#main-nav ul li ul li a:hover,
		#main-nav ul li ul li a:focus,
		#main-nav ul li ul li:hover > a,
		#main-nav ul li ul li:focus-within > a,
		#main-nav ul li.current-menu-item ul li a:hover
		#main-nav ul li.current-menu-item ul li a:focus { color: #dd2c2c; }
        .entry-content { color: #1D1D1D; }
		input[type="reset"],
		input[type="button"],
		input[type="submit"],
		.entry-meta-bar .readmore,
		#controllers a:hover,
		#controllers a.active,
		.pagination span,
		.pagination a:hover span,
		.pagination a:focus span,
		.wp-pagenavi .current,
		.wp-pagenavi a:hover,
		.wp-pagenavi a:focus {
            background: #1c1aa3;
            border-color: #1c1aa3 !important;
        }
		::selection,
		.back-to-top:focus-within a { background: #1c1aa3; }
        blockquote { border-color: #1c1aa3; }
		#controllers a:hover,
		#controllers a.active { color: #1c1aa3; }
		input[type="reset"]:hover,
		input[type="reset"]:focus,
		input[type="button"]:hover,
		input[type="button"]:focus,
		input[type="submit"]:hover,
		input[type="submit"]:focus,
		input[type="reset"]:active,
		input[type="button"]:active,
		input[type="submit"]:active,
		.entry-meta-bar .readmore:hover,
		.entry-meta-bar .readmore:focus,
		.entry-meta-bar .readmore:active,
		ul.default-wp-page li a:hover,
		ul.default-wp-page li a:focus,
		ul.default-wp-page li a:active {
            background: #dd2c2c;
            border-color: #dd2c2c;
        }
    </style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style id="custom-background-css" type="text/css">
body.custom-background { background-image: url("https://thevenezuelareport.com/wp-content/themes/travelify/images/background.png"); background-position: left top; background-size: auto; background-repeat: repeat; background-attachment: scroll; }
</style>
</head>
<body class="home blog custom-background ">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="wrapper">
<header id="branding">
<div class="container clearfix">
<div class="hgroup-wrap clearfix">
<section class="hgroup-right">
</section><!-- .hgroup-right -->
<hgroup class="clearfix" id="site-logo">
<h1 id="site-title">
<a href="https://thevenezuelareport.com/" rel="home" title="The Venezuela Report">
<img alt="The Venezuela Report" src="http://thevenezuelareport.com/wp-content/uploads/2015/11/thevenezuelareport_logo.png"/>
</a>
</h1>
</hgroup><!-- #site-logo -->
</div><!-- .hgroup-wrap -->
</div><!-- .container -->
<nav class="clearfix" id="main-nav">
<div class="container clearfix"><ul class="root"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-20" id="menu-item-20"><a aria-current="page" href="http://thevenezuelareport.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-446" id="menu-item-446"><a href="https://thevenezuelareport.com/current-events-with-gary-patty/">Current Events</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1764" id="menu-item-1764"><a href="https://thevenezuelareport.com/help-for-venezuela/">HELP FOR VENEZUELA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1750" id="menu-item-1750"><a href="https://thevenezuelareport.com/current-events-with-gary-patty/online-or-in-line/">Online or In Line?</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-927" id="menu-item-927"><a href="https://thevenezuelareport.com/feeding-the-elderly-and-others/">Feeding the Elderly and others</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-813" id="menu-item-813"><a href="https://thevenezuelareport.com/learning-center/work-progress-on-new-learning-center/">Work Progress on New Learning Center</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-650" id="menu-item-650"><a href="https://thevenezuelareport.com/leaving-venezuela-2016/">Leaving Venezuela 2016</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" id="menu-item-24"><a href="https://thevenezuelareport.com/who-we-are/">Who We Are</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-40" id="menu-item-40"><a href="https://thevenezuelareport.com/ministries/">Ministries</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38" id="menu-item-38"><a href="https://thevenezuelareport.com/baseball-chapel/">Baseball Chapel</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48" id="menu-item-48"><a href="https://thevenezuelareport.com/center-of-hope-church/">Center of Hope Church</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52" id="menu-item-52"><a href="https://thevenezuelareport.com/destiny-church/">Destiny Church</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56" id="menu-item-56"><a href="https://thevenezuelareport.com/feeding-center/">Feeding Center</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273" id="menu-item-273"><a href="https://thevenezuelareport.com/samuels-house-3/">Samuel’s House- Venezuela</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" id="menu-item-69"><a href="https://thevenezuelareport.com/street-children/">Street Children</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-72" id="menu-item-72"><a href="https://thevenezuelareport.com/teen-challenge/">Teen Challenge</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://thevenezuelareport.com/youth-ministries/">Youth Ministries</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-79" id="menu-item-79"><a href="https://thevenezuelareport.com/get-involved/">Get Involved</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83" id="menu-item-83"><a href="https://thevenezuelareport.com/online-giving/">Online Giving</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-91" id="menu-item-91"><a href="https://thevenezuelareport.com/areas-of-need/">Areas of Need</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-94" id="menu-item-94"><a href="https://thevenezuelareport.com/short-term-ministry-opportunities/">Short-Term Ministry Opportunities</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-100" id="menu-item-100"><a href="https://thevenezuelareport.com/maps/">MAPS</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113" id="menu-item-113"><a href="https://thevenezuelareport.com/aim/">AIM</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119" id="menu-item-119"><a href="https://thevenezuelareport.com/maps-construction-teams/">MAPS Construction Teams</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-200" id="menu-item-200"><a href="https://thevenezuelareport.com/newsletters-and-updates/">Newsletters</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-207" id="menu-item-207"><a href="https://thevenezuelareport.com/newsletters-and-updates/samuels-house/">Samuel’s House</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-278" id="menu-item-278"><a href="https://thevenezuelareport.com/venezuela-destiny-and-rev-gary-heiney-updates-and-newsletters/">Venezuela, Destiny, and Rev. Gary Heiney Updates and Newsletters</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-477" id="menu-item-477"><a href="https://thevenezuelareport.com/gallery/">Gallery</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1767" id="menu-item-1767"><a href="https://thevenezuelareport.com/help-for-venezuela/">HELP FOR VENEZUELA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1749" id="menu-item-1749"><a href="https://thevenezuelareport.com/current-events-with-gary-patty/christmas-2016/">Christmas 2016</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-926" id="menu-item-926"><a href="https://thevenezuelareport.com/feeding-the-elderly-and-others/">Feeding the Elderly and others</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-814" id="menu-item-814"><a href="https://thevenezuelareport.com/learning-center/work-progress-on-new-learning-center/">Work Progress on New Learning Center</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-758" id="menu-item-758"><a href="https://thevenezuelareport.com/baptism-and-church-picnic-july-2016/">Baptism and Church Picnic July 2016</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-652" id="menu-item-652"><a href="https://thevenezuelareport.com/april-2016-spain/">April 2016 Spain</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296" id="menu-item-296"><a href="https://thevenezuelareport.com/2016april-pool-time/">2016April Pool Time  Samuel’s House</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-581" id="menu-item-581"><a href="https://thevenezuelareport.com/germany-2016/">Germany 2016</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-534" id="menu-item-534"><a href="https://thevenezuelareport.com/around-and-about-at-samuels-house/">Around and about at Samuel’s House</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-305" id="menu-item-305"><a href="https://thevenezuelareport.com/new-puppy/">New puppy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-501" id="menu-item-501"><a href="https://thevenezuelareport.com/aguaponics/">Aguaponics</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-978" id="menu-item-978"><a href="https://thevenezuelareport.com/my-calendar/">Events Calendar</a></li>
</ul></div><!-- .container -->
</nav><!-- #main-nav --> </header>
<div class="container clearfix" id="main">
<div id="container">
<div class="no-margin-left" id="primary">
<div id="content"> <section class="post-2050 post type-post status-publish format-standard sticky hentry category-uncategorized" id="post-2050">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2021/01/09/january-2021/" title="January 2021">January 2021</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>HAPPY NEW YEAR Patty and I are excited about this new year.    We know we are living in the last days before Christ comes for His bride.  Will you be ready?  I know it is easy to focus on the… </p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2021/01/09/january-2021/" rel="bookmark"><time class="entry-date published" datetime="2021-01-09T13:46:16-06:00">January 9, 2021</time><time class="updated" datetime="2021-01-09T13:46:18-06:00">January 9, 2021</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2021/01/09/january-2021/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			January 2021<a class="readmore" href="https://thevenezuelareport.com/2021/01/09/january-2021/" title="">Read more</a> </div>
</article>
</section>
<section class="post-2038 post type-post status-publish format-standard sticky hentry category-uncategorized" id="post-2038">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2021/01/09/christmas-2020-samuels-house/" title="Christmas 2020 Samuel’s House">Christmas 2020 Samuel’s House</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>Christmas 2020 Samuel’s House</p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2021/01/09/christmas-2020-samuels-house/" rel="bookmark"><time class="entry-date published" datetime="2021-01-09T13:39:07-06:00">January 9, 2021</time><time class="updated" datetime="2021-01-09T13:39:08-06:00">January 9, 2021</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2021/01/09/christmas-2020-samuels-house/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			Christmas 2020 Samuel’s House<a class="readmore" href="https://thevenezuelareport.com/2021/01/09/christmas-2020-samuels-house/" title="">Read more</a> </div>
</article>
</section>
<section class="post-2016 post type-post status-publish format-standard sticky hentry category-uncategorized" id="post-2016">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2019/01/01/happy-new-year-2019/" title="Happy  New  Year  2019  From Gary &amp; Patty">Happy  New  Year  2019  From Gary &amp; Patty</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>happy new year Posted by Gary Heiney on Monday, December 31, 2018</p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2019/01/01/happy-new-year-2019/" rel="bookmark"><time class="entry-date published" datetime="2019-01-01T11:42:09-06:00">January 1, 2019</time><time class="updated" datetime="2019-01-01T11:52:38-06:00">January 1, 2019</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2019/01/01/happy-new-year-2019/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			Happy  New  Year  2019  From Gary &amp; Patty<a class="readmore" href="https://thevenezuelareport.com/2019/01/01/happy-new-year-2019/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1773 post type-post status-publish format-standard sticky hentry category-uncategorized" id="post-1773">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/10/merry-christmas-2018-from-gary-and-patty-heiney/" title="Merry Christmas 2018   from  Gary and Patty Heiney">Merry Christmas 2018   from  Gary and Patty Heiney</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>Venezuela Report Venezuela Report December 2018</p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/10/merry-christmas-2018-from-gary-and-patty-heiney/" rel="bookmark"><time class="entry-date published" datetime="2018-12-10T14:36:01-06:00">December 10, 2018</time><time class="updated" datetime="2018-12-10T14:53:28-06:00">December 10, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/10/merry-christmas-2018-from-gary-and-patty-heiney/#comments">1 Comment</a></span>
</div><!-- .entry-meta -->
    			Merry Christmas 2018   from  Gary and Patty Heiney<a class="readmore" href="https://thevenezuelareport.com/2018/12/10/merry-christmas-2018-from-gary-and-patty-heiney/" title="">Read more</a> </div>
</article>
</section>
<section class="post-2034 post type-post status-publish format-standard hentry category-uncategorized" id="post-2034">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2021/01/09/merry-christmas-2020/" title="Merry Christmas 2020">Merry Christmas 2020</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>December 2020MERRY CHRISTMAS!!! On behalf of Patty and myself we want to say thank you for your partnership, friendship, prayers, oﬀerings and all the other ways that you have shown us your generosity this year. We pray that you and… </p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2021/01/09/merry-christmas-2020/" rel="bookmark"><time class="entry-date published" datetime="2021-01-09T13:33:16-06:00">January 9, 2021</time><time class="updated" datetime="2021-01-09T13:33:17-06:00">January 9, 2021</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2021/01/09/merry-christmas-2020/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			Merry Christmas 2020<a class="readmore" href="https://thevenezuelareport.com/2021/01/09/merry-christmas-2020/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1772 post type-post status-publish format-standard hentry category-uncategorized" id="post-1772">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/a-christmas-celebration/" title="A Christmas Celebration">A Christmas Celebration</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>One last post of the Christmas party. Misael and his brother Luis enjoying there cotton candy and some typical Venezuelan Christmas music with two of our Destiny church pastors. Posted by Patricia Heiney on Thursday, December 13, 2018 One last… </p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/a-christmas-celebration/" rel="bookmark"><time class="entry-date published" datetime="2018-12-15T15:38:14-06:00">December 15, 2018</time><time class="updated" datetime="2018-12-15T15:40:21-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/a-christmas-celebration/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			A Christmas Celebration<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/a-christmas-celebration/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1918 post type-post status-publish format-standard hentry category-uncategorized" id="post-1918">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/what-a-blessing-convoy-of-hope-has-been-to-samuels-house-and-the-surrounding-community/" title="What a blessing Convoy of Hope has been to Samuels house and the surrounding community">What a blessing Convoy of Hope has been to Samuels house and the surrounding community</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>Some of the kids in the community and some of Samuels house kids eating lunch at the church. </p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/what-a-blessing-convoy-of-hope-has-been-to-samuels-house-and-the-surrounding-community/" rel="bookmark"><time class="entry-date published" datetime="2018-12-15T15:34:34-06:00">December 15, 2018</time><time class="updated" datetime="2018-12-15T15:34:35-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/what-a-blessing-convoy-of-hope-has-been-to-samuels-house-and-the-surrounding-community/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			What a blessing Convoy of Hope has been to Samuels house and the surrounding community<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/what-a-blessing-convoy-of-hope-has-been-to-samuels-house-and-the-surrounding-community/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1913 post type-post status-publish format-standard hentry category-uncategorized" id="post-1913">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/house-parents-at-samuels-house/" title="House Parents at Samuel’s House">House Parents at Samuel’s House</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
<p>God has blessed us with some truly great house parents at Samuels house. Please pray for God to give them strength and wisdom</p>
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/house-parents-at-samuels-house/" rel="bookmark"><time class="entry-date published" datetime="2018-12-15T15:28:04-06:00">December 15, 2018</time><time class="updated" datetime="2018-12-15T15:28:05-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/house-parents-at-samuels-house/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			House Parents at Samuel’s House<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/house-parents-at-samuels-house/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1905 post type-post status-publish format-standard hentry category-uncategorized" id="post-1905">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/cuties-at-samuels-house/" title="Cuties at Samuel’s House">Cuties at Samuel’s House</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/cuties-at-samuels-house/" rel="bookmark"><time class="entry-date published" datetime="2018-12-15T15:25:17-06:00">December 15, 2018</time><time class="updated" datetime="2018-12-15T15:25:19-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/cuties-at-samuels-house/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			Cuties at Samuel’s House<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/cuties-at-samuels-house/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1902 post type-post status-publish format-standard hentry category-uncategorized" id="post-1902">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/the-ladies-had-a-dessert-contests-at-the-christmas-party-the-kids-chowed-down/" title="The ladies had a dessert contests at the Christmas party. The kids chowed down">The ladies had a dessert contests at the Christmas party. The kids chowed down</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/the-ladies-had-a-dessert-contests-at-the-christmas-party-the-kids-chowed-down/" rel="bookmark"><time class="entry-date published" datetime="2018-12-15T15:23:59-06:00">December 15, 2018</time><time class="updated" datetime="2018-12-15T15:24:00-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/the-ladies-had-a-dessert-contests-at-the-christmas-party-the-kids-chowed-down/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			The ladies had a dessert contests at the Christmas party. The kids chowed down<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/the-ladies-had-a-dessert-contests-at-the-christmas-party-the-kids-chowed-down/" title="">Read more</a> </div>
</article>
</section>
<section class="post-1899 post type-post status-publish format-standard hentry category-uncategorized" id="post-1899">
<article>
<header class="entry-header">
<h2 class="entry-title">
<a href="https://thevenezuelareport.com/2018/12/15/the-lady-on-the-left-is-in-charge-of-our-house-parents-and-the-samuels-house-children-her-and-her-husband-on-the-right-are-pastors-of-destiny-church-soapire/" title="The lady on the left is in charge of our house parents and the Samuels house children. Her and her husband on the right are pastors of Destiny church Soapire.">The lady on the left is in charge of our house parents and the Samuels house children. Her and her husband on the right are pastors of Destiny church Soapire.</a>
</h2><!-- .entry-title -->
</header>
<div class="entry-content clearfix">
</div>
<div class="entry-meta-bar clearfix">
<div class="entry-meta">
<span class="byline"> <span class="author vcard"><a class="url fn n" href="https://thevenezuelareport.com/author/rkhandley/">Rosalynn Handley</a></span></span><span class="posted-on"><a href="https://thevenezuelareport.com/2018/12/15/the-lady-on-the-left-is-in-charge-of-our-house-parents-and-the-samuels-house-children-her-and-her-husband-on-the-right-are-pastors-of-destiny-church-soapire/" rel="bookmark"><time class="entry-date published updated" datetime="2018-12-15T15:22:52-06:00">December 15, 2018</time></a></span> <span class="category"><a href="https://thevenezuelareport.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
<span class="comments"><a href="https://thevenezuelareport.com/2018/12/15/the-lady-on-the-left-is-in-charge-of-our-house-parents-and-the-samuels-house-children-her-and-her-husband-on-the-right-are-pastors-of-destiny-church-soapire/#respond">No Comments</a></span>
</div><!-- .entry-meta -->
    			The lady on the left is in charge of our house parents and the Samuels house children. Her and her husband on the right are pastors of Destiny church Soapire.<a class="readmore" href="https://thevenezuelareport.com/2018/12/15/the-lady-on-the-left-is-in-charge-of-our-house-parents-and-the-samuels-house-children-her-and-her-husband-on-the-right-are-pastors-of-destiny-church-soapire/" title="">Read more</a> </div>
</article>
</section>
<ul class="default-wp-page clearfix">
<li class="previous"><a href="https://thevenezuelareport.com/page/2/">« Previous</a></li>
<li class="next"></li>
</ul>
</div><!-- #content --></div><!-- #primary -->
<div id="secondary">
</div><!-- #secondary --></div><!-- #container -->
</div><!-- #main -->
<footer class="clearfix" id="footerarea">
<div id="site-generator">
<div class="container"><div class="copyright">Copyright © 2021 <a href="https://thevenezuelareport.com/" title="The Venezuela Report"><span>The Venezuela Report</span></a>. Theme by <a href="http://colorlib.com/wp/travelify/" target="_blank" title="Colorlib"><span>Colorlib</span></a> Powered by <a href="http://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a></div><!-- .copyright --><div class="footer-right"></div><div style="clear:both;"></div>
</div><!-- .container -->
</div><!-- #site-generator --><div class="back-to-top"><a href="#branding"></a></div> </footer>
</div><!-- .wrapper -->
<link href="https://thevenezuelareport.com/wp-content/plugins/wp-author-date-and-meta-remover/css/entrymetastyle.css?ver=1.0" id="remove-style-meta-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery_cycle-js" src="https://thevenezuelareport.com/wp-content/themes/travelify/library/js/jquery.cycle.all.min.js?ver=2.9999.5" type="text/javascript"></script>
<script id="travelify_slider-js-extra" type="text/javascript">
/* <![CDATA[ */
var travelify_slider_value = {"transition_effect":"fade","transition_delay":"4000","transition_duration":"1000"};
/* ]]> */
</script>
<script id="travelify_slider-js" src="https://thevenezuelareport.com/wp-content/themes/travelify/library/js/slider-settings.min.js?ver=5.6" type="text/javascript"></script>
<script id="wp-embed-js" src="https://thevenezuelareport.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>