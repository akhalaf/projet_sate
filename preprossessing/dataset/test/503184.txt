<!DOCTYPE HTML>
<html lang="tr-TR">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/html;CHARSET=iso-8859-9" http-equiv="Content-Type"/>
<meta content="tr" http-equiv="Content-Language"/>
<meta charset="utf-8"/>
<title>Erdem Hýrdavat ve Boya Sanayi Ticaret A.Þ.</title>
<meta content="width=device-width" name="viewport"/>
<link href="style/images/favicon.png" rel="shortcut icon" type="image/x-icon"/>
<link href="/templates/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/templates/style/css/zoombox.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/style/css/flexslider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/style/css/flext.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/js/demo.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/css/kickstart.css" media="screen" rel="stylesheet" type="text/css"/>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="/templates/style/css/ie7.css" media="all" />
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="/templates/style/css/ie8.css" media="all" />
<![endif]-->
<script src="/templates/js/jquery.min.js" type="text/javascript"></script>
<script src="/templates/js/cufon-yui.js" type="text/javascript"></script>
<script src="/templates/js/jqFancyTransitions.1.8.min.js" type="text/javascript"></script>
<script src="/templates/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="/js/jquery.scrollbox.js" type="text/javascript"></script>
<script src="/templates/style/js/faded.js" type="text/javascript"></script>
<script src="/templates/style/js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="/templates/style/js/sliding_effect.js" type="text/javascript"></script>
<script src="/templates/style/js/twitter.min.js" type="text/javascript"></script>
<script src="/templates/style/js/zoombox.js" type="text/javascript"></script>
<script src="/templates/style/js/scripts.js" type="text/javascript"></script>
<script src="/templates/style/js/jquery.flexslider.js" type="text/javascript"></script>
<meta content="Ana Sayfa" name="description"/>
<meta content="Ana Sayfa" name="keywords"/>
</head>
<body>
<div id="home-header">
<!-- Begin Header -->
<div id="header">
<!-- Begin Logo -->
<div id="logo"><a href="/"><img alt="Erdem Boya" src="/templates/img/logo.png"/></a></div>
<!-- End Logo -->
<!-- Begin Menu -->
<div id="menu">
<div class="ddsmoothmenu" id="smoothmenu1">
<ul>
<li><a href="/">Ana Sayfa</a> </li>
<li><a href="#">Ürünlerimiz</a>
<ul>
<li><a href="tr_boya-25-1.html">Boya</a> </li>
<li><a href="tr_kartonpiyer-31-1.html">Kartonpiyer</a> </li>
<li><a href="tr_su-yalitimi-32-1.html">Su Yalýtýmý</a> </li>
<li><a href="tr_seramik-yapistiricilari-33-1.html">Seramik Yapýþtýrýcýlarý</a> </li>
<li><a href="tr_mantolama-34-1.html">Mantolama</a> </li>
<li><a href="tr_vitrifiye-42-1.html">Vitrifiye</a> </li>
<li><a href="tr_armatur-43-1.html">Armatür</a> </li>
<li><a href="tr_tesisat-44-1.html">Tesisat</a> </li>
<li><a href="tr_alci-80-1.html">Alçý</a> </li>
<li><a href="tr_tiner-81-1.html">Tiner</a> </li>
<li><a href="tr_yardimci-urunler-82-1.html">Yardýmcý Ürünler</a> </li>
</ul>
</li>
<li><a href="#">Bayiliklerimiz</a>
<ul>
<li><a href="tr_knauf-1319.html">Knauf</a> </li>
<li><a href="tr_izocam-1321.html">Ýzocam</a> </li>
<li><a href="tr_dyo-83.html">Dyo</a> </li>
<li><a href="tr_weber-84.html">Weber</a> </li>
<li><a href="tr_pak-plast-85.html">Pak Plast</a> </li>
<li><a href="tr_yeni-turan-86.html">Yeni Turan</a> </li>
<li><a href="tr_alvit-87.html">Alvit</a> </li>
<li><a href="tr_bonuspan-88.html">Bonuspan</a> </li>
<li><a href="tr_wooler-89.html">Wooler</a> </li>
<li><a href="tr_hassan-90.html">Hassan</a> </li>
<li><a href="tr_dekor-91.html">Dekor</a> </li>
<li><a href="#">Star * GiL</a> </li>
<li><a href="tr_polex-93.html">Polex</a> </li>
<li><a href="tr_darsa-94.html">Darsa</a> </li>
<li><a href="tr_karsis-95.html">Karsis</a> </li>
<li><a href="tr_mobel-96.html">Mobel</a> </li>
<li><a href="tr_focus-membran-97.html">Focus Membran</a> </li>
<li><a href="tr_adell-159.html">Adell</a> </li>
<li><a href="tr_abs-168.html">ABS</a> </li>
<li><a href="tr_eva-1318.html">Eva</a> </li>
</ul>
</li>
<li><a href="#">Kurumsal</a>
<ul>
<li><a href="tr_sirketimiz-hakkinda-71.html">Þirketimiz Hakkýnda</a> </li>
<li><a href="tr_kalite-politikamiz-72.html">Kalite Politikamýz</a> </li>
<li><a href="tr_vizyonumuz-73.html">Vizyonumuz</a> </li>
</ul>
</li>
<li><a href="tr_iletisim-76.html">Ýletiþim</a> </li>
</ul>
<span></span> </div>
</div>
<div class="clearfix"></div>
</div>
<!-- End Menu -->
<!-- Begin Container -->
<!-- End Header -->
</div>
<!-- Begin Slider -->
<div align="center">
<div class="img-banner">
<div id="slideshowHolder">
<img alt="" src="/templates/3dslide/slide-adell.png"/>
<img alt="" src="/templates/3dslide/slide-alvit.png"/>
<img alt="" src="/templates/3dslide/slide-bonuspan.png"/>
<img alt="" src="/templates/3dslide/slide-dyo.png"/>
<img alt="" src="/templates/3dslide/slide-weber2.png"/>
</div>
</div>
</div>
<!-- End Slider -->
<div id="container">
<div class="clearfix"></div>
<!-- Begin Intro -->
<!-- Place somewhere in the <body> of your page -->
<!-- box -->
<!-- entries -->
<div class="entries">
<div class="entry">
<a href="/"><img alt="" src="/templates/img/entry1.png"/></a>
<span class="shadow"></span>
</div>
<div class="entry">
<a href="/"><img alt="" src="/templates/img/entry2.png"/></a>
<span class="shadow"></span>
</div>
<div class="entry">
<a href="/"><img alt="" src="/templates/img/entry3.png"/></a>
<span class="shadow"></span>
</div>
<div class="entry">
<a href="/"><img alt="" src="/templates/img/entry4.png"/></a>
<span class="shadow"></span>
</div>
<div class="cl"> </div>
</div>
<!-- end of entries -->
<!-- end of box -->
<div class="clearfix"></div>
<br/>
<h3>Ürünler</h3>
<div class="divider2"></div>
<!-- it works the same with all jquery version from 1.x to 2.x -->
<!-- use jssor.slider.mini.js (39KB) or jssor.sliderc.mini.js (31KB, with caption, no slideshow) or jssor.sliders.mini.js (26KB, no caption, no slideshow) instead for release -->
<!-- jssor.slider.mini.js = jssor.sliderc.mini.js = jssor.sliders.mini.js = (jssor.core.js + jssor.utils.js + jssor.slider.js) -->
<script src="/js/jssor.core.js" type="text/javascript"></script>
<script src="/js/jssor.utils.js" type="text/javascript"></script>
<script src="/js/jssor.slider.js" type="text/javascript"></script>
<script>
        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 0,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseLinear,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 3000,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: 140,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 100,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 9,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 980));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }


            //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            //    $(window).bind("orientationchange", ScaleSlider);
            //}
            //responsive code end
        });
    </script>
<div class="flexslider carousel">
<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->
<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 980px; height: 126px; overflow: hidden; ">
<!-- Loading Screen -->
<div style="position: absolute; top: 0px; left: 0px;" u="loading">
<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
</div>
<div style="position: absolute; display: block; background: url(/templates/style/images/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
</div>
</div>
<!-- Slides Container -->
<!-- Slides Container -->
<div style="cursor: move; position: absolute; left: 0px; top: 0px; width: 980px; height: 126px; overflow: hidden;" u="slides">
<div><a href="http://www.amazon.com"><img alt="amazon" border="0" src="/templates/urunler/smallslide/1.png"/></a></div>
<div><img alt="android" src="/templates/urunler/smallslide/2.png" u="image"/></div>
<div><img alt="bitly" src="/templates/urunler/smallslide/3.png" u="image"/></div>
<div><img alt="blogger" src="/templates/urunler/smallslide/4.png" u="image"/></div>
<div><img alt="dnn" src="/templates/urunler/smallslide/5.png" u="image"/></div>
<div><img alt="drupal" src="/templates/urunler/smallslide/6.png" u="image"/></div>
<div><img alt="drupal" src="/templates/urunler/smallslide/7.png" u="image"/></div>
<div><img alt="drupal" src="/templates/urunler/smallslide/10.png" u="image"/></div>
<div><img alt="drupal" src="/templates/urunler/smallslide/11.png" u="image"/></div>
<div><img alt="drupal" src="/templates/urunler/smallslide/12.png" u="image"/></div>
</div>
</div>
<!-- Jssor Slider End -->
</div>
<h3>Bayiliklerimiz</h3>
<div class="divider3"></div>
<ul class="frame" style="text-align:center;">
<li><a href="http://www.dyo.com.tr" rel="gallery0" style="line-height:85px;" target="new"><img src="/templates/bayiler/dufa.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.weber.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/weber.png" width="120"/></a>
</li><li><a href="http://www.adell.com/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/adell.png" width="120"/></a>
</li><li><a href="http://www.dekor.com/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/dekor.png" width="120"/></a>
</li><li><a href="http://www.bonuspan.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/bonuspan.png" width="120"/></a>
</li><li><a href="http://www.izocam.com.tr/izocam.html" rel="gallery0" style="line-height:60px;" target="new"><img src="/templates/bayiler/izocam.gif" style="max-width:110px; height:auto;"/></a>
</li><li><a href="http://www.panelsan.com" rel="gallery0" style="line-height:60px;" target="new"><img src="/templates/bayiler/dunya.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.mobelkimya.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/mobel.png" width="120"/></a>
</li><li><a href="http://www.kar-yapi.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/karsis.png" width="120"/></a>
</li><li><a href="http://www.birlesikfirca.com/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/stargil.png" width="120"/></a>
</li><li><a href="http://www.hassan.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/dekor-hassan.png" width="120"/></a>
</li><li><a href="http://www.alvit.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/alvit.png" width="120"/></a>
</li><li><a href="http://www.wooler.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/wooler.png" width="120"/></a>
</li><li><a href="http://www.austrotherm.com.tr/" rel="gallery0" target="new"><img src="/templates/bayiler/pakboard.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.hakan.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/hakan-plastik.png" width="120"/></a>
</li><li><a href="http://www.polexchem.com/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/polex.png" width="120"/></a>
</li><li><a href="http://www.absalci.com.tr/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/abs-alci.png" width="120"/></a>
</li><li><a href="http://www.focusmembran.com/" rel="gallery0" target="new"><img height="101" src="/templates/bayiler/focus-membran.png" width="120"/></a>
</li><li><a href="http://www.darsaboya.com.tr/" rel="gallery0" style="line-height:80px;" target="new"><img src="/templates/bayiler/darsa.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.yenituran.com" rel="gallery0" style="line-height:75px;" target="new"><img src="/templates/bayiler/yenituran.gif" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.evaboya.com.tr/" rel="gallery0" style="line-height:75px;" target="new"><img src="/templates/bayiler/eva.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.knauf.com.tr/" rel="gallery0" style="line-height:75px;" target="new"><img src="/templates/bayiler/knauf.png" style="max-width:120px; height:auto;"/></a>
</li><li><a href="http://www.spstarplast.com/" rel="gallery0" style="line-height:60px;" target="new"><img src="/templates/bayiler/spstarplast.png" style="max-width:110px; height:auto;"/></a>
</li></ul>
<div class="clearfix"></div>
<br/><br/>
<!--End Container -->
<!--Begin Footer -->
<br/><br/> <p></p><p>
</p></div>
<div id="footer-bottom">
<div id="footer-bottom-light"></div>
<div class="footer-bottom-content">
<div class="copyright">
<p>© 2015. Her hakký saklýdýr.</p>
</div>
<div class="social">
</div>
</div>
</div>
<!-- End Footer -->
<!-- jQuery -->
<script type="text/javascript">

// Can also be used with $(document).ready()

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 128,
    itemMargin: 2,
    controlNav: false,
    directionNav: false,
    minItems: 2,
    maxItems: 4
  });
});


	$(function(){
		$("#faded").faded({
			speed: 1000,
			crossfade: false,
			bigtarget: true,
			loading: true,
			loadingimg: "/templates/style/images/loading.gif",
            autoplay: 5000,
			autorestart: 6000,
			random: false,
			autopagination:false
		});
	});



	</script>
<script>
$(function () {
  $('#demo4').scrollbox({
    direction: 'h',
    switchItems: 5,
    distance: 770
  });

</script>
<script>

$(document).ready( function(){
    $('#slideshowHolder').jqFancyTransitions({ width: 716, height: 458, effect: 'wave', });
});


  jQuery(document).ready(function(){
    jQuery('ul.sf-menu').superfish();
  });

</script>
</body>
</html>
