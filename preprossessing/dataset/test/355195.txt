<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Consejo de Contadores</title>
<!--Archivos Comunes-->
<!--Comunes-->
<link href="css/reset.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/consejo.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/layers.css" media="screen" rel="stylesheet" type="text/css"/>
<!--MenuPrincipal-->
<link href="css/dropdown.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="css/login.css" rel="stylesheet" type="text/css"/>
<link href="img/favicon.ico" rel="SHORTCUT ICON"/>
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/menu.js" type="text/javascript"></script>
<script src="js/jqueryFunctions.js" type="text/javascript"></script>
<script language="javascript">
	$(document).ready(function() {
    $().ajaxStart(function() {
        $('#loading').show();
        $('#respuesta').hide();
    }).ajaxStop(function() {
        $('#loading').hide();
        $('#respuesta').fadeIn('slow');
    });
    $('#formLogin').submit(function() {
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data) {
                $('#respuesta').html(data);
            }
        })
        
        return false;
    }); 
}) 

function denegado(){
	
alert("Favor Registrese o acceda para descargar.")	
	
}

</script>
<!--[if lt IE 9]>
<script src="js/IE9.js" type="text/javascript"></script>
<script src="js/ie7-squish.js" type="text/javascript"></script>
<![endif]-->
<!--z-index-IE-->
<!--[if lte IE 7]>
<script type="text/javascript">
$(function() {
	var zIndexNumber = 1000;
	$('div').each(function() {
		$(this).css('zIndex', zIndexNumber);
		zIndexNumber -= 10;
	});
});
</script>
<![endif]-->
<!--Shadowbox-->
<link href="css/colorbox.css" media="screen" rel="stylesheet"/>
<!-- Fancybox -->
<script src="js/jquery.colorbox.js"></script>
<script type="text/javascript">
$(document).ready(function(){
			$('a#ifrs').colorbox({transition:'fade',iframe:true, speed:500,width:'90%',height:'90%'});
			});
	
</script> <link href="css/slider.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
#preview{
	position:absolute;
	border:1px solid #D7D7D7;
	background:#E5E5E5;
	text-align:center;
	font-family:Georgia, "Times New Roman", Times, serif;
	padding:3px;
	display:none;
	color:#C30;
	z-index:9000;
	-moz-box-shadow: 0 0 2px rgba(0, 0, 0, 0.2);
    -webkit-box-shadow:0 0 2px rgba(0, 0, 0, 0.2);
    box-shadow:0 0 2px rgba(0, 0, 0, 0.2);
}
</style>
<!--Slider-->
<script src="js/easySlider1.7.js" type="text/javascript"></script>
<script type="text/javascript">
		$(document).ready(function(){	
			$("#slider").easySlider({
				auto: true, 
				continuous: true,
				numeric: true
			});
		});	
</script>
<script src="js/tooltip.js" type="text/javascript"></script>
</head>
<body>
<div id="wrapper-principal">
<div id="wrapper-header">
<a href="index.php" id="inicio">Inicio</a>
<a href="mapa.php" id="map">Mapa</a>
<div id="box_sing">
<div id="registrarse"><a href="registrarse.php">Registrarse</a></div>
<div><a class="sign_in" href="#">Iniciar sesión</a></div>
<div id="sign_box">
<form action="asincronos/acceder.php" id="formLogin" method="post" name="login">
<ul>
<li>
<label class="desc">Usuario</label>
<div><input class="field_text_medium" id="username" name="username" type="text"/> </div>
</li>
<li>
<label class="desc">Contraseña</label>
<div><input class="field_text_medium" id="password" name="password" type="password"/></div>
</li>
<input class="submit" id="ingresar" name="ingresar" type="submit" value="INGRESAR"/>
<a class="olvidar" href="contrasenha.php" style="text-decoration:none;">¿Olvidaste tu contraseña?</a>
</ul>
</form>
<div id="loading" style="display:none; text-align:center;"><img src="img/ajax-loader.gif"/></div>
<div class="mensaje cFF0">
<span id="respuesta"></span>
</div>
</div><!--end-sign_box-->
</div><!--box_sing-->
<h1><a href="index.php"><img height="128" src="img/logo.jpg"/></a></h1>
<h3><h2 style="text-align: center;">
	ORGANO EMISOR DE NORMAS PROFESIONALES EN PARAGUAY</h2>
</h3><p>
	 </p>
</div><!--end-wrapper-header-->
<div id="menu">
<ul id="nav">
<li><a href="interna.php?pag=52">Organismos</a>
<ul>
<li><a href="interna.php?pag=52">Junta Directiva</a></li>
<li><a href="interna.php?pag=53">Tribunal de Ética</a></li>
<li><a href="interna.php?pag=54">Revisor de Cuentas</a></li>
<li><a href="interna.php?pag=58">Junta Electoral</a></li>
<li><a href="interna.php?pag=75">Direcciones</a></li>
</ul>
</li>
<li><a href="interna.php?pag=56">El Ente</a>
<ul>
<li><a href="interna.php?pag=56">Institucional</a></li>
<li><a href="interna.php?pag=59">Símbolo</a></li>
<li><a href="interna.php?pag=60">Estatutos</a></li>
</ul>
</li>
<li><a href="interna.php?pag=57">Plan Estratégico</a>
</li>
<li><a href="interna.php?pag=104">Sedes Regionales</a>
<ul>
<li><a href="interna.php?pag=104">ITAPUA</a></li>
<li><a href="interna.php?pag=105">CAAGUAZU</a></li>
<li><a href="interna.php?pag=106">ALTO PARANA</a></li>
</ul>
</li>
<li><a href="interna.php?pag=68">Matricularse</a>
<ul>
<li><a href="interna.php?pag=68">Reglamento de Matrícula</a></li>
<li><a href="interna.php?pag=70">Beneficios</a></li>
<li><a href="interna.php?pag=71">Costos</a></li>
</ul>
</li>
<li><a href="interna.php?pag=63">Matrículas Emitidas</a>
<ul>
<li><a href="interna.php?pag=63">Profesionales Habilitados</a></li>
<li><a href="interna.php?pag=119">Firmas Habilitadas</a></li>
</ul>
</li>
<li><a href="interna.php?pag=121">Adherentes</a>
<ul>
<li><a href="interna.php?pag=121">Honorarios</a></li>
<li><a href="interna.php?pag=94">Institucionales</a></li>
<li><a href="interna.php?pag=95">Individuales</a></li>
<li><a href="interna.php?pag=96">Empresariales</a></li>
</ul>
</li>
<li><a href="interna.php?pag=72">Publicaciones</a>
<ul>
<li><a href="interna.php?pag=72">Libros</a></li>
<li><a href="interna.php?pag=73">Revistas</a></li>
</ul>
</li>
<li><a href="interna.php?pag=97">Novedades Impositivas</a>
<ul>
<li><a href="interna.php?pag=97">2007</a></li>
<li><a href="interna.php?pag=98">2008</a></li>
<li><a href="interna.php?pag=92">2009</a></li>
<li><a href="interna.php?pag=93">2010</a></li>
<li><a href="interna.php?pag=116">2011</a></li>
<li><a href="interna.php?pag=120">2012</a></li>
<li><a href="interna.php?pag=125">2016</a></li>
</ul>
</li>
<li><a href="noticias.php">Noticias</a>
<ul>
<li><a href="eventos.php?t=2">Eventos Próximos</a></li>
<li><a href="eventos.php?t=3">Eventos Realizados</a></li>
<li><a href="galeria.php">Galería de Fotos</a></li>
</ul>
</li>
<li class="last"><a href="contacto.php">Contacto</a></li>
</ul><!--end-nav--> </div><!--end-menu-->
<div class="wrapper">
<div id="content-slider">
<div id="welcome" style="z-index:10;">
<h1>Bienvenidos al Sitio Web del Consejo<br/>
                  de Contadores Públicos del Paraguay.
                  </h1>
<p>
                  La misión  del Consejo es lograr la superación y formación Profesional integral  de  los contadores  del País,  para   alcanzar  una profesión                  fuerte   y  coherente, que   cumpla   con   su  responsabilidad   ante la sociedad dentro de un  sincero intercambio, y fraternal convivencia.
                  </p>
</div><!--end-welcome-->
<div id="slider">
<ul>
<li><img alt="Css Template Preview" src="img/02.jpg"/></li>
<li><img alt="Css Template Preview" src="img/01.jpg"/></li>
<li><img alt="Css Template Preview" src="img/03.jpg"/></li>
</ul>
</div><!--end-slider-->
</div><!--end-content-slider-->
</div><!--end-wrapper-->
<div id="wrapper-content">
<div class="col w300 mr20">
<h2 class="tit_news"><a href="noticias.php">Noticias y Eventos</a></h2>
<ul class="content_col">
<li>
<h3><a href="noticia.php?id=196">CAPACITACIÓN EN NORMAS DE INFORMACIÓN FINANCIERA (NIF)</a></h3>
<span class="date">06-11-2020</span>
<p>CUPOS LIMITADOS!!!</p>
</li>
<li>
<h3><a href="noticia.php?id=195">CAPACITACIÓN  Y ACTUALIZACIÓN EN NORMATIVA DEL MERCADO DE VALORES</a></h3>
<span class="date">31-07-2020</span>
<p>Válido para el cumplimiento de capacitación mínima exigida por la CNV</p>
</li>
<li>
<h3><a href="noticia.php?id=194">FORO TRIBUTARIO INTERNACIONAL</a></h3>
<span class="date">04-06-2020</span>
<p>DIA DEL CONTADOR PARAGUAYO</p>
</li>
</ul><!--end-content_col-->
<a class="link" href="noticias.php">&gt;&gt;Ver más</a>
</div><!--end-col-->
<div class="col w300 mr20">
<h2 class="tit_file"><a href="download.php">Descargas de Archivos</a></h2>
<ul class="content_col">
<li>
<h3><a href="descarga.php?id=229">RESOLUCION GENERAL Nº 73/12</a></h3>
<span class="date">11-04-2013</span>
<p>Por la cual se modifica la RG Nº 20/2008 y se deroga la RG Nº 70/2012</p>
</li>
<li>
<h3><a href="descarga.php?id=233">RESOLUCION GENERAL Nº 76/12</a></h3>
<span class="date">11-04-2013</span>
<p>POR LA CUAL SE MODIFICAN LOS ARTÍCULOS 1º Y 3º DE LA RESOLUCIÓN GENERAL N° 49 DE FECHA 9 DE MAR</p>
</li>
<li>
<h3><a href="descarga.php?id=234">RESOLUCION GENERAL Nº 77/12</a></h3>
<span class="date">11-04-2013</span>
<p>POR LA CUAL SE APRUEBAN NORMAS COMPLEMENTARIAS PARA LA APLICACIÓN DEL IMPUESTO A LAS RENTAS DE LAS </p>
</li>
</ul><!--end-content_col-->
<a class="link" href="download.php">&gt;&gt;Ver más</a>
</div><!--end-col-->
<div class="col w300">
<h2 class="tit_foro"><a href="foro.php">Foro</a></h2>
<ul class="content_col" id="foro">
<li>
<h3><a href="topic.php?id=66">NIF Nº 21  -  Reconocimiento de Ingresos</a></h3>
<span class="date">07-02-2012</span>
<p>NORMA DE INFORMACION FINANCIERA N° 21

	 

	Reconocimiento de ingresos

	
		
			
	...</p>
</li>
<li>
<h3><a href="topic.php?id=65">NIF Nº 10 - Contabilización de inversiones distintas a una inversión en una asociada o subsidiaria</a></h3>
<span class="date">07-02-2012</span>
<p>Revisando la NIF 10 encontré lo que me parece es un error de la norma.

	En el párra...</p>
</li>
<li>
<h3><a href="topic.php?id=64">RECOMENDACIONES PARA USO DEL FORO</a></h3>
<span class="date">15-11-2010</span>
<p>Los temas que se publican son –exclusivamente- aquellos que interesan a la profesión.
...</p>
</li>
</ul><!--end-content_col-->
<a class="link" href="foro.php">&gt;&gt;Ver más</a>
</div><!--end-col-->
<div class="clear"></div>
<table class="table_anuncio">
<tr>
<td><a href="http://www.pwc.com/py/es/" target="_blank" title="PWC"><img src="img/banners/small/s-110.jpg" width="125"/></a></td>
<td><a href="#" target="" title=""><img src="img/banners/small/s-112.jpg" width="125"/></a></td>
<td><a href="http://www2.deloitte.com/py/es.html" target="_blank" title="DELOITTE"><img src="img/banners/small/s-114.jpg" width="125"/></a></td>
<td><a href="#" target="" title=""><img src="img/banners/small/s-107.jpg" width="125"/></a></td>
<td><a href="http://www.cyce.com.py/" target="_blank" title="CYCE"><img src="img/banners/small/s-109.jpg" width="125"/></a></td>
<td><a href="#" target="" title=""><img src="img/banners/small/s-113.jpg" width="125"/></a></td>
</tr>
</table>
</div><!--end-wrapper-content-->
<div id="wrapper-footer">
<hr/>
<p>
	                                                                                     </p>
<p>
	 </p>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19866005-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<div class="clear"></div>
<address>
        	© 2010 Todos los Derechos Reservados<br/>
            Consejo de Contadores Públicos del Paraguay<br/>
</address>
<p id="id">Desarrollado por <a href="http://www.idtknology.com" target="_blank"><img border="0" src="img/logo_id.jpg"/></a></p> </div><!--end-wrapper-footer-->
</div>
<!--end-wrapper-principal-->
</body>
</html>
