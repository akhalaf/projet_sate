<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>:::丞永實業:::</title>
<meta content="大陸公司於2001年3月通過ISO9001認證、2006年2月通過ISO14001認證；産品遠銷東南亞、歐美等地，深受用戶好評。公司本著『品質讓客戶滿意，管理創最佳效益的宗旨』，以 『零退貨和交貨準時率100%』 爲目標，與新老客戶努力攜手邁進新紀元，地址：台北縣林口鄉中正路82號1F ，TEL: 886-2-26023370" name="description"/>
<meta content="DC Jack, Phone Jack, connecter, FPC, 連接器, 插座, USB, RJ11, RJ45, MT, DIP, Socket, Mini Din, D-SUB, RS232" name="keywords"/>
<meta content="all" name="robots"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<link href="css.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>
<body onload="MM_preloadImages('images/about_btn_over.jpg','images/product_btn_over.jpg','images/cooperation_btn_over.jpg','images/contact_btn_over.jpg','images/more_over.jpg')">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" scope="col"><table border="0" cellpadding="0" cellspacing="0" width="981">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<th align="left" scope="col" width="34%"><img border="0" height="69" src="images/logo.jpg" width="335"/></th>
<td scope="col" valign="bottom" width="66%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td height="30" scope="col" valign="top" width="86%"><marquee class="back" direction="left" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="2" scrolldelay="0">        <a href="http://www.blue-sources.com.tw/product.php?pcno=14" target="_brank"> FPC、RJ45、MINI FIT、Wire harness、客制化線材、AC Adaport 等各式連接器歡迎來信詢問</a></marquee></td>
<td class="ch12" scope="col" valign="top" width="14%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="25" valign="middle" width="13%"> </td>
<td align="right" valign="middle" width="14%"><img height="18" src="images/arrow_home.jpg" width="18"/></td>
<td align="left" class="ch12" height="25" valign="middle" width="73%"><a class="back" href="index.php">回首頁</a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="23%"><img height="34" src="images/btn01.jpg" width="226"/></td>
<td scope="col" width="21%"><a href="about_us.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/about_btn_over.jpg',1)"><img border="0" height="34" id="Image3" name="Image3" src="images/about_btn.jpg" width="203"/></a></td>
<td scope="col" width="19%"><a href="product.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/product_btn_over.jpg',1)"><img border="0" height="34" id="Image4" name="Image4" src="images/product_btn.jpg" width="185"/></a></td>
<td scope="col" width="18%"><a href="cooperation.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','images/cooperation_btn_over.jpg',1)"><img border="0" height="34" id="Image5" name="Image5" src="images/cooperation_btn.jpg" width="181"/></a></td>
<td scope="col" width="19%"><a href="contact.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/contact_btn_over.jpg',1)"><img border="0" height="34" id="Image6" name="Image6" src="images/contact_btn.jpg" width="186"/></a></td>
</tr>
</table></td>
</tr><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>:::丞永實業:::</title>
<meta content="大陸公司於2001年3月通過ISO9001認證、2006年2月通過ISO14001認證；産品遠銷東南亞、歐美等地，深受用戶好評。公司本著『品質讓客戶滿意，管理創最佳效益的宗旨』，以 『零退貨和交貨準時率100%』 爲目標，與新老客戶努力攜手邁進新紀元，地址：台北縣林口鄉中正路82號1F ，TEL: 886-2-26023370" name="description"/>
<meta content="DC Jack, Phone Jack, connecter, FPC, 連接器, 插座, USB, RJ11, RJ45, MT, DIP, Socket, Mini Din, D-SUB, RS232" name="keywords"/>
<meta content="all" name="robots"/>
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" valign="top" width="17%"><table border="0" cellpadding="0" cellspacing="0" width="5%">
<tr>
<td scope="col"><img height="443" src="images/left_banner.gif" width="226"/></td>
</tr>
<tr>
<td bgcolor="#999999" height="130" valign="middle"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="51%"><img height="115" src="images/w.jpg" width="116"/></td>
<td background="images/w_r.jpg" bgcolor="#999999" scope="col" width="49%"><table border="0" cellpadding="0" cellspacing="3" width="100%">
<tr>
<td align="left" class="ch12_w" scope="col">我們竭誠的歡迎您來電洽詢我們的商品，我們將給您滿意的服務!!</td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
<td scope="col" valign="top" width="83%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><img height="227" src="images/index_top.gif" width="755"/></td>
</tr>
<tr>
<td><img height="8" src="images/shadow_top.jpg" width="755"/></td>
</tr>
<tr>
<td background="images/shadow_left.jpg" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="2%"> </td>
<td scope="col"><img height="33" src="images/product_title.jpg" width="740"/></td>
</tr>
<tr>
<td height="305" scope="col"> </td>
<td scope="col" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="98%"><table border="0" cellpadding="0" cellspacing="6" width="100%">
<tr>
<td scope="col" width="33%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">BH254XXS-PBT</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/00.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">簡牛 P:2.54 180D DIP</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=142" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">PH20-1XXS-6T</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/PH20.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">Pin header P:2.0 單排 180D DIP</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=144" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td><img height="18" src="images/product_arrow.jpg" width="237"/></td>
</tr>
</table></td>
<td scope="col" width="33%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">PCB20-207S-D4P-6T</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/pcb20.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">Female Header P:2.0mm H:4.3 DIP</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=148" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">RJF8P8C03AA</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/RJ-4.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">水晶頭</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=135" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td><img height="18" src="images/product_arrow.jpg" width="237"/></td>
</tr>
</table></td>
<td scope="col" width="33%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">CW203R-XX-LF</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/W4.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">Wafer P:2.0mm 90D DIP</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=147" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td scope="col">
<table border="0" cellpadding="0" cellspacing="0" class="table_style" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td scope="col" width="3%"><img height="30" src="images/red_line.jpg" width="13"/></td>
<td align="left" class="ch11_black" scope="col" width="97%">電子線</td>
</tr>
</table></td>
</tr>
<tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="right" height="90" scope="col" width="51%"><img height="72" src="./pic/product/C-3.jpg" width="104"/></td>
<td scope="col" width="49%"><table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
<td align="left" class="ch11_b" height="65" scope="col">170</td>
</tr>
<tr>
<td align="right"><a href="product_content.php?pno=128" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image14','','images/more_over.jpg',1)"><img border="0" height="15" id="Image14" name="Image14" src="images/more.jpg" width="41"/></a></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
<tr>
<td><img height="18" src="images/product_arrow.jpg" width="237"/></td>
</tr>
</table></td>
</tr>
</table></td>
<td scope="col" width="2%"> </td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td scope="col"><img height="2" src="images/blue_line.jpg" width="981"/></td>
</tr>
<tr>
<td scope="col"><table border="0" cellpadding="0" cellspacing="5" width="100%">
<tr>
<td align="left" class="ch11_b" height="28" scope="col">Copyright ©  2009 - 2018 丞永實業有限公司 ( BLUE SOURCES ELECTRONICS CO.,LTD. ), All Rights Reserved.<img height="1" src="images/space.jpg" width="10"/> | <img height="1" src="images/space.jpg" width="5"/>Design by <a class="menu" href="http://www.vj007.com/" target="_blank">VJ007瑞翌設計</a><br/>
地址：新北市林口區自強二街58號 <img height="1" src="images/space.jpg" width="15"/>Adress: NO. 58 zih ciang two street Linkou TOWNSHIP, TAIPEI COUNTY 244, TAIWAN<img height="1" src="images/space.jpg" width="15"/>TEL: 886-2-26023370<img height="1" src="images/space.jpg" width="15"/>FAX: 886-2-81926776</td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
