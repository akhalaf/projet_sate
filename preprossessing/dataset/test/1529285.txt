<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Etat-civil – Gestion des rendez-vous de papiers d'identité</title>
<meta content="noindex,nofollow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://vdr99.valdereuil.fr/feed/" rel="alternate" title="Etat-civil » Flux" type="application/rss+xml"/>
<link href="https://vdr99.valdereuil.fr/comments/feed/" rel="alternate" title="Etat-civil » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/vdr99.valdereuil.fr\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://vdr99.valdereuil.fr/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Gentium+Basic%3A700%7CGentium+Book+Basic%3A400%2C400italic%2C700%7CSlabo+27px%3A400&amp;subset=latin%2Clatin-ext" id="write-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/genericons/genericons.css?ver=3.4.1" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/css/normalize.css?ver=8.0.0" id="normalize-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/style.css?ver=2.1.2" id="write-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/css/drawer.css?ver=3.2.2" id="drawer-style-css" media="screen and (max-width: 782px)" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/sln-bootstrap.css?ver=20201028" id="salon-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/select2.min.css?scope=sln&amp;ver=4.9.3" id="salon-admin-select2-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/booking-calendar-shortcode/css/style.css?ver=20201028" id="salon-booking-calendar-shortcode-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/salon.css?ver=20201028" id="salon-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://vdr99.valdereuil.fr/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="iscroll-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/iscroll.js?ver=5.2.0" type="text/javascript"></script>
<script id="drawer-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/drawer.js?ver=3.2.2" type="text/javascript"></script>
<script id="salon-admin-select2-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/select2.min.js?scope=sln&amp;ver=1" type="text/javascript"></script>
<link href="https://vdr99.valdereuil.fr/wp-json/" rel="https://api.w.org/"/><link href="https://vdr99.valdereuil.fr/wp-json/wp/v2/pages/10" rel="alternate" type="application/json"/><link href="https://vdr99.valdereuil.fr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://vdr99.valdereuil.fr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<link href="https://vdr99.valdereuil.fr/" rel="canonical"/>
<link href="https://vdr99.valdereuil.fr/" rel="shortlink"/>
<link href="https://vdr99.valdereuil.fr/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fvdr99.valdereuil.fr%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://vdr99.valdereuil.fr/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fvdr99.valdereuil.fr%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">
		/* Colors */
				
			</style>
</head>
<body class="home page-template-default page page-id-10 sln-salon-page drawer header-side footer-side no-sidebar footer-0">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="site-top">
<div class="site-top-table">
<div class="site-branding">
<h1 class="site-title"><a href="https://vdr99.valdereuil.fr/" rel="home">Etat-civil</a></h1>
<div class="site-description">Gestion des rendez-vous de papiers d'identité</div>
</div><!-- .site-branding -->
<nav class="main-navigation" id="site-navigation">
<button class="drawer-toggle drawer-hamburger">
<span class="screen-reader-text">Menu</span>
<span class="drawer-hamburger-icon"></span>
</button>
<div class="drawer-nav">
<div class="drawer-content">
<div class="drawer-content-inner">
<div class="menu-navig-container"><ul class="menu" id="menu-navig"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-26" id="menu-item-26"><a aria-current="page" href="https://vdr99.valdereuil.fr">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-10 current_page_item menu-item-25" id="menu-item-25"><a aria-current="page" href="https://vdr99.valdereuil.fr/">Gestion des rendez-vous</a></li>
</ul></div> <form action="https://vdr99.valdereuil.fr/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Rechercher :</span>
<input class="search-field" name="s" placeholder="Rechercher…" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Rechercher"/>
</form> </div><!-- .drawer-content-inner -->
</div><!-- .drawer-content -->
</div><!-- .drawer-nav -->
</nav><!-- #site-navigation -->
</div><!-- .site-top-table -->
</div><!-- .site-top -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<article class="post-10 page type-page status-publish hentry" id="post-10">
<header class="entry-header">
<h2 class="entry-title">Gestion des rendez-vous</h2>
</header><!-- .entry-header -->
<div class="entry-content">
<div class="sln-bootstrap container-fluid sln-salon--l sln-step-services" id="sln-salon">
<h2 class="sln-salon-title">Réservez un rendez-vous</h2>
<form action="/?sln_step_page=services" id="salon-step-services" method="post" role="form">
<h2 class="salon-step-title">De quoi avez-vous besoin ?</h2>
<div class="row sln-box--main">
<div class="col-xs-12 col-md-8"> <input class="sln-input sln-input--text" id="sln_date" name="sln[date]" type="hidden" value="2021-01-13"/>
<input class="sln-input sln-input--text" id="sln_time" name="sln[time]" type="hidden" value="20:30"/>
<div class="sln-service-list">
<div class="row sln-panel">
<a aria-controls="collapse2" aria-expanded="false" class="col-xs-12 sln-panel-heading collapsed " data-toggle="collapse" href="#collapse2" role="button">
<h2 class="sln-btn sln-btn--icon sln-btn--fullwidth">
                    Papiers d'identité</h2>
</a>
<div aria-expanded="false" aria-labelledby="collapse2Heading" class="col-xs-12 sln-panel-content panel-collapse collapse " id="collapse2" role="tabpanel" style="height: 0px;">
<label class="row sln-service sln-service--184" for="sln_services_184">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="25" data-price="0" id="sln_services_184" name="sln[services][184]" type="checkbox" value="1"/>
<label for="sln_services_184"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE PASSEPORT ADULTE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p></p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:25</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--57" for="sln_services_57">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="25" data-price="0" id="sln_services_57" name="sln[services][57]" type="checkbox" value="1"/>
<label for="sln_services_57"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE CARTE IDENTITE ADULTE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>ATTENTION certaine carte d'identité bénéficie d'une prolongation de 5 ans</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:25</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--356" for="sln_services_356">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="30" data-price="0" id="sln_services_356" name="sln[services][356]" type="checkbox" value="1"/>
<label for="sln_services_356"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE CARTE IDENTITE ENFANT</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:30</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--369" for="sln_services_369">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="30" data-price="0" id="sln_services_369" name="sln[services][369]" type="checkbox" value="1"/>
<label for="sln_services_369"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE PASSEPORT ENFANT</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:30</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--367" for="sln_services_367">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="35" data-price="0" id="sln_services_367" name="sln[services][367]" type="checkbox" value="1"/>
<label for="sln_services_367"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE CARTE IDENTITE ENFANT AVEC JUGEMENT DE GARDE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:35</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--372" for="sln_services_372">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="35" data-price="0" id="sln_services_372" name="sln[services][372]" type="checkbox" value="1"/>
<label for="sln_services_372"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE PASSEPORT ENFANT AVEC JUGEMENT DE GARDE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:35</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--374" for="sln_services_374">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="5" data-price="0" id="sln_services_374" name="sln[services][374]" type="checkbox" value="1"/>
<label for="sln_services_374"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">RETRAIT CARTE IDENTITE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p></p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:05</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--376" for="sln_services_376">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="10" data-price="0" id="sln_services_376" name="sln[services][376]" type="checkbox" value="1"/>
<label for="sln_services_376"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">RETRAIT PASSEPORT</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p></p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:10</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--378" for="sln_services_378">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="15" data-price="0" id="sln_services_378" name="sln[services][378]" type="checkbox" value="1"/>
<label for="sln_services_378"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">RETRAIT CARTE IDENTITE ET PASSEPORT</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p></p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:15</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--415" for="sln_services_415">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="30" data-price="0" id="sln_services_415" name="sln[services][415]" type="checkbox" value="1"/>
<label for="sln_services_415"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE CARTE IDENTITE ET PASSEPORT ADULTE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p></p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:30</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--417" for="sln_services_417">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="35" data-price="0" id="sln_services_417" name="sln[services][417]" type="checkbox" value="1"/>
<label for="sln_services_417"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE DE CARTE IDENTITE ET PASSEPORT ENFANT</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:35</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<label class="row sln-service sln-service--419" for="sln_services_419">
<div class="col-xs-12 sln-service__header">
<div class="row sln-steps-info sln-service-info">
<div class="col-xs-2 col-sm-1 col-xs-push-10 col-sm-push-0 sln-checkbox sln-steps-check sln-service-check">
<div class="sln-checkbox">
<input data-duration="40" data-price="0" id="sln_services_419" name="sln[services][419]" type="checkbox" value="1"/>
<label for="sln_services_419"></label>
</div>
<!-- .sln-service-check // END -->
</div>
<div class="col-xs-10 col-sm-8 col-xs-pull-2 col-sm-pull-0">
<h3 class="sln-steps-name sln-service-name">DEMANDE CARTE IDENTITE ET PASSEPORT ENFANT AVEC JUGEMENT DE GARDE</h3>
</div>
<div class="col-xs-10 col-sm-3 col-xs-push-10- col-sm-push-0">
</div>
<!-- .sln-service-info // END -->
</div>
</div>
<div class="col-xs-12">
<div class="row sln-steps-description sln-service-description">
<!--<div class="col-xs-12"><hr></div>
            <div class="col-xs-12 col-sm-1 col-md-1 hidden-xs hidden-sm">&nbsp;</div>-->
<div class="col-xs-12 col-md-9 col-sm-offset-1">
<p>La présence du mineur est obligatoire</p>
<span class="sln-steps-duration sln-service-duration"><small>Durée:</small> 00:40</span>
<!-- .sln-service-info // END -->
</div>
</div>
</div>
<div class="col-xs-12">
<div class="row">
<div class="col-xs-11 col-md-offset-1">
<span class="errors-area" data-class="sln-alert sln-alert-medium sln-alert--problem">
<div class="sln-alert sln-alert-medium sln-alert--problem" id="availabilityerror" style="display: none">Pas assez de temps pour ce service</div>
</span>
</div>
</div>
</div>
<div class="sln-service__fkbkg"></div>
</label>
<!-- panel END -->
</div>
</div>
<!-- panel END -->
<!-- .sln-service-list // END -->
</div>
</div>
<div class="col-xs-12 col-md-4 sln-box--formactions"> <div class="sln-box--formactions form-actions row">
<div class="col-xs-12 pull-right">
<div class="sln-btn sln-btn--emphasis sln-btn--medium sln-btn--fullwidth">
<button data-salon-data="sln_step_page=services&amp;submit_services=next" data-salon-toggle="next" id="sln-step-submit" name="submit_services" type="submit" value="next">
                Étape suivante <i class="glyphicon glyphicon-chevron-right"></i>
</button>
</div>
</div>
</div>
</div>
</div>
</form>
<div id="sln-notifications"></div>
</div>
</div><!-- .entry-content -->
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="site-bottom">
<div class="site-bottom-table">
<nav class="footer-social-link social-link" id="footer-social-link">
</nav><!-- #footer-social-link -->
<div class="site-info">
<div class="site-credit">
						Powered by <a href="https://wordpress.org/">WordPress</a> <span class="site-credit-sep"> | </span>
						Theme: <a href="http://themegraphy.com/wordpress-themes/write/">Write</a> by Themegraphy					</div><!-- .site-credit -->
</div><!-- .site-info -->
</div><!-- .site-bottom-table -->
</div><!-- .site-bottom -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script id="write-skip-link-focus-fix-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/skip-link-focus-fix.js?ver=20160525" type="text/javascript"></script>
<script id="double-tap-to-go-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/doubletaptogo.js?ver=1.0.0" type="text/javascript"></script>
<script id="write-functions-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/functions.js?ver=20190226" type="text/javascript"></script>
<script id="salon-raty-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/jquery.raty.js?ver=20201028" type="text/javascript"></script>
<script id="salon-js-extra" type="text/javascript">
/* <![CDATA[ */
var salon = {"ajax_url":"https:\/\/vdr99.valdereuil.fr\/wp-admin\/admin-ajax.php?lang=fr","ajax_nonce":"90b5e70603","loading":"https:\/\/vdr99.valdereuil.fr\/wp-content\/plugins\/salon-booking-system\/img\/preloader.gif","txt_validating":"v\u00e9rification de la disponibilit\u00e9","images_folder":"https:\/\/vdr99.valdereuil.fr\/wp-content\/plugins\/salon-booking-system\/img","confirm_cancellation_text":"Voulez-vous vraiment annuler ?","time_format":"hh:ii","has_stockholm_transition":"no","checkout_field_placeholder":"remplissez ce champ","txt_close":"Ferm\u00e9"};
/* ]]> */
</script>
<script id="salon-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/salon.js?ver=20201028" type="text/javascript"></script>
<script id="salon-bootstrap-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/bootstrap.min.js?ver=20201028" type="text/javascript"></script>
<script id="smalot-datepicker-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/bootstrap-datetimepicker.js?ver=20140711" type="text/javascript"></script>
<script id="smalot-datepicker-lang-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/datepicker_language/bootstrap-datetimepicker.fr_FR.js?ver=2016-02-16" type="text/javascript"></script>
<script id="salon-customSelect2-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/admin/customSelect2.js?scope=sln&amp;ver=20201028" type="text/javascript"></script>
<script id="salon-my-account-js-extra" type="text/javascript">
/* <![CDATA[ */
var salonMyAccount_l10n = {"success":"Profile updated successfully."};
/* ]]> */
</script>
<script id="salon-my-account-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/salon-my-account.js?ver=20201028" type="text/javascript"></script>
<script id="wp-embed-js" src="https://vdr99.valdereuil.fr/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
