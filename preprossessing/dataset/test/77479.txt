<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Adrenocortical Carcinoma, Adrenal Cancer, Adrenal Tumor" name="Description"/>
<meta content="Aaron Ganschow, aaron@ganschow.us" name="author"/>
<link href="/css/style.css" rel="stylesheet" type="text/css"/>
<title>Adrenal Cancer Support</title>
</head>
<body>
<div id="head">
<div id="title">
AdrenalCancerSupport</div>
<div id="menu">
<ul>
<li class="active">
<a href="index.html" title="Home">Home</a>
</li>
<li>
<a href="help.html" title="Support">Support</a>
</li>
<li>
<a href="treatment.html" title="Treatment">Treatment</a>
</li>
<li>
<a href="health.html" title="Health">Health</a>
</li>
</ul>
</div>
</div>
<div id="body_wrapper">
<div id="body">
<div id="split">
<div class="top"></div>
<div id="left">
<div class="content">
<h1>Welcome</h1>
<h2><img align="right" alt="TGEN Symposium" height="250" longdesc="http://www.atacfund.org" src="/images/tgenSymposiumBorder.jpg" width="350"/></h2>
<p> The links and tools noted on this site have been gleaned from patient discussions on various ACC message boards (see Support page). </p>
<p>This site is not meant to advocate one approach over another. It should only be used to inspire research of your own. Submissions for new links and physician referrals can be submitted via the Contact Us form. Feedback is always welcomed. </p>
<table cellpadding="0" cellspacing="0">
<tr>
<td align="left" colspan="2" valign="middle"><ul>
<h4>What is Adrenocortical Carcinoma? </h4>
<li><a href="http://www.cancer.gov/cancertopics/types/adrenocortical">Adrenocortical Carcinoma for Patients</a></li>
<li><a href="http://www.cancer.gov/cancertopics/pdq/treatment/adrenocortical/HealthProfessional">Adrenocortical Carcinoma for Healthcare Professionals</a></li>
<li><a href="http://www.endotext.org/adrenal/adrenal22/adrenalframe22.htm">Current Issues in the Diagnosis and Management of Adrenocortical Carcinomas</a></li>
<li><a href="http://documents.cancer.org/6641.00/6641.00.pdf">American Cancer Society ACC Guide</a></li>
<li><a href="http://www.endocrineweb.com/adreca.html">Endocrine Web</a></li>
<li><a href="http://www.ensat.org/acc.htm">European Network for the Study of Adrenal Tumours</a></li>
<li><a href="http://www.cancer.med.umich.edu/cancertreat/endocrine/adrenal_resources_guide.shtml ">University of Michigan Comprehensive Cancer Center’s Adrenal Cancer Information Guide</a></li>
<li><a href="http://www.oncolink.org/types/article.cfm?c=4&amp;s=6&amp;ss=821&amp;id=9540&amp;CFID=2108569&amp;CFTOKEN=69967896">Adrenal Cancer: The Basics</a></li>
</ul></td>
</tr>
<tr>
<td valign="top"><ul>
<h4>ACC Treatment Overviews</h4>
<li><a href="http://www.zuidencomm.nl/njm/getpdf.php?id=10000147">The Netherlands Journal of Medicine - Treatment, Therapy</a></li>
<li><a href="http://imsdd.meb.uni-bonn.de/cancer.gov/CDR0000062704.html">National Cancer Institute Med News</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19500769">Clinical management of adrenocortical carcinoma.</a></li>
<li><a href="http://www.adrenocorticalcarcinoma.com/"></a><a href="http://www.ncbi.nlm.nih.gov/pubmed/19635227">Management of adrenocortical carcinoma</a></li>
<li><a href="http://www.adrenocorticalcarcinoma.com/">Help dedicated to patients, caregivers, physicians, and researchers</a></li>
<li><a href="http://jcem.endojournals.org/cgi/content/full/91/1/14">Emerging Treatment Strategies for Adrenocortical Carcinoma: A New Hope</a></li>
<li> <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=search&amp;db=PubMed&amp;term=Adrenocortical%20Carcinoma"></a><a href="http://www.informapharmascience.com/doi/abs/10.1517/14728214.13.3.497">Emerging drugs for adrenocortical carcinoma</a> - Full Text $65 </li>
</ul></td>
</tr>
<tr>
<td colspan="2" valign="top"><ul>
<h4>Prognosis Articles <a id="Prognosis" name="Prognosis"></a></h4>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/18973179">Adrenocortical carcinoma in the United States: <strong>treatment utilization and prognostic factors</strong></a><strong></strong></li>
<li><a href="http://jco.ascopubs.org/cgi/content/full/20/4/941">Adrenocortical Carcinoma: Clinical, Morphologic, and Molecular <strong>Characterization</strong></a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/18973179">Adrenocortical carcinoma in the United States: Treatment utilization and <strong>prognostic factors</strong></a></li>
<li><a href="http://archsurg.ama-assn.org/cgi/reprint/134/2/181.pdf"><strong>Pathologic Features</strong> of Prognostic Significance for Adrenocortical Carcinoma After Curative Resection</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19025987"><strong>Limited prognostic value</strong> of the 2004 International Union Against Cancer staging classification for adrenocortical carcinoma: proposal for a Revised TNM Classification</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19451490"><strong>Limited value of adrenal biopsy</strong> in the evaluation of adrenal neoplasm: a decade of experience.</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/10622498"><strong>Long-Term Survival After Complete Resection</strong> and Repeat Resection in Patients With Adrenocortical Carcinoma</a></li>
<li><a href="http://jcem.endojournals.org/cgi/content/abstract/91/7/2650">Clinical and Biological Features in the Prognosis of Adrenocortical Cancer: Poor Outcome of <strong>Cortisol-Secreting Tumors</strong> in a Series of 202 Consecutive Patients</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19442788">The <strong>Weiss system</strong> for evaluating adrenocortical neoplasms: 25 years later</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/20044246?itool=Email.EmailReport.Pubmed_ReportSelector.Pubmed_RVDocSum&amp;ordinalpos=3">The European Network for the Study of Adrenal Tumors staging system is prognostically superior to the international union against <strong>cancer-staging system</strong>: a North American validation</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19912359?itool=Email.EmailReport.Pubmed_ReportSelector.Pubmed_RVDocSum&amp;ordinalpos=2">Clinicopathological study of a series of 92 adrenocortical carcinomas: from a proposal of <strong>simplified diagnostic algorithm</strong> to prognostic stratification</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19198016?ordinalpos=3&amp;itool=Email.EmailReport.Pubmed_ReportSelector.Pubmed_RVDocSum"><strong>Estrogen receptor</strong> expression in adrenocortical carcinoma.</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19031940?ordinalpos=2&amp;itool=Email.EmailReport.Pubmed_ReportSelector.Pubmed_RVDocSum">Immunohistochemical evaluation of metallothionein, <strong>Mcm-2 and Ki-67 </strong>antigen expression in tumors of the adrenal cortex.</a></li>
<li><a href="http://www.ncbi.nlm.nih.gov/pubmed/19465749">Glucose transporter <strong>GLUT1 expression</strong> is an stage-independent predictor of clinical outcome in adrenocortical carcinoma</a></li>
<li> <a href="http://www.ncbi.nlm.nih.gov/pubmed/19326954?itool=Email.EmailReport.Pubmed_ReportSelector.Pubmed_RVDocSum&amp;ordinalpos=3">Adrenal cortical neoplasms <strong>in children</strong>: why so many carcinomas and yet so many survivors?</a></li>
</ul>
<p> </p></td>
</tr>
<tr>
<td colspan="2" valign="top"> </td>
</tr>
</table>
</div>
</div>
<div id="right">
<div class="content">
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td align="center"><input onclick="window.open('drs.html','mywindow','width=800,height=700,scrollbars=yes')" type="button" value="Find Physicians"/></td>
</tr>
</table>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td height="27"><div align="center">
<input onclick="window.open('cushings.html','mywindow','width=800,height=500,scrollbars=yes')" type="button" value="Cushings"/>
</div></td>
</tr>
</table>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td height="27"><div align="center">
<input onclick="window.open('donations.html','mywindow','width=800,height=600')" type="button" value="Donations"/>
</div></td>
</tr>
</table>
<p></p>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td align="center"><input onclick="window.open('questions.html','mywindow','width=800,height=700')" type="button" value="Contact Us"/></td>
</tr>
</table><p> </p>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td><a href="http://www.youtube.com/watch?v=OIB38OUTm3E"><strong><em>TGEN's ATAC Fund Video</em></strong></a></td>
</tr>
</table>
<p> </p>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td><a href="http://vitaloptions.org/grouproom_archives_632.htm"><em><strong>International Adrenal Cancer Symposium: Clinical and Basic Science</strong></em></a></td>
</tr>
</table>
<p> </p>
<table align="left" bgcolor="#FFFFFF" border="0" cellpadding="4" cellspacing="0" width="135">
<tr>
<td><a href="http://www.vitaloptions.org/grouproom_archives_602.htm"><strong><em>Rare Cancers &amp; Unusual Clinical Situations, with a special feature on Adrenal Cancers</em></strong></a></td>
</tr>
</table>
</div>
</div>
<div class="clearer"></div>
<div class="bottom"></div>
</div>
<div class="clearer"></div>
</div>
<div class="clearer"></div>
</div>
<div id="end_body"></div>
<div id="footer">Template Designed by <a href="http://aaron.ganschow.us/">Aaron Ganschow</a> and downloaded from <a href="http://www.oswd.org">Open Source Web Design</a>.<br/>
</div>
</body>
</html>
