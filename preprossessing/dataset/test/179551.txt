<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>AYK Associates</title>
<link href="css/ayk.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
a:link {
	color: #000000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
	color: #FFFFFF;
}
a:active {
	text-decoration: none;
}
body,td,th {
	font-family: Trebuchet MS;
	font-size: 12px;
	color: #000066;
}
a {
	font-family: Trebuchet MS;
	font-size: 12px;
}
.style3 {color: #FFFFFF}
.style4 {font-size: 11px}
-->
</style></head>
<body bgcolor="#CCCCCC">
<br/>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="border" width="800">
<!--DWLayoutTable-->
<tr>
<td colspan="8" height="102">
<img alt="" height="102" src="images/banner.jpg" width="800"/></td>
</tr>
<tr>
<td height="2" width="14"></td>
<td width="249"></td>
<td width="12"></td>
<td width="249"></td>
<td width="13"></td>
<td width="249"></td>
<td width="2"></td>
<td width="12"></td>
</tr>
<tr>
<td colspan="8" height="34" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="26" valign="top" width="793"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="26" width="248"> </td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="onmouse" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a class="style3" href="index.htm">Home</a></td>
</tr>
</table></td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="button_middle" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a href="aboutus.htm">About Us </a></td>
</tr>
</table></td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="button_middle" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a href="services.htm">Services </a></td>
</tr>
</table></td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="button_middle" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a href="mainprofile.htm">Profile</a></td>
</tr>
</table></td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="button_middle" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a href="clients.htm">Clients</a></td>
</tr>
</table></td>
<td valign="top" width="78"><table border="0" cellpadding="0" cellspacing="0" class="button_middle" width="100%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="78"><a href="feedback.htm">Feedback</a></td>
</tr>
</table></td>
<td valign="top" width="77"><table border="0" cellpadding="0" cellspacing="0" class="button_right" width="98%">
<!--DWLayoutTable-->
<tr>
<td align="center" class="toplink" height="26" valign="middle" width="77"><a href="contactus.htm">Contact Us</a> </td>
</tr>
</table></td>
</tr>
</table></td>
<td width="7"> </td>
</tr>
<tr>
<td height="8"></td>
<td></td>
</tr>
</table></td>
</tr>
<tr>
<td height="14"></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td height="283"></td>
<td colspan="6" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="13" width="279"></td>
<td width="494"></td>
</tr>
<tr>
<td class="welcome" height="30" valign="top">Welcome          </td>
<td></td>
</tr>
<tr>
<td colspan="2" height="240" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td class="txt" height="206" valign="top" width="773"><p align="justify">AYK Associates is an audit, accountancy and consulting firm managed by a team of professionals with several years of rich experience in various fields. The firm provides business solutions in auditing, accounting and management consulting to businesses and other organisations.<br/>
<br/>
<br/>
	            Established in 1999, AYK Associates has 4 (Four) qualified Chartered Accountants and support staff of 10 (Ten) who are at various stages of professional qualification, as well as a number of Associates in a broad spectrum of disciplines. <br/>
<br/>
<br/>
	            The combined experiences of our professionals cut across several business sectors, including industry, finance and commerce, as well as non-governmental organisations and donor-funded projects. <br/>
<br/>
<br/>
	            Several years of overseas experience and training within Africa, and in the United States of America and Europe ensure important international exposure in addition to a firm grounding in the local environment.</p></td>
</tr>
<tr>
<td height="2"></td>
</tr>
<!--DWLayoutTable-->
</table></td>
</tr>
<!--DWLayoutTable-->
</table></td>
<td></td>
</tr>
<tr>
<td height="244"></td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td colspan="2" height="47" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="banner" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="47" width="5"> </td>
<td class="minitxt" valign="middle" width="233">Audit &amp; Accountancy </td>
<td width="11"> </td>
</tr>
</table></td>
</tr>
<tr>
<td height="3" width="248"></td>
<td width="1"></td>
</tr>
<tr>
<td height="165" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="smallborder" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="6" width="5"></td>
<td width="240"></td>
</tr>
<tr>
<td height="141"></td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td rowspan="8" valign="top" width="72"><img height="107" src="images/icon2.jpg" width="72"/></td>
<td height="39" width="4"> </td>
<td class="txt" valign="top" width="168"><img height="7" src="images/bullet.gif" width="8"/> <span class="style4">Tax compliance and tax    planning for companies    and individuals</span></td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="13"></td>
<td class="txt" valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Due diligence reporting</span></td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="13"></td>
<td class="txt" valign="middle"><img height="7" src="images/bullet.gif" width="8"/> Business valuations</td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="26"></td>
<td class="txt" valign="top"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/></span> <span class="style4">Advising companies share    floatation</span></td>
</tr>
<tr>
<td height="6"></td>
<td></td>
</tr>
</table></td>
</tr>
<tr>
<td height="16"></td>
<td></td>
</tr>
</table></td>
<td></td>
</tr>
<tr>
<td height="29"> </td>
<td></td>
</tr>
</table></td>
<td> </td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td colspan="2" height="47" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="banner" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="47" width="5"> </td>
<td class="minitxt" valign="middle" width="227">Finance &amp; Management </td>
<td width="17"> </td>
</tr>
</table></td>
</tr>
<tr>
<td height="3" width="248"></td>
<td width="1"></td>
</tr>
<tr>
<td height="165" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="smallborder" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="7" width="6"></td>
<td width="240"></td>
</tr>
<tr>
<td height="141"></td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td rowspan="8" valign="top" width="72"><img height="107" src="images/icon1.jpg" width="72"/></td>
<td height="13" width="6"></td>
<td class="txt" valign="middle" width="166"><img height="7" src="images/bullet.gif" width="8"/> <span class="style4">Financing and investment</span> </td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="13"></td>
<td valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Project evaluation</span></td>
</tr>
<tr>
<td height="5"></td>
<td></td>
</tr>
<tr>
<td height="26"></td>
<td valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Project management and <br/>
   quality assurance</span></td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="26"></td>
<td valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Restructuring and    privatisation</span></td>
</tr>
<tr>
<td height="16"></td>
<td></td>
</tr>
</table></td>
</tr>
<tr>
<td height="15"></td>
<td></td>
</tr>
</table></td>
<td></td>
</tr>
<tr>
<td height="29"> </td>
<td></td>
</tr>
</table></td>
<td> </td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td colspan="2" height="47" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="banner" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="47" width="5"> </td>
<td class="minitxt" valign="middle" width="233">Consultancy</td>
<td width="11"> </td>
</tr>
</table></td>
</tr>
<tr>
<td height="3" width="248"></td>
<td width="1"></td>
</tr>
<tr>
<td height="165" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="smallborder" width="100%">
<!--DWLayoutTable-->
<tr>
<td height="7" width="6"></td>
<td width="240"></td>
</tr>
<tr>
<td height="137"></td>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td rowspan="5" valign="top" width="73"><img height="107" src="images/icon.jpg" width="73"/></td>
<td height="26" width="4"> </td>
<td class="txt" valign="middle" width="167"><img height="7" src="images/bullet.gif" width="8"/> Development of business    plans and proposals</td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="39"></td>
<td valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Corporate innovation and <br/>
   entrepreneurship    development</span></td>
</tr>
<tr>
<td height="4"></td>
<td></td>
</tr>
<tr>
<td height="34"></td>
<td rowspan="2" valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/> Staff recruitment, skills    and training needs    assessment </span></td>
</tr>
<tr>
<td height="5"></td>
<td></td>
</tr>
<tr>
<td height="4"></td>
<td></td>
<td></td>
</tr>
<tr>
<td height="19"></td>
<td></td>
<td valign="middle"><span class="txt"><img height="7" src="images/bullet.gif" width="8"/></span> <span class="txt">Staff training</span></td>
</tr>
</table></td>
</tr>
<tr>
<td height="19"></td>
<td> </td>
</tr>
</table></td>
<td></td>
</tr>
<tr>
<td height="29"> </td>
<td></td>
</tr>
</table></td>
<td> </td>
<td> </td>
</tr>
<tr>
<td colspan="8" height="18" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
<!--DWLayoutTable-->
<tr>
<td class="copyright" height="12" valign="middle" width="256">     Designed &amp; Hosted by <a href="http://www.businessghana.com" target="_blank">BusinessGhana.com</a> </td>
<td width="326"></td>
<td valign="middle" width="205"><span class="copyright">   Copyright 2007| AYK &amp; Associates </span></td>
<td width="13"></td>
</tr>
</table></td>
</tr>
<tr>
<td height="1">
<img alt="" height="1" src="images/spacer.gif" width="14"/></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
</body>
</html>
