<!DOCTYPE html>
<!-- saved from url=(0059)http://s3-us-west-1.amazonaws.com/clkim/oops/index_new.html --><html lang="en"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta content="Clkim - This link is broken. The short link you have entered doesn't have a destination. Please check your link and make sure you've entered the right one." name="description"/>
<meta content="Clkim" name="author"/>
<!--<meta http-equiv="refresh" content="90;url=https://clkim.com/">-->
<!-- Favicon -->
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="http://s3-us-west-1.amazonaws.com/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="http://s3-us-west-1.amazonaws.com/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png"/>
<link href="http://s3-us-west-1.amazonaws.com/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="http://s3-us-west-1.amazonaws.com/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png"/>
<link href="http://s3-us-west-1.amazonaws.com/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<title>Clkim - Broken Link</title>
<meta content="1490324971235861" property="fb:app_id"/>
<meta content="website" property="og:type"/>
<meta content="Clkim" property="og:site_name"/>
<meta content="http://oops.clkim.com" property="og:url"/>
<meta content="Clkim - This link is broken." property="og:title"/>
<meta content="The short link you have entered doesn't have a destination. Please check your link and make sure you've entered the right one." property="og:description"/>
<meta content="oops.png" property="og:image"/>
<meta content="618" property="og:image:width"/>
<meta content="460" property="og:image:height"/>
<meta content="summary" property="twitter:card"/>
<meta content="http://oops.clkim.com" property="twitter:url"/>
<meta content="Clkim - This link is broken." property="twitter:title"/>
<meta content="The short link you have entered doesn't have a destination. Please check your link and make sure you've entered the right one." property="twitter:description"/>
<meta content="oops.png" property="twitter:image"/>
<!-- Bootstrap core CSS -->
<link href="bootstrap.min.css" rel="stylesheet"/>
<style type="text/css">

        .heading h3 {
          font-weight: bold;
          font-size: 34px;
          margin-top: 0;
        }

        .text-center {
          margin-top: 50px;
        }

        .heading strong {
          font-size: 18px;
          margin-top: 23px;
          display: block;
          color: #424141;
        }

        .heading p {
          font-size: 16px;
          line-height: 29px;
          color: #616060;
        }

        .cta-default {
          margin-top: 44px;
          background-color: #151515;
          padding: 24px 0;
        }

        .cta-default .row {
            background-color: #151515;
        }

        .cta-default .container h5 {
          font-size: 20px;
          font-weight: bold;
          color: white;
        }

        .cta-default .container p {
          color: #737272;
          font-weight: 700;
        }

        .cta-default .container .btn {
          border-radius: 0;
          padding: 12px 50px;
          font-weight: bold;
          text-transform: uppercase;
          position: relative;
        }

        .cta-default .container .btn i{
          background-color: #3e7da7;
          padding: 18px;
          position: absolute;
          top: -1px;
          right: -49px;
        }

        .cta-default .container .btn-primary {
          background-color: #41a2d4;
          padding: 14px 28px;
          margin-right: 40px;
        }

        .maintenance-icon img{
          width: 100%;
        }

        .contact-button {
          float: right;
        }

        @media (min-height: 801px)  {
          .cta-default {
            position: absolute;
            bottom: 0;
            width: 100%;
          }
        }
    </style>
</head>
<body>
<!-- Start contain wrapp -->
<div class="contain-wrapp">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center">
<a href="https://clkim.com/"><img src="logo.png" style="margin-bottom: 15px;"/></a>
<div class="heading">
<h3>Yikes, this link is broken!</h3>
<p><strong>Our links are case sensative!</strong>
						The short link you have entered doesn't have a destination.<br/>
						Please check your link and make sure you've entered the right one.</p>
</div>
<div class="maintenance-wrapper">
<div class="maintenance-icon">
<img alt="Maintenance icon" src="broken.png"/>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- End contain wrapp -->
<!-- Start cta primary -->
<div class="cta-wrapper cta-default">
<div class="container">
<div class="row">
<div class="col-md-8 col-sm-12 col-xs-12">
<h5>This link is powered by Clkim</h5>
<p>If you feel you have reached this page in error please contact us and let us know.</p>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
<a class="btn btn-primary btn-icon contact-button" href="https://clkim.com/contact">Contact Us <i class="fa fa-envelope"></i></a>
</div>
</div>
</div>
</div>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-55270039-1', 'auto');
    ga('send', 'pageview');

  </script>
</body>
</html>
