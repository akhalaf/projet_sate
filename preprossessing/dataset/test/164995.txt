<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "4519",
      cRay: "610f0d2dfa3d17bf",
      cHash: "26a17f17c2e0e47",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXRjb3dvcmxkLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "ajjIrMe8/oMyoAZZvYGao9k7Q6tx4rOToXw+BxqgTypY1j662lcyXA1aaMEZzCFYyjcejq+dvZZ/dC+Xs8zaf2tjKZ1zQUZEwnY12tCqXDZky+1bDIUhfSHXZkBzVocMUhZ5EMd/JjOYiHAWXyG4gnmJ2sS3p3YZOktqIfqViJqoFI1FVexDIK9iFTWz/5NqSiQgX14dSgYpx6gx3n80LhgU3bAtRp/knWj37aC5zfsgS9HMiWlcJCqksOqjWPkFMbTxwrDtMuWwBVCtyMImRoBBVb1+ZW0ffKLUjqG8pngoRF9W0+3PZDM8oGFeI/NM3KXwu10U0iYMUTwwiVeYPc7CndnWV0wmOz7Z1H5VfSjyEmFbZkOjauSsh919uK3aLx3FB/t+OuC990g/Rta00CdEPKHRmRjussuSDZaWc6vle9kTdia+8gmP9fXiV1j9xJGZIoTzqSEOk4NmWjyEWxKmW/HrI6Lq2zNWWSIhKyW+eOjb9y6hCbK16a2DbUB6WBTfTwGNU3wJr6/PssvRIILlXswS8v2Jxvf7a4E9nUb9VCZQzEFAejaJ6Sn1yrw9u8nQ3V6iwTnuYd4qWg4cF/fa9Ku2hjKIkVI2CUTMKEqWLuAqVb3LdpJQx04uAxXF7s3yrl74NEzgM2eqRDfLW7QrG/2ZnMFlT25lAoQoxlVkVX4ew7kaPnd+Hj0dtiaawBJNrSjSeorVipSidk9a6tY1y21CcQVRxpEotxjpXOOM8K/oIbxMwdqaxS0+ql52",
        t: "MTYxMDU0MDIxOC41NjAwMDA=",
        m: "LKht6wCXzD5WeJ20z/EhwraUI/FtR+CBD9w4/HvSpGA=",
        i1: "sPjDfn9Es0Kv38D7OmEpfA==",
        i2: "9NvTkE7Rdui6Uq1XUTC49w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "1gWh09U9tSjsfogANU/2rPYo3uPVC0lC8rRVh9TaY7w=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.atcoworld.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<a href="https://bt50.org/nonalignedfrequent.php?year=40"><!-- table --></a>
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=0187ac2cf681ccbd8a48be194796bbc488f20713-1610540218-0-AS02cWsNCbNcT6XpQGlRBxfLsb4lPeFHR_YcJfFlmS0er8AH-vxIIFMqXz9Z06yWzVuQSqSDddAVFQ0RswGXuwrcJwqoxc6AzRi-MOy5xHDQRcb0r8TOSERnuGg46iG-a8SCaFbwT56aoehnKpy388RcUZT_ZUFKIigJFSomL41FdEx94nuUZNVFs16z4swy8Ij1vtwJPBrDYLM4cNVUulSFAKlnpbE4PNgYhQCoWiCtyKNWE3LQ2gZffbopHMNkCRiWvA0f6sdOYO7ekJY8xYHRpe-LOZ5UoFSIrRLrXA0h55Rqn617d2rNec5n2oM9NOK8v7mWUqbJU6_3WWwroheLSNgcoGUJ24L5NYS8YAoIuQw_pwCzO9TbiJ-8Orf16WpZVtl84WaWfY867S-P8ekrooPEweiyUe8HxsFPZCITqqG19AVaACtZSnRoBe6DugUBU6RubbpiDxES402a9ZVsLJQ7EOg-71kQiI61I46a0vvWIpW9K5g-NUXCmDk8ApaSyFvCrYhOEDlnPdiiSPo" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="1318365d70793a4b1bebacb47e89482b8fb5573d-1610540218-0-AWnxGpC9pfHrhJEobUbcDe7rFWEDQeQf/MnAgwMcXuJwu/uAiT2tqz7SkyUGt+KEvFJChsgXmbz5Isys0y2hfBXm+bHQT/ii4NO5+jhkQE585uzTpMzBCczmzZO5DaUX+rBxDnjPTvuVfH8CCWaovtqDmMFTvot/KHB39PZzBAMNDXI3unKirULOe6WKAr0Qu6cNy0ERmXAG/4H3w3HWnbx832kPHkJLsvS7uuU8kPgBRyUj2q7REcB4sBFczFPUw9NRBVl1TKLo0DD4EG1/bgUzB/+E892J0iIbCM249f8m34KwyHrpmwzUYQBFx/2hsA+O7MVwUCs2sp2nIlJ/dSYheF3lXuSBXhFG3d6oJzNZ37Wa3s88UaXZuu20X8ZeqQiWRWFenLJRJHkapeKFLvn38aWST3D7B0eESoMmbvQNFDgoWBptSGwNTizMdemlvK3weUABMmzld+H9eWy8NouwTrE4bXQbKiUUNWgRTl00t5Qh16gSwXuF7ijmSdup2eEsLRly1jeUQkWACyZRcbtlDVbUDsFJpW+Rx5tbfOMAGDsD8dELsMO7Uq+do5qEsGeA4G0mZl+JC7ONsvYFnQ3WJ+5vNtIG5C4rFC1XxvBBjNs9NTwe5Q6YVs1tmBbI7cjNy+MS9JSGLHCT4XayVSd3asNHp8MVpV8Q3A6LyBHNQHXhoLPudZ7KetEH3Gn1LV9bKSssmd7VPIoaA2U2e393w06LBDcDEErc3o2Q0nbneZrkHk1fm/C4PxZ3KaGXxpIaYN49qCUzr1nRYCY7pxW0EN3wUfABwfvBWqaAnDGqRVs8Ti2T9OVHt8Jh0+v5cfag3l73R2bURWa0TrlqGR+Oio0bbj6fiGHhdDx9wyZdoZ/M7t5ib8Yr5H+fWTDHUTtqCH/Yq1XUJ9v5bPmt70wK2EOFTTZHA8FwGwuqUL+jLWFIxw2cCXHRC1f4UKPDTnJG/TFKmWwdd+zauoOCKdx0s3dVlgMj4jfCNS3h+sfoBSKRG3DWinAr4hnhFffzuO+7G+TRwxktCq0zqtsPAg89mC3DP947Q6xrnBnsGvFXSKHZcON+DwrBKQxHEtbF+hx2hlJiZl3J6AR/YVeW9Fd+dqfF9RtwDYMvY+cF4l56/FzMlqFdA6GDZrhsRuD3YYLiKj9+cuIXlY4jD3Zp64KndLV78Z6f7kgr2CkYjOe6X4Fcrjo8NQrh+ONPuGxntHHehnoNQqk2wJ9XCs2xuYEouX/MylFbbxzYuEuM13YEr3kEJDI8HEIxpg6yZ/ORTS6HQDy+kR+ZTkUpxxxvVwzGIwn7k3LPu4px8sBUNBip+BBz6qihidvun31pbzVLZA=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="845981657058b4819320f2f670c1f457"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610f0d2dfa3d17bf')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610f0d2dfa3d17bf</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
