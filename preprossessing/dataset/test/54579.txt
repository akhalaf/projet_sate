<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<!--[if IE 8 ]><html lang="ru" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#" class="no-js ie8"><![endif]--><!--[if IE 9 ]><html lang="ru" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#" class="no-js ie9"><![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" dir="ltr" lang="ru" version="HTML+RDFa 1.1" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->
<head profile="http://www.w3.org/1999/xhtml/vocab">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-5735335180209531",
enable_page_level_ads: true
});
</script>
<meta content="cca9558513b3282b" name="yandex-verification"/>
<meta charset="utf-8"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<link href="/content/dobro-pozhalovat" rel="canonical"/>
<link href="/node/6" rel="shortlink"/>
<meta content="AaBaby - бережное развитие. Раскраски, развивающие игры для детей, каталог детских книг, аудиокниги, сказки, песни, музыка для сна, басни, загадки с ответами." name="description"/>
<meta content="aababy, дети онлайн, аудиосказки слушать, стихи, сказки, басни, песни для детей, загадки с ответами, раннее развитие, раскраски, уроки рисования, детский сайт" name="keywords"/>
<meta content="Дети онлайн. AaBaby - Чем занять ребенка" name="title"/>
<link href="https://aababy.ru/sites/all/themes/aababy/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<!--[if lte IE 7]> <div style=' text-align:center; clear: both; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
<title>Детский сайт | aaBaby - Чем занять ребенка</title>
<!--[if LT IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<!--[if lte IE 8]>
	<style type="text/css">
    	.poll .bar, .poll .bar .foreground { behavior:url(/sites/all/themes/aababy/js/PIE.php); zoom:1}

      body {min-width: 1152px !important;}
	</style>
<![endif]-->
<style media="all" type="text/css">
@import url("https://aababy.ru/modules/system/system.base.css?qmojoc");
@import url("https://aababy.ru/modules/system/system.menus.css?qmojoc");
@import url("https://aababy.ru/modules/system/system.messages.css?qmojoc");
@import url("https://aababy.ru/modules/system/system.theme.css?qmojoc");
</style>
<style media="all" type="text/css">
@import url("https://aababy.ru/modules/book/book.css?qmojoc");
@import url("https://aababy.ru/modules/comment/comment.css?qmojoc");
@import url("https://aababy.ru/modules/field/theme/field.css?qmojoc");
@import url("https://aababy.ru/modules/node/node.css?qmojoc");
@import url("https://aababy.ru/modules/search/search.css?qmojoc");
@import url("https://aababy.ru/modules/user/user.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/views/css/views.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/back_to_top/css/back_to_top.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/ckeditor/css/ckeditor.css?qmojoc");
</style>
<style media="all" type="text/css">
@import url("https://aababy.ru/sites/all/modules/colorbox/styles/default/colorbox_style.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/ctools/css/ctools.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/flexslider/assets/css/flexslider_img.css?qmojoc");
@import url("https://aababy.ru/sites/all/libraries/flexslider/flexslider.css?qmojoc");
@import url("https://aababy.ru/sites/all/modules/yandex_metrics/css/yandex_metrics.css?qmojoc");
</style>
<style media="screen" type="text/css">
@import url("https://aababy.ru/sites/all/themes/aababy/css/boilerplate.css?qmojoc");
@import url("https://aababy.ru/sites/all/themes/aababy/css/style.css?qmojoc");
@import url("https://aababy.ru/sites/all/themes/aababy/css/maintenance-page.css?qmojoc");
@import url("https://aababy.ru/sites/all/themes/aababy/css/skeleton.css?qmojoc");
</style>
<script src="https://aababy.ru/sites/all/modules/jquery_update/replace/jquery/1.10/jquery.min.js?v=1.10.2" type="text/javascript"></script>
<script src="https://aababy.ru/misc/jquery-extend-3.4.0.js?v=1.10.2" type="text/javascript"></script>
<script src="https://aababy.ru/misc/jquery-html-prefilter-3.5.0-backport.js?v=1.10.2" type="text/javascript"></script>
<script src="https://aababy.ru/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="https://aababy.ru/misc/drupal.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/modules/jquery_update/replace/ui/ui/minified/jquery.ui.effect.min.js?v=1.10.2" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/modules/admin_menu/admin_devel/admin_devel.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/modules/back_to_top/js/back_to_top.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/default/files/languages/ru_bdxn-tDSu6r32YE5oHFzwG9ALCVUKIoYhzFBDYJRtYk.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/libraries/colorbox/jquery.colorbox-min.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/modules/colorbox/js/colorbox.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/modules/colorbox/styles/default/colorbox_style.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/libraries/flexslider/jquery.flexslider-min.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/themes/aababy/js/theme838.core.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/themes/aababy/js/jquery.loader.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/themes/aababy/js/jquery.debouncedresize.js?qmojoc" type="text/javascript"></script>
<script src="https://aababy.ru/sites/all/themes/aababy/js/jquery.mobilemenu.js?qmojoc" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"aababy","theme_token":"BfK8KasVEzA7UWQ5PUoELaHQtaSEMAc_AQDBE3vd4Dk","js":{"sites\/all\/modules\/flexslider\/assets\/js\/flexslider.load.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.effect.min.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"sites\/all\/modules\/back_to_top\/js\/back_to_top.js":1,"public:\/\/languages\/ru_bdxn-tDSu6r32YE5oHFzwG9ALCVUKIoYhzFBDYJRtYk.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/libraries\/flexslider\/jquery.flexslider-min.js":1,"sites\/all\/themes\/aababy\/js\/theme838.core.js":1,"sites\/all\/themes\/aababy\/js\/jquery.loader.js":1,"sites\/all\/themes\/aababy\/js\/jquery.debouncedresize.js":1,"sites\/all\/themes\/aababy\/js\/jquery.mobilemenu.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/back_to_top\/css\/back_to_top.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/flexslider\/assets\/css\/flexslider_img.css":1,"sites\/all\/libraries\/flexslider\/flexslider.css":1,"sites\/all\/modules\/yandex_metrics\/css\/yandex_metrics.css":1,"sites\/all\/themes\/aababy\/css\/boilerplate.css":1,"sites\/all\/themes\/aababy\/css\/style.css":1,"sites\/all\/themes\/aababy\/css\/maintenance-page.css":1,"sites\/all\/themes\/aababy\/css\/skeleton.css":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"\u0417\u0430\u043a\u0440\u044b\u0442\u044c","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"flexslider":{"optionsets":{"default":{"namespace":"flex-","selector":".slides \u003E li","easing":"swing","direction":"horizontal","reverse":false,"smoothHeight":false,"startAt":0,"animationSpeed":600,"initDelay":0,"useCSS":true,"touch":true,"video":false,"keyboard":true,"multipleKeyboard":false,"mousewheel":0,"controlsContainer":".flex-control-nav-container","sync":"","asNavFor":"","itemWidth":0,"itemMargin":0,"minItems":0,"maxItems":0,"move":0,"animation":"fade","slideshow":true,"slideshowSpeed":"7000","directionNav":true,"controlNav":true,"prevText":"\u003C","nextText":"\u003E","pausePlay":false,"pauseText":"Pause","playText":"Play","randomize":false,"thumbCaptions":false,"thumbCaptionsBoth":false,"animationLoop":true,"pauseOnAction":true,"pauseOnHover":false,"manualControls":""}},"instances":{"flexslider-1":"default"}},"back_to_top":{"back_to_top_button_trigger":"100","back_to_top_button_text":"\u0412\u0432\u0435\u0440\u0445","#attached":{"library":[["system","ui"]]}},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>
<meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<script>
        (function()
        {
            var abg = document.createElement('script');
            abg.async = true;
            abg.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            abg.src = (useSSL ? 'https:' : 'http:') +
                '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(abg, node);
        })();

    </script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-page with-navigation with-subnav" id="body">
<div id="page-wrapper">
<div id="page">
<header class="clearfix" id="header" role="banner">
<div class="section-1 clearfix headerr">
<div class="container-12">
<div class="grid-12">
<div class="t-line"><div class="t-line1"><div class="t-line2"></div></div></div>
<div class="clearfix">
<div class="col1">
<a href="/" id="logo" rel="home" title="Главная">
<img alt="Главная" src="https://aababy.ru/sites/all/themes/aababy/logo.png"/>
</a>
</div>
<div class="col2">
<div class="section-2 clearfix">
<div class="region region-header">
<div class="block block-search block-odd" id="block-search-form">
<div class="content">
<form accept-charset="UTF-8" action="/" id="search-block-form" method="post"><div><div class="container-inline">
<h2 class="element-invisible">Форма поиска</h2>
<div class="form-item form-type-textfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Найти </label>
<input class="form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" size="15" title="Введите ключевые слова для поиска." type="search" value=""/>
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input class="form-submit" id="edit-submit" name="op" type="submit" value="Найти"/></div><input name="form_build_id" type="hidden" value="form-qFvK7msov8Y2uizVk1yNBESKPoICOwJqbfkuxug7alE"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form> </div><!-- /.content -->
</div><!-- /.block --><div class="block block-block block-even" id="block-block-1">
<div class="content">
<div class="soc">
<div class="m-soc">Мы в социальных сетях:</div>
<a class="soc1" href="https://www.instagram.com/"><img src="/sites/all/themes/aababy/images/instogramm.png"/></a>
<a class="soc1" href="https://twitter.com/"><img src="/sites/all/themes/aababy/images/tvit.png"/></a>
<a class="soc1" href="http://vk.com/"><img src="/sites/all/themes/aababy/images/vk_.png"/></a>
<a class="soc1" href="https://www.facebook.com"><img src="/sites/all/themes/aababy/images/facebock.png"/></a>
<a class="soc1" href="https://www.ok.ru/"><img src="/sites/all/themes/aababy/images/odnoklassniki.png"/></a>
</div> </div><!-- /.content -->
</div><!-- /.block --> </div>
</div>
</div>
<div class="col3">
<div class="user-m">
<a class="vhov" href="/user">Вход</a>
<a class="regist" href="/user/register">Регистрация</a>
</div>
<div class="section-4 clearfix">
<div class="region region-header-bottom">
<div class="block block-block block-odd" id="block-block-2">
<div class="content">
<p><a href="/"><img src="/sites/all/themes/aababy/images/ban-top-right.jpg"/></a></p>
</div><!-- /.content -->
</div><!-- /.block --> </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="section-2-wrap">
<div class="container-12">
<div class="grid-12">
<div class="section-3 clearfix">
<div class="region region-menu">
<div class="block block-menu-block block-even" id="block-menu-block-1">
<div class="content">
<div class="menu-block-wrapper menu-block-1 menu-name-main-menu parent-mlid-0 menu-level-1">
<ul class="menu clearfix"><li class="first leaf has-children menu-mlid-395 id- mid-395"><a href="/stati" title="Статьи">Статьи</a></li>
<li class="leaf has-children menu-mlid-396 id- mid-396"><a href="/raskraski" title="Раскраски">Раскраски</a></li>
<li class="leaf has-children menu-mlid-568 id- mid-568"><a href="/podelki" title="Поделки">Поделки</a></li>
<li class="leaf has-children menu-mlid-587 id- mid-587"><a href="/audioskazki" title="Аудиосказки">Аудиосказки</a></li>
<li class="leaf has-children menu-mlid-402 id- mid-402"><a href="/muzyka-i-pesni" title="Музыка и песни">Музыка и песни</a></li>
<li class="leaf has-children menu-mlid-403 id- mid-403"><a href="/rannee-razvitie" title="Раннее развитие">Раннее развитие</a></li>
<li class="leaf has-children menu-mlid-404 id- mid-404"><a href="/skazki" title="Сказки">Сказки</a></li>
<li class="leaf has-children menu-mlid-417 id- mid-417"><a href="/stihi" title="Стихи">Стихи</a></li>
<li class="leaf has-children menu-mlid-418 id- mid-418"><a href="/basni" title="Басни">Басни</a></li>
<li class="last leaf has-children menu-mlid-419 id- mid-419"><a href="/zagadki" title="Загадки">Загадки</a></li>
</ul></div>
</div><!-- /.content -->
</div><!-- /.block --> </div>
</div>
</div>
</div>
</div>
</header><!-- /#header -->
<div id="main-wrapper">
<div class="container-12">
<div class="grid-12">
<div class="clearfix" id="main">
<aside class="column column_right grid-4 omega" id="sidebar-second-wide" role="complementary">
<div class="section">
<div class="region region-sidebar-second-wide">
<section class="block block-views block-odd" id="block-views-stat-block">
<h2>Последние статьи</h2>
<div class="content">
<div class="view view-stat view-id-stat view-display-id-block view-dom-id-833637a86c34f870e80195117010db44">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-created"> <span class="field-content">13.11.2019</span> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="https://aababy.ru/zagadki/populyarnye/zagadki-dlya-detey-12-let">Загадки для детей 12 лет</a></span> </div>
<div class="views-field views-field-body"> <div class="field-content">Интересные загдки для зарядки ума
</div> </div> </div>
</div>
</div> </div><!-- /.content -->
</section><!-- /.block --><div class="block block-block block-even" id="block-block-5">
<div class="content">
<ins class="adsbygoogle" data-ad-client="ca-pub-5735335180209531" data-ad-slot="1662642800" style="display:inline-block;width:300px;height:250px"></ins>
<script>
if (document.body.clientWidth >767){
(adsbygoogle = window.adsbygoogle || []).push({});
}
</script> </div><!-- /.content -->
</div><!-- /.block --><div class="block block-views block-odd" id="block-views-stat-block-1">
<div class="content">
<div class="view view-stat view-id-stat view-display-id-block_1 view-dom-id-0fc60097cd86b41db6afb9b41cc0b820">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-created"> <span class="field-content">01.11.2019</span> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="https://aababy.ru/zagadki/populyarnye/zagadki-dlya-detey-11-let">Загадки для детей 11 лет</a></span> </div>
<div class="views-field views-field-body"> <div class="field-content">Интересные загадки
</div> </div> </div>
</div>
</div> </div><!-- /.content -->
</div><!-- /.block --><section class="block block-block block-even" id="block-block-6">
<h2>Последние добавленные материалы</h2>
<div class="content">
<!--<a href="/"><img src="/sites/all/themes/aababy/images/raskraski.jpg" /></a>
<a href="/"><img src="/sites/all/themes/aababy/images/fei.jpg" /></a>
<a href="/"><img src="/sites/all/themes/aababy/images/audio.jpg" /></a>
--> </div><!-- /.content -->
</section><!-- /.block --><div class="block block-views block-odd" id="block-views-fraskraski-block">
<div class="content">
<div class="view view-fraskraski view-id-fraskraski view-display-id-block view-dom-id-1226f8d23c4aa47715b63f32f7c9a767">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-field-scat"> <div class="field-content">
<div class="ct2 ct">Раскраски</div></div> </div>
<div class="views-field views-field-field-images"> <div class="field-content"><a href="/raskraski/raskraski-dlya-malyshey-2-4-goda/yagody-i-frukty/veselye-frukty-i-yagody-abrikosy"><img alt="" height="232" src="https://aababy.ru/sites/default/files/styles/content_263/public/main-images/veselye_frukty_i_yagody_-_abrikosy.png?itok=u6Z2SpoL" typeof="foaf:Image" width="300"/></a></div> </div> </div>
</div>
</div> </div><!-- /.content -->
</div><!-- /.block --><div class="block block-block block-even" id="block-block-15">
<div class="content">
<ins class="adsbygoogle" data-ad-client="ca-pub-5735335180209531" data-ad-slot="3139376009" style="display:inline-block;width:300px;height:250px"></ins>
<script>
if (document.body.clientWidth >767){
(adsbygoogle = window.adsbygoogle || []).push({});
}
</script> </div><!-- /.content -->
</div><!-- /.block --><div class="block block-views block-odd" id="block-views-fraskraski-block-1">
<div class="content">
<div class="view view-fraskraski view-id-fraskraski view-display-id-block_1 view-dom-id-24b96156b1c1a636c8e6c7dfdf3906c6">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div class="views-field views-field-field-scat"> <div class="field-content">
<div class="ct91 ct">Аудиосказки</div></div> </div>
<div class="views-field views-field-field-images"> <div class="field-content"><a href="/audioskazki/slushat-stihi-onlayn/agniya-barto/audiostihi-agnii-barto-chast-3"><img alt="" height="259" src="https://aababy.ru/sites/default/files/styles/content_263/public/audio_barto_0.png?itok=OvjjPAQs" typeof="foaf:Image" width="300"/></a></div> </div> </div>
</div>
</div> </div><!-- /.content -->
</div><!-- /.block --> </div>
</div>
</aside>
<div class="column" id="content" role="main">
<div class="section">
<div class="region region-content">
<div class="block block-block block-even" id="block-block-3">
<div class="content">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Адаптивный горизонтальный блок под H1 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5735335180209531" data-ad-format="auto" data-ad-slot="4894774402" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div><!-- /.content -->
</div><!-- /.block -->
<div id="block-views-slider-block">
<div class="view view-slider view-id-slider view-display-id-block view-dom-id-43e22184ac0f8ee2ce0d39ccbe7be3b4">
<div class="view-content">
<div class="flexslider" id="flexslider-1">
<ul class="slides"><li>
<div class="views-field views-field-field-photo"> <div class="field-content"><a href="http://aababy.ru/stihi"><img alt="" height="303" src="https://aababy.ru/sites/default/files/sl3_0.jpg" typeof="foaf:Image" width="688"/></a></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><div class="sl-title"><a href="http://aababy.ru/stihi">Детские стихи</a></div>
<div class="sl-text"><a href="http://aababy.ru/stihi"></a><a href="http://aababy.ru/stihi"><p>Стихи к праздникам<br/>
	Обучающие и развивающие стишки<br/>
	Авторские и тематические стихотворения для детей</p>
</a></div> </span> </div>
<div class="views-field views-field-counter"> <span class="field-content">1</span> </div></li>
<li>
<div class="views-field views-field-field-photo"> <div class="field-content"><a href="http://aababy.ru/audioskazki"><img alt="" height="303" src="https://aababy.ru/sites/default/files/sl2.jpg" typeof="foaf:Image" width="688"/></a></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><div class="sl-title"><a href="http://aababy.ru/audioskazki">Аудиосказки для детей</a></div>
<div class="sl-text"><a href="http://aababy.ru/audioskazki"></a><a href="http://aababy.ru/audioskazki">Лучшие сказки в формате мp3</a></div> </span> </div>
<div class="views-field views-field-counter"> <span class="field-content">2</span> </div></li>
<li>
<div class="views-field views-field-field-photo"> <div class="field-content"><a href="http://aababy.ru/skazki"><img alt="" height="303" src="https://aababy.ru/sites/default/files/sl1.jpg" typeof="foaf:Image" width="688"/></a></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><div class="sl-title"><a href="http://aababy.ru/skazki">Сказки для детей</a></div>
<div class="sl-text"><a href="http://aababy.ru/skazki"></a><a href="http://aababy.ru/skazki">Лучшие произведения для детей от зарубежных и отечественных авторов</a></div> </span> </div>
<div class="views-field views-field-counter"> <span class="field-content">3</span> </div></li>
</ul></div>
</div>
<div class="view-footer">
      / 3     </div>
</div></div><!-- /.block --><div class="block block-views block-even" id="block-views-frontcat-block">
<div class="content">
<div class="view view-frontcat view-id-frontcat view-display-id-block view-dom-id-45d743ef028c9d79a63a8e2dc81e942c">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/stati">Статьи</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p class="rtejustify">Как развивается Ваш ребенок, чем его лучше кормить, как играть. О многих полезных вещах как научить ребенка говорить, читать, писать Вы сможете узнать в разделе Статьи. Развивайтесь вместе с малышом.</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/stati">Подробнее</a></span> </div> </div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/raskraski">Раскраски</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p class="rtejustify">Большая коллекция раскрасок для мальчиков и для девочек, с героями из мультфильмов, развивающие картинки и многое другое. Изучайте и наслаждайтесь.</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/raskraski">Подробнее</a></span> </div> </div>
<div class="views-row views-row-3 views-row-odd">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/podelki">Поделки</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p>В данном разделе Вы найдете учебные материалы как рисовать, делать поделки из пластилина, складывать оригами и создавать аппликации</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/podelki">Подробнее</a></span> </div> </div>
<div class="views-row views-row-4 views-row-even">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/audioskazki">Аудиосказки</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p class="rtejustify">Лучшие сказки в аудио озвучке от профессиональных ведущих. Слушайте онлайн и качайте бесплатно в mp3. Идеальный вариант чтения сказки на ночь</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/audioskazki">Подробнее</a></span> </div> </div>
<div class="views-row views-row-5 views-row-odd">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/muzyka-i-pesni">Музыка и песни</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p class="rtejustify">Слушать онлайн детские песни или скачать бесплатно. Выучить слова детских песен и петь под минусовки с AaBaby</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/muzyka-i-pesni">Подробнее</a></span> </div> </div>
<div class="views-row views-row-6 views-row-even">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/rannee-razvitie">Раннее развитие</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content">Полезный раздел для проведения развивающих занятий с детьми с самого рождения. Ваш малыш познает мир в игровой форме, что облегчает обучение.</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/rannee-razvitie">Подробнее</a></span> </div> </div>
<div class="views-row views-row-7 views-row-odd">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/skazki">Сказки</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content">Повести, рассказы, сказки общепризнанных авторов. Такие произведения обязательны к прочтению ребенку.</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/skazki">Подробнее</a></span> </div> </div>
<div class="views-row views-row-8 views-row-even">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/stihi">Стихи</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content">Собрание стихотворений от известных детских поэтов. Стихи поделены по тематикам, что удобно для проведения занятий, утренников, праздников в садике и школе.</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/stihi">Подробнее</a></span> </div> </div>
<div class="views-row views-row-9 views-row-odd">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/basni">Басни</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content"><p class="rtejustify">Представлен огромный сборник басен. Произведения Крылова и Михалкова содержат мораль и анализ, Вашему школьнику будет полезно</p>
</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/basni">Подробнее</a></span> </div> </div>
<div class="views-row views-row-10 views-row-even views-row-last">
<div class="views-field views-field-name"> <span class="field-content"><a href="https://aababy.ru/zagadki">Загадки</a></span> </div>
<div class="views-field views-field-description"> <div class="field-content">Мы собрали загадки разных тематик. Для удобства разделили весь материал на категории. Ваши детки оценят их, а Вы весело проведете время</div> </div>
<div class="views-field views-field-name-1"> <span class="field-content"><a href="https://aababy.ru/zagadki">Подробнее</a></span> </div> </div>
</div>
</div> </div><!-- /.content -->
</div><!-- /.block --><article about="/content/dobro-pozhalovat" class="node node-page node-odd published with-comments node-full clearfix" id="node-6" typeof="foaf:Document">
<div class="content">
</div><!-- /.content -->
<footer>
</footer>
</article><!-- /.node -->
</div>
<div class="region region-content-bottom">
<div class="block block-block block-odd" id="block-block-4">
<div class="content">
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-image="http://aababy.ru/sites/default/files/aababy_promo.png" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,pinterest,twitter,tumblr,viber,whatsapp,skype"></div>
<!--<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
<div class="ya-share2" data-services="facebook,vkontakte,tumblr" data-size='m'></div>-->
</div><!-- /.content -->
</div><!-- /.block --> </div>
</div>
</div>
</div><!-- /#main -->
</div>
</div>
</div>
<footer id="footer" role="contentinfo">
<div class="footer-wrapper">
<div class="container-12">
<div class="grid-12">
<div class="clearfix">
<div class="region region-footer">
<div class="block block-block block-even" id="block-block-7">
<div class="content">
    © 2015-2020  Копирование материалов запрещено   </div><!-- /.content -->
</div><!-- /.block --><div class="block block-block block-odd" id="block-block-8">
<div class="content">
<a href="/content/pravoobladatelyam">Правообладателям</a>
<a href="/contact">Обратная форма</a> </div><!-- /.content -->
</div><!-- /.block --> </div>
<!--{%FOOTER_LINK} --> </div>
</div>
</div>
</div>
</footer>
</div><!-- /#page -->
</div><!-- /#page-wrapper --> <div class="region region-page-bottom">
<div class="ym-counter"><!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41264629 = new Ya.Metrika2({
                    id:41264629,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/41264629" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter --></div> </div>
<script src="https://aababy.ru/sites/all/modules/flexslider/assets/js/flexslider.load.js?qmojoc" type="text/javascript"></script>
<script>top.hpmd=top.hpmd||[];(function(l,a,d,b){if(d.querySelector('script[src=\''+l+'\']'))return;b=d.createElement(a);b.async=1;b.src=l;a=d.querySelector(a);a.parentNode.insertBefore(b,a)})('//banner.hpmdnetwork.ru/client/410.min.js','script',top.document);

	top.hpmd.push(function () {
		top.hpmd.events.add('view', function (event) {
			if (!event.hasBanner) {
				// code
			};
		});
	});
</script>
</body>
</html>