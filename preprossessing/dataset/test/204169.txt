<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Beatpad</title>
<base href="/"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Beatpad: Free beatpad. Play with your keyboard your favorite beats." name="description"/>
<meta content="Beatpad, free, app, keyboard, beats, beatmaker, launchpad" name="keywords"/>
<meta content="_Iw3coUVj9E_zC-t01kJgzjZFZPzpq3StB8oB01AX9Q" name="google-site-verification"/>
<meta content="5875BCA090B5745B4A4A894222F90EB4" name="msvalidate.01"/>
<meta content="63c040cabfdfe856" name="yandex-verification"/>
<meta content="en_US" property="og:locale"/>
<meta content="Beatpad" property="og:title"/>
<meta content="Beatpad" property="og:site_name"/>
<meta content="Beatpad: Free beatpad. Play with your keyboard your favorite beats." property="og:description"/>
<meta content="https://www.beatpad.net" property="og:url"/>
<meta content="433043793495067" property="fb:app_id"/>
<link href="favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600&amp;display=swap" rel="stylesheet"/>
<link href="styles.e1f18824509257f8c164.css" rel="stylesheet"/></head>
<body>
<app-root></app-root>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-49534406-1', 'auto');
    ga('send', 'pageview');
  </script>
<script data-cfasync="false" type="text/javascript">eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('!1(){2 e=d;2 t,o=["//g.s.j/3.5","//l.h/3.5",""],n=0,r=1(){f(""!=o[n]){t=e.4.c("6"),t.b="7/8",t.9=!0;2 a=e.4.q("6")[0];t.i=o[n],t.k=1(){n++,r()},a.m.p(t,a)}};r()}();',30,30,'|function|var|12634|document|js|script|text|javascript|async||type|createElement|window||if|code|gq|src|com|onerror|cfasync|parentNode|||insertBefore|getElementsByTagName||poptm|'.split('|'),0,{}))</script>
<script src="runtime-es2015.0811dcefd377500b5b1a.js" type="module"></script><script defer="" nomodule="" src="runtime-es5.0811dcefd377500b5b1a.js"></script><script defer="" nomodule="" src="polyfills-es5.c219b7059a7feb126472.js"></script><script src="polyfills-es2015.3fd94ed6a324eee92aec.js" type="module"></script><script defer="" src="scripts.9d2f6bf5ef6cc9de3252.js"></script><script src="main-es2015.4a5407b3fec22cadc3d9.js" type="module"></script><script defer="" nomodule="" src="main-es5.4a5407b3fec22cadc3d9.js"></script></body>
</html>
