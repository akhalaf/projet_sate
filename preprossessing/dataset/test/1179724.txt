<!DOCTYPE html>
<html class="avada-html-layout-wide avada-html-header-position-top" lang="sr-RS" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Stranica nije pronađena - Profesional MD</title>
<!-- This site is optimized with the Yoast SEO plugin v13.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="sr_RS" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Stranica nije pronađena - Profesional MD" property="og:title"/>
<meta content="Profesional MD" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Stranica nije pronađena - Profesional MD" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.profesionalmd.com/#organization","name":"MD PROFESIONAL & CO","url":"https://www.profesionalmd.com/","sameAs":["https://www.facebook.com/Md-Profesional-Co-Doo-865881893587554/"],"logo":{"@type":"ImageObject","@id":"https://www.profesionalmd.com/#logo","url":"https://www.profesionalmd.com/media/MD-profesional-logo.png","width":470,"height":280,"caption":"MD PROFESIONAL & CO"},"image":{"@id":"https://www.profesionalmd.com/#logo"}},{"@type":"WebSite","@id":"https://www.profesionalmd.com/#website","url":"https://www.profesionalmd.com/","name":"Profesional MD","description":"Profesionalna usluga tehni\u010dkog odr\u017eavanja, odr\u017eavanja higijene i video nadzora u va\u0161im objektima u Novom Sadu","publisher":{"@id":"https://www.profesionalmd.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.profesionalmd.com/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.profesionalmd.com/feed/" rel="alternate" title="Profesional MD » dovod" type="application/rss+xml"/>
<link href="https://www.profesionalmd.com/comments/feed/" rel="alternate" title="Profesional MD » dovod komentara" type="application/rss+xml"/>
<link href="https://www.profesionalmd.com/media/Profesional-MD-logo.png" rel="shortcut icon" type="image/x-icon"/>
<!-- For iPhone -->
<link href="https://www.profesionalmd.com/media/Profesional-MD-logo.png" rel="apple-touch-icon"/>
<!-- For iPhone Retina display -->
<link href="https://www.profesionalmd.com/media/Profesional-MD-logo.png" rel="apple-touch-icon" sizes="114x114"/>
<!-- For iPad -->
<link href="https://www.profesionalmd.com/media/Profesional-MD-logo.png" rel="apple-touch-icon" sizes="72x72"/>
<!-- For iPad Retina display -->
<link href="https://www.profesionalmd.com/media/Profesional-MD-logo.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="https://www.profesionalmd.com/wp-content/plugins/cookie-notice/css/front.min.css?ver=5.3.6" id="cookie-notice-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.profesionalmd.com/wp-content/themes/Avada/assets/css/style.min.css?ver=6.1.2" id="avada-stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://www.profesionalmd.com/wp-content/themes/Avada/assets/css/ie.min.css?ver=6.1.2' type='text/css' media='all' />
<style id='avada-IE-inline-css' type='text/css'>
.avada-select-parent .select-arrow{background-color:#373d4d}
.select-arrow{background-color:#373d4d}
</style>
<![endif]-->
<link href="https://www.profesionalmd.com/wp-content/plugins/recent-posts-widget-with-thumbnails/public.css?ver=6.7.0" id="recent-posts-widget-with-thumbnails-public-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.profesionalmd.com/media/fusion-styles/9729b76e14a6c70d440e70ee0d31101d.min.css?ver=2.1.2" id="fusion-dynamic-css-css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
		var obHost ="https://www.profesionalmd.com/";
		</script><script src="https://www.profesionalmd.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.profesionalmd.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.profesionalmd.com/wp-content/plugins/wp-pipes/assets/js/call_pipe.js?ver=5.3.6" type="text/javascript"></script>
<link href="https://www.profesionalmd.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.profesionalmd.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.profesionalmd.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://www.profesionalmd.com/wp-json/wp/v2/web-app-manifest" rel="manifest"/>
<meta content="#fff" name="theme-color"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="yes" name="mobile-web-app-capable"/>
<meta content="YES" name="apple-touch-fullscreen"/>
<meta content="Profesional MD" name="apple-mobile-web-app-title"/>
<meta content="Profesional MD" name="application-name"/>
<style id="simple-css-output" type="text/css">/*Header */.header-color.social-header {background-color: #ffffff4d;}.alternate-color .muted, .alternate-color .hr-title, .alternate-color .breadcrumb, .alternate-color .breadcrumb .active, .alternate-color .panel-kleo .icon-closed, .alternate-color .panel-kleo .icon-opened, .alternate-color .pagination > li > a, .alternate-color .pagination > li > span, .alternate-color .post-meta, .alternate-color .post-meta a, .alternate-color .post-footer a, .alternate-color .dropdown-submenu > a:after, .alternate-color .pricing-table .list-group-item.unavailable, .single-attachment .alternate-color .link-list, .single-attachment .alternate-color .link-list a, .alternate-color .form-control, .alternate-color #kleo-ajax-search-loading, .alternate-color .kleo_ajax_entry .search_excerpt, .alternate-color .ajax_search_image, .alternate-color .news-focus .left-thumb-listing .post-date, .alternate-color .news-highlight .left-thumb-listing .post-date, .alternate-color .activity-timeline, .alternate-color #rtMedia-queue-list tr td:first-child:before, .alternate-color .sidebar .widget.buddypress div.item-meta, .alternate-color .sidebar .widget.buddypress div.item-content, .alternate-color table.notifications td.notify-actions, .alternate-color .read-notifications table.notifications tr td, .alternate-color .unread-notifications table.notifications tr td, .alternate-color .bbp-pagination-links a, .alternate-color .bbp-pagination-links span, .woocommerce .alternate-color ul.products li.product .price del, .woocommerce-page .alternate-color ul.products li.product .price del, .alternate-color .kleo_ajax_results h4, .alternate-color .kleo-toggle-menu .quick-view, .alternate-color .ajax_not_found, .alternate-color .article-content .author-options .edit:hover:before, .alternate-color .article-content .author-options .delete:hover:before { color: #272727; font-weight: 700;}/*Menu*/.kleo-main-header:not(.header-left):not(.header-centered) .navbar-collapse > ul > li > a {text-transform: uppercase;}/*Global*//*#main {height: 100% !important;}*//*#main-container > .row, .article-content {background-color: #ffffffbd;}*//*div.panel-body.toggle-content.post-content ul li {margin-bottom: 10px;}div.fusion-text ol li {margin-bottom: 10px;}.post-content h4 {font-size: 22px; line-height: inherit;} *//*Footer*/.fusion-footer-widget-area {color: #eaeaea;}.fb_iframe_widget {padding-top: 20px;}/*Contact Form 7*/input.wpcf7-form-control.wpcf7-submit {float: right;}/*Google Maps*/.fusion-layout-column.fusion-column-last {margin-bottom: 0 !important;} .google-maps { position: relative; padding-bottom: 300px; // This is the aspect ratio height: 0; overflow: hidden; } .google-maps iframe { position: absolute; top: 0; left: 0; width: 100% !important; height: 300px !important; }</style><style id="css-fb-visibility" type="text/css">@media screen and (max-width: 640px){body:not(.fusion-builder-ui-wireframe) .fusion-no-small-visibility{display:none !important;}}@media screen and (min-width: 641px) and (max-width: 1024px){body:not(.fusion-builder-ui-wireframe) .fusion-no-medium-visibility{display:none !important;}}@media screen and (min-width: 1025px){body:not(.fusion-builder-ui-wireframe) .fusion-no-large-visibility{display:none !important;}}</style>
<!-- BEGIN ExactMetrics v5.3.10 Universal Analytics - https://exactmetrics.com/ -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-157934170-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- END ExactMetrics Universal Analytics -->
<script type="text/javascript">
			var doc = document.documentElement;
			doc.setAttribute( 'data-useragent', navigator.userAgent );
		</script>
</head>
<body class="error404 cookies-not-set fusion-image-hovers fusion-pagination-sizing fusion-button_size-large fusion-button_type-flat fusion-button_span-no avada-image-rollover-circle-yes avada-image-rollover-yes avada-image-rollover-direction-bottom fusion-has-button-gradient fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-totop avada-has-rev-slider-styles fusion-disable-outline fusion-sub-menu-fade mobile-logo-pos-left layout-wide-mode avada-has-boxed-modal-shadow-none layout-scroll-offset-full avada-has-zero-margin-offset-top fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v2 avada-responsive avada-footer-fx-bg-parallax avada-menu-highlight-style-bar fusion-search-form-classic fusion-main-menu-search-dropdown fusion-avatar-square avada-dropdown-styles avada-blog-layout-large avada-blog-archive-layout-medium avada-header-shadow-no avada-menu-icon-position-left avada-has-megamenu-shadow avada-has-mainmenu-dropdown-divider avada-has-breadcrumb-mobile-hidden avada-has-titlebar-hide avada-has-footer-widget-bg-image avada-has-header-bg-image avada-header-bg-no-repeat avada-has-header-bg-parallax avada-has-pagination-padding avada-flyout-menu-direction-fade">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div id="boxed-wrapper">
<div class="fusion-sides-frame"></div>
<div class="fusion-wrapper" id="wrapper">
<div id="home" style="position:relative;top:-1px;"></div>
<header class="fusion-header-wrapper">
<div class="fusion-header-v2 fusion-logo-alignment fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern">
<div class="fusion-secondary-header">
<div class="fusion-row">
<div class="fusion-alignleft">
<div class="fusion-contact-info"><span class="fusion-contact-info-phone-number"><a href="tel:+38121529400">+381 21 529 400</a></span><span class="fusion-header-separator">|</span><span class="fusion-contact-info-email-address"><a href="mailto:profesional.md@gmail.com">profesional.md@gmail.com</a></span></div> </div>
<div class="fusion-alignright">
<nav aria-label="Secondary Menu" class="fusion-secondary-menu" role="navigation"><ul class="menu" id="menu-top-meni"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88 fusion-menu-item-button" data-item-id="88" id="menu-item-88"><a class="fusion-bar-highlight" href="https://pfs.rs/" rel="noopener noreferrer" target="_blank"><span class="menu-text fusion-button button-default button-medium">Profesionalni upravnik stambene zajednice</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147 fusion-menu-item-button" data-item-id="147" id="menu-item-147"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/zahtev-za-ponudu/"><span class="menu-text fusion-button button-default button-medium">Zahtev za ponudu</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87" data-item-id="87" id="menu-item-87"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/kontakt/"><span class="menu-text">Kontakt</span></a></li></ul></nav><nav aria-label="Secondary Mobile Menu" class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav> </div>
</div>
</div>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
<div class="fusion-row">
<div class="fusion-logo" data-margin-bottom="5px" data-margin-left="0px" data-margin-right="0px" data-margin-top="5px">
<a class="fusion-logo-link" href="https://www.profesionalmd.com/">
<!-- standard logo -->
<img alt="Profesional MD Logo" class="fusion-standard-logo" data-retina_logo_url="" height="78" src="https://www.profesionalmd.com/media/Profesional-MD-logo.png" srcset="https://www.profesionalmd.com/media/Profesional-MD-logo.png 1x" width="129"/>
<!-- mobile logo -->
<img alt="Profesional MD Logo" class="fusion-mobile-logo" data-retina_logo_url="" height="78" src="https://www.profesionalmd.com/media/Profesional-MD-logo.png" srcset="https://www.profesionalmd.com/media/Profesional-MD-logo.png 1x" width="129"/>
<!-- sticky header logo -->
<img alt="Profesional MD Logo" class="fusion-sticky-logo" data-retina_logo_url="" height="78" src="https://www.profesionalmd.com/media/Profesional-MD-logo.png" srcset="https://www.profesionalmd.com/media/Profesional-MD-logo.png 1x" width="129"/>
</a>
</div> <nav aria-label="Main Menu" class="fusion-main-menu"><ul class="fusion-menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-14 fusion-dropdown-menu" data-item-id="14" id="menu-item-14"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/tehnicko-odrzavanje/"><span class="menu-text">Tehničko održavanje</span> <span class="fusion-caret"><i class="fusion-dropdown-indicator"></i></span></a><ul class="sub-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-210 fusion-dropdown-submenu" id="menu-item-210"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/tehnicko-odrzavanje/#opsta-ponuda-i-program-tehnickog-odrzavanja"><span>Opšta ponuda i program tehničkog održavanja</span></a></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-208 fusion-dropdown-submenu" id="menu-item-208"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/tehnicko-odrzavanje/#tehnicko-odrzavanje-ponude"><span>Ponude</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-204 fusion-dropdown-submenu" id="menu-item-204"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/tehnicko-odrzavanje/reference/"><span>Reference</span></a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-73 fusion-dropdown-menu" data-item-id="73" id="menu-item-73"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/odrzavanje-higijene/"><span class="menu-text">Održavanje higijene</span> <span class="fusion-caret"><i class="fusion-dropdown-indicator"></i></span></a><ul class="sub-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-271 fusion-dropdown-submenu" id="menu-item-271"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/odrzavanje-higijene/#opsta-ponuda-i-program-odrzavanja-higijene"><span>Opšta ponuda održavanja higijene</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-232 fusion-dropdown-submenu" id="menu-item-232"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/odrzavanje-higijene/reference/"><span>Reference</span></a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-72 fusion-dropdown-menu" data-item-id="72" id="menu-item-72"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/video-nadzor/"><span class="menu-text">Video nadzor</span> <span class="fusion-caret"><i class="fusion-dropdown-indicator"></i></span></a><ul class="sub-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255 fusion-dropdown-submenu" id="menu-item-255"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/video-nadzor/reference/"><span>Reference</span></a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-71 fusion-dropdown-menu" data-item-id="71" id="menu-item-71"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/kontrola-pristupa/"><span class="menu-text">Kontrola pristupa</span> <span class="fusion-caret"><i class="fusion-dropdown-indicator"></i></span></a><ul class="sub-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-274 fusion-dropdown-submenu" id="menu-item-274"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/kontrola-pristupa/#opsta-ponuda-ugradnje-kontrole-pristupa"><span>Opšta ponuda ugradnje kontrole pristupa</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-275 fusion-dropdown-submenu" id="menu-item-275"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/kontrola-pristupa/reference/"><span>Reference</span></a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69" data-item-id="69" id="menu-item-69"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/interfoni/"><span class="menu-text">Interfoni</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-468" data-item-id="468" id="menu-item-468"><a class="fusion-bar-highlight" href="https://www.profesionalmd.com/galerija/"><span class="menu-text">Galerija</span></a></li></ul></nav> <div class="fusion-mobile-menu-icons">
<a aria-expanded="false" aria-label="Toggle mobile menu" class="fusion-icon fusion-icon-bars" href="#"></a>
</div>
<nav aria-label="Main Menu Mobile" class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
</div>
</div>
</div>
<div class="fusion-clearfix"></div>
</header>
<div id="sliders-container">
</div>
<div class="avada-page-titlebar-wrapper">
</div>
<main class="clearfix " id="main">
<div class="fusion-row" style="">
<section class="full-width" id="content">
<div id="post-404page">
<div class="post-content">
<div class="fusion-title fusion-title-size-two sep-none fusion-sep-none" style="margin-top:0px;margin-bottom:30px;">
<h2 class="title-heading-left" style="margin:0;">
					Ups, izgleda da ono što tražite ne postoji... :(				</h2>
</div>
<div class="fusion-clearfix"></div>
<div class="error-page">
<div class="fusion-columns fusion-columns-3">
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-404">
<div class="error-message">404</div>
</div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 useful-links fusion-error-page-useful-links">
<h3>Možda će Vas zanimati</h3>
<ul class="fusion-checklist fusion-404-checklist error-menu" id="fusion-checklist-1" style="font-size:14px;line-height:23.8px;"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-14"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/tehnicko-odrzavanje/">Tehničko održavanje</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-73"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/odrzavanje-higijene/">Održavanje higijene</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-72"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/video-nadzor/">Video nadzor</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-71"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/kontrola-pristupa/">Kontrola pristupa</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/interfoni/">Interfoni</a></div></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-468"><span class="icon-wrapper circle-yes" style="background-color:var(--checklist_circle_color);font-size:14px;height:23.8px;width:23.8px;margin-right:9.8px;"><i class="fusion-li-icon fa fa-angle-right" style="color:var(--checklist_icons_color);"></i></span><div class="fusion-li-item-content" style="margin-left:33.6px;"><a href="https://www.profesionalmd.com/galerija/">Galerija</a></div></li></ul> </div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4 fusion-error-page-search">
<h3>Pretražite sajt</h3>
<p>Ne uspevate da pronađete ono što vam je potrebno? Pokušajte sa pretragom:</p>
<div class="search-page-search-form">
<form action="https://www.profesionalmd.com/" class="searchform fusion-search-form fusion-live-search" method="get" role="search">
<div class="fusion-search-form-content">
<div class="fusion-search-field search-field">
<label><span class="screen-reader-text">Search for:</span>
<input aria-label="Pretraga..." aria-required="true" class="s" name="s" placeholder="Pretraga..." required="" type="search" value=""/>
</label>
</div>
<div class="fusion-search-button search-button">
<input class="fusion-search-submit searchsubmit" type="submit" value=""/>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--WPFC_FOOTER_START-->
</div> <!-- fusion-row -->
</main> <!-- #main -->
<div class="fusion-footer">
<footer class="fusion-footer-widget-area fusion-widget-area">
<div class="fusion-row">
<div class="fusion-columns fusion-columns-3 fusion-widget-area">
<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
<section class="fusion-footer-widget-column widget widget_text" id="text-4"> <div class="textwidget"><p>MD PROFESIONAL &amp; CO doo je kompanija koja svoje temelje gradi na poverenju i zadovoljstvu klijenata, osnovana 2009 . godine, sa jedinim ciljem, a to je <strong>kvalitetno izvršenje svih intrevencija i dogovora sa klijentima</strong>, sa pristupom dobrog domaćina kako bi klijentima olakšali rešavanje problema u svom domu ili poslovnom objektu.</p>
<p>Naša kompanija je opremljena i stručnim kadrom i alatima  koji su neophodni za funkcionisanje i liderstvo na teritiriji Vojvodine.</p>
</div>
<div style="clear:both;"></div></section> </div>
<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
<section class="fusion-footer-widget-column widget widget_easy_facebook_like_box" id="easy_facebook_like_box-2"><div class="widget-text easy-facebook-like-box_box"><div id="fb-root"></div>
<script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/sr_RS/sdk.js#xfbml=1&version=v2.10";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, "script", "facebook-jssdk"));</script><div class="fb-page" data-adapt-container-width="true" data-height="" data-hide-cover="false" data-hide-cta="true" data-href="https://www.facebook.com/Md-Profesional-Co-Doo-865881893587554/" data-show-facepile="true" data-small-header="false" data-tabs="" data-width=""><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div><div style="clear:both;"></div></section> </div>
<div class="fusion-column fusion-column-last col-lg-4 col-md-4 col-sm-4">
<section class="fusion-footer-widget-column widget widget_text" id="text-3"> <div class="textwidget"><p><strong>KONTAKT</strong></p>
<p>E-mail: <strong><a href="mailto:profesional.md@gmail.com">profesional.md@gmail.com</a></strong><br/>
Telefon:<strong><a href="tel:+38121529400">+381 21 529400</a><br/>
</strong>Telefon:<strong><a href="tel:+381216611255">+381 21 6611255</a><br/>
</strong>Telefon:<strong><a href="tel:+38162410809">+381 62 410809</a><br/>
</strong></p>
<p>Adresa:<br/>
<strong>MD PROFESIONAL &amp; CO<br/>
</strong><strong>Trg Slobode 3</strong><br/>
Apolo centar II sprat<br/>
<strong>21000 Novi Sad</strong><br/>
Vojvodina, Srbija</p>
</div>
<div style="clear:both;"></div></section> </div>
<div class="fusion-clearfix"></div>
</div> <!-- fusion-columns -->
</div> <!-- fusion-row -->
</footer> <!-- fusion-footer-widget-area -->
<footer class="fusion-footer-copyright-area" id="footer">
<div class="fusion-row">
<div class="fusion-copyright-content">
<div class="fusion-copyright-notice">
<div>
<span style="line-height:30px !important;">© Copyright 2006 - <script>document.write(new Date().getFullYear());</script>    |   Profesional MD   |   Sva prava zadržana   |   <a href="https://www.connections.rs/" target="_blank">Politika privatnosti</a>   |   Website by <a href="https://www.connections.rs/" target="_blank"> CONNECTIONS</a></span> </div>
</div>
</div> <!-- fusion-fusion-copyright-content -->
</div> <!-- fusion-row -->
</footer> <!-- #footer -->
</div> <!-- fusion-footer -->
<div class="fusion-sliding-bar-wrapper">
</div>
</div> <!-- wrapper -->
</div> <!-- #boxed-wrapper -->
<div class="fusion-top-frame"></div>
<div class="fusion-bottom-frame"></div>
<div class="fusion-boxed-shadow"></div>
<a class="fusion-one-page-text-link fusion-page-load-link"></a>
<div class="avada-footer-scripts">
<script>
		if ( navigator.serviceWorker ) {
			window.addEventListener( 'load', function() {
									{
						let updatedSw;
						navigator.serviceWorker.register(
							"https:\/\/www.profesionalmd.com\/?wp_service_worker=1",
							{"scope":"\/"}						).then( reg => {
																				} );

											}
				
				let refreshedPage = false;
				navigator.serviceWorker.addEventListener( 'controllerchange', () => {
					if ( ! refreshedPage ) {
						refreshedPage = true;
						window.location.reload();
					}
				} );
			} );
		}
	</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.profesionalmd.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.profesionalmd.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"https:\/\/www.profesionalmd.com\/wp-admin\/admin-ajax.php","hideEffect":"slide","onScroll":"no","onScrollOffset":"100","onClick":"no","cookieName":"cookie_notice_accepted","cookieValue":"true","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":"","refuse":"yes","revoke_cookies":"0","revoke_cookies_opt":"automatic","secure":"1"};
/* ]]> */
</script>
<script src="https://www.profesionalmd.com/wp-content/plugins/cookie-notice/js/front.min.js?ver=1.2.50" type="text/javascript"></script>
<script src="https://www.profesionalmd.com/media/fusion-scripts/810a42665508320b2f128b4c3e7c782d.min.js?ver=2.1.2" type="text/javascript"></script>
<script src="https://www.profesionalmd.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script type="text/javascript">
				jQuery( document ).ready( function() {
					var ajaxurl = 'https://www.profesionalmd.com/wp-admin/admin-ajax.php';
					if ( 0 < jQuery( '.fusion-login-nonce' ).length ) {
						jQuery.get( ajaxurl, { 'action': 'fusion_login_nonce' }, function( response ) {
							jQuery( '.fusion-login-nonce' ).html( response );
						});
					}
				});
				</script>
<!-- Cookie Notice plugin v1.2.50 by Digital Factory https://dfactory.eu/ -->
<div aria-label="Cookie Notice" class="cookie-notice-hidden cookie-revoke-hidden cn-position-bottom" id="cookie-notice" role="banner" style="background-color: #000;"><div class="cookie-notice-container" style="color: #fff;"><span class="cn-text-container" id="cn-notice-text">Ovaj veb sajt koristi Cockies ("Kolačiće") kako bi poboljšao korisničko iskustvo.</span><span class="cn-buttons-container" id="cn-notice-buttons"><a class="cn-set-cookie cn-button bootstrap button" data-cookie-set="accept" href="#" id="cn-accept-cookie">PRIHVATAM</a><a class="cn-set-cookie cn-button bootstrap button" data-cookie-set="refuse" href="#" id="cn-refuse-cookie">NE PRIHVATAM</a><a class="cn-more-info cn-button bootstrap button" href="https://www.profesionalmd.com/politika-privatnosti/" id="cn-more-info" target="_blank">Politika privatnosti</a></span></div>
</div>
<!-- / Cookie Notice plugin --> </div>
</body>
</html>
