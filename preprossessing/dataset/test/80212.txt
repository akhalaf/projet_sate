<?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Area Education Agency Manager</title>
<link href="/css/screen.css" media="screen" rel="stylesheet" type="text/css"/>
<meta content="text/html;charset=utf-8" http-equiv="content-type"/>
</head>
<body style="background-color: #FFFFFF;">
<br/>
<div class="oddrow2"><img alt="" border="0" height="1" src="images/space.gif" width="1"/></div>
<div class="oddrow" style="padding: 20px;">
<div style="text-align: center; font-size: 12pt; font-weight: bolder;">Area Education Agency Manager</div>
<br/>
<form style="margin: 0px;">
<table align="center" border="0" cellpadding="5" cellspacing="0">
<tr>
<td align="right">Agency:</td>
<td>
<select name="Location">
<option value=""></option>
<option value="aea08">Prairie Lakes Area Education Agency</option>
<option value="aea09">Mississippi Bend Area Education Agency</option>
<option value="aea12">Northwest Area Education Agency</option>
</select>
</td>
</tr>
<tr>
<td></td>
<td><input class="flat_button" type="submit" value="Launch Site"/></td>
</tr>
</table>
</form>
</div>
<div class="oddrow2"><img alt="" border="0" height="1" src="images/space.gif" width="1"/></div>
</body>
</html>
