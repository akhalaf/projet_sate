<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Inițiativa pentru antrepenoriat social în Moldova</title>
<meta content="Inițiativa pentru antrepenoriat social în Moldova" name="description"/>
<meta content="Inițiativa, antrepenoriat social, Moldova" name="keywords"/>
<meta content="Inițiativa pentru antrepenoriat social în Moldova" property="og:site_name"/>
<meta content="1306011343" property="fb:admins"/>
<meta content="Inițiativa pentru antrepenoriat social în Moldova" property="og:title"/>
<meta content="article" property="og:type"/>
<meta content="http://www.antreprenoriatsocial.md/index.php?l=ro" property="og:url"/>
<meta content="http://www.antreprenoriatsocial.md/images/site_logos/logo_ro.png" property="og:image"/>
<meta content="no-cache" http-equiv="cache-control"/>
<meta content="-1" http-equiv="Expires"/>
<meta content="index, follow" name="googlebot"/>
<meta content="1 days" name="revisit-after"/>
<meta content="follow,index" name="robots"/>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<script src="js/fontsizer.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/pdfobject.js" type="text/javascript"></script>
<script src="slick/slick.min.js" type="text/javascript"></script>
<script src="includes/fancy/jquery.fancybox.js?v=2.0.6" type="text/javascript"></script>
<script src="js/youmax.js"></script>
<link href="includes/fancy/jquery.fancybox.css?v=2.0.6" media="screen" rel="stylesheet" type="text/css"/>
<link href="includes/fancy/helpers/jquery.fancybox-buttons.css?v=1.0.2" rel="stylesheet" type="text/css"/>
<script src="includes/fancy/helpers/jquery.fancybox-buttons.js?v=1.0.2" type="text/javascript"></script>
<link href="includes/fancy/helpers/jquery.fancybox-thumbs.css?v=1.0.2" rel="stylesheet" type="text/css"/>
<script src="includes/fancy/helpers/jquery.fancybox-thumbs.js?v=1.0.2" type="text/javascript"></script>
<script src="includes/fancy/helpers/jquery.fancybox-media.js?v=1.0.0" type="text/javascript"></script>
<link href="http://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css"/>
<link href="slider/flexslider.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="css/animate.css" rel="stylesheet" type="text/css"/>
<link href="includes/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/jquery.event.calendar.css" rel="stylesheet"/>
<link href="slick/slick.css" rel="stylesheet"/>
<link href="slick/slick-theme.css" rel="stylesheet"/>
<style>
.fc-day-grid-event .fc-time{
    display:none;
}
</style>
<link href="http://antreprenoriatsocial.md/favicon.ico" rel="icon" type="image/x-icon"/>
</head>
<body>
<div class="top_bar_line">
<div class="top_bar_line_container">
<div style="float:left; height:20px;">
<p>
	   <a href="index.php?l=ro">Prima pagină</a><a href="http://feedburner.google.com/fb/a/mailverify?uri=cicde&amp;amp;loc=en_US" onclick="window.open(this.href, '', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no,width=600,height=550'); return false;"><span class="short_text" id="result_box" lang="en"><span class="hps"></span></span></a>   |   <a href="index.php?pag=sitemap&amp;l=ro">Harta site-ului</a>   |  <a href="rss.php?l=ro" target="_blank"><img alt="" src="/media/images/Icons/rss.png" style="width: 13px; height: 13px; margin-bottom:-1px;"/> Flux RSS</a>    |   <img alt="" src="/media/images/Icons/email.png" style="width: 14px; height: 14px; margin-bottom: -1px;"/> <span class="short_text" id="result_box" lang="en"><span class="hps">Abonare la stiri prin email</span></span><strong><a href="http://e-learning.antreprenoriatsocial.md/" target="_blank"><span class="short_text" id="result_box" lang="en"><span class="hps"></span></span></a></strong>   |   <a href="index.php?pag=feedback&amp;id=896&amp;l=ro">Contacte</a></p>
</div>
<div class="clear"></div>
</div>
</div>
<div class="clear"></div>
<div class="container">
<div class="header">
<a href="http://www.antreprenoriatsocial.md/index.php?l=ro">
<div class="site_logo" style="background:url(images/site_logos/logo_ro.png) no-repeat;"></div></a>
<div id="search_box">
<script>
  (function() {
    var cx = '';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>
<div class="menu_bar">
<ul class="dropdown">
<li style="margin-left:5px;"><a href="http://www.antreprenoriatsocial.md/index.php?l=ro"><img src="images/home_03.png"/></a></li>
<li>
<a href="index.php?pag=cat&amp;id=1065&amp;l=ro">Resurse</a>
<ul><li><a href="index.php?pag=page&amp;id=1191&amp;l=ro">Legislație</a>
</li>
<li><a href="index.php?pag=page&amp;id=1192&amp;l=ro">Surse de finanțare </a>
</li>
<li><a href="index.php?pag=page&amp;id=1190&amp;l=ro">Comisia Națională pentru Antreprenoriat Social</a>
</li>
<li><a href="index.php?pag=news&amp;id=1084&amp;l=ro">Publicații</a>
</li>
<li><a href="http://antreprenoriatsocial.md/?pag=album&amp;l=ro" target="_self">Galerie Foto</a>
</li>
<li><a href="https://www.youtube.com/watch?v=UipEhYams_M&amp;t=26s" target="_self">Galerie Video</a>
</li>
<li><a href="index.php?pag=page&amp;id=1053&amp;l=ro"> Linkuri utile</a>
</li>
</ul> </li>
<li>
<a href="index.php?pag=cat&amp;id=1049&amp;l=ro">Despre Noi</a>
<ul><li><a href="index.php?pag=page&amp;id=1181&amp;l=ro">Prezentare</a>
</li>
<li><a href="index.php?pag=news&amp;id=1055&amp;l=ro">Echipa</a>
</li>
<li><a href="index.php?pag=news&amp;id=1199&amp;l=ro">Parteneri</a>
</li>
<li><a href="index.php?pag=feedback&amp;id=896&amp;l=ro">Contacte</a>
</li>
</ul> </li>
<li>
<a href="index.php?pag=cat&amp;id=1177&amp;l=ro">Întreprinderi sociale</a>
<ul><li><a href="index.php?pag=news&amp;id=1178&amp;l=ro">Catalogul întreprinderilor sociale</a>
</li>
<li><a href="index.php?pag=news&amp;id=1179&amp;l=ro">Modele de întreprinderi sociale în lume </a>
</li>
<li><a href="index.php?pag=page&amp;id=1180&amp;l=ro">Cumpără responsabil </a>
</li>
</ul> </li>
<li>
<a href="index.php?pag=cat&amp;id=1182&amp;l=ro">Evenimente</a>
<ul><li><a href="index.php?pag=news&amp;id=1183&amp;l=ro">Conferința Națională pentru Antreprenoriat Social </a>
</li>
<li><a href="index.php?pag=news&amp;id=1184&amp;l=ro">Tîrgul Întreprinderilor Sociale </a>
</li>
<li><a href="index.php?pag=page&amp;id=1185&amp;l=ro">Platforma pentru Antreprenoriat Social </a>
</li>
</ul> </li>
<li>
<a href="index.php?pag=cat&amp;id=1186&amp;l=ro">Instruire</a>
<ul><li><a href="index.php?pag=news&amp;id=1188&amp;l=ro">Programul de instruire la locul de muncă </a>
</li>
<li><a href="index.php?pag=news&amp;id=1189&amp;l=ro">Oportunități de formare</a>
</li>
<li><a href="index.php?pag=news&amp;id=1187&amp;l=ro">Școala de Antreprenoriat Social </a>
</li>
</ul> </li>
<li>
<a href="index.php?pag=news&amp;id=1197&amp;l=ro">Noutăți</a>
</li>
<li>
<a href="index.php?pag=news&amp;id=1193&amp;l=ro">Istorii de succes</a>
</li>
<li>
<a href="index.php?pag=feedback&amp;id=896&amp;l=ro">Contacte</a>
</li>
</ul>
</div>
</div>
<div class="clear"></div>
<link href="news_slider/css/slideshows.css" rel="stylesheet"/>
<script src="news_slider/js/jquery.cycle.all.js"></script>
<script src="news_slider/js/jquery.easing.1.3.js"></script>
<script>

$(function() {

$("#slider_container").fadeIn(1000);

    $('#slideshow_1').cycle({

        fx: 'scrollHorz',		

		easing: 'easeInOutCirc',

		speed:  700, 

		timeout: 5000, 

		pager: '.ss1_wrapper .slideshow_paging', 

        prev: '.ss1_wrapper .slideshow_prev',

        next: '.ss1_wrapper .slideshow_next',

		before: function(currSlideElement, nextSlideElement) {

			var data = $('.data', $(nextSlideElement)).html();

			$('.ss1_wrapper .slideshow_box .data').fadeOut(300, function(){

				$('.ss1_wrapper .slideshow_box .data').remove();

				$('<div class="data">'+data+'</div>').hide().appendTo('.ss1_wrapper .slideshow_box').fadeIn(600);

			});

		}

    });

	

	

	$('.ss1_wrapper').mouseenter(function(){

		$('#slideshow_1').cycle('pause');

    }).mouseleave(function(){

		$('#slideshow_1').cycle('resume');

    });



	

});

</script>
<div id="slider_container">
<div class="ss1_wrapper">
<a class="slideshow_prev" href="#"><span>Previous</span></a>
<a class="slideshow_next" href="#"><span>Next</span></a>
<div class="slideshow_paging"></div>
<div class="slideshow_box">
<div class="data"></div>
</div>
<div class="slideshow" id="slideshow_1">
<div class="slideshow_item">
<div class="image">
<img src="media/images/slider/1111_4786850.jpg"/>
</div>
<div class="data">
<h4>Ce este Antreprenoriatul Social?</h4>
<p>
	Antreprenoriatul social reprezintă activitatea continuă de fabricare a producției, executare a lucrărilor sau prestare a serviciilor în scopul obținerii de venituri pentru a fi utilizate în scopuri sociale, inclusiv pentru prestarea serviciilor sociale.</p>
<p>
	Scopul antreprenoriatului social este soluționarea unor probleme sociale și creșterea gradului de ocupare a persoanelor aparținând grupurilor defavorizate.</p>
</div>
</div>
<div class="slideshow_item">
<div class="image">
<img src="media/images/slider/2222_2188281.jpg"/>
</div>
<div class="data">
<h4>La ce contribuie Antreprenoriatul Social?</h4>
<p>
	Antreprenoriatul social contribuie la creșterea incluziunii sociale, crearea de locuri de munca, implicarea persoanelor aparținând grupurilor defavorizate în activități economice și facilitarea accesului acestora la resursele și serviciile sociale.</p>
</div>
</div>
<div class="slideshow_item">
<div class="image">
<a href="https://www.facebook.com/antreprenoriatsocial.md/timeline"><img src="media/images/slider/333_3752377.jpg"/></a>
</div>
<div class="data">
<h4><a href="https://www.facebook.com/antreprenoriatsocial.md/timeline">Antreprenoriatul social are ca obiective</a></h4>
<p>
	- producerea de bunuri și/sau prestarea de servicii care contribuie la bunăstarea comunităţii sau a membrilor acesteia;</p>
<p>
	- promovarea, cu prioritate, a unor activităţi care pot genera sau asigura locuri de muncă pentru încadrarea persoanelor aparținând grupurilor defavorizate;</p>
<p>
	- dezvoltarea unor programe eficiente de formare dedicate persoanelor din grupurile defavorizate.</p>
<div class="readmore_slider"><div class="readmore2"><a href="https://www.facebook.com/antreprenoriatsocial.md/timeline">Citeşte mai mult... <i class="fa fa-angle-double-right"></i></a></div></div>
</div>
</div>
</div>
</div>
</div>
<div class="clear" style="height:5px;"></div>
<div class="sub_container">
<div class="right_side2" style="padding-top:25px;">
<div class="clear"></div>
<div class="nav_cap"><span class="stema"></span>  Antreprenoriat</div>
<div class="nav_block">
<ul class="navigation">
<li><a href="index.php?pag=news&amp;id=1194&amp;l=ro">Politici publice</a>
</li>
<li><a href="index.php?pag=news&amp;id=1195&amp;l=ro">Dezvoltarea afacerilor sociale</a>
</li>
</ul>
</div>
<div class="clear"></div>
<div class="nav_cap">Ultimele adaugări pe site</div>
<div class="clear"></div><br/>
<div style="padding-left:15px; padding-right:15px;"> <span style="color:#999; font-size:11px;">04.01.2021</span><br/><a href="index.php?pag=news&amp;id=1197&amp;rid=1336&amp;l=ro">(video) Trei companii sociale din ţară care au angajat persoane cu dizabilităţi au primit ajutor financiar</a>
<div class="clear"></div>
<div class="line2"></div>
<span style="color:#999; font-size:11px;">28.12.2020</span><br/><a href="index.php?pag=news&amp;id=1197&amp;rid=1340&amp;l=ro">Servicii de catering „Credem-Eco”- clientul este prietenul nostru!</a>
<div class="clear"></div>
<div class="line2"></div>
<span style="color:#999; font-size:11px;">28.12.2020</span><br/><a href="index.php?pag=news&amp;id=1197&amp;rid=1341&amp;l=ro">Întreprinderile sociale, susținute de Republica Cehă să depășească impactul negativ al pandemiei</a>
<div class="clear"></div>
<div class="line2"></div>
</div> <div class="clear"></div>
<p>
<a href="index.php?pag=video&amp;l=ro"><img alt="" src="/media/images/video.png" style="width: 250px; height: 80px;"/></a></p>
<hr/>
<div id="fb-root">
	 </div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.5&appId=478429655530802";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-page" data-adapt-container-width="true" data-hide-cover="false" data-href="https://www.facebook.com/antreprenoriatsocial.md/?fref=ts" data-show-facepile="true" data-small-header="false" data-tabs="timeline" data-width="250">
<div class="fb-xfbml-parse-ignore">
<blockquote cite="https://www.facebook.com/antreprenoriatsocial.md/?fref=ts">
<a href="https://www.facebook.com/antreprenoriatsocial.md/?fref=ts">Antreprenoriat social în Moldova</a></blockquote>
</div>
</div>
<hr/>
<p>
	  </p>
<div class="clear"></div><br/>
</div>
<div class="right_side">
<div class="text_content">
<div class="clear"></div>
﻿

    




<div class="page_title">Bine ai venit!</div>
<div class="clear"></div>
<div class="text_content"><p>
<img alt="" src="/media/images/12088251_875495052547598_3447101658573977674_n.jpg" style="width: 400px; height: 267px; float: right;"/>Antreprenoriatul social reprezintă activitatea continuă de fabricare a producției, executare a lucrărilor sau prestare a serviciilor în scopul obținerii de venituri pentru a fi utilizate în scopuri sociale, inclusiv pentru prestarea serviciilor sociale.</p>
<p>
	Scopul antreprenoriatului social este soluționarea unor probleme sociale și creșterea gradului de ocupare a persoanelor aparținând grupurilor defavorizate.</p>
<p>
	Antreprenoriatul social contribuie la creșterea incluziunii sociale, crearea de locuri de munca, implicarea persoanelor aparținând grupurilor defavorizate în activități economice și facilitarea accesului acestora la resursele și serviciile sociale.</p>
<h4>
	Antreprenoriatul social are următoarele obiective:</h4>
<ul>
<li>
		producerea de bunuri și/sau prestarea de servicii care contribuie la bunăstarea comunităţii sau a membrilor acesteia;</li>
<li>
		promovarea, cu prioritate, a unor activităţi care pot genera sau asigura locuri de muncă pentru încadrarea persoanelor aparținând grupurilor defavorizate;</li>
<li>
		dezvoltarea unor programe eficiente de formare dedicate persoanelor din grupurile defavorizate;</li>
<li>
		dezvoltarea serviciilor sociale pentru creşterea capacităţii de inserţie pe piaţa muncii a persoanelor din grupurile defavorizate;</li>
<li>
		implicarea indivizilor în dezvoltarea durabilă a comunităților locale;</li>
<li>
		dezvoltarea unei societăți incluzive și a responsabilității sociale;</li>
<li>
		permutarea accentului de pe asistența socială pe promovarea practicilor de incluziune socială, transformând astfel persoanele defavorizate din consumatori de resurse în generatori de valoare.</li>
</ul>
<p>
<a href="http://antreprenoriatsocial.md/index.php?pag=page&amp;id=1181&amp;l=ro"><strong>Citește mai mult ...</strong></a></p>
<hr/>
</div>
<div class="clear"></div>
<div class="clear"></div>
<br/>
<div class="page_title">Noutăți</div>
<div class="clear"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1197&amp;rid=1336&amp;l=ro">(video) Trei companii sociale din ţară care au angajat persoane cu dizabilităţi au primit ajutor financiar</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>04.01.2021</strong>   </div>
<div class="clear"></div><span>Trei companii sociale din ţară care au angajat persoane cu dizabilităţi au primit ajutor financiar pentru a-şi putea continua activitatea în pandemie. În cadrul unui proiect finanţat de Ministerul ceh al Afacerilor Externe şi un ONG de la noi, acestea au beneficiat de sume importante. Cantina socială din Răzeni, raionul Ialoveni se numără printre cele trei. Aici sunt angajate 25 de persoane cu dizabilităţi şi din familii nevoiaşe care prepară prânzuri calde pentru bătrânii din sat.</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1197&amp;rid=1336&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1197&amp;rid=1340&amp;l=ro">Servicii de catering „Credem-Eco”- clientul este prietenul nostru!</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>28.12.2020</strong>   </div>
<div class="clear"></div><span>Exclusiv pentru tine! Aceasta este deviza după care se ghidează întreprinderea socială „Credem-Eco” din municipiul Bălți care oferă servicii de catering. Pentru reprezentanții întreprinderii, fiecare client este unic și special, de aceea, zilnic se adaptează la dorințele și nevoile fiecăruia, iar acest lucru îi motivează și mai mult să aplice standarde ridicate de calitate, flexibilitate în alcătuirea meniului și adaptarea lui la bugetul clientului. </span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1197&amp;rid=1340&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1197&amp;rid=1341&amp;l=ro">Întreprinderile sociale, susținute de Republica Cehă să depășească impactul negativ al pandemiei</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>28.12.2020</strong>   </div>
<div class="clear"></div><span>Trei întreprinderi sociale au beneficiat de granturi pentru a-și putea continua activitatea și a susține persoanele cu dizabilități, afectate într-o măsură mai mare de criza generată de virusul COVID-19. „Credem-Eco” SRL, „Floare de cireş” SRL și Comunitatea Religioasă Mănăstirea cu hramul „Nașterea Domnului” au primit câte 236 mii de lei fiecare în cadrul proiectului „Răspuns la COVID-19: Ajutăm oamenii cu dizabilități să facă față pandemiei”, finanțat de Ministerul Afacerilor Externe al Republicii Cehe și implementat de People in Need Moldova.</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1197&amp;rid=1341&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="clear"></div>
<div align="right" style="float:right;"><div style="float:left; margin-right:10px; color:#FF9207;"><i class="fa fa-arrow-circle-right"></i>
</div><div style="float:left;"> <a href="index.php?pag=news&amp;id=1197&amp;l=ro">
  Mai multe din 
  Noutăți<br/>
</a></div><br/>
</div>
<div class="clear"></div>
<br/>
<div class="page_title">Publicații</div>
<div class="clear"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1339&amp;l=ro">Baseline study report of EUnlocking project (EU4Youth)</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>11.12.2020</strong>   </div>
<div class="clear"></div><span>This baseline study report presents the results of research on the state of social entrepreneurship in the Republic of Moldova. The Baseline Study Report in Republic of Moldova has been prepared within the framework of the EU-funded project ‘EU4Youth – Unlocking the potential of young social entrepreneurs in Moldova and Ukraine’. </span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1339&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1299&amp;l=ro">Raport de analiză a situației actuale și a provocărilor în ceea ce privește dezvoltarea antreprenoriatului social în Republica Moldova</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>01.10.2019</strong>   </div>
<div class="clear"></div><span>Documentul vizează analiza contextului actual și proiectarea cadrului de acțiune pentru perioada 2020-2025 de dezvoltare a antreprenoriatului social în vederea maximalizării potențialului pe care îl poate aduce acest sector în dezvoltarea comunitară, crearea de locuri de muncă în special pentru categorii defavorizate, creșterea gradului de asociere a producătorilor locali, dezvoltare rurală, durabilă și sustenabilă.</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1299&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1253&amp;l=ro">Studiu privind situația antreprenoriatului social și posibilitățile antreprenoriale ale tinerilor în Republica Moldova</a></div>
<div style=" ">
</div>
<div style="float:left; width:680px;">
<div class="data">Publicat: <strong>21.12.2018</strong>   </div>
<div class="clear"></div><span>Institutul de Instruire în Dezvoltare "MilleniuM" a elaborat în 2018 un studiu amplu, de ansamblu privind situația antreprenoriatului social  și posibilitățile antreprenoriale ale tinerilor în Republica Moldova. Studiul a fost elaborat de Asociația Națională a Tinerilor Manageri în cadrul proiectului Erasmus + KA2 "EAST Entrepreneurial (Training Strategic)".</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1253&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1198&amp;l=ro">Dezvoltarea economiei sociale din Republica Moldova prin prisma promovării egalităţii de gen, IDIS Viitorul, martie 2016</a></div>
<div style="float:left; width:30px;">
<div style="
  width:0px; ">
</div></div>
<div style="float:left; width:860px;">
<div class="data">Publicat: <strong>27.11.2017</strong>   </div>
<div class="clear"></div><span>Studiul recomandă elaborarea și adoptarea unei legi şi politici de cadru privind reglementarea domeniului economiei sociale, stabilirea măsurilor de promovare şi de susținere a economiei sociale ce ar facilita dezvoltarea antreprenoriatului social şi, respectiv, ar încuraja participarea economică a femeilor şi incluziunea socială a celor mai vulnerabile pături din populație.</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1198&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1199&amp;l=ro">Antreprenoriatul social în Republica Moldova: Realităţi şi perspective, Fundația Est-Europeană, 2013</a></div>
<div style="float:left; width:30px;">
<div style="
  width:0px; ">
</div></div>
<div style="float:left; width:860px;">
<div class="data">Publicat: <strong>27.11.2017</strong>   </div>
<div class="clear"></div><span>Acest studiu a fost elaborat în contextul Strategiei de Dezvoltare a Societăţii Civile 2012-2015 şi a Planului de Acţiuni care prevede valorificarea antreprenoriatului social şi instituirea de facilităţi corespunzătoare. Documentul are menirea de a facilita activitatea grupului de lucru interguvernamental instituit pe lângă Ministerul Economiei al Republicii Moldova pentru elaborarea Legii privind antreprenoriatul social.</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1199&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="news_title"><a href="index.php?pag=news&amp;id=1084&amp;rid=1200&amp;l=ro">Economie socială și antreprenoriat social</a></div>
<div style="float:left; width:30px;">
<div style="
  width:0px; ">
</div></div>
<div style="float:left; width:860px;">
<div class="data">Publicat: <strong>27.11.2017</strong>   </div>
<div class="clear"></div><span>Această broșură a fost creată în cadrul Conferinței Naționale pentru Antreprenoriat Social, ediția a II-a, Chișinău 2016</span>
<span class="readmore2">
<a href="index.php?pag=news&amp;id=1084&amp;rid=1200&amp;l=ro">Citeşte mai mult... <i class="fa fa-angle-double-right"></i>
</a></span>
</div>
<div class="clear"></div>
<div class="line_news"></div>
<div class="clear"></div>
<div align="right" style="float:right;"><div style="float:left; margin-right:10px; color:#FF9207;"><i class="fa fa-arrow-circle-right"></i>
</div><div style="float:left;"> <a href="index.php?pag=news&amp;id=1084&amp;l=ro">
  Mai multe din 
  Publicații<br/>
</a></div><br/>
</div>
<div class="clear"></div>
<br/>
</div>
</div>
<div class="clear"></div>
<div>
<hr/>
<table border="0" cellpadding="10" cellspacing="0" style="width: 1100px;">
<tbody>
<tr>
<td style="text-align: center;">
				 </td>
<td style="text-align: center;">
				 </td>
<td style="text-align: center;">
				 </td>
<td style="text-align: center;">
				 </td>
<td style="text-align: center;">
				 </td>
</tr>
</tbody>
</table>
</div>
</div>
</div><br/>
<div class="clear"></div>
<div align="center" class="foto_scroll">
<div class="container">
<div class="foto_scroll_title"><a href="index.php?pag=album&amp;l=ro">Imagini aleatoare din Albume Foto</a></div>
<div class="clear"></div>
<div class="gallery_scroll">
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12096513_875495039214266_2669714843285969075_n_2720321.jpg"/></a></div>
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12074836_875495042547599_4903222190702194148_n_8883954.jpg"/></a></div>
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12141622_875495285880908_7607249776443300980_n_9622043.jpg"/></a></div>
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12107904_875495045880932_7711046609912161854_n_3427594.jpg"/></a></div>
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12088251_875495052547598_3447101658573977674_n_1281909.jpg"/></a></div>
<div><a href="index.php?pag=album&amp;rid=42&amp;l=ro"><img align="absmiddle" data-lazy="media/images/small/12108778_875495049214265_2279107083708060104_n_6734342.jpg"/></a></div>
</div>
</div>
<div class="clear"></div>
<div></div>
<div class="clear"></div>
<br/>
</div>
<div class="clear"></div>
<div class="footer">
<footer class="footer-distributed">
<div class="footer-left">
<img alt="" src="images/cicde_logo_03.png"/></div>
<div class="footer-center">
<div>
<i class="fa fa-map-marker"></i>
<p>
			Adresa: str. Ştefan cel Mare 49/1b,<br/>
			sat. Răzeni, MD 7728, r. Ialoveni</p>
</div>
<div>
<i class="fa fa-phone"></i>
<p>
			(+373) 268 95146, 0 268 73325</p>
</div>
<div>
<i class="fa fa-envelope"></i>
<p>
<a href="maito:info@antreprenoriatsocial.md">info@antreprenoriatsocial.md</a></p>
</div>
</div>
<div class="footer-right">
<p class="footer-company-about">
<span>Despre Noi</span> Asoсiaţia Obştească “Eco-Răzeni” este o organizaţie neguvernamentală locală fondată în 1998 care activează pentru dezvoltarea programelor eficiente și sustenabile în crearea oportunităților de participare a tinerilor la viața comunității și incluziunea socio-profesională a tinerilor cu dizabilități.<br/>
<br/>
<a href="https://www.facebook.com/antreprenoriatsocial.md/timeline" target="_blank"><img alt="" src="/media/images/Icons/facebook.png" style="width: 32px; height: 32px;"/></a>  <img alt="" src="/media/images/Icons/twitter.png" style="width: 32px; height: 32px;"/> <img alt="" src="/media/images/Icons/linkedin.png" style="width: 32px; height: 32px;"/>  <img alt="" src="/media/images/Icons/youtube.png" style="width: 32px; height: 32px;"/>  <img alt="" src="/media/images/Icons/ok.png" style="width: 32px; height: 32px;"/>  <img alt="" src="/media/images/Icons/skype.png" style="width: 32px; height: 32px;"/>  <a href="index.php?pag=feedback&amp;id=896&amp;l=ro"><img alt="" height="32" src="/media/images/Icons/email.png" width="32"/></a>  <a href="rss.php?l=ro" target="_blank"><img alt="" src="/media/images/Icons/rss.png" style="width: 32px; height: 32px;"/></a></p>
</div>
<div class="line">
	 </div>
<p class="footer-company-about">
	Această pagină web a fost creată cu suportul financiar acordat din partea IM Swedish Development Partner. IM și donatorii săi inițiali nu împărtășesc neapărat punctele de vedere și opiniile prezentate aici. Responsabilitatea aparține exclusiv autorilor.</p>
<div class="clear"></div>
</footer>
</div>
<div class="bottom_bg">
<div class="bottom">
<div class="clear"></div>
<div align="left" style="float:left;">
© 2021 <strong>Inițiativa pentru antrepenoriat social în Moldova</strong>   |   Site elaborat de <a href="http://webdesign.md">Andrei Madan</a> </div>
<div align="right" id="page_up" style="float:right; height:10px; margin-top:-7px;">
PAGINA ÎN SUS <img align="absmiddle" height="35" src="images/totop-arrow-hover.png" width="37"/><br/>
</div>
</div>
</div>
<script language="JavaScript">

$(document).ready(function(){

$(".navigation").accordion({
		accordion:true,
		speed: 500,
		closedSign: '+',
		openedSign: '-'
	});
	
$('#page_up').click(function () {
		        jQuery('body,html').animate({
		            scrollTop: 0
		        },
		        800);
	});

$('.label').fadeIn('slow');




$('.gallery_scroll').slick({
  slidesToShow: 6,
  speed: 1500,
  slidesToScroll: 3,
  autoplay: true,
  autoplaySpeed: 4000,
});

setTimeout( function(){
    jQuery(".gsc-input").attr("placeholder", "Căutare pe site ...");
  }
 , 1000 );
});


</script>
<script type="text/javascript">
$(document).ready(function() {
$('.fancy').fancybox();
});
</script>
</body>
</html>
