<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ --><!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8"/>
<!-- Mobile View -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://therailhousegrill.com/xmlrpc.php" rel="pingback"/>
<title>The Railhouse Grill – Lake City, MN</title>
<!-- Async WordPress.com Remote Login -->
<script id="wpcom_remote_login_js">
var wpcom_remote_login_extra_auth = '';
function wpcom_remote_login_remove_dom_node_id( element_id ) {
	var dom_node = document.getElementById( element_id );
	if ( dom_node ) { dom_node.parentNode.removeChild( dom_node ); }
}
function wpcom_remote_login_remove_dom_node_classes( class_name ) {
	var dom_nodes = document.querySelectorAll( '.' + class_name );
	for ( var i = 0; i < dom_nodes.length; i++ ) {
		dom_nodes[ i ].parentNode.removeChild( dom_nodes[ i ] );
	}
}
function wpcom_remote_login_final_cleanup() {
	wpcom_remote_login_remove_dom_node_classes( "wpcom_remote_login_msg" );
	wpcom_remote_login_remove_dom_node_id( "wpcom_remote_login_key" );
	wpcom_remote_login_remove_dom_node_id( "wpcom_remote_login_validate" );
	wpcom_remote_login_remove_dom_node_id( "wpcom_remote_login_js" );
	wpcom_remote_login_remove_dom_node_id( "wpcom_request_access_iframe" );
	wpcom_remote_login_remove_dom_node_id( "wpcom_request_access_styles" );
}

// Watch for messages back from the remote login
window.addEventListener( "message", function( e ) {
	if ( e.origin === "https://r-login.wordpress.com" ) {
		var data = {};
		try {
			data = JSON.parse( e.data );
		} catch( e ) {
			wpcom_remote_login_final_cleanup();
			return;
		}

		if ( data.msg === 'LOGIN' ) {
			// Clean up the login check iframe
			wpcom_remote_login_remove_dom_node_id( "wpcom_remote_login_key" );

			var id_regex = new RegExp( /^[0-9]+$/ );
			var token_regex = new RegExp( /^.*|.*|.*$/ );
			if (
				token_regex.test( data.token )
				&& id_regex.test( data.wpcomid )
			) {
				// We have everything we need to ask for a login
				var script = document.createElement( "script" );
				script.setAttribute( "id", "wpcom_remote_login_validate" );
				script.src = '/remote-login.php?wpcom_remote_login=validate'
					+ '&wpcomid=' + data.wpcomid
					+ '&token=' + encodeURIComponent( data.token )
					+ '&host=' + window.location.protocol
						+ '//' + window.location.hostname;
				document.body.appendChild( script );
			}

			return;
		}

		// Safari ITP, not logged in, so redirect
		if ( data.msg === 'LOGIN-REDIRECT' ) {
			window.location = 'https://wordpress.com/log-in?redirect_to=' + window.location.href;
			return;
		}

		// Safari ITP, storage access failed, remove the request
		if ( data.msg === 'LOGIN-REMOVE' ) {
			var css_zap = 'html { -webkit-transition: margin-top 1s; transition: margin-top 1s; } /* 9001 */ html { margin-top: 0 !important; } * html body { margin-top: 0 !important; } @media screen and ( max-width: 782px ) { html { margin-top: 0 !important; } * html body { margin-top: 0 !important; } }';
			var style_zap = document.createElement( 'style' );
			style_zap.type = 'text/css';
			style_zap.appendChild( document.createTextNode( css_zap ) );
			document.body.appendChild( style_zap );

			var e = document.getElementById( 'wpcom_request_access_iframe' );
			e.parentNode.removeChild( e );

			document.cookie = 'wordpress_com_login_access=denied; path=/; max-age=31536000';

			return;
		}

		// Safari ITP
		if ( data.msg === 'REQUEST_ACCESS' ) {
			console.log( 'request access: safari' );

			// Check ITP iframe enable/disable knob
			if ( wpcom_remote_login_extra_auth !== 'safari_itp_iframe' ) {
				return;
			}

			// If we are in a "private window" there is no ITP.
			var private_window = false;
			try {
				var opendb = window.openDatabase( null, null, null, null );
			} catch( e ) {
				private_window = true;
			}

			if ( private_window ) {
				console.log( 'private window' );
				return;
			}

			var iframe = document.createElement( 'iframe' );
			iframe.id = 'wpcom_request_access_iframe';
			iframe.setAttribute( 'scrolling', 'no' );
			iframe.setAttribute( 'sandbox', 'allow-storage-access-by-user-activation allow-scripts allow-same-origin allow-top-navigation-by-user-activation' );
			iframe.src = 'https://r-login.wordpress.com/remote-login.php?wpcom_remote_login=request_access&origin=' + encodeURIComponent( data.origin ) + '&wpcomid=' + encodeURIComponent( data.wpcomid );

			var css = 'html { -webkit-transition: margin-top 1s; transition: margin-top 1s; } /* 9001 */ html { margin-top: 46px !important; } * html body { margin-top: 46px !important; } @media screen and ( max-width: 660px ) { html { margin-top: 71px !important; } * html body { margin-top: 71px !important; } #wpcom_request_access_iframe { display: block; height: 71px !important; } } #wpcom_request_access_iframe { border: 0px; height: 46px; position: fixed; top: 0; left: 0; width: 100%; min-width: 100%; z-index: 99999; background: #23282d; } ';

			var style = document.createElement( 'style' );
			style.type = 'text/css';
			style.id = 'wpcom_request_access_styles';
			style.appendChild( document.createTextNode( css ) );
			document.body.appendChild( style );

			document.body.appendChild( iframe );
		}

		if ( data.msg === 'DONE' ) {
			wpcom_remote_login_final_cleanup();
		}
	}
}, false );

// Inject the remote login iframe after the page has had a chance to load
// more critical resources
window.addEventListener( "DOMContentLoaded", function( e ) {
	var iframe = document.createElement( "iframe" );
	iframe.style.display = "none";
	iframe.setAttribute( "scrolling", "no" );
	iframe.setAttribute( "id", "wpcom_remote_login_key" );
	iframe.src = "https://r-login.wordpress.com/remote-login.php"
		+ "?wpcom_remote_login=key"
		+ "&origin=aHR0cHM6Ly90aGVyYWlsaG91c2VncmlsbC5jb20%3D"
		+ "&wpcomid=149458823"
		+ "&time=1610614426";
	document.body.appendChild( iframe );
}, false );
</script>
<link href="//s2.wp.com" rel="dns-prefetch"/>
<link href="//therailhousegrill.wordpress.com" rel="dns-prefetch"/>
<link href="//s0.wp.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="https://therailhousegrill.com/feed/" rel="alternate" title="The Railhouse Grill » Feed" type="application/rss+xml"/>
<link href="https://therailhousegrill.com/comments/feed/" rel="alternate" title="The Railhouse Grill » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
		/* <![CDATA[ */
		function addLoadEvent(func) {
			var oldonload = window.onload;
			if (typeof window.onload != 'function') {
				window.onload = func;
			} else {
				window.onload = function () {
					oldonload();
					func();
				}
			}
		}
		/* ]]> */
	</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s0.wp.com\/wp-content\/mu-plugins\/wpcom-smileys\/twemoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s0.wp.com\/wp-content\/mu-plugins\/wpcom-smileys\/twemoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/s2.wp.com\/wp-includes\/js\/wp-emoji-release.min.js?m=1605528427h&ver=5.6-RC5-49737"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://s0.wp.com/_static/??-eJydkFFOwzAMQC+E621IZXwgzpK0JnjETRQnqnp73BWkCdgE/Fiy/Z5lG+cMQ5oqTRWlQY4t8KQ45yEJqHCk5UvWDap3eKF9OqFZ6qkE6xTCx67vDugbxxF9TMMbRPbFlQW1LpF+MeZsKZ6oZrfqbkmtQig8/ndEcZWnoFf0i/vXC6wu2dWVEBrZUSQx7Ja2Pcr7XEgVLAo3gfpq4ve/bWX8oNCU6lpx1vl5veu8kZsDkjz/WX0xCtxMmuSsPsvTvt/v7o/9w2F3egfl7M62?cssminify=yes" id="all-css-0-1" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css">
.has-text-align-justify {
	text-align:justify;
}
</style>
<link crossorigin="anonymous" href="https://fonts.googleapis.com/css?family=Oswald%3A400%2C700%2C300%7COpen+Sans%3A400%2C300%2C600%2C700%2C800%2C800italic%2C700italic%2C600italic%2C400italic%2C300italic%7CMerriweather%3A400%2C700%2C300%2C900%7CPlayfair+Display%3A400%2C400italic%2C700%2C700italic%2C900%2C900italic&amp;subset=latin%2Clatin-ext&amp;ver=1.0" id="restaurant-fonts-css" media="all" rel="stylesheet"/>
<link href="https://s1.wp.com/_static/??-eJzTLy/QTc7PK0nNK9HPLdUtyClNz8wr1k9PzdfNyU9OLMnMz0Ph6KblJGYW6SUXF+voY9dalJqUk58OZKbrA1UhcUGa7HNtDU1MLU1MLMwNTbIAmkQtqg==?cssminify=yes" id="all-css-2-1" media="all" rel="stylesheet" type="text/css"/>
<style id="jetpack-global-styles-frontend-style-inline-css">
:root { --font-headings: unset; --font-base: unset; --font-headings-default: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif; --font-base-default: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;}
</style>
<link href="https://s0.wp.com/wp-content/themes/h4/global.css?m=1420737423h&amp;cssminify=yes" id="all-css-4-1" media="all" rel="stylesheet" type="text/css"/>
<script id="wpcom-actionbar-placeholder-js-extra">
var actionbardata = {"siteID":"149458823","siteName":"The Railhouse Grill","siteURL":"https:\/\/therailhousegrill.com","icon":"<img alt='' src='https:\/\/therailhousegrill.files.wordpress.com\/2020\/11\/cropped-rhg-logo.jpg?w=50' class='avatar avatar-50' height='50' width='50' \/>","canManageOptions":"","canCustomizeSite":"","isFollowing":"","themeSlug":"premium\/restaurant","signupURL":"https:\/\/wordpress.com\/start\/","loginURL":"https:\/\/wordpress.com\/log-in?redirect_to=https%3A%2F%2Ftherailhousegrill.com%2F&signup_flow=account&domain=therailhousegrill.com","themeURL":"https:\/\/wordpress.com\/theme\/restaurant\/","xhrURL":"https:\/\/therailhousegrill.com\/wp-admin\/admin-ajax.php","nonce":"fe74c7516f","isSingular":"1","isFolded":"","isLoggedIn":"","isMobile":"","subscribeNonce":"<input type=\"hidden\" id=\"_wpnonce\" name=\"_wpnonce\" value=\"8de5c26b75\" \/>","referer":"https:\/\/therailhousegrill.com\/","canFollow":"","feedID":"85682883","statusMessage":"","customizeLink":"https:\/\/therailhousegrill.wordpress.com\/wp-admin\/customize.php?url=https%3A%2F%2Ftherailhousegrill.wordpress.com%2F","postID":"4","shortlink":"https:\/\/wp.me\/Pa774b-4","canEditPost":"","editLink":"https:\/\/wordpress.com\/page\/therailhousegrill.com\/4","statsLink":"https:\/\/wordpress.com\/stats\/post\/4\/therailhousegrill.com","i18n":{"view":"View site","follow":"Follow","following":"Following","edit":"Edit","login":"Log in","signup":"Sign up","customize":"Customize","report":"Report this content","themeInfo":"Get theme: Restaurant","shortlink":"Copy shortlink","copied":"Copied","followedText":"New posts from this site will now appear in your <a href=\"https:\/\/wordpress.com\/read\">Reader<\/a>","foldBar":"Collapse this bar","unfoldBar":"Expand this bar","editSubs":"Manage subscriptions","viewReader":"View site in Reader","viewReadPost":"View post in Reader","subscribe":"Sign me up","enterEmail":"Enter your email address","followers":"","alreadyUser":"Already have a WordPress.com account? <a href=\"https:\/\/wordpress.com\/log-in?redirect_to=https%3A%2F%2Ftherailhousegrill.com%2F&signup_flow=account&domain=therailhousegrill.com\">Log in now.<\/a>","stats":"Stats"}};
</script>
<script src="https://s0.wp.com/_static/??/wp-content/js/postmessage.js,/wp-content/js/mobile-useragent-info.js,/wp-includes/js/jquery/jquery.js?m=1609849039j" type="text/javascript"></script>
<link href="https://therailhousegrill.wordpress.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://s1.wp.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress.com" name="generator"/>
<link href="https://therailhousegrill.com/" rel="canonical"/>
<link href="https://wp.me/Pa774b-4" rel="shortlink"/>
<link href="https://public-api.wordpress.com/oembed/?format=json&amp;url=https%3A%2F%2Ftherailhousegrill.com%2F&amp;for=wpcom-auto-discovery" rel="alternate" type="application/json+oembed"/><link href="https://public-api.wordpress.com/oembed/?format=xml&amp;url=https%3A%2F%2Ftherailhousegrill.com%2F&amp;for=wpcom-auto-discovery" rel="alternate" type="application/xml+oembed"/>
<!-- Jetpack Open Graph Tags -->
<meta content="website" property="og:type"/>
<meta content="The Railhouse Grill" property="og:title"/>
<meta content="Lake City, MN" property="og:description"/>
<meta content="https://therailhousegrill.com/" property="og:url"/>
<meta content="The Railhouse Grill" property="og:site_name"/>
<meta content="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=200" property="og:image"/>
<meta content="200" property="og:image:width"/>
<meta content="200" property="og:image:height"/>
<meta content="en_US" property="og:locale"/>
<meta content="@wordpressdotcom" name="twitter:site"/>
<meta content="Home" name="twitter:text:title"/>
<meta content="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=240" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://www.facebook.com/WordPresscom" property="article:publisher"/>
<!-- End Jetpack Open Graph Tags -->
<link href="https://therailhousegrill.com/osd.xml" rel="search" title="The Railhouse Grill" type="application/opensearchdescription+xml"/>
<link href="https://s1.wp.com/opensearch.xml" rel="search" title="WordPress.com" type="application/opensearchdescription+xml"/>
<meta content="#0a0a0a" name="theme-color"/>
<meta content="The Railhouse Grill" name="application-name"/><meta content="width=device-width;height=device-height" name="msapplication-window"/><meta content="Lake City, MN" name="msapplication-tooltip"/><meta content="name=Subscribe;action-uri=https://therailhousegrill.com/feed/;icon-uri=https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=16" name="msapplication-task"/><meta content="name=Sign up for a free blog;action-uri=http://wordpress.com/signup/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="name=WordPress.com Support;action-uri=http://support.wordpress.com/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="name=WordPress.com Forums;action-uri=http://forums.wordpress.com/;icon-uri=https://s1.wp.com/i/favicon.ico" name="msapplication-task"/><meta content="Welcome to my site!" name="description"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #0a0a0a; }
</style>
<style id="custom-colors-css" type="text/css">a.button:hover,.reply a:hover,#searchsubmit:hover,#prevLink a:hover,#nextLink a:hover,.more-link:hover,.featured-pages .read-more:hover,#submit:hover,#comments #respond input#submit:hover,input[type=submit]:hover{color:#3d3d3d}.blog .sidebar #wp-calendar #today,.category .sidebar #wp-calendar #today{color:#828282}.pagination .page-numbers{color:#828282}.blog .sidebar,.category .sidebar{color:#888}body{background-color:#0a0a0a}a.button:hover,.reply a:hover,#searchsubmit:hover,#prevLink a:hover,#nextLink a:hover,.more-link:hover,.featured-pages .read-more:hover,#submit:hover,#comments #respond input#submit:hover,input[type=submit]:hover{background-color:#ff948c}a,a:link,a:visited,.widget ul.menu li a,.widget ul.menu li a:hover{color:#da1305}h1 a:hover,h2 a:hover,h3 a:hover,h4 a:hover,h5 a:hover,h6 a:hover,h1 a:focus,h2 a:focus,h3 a:focus,h4 a:focus,h5 a:focus,h6 a:focus,h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#da1305}.blog .sidebar a,.category .sidebar a,.blog .sidebar a:link,.blog .sidebar a:visited,.blog .sidebar a:visited,.category .sidebar a:visited{color:#ff948c}h1,h2,h3,h4,h5,h6{color:#a55960}#header .site-title a{color:#a55960}h1 a,h2 a,h3 a,h4 a,h5 a,h6 a,h1 a:link,h2 a:link,h3 a:link,h4 a:link,h5 a:link,h6 a:link,h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#a55960}.blog .sidebar h1,.category .sidebar h1{color:#ba7c81}.blog .sidebar h2,.category .sidebar h2{color:#ba7c81}.blog .sidebar h3,.category .sidebar h3{color:#ba7c81}.blog .sidebar h4,.category .sidebar h4{color:#ba7c81}.blog .sidebar h5,.category .sidebar h5{color:#ba7c81}.blog .sidebar h6,.category .sidebar h6{color:#ba7c81}</style>
<link href="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=32" rel="icon" sizes="32x32"/>
<link href="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=192" rel="icon" sizes="192x192"/>
<link href="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=180" rel="apple-touch-icon"/>
<meta content="https://therailhousegrill.files.wordpress.com/2020/11/cropped-rhg-logo.jpg?w=270" name="msapplication-TileImage"/>
<link href="https://s0.wp.com/?custom-css=1&amp;csblog=a774b&amp;cscache=6&amp;csrev=19" id="custom-css-css" rel="stylesheet" type="text/css"/>
<!-- Your Google Analytics Plugin is missing the tracking ID -->
</head>
<body class="home page-template page-template-template-home page-template-template-home-php page page-id-4 custom-background customizer-styles-applied restaurant-singular restaurant-header-inactive restaurant-sidebar-inactive restaurant-title-left restaurant-slider-active restaurant-relative-text has-site-logo highlander-enabled highlander-light custom-colors">
<!-- BEGIN #wrap -->
<div id="wrap">
<!-- BEGIN .container -->
<div class="container">
<!-- BEGIN #header -->
<div class="absolute" data-speed="10" data-type="background" id="header">
<!-- BEGIN .row -->
<div class="row">
<!-- BEGIN .content -->
<div class="content shadow radius-bottom">
<!-- BEGIN .padded -->
<div class="padded">
<!-- BEGIN .outline -->
<div class="outline">
<!-- BEGIN .contact-info -->
<div class="contact-info">
<div class="align-left">
<span class="contact-address"><i class="fa fa-map-marker"></i> 800 W Lyon Ave Lake City, MN 55041</span>
</div>
<div class="align-right">
<span class="contact-phone text-right"><i class="fa fa-phone"></i> 651-345-5762</span>
</div>
<!-- END .contact-info -->
</div>
<!-- BEGIN .logo-nav -->
<div class="logo-nav">
<!-- BEGIN .title-holder -->
<div class="title-holder">
<a class="site-logo-link" href="https://therailhousegrill.com/" itemprop="url" rel="home"><img alt="" class="site-logo attachment-restaurant-logo-size" data-attachment-id="1604" data-comments-opened="0" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="RHG Logo" data-large-file="https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=889" data-medium-file="https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=260" data-orig-file="https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg" data-orig-size="1576,1816" data-permalink="https://therailhousegrill.com/rhg-logo/" data-size="restaurant-logo-size" height="280" itemprop="logo" loading="lazy" sizes="(max-width: 243px) 100vw, 243px" src="https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=243" srcset="https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=243 243w, https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=486 486w, https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=130 130w, https://therailhousegrill.files.wordpress.com/2020/11/rhg-logo.jpg?w=260 260w" width="243"/></a>
<div class="title-disabled" id="masthead">
<h1 class="site-title">
<a href="https://therailhousegrill.com/" rel="home">The Railhouse Grill</a>
</h1>
<h2 class="site-description">
											Lake City, MN										</h2>
</div>
<!-- END .title-holder -->
</div>
<!-- BEGIN .nav-holder -->
<div class="nav-holder">
<!-- BEGIN #navigation -->
<nav class="navigation-main" id="navigation" role="navigation">
<button class="menu-toggle">Menu</button>
<div class="menu-container"><ul class="menu" id="menu-primary"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-11" id="menu-item-11"><a aria-current="page" href="https://therailhousegrill.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-120" id="menu-item-120"><a href="https://therailhousegrill.com/menustherai/">Menus</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-145" id="menu-item-145"><a href="https://therailhousegrill.com/coffee-depot/">Coffee Depot</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121" id="menu-item-121"><a href="https://therailhousegrill.com/live-music/">Live Music</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-122" id="menu-item-122"><a href="https://therailhousegrill.com/hours/">Hours</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-123" id="menu-item-123"><a href="https://therailhousegrill.com/location/">Location</a></li>
</ul></div>
<!-- END #navigation -->
</nav>
<!-- END .nav-holder -->
</div>
<!-- END .logo-nav -->
</div>
<!-- END .outline -->
</div>
<!-- END .padded -->
</div>
<!-- END .content -->
</div>
<!-- END .row -->
</div>
<!-- END #header -->
</div>
<!-- BEGIN .home-slider -->
<div class="home-slider">
<!-- BEGIN .row -->
<div class="row">
<!-- BEGIN .slideshow -->
<div class="slideshow">
<!-- BEGIN .flexslider -->
<div class="flexslider loading" data-speed="8000">
<div class="preloader"></div>
<!-- BEGIN .slides -->
<ul class="slides">
<li class="post-34 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized tag-featured" data-speed="10" data-type="background" id="post-34" style="background-image: url(https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=2327);">
<!-- BEGIN .information -->
<div class="information vertical-center">
<!-- BEGIN .content -->
<div class="content no-bg text-white clearfix">
<!-- BEGIN .text-center -->
<div class="text-center">
<h2 class="headline"><a href="https://therailhousegrill.com/2018/07/24/welcome/" rel="bookmark">Welcome to the Railhouse</a></h2>
<!-- END .text-center -->
</div>
<!-- END .content -->
</div>
<!-- END .information -->
</div>
<div class="hide-img"><img alt="" class="attachment-restaurant-featured-large size-restaurant-featured-large wp-post-image" data-attachment-id="129" data-comments-opened="0" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="Ahi Salad" data-large-file="https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=1024" data-medium-file="https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=300" data-orig-file="https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg" data-orig-size="2327,1745" data-permalink="https://therailhousegrill.com/2018/07/24/welcome/ahi-salad-2/" height="1745" loading="lazy" sizes="(max-width: 2327px) 100vw, 2327px" src="https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=2327" srcset="https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg 2327w, https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=150 150w, https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=300 300w, https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=768 768w, https://therailhousegrill.files.wordpress.com/2018/07/ahi-salad1.jpg?w=1024 1024w" width="2327"/></div>
</li>
<!-- END .slides -->
</ul>
<!-- END .flexslider -->
</div>
<!-- END .slideshow -->
</div>
<!-- END .row -->
</div>
<!-- END .home-slider -->
</div>
<!-- BEGIN .homepage -->
<div class="homepage">
<!-- BEGIN .row -->
<div class="row">
<!-- BEGIN .content -->
<div class="content featured-pages no-bg">
<div class="holder third first">
<!-- BEGIN .content -->
<div class="content shadow radius-full">
<!-- BEGIN .padded -->
<div class="padded">
<!-- BEGIN .outline -->
<div class="outline">
<!-- BEGIN .information -->
<div class="information">
<h2 class="headline text-center">Menus</h2>
<p>Breakfast, lunch, and dinner.<br/>
Weekend entrée specials.<br/>
Coffee Depot with espresso, pastries, ice cream, and more.</p>
<!-- END .information -->
</div>
<!-- END .outline -->
</div>
<!-- END .padded -->
</div>
<!-- END .content -->
</div>
</div>
<div class="holder third">
<!-- BEGIN .content -->
<div class="content shadow radius-full">
<!-- BEGIN .padded -->
<div class="padded">
<div class="home-featured-banner" style="background-image:url(https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=577);">
<div class="flex-wrapper">
<div class="banner-title-wrapper">
<a href="https://therailhousegrill.com/live-music/">
<h1 class="headline img-headline">Live Music</h1>
</a>
</div>
<img alt="" class="attachment-restaurant-featured-large size-restaurant-featured-large wp-post-image" data-attachment-id="147" data-comments-opened="0" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"1"}' data-image-title="B&amp;amp;Wcuchetti" data-large-file="https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=577" data-medium-file="https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=300" data-orig-file="https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg" data-orig-size="577,412" data-permalink="https://therailhousegrill.com/live-music/bwcuchetti/" height="412" loading="lazy" sizes="(max-width: 577px) 100vw, 577px" src="https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=577" srcset="https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg 577w, https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=150 150w, https://therailhousegrill.files.wordpress.com/2018/07/bwcuchetti1.jpeg?w=300 300w" width="577"/> </div>
</div>
<!-- BEGIN .outline -->
<div class="outline">
<!-- BEGIN .information -->
<div class="information">
<p>ALL MUSIC POSTPONED UNTIL FURTHER NOTICE DUE TO COVID RELATED RESTRICTIONS ENFORCED BY THE STATE OF MN ON BARS &amp; RESTAURANTS</p>
<!-- END .information -->
</div>
<!-- END .outline -->
</div>
<!-- END .padded -->
</div>
<!-- END .content -->
</div>
</div>
<div class="holder third last">
<!-- BEGIN .content -->
<div class="content shadow radius-full">
<!-- BEGIN .padded -->
<div class="padded">
<div class="home-featured-banner" style="background-image:url(https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=640);">
<div class="flex-wrapper">
<div class="banner-title-wrapper">
<a href="https://therailhousegrill.com/hours/">
<h1 class="headline img-headline">Hours</h1>
</a>
</div>
<img alt="" class="attachment-restaurant-featured-large size-restaurant-featured-large wp-post-image" data-attachment-id="148" data-comments-opened="0" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"1"}' data-image-title="parmwalleye – Copy" data-large-file="https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=894" data-medium-file="https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=300" data-orig-file="https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg" data-orig-size="894,638" data-permalink="https://therailhousegrill.com/hours/parmwalleye-copy/" height="638" loading="lazy" sizes="(max-width: 894px) 100vw, 894px" src="https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=894" srcset="https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg 894w, https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=150 150w, https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=300 300w, https://therailhousegrill.files.wordpress.com/2018/07/parmwalleye-copy.jpg?w=768 768w" width="894"/> </div>
</div>
<!-- BEGIN .outline -->
<div class="outline">
<!-- BEGIN .information -->
<div class="information">
<p>NEW HOURS</p>
<p>RAILHOUSE:<br/>
Mon 4-7pm<br/>
Tues-Thu 11am-7pm<br/>
Fri-Sat 11am-8pm<br/>
Sun 11am-7pm<br/>
TAKEOUT &amp; CURBSIDE</p>
<p>NOW OFFERING DELIVERY FRI, SAT &amp; SUN</p>
<p>651-345-5762</p>
<p>COFFEE DEPOT:<br/>
Mon 6am-Noon<br/>
Tue-Thu 6am-7pm<br/>
Fri-Sat 6am-8pm<br/>
Sun 7am-7pm</p>
<!-- END .information -->
</div>
<!-- END .outline -->
</div>
<!-- END .padded -->
</div>
<!-- END .content -->
</div>
</div>
<!-- END .content -->
</div>
<!-- BEGIN .content -->
<div class="content featured-bottom shadow radius-full">
<div class="feature-img post-banner" style="background-image: url(https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=816);">
<div class="vertical-center">
<h2 class="headline">Location</h2>
<h4 class="excerpt"></h4><p><span style="background-color:rgba(0,0,0,0.6);padding-left:5px;padding-right:5px;padding-bottom:2px;">800 West Lyon Avenue, Lake City MN</span><br/><span style="background-color:rgba(0,0,0,0.6);padding-left:5px;padding-right:5px;padding-bottom:2px;margin-top:2px;"><a href="tel:651-345-5762" style="color:white;">651-345-5762</a></span></p>
</div>
<img alt="" class="attachment-restaurant-featured-medium size-restaurant-featured-medium wp-post-image" data-attachment-id="51" data-comments-opened="1" data-image-description="" data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}' data-image-title="Lake_City_140-816×282" data-large-file="https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=816" data-medium-file="https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=300" data-orig-file="https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg" data-orig-size="816,282" data-permalink="https://therailhousegrill.com/hours/lake_city_140-816x282/" height="282" loading="lazy" sizes="(max-width: 816px) 100vw, 816px" src="https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=816" srcset="https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg 816w, https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=150 150w, https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=300 300w, https://therailhousegrill.files.wordpress.com/2018/07/lake_city_140-816x282.jpg?w=768 768w" width="816"/> </div>
<!-- END .content -->
</div>
<!-- END .row -->
</div>
<!-- END .homepage -->
</div>
<!-- END .container -->
</div>
<!-- BEGIN .footer -->
<div class="footer">
<!-- BEGIN .content -->
<div class="content shadow radius-top">
<!-- BEGIN .padded -->
<div class="padded">
<!-- BEGIN .outline -->
<div class="outline">
<!-- BEGIN .row -->
<div class="row">
<!-- BEGIN .footer-information -->
<div class="footer-information">
<div class="align-left">
<p><a href="https://wordpress.com/?ref=footer_custom_powered" rel="nofollow">Website Powered by WordPress.com</a>.
							
							</p>
</div>
<!-- END .footer-information -->
</div>
<!-- END .row -->
</div>
<!-- END .outline -->
</div>
<!-- END .padded -->
</div>
<!-- END .content -->
</div>
<!-- END .footer -->
</div>
<!-- END #wrap -->
</div>
<!-- -->
<script id="grofiles-cards-js" src="//0.gravatar.com/js/gprofiles.js?ver=202102y"></script>
<script id="wpgroho-js-extra">
var WPGroHo = {"my_hash":""};
</script>
<script src="https://s1.wp.com/wp-content/mu-plugins/gravatar-hovercards/wpgroho.js?m=1610363240h" type="text/javascript"></script>
<script>
		// Initialize and attach hovercards to all gravatars
		( function() {
			function init() {
				if ( typeof Gravatar === 'undefined' ) {
					return;
				}

				if ( typeof Gravatar.init !== 'function' ) {
					return;
				}

				Gravatar.profile_cb = function ( hash, id ) {
					WPGroHo.syncProfileData( hash, id );
				};

				Gravatar.my_hash = WPGroHo.my_hash;
				Gravatar.init( 'body', '#wp-admin-bar-my-account' );
			}

			if ( document.readyState !== 'loading' ) {
				init();
			} else {
				document.addEventListener( 'DOMContentLoaded', init );
			}
		} )();
	</script>
<div style="display:none">
</div>
<script>
window.addEventListener( "load", function( event ) {
	var link = document.createElement( "link" );
	link.href = "https://s0.wp.com/wp-content/mu-plugins/actionbar/actionbar.css?v=20201002";
	link.type = "text/css";
	link.rel = "stylesheet";
	document.head.appendChild( link );

	var script = document.createElement( "script" );
	script.src = "https://s0.wp.com/wp-content/mu-plugins/actionbar/actionbar.js?v=20201002";
	script.defer = true;
	document.body.appendChild( script );
} );
</script>
<script src="https://s0.wp.com/_static/??-eJydj8sOAiEMRX9IBh9xjAvj2s9AqDMgBWxhHP9eRmNijG7cNTn3tLfymoSOIUPI0rE0MFgNaWwcz2RFNmhfDPDELKoO2EdlwDRow7cMKo6Bbr9wHwegw+Pae+RVIPeANZgI0BaUBJxVIfWs5i4F6mZdOEf8xzx5GNlbA/RpYxHJl84GlsrUXuKoaHolA9VJZFL6zFXa427RzlebZbtdt+4ORY91qA==" type="text/javascript"></script>
<script type="text/javascript">
// <![CDATA[
(function() {
try{
  if ( window.external &&'msIsSiteMode' in window.external) {
    if (window.external.msIsSiteMode()) {
      var jl = document.createElement('script');
      jl.type='text/javascript';
      jl.async=true;
      jl.src='/wp-content/plugins/ie-sitemode/custom-jumplist.php';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(jl, s);
    }
  }
}catch(e){}
})();
// ]]>
</script><script defer="" src="//stats.wp.com/w.js?61"></script> <script type="text/javascript">
_tkq = window._tkq || [];
_stq = window._stq || [];
_tkq.push(['storeContext', {'blog_id':'149458823','blog_tz':'0','user_lang':'en','blog_lang':'en','user_id':'0'}]);
_stq.push(['view', {'blog':'149458823','v':'wpcom','tz':'0','user_id':'0','post':'4','subd':'therailhousegrill'}]);
_stq.push(['extra', {'crypt':'UE40eW5QN0p8M2Y/RE1TaVhzUzFMbjdWNHpwZGhTayxPSUFCMGNrd29+Smw0TDhnZmRTK0hlRi9QSGh6bi9GXVhBJWIlZlR5U1JMLU8/MkNtblkvY1dMdl1vL2NfX3g5bm1DbH5BLGw3dGdoRnZSdDIwUHJ3PVFibjRFcldJcXd1VUdjSGhfREtjRmR4Lyx4bV9ycltCZnAvZitNdCU9TmRMYWw3TC1pb0JIcHdOfkNBOSZoeXBnaUIrcHVSWUlhbzVSTUlEW0E9NkR3OHNjeCt5YmtialBvZi1jVn4wLElKSUssSnF3Pz1qOHdTbU95NlNhYUUlRUNKZHYlRWlZaGFNcWFkOC1SV2tQRG18SjRnRTI5'}]);
_stq.push([ 'clickTrackerInit', '149458823', '4' ]);
	</script>
<noscript><img alt="" src="https://pixel.wp.com/b.gif?v=noscript" style="height:1px;width:1px;overflow:hidden;position:absolute;bottom:1px;"/></noscript>
<script>
if ( 'object' === typeof wpcom_mobile_user_agent_info ) {

	wpcom_mobile_user_agent_info.init();
	var mobileStatsQueryString = "";
	
	if( false !== wpcom_mobile_user_agent_info.matchedPlatformName )
		mobileStatsQueryString += "&x_" + 'mobile_platforms' + '=' + wpcom_mobile_user_agent_info.matchedPlatformName;
	
	if( false !== wpcom_mobile_user_agent_info.matchedUserAgentName )
		mobileStatsQueryString += "&x_" + 'mobile_devices' + '=' + wpcom_mobile_user_agent_info.matchedUserAgentName;
	
	if( wpcom_mobile_user_agent_info.isIPad() )
		mobileStatsQueryString += "&x_" + 'ipad_views' + '=' + 'views';

	if( "" != mobileStatsQueryString ) {
		new Image().src = document.location.protocol + '//pixel.wp.com/g.gif?v=wpcom-no-pv' + mobileStatsQueryString + '&baba=' + Math.random();
	}
	
}
</script>
</body>
</html>
