<html><body><p>ï»¿<!DOCTYPE html>

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<!--<![endif]-->
</p>
<meta charset="utf-8"/>
<title>Actuarial Education Company</title>
<meta content="" name="description"/>
<!-- Mobile Meta -->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- Favicon -->
<link href="images/favicon.ico" rel="shortcut icon"/>
<!-- Web Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet" type="text/css"/>
<!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet"/>
<!-- Font Awesome CSS -->
<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet"/>
<!-- Fontello CSS -->
<link href="fonts/fontello/css/fontello.css" rel="stylesheet"/>
<!-- Plugins 
   		<link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
        -->
<link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet"/>
<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet"/>
<link href="css/animations.css" rel="stylesheet"/>
<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet"/>
<!-- core CSS file -->
<link href="css/style.css" rel="stylesheet"/>
<!-- Color Scheme (In order to change the color scheme, replace the red.css with the color scheme that you prefer)-->
<link href="css/skins/purple.css" rel="stylesheet"/>
<!-- Custom css -->
<link href="css/custom.css" rel="stylesheet"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<!-- body classes: 
			"boxed": boxed layout mode e.g. <body class="boxed">
			"pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> 
	-->
<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>
<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">
<!-- header-top start (Add "dark" class to .header-top in order to enable dark header-top e.g <div class="header-top dark">) -->
<!-- ================ -->
<div class="header-top dark">
<div class="container">
<div class="row">
<div class="col-xs-6 col-sm-4 col-md-3">
<!-- header-top-first start -->
<!-- ================ -->
<div class="header-top-first clearfix">
<ul class="social-links clearfix">
<li><a aria-label="Facebook" href="https://www.acted.co.uk/general/socialnet.fwx?link=1" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a aria-label="Twitter" href="https://www.acted.co.uk/general/socialnet.fwx?link=2" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a aria-label="LinkedIn" href="https://www.acted.co.uk/general/socialnet.fwx?link=3" target="_blank"><i class="fa fa-linkedin"></i></a></li>
<li><a aria-label="YouTube" href="https://www.youtube.com/user/ActuaryEducation" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
</ul>
</div>
</div>
<!-- header-top-first end -->
<div class="col-xs-6 col-sm-8 col-md-9">
<!-- header-top-second start -->
<!-- ================ -->
<div class="clearfix" id="header-top-second">
<ul class="list-inline pull-right hidden-xs">
<li><a href="site_map.html" target="_blank">Site Map</a></li>
<li><a href="http://www.bppacted.com" target="_blank">CAA</a></li>
<li><a href="https://www.acted.co.uk/forums" target="_blank"><i class="fa fa-user pr-5 pl-10"></i>Discussion Forums</a></li>
<li><a href="https://www.acted.co.uk/estore" target="_blank"><i class="fa fa-shopping-cart pr-5 pl-10"></i>Buy Now</a></li>
</ul>
</div>
<div class="clearfix" id="header-top-second">
<ul class="list-inline pull-right hidden-lg hidden-md hidden-sm">
<li><a href="http://www.bppacted.com" target="_blank">CAA</a></li>
<li><a href="http://www.bpptraining.com" target="_blank">SoA</a></li>
<li><a href="https://www.acted.co.uk/forums" target="_blank"><i class="fa fa-user pr-5"></i></a></li>
<li><a href="https://www.acted.co.uk/estore" target="_blank"><i class="fa fa-shopping-cart"></i> </a></li>
</ul>
</div>
<!-- header-top-second end -->
</div>
</div>
</div>
</div>
<!-- header-top end -->
<!-- header start classes:
				fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
				 dark: dark header version e.g. <header class="header dark clearfix">
			================ -->
<header class="header clearfix">
<div class="container">
<div class="row row-mist">
<div class="hidden-xs hidden-sm col-md-3 space-top ">
<!-- header-left start -->
<!-- ================ -->
<div class="header-left clearfix">
<!-- logo -->
<div class="logo space-left10 space-bottom">
<a href="index.html"><img alt="BPP Acted" id="logo" src="images/actednewlogo.png"/></a>
</div>
<!-- name-and-slogan -->
<div class="site-slogan">
<!-- on Behalf of Insititue and Faculty of Actuaries-->
</div>
</div>
<!-- header-left end -->
</div>
<div class="col-xs-11 col-md-9 col-sm-12">
<div class="row">
<div class="col-md-12 space-top5 clearfix vertical">
<img class=" pull-right" src="images/acted.jpg"/>
</div>
</div>
</div>
</div>
<div class="row gray_bg">
<div class="col-md-12">
<!-- header-right start -->
<!-- ================ -->
<div class="row header-right">
<!-- main-navigation start -->
<!-- ================ -->
<div class="main-navigation animated">
<!-- navbar start -->
<!-- ================ -->
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid">
<!-- Toggle get grouped for better mobile display -->
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="navbar-collapse-1">
<ul class="nav navbar-nav navbar-right">
<li class="active">
<a class="dropdown-toggle" href="index.html">Home</a>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Products</a>
<ul class="dropdown-menu">
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Core Study Materials</a>
<ul class="dropdown-menu">
<li><a href="paper.html" target="">Core Study Materials Home Page</a></li>
<li><a href="paper_cmp.html" target="">Combined Materials Pack</a></li>
<li><a href="paper_course_notes.html" target="">Course Notes</a></li>
<li><a href="paper_core_reading.html" target="">Core Reading</a></li>
<li><a href="paper_assignments.html" target="">Assignments</a></li>
<li><a href="paper_study_guide.html" target="">Study Guide</a></li>
<li><a href="paper_cmp_upgrade.html" target="">CMP Upgrade</a></li>
<li><a href="paper_corrections.html" target="">Corrections</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Revision Materials</a>
<ul class="dropdown-menu">
<li>
<a href="paper_revision_products.html">Revision Materials Home Page</a>
</li>
<li>
<a href="paper_mock_exams.html">Mock Exams</a>
</li>
<li>
<a href="paper_aset.html">ASET</a>
</li>
<li>
<a href="paper_flashcards.html">Flashcards</a>
</li>
<li>
<a href="paper_revision_notes.html">Revision Notes</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Specialist Materials</a>
<ul class="dropdown-menu">
<li>
<a href="paper_pure_maths_and_statistics_for_actuarial_studies.html">Pure Maths and Statistics for Actuarial Studies</a>
</li>
<li>
<a href="paper_P0.html">Practice Module Exams</a>
</li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Marking</a>
<ul class="dropdown-menu">
<li><a href="marking.html">Marking Home Page</a></li>
<li><a href="marking_series.html">Series Marking</a></li>
<li><a href="marking_vouchers.html">Marking Vouchers</a></li>
<li><a href="marking_mock.html">Mock Exam Marking</a></li>
<li><a href="marking_submitting.html">How to Submit</a></li>
<li><a href="marking_assignment_deadlines.html">Deadlines</a></li>
<li><a href="marking_distribution_of_marks.html">Marking Distributions</a></li>
<li><a href="marking_coversheets.html">Coversheets</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Tutorials</a>
<ul class="dropdown-menu">
<li><a href="tutorials.html">Tutorials Home Page</a></li>
<li><a href="tutorials_tuition_bulletin.html">Tuition Bulletin</a></li>
<li><a href="online_tutorials.html">Online Learning</a></li>
<li><a href="tutorials_inhouse.html">In-house Tutorials</a></li>
<li><a href="induction_courses.html">Induction Courses</a></li>
<li><a href="tutorials_maps.html">Tutorial Venues</a></li>
</ul>
</li>
</ul>
</li>
<li class="dropdown mega-menu">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Subjects</a>
<ul class="dropdown-menu">
<li>
<div class="row">
<h4>List of Subjects</h4>
<div class="col-sm-3">
<div class="divider"></div>
<ul class="menu">
<li><a href="help_and_advice_CM1.html"><i class="icon-right-open"></i>CM1</a></li>
<li><a href="help_and_advice_CM2.html"><i class="icon-right-open"></i>CM2</a></li>
<li><a href="help_and_advice_CS1.html"><i class="icon-right-open"></i>CS1</a></li>
<li><a href="help_and_advice_CS2.html"><i class="icon-right-open"></i>CS2</a></li>
<li><a href="help_and_advice_CB1.html"><i class="icon-right-open"></i>CB1</a></li>
<li><a href="help_and_advice_CB2.html"><i class="icon-right-open"></i>CB2</a></li>
</ul>
</div>
<div class="col-sm-3">
<div class="divider"></div>
<ul class="menu">
<li><a href="help_and_advice_CP1.html"><i class="icon-right-open"></i>CP1</a></li>
<li><a href="help_and_advice_CP2.html"><i class="icon-right-open"></i>CP2</a></li>
<li><a href="help_and_advice_CP3.html"><i class="icon-right-open"></i>CP3</a></li>
</ul>
</div>
<div class="col-sm-3">
<div class="divider"></div>
<ul class="menu">
<li><a href="help_and_advice_SP1.html"><i class="icon-right-open"></i>SP1</a></li>
<li><a href="help_and_advice_SP2.html"><i class="icon-right-open"></i>SP2</a></li>
<li><a href="help_and_advice_SP4.html"><i class="icon-right-open"></i>SP4</a></li>
<li><a href="help_and_advice_SP5.html"><i class="icon-right-open"></i>SP5</a></li>
<li><a href="help_and_advice_SP6.html"><i class="icon-right-open"></i>SP6</a></li>
<li><a href="help_and_advice_SP7.html"><i class="icon-right-open"></i>SP7</a></li>
<li><a href="help_and_advice_SP8.html"><i class="icon-right-open"></i>SP8</a></li>
<li><a href="help_and_advice_SP9.html"><i class="icon-right-open"></i>SP9</a></li>
</ul>
</div>
<div class="col-sm-3">
<div class="divider"></div>
<ul class="menu">
<li><a href="help_and_advice_SA1.html"><i class="icon-right-open"></i>SA1</a></li>
<li><a href="help_and_advice_SA2.html"><i class="icon-right-open"></i>SA2</a></li>
<li><a href="help_and_advice_SA3.html"><i class="icon-right-open"></i>SA3</a></li>
<li><a href="help_and_advice_SA4.html"><i class="icon-right-open"></i>SA4</a></li>
<li><a href="help_and_advice_SA7.html"><i class="icon-right-open"></i>SA7</a></li>
<li><a href="paper_P0.html"><i class="icon-right-open"></i>P0</a></li>
</ul>
</div>
</div>
</li></ul>
</li>
<!-- mega-menu start -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Prices</a>
<ul class="dropdown-menu">
<li><a href="paper_prices.html">Study Materials</a></li>
<li><a href="marking_prices.html">Marking</a></li>
<li><a href="tutorials_prices.html">Tutorials</a></li>
<li><a href="terms_conditions.html">Refund Policy</a></li>
<li><a href="ordering_reduced_rate_material.html">Reduced-rate Prices</a></li>
</ul>
</li>
<!-- mega-menu end -->
<!-- mega-menu start -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">New Students</a>
<ul class="dropdown-menu">
<li><a href="help_and_advice_new_students.html">New Students Home Page</a></li>
<li><a href="help_and_advice_placing_first_order.html">Placing First Order</a></li>
<li><a href="help_and_advice_exam_entries.html">Exam Entries</a></li>
<li><a href="help_and_advice_studying_non-members.html">Sitting the CS1 or CM1 exam as a non-member</a></li>
<li><a href="help_and_advice_subject_list.html">Subject List</a></li>
<li><a href="news_product_guide.html">Product Guide</a></li>
<li><a href="help_and_advice_new_students.html#help">Help with maths and statistics</a></li>
<li><a href="Docs/Devices and Products.pdf" target="_blank">Devices and Products</a></li>
<li><a href="help_and_advice_ACET.html" target="_blank">ACET</a></li>
</ul>
</li>
<!-- mega-menu end -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Help and Advice</a>
<ul class="dropdown-menu">
<li>
<a href="help_and_advice_common_queries.html">Common Questions</a>
</li>
<li><a href="paper_corrections.html">Corrections</a></li>
<li><a href="help_and_advice_disabilities.html">Learning Support</a>
</li>
<li><a href="https://www.acted.co.uk/forums" target="_blank">Discussion Forums</a></li>
<li><a href="electronic_ebook.html">Ebook Instructions</a></li>
<li><a href="news_product_guide.html">Product Guide</a></li>
<li><a href="help_and_advice_retaking_a_subject.html">Retakers</a></li>
<li><a href="help_and_advice_study_plans.html">Study Plans</a></li>
<li><a href="help_and_advice_waiting_on_results.html">Waiting for Results</a></li>
<li><a href="employers.html">Employers</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Overseas Students</a>
<ul class="dropdown-menu">
<li><a href="overseas_materials.html">Study Materials</a></li>
<li><a href="marking_submitting.html">Marking</a></li>
<li><a href="tutorials_overseas.html">Tutorials</a></li>
<li><a href="ordering_reduced_rate_material.html">Reduced-rate Prices</a></li>
<li><a href="india.html">Students in India</a></li>
<li><a href="south_africa.html">Students in South Africa</a></li>
</ul>
</li>
<!-- mega-menu start 
													<li class="dropdown">
														<a href="contacting_us.html">Contact us</a>														
													</li>
                                                    -->
<!-- mega-menu end -->
<!--
													<li class="">
														<a href="blog-right-sidebar.html" class="dropdown-toggle" data-toggle="dropdown">Contact us</a>
													</li>
                                                    -->
</ul>
</div>
</div>
</nav>
<!-- navbar end -->
</div>
<!-- main-navigation end -->
</div>
<!-- header-right end -->
</div>
</div>
</div>
</header>
<!-- header end -->
<!-- banner start -->
<!-- ================ -->
<div class="banner">
<!-- slideshow start -->
<!-- ================ -->
<div class="slideshow">
<!-- slider revolution start -->
<!-- ================ -->
<div class="slider-banner-container">
<div class="slider-banner">
<ul>
<!-- slide 1 start -->
<li data-masterspeed="300" data-slotamount="7" data-title="Why Acted" data-transition="sfl">
<!-- main image -->
<img alt="slidebg1" data-bgposition="left top" data-bgrepeat="no-repeat" src="images/mainbg-white2.jpg"/>
<div class="tp-caption large sfr " data-captionhidden="off" data-endspeed="300" data-hoffset="-30" data-speed="600" data-x="right" data-y="30">Why study with ActEd?
								</div>
<!-- LAYER NR. 2 -->
<div class="tp-caption dark_gray_bg sfl medium tp-resizeme" data-endspeed="300" data-hoffset="-30" data-speed="300" data-start="1600" data-x="right" data-y="120"><i class="icon-check"></i>
</div>
<!-- LAYER NR. 3 -->
<div class="tp-caption light_gray_bg sfb medium tp-resizeme" data-endspeed="300" data-hoffset="-80" data-speed="300" data-start="1000" data-x="right" data-y="120">Outstanding Education
								</div>
<!-- LAYER NR. 4 -->
<div class="tp-caption dark_gray_bg sfl medium tp-resizeme" data-endspeed="300" data-hoffset="-30" data-speed="300" data-start="1800" data-x="right" data-y="170"><i class="icon-check"></i>
</div>
<!-- LAYER NR. 5 -->
<div class="tp-caption light_gray_bg sfb medium tp-resizeme" data-endspeed="300" data-hoffset="-80" data-speed="300" data-start="1200" data-x="right" data-y="170">Flexible Approach
								</div>
<!-- LAYER NR. 6 -->
<div class="tp-caption dark_gray_bg sfl medium tp-resizeme" data-endspeed="300" data-hoffset="-30" data-speed="300" data-start="2000" data-x="right" data-y="220"><i class="icon-check"></i>
</div>
<!-- LAYER NR. 7 -->
<div class="tp-caption light_gray_bg sfb medium tp-resizeme" data-endspeed="300" data-hoffset="-80" data-speed="300" data-start="1400" data-x="right" data-y="220">Personalised Learning
								</div>
<!-- LAYER NR. 8 -->
<div class="tp-caption dark_gray_bg sfl medium tp-resizeme" data-endspeed="300" data-hoffset="-30" data-speed="300" data-start="2200" data-x="right" data-y="270"><i class="icon-check"></i>
</div>
<!-- LAYER NR. 10 -->
<div class="tp-caption light_gray_bg sfb medium tp-resizeme" data-endspeed="300" data-hoffset="-80" data-speed="300" data-start="1800" data-x="right" data-y="270">Fantastic Support
								</div>
<!-- LAYER NR. 8 -->
</li>
<!-- slide 1 end -->
<!-- slide 2 start -->
<!-- main image -->
<!-- slide 2 end -->
<!-- slide 3 start -->
<!-- main image -->
<!-- slide 3 end -->
</ul>
<div class="tp-bannertimer tp-bottom"></div>
</div>
</div>
<!-- slider revolution end -->
</div>
<!-- slideshow end -->
</div>
<!-- banner end -->
<!-- page-top start-->
<!-- ================ -->
<!-- 
			<div class="page-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="row grid-space-10">
								<div class="col-sm-3">
									<div class="box-style-2">
										<div class="icon-container default-bg">
											<i class="fa fa-pencil-square-o"></i>
										</div>
										<div class="body">
											<h2>2015 Brochure</h2>
											<p>Download our 2015 Brochure t oday. We need some more info here for this.</p>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="box-style-2">
										<div class="icon-container default-bg">
											<i class="fa fa-laptop"></i>
										</div>
										<div class="body">
											<h2>Online Tutorials</h2>
											<p>Require short description about online tutorials. please provide more info here.</p>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="box-style-2">
										<div class="icon-container default-bg">
											<i class="fa icon-book-open"></i>
										</div>
										<div class="body">
										  <h2>Tuition Bulletin</h2>
											<p>Require short description about  tutorial bulletin section. please provide more info here.</p>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="box-style-2">
										<div class="icon-container default-bg">
											<i class="fa fa-shopping-cart"></i>
										</div>
										<div class="body">
											<h2>Buy now</h2>
											<p>Require short description about  acted estore. please provide more info here..</p>
										</div>
									</div>
								</div>                                
							</div>
						</div>
					</div>
				</div>
			</div>
			-->
<!-- page-top end -->
<!-- main-container start -->
<!-- ================ -->
<div class="section light-translucent-bg whyacted-bg space-10 ">
<div class="container">
<div class="text-center">
<div class="row">
<div class="col-sm-9 col-md-9">
<h3 class="nopadding title ">Wherever you are in the world, we can help you <br/>realise your career ambitions.</h3><br/>
<div class="text-center">ActEd has an exceptional heritage of success stretching over 25 years, and now helps over 5,000 students pass their exams each year.   Our broad range of study products can help you at each stage of your exam preparation.</div>
</div>
<div class="col-sm-3 col-md-3">
<div class="text-center">
<p>Not sure where to start? Try our </p>
<a class="btn btn-default" href="help_and_advice_new_students.html">Help And Advice</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="section clearfix gray_bg space-10 mb-20">
<div class="container">
<div class="row col-md-12 text-left">
<h4 class="nopadding">LATEST NEWS</h4>
<div class="small">Updated: 5 January 2021</div>
<br/>
<p><a href="news_update.html" target="_blank">Impact of Covid-19 on ActEd's services Update</a> - <em>updated on 23/12/2020</em></p>
<p><a href="Docs/2021/ActEd Student Brochure 2021.pdf" target="_blank"> Student Brochure 2021 Exams</a> - <em>updated on 21/09/2020</em></p>
<p><a href="tutorials_tuition_bulletin.html" target="_blank"> Tuition Bulletin 2021 Exams</a> - <em>updated on 21/12/2020</em></p>
<p><a href="help_and_advice_new_students.html#webinar" target="_blank">Recording for "New Students Webinar" 16 October 2020</a> - <em>updated on 19/10/2020</em></p>
<p><a href="online_classroom.html" target="_blank"> New Mini-Online Classroom in Subject SA1</a></p>
<p><a href="paper_P0.html" target="_blank">Practice Module Exams</a> - <em>updated on 01/12/2020</em></p>
<p> <a href="news_product_guide.html" target="_blank">Guide to choosing material</a></p>
</div>
</div>
</div>
<!-- section start -->
<!-- ================ -->
<div class="main clearfix ">
<div class="container">
<div class="row">
<div class="col-md-12">
</div>
</div>
</div>
</div>
<!-- section end -->
<div class="section clearfix nopadding gray-bg">
<div class="container">
<div class="call-to-action &gt; &lt;div class=" row="">
<div class="col-md-4 space-top20">
<img alt="Lion Background" class="img-responsive" src="images/lionbg.png"/>
</div>
<div class="col-md-8 space-top10">
<h2 class="nopadding title">Certified Actuarial Analyst</h2><br/>
<p class="text-center">We are specialist actuarial educators and we are producing a wide range of affordable, online study resources to prepare students studying for the Certified Actuarial Analyst (CAA) exams offered by CAA Global.</p>
<a class="btn btn-default" href="http://www.bppacted.com">Find Out More</a>
</div>
</div>
</div>
</div>
<!-- footer start (Add "light" class to #footer in order to enable light footer) -->
<!-- ================ -->
<footer id="footer clearfix">
<!-- .footer start -->
<!-- ================ -->
<div class="footer space-10">
<div class="container space-top10">
<div class="row">
<div class="col-sm-3 col-md-3 ">
<div class="footer-content">
<p class="gray-txt">Further information</p>
<nav>
<ul class="nav nav-pills nav-stacked">
<li><a href="markers.html"> For Markers</a></li>
<li><a href="employers.html"> For Employers</a></li>
<li><a href="useful_links.html"> Useful Links </a></li>
<li><a href="key_dates.html"> Key Dates</a></li>
</ul>
</nav>
</div>
</div>
<div class="col-sm-3 col-md-3 ">
<div class="footer-content">
<p class="gray-txt">Terms and Conditions</p>
<nav>
<ul class="nav nav-pills nav-stacked">
<li><a href="http://www.bpp.com/professional-education/terms/cookie-policy"> Cookie Use</a></li>
<li><a href="terms_conditions.html"> General Terms of Use</a></li>
<li><a href="complaints.html">Complaints</a></li>
</ul>
</nav>
</div>
</div>
<div class="col-sm-3 col-md-3 ">
<div class="footer-content">
<p class="gray-txt">About Us</p>
<nav>
<ul class="nav nav-pills nav-stacked">
<li><a href="about_acted.html">Meet the Team</a></li>
<li><a href="https://s3-eu-west-1.amazonaws.com/bppassets/public/assets/pdf/policies/bpp_code_of_ethics.pdf"> Codes of Business Ethics</a></li>
<li><a href="https://www.jobtrain.co.uk/bpp/"> Jobs at BPP </a></li><li><a href="contacting_us.html"> Contact us </a></li>
</ul>
</nav>
</div>
</div>
<div class="col-sm-3 col-md-3 ">
<div class="footer-content">
<p class="gray-txt">Interact</p>
<nav>
<ul class="nav nav-pills nav-stacked">
<li><a href="https://www.acted.co.uk/forums/">Discussion Forums</a></li>
<li><a href="https://www.acted.co.uk/general/socialnet.fwx?link=1"> Facebook</a></li>
<li><a href="https://www.acted.co.uk/general/socialnet.fwx?link=2"> Twitter</a></li>
<li><a href="https://www.acted.co.uk/general/socialnet.fwx?link=3"> LinkedIn</a></li>
</ul>
</nav>
</div>
</div>
</div>
<div class="space-bottom hidden-lg hidden-xs"></div>
</div>
</div>
<!-- .footer end -->
<!-- .subfooter start -->
<!-- ================ -->
<div class="subfooter space-10">
<div class="container">
<div class="row">
<div class="col-md-12">
<p class="gray-txt">Copyright Â© 2019 Actuarial Education Company Ltd â Part of the BPP Professional Education Group â ActEd@bpp.com.</p>
</div>
</div>
</div>
</div>
<!-- .subfooter end -->
</footer>
<!-- footer end -->
</div>
<!-- page-wrapper end -->
<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
<!-- Jquery and Bootstap core js files -->
<script src="plugins/jquery.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Modernizr javascript -->
<script src="plugins/modernizr.js" type="text/javascript"></script>
<!-- jQuery REVOLUTION Slider  -->
<script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<!-- Isotope javascript -->
<script src="plugins/isotope/isotope.pkgd.min.js" type="text/javascript"></script>
<!-- Owl carousel javascript -->
<script src="plugins/owl-carousel/owl.carousel.js" type="text/javascript"></script>
<!-- Magnific Popup javascript 
		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		-->
<!-- Appear javascript 
		<script type="text/javascript" src="plugins/jquery.appear.js"></script>
		-->
<!-- Count To javascript 
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		-->
<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>
<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>
<!-- Initialization of Plugins -->
<script src="js/template.js" type="text/javascript"></script>
<!-- Custom Scripts -->
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['_setAccount', 'UA-35827653-1']);
			  _gaq.push(['_trackPageview']);
			
			  (function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
	   </script>
</body></html>