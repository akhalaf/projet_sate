<!DOCTYPE html>
<html class="weatherstack index">
<head>
<meta charset="utf-8"/>
<title>weatherstack - Real-Time World Weather REST API</title>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-62924033-23"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
      
        gtag('config', 'UA-62924033-23');
      </script>
<script>
    window.stripePublishableKey = 'pk_live_SbOhR0mPK55dMckhtbSufjdM';
</script>
<meta content="Get current weather information, historical data and forecasts using weatherstack - a free world weather REST API supporting millions of global locations." name="description"/>
<meta content="weather api, weather data php, world weather api, current weather api, weather forecasts api" name="keywords"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
<link href="https://weatherstack.com/" rel="canonical"/>
<link href="/site_images/weatherstack_shortcut_icon.ico" rel="shortcut icon"/>
<link href="/site_images/weatherstack_icon.png" rel="apple-touch-icon"/>
<script src="js/shared/run_prettify.js"></script>
<link href="/css/prettify.css" rel="stylesheet" type="text/css"/>
<link href="/site_css/style.css" media="screen" rel="stylesheet"/>
<script src="/site_js/jquery-1.11.3.min.js" type="text/javascript"></script>
</head>
<body class="">
<div>
<nav class="header index">
<div class="container">
<div class="logo">
<a href="/" title="weatherstack Logo"><img alt="weatherstack Logo" draggable="false" src="/site_images/weatherstack_logo_white.png"/></a>
</div>
<span class="mobile_menu_icon" data-header-toggle="true"></span>
<ul>
<li><a href="/product" title="Pricing Plans">Pricing</a></li>
<li><a href="/documentation" title="API Documentation">Documentation</a></li>
<li><a href="/faq" title="Frequently Asked Questions">FAQ</a></li>
<li class="status"><a href="https://status.weatherstack.com/" target="_blank" title="weatherstack System Status">Status</a></li>
<li class="action login"><a href="/login" title="Log in to your account">Log In</a></li>
<li class="action cta"><a href="/product" title="Start using the API">Sign Up Free</a></li>
</ul>
</div>
</nav>
<section class="hero index">
<div class="container">
<div class="hero_main">
<div class="inline">
<h2>Real-Time &amp; Historical<br/>
                        World Weather Data API
                     </h2>
<h4>Retrieve instant, accurate weather information for<br/>any location in the world in lightweight JSON format</h4>
<span class="number_customers">Trusted by 75,000 companies worldwide</span>
<a class="button cta" href="/product" title="Create a free weather data API account">Start using the API</a> <span class="friendly">— It's free!</span>
</div>
<div class="weather_animated loading">
<input name="client_ip" type="hidden" value="2001:250:3c02:749:e444:4db8:6cea:7c56"/>
<div class="location">
<span data-api="location">New York, United States</span>
</div>
<div class="main_left">
<i class="partly_cloudy" data-api="current_icon"></i>
<span data-api="current_main_descr">Partly Cloudy</span>
</div>
<div class="main_right">
<span class="wind" data-api="current_wind_speed">Wind: 6.1 kmph</span>
<span class="precip" data-api="current_precip">Precip: 0.00 mm</span>
<span class="pressure" data-api="current_pressure">Pressure: 1025.0 mb</span>
<span class="temperature" data-api="current_temperature">21.0 °c</span>
</div>
<div class="week" data-api="forecast_week">
<div class="day">
<span class="name">MON</span>
<i class="partly_cloudy"></i>
<span class="temperature">21.0 °c</span>
</div>
<div class="day">
<span class="name">TUE</span>
<i class="full_sun"></i>
<span class="temperature">23.3 °c</span>
</div>
<div class="day">
<span class="name">WED</span>
<i class="rainy"></i>
<span class="temperature">27.8 °c</span>
</div>
<div class="day">
<span class="name">THU</span>
<i class="thunder"></i>
<span class="temperature">31.3 °c</span>
</div>
<div class="day">
<span class="name">FRI</span>
<i class="full_clouds"></i>
<span class="temperature">17.0 °c</span>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="customers">
<div class="container">
<ul>
<li>Happy Customers:</li>
<li class="deloitte"></li>
<li class="microsoft"></li>
<li class="warnerbros"></li>
<li class="wawa"></li>
<li class="schneider"></li>
<li class="ericsson"></li>
</ul>
</div>
</section>
<section class="show">
<div class="container">
<div class="boxes">
<div class="box realtime">
<h5 class="heading">Real-Time, History, Forecast</h5>
<p>Our API covers global weather data across the board — from a multi-year history all the way to live information and accurate weather forecasts.</p>
</div>
<div class="box locations">
<h5 class="heading">Millions of Locations</h5>
<p>Make use of live or hour-by-hour weather data for millions of cities and towns worldwide, supporting a variety methods to look up any location.</p>
</div>
<div class="box uptime">
<h5 class="heading">Rock-Solid Uptime &amp; Speed</h5>
<p>Powered by best-in-class cloud infrastructure, our API delivers data in milliseconds around the clock with an uptime of nearly 100%.</p>
</div>
<div class="box price">
<h5 class="heading">Start Free, Upgrade Later</h5>
<p>Start testing the API immediately by signing up for our Free Plan. You can always upgrade later — no strings attached, no credit card required.</p>
</div>
</div>
</div>
</section>
<section class="main_feature">
<div class="container">
<div class="details">
<h4>Access to Global Weather Data, Developer-friendly</h4>
<p>Get instant access to accurate weather data for any geo-point in the world and enjoy a rich set of capabilities:</p>
<ul>
<li class="realtime">Real-Time Weather API</li>
<li class="historical">Historical Weather API</li>
<li class="forecasts">Weather Forecasts API</li>
<li class="autocomplete">Location Autocomplete</li>
<li class="bulk">Bulk API Endpoint</li>
</ul>
</div>
<img alt="World Weather Map" draggable="false" src="/site_images/world_map_vector.svg"/>
</div>
</section>
<section class="methods">
<div class="container">
<h4>Complete Weather Data Coverage</h4>
<h5>Get years of historical weather data, request real-time weather information or make use of accurate weather forecasts.</h5>
<ul class="buttons">
<li><a class="selected" data-endpoint-select="current" href="javascript:void(0)">Current Weather</a></li>
<li><a data-endpoint-select="history" href="javascript:void(0)">Weather History</a></li>
<li><a data-endpoint-select="forecast" href="javascript:void(0)">Weather Forecast</a></li>
</ul>
<div class="result">
<pre class="prettyprint" data-endpoint="history">{
    "request": {
        "type": "City",
        "query": "New York, United States of America",
        "language": "en",
        "unit": "m"
    },
    "location": {
        "name": "New York",
        "country": "United States of America",
        "region": "New York",
        "lat": "40.714",
        "lon": "-74.006",
        "timezone_id": "America/New_York",
        "localtime": "2019-09-08 09:36",
        "localtime_epoch": 1567935360,
        "utc_offset": "-4.0"
    },
    "current": {
        "observation_time": "01:36 PM",
        "temparature": 18,
        "weather_code": 113,
        "weather_icons": [
            "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
        ],
        "weather_descriptions": [
            "Sunny"
        ],
        "wind_speed": 7,
        "wind_degree": 270,
        "wind_dir": "W",
        "pressure": 1018,
        "precip": 0,
        "humidity": 70,
        "cloudcover": 0,
        "feelslike": 18,
        "uv_index": 5,
        "visibility": 16
    },
    "historical": {
        "2013-07-01": {
            "date": "2013-07-01",
            "date_epoch": 1372636800,
            "astro": {
                "sunrise": "05:29 AM",
                "sunset": "08:31 PM",
                "moonrise": "12:59 AM",
                "moonset": "02:42 PM",
                "moon_phase": "Last Quarter",
                "moon_illumination": 40
            },
            "mintemp": 21,
            "maxtemp": 25,
            "avgtemp": 22,
            "totalsnow": 0,
            "sunhour": 12.5,
            "uv_index": 5
        }
    }
}
</pre>
<pre class="prettyprint" data-endpoint="current" style="display: block;">{
    "request": {
        "type": "City",
        "query": "San Francisco, United States of America",
        "language": "en",
        "unit": "m"
    },
    "location": {
        "name": "San Francisco",
        "country": "United States of America",
        "region": "California",
        "lat": "37.775",
        "lon": "-122.418",
        "timezone_id": "America/Los_Angeles",
        "localtime": "2019-09-03 05:35",
        "localtime_epoch": 1567488900,
        "utc_offset": "-7.0"
    },
    "current": {
        "observation_time": "12:35 PM",
        "temparature": 16,
        "weather_code": 122,
        "weather_icons": [
            "https://assets.weatherstack.com/images/symbol.png"
        ],
        "weather_descriptions": [
            "Overcast"
        ],
    "wind_speed": 17,
    "wind_degree": 260,
    "wind_dir": "W",
    "pressure": 1016,
    "precip": 0,
    "humidity": 87,
    "cloudcover": 100,
    "feelslike": 16,
    "uv_index": 0,
    "visibility": 16
    }
}
</pre>
<pre class="prettyprint" data-endpoint="forecast">{
    "request": {
        "type": "City",
        "query": "Chicago, United States of America",
        "language": "en",
        "unit": "m"
    },
    "location": {
        "name": "Chicago",
        "country": "United States of America",
        "region": "Illinois",
        "lat": "41.850",
        "lon": "-87.650",
        "timezone_id": "America/Chicago",
        "localtime": "2019-09-08 08:39",
        "localtime_epoch": 1567931940,
        "utc_offset": "-5.0"
    },
    "current": {
        "observation_time": "01:39 PM",
        "temparature": 19,
        "weather_code": 122,
        "weather_icons": [
            "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
        ],
        "weather_descriptions": [
            "Overcast"
        ],
        "wind_speed": 17,
        "wind_degree": 100,
        "wind_dir": "E",
        "pressure": 1019,
        "precip": 0,
        "humidity": 73,
        "cloudcover": 100,
        "feelslike": 19,
        "uv_index": 4,
        "visibility": 16
    },
    "forecast": {
        "2019-09-08": {
            "date": "2019-09-08",
            "date_epoch": 1567900800,
            "astro": {
                "sunrise": "06:23 AM",
                "sunset": "07:13 PM",
                "moonrise": "04:25 PM",
                "moonset": "12:58 AM",
                "moon_phase": "First Quarter",
                "moon_illumination": 62
            },
            "mintemp": 10,
            "maxtemp": 18,
            "avgtemp": 16,
            "totalsnow": 0,
            "sunhour": 6.5,
            "uv_index": 4
        }
    }
}
</pre>
</div>
<div class="more_features">
<ul>
<li class="sources">
<span class="heading">Reliable Data Sources</span>
<p>Powered by a strong backbone of data sources, our weather data API comes with the highest level of reliability, consistency and accuracy.</p>
</li>
<li class="speed">
<span class="heading">Lightning-fast Response</span>
<p>Weather data is delivered in lightweight JSON format to ensure a high level of speed and compatibility with any programming language.</p>
</li>
<li class="scalable">
<span class="heading">Scalable Infrastructure</span>
<p>Our REST API is backed by scalable cloud infrastructure built and maintained by apilayer, capable of handling billions of requests per day.</p>
</li>
<li class="location_lookup">
<span class="heading">Flexible Location Lookup</span>
<p>Millions of locations can be looked up by city or region name, ZIP code, IP address, or even using latitude and longitude coordinates.</p>
</li>
<li class="security">
<span class="heading">Bank-Level Security</span>
<p>All data streams sent to and from the weatherstack API are secured using industry-standard 256-bit HTTPS (SSL) encryption. </p>
</li>
<li class="api_docs">
<span class="heading">Extensive API Documentation</span>
<p>An API is only as good as its documentation, which is why a series of interactive code examples in multiple languages are waiting for you.</p>
</li>
</ul>
</div>
</div>
</section>
<section class="grid">
<div class="container">
<h4>Join more than 75,000 companies worldwide using the weatherstack API</h4>
<h5>We are proud of delivering accurate weather data to some of the smartest brands out there.</h5>
<ul>
<li class="daimler"></li>
<li class="deloitte"></li>
<li class="ericsson"></li>
<li class="microsoft"></li>
<li class="schneider"></li>
<li class="warnerbros"></li>
<li class="wawa"></li>
<li class="canda"></li>
<li class="mckinsey"></li>
<li class="rimowa"></li>
</ul>
</div>
</section>
<section class="cta">
<div class="container">
<p><span>Sign up for our Free Plan to get instant access to weather data </span> <a href="/signup" title="Sign up for free API access">Get Free API Access</a></p>
</div>
</section>
<!-- start template -->
<footer>
<div class="container">
<ul>
<li class="heading">General</li>
<li><a href="/about" title="About weatherstack">About</a></li>
<li><a href="/product" title="Pricing Plans">Pricing</a></li>
<li><a href="/faq" title="Frequently Asked Questions">FAQ</a></li>
<li><a href="/contact" title="Contact Sales and Support">Contact</a></li>
<li><a href="/sitemap.php" title="Site Map">Site Map</a></li>
</ul>
<ul>
<li class="heading">Account</li>
<li><a href="/product" title="Get started for free">Sign Up Free</a></li>
<li><a href="/login" title="Login">Log in</a></li>
<li><a href="/forgot" title="Reset Password">Forgot Password</a></li>
<li class="empty"><a> </a></li>
<li class="empty"><a> </a></li>
</ul>
<ul>
<li class="heading">Developer</li>
<li><a href="/documentation" title="API Documentation">Documentation</a></li>
<li><a href="https://github.com/apilayer/weatherstack" target="_blank" title="weatherstack on Github">Github</a></li>
<li class="empty"><a> </a></li>
<li class="empty"><a> </a></li>
<li class="empty"><a> </a></li>
</ul>
<ul>
<li class="heading">Legal</li>
<li><a href="/terms" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
<li><a href="/privacy" title="Privacy Policy">Privacy Policy</a></li>
<li><a href="/cookies" title="Cookie Policy">Cookie Policy</a></li>
<li><a href="/agreement" title="Service Agreement">Service Agreement</a></li>
<li class="empty"><a> </a></li>
</ul>
<div class="right_side">
<img class="logo" draggable="false" src="/site_images/weatherstack_logo_footer.png"/>
<div class="socials">
<a class="twitter" href="https://twitter.com/apilayer" target="_blank" title="apilayer on Twitter"></a>
<a class="facebook" href="https://www.facebook.com/apilayer" target="_blank" title="apilayer on Facebook"></a>
<a class="linkedin" href="https://www.linkedin.com/company/apilayer-ltd" target="_blank" title="apilayer on Linkedin"></a>
<a class="github" href="https://github.com/apilayer/weatherstack" target="_blank" title="weatherstack API on Github"></a>
</div>
<span>© 2021 weatherstack, an <a href="https://apilayer.com" target="_blank" title="apilayer.com">apilayer</a> product. <br/>All rights reserved.</span>
</div>
</div>
</footer>
</div>
<script src="/site_js/scripts.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</body>
</html>
