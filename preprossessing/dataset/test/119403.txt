<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width" name="viewport"/>
<title>Page not found | Amtul Public School</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://amtul.com/feed/" rel="alternate" title="Amtul Public School » Feed" type="application/rss+xml"/>
<link href="https://amtul.com/comments/feed/" rel="alternate" title="Amtul Public School » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/amtul.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://amtul.com/wp-content/themes/blankslate/style.css?ver=4.9.16" id="blankslate-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://amtul.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://amtul.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://amtul.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://amtul.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://amtul.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.9.16" name="generator"/>
<style id="wp-custom-css" type="text/css">
			.elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated {
    padding: 0px !important;
}
.ulcustom>li::before {
    content: "•";
    padding-right: 8px;
    color: black;
    font-size: 20px;
}
.ulcustom>li {
    padding-left: 16px;
}

ol>li::before {
    content: "•";
    padding-right: 8px;
    color: black;
    font-size: 20px;
}
ol>li {
    padding-left: 16px;
}
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #4CAF50;
  color: white;
}
table.custom2 {
    background: #fafafa;
    font-size: 13px;
    text-align: center !important;
}
.logo>a>img {
    height: 90px !important;
	margin: 5px;
}
html {
    margin: 0 !important;
 
}

@media only screen and (min-width:760px){
	.padding-qa {
    padding: 20px 1px 20px 1px; 
}
	
}
img.footer-logo {
    width: 115px !important;
	background:white;
}		</style>
</head>
<!--====== Required meta tags ======-->
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="" name="description"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<!--====== Title ======-->
<title>Amtul Public School</title>
<!--====== Favicon Icon ======-->
<link href="../wp-content/themes/blankslate/images/favicon.png" rel="shortcut icon" type="image/png"/>
<!--====== Slick css ======-->
<link href="../wp-content/themes/blankslate/css/slick.css" rel="stylesheet"/>
<!--====== Animate css ======-->
<link href="../wp-content/themes/blankslate/css/animate.css" rel="stylesheet"/>
<!--====== Nice Select css ======-->
<link href="../wp-content/themes/blankslate/css/nice-select.css" rel="stylesheet"/>
<!--====== Nice Number css ======-->
<link href="../wp-content/themes/blankslate/css/jquery.nice-number.min.css" rel="stylesheet"/>
<!--====== Magnific Popup css ======-->
<link href="../wp-content/themes/blankslate/css/magnific-popup.css" rel="stylesheet"/>
<!--====== Bootstrap css ======-->
<link href="../wp-content/themes/blankslate/css/bootstrap.min.css" rel="stylesheet"/>
<!--====== Fontawesome css ======-->
<link href="../wp-content/themes/blankslate/css/font-awesome.min.css" rel="stylesheet"/>
<!--====== Default css ======-->
<link href="../wp-content/themes/blankslate/css/default.css" rel="stylesheet"/>
<!--====== Style css ======-->
<link href="../wp-content/themes/blankslate/css/style.css" rel="stylesheet"/>
<!--====== Responsive css ======-->
<link href="../wp-content/themes/blankslate/css/responsive.css" rel="stylesheet"/>
<!--====== PRELOADER PART START ======-->
<!--====== PRELOADER PART START ======-->
<!--====== HEADER PART START ======-->
<body><header id="header-part">
<div class="header-top d-none d-lg-block">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="header-contact text-lg-left text-center">
<ul>
<li><img alt="icon" src="../wp-content/themes/blankslate/images/all-icon/map.png"/><span>Arranmore Estate, Ayarpatta Hill, Nainital</span></li>
<li><img alt="icon" src="../wp-content/themes/blankslate/images/all-icon/email.png"/><span><a class="emailwhite" href="mailto:nainital@amtul.com">nainital@amtul.com</a></span></li>
</ul>
</div>
</div>
<div class="col-lg-6">
<div class="header-opening-time text-lg-right text-center">
<p>Opening Hours : Monday to Saturay - 8 Am to 5 Pm</p>
</div>
</div>
</div> <!-- row -->
</div> <!-- container -->
</div> <!-- header top -->
<div class="header-logo-support">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-4">
<div class="logo">
<a href="https://amtul.com/">
<img alt="Logo" src="../wp-content/themes/blankslate/images/logo.png"/>
</a>
</div>
</div>
<div class="col-lg-8 col-md-8 padding-qa">
<div class="support-button float-right d-none d-md-block">
<div class="support float-left">
<div class="icon">
<img alt="icon" src="../wp-content/themes/blankslate/images/all-icon/support.png"/>
</div>
<div class="cont">
<p>Need Help? call us</p>
<span>94569 20608</span>
</div>
</div>
<div class="button float-left">
<a class="main-btn" href="/download/Admission-Form.doc">Admission Open</a>
</div>
</div>
</div>
</div> <!-- row -->
</div> <!-- container -->
</div> <!-- header logo support -->
<main id="content">
<article class="post not-found" id="post-0">
<header class="header">
<h1 class="entry-title">Not Found</h1>
</header>
<div class="entry-content">
<p>Nothing found for the requested page. Try a search instead?</p>
<form action="https://amtul.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form></div>
</article>
</main>
<aside id="sidebar">
</aside> <!--====== FOOTER PART START ======-->
<footer id="footer-part">
<div class="footer-top pt-40 pb-70">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-6">
<div class="footer-about mt-40">
<div class="logo">
<a href="/about"><img alt="Logo" src="../wp-content/themes/blankslate/images/amtul.png" style="
    width: 120px !important;
    height: 120px !important;
"/></a>
</div>
<p>Nestled in the sylvan ambiance of the Kumaon hills, Amtul’s Public School is a co-educational residential school from class I to XII.</p>
</div> <!-- footer about -->
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="footer-link mt-40">
<div class="footer-title pb-25">
<h6>Quick Link</h6>
</div>
<ul>
<li><a href="https://amtul.com/"><i class="fa fa-angle-right"></i>Home</a></li>
<li><a href="https://amtul.com/about-us/"><i class="fa fa-angle-right"></i>About us</a></li>
<li><a href="https://amtul.com/our-classrooms/"><i class="fa fa-angle-right"></i>Our Classrooms

</a></li>
<li><a href="https://amtul.com/procedure-and-rules/"><i class="fa fa-angle-right"></i>Procedure And Rules

</a></li>
<li><a href="https://amtul.com/achievements/"><i class="fa fa-angle-right"></i>Achievements</a></li>
</ul>
</div> <!-- footer link -->
</div>
<div class="col-lg-2 col-md-6 col-sm-6">
<div class="footer-link support mt-40">
<div class="footer-title pb-25">
<h6>Sitemap</h6>
</div>
<ul>
<li><a href="https://amtul.com/achievements/"><i class="fa fa-angle-right"></i>Achievements</a></li>
<li><a href="https://amtul.com/staff-details/"><i class="fa fa-angle-right"></i>Staff Details</a></li>
<li><a href="https://amtul.com/clothing-list/"><i class="fa fa-angle-right"></i>Clothing List

</a></li>
<li><a href="https://amtul.com/text-books-details/"><i class="fa fa-angle-right"></i>Book Details</a></li>
</ul>
</div> <!-- support -->
</div>
<div class="col-lg-3 col-md-6">
<div class="footer-address mt-40">
<div class="footer-title pb-25">
<h6>Contact Us</h6>
</div>
<ul>
<li>
<div class="icon">
<i class="fa fa-home"></i>
</div>
<div class="cont">
<p>Arranmore Estate, Ayarpatta Hill, Nainital, Uttarakhand 263002</p>
</div>
</li>
<li>
<div class="icon">
<i class="fa fa-phone"></i>
</div>
<div class="cont">
<p>05942-235752, 236557</p>
</div>
</li>
<li>
<div class="icon">
<i class="fa fa-envelope-o"></i>
</div>
<div class="cont">
<p><a class="emailwhite" href="mailto:nainital@amtul.com">nainital@amtul.com</a></p>
</div>
</li>
</ul>
</div> <!-- footer address -->
</div>
</div> <!-- row -->
</div> <!-- container -->
</div> <!-- footer top -->
<div class="footer-copyright pt-10 pb-25">
<div class="container">
<div class="row">
<div class="col-md-8">
<div class="copyright text-md-left text-center pt-15">
<p>© Copyrights 2019 Amtul All rights reserved. </p>
</div>
</div>
<div class="col-md-4">
<div class="copyright text-md-right text-center pt-15">
</div>
</div>
</div> <!-- row -->
</div> <!-- container -->
</div> <!-- footer copyright -->
</footer>
<!--====== FOOTER PART ENDS ======-->
<!--====== BACK TO TP PART START ======-->
<a class="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
<!--====== BACK TO TP PART ENDS ======-->
<!--====== jquery js ======-->
<script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/modernizr-3.6.0.min.js"></script>
<script src="../wp-content/themes/blankslate/js/vendor/jquery-1.12.4.min.js"></script>
<!--====== Bootstrap js ======-->
<script src="../wp-content/themes/blankslate/js/bootstrap.min.js"></script>
<!--====== Slick js ======-->
<script src="../wp-content/themes/blankslate/js/slick.min.js"></script>
<!--====== Magnific Popup js ======-->
<script src="../wp-content/themes/blankslate/js/jquery.magnific-popup.min.js"></script>
<!--====== Counter Up js ======-->
<script src="../wp-content/themes/blankslate/js/waypoints.min.js"></script>
<script src="../wp-content/themes/blankslate/js/jquery.counterup.min.js"></script>
<!--====== Nice Select js ======-->
<script src="../wp-content/themes/blankslate/js/jquery.nice-select.min.js"></script>
<!--====== Nice Number js ======-->
<script src="../wp-content/themes/blankslate/js/jquery.nice-number.min.js"></script>
<!--====== Count Down js ======-->
<script src="../wp-content/themes/blankslate/js/jquery.countdown.min.js"></script>
<!--====== Validator js ======-->
<script src="../wp-content/themes/blankslate/js/validator.min.js"></script>
<!--====== Ajax Contact js ======-->
<script src="../wp-content/themes/blankslate/js/ajax-contact.js"></script>
<!--====== Main js ======-->
<script src="../wp-content/themes/blankslate/js/main.js"></script>
<!--====== Map js ======-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
<script src="../wp-content/themes/blankslate/js/map-script.js"></script>
</header></body></html>