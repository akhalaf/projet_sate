<!DOCTYPE html>
<html lang="ro">
<head>
<title>Stoloni Capsuni De Vanzare</title>
<meta content="De la soiuri timpurii, remontante si pana la cele de sezon tarziu, gasiti informatii complete despre stolonii de capsuni!" name="description"/>
<meta content="width=device-width,initial-scale=1.0,minimum-scale=1.0" name="viewport"/>
<link href="x/stil.css?v=1" media="screen,print" rel="stylesheet" type="text/css"/>
<link href="https://www.stolonicapsuni.ro/" rel="canonical"/>
</head>
<body>
<div id="head">
<div class="wrap">
<a href="https://www.stolonicapsuni.ro"><img alt="Stoloni Capsuni" id="logo" src="stoloni-capsuni.png"/></a>
<!--
  <a href="#" id="open"></a>
  <ul>
   <li><a href="#" title="#">#</a></li>
  </ul>
  -->
</div>
</div>
<div id="text">
<div class="wrap">
<h1>Stoloni de capsuni</h1>
<p>Fie ca sunteti interesat de plante A+, plante de pepiniera, plante "frigo" sau stoloni proaspeti de capsuni, gasiti informatii complete despre fiecare dintre aceste tipuri dar si despre soiurile variate de capsuni.</p>
</div>
</div>
<div class="row3">
<div class="wrap">
<h2>Soiuri timpurii</h2>
<ul>
<li>
<a href="capsuni-rumba.html"><img alt="Capsuni Rumba" src="img/capsuni-rumba.jpg"/></a>
<a class="info" href="capsuni-rumba.html">Rumba</a>
</li>
<li>
<a href="capsuni-flair.html"><img alt="Capsuni Flair" src="img/capsuni-flair.jpg"/></a>
<a class="info" href="capsuni-flair.html">Flair</a>
</li>
<li>
<a href="capsuni-felicita.html"><img alt="Capsuni Felicita" src="img/capsuni-felicita.jpg"/></a>
<a class="info" href="capsuni-felicita.html">Felicita</a>
</li>
</ul>
</div>
</div>
<div class="row3">
<div class="wrap">
<h2>La mijlocul sezonului</h2>
<ul>
<li>
<a href="capsuni-elsanta.html"><img alt="Capsuni Elsanta" src="img/capsuni-elsanta.jpg"/></a>
<a class="info" href="capsuni-elsanta.html">Elsanta</a>
</li>
<li>
<a href="capsuni-sonata.html"><img alt="Capsuni Sonata" src="img/capsuni-sonata.jpg"/></a>
<a class="info" href="capsuni-sonata.html">Sonata</a>
</li>
<li>
<a href="capsuni-vivaldi.html"><img alt="Capsuni Vivaldi" src="img/capsuni-vivaldi.jpg"/></a>
<a class="info" href="capsuni-vivaldi.html">Vivaldi</a>
</li>
</ul>
</div>
</div>
<div class="row4">
<div class="wrap">
<h2>Sezonul tarziu</h2>
<ul>
<li>
<a href="capsuni-filicia.html"><img alt="Capsuni Filicia" src="img/capsuni-filicia.jpg"/></a>
<a class="info" href="capsuni-filicia.html">Filicia</a>
</li>
<li>
<img alt="Capsuni Salsa" src="img/capsuni-salsa.jpg"/>
<a class="info" href="#">Salsa</a>
</li>
<li>
<img alt="Capsuni Malvina" src="img/capsuni-malvina.jpg"/>
<a class="info" href="#">Malvina</a>
</li>
<li>
<img alt="Capsuni Jive" src="img/capsuni-jive.jpg"/>
<a class="info" href="#">Jive</a>
</li>
</ul>
</div>
</div>
<div class="row4">
<div class="wrap">
<h2>Capsuni remontanti</h2>
<ul>
<li>
<a href="capsuni-florin.html"><img alt="Capsuni Florin" src="img/capsuni-florin.jpg"/></a>
<a class="info" href="capsuni-florin.html">Florin</a>
</li>
<li>
<a href="capsuni-everest.html"><img alt="Capsuni Everest" src="img/capsuni-everest.jpg"/></a>
<a class="info" href="capsuni-everest.html">Everest</a>
</li>
<li>
<a href="capsuni-evie-2.html"><img alt="Capsuni Evie 2" src="img/capsuni-evie-2.jpg"/></a>
<a class="info" href="capsuni-evie-2.html">Evie 2</a>
</li>
<li>
<a href="capsuni-florina.html"><img alt="Capsuni Florina" src="img/capsuni-florina.jpg"/></a>
<a class="info" href="capsuni-florina.html">Florina</a>
</li>
</ul>
</div>
</div>
<div id="info">
<div class="wrap">
<h3>Plante A+</h3>
<p>Plantele refrigerate din categoria A+ au parte de acelasi tratament ca celelalte plante refrigerate. Aceste plante se livreaza de la sfarsitul lui noiembrie pana la sfarsitul lui august. Plantele A+ pot produce capsuni in acelasi an in care au fost plantate. Aceste plante sunt disponibile în varietatile: A+3, A+ si 20+. Plantele A+3 sunt plante refrigerate grele cu rizomul de 12 - 15 mm si in mod normal aceste plante vor avea 2 inflorescente. Rizomul unei plante A+ este mai lung de 15 mm si in mod normal aceste plante vor avea 3 inflorescente. Plantele cu rizom mai lung de 20 mm sunt singurele disponibile in cantitati mici.</p>
<h3>Plante de pepiniera</h3>
<p>Plantele proaspete recoltate din pepiniera sunt plantate in iulie si august. Este nevoie de mare grija pentru ca aceste plante sa se dezvolte intr-o planta productiva buna in anul urmator. Regula pentru plantele de pepiniera este: cu cat plantele sunt mai grele, cu atat vor produce mai mult, insa fructele vor fi mai mici. Tipurile de plante de pepiniera sunt: grele (22 mm), medii (18 - 22 mm) si usoare (14-18 mm).</p>
<h3>Plante "frigo"</h3>
<p>Plantele "frigo" sunt scoase iarna, cand plantele vegeteaza. Dupa aceea, sunt depozitate la -1.8 °C in camere frigorifice pana ce vor putea fi plantate primavara. Plantele "frigo" sunt livrate de la sfarsitul lui noiembrie pana la sfarsitul lui august. Plantele standard sunt triate dupa dimensiunea rizomului de 8, 9 sau 10 mm pana la 15 mm. Varietati usoare precum Koronasau Lambada sunt triate sa aiba dimensiunea de 8 mm. Majoritatea varietatilor sunt triate sa aiba dimensiune de 9 mm. Soiul Elsantava fi triat la dimensiunea de 10 mm.</p>
<h3>Plante proaspete</h3>
<p>Plantele proaspete de diferite varietati sunt scoase de la sfarsitul lui iulie pana la sfarsitul lui august. Soiurile cu o singura fructificare pot fi plantate pana la sfarsitul lui august. Cateva soiuri de plante remontante sunt disponibile si ca plante proaspete.</p>
<h3>Stoloni proaspeti</h3>
<p>Stolonii proaspeti sunt disponibili din iulie. Caracteristicile tipului fac ca aceasta planta sa fie extrem de indicata pentru cultivarea in tavi, jardiniere si ghivece. Stolonul proaspat este disponibil si cu radacini, crescut in camp. Este livrat si ca stolon normal, crescut in substrat. Stolonul cu radacini are cateva radacini cu lungimea de 1 cm. Avantajul acestuia este ca stolonul este drept si creste ușor. Stolonul normal are cateva inceputuri proaspete de radacina.</p>
</div>
</div>
<div class="mail">
<div class="wrap">
<a href="mailto:contact@stolonicapsuni.ro">contact@stolonicapsuni.ro</a>
</div>
</div>
<div id="foot">
<div class="wrap">
<p>2021 - <a href="https://www.stolonicapsuni.ro">StoloniCapsuni.ro</a></p>
</div>
</div>
<script type="text/javascript">
 var sc_project=12197352;
 var sc_invisible=1;
 var sc_security="94c6defb";
</script>
<script async="" src="https://www.statcounter.com/counter/counter.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
<script src="x/main.js" type="text/javascript"></script>
</body>
</html>