<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<!-- This is Squarespace. --><!-- shivaani-shashi-e2pn -->
<base href=""/>
<meta charset="utf-8"/>
<title>Shivaani Shashi</title>
<link href="https://images.squarespace-cdn.com/content/v1/593a669086e6c0eb08d667e6/1526798107909-CEUDWLGCN18PSN14KAA0/ke17ZwdGBToddI8pDm48kOvp5grM46wJYeGA-7hEcbdZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpzzmiSFHqTo6mDDxfugW1X4XNz38bA0t74tMCOxidyzqGjtW00cDIaHg3qrvRJqLcE/favicon.ico?format=100w" rel="shortcut icon" type="image/x-icon"/>
<meta content="Shivaani Shashi" property="og:site_name"/>
<meta content="Shivaani Shashi" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="http://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=1500w" property="og:image"/>
<meta content="Shivaani Shashi" itemprop="name"/>
<meta content="http://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=1500w" itemprop="thumbnailUrl"/>
<link href="http://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=1500w" rel="image_src"/>
<meta content="http://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=1500w" itemprop="image"/>
<meta content="Shivaani Shashi" name="twitter:title"/>
<meta content="http://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=1500w" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="" name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/hlW56JdEtgJG9CxjECdFu3rtwT6g0GvWrhi4wGI0yTjfeltffFHN4UJLFRbh52jhWD9owhIhFRq352Z8Fcw3ZAJ35ebhFRi8Fy7GMPG0ShyTicBydcqlShyyShyTOA9kSet0SaBujW48Sagyjh90jhNlOeUzjhBC-eNDifUD-e8hjAB0-PuD-eBD-eqlZQ43dKoDSWmyScmDSeBRZPoRdhXCShyTicBydc8D-eBD-eq0jhNlOYiaikoD-e8hjAB0-PuD-eBD-eqlZQ43dKoDSWmyScmDSeBRZPoRdhXCiaiaO1FG-WZyjAoTShyyShyTOcFzdPJwSY4zpe8ljPu0daZyJ68ciWsuScIlSYb7fbRKHyMMeMw6MKG4fHvgIMMjgfMfH6qJnbIbMg6eJMJ7fbKOMsMMeMS6MKG4fOMgIMMj2KMfH6qJRMIbMg6sJMJ7fbKImsMgeMb6MKG4fJBmIMIjgkMfH6qJym9bMs65JMJ7fbKfmsMgegI6MTMgy_-IhM9.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Oswald:400|Roboto+Condensed:400" rel="stylesheet" type="text/css"/>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-5bfde57f439f3dd235e46-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-5bfde57f439f3dd235e46-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"></script><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8b244fce99594deac3684-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-9d33505677455a9b22add-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-82b361c64e6e75913711e-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-1b8d1adb275f098d8dc6d-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-335f062a058d1b1d85cee-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-614b07c3f84e1e3b30662-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-415312b37ef6978cf711b-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-da71321e53a08371a214c-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-9fe4eb33891804b4464fe-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-36ba56de635bc416bac0b-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-ce2b51182ccd8ac6accc6-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-5426bac76cb8bd5a92e8b-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-ac207ca39a69cb84f84f0-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-638ba2bf8ec524b820947-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-32cc08f2dd7137611cfc4-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-146a4d691830282d9ce5a-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-ee4869629f6a684b35f94-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-9e5a6a309dfd7e877bf6f-min.en-US.js"}},"pageType":100,"website":{"id":"593a669086e6c0eb08d667e6","identifier":"shivaani-shashi-e2pn","websiteType":1,"contentModifiedOn":1589201926540,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"Asia/Kolkata","machineTimeZoneOffset":19800000,"timeZoneOffset":19800000,"timeZoneAbbr":"IST","siteTitle":"Shivaani Shashi","fullSiteTitle":"Shivaani Shashi","siteTagLine":"Photographer","siteDescription":"<p>Shivaani Shashi is a Chennai based interior &amp; architecture photographer. </p>","location":{"mapZoom":7.0,"addressTitle":"Shivaani Shashi","addressLine1":"No 3 Avenue Road Nungambakkam","addressLine2":"Chennai, Tamilnadu 600034","addressCountry":"India"},"logoImageId":"5aa134fa53450af5d8e4793b","socialLogoImageId":"5b01172cf950b7644ad0b26e","shareButtonOptions":{"7":true,"2":true,"6":true,"4":true,"3":true,"1":true},"logoImageUrl":"//static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/","socialLogoImageUrl":"//static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5b01172cf950b7644ad0b26e/1589201926540/","authenticUrl":"https://www.shivaanishashi.com","internalUrl":"https://shivaani-shashi-e2pn.squarespace.com","baseUrl":"https://www.shivaanishashi.com","primaryDomain":"www.shivaanishashi.com","sslSetting":3,"isHstsEnabled":false,"socialAccounts":[{"serviceId":10,"userId":"2271179213","userName":"shivaanishashi","screenname":"Shivaani Shashi Photography","addedOn":1498309096600,"profileUrl":"http://instagram.com/shivaanishashi","iconUrl":"https://scontent.cdninstagram.com/t51.2885-19/s150x150/18879150_776642659172193_3179131953525817344_a.jpg","collectionId":"594e61e8ccf210e3df348d0f","iconEnabled":true,"serviceName":"instagram"},{"serviceId":4,"userId":"3246127513","userName":"shash0518","screenname":"Shivaani Shashi","addedOn":1498309150692,"profileUrl":"https://twitter.com/shash0518","iconUrl":"http://pbs.twimg.com/profile_images/788073604294451200/klE6MO-O_normal.jpg","collectionId":"594e621e1e5b6c22974521fe","iconEnabled":true,"serviceName":"twitter"},{"serviceId":2,"userId":"10154927683224118","screenname":"Shivaani Shashi","addedOn":1498309749644,"profileUrl":"https://www.facebook.com/shivaanishashiphotography/","iconUrl":"http://graph.facebook.com/10154927683224118/picture?type=square","metaData":{"service":"facebook"},"iconEnabled":true,"serviceName":"facebook"}],"typekitId":"","statsMigrated":false,"imageMetadataProcessingEnabled":false,"screenshotId":"9d545291aea6b1e23281f7c7cd6799ec73b9631eb7c5a07b68fe849346778b34","showOwnerLogin":false},"websiteSettings":{"id":"593a669086e6c0eb08d667e9","websiteId":"593a669086e6c0eb08d667e6","type":"Business","subjects":[],"country":"IN","state":"TN","simpleLikingEnabled":true,"mobileInfoBarSettings":{"style":1,"isContactEmailEnabled":true,"isContactPhoneNumberEnabled":false,"isLocationEnabled":false,"isBusinessHoursEnabled":false},"announcementBarSettings":{"style":1},"popupOverlaySettings":{"style":1,"enabledPages":[]},"commentLikesAllowed":true,"commentAnonAllowed":true,"commentThreaded":true,"commentApprovalRequired":false,"commentAvatarsOn":true,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":true,"commentEnableByDefault":true,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"contactPhoneNumber":"+919003096903","businessHours":{"monday":{"text":"","ranges":[{}]},"tuesday":{"text":"","ranges":[{}]},"wednesday":{"text":"","ranges":[{}]},"thursday":{"text":"","ranges":[{}]},"friday":{"text":"","ranges":[{}]},"saturday":{"text":"","ranges":[{}]},"sunday":{"text":"","ranges":[{}]}},"contactEmail":"shashi.shivaani@gmail.com","storeSettings":{"returnPolicy":null,"termsOfService":null,"privacyPolicy":null,"expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"US","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":true,"isBillingPhoneRequired":false,"currenciesSupported":["CHF","HKD","MXN","EUR","DKK","USD","CAD","MYR","NOK","THB","AUD","SGD","ILS","PLN","GBP","CZK","SEK","NZD","PHP","RUB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":false,"contactLocation":{"addressLine1":"No 3 Avenue Road, Nungambakkam","addressLine2":"Chennai, Tamilnadu","addressCountry":"India"},"businessName":"Shivaani Shashi Photography","sameAsRetailLocation":false,"businessId":"FFQPS7373L","merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":false,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"enabled_for_blogs_and_pages","size":"small","shape":"rect","color":"white"},"ampEnabled":true},"cookieSettings":{"isCookieBannerEnabled":false,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"gallery-style":"Fit","galleryPadding":"60px","product-gallery-auto-crop":"true","product-image-auto-crop":"true","thumbnail-aspect-ratio":"Auto","thumbnailWidth":"350px","tweak-v1-related-products-title-spacing":"50px"},"templateId":"4f9adc1524ac5df956fdf98f","templateVersion":"7","pageFeatures":[1,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/4f9adbe124ac5df956fdf900/864/scripts/","betaFeatureFlags":["ORDER_SERVICE-submit-subscription-order-through-service","crm_campaigns_sending","omit_tweakengine_tweakvalues","commerce_instagram_product_checkout_links","commerce_add_to_cart_rate_limiting","domains_transfer_flow_hide_preface","domains_allow_async_gsuite","donations_customer_accounts","domain_deletion_via_registrar_service","domains_transfer_flow_improvements","gallery_captions_71","domains_use_new_domain_connect_strategy","commerce_minimum_order_amount","commerce_subscription_order_delay","ORDERS-SERVICE-reset-digital-goods-access-with-service","seven_one_portfolio_hover_layouts","campaigns_new_sender_profile_page","commerce_tax_panel_v2","seven-one-menu-overlay-theme-switcher","list_sent_to_groups","commerce_category_id_discounts_enabled","events_panel_70","commerce_reduce_cart_calculations","member_areas_ga","ORDERS-SERVICE-check-digital-good-access-with-service","local_listings","domain_locking_via_registrar_service","customer_notifications_panel_v2","seven-one-main-content-preview-api","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","seven-one-content-preview-section-api","domain_info_via_registrar_service","seven_one_image_effects","commerce_setup_wizard","dg_downloads_from_fastly","page_interactions_improvements","seven_one_image_overlay_opacity","newsletter_block_captcha","animations_august_2020_new_preset","commerce_restock_notifications","campaigns_single_opt_in","generic_iframe_loader_for_campaigns","seven_one_header_editor_update","commerce-recaptcha-enterprise","commerce_activation_experiment_add_payment_processor_card","commerce_afterpay_toggle","seven_one_frontend_render_page_section","commerce_afterpay","commerce_pdp_survey_modal","nested_categories_migration_enabled","commerce_orders_elasticsearch_migration","seven_one_frontend_render_gallery_section","campaigns_user_templates_in_sidebar","domains_universal_search","campaigns_email_reuse_template_flow","commerce_pdp_edit_mode","commerce_demo_products_modal"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"tzData":{"zones":[[330,null,"IST",null]],"rules":{}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><script type="application/ld+json">{"url":"https://www.shivaanishashi.com","name":"Shivaani Shashi","description":"<p>Shivaani Shashi is a Chennai based interior &amp; architecture photographer. </p>","image":"//static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/","@context":"http://schema.org","@type":"WebSite"}</script><script type="application/ld+json">{"legalName":"Shivaani Shashi Photography","address":"No 3 Avenue Road Nungambakkam\nChennai, Tamilnadu 600034\nIndia","email":"shashi.shivaani@gmail.com","telephone":"+919003096903","sameAs":["http://instagram.com/shivaanishashi","https://twitter.com/shash0518","https://www.facebook.com/shivaanishashiphotography/"],"@context":"http://schema.org","@type":"Organization"}</script><script type="application/ld+json">{"address":"No 3 Avenue Road, Nungambakkam\nChennai, Tamilnadu\nIndia","image":"https://static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/","name":"Shivaani Shashi Photography","openingHours":", , , , , , ","@context":"http://schema.org","@type":"LocalBusiness"}</script><link href="//static1.squarespace.com/static/sitecss/593a669086e6c0eb08d667e6/28/4f9adc1524ac5df956fdf98f/593a669086e6c0eb08d667ef/864-05142015/1549345889431/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<!-- End of Squarespace Headers -->
</head>
<body class="sidebar-text-alignment-left sidebar-position-left sidebar-fixed mobile-site-title-style-auto blog-sidebar-right gallery-style-fit enable-gallery-thumbnails initial-gallery-view-thumbnails homepage-gallery-view-inherit thumbnail-aspect-ratio-auto gallery-controls-simple social-icon-style-round tweak-template-social-icon-standard-color show-category-navigation event-show-past-events event-thumbnails event-thumbnail-size-32-standard event-date-label event-list-show-cats event-list-date event-list-time event-list-address event-icalgcal-links event-excerpts event-item-back-link product-list-titles-under product-list-alignment-center product-item-size-23-standard-vertical product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark hide-opentable-icons opentable-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-left image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-solid button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd not-found-page mobile-style-available" id="not-found">
<div id="canvasWrapper">
<div id="canvas">
<div id="mobileNav"><div class="wrapper">
<nav class="main-nav mobile-nav"><ul>
<li class="mobile-folder">
<a href="/architecture">Architecture</a>
<ul>
<li class="gallery-collection">
<a href="/hospitality">Hospitality</a>
</li>
<li class="gallery-collection">
<a href="/commercial">Commercial</a>
</li>
<li class="gallery-collection">
<a href="/residences">Residences</a>
</li>
</ul>
</li>
<li class="gallery-collection">
<a href="/people">People</a>
</li>
<li class="gallery-collection">
<a href="/food">Food</a>
</li>
</ul>
</nav>
<nav class="main-nav mobile-nav"><ul>
<li class="gallery-collection">
<a href="/travel">Travel</a>
</li>
<li class="mobile-folder">
<a href="/projects">Projects</a>
<ul>
<li class="page-collection">
<a href="/likeapainting">Like a painting...</a>
</li>
<li class="page-collection">
<a href="/thampuratty">Thampuratty</a>
</li>
</ul>
</li>
<li class="blog-collection">
<a href="/blog">Blog</a>
</li>
<li class="gallery-collection">
<a href="/press">Press</a>
</li>
<li class="page-collection">
<a href="/contact">Contact</a>
</li>
</ul>
</nav>
</div></div>
<div id="headerWrapper">
<header id="header">
<div id="topNavMobile">
<nav class="main-nav clear" id="mobileMenuLink"><ul><li class="active-link"><a>Menu</a></li></ul></nav>
</div>
<div data-content-field="site-title" id="logo">
<h1 class="logo image">
<a href="/">
<img alt="Shivaani Shashi" src="//static1.squarespace.com/static/593a669086e6c0eb08d667e6/t/5aa134fa53450af5d8e4793b/1589201926540/?format=750w"/>
</a>
</h1>
<div class="logo-subtitle">Photographer</div>
</div>
<script>
          Y.use('squarespace-ui-base', function(Y) {
            Y.one("h1.logo").plug(Y.Squarespace.TextShrink);
          });
        </script>
<div data-content-field="navigation" id="topNav">
<nav class="main-nav dropdown-click desktop-nav" id="mainNavigation">
<ul>
<li class="folder-collection folder">
<a>Architecture</a>
<div class="subnav">
<ul>
<li class="gallery-collection">
<a href="/hospitality">Hospitality</a>
</li>
<li class="gallery-collection">
<a href="/commercial">Commercial</a>
</li>
<li class="gallery-collection">
<a href="/residences">Residences</a>
</li>
</ul>
</div>
</li>
<li class="gallery-collection">
<a href="/people">People</a>
</li>
<li class="gallery-collection">
<a href="/food">Food</a>
</li>
</ul>
</nav>
<nav class="main-nav dropdown-click desktop-nav" id="secondaryNavigation">
<ul>
<li class="gallery-collection">
<a href="/travel">Travel</a>
</li>
<li class="folder-collection folder">
<a>Projects</a>
<div class="subnav">
<ul>
<li class="page-collection">
<a href="/likeapainting">Like a painting...</a>
</li>
<li class="page-collection">
<a href="/thampuratty">Thampuratty</a>
</li>
</ul>
</div>
</li>
<li class="blog-collection">
<a href="/blog">Blog</a>
</li>
<li class="gallery-collection">
<a href="/press">Press</a>
</li>
<li class="page-collection">
<a href="/contact">Contact</a>
</li>
</ul>
</nav>
</div>
<div class="sqs-layout sqs-grid-1 columns-1" data-layout-label="Header Content" data-type="block-field" data-updated-on="1498310294210" id="headerBlocks"><div class="row sqs-row"><div class="col sqs-col-1 span-1"><div class="sqs-block spacer-block sqs-block-spacer sized vsize-1" data-block-type="21" id="block-yui_3_17_2_4_1498306813954_30941"><div class="sqs-block-content"> </div></div></div></div></div>
<div class="social-links sqs-svg-icon--list" data-content-field="connected-accounts" id="footerBlocks-2">
<a class="sqs-svg-icon--wrapper instagram" href="http://instagram.com/shivaanishashi" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--icon" xlink:href="#instagram-icon"></use>
<use class="sqs-use--mask" xlink:href="#instagram-mask"></use>
</svg>
</div>
</a>
<a class="sqs-svg-icon--wrapper twitter" href="https://twitter.com/shash0518" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--icon" xlink:href="#twitter-icon"></use>
<use class="sqs-use--mask" xlink:href="#twitter-mask"></use>
</svg>
</div>
</a>
<a class="sqs-svg-icon--wrapper facebook" href="https://www.facebook.com/shivaanishashiphotography/" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--icon" xlink:href="#facebook-icon"></use>
<use class="sqs-use--mask" xlink:href="#facebook-mask"></use>
</svg>
</div>
</a>
</div>
</header>
</div>
<div class="hfeed" id="pageWrapper" role="main">
<!-- CATEGORY NAV -->
<section id="page">
<div class="main-content" data-content-field="main-content">
<p>We couldn't find the page you were looking for. This is either because:</p>
<ul>
<li>There is an error in the URL entered into your web browser. Please check the URL and try again.</li>
<li>The page you are looking for has been moved or deleted.</li>
</ul>
<p>
  You can return to our homepage by <a href="/">clicking here</a>, or you can try searching for the
  content you are seeking by <a href="/search">clicking here</a>.
</p>
</div>
</section>
</div>
</div>
</div>
<script src="https://static1.squarespace.com/static/ta/4f9adbe124ac5df956fdf900/864/scripts/combo/?site.js" type="text/javascript"></script>
<script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script><svg data-usage="social-icons-svg" style="display:none" version="1.1" xmlns="http://www.w3.org/2000/svg"><symbol id="instagram-icon" viewbox="0 0 64 64"><path d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z"></path></symbol><symbol id="instagram-mask" viewbox="0 0 64 64"><path d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z"></path></symbol><symbol id="twitter-icon" viewbox="0 0 64 64"><path d="M48,22.1c-1.2,0.5-2.4,0.9-3.8,1c1.4-0.8,2.4-2.1,2.9-3.6c-1.3,0.8-2.7,1.3-4.2,1.6 C41.7,19.8,40,19,38.2,19c-3.6,0-6.6,2.9-6.6,6.6c0,0.5,0.1,1,0.2,1.5c-5.5-0.3-10.3-2.9-13.5-6.9c-0.6,1-0.9,2.1-0.9,3.3 c0,2.3,1.2,4.3,2.9,5.5c-1.1,0-2.1-0.3-3-0.8c0,0,0,0.1,0,0.1c0,3.2,2.3,5.8,5.3,6.4c-0.6,0.1-1.1,0.2-1.7,0.2c-0.4,0-0.8,0-1.2-0.1 c0.8,2.6,3.3,4.5,6.1,4.6c-2.2,1.8-5.1,2.8-8.2,2.8c-0.5,0-1.1,0-1.6-0.1c2.9,1.9,6.4,2.9,10.1,2.9c12.1,0,18.7-10,18.7-18.7 c0-0.3,0-0.6,0-0.8C46,24.5,47.1,23.4,48,22.1z"></path></symbol><symbol id="twitter-mask" viewbox="0 0 64 64"><path d="M0,0v64h64V0H0z M44.7,25.5c0,0.3,0,0.6,0,0.8C44.7,35,38.1,45,26.1,45c-3.7,0-7.2-1.1-10.1-2.9 c0.5,0.1,1,0.1,1.6,0.1c3.1,0,5.9-1,8.2-2.8c-2.9-0.1-5.3-2-6.1-4.6c0.4,0.1,0.8,0.1,1.2,0.1c0.6,0,1.2-0.1,1.7-0.2 c-3-0.6-5.3-3.3-5.3-6.4c0,0,0-0.1,0-0.1c0.9,0.5,1.9,0.8,3,0.8c-1.8-1.2-2.9-3.2-2.9-5.5c0-1.2,0.3-2.3,0.9-3.3 c3.2,4,8.1,6.6,13.5,6.9c-0.1-0.5-0.2-1-0.2-1.5c0-3.6,2.9-6.6,6.6-6.6c1.9,0,3.6,0.8,4.8,2.1c1.5-0.3,2.9-0.8,4.2-1.6 c-0.5,1.5-1.5,2.8-2.9,3.6c1.3-0.2,2.6-0.5,3.8-1C47.1,23.4,46,24.5,44.7,25.5z"></path></symbol><symbol id="facebook-icon" viewbox="0 0 64 64"><path d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z"></path></symbol><symbol id="facebook-mask" viewbox="0 0 64 64"><path d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z"></path></symbol></svg>
</body>
</html>
