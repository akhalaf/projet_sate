<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<!-- This site is optimized with the Yoast SEO plugin v15.6.2 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found | Redding Tree Service Inc.</title>
<meta content="noindex, follow" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="Page not found | Redding Tree Service Inc." property="og:title"/>
<meta content="Redding Tree Service Inc." property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://reddingtree.com/#organization","name":"Redding Tree Service, Inc.","url":"https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/","sameAs":[],"logo":{"@type":"ImageObject","@id":"https://reddingtree.com/#logo","inLanguage":"en-US","url":"https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/Redding-Tree-Service-Logo-72dpi-Horizontal-06-06.png?time=1610648349","width":1676,"height":422,"caption":"Redding Tree Service, Inc."},"image":{"@id":"https://reddingtree.com/#logo"}},{"@type":"WebSite","@id":"https://reddingtree.com/#website","url":"https://reddingtree.com/","name":"Redding Tree Service Inc.","description":"Serving Northern California Since 1980","publisher":{"@id":"https://reddingtree.com/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://reddingtree.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://reddingtree.com/feed/" rel="alternate" title="Redding Tree Service Inc. » Feed" type="application/rss+xml"/>
<link href="https://reddingtree.com/comments/feed/" rel="alternate" title="Redding Tree Service Inc. » Comments Feed" type="application/rss+xml"/>
<link crossorigin="" href="https://secureservercdn.net" rel="preconnect"/>
<!-- This site uses the Google Analytics by MonsterInsights plugin v7.14.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
<script data-cfasync="false" type="text/javascript">
    (window.gaDevIds=window.gaDevIds||[]).push("dZGIzZG");
	var mi_version         = '7.14.0';
	var mi_track_user      = true;
	var mi_no_track_reason = '';
	
	var disableStr = 'ga-disable-UA-42157409-30';

	/* Function to detect opted out users */
	function __gaTrackerIsOptedOut() {
		return document.cookie.indexOf(disableStr + '=true') > -1;
	}

	/* Disable tracking if the opt-out cookie exists. */
	if ( __gaTrackerIsOptedOut() ) {
		window[disableStr] = true;
	}

	/* Opt-out function */
	function __gaTrackerOptout() {
	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
	  window[disableStr] = true;
	}

	if ( 'undefined' === typeof gaOptout ) {
		function gaOptout() {
			__gaTrackerOptout();
		}
	}
	
	if ( mi_track_user ) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

		__gaTracker('create', 'UA-42157409-30', 'auto');
		__gaTracker('set', 'forceSSL', true);
		__gaTracker('require', 'displayfeatures');
		__gaTracker('require', 'linkid', 'linkid.js');
		__gaTracker('send','pageview','/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer);
	} else {
		console.log( "" );
		(function() {
			/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
			var noopfn = function() {
				return null;
			};
			var noopnullfn = function() {
				return null;
			};
			var Tracker = function() {
				return null;
			};
			var p = Tracker.prototype;
			p.get = noopfn;
			p.set = noopfn;
			p.send = noopfn;
			var __gaTracker = function() {
				var len = arguments.length;
				if ( len === 0 ) {
					return;
				}
				var f = arguments[len-1];
				if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
					console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
					return;
				}
				try {
					f.hitCallback();
				} catch (ex) {

				}
			};
			__gaTracker.create = function() {
				return new Tracker();
			};
			__gaTracker.getByName = noopnullfn;
			__gaTracker.getAll = function() {
				return [];
			};
			__gaTracker.remove = noopfn;
			window['__gaTracker'] = __gaTracker;
					})();
		}
</script>
<!-- / Google Analytics by MonsterInsights -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"wpemoji":"https:\/\/secureservercdn.net\/104.238.69.231\/zhi.8cb.myftpupload.com\/wp-includes\/js\/wp-emoji.js?time=1610648349","twemoji":"https:\/\/secureservercdn.net\/104.238.69.231\/zhi.8cb.myftpupload.com\/wp-includes\/js\/twemoji.js?time=1610648349"}};
			/**
 * @output wp-includes/js/wp-emoji-loader.js
 */

( function( window, document, settings ) {
	var src, ready, ii, tests;

	// Create a canvas element for testing native browser support of emoji.
	var canvas = document.createElement( 'canvas' );
	var context = canvas.getContext && canvas.getContext( '2d' );

	/**
	 * Checks if two sets of Emoji characters render the same visually.
	 *
	 * @since 4.9.0
	 *
	 * @private
	 *
	 * @param {number[]} set1 Set of Emoji character codes.
	 * @param {number[]} set2 Set of Emoji character codes.
	 *
	 * @return {boolean} True if the two sets render the same.
	 */
	function emojiSetsRenderIdentically( set1, set2 ) {
		var stringFromCharCode = String.fromCharCode;

		// Cleanup from previous test.
		context.clearRect( 0, 0, canvas.width, canvas.height );
		context.fillText( stringFromCharCode.apply( this, set1 ), 0, 0 );
		var rendered1 = canvas.toDataURL();

		// Cleanup from previous test.
		context.clearRect( 0, 0, canvas.width, canvas.height );
		context.fillText( stringFromCharCode.apply( this, set2 ), 0, 0 );
		var rendered2 = canvas.toDataURL();

		return rendered1 === rendered2;
	}

	/**
	 * Detects if the browser supports rendering emoji or flag emoji.
	 *
	 * Flag emoji are a single glyph made of two characters, so some browsers
	 * (notably, Firefox OS X) don't support them.
	 *
	 * @since 4.2.0
	 *
	 * @private
	 *
	 * @param {string} type Whether to test for support of "flag" or "emoji".
	 *
	 * @return {boolean} True if the browser can render emoji, false if it cannot.
	 */
	function browserSupportsEmoji( type ) {
		var isIdentical;

		if ( ! context || ! context.fillText ) {
			return false;
		}

		/*
		 * Chrome on OS X added native emoji rendering in M41. Unfortunately,
		 * it doesn't work when the font is bolder than 500 weight. So, we
		 * check for bold rendering support to avoid invisible emoji in Chrome.
		 */
		context.textBaseline = 'top';
		context.font = '600 32px Arial';

		switch ( type ) {
			case 'flag':
				/*
				 * Test for Transgender flag compatibility. This flag is shortlisted for the Emoji 13 spec,
				 * but has landed in Twemoji early, so we can add support for it, too.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly (white flag emoji + transgender symbol).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0x1F3F3, 0xFE0F, 0x200D, 0x26A7, 0xFE0F ],
					[ 0x1F3F3, 0xFE0F, 0x200B, 0x26A7, 0xFE0F ]
				);

				if ( isIdentical ) {
					return false;
				}

				/*
				 * Test for UN flag compatibility. This is the least supported of the letter locale flags,
				 * so gives us an easy test for full support.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly ([U] + [N]).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0xD83C, 0xDDFA, 0xD83C, 0xDDF3 ],
					[ 0xD83C, 0xDDFA, 0x200B, 0xD83C, 0xDDF3 ]
				);

				if ( isIdentical ) {
					return false;
				}

				/*
				 * Test for English flag compatibility. England is a country in the United Kingdom, it
				 * does not have a two letter locale code but rather an five letter sub-division code.
				 *
				 * To test for support, we try to render it, and compare the rendering to how it would look if
				 * the browser doesn't render it correctly (black flag emoji + [G] + [B] + [E] + [N] + [G]).
				 */
				isIdentical = emojiSetsRenderIdentically(
					[ 0xD83C, 0xDFF4, 0xDB40, 0xDC67, 0xDB40, 0xDC62, 0xDB40, 0xDC65, 0xDB40, 0xDC6E, 0xDB40, 0xDC67, 0xDB40, 0xDC7F ],
					[ 0xD83C, 0xDFF4, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC62, 0x200B, 0xDB40, 0xDC65, 0x200B, 0xDB40, 0xDC6E, 0x200B, 0xDB40, 0xDC67, 0x200B, 0xDB40, 0xDC7F ]
				);

				return ! isIdentical;
			case 'emoji':
				/*
				 * So easy, even a baby could do it!
				 *
				 *  To test for Emoji 13 support, try to render a new emoji: Man Feeding Baby.
				 *
				 * The Man Feeding Baby emoji is a ZWJ sequence combining 👨 Man, a Zero Width Joiner and 🍼 Baby Bottle.
				 *
				 * 0xD83D, 0xDC68 == Man emoji.
				 * 0x200D == Zero-Width Joiner (ZWJ) that links the two code points for the new emoji or
				 * 0x200B == Zero-Width Space (ZWS) that is rendered for clients not supporting the new emoji.
				 * 0xD83C, 0xDF7C == Baby Bottle.
				 *
				 * When updating this test for future Emoji releases, ensure that individual emoji that make up the
				 * sequence come from older emoji standards.
				 */
				isIdentical = emojiSetsRenderIdentically(
					[0xD83D, 0xDC68, 0x200D, 0xD83C, 0xDF7C],
					[0xD83D, 0xDC68, 0x200B, 0xD83C, 0xDF7C]
				);

				return ! isIdentical;
		}

		return false;
	}

	/**
	 * Adds a script to the head of the document.
	 *
	 * @ignore
	 *
	 * @since 4.2.0
	 *
	 * @param {Object} src The url where the script is located.
	 * @return {void}
	 */
	function addScript( src ) {
		var script = document.createElement( 'script' );

		script.src = src;
		script.defer = script.type = 'text/javascript';
		document.getElementsByTagName( 'head' )[0].appendChild( script );
	}

	tests = Array( 'flag', 'emoji' );

	settings.supports = {
		everything: true,
		everythingExceptFlag: true
	};

	/*
	 * Tests the browser support for flag emojis and other emojis, and adjusts the
	 * support settings accordingly.
	 */
	for( ii = 0; ii < tests.length; ii++ ) {
		settings.supports[ tests[ ii ] ] = browserSupportsEmoji( tests[ ii ] );

		settings.supports.everything = settings.supports.everything && settings.supports[ tests[ ii ] ];

		if ( 'flag' !== tests[ ii ] ) {
			settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && settings.supports[ tests[ ii ] ];
		}
	}

	settings.supports.everythingExceptFlag = settings.supports.everythingExceptFlag && ! settings.supports.flag;

	// Sets DOMReady to false and assigns a ready function to settings.
	settings.DOMReady = false;
	settings.readyCallback = function() {
		settings.DOMReady = true;
	};

	// When the browser can not render everything we need to load a polyfill.
	if ( ! settings.supports.everything ) {
		ready = function() {
			settings.readyCallback();
		};

		/*
		 * Cross-browser version of adding a dom ready event.
		 */
		if ( document.addEventListener ) {
			document.addEventListener( 'DOMContentLoaded', ready, false );
			window.addEventListener( 'load', ready, false );
		} else {
			window.attachEvent( 'onload', ready );
			document.attachEvent( 'onreadystatechange', function() {
				if ( 'complete' === document.readyState ) {
					settings.readyCallback();
				}
			} );
		}

		src = settings.source || {};

		if ( src.concatemoji ) {
			addScript( src.concatemoji );
		} else if ( src.wpemoji && src.twemoji ) {
			addScript( src.twemoji );
			addScript( src.wpemoji );
		}
	}

} )( window, document, window._wpemojiSettings );
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-includes/css/dist/block-library/style.css?time=1610648349" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-includes/css/dist/block-library/theme.css?time=1610648349" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/simple-sitemap/lib/assets/css/simple-sitemap.css?time=1610648349" id="simple-sitemap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/contact-form-7/includes/css/styles.css?time=1610648349" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/google-analytics-for-wordpress/assets/css/frontend.css?time=1610648349" id="monsterinsights-popular-posts-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Crimson+Text%3A700%7CRoboto%3A400%2C700%2C900%2C300&amp;ver=5.6" id="himalayas-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/font-awesome/css/font-awesome.min.css?time=1610648349" id="himalayas-fontawesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/style.css?time=1610648349" id="himalayas-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/magnific-popup/magnific-popup.css?time=1610648349" id="himalayas-featured-image-popup-css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="monsterinsights-frontend-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/reddingtree.com","hash_tracking":"false"};
/* ]]> */
</script>
<script id="monsterinsights-frontend-script-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.js?time=1610648349" type="text/javascript"></script>
<script id="jquery-core-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-includes/js/jquery/jquery.js?time=1610648349" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-includes/js/jquery/jquery-migrate.js?time=1610648349" type="text/javascript"></script>
<link href="https://reddingtree.com/wp-json/" rel="https://api.w.org/"/><meta content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta content="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/pruning-1.jpg?time=1610648349" name="twitter:image"/><meta content="summary" name="twitter:card"/><meta content="Redding Tree Service Inc." name="twitter:domain"/><meta content="it is important to not cut your foot off.

hbsjdhcbvaskjdhbkjasdas

dvas

dvasdfvasdfvasdfvsdfv

sdfvasfdvsafvdasfdvsfdvsadvbqshdfvbjsv

sdvjsdvjsdvsdvwwsd

Be safe. ..." name="twitter:description"/><meta content="Chainsaw safety - Redding Tree Service Inc." name="twitter:title"/><meta content="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/pruning-1.jpg?time=1610648349" property="og:image"/><meta content="Redding Tree Service Inc." property="og:site_name"/><meta content="it is important to not cut your foot off.

hbsjdhcbvaskjdhbkjasdas

dvas

dvasdfvasdfvasdfvsdfv

sdfvasfdvsafvdasfdvsfdvsadvbqshdfvbjsv

sdvjsdvjsdvsdvwwsd

Be safe. ..." property="og:description"/><meta content="https://reddingtree.com/xcxxc/alibabaemail/alibaba/?email=abuse@sian.com.cn%09" property="og:url"/><meta content="object" property="og:type"/><meta content="Chainsaw safety - Redding Tree Service Inc." property="og:title"/><meta content="it is important to not cut your foot off.

hbsjdhcbvaskjdhbkjasdas

dvas

dvasdfvasdfvasdfvsdfv

sdfvasfdvsafvdasfdvsfdvsadvbqshdfvbjsv

sdvjsdvjsdvsdvwwsd

Be safe. ..." name="description"/><meta content="Chainsaw safety - Redding Tree Service Inc." name="title"/><link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-72dpi-circle-round-05-05-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-72dpi-circle-round-05-05-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-72dpi-circle-round-05-05-180x180.png" rel="apple-touch-icon"/>
<meta content="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-72dpi-circle-round-05-05-270x270.png" name="msapplication-TileImage"/>
<style type="text/css"> .about-btn a:hover,.bttn:hover,.icon-img-wrap:hover,.navigation .nav-links a:hover,.service_icon_class .image-wrap:hover i,.slider-readmore:before,.subscribe-form .subscribe-submit .subscribe-btn,button,input[type=button]:hover,input[type=reset]:hover,input[type=submit]:hover,.contact-form-wrapper input[type=submit],.default-wp-page a:hover,.team-desc-wrapper{background:#81d742}a, .cta-text-btn:hover,.blog-readmore:hover, .entry-meta a:hover,.entry-meta > span:hover::before,#content .comments-area article header cite a:hover, #content .comments-area a.comment-edit-link:hover, #content .comments-area a.comment-permalink:hover,.comment .comment-reply-link:hover{color:#81d742}.comments-area .comment-author-link span{background-color:#81d742}.slider-readmore:hover{border:1px solid #81d742}.icon-wrap:hover,.image-wrap:hover,.port-link a:hover{border-color:#81d742}.main-title:after,.main-title:before{border-top:2px solid #81d742}.blog-view,.port-link a:hover{background:#81d742}.port-title-wrapper .port-desc{color:#81d742}#top-footer a:hover,.blog-title a:hover,.entry-title a:hover,.footer-nav li a:hover,.footer-social a:hover,.widget ul li a:hover,.widget ul li:hover:before{color:#81d742}.scrollup{background-color:#81d742}#stick-navigation li.current-one-page-item a,#stick-navigation li:hover a,.blog-hover-link a:hover,.entry-btn .btn:hover{background:#81d742}#secondary .widget-title:after,#top-footer .widget-title:after{background:#81d742}.widget-tags a:hover,.sub-toggle{background:#81d742;border:1px solid #81d742}#site-navigation .menu li.current-one-page-item > a,#site-navigation .menu li:hover > a,.about-title a:hover,.caption-title a:hover,.header-wrapper.no-slider #site-navigation .menu li.current-one-page-item > a,.header-wrapper.no-slider #site-navigation .menu li:hover > a,.header-wrapper.no-slider .search-icon:hover,.header-wrapper.stick #site-navigation .menu li.current-one-page-item > a,.header-wrapper.stick #site-navigation .menu li:hover > a,.header-wrapper.stick .search-icon:hover,.scroll-down,.search-icon:hover,.service-title a:hover,.service-read-more:hover,.num-404,blog-readmore:hover{color:#81d742}.error{background:#81d742}.blog-view:hover,.scrollup:hover,.contact-form-wrapper input[type="submit"]:hover{background:#59af1a}.blog-view{border-color:#59af1a}.posted-date span a:hover, .copyright-text a:hover,.contact-content a:hover,.logged-in-as a:hover, .logged-in-as a hover{color:#59af1a}.widget_call_to_action_block .parallax-overlay,.search-box,.scrollup,.sub-toggle:hover{background-color:rgba(129,215,66, 0.85)}.author-box{border:1px solid #81d742}</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 wp-custom-logo wp-embed-responsive stick non-transparent wpb-js-composer js-comp-ver-5.7 vc_responsive">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header clearfix" id="masthead" role="banner">
<div class="header-wrapper clearfix">
<div class="tg-container">
<div class="logo">
<a class="custom-logo-link" href="https://reddingtree.com/" rel="home"><img alt="Redding Tree Service Inc." class="custom-logo" height="92" sizes="(max-width: 351px) 100vw, 351px" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-Small300dpi-07-08-08-09-09-09.png?time=1610648349" srcset="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-Small300dpi-07-08-08-09-09-09.png 351w, https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-Logo-Small300dpi-07-08-08-09-09-09-300x79.png 300w" width="351"/></a>
</div> <!-- logo-end -->
<div class="screen-reader-text" id="header-text">
<h3 id="site-title">
<a href="https://reddingtree.com/" rel="home" title="Redding Tree Service Inc.">Redding Tree Service Inc.</a>
</h3>
<p id="site-description">Serving Northern California Since 1980</p>
</div><!-- #header-text -->
<div class="menu-search-wrapper">
<div class="home-search">
<div class="search-icon">
<i class="fa fa-search"> </i>
</div>
<div class="search-box">
<div class="close"> ×</div>
<form action="https://reddingtree.com/" class="searchform" method="get" role="search">
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
<button class="searchsubmit" name="submit" type="submit" value="Search"><i class="fa fa-search"></i></button>
</form> </div>
</div> <!-- home-search-end -->
<nav class="main-navigation" id="site-navigation" role="navigation">
<p class="menu-toggle hide"></p>
<div class="menu-primary-container"><ul class="menu" id="menu-main"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-167" id="menu-item-167"><a href="https://reddingtree.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-188" id="menu-item-188"><a href="https://reddingtree.com/services/">Tree Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166" id="menu-item-166"><a href="https://reddingtree.com/contact/">Contact</a></li>
</ul></div> </nav> <!-- nav-end -->
</div> <!-- Menu-search-wrapper end -->
</div><!-- tg-container -->
</div><!-- header-wrapepr end -->
<div class="wp-custom-header" id="wp-custom-header"><div class="header-image-wrap"><img alt="Redding Tree Service Inc." class="header-image" height="1063" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/uploads/2020/04/cropped-Redding-Tree-Service-003.jpg?time=1610648349" width="1891"/></div></div>
</header>
<div class="site-content" id="content">
<main class="clearfix no-sidebar-full-width" id="main">
<div class="tg-container">
<div id="primary">
<div id="content-2">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header>
<p>It looks like nothing was found at this location. Try the search below.</p>
<div class="error-wrap">
<span class="num-404">
                                 404                              </span>
<span class="error">error</span>
</div>
<form action="https://reddingtree.com/" class="searchform" method="get" role="search">
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
<button class="searchsubmit" name="submit" type="submit" value="Search"><i class="fa fa-search"></i></button>
</form> </div>
</section>
</div>
</div>
</div>
</main>
</div>
<footer class="footer-with-widget footer-layout-one" id="colophon">
<div id="bottom-footer">
<div class="tg-container">
<div class="copyright"><span class="copyright-text">Copyright © 2021 <a href="https://reddingtree.com/" title="Redding Tree Service Inc.">Redding Tree Service Inc.</a>. Theme: Himalayas by <a href="https://themegrill.com/themes/himalayas" rel="author" target="_blank" title="ThemeGrill">ThemeGrill</a>. Powered by <a href="https://wordpress.org" target="_blank" title="WordPress">WordPress</a>.</span></div>
<div class="footer-nav">
</div>
</div>
</div>
</footer>
<a class="scrollup" href="#"><i class="fa fa-angle-up"> </i> </a>
</div> <!-- #Page -->
<!--
The IP2Location Country Blocker is using IP2Location LITE geolocation database. Please visit http://lite.ip2location.com for more information.
-->
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/reddingtree.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?time=1610648349" type="text/javascript"></script>
<script id="google-recaptcha-js" src="https://www.google.com/recaptcha/api.js?render=6LcODOwUAAAAAJ_J9JMySqCyGZADUnoAd_99FUuo&amp;ver=3.0" type="text/javascript"></script>
<script id="wpcf7-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7_recaptcha = {"sitekey":"6LcODOwUAAAAAJ_J9JMySqCyGZADUnoAd_99FUuo","actions":{"homepage":"homepage","contactform":"contactform"}};
/* ]]> */
</script>
<script id="wpcf7-recaptcha-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/plugins/contact-form-7/modules/recaptcha/script.js?time=1610648349" type="text/javascript"></script>
<script id="himalayas-onepagenav-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/jquery.nav.js?time=1610648349" type="text/javascript"></script>
<script id="jarallax-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/jarallax/jarallax.min.js?time=1610648349" type="text/javascript"></script>
<script id="himalayas-featured-image-popup-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/magnific-popup/jquery.magnific-popup.min.js?time=1610648349" type="text/javascript"></script>
<script id="himalayas-skip-link-focus-fix-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/skip-link-focus-fix.js?time=1610648349" type="text/javascript"></script>
<script id="himalayas-custom-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-content/themes/himalayas/js/himalayas.js?time=1610648349" type="text/javascript"></script>
<script id="wp-embed-js" src="https://secureservercdn.net/104.238.69.231/zhi.8cb.myftpupload.com/wp-includes/js/wp-embed.js?time=1610648349" type="text/javascript"></script>
</body>
</html>
