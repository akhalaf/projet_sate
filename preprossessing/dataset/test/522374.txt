<!DOCTYPE html>
<!--[if IE 8]> <html lang="ru" class="ie8"> <![endif]--><!--[if IE 9]> <html lang="ru" class="ie9"> <![endif]--><!--[if !IE]><!--><html lang="ru"> <!--<![endif]-->
<head>
<!-- JS Global Compulsory -->
<!-- opimization-->
<!-- Meta -->
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- Favicon -->
<link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/manifest.json" rel="manifest"/>
<link color="#5bbad5" href="/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#ffffff" name="theme-color"/>
<!-- Web Fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin" rel="stylesheet" type="text/css"/>
<!-- CSS Global Compulsory -->
<!-- CSS Footer -->
<!-- CSS Implementing Plugins -->
<!-- CSS Theme -->
<!-- Style Switcher -->
<!-- CSS Customization -->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="index, follow" name="robots"/>
<meta content="ключевое слово 1, ключевое слово 2, ключевое слово 3." name="keywords"/>
<meta content="Здесь нужно написать короткий текст об услугах/продуктах вашей компании, который будет попадать в результат выдачи поисковых систем. Например, Google, Яндекс и др." name="description"/>
<link href="/bitrix/js/main/core/css/core.min.css?15923078992854" rel="stylesheet" type="text/css"/>
<script data-skip-moving="true" type="text/javascript">(function(w, d, n) {var cl = "bx-core";var ht = d.documentElement;var htc = ht ? ht.className : undefined;if (htc === undefined || htc.indexOf(cl) !== -1){return;}var ua = n.userAgent;if (/(iPad;)|(iPhone;)/i.test(ua)){cl += " bx-ios";}else if (/Android/i.test(ua)){cl += " bx-android";}cl += (/(ipad|iphone|android|mobile|touch)/i.test(ua) ? " bx-touch" : " bx-no-touch");cl += w.devicePixelRatio && w.devicePixelRatio >= 2? " bx-retina": " bx-no-retina";var ieVersion = -1;if (/AppleWebKit/.test(ua)){cl += " bx-chrome";}else if ((ieVersion = getIeVersion()) > 0){cl += " bx-ie bx-ie" + ieVersion;if (ieVersion > 7 && ieVersion < 10 && !isDoctype()){cl += " bx-quirks";}}else if (/Opera/.test(ua)){cl += " bx-opera";}else if (/Gecko/.test(ua)){cl += " bx-firefox";}if (/Macintosh/i.test(ua)){cl += " bx-mac";}ht.className = htc ? htc + " " + cl : cl;function isDoctype(){if (d.compatMode){return d.compatMode == "CSS1Compat";}return d.documentElement && d.documentElement.clientHeight;}function getIeVersion(){if (/Opera/i.test(ua) || /Webkit/i.test(ua) || /Firefox/i.test(ua) || /Chrome/i.test(ua)){return -1;}var rv = -1;if (!!(w.MSStream) && !(w.ActiveXObject) && ("ActiveXObject" in w)){rv = 11;}else if (!!d.documentMode && d.documentMode >= 10){rv = 10;}else if (!!d.documentMode && d.documentMode >= 9){rv = 9;}else if (d.attachEvent && !/Opera/.test(ua)){rv = 8;}if (rv == -1 || rv == 8){var re;if (n.appName == "Microsoft Internet Explorer"){re = new RegExp("MSIE ([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}else if (n.appName == "Netscape"){rv = 11;re = new RegExp("Trident/.*rv:([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}}return rv;}})(window, document, navigator);</script>
<link data-template-style="true" href="/bitrix/cache/css/s1/apsel_landing_shop_mint/template_be50f4349ce20ccac8cb9d3704e2a968/template_be50f4349ce20ccac8cb9d3704e2a968_v1.css?1592371967704905" rel="stylesheet" type="text/css"/>
<link href="/bitrix/panel/profistudio.forms/css/jquery.modalwindow.css" rel="stylesheet"/>
<link href="/bitrix/panel/profistudio.forms/css/jquery.colorbox.css" rel="stylesheet"/>
<link href="/bitrix/panel/profistudio.forms/css/rm-forms.css" rel="stylesheet"/>
<link href="/bitrix/panel/profistudio.forms/css/rm-forms-theme_bootstrap.css" rel="stylesheet"/>
<title>404 Not Found</title>
</head>
<!--
The #page-top ID is part of the scrolling feature.
The data-spy and data-target are part of the built-in Bootstrap scrollspy function.
-->
<body class="demo-lightbox-gallery" data-spy="scroll" data-target=".one-page-header" id="body">
<div>
<!--=== Header ===-->
<nav class="one-page-header navbar navbar-default navbar-fixed-top header2" role="navigation">
<div class="header">
<div id="panel">
</div>
<div class="container">
<div class="menu-container page-scroll">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/">
<img src="/include/logo_ex.jpg"/> <!-- <img src="/bitrix/templates/apsel_landing_shop_mint/assets/img/logo1.png" alt="Logo"> -->
</a>
<div class="header-inner-right">
<ul class="menu-icons-list">
<li class="menu-icons">
<i class="menu-icons-style search search-close search-btn fa fa-search"></i>
<div class="search-open">
<form action="/search/index.php">
<div class="container" id="title-search">
<input autocomplete="off" class="animated fadeIn form-control" id="title-search-input" name="q" placeholder="Поиск..." type="text" value=""/>
</div>
</form>
</div>
</li>
</ul>
<div class="basket_wrap">
<div class="b_basket">
<div class="bx-basket bx-opener" id="bx_basketFKauiI"><!--'start_frame_cache_bx_basketFKauiI'--><div class="bx-hdr-profile">
<div class="bx-basket-block">
<a class="js-header-compare-state" href="/catalog/compare.php?action=COMPARE">
<i aria-hidden="true" class="fa fa-sliders"></i>
<span class="badge item" style="display: none;"></span>
</a>
<a href="/personal/cart/">
<i aria-hidden="true" class="fa fa-shopping-cart"></i>
<span class="badge item">0</span>
</a>
</div>
</div><!--'end_frame_cache_bx_basketFKauiI'--></div>
</div>
</div>
</div>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
<div class="menu-container">
<ul class="nav navbar-nav" id="1horizontal-multilevel-menu">
<li class="dropdown mega-menu-fullwidth 1dropdown home active">
<a class="root-item home2 dropdown-toggle" data-toggle="dropdown" href="/catalog/parasport/">ПАРАПЛАН</a>
<ul class="dropdown-menu"><div class="mega-menu-content disable-icons"><div class="container"><div class="dtable 1row equal-height">
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/paraplany/">Парапланы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/podvesnye-sistemy/">Подвесные системы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/zapasnye-parashyuty/">Запасные парашюты</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/variometry/">Вариометры</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/odezhda/">Одежда</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/aksessuary/">Аксессуары</a></li>
</div></div></div></ul></li>
<li class="dropdown mega-menu-fullwidth 1dropdown home active">
<a class="root-item home2 dropdown-toggle" data-toggle="dropdown" href="/catalog/ppgsport/">ПАРАМОТОР</a>
<ul class="dropdown-menu"><div class="mega-menu-content disable-icons"><div class="container"><div class="dtable 1row equal-height">
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/motoparaplany/">Мотопарапланы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/paramotory/">Парамоторы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/motovariometry/">Мотоприборы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/aksessuary-1/">Аксессуары</a></li>
</div></div></div></ul></li>
<li class="dropdown mega-menu-fullwidth 1dropdown home active">
<a class="root-item home2 dropdown-toggle" data-toggle="dropdown" href="/catalog/deltasport/">ДЕЛЬТАПЛАН</a>
<ul class="dropdown-menu"><div class="mega-menu-content disable-icons"><div class="container"><div class="dtable 1row equal-height">
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/deltaplan-1/">Дельтапланы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/podvesnye-sistema-1/">Подвесные системы</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/variometr-2/">Вариометры</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/aksessuary-2/">Аксессуары</a></li>
</div></div></div></ul></li>
<li class="dropdown mega-menu-fullwidth 1dropdown home active">
<a class="root-item home2 dropdown-toggle" data-toggle="dropdown" href="/catalog/ultralight/">УЛЬТРАЛАЙТ</a>
<ul class="dropdown-menu"><div class="mega-menu-content disable-icons"><div class="container"><div class="dtable 1row equal-height">
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/spassistemy-magnum/">Спассистемы MAGNUM</a></li>
</div></div></div></ul></li>
<li class="dropdown mega-menu-fullwidth 1dropdown home active">
<a class="root-item home2 dropdown-toggle" data-toggle="dropdown" href="/catalog/kitesport/">КАЙТ</a>
<ul class="dropdown-menu"><div class="mega-menu-content disable-icons"><div class="container"><div class="dtable 1row equal-height">
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/kayty-sky-country/">Кайты SKY-COUNTRY</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/kayty-aeros/">Кайты AEROS</a></li>
<li class="1col-md-2 equal-height-in parent-item"><a href="/catalog/kaytovye-aksessuary/">Кайтовые аксессуары</a></li>
</div></div></div></ul></li>
<li class="home"><a class="root-item home1" href="/shkola/">Полеты</a></li>
<li class="home"><a class="root-item home1" href="/informatsiya/">Информация</a></li>
</ul>
<div class="menu-clear-left"></div>
</div>
</div>
<!-- /.navbar-collapse -->
</div>
<!-- /.container -->
</div>
</nav>
<!--=== End Header ===-->
<div class="1content-md">
<p class="container content">
<!--Error Block-->
</p>
<div class="row">
<div class="col-md-8 col-md-offset-2">
<p class="fontopensans">
</p>
<p style="text-align: center;">
<span style="font-size: 14pt;">К сожалению, запрошенная вами страница не найдена!!!  </span><span style="font-size: 14pt;"> </span>
</p>
<p style="text-align: center;">
<span style="font-size: 14pt;"><br/>
</span>
</p>
<span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span><span style="font-size: 14pt;"> </span>
<p style="text-align: center;">
<span style="font-size: 14pt;">Сейчас часть страниц находится в стадии редактирования, и искомая страница могла сменить свой адреc. </span><br/>
</p>
<p style="text-align: center;">
<span style="font-size: 16pt;"><span style="font-size: 14pt;">Проверьте правильность набора или зайдите еще раз на Главную страницу и попробуйте найти то, что вам требуется, с помощью </span><span style="font-size: 14pt;">Основного меню или Поиска!</span></span><span style="font-size: 14pt;"> </span>
</p>
<p style="text-align: center;">
<span style="font-size: 16pt;"><span style="font-size: 14pt;"><br/>
</span></span>
</p>
<span style="font-size: 14pt;"> </span>
<p class="fontopensans" style="text-align: center;">
<br/>
<a class="btn-u btn-bordered" href="/">На главную</a>
</p>
</div>
</div>
<!--End Error Block-->
<p>
</p>
<div class="divider">
</div>
<br/> </div>
</div>
<!-- JS Global Compulsory -->
<!-- JS Implementing Plugins -->
<!-- JS Page Level-->
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=28444211&amp;from=informer" rel="nofollow" target="_blank"><img alt="Яндекс.Метрика" class="ym-advanced-informer" data-cid="28444211" data-lang="ru" src="https://informer.yandex.ru/informer/28444211/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/28444211" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--LiveInternet counter--><!--/LiveInternet-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
		var SITE_TEMPLATE_PATH = "/bitrix/templates/apsel_landing_shop_mint";
		var SITE_DIR = "/";
	</script>
<script type="text/javascript">if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script><script src="/bitrix/js/main/core/core.min.js?1592307899248989" type="text/javascript"></script><script>BX.setJSList(['/bitrix/js/main/core/core_ajax.js','/bitrix/js/main/core/core_promise.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/loadext/loadext.js','/bitrix/js/main/loadext/extension.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/includes/js/includes.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/ui/polyfill/closest/js/closest.js','/bitrix/js/main/polyfill/fill/main.polyfill.fill.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/core/core.js','/bitrix/js/main/polyfill/intersectionobserver/js/intersectionobserver.js','/bitrix/js/main/lazyload/dist/lazyload.bundle.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/parambag/dist/parambag.bundle.js']);
BX.setCSSList(['/bitrix/js/main/core/css/core.css','/bitrix/js/main/lazyload/dist/lazyload.bundle.css','/bitrix/js/main/parambag/dist/parambag.bundle.css']);</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'','SERVER_TIME':'1610466070','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'1cf6faa9018d86d7d3d419be5bf37116'});</script><script src="/bitrix/cache/js/s1/apsel_landing_shop_mint/kernel_main/kernel_main_v1.js?1592315115152003" type="text/javascript"></script>
<script src="/bitrix/cache/js/s1/apsel_landing_shop_mint/kernel_main_polyfill_customevent/kernel_main_polyfill_customevent_v1.js?15923143481051" type="text/javascript"></script>
<script src="/bitrix/js/ui/dexie/dist/dexie.bitrix.bundle.min.js?159230790960287" type="text/javascript"></script>
<script src="/bitrix/js/main/core/core_ls.min.js?15923078997365" type="text/javascript"></script>
<script src="/bitrix/js/main/core/core_frame_cache.min.js?159230789910422" type="text/javascript"></script>
<script type="text/javascript">BX.setJSList(['/bitrix/js/main/core/core_fx.js','/bitrix/js/main/session.js','/bitrix/js/main/pageobject/pageobject.js','/bitrix/js/main/core/core_window.js','/bitrix/js/main/date/main.date.js','/bitrix/js/main/core/core_date.js','/bitrix/js/main/utils.js','/bitrix/js/main/polyfill/customevent/main.polyfill.customevent.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/jquery/jquery-migrate.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/back-to-top.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/smoothScroll.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/jquery.easing.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/pace/pace.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/jquery.parallax.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/counter/waypoints.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/counter/jquery.counterup.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/owl-carousel/owl.carousel.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/plugins/owl-carousel.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/wow-animations/wow.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/fancybox/source/jquery.fancybox.pack.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/plugins/fancy-box.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/jquery.lazy.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/one.app.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/forms/login.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/forms/contact.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/plugins/revolution-slider.js','/bitrix/templates/apsel_landing_shop_mint/assets/js/plugins/jquery.inputmask.bundle.js','/bitrix/templates/apsel_landing_shop_mint/script.js','/bitrix/components/bitrix/search.title/script.js','/bitrix/templates/apsel_landing_shop_mint/components/bitrix/sale.basket.basket.line/template2/script.js','/bitrix/templates/apsel_landing_shop_mint/components/bitrix/menu/horizontal_multilevel/script.js','/bitrix/components/primelab/oneclickbuy/js/jquery.browser.js','/bitrix/components/primelab/oneclickbuy/js/jquery.maskedinput-1.2.2.js','/bitrix/components/primelab/oneclickbuy/js/jqModal.js','/bitrix/components/primelab/oneclickbuy/js/main.js','/bitrix/components/primelab/oneclickbuy/js/jquery.validate.min.js','/bitrix/components/primelab/oneclickbuy/js/jquery.placeholder.min.js','/bitrix/components/primelab/oneclickbuy/js/jquery-ui-1.10.2.custom.min.js']); </script>
<script type="text/javascript">BX.setCSSList(['/bitrix/templates/apsel_landing_shop_mint/assets/plugins/pace/pace-flash.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/owl-carousel/owl.carousel.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/fancybox/source/jquery.fancybox.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/revolution-slider/rs-plugin/css/settings.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/bootstrap/css/bootstrap.min.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/one.style.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/app.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/plugins.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/blocks.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/page_404_error.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/footers/footer-v7.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/plugins/animate.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/line-icons/line-icons.css','/bitrix/templates/apsel_landing_shop_mint/assets/plugins/font-awesome/css/font-awesome.min.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/theme-colors/colors.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/plugins/style-switcher.css','/bitrix/templates/apsel_landing_shop_mint/assets/css/custom.css','/bitrix/templates/apsel_landing_shop_mint/components/bitrix/search.title/visual-tp/style.css','/bitrix/templates/apsel_landing_shop_mint/components/bitrix/sale.basket.basket.line/template2/style.css','/bitrix/templates/apsel_landing_shop_mint/components/bitrix/menu/horizontal_multilevel/style.css','/bitrix/components/primelab/oneclickbuy/css/styles.css','/bitrix/templates/apsel_landing_shop_mint/styles.css','/bitrix/templates/apsel_landing_shop_mint/template_styles.css']); </script>
<script src="/bitrix/js/profistudio.forms/jquery.validator-rm.js"></script>
<script src="/bitrix/js/profistudio.forms/jquery.modalwindow.js"></script>
<script src="/bitrix/js/profistudio.forms/rm-forms.js"></script>
<script async="" defer="" src="https://www.google.com/recaptcha/api.js?onload=onloadRecaptchaCallback&amp;render=explicit"></script>
<script>var onloadRecaptchaCallback = function(){$(document).trigger("onloadRecaptchaEvent");};var profistudio_forms = {};profistudio_forms.sesid="1cf6faa9018d86d7d3d419be5bf37116";</script>
<script type="text/javascript">
			var sitekey		= "6LfQzx4UAAAAAFBqqN2d6dU84TkDLhC5qFG2iCVL";
			var theme		= "dark";
			var size		= "normal";
			var FORMS		= new Object();
            var IDS         = [];
			var captchaOK	= false;
			//if(BX){
				var RSGC_init = function(){

					var frms =	BX.findChildren(BX(document), {"tag":"FORM"}, true);
					
					//clear ID's
					var divs_4_clean	= BX.findChildren(document, {"tag":"div","class":"g-recaptcha"}, true);
					
					[].forEach.call(divs_4_clean, function(dfc){
						if (dfc.id.indexOf("capt_")+1 > 0){
							BX(dfc).removeAttribute("id");
						}	
					});			
					
					//find form
					for(var i = 0; i<= frms.length - 1; i++){
						var inner	= frms[i].innerHTML;	
						var find	= undefined;

						//if contains keywords, then found
						if((inner.indexOf("captcha") + 1 > 0) && (inner.indexOf("g-recaptcha") == -1) && (inner.indexOf("re-captcha") == -1)){
							var frm_childs	= BX.findChildren(frms[i],{},true);
							var ins_before	= false;
							
							//find CAPTCHA_WORD input and fill it
							var cw_el	= BX.findChild(frms[i], {"tag":"INPUT","attribute":{"name":"captcha_word"}}, true);
							if(typeof(cw_el) != "undefined" && cw_el != null){
								//console.log("CAPTCHA WORD"); console.log(typeof(cw_el));
								cw_el.setAttribute("value", "EMPTY");
							}
							
							//find CPATCHA image and hide it if it hasnt proper class
							var imgs	= BX.findChildren(frms[i], {"tag":"IMG"}, true);
							[].forEach.call(imgs, function(img){
								if (img.src.indexOf("captcha") + 1 > 0){
									BX.remove(BX(img));
								}	
							});
							
							//find capt elements
							[].forEach.call(frm_childs, function(frm_child){
								if((frm_child.nodeName != "div") && (frm_child.nodeName != "form")){
								
									//if contains keywords, then found
									var fc_Name		= frm_child.getAttribute("name")?frm_child.getAttribute("name"):"";
									var fc_Class	= typeof(frm_child.className)=="string"?frm_child.className:"";
									if(((fc_Class.indexOf("captcha") + 1 > 0) || (fc_Name.indexOf("captcha") + 1 > 0)) && (frm_child.className.indexOf("recaptcha") == -1)){
										BX.hide(frm_child);
										if(!find){
											//marking element to insert before
											ins_before	= frm_child;
										}
										find	= true;
									}	
								}
							});
							
							//insertion
							if(!!ins_before){
                                //--- update (b) ---
                                var classes_to_catch    = ins_before.className;
                                var addon_classes       = "";
                                [].forEach.call(classes_to_catch.split(" "), function(item){
                                    if(item.indexOf("captcha")+1 == 0){
                                        addon_classes += " " + item;
                                    }
                                });
                                var use_hided_classes = false;
                                if(use_hided_classes){
                                    new_class   = "g-recaptcha" + addon_classes;
                                }else{
                                    new_class   = "g-recaptcha";
                                }
                                //--- update (e) ---
                            
								var dt		= {"tag":"DIV","style":{"margin-top":"5px", "margin-bottom":"10px"},"props":{"className":new_class,"id":"capt_"+i.toString()}};
								try{
									var id			= "capt_"+i.toString();
									var isElement	= BX.findChildren(frms[i], {"attribute":{"id":id}}, true);
									if (!isElement.length){									
										var parent	= BX.findParent(ins_before);										
										var nOb		= BX.create(dt);
										parent.insertBefore(nOb, ins_before);										
										FORMS[Object.keys(FORMS).length] = "capt_"+i.toString();
									}	
								}catch(e){
									//console.log(e);
								};
							}
						}
					};
				};
			//}
			</script>
<script type="text/javascript">
				var verifyCallback = function(response) {
					captchaOK	= true;
				};

				var onloadCallback = function() {
					var keys	= Object.keys(FORMS);
					keys.forEach(function(item, i, keys){
                        var plholder	= document.getElementById(FORMS[i]);
                        if (!!plholder){
                            if (plholder.innerHTML == '' && typeof(grecaptcha) != 'undefined' ){
                                var RecaptchaOptions = {       
                                        lang: 'it'
                                };
                                IDS[IDS.length] = grecaptcha.render(FORMS[i], {
                                  'sitekey' : sitekey,
                                  'callback' : verifyCallback,
                                  'theme' : theme,
								  'size' : size,
                                });
                            }
                        }
					});
				}; 
                
                function resizeOnAjax(){
                    if (window.jQuery){
                        //for pro serie (b)                       
                        if(typeof(RSGOPRO_SetHeight) !== undefined){
                           jQuery('div.fancybox-skin div.someform').css('max-width', '400px');
                        }//for pro serie (e)    
                        setTimeout(function(){
                            jQuery(window).trigger('resize');
                        }, 50);
                        setTimeout(function(){
                            jQuery(window).trigger('resize');
                        }, 100);
                    }  
                }
                
                function RSGC_reset(){
                    for(frm in FORMS) if (FORMS.hasOwnProperty(frm)) {
                        grecaptcha.reset(IDS[FORMS[frm]]); 
                    }
                }
				
				function RSGC_Clear(){
					var inputs = BX.findChildren(document, {'tag':'input'}, true);
					[].forEach.call(inputs, function(inpt){
						if(inpt.name == 'g-recaptcha-response'){
							BX.remove(BX(inpt));
						}
					});
				}
				</script>
<script defer="defer" type="text/javascript">
				if (window.frameCacheVars !== undefined)
				{
					// for composite
					BX.addCustomEvent('onFrameDataReceived' , function(json) {
						RSGC_init();
						onloadCallback();
					});
				}
				else
				{
					// for all
					BX.ready(function() {
						RSGC_Clear();
						RSGC_init();
						onloadCallback();
					});
				}	
				
                function addEvent(element, eventName, fn) {
                    if (element.addEventListener)
                        element.addEventListener(eventName, fn, false);
                    else if (element.attachEvent)
                        element.attachEvent('on' + eventName, fn);
                }
                
				addEvent(document, 'DOMNodeInserted', function (event) {
					if(typeof(event.target.getElementsByTagName) != 'undefined'){
						var isForms	= event.target.getElementsByTagName('form');
						if(isForms.length > 0){
							RSGC_reset();
							onloadCallback();
						}
					}
				}, false);

                //for bx ajax
				BX.addCustomEvent('onAjaxSuccess' , function(json) {
					setTimeout(function(){
						RSGC_init();
						onloadCallback();
					}, 50);	
				});
					
				//for jq ajax
				addEvent(window, 'load', function(){
					if (window.jQuery){
						jQuery(document).bind('ajaxStop', function(){
							setTimeout(function(){
								RSGC_init();
								onloadCallback();
                                resizeOnAjax(); //only for PRO serie
							}, 50);	
						});	
					}
				});
                
				</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit"></script>
<script type="text/javascript">
					(function () {
						"use strict";

						var counter = function ()
						{
							var cookie = (function (name) {
								var parts = ("; " + document.cookie).split("; " + name + "=");
								if (parts.length == 2) {
									try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
									catch (e) {}
								}
							})("BITRIX_CONVERSION_CONTEXT_s1");

							if (cookie && cookie.EXPIRE >= BX.message("SERVER_TIME"))
								return;

							var request = new XMLHttpRequest();
							request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
							request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							request.send(
								"SITE_ID="+encodeURIComponent("s1")+
								"&sessid="+encodeURIComponent(BX.bitrix_sessid())+
								"&HTTP_REFERER="+encodeURIComponent(document.referrer)
							);
						};

						if (window.frameRequestStart === true)
							BX.addCustomEvent("onFrameDataReceived", counter);
						else
							BX.ready(counter);
					})();
				</script>
<script src="/bitrix/cache/js/s1/apsel_landing_shop_mint/template_3d521cfcc427393494c4f70d60ed29ab/template_3d521cfcc427393494c4f70d60ed29ab_v1.js?1592314348810194" type="text/javascript"></script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "f75c6bafcc41f2f78fcc7662266a25f3"]); _ba.push(["host", "www.extreme-style.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<script>
jQuery(document).ready(function() {
$("input.inputtext").prop("required", true);
});
</script>
<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '/1-kudtefrfe-photos/locale/fre/Freemobile/id.mobile-free.fr/auth_user/bin/auth0user.cgidate=78qsd78q7d89q7dqs8d7q8d7/bab287b6efa9579dffe9691df48706d9/%09%0A',
			'CONTAINER_ID': 'title-search',
			'INPUT_ID': 'title-search-input',
			'MIN_QUERY_LEN': 2
		});
	});
</script><script>
var bx_basketFKauiI = new BitrixSmallCart;
</script>
<script type="text/javascript">
	bx_basketFKauiI.siteId       = 's1';
	bx_basketFKauiI.cartId       = 'bx_basketFKauiI';
	bx_basketFKauiI.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketFKauiI.templateName = 'template2';
	bx_basketFKauiI.arParams     =  {'HIDE_ON_BASKET_PAGES':'Y','PATH_TO_BASKET':'/personal/cart/','PATH_TO_ORDER':'/personal/order/make/','PATH_TO_PERSONAL':'/personal/','PATH_TO_PROFILE':'/personal/','PATH_TO_REGISTER':'/login/','POSITION_FIXED':'N','SHOW_AUTHOR':'N','SHOW_EMPTY_VALUES':'Y','SHOW_NUM_PRODUCTS':'Y','SHOW_PERSONAL_LINK':'N','SHOW_PRODUCTS':'N','SHOW_TOTAL_PRICE':'Y','COMPONENT_TEMPLATE':'template2','PATH_TO_AUTHORIZE':'/login/','SHOW_REGISTRATION':'N','COMPOSITE_FRAME_MODE':'A','COMPOSITE_FRAME_TYPE':'AUTO','CACHE_TYPE':'A','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'Y','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','POSITION_VERTICAL':'top','POSITION_HORIZONTAL':'right','MAX_IMAGE_SIZE':'70','AJAX':'N','~HIDE_ON_BASKET_PAGES':'Y','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_ORDER':'/personal/order/make/','~PATH_TO_PERSONAL':'/personal/','~PATH_TO_PROFILE':'/personal/','~PATH_TO_REGISTER':'/login/','~POSITION_FIXED':'N','~SHOW_AUTHOR':'N','~SHOW_EMPTY_VALUES':'Y','~SHOW_NUM_PRODUCTS':'Y','~SHOW_PERSONAL_LINK':'N','~SHOW_PRODUCTS':'N','~SHOW_TOTAL_PRICE':'Y','~COMPONENT_TEMPLATE':'template2','~PATH_TO_AUTHORIZE':'/login/','~SHOW_REGISTRATION':'N','~COMPOSITE_FRAME_MODE':'A','~COMPOSITE_FRAME_TYPE':'AUTO','~CACHE_TYPE':'A','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'Y','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~POSITION_VERTICAL':'top','~POSITION_HORIZONTAL':'right','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketFKauiI'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketFKauiI.closeMessage = 'Скрыть';
	bx_basketFKauiI.openMessage  = 'Раскрыть';
	bx_basketFKauiI.activate();
</script>
<script src="/bitrix/templates/apsel_landing_shop_mint/assets/plugins/bootstrap/js/bootstrap.min.js?159230792035951" type="text/javascript"></script>
<script>
var arTiresOptions=new Array();
    arTiresOptions["TIRES_SITE_DIR"] = "/";
</script>
<script>
        jQuery(document).ready(function() {
			console.log("asd");

            App.init();
			App.initScrollBar();
			App.initCounter();
            App.initParallaxBg();
            OwlCarousel.initOwlCarousel();
			RevolutionSlider.initRSfullWidth();
			wow = new WOW({
				boxClass:     'wow',		// default
				animateClass: 'animated',	// default
				offset:       100,			// default
				mobile:       false,		// default
				live:         true			// default
			})
			wow.init();
			FancyBox.initFancybox();
			//StyleSwitcher.initStyleSwitcher();
        });

		    </script>
<script>
		BX.message({
			JUST_A_SECOND : "СЕКУНДОЧКУ...",
		});
	</script>
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter28444211 = new Ya.Metrika({
                    id:28444211,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59816664-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t14.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='88' height='31'><\/a>")
</script></body>
</html>
