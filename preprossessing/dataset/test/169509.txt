<!DOCTYPE html>
<html lang="fi">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet"/>
<title>Audiclub Finland</title>
<link href="//use.fontawesome.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.audiclub.fi/feed/" rel="alternate" title="Audiclub Finland » syöte" type="application/rss+xml"/>
<link href="https://www.audiclub.fi/comments/feed/" rel="alternate" title="Audiclub Finland » kommenttien syöte" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.audiclub.fi\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.audiclub.fi/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.audiclub.fi/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=2.4.5" id="wc-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link href="https://www.audiclub.fi/wp-content/themes/acf/assets/css/bootstrap.min.css?ver=v4.1.0" id="bootstrap-4-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.audiclub.fi/wp-content/themes/acf/style.css?ver=1.0.2" id="acf-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css?ver=5.5.3" id="fa-iconset-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.audiclub.fi/wp-content/themes/acf/woocommerce.css?ver=5.5.3" id="wp-bootstrap-4-woocommerce-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-bootstrap-4-woocommerce-style-inline-css" type="text/css">
@font-face {
			font-family: "star";
			src: url("https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/fonts/star.eot");
			src: url("https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/fonts/star.eot?#iefix") format("embedded-opentype"),
				url("https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/fonts/star.woff") format("woff"),
				url("https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/fonts/star.ttf") format("truetype"),
				url("https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/fonts/star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}
</style>
<link href="https://www.audiclub.fi/wp-content/plugins/add-to-any/addtoany.min.css?ver=1.15" id="addtoany-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.audiclub.fi/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="addtoany-js" src="https://www.audiclub.fi/wp-content/plugins/add-to-any/addtoany.min.js?ver=1.1" type="text/javascript"></script>
<link href="https://www.audiclub.fi/wp-json/" rel="https://api.w.org/"/><link href="https://www.audiclub.fi/wp-json/wp/v2/pages/96" rel="alternate" type="application/json"/><link href="https://www.audiclub.fi/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.audiclub.fi/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<meta content="WooCommerce 3.8.1" name="generator"/>
<link href="https://www.audiclub.fi/" rel="canonical"/>
<link href="https://www.audiclub.fi/" rel="shortlink"/>
<link href="https://www.audiclub.fi/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.audiclub.fi%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://www.audiclub.fi/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.audiclub.fi%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<script data-cfasync="false">
window.a2a_config=window.a2a_config||{};a2a_config.callbacks=[];a2a_config.overlays=[];a2a_config.templates={};a2a_localize = {
	Share: "Share",
	Save: "Save",
	Subscribe: "Subscribe",
	Email: "Email",
	Bookmark: "Bookmark",
	ShowAll: "Show all",
	ShowLess: "Show less",
	FindServices: "Find service(s)",
	FindAnyServiceToAddTo: "Instantly find any service to add to",
	PoweredBy: "Powered by",
	ShareViaEmail: "Share via email",
	SubscribeViaEmail: "Subscribe via email",
	BookmarkInYourBrowser: "Bookmark in your browser",
	BookmarkInstructions: "Press Ctrl+D or \u2318+D to bookmark this page",
	AddToYourFavorites: "Add to your favorites",
	SendFromWebOrProgram: "Send from any email address or email program",
	EmailProgram: "Email program",
	More: "More&#8230;",
	ThanksForSharing: "Thanks for sharing!",
	ThanksForFollowing: "Thanks for following!"
};

(function(d,s,a,b){a=d.createElement(s);b=d.getElementsByTagName(s)[0];a.async=1;a.src="https://static.addtoany.com/menu/page.js";b.parentNode.insertBefore(a,b);})(document,"script");
</script>
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<style type="text/css">
				.wb-bp-front-page .wp-bs-4-jumbotron {
					background-image: url(https://www.audiclub.fi/wp-content/uploads/2019/04/IMG_9642-copy.jpg);
				}
			</style>
<style type="text/css">
					.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
				</style>
<link href="https://www.audiclub.fi/wp-content/uploads/2018/06/cropped-logo_acf_512-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.audiclub.fi/wp-content/uploads/2018/06/cropped-logo_acf_512-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.audiclub.fi/wp-content/uploads/2018/06/cropped-logo_acf_512-180x180.png" rel="apple-touch-icon"/>
<meta content="https://www.audiclub.fi/wp-content/uploads/2018/06/cropped-logo_acf_512-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="home page-template-default page page-id-96 wp-custom-logo theme-acf woocommerce-no-js wb-bp-front-page woocommerce-active">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header sticky-top" id="masthead">
<nav class="main-navigation navbar navbar-expand-lg navbar-light" id="site-navigation">
<div class="container">
<a aria-current="page" class="custom-logo-link" href="https://www.audiclub.fi/" rel="home"><img alt="Audiclub Finland" class="custom-logo" height="216" sizes="(max-width: 385px) 100vw, 385px" src="https://www.audiclub.fi/wp-content/uploads/2018/06/ACF_logo.png" srcset="https://www.audiclub.fi/wp-content/uploads/2018/06/ACF_logo.png 385w, https://www.audiclub.fi/wp-content/uploads/2018/06/ACF_logo-300x168.png 300w" width="385"/></a>
<div class="site-branding-text">
</div>
<button aria-controls="primary-menu-wrap" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#primary-menu-wrap" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="primary-menu-wrap"><ul class="navbar-nav ml-auto" id="primary-menu"><li class="nav-item active menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a class="nav-link" href="http://www.audiclub.fi/" target="_self"><u>Etusivu</u></a></li>
<li class="nav-item dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.audiclub.fi/kerho/">Kerho</a><ul class="dropdown-menu depth_0"><li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/kerho/historia/" target="_self">Historia</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/kerho/liity-jaseneksi/" target="_self">Liity jäseneksi</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/kerho/rekisteriseloste-ja-saannot/" target="_self">Rekisteriseloste ja säännöt</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/kerho/tapahtumat/" target="_self">Tapahtumat</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/kerho/yhteystiedot/" target="_self">Yhteystiedot</a></li>
</ul>
</li>
<li class="nav-item dropdown menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.audiclub.fi/verkkokauppa/">Verkkokauppa</a><ul class="dropdown-menu depth_0"><li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/verkkokauppa/" target="_self">Tuotteet</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/verkkokauppa/toimitusehdot/" target="_self">Toimitusehdot</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy"><a class="dropdown-item" href="https://www.audiclub.fi/verkkokauppa/verkkokaupan-rekisteriseloste/" target="_self">Rekisteriseloste</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/verkkokauppa/ostoskori/" target="_self">Ostoskori</a></li>
<li class="nav-item menu-item menu-item-type-post_type menu-item-object-page"><a class="dropdown-item" href="https://www.audiclub.fi/verkkokauppa/kassa/" target="_self">Kassa</a></li>
</ul>
</li>
<li class="nav-item menu-item menu-item-type-taxonomy menu-item-object-category"><a class="nav-link" href="https://www.audiclub.fi/category/artikkelit/" target="_self">Artikkelit</a></li>
<li class="nav-item menu-item menu-item-type-custom menu-item-object-custom"><a class="nav-link" href="https://acf.kuvat.fi/kuvat/" target="_blank">Kuvagalleria</a></li>
<li class="nav-item menu-item menu-item-type-custom menu-item-object-custom"><a class="nav-link" href="http://www.audiclub.fi/audifinns" target="_blank">Keskustelufoorumi</a></li>
<li class="nav-item navbar-special menu-item menu-item-type-post_type menu-item-object-page"><a class="nav-link" href="https://www.audiclub.fi/kerho/liity-jaseneksi/" target="_self">Liity jäseneksi</a></li>
</ul></div> </div><!-- /.container -->
</nav><!-- #site-navigation -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<section class="jumbotron wp-bs-4-jumbotron border-bottom">
<div class="wp-bp-jumbo-overlay">
</div>
<div class="row" style="height: 6px;">
<div class="color-column col-3" style="background-color: #E40039;">
</div>
<div class="color-column col-3" style="background-color: #B6B1A9;">
</div>
<div class="color-column col-3" style="background-color: #B3B3B3;">
</div>
<div class="color-column col-3" style="background-color: black;">
</div> </div>
</section>
<section class="main-content">
<div class="container">
<div class="row">
<div class="col-md-8">
<h2 class="mb-4">Audiclub Finland – nuoreen aikuiseen ikään ja aikuiseen makuun</h2>
<p>Audiclubin toimintaan osallistuvan jäsenistön määrä on vakiintunut noin 2.000 henkilöön. Tapahtumakirjo on laaja. Kesäkauden ratatapahtumien lisäksi järjestämme talvella sekä ajokoulutusta että jääratatapahtumia, osallistumme näyttelyihin, järjestämme yhteisiä ajeluretkiä ja muita aktiviteettejä.</p>
<p>Kotimaiset moottoriradat ovat tulleet matkan varrella hyvin tutuiksi. Ulkomaisista radoista tapahtumapaikkoina ovat puolestaan olleet Pärnu, Gotlandring, Nürburgring ja Spa.</p>
<p>Audifinns-keskustelufoorumi on merkittävä osa toimintaamme. Laajasta sosiaalisen median tarjonnasta huolimatta Audifinns-keskustelufoorumin käyttö on jatkunut aktiivisena. Foorumin sisällöstä voi hakusanojen avulla löytää mielettömän määrän erilaisia teknisiä vinkkejä ja muita hyödyllisiä asioita.</p>
<p>Liity mukaan Audiclubiin ja tule rohkeasti mukaan rentoihin tapahtumiin tapaamaan samanhenkisiä ihmisiä. Ei kannata ujostella radalle tai jääradalle lähtöä – siksi tapahtumia järjestetään, että kuljettajat oppivat tuntemaan paremmin sekä omat että autonsa käytöstavat.</p>
<p>Erinomaisen hyviä quattrokelejä ja turvallisia ajokokemuksia kaikille Audiclubin jäsenille!</p>
<p><em>Hannu Mäntyharju<br/>
puheenjohtaja</em></p>
<h2>ACF tapahtumat</h2>
<p>Tapahtumat ovat suunnattu kerhomme jäsenille ja ne ovat pääasiassa maksuttomia. Ajankohtaiset tiedot ja keskustelua tapahtumista löydät keskustelufoorumin Jäsentiedotus- sekä Tapahtumat -osioista. Audiclubilaisia voit nähdä myös Tapahtumat-osiossa mainituissa alueellisissa tapahtumissa.</p>
<p>Tapahtumakalenteri elää sitä myötä kun tapahtumia saadaan varmistettua.</p>
<p><strong>KAUSI 2021</strong></p>
<p>30.1. Jämi ACF Winter Driving School (<a href="https://www.audiclub.fi/audifinns/forum/kerho-tapahtumat/kerhon-tapahtumat/1905887-audiclub-winter-driving-school-30-1-2021" rel="noopener noreferrer" target="_blank">foorumilinkki</a>)<br/>
26.-27.2. Rovaniemi (<a href="https://www.audiclub.fi/audifinns/forum/kerho-tapahtumat/kerhon-tapahtumat/1905880-rovaniemi-snow-rally-rings-26-27-2-2021" rel="noopener noreferrer" target="_blank">foorumilinkki</a>)<br/>
Maaliskuu (kelivaraus) Mäntyharjun jäärata<br/>
15.5. Alastaro<br/>
19.6. Kesäkruising<br/>
19.7. KymiRing<br/>
Elokuussa (odottaa vahvistusta) Ahvenisto<br/>
11.9. Pärnu<br/>
Lokakuussa (odottaa vahvistusta) Kalpalinna</p>
<hr/>
</div>
<div class="col-md-4 some-sidebar">
<div class="d-none d-md-block" style="border:2px solid #e40039; position: absolute; left: 0; height: 100px;"></div>
<h2> ACF sosiaalisessa mediassa </h2>
<p>
<i class="fab fa-instagram icon-balloon"></i>
<a class="text-dark" href="https://www.instagram.com/audiclubfinland/" target="_blank">Instagram @audiclubfinland</a>
</p>
                                                            Instagram has returned invalid data.                                                        <p>
<i class="fab fa-youtube icon-balloon"></i>
<a class="text-dark" href="https://www.youtube.com/AudiclubFinland" target="_blank">Youtube.com/audiclubfinland</a>
</p>
<p>
<i class="fab fa-facebook-f icon-balloon"></i>
<a class="text-dark" href="https://www.facebook.com/audiclubfinland/" target="_blank">Facebook.com/audiclubfinland</a>
</p>
<p>
<i class="fab fa-twitter icon-balloon"></i>
<a class="text-dark" href="https://www.twitter.com/audifinns" target="_blank">Twitter @audifinns</a>
</p>
<p>
<i class="far fa-comments icon-balloon"></i>
<a class="text-dark" href="http://www.audiclub.fi/audifinns" target="_blank">Audifinns-keskustelufoorumi</a>
</p>
</div>
<div class="col-md-8">
<h2>Viimeisimmät artikkelit</h2>
<div class="row">
<div class="col-md-4">
<span>24.1.2020</span>
<h3><a href="https://www.audiclub.fi/jaaratapaivat-rovaniemella-22-23-2-2019/">Jääratapäivät Rovaniemellä 2019</a></h3>
<div class="post-entry">
<p>Huutonaurua vitilumessa Pitkään oli jo tiedossa siirtyminen pois Tahkon maisemista. Tämän tiedostaen kerho olikin jo aikaisessa vaiheessa tehnyt varauksen Rovaniemelle, tarkemmin katsottuna lähelle Vanttauskoskea. Ratoja ylläpitävänä tahona toimii SnowRallyRings. Radat… <br/><a class="moretag" href="https://www.audiclub.fi/jaaratapaivat-rovaniemella-22-23-2-2019/" style="font-style: italic;">(Lue koko artikkeli)</a></p>
</div>
</div>
<div class="col-md-4">
<span>30.7.2019</span>
<h3><a href="https://www.audiclub.fi/kesacruising-2019/">Kesäcruising 2019</a></h3>
<div class="post-entry">
<p>Audiclubin kymmenes kesäcruising oli kesäkuun puolivälissä. Lähtöpaikkana toimi ryhmällemme erikoistilauksesta normaalia aiemmin auennut Cafe Muurla. Paikka valikoitui sijaintinsa ja riittävien pysäköintitilojensa vuoksi. Pysäköintitilaa tarvittiinkin, sillä laskin että letkassa olisi ollut… <br/><a class="moretag" href="https://www.audiclub.fi/kesacruising-2019/" style="font-style: italic;">(Lue koko artikkeli)</a></p>
</div>
</div>
<div class="col-md-4">
<span>28.2.2019</span>
<h3><a href="https://www.audiclub.fi/acf-winter-driving-school-2019-jami/">ACF Winter Driving School 2019 – Jämi</a></h3>
<div class="post-entry">
<p>Perinteisesti Joutsassa pidetty Audiclub Winter Driving School siirtyi kaudella 2019 Satakuntaan Jämille. Joutsalla on ollut oma paikkansa Audiclubin talvitapahtumapaikkana vuosikausia. Tapahtuman luonne oli alkuun oma-aloitteista ajoharjoittelua ilman ohjausta, mutta jossain… <br/><a class="moretag" href="https://www.audiclub.fi/acf-winter-driving-school-2019-jami/" style="font-style: italic;">(Lue koko artikkeli)</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div><!-- #content -->
<footer class="navbar-expand site-footer text-center bg-white text-muted d-md-block w-100" id="colophon">
<div class="container-fluid">
<div class="row" style="height: 80px; bottom: 0">
<div class="color-column col-12 col-md-3 col-sm-6" style="background-color: #E40039;">
<a href="//www.audiclub.fi">
<img alt="audiclub.fi" src="https://www.audiclub.fi/wp-content/themes/acf/assets/images/audiclub_white.png" style="max-height: 30%; width: auto"/>
</a>
</div>
<div class="color-column col-12 col-md-3 col-sm-6" style="background-color: #B6B1A9;">
<a href="//www.audiclub.fi/audifinns/" target="_blank">
<img alt="Audifinns" src="https://www.audiclub.fi/wp-content/themes/acf/assets/images/audifinns_logo.png"/>
</a>
</div>
<div class="color-column col-12 col-md-3 col-sm-6" style="background-color: #B3B3B3;">
<a href="//www.audiclub.fi/kerho/liity-jaseneksi/">
<p>
							Liity jäseneksi tästä!
						</p>
</a>
</div>
<div class="color-column col-12 col-md-3 col-sm-6" style="background-color: black">
<div class="footer-some">
<p>
<a href="https://www.facebook.com/audiclubfinland/" target="_blank">
<i class="fab fa-facebook-f"></i>
</a>
<a href="https://www.instagram.com/audiclubfinland/" target="_blank">
<i class="fab fa-instagram"></i>
</a>
<a href="https://twitter.com/audifinns" target="_blank">
<i class="fab fa-twitter"></i>
</a>
<a href="https://www.youtube.com/AudiclubFinland" target="_blank">
<i class="fab fa-youtube"></i>
</a>
<a href="http://www.audiclub.fi/audifinns" target="_blank">
<i class="far fa-comments"></i>
</a>
</p>
<a href="http://www.juhokomulainen.fi" target="_blank">
<img alt="juhokomulainen.fi" src="https://www.audiclub.fi/wp-content/themes/acf/assets/images/carved_by_juhokomulainen.png" style="position:absolute; bottom: 4px; right:4px; max-width: 100px; height: auto;"/>
</a>
</div>
</div>
</div>
</div>
</footer><!-- #colophon -->
<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
<script id="jquery-blockui-js" src="https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70" type="text/javascript"></script>
<script id="wc-add-to-cart-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"N\u00e4yt\u00e4 ostoskori","cart_url":"https:\/\/www.audiclub.fi\/verkkokauppa\/ostoskori\/","is_cart":"","cart_redirect_after_add":"yes"};
/* ]]> */
</script>
<script id="wc-add-to-cart-js" src="https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.8.1" type="text/javascript"></script>
<script id="js-cookie-js" src="https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4" type="text/javascript"></script>
<script id="woocommerce-js-extra" type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script id="woocommerce-js" src="https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.8.1" type="text/javascript"></script>
<script id="wc-cart-fragments-js-extra" type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_7f96663888369c1da8af321aac6b110b","fragment_name":"wc_fragments_7f96663888369c1da8af321aac6b110b","request_timeout":"5000"};
/* ]]> */
</script>
<script id="wc-cart-fragments-js" src="https://www.audiclub.fi/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.8.1" type="text/javascript"></script>
<script id="wc-cart-fragments-js-after" type="text/javascript">
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
		} );
	
</script>
<script id="bootstrap-4-js-js" src="https://www.audiclub.fi/wp-content/themes/acf/assets/js/bootstrap.min.js?ver=v4.1.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.audiclub.fi/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
