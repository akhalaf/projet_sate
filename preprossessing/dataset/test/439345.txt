<!DOCTYPE html>
<html lang="en">
<head>
<title>Таны хайсан хуудас олдсонгүй | American Dress</title>
<meta charset="utf-8"/>
<meta content="#b02538" name="theme-color"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="2355843981140282" property="fb:app_id"/>
<link href="/theme/dress-2018/images/favicon.png?9374b42bca" rel="icon" type="image/png"/>
<link href="/theme/dress-2018/images/favicon.png?9374b42bca" rel="apple-touch-icon"/>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/theme/dress-2018/styles/main.css?e8aeb94f79" rel="stylesheet"/>
<link href="/theme/dress-2018/styles/responsive.css?fb5354ddd4" rel="stylesheet"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-72597717-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-72597717-4');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '282886845799617'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" src="https://www.facebook.com/tr?id=282886845799617&amp;ev=PageView
&amp;noscript=1" width="1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Hotjar Tracking Code for http://www.dress.mn/ -->
<script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:959771,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<meta content="Bolt" name="generator"/>
</head>
<body>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
var js,
fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id))
return;
js = d.createElement(s);
js.id = id;
js.src = "//connect.facebook.net/mn_MN/sdk.js#xfbml=1&version=v2.10&appId=2355843981140282";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<header id="header">
<div class="header-top">
<div class="container py-2">
<div class="row">
<div class="col-sm-6 text-left d-none d-sm-block">Dress.mn - Америкаас ирсэн брэндийн даашинз</div>
<div class="col-sm-6 text-center text-sm-right">
<ul class="list-unstyled list-inline m-0">
<li class="list-inline-item"><a href="//fb.com/dressmn" target="_blank"><i class="fab fa-facebook-square"></i> fb.com/dressmn</a></li>
<li class="list-inline-item"><a href="mailto:info@dress.mn" target="_blank"><i class="fas fa-envelope-square"></i> info@dress.mn</a></li>
<li class="list-inline-item"><i class="fas fa-phone-square-alt"></i> 9909-7721</li>
</ul>
</div>
</div>
</div>
</div>
<div class="header-bottom">
<div class="container py-2 py-md-3 py-lg-4">
<div class="row align-items-center">
<div class="col-9 col-md-12"><a href="//www.dress.mn"><img class="img-fluid logo" src="/theme/dress-2018/images/header-logo.png?0b7efd4055" srcset="/theme/dress-2018/images/header-logo.png?0b7efd4055 1x, /theme/dress-2018/images/header-logo@2x.png?c2fd4afda1 2x"/></a> </div>
<div class="col-3 d-block d-md-none text-right">
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"> <span class="navbar-toggler-icon"></span> </button>
</div>
</div>
</div>
</div>
</header>
<nav class="navbar navbar-expand-md">
<div class="container">
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ">
<li class="nav-item "><a class="nav-link " href="/" title="">Нүүр хуудас</a></li>
<li class="nav-item "><a class="nav-link " href="/products" title="">Бүх бараанууд</a></li>
<li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="" id="navbarDropdownMenuLink" title="">Тусламж</a><div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu"><a class="dropdown-item " href="/page/company" title="">Компанийн танилцуулга</a><a class="dropdown-item " href="/page/terms-of-use" title="">Үйлчилгээний ерөнхий нөхцөл</a><a class="dropdown-item " href="/page/faqs" title="">Асуулт, хариулт</a><a class="dropdown-item " href="/page/financing" title="">Лизингээр бараа авах</a><a class="dropdown-item " href="/page/why-us" title="">Яагаад биднийг сонгох хэрэгтэй вэ?</a></div></li>
</ul>
<ul class="navbar-nav ml-auto">
<li class="nav-item "><a class="nav-link " href="/page/reviews" title="">Хэрэглэгчдийн сэтгэгдэл</a></li>
<li class="nav-item "><a class="nav-link " href="/page/contacts" title="">Холбоо барих</a></li>
</ul>
</div>
</div>
</nav>
<section class="error spacer">
<div class="container">
<h1>
<div class="h1-ghost">Алдаа 404</div>
<div class="h1-main">Таны хайсан хуудас олдсонгүй</div>
</h1>
<hr class="dotted-hr"/>
<p>Таны бичсэн хаяг байхгүй байна. Та хаягаа шалгаад дахин оролно уу. Баярлалаа.</p>
<br/>
<a class="btn btn-primary" href="/"><i aria-hidden="true" class="fa fa-home"></i> Нүүр хуудасруу буцах</a> </div>
</section>
<footer class="section-footer">
<div class="container py-3 py-md-4 text-center">
<ul class="list-unstyled list-inline ">
<li class="mb-2 mb-md-0 mr-md-4 ml-md-3 d-block d-md-inline list-inline-item "><a href="/">Нүүр хуудас</a></li>
<li class="mb-2 mb-md-0 mr-md-4 ml-md-3 d-block d-md-inline list-inline-item "><a href="/products">Бүх бараанууд</a></li>
<li class="mb-2 mb-md-0 mr-md-4 ml-md-3 d-block d-md-inline list-inline-item "><a href="/page/reviews">Хэрэглэгчдийн сэтгэгдэл</a></li>
<li class="mb-2 mb-md-0 mr-md-4 ml-md-3 d-block d-md-inline list-inline-item "><a href="/page/contacts">Холбоо барих</a></li>
</ul>
<img class="my-3" src="/theme/dress-2018/images/footer-logo.png?e065a40214" srcset="/theme/dress-2018/images/footer-logo.png?e065a40214 1x, /theme/dress-2018/images/footer-logo@2x.png?40fbe60a48 2x"/>
<p class="copyright">© 2010-2019 Dress.mn</p>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.Marquee/1.5.0/jquery.marquee.min.js"></script>
<script src="/theme/dress-2018/scripts/main.js?e8f6a262a9"></script>
<script src="//www.dress.mn/livechat/php/app.php?widget-init.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {
$("#order_orderlink").val('');
});
</script>
</body>
</html>
