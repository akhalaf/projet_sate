<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="es-es" xml:lang="es-es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="es-es" name="language"/>
<title>404 - Error: 404</title>
<link href="/templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/position.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/layout.css" media="screen,projection" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/print.css" media="Print" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/general.css" rel="stylesheet" type="text/css"/>
<link href="/templates/beez_20/css/.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 6]>
			<link href="/templates/beez_20/css/ieonly.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<!--[if IE 7]>
			<link href="/templates/beez_20/css/ie7only.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<style type="text/css">
			<!--
			#errorboxbody
			{margin:30px}
			#errorboxbody h2
			{font-weight:normal;
			font-size:1.5em}
			#searchbox
			{background:#eee;
			padding:10px;
			margin-top:20px;
			border:solid 1px #ddd
			}
			-->
</style>
</head>
<body>
<div id="all">
<div id="back">
<div id="header">
<div class="logoheader">
<h1 id="logo">
<img alt="Inventiaplus " src="/images/sampledata/fruitshop/fruits.gif"/>
<span class="header1">
                                        Inventiaplus                                        </span></h1>
</div><!-- end logoheader -->
<ul class="skiplinks">
<li><a class="u2" href="#wrapper2">TPL_BEEZ2_SKIP_TO_ERROR_CONTENT</a></li>
<li><a class="u2" href="#nav">TPL_BEEZ2_ERROR_JUMP_TO_NAV</a></li>
</ul>
<div id="line"></div>
</div><!-- end header -->
<div id="contentarea2">
<div class="left1" id="nav">
<h2 class="unseen">TPL_BEEZ2_NAVIGATION</h2>
<ul class="nav menu">
<li class="item-114 divider"><span class="separator ">Copyright @ Electro Mecánicos Rodritol SL</span>
</li><li class="item-115"><a href="/index.php/es/informacion-general">Información General</a></li><li class="item-116"><a href="/index.php/es/aviso-legal">Aviso Legal</a></li><li class="item-117"><a href="/index.php/es/politica-de-privacidad">Política de Privacidad</a></li><li class="item-124"><a href="/index.php/es/mapa-web">Mapa web</a></li></ul>
</div>
<!-- end navi -->
<div id="wrapper2">
<div id="errorboxbody">
<h2>Se ha producido un error.<br/>
								No se puede encontrar la página solicitada.</h2>
<div>
<p><a href="/index.php" title="Ir a la página de inicio">Página de inicio</a></p>
</div>
<h3>Si la dificultad persiste, por favor, contacte con el administrador del sistema de este sitio y reporte el error de más abajo.</h3>
<h2>#404 Categoría no encontrada</h2> <br/>
</div><!-- end wrapper -->
</div><!-- end contentarea -->
</div> <!--end all -->
</div>
</div>
<div id="footer-outer">
<div id="footer-sub">
<div id="footer">
<p>
                                                TPL_BEEZ2_POWERED_BY <a href="http://www.joomla.org/">Joomla!®</a>
</p>
</div><!-- end footer -->
</div><!-- end footer-sub -->
</div>
</body>
</html>
