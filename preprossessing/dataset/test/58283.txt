<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<!-- Responsive Desing -->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- META Daten -->
<meta content="Mehr als 15000 Hausaufgaben, Klausuren, Referate, Facharbeiten, Biographien, Interpretationen und Zusammenfassungen! Mit Community, Forum und großem Hausaufgaben-Archiv" name="description"/>
<meta content="abi-pur.de -&gt; Mehr als 15000 Hausaufgaben, Klausuren, Referate, Facharbeiten, Biographien, Analysen, Interpretationen, Übersetzungen, Schularbeiten und Nachhilfe + Abi-Forum" name="abstract"/>
<meta content="Hausaufgaben, Hausaufgabe, Referate, Referat, Interpretation, Analyse, Zusammenfassung, Inhaltsangabe, Forum, Abitur, Abi-Forum, Abi, Hausarbeit, kostenlos, gratis, download, Schule, Studium, Nachhilfe, Erörterung, Deutsch, Englisch, English, Latein, Mathe, Mathematik, Informatik, Französisch, Spanisch, Italienisch, Erdkunde, Geschichte, Kunst, Politik, Biologie, Chemie, Physik, Kriegsdienstverweigerung, Wehrdienst, Wehrdienstverweigerung" name="keywords"/>
<meta content="Katalog" name="page-type"/>
<meta content="Bildung &amp; Wissenschaft" name="page-topic"/>
<meta content="5 days" name="revisit-after"/>
<meta content="index, follow" name="robots"/>
<!-- Favicon -->
<link href="/bilder/icon/favicon.ico" rel="icon"/>
<link href="/bilder/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/bilder/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/bilder/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/bilder/favicon/site.webmanifest" rel="manifest"/>
<link color="#5bbad5" href="/bilder/favicon/safari-pinned-tab.svg" rel="mask-icon"/>
<meta content="#2d89ef" name="msapplication-TileColor"/>
<meta content="#ffffff" name="theme-color"/>
<title>Hausaufgaben / Referate =&gt; abi-pur.de :: Startseite</title>
<!-- CSS -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800&amp;display=swap" rel="stylesheet" type="text/css"/>
<link href="/css/min.style.css?v=20" rel="stylesheet"/>
<!-- JS Bibliotheken und Basic -->
<script defer="" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script defer="" src="/js/min.basic.js?v=21"></script>
<script type="text/javascript">
        var Ads_BA_ADIDsite = "abi-pur.de";
        var Ads_BA_ADIDsection = "rotation";
        var Ads_BA_keyword = "";
    </script>
<script async="async" src="https://www.googletagservices.com/tag/js/gpt.js" type="text/javascript"></script>
<script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>
<script async="async" src="https://storage.googleapis.com/ba_utils/apu.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1487222-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-1487222-1', { 'anonymize_ip': true });
    </script>
</head>
<body>
<div class="boxed">
<div id="Ads_BA_BS"></div>
<div id="pagewrap">
<div id="skyscraper"><div id="Ads_BA_SKY"></div></div>
<!-- Top Bar -->
<div class="top-bar">
<div class="container">
<nav class="top-menu nomobile">
<ul class="menu" id="mobile-menu">
<li>
<a href="/">
<span class="icon" data-icon=""></span>
</a>
</li>
<li><a href="/hausaufgaben/neu/kontakt.shtml">Kontakt</a></li>
<li><a href="/hausaufgaben/neu/impressum.shtml">Impressum</a></li>
<li><a href="/sms/upload.php">Upload</a></li>
</ul>
</nav>
<span class="top-bar-socials">
<!-- Facebook -->
<a href="/ads/share/share.php?prov=facebook&amp;url=%2F" target="_blank" title="Share on Facebook"><span class="icon" data-icon=""></span></a>
<!-- Facebook Messenger (Messenger muss installiert sein!) -->
<a class="onlymobile" href="/ads/share/share.php?prov=fbmessenger&amp;url=%2F" target="_blank" title="Share on Facebook Messenger"><span class="icon" data-icon=""></span></a>
<!-- Twitter -->
<a href="/ads/share/share.php?prov=twitter&amp;url=%2F" target="_blank" title="Share on Twitter"><span class="icon" data-icon=""></span></a>
<!-- Google+ -->
<a href="/ads/share/share.php?prov=google&amp;url=%2F" target="_blank" title="Share on Google+"><span class="icon" data-icon=""></span></a>
<!-- Whatsapp -->
<a class="onlymobile" href="whatsapp://send?text=https%3A%2F%2Fwww.abipur.de%2F" target="_blank" title="Share on WhatsApp"><span class="icon" data-icon=""></span></a>
<!-- Mail -->
<a href="mailto:?&amp;subject=Hausaufgabe+auf+abi-pur.de&amp;body=https%3A%2F%2Fwww.abipur.de%2F" target="_blank" title="Share by E-Mail"><span class="icon" data-icon=""></span></a>
</span>
</div>
</div>
<!-- Header -->
<header class="header">
<div class="container">
<div class="logo">
<a href="/">
<img alt="logo" border="0px" id="logo" src="/bilder/neu_logo.png" srcset="/bilder/neu_logo.png 1x, /bilder/neu_logo2x.png 2x"/>
</a>
</div>
<div class="ad">
<a class="track" href="/ausbildung/stellenangebote/"><img alt="ad-728x90" border="0px" src="/bilder/banner/schuelerkarriere/728x90.jpg"/></a>
</div>
</div>
</header>
<!-- Mainmenu -->
<!-- Mainmenu -->
<nav class="main-menu">
<div class="container">
<a class="show-menu" href="/sitemap/">
<span class="icon" data-icon=""></span>
</a>
<ul class="menu" id="main-mobile-menu">
<!-- Suche im Menu -->
<li class="search-menu">
<a href="#">
<span class="icon" data-icon=""></span>
                   Suche
                </a>
<ul class="sub-menu">
<li>
<form action="/suche/index.php" class="navbar-form search">
<div class="input-group">
<input class="form-control" name="query" placeholder="Suchwort eingeben" type="search" value=""/>
<input name="bool" type="hidden" value="and"/>
<span class="input-group-btn">
<button class="btn btn-default btn-submit" type="submit">
<span class="icon" data-icon=""></span>
</button>
</span>
</div>
</form>
</li>
</ul>
</li>
<!-- Icon Home -->
<li class="home">
<a href="/">
<span class="icon" data-icon=""></span>
</a>
</li>
<!-- mit Submenu -->
<li>
<a href="/hausaufgaben.shtml">
                  Hausaufgaben <span class="icon" data-icon=""></span>
</a>
<ul class="sub-menu">
<li><a href="/hausaufgaben/neu/referate/Deutsch/">Deutsch</a></li>
<li><a href="/hausaufgaben/neu/referate/Englisch/">Englisch</a></li>
<li><a href="/hausaufgaben/neu/referate/Latein/">Latein</a></li>
<li><a href="/hausaufgaben/neu/referate/Geschichte/">Geschichte</a></li>
<li><a href="/hausaufgaben/neu/referate/Politik/">Politik</a></li>
<li><a href="/hausaufgaben/neu/referate/Erdkunde/">Erdkunde</a></li>
<li><a href="/hausaufgaben/neu/referate/Physik/">Physik</a></li>
<li><a href="/hausaufgaben/neu/referate/Biologie/">Biologie</a></li>
<li><a href="/hausaufgaben/neu/referate/Chemie/">Chemie</a></li>
<li><a href="/hausaufgaben/neu/referate/Mathe/">Mathematik</a></li>
<li><a href="/hausaufgaben/neu/referate/Musik/">Musik</a></li>
<li><a href="/hausaufgaben/neu/referate/Kunst/">Kunst</a></li>
</ul>
</li>
<!-- Normale Menu -->
<li><a href="/hausaufgaben/neu/dict/">Übersetzer</a></li>
<li><a href="/sms/upload.php">Upload</a></li>
<li><a href="/abi-reisen/">Abi-Reisen <span class="icon" data-icon=""></span></a>
<ul class="sub-menu">
<li><a href="/abi-reisen/">Abi-Reisen - Überblick</a></li>
<li><a href="/abi-reisen/die_planung.html">Tipps und Tricks</a></li>
<li><a href="/ads/www/delivery/ck.php?bannerid=7" target="_blank">Reise-Katalog anfordern</a></li>
</ul>
</li>
<li><a href="/gedichte/">Gedichte <span class="icon" data-icon=""></span></a>
<ul class="sub-menu">
<li><a href="/gedichte/">Gedichte - Überblick</a></li>
<li><a href="/gedichte/werke/">Verfügbare Gedichte</a></li>
<li><a href="/gedichte/autoren/">Unsere Autoren</a></li>
</ul>
</li>
<li><a href="/abizeitung/">Abi-Zeitung <span class="icon" data-icon=""></span></a>
<ul class="sub-menu">
<li><a href="/abizeitung/">Abi-Zeitung - Überblick</a></li>
<li><a href="/abizeitung/anregungen.html">Anregungen</a></li>
<li><a href="/abizeitung/die_planung.html">Tipps und Tricks</a></li>
</ul>
</li>
<li><a href="/abitur/">Abi-Infos <span class="icon" data-icon=""></span></a>
<ul class="sub-menu">
<li><a href="/abitur/">Abitur - Überblick</a></li>
<li><a href="/abitur/geschichte.html">Geschichte des Abiturs</a></li>
<li><a href="/abitur/nachhilfe.html">Nachhilfe</a></li>
<li><a href="/abitur/studium.html">Abitur und dann?</a></li>
</ul>
</li>
<li><a href="/abitur-nachholen/">Abi Nachholen</a></li>
<li><a href="/ausbildung/">Ausbildung <span class="icon" data-icon=""></span></a>
<ul class="sub-menu">
<li><a href="/ausbildung/">Studium oder Ausbildung?</a></li>
<li><a href="/ausbildung/stellenangebote/">Freie Ausbildungsplätze</a></li>
<li><a class="track" href="/ausbildung/dz-bank-gruppe/">Spezial: DZ BANK GRUPPE</a></li>
</ul>
</li>
<li><a href="/abishirts/">Abi-Shirts</a></li>
</ul>
</div>
</nav>
<section class="content">
<div class="container">
<!-- Content / Sidebar -->
<div class="row">
<div class="col-8">
<div class="breadcrump">
<ul>
<li><a href="/">Home</a></li>
</ul>
</div>
<div class="content">
<div class="row">
<div class="col-12">
<h1>Hausaufgaben, Referate, Facharbeiten, Hausarbeiten - abi-pur.de hilft!</h1>
</div>
<div class="col-3 nomobile">
<!-- Iconbox als Link -->
<a class="iconbox" href="/hausaufgaben/neu/referate/Deutsch/">
<span class="icon" data-icon=""></span>
<strong>Deutsch</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Englisch/">
<span class="icon" data-icon=""></span>
<strong>Englisch</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Latein/">
<span class="icon" data-icon=""></span>
<strong>Latein</strong>
                        jetzt ansehen
                      </a>
</div>
<div class="col-3 nomobile">
<a class="iconbox" href="/hausaufgaben/neu/referate/Geschichte/">
<span class="icon" data-icon=""></span>
<strong>Geschichte</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Politik/">
<span class="icon" data-icon=""></span>
<strong>Politik</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Erdkunde/">
<span class="icon" data-icon=""></span>
<strong>Erdkunde</strong>
                        jetzt ansehen
                      </a>
</div>
<div class="col-3 nomobile">
<a class="iconbox" href="/hausaufgaben/neu/referate/Physik/">
<span class="icon" data-icon=""></span>
<strong>Physik</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Biologie/">
<span class="icon" data-icon=""></span>
<strong>Biologie</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Chemie/">
<span class="icon" data-icon=""></span>
<strong>Chemie</strong>
                        jetzt ansehen
                      </a>
</div>
<div class="col-3 nomobile">
<a class="iconbox" href="/hausaufgaben/neu/referate/Mathe/">
<span class="icon" data-icon=""></span>
<strong>Mathe</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Musik/">
<span class="icon" data-icon=""></span>
<strong>Musik</strong>
                        jetzt ansehen
                      </a>
<a class="iconbox" href="/hausaufgaben/neu/referate/Kunst/">
<span class="icon" data-icon=""></span>
<strong>Kunst</strong>
                        jetzt ansehen
                      </a>
</div>
<div class="col-12">
<p><b>Referate, Hausaufgaben, Hausarbeiten, Facharbeiten - all das findest Du auf abi-pur.de!</b><br/>abi-pur.de hat ein eigenes großes Archiv an Hausaufgaben, Hausarbeiten, Referaten und verschiedensten Facharbeiten, damit ist abi-pur.de eine wichtige Hilfe bei Deinen Schulaufgaben. Diese Seite kann aber noch viel mehr! abi-pur.de durchsucht auch andere Referate-Archive. So findet man schnell und einfach aus mehr als 15.000 Referaten das Passende für sich selbst.</p>
</div>
<div class="col-12">
<h3>Neue Hausaufgaben, Referate, Facharbeiten oder Hausarbeiten</h3>
<!-- Accordeon -->
<div class="accordion">
<div>
<a href="#">Bachmann, Ingeborg - Nebelland (Interpretation 4. &amp; 5. Strophe)</a>
<div>
<p>
<b>Schlagwörter:</b><br/>
                              Ingeborg Bachmann, Analyse der vierten und fünften Strophe, Gedichtinterpretation, Gedichtanalyse
                            </p>
<p>
<b>Auszug</b><br/>
                              und Analyse der vierten und fünften Strophe des Gedichtes Nebelland von Ingeborg Bachmann Das Gedicht Nebelland , von Ingeborg Bachmann, entstand 1956 in Deutschland. Es handelt sich um ein Gedicht über negative Erfahrungen in der Liebe. Nach ersten Eindrücken ist die verwendete Bildersprache schwer zu verstehen und zu entschlüsseln. Das Gedicht weist eine sehr regelmäßige fast schon prosamäßige Form auf und umfasst sechs Strophen á 7 Zeilen. Die letzte Strophe bildet mit nur ...
                              <a class="btn gelb" href="/referate/stat/690748642.html">zum Referat</a>
</p>
</div>
</div>
<div>
<a href="#">Ginzberg, Eli - Laufbahnentwicklungstheorie</a>
<div>
<p>
<b>Schlagwörter:</b><br/>
                              Entwicklungsprozess, Berufswahl, Berufsleben
                            </p>
<p>
<b>Auszug</b><br/>
                              (Eli Ginzberg) 1951 entwickelte Ginzberg, als erster Vertreter entwicklungspsychologischer Erklärungsansätze, den Ansatz der Phasentheorie der Berufswahl. Diese Theorie besagt, dass Berufswahl ein Entwicklungsprozess ist. Während dieses Prozesses werden verschiedene Stadien mit berufsrelevanten Entscheidungen durchlaufen. Der Ansatz basiert auf der Frage, welche für einen Beruf relevanten Persönlichkeitsmerkmale sich in welchem Lebensabschnitt ausbilden und wie die ...
                              <a class="btn gelb" href="/referate/stat/690727381.html">zum Referat</a>
</p>
</div>
</div>
<div>
<a href="#">Osmose - Kartoffelexperiment (Kartoffelzylinder in Salzwasser)</a>
<div>
<p>
<b>Schlagwörter:</b><br/>
                              Experiment zum Thema Osmose, Plasmolyse, Deplasmolyse, Kartoffeln
                            </p>
<p>
<b>Auszug</b><br/>
                              in Salzwasser (Experiment zum Thema Osmose Plasmolyse Deplasmolyse) Durchführung: Es werden drei Kartoffelzylinder mithilfe eines Apfelausstechers aus einer Kartoffel ausgestochen. Die Länge der Kartoffelzylinder wird mit einem Messer auf exakt 3,0 cm gekürzt. Die Kartoffelzylinder werden für 12 Stunden in unterschiedliche Lösungen eingelegt: Becherglas Nr. 1: Der erste Kartoffelzylinder wird in 100,0 ml destilliertem Wasser eingelegt. Becherglas Nr. 2: Der zweite ...
                              <a class="btn gelb" href="/referate/stat/690706120.html">zum Referat</a>
</p>
</div>
</div>
<div>
<a href="#">Afghanistan – Maßnahmen und Strategien deutscher Entwicklungspolitik</a>
<div>
<p>
<b>Schlagwörter:</b><br/>
                              Afghanistan, Basir Sowida, Hühnerwirt, Mazar-e Sharif, GIZ, Entwicklungszusammenarbeit
                            </p>
<p>
<b>Auszug</b><br/>
                              Maßnahmen und Strategien deutscher Entwicklungspolitik Afghanistan: Basir Sowida, Hühnerwirt Ordnen Sie das Beispiel des Hühnerwirts Sowida (M3) in die Strategien und Maßnahmen deutscher Entwicklungspolitik ein. Die Strategien und Maßnahmen deutscher Entwicklungspolitik lassen sich auf zwei Grundgedanken zurückführen, welche essenziell für die oftmals langfristig angelegte entwicklungspolitische Arbeit ist. Der erste Grundsatz ist die Hilfe zur Selbsthilfe , welche durch einen ...
                              <a class="btn gelb" href="/referate/stat/690684859.html">zum Referat</a>
</p>
</div>
</div>
<div>
<a href="#">Afghanistan - Ist die deutsche Entwicklungszusammenarbeit mit Afghanistan (als präventive Friedenspolitik) erfolgreich?</a>
<div>
<p>
<b>Schlagwörter:</b><br/>
                              Erfolg oder Misserfolg der deutschen Entwicklungspolitik in Afghanistan
                            </p>
<p>
<b>Auszug</b><br/>
                              die deutsche Entwicklungszusammenarbeit mit Afghanistan (als präventive Friedenspolitik) erfolgreich? Wir sind besiegt (Artikel im Zeit-Magazin erschienen). Überprüfen Sie die Einschätzung Bauers zur deutschen Entwicklungs- und Sicherheitspolitik in Afghanistan (M10 M12). In dem Ausschnitt aus dem Artikel Wir sind besiegt von Wolfgang Bauer, welcher am 08.03.2018 in der Zeit erschienen ist, geht es um das scheinbare Scheitern der deutschen Entwicklungs- und Sicherheitspolitik in ...
                              <a class="btn gelb" href="/referate/stat/690663598.html">zum Referat</a>
</p>
</div>
</div>
</div> </div>
<div class="col-12">
<!-- Banner -->
<a class="banner" href="/ausbildung/dz-bank-gruppe/">
<img alt="" border="0px" src="/bilder/banner/dz_bank_gruppe.png"/>
<div>
<h5>Starte Deine Ausbildung oder Dein Duales Studium bei der DZ BANK Gruppe</h5>
                                Die Zeit der Hausaufgaben ist bald vorbei und Du startest ins Berufsleben? Informiere Dich über die Ausbildungsmöglichkeiten bei der DZ BANK Gruppe.
                            </div>
</a>
</div>
<div class="col-12">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>Freie Ausbildungsplätze in Deiner Region</h5><br/>
<p>besuche unsere Stellenbörse und finde mit uns Deinen Ausbildungsplatz</p><br/>
<a href="/ausbildung/stellenangebote/">erfahre mehr und bewirb Dich direkt</a>
</div>
</div>
<!-- Partner -->
<div class="col-12">
<h5>Partner</h5>
</div>
<div class="col-6">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>e-hausaufgaben.de</h5><br/>
<p>Community und Referate-Archiv</p><br/>
<a href="http://www.e-hausaufgaben.de/" target="_blank">mehr erfahren</a>
</div>
</div>
<div class="col-6">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>schule-studium.de</h5><br/>
<p>Infos zu Schule und Studium</p><br/>
<a href="http://www.schule-studium.de/" target="_blank">mehr erfahren</a>
</div>
</div>
<div class="col-6">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>english-readers.de</h5><br/>
<p>Infoplattform für Englisch</p><br/>
<a href="http://www.english-readers.de/" target="_blank">mehr erfahren</a>
</div>
</div>
<div class="col-6">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>kostenlose-referate.de</h5><br/>
<p>noch mehr Referate und Hausaufgaben</p><br/>
<a href="http://www.kostenlose-referate.de/" target="_blank">mehr erfahren</a>
</div>
</div>
<div class="col-6">
<div class="linkbox">
<span class="icon" data-icon=""></span>
<h5>abi-pur.de</h5><br/>
<p>... weitere Tipps</p><br/>
<a href="/hausaufgaben/neu/linksammlung/">zu den Tipps</a>
</div>
</div>
</div>
</div>
</div>
<!-- Sidebar -->
<div class="col-4 right">
<div class="sidebar">
<!-- Suche Widget -->
<div class="widget-container">
<h4 class="section-title">Suchen</h4>
<div class="search">
                      Durchsucht die Hausaufgaben Datenbank
                      <form action="/suche/index.php" class="sidebar-search">
<div class="input-group">
<input class="form-control" name="query" placeholder="Suchwort eingeben" type="search" value=""/>
<span class="input-group-btn">
<button class="btn btn-default btn-submit" type="submit">
<span class="icon" data-icon=""></span>
</button>
</span>
</div>
                        Suche mit: <label><input checked="" name="bool" type="radio" value="and"/> UND</label> <label><input name="bool" type="radio" value="or"/> ODER</label>
</form>
</div>
</div>
<!-- Social Media Widget -->
<div class="widget-container">
<h4 class="section-title">Social</h4>
<div class="social-button">
<!-- Facebook -->
<a href="/ads/share/share.php?prov=facebook&amp;url=%2F" target="_blank" title="Share on Facebook"><span class="icon" data-icon=""></span></a>
<!-- Facebook Messenger (Messenger muss installiert sein!) -->
<a class="onlymobile" href="/ads/share/share.php?prov=fbmessenger&amp;url=%2F" target="_blank" title="Share on Facebook Messenger"><span class="icon" data-icon=""></span></a>
<!-- Twitter -->
<a href="/ads/share/share.php?prov=twitter&amp;url=%2F" target="_blank" title="Share on Twitter"><span class="icon" data-icon=""></span></a>
<!-- Google+ -->
<a href="/ads/share/share.php?prov=google&amp;url=%2F" target="_blank" title="Share on Google+"><span class="icon" data-icon=""></span></a>
<!-- Whatsapp -->
<a class="onlymobile" href="whatsapp://send?text=https%3A%2F%2Fwww.abipur.de%2F" target="_blank" title="Share on WhatsApp"><span class="icon" data-icon=""></span></a>
<!-- Mail -->
<a href="mailto:?&amp;subject=Hausaufgabe+auf+abi-pur.de&amp;body=https%3A%2F%2Fwww.abipur.de%2F" target="_blank" title="Share by E-Mail"><span class="icon" data-icon=""></span></a>
</div>
<div class="clearfix"></div>
</div>
<div class="widget-container jobs">
<a class="track" href="/ausbildung/dz-bank-gruppe/">
<h4 class="section-title">Studium oder Ausbildung</h4>
<div class="jobs-box">
<span class="icon" data-icon=""></span>
<p><u>Anzeige:</u> Finanziell auf eigenen Beinen stehen – mit einer Ausbildung bei der DZ BANK Gruppe.</p>
</div>
</a>
</div>
<!-- Anzeigen Widget -->
<div class="widget-container">
<h4 class="section-title" id="anzeige-sidebar">Anzeige</h4>
<div class="ad">
<div id="Ads_BA_CAD" style="text-align:center;"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Footer -->
<footer class="footer">
<div class="container">
<div class="row">
<!-- 4 von 12 Spalten -->
<div class="col-4">
<!-- Text Widget -->
<div class="footer-widget">
<h4 class="footer-title"><span>Über abi-pur.de</span></h4>
<p>abi-pur.de lebt vom Mitmachen! Hier könnt ihr eure Hausaufgaben online stellen. Für jedes veröffentlichte Referat gibt es sogar Bares!<br/><br/><a href="/sms/upload.php">zum Hausaufgabe-Upload</a></p>
</div>
</div>
<!-- 4 von 12 Spalten -->
<div class="col-4">
<!-- Menu Widget -->
<div class="footer-widget">
<h4 class="footer-title"><span>Schulfächer</span></h4>
<!-- neues 12er Grid -->
<div class="row">
<!-- 6 von 12 Spalten -->
<div class="col-6">
<ul class="footer-categories">
<li><a href="/hausaufgaben/neu/referate/Deutsch/">Deutsch</a></li>
<li><a href="/hausaufgaben/neu/referate/Englisch/">Englisch</a></li>
<li><a href="/hausaufgaben/neu/referate/Latein/">Latein</a></li>
<li><a href="/hausaufgaben/neu/referate/Geschichte/">Geschichte</a></li>
<li><a href="/hausaufgaben/neu/referate/Politik/">Politik</a></li>
<li><a href="/hausaufgaben/neu/referate/Erdkunde/">Erdkunde</a></li>
</ul>
</div>
<!-- 6 von 12 Spalten -->
<div class="col-6">
<ul class="footer-categories">
<li><a href="/hausaufgaben/neu/referate/Physik/">Physik</a></li>
<li><a href="/hausaufgaben/neu/referate/Biologie/">Biologie</a></li>
<li><a href="/hausaufgaben/neu/referate/Chemie/">Chemie</a></li>
<li><a href="/hausaufgaben/neu/referate/Mathe/">Mathematik</a></li>
<li><a href="/hausaufgaben/neu/referate/Musik/">Musik</a></li>
<li><a href="/hausaufgaben/neu/referate/Kunst/">Kunst</a></li>
</ul>
</div>
</div>
</div>
</div>
<!-- 4 von 12 Spalte -->
<div class="col-4">
<!-- Anzeigen Widget -->
<div class="footer-widget">
<h4 class="footer-title"><span>Anzeige</span></h4>
<div class="row">
<div class="col-12">
<div class="ad max360px">
<a class="track" href="/ads/www/delivery/ck.php?bannerid=10" target="_blank"><img alt="" height="250" src="/bilder/banner/kdl/kdl300x250.gif" width="300"/></a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Last Footer -->
<div class="footer-bottom">
            © Copyright 1999 - 2017, abi-pur.de<br/>
<a href="/">Home</a><a href="/hausaufgaben/m/new-1.html">Neue Referate</a><a href="/hausaufgaben/m/top-1.html">Top-Suchbegriffe</a><a href="/hausaufgaben/neu/archiv/hausaufgaben1.php">Archiv</a><a href="/sprachreisen/">Sprachreisen</a><br/>
<a href="/hausaufgaben/neu/kriegsdienstverweigerungen/">KDV</a><a href="https://forum.abi-pur.de/" target="_blank">Forum</a><a href="/hausaufgaben/neu/linksammlung/">Links</a><a href="/hausaufgaben/neu/kontakt.shtml">Kontakt</a><a href="/hausaufgaben/neu/datenschutz.shtml">Datenschutz</a>
</div>
</footer>
</div>
</div>
</body>
</html>