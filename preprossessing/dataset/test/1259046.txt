<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"><head><meta charset="utf-8"/><meta content="width=device-width" name="viewport"/><link href="https://fonts.googleapis.com/css?family=Assistant|Montserrat" rel="stylesheet"/><link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"/><link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/><link href="/favicon.ico" rel="icon" type="image/x-icon"/><link href="https://www.quality-patient-care.org/wp-content/cache/autoptimize/css/autoptimize_5a72adc1506e9c7a1c2f3950ea388283.css" media="all" rel="stylesheet"/><link href="https://www.quality-patient-care.org/wp-content/cache/autoptimize/css/autoptimize_3e0fbc758330eda7029a6fd48597c715.css" media="screen" rel="stylesheet"/><title>Page not found – Quality Patient Care</title> <script>(function(d){
              var js, id = 'powr-js', ref = d.getElementsByTagName('script')[0];
              if (d.getElementById(id)) {return;}
              js = d.createElement('script'); js.id = id; js.async = true;
              js.src = '//www.powr.io/powr.js';
              js.setAttribute('powr-token','RP3f1f9YnI1431887561');
              js.setAttribute('external-type','wordpress');
              ref.parentNode.insertBefore(js, ref);
            }(document));</script> <link href="https://www.quality-patient-care.org/feed/" rel="alternate" title="Quality Patient Care » Feed" type="application/rss+xml"/><link href="https://www.quality-patient-care.org/comments/feed/" rel="alternate" title="Quality Patient Care » Comments Feed" type="application/rss+xml"/> <script id="jquery-core-js" src="https://www.quality-patient-care.org/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script> <link href="https://www.quality-patient-care.org/wp-json/" rel="https://api.w.org/"/><link href="https://www.quality-patient-care.org/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/><link href="https://www.quality-patient-care.org/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/><meta content="WordPress 5.6" name="generator"/><meta content="en_US" property="og:locale"/><meta content="Quality Patient Care" property="og:site_name"/><meta content="https://www.quality-patient-care.org/mn/%09%0a" property="og:url"/><meta content="article" property="og:type"/><meta content="Support safe nurse-to-patient ratios and quality care for all Minnesotans" property="og:description"/><meta content="https://www.facebook.com/QualityPatientCare/" property="article:publisher"/><meta content="Support safe nurse-to-patient ratios and quality care for all Minnesotans" itemprop="description"/><meta content="https://www.quality-patient-care.org/mn/%09%0a" name="twitter:url"/><meta content="Support safe nurse-to-patient ratios and quality care for all Minnesotans" name="twitter:description"/><meta content="summary_large_image" name="twitter:card"/> <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49349769-3', 'auto');
  ga('send', 'pageview');</script> </head><body class="error404"><header id="header" role="banner"><div class="inner"><div class="left"><section id="branding"><div id="logo"> <a href="https://www.quality-patient-care.org"><img src="https://www.quality-patient-care.org/wp-content/themes/patientcare/images/logo-new2.png"/></a></div></section></div><div class="right"><div id="hamburger-train"> <span class="fa fa-reorder"></span> <span class="hamburger-heading">MENU</span></div><nav id="menu" role="navigation"><div class="menu-main-menu-container"><ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5342" id="menu-item-5342"><a href="https://www.quality-patient-care.org/join-us/">Join Us</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5345" id="menu-item-5345"><a href="https://www.quality-patient-care.org/for-patients/">For Patients</a><ul class="sub-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5394" id="menu-item-5394"><a href="https://www.quality-patient-care.org/for-patients/why-safe-staffing-matters/">Why Safe Staffing Matters</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5374" id="menu-item-5374"><a href="https://www.quality-patient-care.org/patient-safety/concern-for-safe-staffing-report-2016/">Read the Report</a></li><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5360" id="menu-item-5360"><a href="https://www.quality-patient-care.org/category/patient-story/">Patient Stories</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5373" id="menu-item-5373"><a href="https://www.quality-patient-care.org/patient-stories/">Share Your Story</a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5346" id="menu-item-5346"><a href="https://www.quality-patient-care.org/for-nurses/">For Nurses</a><ul class="sub-menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5361" id="menu-item-5361"><a href="https://www.quality-patient-care.org/category/nurse-story/">Nurse Stories</a></li></ul></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5343" id="menu-item-5343"><a href="https://www.quality-patient-care.org/our-work/">Our Work</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-5344" id="menu-item-5344"><a href="https://www.quality-patient-care.org/updates/">Blog</a></li></ul></div></nav></div><div class="clear"></div></div></header><section id="content" role="main"><article class="post not-found" id="post-0"><header class="header"><h1 class="entry-title">Not Found</h1></header><section class="entry-content"><p>Nothing found for the requested page. Try a search instead?</p><form action="https://www.quality-patient-care.org/" class="searchform" id="searchform" method="get" role="search"><div> <label class="screen-reader-text" for="s">Search for:</label> <input id="s" name="s" type="text" value=""/> <input id="searchsubmit" type="submit" value="Search"/></div></form></section></article></section><section id="footer-callout"><div class="inner"><div id="footer-callouts"><div class="callout"> <a class="biggun" href="https://votervoice.net/SOCA/register" title="Sign Our Petition"><div class="callout-icon"> <span class="fa fa-pencil"></span></div><div class="callout-title"> Sign the Petition</div> </a></div><div class="callout"> <a class="biggun" href="https://www.quality-patient-care.org/patient-stories/" title="Tell Your Patient Story"><div class="callout-icon"> <span class="fa fa-comment"></span></div><div class="callout-title"> Tell Your Patient Story</div> </a></div><div class="callout"> <a class="biggun" href="https://www.quality-patient-care.org/patient-safety/concern-for-safe-staffing-report-2016/" title="Learn What Safe Staffing Means"><div class="callout-icon"> <span class="fa fa-user-md"></span></div><div class="callout-title"> Learn What Safe Staffing Means</div> </a></div><div class="callout" onclick=""> <a class="biggun" href="https://www.quality-patient-care.org/patient-safety/new-patient-orientation/" title="Get the New Patient Orientation"><div class="callout-icon"> <span class="fa fa-map-signs"></span></div><div class="callout-title"> Get The New Patient Orientation</div> </a></div><div class="gapfill"></div></div></div></section><footer id="footer" role="contentinfo"><div id="pre-footer"><div class="inner"><div class="footer-column"><div class="footer-logo"><a href="https://www.quality-patient-care.org"><img src="https://www.quality-patient-care.org/wp-content/themes/patientcare/images/footer-logo-new.png"/></a></div><div class="textwidget"><p>The Quality Patient Care campaign is supported by the Minnesota Nurses Association. The purpose of the Minnesota Nurses Association, a union of professional nurses with unrestricted RN membership, is to advance the professional, economic, and general well-being of nurses and to promote the health and well-being of the public. These purposes are unrestricted by considerations of age, color, creed, disability, gender, health status, lifestyle, nationality, race, religion, or sexual orientation.</p></div></div><div class="footer-column"><h3 class="footer-title">Pages</h3><div class="footer-menu"><div class="menu-footer-menu-container"><ul class="menu" id="menu-footer-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5362" id="menu-item-5362"><a href="https://www.quality-patient-care.org/join-us/">Join Us</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5365" id="menu-item-5365"><a href="https://www.quality-patient-care.org/for-patients/">For Patients</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5366" id="menu-item-5366"><a href="https://www.quality-patient-care.org/for-nurses/">For Nurses</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5363" id="menu-item-5363"><a href="https://www.quality-patient-care.org/our-work/">Our Work</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-5364" id="menu-item-5364"><a href="https://www.quality-patient-care.org/updates/">Updates</a></li></ul></div></div></div><div class="footer-column"><h3 class="footer-title">Follow Us</h3><ul><li><a href="http://twitter.com/SafePatientStd" title="Follow Us on Facebook"><span class="fa fa-facebook"></span> Facebook</a></li><li><a href="#" title="Follow Us on Twitter"><span class="fa fa-twitter"></span> Twitter</a></li></ul></div><div class="gapfill"></div></div></div><div id="copyright"><div class="inner"> © 2021 Quality Patient Care. All Rights Reserved.</div></div></footer> <script id="social_js-js-extra" type="text/javascript">var Sociali18n = {"commentReplyTitle":"Post a Reply"};</script> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> <script>jQuery(document).ready(function($){
		
		//function sizeboxes(){
			$.fn.setAllToMaxHeight = function(){
				return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
			}
		//}
					
		function sizeslider(){
			var windowheight = 0;
			var sliderheight = 0;
			var slideheight = 0;
			var headerheight = 0;
			
			var numberofslides = 0;
			
			var count = 0;
			
			$('#slider .slide').each(function(){
				numberofslides += 1;
				$(this).addClass('inactive');
			});
			
			$('#slider .slide').first().addClass('active').addClass('first').removeClass('inactive');
			
			$('#slider .slide').last().addClass('last');
			
			windowheight = $(window).height();
			sliderheight - $('#main-slider').height();
			headerheight = $('#header').height();
			
			$('#slider').height(windowheight/1.75);
			$('#slider .slide').each(function(){
				$(this).children('.inner-slide').each(function(){
					$(this).children().each(function(){
						slideheight += $(this).outerHeight(true);
					});
					$(this).height(slideheight);
					slideheight = 0;
				});
			});
			
			$('#video iframe').height($('#video .right').height()+35);
			
		}
		
		$('.nav-right').click(function(){
			$('.slide.active').each(function(){
				if($(this).hasClass('last')){
					$('.slide').addClass('inactive').removeClass('active');
					$('.slide.first').addClass('active').removeClass('inactive');
				} else {
						$(this).addClass('inactive').removeClass('active');
						$(this).next('.slide').addClass('active').removeClass('inactive');
				}
			});
		});
		$('.nav-left').click(function(){
			$('.slide.active').each(function(){
				if($(this).hasClass('first')){
					$('.slide').addClass('inactive').removeClass('active');
					$('.slide.last').addClass('active').removeClass('inactive');
				} else {
						$(this).addClass('inactive').removeClass('active');
						$(this).prev('.slide').addClass('active').removeClass('inactive');
				}
			});
		})
		
		sizeslider();
		
		function autoslide(){
			setInterval(function(){
				$('.nav-right').trigger('click');
			},5000);
		}
		
		autoslide();
		
		$(window).resize(function(){
			sizeslider();
					});
		
		function countdat(){			
			
				$('.countit').each(function () {
    				$(this).prop('Counter',0).animate({
        				Counter: $(this).text()
    				}, {
        				duration: 2000,
        				easing: 'swing',
						step: function (now) {
            			$(this).text(Math.ceil(now));
        			}
    				});
				});

		}
		
		$(window).scroll(function () {
    			console.log($(window).scrollTop());
    			var topDivHeight = $("#by-the-numbers").height();
    			var viewPortSize = $(window).height();

    			var triggerAt = 150;
    			var triggerHeight = (topDivHeight - viewPortSize) + triggerAt;

    			if ($(window).scrollTop() >= triggerHeight) {
        			countdat();
        			$(this).off('scroll');
    			}
			});
		
		//$(".owl-carousel").owlCarousel({
			//items: 1,
			//nav: true,
			//loop:true,
			//autoWidth:true,
		//});
		
		$('#hamburger-train').click(function(){
			$('#menu').slideToggle(function(){
					if($('#menu').is(':visible')){
						$('#hamburger-train .fa').addClass('fa-close').removeClass('fa-reorder');
					} else {
						$('#hamburger-train .fa').addClass('fa-reorder').removeClass('fa-close');
					}
				
			});
		});
		
	});</script> <script defer="" src="https://www.quality-patient-care.org/wp-content/cache/autoptimize/js/autoptimize_4f311a23ed56d4f826546f583476b977.js"></script></body></html>