<!DOCTYPE html>
<html lang="en-GB">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<link href="https://use.typekit.net/hxl8pnm.css" rel="stylesheet"/>
<link href="https://www.blueforest.com/wp-content/themes/blueforest/images/favicon.png" rel="icon"/>
<!-- This site is optimized with the Yoast SEO plugin v14.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found - Blue Forest</title>
<meta content="noindex, follow" name="robots"/>
<meta content="en_GB" property="og:locale"/>
<meta content="Page not found - Blue Forest" property="og:title"/>
<meta content="Blue Forest" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.blueforest.com/#website","url":"https://www.blueforest.com/","name":"Blue Forest","description":"","potentialAction":[{"@type":"SearchAction","target":"https://www.blueforest.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-GB"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//www.blueforest.com" rel="dns-prefetch"/>
<link href="//www.google.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.blueforest.com/feed/" rel="alternate" title="Blue Forest » Feed" type="application/rss+xml"/>
<link href="https://www.blueforest.com/comments/feed/" rel="alternate" title="Blue Forest » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.blueforest.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.blueforest.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.blueforest.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.9" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.blueforest.com/wp-content/plugins/sharethis-share-buttons/css/mu-style.css?ver=5.4.4" id="share-this-share-buttons-sticky-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.blueforest.com/wp-content/plugins/wp-popups-lite/src/assets/css/wppopups-base.css?ver=2.0.3.6" id="wppopups-base-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.blueforest.com/wp-content/themes/blueforest/css/jquery.fancybox.min.css?ver=5.4.4" id="site_fancybox_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.blueforest.com/wp-content/themes/blueforest/css/build/main.min.css?ver=5.4.4" id="site_main_css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.blueforest.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.blueforest.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.blueforest.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.blueforest.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-6038521-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-6038521-1');
</script>
<script>
        var ajaxurl ='https://www.blueforest.com/wp-admin/admin-ajax.php';
     </script><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style> <script>
    var siteUrl = 'https://www.blueforest.com/wp-content/themes/blueforest';
</script>
<!-- Facebook Pixel Code -->
<script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '900321000042341');
        fbq('track', "PageView");</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=900321000042341&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6038521-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body class="error404 hfeed">
<div class="site" id="page">
<header class="site-header darkheader " id="masthead">
<div class="wrapper header-wrapper">
<nav class="left-navigation">
<ul class="menu" id="menu-top-menu-left"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-235" id="menu-item-235"><a href="https://www.blueforest.com/our-work/">Our Work</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18319" id="menu-item-18319"><a href="https://www.blueforest.com/up-in-the-trees/">Up in the Trees</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18320" id="menu-item-18320"><a href="https://www.blueforest.com/made-to-stay/">Made to Stay</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18317" id="menu-item-18317"><a href="https://www.blueforest.com/built-on-the-ground/">Built on the Ground</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18318" id="menu-item-18318"><a href="https://www.blueforest.com/created-for-play/">Created for Play</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-296" id="menu-item-296"><a href="https://www.blueforest.com/our-process/">Our Process</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-19930" id="menu-item-19930"><a href="#">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14345" id="menu-item-14345"><a href="https://www.blueforest.com/our-story/">Our Story</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19931" id="menu-item-19931"><a href="https://www.blueforest.com/our-team/">Our Team</a></li>
</ul>
</li>
</ul> </nav>
<div class="site-logo">
<a href="https://www.blueforest.com/"><?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 24.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg style="enable-background:new 0 0 196.9 90.1;" version="1.1" viewbox="0 0 196.9 90.1" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<style type="text/css">
	.st_bg{fill:none;}
	.st0{fill:#0C1B47;}
</style>
<g id="clear_space">
<rect class="st_bg" height="124.7" width="232.4" x="-17.7" y="-17.3"></rect>
</g>
<g id="logo">
<g>
<g>
<path class="st0" d="M0,58.5h9.1c3.3,0,5,2.1,5,4.4c0,2.2-1.4,3.6-3,4c1.8,0.3,3.3,2.1,3.3,4.3c0,2.6-1.7,4.7-5,4.7H0V58.5z
				 M8.3,65.5c1.2,0,2-0.8,2-1.9c0-1.1-0.8-1.9-2-1.9H3.7v3.8H8.3z M8.5,72.7c1.4,0,2.2-0.8,2.2-2.1c0-1.1-0.8-2-2.2-2H3.7v4.1H8.5z
				"></path>
<path class="st0" d="M20.4,58.5H24v14.1h7.3v3.2h-11V58.5z"></path>
<path class="st0" d="M36.5,58.5h3.7v10.3c0,2.4,1.3,4.1,4,4.1c2.7,0,4-1.7,4-4.1V58.5h3.7v10.4c0,4.3-2.5,7.3-7.7,7.3
				c-5.3,0-7.8-3-7.8-7.2V58.5z"></path>
<path class="st0" d="M58.5,58.5h12.3v3.2h-8.6v3.8h8.4v3.2h-8.4v4h8.6v3.2H58.5V58.5z"></path>
<path class="st0" d="M86.6,58.5h12.3v3.2h-8.6v3.8h8.4v3.2h-8.4v7.2h-3.7V58.5z"></path>
<path class="st0" d="M112.7,58.2c5.3,0,9.1,3.7,9.1,9s-3.8,9-9.1,9c-5.2,0-9-3.7-9-9S107.5,58.2,112.7,58.2z M112.7,61.5
				c-3.2,0-5.3,2.4-5.3,5.7c0,3.2,2.1,5.7,5.3,5.7s5.3-2.5,5.3-5.7C118,63.9,115.9,61.5,112.7,61.5z"></path>
<path class="st0" d="M133.9,69.6h-2.7v6.2h-3.7V58.5h8.1c3.6,0,5.8,2.4,5.8,5.6c0,3.1-1.9,4.7-3.8,5.1l3.9,6.6h-4.2L133.9,69.6z
				 M135.1,61.7h-3.9v4.8h3.9c1.5,0,2.6-0.9,2.6-2.4C137.7,62.6,136.6,61.7,135.1,61.7z"></path>
<path class="st0" d="M147.4,58.5h12.3v3.2h-8.6v3.8h8.4v3.2h-8.4v4h8.6v3.2h-12.3V58.5z"></path>
<path class="st0" d="M166.5,70.6c1.2,1.3,3.1,2.3,5.5,2.3c2,0,3-1,3-2c0-1.3-1.5-1.7-3.5-2.2c-2.8-0.7-6.5-1.4-6.5-5.3
				c0-2.9,2.5-5.2,6.6-5.2c2.8,0,5,0.8,6.8,2.4l-2.1,2.7c-1.4-1.3-3.3-1.9-5-1.9c-1.7,0-2.5,0.7-2.5,1.8c0,1.2,1.5,1.5,3.5,2
				c2.9,0.7,6.5,1.5,6.5,5.4c0,3.2-2.3,5.6-6.9,5.6c-3.3,0-5.7-1.1-7.4-2.8L166.5,70.6z"></path>
<path class="st0" d="M188.2,61.7h-5.1v-3.2h13.8v3.2h-5v14.1h-3.7V61.7z"></path>
</g>
<g>
<path class="st0" d="M14,84.2h-2.1v-0.9h5.1v0.9H15V90h-1V84.2z"></path>
<path class="st0" d="M25.8,87h-3.7v3h-1v-6.7h1v2.8h3.7v-2.8h1V90h-1V87z"></path>
<path class="st0" d="M31.3,83.3h4.5v0.9h-3.5v1.9h3.4V87h-3.4v2.1h3.5V90h-4.5V83.3z"></path>
<path class="st0" d="M47.2,84.2h-2.1v-0.9h5.1v0.9h-2.1V90h-1V84.2z"></path>
<path class="st0" d="M56.6,87.4h-1.2V90h-1v-6.7h2.8c1.3,0,2.1,0.8,2.1,2c0,1.2-0.8,1.8-1.7,1.9l1.7,2.7h-1.2L56.6,87.4z
				 M57,84.2h-1.7v2.3H57c0.7,0,1.2-0.5,1.2-1.2C58.2,84.7,57.7,84.2,57,84.2z"></path>
<path class="st0" d="M63.5,83.3H68v0.9h-3.5v1.9h3.4V87h-3.4v2.1H68V90h-4.5V83.3z"></path>
<path class="st0" d="M72.2,83.3h4.5v0.9h-3.5v1.9h3.4V87h-3.4v2.1h3.5V90h-4.5V83.3z"></path>
<path class="st0" d="M85.6,87H82v3h-1v-6.7h1v2.8h3.7v-2.8h1V90h-1V87z"></path>
<path class="st0" d="M94.2,83.2c2,0,3.4,1.5,3.4,3.4s-1.4,3.4-3.4,3.4s-3.4-1.5-3.4-3.4S92.2,83.2,94.2,83.2z M94.2,84.1
				c-1.4,0-2.3,1.1-2.3,2.6c0,1.5,0.9,2.6,2.3,2.6c1.4,0,2.3-1.1,2.3-2.6C96.5,85.2,95.6,84.1,94.2,84.1z"></path>
<path class="st0" d="M101.8,83.3h1v4c0,1.1,0.6,1.9,1.8,1.9c1.2,0,1.8-0.7,1.8-1.9v-4h1v4c0,1.7-0.9,2.7-2.8,2.7
				s-2.8-1.1-2.8-2.7V83.3z"></path>
<path class="st0" d="M112,88.3c0.5,0.5,1.2,0.9,2.1,0.9c1.1,0,1.5-0.5,1.5-1c0-0.7-0.8-0.9-1.6-1.1c-1.1-0.3-2.3-0.6-2.3-1.9
				c0-1.1,1-1.9,2.4-1.9c1,0,1.8,0.3,2.4,0.9l-0.6,0.7c-0.5-0.5-1.2-0.8-1.9-0.8c-0.7,0-1.2,0.4-1.2,0.9c0,0.6,0.7,0.8,1.5,1
				c1.1,0.3,2.4,0.6,2.4,2c0,1-0.7,2-2.5,2c-1.2,0-2.1-0.4-2.6-1.1L112,88.3z"></path>
<path class="st0" d="M120.8,83.3h4.5v0.9h-3.5v1.9h3.4V87h-3.4v2.1h3.5V90h-4.5V83.3z"></path>
<path class="st0" d="M135.1,83.3h2.8c1.4,0,2.1,0.9,2.1,2s-0.8,2-2.1,2h-1.8V90h-1V83.3z M137.8,84.2h-1.7v2.3h1.7
				c0.7,0,1.2-0.5,1.2-1.2C139,84.7,138.5,84.2,137.8,84.2z"></path>
<path class="st0" d="M144.1,83.3h4.5v0.9h-3.5v1.9h3.4V87h-3.4v2.1h3.5V90h-4.5V83.3z"></path>
<path class="st0" d="M155.9,83.2c2,0,3.4,1.5,3.4,3.4s-1.4,3.4-3.4,3.4c-2,0-3.4-1.5-3.4-3.4S153.9,83.2,155.9,83.2z M155.9,84.1
				c-1.4,0-2.3,1.1-2.3,2.6c0,1.5,0.9,2.6,2.3,2.6c1.4,0,2.3-1.1,2.3-2.6C158.2,85.2,157.3,84.1,155.9,84.1z"></path>
<path class="st0" d="M163.4,83.3h2.8c1.4,0,2.1,0.9,2.1,2s-0.8,2-2.1,2h-1.8V90h-1V83.3z M166.1,84.2h-1.7v2.3h1.7
				c0.7,0,1.2-0.5,1.2-1.2C167.3,84.7,166.8,84.2,166.1,84.2z"></path>
<path class="st0" d="M172.4,83.3h1v5.8h3V90h-4V83.3z"></path>
<path class="st0" d="M180.5,83.3h4.5v0.9h-3.5v1.9h3.4V87h-3.4v2.1h3.5V90h-4.5V83.3z"></path>
</g>
<path class="st0" d="M117.8,24.6c0-3.5-1.7-6.5-4.3-8.4c0.9-1.4,1.4-3,1.4-4.8c0-4.9-3.9-8.8-8.8-8.9l-5.3,0V0h-6v14.3l-11.2-3.9
			v6.1l11.2,3.9v8.6l-15.5-4.6v6.1L94.7,35v11.8h6V35l7,0C113.3,34.9,117.8,30.2,117.8,24.6z M108.8,11.4c0,1.5-1.1,2.8-2.7,2.8
			h-5.4V8.6l5.2,0C107.5,8.6,108.8,9.9,108.8,11.4z M107.4,28.9L107.4,28.9l-6.7,0v-8.6h6.7c2.3,0,4.3,2,4.3,4.3
			C111.7,27,109.8,28.9,107.4,28.9z"></path>
<g>
<path class="st0" d="M118.6,0.4h-0.8V0h2v0.4H119v2.1h-0.4V0.4z"></path>
<path class="st0" d="M122.4,0.6l-0.8,1.9h-0.2l-0.8-1.9v1.9h-0.4V0h0.6l0.7,1.7l0.7-1.7h0.6v2.5h-0.4V0.6z"></path>
</g>
</g>
</g>
</svg>
</a>
</div>
<nav class="right-navigation">
<ul class="menu" id="menu-top-menu-right"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19935" id="menu-item-19935"><a href="https://blue-forest-treehouses.myshopify.com/" rel="noopener noreferrer" target="_blank">Our Shop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14343" id="menu-item-14343"><a href="https://www.blueforest.com/our-news/">Our News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14344" id="menu-item-14344"><a href="https://www.blueforest.com/contact-us/">Contact Us</a></li>
</ul> </nav>
<nav class="mobile-navigation">
<ul class="menu" id="menu-mobile-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-17231" id="menu-item-17231"><a href="https://www.blueforest.com/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-18506" id="menu-item-18506"><a href="#">Our Work</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17625" id="menu-item-17625"><a href="https://www.blueforest.com/our-work/">View all work</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18509" id="menu-item-18509"><a href="https://www.blueforest.com/up-in-the-trees/">Up in the Trees</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18510" id="menu-item-18510"><a href="https://www.blueforest.com/made-to-stay/">Made to Stay</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18507" id="menu-item-18507"><a href="https://www.blueforest.com/built-on-the-ground/">Built on the Ground</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18508" id="menu-item-18508"><a href="https://www.blueforest.com/created-for-play/">Created for Play</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17622" id="menu-item-17622"><a href="https://www.blueforest.com/our-process/">Our Process</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-19932" id="menu-item-19932"><a href="#">About Us</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17623" id="menu-item-17623"><a href="https://www.blueforest.com/our-story/">Our Story</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17624" id="menu-item-17624"><a href="https://www.blueforest.com/our-team/">Our Team</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19936" id="menu-item-19936"><a href="https://blue-forest-treehouses.myshopify.com/" rel="noopener noreferrer" target="_blank">Our Shop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17621" id="menu-item-17621"><a href="https://www.blueforest.com/our-news/">Our News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17626" id="menu-item-17626"><a href="https://www.blueforest.com/contact-us/">Contact Us</a></li>
</ul> </nav>
<div class="hamburger-container">
<!-- <span class="hamburger-menu hamburger-link hamburger-closed">
            <button class="hamburger"></button>
          </span> -->
<div class=" icon nav-icon-5" id="hamburger">
<span></span>
<span></span>
<span></span>
</div>
</div>
</div>
</header>
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<div class="topheader thwl autoheight-header parallaxbg">
<div class="wrapper leaves-header ">
<div class="topheader-leaves-content top-header-contact">
<h1 class="part-italic-title headerfade"><span>404</span> Error</h1>
<p class="headerfade"></p><p>We’re sorry, we are unable to locate the page you are looking for. This page may have been moved or deleted.</p>
<div class="cta_container">
<a class="btn-orange-button" href="https://www.blueforest.com/">back to homepage</a>
</div>
</div>
</div>
</div>
<div class="wrapper">
<div class="top-border-for-title">
<h3 class="h3"><span>Our</span> Work</h3>
</div>
</div>
<div class="our-services-grid our-services-grid-with-title">
<div class="our-services-grid-wrapp">
<div class="our-services-grid-item">
<a href="https://www.blueforest.com/up-in-the-trees/">
<figure>
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/designed-for-families-1.jpg"/>
<span class="imggradient"></span>
<figcaption>
<span class="part-italic-title">Designed for <span>Families</span></span>
</figcaption>
</figure>
</a>
</div>
<div class="our-services-grid-item">
<a href="https://www.blueforest.com/made-to-stay/">
<figure>
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/made-to-stay.jpg"/>
<span class="imggradient"></span>
<figcaption>
<span class="part-italic-title">Made to <span class="last">Stay</span></span>
</figcaption>
</figure>
</a>
</div>
<div class="our-services-grid-item">
<a href="https://www.blueforest.com/built-on-the-ground/">
<figure>
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/built-for-education.jpg"/>
<span class="imggradient"></span>
<figcaption>
<span class="part-italic-title">Built on the <span class="last">Ground</span></span>
</figcaption>
</figure>
</a>
</div>
<div class="our-services-grid-item">
<a href="https://www.blueforest.com/created-for-play/">
<figure>
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/created-for-play.jpg"/>
<span class="imggradient"></span>
<figcaption>
<span class="part-italic-title">Created for <span class="last">Play</span></span>
</figcaption>
</figure>
</a>
</div>
</div>
</div>
</main><!-- #main -->
</div><!-- #primary -->
<!--  end latest_news-->
<aside class="beinspired-container">
<div class="wrapper">
<h3>Be inspired</h3>
<a class="line-link" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">follow us</a>
</div>
<div class="instagram-masonry">
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-our-story-03.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-our-work-09.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-designed-for-families-02.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-made-to-stay-08.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/lake-house.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-our-process-07.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-made-to-stay-01.jpg"/>
</a>
</div>
<div class="instagram-masonry-text">
<p>Sit back, relax &amp; let<br/>
your imagination run wild!</p>
<a class="orange-button" href="https://www.blueforest.com/contact-us/">Contact Us</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-our-work-01.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-our-work-04.jpg"/>
</a>
</div>
<div class="instagram-masonry-image">
<a class="hover-image" href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="nofollow noopnener" target="_blank">
<img alt="" src="https://www.blueforest.com/wp-content/uploads/2019/09/be-inspired-designed-for-families-06.jpg"/>
</a>
</div>
</div>
</aside>
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="wrapper wrapper_withpadding footer_wrapper">
<div class="footer-col">
<span class="h4">Get in touch</span>
<p><a href="tel:01892750090">01892 75 00 90</a></p>
<a href="mailto:info@blueforest.com">info@blueforest.com</a>
<a class="footer-download" href="https://www.blueforest.com/contact-us#our-brouchures">Download Brochure</a>
</div>
<div class="footer-col">
<span class="h4">Find us</span>
<p>Blue Forest (UK) Ltd<br/>
The Studio, Bensfield Farm,<br/>
Beech Hill, Wadhurst,<br/>
East Sussex,<br/>
TN5 6JR</p>
</div>
<div class="footer-col">
<span class="h4">Legal</span>
<a href="https://www.blueforest.com/privacy-policy/">Privacy Policy</a>
<a href="https://www.blueforest.com/data-protection-policy/">Data Protection Policy</a>
<a href="https://www.blueforest.com/website-terms-and-conditions/">Terms &amp; Conditions</a>
</div>
<div class="footer-col">
<span class="h4">Follow us</span>
<ul class="social">
<li>
<a href="https://twitter.com/BlueForest_" rel="noreferrer nofollow noopener" style="background-image: url('https://www.blueforest.com/wp-content/uploads/2019/05/footer-twitter.svg');" target="_blank">Social link https://twitter.com/BlueForest_</a>
</li>
<li>
<a href="https://www.facebook.com/BlueForest.treehouse" rel="noreferrer nofollow noopener" style="background-image: url('https://www.blueforest.com/wp-content/uploads/2019/05/footer-facebook.svg');" target="_blank">Social link https://www.facebook.com/BlueForest.treehouse</a>
</li>
<li>
<a href="https://www.instagram.com/blueforest_treehouses/?hl=en" rel="noreferrer nofollow noopener" style="background-image: url('https://www.blueforest.com/wp-content/uploads/2019/05/footer-instagram.svg');" target="_blank">Social link https://www.instagram.com/blueforest_treehouses/?hl=en</a>
</li>
<li>
<a href="https://www.pinterest.co.uk/blueforest/" rel="noreferrer nofollow noopener" style="background-image: url('https://www.blueforest.com/wp-content/uploads/2019/05/footer-pinterest.svg');" target="_blank">Social link https://www.pinterest.co.uk/blueforest/</a>
</li>
<li>
<a href="https://www.youtube.com/channel/UC3eBzM2W9tc21ofvjEMkpQg" rel="noreferrer nofollow noopener" style="background-image: url('https://www.blueforest.com/wp-content/uploads/2019/09/youtube-01.svg');" target="_blank">Social link https://www.youtube.com/channel/UC3eBzM2W9tc21ofvjEMkpQg</a>
</li>
</ul>
</div>
<div class="footer-copy">
<p>© 2021 Blue Forest Ltd. All rights reserved.</p>
</div>
<div class="footer-tree">
<img alt="Footer threehouse" src="https://www.blueforest.com/wp-content/themes/blueforest/images/footer-tree.svg"/>
</div>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<div class="scrolltotop" id="scrolltotop">
<svg id="Layer_1" style="enable-background:new 0 0 37 37;" version="1.1" viewbox="0 0 37 37" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<g>
<polygon class="st0" points="18.5,21.5 13.9,21.5 16.2,17.5 18.5,13.5 20.8,17.5 23.1,21.5 		"></polygon>
</g>
<g>
<path class="st0" d="M18.5,37C8.3,37,0,28.7,0,18.5S8.3,0,18.5,0S37,8.3,37,18.5S28.7,37,18.5,37z M18.5,1C8.9,1,1,8.9,1,18.5
  			S8.9,36,18.5,36S36,28.1,36,18.5S28.1,1,18.5,1z"></path>
</g>
</g>
</svg>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
<script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2y1M8DFLfafPubcYM5lDWU2G0Vysh_k8"></script>
<script>
  window.addEventListener('load',function(){

    jQuery('[href*="mailto:"]').click(function(){
      ga('send','event','email','click',jQuery(this).attr('href'));
    });

    jQuery('[href*="tel:"]').click(function(){
      ga('send','event','number','click',jQuery(this).attr('href'));
    });

    jQuery('[href*="BlueForest_Residential-Brochure_2018.pdf"], [href*="BlueForest_Commercial-Brochure_2019.pdf"]').click(function(){
      ga('send','event','brochure','click',jQuery(this).attr('href').split('/09/')[1])
    });

  });
</script>
<div class="wppopups-whole" style="display: none"></div><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.blueforest.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script src="https://www.blueforest.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.9" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6Le04bsUAAAAALLMfJQEcx35PCORadlAja-osjIL&amp;ver=3.0" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=7.4.4" type="text/javascript"></script>
<script type="text/javascript">
( 'fetch' in window ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-url.min.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="https://www.blueforest.com/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js?ver=2.0.2"></scr' + 'ipt>' );
</script>
<script src="https://www.blueforest.com/wp-includes/js/dist/hooks.min.js?ver=552e55b6e60db2edbd9073097f2686f7" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wppopups_vars = {"is_admin":"","ajax_url":"https:\/\/www.blueforest.com\/wp-admin\/admin-ajax.php","pid":"0","is_front_page":"","is_blog_page":"","is_category":"","site_url":"https:\/\/www.blueforest.com","is_archive":"","is_search":"","is_singular":"","is_preview":"","facebook":"","twitter":"","nonce":"e8c28fe055"};
/* ]]> */
</script>
<script src="https://www.blueforest.com/wp-content/plugins/wp-popups-lite/src/assets/js/wppopups.js?ver=2.0.3.6" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-content/themes/blueforest/js/jquery.fancybox.min.js?ver=3.5.7" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-content/themes/blueforest/js/slick.min.js?ver=1.8.0" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-content/themes/blueforest/js/build/app.js" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-content/themes/blueforest/js/objectFitPolyfill.min.js?ver=1.0.1" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-content/themes/blueforest/js/skip-link-focus-fix.js?ver=20151215" type="text/javascript"></script>
<script src="https://www.blueforest.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
( function( sitekey, actions ) {

	document.addEventListener( 'DOMContentLoaded', function( event ) {
		var wpcf7recaptcha = {

			execute: function( action ) {
				grecaptcha.execute(
					sitekey,
					{ action: action }
				).then( function( token ) {
					var event = new CustomEvent( 'wpcf7grecaptchaexecuted', {
						detail: {
							action: action,
							token: token,
						},
					} );

					document.dispatchEvent( event );
				} );
			},

			executeOnHomepage: function() {
				wpcf7recaptcha.execute( actions[ 'homepage' ] );
			},

			executeOnContactform: function() {
				wpcf7recaptcha.execute( actions[ 'contactform' ] );
			},

		};

		grecaptcha.ready(
			wpcf7recaptcha.executeOnHomepage
		);

		document.addEventListener( 'change',
			wpcf7recaptcha.executeOnContactform, false
		);

		document.addEventListener( 'wpcf7submit',
			wpcf7recaptcha.executeOnHomepage, false
		);

	} );

	document.addEventListener( 'wpcf7grecaptchaexecuted', function( event ) {
		var fields = document.querySelectorAll(
			"form.wpcf7-form input[name='g-recaptcha-response']"
		);

		for ( var i = 0; i < fields.length; i++ ) {
			var field = fields[ i ];
			field.setAttribute( 'value', event.detail.token );
		}
	} );

} )(
	'6Le04bsUAAAAALLMfJQEcx35PCORadlAja-osjIL',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
<div class="cookieConsent fixedBottom">
<p>
			This website uses tracking cookies to improve user experience. By using our website you consent to all tracking cookies in accordance with our <a href="https://www.blueforest.com/data-protection-policy/" target="_blank">Data Protection Policy</a>.
		</p>
<span class="accept">Accept</span>
<span class="dismiss"></span>
</div>
<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = 'https://www.blueforest.com/thank-you/';
}, false );
</script>
</body></html>
