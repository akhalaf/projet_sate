<!DOCTYPE html>
<html lang="it" ng-app="ALD">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<meta content="_csrf" name="csrf-param"/>
<meta content="smld6RAd7AuIlFZm2tg4MaOYtZrjda96YEZy3SZG_uBQI600lq61oHu3fidyVFMNIdhJTjFqvwMkiLP2fJhu9w==" name="csrf-token"/>
<title>Login</title>
<script src="/js/polyfill.js"></script>
<link href="/assets/518f2e31/angular-loading-bar/build/loading-bar.min.css?v=1458159618" rel="stylesheet"/>
<link href="/assets/518f2e31/animate.css/animate.min.css?v=1465234672" rel="stylesheet"/>
<link href="/assets/518f2e31/angular-toastr/dist/angular-toastr.min.css?v=1471818311" rel="stylesheet"/>
<link href="/css/site.css" rel="stylesheet"/>
<!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="style/css/ie9-FIX.css">
    <![endif]-->
</head>
<body>
<div class="container-fluid padding_zero" id="scroll-top">
<script type="text/javascript">
    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    createCookie("hidePopUpMessage", "", -1);
    createCookie("hideNewsModal", "", -1);
    createCookie("hidePassExpireRemindPopup", "", -1);
</script>
<div class="site-login" id="login-form-container">
<!-- START Login -->
<div class="centered">
<div class="contcustom" id="contentdiv">
<div>
<span class="fa fa-car bigicon"></span>
<form action="/site/login" id="registration-form" method="post">
<input name="_csrf" type="hidden" value="smld6RAd7AuIlFZm2tg4MaOYtZrjda96YEZy3SZG_uBQI600lq61oHu3fidyVFMNIdhJTjFqvwMkiLP2fJhu9w=="/>
<div class="form-group field-loginform-username required">
<input aria-required="true" id="loginform-username" name="LoginForm[username]" placeholder="Nome utente" type="text"/>
<div class="popover errors">
<div class="arrow"></div>
<div class="popover-content">
<div class="help-block"></div>
</div>
</div>
</div>
<div class="form-group field-loginform-password required">
<input aria-required="true" id="loginform-password" name="LoginForm[password]" placeholder="Password" type="password"/>
<div class="popover errors">
<div class="arrow"></div>
<div class="popover-content">
<div class="help-block"></div>
</div>
</div>
</div>
<button class="btn btn-primary btn-lg btn-block" name="login-button" type="submit">Effettua il login</button> </form> <br/>
<a href="/site/remind">Recupera Password</a>
</div>
</div>
</div>
<!-- END Login -->
</div>
</div>
<script src="/assets/e4247871/jquery.js?v=1463765083"></script>
<script src="/assets/81562be4/yii.js?v=1521658083"></script>
<script src="/assets/81562be4/yii.validation.js?v=1521658083"></script>
<script src="/assets/81562be4/yii.activeForm.js?v=1521658083"></script>
<script src="/assets/518f2e31/angular/angular.min.js?v=1591287442"></script>
<script src="/assets/518f2e31/angular-cookies/angular-cookies.min.js?v=1591287443"></script>
<script src="/assets/518f2e31/angular-loader/angular-loader.min.js?v=1591287443"></script>
<script src="/assets/518f2e31/jquery/dist/jquery.js?v=1463765083"></script>
<script src="/assets/518f2e31/angular-mocks/angular-mocks.js?v=1591287443"></script>
<script src="/assets/518f2e31/angular-resource/angular-resource.min.js?v=1591287443"></script>
<script src="/assets/518f2e31/angular-route/angular-route.min.js?v=1601386245"></script>
<script src="/assets/518f2e31/angular-sanitize/angular-sanitize.min.js?v=1601386253"></script>
<script src="/assets/518f2e31/angular-scenario/angular-scenario.js?v=1579693501"></script>
<script src="/assets/518f2e31/angular-touch/angular-touch.min.js?v=1591287444"></script>
<script src="/assets/518f2e31/angular-animate/angular-animate.min.js?v=1591287442"></script>
<script src="/assets/518f2e31/angular-ui-router/release/angular-ui-router.min.js?v=1507164215"></script>
<script src="/js/ng-infinite-scroll.min.js"></script>
<script src="/assets/518f2e31/bootstrap/dist/js/bootstrap.min.js?v=1550073338"></script>
<script src="/assets/518f2e31/angular-ui-indeterminate/dist/indeterminate.min.js?v=1435720770"></script>
<script src="/assets/518f2e31/angular-ui-uploader/dist/uploader.min.js?v=1492668846"></script>
<script src="/assets/518f2e31/angular-ui-validate/dist/validate.min.js?v=1495094686"></script>
<script src="/assets/518f2e31/angular-ui-mask/dist/mask.min.js?v=1469548748"></script>
<script src="/assets/518f2e31/angular-ui-event/dist/event.min.js?v=1435720751"></script>
<script src="/assets/518f2e31/angular-ui-scroll/dist/ui-scroll.min.js?v=1589378681"></script>
<script src="/assets/518f2e31/angular-ui-scrollpoint/dist/scrollpoint.min.js?v=1456104909"></script>
<script src="/assets/518f2e31/angular-ui-utils/index.js?v=1437443592"></script>
<script src="/assets/518f2e31/angular-bootstrap/ui-bootstrap-tpls.min.js?v=1485610379"></script>
<script src="/assets/518f2e31/angular-highlightjs/angular-highlightjs.min.js?v=1483602175"></script>
<script src="/assets/518f2e31/angular-i18n/angular-locale_it.js?v=1591287443"></script>
<script src="/assets/518f2e31/angular-filter/dist/angular-filter.min.js?v=1506097204"></script>
<script src="/assets/518f2e31/angular-loading-bar/build/loading-bar.min.js?v=1458159618"></script>
<script src="/assets/518f2e31/angular-toastr/dist/angular-toastr.tpls.min.js?v=1471818311"></script>
<script src="/assets/518f2e31/angular-translate/angular-translate.min.js?v=1594240416"></script>
<script src="/js/home.js"></script>
<script type="text/javascript">jQuery(document).ready(function () {
jQuery('#registration-form').yiiActiveForm([{"id":"loginform-username","name":"username","container":".field-loginform-username","input":"#loginform-username","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Username non può essere vuoto."});value = yii.validation.trim($form, attribute, []);}},{"id":"loginform-password","name":"password","container":".field-loginform-password","input":"#loginform-password","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Password non può essere vuoto."});value = yii.validation.trim($form, attribute, []);}}], []);
});</script><div class="hidden" id="version">1.4.3-0e47a22</div>
</body>
</html>
