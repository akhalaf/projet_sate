<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://feeds.feedburner.com/betapage" rel="alternate" title="BetaPage - Community of tech lovers and early adopters" type="application/atom+xml"/>
<title>BetaPage - Community of tech lovers and early adopters</title>
<meta content="Community of tech lovers and early adopters. Browse, search or submit your product or startup" name="description"/>
<meta content=" Latest Tech Startups, Startup Community, Startup List, early adopters showcase, Tech Startups, Startups." name="keywords"/>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="#4A90E2" name="theme-color"/>
<meta content="Xu3dw7XmtBXBsjzehb_9dPVEy2GzJ_far2cRechbwcA" name="google-site-verification"/>
<link href="https://betapage.co/manifest.json" rel="manifest"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet"/>
<link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet"/>
<link href="https://betapage.co/assets/css/style.min.css?v=220219" rel="stylesheet"/>
<link href="https://betapage.co/assets/css/addon.css" rel="stylesheet"/>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
<div class="container-fluid">
<div class="navbar-header">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="https://betapage.co/"><img alt="BetaPage logo" src="https://betapage.co/assets/img/logo.png"/></a>
</div>
<div class="navbar-collapse collapse" id="navbar">
<ul class="nav navbar-nav navbar-right">
<li><a href="https://deals.betapage.co" style="position:relative; margin-right:20px;" target="_blank">Deals <span style="position:absolute; background:red; color:white; font-size:8px; padding:3px; border-radius:3px; line-height:8px; right:-10px; top:5px;">NEW</span></a></li>
<li><a href="https://betapage.co/submit-startup">Submit</a></li>
<li><a data-target="#login_modal" data-toggle="modal" href="javascript:void(0);">Login / Register</a></li>
</ul>
<form action="https://betapage.co/search" class="navbar-form" id="search-box" method="get">
<input name="query" type="hidden" value=""/>
<input class="search form-control input-lg" placeholder="Search..." type="search" value=""/>
<input hidefocus="true" style="height: 0px; width: 0px; border: none; padding: 0px;" type="submit"/>
</form>
</div>
</div>
</nav>
<div class="container height-wraper">
<div class="row">
<div class="col-md-8">
<div class="top_recent_switch">
<a class="active" href="https://betapage.co/">Trending</a>
<a href="https://betapage.co/new">New</a>
</div>
<form name="entitylist">
<input name="entity_type" type="hidden" value=""/>
<input name="order_by" type="hidden" value="trending"/>
<input name="keyword" type="hidden" value=""/>
<input name="next" type="hidden" value=""/>
<div class="product_items_container">
<div class="preload_filler">
Loading....Please wait...
</div>
</div>
<a class="btn-default load_more" href="javascriot:void(0);" style="display:none;">Load More</a>
</form>
</div>
<div class="col-md-4">
<div class="m-b-40 m-t-10"></div>
<div class="panel m-b-25">
<div class="text-center">
<h4 class="m-b-25 dark">Get new startups in your inbox </h4>
<form method="post" name="subscribe" onsubmit="return subscribe_by_email($(this));">
<div class="input-group round">
<input class="form-control-group" name="email" placeholder="Email Address" type="email"/>
<div class="input-group-btn">
<button class="btn btn-primary" type="submit">
<i class="ion-android-arrow-forward"></i>
</button>
</div>
</div>
<p class="text-primary m-t-10">30,000+ Newsletter Subscribers</p>
</form>
</div>
</div>
<div class="panel m-b-25">
<a class="btn btn-yellow dark btn-block rounded-0" href="https://betapage.co/add-startup">Submit Startup</a>
</div>
<div class="panel m-b-25">
<div class="d-flex">
<h6 class="mt-0 mb-3">
<a href="https://betapage.typeform.com/to/hBONNg" rel="nofollow"><strong>sponsored</strong></a></h6>
</div>
<a class="sponsor-item d-flex align-items-center" href="https://bit.ly/workfluence">
<img class="img-responsive" src="https://s3.amazonaws.com/betapage.co/uploads/workfluence.png"/>
<div class="info">
<h5>Workfluence</h5>
<p>Personalized, Interactive, AI-Driven Career Pathing for Professionals</p>
</div>
</a>
</div>
<div class="panel m-b-25">
<div class="d-flex">
<h5 class="mt-0 mb-3 text-primary"><strong>Join BetaPage</strong></h5>
<div class="ml-auto">
<a class="btn btn-xs btn-success px-5" data-target="#login_modal" data-toggle="modal" href="javascript:void(0);">SIGNUP</a>
</div>
</div>
<div class="d-flex mt-4">
<div class="feature-block-1">
<h5>40,000+</h5>
<span>Products and Startups</span>
</div>
<div class="feature-block-1 mx-3">
<h5>65,000+</h5>
<span>Registered Members</span>
</div>
<div class="feature-block-1">
<h5>13,000+</h5>
<span>Twitter Followers</span>
</div>
</div>
</div>
<div class="panel m-b-25 links">
<a href="https://betapage.typeform.com/to/LHglKX" rel="nofollow" target="_blank">Partnership</a>
<a href="https://blog.betapage.co" rel="nofollow" target="_blank">Blog</a>
<a href="https://blog.betapage.co/contribute-to-betapage-a61820bca84c" rel="nofollow" target="_blank">Write Guest Post</a>
<a href="/media-kit">Media Kit</a>
</div>
</div>
</div>
</div>
<footer>
<div class="row">
<div class="col-lg-3">
© 2021 BetaPage. All rights reserved.
</div>
<div align="center" class="col-lg-6 links">
<a href="https://betapage.co/terms">Terms &amp; Conditions</a>
<a href="https://betapage.co/privacy-policy">Privacy</a>
<a href="https://betapage.co/faq">FAQs</a>
<a href="mailto:support@betapage.co" rel="nofollow">Contact</a>
<a href="https://betapage.typeform.com/to/bRiGjx" rel="nofollow" target="_blank">Feedback</a>
<a href="https://blog.betapage.co/" rel="nofollow">Blog</a>
<a href="/media-kit" rel="nofollow">Media Kit</a>
</div>
<div align="right" class="col-lg-2">
<div class="social_accounts">
<a href="https://twitter.com/beta_page" rel="nofollow"><i class="ion-social-twitter"></i></a>
<a href="https://facebook.com/betapage.co" rel="nofollow"><i class="ion-social-facebook"></i></a>
</div>
</div>
</div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-67820266-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59e45947c28eca75e46263e9/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:901324,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<div class="modal fade" id="login_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body">
<button aria-label="Close" class="close" data-dismiss="modal" type="button"><i aria-hidden="true" class="ion-android-close"></i></button>
<div class="social-login">
<a class="btn btn-twitter" href="https://betapage.co/login/twitter">
<i class="ion-social-twitter"></i> Continue with Twitter</a>
<a class="btn btn-google" href="https://betapage.co/login/google">
<i class="ion-social-googleplus"></i> Continue with Google</a>
<a class="btn btn-github" href="https://betapage.co/login/github">
<i class="ion-social-github"></i> Continue with Github</a>
</div>
<div align="center" class="fs-10 m-tb-10">We don't ask for write permission.</div>
<div align="center" class="dark m-tb-10">By joining you agree to our <a href="https://betapage.co/terms"><strong>terms</strong></a> and <a href="https://betapage.co/privacy-policy"><strong>privacy policies</strong></a>.</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">

var logo_path = 'https://cdn1.betapage.co/logo/';
var IMAGE_PATH = 'https://cdn1.betapage.co/images/';
var PROFILE_PIC_PATH = 'https://cdn1.betapage.co/profile_pics/';
var LOGO_POSTFIX = '?w=200&h=200&fit=crop';
var IMAGE_POSTFIX = '?w=600&h=315&fit=crop';
var IMAGE_POSTFIX_XS = '?w=300&h=157';
var root = 'https://betapage.co/';
var loggedin = false;
var upvoted = [];
var following = [];
var current_user = false;

</script>
<script id="search-suggestion-template" type="x-tmpl-mustache">
<a class="search-result-row" href="https://betapage.co/{{#entity}}{{entity}}{{/entity}}{{^entity}}startup{{/entity}}/{{path}}">
<div class="col1"><img class="image" src="{{img}}" /></div>
<div class="col2"><h2>{{{name}}}</h2><p class="summary">{{summary}}</p></div>
</a>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js" type="text/javascript"></script>
<script src="https://betapage.co/assets/js/scripts.js" type="text/javascript"></script>
<script src="https://betapage.co/assets/js/app.js?v=220219" type="text/javascript"></script>
<script>
quickSearch();</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[name=next]').val('');
    ShowProducts([]);
    ShowProducts([{"id":"190826273","entity":"startup","path":"inventoro-com","name":"Inventoro.com","summary":"Sales forecasting, product segmentation and automatic replenishment for SMEs.","url":"https:\/\/inventoro.com\/","img":"190826273-190826414.png","tags":"[{\"id\":\"inventory-management-software\",\"name\":\"Inventory Management Software\"},{\"id\":\"logistics-optimization\",\"name\":\"logistics optimization\"},{\"id\":\"sales-forecasting\",\"name\":\"Sales forecasting\"}]","upvotes":"48","datetime":"2021-01-10 17:25:10"},{"id":"188995365","entity":"startup","path":"balloon","name":"Balloon","summary":"Balloon is a virtual events platform, focusing on attendee engagement and organizer monetization.","url":"http:\/\/joinballoon.com","img":"188995365-188995497.png","tags":"[{\"id\":\"b2b-saas\",\"name\":\"b2b saas\"},{\"id\":\"events\",\"name\":\"events\"},{\"id\":\"saas\",\"name\":\"Saas\"}]","upvotes":"48","datetime":"2021-01-11 08:32:32"},{"id":"191119647","entity":"startup","path":"voices","name":"Voices","summary":"Online marketplace that connects businesses with voice actors","url":"https:\/\/www.voices.com","img":"191119647-191119787.png","tags":"[{\"id\":\"freelancers\",\"name\":\"freelancers\"},{\"id\":\"marketplaces\",\"name\":\"Marketplaces\"},{\"id\":\"voice\",\"name\":\"voice\"}]","upvotes":"34","datetime":"2021-01-11 15:47:07"},{"id":"190150503","entity":"startup","path":"recruiting-tool-1","name":"Recruiting Tool.","summary":"Smart Recruiting SaaS","url":"https:\/\/www.RecruitingTool.io","img":"190150503-190151236.png","tags":"[{\"id\":\"hiring-and-recruiting\",\"name\":\"Hiring and Recruiting\"},{\"id\":\"recruiting-software\",\"name\":\"Recruiting Software\"},{\"id\":\"saas\",\"name\":\"Saas\"}]","upvotes":"35","datetime":"2021-01-10 17:26:40"}]);

    LoadProducts();
    $('.load_more').click(function(){ LoadProducts(); return false;});

});
/*
if(Cookies.get('subscribe') == 'hide')
{
  $('.subscribe_container').remove();
  Cookies.set('subscribe', 'hide', { expires: Infinity });
}else{

$('.subscribe_container').show();

}
$('#close_subscribe_container').click(function(){

  $('.subscribe_container').remove();
  Cookies.set('subscribe', 'hide', { expires: Infinity });
});
*/

</script>
</body>
<script>
    if ('serviceWorker' in navigator) {
      console.log("Will the service worker register?");
      navigator.serviceWorker.register('service-worker.js')
        .then(function(reg){
          console.log("Yes, it did.");
        }).catch(function(err) {
          console.log("No it didn't. This happened: ", err)
        });
    }
  </script>
</html>
