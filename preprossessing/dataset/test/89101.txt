<html>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED.  -->
<!-- DO NOT COPY THIS CODE WITHOUT PERMISSION.     -->
<!-- DO NOT MAKE DERIVATIVE WORKS.                 -->
<!-- If you are thinking of copying this code      -->
<!-- email contact@airnav.com for authorization.   -->
<head>
<title>AirNav: KCSM - Clinton-Sherman Airport</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Complete aeronautical information about Clinton-Sherman Airport (Burns Flat, OK, USA), including location, runways, taxiways, navaids, radio frequencies, FBO information, fuel prices, sunrise and sunset times, aerial photo, airport diagram." name="description"/>
<meta content="Burns Flat" name="keywords"/>
<script type="text/javascript"><!--//--><![CDATA[//><!--

sfHover = function() {
        var sfEls = document.getElementById("navres").getElementsByTagName("LI");
        for (var i=0; i<sfEls.length; i++) {
                sfEls[i].onmouseover=function() {
                        this.className+=" sfhover";
                }
                sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
                }
        }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

//--><!]]></script>
<link href="//img.airnav.com/css/css2.css?v=0" rel="StyleSheet" type="text/css"/>
<link href="//img.airnav.com/css/airport.css?v=0" rel="StyleSheet" type="text/css"/>
<base target="_top"/>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" width="100%">
<tr valign="bottom">
<td align="left" height="60" valign="top">
<a href="/"><img alt="AirNav" border="0" height="60" src="//img.airnav.com/logo/header.gif?v=HTWA8Q" width="280"/></a>
</td>
<td> </td>
<td align="right" height="60" width="468">
<a href="/ad/click/SYWlyd2lzY29uc2luMj.xOS0z" onclick="_gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-3', 'Click']);" target="_new"><img alt="Air Wisconsin Up To $57,000 Bonus" border="0" height="60" src="//img.airnav.com/banners/airwisconsin2019-3.gif?v=QHE2CR" width="468"/></a>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td width="20"> </td><td align="left">
<table border="0" cellpadding="0" cellspacing="0"><tr><td>
<div id="mainmenu">
<ul>
<li><a href="/airports/" id="airports">Airports</a>
</li>
<li><a href="/navaids/" id="navaids">Navaids</a>
</li>
<li><a href="/airspace/fix/" id="fix">Airspace Fixes</a>
</li>
<li><a href="/fuel/" id="fuel">Aviation Fuel</a>
</li>
<li><a href="/hotels/" id="hotels">Hotels</a>
</li>
<li><a href="/airboss/" id="airboss">AIRBOSS</a>
</li>
<li><a href="/iphoneapp/" id="iphoneapp">iPhone App</a>
</li>
<li><a href="https://www.airnav.com/members/login?return=//my.airnav.com/my" id="my">My AirNav</a>
</li>
</ul>
</div>
</td></tr></table>
</td></tr>
<tr><td bgcolor="#9999FF" width="20"> </td><td align="right" bgcolor="#9999FF" width="100%"><font size="-1">933 users online
 <a href="/members/login"><img alt="" border="0" height="10" src="//img.airnav.com/btn/mini/login.gif?v=JZBHPB" width="45"/></a></font> </td></tr>
</table>
<hr/>
<table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr bgcolor="#CCCCFF" valign="middle">
<td align="center" nowrap=""><font face="Verdana, Arial, Helvetica, sans-serif"><font size="5"><b>KCSM</b></font></font></td>
<td bgcolor="#FFFFFF" width="99%"> <font face="Verdana, Arial, Helvetica, sans-serif" size="+1"><b>Clinton-Sherman Airport</b><br/>Burns Flat, Oklahoma, USA</font> </td>
<td bgcolor="#FFFFFF" nowrap=""><img alt="" border="0" height="36" src="//img.airnav.com/flag/s/us?v=HTWA6S" width="60"/></td>
</tr>
</table>
<hr/>
<table border="1" cellpadding="7" cellspacing="0" width="100%">
<tr><td bgcolor="#ccccff" nowrap="" width="20%">
<h3>GOING TO BURNS FLAT?</h3>
</td><td>
<ul id="navres">
<li> <table><tr><td align="center">
<a href="/hotels/selecthotel?airport=KCSM">
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/7a2dce28-da13-4198-ab6f-73bf62b69376-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/374c4614-a416-4047-af11-28151c6c1d86-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/4ea00acf-89da-4ef1-a640-fb3fc5bcdfa6-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/e5700a21-b5a2-4f23-a4f3-0fb69c753ec4-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/8b5dda99-cfc9-4abd-b394-9f4132565f3b-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/8cea013f-f4b5-453a-8314-55d64311819c-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/1fbc4bc8-a00b-401e-bee6-96cdafdb87c6-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/7d494c47-286f-413d-bdd7-91c3184bf01d-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/73323f56-71cf-44ca-908f-e0ba1ffc337f-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/85f20123-ac3f-4cc0-a8fc-d08fa6153dae-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/3342c598-94de-42b5-8c0f-db3070afef04-120x60.gif?v=PYUGK3" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/4a572ba5-71ca-41b0-86e1-4e158064a74b-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/955e4d79-2cb1-482d-9f5e-b5146b69d191-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/5e2e1fc9-cee5-4f17-b43f-b848f546a45f-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/b88aadae-2d21-4a88-af0c-65bdf0d1ec08-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/cc66b087-f7aa-4dd8-874b-0e9dda78bc45-120x60.gif" width="60"/>
<img alt="" border="0" class="hotelbrands" height="30" src="//img.airnav.com/br/94e48416-dcb6-4ca5-9bab-6e89cab71212-120x60.gif" width="60"/>
</a><a></a></td></tr><tr><td align="center">
<font size="-1"><a href="/hotels/selecthotel?airport=KCSM">Reserve a <b>Hotel Room</b></a></font>
</td></tr></table>
<script>
var slideIndex = 0;
carousel("hotelbrands");

function carousel() {
    var c ='hotelbrands';
    var i;
    var x = document.getElementsByClassName(c);
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1} 
    x[slideIndex-1].style.display = "block"; 
    setTimeout(carousel, 500); // Change image every 2 seconds
}
</script>
</li>
</ul>
</td></tr></table>
<br/>
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td width="99%">
<h3>FAA INFORMATION EFFECTIVE 31 DECEMBER 2020</h3>
<a name="loc"></a><h3>Location</h3><table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" nowrap="">FAA Identifier: </td><td>CSM</td></tr>
<tr><td align="right" valign="top">Lat/Long: </td><td valign="top">35-20-23.4210N 099-12-01.7980W<br/>35-20.390350N 099-12.029967W<br/>35.3398392,-99.2004994<br/>(estimated)</td></tr>
<tr><td align="right" valign="top">Elevation: </td><td valign="top">1922.1 ft. / 586 m (estimated)</td></tr>
<tr><td align="right" valign="top">Variation: </td><td valign="top">05E (2020)</td></tr>
<tr><td align="right" nowrap="" valign="top">From city: </td><td valign="top">2 miles SW of BURNS FLAT, OK</td></tr>
<tr><td align="right" nowrap="" valign="top">Time zone: </td><td valign="top">UTC -6 (UTC -5 during Daylight Saving Time)</td></tr>
<tr><td align="right" nowrap="">Zip code: </td><td>73641</td></tr>
</table>
<a name="ops"></a><h3>Airport Operations</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" nowrap="" valign="top">Airport use: </td><td valign="top">Open to the public</td></tr>
<tr><td align="right" nowrap="">Activation date: </td><td>10/1971</td></tr>
<tr><td align="right" nowrap="">Control tower: </td><td>yes</td></tr>
<tr><td align="right" valign="top">ARTCC: </td><td valign="top">FORT WORTH CENTER</td></tr>
<tr><td align="right" valign="top">FSS: </td><td valign="top">MC ALESTER FLIGHT SERVICE STATION</td></tr>
<tr><td align="right" nowrap="" valign="top">NOTAMs facility: </td><td valign="top">CSM (NOTAM-D service available)</td></tr>
<tr><td align="right" valign="top">Attendance: </td><td valign="top">MON-FRI 0900-0100<br/>ATCT - EXCEPT FEDERAL HOLIDAYS</td></tr>
<tr><td align="right" nowrap="">Wind indicator: </td><td>lighted</td></tr>
<tr><td align="right" nowrap="">Segmented circle: </td><td>no</td></tr>
<tr><td align="right" valign="top">Lights: </td><td valign="top">ACTVT HIRL RWY 17R/35L; REIL RY 17R/35L; TWY LGTS &amp; WINDSOCK - 119.6. PAPI RWYS 17R &amp; 35L OPERATES CONTINOUSLY.</td></tr>
<tr><td align="right" valign="top">Beacon: </td><td valign="top">white-green (lighted land airport)<br/>Operates sunset to sunrise.</td></tr>
</table>
<a name="com"></a><h3>Airport Communications</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right">CTAF: </td><td>119.6</td></tr>
<tr><td align="right">UNICOM: </td><td>122.95</td></tr>
<tr><td align="right" nowrap="" valign="top">WX ASOS: </td><td valign="top">118.450 (580-562-4811)</td></tr>
<tr><td align="right" nowrap="" valign="top">CLINTON-SHERMAN GROUND: </td><td valign="top">121.7 239.0 [0900-0100 MON-FRI ; EXCEPT FEDERAL HOL.]</td></tr>
<tr><td align="right" nowrap="" valign="top">CLINTON-SHERMAN TOWER: </td><td valign="top">119.6 256.9 [0900-0100 MON-FRI ; EXCEPT FEDERAL HOL.]</td></tr>
<tr><td align="right" valign="top">EMERG: </td><td valign="top">121.5 243.0</td></tr>
<tr><td align="right" nowrap="" valign="top">WX AWOS-3 at ELK (11 nm NW): </td><td valign="top">118.225 (580-303-9147)</td></tr>
<tr><td align="right" nowrap="" valign="top">WX AWOS-3 at CLK (18 nm NE): </td><td valign="top">119.225 (580-323-8477)</td></tr>
</table>
<ul>
<li class="rmk">APCH/DEP SVC PRVDD BY FORT WORTH ARTCC ON FREQS 128.4/269.37 (CLINTON-SHERMAN RCAG).
</li></ul>
<a name="nav"></a><h3>Nearby radio navigation aids</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left">VOR radial/distance</th><th>  </th><th align="right">VOR name</th><th>  </th><th>Freq</th><th>  </th><th>Var</th></tr>
<tr><td><a href="/cgi-bin/navaid-info?id=BFV&amp;type=VORTAC&amp;name=BURNS+FLAT">BFV</a>r358/6.2</td><td></td><td align="right">BURNS FLAT VORTAC</td><td></td><td>110.00</td><td></td><td>05E</td></tr>
<tr><td><a href="/cgi-bin/navaid-info?id=HBR&amp;type=VORTAC&amp;name=HOBART">HBR</a>r337/29.2</td><td></td><td align="right">HOBART VORTAC</td><td></td><td>111.80</td><td></td><td>10E</td></tr>
</table>
<br/> <br/><table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left">NDB name</th><th>  </th><th>Hdg/Dist</th><th>  </th><th>Freq</th><th>  </th><th>Var</th><th>  </th><th align="left" colspan="2">ID</th></tr>
<tr><td><a href="/cgi-bin/navaid-info?type=NDB&amp;id=BZ&amp;name=FOSSI">FOSSI</a></td><td></td><td>175/6.6</td><td></td><td>393</td><td></td><td>05E</td><td></td><td>BZ</td><td> <font face="Monaco,Courier">-... --..</font></td></tr>
</table>
<a name="svcs"></a><h3>Airport Services</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" valign="top">Parking: </td><td valign="top">hangars and tiedowns</td></tr>
<tr><td align="right" nowrap="">Airframe service: </td><td>NONE</td></tr>
<tr><td align="right" nowrap="">Powerplant service: </td><td>NONE</td></tr>
<tr><td align="right" nowrap="">Bottled oxygen: </td><td>NONE</td></tr>
<tr><td align="right" nowrap="">Bulk oxygen: </td><td>NONE</td></tr>
</table>
<a name="rwys"></a><h3>Runway Information</h3>
<h4>Runway 17R/35L</h4>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" valign="top">Dimensions: </td><td colspan="3" valign="top">13503 x 200 ft. / 4116 x 61 m</td></tr>
<tr><td align="right" valign="top">Surface: </td><td colspan="3" valign="top">concrete, in good condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Weight bearing capacity: </td><td colspan="3" valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr><td nowrap="" valign="top">Single wheel: </td><td valign="top">50.0</td></tr><tr><td nowrap="" valign="top">Double wheel: </td><td valign="top">200.0</td></tr><tr><td nowrap="" valign="top">Double tandem: </td><td valign="top">390.0</td></tr></table></td></tr>
<tr><td align="right" nowrap="" valign="top">Runway edge lights: </td><td colspan="3" valign="top">high intensity<br/>NSTD HIRL. RWY LGTS LCTD 75 FT OFF EACH SIDE OF RWY.</td></tr>
<tr><td valign="top"></td><td valign="top"><b>RUNWAY 17R</b></td><td>  </td><td valign="top"><b>RUNWAY 35L</b></td></tr>
<tr><td align="right">Latitude: </td><td>35-21.579683N</td><td></td><td>35-19.354033N</td></tr>
<tr><td align="right">Longitude: </td><td>099-12.086433W</td><td></td><td>099-12.090533W</td></tr>
<tr><td align="right" valign="top">Elevation: </td><td valign="top">1922.1 ft.</td><td></td><td valign="top">1911.8 ft.</td></tr>
<tr><td align="right" nowrap="">Traffic pattern: </td><td>right</td><td></td><td>left</td></tr>
<tr><td align="right" nowrap="" valign="top">Runway heading: </td><td valign="top">175 magnetic, 180 true</td><td></td><td valign="top">355 magnetic, 000 true</td></tr>
<tr><td align="right" valign="top">Markings: </td><td valign="top">precision, in good condition</td><td></td><td valign="top">precision, in good condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Visual slope indicator: </td><td valign="top">4-light PAPI on left (3.00 degrees glide path)</td><td></td><td valign="top">4-light PAPI on left (3.00 degrees glide path)</td></tr>
<tr><td align="right" nowrap="">Runway end identifier lights: </td><td>yes</td><td></td><td>yes</td></tr>
<tr><td align="right" nowrap="" valign="top">Touchdown point: </td><td valign="top">yes, no lights</td><td></td><td valign="top">yes, no lights</td></tr>
<tr><td align="right" nowrap="">Instrument approach: </td><td>ILS</td><td></td><td></td></tr>
</table>
<h4>Runway 17L/35R</h4>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" valign="top">Dimensions: </td><td colspan="3" valign="top">5193 x 75 ft. / 1583 x 23 m</td></tr>
<tr><td align="right" valign="top">Surface: </td><td colspan="3" valign="top">concrete, in good condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Weight bearing capacity: </td><td colspan="3" valign="top"><table border="0" cellpadding="0" cellspacing="0"><tr><td nowrap="" valign="top">Single wheel: </td><td valign="top">50.0</td></tr><tr><td nowrap="" valign="top">Double wheel: </td><td valign="top">200.0</td></tr><tr><td nowrap="" valign="top">Double tandem: </td><td valign="top">390.0</td></tr></table></td></tr>
<tr><td valign="top"></td><td valign="top"><b>RUNWAY 17L</b></td><td>  </td><td valign="top"><b>RUNWAY 35R</b></td></tr>
<tr><td align="right">Latitude: </td><td>35-20.619350N</td><td></td><td>35-19.763450N</td></tr>
<tr><td align="right">Longitude: </td><td>099-11.876983W</td><td></td><td>099-11.878567W</td></tr>
<tr><td align="right" valign="top">Elevation: </td><td valign="top">1906.2 ft.</td><td></td><td valign="top">1906.8 ft.</td></tr>
<tr><td align="right" nowrap="">Traffic pattern: </td><td>left</td><td></td><td>right</td></tr>
<tr><td align="right" nowrap="" valign="top">Runway heading: </td><td valign="top">175 magnetic, 180 true</td><td></td><td valign="top">355 magnetic, 000 true</td></tr>
<tr><td align="right" valign="top">Markings: </td><td valign="top">basic, in fair condition</td><td></td><td valign="top">basic, in fair condition</td></tr>
<tr><td align="right" nowrap="" valign="top">Touchdown point: </td><td valign="top">yes, no lights</td><td></td><td valign="top">yes, no lights</td></tr>
</table>
<h3>Airport Ownership and Management from official FAA records</h3>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td align="right">Ownership: </td><td>Publicly-owned</td></tr>
<tr><td align="right" valign="top">Owner: </td><td valign="top">OSIDA/STATE OF OKLAHOMA<br/>BOX 689<br/>BURNS FLAT, OK 73624<br/>Phone 580-562-3500</td></tr>
<tr><td align="right" valign="top">Manager: </td><td valign="top">CRAIG J. SMITH<br/>121 1ST STREET<br/>BURNS FLAT, OK 73624<br/>Phone 580-562-3500<br/>EXECUTIVE DIRECTOR</td></tr>
</table>
<a name="stats"></a><h3>Airport Operational Statistics</h3>
<table border="0" cellpadding="0" cellspacing="0"><tr>
<td valign="top"><table border="0" cellpadding="0" cellspacing="0">
<tr valign="bottom"><td align="center" colspan="2" valign="bottom">Aircraft operations: avg 101/day<font size="-1"> *</font></td></tr><tr><td align="right" valign="top">88% </td><td valign="top">military</td></tr><tr><td align="right" valign="top">12% </td><td valign="top">transient general aviation</td></tr><tr><td colspan="2"><font size="-1">* for 12-month period ending 24 June 2020</font></td></tr></table></td></tr></table>
<a name="notes"></a><h3>Additional Remarks</h3>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">MILITARY JET TRNG: HVY JETS SFC TO 5000 FT WITHIN 25 NM RADIUS. EXTSV STUDENT PILOT TRNG FM SFC TO 5000 FT WITHIN 10 NM RADIUS BTN 0900-1200.</td></tr>
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">RWY 17L/35R IS LAID OUT ON PARL TWY TO RWY 17R/35L. RWY 17L/35R VFR DALGT USE ONLY.</td></tr>
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">MIL JET NIGHT VISION TRNG CTC ATCT 10 MINUTES PRIOR TO LNDG AFT SS TO ADJUST FIELD LIGHTING.</td></tr>
<tr><td class="rmk" valign="top">- </td><td class="rmk" valign="top">VFR ACFT ADVISED TO CTC ATC 15 NM OUT FOR SEQUENCING.</td></tr>
</table>
<a name="ifr"></a><h3>Instrument Procedures</h3>
<font size="-1">NOTE: All procedures below are presented as PDF files. If you need a reader for these files, you should <a href="/depart?http://www.adobe.com/products/acrobat/readstep2.html" target="adobereader">download</a> the free Adobe Reader.<p><strong>NOT FOR NAVIGATION</strong>. Please procure official charts for flight.<br/>FAA instrument procedures published for use from 31 December 2020 at 0901Z to 28 January 2021 at 0900Z.</p></font>
<table border="0" cellpadding="0" cellspacing="0">
<tr><th align="left" colspan="3"> <br/>IAPs - Instrument Approach Procedures</th></tr>
<tr>
<td>ILS OR LOC RWY 17R</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/00778IL17R.PDF" target="ifrproc">download</a> <font size="-1">(265KB)</font></td></tr><tr>
<td>RNAV (GPS) RWY 17R</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/00778R17R.PDF" target="ifrproc">download</a> <font size="-1">(207KB)</font></td></tr><tr>
<td>RNAV (GPS) RWY 35L</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/00778R35L.PDF" target="ifrproc">download</a> <font size="-1">(220KB)</font></td></tr><tr>
<td>VOR RWY 35L</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/00778V35L.PDF" target="ifrproc">download</a> <font size="-1">(200KB)</font></td></tr><tr>
<td>HI-VOR OR TACAN RWY 17R</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/00778HVT17R.PDF" target="ifrproc">download</a> <font size="-1">(119KB)</font></td></tr>
<tr>
<td>NOTE: Special Alternate Minimums apply</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/SC1ALT.PDF" target="ifrproc">download</a></td></tr>
<tr>
<td>NOTE: Special Take-Off Minimums/Departure Procedures apply</td><td>  </td>
<td nowrap=""><a href="/depart?http://aeronav.faa.gov/d-tpp/2014/SC1TO.PDF" target="ifrproc">download</a> <font size="-1">(250KB)</font></td></tr>
</table>
<p>Other nearby airports with instrument procedures:</p>
<a href="/airport/KELK">KELK</a> - Elk City Regional Business Airport (11 nm NW)<br/>
<a href="/airport/KCLK">KCLK</a> - Clinton Regional Airport (18 nm NE)<br/>
<a href="/airport/KHBR">KHBR</a> - Hobart Regional Airport (22 nm S)<br/>
<a href="/airport/KOJA">KOJA</a> - Thomas P Stafford Airport (29 nm NE)<br/>
<a href="/airport/2K4">2K4</a> - Scott Field Airport (31 nm SW)<br/>
</td>
<td> </td>
<td width="240">
<table border="0" cellpadding="0" cellspacing="0" width="240"><tr><td align="center">
<font size="-2">
<a href="#loc">Loc</a> | <a href="#ops">Ops</a> | <a href="#rwys">Rwys</a> | <a href="#ifr">IFR</a> | <a href="#biz">FBO</a> | <a href="#links">Links</a><br/>
<a href="#com">Com</a> | <a href="#nav">Nav</a> | <a href="#svcs">Svcs</a> | <a href="#stats">Stats</a> | <a href="#notes">Notes</a>
</font>
</td></tr>
</table>
<div align="center">
<br/><table border="0" cellpadding="0" cellspacing="0" wdth="240"><tr><td align="right"><table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" height="360" width="240"><tr><td align="center" height="100%" valign="middle"><font size="-1"><style type="text/css">
<!--
#aptad1763 { font-family: "Lucida Grande", sans-serif; font-size: 17px; font-weight: normal; color: #ffffff; position: relative; width: 100%; letter-spacing: -0.05em /* for IE 6 */ }
#aptad1763 #price1 { position: absolute; top: 120px; left: 165px; }
#aptad1763 #priceA { position: absolute; top: 65px; left: 165px; }
#aptad1763 a:link  { text-decoration: none; color: #ffffff; }
#aptad1763 a:visited { text-decoration: none; color: #ffffff; }

-->
</style>
<div id="aptad1763"><a href="/adclick?1CZ"><img alt="" border="0" height="360" src="//img.airnav.com/sp/1763.jpg?v=PR7R1E" width="240"/>
<span id="priceA">$3.50</span>
<span id="price1">$3.50</span>
</a></div>
</font></td></tr></table>
</td></tr><tr><td><a href="/airboss/"><img alt="AirBoss" border="0" height="60" src="//img.airnav.com/sp/airboss/1.gif?v=L8HQN7" width="240"/></a>
</td></tr></table><br/>
</div>
 <br/>
<img alt="Area around KCSM (Clinton-Sherman Airport)" border="0" height="200" src="//img.airnav.com/sm/01964?v=JPOX41" width="240"/><br/> <br/>
<font size="-1">
Road maps at:
<a href="http://www.mapquest.com/maps/map.adp?latlongtype=decimal&amp;zoom=6&amp;latitude=35.339839&amp;longitude=-99.200499&amp;name=KCSM" target="airport_maps">MapQuest</a>
<a href="http://www.bing.com/maps/?sp=aN.35.339839_-99.200499_KCSM&amp;lvl=14" target="airport_maps">Bing</a>
<a href="http://maps.google.com/maps?ll=35.339839%2C-99.200499&amp;spn=0.0521,0.0521" target="airport_maps">Google</a>
</font>
<br/> <br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Aerial photo</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<font size="-2">
<b>WARNING:</b> Photo may not be current or correct<br/>
<img alt="Aerial photo of KCSM (Clinton-Sherman Airport)" border="0" height="180" src="//img.airnav.com/ap/01964.jpg?v=HTW7HW" width="240"/>
<hr/>
Do you have a better or more recent aerial photo of Clinton-Sherman Airport that you would like to share?  If so, please <b><a href="/airports/submitphoto.html?id=KCSM">send us your photo</a></b>.<br/>
<br/> <br/>
</font></td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Sectional chart</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<a href="http://www.airnav.com/depart?http://www.vfrmap.com/?type=vfrc&amp;lat=35.340&amp;lon=-99.200&amp;zoom=10&amp;api_key=763xxE1MJHyhr48DlAP2qQ" target="vfrmap"><img src="http://vfrmap.com/api?req=map&amp;type=sectc&amp;lat=35.339839&amp;lon=-99.200499&amp;zoom=10&amp;width=240&amp;height=240&amp;api_key=763xxE1MJHyhr48DlAP2qQ"/></a>
<br/> <br/>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Airport diagram</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
</td></tr>
</table>
<img alt="Diagram of KCSM (Clinton-Sherman Airport)" border="0" height="368" src="//img.airnav.com/aptdiag/FAA/00778AD.gif?v=JPUQ6T" width="240"/>
<center><font size="-2"><a href="http://aeronav.faa.gov/d-tpp/2014/00778AD.PDF" target="ifrproc">Download PDF</a><br/>of official airport diagram from the FAA</font></center>
<br/> <br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Airport distance calculator</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<form action="/airport/KCSM" method="POST">
<font size="-1">Flying to Clinton-Sherman Airport?
Find the distance to fly.</font>
<p align="center">From <input maxlength="4" name="distance_from" size="5"/>
to KCSM<br/>
<input alt="Calculate Distance" border="0" id="calculatedistance" name="calculatedistance" src="//img.airnav.com/btn/calculate-distance.gif?v=HTW9TQ" type="image" value=""/></p>
</form>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Sunrise and sunset</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><i>Times for 12-Jan-2021</i></font></div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><th></th><th> </th><th><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Local<br/>(UTC-6)</font></th><th> </th><th><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Zulu<br/>(UTC)</font></th></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Morning civil twilight</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">07:19</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13:19</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Sunrise</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">07:47</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13:47</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Sunset</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">17:44</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">23:44</font></td></tr>
<tr><td><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Evening civil twilight</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">18:11</font></td><td></td><td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">00:11</font></td></tr>
</table>
</td></tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">Current date and time</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top"><th align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Zulu (UTC)  </font></th><td align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">13-Jan-2021 03:49:05</font></td></tr>
<tr valign="top"><th align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Local (UTC-6)  </font></th><td align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">12-Jan-2021 21:49:05</font></td></tr>
</table>
</td></tr>
</table>

    <br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">METAR</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellspacing="0">
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KCSM </b></font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130253Z 19011KT 10SM CLR 02/M01 A3018 RMK AO2 SLP232 T00221006 58004
</font></td></tr>
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b><a href="/airport/KELK">KELK</a> </b><br/>11nm NW </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130335Z AUTO 21009KT 10SM CLR 02/M01 A3015 RMK AO2 T00221010
</font></td></tr>
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b><a href="/airport/KCLK">KCLK</a> </b><br/>17nm NE </font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">130335Z AUTO 00/M01 A3018 RMK AO2 PNO TSNO
</font></td></tr>
</table>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">TAF</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellspacing="0">
<tr><td align="left" nowrap="" valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KCSM </b></font></td><td valign="top"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><font color="#ff0000">121720Z</font> 1218/1318 22009KT P6SM FEW250 
</font></td></tr>
</table>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr class="part_title"><th align="left" bgcolor="#ccccff">NOTAMs</th></tr>
<tr class="part_body"><td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td align="left" width="20"><a href="https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&amp;reportType=RAW&amp;formatType=DOMESTIC&amp;retrieveLocId=CSM&amp;actionType=notamRetrievalByICAOs" target="NOTAMS"><img alt="" border="0" height="16" src="//img.airnav.com/wing.gif?v=HTWTJ4" width="16"/></a></td>
<td align="left"><a class="wl" href="https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&amp;reportType=RAW&amp;formatType=DOMESTIC&amp;retrieveLocId=CSM&amp;actionType=notamRetrievalByICAOs" target="NOTAMS">Click for the latest <b>NOTAMs</b></a></td>
</tr>
</table>
<font size="-1">NOTAMs are issued by the DoD/FAA and will open in a separate window not controlled by AirNav.</font>
 <br/>
</td></tr>
</table>
<br/> 
  </td>
</tr>
</table>
 <br/>
<a name="biz"></a>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr bgcolor="#ffffff" valign="middle"><th align="left" bgcolor="#9999ff" colspan="12" width="100%"><h3>Would you like to see your business listed on this page?</h3></th></tr>
<tr><td> </td><td colspan="10"><font size="-1">If your business provides an interesting product or service to pilots, flight crews, aircraft, or users of the Clinton-Sherman Airport, you should consider listing it here.  To start the listing process, click on the button below</font><br/> <br/>
<a href="/listings/subscribe/KCSM"><img alt="Add a business" border="0" height="20" src="//img.airnav.com/btn/add-listing.gif?v=HTW9TQ" width="240"/></a>
</td></tr>
</table>
 <br/>
<a name="links"></a><h3>Other Pages about Clinton-Sherman Airport</h3>
<a href="/airport/KCSM/reportlinks"><img alt="Update a link" border="0" height="20" src="//img.airnav.com/btn/add-a-link.gif?v=HTW9TQ" width="104"/></a>
<br/> <br/>
<table bgcolor="#CCCCFF" border="0" cellpadding="0" cellspacing="5" width="100%">
<tr><td bgcolor="#333399" colspan="2"><img alt="" border="0" height="1" src="//img.airnav.com/1dot.gif?v=HTWTIM" width="1"/></td></tr>
<tr valign="bottom">
<td><font color="#404040" size="-2">
Copyright © AirNav, LLC. All rights reserved.
</font>
</td>
<td align="right"><font size="-2"><a href="/info/privacy.html">Privacy Policy</a> 
<a href="/info/contact.html">Contact</a></font></td>
</tr>
</table>
<script type="text/javascript">
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-467723-1']); 
 _gaq.push(['_trackPageview']);
 _gaq.push(['_trackEvent', 'Banner', 'airwisconsin2019-3', 'Impression']);
 _gaq.push(['_trackEvent', 'AirportAd', '1763', 'KCSM']);
 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
</script>
</body>
<!-- Copyright AirNav, LLC.  ALL RIGHTS RESERVED. -->
</html>
