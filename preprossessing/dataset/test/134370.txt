<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Aporte Familiar Permanente</title>
<!-- Metas Contenido -->
<meta content="General" name="rating"/>
<meta content="index,follow,all" name="robots"/>
<meta content="15 days" name="revisit-after"/>
<meta content="Aporte familiar Permanente, Bono marzo, Aporte Familiar, IPS, suf, suseso, Bono marzo, beneficiario, consulta, beneficio, gobierno, social, ingreso, dinero, Chile" name="keywords"/>
<meta content="Consulte si recibirÃ¡ el beneficio Aporte Familiar Permanente." name="description"/>
<meta content="Instituto de PrevisiÃ³n Social" name="author"/>
<!-- Metas Twitter -->
<meta content="Consulte si recibirÃ¡ el beneficio Aporte Familiar Permanente." name="twitter:description"/>
<meta content="@IPS" name="twitter:creator"/>
<meta content="summary" name="twitter:card"/>
<meta content="Aporte Familiar Permanente" name="twitter:title"/>
<meta content="img/afp-social.jpg" name="twitter:image"/>
<meta content="@AporteFamiliar" name="twitter:site"/>
<!--<meta http-equiv="X-UA-Compatible" content="IE=9" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
<!--metas-->
<!-- Metas Facebook -->
<meta content="article" property="og:type"/>
<meta content="Aporte Familiar Permanente" property="og:site_name"/>
<meta content="img/afp-social.jpg" property="og:image"/>
<meta content="Aporte Familiar Permanente" property="og:title"/>
<meta content="http://www.aportefamiliar.cl/" property="og:url"/>
<meta content="Consulte si recibirÃ¡ el beneficio Aporte Familiar Permanente." property="og:description"/>
<!--Favicon-->
<link href="img/favicon.ico" rel="shortcut icon"/>
<!--<meta http-equiv="X-UA-Compatible" content="IE=9" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
<!--metas-->
<!--aporte familiar permanente-->
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/all.css" rel="stylesheet"/>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]>
	    <link rel="stylesheet" href="css/ie.css" />
		<![endif]-->
<!--[if IE 9]> <link rel="stylesheet" href="css/ie9.css" /> <![endif]-->
<!--HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	      <script src="js/html5shiv.min.js"></script>
	      <script src="js/respond.min.js"></script>

            <link href="js/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
            <link href="img/respond.proxy.gif" id="respond-redirect" rel="respond-redirect" />
            <script src="js/respond.proxy.js"></script>

  		<![endif]-->
<!-- Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-58769206-1', 'auto');
    ga('send', 'pageview');
  </script>
</head>
<body>
<!-- CONSULTA FIJA -->
<div class="consulta-fija">
<a href="https://consulta.aportefamiliar.cl/afper-consulta/consultaBeneficio" target="_blank" title="Link a consulta beneficio">
<h2>Â¿Tengo el beneficio?</h2>
</a>
</div>
<!-- FIN CONSULTA FIJA -->
<div id="inclusive">
<!-- NAV Movil-->
<div class="container-fluid visible-xs contentNexusMenu">
<div class="row">
<div class="col-xs-4">
<div class="logo-chileatiende-nav">
<img alt="Logo ChileAtiende" src="img/logo-chileatiende-header.png" title="Logo ChileAtiende"/>
</div>
</div>
<div class="col-xs-8">
<div class="navbar-header head-movil">
<a class="call" href="tel:101" title="Call Center">
<i class="fas fa-phone-alt"></i>
</a>
<a class="navbar-toggle menu-afper-movil" data-target=".bs-example-js-navbar-collapse" data-toggle="collapse" title="MenÃº principal">
<i class="fas fa-bars"></i>
</a>
</div>
</div>
</div>
<div class="row">
<div class="navbar-collapse bs-example-js-navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="itemMenu1 current">
<a class="txtMenu" href="index.html">Inicio</a>
</li>
<li class="itemMenu2">
<a class="txtMenu" href="cuando-se-paga.html">Â¿CuÃ¡ndo se paga?</a>
</li>
<li class="itemMenu3">
<a class="txtMenu" href="para-quien-es.html">Â¿Para quiÃ©n es?</a>
</li>
<li class="itemMenu4">
<a class="txtMenu" href="preguntas-frecuentes.html">Preguntas frecuentes</a>
</li>
<li class="itemMenu5">
<a class="txtMenu" href="https://consulta.aportefamiliar.cl/afper-consulta/consultaBeneficio" target="_blank">AÃ±os anteriores</a>
</li>
</ul>
</div>
</div>
</div>
<!-- NAV Movil fin-->
<!-- CONTENEDOR HEADER -->
<div class="contenedor-header hidden-xs">
<!-- accesibilidad -->
<div class="menu-accesibilidad">
<div class="container">
<div class="collapse" id="accesibilidad">
<div class="clearfix"></div>
<div class="col-xs-3 titPref hidden-sm">
<div class="acce"><i class="fas fa-universal-access"></i></div>
<h4 class="menupref">MenÃº de <br/>
						preferencias</h4>
</div>
<div class="col-xs-2 col-md-1 modulo-accesibilidad">
<ul class="list-inline text-center">
<li> <a href="javascript:decreaseFont();" title="Disminuir tamaÃ±o de letra"><i class="fas fa-minus-square"></i>Disminuir</a></li>
</ul>
</div>
<div class="col-xs-2 col-md-1 modulo-accesibilidad">
<ul class="list-inline text-center">
<li> <a href="javascript:increaseFont();" title="Aumentar tamaÃ±o de letra"><i class="fas fa-plus-square"></i>Aumentar </a></li>
</ul>
</div>
<div class="col-xs-2 modulo-accesibilidad">
<ul class="list-inline text-center">
<li> <a class="btn-acce" data-bloqueado="0" href="#" title="Ver el sitio en escala de grises">
<div class="grises"> <i class="fas fa-adjust"></i> </div>
							Ver el sitio en grises </a>
<input class="filter-check" id="ver-en-grises" name="filter" style="display:none;" type="checkbox" value="checkbox"/>
<span class="fa fa-spinner" style="display: none;"></span></li>
</ul>
</div>
<div class="col-xs-2 modulo-accesibilidad">
<ul class="list-inline text-center">
<li> <a class="btn-acce" data-bloqueado="0" href="#" title="Subrayar enlaces del sitio">
<div class="subrayado"> <i class="fas fa-underline"></i> </div>
								Activar links Subrayado </a>
<input class="underline-check" id="activar-subrayado" name="subrayado" style="display:none;" type="checkbox" value="checkbox"/>
</li>
</ul>
</div>
<div class="col-xs-2 modulo-accesibilidad">
<ul class="list-inline text-center">
<li>
<a class="btn-acce" href="#" id="restaurar" title="Restaurar el sitio a su versiÃ³n original">
<div class="restaurar"><i class="fas fa-recycle"></i></div>
						VersiÃ³n original </a>
</li>
</ul>
</div>
<div class="col-xs-2 col-md-1 modulo-accesibilidad">
<ul class="list-inline text-center">
<li>
<a class="btn_cerrar cerrar" href="#" title="Cerrar menÃº de preferencias">
<div class="restaurar"><i class="fas fa-times"></i></div>
									Cerrar</a>
</li>
</ul>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
</div>
<!-- fin accesibilidad -->
<!-- menu accesibilidad -->
<div class="headAcc">
<div class="container">
<div class="row">
<div class="col-sm-offset-8 col-sm-4 col-md-offset-9 col-md-3">
<div class="CallInfo">
<ul class="nav navbar-nav">
<li class="dropdown btn-pref">
<button aria-expanded="false" class="btn btn-default access pull-right collapsed" data-target="#accesibilidad" data-toggle="collapse" type="button"> <i class="fas fa-universal-access"></i> <span class="hidden-xs"> A+ A- | MenÃº de preferencias</span> </button>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- FIN menu accesibilidad -->
<!-- NAV -->
<div class="head">
<div class="container">
<div class="row">
<div class="col-sm-3 col-md-4 nopadding">
<div class="logo-ips-nav">
<a href="index.html" title="Volver al inicio">
<img alt="Logo IPS" src="img/logo-ips-header.png" title="Logo IPS"/>
</a>
</div>
<div class="logo-chileatiende-nav">
<a href="http://chileatiende.cl/" target="_blank" title="Chile Atiende">
<img alt="Logo ChileAtiende" src="img/logo-chileatiende-header.png" title="Logo ChileAtiende"/>
</a>
</div>
</div>
<div class="col-sm-9 col-md-8 nopadding">
<div class="menu-desktop navbar-header">
<ul class="nav navbar-nav">
<li class="itemMenu1 current"><a class="txtMenu" href="index.html">Inicio</a></li>
<li class="itemMenu2"><a class="txtMenu" href="cuando-se-paga.html">Â¿CuÃ¡ndo se paga?</a> </li>
<li class="itemMenu3"><a class="txtMenu" href="para-quien-es.html">Â¿Para quiÃ©n es?</a> </li>
<li class="itemMenu4"><a class="txtMenu" href="preguntas-frecuentes.html">Preguntas frecuentes</a> </li>
<li class="itemMenu5"><a class="txtMenu" href="https://consulta.aportefamiliar.cl/afper-consulta/consultaBeneficio" target="_blank">AÃ±os anteriores</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- FIN NAV -->
</div>
<!-- CONTENEDOR HEADER -->
<div class="clearfix"></div>
<!-- BANNER -->
<div class="row banner banner-principal">
<div class="col-md-6 bg-familia nopadding hidden-sm hidden-xs"></div>
<div class="col-xs-12 col-md-6 bg-color">
<div class="caja-texto-bg-color">
<h1><span>Â¿Desea saber si tiene derecho al</span><br/>APORTE FAMILIAR PERMANENTE 2020?</h1>
<form action="https://consulta.aportefamiliar.cl/afper-consulta/consultaBeneficio" method="get" target="_blank">
<button class="boton-rojo" title="Consultar Aporte Familiar 2019" type="submit">Consultar</button>
</form>
</div>
</div>
</div>
<!-- FIN BANNER -->
<!-- MENSAJES BAJO HEADER -->
<div class="mensajes-ips">
<div class="container">
<div class="row">
<div class="col-sm-4">
<h2>GRUPO 1</h2>
<p>Personas que la segunda mitad de cada mes cobran beneficios por Subsidio Familiar, Chile Solidario o por el Subsistema de Seguridades y Oportunidades (Ingreso Ãtico Familiar), a travÃ©s del IPS.</p>
<p>Este grupo recibirÃ¡ el Aporte Familiar desde el 15 al 28 de febrero, en su mismo lugar y fecha de pago habitual, sin necesidad de hacer ningÃºn trÃ¡mite.</p>
</div>
<div class="col-sm-4">
<h2>GRUPO 2</h2>
<p>Personas que la primera mitad de cada mes cobran beneficios por Subsidio Familiar, Chile Solidario o por el Subsistema de Seguridades y Oportunidades, a travÃ©s del IPS.</p>
<p>A los pensionados del Instituto de PrevisiÃ³n Social (IPS) que cobran AsignaciÃ³n por sus cargas familiares, tambiÃ©n se les habilitarÃ¡ el pago en esta etapa.</p>
<p>Este grupo recibirÃ¡ su beneficio entre el 2 y 14 de marzo, en su mismo lugar y fecha de pago habitual, sin necesidad de hacer ningÃºn trÃ¡mite.</p>
</div>
<div class="col-sm-4">
<h2>GRUPO 3</h2>
<p>Trabajadores, trabajadoras y personas pensionadas de entidades distintas al IPS que cobran AsignaciÃ³n Familiar o Maternal por sus cargas familiares. Estas personas podrÃ¡n consultar su dÃ­a y lugar de pago en www.aportefamiliar.cl, a partir del 16 de marzo, ingresando su RUN y fecha de nacimiento.</p>
</div>
</div>
</div>
</div>
<!-- FIN MENSAJE BAJO HEADER -->
<!-- CONTENIDO -->
<div class="container cuerpo-home">
<div class="row">
<div class="col-sm-6">
<img alt="Imagen Familia Chilena" src="img/familia-aporte-index.jpg"/>
</div>
<div class="col-sm-6 texto-derecha">
<h3>Aporte Familiar Permanente<br/>(ex Bono marzo)</h3>
<p>Se paga una vez al aÃ±o a las familias que, al 31 de diciembre anterior, sean beneficiarias de Subsidio Familiar, Chile Solidario, el Subsistema de Seguridades y Oportunidades (Ingreso Ãtico Familiar), AsignaciÃ³n Familiar o AsignaciÃ³n Maternal.</p>
</div>
</div>
<hr/>
<div class="row">
<div class="col-sm-6">
<h3>Â¿Desde cuÃ¡ndo podrÃ© consultar con mi RUN?</h3>
<p>Los beneficiarios del Aporte Familiar Permanente pueden consultar a contar del 15 de febrero, 2 de marzo y 16 de marzo, dependiendo en quÃ© grupo se encuentre cada persona.</p>
</div>
<div class="col-sm-6">
<img alt="Imagen Familia Chilena" src="img/familia-consulta-index.jpg"/>
</div>
</div>
</div>
<!-- FIN CONTENIDO -->
<!-- BENEFICIOS IPS -->
<div class="beneficios">
<div class="container">
<div class="row">
<div class="col-sm-4"><img alt="Logo ChileAtiende" src="img/chileatiende-beneficios.png" title="Logo ChileAtiende"/>
</div>
<div class="col-sm-2"></div>
<div class="col-sm-6 mt-3">
<h3>Conozca otros beneficios en ChileAtiende.</h3>
<form action="https://www.chileatiende.gob.cl/" method="get" target="_blank">
<button class="boton-beneficios" title="Ver beneficios de ChileAtiende" type="submit">
							Ver mÃ¡s <span>beneficios</span>
</button>
</form>
</div>
</div>
</div>
</div>
<!-- FIN BENEFICIOS IPS -->
<!-- footer -->
<div class="footer">
<div class="container">
<div class="row mb-5">
<div class="col-lg-8 col-md-8 col-xs-12">
<div class="row enlaces-footer">
<div class="col-xs-12 col-sm-4"><a href="https://consulta.aportefamiliar.cl/afper-consulta/consultaBeneficio" target="_blank" title="Consulte aÃ±os anteriores">Consulte aÃ±os anteriores</a></div>
<div class="col-xs-12 col-sm-4"><a href="politicas-de-privacidad.html" title="PolÃ­tica de privacidad">PolÃ­tica de privacidad</a></div>
<div class="col-xs-12 col-sm-3"><a href="visualizadores.html" title="Visualizadores">Visualizadores</a></div>
<div class="col-xs-12 col-sm-1"><a href="https://creativecommons.org/licenses/by/3.0/cl/" target="_blank" title="Creative Commons">CC</a></div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="redes-footer">
<div class="col-xs-3 col-md-3"><a href="tel:101"><i class="fas fa-phone-alt"></i></a></div>
<div class="col-xs-3 col-md-3"><a href="https://twitter.com/ChileAtiende" target="_blank"><i class="fab fa-twitter"></i></a></div>
<div class="col-xs-3 col-md-3"><a href="https://www.facebook.com/ChileAtiende" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
<div class="col-xs-3 col-md-3" id="back-top"><a href="#top"><i class="fas fa-angle-up"></i></a></div>
</div>
</div>
</div>
<div class="col-sm-2 iso-footer iconAzul"></div>
<div class="col-sm-2 iso-footer iconRojo"></div>
</div>
</div>
<!--  FIN footer -->
</div>
<!-- FIN ACCESIBILIDAD -->
<!-- JS -->
<script src="js/scripts.base.js"></script>
<script src="js/scripts-new.js"></script>
<script src="js/accessibility.js"></script>
<script src="js/funciones.js"></script>
<script src="js/funciones-accesibilidad.js"></script>
<!--[if !IEMobile]>
<script src="js/jsiemobile.js"></script>
<![endif]-->
</body>
</html>