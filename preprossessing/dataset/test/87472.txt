<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>
<html class="lt-ie9"> <![endif]--><!--[if gt IE 8]><!--><html class="">
<!--<![endif]-->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=edge,Chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport"/>
<meta content="webkit" name="renderer"/>
<title>首页 - 爱E族</title>
<meta content="爱E族,技术问答,计算博客,php,mysql,linux" name="keywords"/>
<meta content="爱E族计算机网络技术社区" name="description"/>
<link href="/assets/img/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/assets/css/bootstrap.min.css" media="screen" rel="stylesheet"/>
<link href="/assets/libs/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"/>
<link href="/assets/libs/fastadmin-layer/dist/theme/default/layer.css" media="screen" rel="stylesheet"/>
<link href="/assets/addons/cms/css/swiper.min.css" media="screen" rel="stylesheet"/>
<link href="/assets/addons/cms/css/common.css?v=1.0.1" media="screen" rel="stylesheet"/>
<link href="//at.alicdn.com/t/font_1104524_z1zcv22ej09.css" rel="stylesheet"/>
<!--[if lt IE 9]>
    <script src="/libs/html5shiv.js"></script>
    <script src="/libs/respond.min.js"></script>
    <![endif]-->
</head>
<body class="group-page">
<header class="header">
<!-- S 导航 -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/"><img alt="" height="100%" src="/assets/addons/cms/img/logo.png"/></a>
</div>
<div class="collapse navbar-collapse" id="navbar-collapse">
<ul class="nav navbar-nav">
<!--如果你需要自定义NAV,可使用channellist标签来完成,这里只设置了2级,如果显示无限级,请使用cms:nav标签-->
<!--判断是否有子级或高亮当前栏目-->
<li class="dropdown">
<a data-toggle="dropdown" href="/c/article.html">文章 <b class="caret"></b></a>
<ul class="dropdown-menu" role="menu">
<li><a href="/c/program.html">编程开发</a></li>
<li><a href="/c/os.html">操作系统</a></li>
<li><a href="/c/db.html">数据库</a></li>
<li><a href="/c/ops.html">IT运维</a></li>
</ul>
</li>
<!--判断是否有子级或高亮当前栏目-->
<li class="dropdown">
<a data-toggle="dropdown" href="/c/baiduyun.html">百度云 <b class="caret"></b></a>
<ul class="dropdown-menu" role="menu">
<li><a href="/c/applicationsoftware.html">应用软件</a></li>
<li><a href="/c/systemsoftware.html">系统软件</a></li>
</ul>
</li>
</ul>
<ul class="nav navbar-right hidden">
<ul class="nav navbar-nav">
<li><a class="addbookbark" href="javascript:;"><i class="fa fa-star"></i> 加入收藏</a></li>
<li><a class="" href="javascript:;"><i class="fa fa-phone"></i> 联系我们</a></li>
</ul>
</ul>
<ul class="nav navbar-nav navbar-right">
<li>
<form action="/s.html" class="form-inline navbar-form" method="get">
<div class="form-search hidden-sm hidden-md">
<input class="form-control" data-suggestion-url="/addons/cms/search/suggestion.html" id="searchinput" name="q" placeholder="搜索" type="text" value=""/>
</div>
</form>
</li>
<!--
						<li class="dropdown">
														<a href="/index/user/index" class="dropdown-toggle" data-toggle="dropdown">会员<span class="hidden-sm">中心</span> <b class="caret"></b></a>
														<ul class="dropdown-menu">
																<li><a href="/index/user/login"><i class="fa fa-sign-in fa-fw"></i>登录</a></li>
								<li><a href="/index/user/register"><i class="fa fa-user-o fa-fw"></i>注册</a></li>
															</ul>
						</li>
						-->
</ul>
</div>
</div>
</nav>
<!-- E 导航 -->
</header>
<div class="container" id="content-container">
<!--<div style="margin-bottom:20px;">-->
<!-- -->
<!--</div>-->
<div class="row">
<main class="col-md-8">
<div class="swiper-container index-focus">
<!-- S 焦点图 -->
<div class="carousel slide carousel-focus" data-ride="carousel" id="index-focus">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#index-focus"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="item active">
<a href="/">
<div class="carousel-img" style="background-image:url('/assets/addons/cms/img/noimage.jpg');"></div>
<div class="carousel-caption hidden-xs">
<h3>首页焦点图标题1</h3>
</div>
</a>
</div>
</div>
<a class="left carousel-control" data-slide="prev" href="#index-focus" role="button">
<span aria-hidden="true" class="icon-prev fa fa-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" data-slide="next" href="#index-focus" role="button">
<span aria-hidden="true" class="icon-next fa fa-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>
<!-- E 焦点图 -->
</div>
<div class="panel panel-default index-gallary">
<div class="panel-heading">
<h3 class="panel-title">
<span>热门图集</span>
<div class="more">
<a href="/c/product.html">查看更多</a>
</div>
</h3>
</div>
<div class="panel-body">
<div class="related-article">
<div class="row">
<!-- S 热门图集 -->
<!-- E 热门图集 -->
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">
<span>最近更新</span>
<div class="more hidden-xs">
<ul class="list-unstyled list-inline">
<!-- E 栏目筛选 -->
<li><a href="/c/program.html">编程开发</a></li>
<li><a href="/c/os.html">操作系统</a></li>
<li><a href="/c/db.html">数据库</a></li>
<li><a href="/c/ops.html">IT运维</a></li>
<li><a href="/c/syssoft.html">系统软件</a></li>
<li><a href="/c/appsoft.html">应用软件</a></li>
<!-- E 栏目筛选 -->
</ul>
</div>
</h3>
</div>
<div class="panel-body p-0">
<div class="article-list">
<!-- S 首页列表 -->
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/ubuntu_20.04_beta_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/600edc1dc73f8e1002f5d7c9b2e31065.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/ubuntu_20.04_beta_iso_baiduwangpan_download.html">Ubuntu 20.04 beta版 ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                Ubuntu-20.04-beta			   	 2020-04-22 13:2046.82KB		ubuntu-20.04-beta-live-server-amd64.metalink			 	 2020-04-22 13:2014.65KB		ubuntu-20.04-beta-live-server-amd64.manifest			 	 2020-04-2            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 11 点赞</span>
<span itemprop="comments"><a href="/a/ubuntu_20.04_beta_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 4644 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/centos_8.1_1911_iso_baiduyun_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/60816d8f1a1b532f849a0b0d271e9ded.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/centos_8.1_1911_iso_baiduyun_download.html">CentOS 8.1.1911全部ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                CentOS-8.1-1911			   	   		x86_64			   	 2020-04-21 23:321.15KB		CHECKSUM.asc			 	 2020-04-21 23:34282KB		CentOS-8.1.1911-x86_64-dvd1.torrent			 	 2020-04-21 23:33408.14KB		C            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 14 点赞</span>
<span itemprop="comments"><a href="/a/centos_8.1_1911_iso_baiduyun_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 2495 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/centos_8.0_1905_iso_baiduyun_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/60816d8f1a1b532f849a0b0d271e9ded.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/centos_8.0_1905_iso_baiduyun_download.html">CentOS 8.0.1905全部ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                CentOS-8.0-1905			   	   		x86_64			   	 2020-04-21 23:031.14KB		CHECKSUM.asc			 	 2020-04-21 23:05266.37KB		CentOS-8-x86_64-1905-dvd1.torrent			 	 2020-04-21 23:04401.29KB		            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月21日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 15 点赞</span>
<span itemprop="comments"><a href="/a/centos_8.0_1905_iso_baiduyun_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 2868 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/CentOS_7.7_1908_iso_baiduyun_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/60816d8f1a1b532f849a0b0d271e9ded.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/CentOS_7.7_1908_iso_baiduyun_download.html">CentOS 7.7 1908安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                CentOS-7.7-1908  2020-04-21 22:261.42KB		sha256sum.txt.asc			 	 2020-04-21 22:26598B		sha256sum.txt			 	 2020-04-21 22:2722.08KB		CentOS-7-x86_64-NetInstall-1908.torrent			 	 202            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月21日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 19 点赞</span>
<span itemprop="comments"><a href="/a/CentOS_7.7_1908_iso_baiduyun_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 2579 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/php_abs_length_byteslength.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/60816d8f1a1b532f849a0b0d271e9ded.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/php_abs_length_byteslength.html">PHP计算字符串字宽长度计算</a>
</h3>
<div class="article-intro hidden-xs">
                PHP计算字符串宽度，英文字符、数字等ASCII字符记长度为1，汉字等多字节字符长度计2            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/program.html">编程开发</a>
<span itemprop="date">2013年09月26日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 1 点赞</span>
<span itemprop="comments"><a href="/a/php_abs_length_byteslength.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 1351 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/centos_7.6_1810_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/60816d8f1a1b532f849a0b0d271e9ded.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/centos_7.6_1810_iso_baiduwangpan_download.html">CentOS 7.6 (1810)  安装ISO镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                CentOS-7.6-x86_64			   	 2019-07-06 10:28918MB		CentOS-7.6-x86_64-Minimal-1810.iso			 	 2019-07-05 22:2010.01GB		CentOS-7.6-x86_64-Everything-1810.iso			 	 2019-07-06 10:344.27GB		Cent            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年05月29日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 50 点赞</span>
<span itemprop="comments"><a href="/a/centos_7.6_1810_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 6094 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/ubuntu_12.04.5_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/600edc1dc73f8e1002f5d7c9b2e31065.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/ubuntu_12.04.5_iso_baiduwangpan_download.html">Ubuntu 12.04.5 ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                Ubuntu-12.04.5			   	   		i386			   	 2016-11-03 23:0026.5KB		ubuntu-12.04.5-server-i386.iso.torrent			 	 2016-11-03 22:59670MB		ubuntu-12.04.5-server-i386.iso			 	 2016-11-0            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 5 点赞</span>
<span itemprop="comments"><a href="/a/ubuntu_12.04.5_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 941 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/ubuntu_14.04.4_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/600edc1dc73f8e1002f5d7c9b2e31065.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/ubuntu_14.04.4_iso_baiduwangpan_download.html">Ubuntu 14.04.4 ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                Ubuntu-14.04.4			   	   		i386			   	 2016-11-03 22:5522.05KB		ubuntu-14.04.4-server-i386.iso.torrent			 	 2016-11-03 22:51556MB		ubuntu-14.04.4-server-i386.iso			 	 2016-11-            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 0 点赞</span>
<span itemprop="comments"><a href="/a/ubuntu_14.04.4_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 929 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/ubuntu_14.04.5_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/600edc1dc73f8e1002f5d7c9b2e31065.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/ubuntu_14.04.5_iso_baiduwangpan_download.html">Ubuntu 14.04.5 ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                Ubuntu-14.04.5			   	   		i386			   	 2016-11-03 22:5423.57KB		ubuntu-14.04.5-server-i386.iso.torrent			 	 2016-11-03 22:53595MB		ubuntu-14.04.5-server-i386.iso			 	 2016-11-            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 7 点赞</span>
<span itemprop="comments"><a href="/a/ubuntu_14.04.5_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 1428 浏览</span>
</div>
</div>
</div>
</article>
<article class="article-item">
<div class="media">
<div class="media-left">
<a href="/a/ubuntu_14.04.6_iso_baiduwangpan_download.html">
<div class="embed-responsive embed-responsive-4by3 img-zoom">
<img src="https://www.aiezu.com/uploads/20200422/600edc1dc73f8e1002f5d7c9b2e31065.jpg"/>
</div>
</a>
</div>
<div class="media-body">
<h3 class="article-title">
<a href="/a/ubuntu_14.04.6_iso_baiduwangpan_download.html">Ubuntu 14.04.6 ISO安装镜像百度云下载</a>
</h3>
<div class="article-intro hidden-xs">
                Ubuntu-14.04.6			   	   		i386			   	 2020-04-22 17:0763.8MB		ubuntu-14.04.6-server-i386.template			 	 2020-04-22 17:0647.05KB		ubuntu-14.04.6-server-i386.metalink			 	 2020-            </div>
<div class="article-tag">
<a class="tag tag-primary" href="/c/systemsoftware.html">系统软件</a>
<span itemprop="date">2020年04月22日</span>
<span itemprop="likes" title="点赞次数"><i class="fa fa-thumbs-up"></i> 4 点赞</span>
<span itemprop="comments"><a href="/a/ubuntu_14.04.6_iso_baiduwangpan_download.html#comments" target="_blank" title="评论数"><i class="fa fa-comments"></i> 0</a> 评论</span>
<span itemprop="views" title="浏览次数"><i class="fa fa-eye"></i> 899 浏览</span>
</div>
</div>
</div>
</article>
<!-- E 首页列表 -->
<div class="text-center">
<a class="btn btn-default my-4 px-4 btn-loadmore" data-page="1" href="?page=2">加载更多</a>
</div>
</div>
</div>
</div>
</main>
<aside class="col-xs-12 col-sm-4">
<div class="panel panel-default lasest-update">
<!-- S 最近更新 -->
<div class="panel-heading">
<h3 class="panel-title">最新更新</h3>
</div>
<div class="panel-body">
<ul class="list-unstyled">
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/centos_7.6_1810_iso_baiduwangpan_download.html" title="CentOS 7.6 (1810)  安装ISO镜像百度云下载">CentOS 7.6 (1810)  安装ISO镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_12.04.5_iso_baiduwangpan_download.html" title="Ubuntu 12.04.5 ISO安装镜像百度云下载">Ubuntu 12.04.5 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_14.04.4_iso_baiduwangpan_download.html" title="Ubuntu 14.04.4 ISO安装镜像百度云下载">Ubuntu 14.04.4 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_14.04.5_iso_baiduwangpan_download.html" title="Ubuntu 14.04.5 ISO安装镜像百度云下载">Ubuntu 14.04.5 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_14.04.6_iso_baiduwangpan_download.html" title="Ubuntu 14.04.6 ISO安装镜像百度云下载">Ubuntu 14.04.6 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_15.04_iso_baiduwangpan_download.html" title="Ubuntu 15.04 ISO安装镜像百度云下载">Ubuntu 15.04 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_16.04.1_iso_baiduwangpan_download.html" title="Ubuntu 16.04.1 ISO安装镜像百度云下载">Ubuntu 16.04.1 ISO安装镜像百度云下载</a>
</li>
<li>
<span>[<a href="/c/systemsoftware.html">系统软件</a>]</span>
<a class="link-dark" href="/a/ubuntu_16.04.6_iso_baiduwangpan_download.html" title="Ubuntu 16.04.6 ISO安装镜像百度云下载">Ubuntu 16.04.6 ISO安装镜像百度云下载</a>
</li>
</ul>
</div>
<!-- E 最近更新 -->
</div>
<div class="panel panel-blockimg">
</div>
<div class="panel panel-blockimg">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 自适应广告 -->
<ins class="adsbygoogle" data-ad-client="ca-pub-3915656087500744" data-ad-format="auto" data-ad-slot="6674331477" data-full-width-responsive="true" style="display:block"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<!-- S 热门资讯 -->
<div class="panel panel-default hot-article">
<div class="panel-heading">
<h3 class="panel-title">推荐文章</h3>
</div>
<div class="panel-body">
<div class="media media-number">
<div class="media-left">
<span class="num">1</span>
</div>
<div class="media-body">
<a class="link-dark" href="/a/php_abs_length_byteslength.html" title="PHP计算字符串字宽长度计算">PHP计算字符串字宽长度计算</a>
</div>
</div>
</div>
</div>
<!-- E 热门资讯 -->
<div class="panel panel-blockimg">
<a href="https://www.fastadmin.net/go/aliyun" rel="nofollow" target="_blank" title="FastAdmin推荐企业服务器">
<img alt="" class="img-responsive" src="https://cdn.fastadmin.net/uploads/store/aliyun-sidebar.png"/>
</a>
</div>
<!-- S 热门标签 -->
<div class="panel panel-default hot-tags">
<div class="panel-heading">
<h3 class="panel-title">热门标签</h3>
</div>
<div class="panel-body">
<div class="tags">
<a class="tag" href="/t/rhe.html"> <span>rhe</span></a>
<a class="tag" href="/t/rhel.html"> <span>rhel</span></a>
<a class="tag" href="/t/js.html"> <span>js</span></a>
<a class="tag" href="/t/centos.html"> <span>centos</span></a>
<a class="tag" href="/t/eval.html"> <span>eval</span></a>
<a class="tag" href="/t/bash.html"> <span>bash</span></a>
<a class="tag" href="/t/strace.html"> <span>strace</span></a>
<a class="tag" href="/t/unix.html"> <span>unix</span></a>
<a class="tag" href="/t/uname.html"> <span>uname</span></a>
<a class="tag" href="/t/strlen.html"> <span>strlen</span></a>
<a class="tag" href="/t/iso.html"> <span>iso</span></a>
<a class="tag" href="/t/apache.html"> <span>apache</span></a>
<a class="tag" href="/t/oracle.html"> <span>oracle</span></a>
<a class="tag" href="/t/nginx.html"> <span>nginx</span></a>
<a class="tag" href="/t/lamp.html"> <span>lamp</span></a>
<a class="tag" href="/t/whoami.html"> <span>whoami</span></a>
<a class="tag" href="/t/mysql.html"> <span>mysql</span></a>
<a class="tag" href="/t/vnc.html"> <span>vnc</span></a>
<a class="tag" href="/t/wifi.html"> <span>wifi</span></a>
<a class="tag" href="/t/error.html"> <span>error</span></a>
<a class="tag" href="/t/ajax.html"> <span>ajax</span></a>
<a class="tag" href="/t/useragent.html"> <span>useragent</span></a>
<a class="tag" href="/t/iconv.html"> <span>iconv</span></a>
<a class="tag" href="/t/linux.html"> <span>linux</span></a>
<a class="tag" href="/t/curl.html"> <span>curl</span></a>
<a class="tag" href="/t/中国电信.html"> <span>中国电信</span></a>
<a class="tag" href="/t/dwg.html"> <span>dwg</span></a>
<a class="tag" href="/t/http.html"> <span>http</span></a>
<a class="tag" href="/t/ubuntu.html"> <span>ubuntu</span></a>
<a class="tag" href="/t/postgresql.html"> <span>postgresql</span></a>
</div>
</div>
</div>
<!-- E 热门标签 -->
<!-- S 推荐下载 -->
<div class="panel panel-default recommend-article">
<div class="panel-heading">
<h3 class="panel-title">推荐下载</h3>
</div>
<div class="panel-body">
<div class="media media-number">
<div class="media-left">
<span class="num">1</span>
</div>
<div class="media-body">
<a href="/a/rhel_7.5_iso_baiduwangpan_download.html" title="Rhel 7.5 ISO安装镜像合集百度云下载">Rhel 7.5 ISO安装镜像合集百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">2</span>
</div>
<div class="media-body">
<a href="/a/CentOS_7.7_1908_iso_baiduyun_download.html" title="CentOS 7.7 1908安装镜像百度云下载">CentOS 7.7 1908安装镜像百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">3</span>
</div>
<div class="media-body">
<a href="/a/centos_8.0_1905_iso_baiduyun_download.html" title="CentOS 8.0.1905全部ISO安装镜像百度云下载">CentOS 8.0.1905全部ISO安装镜像百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">4</span>
</div>
<div class="media-body">
<a href="/a/centos_8_stream_iso_baiduyun_download.html" title="CentOS 8 stream全部ISO安装镜像百度云下载">CentOS 8 stream全部ISO安装镜像百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">5</span>
</div>
<div class="media-body">
<a href="/a/centos_8.1_1911_iso_baiduyun_download.html" title="CentOS 8.1.1911全部ISO安装镜像百度云下载">CentOS 8.1.1911全部ISO安装镜像百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">6</span>
</div>
<div class="media-body">
<a href="/a/ubuntu_20.04_beta_iso_baiduwangpan_download.html" title="Ubuntu 20.04 beta版 ISO安装镜像百度云下载">Ubuntu 20.04 beta版 ISO安装镜像百度云下载</a>
</div>
</div>
<div class="media media-number">
<div class="media-left">
<span class="num">7</span>
</div>
<div class="media-body">
<a href="/a/ubuntu_19.10_iso_baiduwangpan_download.html" title="Ubuntu 19.10 ISO安装镜像百度云下载">Ubuntu 19.10 ISO安装镜像百度云下载</a>
</div>
</div>
</div>
</div>
<!-- E 推荐下载 -->
<div class="panel panel-blockimg">
<a href="http://www.fastadmin.net/go/aliyun"><img class="img-responsive" src="https://cdn.fastadmin.net/uploads/store/enterprisehost.png"/></a>
</div>
</aside>
</div>
</div>
<div class="container hidden-xs j-partner">
<div class="panel panel-default">
<!-- S 合作伙伴 -->
<div class="panel-heading">
<h3 class="panel-title">
                合作伙伴
                <small>感谢以下的合作伙伴的大力支持</small>
<a class="more" href="https://wpa.qq.com/msgrd?v=3&amp;uin=465272&amp;site=&amp;menu=yes" target="_blank">联系我们</a>
</h3>
</div>
<div class="panel-body">
<ul class="list-unstyled list-partner">
<li><a href="/"><img src="/assets/addons/cms/img/logo/58.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/360.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/alipay.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/baidu.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/boc.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/cctv.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/didi.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/iqiyi.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/qq.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/suning.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/taobao.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/tuniu.png"/></a></li><li><a href="/"><img src="/assets/addons/cms/img/logo/weibo.png"/></a></li>
</ul>
</div>
<!-- E 合作伙伴 -->
<!-- S 友情链接 -->
<div class="panel-heading">
<h3 class="panel-title">友情链接
                <small>申请友情链接请务必先做好本站链接</small>
<a class="more" href="https://wpa.qq.com/msgrd?v=3&amp;uin=465272&amp;site=&amp;menu=yes" target="_blank">申请友链</a></h3>
</div>
<div class="panel-body">
<div class="list-unstyled list-links">
<a href="https://www.fastadmin.net" title="FastAdmin - 极速后台开发框架">FastAdmin</a> <a href="https://gitee.com" title="FastAdmin码云仓库">码云</a> <a href="https://github.com" title="FastAdminGithub仓库">Github</a> <a href="https://doc.fastadmin.net" title="FastAdmin文档 - 极速后台开发框架">FastAdmin文档</a> <a href="https://ask.fastadmin.net" title="FastAdmin问答社区 - 极速后台开发框架">FastAdmin问答社区</a>
</div>
</div>
<!-- E 友情链接 -->
</div>
</div>
<footer>
<div class="container-fluid" id="footer">
<div class="container">
<div class="row footer-inner">
<div class="col-md-3 col-sm-3"><div class="footer-logo"><a href="javascript:void(0);"><i class="fa fa-bookmark"></i></a> </div><p class="copyright"><small>© 2020 爱E族 All Rights Reserved.</small> <br/><small>www.aiezu.com</small></p><p class="copyright"><a href="https://www.miit.gov.cn/" target="_blank"> <small>湘ICP备15005284号-1</small></a></p><p class="copyright"><a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=43010302000147" target="_blank"><img src="/assets/img/ghs.png" style="width:18px;vertical-align:text-bottom;margin-right:3px;"/>湘公网安备 43010302000147号</a> </p></div><div class="col-md-5 col-md-push-1 col-sm-5 col-sm-push-1"><div class="row"><div class="col-xs-4"><ul class="links"><li><a href="javascript:void(0);">关于我们</a></li><li><a href="javascript:void(0);">发展历程</a></li><li><a href="javascript:void(0);">服务项目</a></li><li><a href="javascript:void(0);">团队成员</a></li></ul></div><div class="col-xs-4"><ul class="links"><li><a href="javascript:void(0);">新闻</a></li><li><a href="javascript:void(0);">资讯</a></li><li><a href="javascript:void(0);">推荐</a></li><li><a href="javascript:void(0);">博客</a></li></ul></div><div class="col-xs-4"><ul class="links"><li><a href="javascript:void(0);">服务</a></li><li><a href="javascript:void(0);">圈子</a></li><li><a href="javascript:void(0);">论坛</a></li><li><a href="javascript:void(0);">广告</a></li></ul></div></div></div><div class="col-md-3 col-sm-3 col-md-push-1 col-sm-push-1"><div class="footer-social"><a href="javascript:void(0);"><i class="fa fa-weibo"></i></a> <a href="javascript:void(0);"><i class="fa fa-qq"></i></a> <a href="javascript:void(0);"><i class="fa fa-wechat"></i></a> </div></div><br/>
</div>
</div>
</div>
</footer>
<div id="floatbtn">
<!-- S 浮动按钮 -->
<!--
		<a class="hover" href="/index/cms.archives/post" target="_blank">
			<i class="iconfont icon-pencil"></i>
			<em>立即<br>投稿</em>
		</a>
		-->
<a class="hover" href="javascript:;" id="back-to-top">
<i class="iconfont icon-backtotop"></i>
<em>返回<br/>顶部</em>
</a>
<!-- E 浮动按钮 -->
</div>
<script src="/assets/libs/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="/assets/libs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/libs/fastadmin-layer/dist/layer.js" type="text/javascript"></script>
<script src="/assets/libs/art-template/dist/template-native.js" type="text/javascript"></script>
<script src="/assets/addons/cms/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/assets/addons/cms/js/swiper.min.js" type="text/javascript"></script>
<script src="/assets/addons/cms/js/cms.js?r=1.0.1" type="text/javascript"></script>
<script src="/assets/addons/cms/js/common.js?r=1.0.1" type="text/javascript"></script>
<div style="display: none;">
<script src="https://v1.cnzz.com/z_stat.php?id=394727&amp;web_id=394727" type="text/javascript"></script>
</div>
</body>
</html>
