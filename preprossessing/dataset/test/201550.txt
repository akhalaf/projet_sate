<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "23050",
      cRay: "611074ef6fad22c0",
      cHash: "d96fa555a5df250",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYmNkYi5jb20vY2FydG9vbi8xNTc4Ny1Db21pbl9Sb3VuZF9UaGVfTW91bnRhaW4uaHRtbA==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "OiRDB6DTGNSlEib4e0Rh7yAc37Y2kgmA+2Om2uQqQG/7WFBCnPGPQf5wlnls6z5r8n1zBx7ARmwjN90HY40IeSsQkWLPBxWuUqdlSu5iAPFQ5UMjanQxmHc4dU4KLAEXFldy1gC7x9e82nmXcJUm9EX0e9prNf+2CJKjg1+eZG1+lzAyaw8ayH6DCTBlJV1FYBjlNORsYSBNlvKqDCi2PU2BZ4X67g9N2nV4NEzsI9H+4g/Q8Cjyr8WERywaSbLn6JTashkZvy/nod+cLFeIRCJIE5pBwrwRqvgD/IjRd3sjXwQ9DkuzzHuu/wacmV/aoJRfi/ZxYYXGA/AeCWy7TDBakbMXd7dRGYYP/eIGwpqr1IzeBw6Yj81bkRhgvkYVx2cWkugzIfX+ryDbudWSrnjIHcsvVIdoK+ttdGsCburPB8YwEyNnamYot+yLC93rc17M1COgPQQTx2cQU0JvGuFfmSDfRhoa+t1UJJ3nwdF3cabNhCAzTOBySqMgEtrlySiEqAbENcl60yljPFdEgUQbMNQyWT7BPnIqf4f7JEs1JsMC7yPFuU8xZAHybdIi7e+tsyqQL+ZWOfB4J0pyNQsu8KVbs+ViXOMznY7PIDw9IdaA5nQoQzmIwDzzdITWkzRE1Y3hNpFdCd0x/iGcS4rMaFgViavLsiZhaunjjJNqSiXZqKjJOIMZUbsdsLnl75fy7JyI4g52vmmvyRflm70zwVhXX2o3BQMADZq6N8CGD/ofplibWUt3QPvHx+Mp",
        t: "MTYxMDU1NDk1NC4xNDkwMDA=",
        m: "gscNwVR69uKELO0/xJVeJiZYYJt7mnAGEtd1S3mYJZY=",
        i1: "zQEZgPZ+CkFv+GP1Mt78xQ==",
        i2: "1SxchhFCULPsE9RGOwK+Cg==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "cXDaOma3wiBrP6kg81PYP0GD5VWUQPtDIIDjSKLqBr4=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.bcdb.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/cartoon/15787-Comin_Round_The_Mountain.html?__cf_chl_captcha_tk__=1553831924b7493c6cc68ffc137fd5909cdd8b2e-1610554954-0-AZtK-gahS786Vo5nWk_9TkA8T_KmAqEPIRT-FomcAWk6W8J3tFfr1saKiH6SKsl--GYM73-5-w_i6tjozmxQ9zoZtxgXmcqMW88Pbqf91hvZZkQmRmK_xCyG5aR1Wzs4K2NlJ1bbr1gMnYPQY1onqIKYkNB1HSx8_wRH1rBAnhPCmvVuX-SMEkpsqzam0MOOQYVhgP9xggBfu4BFQAGXY5zJfIA7o6HkSXRnUtq-r1lJXZivIEKeNMbPFFQ0Hu3Np4jpS3G5OXYwYfMC2FDu7QH1ikSb2zEGyUqNojdfbKF0b0zUAI0r4w7kA6PlqF2s_vQ38ocFOpXefkva9a4VAF6LeRe9Wt_uK3cIrIfemOtZxNTW57lJAB2HPxRgy6hIIC70egQCnHyCXrr-XxmLSSz8sXZJ06Onu6jGu70qdf546_L28HpBv0WEEnV182adCApXYXc3FmYYYERkfhvd1_8_SWVw7aS5ZTNzibSblbC-ujtkJnykHihOd3tPdFOWEnRpSAW5wawPBtUDI2l6VbI-bwIlTbmGgJEENYdPe0qo-b1-xkok09tCBfwJW6LB7butoO5cSsbrV1K1nn-Jpfs" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="f92d3bc19044d2c29651ec6835a438ce86df3ef9-1610554954-0-AfXxPG9fF4LKS6cS4OqluaTV6TUIWUyKG/UcvIaICNmLHLJfcnUkaAbEXyqTLNxM9Qdn8FtmZ6aKI6bautXEZjohbTsIvMmeUW+vJVju6N6G2ei1h5SMAk5FyDUuFkMTzHcyOM1g1mYfAWS/fYz/Lc+5AfOtX/enrMuMqp+QrU3nm8mI4qon+2eOr6D5u5mYV/IvjnycjrMD+mMBSRuFNTGMcefHZn2Oo/8A3nx9EnJjM+sNTBgN8uf2odWO/7hGQ3KWfqVBcNqVXXhJtV2EE59Tudv3jcLallZAEMVClbzCH4VLtbsWDrc960/FkTJL7dKDa+pQaaUAP7CaJu5k9S56lFNbTvVPHGu1psjKNOPDMr5ZbBIWPP5HXCmoudoyoAgIg1wv7Sv53e7n5ZmpdRo8WJKlCfQDGhakXFnfl68nL/Pbca1yVJ7T/YiMjvfrqC7MDJBcefAK1+FUv8U6Z8ZmW8RmRBV2yKMFL/qlP1ESGiZ7KvBDfR352ZgdhdTFC8UgBCcqgRfqsLyGvS3OJ1XamviiNg7N/baMjZOOi63/BqADgf7yTgmNuHY2AXFq3EaFg2LO1kacAguKolzdhWeltc6LJj1A2U7zDxnsHyEq/0xWnIif+jRTUpi7VwWtxXI4Xh6US6TJ2K+YR59kWMGLd5TpW9cg514HXcavmNq2s2DBis6spqfmRsfM8WS51Wy90gY9jzfVCHMi/rgeML9YLh5Cq/W1g+7lsFr+qo4Z1XzdTBCVPrcjTDH0m8xqNN1BGtfkYEFaqqSlegNWp6IfnmfAB+HjyHbsKooDtGJ5KydN7W7KYAMb/c9rb1U3R8EJZK5RstxsO88Kon5cq19jAmSekyBJgtBoC/v9gsQLgfAoMw1+sD6JXblW/zIPlCgPOM6e2ntl5t/Mlgc59ZEAewDbJhaGstmdUDB+p3jtEOmNo/MbHNSW9rFFhh4/SgEoWeKf8ZGBrAOf+ryEzs4IH+Z6Kxww2T5idbH9+XYtNe3N1dyxKq+1vpZr4joIZcZkRv+sjNBdtKG5p3LQzoG621AXaSYz6ULDUVU5KMJZkyPafts5wqcrJogU1uXehO/QG5FIVX78C7gcY+p3u4r/I552fvkq3NOyBsrL7G3hugDyp3pT6/5xfC4pFQCRqhNenPmqwxNFocqi+MdaCx6nm7GKfg0EiTHhT4Loe5wXV/CUx+JShJQDg4iAlXNGBAN1QjA76Zpipflld2qNm3b2BbIaWHnx7DtRr4btcvRcDzUJ9DtW/7kroisZA4IKYljfCUpZ4wkMqz0jH9neZtc="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="36d3a6272a591cfebf371f003b9f9946"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=611074ef6fad22c0')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div style="display: none;"><a href="https://tinwatch.net/inclusivecool.php?tag=9">table</a></div>
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">611074ef6fad22c0</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
