<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="nl-nl" xml:lang="nl-nl" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<title>404 - Categorie niet gevonden</title>
<link href="/cms/templates/system/css/error.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="error">
<div id="outline">
<div id="errorboxoutline">
<div id="errorboxheader">404 - Categorie niet gevonden</div>
<div id="errorboxbody">
<p><strong>U kunt deze pagina mogelijk niet bezoeken door:</strong></p>
<ol>
<li>een <strong>verouderde favoriet</strong></li>
<li>een zoekmachine met een <strong>verouderde vermelding voor deze website</strong></li>
<li>een <strong>verkeerd ingetypt adres</strong></li>
<li>u heeft <strong>geen toegang</strong> tot deze pagina</li>
<li>De gevraagde bron is niet gevonden.</li>
<li>Er is een fout opgetreden bij het verwerken van uw verzoek.</li>
</ol>
<p><strong>Probeer een van de volgende pagina's:</strong></p>
<ul>
<li><a href="/cms/index.php" title="Ga naar de home-pagina">Home-pagina</a></li>
</ul>
<p>Als problemen blijven bestaan, neem dan contact op met de beheerder van deze website.</p>
<div id="techinfo">
<p>Categorie niet gevonden</p>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
