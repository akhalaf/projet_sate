<!DOCTYPE html>
<!-- Last Published: Mon Jan 11 2021 15:23:06 GMT+0000 (Coordinated Universal Time) --><html data-wf-domain="www.anedot.com" data-wf-page="5f1b6d5920ad6e3d9c0c737b" data-wf-site="5f1b6d58945d3050cff2dbda">
<head>
<meta charset="utf-8"/>
<title>Anedot | Raising the standard for online giving</title>
<meta content="Trusted by more than 20,000 churches, campaigns, schools, and nonprofits, Anedot optimizes the donation experience resulting in more donations for your organization." name="description"/>
<meta content="Anedot | Raising the standard for online giving" property="og:title"/>
<meta content="Trusted by more than 20,000 churches, campaigns, schools, and nonprofits, Anedot optimizes the donation experience resulting in more donations for your organization." property="og:description"/>
<meta content="https://assets.website-files.com/5dfa6ae441a0f0233c704c0b/5e1f8dd1ec6f621804114272_Artboard.png" property="og:image"/>
<meta content="Anedot | Raising the standard for online giving" property="twitter:title"/>
<meta content="Trusted by more than 20,000 churches, campaigns, schools, and nonprofits, Anedot optimizes the donation experience resulting in more donations for your organization." property="twitter:description"/>
<meta content="https://assets.website-files.com/5dfa6ae441a0f0233c704c0b/5e1f8dd1ec6f621804114272_Artboard.png" property="twitter:image"/>
<meta content="website" property="og:type"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/css/anedot.11937e914.css" rel="stylesheet" type="text/css"/>
<style>
      @media (min-width:992px) {
        html.w-mod-js:not(.w-mod-ix) [data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e68"] {
          -webkit-transform: translate3d(-248PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
          -moz-transform: translate3d(-248PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
          -ms-transform: translate3d(-248PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
          transform: translate3d(-248PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);
          opacity: 0;
        }

        html.w-mod-js:not(.w-mod-ix) [data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e5d"] {
          -webkit-transform: translate3d(0, 22PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);
          -moz-transform: translate3d(0, 22PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);
          -ms-transform: translate3d(0, 22PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);
          transform: translate3d(0, 22PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);
          display: none;
        }
      }
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
<script type="text/javascript">
      WebFont.load({
        google: {
          families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic", "Barlow:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic", "Roboto:regular"]
        }
      });
    </script>
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]-->
<script type="text/javascript">
      ! function(o, c) {
        var n = c.documentElement,
          t = " w-mod-";
        n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
      }(window, document);
    </script>
<link href="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6eedba8e58cb02716c61_96_A.png" rel="shortcut icon" type="image/x-icon"/>
<link href="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6f09df2b083fd86fa49c_800_A_LinkedIn.png" rel="apple-touch-icon"/><!-- Anedot Analytics -->
<script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setCookieDomain", "*.anedot.com"]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u = "//analytics.anedot.com/";
        _paq.push(['setTrackerUrl', u + 'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d = document,
          g = d.createElement('script'),
          s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + 'matomo.js';
        s.parentNode.insertBefore(g, s);
      })();
    </script>
<noscript>
<p><img alt="" src="//analytics.anedot.com/matomo.php?idsite=1&amp;rec=1" style="border:0;"/></p>
</noscript>
<!-- End Anedot Analytics Code -->
<style>
      @media only screen and (min-width: 2400px) {
        .home-hero-bg-x {
          background-size: cover;
        }
      }
    </style>
<script type="text/javascript">
      window.__WEBFLOW_CURRENCY_SETTINGS = {
        "currencyCode": "USD",
        "$init": true,
        "symbol": "$",
        "decimal": ".",
        "fractionDigits": 2,
        "group": ",",
        "template": "{{wf {\"path\":\"symbol\",\"type\":\"PlainText\"} }} {{wf {\"path\":\"amount\",\"type\":\"CommercePrice\"} }} {{wf {\"path\":\"currencyCode\",\"type\":\"PlainText\"} }}",
        "hideDecimalForWholeNumbers": false
      };
    </script>
</head>
<body class="body">
<div class="navbar w-nav" data-animation="over-right" data-collapse="medium" data-duration="400" role="banner">
<div class="nav-container">
<div class="div-block-13"><a aria-current="page" class="brand w-nav-brand w--current" href="/"><img alt="Anedot logo" class="logo" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b795511d9ea61a236b3be_Anedot--Blue.svg" width="100"/></a>
<div class="menu_wrap-copy">
<nav class="nav-menu w-nav-menu" role="navigation">
<div class="nav_links_wrapper">
<div class="dropdown w-dropdown" data-delay="0" data-hover="1" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e5b">
<div class="dropdown-toggle w-dropdown-toggle">
<div class="mega_nav_pointer" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e5d"></div>
<div>Solutions</div>
</div>
<nav class="dropdown-list w-dropdown-list">
<div class="container mega_nav_container">
<div class="mega_menu_title-box">
<h2>Who we serve</h2>
<p>Anedot builds powerful solutions that empower generosity.</p>
</div>
<div class="mega-menu-main-content">
<div class="w-layout-grid _2_col_grid mega_nav_links" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e68"><a class="mega_nav_link w-inline-block" href="/home/landing-1">
<div class="mega_nav_icon_wrap"><img alt="" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb332447e5065cc06d508_Churches.svg" width="33"/></div>
<div class="written_content">
<h4 class="no_margin">Churches and Ministries</h4>
</div>
</a><a aria-current="page" class="mega_nav_link w-inline-block w--current" href="/">
<div class="mega_nav_icon_wrap"><img alt="" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb394368ff64da8963954_Schools.svg" width="33"/></div>
<div class="written_content">
<h4 class="no_margin">Colleges, Universities, and Schools</h4>
</div>
</a><a class="mega_nav_link w-inline-block" href="/home/landing-3">
<div class="mega_nav_icon_wrap"><img alt="" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb37f488481aa6b81b58a_Campaigns.svg" width="33"/></div>
<div class="written_content">
<h4 class="no_margin">Political Campaigns and Committees</h4>
</div>
</a><a class="mega_nav_link w-inline-block" href="/home/landing-4">
<div class="mega_nav_icon_wrap"><img alt="" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb3ddf60f94607e5bc74e_Nonprofits.svg" width="33"/></div>
<div class="written_content">
<h4 class="no_margin">Nonprofits</h4>
</div>
</a></div>
</div>
</div>
</nav>
</div><a class="nav_link w-inline-block" href="/pricing">
<div>Pricing</div>
</a>
</div>
</nav>
<div class="hamburger_wrap" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e84">
<div class="hamburger_line" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e85" style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0DEG) skew(0, 0);background-color:rgb(44,57,65)"></div>
<div class="hamburger_line" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e86" style="background-color:rgb(44,57,65);opacity:1"></div>
<div class="hamburger_line" data-w-id="d56bd897-9a28-96d8-da5d-085fac3d2e87" style="background-color:rgb(44,57,65);-webkit-transform:translate3d(0, 0, 0PX) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0, 0PX) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0, 0PX) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0, 0PX) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform-style:preserve-3d"></div>
</div>
</div>
</div><a class="outlined_button dark_outline w-button" href="https://anedot.com/login/">LoginÂ â</a>
</div>
<div class="mobile_menu" style="-webkit-transform:translate3d(-100%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-100%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-100%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-100%, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)">
<div class="sub_links_header login"></div>
<div class="mobile_menu_content">
<div class="mobile-links-wrap">
<div class="mobile-nav-link-contain expand">
<h3>Solutions</h3><a class="mobile-nav-link w-inline-block" href="/about/about">
<h4 class="mobile-sub-link">For Churches</h4>
</a><a class="mobile-nav-link w-inline-block" href="/pages/contact-v-1">
<h4 class="mobile-sub-link">For Campaigns</h4>
</a><a class="mobile-nav-link w-inline-block" href="/pages/contact-v-2">
<h4 class="mobile-sub-link">For Nonprofits</h4>
</a><a class="mobile-nav-link w-inline-block" href="/pages/faqs">
<h4 class="mobile-sub-link">For Education</h4>
</a>
</div>
<div class="div-block-14">
<h3></h3><a class="mobile-nav-link w-inline-block" href="/pages/faqs">
<h4 class="mobile-sub-link">Pricing</h4>
</a>
<div class="expand_heading"></div>
<div class="sub_links_contain"></div>
</div>
<div class="div-block-15"><a class="solid-button mobile_login button-2 w-button" href="/login/login">Sign upÂ âÂ </a><a class="outlined_button mobile_login w-button" href="/login/login">Login</a></div>
</div>
</div>
</div>
</div>
<div class="section hero_section_l2">
<div class="container flex_centered_vertical" data-w-id="01bae10d-a2ab-245c-0321-3ba93d264ef6" style="opacity:0">
<h1 class="align_center">Powerful giving tools made easy for everyone.</h1>
<div class="sub_heading_large centered_aligned">Anedot is for <span class="text-span" id="typed">churches.</span></div><a class="hero_button w-button" href="https://anedot.com/signup">Get started for free</a>
<div class="ticks_contain">
<div class="tick">â Free text-to-give</div>
<div class="tick" data-w-id="7d441e79-6881-84af-8d29-5b6e4a909232" style="-webkit-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)">â No setup fees</div>
<div class="tick" data-w-id="5ecf7ce5-10c9-6302-f876-69128443b563" style="-webkit-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 83PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0">â $0 / month</div>
</div>
</div>
<div class="home-hero-bg-x"></div>
</div>
<div class="section no_top_bottom_padding">
<div class="container">
<div class="w-layout-grid _2_col_grid with-margin reverse_on_mobile"><img alt="Anedot donation page" class="image-42" data-w-id="def538ba-15e3-d3ea-a08e-b73206d43c8c" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48233b196fd47b064164_beautiful-donation-pages-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48233b196fd47b064164_beautiful-donation-pages-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48233b196fd47b064164_beautiful-donation-pages-anedot.png 1442w" style="opacity:0"/>
<div class="content_contain no_bottom_margin" data-w-id="def538ba-15e3-d3ea-a08e-b73206d43c8d" style="opacity:0">
<h2 class="why_anedot_titles">Beautiful donation pages</h2>
<p class="max_450px">Create unlimited beautiful donation pages for every event or fund.</p>
</div>
</div>
<div class="w-layout-grid _2_col_grid with-margin">
<div class="content_contain no_bottom_margin" data-w-id="e546275d-d1d9-1fa9-b191-27f14bc8fed3" style="opacity:0">
<h2 class="why_anedot_titles">Simple drag and drop page editor</h2>
<p class="max_450px">Change the look of any page with ease. Even if you aren't the most tech savvy!<br/></p>
</div><img alt="Drag and drop page editor" class="image-34" data-w-id="e546275d-d1d9-1fa9-b191-27f14bc8fedb" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48311bbbd4140967ae5b_simple-drag-and-drop-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48311bbbd4140967ae5b_simple-drag-and-drop-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b48311bbbd4140967ae5b_simple-drag-and-drop-anedot.png 1444w" style="opacity:0"/>
</div>
<div class="w-layout-grid _2_col_grid with-margin reverse_on_mobile"><img alt="Text message bubbles that showchase text to give feature" class="image-43" data-w-id="995ce6ae-14c9-3765-314f-9cbdd337a650" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b483b17b9295c534d9199_free-text-to-give-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b483b17b9295c534d9199_free-text-to-give-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b483b17b9295c534d9199_free-text-to-give-anedot.png 1440w" style="opacity:0"/>
<div class="content_contain no_bottom_margin" data-w-id="995ce6ae-14c9-3765-314f-9cbdd337a651" style="opacity:0">
<h2 class="why_anedot_titles">Free text-to-give</h2>
<p class="max_450px">With our popular text-to-give feature, donors can make a donation by simply sending a text message. The best part? We include text-to-give with every account, so you donât have to worry about paying extra fees.</p>
</div>
</div>
<div class="w-layout-grid _2_col_grid with-margin">
<div class="content_contain no_bottom_margin" data-w-id="b2e313a5-af90-cd2d-baa1-49df49de129c" style="opacity:0">
<h2 class="why_anedot_titles">Powerful reporting</h2>
<p class="max_450px">Reporting and bookkeeping are simple with Anedot. Build fully customizable reports and track donation data in real time.<br/></p>
</div><img alt="Illustration of customized reports" class="image-34" data-w-id="b2e313a5-af90-cd2d-baa1-49df49de12a2" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e443dc0505da9dedaaf_powerful-reporting-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e443dc0505da9dedaaf_powerful-reporting-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e443dc0505da9dedaaf_powerful-reporting-anedot.png 1440w" style="opacity:0"/>
</div>
<div class="w-layout-grid _2_col_grid with-margin reverse_on_mobile"><img alt="Illustration of covering processing fee" class="image-44" data-w-id="ee687d19-aaca-8b1f-07e8-7f26122ec359" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b4858f84dbb7bdaa70460_donor-pays-fees-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b4858f84dbb7bdaa70460_donor-pays-fees-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b4858f84dbb7bdaa70460_donor-pays-fees-anedot.png 1440w" style="opacity:0"/>
<div class="content_contain no_bottom_margin" data-w-id="ee687d19-aaca-8b1f-07e8-7f26122ec35a" style="opacity:0">
<h2 class="why_anedot_titles">Donor pays fees</h2>
<p class="max_450px">Save your organization money by allowing donors to cover their own transaction fees.</p>
</div>
</div>
<div class="w-layout-grid _2_col_grid with-margin">
<div class="content_contain no_bottom_margin" data-w-id="dc5ba045-4c4e-09d5-d053-8c01746e310f" style="opacity:0">
<h2 class="why_anedot_titles">Multiple account support</h2>
<p class="max_450px">Partner with your own entities or allied causes to raise money into multiple bank accounts at the same time.<br/></p>
</div><img alt="Illustration of multiple account support" class="image-34" data-w-id="dc5ba045-4c4e-09d5-d053-8c01746e3115" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5217bfde7c728fbae4_multiple-account-support-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5217bfde7c728fbae4_multiple-account-support-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5217bfde7c728fbae4_multiple-account-support-anedot.png 1440w" style="opacity:0"/>
</div>
<div class="w-layout-grid _2_col_grid with-margin reverse_on_mobile"><img alt="Illustration of data privacy" class="image-45" data-w-id="77d6bbae-1525-3fe1-b126-03adae6f5fd7" sizes="(max-width: 479px) 88vw, 450px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5d6247ad1c02187e7c_secure-not-for-sale-anedot.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5d6247ad1c02187e7c_secure-not-for-sale-anedot-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b5e5d6247ad1c02187e7c_secure-not-for-sale-anedot.png 1440w" style="opacity:0"/>
<div class="content_contain no_bottom_margin" data-w-id="77d6bbae-1525-3fe1-b126-03adae6f5fd8" style="opacity:0">
<h2 class="why_anedot_titles">Secure, not for sale and committed to your privacy</h2>
<p class="max_450px">Anedot is privately owned and not guided by quarterly numbers. Our team is focused on providing long-term, best-in-class fundraising solutions. As a part of that focus, we're committed to your privacy. Your data is not for sale.</p>
</div>
</div>
</div><img alt="" class="fill_space_image" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e3be60c73d4_Whiteboard%20no%20grey%20bg.svg" width="615"/>
</div>
<div class="section dark_bg">
<div class="container flex_centered_vertical">
<div class="max_600px max_800px">
<h2 class="heading">Trusted by more than 20,000 organizations<br/>and millions of donors</h2>
</div>
<div class="logo_contain-copy-copy">
<div class="logo_wrap" data-w-id="b3e2f83c-89ca-6636-f0c3-580c228fee73" id="w-node-580c228fee73-9c0c737b" style="opacity:0"><img alt="Susan B. Anthony List logo" class="image-31" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb914368ff6750d964215_SBA.svg" width="61"/></div>
<div class="logo_wrap" data-w-id="8592ef3f-8627-4ac0-eb37-6f555fdb9eb1" id="w-node-6f555fdb9eb1-9c0c737b" style="opacity:0"><img alt="Focus on the Family logo" class="image-30" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1cba269c781414c5b54bf5_FOTF_logo%201.svg" width="290"/></div>
<div class="logo_wrap" data-w-id="d5bede3e-82e6-f70d-7d02-475cbd306f6c" style="opacity:0"><img alt="IACE logo" class="image-35" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c2723811481bb60660a3a_static1.squarespace-1.png" width="80"/></div>
<div class="logo_wrap" data-w-id="e49d60eb-cccb-7603-1eaa-eafd4c0dbc7e" style="opacity:0"><img alt="Thomas More Society logo" class="image-37" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c2e7c4bf7d4fb10c117ad_thomasmore.png" width="122"/></div>
<div class="logo_wrap" data-w-id="c8b9b8f2-6e8b-af08-97c7-d0cd7720c944" style="opacity:0"><img alt="DC Dream Center logo" class="image-37" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c2f134ee781bd69858732_logo_header1.png" width="122"/></div>
<div class="logo_wrap" data-w-id="121a2aae-b571-a6c1-05ee-e3922ad2bfb4" style="opacity:0"><img alt="Woodson Center logo" class="image-32" sizes="(max-width: 479px) 44vw, (max-width: 767px) 38vw, (max-width: 991px) 245px, 220.5px" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bba80447e506db006e088_logo%403x.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bba80447e506db006e088_logo%403x-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bba80447e506db006e088_logo%403x.png 1005w" width="245"/></div>
</div>
</div>
</div>
<div class="section overflow_hidden">
<div class="container">
<div class="w-layout-grid _2_col_grid who_we_serve_grid">
<div class="content_contain">
<h2 class="why_anedot_titles" data-w-id="d8d976fb-a193-866d-2062-597bfee31970">Who we serve</h2>
<p class="max_450px" data-w-id="a19a49e7-bf9e-8348-5d6e-a8daca2402af" style="opacity:0">Anedot builds powerful solutions that empower generosity.</p>
</div>
<div class="content_contain industry_list">
<div class="who_we_serve">
<div class="mega_nav_icon_wrap"><img alt="Dove icon" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb332447e5065cc06d508_Churches.svg" width="33"/></div>
<div class="written_content">
<h3 class="heading-14">Churches and Ministries</h3>
</div>
</div>
<div class="who_we_serve">
<div class="mega_nav_icon_wrap"><img alt="Building icon" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb37f488481aa6b81b58a_Campaigns.svg" width="33"/></div>
<div class="written_content">
<h3 class="heading-15">Political Campaigns and Committees</h3>
</div>
</div>
<div class="who_we_serve">
<div class="mega_nav_icon_wrap"><img alt="Apple icon" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb394368ff64da8963954_Schools.svg" width="33"/></div>
<div class="written_content">
<h3 class="heading-16">Colleges, Universities, and Schools</h3>
</div>
</div>
<div class="who_we_serve">
<div class="mega_nav_icon_wrap"><img alt="Heart icon" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1bb3ddf60f94607e5bc74e_Nonprofits.svg" width="33"/></div>
<div class="written_content">
<h3 class="heading-17">Nonprofits</h3>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="section with-curves"><img alt="" class="great-gradient-curve image-78" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e99ab0c744e_Grey%20Gradient%20Curve.svg"/></div>
<div class="section off_white_bg overflow_hidden">
<div class="container">
<div class="w-layout-grid _2_col_grid with-margin reverse_on_mobile"><img alt="Illustration of integration support" class="image-46" loading="lazy" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f21ea4b3061b651350aa37c_integrations-final.svg"/>
<div class="content_contain">
<h2 class="why_anedot_titles">Integrations<br/></h2>
<p class="max_450px" data-w-id="c4375a3d-f224-11f3-8a0d-516b014c0d10" style="opacity:0">Connect with more than 20 official integration partners and more than 1,000 other integrations through Zapier! </p>
</div>
</div>
<div class="w-layout-grid _2_col_grid">
<div class="content_contain">
<h2 class="why_anedot_titles">Straight-forward,<br/>honest pricing</h2>
<p class="max_450px" data-w-id="9824357d-a338-245d-1603-1ed038ecf3ba" style="opacity:0">No salespeople or hidden fees.<br/>You only pay Anedot when you process donations!<br/><br/>Tailored for organizations of all sizes. Large organization? <a href="mailto:help@anedot.com?subject=Pricing%20Inquiry%20for%20Large%20Organization">Send us a note.</a></p>
<div class="quote_contain-2" data-w-id="b54408f3-b20d-c73c-dccd-afe433891dd1" style="opacity:0">
<div class="quote">"<em class="italic-text-3">When you sign up with Anedot, you can have the peace of mind that we have no hidden fees, no contracts, and no surprises. Everything you need is included.</em>"</div>
<div class="quote_author"><span class="bold_text_span">Paul Dietzel,</span> Founder and CEO</div><img alt="" class="quote_logo" sizes="100vw" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark.png" srcset="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark-p-500.png 500w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark-p-800.png 800w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark-p-1080.png 1080w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark-p-1600.png 1600w, https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1b6d5920ad6e8c800c73d6_webflow_dark.png 1723w" width="60"/>
</div>
</div>
<div class="content_contain">
<div class="stacking_cards_contain" data-w-id="3f6d3c6a-a5d3-0b6f-e8c5-ecbdf215d2ed" style="opacity:0">
<div class="card_stack">
<div class="dots_wrap">
<div class="dot"></div>
<div class="dot"></div>
<div class="dot"></div>
</div>
<div class="card_stack_content_contain">
<div class="total-orders-contain">
<h2 class="total-orders">$0/month</h2>
<div class="card_stack_smal_h">Setup fees</div>
<div class="card_stack_smal_h">hidden fees</div>
<div class="card_stack_smal_h">Contracts</div>
<div class="card_stack_smal_h no-strike">Free text-to-give</div>
</div>
</div>
</div>
<div class="card_stack card_stack_2"></div>
<div class="card_stack card_stack_3"></div>
<div class="stacked_cards_background_contrast"></div>
</div>
</div>
</div>
<div class="w-layout-grid _2_col_grid reverse_wrap_on_mobile"><img alt="Highlighted map of USA that shows where Anedot employees are located" class="image-47" loading="lazy" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b4814d5728917517f2839_fast-friendly-service-anedot.png"/>
<div class="content_contain">
<h2 class="why_anedot_titles">Fast and friendly service</h2>
<p class="max_450px" data-w-id="ee34b852-d58a-d9ff-ccf2-3d7e67232dac" style="opacity:0">Anedot is rooted in South Louisiana, but the Anedot family has been 100%Â remote for 10 years working from Louisiana, Washington, California, Texas, Florida, Maryland, and Ohio. <br/><br/>Our team loves serving our customers and carries a sense of southern hospitality.</p>
</div>
</div>
</div>
</div>
<div class="testimonial-section">
<div class="testimonials-feature">
<div class="slider w-slider" data-animation="slide" data-autoplay="1" data-delay="14000" data-duration="1200" data-easing="ease-in-out-quint" data-infinite="1"><img alt="Plane carrying a banner " class="image-2-copy" data-w-id="08559135-ffd1-0694-7f77-7b04a4e6b13c" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1efa99d806914e963f943c_testimonials--ft--airplane.svg"/>
<div class="mask w-slider-mask">
<div class="w-slide">
<div class="testimonial-slide"><img alt="Illustration of a man" class="quote-logo" height="48" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7b6655a50f4b6198e912c5_joey-testimonial.svg" width="48"/>
<div class="quote-details">
<div class="block-quote">âOur church used a different online giving platform for three years. During that time we had a total of nine online givers and were paying a monthly fee for 'free online giving.' Within three weeks of switching to Anedot, our total online givers have more than quadrupled and we truly have no monthly fees.â</div>
<h3 class="heading-10">Joey D.<strong><br/></strong></h3>
<div class="paragraph-3">Oakdale Baptist Church<br/></div>
</div>
</div>
</div>
<div class="w-slide">
<div class="testimonial-slide"><img alt="Illustration of a man with a suit" class="quote-logo" height="48" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1efa99d8069122053f943b_testimonials--ft-two.svg" width="48"/>
<div class="quote-details">
<div class="block-quote">âAfter 5 years of working with them, I don't and have not had one complaint about the service they offer. On every level, they provide a service that meets and exceeds all expectations. Anedot's prices are extremely competitive, although I would choose their service over any others even if their fees were doubled.â<br/></div>
<h3 class="heading-10"><strong>Ryan P.<br/></strong></h3>
<div class="paragraph-3">Political Organization<br/></div>
</div>
</div>
</div>
<div class="w-slide">
<div class="testimonial-slide"><img alt="Illustration of a man" class="quote-logo" height="48" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f7e2d25c36d8bb38548a551_brandon-testimonial.svg" width="48"/>
<div class="quote-details">
<div class="block-quote">âOne reason I believe our giving increased when we switched from PayPal to Anedot is the fact that Anedot allowed us to carry our branding over - so you still feel like you are within our website. You have our logo, our colors, etc. I also love that the developers are continuing to enhance the product.â</div>
<h3 class="heading-10">Brandon J.<strong><br/></strong></h3>
<div class="paragraph-3">MacTech Coordinator<br/></div>
</div>
</div>
</div>
</div>
<div class="left-arrow w-slider-arrow-left">
<div class="icon w-icon-slider-left"></div>
</div>
<div class="right-arrow-2 w-slider-arrow-right">
<div class="w-icon-slider-right"></div>
</div>
<div class="slide-nav w-slider-nav w-slider-nav-invert w-round"></div>
</div>
</div>
</div>
<div class="section">
<div class="container">
<div class="cta_block">
<div class="title-wrap-copy w-clearfix">
<h2 class="heading-9 heading-title">See why thousands of churches, campaigns, schools, and nonprofits across the United States use Anedot</h2><a class="button-4 button-medium w-inline-block" href="https://anedot.com/signup/">
<div class="text-block-4">Get started today</div>
</a>
</div>
</div>
</div>
</div>
<div class="section">
<div class="section-6">
<div class="support-wrap">
<div class="support-panel"><img alt="Support icon" class="support-icon" height="48" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1efb6103215585830d0760_ico--faq.svg" width="48"/>
<div>
<h3 class="heading-10">Frequently asked questions</h3>
<p class="paragraph-small">Visit our FAQ for help answering all of your questions.<br/></p><a class="link-3" href="https://help.anedot.com/">Visit our FAQ <strong>â</strong></a>
</div>
</div>
<div class="support-panel-spacer"></div>
<div class="support-panel"><img alt="Chat icon" class="support-icon" height="48" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1efb61032155332b0d0761_ico--service.svg" width="48"/>
<div>
<h3 class="heading-10">Need assistance? Here to help</h3>
<p class="paragraph-small">Support is available MonâFri from 8AMâ6PM CST.<br/></p><a class="link-3 support-link" href="mailto:help@anedot.com">help@anedot.com<strong></strong></a><a class="link-3" href="tel:(225)250-1301">(225) 250-1301<strong></strong></a>
</div>
</div>
</div>
</div>
</div>
<div class="section section-footer">
<div class="container">
<div class="footer_cta_wrap" data-w-id="8dcc93f0-bcd1-d7d5-2398-b5601542998a">
<div class="max_300px">
<h2 class="heading-6">Ready to save time and grow?</h2><a class="orange_cta_button display_block w-button" href="https://anedot.com/signup/">Get started with Anedot<span class="button_arrow"> â</span></a>
</div>
</div>
<div class="w-layout-grid _4_col_grid hide">
<div class="footer_column" data-w-id="8dcc93f0-bcd1-d7d5-2398-b56015429998">
<div class="footer_title">Getting Started</div><a class="footer_link" href="/pages/faqs">FAQs</a><a class="footer_link" href="/pricing">Pricing</a><a class="footer_link" href="/home/landing-3">Reviews</a><a class="footer_link" href="https://anedot.com/signup/">Sign up</a>
</div>
<div class="footer_column" data-w-id="8dcc93f0-bcd1-d7d5-2398-b560154299a5">
<div class="footer_title">Solutions</div><a class="footer_link" href="/about/about">For Churches and Ministries</a><a class="footer_link" href="/pricing">For Political Campaigns</a><a class="footer_link" href="/about/pricing-v-2">For Nonprofits</a><a class="footer_link" href="/about/careers">For Education</a>
</div>
<div class="footer_column" data-w-id="8dcc93f0-bcd1-d7d5-2398-b560154299b0">
<div class="footer_title">Resources</div><a class="footer_link" href="#">Center for Digital Action</a><a class="footer_link" href="#">Marketplace</a><a class="footer_link" href="https://blog.anedot.com/">Blog</a>
<div>
<div class="footer_column second">
<div class="footer_title">Support</div><a class="footer_link" href="https://help.anedot.com">Help Center</a>
</div>
</div>
</div>
<div class="footer_column" data-w-id="8dcc93f0-bcd1-d7d5-2398-b560154299c5">
<div class="footer_title">Company</div><a class="footer_link" href="/pages/our-story">Our Story</a><a class="footer_link" href="/pages/contact-v-2">Our Founder</a><a class="footer_link" href="/pages/press-media">Press and Media</a><a class="footer_link" href="/about/careers">Careers</a>
</div>
</div>
<div class="copy_rights_contain">
<div class="copyrights" data-w-id="8dcc93f0-bcd1-d7d5-2398-b560154299d9">Â© Anedot, Inc.<span class="copyright_spacer"> Â Â· Â </span><a class="copyrights_link" href="https://anedot.com/privacy">Privacy Policy</a><span class="copyright_spacer"> Â Â· Â </span><a class="copyrights_link" href="https://anedot.com/terms">Terms</a><span class="copyright_spacer"> Â Â· Â </span><a class="copyrights_link" href="https://status.anedot.com">Platform Status</a></div>
</div>
<div class="social_links_wrapper justify_start"><a class="social_link first-link w-inline-block" href="https://www.linkedin.com/company/anedot" target="_blank"><img alt="LinkedIn logo" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c75b295f687e8f95aa3e8_LinkedIn.svg" width="18"/></a><a class="social_link w-inline-block" href="https://facebook.com/anedot"><img alt="Facebook logo" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c75b2b574ea3c0ec0a3bd_Facebook.svg" width="18"/></a><a class="social_link w-inline-block" href="https://instagram.com/anedot_"><img alt="Instagram logo" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c75b1dd643e314892abe9_Instagram.svg" width="18"/></a><a class="social_link w-inline-block" href="https://twitter.com/anedot" target="_blank"><img alt="Twitter logo" src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/5f1c75b1e73d4179e6c3fa89_Twitter.svg" width="18"/></a></div>
</div>
</div>
<script crossorigin="anonymous" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f1b6d58945d3050cff2dbda" type="text/javascript"></script>
<script src="https://assets.website-files.com/5f1b6d58945d3050cff2dbda/js/anedot.bfab7a70d.js" type="text/javascript"></script>
<!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
<!-- Start of HubSpot Embed Code -->
<script async="" defer="" id="hs-script-loader" src="//js.hs-scripts.com/6781885.js" type="text/javascript"></script>
<!-- End of HubSpot Embed Code -->
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>
<script>
      var typed = new Typed("#typed", {
        strings: ['churches.', 'campaigns.', 'universities.', 'nonprofits.', 'action.', 'churches.', 'campaigns.', 'universities.', 'nonprofits.', 'action.', 'churches, campaigns, nonprofits, universities, and action.'],
        typeSpeed: 125,
        backSpeed: 75,
        backDelay: 1500,
        startDelay: 2500,
        loop: false,
        showCursor: true,
        cursorChar: "|",
        attr: null,
      });
    </script>
</body>
</html>
