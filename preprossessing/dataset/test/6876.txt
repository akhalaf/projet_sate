<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<meta charset="utf-8"/>
<title>123 Free Solitaire</title>
<meta content="123 Free Solitaire game. 100% free! Play Spider solitaire, FreeCell and many others solitaire games for free." name="description"/>
<meta content="solitaire, card games, spider solitaire, free solitaire, solitaires, solitaire game, solitaire card games, solitiare" name="keywords"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="/css/main.mind401.css" rel="stylesheet"/>
<link href="/css/style.css" id="main-style" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Raleway%3A400%2C700%2C600&amp;ver=1.0.4" rel="stylesheet"/>
<link href="/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="/css/beauty-line-icons.css" rel="stylesheet"/>
<!-- <link rel="shortcut icon" href="/img/favicon.png"> -->
<link href="/index.htm" rel="canonical"/>
<link href="/css/popup.css" rel="stylesheet"/>
<script src="/js/cookie-consent/cookieconsent.js" type="text/javascript"></script>
<link href="/js/cookie-consent/cookieconsent.css" rel="stylesheet"/>
</head>
<body class="home page">
<header class="banner navbar navbar-default navbar-fixed-top" role="banner">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" id="showRightPush" type="button"><i class="klico-sidebar"></i></button>
<a class="navbar-brand" href="https://www.123freesolitaire.com">
<img alt="123 Free Solitaire" src="/img/logo/123_free_solitaire.png"/>
</a>
</div>
<nav class="collapse navbar-collapse" role="navigation">
<ul class="nav navbar-nav" id="menu-primary-navigation">
<li class="menu-home"><a href="/download.htm">Free Download</a></li>
<li class="menu-home"><a href="https://www.123freesolitaire.com/online" target="_blank">Play Online</a></li>
<li class="menu-home"><a href="https://www.treecardgames.com/wn/news.htm" target="_blank">What's New</a></li>
</ul></nav>
</div>
<div align="center">
<div class="cookie-consent-web-apps-full-width" id="cookie-consent" style="display:none;">
<span class="cookie-consent-message">This website uses cookies to ensure you get the best experience on our website.
                     
                    <a href="privacy.htm" target="_blank">Learn More</a>
</span>
<a class="cookie-consent-button" href="javascript:cookieConsentClick()">Got it!</a>
</div>
</div>
</header>
<div class="style-switcher">
<div class="style-container">
<input class="current-color form-input"/> <br/>
</div>
</div>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" data-title="Sidebar menu" id="cbp-spmenu-s2"></nav>
<section class="home-slider">
<div class="carousel slide" data-interval="3000" data-pause="hover" data-ride="carousel" data-wrap="true" id="home-slider">
<div class="carousel-inner">
<div class="item active">
<div class="container">
<h3>123 Free Solitaire</h3>
<h4>The Free Solitaire Enjoyed by Millions of Players!</h4>
<!--<div class="description">700+ World's best solitaire games!</div> -->
<p class="btn-margin"><a class="btn btn-default btn-lg" href="download.htm">Try it Now for Free!</a><br/></p>
</div>
<div class="image">
<a href="download.htm" target="_blank"> <img alt="" class="attachment-large wp-post-image" src="img/home/123_free_solitaire.jpg"/> </a>
</div>
</div>
<div class="item">
<div class="container">
<h3>Available in the Microsoft Store</h3>
<h4>for your Windows 10 computer and tablet</h4>
<a href="https://www.microsoft.com/store/productId/9WZDNCRDNB1Z" target="_blank"> <img alt="" class="attachment-large wp-post-image" src="img/badges/get_it_win_store.png" style="margin-top: 10px;"/> </a>
</div>
<div class="image" style="margin-top: -50px; margin-bottom: 10px">
<a href="https://www.microsoft.com/store/productId/9WZDNCRDNB1Z" target="_blank"> <img alt="" class="attachment-large wp-post-image" src="img/home/solitaire_windows_app.jpg"/> </a>
</div>
</div>
<div class="item">
<div class="container">
<h3>Play 123 Free Solitaire in your browser</h3>
<a href="https://www.123freesolitaire.com/online" target="_blank"> <img alt="" class="attachment-large wp-post-image" src="img/badges/browsers-badge-large.png" style="margin-top: 20px; margin-bottom: 10px" width="280"/> </a>
</div>
<div class="image">
<a href="https://www.123freesolitaire.com/online" target="_blank"> <img alt="" class="attachment-large wp-post-image" src="img/home/solitaire_online.jpg"/> </a>
</div>
</div>
</div>
<a class="slider-control left" data-slide="prev" href="#home-slider"></a>
<a class="slider-control right" data-slide="next" href="#home-slider"></a>
</div>
</section>
<!-- templates / home-client -->
<section class="wrap container">
<br/>
<br/>
<br/>
<br/>
<h1 class="text-center upper">12 Games included <span><img src="img/exclamation_mark.png" style="margin-top: -35px"/></span></h1>
<div class="container">
<div class="row text-center sized">
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows">
<h2><span><img src="img/suit_hearts.png"/></span>  <a href="games/diplomat.htm">Diplomat</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store">
<h2><span><img src="img/suit_spades.png"/></span>  <a href="games/flower_garden.htm">Flower Garden</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Browser">
<h2><span><img src="img/suit_diamonds.png"/></span>  <a href="games/forty_thieves.htm">Forty Thieves</a></h2>
</div>
</div>
</div>
<div class="row text-center sized">
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows">
<h2><span><img src="img/suit_clubs.png"/></span>  <a href="games/freecell.htm">FreeCell</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store">
<h2><span><img src="img/suit_hearts.png"/></span>  <a href="games/golf_a-k.htm">Golf A-K</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Browser">
<h2><span><img src="img/suit_spades.png"/></span>  <a href="games/klondike.htm">Klondike</a></h2>
</div>
</div>
</div>
<div class="row text-center sized">
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows">
<h2><span><img src="img/suit_diamonds.png"/></span>  <a href="games/klondike_by_threes.htm">Klondike by Threes</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store">
<h2><span><img src="img/suit_clubs.png"/></span>  <a href="games/pyramid.htm">Pyramid</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Browser">
<h2><span><img src="img/suit_hearts.png"/></span>  <a href="games/spider_solitaire.htm">Spider</a></h2>
</div>
</div>
</div>
<div class="row text-center sized">
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows">
<h2><span><img src="img/suit_spades.png"/></span>  <a href="games/spider_solitaire_one_suit.htm">Spider One Suit</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store">
<h2><span><img src="img/suit_diamonds.png"/></span>  <a href="games/spider_solitaire_two_suits.htm">Spider Two Suits</a></h2>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Browser">
<h2><span><img src="img/suit_clubs.png"/></span>  <a href="games/yukon.htm">Yukon</a></h2>
</div>
</div>
</div>
</div>
<br/>
<br/>
<br/>
<br/>
</section>
<!-- /.wrap -->
<!-- templates / home-testimonial -->
<section class="fullscr-testimonial">
<div class="owl-carousel owl-demo carousel slide" id="testimonial-slider">
<div class="item">
<blockquote class="container">
<p style="font-size:26px">Play <b>123 Free Solitaire</b> now and bring the <b>excitement</b> of card solitaire gaming right to your personal computer!<br/></p>
</blockquote>
</div>
<div class="item">
<blockquote class="container">
<p style="font-size:26px">More than <b>9 trillion (9,999,999,999,999)</b> possible games (shuffles) to play for each Solitaire, so the game remains fresh no matter how many times you play.</p><br/>
</blockquote>
</div>
<div class="item">
<blockquote class="container">
<p style="font-size:26px">Customizable with many beautiful <b>Card Set</b>, <b>Card Backs</b> and <b>Backgrounds</b> to choose from.</p><br/>
</blockquote>
</div>
<div class="item">
<blockquote class="container">
<p style="font-size:26px"><b>Autoplay</b> function to <b>automatically</b> play any card to the foundations during the game and at the end of the game.</p><br/>
</blockquote>
</div>
<div class="item">
<blockquote class="container">
<p style="font-size:26px">One level of <b>Undo</b> and <b>Redo</b> feature, to reverse the last move or to "take back" any card moved.</p><br/>
</blockquote>
</div>
<div class="item">
<blockquote class="container">
<p style="font-size:26px">Downloaded more than <b>21,800,000</b> times worldwide and counting!</p><br/>
</blockquote>
</div>
</div>
<div class="slider-control">
<a class="left" data-slide="prev" href="#testimonial-slider"><i class="klico-page-arrow-left"></i></a>
<a class="right" data-slide="next" href="#testimonial-slider"><i class="klico-page-arrow-right"></i></a>
</div>
</section>
<!-- / templates / home-testimonial -->
<!-- templates / home-client -->
<section class="wrap container">
<br/>
<br/>
<br/>
<h2 class="text-center upper">Screenshots</h2>
<div class="container">
<div class="row text-center sized">
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Windows"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide1.jpg" title="123 Free Solitaire - Select a Solitaire" width="300px"/>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide2.jpg" title="123 Free Solitaire - FreeCell Solitaire" width="300px"/>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Browser"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide3.jpg" title="123 Free Solitaire - Klondike by Threes Solitaire" width="300px"/>
</div>
</div>
</div>
<br/>
<br/>
<div class="row text-center sized">
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Windows"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide4.jpg" title="123 Free Solitaire - Spider Solitaire" width="300px"/>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Windows 10 Store"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide5.jpg" title="123 Free Solitaire - Select a Card Back" width="300px"/>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 sizer-item">
<div class="bw-wrapper" title="Browser"><img class="attachment-212x100 wp-post-image" src="img/screenshots/solitaire-features-slide6.jpg" title="123 Free Solitaire - Spider One Suit Solitaire" width="300px"/>
</div>
</div>
</div>
</div>
<br/>
</section>
<!-- templates / home-client -->
<section class="wrap container">
<br/>
<br/>
<br/>
<h2 class="text-center upper">Available on these Platforms</h2>
<div class="container">
<div class="row text-center sized">
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Windows">
<a href="download.htm"> <img alt="" class="attachment-212x100 wp-post-image" src="img/badges/badge_windows.jpg"/></a>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Microsoft Store">
<a href="https://www.microsoft.com/store/productId/9WZDNCRDNB1Z" target="_blank"> <img alt="" class="attachment-212x100 wp-post-image" src="img/badges/get_it_win_store.png"/></a>
</div>
</div>
<div class="col-md-4 col-sm-4 col-xs-10 sizer-item">
<div class="bw-wrapper" title="Browser">
<a href="https://www.123freesolitaire.com/online" target="_blank"> <img alt="" class="attachment-212x100 wp-post-image" src="img/badges/browsers-badge.jpg"/></a>
</div>
</div>
</div>
</div>
<br/>
</section>
<section>
<div align="center">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 123freesolitaire.com - 728x90 horizontal - homepage -->
<ins class="adsbygoogle" data-ad-client="ca-pub-5240746718396645" data-ad-slot="6255139792" style="display:inline-block;width:728px;height:90px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<br/>
</section>
<!-- Footer -->
<footer class="content-info" role="contentinfo">
<div class="container">
<div class="row widgets">
<div class="col-md-3 col-sm-6">
<section class="widget text-2 widget_text">
<h3>About us</h3>
<div class="textwidget"><img alt="" src="/img/logo/tcg-footer.png" width="220"/>TreeCardGames develops exclusive games with your fun in mind!
                     <br/>
<a href="https://www.treecardgames.com/win_free/support/faq.htm" target="_blank"><strong>Support</strong></a>
<br/>
<a href="/newsl.htm"><strong>Free Newsletter</strong></a>
<br/>
<a href="/privacy.htm"><strong>Privacy Policy</strong></a>
</div>
</section>
</div>
<div class="col-md-3 col-sm-6">
<section class="widget tag_cloud-2 widget_tag_cloud">
<h3>Play Solitaire Online</h3>
<div class="tagcloud">
<a class="btn btn-light tag" href="https://www.free-spider-solitaire.com" target="_blank">Spider Solitaire</a>
<a class="btn btn-light tag" href="https://www.free-freecell-solitaire.com" target="_blank">FreeCell Solitaire</a>
<a class="btn btn-light tag" href="https://www.solitaire-klondike.com" target="_blank">Klondike Solitaire</a>
<a class="btn btn-light tag" href="https://www.treecardgames.com/games_windows.htm" target="_blank">More Free Games</a>
</div>
</section>
</div>
<div class="col-md-3 col-sm-6">
<section class="widget tag_cloud-2 widget_tag_cloud">
<h3>Solitaire Games</h3>
<div class="tagcloud">
<a class="btn btn-light tag" href="https://www.solsuite.com" target="_blank">SolSuite Solitaire</a>
<a class="btn btn-light tag" href="/solitaire-games.htm">Solitaire Games</a>
<a class="btn btn-light tag" href="/spider-solitaire.htm">Spider Solitaire</a>
<a class="btn btn-light tag" href="https://www.solsuite.com/freeware.htm" target="_blank">Solitaire Download</a>
</div>
</section>
</div>
<div class="col-md-3 col-sm-6">
<section class="widget social-4 widget_social">
<h3>Keep in touch!</h3>
<b>We are social</b>
<div class="row">
<div class="col-xs-2 text-center"><a href="https://www.facebook.com/123FreeSolitaire" rel="nofollow" target="_blank" title="123 Free Solitaire - Home | Facebook"><img alt="123 Free Solitaire - Home | Facebook" src="/img/social-icon/facebook.png"/></a></div>
<div class="col-xs-2 text-center"><a href="https://treecardgames.blogspot.com" rel="nofollow" target="_blank" title="TreeCardGames Blog"><img alt="TreeCardGames Blog" src="/img/social-icon/blogger.png"/></a></div>
<div class="col-xs-2 text-center"><a href="https://twitter.com/123freesol" rel="nofollow" target="_blank" title="Twitter"><img alt="Twitter" src="/img/social-icon/twitter.png"/></a></div>
<div class="col-xs-2 text-center"><a href="https://www.youtube.com/user/treecardgames" rel="nofollow" target="_blank" title="YouTube"><img alt="YouTube" src="/img/social-icon/youtube.png"/></a></div>
<div class="col-xs-2 text-center"><a href="https://www.pinterest.com/treecardgames" rel="nofollow" target="_blank" title="Pinterest"><img alt="Pinterest" src="/img/social-icon/pinterest.png"/></a></div>
<div class="col-xs-2 text-center"><a href="https://www.instagram.com/treecardgames" rel="nofollow" target="_blank" title="Instagram"><img alt="Instagram" src="/img/social-icon/instagram.png"/></a></div>
<br/>
</div>
</section></div>
<div class="text-center copyright">
               Copyright Â© 1998-2021 <a href="https://www.treecardgames.com"><strong>TreeCardGames</strong></a> | All rights reserved 
            </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="/js/owl.carousel.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/main.js?ver=1.0.4"></script>
</div></footer></body>
</html>