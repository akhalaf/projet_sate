<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]--><!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]--><!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]--><!--[if gt IE 8]><!--><html lang="en">
<!--<![endif]-->
<head>
<title>Jobs offers, Job vacancies in the World | BlackboardJob</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="//cdn.blackboardjob.com/img/tpl/blackboard/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="//cdn.blackboardjob.com/min/css/blackboard/HOME/dd6a4f7ec567e33f2dc534fbd4a7f9ef.css" rel="stylesheet" type="text/css"/>
<style>
			h1 { float: none !important; font-family: 'Pencil', cursive; font-size: 30px !important; line-height: 45px !important; margin: 0 auto !important }
			h1 strong { color: #CCCCCC !important; float: none; margin: 0 auto }
			#pHome { margin: 0 0 55px }
			#pHome #search-box { margin: 20px auto 0 }
			#pHome #logo { float: none; margin: 5% auto 10px; width: 258px }
			#map { border-left: 0; border-right: 0 }
			#canvas { height: 250px }
			#countries { font-family: 'Pencil', cursive; text-align: center }
			#countries .container { padding: 0 !important }
			#countries a,#countries a:visited { color: #CCC !important; font-family: 'Pencil', cursive; font-size: 16px !important; cursor: pointer; padding-right: 10px }
			#countries a.hover, #countries a:focus, #countries a:hover { color: #E89920 !important; text-decoration: none }
			.jqvmap-label { background: #292929; color: white; display: none; font-family: sans-serif, Verdana; font-size: smaller; padding: 3px; pointer-events:none; position: absolute; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px }
			#jqvmap1_ar, #jqvmap1_au, #jqvmap1_at, #jqvmap1_br, #jqvmap1_ca, #jqvmap1_cl, #jqvmap1_co, #jqvmap1_cr, #jqvmap1_do, #jqvmap1_ec, #jqvmap1_fr, #jqvmap1_de, #jqvmap1_in, #jqvmap1_id, #jqvmap1_ie, #jqvmap1_it, #jqvmap1_jp, #jqvmap1_mx, #jqvmap1_pa, #jqvmap1_pe, #jqvmap1_pl, #jqvmap1_pt, #jqvmap1_pr, #jqvmap1_ru, #jqvmap1_za, #jqvmap1_es, #jqvmap1_ch, #jqvmap1_tr, #jqvmap1_ae, #jqvmap1_gb, #jqvmap1_us, #jqvmap1_uy, #jqvmap1_ve { cursor: pointer }
			@media (min-width: 768px) {
				h2 { padding-bottom: 10px }
				#content { padding: 0 }
				#content .container { padding: 0 }
				#canvas { height: 400px }
			}
			@media (min-width: 992px) {
				h2 { border: 0; padding-bottom: 0 }
				#meta { padding: 10px 0 25px }
				#canvas { height: 600px }
			}
		</style>
<!--[if IE]>
			<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
			<meta http-equiv="imagetoolbar" content="no" />
		<![endif]-->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body class="en logged" id="pHome">
<script language="JavaScript" type="text/javascript">
			if( top != self ) top.location.replace( location.href );
		</script>
<div id="container">
<div id="content">
<div class="container text-center" id="search-box">
<div id="logo"><strong>BlackboardJob</strong></div>
<h1 class="text">Jobs offers, Job vacancies <strong>in the World:</strong></h1>
</div>
<div id="countries">
<div class="container">
<a href="//ar.blackboardjob.com" id="country-ar" rel="ar" target="_blank" title="Argentina">Argentina</a>
<a href="//au.blackboardjob.com" id="country-au" rel="au" target="_blank" title="Australia">Australia</a>
<a href="//at.blackboardjob.com" id="country-at" rel="at" target="_blank" title="Austria">Austria</a>
<a href="//br.blackboardjob.com" id="country-br" rel="br" target="_blank" title="Brazil">Brazil</a>
<a href="//ca.blackboardjob.com" id="country-ca" rel="ca" target="_blank" title="Canada">Canada</a>
<a href="//cl.blackboardjob.com" id="country-cl" rel="cl" target="_blank" title="Chile">Chile</a>
<a href="//co.blackboardjob.com" id="country-co" rel="co" target="_blank" title="Colombia">Colombia</a>
<a href="//cr.blackboardjob.com" id="country-cr" rel="cr" target="_blank" title="Costa Rica">Costa Rica</a>
<a href="//do.blackboardjob.com" id="country-do" rel="do" target="_blank" title="Dominican Republic">Dominican Republic</a>
<a href="//ec.blackboardjob.com" id="country-ec" rel="ec" target="_blank" title="Ecuador">Ecuador</a>
<a href="//fr.blackboardjob.com" id="country-fr" rel="fr" target="_blank" title="France">France</a>
<a href="//de.blackboardjob.com" id="country-de" rel="de" target="_blank" title="Germany">Germany</a>
<a href="//in.blackboardjob.com" id="country-in" rel="in" target="_blank" title="India">India</a>
<a href="//id.blackboardjob.com" id="country-id" rel="id" target="_blank" title="Indonesia">Indonesia</a>
<a href="//ie.blackboardjob.com" id="country-ie" rel="ie" target="_blank" title="Ireland">Ireland</a>
<a href="//it.blackboardjob.com" id="country-it" rel="it" target="_blank" title="Italy">Italy</a>
<a href="//jp.blackboardjob.com" id="country-jp" rel="jp" target="_blank" title="Japan">Japan</a>
<a href="//mx.blackboardjob.com" id="country-mx" rel="mx" target="_blank" title="Mexico">Mexico</a>
<a href="//pa.blackboardjob.com" id="country-pa" rel="pa" target="_blank" title="Panama">Panama</a>
<a href="//pe.blackboardjob.com" id="country-pe" rel="pe" target="_blank" title="Peru">Peru</a>
<a href="//pl.blackboardjob.com" id="country-pl" rel="pl" target="_blank" title="Poland">Poland</a>
<a href="//pt.blackboardjob.com" id="country-pt" rel="pt" target="_blank" title="Portogallo">Portogallo</a>
<a href="//pr.blackboardjob.com" id="country-pr" rel="pr" target="_blank" title="Puerto Rico">Puerto Rico</a>
<a href="//ru.blackboardjob.com" id="country-ru" rel="ru" target="_blank" title="Russia">Russia</a>
<a href="//za.blackboardjob.com" id="country-za" rel="za" target="_blank" title="South Africa">South Africa</a>
<a href="//es.blackboardjob.com" id="country-es" rel="es" target="_blank" title="Spain">Spain</a>
<a href="//ch.blackboardjob.com" id="country-ch" rel="ch" target="_blank" title="Svizzera">Svizzera</a>
<a href="//tr.blackboardjob.com" id="country-tr" rel="tr" target="_blank" title="Turkey">Turkey</a>
<a href="//ae.blackboardjob.com" id="country-ae" rel="ae" target="_blank" title="United Arab Emirates">United Arab Emirates</a>
<a href="//uk.blackboardjob.com" id="country-gb" rel="gb" target="_blank" title="United Kingdom">United Kingdom</a>
<a href="//us.blackboardjob.com" id="country-us" rel="us" target="_blank" title="United States">United States</a>
<a href="//uy.blackboardjob.com" id="country-uy" rel="uy" target="_blank" title="Uruguay">Uruguay</a>
<a href="//ve.blackboardjob.com" id="country-ve" rel="ve" target="_blank" title="Venezuela">Venezuela</a>
<br clear="all"/>
</div>
</div>
</div>
<div id="map" style="">
<div class="container" id="canvas"></div>
</div>
</div>
<footer class="footer hide-in-page" id="footer">
<div class="container">
<span class="info text11">
					BlackBoardJob © 2015 - 2021				</span>
</div>
<!-- Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-uXdbknBN59ZVR"
});
</script>
<noscript>
<div style="display:none;">
<img alt="Quantcast" border="0" height="1" src="//pixel.quantserve.com/pixel/p-uXdbknBN59ZVR.gif" width="1"/>
</div>
</noscript>
<!-- End Quantcast tag -->
</footer>
<script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery.vmap.js?v=1.0" type="text/javascript"></script>
<script src="/assets/js/maps/jquery.vmap.world.js?v=1.0" type="text/javascript"></script>
<script src="/assets/js/maps/jquery.vmap.sampledata.js?v=1.0" type="text/javascript"></script>
<script language="javascript">
			$(document).ready(function () {
				var activeCountry = false;
				var activeCountries = new Array( 'ar', 'au', 'at', 'br', 'ca', 'cl', 'co', 'cr', 'do', 'ec', 'fr', 'de', 'in', 'id', 'ie', 'it', 'jp', 'mx', 'pa', 'pe', 'pl', 'pt', 'pr', 'ru', 'za', 'es', 'ch', 'tr', 'ae', 'gb', 'us', 'uy', 've' );	
				jQuery('#canvas').vectorMap({
					map: 'world_en',
					backgroundColor: false,
					borderOpacity: 0.01,
					borderWidth: 2,
					borderColor: '#F7F7F7',
					color: '#4e4e4e',
					colors: { 'ar': '#CCCCCC', 'au': '#CCCCCC', 'at': '#CCCCCC', 'br': '#CCCCCC', 'ca': '#CCCCCC', 'cl': '#CCCCCC', 'co': '#CCCCCC', 'cr': '#CCCCCC', 'do': '#CCCCCC', 'ec': '#CCCCCC', 'fr': '#CCCCCC', 'de': '#CCCCCC', 'in': '#CCCCCC', 'id': '#CCCCCC', 'ie': '#CCCCCC', 'it': '#CCCCCC', 'jp': '#CCCCCC', 'mx': '#CCCCCC', 'pa': '#CCCCCC', 'pe': '#CCCCCC', 'pl': '#CCCCCC', 'pt': '#CCCCCC', 'pr': '#CCCCCC', 'ru': '#CCCCCC', 'za': '#CCCCCC', 'es': '#CCCCCC', 'ch': '#CCCCCC', 'tr': '#CCCCCC', 'ae': '#CCCCCC', 'gb': '#CCCCCC', 'us': '#CCCCCC', 'uy': '#CCCCCC', 've': '#CCCCCC' },
					enableZoom: true,
					hoverColor: '#E89920',
					hoverOpacity: null,
					normalizeFunction: 'linear',
					selectedColor: '#E89920',
					showTooltip: true,
					onRegionOver: function (event, code, region) {
						$( '#countries a').removeClass('hover');
						if( activeCountries.indexOf(code) !== -1 )
							$( '#country-' + code ).addClass('hover');
						else
							event.preventDefault();
						return true;
					},
					onRegionClick: function (element, code, region) {
						if( activeCountries.indexOf(code) > -1 )
							$( '#country-' + code )[0].click();
						else
							element.preventDefault();
					},
					onLabelShow: function (event, label, code)
					{
						if( activeCountries.indexOf(code) === -1 )
							event.preventDefault();
					}
				});
			
				$("#countries a").hover(
					function () {
						$( '#countries a').removeClass('hover');
						if( activeCountry )
							$('#canvas').vectorMap('deselect', activeCountry);
						  
						activeCountry = $(this).attr("rel");
						$('#canvas').vectorMap('select', $(this).attr("rel"));
					},
					function () {
						if( activeCountry )
						{
							$( '#countries a').removeClass('hover');
							$('#canvas').vectorMap('deselect', activeCountry);
							activeCountry = false;
						}
					}
				);
			});
		</script>
</body>
</html>
