<!DOCTYPE html>
<html dir="rtl" lang="he-IL">
<head>
<meta charset="utf-8"/>
<title>
העמוד לא נמצא | B.BCM	</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://bbcm.co.il/wp-content/themes/twentyten/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bbcm.co.il/xmlrpc.php" rel="pingback"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bbcm.co.il/feed/" rel="alternate" title="B.BCM « פיד‏" type="application/rss+xml"/>
<link href="https://bbcm.co.il/comments/feed/" rel="alternate" title="B.BCM « פיד תגובות‏" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/bbcm.co.il\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://bbcm.co.il/wp-includes/css/dist/block-library/style-rtl.min.css?ver=5.3.6" id="wp-block-library-rtl-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bbcm.co.il/wp-includes/css/dist/block-library/theme-rtl.min.css?ver=5.3.6" id="wp-block-library-theme-rtl-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bbcm.co.il/wp-content/plugins/contact-form-plugin/css/form_style.css?ver=4.1.8" id="cntctfrm_form_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bbcm.co.il/wp-content/plugins/custom-search-plugin/css/style.css?ver=5.3.6" id="cstmsrch_stylesheet-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bbcm.co.il/wp-content/themes/twentyten/blocks.css?ver=20181018" id="twentyten-block-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://bbcm.co.il/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://bbcm.co.il/wp-content/plugins/custom-search-plugin/js/script.js?ver=5.3.6" type="text/javascript"></script>
<link href="https://bbcm.co.il/wp-json/" rel="https://api.w.org/"/>
<link href="https://bbcm.co.il/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bbcm.co.il/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="https://bbcm.co.il/wp-content/themes/twentyten/rtl.css" media="screen" rel="stylesheet" type="text/css"/><meta content="WordPress 5.3.6" name="generator"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #3f3f3f; background-image: url("https://bbcm.co.il/wp-content/uploads/2013/07/background_bbcm-11.jpg"); background-position: center top; background-size: auto; background-repeat: repeat; background-attachment: fixed; }
</style>
</head>
<body class="rtl error404 custom-background">
<div class="hfeed" id="wrapper">
<div id="header">
<div id="masthead">
<div id="branding" role="banner">
<div id="site-title">
<span>
<a href="https://bbcm.co.il/" rel="home" title="B.BCM">B.BCM</a>
</span>
</div>
<div id="site-description">יעוץ, ליווי ניהול המשכיות עסקית</div>
<img alt="" height="198" src="https://bbcm.co.il/wp-content/uploads/2013/04/cropped-header1.jpg" width="940"/>
</div><!-- #branding -->
<div id="access" role="navigation">
<div class="skip-link screen-reader-text"><a href="#content" title="לדלג לתוכן">לדלג לתוכן</a></div>
<div class="menu-header"><ul class="menu" id="menu-%d7%a2%d7%9c%d7%99%d7%95%d7%9f"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-29" id="menu-item-29"><a href="https://bbcm.co.il/">אודותינו</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-27" id="menu-item-27"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/">המשכיות עסקית</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89" id="menu-item-89"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa-%d7%9e%d7%94%d7%99/">מהי המשכיות עסקית</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-28" id="menu-item-28"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/bcm-%d7%a0%d7%99%d7%94%d7%95%d7%9c-%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/">BCM – ניהול המשכיות עסקית</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53" id="menu-item-53"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%94%d7%9b%d7%a8%d7%aa-%d7%94%d7%90%d7%a8%d7%92%d7%95%d7%9f/">הכרת הארגון</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59" id="menu-item-59"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%92%d7%99%d7%91%d7%95%d7%a9-%d7%90%d7%a1%d7%98%d7%a8%d7%98%d7%92%d7%99%d7%94/">גיבוש אסטרטגיה</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64" id="menu-item-64"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%94%d7%9b%d7%a0%d7%aa-%d7%94%d7%aa%d7%95%d7%9b%d7%a0%d7%99%d7%95%d7%aa/">הכנת התוכניות</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-73" id="menu-item-73"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%91%d7%97%d7%99%d7%a0%d7%94-%d7%95%d7%91%d7%93%d7%99%d7%a7%d7%94/">בחינה ובדיקה</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77" id="menu-item-77"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%a0%d7%99%d7%94%d7%95%d7%9c-%d7%94%d7%9e%d7%a2%d7%a8%d7%9b%d7%aa-bcm/">ניהול המערכת – BCM</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82" id="menu-item-82"><a href="https://bbcm.co.il/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa/%d7%94%d7%98%d7%9e%d7%a2%d7%94-%d7%91%d7%aa%d7%a8%d7%91%d7%95%d7%aa-%d7%94%d7%90%d7%a8%d7%92%d7%95%d7%a0%d7%99%d7%aa/">הטמעה בתרבות הארגונית</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-25" id="menu-item-25"><a href="https://bbcm.co.il/%d7%a0%d7%99%d7%94%d7%95%d7%9c-%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98%d7%99%d7%9d/">ניהול פרוייקטים</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-26" id="menu-item-26"><a href="https://bbcm.co.il/%d7%a0%d7%99%d7%94%d7%95%d7%9c-%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98%d7%99%d7%9d/%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98-%d7%9e%d7%94%d7%95/">פרוייקט מהו</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135" id="menu-item-135"><a href="https://bbcm.co.il/%d7%a0%d7%99%d7%94%d7%95%d7%9c-%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98%d7%99%d7%9d/%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98-%d7%9e%d7%94%d7%95/%d7%a9%d7%9c%d7%91%d7%99-%d7%94%d7%a4%d7%a8%d7%95%d7%99%d7%99%d7%a7%d7%98/">שלבי הפרוייקט</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" id="menu-item-24"><a href="https://bbcm.co.il/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/">צור קשר</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-229" id="menu-item-229"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/">מאמרים</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291" id="menu-item-291"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa-%d7%9c%d7%90-%d7%a6%d7%a8%d7%99%d7%9a-%d7%99%d7%a9-%d7%9c%d7%99-drp/">המשכיות עסקית? לא צריך, יש לי DRP!</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-288" id="menu-item-288"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa-%d7%91%d7%a7%d7%9c%d7%99%d7%a4%d7%aa-%d7%90%d7%92%d7%95%d7%96-2/">המשכיות עסקית בקליפת אגוז</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289" id="menu-item-289"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%94%d7%99%d7%a6%d7%99%d7%a8%d7%94-%d7%94%d7%a1%d7%99%d7%9e%d7%a4%d7%95%d7%a0%d7%99%d7%aa-%d7%a9%d7%a0%d7%a7%d7%a8%d7%90%d7%aa-%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99/">היצירה הסימפונית שנקראת "המשכיות עסקית"</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290" id="menu-item-290"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%9e%d7%94-%d7%91%d7%99%d7%9f-%d7%94%d7%a8%d7%97%d7%91%d7%aa-%d7%94%d7%9e%d7%95%d7%a0%d7%97-%d7%91%d7%a2%d7%9c%d7%99-%d7%a2%d7%a0%d7%99%d7%99%d7%9f-%d7%9c%d7%90%d7%97%d7%a8%d7%99%d7%95%d7%aa/">מה בין הרחבת המונח "בעלי עניין" לאחריות חברתית?</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287" id="menu-item-287"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa-%d7%90%d7%95-%d7%94%d7%a7%d7%a9%d7%a8-%d7%91%d7%99%d7%9f-%d7%a7%d7%a6%d7%a8-%d7%97%d7%a9%d7%9e%d7%9c%d7%99-%d7%91%d7%a8/">המשכיות עסקית – או הקשר בין קצר חשמלי ברומניה ל"טור דה פראנס"</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-230" id="menu-item-230"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%9b%d7%9e%d7%94-%d7%9e%d7%97%d7%a9%d7%91%d7%95%d7%aa-%d7%a2%d7%9c-%d7%97%d7%95%d7%a1%d7%9f-%d7%90%d7%a8%d7%92%d7%95%d7%a0%d7%99-%d7%91%d7%a2%d7%9c%d7%99-%d7%a2%d7%a0%d7%99%d7%99%d7%9f-%d7%95%d7%94/">כמה מחשבות על חוסן ארגוני, בעלי עניין והמשכיות עסקית</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-231" id="menu-item-231"><a href="https://bbcm.co.il/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/%d7%94%d7%9e%d7%a9%d7%9b%d7%99%d7%95%d7%aa-%d7%a2%d7%a1%d7%a7%d7%99%d7%aa-%d7%91%d7%a7%d7%9c%d7%99%d7%a4%d7%aa-%d7%90%d7%92%d7%95%d7%96/">המשכיות עסקית בקליפת אגוז ובהרחבה</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-93" id="menu-item-93"><a href="https://bbcm.co.il/%d7%91%d7%9c%d7%95%d7%92/">בלוג</a></li>
</ul></div> </div><!-- #access -->
</div><!-- #masthead -->
</div><!-- #header -->
<div id="main">
<div id="container">
<div id="content" role="main">
<div class="post error404 not-found" id="post-0">
<h1 class="entry-title">לא נמצא</h1>
<div class="entry-content">
<p>ביקשת לראות משהו שלא קיים פה. אם רצית פוסט מסויים, אפשר לנסות לחפש אותו.</p>
<form action="https://bbcm.co.il/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">חיפוש:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="חיפוש"/>
</div>
</form> </div><!-- .entry-content -->
</div><!-- #post-0 -->
</div><!-- #content -->
</div><!-- #container -->
<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>
</div><!-- #main -->
<div id="footer" role="contentinfo">
<div id="colophon">
<div id="site-info">
<a href="https://bbcm.co.il/" rel="home" title="B.BCM">
					B.BCM				</a>
</div><!-- #site-info -->
<div id="site-generator">
<a class="imprint" href="https://he.wordpress.org/" title="מערכת אישית וחופשית לניהול אתרים עצמאיים">
					פועל על WordPress.				</a>
</div><!-- #site-generator -->
</div><!-- #colophon -->
</div><!-- #footer -->
</div><!-- #wrapper -->
<script src="https://bbcm.co.il/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
</body>
</html>
