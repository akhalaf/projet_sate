<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Marianna Venczak Original signed watercolor painting</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<metaname content="painter;original;signed;painting;art;contemporary_art;venczak;watercolor;hungarian_art;energologart;hunagram;szeretet_egység;mandala;eredeti;kortárs;akvarell;festmények;Venczák;Marianna;magyar;">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<style type="text/css">
="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<style type="text/css">
<!--
body,td,th {
	font-family: Georgia, Times New Roman, Times, serif;
	font-size: 14px;
	color: #999999;
	font-style: italic;
}
a {
	font-family: Georgia, Times New Roman, Times, serif;
	font-size: 14px;
	color: #F6F3EA;
}
a:link {
	text-decoration: none;
	color: #999999;
}
a:visited {
	text-decoration: none;
	color: #999933;
}
a:hover {
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
.style4 {font-size: 18px; color: #996600; font-weight: bold; }
.style7 {
	font-size: 16px;
	font-style: italic;
}
.style10 {
	font-size: 16px;
	color: #999999;
	font-style: italic;
}
.style11 {font-size: 14px}
.style12 {font-size: 14px; color: #999999; }
.style13 {
	color: #999999;
	font-weight: bold;
	font-style: italic;
}
.style15 {font-size: 14px; color: #999999; font-weight: bold; }
.style18 {
	font-size: 18px;
	color: #CC9900;
	font-weight: bold;
	font-style: italic;
}
.style19 {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-weight: bold;
	font-size: 14px;
	color: #999999;
	font-style: italic;
}
body,td,th {
	font-family: Georgia, Times New Roman, Times, serif;
	font-size: 14px;
	color: #999999;
}
.style21 {font-size: 14px; color: #999999; font-weight: bold; font-style: italic; }
-->
</style>
</metaname></head>
<body>
<div id="Layer1" style="position:absolute; width:200px; height:115px; z-index:1; left: 0; top: 0;">
<div id="Layer9" style="position:absolute; width:185px; height:63px; z-index:2; left: 26px; top: 933px;"><a href="http://www.fanartreview.com" target="_blank"><img border="0" height="85" src="fanart.JPG" width="241"/></a></div>
<div id="Layer8" style="position:absolute; width:365px; height:80px; z-index:3; left: 298px; top: 439px;"><img height="65" src="final_338792520.gif" width="321"/></div>
<div id="Layer3" style="position:absolute; width:341px; height:19px; z-index:5; left: 1524px; top: 349px;"><strong><span class="style18"><a href="http://www.venczakmarianna.com/hunversion.htm" target="_blank">Hungarian version </a></span></strong></div>
<img height="526" src="fejleceng.JPG" width="1919"/>
</div>
<div id="Layer5" style="position:absolute; width:200px; height:35px; z-index:28; left: 26px; top: 770px;"><img height="44" src="button7.JPG" width="242"/></div>
<div id="Layer12" style="position:absolute; width:241px; height:94px; z-index:11; left: 26px; top: 1382px;"><a href="http://www.maskepplatok.hu" target="_blank"><img border="0" height="90" src="maskepplatok.jpg" width="241"/></a></div>
<div id="Layer13" style="position:absolute; width:200px; height:115px; z-index:12; left: 26px; top: 1022px;"><img height="112" src="michelangeloprize.jpg" width="241"/>
<div id="Layer14" style="position:absolute; width:241px; height:80px; z-index:1; top: 276px; left: 0px;"><a href="http://www.mybestcanvas.com" target="_blank"><img border="0" height="77" src="mybestcanvas.gif" width="241"/></a></div>
<div id="Layer15" style="position:absolute; width:200px; height:68px; z-index:2; left: -1145px; top: 146px;"><img height="68" src="absoluteartslogo.gif" width="239"/></div>
</div>
<div id="Layer18" style="position:absolute; width:51px; height:49px; z-index:33; left: 25px; top: 873px;"><a href="http://www.facebook.com" target="_blank"><img border="0" height="54" src="facebook.jpg" width="56"/></a></div>
<div id="Layer17" style="position:absolute; width:240px; height:49px; z-index:13; left: 26px; top: 1140px;"><a href="http://www.paintingsilove.com" target="_blank"><img border="0" height="49" src="paintingslove.jpg" width="241"/></a></div>
<div id="Layer19" style="position:absolute; width:200px; height:100px; z-index:14; left: 26px; top: 1194px;"><a href="http://www.saatchigallery.com" target="_blank"><img border="0" height="97" src="saatchigallery.jpg" width="241"/></a></div>
<div id="Layer21" style="position:absolute; width:243px; height:45px; z-index:16; left: 26px; top: 528px;"><img height="44" src="button7.JPG" width="242"/></div>
<div class="style4" id="Layer2" style="position:absolute; width:230px; height:27px; z-index:17; left: 30px; top: 538px;">
<div align="center" class="style7 style12"><a href="bioeng.htm" target="_blank">Bio</a></div>
</div>
<div id="Layer7" style="position:absolute; width:200px; height:45px; z-index:18; left: 26px; top: 576px;"> <img height="44" src="button7.JPG" width="242"/></div>
<div class="style4" id="Layer22" style="position:absolute; width:230px; height:27px; z-index:19; left: 30px; top: 583px;">
<div align="center" class="style12"><em><a href="classiceng.htm" target="_blank">Classical Art </a></em></div>
</div>
<div id="Layer23" style="position:absolute; width:200px; height:44px; z-index:20; left: 26px; top: 625px;"><img height="44" src="button7.JPG" width="242"/></div>
<div class="style4" id="Layer24" style="position:absolute; width:233px; height:17px; z-index:21; left: 29px; top: 633px;">
<div align="center" class="style12"><em><a href="illusteng.htm" target="_blank">Illustration</a></em></div>
</div>
<div id="Layer25" style="position:absolute; width:200px; height:42px; z-index:22; left: 26px; top: 672px;"><img height="44" src="button7.JPG" width="242"/></div>
<div class="style4" id="Layer26" style="position:absolute; width:233px; height:28px; z-index:23; left: 29px; top: 670px;">
<div align="center">
<p class="style10 style11"><a href="http://www.hunagram.hu/englishversion.htm" target="_blank">Energologart: HUNAGRAM </a></p>
</div>
</div>
<div id="Layer27" style="position:absolute; width:200px; height:37px; z-index:26; left: 26px; top: 721px;"><img height="44" src="button7.JPG" width="242"/></div>
<div id="Layer28" style="position:absolute; width:232px; height:23px; z-index:27; left: 29px; top: 730px;">
<div align="center" class="style19"><a href="kritikakeng.htm" target="_blank">What Others say </a></div>
</div>
<div id="Layer6" style="position:absolute; width:231px; height:20px; z-index:29; left: 30px; top: 781px;">
<div align="center"><em><strong><a href="contacteng.htm" target="_blank">Contact</a></strong></em></div>
</div>
<div id="Layer20" style="position:absolute; width:59px; height:56px; z-index:30; left: 208px; top: 874px;"><a href="http://www.redbubble.com/people/mariannv" target="_blank"><img border="0" height="54" src="redbubble.jpg" width="57"/></a></div>
<div id="Layer29" style="position:absolute; width:52px; height:55px; z-index:31; left: 151px; top: 874px;"><a href="http://www.linkedin.com" target="_blank"><img border="0" height="54" src="linkedin_logo.jpg" width="56"/></a></div>
<div id="Layer30" style="position:absolute; width:59px; height:50px; z-index:32; left: 88px; top: 876px;"><a href="http://image-poesie.over-blog.com/" target="_blank"><img border="0" height="48" src="logo-blu.-piccolo-1.jpg" width="57"/></a></div>
<div id="Layer10" style="position:absolute; width:205px; height:183px; z-index:34; left: 298px; top: 562px;">
<div align="justify"><em><strong>2016 December: selected into the 100 international artists in Inspiration: International Art Book, 2017 Edition (published in 50.000 copies for galleries and art collectors globally) 


 


 Appearance: in the summer of 2017.</strong></em></div>
</div>
<div id="Layer11" style="position:absolute; width:200px; height:121px; z-index:35; left: 526px; top: 561px;"><img height="352" src="book.jpg" width="271"/></div>
<div id="Layer16" style="position:absolute; width:200px; height:223px; z-index:36; left: 850px; top: 562px;"><img height="276" src="hunagramuj2netre.jpg" width="261"/></div>
<div id="Layer31" style="position:absolute; width:407px; height:167px; z-index:37; left: 1124px; top: 558px;">
<p align="left" class="style11 style13">It has been created the HUNAGRAM (R) Love-Unity Mandala </p>
<p align="left"><span class="style21">See more: <a href="http://www.hunagram.hu" target="_blank">http://www.hunagram.hu</a></span></p>
<div id="Layer32" style="position:absolute; width:198px; height:117px; z-index:38; left: -818px; top: 394px; font-weight: bold;">
<div align="justify"><em>Awareness has been digital exhibited in Louvre Paris on july 13th 2015 as part of the Art Photography Collection by SeeMe </em></div>
</div>
<p align="left"><span class="style15"><a href="http://www.hunagram.hu" target="_blank"> </a></span></p>
</div>
<div id="Layer33" style="position:absolute; width:200px; height:318px; z-index:38; left: 526px; top: 956px;"><img height="370" src="ebredesnet.JPG" width="274"/></div>
<div id="Layer34" style="position:absolute; width:655px; height:263px; z-index:39; left: 850px; top: 959px; font-size: 14px; font-weight: bold;">
<div align="justify"><em>Appear on the Wikigallery.org site. This the Largest Gallery in the World. See more: <a href="http://www.1st-art-gallery.com" target="_blank">http://www.1st-art-gallery.com </a> </em></div>
</div>
<div id="Layer4" style="position:absolute; width:200px; height:48px; z-index:40; left: 26px; top: 822px;"><a href="http://www.absolutearts.com" target="_blank"><img border="0" height="36" src="absoluteartslogo.jpg" width="242"/></a></div>
<div id="Layer35" style="position:absolute; width:200px; height:55px; z-index:41; left: 26px; top: 1481px;"><a href="http://www.artwanted.com" target="_blank"><img border="0" height="50" src="artwanted.jpg" width="243"/></a></div>
</body>
</html>
