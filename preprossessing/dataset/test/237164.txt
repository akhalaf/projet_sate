<!DOCTYPE html>
<html><head>
<meta charset="utf-8"/>
<meta content="757a27f3fae2e117" name="yandex-verification"/>
<meta content="1xi7xoW2Mx4A7Nqb5WstNnC18pK4gBj1PVWbnWJDr2o" name="google-site-verification"/>
<title>Уроки по Blender / Видеоуроки по Blender / Blender 3D</title>
<link href="https://blender3d.com.ua/wp-content/themes/suppose/style.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/themes/suppose/favicon.png" rel="shortcut icon"/>
<script src="https://apis.google.com/js/plusone.js"></script>
<!-- This site is optimized with the Yoast SEO plugin v15.6.2 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="На сайте Blender3D собрано огромное количество уроков по программе трехмерного моделирования Blender." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://blender3d.com.ua/" rel="canonical"/>
<link href="https://blender3d.com.ua/page/2/" rel="next"/>
<meta content="ru_RU" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Уроки по Blender / Видеоуроки по Blender / Blender 3D" property="og:title"/>
<meta content="На сайте Blender3D собрано огромное количество уроков по программе трехмерного моделирования Blender." property="og:description"/>
<meta content="https://blender3d.com.ua/" property="og:url"/>
<meta content="Blender 3D" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="@blender3dcomua" name="twitter:site"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://blender3d.com.ua/#website","url":"https://blender3d.com.ua/","name":"Blender 3D","description":"","potentialAction":[{"@type":"SearchAction","target":"https://blender3d.com.ua/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"ru-RU"},{"@type":"CollectionPage","@id":"https://blender3d.com.ua/#webpage","url":"https://blender3d.com.ua/","name":"\u0423\u0440\u043e\u043a\u0438 \u043f\u043e Blender / \u0412\u0438\u0434\u0435\u043e\u0443\u0440\u043e\u043a\u0438 \u043f\u043e Blender / Blender 3D","isPartOf":{"@id":"https://blender3d.com.ua/#website"},"description":"\u041d\u0430 \u0441\u0430\u0439\u0442\u0435 Blender3D \u0441\u043e\u0431\u0440\u0430\u043d\u043e \u043e\u0433\u0440\u043e\u043c\u043d\u043e\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0443\u0440\u043e\u043a\u043e\u0432 \u043f\u043e \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u0435 \u0442\u0440\u0435\u0445\u043c\u0435\u0440\u043d\u043e\u0433\u043e \u043c\u043e\u0434\u0435\u043b\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f Blender.","inLanguage":"ru-RU","potentialAction":[{"@type":"ReadAction","target":["https://blender3d.com.ua/"]}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/blender3d.com.ua\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://blender3d.com.ua/wp-content/plugins/wpw_ba_viewer/wpw_ba.css" id="ba_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-includes/css/dist/block-library/style.min.css" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/plugins/bbpress/templates/default/css/bbpress.min.css?ver=2.6.6" id="bbp-default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/plugins/moderation-tools-for-bbpress/css/front.css?ver=1.2.0" id="moderation-tools-bbpress-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/plugins/smooth-scroll-up/css/scrollup.css" id="scrollup-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/plugins/wp-syntax/css/wp-syntax.css?ver=1.1" id="wp-syntax-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://blender3d.com.ua/wp-content/plugins/simple-lightbox/client/css/app.css?ver=2.8.1" id="slb_core-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://blender3d.com.ua/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://blender3d.com.ua/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="jquery_easing-js" src="https://blender3d.com.ua/wp-content/plugins/wpw_ba_viewer/jquery.easing.1.3.js" type="text/javascript"></script>
<script id="wpw-drag-stuff-js" src="https://blender3d.com.ua/wp-content/plugins/wpw_ba_viewer/wpw.drag.stuff.js" type="text/javascript"></script>
<script id="wpw_ba_viewer_js-js" src="https://blender3d.com.ua/wp-content/plugins/wpw_ba_viewer/wpw.ba.viewer.min.js" type="text/javascript"></script>
<link href="https://blender3d.com.ua/wp-json/" rel="https://api.w.org/"/><link href="https://blender3d.com.ua/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://blender3d.com.ua/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<style id="wp-custom-css" type="text/css">
			#bbpress-forums ul.bbp-replies,.bbp-forums,.bbp-topics{
	border: 1px solid #ccc !important;
	border-radius: 5px;
}
.bbp-forum-title,.bbp-topic-permalink{
	font-size: 16px
}
.bbp-topic-title,.bbp-forum-info{
	width: 50% !important
}
.bbp-forum-topic-count,.bbp-topic-voice-count,.bbp-forum-reply-count,.bbp-topic-reply-count{
	width: 13% !important
}
.bbp-forum-freshness,.bbp-topic-freshness{
	width: 24% !important
}		</style>
<!--[if lt IE 9]>
		<script type="text/javascript">
		window.document.location = 'https://blender3d.com.ua/ie/ie.html';
		</script>
		<![endif]-->
</head>
<body data-rsssl="1">
<section id="wrap">
<section class="wrap_block">
<!--======= BEGIN OF header (.main_header) ======-->
<header class="main_header">
<section class="container">
<nav id="horizontal">
<a class="logo" href="https://blender3d.com.ua/"></a>
<ul id="nav">
<li><a href="https://blender3d.com.ua/">Главная<br/><em>Уроки по Blender</em></a></li>
<li><a href="https://blender3d.com.ua/forum/">Форум<br/><em>Обсуждение Blender</em></a></li>
<li><a href="https://blender3d.com.ua/blender-basics/">Курс<br/><em>Основы Blender</em></a></li>
<li><a href="https://store.blender3d.com.ua/">Магазин<br/><em>Книги, курсы и материалы</em></a></li>
</ul><!-- End of #nav-->
</nav><!-- End of menu nav#horizontal -->
<div class="clear"></div>
</section>
</header>
<!--======= END OF header (.main_header) ======--> <section class="container layout">
<section class="page_title">
<div class="title"><h1>Blender3D <span class="slogan">Уроки по Blender</span></h1></div>
<div class="widget widget_search" id="search-2" style="float:right"><form action="https://blender3d.com.ua/" class="searchform" id="searchform" method="get" role="search">
<div>
<label class="screen-reader-text" for="s">Найти:</label>
<input id="s" name="s" type="text" value=""/>
<input id="searchsubmit" type="submit" value="Поиск"/>
</div>
</form></div> </section>
<div class="clear"></div>
<section class="grid_8">
<section id="blog">
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/feyyerverk-v-blender/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="198" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/12/Preview_site_2.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">28 декабря 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/feyyerverk-v-blender/#HyperComments_Box"><span class="cackle-postid" id="c35865">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/simulations/" rel="category tag">Симуляция и частицы</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/particles/" rel="tag">Частицы</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/feyyerverk-v-blender/">Фейерверк в Blender</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Завершим 2020-й год ярким фейерверком в Blender. Урок короткий и простой, поэтому новогодняя сценка должна получиться у каждого ;)</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/blender-291/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="198" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/11/Preview_site_1.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">27 ноября 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/blender-291/#HyperComments_Box"><span class="cackle-postid" id="c35788">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/news/" rel="category tag">Новости и обзоры</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/cycles/" rel="tag">Cycles</a>, <a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/blender/" rel="tag">Блендер</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/blender-291/">Blender 2.91</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Blender Foundation и сообщество разработчиков с гордостью представили новую версию Blender 2.91.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/interaktivnaya-svechka-v-blender/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="198" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/10/candle-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">30 октября 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/interaktivnaya-svechka-v-blender/#HyperComments_Box"><span class="cackle-postid" id="c35694">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/animation/" rel="category tag">Анимация и риггинг</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/advanced/" rel="tag">Продвинутый</a>, <a href="https://blender3d.com.ua/tag/physics/" rel="tag">Физика</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/interaktivnaya-svechka-v-blender/">Интерактивная свечка в Blender</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Слишком много внимания для новичков в последнее время, пора бы и бывалых блендеристов уважить ;) В этом ролике мы будем создавать интерактивную свечку с помощью, практически, всего арсенала Blender. Будет задействовано все, кроме Grease Pencil и драйверов.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/tropicheskaya-stsena-v-blender/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/10/boat-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">5 октября 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/tropicheskaya-stsena-v-blender/#HyperComments_Box"><span class="cackle-postid" id="c35598">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/modeling/" rel="category tag">Моделирование и скульптинг</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/cycles/" rel="tag">Cycles</a>, <a href="https://blender3d.com.ua/tag/uv-unwrap/" rel="tag">UVразвертка</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a>, <a href="https://blender3d.com.ua/tag/nodes/" rel="tag">Ноды</a>, <a href="https://blender3d.com.ua/tag/shaders/" rel="tag">Шейдеры</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/tropicheskaya-stsena-v-blender/">Тропическая сцена в Blender</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Это будет достаточно простой урок в рамках которого мы создадим простенькую тропическую сцену. В уроке будут задействованы модификаторы, шейдеры, UV-развертка и простое моделирование.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/blender-290/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/08/290-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">2 сентября 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/blender-290/#HyperComments_Box"><span class="cackle-postid" id="c35171">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/news/" rel="category tag">Новости и обзоры</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/cycles/" rel="tag">Cycles</a>, <a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/blender/" rel="tag">Блендер</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/blender-290/">Blender 2.90</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Blender Foundation и сообщество разработчиков с гордостью представили новую версию Blender 2.90.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/mechanism-animation-in-blender/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/08/mecha-eevee-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">17 августа 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/mechanism-animation-in-blender/#HyperComments_Box"><span class="cackle-postid" id="c35113">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/animation/" rel="category tag">Анимация и риггинг</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/nodes/" rel="tag">Ноды</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/mechanism-animation-in-blender/">Анимация механизма в Blender</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Продолжаем серию из двух уроков, в которых создадим анимацию высокотехнологичного механизма в Blender. Во второй части соберем все детали механизма в единую конструкцию, настроим материалы и свет и создадим анимацию сборки этого механизма.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/hard-surface-modeling-in-blender/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/07/mecha-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">30 июля 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/hard-surface-modeling-in-blender/#HyperComments_Box"><span class="cackle-postid" id="c35021">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/modeling/" rel="category tag">Моделирование и скульптинг</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/tools/" rel="tag">Инструменты</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/hard-surface-modeling-in-blender/">Hard-Surface моделирование в Blender</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Этим уроком мы начнем коротенькую серию из двух уроков, в которых создадим анимацию высокотехнологичного механизма в Blender. Конкретно в первой части мы сосредоточимся на hard-surface моделировании всех основных деталей, а во второй приступим к настройке материалов и анимации.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/3d-graphics-in-1-hour-for-beginners/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/07/hour3d.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">27 июля 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/3d-graphics-in-1-hour-for-beginners/#HyperComments_Box"><span class="cackle-postid" id="c34985">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/osnovu/" rel="category tag">Основы Blender</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/basics/" rel="tag">Базовое</a>, <a href="https://blender3d.com.ua/tag/blender/" rel="tag">Блендер</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/3d-graphics-in-1-hour-for-beginners/">3D графика за 1 час! (для новичков)</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>В ходе этого урока, вы узнаете что такое 3D-графика, для чего она нужна и где используется. Также мы развеем несколько популярных мифов о 3D. </p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<!--============= ARTICLE BEGIN ===========-->
<article class="style2">
<div class="grid_6 alpha">
<div class="img_pf_hover"><a href="https://blender3d.com.ua/blender-283/"><img alt="" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" height="192" loading="lazy" src="https://blender3d.com.ua/wp-content/uploads/2020/06/blender-283-preview.jpg" width="452"/></a></div>
</div>
<!-- Article image end -->
<div class="grid_2 omega meta">
<div class="date">8 июня 2020</div>
<div class="comments"><a href="https://blender3d.com.ua/blender-283/#HyperComments_Box"><span class="cackle-postid" id="c20692">Комментариев нет</span></a></div>
<div class="author">Артем Слаква</div>
<div class="category"><a href="https://blender3d.com.ua/category/news/" rel="category tag">Новости и обзоры</a></div>
<div class="tags"><a href="https://blender3d.com.ua/tag/cycles/" rel="tag">Cycles</a>, <a href="https://blender3d.com.ua/tag/eevee/" rel="tag">EEVEE</a>, <a href="https://blender3d.com.ua/tag/blender/" rel="tag">Блендер</a>, <a href="https://blender3d.com.ua/tag/model/" rel="tag">Моделирование</a></div>
</div>
<div class="clear"></div>
<!-- Article meta end -->
<div>
<div class="title">
<h2><a href="https://blender3d.com.ua/blender-283/">Blender 2.83</a></h2>
</div>
<div class="clear"></div>
<!-- Article title end -->
<p>Blender Foundation и сообщество разработчиков с гордостью представили новую версию Blender 2.83.</p>
<div class="clear"></div>
<!-- Article description end -->
</div>
</article>
<div class="clear"></div>
<!--============= ARTICLE END ===========-->
<section id="list_pagination">
<span aria-current="page" class="page-numbers current">1</span>
<a class="page-numbers" href="https://blender3d.com.ua/page/2/">2</a>
<a class="page-numbers" href="https://blender3d.com.ua/page/3/">3</a>
<a class="page-numbers" href="https://blender3d.com.ua/page/4/">4</a>
<a class="page-numbers" href="https://blender3d.com.ua/page/5/">5</a>
<span class="page-numbers dots">…</span>
<a class="page-numbers" href="https://blender3d.com.ua/page/45/">45</a>
<a class="next page-numbers" href="https://blender3d.com.ua/page/2/">&gt;&gt;</a> </section><!-- / #list_pagination -->
</section> <!-- End #blog -->
</section>
<!--== Start sidebar ==-->
<section class="sidebar_r grid_4">
<section class="widget bbp_widget_login" id="bbp_login_widget-2"><h2>Авторизация</h2><div class="wgt_in">
<form action="https://blender3d.com.ua/wp-login.php" class="bbp-login-form" method="post">
<fieldset class="bbp-form">
<legend>Войти</legend>
<div class="bbp-username">
<label for="user_login">Имя пользователя: </label>
<input autocomplete="off" id="user_login" maxlength="100" name="log" size="20" type="text" value=""/>
</div>
<div class="bbp-password">
<label for="user_pass">Пароль: </label>
<input autocomplete="off" id="user_pass" name="pwd" size="20" type="password" value=""/>
</div>
<div class="bbp-remember-me">
<input id="rememberme" name="rememberme" type="checkbox" value="forever"/>
<label for="rememberme">Запомнить меня</label>
</div>
<div class="bbp-submit-wrapper">
<button class="button submit user-submit" id="user-submit" name="user-submit" type="submit">Войти</button>
<input name="user-cookie" type="hidden" value="1"/>
<input id="bbp_redirect_to" name="redirect_to" type="hidden" value="https://blender3d.com.ua/"/><input id="_wpnonce" name="_wpnonce" type="hidden" value="c7d1d6fc98"/><input name="_wp_http_referer" type="hidden" value="/"/>
</div>
<div class="bbp-login-links">
<a class="bbp-register-link" href="https://blender3d.com.ua/registration/" title="Зарегистрироваться">Зарегистрироваться</a>
<a class="bbp-lostpass-link" href="https://blender3d.com.ua/lost-pass/" title="Забыли пароль?">Забыли пароль?</a>
</div>
</fieldset>
</form>
</div><div class="clear"></div></section><section class="widget_text widget widget_custom_html" id="custom_html-6"><div class="textwidget custom-html-widget"><a href="https://photoshop-master.org/disc321/"><img alt="blender course" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2020/05/super-blender.jpg" style="margin: 9px 0px 3px 9px;"/></a></div><div class="clear"></div></section><section class="widget_text widget widget_custom_html" id="custom_html-4"><div class="textwidget custom-html-widget"><a href="https://blender3d.com.ua/eevee-book/"><img alt="eevee-book" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2019/07/eevee-banner.jpg" style="margin: 9px 0px 3px 9px;"/></a></div><div class="clear"></div></section><section class="widget widget_text" id="text-10"> <div class="textwidget"><a href="https://blender3d.com.ua/cycles-book/"><img alt="cycles-book" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2015/11/cycles-banner.jpg" style="margin: 9px 0px 3px 9px;"/></a></div>
<div class="clear"></div></section><section class="widget widget_categories" id="categories-2"><h2>Рубрики</h2><div class="wgt_in">
<ul>
<li class="cat-item cat-item-9"><a href="https://blender3d.com.ua/category/animation/" title="Создание анимации в Blender">Анимация и риггинг</a>
</li>
<li class="cat-item cat-item-10"><a href="https://blender3d.com.ua/category/downloads/" title="Здесь собрано множество текстур, моделей и различных дополнений для Blender">Загрузки</a>
</li>
<li class="cat-item cat-item-22"><a href="https://blender3d.com.ua/category/materials/" title="Создание материалов и шейдеров в Blender">Материалы и текстуры</a>
</li>
<li class="cat-item cat-item-5"><a href="https://blender3d.com.ua/category/modeling/" title="Моделирование в Blender">Моделирование и скульптинг</a>
</li>
<li class="cat-item cat-item-20"><a href="https://blender3d.com.ua/category/news/" title="Новости трехмерного редактора Blender">Новости и обзоры</a>
</li>
<li class="cat-item cat-item-1"><a href="https://blender3d.com.ua/category/osnovu/" title="Основы работы в трёхмерном редакторе Blender">Основы Blender</a>
</li>
<li class="cat-item cat-item-27"><a href="https://blender3d.com.ua/category/rendering/" title="Возможности движков рендеринга Blender">Рендеринг и освещение</a>
</li>
<li class="cat-item cat-item-14"><a href="https://blender3d.com.ua/category/simulations/" title="Создание различных симуляций огня, воды, ветра, ткани, мягких объектов и т.д.">Симуляция и частицы</a>
</li>
<li class="cat-item cat-item-24"><a href="https://blender3d.com.ua/category/scripting/" title="Написание скриптов на Python в Blender">Скриптинг на Python</a>
</li>
<li class="cat-item cat-item-30"><a href="https://blender3d.com.ua/category/game-engine/" title="Создание игр с помощью движка blender game engine">Создание игр в Blender</a>
</li>
</ul>
</div><div class="clear"></div></section><section class="widget widget_text" id="text-11"> <div class="textwidget"><a href="https://blender3d.com.ua/modeling-tools-book/"><img alt="mod-book" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2016/11/mod-banner.jpg" style="margin: 9px 0px 3px 9px;"/></a></div>
<div class="clear"></div></section><section class="widget_text widget widget_custom_html" id="custom_html-2"><div class="textwidget custom-html-widget"><a href="https://blender3d.com.ua/freestyle-book/"><img alt="freestyle-book" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2018/08/freestyle-banner.jpg" style="margin: 9px 0px 3px 9px;"/></a></div><div class="clear"></div></section><section class="widget widget_text" id="text-9"> <div class="textwidget"><a href="https://blender3d.com.ua/3d-printing-with-blender/"><img alt="3d-printing-with-blender" class="popular-post" src="https://blender3d.com.ua/wp-content/uploads/2015/02/print-banner.jpg" style="margin: 9px 0px 3px 9px;"/></a></div>
<div class="clear"></div></section>
</section>
<!--== End sidebar ==--> </section> <!-- End section .container layout -->
<footer class="main_footer">
<section class="container">
<div class="grid_4">
<h3 class="ft_title">О сайте</h3>
<p>На данном сайте Вы сможете найти множество уроков и материалов по графическому<br/>редактору Blender.</p>
</div>
<div class="grid_4">
<h3 class="ft_title">Контакты</h3>
<p>Для связи с администрацией сайта Вы можете воспользоваться следующими контактами:</p>
<p>Email:<br/><a href="mailto:info@blender3d.com.ua">info@blender3d.com.ua</a></p>
</div>
<div class="grid_4">
<h3 class="ft_title">Следите за нами</h3>
<p>Подписывайтесь на наши страницы в социальных сетях.</p>
<div class="social">
<a class="skype" href="https://www.instagram.com/blender3d.com.ua/" target="_blank"></a>
<a class="feed" href="https://www.youtube.com/user/Blender3Dcomua" target="_blank"></a>
<a class="twitter" href="https://www.pinterest.com/teha2007/" target="_blank"></a>
<a class="email" href="http://vk.com/blender3dcomua" target="_blank"></a>
<a class="facebook" href="https://www.facebook.com/blender3dcomua" target="_blank"></a>
</div>
</div>
<div class="grid_12">
<p>На сайте Blender3D собрано огромное количество уроков по программе трехмерного моделирования Blender. Обучающие материалы представлены как в формате видеоуроков, так и в текстовом виде. Здесь затронуты все аспекты, связанные с Blender, начиная от моделирования и заканчивая созданием игр с применением языка программирования Python.</p><p>Помимо уроков по Blender, Вы сможете найти готовые 3D-модели, материалы и архивы высококачественных текстур. Сайт регулярно пополняется новым контентом и следит за развитием Blender.</p>
</div>
</section>
<section class="mini_footer">
<div class="container">
<div class="alignleft copyright">© 2013 - 2021 Blender3D. Все права защищены</div>
<nav id="ft_links">
<ul id="nav_footer">
<li><a href="https://blender3d.com.ua/">Главная</a></li>
<li><a href="https://blender3d.com.ua/faq/">FAQ</a></li>
<li><a href="https://blender3d.com.ua/pravila-perepechatki/">Правила перепечатки</a></li>
<li><a href="http://feeds.feedburner.com/blender3dcomua" target="_blank">RSS</a></li>
<li><a href="https://blender3d.com.ua/sitemap_index.xml" target="_blank">Карта сайта</a></li>
</ul>
</nav>
</div>
</section>
</footer> <!-- End .main_footer -->
</section><!-- End section .wrap_block -->
</section> <!-- End section #wrap -->
<script type="text/javascript">
                // <![CDATA[
                var nodes = document.getElementsByTagName('span');
                for (var i = 0, url; i < nodes.length; i++) {
                    if (nodes[i].className.indexOf('cackle-postid') != -1) {
                        var c_id = nodes[i].getAttribute('id').split('c');
                        nodes[i].parentNode.setAttribute('cackle-channel', c_id[1] );
                        url = nodes[i].parentNode.href.split('#', 1);
                        if (url.length == 1) url = url[0];
                        else url = url[1]
                        nodes[i].parentNode.href = url + '#mc-container';
                    }
                }


                cackle_widget = window.cackle_widget || [];
                cackle_widget.push({widget: 'CommentCount',  id: '64654'});
                (function() {
                    var mc = document.createElement('script');
                    mc.type = 'text/javascript';
                    mc.async = true;
                    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                })();
                //]]>
            </script>
<script> var $nocnflct = jQuery.noConflict();
			$nocnflct(function () {
			    $nocnflct.scrollUp({
				scrollName: 'scrollUp', // Element ID
				scrollClass: 'scrollUp scrollup-pill scrollup-right', // Element Class
				scrollDistance: 1000, // Distance from top/bottom before showing element (px)
				scrollFrom: 'top', // top or bottom
				scrollSpeed: 1000, // Speed back to top (ms )
				easingType: 'linear', // Scroll to top easing (see http://easings.net/)
				animation: 'fade', // Fade, slide, none
				animationInSpeed: 200, // Animation in speed (ms )
				animationOutSpeed: 200, // Animation out speed (ms )
				scrollText: 'Наверх', // Text for element, can contain HTML
				scrollTitle: false, // Set a custom link title if required. Defaults to scrollText
				scrollImg: false, // Set true to use image
				activeOverlay: false, // Set CSS color to display scrollUp active point
				zIndex: 2147483647 // Z-Index for the overlay
			    });
			});</script><script id="scrollup-js-js" src="https://blender3d.com.ua/wp-content/plugins/smooth-scroll-up/js/jquery.scrollUp.min.js" type="text/javascript"></script>
<script id="wp-syntax-js-js" src="https://blender3d.com.ua/wp-content/plugins/wp-syntax/js/wp-syntax.js?ver=1.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://blender3d.com.ua/wp-includes/js/wp-embed.min.js" type="text/javascript"></script>
<script id="slb_context" type="text/javascript">/* <![CDATA[ */if ( !!window.jQuery ) {(function($){$(document).ready(function(){if ( !!window.SLB ) { {$.extend(SLB, {"context":["public","user_guest"]});} }})})(jQuery);}/* ]]> */</script>
<script>
		jQuery(document).ready(function() {
			mykey = document.getElementById('searchsubmit');
			jQuery(mykey).addClass("button darkgray large");
			mykey2 = document.getElementById('s');
			jQuery(mykey2).attr("placeholder", "Найти урок");
		});
	</script>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21241810 = new Ya.Metrika({id:21241810, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="//mc.yandex.ru/watch/21241810" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
</body>
</html>