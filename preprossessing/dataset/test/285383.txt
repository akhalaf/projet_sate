<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta content="noindex,nofollow" name="robots"/>
<style>
            /* Copyright (c) 2010, Yahoo! Inc. All rights reserved. Code licensed under the BSD License: http://developer.yahoo.com/yui/license.html */
            html{color:#000;background:#FFF;}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}q:before,q:after{content:'';}abbr,acronym{border:0;font-variant:normal;}sup{vertical-align:text-top;}sub{vertical-align:text-bottom;}input,textarea,select{font-family:inherit;font-size:inherit;font-weight:inherit;}input,textarea,select{*font-size:100%;}legend{color:#000;}
            html { background: #eee; padding: 10px }
            img { border: 0; }
            #sf-resetcontent { width:970px; margin:0 auto; }
                        .sf-reset { font: 11px Verdana, Arial, sans-serif; color: #333 }
            .sf-reset .clear { clear:both; height:0; font-size:0; line-height:0; }
            .sf-reset .clear_fix:after { display:block; height:0; clear:both; visibility:hidden; }
            .sf-reset .clear_fix { display:inline-block; }
            .sf-reset * html .clear_fix { height:1%; }
            .sf-reset .clear_fix { display:block; }
            .sf-reset, .sf-reset .block { margin: auto }
            .sf-reset abbr { border-bottom: 1px dotted #000; cursor: help; }
            .sf-reset p { font-size:14px; line-height:20px; color:#868686; padding-bottom:20px }
            .sf-reset strong { font-weight:bold; }
            .sf-reset a { color:#6c6159; cursor: default; }
            .sf-reset a img { border:none; }
            .sf-reset a:hover { text-decoration:underline; }
            .sf-reset em { font-style:italic; }
            .sf-reset h1, .sf-reset h2 { font: 20px Georgia, "Times New Roman", Times, serif }
            .sf-reset .exception_counter { background-color: #fff; color: #333; padding: 6px; float: left; margin-right: 10px; float: left; display: block; }
            .sf-reset .exception_title { margin-left: 3em; margin-bottom: 0.7em; display: block; }
            .sf-reset .exception_message { margin-left: 3em; display: block; }
            .sf-reset .traces li { font-size:12px; padding: 2px 4px; list-style-type:decimal; margin-left:20px; }
            .sf-reset .block { background-color:#FFFFFF; padding:10px 28px; margin-bottom:20px;
                -webkit-border-bottom-right-radius: 16px;
                -webkit-border-bottom-left-radius: 16px;
                -moz-border-radius-bottomright: 16px;
                -moz-border-radius-bottomleft: 16px;
                border-bottom-right-radius: 16px;
                border-bottom-left-radius: 16px;
                border-bottom:1px solid #ccc;
                border-right:1px solid #ccc;
                border-left:1px solid #ccc;
                word-wrap: break-word;
            }
            .sf-reset .block_exception { background-color:#ddd; color: #333; padding:20px;
                -webkit-border-top-left-radius: 16px;
                -webkit-border-top-right-radius: 16px;
                -moz-border-radius-topleft: 16px;
                -moz-border-radius-topright: 16px;
                border-top-left-radius: 16px;
                border-top-right-radius: 16px;
                border-top:1px solid #ccc;
                border-right:1px solid #ccc;
                border-left:1px solid #ccc;
                overflow: hidden;
                word-wrap: break-word;
            }
            .sf-reset a { background:none; color:#868686; text-decoration:none; }
            .sf-reset a:hover { background:none; color:#313131; text-decoration:underline; }
            .sf-reset ol { padding: 10px 0; }
            .sf-reset h1 { background-color:#FFFFFF; padding: 15px 28px; margin-bottom: 20px;
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
                border: 1px solid #ccc;
            }
        </style>
</head>
<body>
<div class="sf-reset" id="sf-resetcontent">
<h1>Sorry, the page you are looking for could not be found.</h1>
<h2 class="block_exception clear_fix">
<span class="exception_counter">1/1</span>
<span class="exception_title"><abbr title="Symfony\Component\HttpKernel\Exception\NotFoundHttpException">NotFoundHttpException</abbr> in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 8324">compiled.php line 8324</a>:</span>
<span class="exception_message"></span>
</h2>
<div class="block">
<ol class="traces list_exception"> <li> in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 8324">compiled.php line 8324</a></li>
<li>at <abbr title="Illuminate\Routing\RouteCollection">RouteCollection</abbr>-&gt;match(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 7558">compiled.php line 7558</a></li>
<li>at <abbr title="Illuminate\Routing\Router">Router</abbr>-&gt;findRoute(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 7523">compiled.php line 7523</a></li>
<li>at <abbr title="Illuminate\Routing\Router">Router</abbr>-&gt;dispatchToRoute(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 7515">compiled.php line 7515</a></li>
<li>at <abbr title="Illuminate\Routing\Router">Router</abbr>-&gt;dispatch(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 2310">compiled.php line 2310</a></li>
<li>at <abbr title="Illuminate\Foundation\Http\Kernel">Kernel</abbr>-&gt;Illuminate\Foundation\Http\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func(<em>object</em>(<abbr title="Closure">Closure</abbr>), <em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9657">compiled.php line 9657</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 2925">compiled.php line 2925</a></li>
<li>at <abbr title="Illuminate\Foundation\Http\Middleware\VerifyCsrfToken">VerifyCsrfToken</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="App\Http\Middleware\VerifyCsrfToken">VerifyCsrfToken</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 13008">compiled.php line 13008</a></li>
<li>at <abbr title="Illuminate\View\Middleware\ShareErrorsFromSession">ShareErrorsFromSession</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="Illuminate\View\Middleware\ShareErrorsFromSession">ShareErrorsFromSession</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 11600">compiled.php line 11600</a></li>
<li>at <abbr title="Illuminate\Session\Middleware\StartSession">StartSession</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="Illuminate\Session\Middleware\StartSession">StartSession</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 12745">compiled.php line 12745</a></li>
<li>at <abbr title="Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse">AddQueuedCookiesToResponse</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse">AddQueuedCookiesToResponse</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 12682">compiled.php line 12682</a></li>
<li>at <abbr title="Illuminate\Cookie\Middleware\EncryptCookies">EncryptCookies</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="App\Http\Middleware\EncryptCookies">EncryptCookies</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 2982">compiled.php line 2982</a></li>
<li>at <abbr title="Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode">CheckForMaintenanceMode</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode">CheckForMaintenanceMode</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/app/Http/Middleware/Language.php line 40">Language.php line 40</a></li>
<li>at <abbr title="App\Http\Middleware\Language">Language</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func_array(<em>array</em>(<em>object</em>(<abbr title="App\Http\Middleware\Language">Language</abbr>), 'handle'), <em>array</em>(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>), <em>object</em>(<abbr title="Closure">Closure</abbr>))) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9649">compiled.php line 9649</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;Illuminate\Pipeline\{closure}(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>))</li>
<li>at <abbr title=""></abbr>call_user_func(<em>object</em>(<abbr title="Closure">Closure</abbr>), <em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 9639">compiled.php line 9639</a></li>
<li>at <abbr title="Illuminate\Pipeline\Pipeline">Pipeline</abbr>-&gt;then(<em>object</em>(<abbr title="Closure">Closure</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 2257">compiled.php line 2257</a></li>
<li>at <abbr title="Illuminate\Foundation\Http\Kernel">Kernel</abbr>-&gt;sendRequestThroughRouter(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/bootstrap/cache/compiled.php line 2240">compiled.php line 2240</a></li>
<li>at <abbr title="Illuminate\Foundation\Http\Kernel">Kernel</abbr>-&gt;handle(<em>object</em>(<abbr title="Illuminate\Http\Request">Request</abbr>)) in <a ondblclick="var f=this.innerHTML;this.innerHTML=this.title;this.title=f;" title="/home/customer/www/caplang/web/public/index.php line 55">index.php line 55</a></li>
</ol>
</div>
</div>
</body>
</html>