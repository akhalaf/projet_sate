<!DOCTYPE html>
<html lang="en-us">
<head>
<title>1907 American League Baseball Debuts / Rookies by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="1907 American League Baseball Debuts Rookies Baseball Almanac 1907 american league baseball debuts rookies baseball almanac" name="keywords"/>
<meta content="Baseball rookies and Major League debuts from the 1907 American League season are presented on Baseball Almanac with team names, debut date and a debut age." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- ' / -->
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
<style><!--  --></style>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
</head>
<body>
<!-- BEGIN FREESTAR -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || new Object();
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || new Object();
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<!-- END FREESTAR -->
<div id="wrapper">
<div class="header-container">
<!-- START _site-header-v2.html -->
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
<!-- END _site-header-v2.html -->
</div>
<div class="container">
<div class="intro">
<h1>1907 American League Debuts</h1>
<p></p><p>Willie Mays played his first Major League game on May 25, 1951, and went 0-for-5 at the plate. He started his career 1-for-25 and told his manager, "I can't do it, Mr. Leo. You better bench me."</p>
<p>Casey Stengel played his first Major League game on July 27, 1912, and went 4-for-4 at the plate. He commented later in his life, "I broke in with four hits and the writers promptly decided they had seen the new Ty Cobb. It took me only a few days to correct that impression."</p>
<p>Both Mays and Stengel would continue their Major League careers down separate paths of greatness, but each still had to appear in that memorable first Major League game. Baseball Almanac is pleased to present what Cubs broadcaster Steve Stone once described as, "His first Major League debut."</p>
<!-- Are we using flycast?
 -->
</div>
<!-- 	End Intro Box -->
<!-- 	Begin Quote Box -->
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"There is no Major League record held by a rookie. It's revealing to see just how far the rookie marks are below the single season marks. This is a testament to how difficult the game is to learn and play." - Luke Salisbury in The Answer is Baseball (1989)</p>
</div>
<!-- 	End Quote Box -->
<!-- Begin Page Body -->
<!--Debut body-->
<div class="ba-table">
<table class="boxed">
<tr><td class="header" colspan="6"><h2>1907 American League Debuts</h2><p>1907 A.L. Rookies | <span style="white-space: nowrap;"><a href="/yearly/debut.php?y=1907&amp;l=NL">1907 N.L. Rookies</a> | </span> <a href="/frstmenu.shtml">Baseball Firsts</a> (All Years)</p></td></tr>
<tr><td class="banner">#</td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=N" title="Sort by Name">Name (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=T" title="Sort by Team">Team (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=D" title="Sort by Debut Date">Debut (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=A" title="Sort by Age">Age (sort)</a></td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=lordha01" title="Show player stats">Harry 
        	Lord</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=BO1" title="Click to see this team's roster for this year">1907 Boston Americans</a></td>
<td class="datacolBoxC">09-25-1907</td>
<td class="datacolBoxC">25</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=pruiete01" title="Show player stats">Tex 
        	Pruiett</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=BO1" title="Click to see this team's roster for this year">1907 Boston Americans</a></td>
<td class="datacolBoxC">04-26-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=speaktr01" title="Show player stats">Tris 
        	Speaker</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=BO1" title="Click to see this team's roster for this year">1907 Boston Americans</a></td>
<td class="datacolBoxC">09-14-1907</td>
<td class="datacolBoxC">19</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=steelel01" title="Show player stats">Elmer 
        	Steele</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=BO1" title="Click to see this team's roster for this year">1907 Boston Americans</a></td>
<td class="datacolBoxC">09-12-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=whitege01" title="Show player stats">George 
        	Whiteman</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=BO1" title="Click to see this team's roster for this year">1907 Boston Americans</a></td>
<td class="datacolBoxC">09-13-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=weldami01" title="Show player stats">Mike 
        	Welday</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=CHA" title="Click to see this team's roster for this year">1907 Chicago White Sox</a></td>
<td class="datacolBoxC">04-17-1907</td>
<td class="datacolBoxC">28</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=bergehe01" title="Show player stats">Heinie 
        	Berger</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=CL9" title="Click to see this team's roster for this year">1907 Cleveland Naps</a></td>
<td class="datacolBoxC">05-06-1907</td>
<td class="datacolBoxC">25</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=hinchha01" title="Show player stats">Harry 
        	Hinchman</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=CL9" title="Click to see this team's roster for this year">1907 Cleveland Naps</a></td>
<td class="datacolBoxC">07-29-1907</td>
<td class="datacolBoxC">28</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=listepe01" title="Show player stats">Pete 
        	Lister</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=CL9" title="Click to see this team's roster for this year">1907 Cleveland Naps</a></td>
<td class="datacolBoxC">09-14-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=downsre01" title="Show player stats">Red 
        	Downs</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=DET" title="Click to see this team's roster for this year">1907 Detroit Tigers</a></td>
<td class="datacolBoxC">05-02-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=erwinte01" title="Show player stats">Tex 
        	Erwin</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=DET" title="Click to see this team's roster for this year">1907 Detroit Tigers</a></td>
<td class="datacolBoxC">08-26-1907</td>
<td class="datacolBoxC">21</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=jonesel01" title="Show player stats">Elijah 
        	Jones</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=DET" title="Click to see this team's roster for this year">1907 Detroit Tigers</a></td>
<td class="datacolBoxC">04-13-1907</td>
<td class="datacolBoxC">25</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=killere01" title="Show player stats">Red 
        	Killefer</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=DET" title="Click to see this team's roster for this year">1907 Detroit Tigers</a></td>
<td class="datacolBoxC">09-16-1907</td>
<td class="datacolBoxC">22</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=mallohe01" title="Show player stats">Herm 
        	Malloy</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=DET" title="Click to see this team's roster for this year">1907 Detroit Tigers</a></td>
<td class="datacolBoxC">10-06-1907</td>
<td class="datacolBoxC">22</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=ballne01" title="Show player stats">Neal 
        	Ball</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">09-11-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=bellru01" title="Show player stats">Rudy 
        	Bell</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">09-14-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=blairwa01" title="Show player stats">Walter 
        	Blair</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">09-17-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=brockle01" title="Show player stats">Lew 
        	Brockett</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">04-13-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=castlro01" title="Show player stats">Roy 
        	Castleton</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">04-16-1907</td>
<td class="datacolBoxC">21</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=keefebo01" title="Show player stats">Bobby 
        	Keefe</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">04-15-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=loudeba01" title="Show player stats">Baldy 
        	Louden</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">09-13-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=manniru01" title="Show player stats">Rube 
        	Manning</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">09-25-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=neuerte01" title="Show player stats">Tacks 
        	Neuer</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">08-28-1907</td>
<td class="datacolBoxC">30</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=tiftra01" title="Show player stats">Ray 
        	Tift</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=NY5" title="Click to see this team's roster for this year">1907 New York Highlanders</a></td>
<td class="datacolBoxC">08-07-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=craigge01" title="Show player stats">George 
        	Craig</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=PHA" title="Click to see this team's roster for this year">1907 Philadelphia Athletics</a></td>
<td class="datacolBoxC">07-19-1907</td>
<td class="datacolBoxC">19</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=fritzch01" title="Show player stats">Charlie 
        	Fritz</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=PHA" title="Click to see this team's roster for this year">1907 Philadelphia Athletics</a></td>
<td class="datacolBoxC">10-05-1907</td>
<td class="datacolBoxC">25</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=hopesa01" title="Show player stats">Sam 
        	Hope</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=PHA" title="Click to see this team's roster for this year">1907 Philadelphia Athletics</a></td>
<td class="datacolBoxC">08-05-1907</td>
<td class="datacolBoxC">28</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=bailebi01" title="Show player stats">Bill 
        	Bailey</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=SLA" title="Click to see this team's roster for this year">1907 St. Louis Browns</a></td>
<td class="datacolBoxC">09-11-1907</td>
<td class="datacolBoxC">18</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=butleki02" title="Show player stats">Kid 
        	Butler</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=SLA" title="Click to see this team's roster for this year">1907 St. Louis Browns</a></td>
<td class="datacolBoxC">04-30-1907</td>
<td class="datacolBoxC">19</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=mcgilbi01" title="Show player stats">Bill 
        	McGill</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=SLA" title="Click to see this team's roster for this year">1907 St. Louis Browns</a></td>
<td class="datacolBoxC">09-16-1907</td>
<td class="datacolBoxC">27</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=stephji01" title="Show player stats">Jim 
        	Stephens</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=SLA" title="Click to see this team's roster for this year">1907 St. Louis Browns</a></td>
<td class="datacolBoxC">04-11-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=blockbr01" title="Show player stats">Bruno 
        	Block</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-05-1907</td>
<td class="datacolBoxC">22</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=edmonsa01" title="Show player stats">Sam 
        	Edmonston</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">06-24-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=gehrihe01" title="Show player stats">Henry 
        	Gehring</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">07-16-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=grahaos01" title="Show player stats">Oscar 
        	Graham</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">04-13-1907</td>
<td class="datacolBoxC">28</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=johnswa01" title="Show player stats">Walter 
        	Johnson</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-02-1907</td>
<td class="datacolBoxC">19</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=kaybi01" title="Show player stats">Bill 
        	Kay</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-12-1907</td>
<td class="datacolBoxC">29</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=lanfosa01" title="Show player stats">Sam 
        	Lanford</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-19-1907</td>
<td class="datacolBoxC">21</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=mcdonjo01" title="Show player stats">John 
        	McDonald</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">09-03-1907</td>
<td class="datacolBoxC">24</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=milancl01" title="Show player stats">Clyde 
        	Milan</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-19-1907</td>
<td class="datacolBoxC">20</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=perrini01" title="Show player stats">Nig 
        	Perrine</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">04-11-1907</td>
<td class="datacolBoxC">22</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=smithto03" title="Show player stats">Tony 
        	Smith</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-12-1907</td>
<td class="datacolBoxC">23</td></tr>
<tr><td class="datacolBoxC">n/a</td>
<td class="datacolBox"><a href="../players/player.php?p=tonkido01" title="Show player stats">Doc 
        	Tonkin</a></td>
<td class="datacolBox"><a href="../teamstats/roster.php?y=1907&amp;t=WS1" title="Click to see this team's roster for this year">1907 Washington Senators</a></td>
<td class="datacolBoxC">08-19-1907</td>
<td class="datacolBoxC">26</td></tr>
<tr><td class="banner">#</td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=N" title="Sort by Name">Name (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=T" title="Sort by Team">Team (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=D" title="Sort by Debut Date">Debut (sort)</a></td>
<td class="banner"><a href="?y=1907&amp;l=AL&amp;s=A" title="Sort by Age">Age (sort)</a></td></tr>
<tr><td class="header" colspan="6">1907 American League Debuts</td></tr>
</table>
</div>
<!--End Debut body-->
<!-- End Page Body -->
<!-- Begin Related Pages -->
<!-- End Related Pages -->
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="/images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>Find out which players bid farewell to their field's of dreams in the <a href="final.php?y=1907&amp;l=AL" title="Finals this year">American League during the 1907</a> season as this group of players made their Major League debut!</p>
<p>On the final day of the 1930 season, <a href="https://www.baseball-almanac.com/players/player.php?p=deandi01">Dizzy Dean</a> was called up and pitched a three-hitter. In Spring Training the following year, <a href="https://www.baseball-almanac.com/players/player.php?p=deandi01">Dean</a> had a fight with catcher <a href="https://www.baseball-almanac.com/players/player.php?p=streega01">Gabby Street</a> and the franchise left him in the Minor Leagues all season long.</p>
<p>Five-for-five debuts? Believe it or not it has happened twice: On June 30, 1894 <a href="https://www.baseball-almanac.com/players/player.php?p=clarkfr01">Fred Clarke</a> of the Louisville Colonels and on May 16, 1933, <a href="https://www.baseball-almanac.com/players/player.php?p=travice01">Cecil Travis</a> of the Washington Senators made their Major League debuts and both went five-for-five at the plate.</p>
</div>
</div>
<div class="footer">
<!-- START _footer-v2.txt" -->
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
			copyright=new Date();
			update=copyright.getFullYear();
			document.write(update);
		</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
<!-- END _footer-v2.txt" -->
</div>
</body>
</html>
