<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html dir="ltr" version="XHTML+RDFa 1.0" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script async="async" defer="defer" src="https://www.google.com/recaptcha/api.js?hl=es"></script>
<link href="https://www.anep.edu.uy/misc/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<meta content="La Administración Nacional de Educación Pública es el organismo estatal responsable de la planificación, gestión y administración del sistema educativo público en sus niveles de educación inicial, primaria, media, técnica y formación en educación terciaria en todo el territorio uruguayo." name="description"/>
<meta content="La ANEP es el organismo estatal responsable de la planificación, gestión y administración del sistema educativo público en todo el territorio uruguayo." name="abstract"/>
<meta content="Educación pública, ANEP, CODICEN," name="keywords"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.anep.edu.uy/" rel="canonical"/>
<link href="https://www.anep.edu.uy/" rel="shortlink"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://www.anep.edu.uy/inicio" name="twitter:url"/>
<title>Administración Nacional de Educación Pública |</title>
<style media="all" type="text/css">
@import url("https://www.anep.edu.uy/modules/system/system.base.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/system/system.menus.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/system/system.messages.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/system/system.theme.css?qk9aog");
</style>
<style media="all" type="text/css">
@import url("https://www.anep.edu.uy/sites/all/modules/calendar/css/calendar_multiday.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/comment/comment.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/date/date_api/date.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/date/date_repeat_field/date_repeat_field.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/field/theme/field.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/node/node.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/search/search.css?qk9aog");
@import url("https://www.anep.edu.uy/modules/user/user.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/views/css/views.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/ckeditor/css/ckeditor.css?qk9aog");
</style>
<style media="all" type="text/css">
@import url("https://www.anep.edu.uy/sites/all/modules/ctools/css/ctools.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/lightbox2/css/lightbox.css?qk9aog");
@import url("https://www.anep.edu.uy/sites/all/modules/addtoany/addtoany.css?qk9aog");
</style>
<style media="all" type="text/css">
@import url("https://www.anep.edu.uy/sites/all/themes/anep/css/template.css?qk9aog");
</style>
<script src="https://www.anep.edu.uy/sites/all/modules/jquery_update/replace/jquery/1.5/jquery.min.js?v=1.5.2" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/misc/jquery.once.js?v=1.2" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/misc/drupal.js?qk9aog" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/sites/all/modules/admin_menu/admin_devel/admin_devel.js?qk9aog" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/sites/default/files/languages/es_PxivDk2hZDFn45FkhYiIXz-aYKZ0mLOGOoC9wCMDvkI.js?qk9aog" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/sites/all/modules/lightbox2/js/lightbox.js?qk9aog" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/sites/all/modules/captcha/captcha.js?qk9aog" type="text/javascript"></script>
<script src="https://www.anep.edu.uy/sites/all/modules/google_analytics/googleanalytics.js?qk9aog" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-1576071-2", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
//--><!]]>
</script>
<script src="https://www.anep.edu.uy/sites/all/themes/anep/js/my_scripts.js?qk9aog" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"anep","theme_token":"n7qur_lBqDixhP-v0elwHA5Jlm3FlFn_QlqCNZowZJY","js":{"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.5\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/admin_menu\/admin_devel\/admin_devel.js":1,"public:\/\/languages\/es_PxivDk2hZDFn45FkhYiIXz-aYKZ0mLOGOoC9wCMDvkI.js":1,"sites\/all\/modules\/lightbox2\/js\/lightbox.js":1,"sites\/all\/modules\/captcha\/captcha.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/anep\/js\/my_scripts.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/lightbox2\/css\/lightbox.css":1,"sites\/all\/modules\/addtoany\/addtoany.css":1,"sites\/all\/themes\/anep\/css\/template.css":1}},"lightbox2":{"rtl":"0","file_path":"\/(\\w\\w\/)public:\/","default_image":"\/sites\/all\/modules\/lightbox2\/images\/brokenimage.jpg","border_size":10,"font_color":"000","box_color":"fff","top_position":"","overlay_opacity":"0.8","overlay_color":"000","disable_close_click":1,"resize_sequence":0,"resize_speed":400,"fade_in_speed":400,"slide_down_speed":600,"use_alt_layout":0,"disable_resize":0,"disable_zoom":1,"force_show_nav":0,"show_caption":1,"loop_items":0,"node_link_text":"Ver detalles de la imagen","node_link_target":0,"image_count":"Imagen !current de !total","video_count":"Video !current of !total","page_count":"Page !current of !total","lite_press_x_close":"press \u003Ca href=\u0022#\u0022 onclick=\u0022hideLightbox(); return FALSE;\u0022\u003E\u003Ckbd\u003Ex\u003C\/kbd\u003E\u003C\/a\u003E to close","download_link_text":"","enable_login":false,"enable_contact":false,"keys_close":"c x 27","keys_previous":"p 37","keys_next":"n 39","keys_zoom":"z","keys_play_pause":"32","display_image_size":"original","image_node_sizes":"()","trigger_lightbox_classes":"","trigger_lightbox_group_classes":"","trigger_slideshow_classes":"","trigger_lightframe_classes":"","trigger_lightframe_group_classes":"","custom_class_handler":0,"custom_trigger_classes":"","disable_for_gallery_lists":true,"disable_for_acidfree_gallery_lists":true,"enable_acidfree_videos":true,"slideshow_interval":5000,"slideshow_automatic_start":true,"slideshow_automatic_exit":true,"show_play_pause":true,"pause_on_next_click":false,"pause_on_previous_click":true,"loop_slides":false,"iframe_width":600,"iframe_height":400,"iframe_border":1,"enable_video":0,"useragent":"python-requests\/2.22.0"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip"},"urlIsAjaxTrusted":{"\/":true}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-inicio">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Pasar al contenido principal</a>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<link href="path/to/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<script src="https://use.fontawesome.com/40e4256e92.js"></script>
<!--<script src="js/my_scripts.js"></script>-->
<meta content="width=device-width" name="viewport"/>
<script type="text/javascript">
var velMove=500;

function verMenu() {
  jQuery('.institucional').stop('true','true');
  jQuery('.institucional').slideToggle(velMove);
  jQuery('.iconToggleMenu').toggleClass('iconoCerrarMenu');
}
</script>
<div class="main">
<header>
<div class="logo">
<a href="https://www.anep.edu.uy"><h1 id="identidad"><span>logo ANEP</span></h1></a>
</div>
<div class="menu">
<div class="iconToggleMenu" onclick="verMenu()"> <i aria-hidden="true" class="fa fa-bars"></i> <i aria-hidden="true" class="fa fa-times"></i>
</div>
<nav class="institucional">
<!--<div class="iconToggleCerrar" onclick="verMenu()"> <i class="fa fa-times"></i></div>-->
<div class="region region-menu-top">
<div class="block block-menu" id="block-menu-menu-menu-corporativo">
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/acerca-anep">Acerca de ANEP</a></li>
<li class="expanded"><a href="/codicen">Consejo Directivo Central</a><ul class="menu"><li class="first leaf"><a href="/codicen/autoridades" title="">Autoridades</a></li>
<li class="leaf"><a href="/codicen/direcciones" title="">Direcciones</a></li>
<li class="leaf"><a href="/codicen/secretar-general" title="">Secretaría General</a></li>
<li class="leaf"><a href="/codicen/secretaria-administrativa" title="">Secretaría Administrativa</a></li>
<li class="leaf"><a href="/codicen/asesoria-letrada" title="">Asesoría Letrada</a></li>
<li class="last leaf"><a href="/codicen/auditoria-interna" title="">Auditoría Interna</a></li>
</ul></li>
<li class="expanded"><a class="active" href="/" title="">Consejos</a><ul class="menu"><li class="first leaf"><a href="http://www.ceip.edu.uy/" title="CEIP">Consejo de Educación Inicial y Primaria</a></li>
<li class="leaf"><a href="http://www.ces.edu.uy/" title="CES">Consejo de Educación Secundaria</a></li>
<li class="leaf"><a href="https://www.utu.edu.uy/utu/inicio.html" title="UTU">Consejo de Educación Técnico Profesional</a></li>
<li class="last leaf"><a href="http://www.cfe.edu.uy/" title="CFE">Consejo de Formación en Educación</a></li>
</ul></li>
<li class="leaf"><a href="/llamados" title="">Llamados</a></li>
<li class="leaf"><a href="/transparencia">Transparencia</a></li>
<li class="last leaf"><a href="/contacto">Contacto</a></li>
</ul> </div>
</div>
</div>
</nav>
<div class="buscar">
<div class="region region-mio">
<div class="block block-search" id="block-search-form">
<div class="content">
<form accept-charset="UTF-8" action="/" id="search-block-form" method="post"><div><div class="container-inline">
<h2 class="element-invisible">Formulario de búsqueda</h2>
<div class="form-item form-type-textfield form-item-search-block-form">
<label class="element-invisible" for="edit-search-block-form--2">Buscar </label>
<input class="form-text" id="edit-search-block-form--2" maxlength="128" name="search_block_form" size="15" title="Escriba lo que quiere buscar." type="text" value=""/>
</div>
<div class="form-actions form-wrapper" id="edit-actions--2"><input class="form-submit" id="edit-submit--2" name="op" type="submit" value="Buscar"/></div><input name="form_build_id" type="hidden" value="form-jcINegtP5egRiHrWjNmLl0XlCk2UGgOpnAUIxqg7cW8"/>
<input name="form_id" type="hidden" value="search_block_form"/>
</div>
</div></form> </div>
</div>
</div>
</div>
</div>
</header>
<div class="contenedor">
<div class="destacados"><div class="uno"> <div class="region region-content-top">
<div class="block block-views" id="block-views-slide-1-block">
<div class="content">
<div class="view view-slide-1 view-id-slide_1 view-display-id-block view-dom-id-e90d8e41df38cfcb32cdbb994e33552c">
<div class="view-content">
<table class="views-view-grid cols-4">
<tbody>
<tr class="row-1 row-first row-last">
<td class="col-1 col-first">
<div> <div><div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/destacada-1/escuela-verano-2021-recibe-m-s-9000-escolares-en-todo-el-pa-s"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2021/noticias/enero/210112/210112_01.jpg" typeof="foaf:Image" width="755"/></a></div></div></div></div> </div>
<div class="views-field views-field-title"> <h5 class="field-content"><a href="/destacada-1/escuela-verano-2021-recibe-m-s-9000-escolares-en-todo-el-pa-s">Escuela de Verano 2021 recibe a más de 9.000 escolares en todo el país</a></h5> </div> </td>
</tr>
</tbody>
</table>
</div>
</div> </div>
</div>
</div>
</div><div class="dos"> <div class="region region-derecha">
<div class="block block-views" id="block-views-clone-of-slide-2-block">
<div class="content">
<div class="view view-clone-of-slide-2 view-id-clone_of_slide_2 view-display-id-block view-dom-id-220b48b039b8f6ec2cfccc39cd9920b3">
<div class="view-content">
<table class="views-view-grid cols-4">
<tbody>
<tr class="row-1 row-first row-last">
<td class="col-1 col-first">
<div> <div><div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/destacada-2y3/inscripci-n-para-alimentaci-n-escolar-se-extendi-hasta-hoy-martes-12-enero-1900-horas"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/logos/iconos/comunicado.jpg" typeof="foaf:Image" width="755"/></a></div></div></div></div> </div>
<div class="views-field views-field-title"> <h5 class="field-content"><a href="/destacada-2y3/inscripci-n-para-alimentaci-n-escolar-se-extendi-hasta-hoy-martes-12-enero-1900-horas">Inscripción para Alimentación Escolar se extendió hasta hoy martes 12 de enero a las 19:00 horas</a></h5> </div> </td>
</tr>
</tbody>
</table>
</div>
</div> </div>
</div>
<div class="block block-views" id="block-views-clone-of-slide-2-block-1">
<div class="content">
<div class="view view-clone-of-slide-2 view-id-clone_of_slide_2 view-display-id-block_1 view-dom-id-64811ec6d28ac80ded6d617b2f143de8">
<div class="view-content">
<table class="views-view-grid cols-4">
<tbody>
<tr class="row-1 row-first row-last">
<td class="col-1 col-first">
<div> <div><div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/destacada-2y3/anep-continuar-brindando-servicio-alimentaci-n-escolar-en-enero-y-febrero"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2021/noticias/enero/210104/210104_01.jpg" typeof="foaf:Image" width="755"/></a></div></div></div></div> </div>
<div class="views-field views-field-title"> <h5 class="field-content"><a href="/destacada-2y3/anep-continuar-brindando-servicio-alimentaci-n-escolar-en-enero-y-febrero">ANEP continuará brindando servicio de alimentación escolar en enero y febrero</a></h5> </div> </td>
</tr>
</tbody>
</table>
</div>
</div> </div>
</div>
</div>
</div></div> <div class="publicos"> <div class="region region-publicos">
<div class="block block-block" id="block-block-2">
<div class="content">
<ul><li><a href="funcionarios-docentes">Funcionarios docentes</a></li>
<li><a href="funcionarios-gestion">Funcionarios de gestión </a></li>
<li><a href="familias-estudiantes">Familias y estudiantes</a></li>
<li><a href="territorios">Territorios</a></li>
</ul> </div>
</div>
</div>
</div>
<div class="contenidos">
<div class="izquierda"> <div class="tabs"> </div>
<div class="region region-content">
<div class="block block-block" id="block-block-49">
<div class="content">
<p style="text-align:center"><a href="https://www.anep.edu.uy/15-d/se-extiende-hasta-el-lunes-11-enero-el-plazo-inscripci-n-al-programa-alimentaci-n-escolar" target="_blank"><img alt="banner alimentacion 03.jpg" height="200" src="/sites/default/files/images/2021/noticias/enero/210111/banner%20alimentacion%2003.jpg" width="766"/></a></p>
</div>
</div>
<div class="block block-system" id="block-system-main">
<div class="content">
<div class="view view-nodequeue-1 view-id-nodequeue_1 view-display-id-page view-dom-id-0ad69308244901bb0d896e046320a615">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div about="/15-d/embajada-israel-don-equipos-inform-ticos-nueve-escuelas-por-un-valor-50000-d-lares" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/embajada-israel-don-equipos-inform-ticos-nueve-escuelas-por-un-valor-50000-d-lares"><img alt="Embajada de Israel donó equipos informáticos a nueve escuelas por un valor de 50.000 dólares" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201216/DSC_6025.JPG" title="Embajada de Israel donó equipos informáticos a nueve escuelas por un valor de 50.000 dólares" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/embajada-israel-don-equipos-inform-ticos-nueve-escuelas-por-un-valor-50000-d-lares">Embajada de Israel donó equipos informáticos a nueve escuelas por un valor de 50.000 dólares</a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>La Embajada de Israel en Uruguay realizó una donación de equipos informáticos a nueve escuelas públicas del Consejo de Educación Inicial y Primaria (CEIP) de la Administración Nacional de Educación Pública (ANEP), en el marco de los proyectos de cooperación en innovación que Israel y Uruguay mantienen.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-2 views-row-even">
<div about="/15-d/germ-n-rama-el-presidente-codicen-que-trabaj-por-reforma-integral-educaci-n-p-blica" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/germ-n-rama-el-presidente-codicen-que-trabaj-por-reforma-integral-educaci-n-p-blica"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201228/rama-1.jpg" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/germ-n-rama-el-presidente-codicen-que-trabaj-por-reforma-integral-educaci-n-p-blica">Germán Rama, el presidente del CODICEN que trabajó por la reforma integral de la educación pública</a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Al finalizar el siglo XX, Rama pensó, elaboró e impulsó una reforma profunda de la educación pública del Uruguay y la concibió como un sistema de protección social.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-3 views-row-odd">
<div about="/15-d/anep-saluda-comunidades-educativas-y-agradece-su-compromiso-con-sociedad" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/anep-saluda-comunidades-educativas-y-agradece-su-compromiso-con-sociedad"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201223/Tarjeta%20Fin%20de%20An%C2%A6%C3%A2o%202021%20web%202.jpg" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/anep-saluda-comunidades-educativas-y-agradece-su-compromiso-con-sociedad">ANEP saluda a las comunidades educativas y agradece su compromiso con la sociedad   </a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>La Administración Nacional de Educación Pública (ANEP) saluda a los alumnos, funcionarios, docentes y las familias, agradeciendo y reconociendo el compromiso de las comunidades educativas para enfrentar la pandemia que nos afecta. Esa actitud, junto a las medidas que fueron instrumentadas, viabilizó la presencialidad en las aulas de la Educación Pública en todo el país.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-4 views-row-even">
<div about="/15-d-covid19informaci-n/nuevos-tel-fonos-para-dar-aviso-nicamente-posibles-casos-covid-19" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d-covid19informaci-n/nuevos-tel-fonos-para-dar-aviso-nicamente-posibles-casos-covid-19"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201230/IMAGEN%2001.jpg" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d-covid19informaci-n/nuevos-tel-fonos-para-dar-aviso-nicamente-posibles-casos-covid-19">Nuevos teléfonos para dar aviso únicamente de posibles casos de COVID-19</a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>El Consejo Directivo Central (CODICEN) de la Administración Nacional de Educación Pública (ANEP) estableció nuevos canales de comunicación para dar aviso ante la detección de posibles casos de coronavirus en centros educativos y dependencias de la ANEP.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-5 views-row-odd">
<div about="/15-d/anep-trabaja-por-transformaci-n-y-profesionalizaci-n-su-gesti-n" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/anep-trabaja-por-transformaci-n-y-profesionalizaci-n-su-gesti-n"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201208/201208_01.jpg" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/anep-trabaja-por-transformaci-n-y-profesionalizaci-n-su-gesti-n">ANEP trabaja por la transformación y profesionalización de su gestión</a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>El Consejo Directivo Central (CODICEN) de la Administración Nacional de Educación Pública (ANEP) trabaja en la transformación de la gestión y la estructura institucional a partir de una profesionalización de los procesos y las funciones correspondientes a sus servicios.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-6 views-row-even">
<div about="/15-d/ni-os-y-ni-escuela-n-87-maldonado-plasman-emociones-y-experiencias-en-un-libro" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/ni-os-y-ni-escuela-n-87-maldonado-plasman-emociones-y-experiencias-en-un-libro"><img alt="Niños y niñas de la Escuela Nº 87 de Maldonado plasman emociones y experiencias en un libro" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201223/201223_01.jpg" title="Niños y niñas de la Escuela Nº 87 de Maldonado plasman emociones y experiencias en un libro" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/ni-os-y-ni-escuela-n-87-maldonado-plasman-emociones-y-experiencias-en-un-libro">Niños y niñas de la Escuela Nº 87 de Maldonado plasman emociones y experiencias en un libro</a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Los docentes de la Escuela N° 87 de Maldonado incentivaron a niños y niñas de entre 4 y 12 años a escribir como forma de canalizar emociones y experiencias sobre la pandemia. A partir de esos textos (orientados a contenidos curriculares) los docentes idearon un libro donde plasmar el sentir de los estudiantes en esta difícil etapa y los 40 años de vida de la Escuela.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
<div class="views-row views-row-7 views-row-odd views-row-last">
<div about="/15-d/se-abrir-n-inscripciones-para-proces-en-febrero" class="ds-2col-stacked node node-noticias- node-teaser view-mode-teaser clearfix" typeof="sioc:Item foaf:Document">
<div class="group-header">
</div>
<div class="group-left">
<div class="field field-name-field-imagen field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/15-d/se-abrir-n-inscripciones-para-proces-en-febrero"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201229/201229_01_ok.jpg" typeof="foaf:Image" width="755"/></a></div></div></div> </div>
<div class="group-right">
<div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even" property="dc:title"><h2><a href="/15-d/se-abrir-n-inscripciones-para-proces-en-febrero">Se abrirán inscripciones para ProCES en febrero </a></h2></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p>Las inscripciones para cursar el Bachillerato en el ProCES durante el primer semestre de 2021 estarán abiertas entre el 9 y el 11 de febrero según el dígito verificador. La convocatoria incluye a todos aquellos funcionarios de la Administración Nacional de Educación Pública (ANEP) que no completaron sus estudios secundarios.</p>
</div></div></div> </div>
<div class="group-footer">
</div>
</div>
</div>
</div>
<div class="more-link">
<a href="/inicio">
    Leer más  </a>
</div>
<div class="view-footer">
<p> </p>
<p><span style="color:#FFFFFF">.</span></p>
<p><span style="color:#FFFFFF">.</span></p>
<p><a href="/noticias">Todas las noticias</a> | <a href="/llamados">Llamados</a> | <a href="/comunicados-avisos">Comunicados</a> | <a href="/codicen/dric/convocatorias">Becas y convocatorias</a> | <a href="/suscripci-n-al-bolet-n-noticias-anep#overlay-context=formulario-para-desuscribirse-bolet-n-noticias-0">Suscripción al boletín de noticias</a> </p>
<p><span style="color:#FFFFFF">.</span></p>
<p><span style="color:#FFFFFF">.</span></p>
</div>
</div> </div>
</div>
</div>
</div>
<div class="derecha">
<div class="region region-agenda">
<div class="block block-block" id="block-block-14">
<div class="content">
<p style="text-align:center"><a href="/corona-virus"><img alt="coronavirus banner ch.jpg" height="200" src="/sites/default/files/images/2020/campanas/coronavirus/coronavirus%20banner%20ch.jpg" width="235"/></a></p>
<p> </p>
</div>
</div>
<div class="block block-menu" id="block-menu-menu-accesos-directos">
<h2>Accesos directos</h2>
<div class="content">
<ul class="menu"><li class="first leaf"><a href="/estadisticas-evaluaciones" title="">Estadísticas y evaluaciones  </a></li>
<li class="leaf"><a href="/publicaciones" title="">Publicaciones</a></li>
<li class="leaf"><a href="http://pcentrales.anep.edu.uy/" title="">Programas y dispositivos</a></li>
<li class="leaf"><a href="/proyectos-operativos" title="">Proyectos operativos</a></li>
<li class="leaf"><a href="/codicen/dci/identidad" title="">Identidad gráfica de la ANEP</a></li>
<li class="last leaf"><a href="/guia-contactos-anep-789-789" title="">Guía de oficinas</a></li>
</ul> </div>
</div>
<div class="block block-menu" id="block-menu-menu-recursos">
<h2>Enlaces de interés</h2>
<div class="content">
<ul class="menu"><li class="first leaf"><a href="https://mcrn.anep.edu.uy/" title="">Marco Curricular de Referencia Nacional</a></li>
<li class="leaf"><a href="https://uruguayeduca.anep.edu.uy/" title="">Uruguay Educa</a></li>
<li class="last leaf"><a href="https://acredita.anep.edu.uy/" title="Prueba de acreditación de Ciclo Básico">AcreditaCB</a></li>
</ul> </div>
</div>
<div class="block block-block" id="block-block-16">
<div class="content">
<p style="text-align:center"><a href="https://uruguayeduca.anep.edu.uy/"><img alt="Logo Uruguay Educa HORT.png" height="49" src="/sites/default/files/images/Institucional/uruguay_educa/Logo%20Uruguay%20Educa%20HORT.png" width="235"/></a></p>
</div>
</div>
</div>
</div>
</div>
<div class="breves"> <div class="region region-content-bottom">
<div class="block block-views" id="block-views-nodequeue-2-block">
<h2>Breves</h2>
<div class="content">
<div class="view view-nodequeue-2 view-id-nodequeue_2 view-display-id-block view-dom-id-80ab066484459360fa3a4f8590019a18">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-field-imagen"> <div class="field-content"><img alt="" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/diciembre/201218/201218_01.jpg" typeof="foaf:Image" width="755"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="/breves-noticias-dsie/estudiantes-melo-comparten-y-cocinan-recetas-familiares">Estudiantes de Melo comparten y cocinan recetas familiares</a></span> </div> </div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-field-imagen"> <div class="field-content"><img alt="Becas 2021 del Fondo de Solidaridad" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/octubre/201008/201008_01.jpg" title="Becas 2021 del Fondo de Solidaridad" typeof="foaf:Image" width="755"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="/breves/becas-2021-fondo-solidaridad">Becas 2021 del Fondo de Solidaridad</a></span> </div> </div>
<div class="views-row views-row-3 views-row-odd">
<div class="views-field views-field-field-imagen"> <div class="field-content"><img alt="La ANEP declara de interés el “Big Bang 2020 Hackaton”" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/noviembre/201118/201118_02.jpg" title="La ANEP declara de interés el “Big Bang 2020 Hackaton”" typeof="foaf:Image" width="755"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="/breves/anep-declara-inter-s-el-big-bang-2020-hackaton">La ANEP declara de interés el “Big Bang 2020 Hackaton”</a></span> </div> </div>
<div class="views-row views-row-4 views-row-even views-row-last">
<div class="views-field views-field-field-imagen"> <div class="field-content"><img alt="Inscripciones abiertas para solicitar la Beca de Educación Média pública" height="390" src="https://www.anep.edu.uy/sites/default/files/images/2020/noticias/noviembre/201106/201106_02.jpg" title="Inscripciones abiertas para solicitar la Beca de Educación Média pública" typeof="foaf:Image" width="755"/></div> </div>
<div class="views-field views-field-title"> <span class="field-content"><a href="/breves/inscripciones-abiertas-para-solicitar-beca-educaci-n-m-dia-p-blica">Inscripciones abiertas para solicitar la Beca de Educación Média pública</a></span> </div> </div>
</div>
</div> </div>
</div>
</div>
</div>
<div class="sitiosrelacionados">
<div class="region region-sitios">
<div class="block block-block" id="block-block-1">
<div class="content">
<ul><li><a href="https://www.ceibal.edu.uy" target="_blank"><img alt="" height="171" src="/sites/default/files/images/logos/logo-plan-ceibal-2020-v.jpg" width="227"/></a></li>
<li><a href="http://redglobal.edu.uy/language/es/inicio/" target="_blank"><img alt="" height="171" src="/sites/default/files/images/logos/logo_redes.png" width="240"/></a></li>
<li><a href="http://snep.edu.uy/" target="_blank"><img alt="" src="/sites/default/files/images/logos/logo_snep.png"/></a></li>
<li><a href="https://www.ineed.edu.uy/" target="_blank"><img alt="" src="/sites/default/files/images/logos/logo_ineed.png"/></a></li>
</ul> </div>
</div>
</div>
</div>
</div>
<footer class="clear">
<div class="footer">
<div class="redes">
<ul>
<li><a href="https://www.facebook.com/ANEPUruguay/"><i aria-hidden="true" class="fa fa-facebook-official"></i></a> </li>
<li><a href="https://twitter.com/ANEP_Uruguay"><i aria-hidden="true" class="fa fa-twitter-square"></i></a></li>
<li><a href="https://www.youtube.com/ANEPuruguay"><i aria-hidden="true" class="fa fa-youtube"></i></a> </li>
<li><a href="https://www.instagram.com/anep_uruguay/"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
<li><a href="https://uy.linkedin.com/company/anep"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
<li><a href="https://soundcloud.com/codicen-anep"><i aria-hidden="true" class="fa fa-soundcloud"></i></a></li>
</ul>
</div>
<div class="derechos"> © 2018 Administración Nacional de Educación Pública  |  Av. Libertador 1409 CP 11.100  |  Tel. (+598) 2900 7070  |  Montevideo - Uruguay</div></div>
</footer>
</div>
</body></html> 