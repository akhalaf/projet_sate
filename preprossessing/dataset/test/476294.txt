<!DOCTYPE html>
<html lang="en">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="" name="description"/>
<meta content="" name="keywords"/>
<meta content="initial-scale=1.0001" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://www.emoryacu.com/personal-branch.php%09%0A" rel="canonical"/>
<title>Page Not Found (Error 404) - Emory Alliance Credit Union</title>
<meta content="" property="og:description"/>
<meta content="Page Not Found (Error 404)" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="https://www.emoryacu.com/personal-branch.php%09%0A" property="og:url"/>
<!-- jQuery & bootstrap -->
<script src="/templates/COMMON_JS/jquery-3.4.1.min.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&amp;display=swap" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="/templates/emory_all/js/jqueriness.js"></script>
<!-- digital marketing -->
<!-- universal styles -->
<link href="/admin/css/universal_template.css" rel="stylesheet"/>
<!-- Locator Module Header Code Start -->
<link href="/templates/COMMON_JS/CSS/new_loc.css" rel="stylesheet" type="text/css"/>
<link href="/templates/COMMON_JS/CSS/new_loc_c19.css" rel="stylesheet" type="text/css"/>
<!-- Locator Module Header Code End -->
<!-- Tables Module Header Code Start -->
<link href="/templates/COMMON_JS/CSS/default_table.css" rel="stylesheet" type="text/css"/>
<!-- Tables Module Header Code End -->
<!-- Video Module Header Code Start -->
<!-- Video.JS Header Code Start -->
<link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet"/>
<!-- Video.JS Header Code End -->
<!-- audio.js Header Code Start -->
<script src="/admin/js/audiojs/audio.js"></script>
<!-- audio.js Header Code End -->
<!-- Video Module Header Code End -->
<!-- Forms Module Header Code Start -->
<link href="/templates/COMMON_JS/CSS/default_form.css" rel="stylesheet" type="text/css"/>
<link href="/templates/COMMON_JS/CSS/default_form_side.css" rel="stylesheet" type="text/css"/>
<!-- Forms Module Header Code End -->
<!--- Tracking Pixel "Reach Local Pixel" (id: 3) Begin -->
<script async="async" src="//cdn.rlets.com/capture_configs/c64/ebc/aa0/98f44ea85dfcf2447f9e3f2.js" type="text/javascript"></script>
<!--- Tracking Pixel "Reach Local Pixel" (id: 3) End -->
<!-- Generic CMS Styles Start -->
<style>UL.content_simple_gallery{list-style:none}UL.content_simple_gallery LI{display:inline-block;margin-right:10px}LI.simple_gallery_view_more{vertical-align:middle;margin-left:40px}</style>
<!-- Generic CMS Styles End -->
<!-- font awesome -->
<link href="/templates/COMMON_JS/fontawesome-pro-5.8.1-web/css/all.min.css" rel="stylesheet"/>
<!-- generic frontend scripting -->
<script src="/admin/js/frontend.js" type="text/javascript"></script>
<!-- for validating forms -->
<script src="/form_system/js/uniValidate.js" type="text/javascript"></script>
<script async="" defer="" src="https://www.google.com/recaptcha/api.js"></script>
<!-- Google Analytics -->
<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-28833378-1', 'www.emoryacu.com');
		ga('send', 'pageview');

		</script>
<!-- End Google Analytics -->
<link href="/templates/emory_all/images/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/templates/emory_all/images/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/templates/emory_all/images/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/templates/emory_all/images/icons/site.webmanifest" rel="manifest"/>
<link color="#860038" href="/templates/emory_all/images/icons/safari-pinned-tab.svg" rel="mask-icon"/>
<link href="/templates/emory_all/images/icons/favicon.ico" rel="shortcut icon"/>
<meta content="#860038" name="msapplication-TileColor"/>
<meta content="/templates/emory_all/images/icons/browserconfig.xml" name="msapplication-config"/>
<meta content="#860038" name="theme-color"/>
<!-- STYLESHEETS -->
<link href="https://use.typekit.net/aqu8yis.css" rel="stylesheet"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/templates/emory_all/css/style.css?version=20210112101822" rel="stylesheet" type="text/css"/>
<!-- for menus -->
<!-- for external links -->
<script language="JavaScript">
		function leave_site() {
		return window.confirm('You are leaving the Emory Alliance Credit Union Web site. The Web site you have selected is an external one located on another server.  Emory Alliance Credit Union has no responsibility for any external Web site.  It neither endorses the information, content, presentation, or accuracy nor makes any warranty, express or implied, regarding any external site. Thank you for visiting Emory Alliance Credit Union.')
		}
	</script>
</head>
<body>
<!-- digital marketing -->
<!-- MJ's INFO -->
<section aria-label="notification" class="nopad" role="complementary">
<noscript>This page uses JavaScript. Your browser either does not support JavaScript or you have it turned off. To see this page properly please use a JavaScript enabled browser.</noscript>
</section>
<!-- ALERT -->
<style type="text/css">
						.visuallyhidden {border: 0; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}

						#alert11{background-color: #B90A04; color: #FFFFFF}
						#alert11 a, #alert11 a:visited, #alert11 button.exed {color: #FFFFFF;font-weight: normal;text-decoration: underline;}
						#alert11 a:hover, #alert11 a:focus {font-weight: normal;text-decoration: underline;}

						#alert11 a:hover, #alert11 a:focus, #alert11 button.exed:hover, #alert11 button.exed:focus{color: #FBD600}
					</style>
<div class="alertholder"><div aria-label="alert" class="alertbar high" id="alert11" role="notification"><div class="alert"><div class="alert_icon"></div><p>For info regarding stimulus payments <a href="/about-emory-alliance/news-and-events#stimulus">Click Here</a>.</p>
<button aria_label="Close Alert" class="exed" id="exed11" which="11"><span class="visuallyhidden">Close Alert</span></button></div></div></div><script src="/templates/COMMON_JS/jquery.cookie.js"></script><script>
					$("button#exed11").click(function(){
						var which = $(this).attr("which");
						$("#alert"+which).slideUp("fast");
						});
					</script>
<div class="contentwrapper">
<!-- SMARTER BANNER -->
<header><div class="liner">
<!-- SKIPNAV -->
<a class="skipper" href="#maincontent" tabindex="0">Go to main content</a>
<!-- TOPLINKS -->
<div class="toplinks" role="navigation">
<a class="related_link" href="https://www.cmg.loanliner.com/loanrequest/presenter/LoanList/01005127/887?isframed=F&amp;cuid=01005127&amp;loanlistid=887&amp;channelid=NONE&amp;locationid=NONE" id="related_link_1" target="_blank">APPLY FOR A LOAN</a> <span class="pipe">|</span> <a class="related_link" href="https://accountcreate.fiservapps.com/App/AC_8020/App.aspx?AppID=11001" id="related_link_3" onclick="return leave_site()" target="_blank">JOIN TODAY</a> <span class="pipe">|</span> <a class="related_link" href="/about-emory-alliance/who-is-emory-alliance" id="related_link_9">ABOUT US</a> <span class="pipe lastpipe">|</span>
</div>
<!-- HOMELINK -->
<a class="homelink" href="/"><img alt="Emory Alliance Credit Union" src="/templates/emory_all/images/logo.svg"/></a>
<!-- MENU TOGGLER -->
<button aria-controls="mobimenu" aria-expanded="false" aria-label="Open/Close Menu" class="menutoggler"></button>
<!-- MENU -->
<nav class="pc">
<div class="navliner"><button aria-controls="CMc1" aria-expanded="false" aria-label="Open/Close Products Section" class="CMitem" id="CMi1" rel="1">Products</button><div class="ddcontent" id="CMc1"><div class="row"><div class="col-12 col-md-4">
<p><span class="menuH">Accounts</span></p>
<p><a href="/products/accounts/checking-accounts">Checking Accounts</a></p>
<p><a href="/accounts/savings">Savings</a></p>
<p><a href="/products/accounts/certificates-of-deposits-and-iras">Certificates of Deposits and IRAs</a></p>
<p><a href="/accounts/club-accounts">Club Accounts</a></p>
<p><a href="/accounts/money-market-accounts">Money Market Accounts</a></p>
<p><a href="/products/accounts/organizational-accounts">Organizational Accounts</a></p>
<p><span class="menuH">Life Stages</span></p>
<p><a href="/products/accounts/advantage-accounts">Advantage (age 62+)</a></p>
<p><a href="/products/accounts/college-essentials-package">College Essentials Package</a></p>
<p><a href="/accounts/second-chance-program">Second Chance Program</a></p>
<p><a href="/accounts/youth-accounts">Youth Accounts</a></p>
</div><div class="col-12 col-md-4">
<p><span class="menuH">Loans &amp; Credit Cards</span></p>
<p><a href="/loans-and-credit-cards/consumer-loans">Consumer Loans</a></p>
<ul>
<li><a href="/loans-and-credit-cards/personal-loans/promotions">Promotions</a></li>
<li><a href="/loans-and-credit-cards/personal-loans/vehicle-loans">Vehicle Loans</a></li>
<li><a href="/loans-and-credit-cards/personal-loans/personal-loans">Personal Loans</a></li>
<li><a href="/loans-and-credit-cards/consumer-loans/visaandreg-credit-cards">Visa® Credit Cards</a></li>
<li><a href="/loans-and-credit-cards/personal-loans/student-loans">Student Loans</a></li>
</ul>
<p><a href="/loans-and-credit-cards/home-loans">Home Loans</a></p>
<ul>
<li><a href="/loans-and-credit-cards/home-loans/home-equity-products-and-second-mortgage-">Home Equity Products and Second Mortgage</a></li>
<li><a href="/loans-and-credit-cards/home-loans/mortgages">Mortgages</a></li>
</ul>
</div><div class="col-12 col-md-4">
<p><span class="menuH">Investments &amp; Retirement</span></p>
<p><a href="/products/investments-and-retirement-accounts">Alliance Retirement &amp; Investment Services</a></p>
</div></div>
</div><button aria-controls="CMc2" aria-expanded="false" aria-label="Open/Close Online Section" class="CMitem" id="CMi2" rel="2">Online</button><div class="ddcontent" id="CMc2"><p><a href="/online-services/online-banking">Online Banking</a></p>
<p><a href="/online-services/mobile-banking">Mobile Banking</a></p>
<p><a href="/online-services/cardguard">CardGuard</a></p>
<p><a href="/online-services/estatements">eStatements</a></p>
<p><a href="/Zelle/index">Zelle</a></p>
<p><a href="/online/faqs">FAQs</a></p>
</div><button aria-controls="CMc3" aria-expanded="false" aria-label="Open/Close Resources Section" class="CMitem" id="CMi3" rel="3">Resources</button><div class="ddcontent" id="CMc3"><div class="row"><div class="col-12 col-md-6">
<p><a href="/tools-and-resources/audio-xpress">Audio Xpress</a></p>
<p><a href="/tools-and-resources/financial-education/balance-financial-education">BALANCE Financial Education</a></p>
<p><a href="/tools-and-resources/coffee-talk-blog">Coffee Talk Blog</a></p>
<p><a href="/resources/coin-counter">Coin Counter</a></p>
<p><a href="/tools-and-resources/member-benefits/credit-report-resources">Credit Report Resources</a></p>
<p><a href="/about-emory-alliance/community-partners">Community Partners</a></p>
<p><a href="/tools-and-resources/financial-education/faqs">FAQs</a></p>
<p><a href="/resources/fees">Fee Schedule</a> </p>
</div><div class="col-12 col-md-6">
<p><a href="http://emoryacu.calculators.finresourcecenter.com/Financial_Calculators_187521.html" target="_blank">Financial Calculators</a></p>
<p><a href="/tools-and-resources/forms-and-disclosures">Forms &amp; Disclosures</a></p>
<p><a href="/resources/fraud-and-identity-theft">Fraud &amp; Identity Theft</a></p>
<p><a href="/tools-and-resources/member-benefits/insurance">Insurance</a></p>
<p><a href="/resources/legalshield">LegalShield</a></p>
<p><a href="/tools-and-resources/member-rewards">Member Rewards</a></p>
<p><a href="/tools-and-resources/member-benefits/visaandreg-gift-cards">VISA<sup>®</sup>Gift Cards</a></p>
<p><a href="/tools-and-resources/wiring-instructions">Wiring Instructions</a></p>
</div></div>
</div></div> </nav>
<!-- SEARCH BUTTON--><a name="nav"></a>
<button aria-controls="searchthesite" aria-expanded="false" aria-label="Open/Close the Search" id="sitesearch"></button>
<!-- SEARCH FORM-->
<div aria-label="Search this Site" class="sitesearch" id="searchthesite" role="search">
<form action="/search" method="get" name="sitesearch" onsubmit="return check_search();">
<label class="visuallyhidden" for="thesearch"> Search: </label><input class="searchbox" id="thesearch" name="q" placeholder="Search" type="text"/>
<button aria-label="Search" type="submit"><span class="far fa-search"></span></button>
</form>
</div>
</div></header>
<!-- HB BUTTON -->
<button aria-controls="OnlineBanking" aria-expanded="false" aria-label="Open/Close Online Banking" id="hbbttn">Online Banking lOGIN</button>
<!-- HB -->
<script type="text/javascript">
       
        function changeLanguage() {
               if (document.getElementById("cultureName").value == "es")
               {
                       document.getElementById("enrollmentLink").href =
                            "https://cert.emoryacu.com/auth/Enrollment?CultureName=es";
                       document.getElementById("forgottenPasswordLink").href =
                            "https://cert.emoryacu.com/auth/ForgottenPassword?CultureName=es";
					   document.getElementById("recoverUserNameLink").href =
						   "https://cert.emoryacu.com/auth/RecoverUserName?CultureName=es";
               }
               else 
               {
                       document.getElementById("enrollmentLink").href =
                           "https://cert.emoryacu.com/auth/Enrollment?CultureName=en";
                       document.getElementById("forgottenPasswordLink").href =
                           "https://cert.emoryacu.com/auth/ForgottenPassword?CultureName=en";
					   document.getElementById("recoverUserNameLink").href =
						   "https://cert.emoryacu.com/auth/RecoverUserName?CultureName=en";
               }
        }
</script>
<style type="text/css">

	#hbbttn{text-transform: uppercase;   width: 240px; height: 44px;  border-radius: 22px; background-color:#106186; color:white;position: absolute; top: 60px; right: 50%; margin-right: -615px; z-index: 99 }

	#hbbttn:hover, #hbbttn:focus {background-color: #860038}
	#hbbttn:before {content: '\f329'; font-family: 'Font Awesome 5 Pro'; font-weight: 400; margin-right: 10px;}
	#hbbttn.down:before {content: '\f32c';}
	#hbbttn.stuck, .hb.stuck{position: fixed;}
	
	.hb{display:none; box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.25);  background-color: #ffffff;position: absolute; top: 119px; right: calc(50% - 615px); padding:  40px; width:300px; z-index: 10}
	.hbtitle{font-family: athelas, serif;  font-weight: 400; letter-spacing: -0.25px; font-size:  26px; line-height: 32px; margin: 0 0 14px 0; color: #860038}

	.hb form {display: inline;}
	.hb input{width: 100%; border:solid 1px #222222;  height: 40px;  border-radius: 20px;margin-bottom: 10px;}
	.hb input[type=text], .hb input[type=password]{padding-left: 20px;}
	.hb input[type=submit]{text-transform: uppercase; background-color:#106186;; color:white; border:0}
	.hb input[type=submit]:hover, .hb input[type=submit]:focus{background-color: #860038}
	
	.hblinks{}
	.hblinks a, .hblinks a:visited{}
	.hblinks a:hover, .hblinks a:focus{}


	/*XL*/
	@media only screen and (max-width: 1300px) {
		.hb{ right: 40px;  margin-right: 0;}
		#hbbttn{ right: 40px; margin-right: 0; }
	}

	/*L*/
	@media only screen and (max-width: 1200px) {
	}

	/*M*/
	@media only screen and (max-width: 1040px) {
		#hbbttn{ margin-right: 64px; }
	}

	/*S*/
	@media only screen and  (max-width: 768px) {
		#hbbttn{ width:100%; height: 44px;  border-radius: 0; top: 120px; left: 0; z-index: 90}
		.hb{ top: 164px; right: 0; padding: 20px 15px; width: 100%;}
		.hblinks br{display: none;}
		.hblinks a{ margin: 0 10px;}

	}


	/*XS*/
	@media only screen and  (max-width: 576px) {
	}

</style>
<div aria-label="Online Banking" class="hb" id="OnlineBanking" role="form">
<div class="hbtitle">Online Banking</div>
<form action="https://online.emoryacu.com/auth/SignIn?wa=wsignin1.0&amp;wtrealm=https://online.emoryacu.com/banking/&amp;wctx=rm=0&amp;id=passive&amp;ru=/banking/" autocomplete="off" method="post" target="_top">
<label class="visuallyhidden" for="UserName" id="UserName-label">Username</label>
<input id="UserName" name="UserName" placeholder="Username" type="text"/>
<label class="visuallyhidden" for="Password" id="Password-label">Password</label>
<input id="Password" name="Password" placeholder="Password" size="8" type="password"/>
<input name="submit" type="submit" value="Sign In"/>
</form>
<div class="hblinks">
<a href="https://online.emoryacu.com/auth/ForgottenPassword" id="forgottenPasswordLink" target="_top">Forgot Password</a><br/>
<a href="https://online.emoryacu.com/auth/Enrollment" id="enrollmentLink" target="_top">Enroll</a><br/>
<a href="https://online.emoryacu.com/auth/ForgottenUserId" id="recoverUserNameLink" target="_top">Recover UserName</a>
</div>
</div> <section class="intbanner" role="complementary" style="background-image: url();">
<div class="overlay">
<div class="liner">
<div class="inttext"><h1>
				Page Not Found (Error 404)				</h1></div>
</div>
</div>
</section>
<section class="inside">
<article aria-label="Main Content" id="maincontent" role="main">
<!-- CONTENT -->
<h1>Page Not Found</h1>
<p>The page you requested may have been moved or deleted.<br/>
Please try one of the following:</p>
<ul>
<li>If you bookmarked the page, please use the search at the top right corner to find what you are looking for.</li>
<li>If you typed in the address, be sure it was entered correctly.</li>
<li>If you clicked on a link from another page, use your back button and try again.</li>
<li>If you still need help finding what you are looking for, visit our <a href="/site-map">site map</a>.</li>
</ul>
<a class="skipper" href="#nav">Go to main navigation</a>
<div class="breaker"></div>
<!-- ICONS -->
<section class="icons" role="complementary">
<div class="liner effect ">
<div class="iconbox">
<span class="RLwords">Locations</span><br/>
<a aria-label="Locations" href="/about-emory-alliance/locations-and-hours">
<div class="far fa-map-marker-alt RLicon"></div>
<span class="RLwords2">Locations</span>
</a>
</div><div class="iconbox">
<span class="RLwords">News</span><br/>
<a aria-label="News" href="/about-emory-alliance/news-and-events">
<div class="far fa-newspaper RLicon"></div>
<span class="RLwords2">News</span>
</a>
</div><div class="iconbox">
<span class="RLwords">Join</span><br/>
<a aria-label="Join" href="https://accountcreate.fiservapps.com/App/AC_8020/App.aspx?AppID=11001" onclick="return leave_site()" target="_blank">
<div class="far fa-users RLicon"></div>
<span class="RLwords2">Join</span>
</a>
</div><div class="iconbox">
<span class="RLwords">Calculators</span><br/>
<a aria-label="Calculators" href="http://emoryacu.calculators.finresourcecenter.com/Financial_Calculators_187521.html" onclick="return leave_site()" target="_blank">
<div class="far fa-calculator RLicon"></div>
<span class="RLwords2">Calculators</span>
</a>
</div><div class="iconbox">
<span class="RLwords">FAQs</span><br/>
<a aria-label="FAQs" href="/tools-and-resources/financial-education/faqs">
<div class="far fa-question-circle RLicon"></div>
<span class="RLwords2">FAQs</span>
</a>
</div>
</div>
</section>
</article>
</section>
<!-- FOOTER -->
<footer>
<div class="row liner">
<div class="bottomlinks d-block d-md-flex justify-content-between align-items-center">
<!-- BOTTOMLINKS -->
<span class="notalink">Routing #261172308</span><span class="notalink">NMLS #464317 </span><a class="related_link" href="/about-emory-alliance/contact-us" id="related_link_7">CONTACT US</a> <a class="related_link" href="/privacy-policy" id="related_link_11">PRIVACY POLICY</a> <a class="related_link" href="/site-map" id="related_link_13">SITE MAP</a> <div class="socmed"><a aria-label="Facebook" class="related_link" href="https://www.facebook.com/EmoryACU/" id="related_link_19" onclick="return leave_site()" target="_blank"><span class="fab fa-facebook-f RLicon"></span> <span class="RLwords">Facebook</span></a> <a aria-label="Twitter" class="related_link" href="https://twitter.com/EmoryACU" id="related_link_21" onclick="return leave_site()" target="_blank"><span class="fab fa-twitter RLicon"></span> <span class="RLwords">Twitter</span></a> <a aria-label="Instagram" class="related_link" href="https://www.instagram.com/emoryalliancecu/" id="related_link_23" onclick="return leave_site()" target="_blank"><span class="fab fa-instagram RLicon"></span> <span class="RLwords">Instagram</span></a> <a aria-label="Linkedin" class="related_link" href="https://www.linkedin.com/company/emoryacu/" id="related_link_25" onclick="return leave_site()" target="_blank"><span class="fab fa-linkedin-in RLicon"></span> <span class="RLwords">Linkedin</span></a> </div>
</div>
<div class="socmed2"><a aria-label="Facebook" class="related_link" href="https://www.facebook.com/EmoryACU/" id="related_link_19" onclick="return leave_site()" target="_blank"><span class="fab fa-facebook-f RLicon"></span> <span class="RLwords">Facebook</span></a> <a aria-label="Twitter" class="related_link" href="https://twitter.com/EmoryACU" id="related_link_21" onclick="return leave_site()" target="_blank"><span class="fab fa-twitter RLicon"></span> <span class="RLwords">Twitter</span></a> <a aria-label="Instagram" class="related_link" href="https://www.instagram.com/emoryalliancecu/" id="related_link_23" onclick="return leave_site()" target="_blank"><span class="fab fa-instagram RLicon"></span> <span class="RLwords">Instagram</span></a> <a aria-label="Linkedin" class="related_link" href="https://www.linkedin.com/company/emoryacu/" id="related_link_25" onclick="return leave_site()" target="_blank"><span class="fab fa-linkedin-in RLicon"></span> <span class="RLwords">Linkedin</span></a> </div>
<!-- EHL & NCUA -->
<div class="footnote flex-column flex-sm-row mx-auto d-flex justify-content-center align-items-center align-content-center" role="contentinfo">
<div class="FNimg"><a href="/go.php?bid=7" id="icon7" onclick="return leave_site();" target="_blank"><img alt="NCUA" src="/files/emoryacu/1/banners/NCUA_7.svg"/></a><a href="/go.php?bid=9" id="icon9" onclick="return leave_site();" target="_blank"><img alt="Equal Housing Lender" src="/files/emoryacu/1/banners/EHL_9.svg"/></a> </div>
<div class="FNtext text-sm-left mt-4 mt-sm-0 text-center"><p>This Credit Union is federally-insured by the National Credit Union Administration.<br/>
We do business in accordance with the Fair Housing Law and Equal Opportunity Credit Act.</p>
</div>
</div>
</div>
</footer>
<!-- SIDE BUTTONS -->
<div class="sidebuttons">
<a aria-label="Contact Us" href="/about-emory-alliance/contact-us"><span class="far fa-envelope"></span></a>
<button aria-controls="phonelisting" aria-expanded="false" aria-label="Open/Close Phone Listing" id="phonebttn"></button>
<a aria-label="Locations" href="/about-emory-alliance/locations-and-hours"><span class="far fa-map-marker-alt"></span></a>
</div>
</div>
<!-- PHONE LISTING -->
<div class="phonelisting" id="phonelisting"><p class="bluetitle"><span style="font-size:18px;">Phone Directory</span></p>
<table border="0" cellpadding="1" cellspacing="1" style="width:100%;">
<tbody>
<tr>
<td>Emory Alliance CU Call Center</td>
<td>404.329.6415</td>
</tr>
<tr>
<td>    Loan Center</td>
<td>Option 3</td>
</tr>
<tr>
<td>    Online Banking Administrator</td>
<td>Option 5</td>
</tr>
<tr>
<td>    Existing Account Questions</td>
<td>Option 6</td>
</tr>
<tr>
<td>Alliance Retirement &amp; Investment Services</td>
<td>404.486.4324</td>
</tr>
<tr>
<td>Audio Xpress - 24/7 Access</td>
<td>404.329.6420</td>
</tr>
<tr>
<td>Card Services Administrator</td>
<td>404.486.4327</td>
</tr>
<tr>
<td>Collections</td>
<td>404.486.4319</td>
</tr>
<tr>
<td>Lost/Stolen Credit Card (24/7)</td>
<td>855.427.3351</td>
</tr>
<tr>
<td>Lost/Stolen Debit Card (24/7)</td>
<td>800.472.3272</td>
</tr>
<tr>
<td>Mortgage Loan Department</td>
<td>404.486.4317</td>
</tr>
<tr>
<td> </td>
<td> </td>
</tr>
<tr>
<td>
<p class="bluetitle">Fax Numbers</p>
</td>
<td> </td>
</tr>
<tr>
<td>Loan Department</td>
<td>404.315.9242</td>
</tr>
<tr>
<td>Call Center</td>
<td>404.329.6423</td>
</tr>
</tbody>
</table>
</div>
<!-- MOBILE MENU -->
<nav class="mobile" id="mobimenu">
<button aria-controls="mobimenu" aria-expanded="true" aria-label="Close Mobile Menu" class="menutoggler2"><span class="far fa-times"></span></button>
<button aria-controls="MM17" aria-expanded="false" aria-label="Open/Close Products Section" class="L1 d-block" which="17">Products</button><div class="L2 py-1 px-2" id="MM17"><button aria-controls="MM133" aria-expanded="false" aria-label="Open/Close Accounts Section" class="d-block" which="133">Accounts</button><div class="L3 p-2" id="MM133"><a href="/products/accounts/checking-accounts" target="_self">Checking Accounts</a><a href="/accounts/savings" target="_self">Savings</a><a href="/products/accounts/certificates-of-deposits-and-iras" target="_self">Certificates of Deposits and IRAs</a><a href="/accounts/club-accounts" target="_self">Club Accounts</a><a href="/accounts/money-market-accounts" target="_self">Money Market Accounts</a><a href="/products/accounts/organizational-accounts" target="_self">Organizational Accounts</a><a href="/products/accounts/advantage-accounts" target="_self">Advantage Accounts</a><a href="/products/accounts/college-essentials-package" target="_self">College Essentials Package</a><a href="/accounts/second-chance-program" target="_self">Second Chance Program</a><a href="/accounts/youth-accounts" target="_self">Youth Accounts</a></div><button aria-controls="MM19" aria-expanded="false" aria-label="Open/Close Loans &amp; Credit Cards Section" class="d-block" which="19">Loans &amp; Credit Cards</button><div class="L3 p-2" id="MM19"><a href="/loans-and-credit-cards/consumer-loans" target="_self">Consumer Loans</a><a href="/loans-and-credit-cards/home-loans" target="_self">Home Loans</a><a href="/loans-and-credit-cards/small-business-loans" target="_self">Small Business Loans</a></div><a class="d-block" href="/products/investments-and-retirement-accounts" target="_self">Investment &amp; Retirement Accounts</a></div><button aria-controls="MM21" aria-expanded="false" aria-label="Open/Close Online Section" class="L1 d-block" which="21">Online</button><div class="L2 py-1 px-2" id="MM21"><button aria-controls="MM79" aria-expanded="false" aria-label="Open/Close Online Banking Section" class="d-block" which="79">Online Banking</button><div class="L3 p-2" id="MM79"><a href="/online-services/online-banking/bill-pay" target="_self">Bill Pay</a></div><a class="d-block" href="/online-services/mobile-banking" target="_self">Mobile Banking</a><a class="d-block" href="/online-services/cardguard" target="_self">CardGuard</a><a class="d-block" href="/online-services/estatements" target="_self">eStatements</a><a class="d-block" href="/Zelle/index" target="_self">Zelle® </a><a class="d-block" href="/online/faqs" target="_self">FAQs</a></div><button aria-controls="MM23" aria-expanded="false" aria-label="Open/Close Resources Section" class="L1 d-block" which="23">Resources</button><div class="L2 py-1 px-2" id="MM23"><a class="d-block" href="/tools-and-resources/audio-xpress" target="_self">Audio Xpress</a><a class="d-block" href="/tools-and-resources/financial-education/balance-financial-education" target="_self">BALANCE Financial Education</a><a class="d-block" href="/tools-and-resources/coffee-talk-blog" target="_self">Coffee Talk Blog</a><a class="d-block" href="/resources/coin-counter" target="_self">Coin Counter</a><a class="d-block" href="/about-emory-alliance/community-partners" target="_self">Community Partners</a><a class="d-block" href="/tools-and-resources/member-benefits/credit-report-resources" target="_self">Credit Report Resources</a><a class="d-block" href="/tools-and-resources/financial-education/faqs" target="_self">FAQs</a><a class="d-block" href="/rates/fee-schedule" target="_self">Fees</a><a class="d-block" href="/tools-and-resources/member-benefits/foreign-currency" target="_self">Foreign Currency</a><a class="d-block" href="/tools-and-resources/forms-and-disclosures" target="_self">Forms &amp; Disclosures</a><a class="d-block" href="/resources/fraud-and-identity-theft" target="_self">Fraud and Identity Theft</a><a class="d-block" href="/tools-and-resources/member-benefits/insurance" target="_self">Insurance</a><a class="d-block" href="/resources/legalshield" target="_self">LegalShield</a><a class="d-block" href="/tools-and-resources/member-rewards" target="_self">Member Rewards</a><a class="d-block" href="/tools-and-resources/member-benefits/visaandreg-gift-cards" target="_self">Visa®  Gift Cards</a><a class="d-block" href="/tools-and-resources/wiring-instructions" target="_self">Wiring Instructions</a></div><button aria-controls="MM25" aria-expanded="false" aria-label="Open/Close About Emory Alliance Section" class="L1 d-block" which="25">About Emory Alliance</button><div class="L2 py-1 px-2" id="MM25"><a class="d-block" href="/about-emory-alliance/who-is-emory-alliance" target="_self">About Emory Alliance Credit Union</a><a class="d-block" href="/about-emory-alliance/become-a-member" target="_self">Become a Member</a><a class="d-block" href="/about-emory-alliance/locations-and-hours" target="_self">Locations &amp; Hours</a><a class="d-block" href="/about-emory-alliance/contact-us" target="_self">Contact Us</a><a class="d-block" href="/about-emory-alliance/news-and-events" target="_self">News &amp; Events</a></div> <div class="TL"><a class="related_link" href="https://www.cmg.loanliner.com/loanrequest/presenter/LoanList/01005127/887?isframed=F&amp;cuid=01005127&amp;loanlistid=887&amp;channelid=NONE&amp;locationid=NONE" id="related_link_1" target="_blank">APPLY FOR A LOAN</a> <a class="related_link" href="https://accountcreate.fiservapps.com/App/AC_8020/App.aspx?AppID=11001" id="related_link_3" onclick="return leave_site()" target="_blank">JOIN TODAY</a> <a class="related_link" href="/about-emory-alliance/who-is-emory-alliance" id="related_link_9">ABOUT US</a> </div>
</nav>
<!-- digital marketing -->
<!-- Video Module Footer Code Start -->
<!-- Video.JS Footer Code Start -->
<script src="/templates/COMMON_JS/video-js-modified.js"></script>
<script src="/admin/js/videojs.ga.js"></script>
<script src="/admin/js/videojs.cuepoints.js"></script>
<script>
$(document).ready(function() {
    $('.video-js').each(function() {
        var my_id = this.id
        var my_vidid = $(this).attr('data-vidid')
        videojs(my_id, { "fluid": true }, function() {
            this.ga();
        }).ready(function() {
            this.cuepoints();
            this.addCuepoint({
                namespace: "logger",
                start: 0,
                end: 5,
                onStart: function(params) {},
                onEnd: function(params) {
                    var data_string = 'id=' + my_vidid;
                    $.ajax({
                        type: 'POST',
                        url: '/admin/frontend/video_log.php',
                        data: data_string,
                        dataType: 'text',
                        success: function(response) {
                            return true;
                        },
                        error: function (XMLHttpRequest, text_status, error_thrown) {
                        }
                    });
                },
                params: {error: false}
            });
        });
    });
});
</script>
<!-- Video.JS Footer Code End -->
<!-- Video Module Footer Code End -->
<!-- AudioEye -->
<script src="/admin/js/AudioEye.js"></script>
<!-- IE11, Edge, FF, Safari, Chrome, Safari Mobile, Chrome Mobile -->
</body>
</html>
