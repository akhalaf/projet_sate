<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<link href="style-static.css" rel="stylesheet"/> <!-- fichier css normal a prendre en reference -->
<meta content="yoga counselling and therapy trish david" name="description"/>
<meta content="integral yoga hatha yoga pregnancy yoga kids yoga children yoga workplace yoga workplace counselling corporate yoga corporate counselling 
		meditation mindfulness yoga therapy alternative therapy grief and loss yoga school yoga classes yoga teacher classic yoga therapeutic yoga in schools
		yoga gold coast queensland australia ashmore molendinar surfers paradise waters bundall benowa carrara highland park nerang tricia trish david" name="keywords"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Yoga Therapy and Counselling Gold Coast Trish David</title>
</head>
<body>
<header> <!-- image et lien banner centrée -->
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="style-static.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Smiling heart yoga</title>
<div class="entete">
<a href="index.php"><p class="styleentete">Smiling  Heart  Yoga  &amp; Therapy</p></a>
<p class="styleentete2">Trish David</p>
</div>
</header>
<center><img alt="yoga therapy" src="photos/logograph.gif" width="150px"/></center> <!-- Logo pour faire joli  -->
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="style-static.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Yoga Therapy Counselling Trish David </title>
<nav>
<center>
<ul><li><a href="index.php"><strong>Home</strong></a></li>
<li><a href="teacher2.php"><strong>Therapist &amp; Musings</strong></a></li>
<li><a href="therapy2.php"><strong>Yoga Therapy</strong></a></li>
<li><a href="workshops2.php"><strong>Workshops &amp; Classes</strong></a></li>
<!--<li><a href="timetable2.php"><strong>Time table</strong></a></li>	-->
<li><a href="contact.php"><strong>Contact us</strong></a></li>
</ul>
</center>
</nav>
<!-- liens de navigation -->
<!----------------------------------------------------------------->
<p class="style">Nurturing  your  holistic  well-being</p>
<div class="photo-title">
<div class="photo-title-flou1"><img alt="meditation gold coast" border="0" src="photos/boulders300.jpg" width="100%"/></div>
<div class="photo-title-flou2"><img alt="counselling gold coast" border="0" src="photos/Studio300.jpg" width="100%"/></div>
<div class="photo-title-flou3"><img alt="yoga therapy gold coast" border="0" src="photos/ChildsPose300.jpg" width="100%"/></div>
<div class="photo-text1"><p><strong>" You need only a prayerful heart, a loving heart,<br/> a grateful heart. "<br/>
            Osho</strong></p>
</div>
<div class="photo-text2"><p><br/><br/><br/><strong>" It is only with ones heart that one can see clearly.<br/> What is essential is invisible to the eye. "<br/>
            Antoine de Saint Exupery</strong></p>
</div>
<div class="photo-text3"><p><strong>" Go placidly amid the noise and haste . . . "<br/> 
            Desiderata</strong></p>
</div>
</div>
<footer>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="style-static.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Smiling Heart Yoga Trish David logos</title>
<center>
<p align="center" class="texte">
		Smiling Heart Yoga Therapy Trish David      <a href="mailto:smilingheart@optusnet.com.au">smilingheart@optusnet.com.au</a>   Gold Coast Queensland Australia  
		</p>
</center>
<center>
<!--   liens vers association   yoga australia                   -->
<a href="https://www.yogaaustralia.org.au/teachers/trish-david?place=ashmore-qld-4214/" target="_blank"><img alt="www.yogaaustralia.org.au" height="60" src="doc/memberregisteredyogatherapist.jpg" width="100"/></a><span> </span>
<!--   liens vers association   yoga therapy                   -->
<a href="https://www.yogatherapy.org.au" target="_blank"><img alt="www.yogatherapy.org.au" height="60" src="doc/AAYTtherapist.jpg" width="100"/></a><span> </span>
<!--   liens vers association   yoga therapy international     -->
<a href="https://www.iayt.org" target="_blank"><img alt="www.iayt.org" height="60" src="doc/logoiayt2.jpg" width="150"/></a><span> </span>
<!--   liens vers association   yoga australia                   -->
<a href="https://www.yogaaustralia.org.au/teachers/trish-david?place=ashmore-qld-4214/" target="_blank"><img alt="www.yogaaustralia.org.au" height="60" src="doc/asso.jpg" width="100"/></a><span> </span>
<!--   liens vers association   counselling australia                   -->
<a href="https://www.theaca.net.au/counsellor/tricia-david" target="_blank"><img alt="www.theaca.net.au" height="60" src="doc/logoaca.jpg" width="150"/></a><span> </span>
</center>
<center>
<!-- ligne de logos avec un lien vers siteadmin sur le deusieme -->
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<!-- le lien a l'administration du site est dans pied2 et a reintegrer sur demande	-->
<!-- la ligne de code suivante dessine le logo mais sans le lien -->
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" title="" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
<span class="petitlogo"><img alt="logo" src="photos/logograph.gif" width="6%"/></span>
<!DOCTYPE html>

<meta charset="utf-8"/>
<link href="lotuslive.css" rel="stylesheet"/> <!-- fichier css a prendre en reference -->
<title>Page de test</title>
<section class="demo">
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
<div class="leaf"></div>
</section>
</center>
<!---------------------------------------------------------------->
</footer>
</body>
<script src="https://code.jquery.com/jquery.min.js"></script>
<script> 
var originbodylarg = 0;
var checkbody = function(){
    if(parseInt( $('body').css('width')) !== originbodylarg)
	{
        originbodylarg = parseInt( $('body').css('width'));
		
        // largeure body changed, do your magic here
	
	   
	   large = parseInt( $('.photo-title').css('width'));  //recuperation largeure du div photo-title
	   haut = large * 0.224287087471;
	   haut = parseInt(haut);  //calcul hauteur du div photo-title
	   
	   $('.photo-title').css('height',haut);  //afectation hauteur du div photo-title et des p
	   $('.photo-text1').css('height',haut);
	   $('.photo-text2').css('height',haut);
	   $('.photo-text3').css('height',haut);

       fontsize = haut /10;                   //afectation des font-size des p
	   $('.photo-text1').css('font-size',fontsize+'px');
	   $('.photo-text2').css('font-size',fontsize+'px');
	   $('.photo-text3').css('font-size',fontsize+'px');
	   
	   pos1top = -(2.6 * haut);              //positionnements relatifs des p
	   $('.photo-text1').css('position','relative');
	   $('.photo-text1').css('left',0);
	   $('.photo-text1').css('top',pos1top);
       
	   pos2top = -(4 * haut);
	   $('.photo-text2').css('position','relative');
	   $('.photo-text2').css('left',0);
	   $('.photo-text2').css('top',pos2top);
	   
	   pos3top = -(4.8 * haut);
	   $('.photo-text3').css('position','relative');
	   $('.photo-text3').css('left',0);
	   $('.photo-text3').css('top',pos3top);
	
    }
	};
window.addEventListener("load", checkbody, false);
window.addEventListener("resize", checkbody, false);
window.addEventListener("orientationchange", checkbody, false);
</script>
</html>
