<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Banyan Villas | Koh Samui Thailand Holiday Villas Bang Por Beach</title>
<meta content="Banyan Villas located on Bang Por Beach in Koh Samui, Thailand has some luxurious holiday villas for rent, some with private swimming pool others with fabulous sea views all tastefully decorated and equipped with all the modern conveniences, so come and be our guests" name="description"/>
<meta content="banyan villas, koh samui, thailand, holiday villas, bang por beach" name="keywords"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="7 days" name="Revisit-After"/>
<meta content="general" name="rating"/>
<meta content="index, follow, all" name="robots"/>
<meta content="Banyan Villas (Thailand) Co., Ltd." name="author"/>
<meta content="Position Front Page International Co., Ltd." name="creator"/>
<meta content="Banyan Villas (Thailand) Co., Ltd." name="publisher"/>
<meta content="9.579640,99.960296" name="geo.position"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- favicon canonical mobile icons -->
<link href="https://banyanvillas.com/" rel="canonical"/>
<link href="https://banyanvillas.com/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://banyanvillas.com/images/banyan-villas-logo-mobile.jpg?fit=180%2C180" rel="apple-touch-icon-precomposed"/>
<link href="https://banyanvillas.com/images/banyan-villas-logo-mobile.jpg?fit=32%2C32" rel="icon" sizes="32x32"/>
<link href="https://banyanvillas.com/images/banyan-villas-logo-mobile.jpg?fit=192%2C192" rel="icon" sizes="192x192"/>
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/owl-carousel.css" rel="stylesheet"/>
<link href="fontawesome/css/all.min.css" rel="stylesheet"/>
<link href="css/sweetalert.css" rel="stylesheet"/>
<script src="js/pgd2cpk.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<!-- Modernizr -->
<script src="js/modernizr-2.8.3.min.js"></script>
<!--gallery css-->
<link href="gallery/dist/simplelightbox.min.css" rel="stylesheet"/>
<link href="gallery/dist/gallery.css" rel="stylesheet" type="text/css"/>
<!--calendar css-->
<link href="css/datepicker.css" rel="stylesheet"/>
<!--calendar css-->
<!--pop-up--->
<link href="https://banyanvillas.com/pop-up/css/popup.css" rel="stylesheet"/>
<!-- Begin Open Graph Data -->
<meta content="https://banyanvillas.com/" property="og:url"/>
<meta content="Banyan Villas | Koh Samui Thailand Holiday Villas Bang Por Beach" property="og:title"/>
<meta content="Banyan Villas located on Bang Por Beach in Koh Samui, Thailand has some luxurious holiday villas for rent, some with private swimming pool others with fabulous sea views all tastefully decorated and equipped with all the modern conveniences, so come and be our guests" property="og:description"/>
<meta content="Banyan Villas" property="og:site_name"/>
<meta content="https://banyanvillas.com/images/fb-banyan-villas.jpg" property="og:image"/>
<meta content="1758767057672259" property="fb:app_id"/>
<meta content="website" property="og:type"/>
<meta content="en_US" property="og:locale"/>
<!-- End Open Graph Data -->
<script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "WebSite",
       "name" : "Banyan Villas",
       "alternateName" : "Banyan Villas Koh Samui",
       "url" : "https://banyanvillas.com"
    }
</script>
<script async="" data-cbid="84816f38-e64f-475a-abeb-82e16a1e04ac" id="Cookiebot" src="https://consent.cookiebot.com/uc.js" type="text/javascript"></script>
<!-- Begin Piwik Tracking Code -->
<script data-cookieconsent="statistics" type="text/plain">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//positionfrontpageinternational.com/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '6']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Tracking Code --></head>
<body>
<!-- Google Analytics -->
<script data-cookieconsent="statistics" type="text/plain">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74991239-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Fixed navbar -->
<nav class="navbar navbar-default">
<div class="container">
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
<div class="navbar-header col-lg-12 col-md-6 col-sm-6 col-xs-12 row ">
<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed row" data-target="#navbar" data-toggle="collapse" type="button">
<span align="center" style="color:#FFF;">MENU</span>
<span class="sr-only">Toggle navigation</span>
<span align="center" class="icon-bar"></span>
<span align="center" class="icon-bar"></span>
<span align="center" class="icon-bar"></span>
</button>
<a href="https://banyanvillas.com/" title="Banyan Villas | Holiday Villas Bang Por Beach Koh Samui Thailand"><img alt="Logo - Banyan Villas Koh Samui Thailand" border="0" src="https://banyanvillas.com/images/logo.png"/></a>
</div>
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide-sm-menu">
<div class="navbar-header col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
<p style="padding-top:1.2em; ">
<strong>Banyan Villas (Thailand) Co., Ltd.</strong><br/>
8/35 Moo5, Bantai, Maenam,<br/>
Koh Samui, Suratthani 84330, Thailand
</p>
</div>
<div class="navbar-header col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
<p class="navbar-right" style="padding-top:1.2em; ">
Banyan Villas | Koh Samui Thailand Holiday Villas Bang Por Beach<br/>
<span aria-hidden="true" class="fas fa-phone"></span> Tel : +66(0)81 710 8906 (En), +66(0)93 582 2250 (Thai)<br/>
<span aria-hidden="true" class="fas fa-envelope"></span> <a href="https://banyanvillas.com/contact.php" title="Contact Banyan Villas Koh Samui For More Information"><span class="black-color">Contact Us</span></a>
</p>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" border-top:1px solid #666; border-bottom:0px solid #666">
<div class="navbar-collapse collapse col-lg-12 col-md-12 col-sm-12 col-xs-12 navbar-left" id="navbar">
<ul class="nav navbar-nav navbar-center">
<li class="active"><a href="https://banyanvillas.com/" title="Banyan Villas | Luxury Holiday Villas in Koh Samui Thailand"><span aria-hidden="true" class="fas fa-home"></span> Home</a></li>
<li><a href="https://banyanvillas.com/our-villas.php" title="Banyan Our Villas">Our Villas</a></li>
<li><a href="https://banyanvillas.com/facilities.php" title="Banyan Facilities">Facilities</a></li>
<li><a href="https://banyanvillas.com/location.php" title="Banyan Villas is located on Bang Por Beach the South East coast of Koh Samui in Thailand">Location</a></li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" title="More Information on Koh Samui">Koh Samui <span class="fas fa-caret-down"></span></a>
<ul class="dropdown-menu">
<li><a href="https://banyanvillas.com/koh-samui.php" title="Koh Samui, Thailand a Perfect Holiday Destination">Koh Samui</a></li>
<li><a href="https://banyanvillas.com/bang-por-beach.php" title="Bang Por Beach Koh Samui, Thailand - Banyan Villas">Bang Por Beach</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" title="Banyan Availability &amp; Booking">Availability &amp; Booking <span class="fas fa-caret-down"></span></a>
<ul class="dropdown-menu">
<li><a href="https://banyanvillas.com/availability-booking.php" title="Availability and Bookings | Banyan Villas Koh Samui">Banyan Villas</a></li>
<li><a href="https://banyanvillas.com/availability-booking-managed.php" title="Availability and Bookings | Managed By Banyan Villas">Managed By Banyan Villas</a></li>
</ul>
</li>
<li class="dropdown">
<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" title="Banyan Villas Photo Gallery">Gallery <span class="fas fa-caret-down"></span></a>
<ul class="dropdown-menu">
<li><a href="https://banyanvillas.com/gallery.php" title="Banyan Villas Photo Gallery">Banyan Villas</a></li>
<li><a href="https://banyanvillas.com/gallery-managed.php" title="Managed By Banyan Villas Photo Gallery">Managed By Banyan Villas</a></li>
</ul>
</li>
<li><a href="https://banyanvillas.com/testimonials.php" title="Client Reviews and Testimonials - Banyan Villas">Reviews</a></li>
<li><a href="https://banyanvillas.com/contact.php" title="Contact Banyan Villas Koh Samui For More Information">Contact</a></li>
</ul>
</div>
</div>
</div>
</div>
</nav>
<section class="jumbotron">
<div class="container-fluid">
<div class="carousel slide carousel-fade" data-ride="carousel" id="mainCarousel">
<!-- Indicators -->
<ol class="carousel-indicators" role="listbox">
<li class="active" data-slide-to="0" data-target="#mainCarousel"></li>
<li data-slide-to="1" data-target="#mainCarousel"></li>
<li data-slide-to="2" data-target="#mainCarousel"></li>
<li data-slide-to="3" data-target="#mainCarousel"></li>
</ol>
<div class="carousel-inner">
<div class="active item image">
<figure class="responsive" data-media1025="https://banyanvillas.com/images/headers/large/banner1.jpg" data-media240="https://banyanvillas.com/images/headers/small/banner1.jpg" data-media768="https://banyanvillas.com/images/headers/medium/banner1.jpg" data-title="Banyan Villas Koh Samui Thailand">
<noscript>
<img alt="Banyan Villas Koh Samui Thailand" src="https://banyanvillas.com/images/headers/large/banner1.jpg"/>
</noscript>
</figure>
<div class="carousel-caption image" id="rmt_trigger1_el">
<div class="container ">
<div align="right" class="hover col-lg-6 col-md-6 col-sm-12 col-xs-12 navbar-right">
<div class="caption-content">
<p>Banyan Villas<br/><span class="gold-color">Koh Samui</span></p>
<span style=" font-size:1em; font-weight:normal;"> Banyan Villas | Luxury Holiday Villas in Koh Samui Thailand  <a class="btn btn-default rmthov" href="https://banyanvillas.com/our-villas.php" id="rmt_trigger1" title="Built in a tropical garden environment all of Banyan Villas each provide spacious, luxury accommodation">MORE</a></span>
</div>
</div>
</div>
</div>
</div>
<div class="item image">
<figure class="responsive" data-media1025="https://banyanvillas.com/images/headers/large/banner5.jpg" data-media240="https://banyanvillas.com/images/headers/small/banner5.jpg" data-media768="https://banyanvillas.com/images/headers/medium/banner5.jpg" data-title="Bang Por Beach Holiday Villas">
<noscript>
<img alt="Bang Por Beach Holiday Villas" src="https://banyanvillas.com/images/headers/large/banner5.jpg"/>
</noscript>
</figure>
<div class="carousel-caption image" id="rmt_trigger2_el">
<div class="container ">
<div align="right" class="hover col-lg-6 col-md-6 col-sm-12 col-xs-12 navbar-right">
<div class="caption-content">
<p>Bang Por Beach<br/> <span class="gold-color">Holiday Villas</span></p>
<span style=" font-size:1em; font-weight:normal;"> Banyan Villas | Every Villa Just Meters From Bang Por Beach  <a class="btn btn-default rmthov" href="https://banyanvillas.com/bang-por-beach.php" id="rmt_trigger2" title="More information on Bang Por Beach">MORE</a></span>
</div>
</div>
</div>
</div>
</div>
<div class="item grey">
<figure class="responsive" data-media1025="https://banyanvillas.com/images/headers/large/banner3.jpg" data-media240="https://banyanvillas.com/images/headers/small/banner3.jpg" data-media768="https://banyanvillas.com/images/headers/medium/banner3.jpg" data-title="Luxurious Holiday Villas">
<noscript>
<img alt="Luxurious Holiday Villas" src="https://banyanvillas.com/images/headers/large/banner3.jpg"/>
</noscript>
</figure>
<div class="carousel-caption image" id="rmt_trigger3_el">
<div class="container ">
<div align="right" class="hover col-lg-6 col-md-6 col-sm-12 col-xs-12 navbar-right">
<div class="caption-content">
<p>Luxurious<br/> <span class="gold-color">Holiday Villas</span></p>
<span style=" font-size:1em; font-weight:normal;"> Banyan Villas | Luxurious Accommodations Affordably Priced  <a class="btn btn-default rmthov" href="https://banyanvillas.com/testimonials.php" id="rmt_trigger3" title="Testimonials see what others have to say about our luxurious holiday villas">MORE</a></span>
</div>
</div>
</div>
</div>
</div>
<div class="item grey">
<figure class="responsive" data-media1025="https://banyanvillas.com/images/headers/large/banner6.jpg" data-media240="https://banyanvillas.com/images/headers/small/banner6.jpg" data-media768="https://banyanvillas.com/images/headers/medium/banner6.jpg" data-title="Koh Samui Holiday Villas">
<noscript>
<img alt="Koh Samui Holiday Villas" src="https://banyanvillas.com/images/headers/large/banner6.jpg"/>
</noscript>
</figure>
<div class="carousel-caption image" id="rmt_trigger4_el">
<div class="container ">
<div align="right" class="hover col-lg-6 col-md-6 col-sm-12 col-xs-12 navbar-right">
<div class="caption-content">
<p>Koh Samui<br/><span class="gold-color">Thailand</span></p>
<span style=" font-size:1em; font-weight:normal;"> Koh Samui, Thailand For Your Next Vacation  <a class="btn btn-default rmthov" href="https://banyanvillas.com/koh-samui.php" id="rmt_trigger4" title="Koh Samui a tropical island paradise in the heart of the Gulf of Thailand">MORE</a></span>
</div>
</div>
</div>
</div>
</div>
</div><!--/.carousel-inner -->
</div><!--/.carousel -->
</div><!-- /.container-fluid -->
</section><!-- /.jumbotron -->
<script>
var monster = {
set: function(name, value, days, path, secure) {
var date = new Date(),
expires = '',
type = typeof(value),
valueToUse = '',
secureFlag = '';
path = path || "/";
if (days) {
date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
expires = "; expires=" + date.toUTCString();
}
if (type === "object" && type !== "undefined") {
if (!("JSON" in window)) throw "Bummer, your browser doesn't support JSON parsing.";
valueToUse = encodeURIComponent(JSON.stringify({
v: value
}));
}
else {
valueToUse = encodeURIComponent(value);
}
if (secure) {
secureFlag = "; secure";
}
document.cookie = name + "=" + valueToUse + expires + "; path=" + path + secureFlag;
},
get: function(name) {
var nameEQ = name + "=",
ca = document.cookie.split(';'),
value = '',
firstChar = '',
parsed = {};
for (var i = 0; i < ca.length; i++) {
var c = ca[i];
while (c.charAt(0) == ' ') c = c.substring(1, c.length);
if (c.indexOf(nameEQ) === 0) {
value = decodeURIComponent(c.substring(nameEQ.length, c.length));
firstChar = value.substring(0, 1);
if (firstChar == "{") {
try {
parsed = JSON.parse(value);
if ("v" in parsed) return parsed.v;
}
catch (e) {
return value;
}
}
if (value == "undefined") return undefined;
return value;
}
}
return null;
}
};
if (!monster.get('cookieConsent')) {
var cookieConsentAct = function() {
document.getElementById('cookieConsent').style.display = 'none';
monster.set('cookieConsent', 1, 360, '/');
};
document.getElementById('cookieConsent').style.display = 'block';
var cookieConsentEl = document.getElementById('cookieConsentAgree');
if (cookieConsentEl.addEventListener) {
cookieConsentEl.addEventListener('click', cookieConsentAct, false);
}
else if (cookieConsentEl.attachEvent) {
cookieConsentEl.attachEvent("onclick", cookieConsentAct);
}
else {
cookieConsentEl["onclick"] = cookieConsentAct;
}
}
</script>
<div class="pb-3 pt-3" id="check-available">
<div class="container">
<div class="col-lg-12">
<div class="row">
<div class="form-group col-lg-12">
<span style="font-weight:bold; font-size:1em;">CHECK AVAILABILITY</span>
<form action="https://banyanvillas.com/user-check-available.php" id="search_available" method="POST">
<input id="date_now" type="hidden" value="2021-01-13"/>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 p-0">
<div class="input-group">
<span class="input-group-addon"><span class="fal fa-calendar-alt"></span></span>
<input autocomplete="off" class="form-control search-slt dpd1" id="dpd1" name="dpd1" placeholder="Check In" type="text" value=""/>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 p-0">
<div class="input-group">
<span class="input-group-addon"><span class="fal fa-calendar-alt"></span></span>
<input autocomplete="off" class="form-control search-slt dpd2" id="dpd2" name="dpd2" placeholder="Check Out" type="text" value=""/>
</div>
</div>
<div class="col-lg-2 col-md-2 col-sm-3 p-0">
<div class="input-group">
<span class="input-group-addon"><span class="fal fa-cloud-moon"></span></span>
<input class="form-control search-slt" id="night" name="night" placeholder="Nights" readonly="" value=""/>
</div>
</div>
</form>
<div class="col-lg-2 col-md-2 col-sm-2 p-0">
<button class="btn btn-warning wrn-btn" id="submit_search"><span class="fas fa-search"></span> Search</button>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="arrow-grey">
<div align="center" class="col-xs-12" style="z-index:999;">
<br/>
</div>
</div>
<!--gallery-->
<script src="gallery/dist/simple-lightbox.js" type="text/javascript"></script>
<script>
$(function(){
var gallery = $('.gallery a').simpleLightbox();
});
</script>
<div id="holiday-villas-content">
<div class="container">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom">
<h1 style="font-size:2.5em;"><span class="white-color">BANYAN VILLAS</span></h1>
<p class="gold-color" style="font-size:2em; line-height:1.15em; font-weight: bold;">Holiday Villas - Bang Por Beach - Koh Samui – Thailand</p>
Secluded in private tropical gardens next to Bang Por Beach on the quiet north coast of Koh Samui, overlooking Koh Phangan, in Thailand, Banyan Villas provides luxury rental accommodation at affordable prices for both short holidays and long stay visitors.
<br/><br/>
Holiday Villa rentals are a great alternative to traditional luxury hotel accommodation, especially for families and anyone in search of a little privacy, or an escape from the madding high season tourist crowds.
<br/><br/>
</div>
</div>
</div>
<div id="banyan-bangpor">
<div class="container">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<h2 style="font-size:1.2em; line-height:1.15em; font-weight: bold;">BANYAN VILLAS, KOH SAMUI, THAILAND</h2>
Banyan Villas is a family operated business offering spacious luxury holiday accommodation with easy access through tropical gardens to Bang Por beach. We currently have 4 villas owned directly by us, 3 with private swimming pools and Jacuzzis and a further villa with spectacular sea views and situated amidst lush tropical gardens immediately in front of a large communal pool. All our villas are within seconds walk to the beach and will comfortably accommodate between 6 and 8 guests, all bedrooms are air-conditioned and almost all are en-suite.
<br/><br/>
In addition to our own villas we also manage further villas and an apartment on behalf of owners living abroad, we refer to these as our “Managed Villas”. One of these villas is located on the same resort as banyan Villas, whilst others are on a similar resort about 1 km away.
<br/><br/>
We look forward to welcoming you to Banyan Villas!
<br/><br/>
<a href="https://banyanvillas.com/our-villas.php" style="font-weight: bold;" title="Our holiday villas for your next vacation in Thailand">+ More Info - Our Holiday Villas</a>
<br/><br/>
</div>
<div class="hover col-lg-6 col-md-6 col-sm-12 col-xs-12">
<figure class="responsive" data-media1281="https://banyanvillas.com/images/bangpor.jpg" data-media240="https://banyanvillas.com/images/bangpor.jpg" data-title="Our Holiday Villas">
<noscript>
<img alt="Our Holiday Villas" src="https://banyanvillas.com/images/bangpor.jpg"/>
</noscript>
</figure>
</div>
</div>
</div>
<div id="arrow-white">
<div align="center" class="col-xs-12 margin-bottom" style="z-index:999;">
<br/>
</div>
</div>
<div id="bangpor-samui-facilities" style="padding-bottom:1.5em;">
<div class="container">
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-xs-12" style="background-color:#FFF; margin-bottom:1em;">
<h3 style="line-height:1.15em;">BANG POR BEACH</h3>
The palm-fringed white sands of <em>Bang Por Beach</em> lie on the northern coast of Koh Samui, <em>Thailand</em>, facing the neighbouring island of Koh Phangan.
<br/><br/>
<a href="https://banyanvillas.com/bang-por-beach.php" style="font-weight: bold;" title="More Information on Bang Por Beach Koh Samui Thailand">+ More Info - Bang Por Beach</a>
<div class="hover col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom margin-top">
<figure class="responsive" data-media1281="https://banyanvillas.com/images/bang-por-beach.jpg" data-media240="https://banyanvillas.com/images/bang-por-beach.jpg" data-title="Bang Por Beach Koh Samui Thailand">
<noscript>
<img alt="Bang Por Beach Koh Samui Thailand" src="https://banyanvillas.com/images/bang-por-beach.jpg"/>
</noscript>
</figure>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-xs-12" style="background-color:#FFF; margin-bottom:1em;">
<h3 style="line-height:1.15em;">KOH SAMUI</h3>
<em>Koh Samui</em> has been a holiday destination since the 1980s, originally attracting budget travellers. Today it caters to a full range of travellers!
<br/><br/>
<a href="https://banyanvillas.com/koh-samui.php" style="font-weight: bold;" title="Koh Samui, Thailand the Perfect Holiday Destination">+ More Info - Koh Samui</a>
<div class="hover col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom margin-top">
<figure class="responsive" data-media1281="https://banyanvillas.com/images/koh-samui.jpg" data-media240="https://banyanvillas.com/images/koh-samui.jpg" data-title="KOH SAMUI THAILAND">
<noscript>
<img alt="Koh Samui Thailand - The Perfect Holiday Destination" src="https://banyanvillas.com/images/koh-samui.jpg"/>
</noscript>
</figure>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-xs-12" style="background-color:#FFF; margin-bottom:1em;">
<h3 style="line-height:1.15em;">FACILITIES &amp; SERVICES</h3>
Spacious living room, Air-conditioned bedrooms, Internet TV, DVD player, Stereo system, WiFi, Large terrace with sun-loungers and more
<br/><br/>
<a href="https://banyanvillas.com/facilities.php" style="font-weight: bold;" title="Guest Facilities and Services Available at Banyan Villas">+ More Info - Facilities &amp; Services</a>
<div class="hover col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom margin-top">
<figure class="responsive" data-media1281="https://banyanvillas.com/images/facilities-services.jpg" data-media240="https://banyanvillas.com/images/facilities-services.jpg" data-title="GUEST FACILITIES and SERVICES">
<noscript>
<img alt="Guest Facilities and Services available" src="https://banyanvillas.com/images/facilities-services.jpg"/>
</noscript>
</figure>
</div>
</div>
</div>
</div>
</div>
<!--end id bangpor-samui-facilities-->
<script src="gallery/dist/gallery-jquery.min.js"></script>
<br/>
<div id="gallery">
<h3 class="align-center">PHOTO GALLERY</h3>
<div class="container">
<div class="gallery margin-top">
<a href="https://banyanvillas.com/gallery/images/1.jpg"><img alt="Our Villas - Balcony" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/1.jpg"/></a>
<a href="https://banyanvillas.com/gallery/images/2.jpg"><img alt="Our Villas - Living Room" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/2.jpg"/></a>
<a href="https://banyanvillas.com/gallery/images/3.jpg"><img alt="Our Villas - Private Pool" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/3.jpg"/></a>
<a href="https://banyanvillas.com/gallery/images/4.jpg"><img alt="Our Villas - Bang Por Beach" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/4.jpg"/></a>
<a href="https://banyanvillas.com/gallery/images/5.jpg"><img alt="Our Villas - Bedroom" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/5.jpg"/></a>
<a href="https://banyanvillas.com/gallery/images/6.jpg"><img alt="Banyan Villas - Exterior" class="col-lg-2 col-md-4 col-sm-6 col-xs-6 margin-bottom" src="https://banyanvillas.com/gallery/images/6.jpg"/></a>
</div>
<p align="center"><a href="https://banyanvillas.com/gallery.php" style="font-weight: bold;" title="Come Discover Banyan Villas Photo Gallery">+ Come discover more photos of Banyan Villas</a></p>
</div>
<br/>
</div>
<script>
	$(function(){
		var gallery = $('.gallery a').simpleLightbox();
	});
</script>
<div class="footer">
<div class="bg-footer">
<div class="container">
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top pb-3">
<p style=" font-size:1.2em; color:#FFF; font-weight:bold;">ADDRESS</p>
<strong>Banyan Villas (Thailand) Co., Ltd.</strong><br/>
8/35 Moo5, Bantai, Maenam,<br/>
Koh Samui, Suratthani 84330, Thailand
<br/><br/>
<span aria-hidden="true" class="fas fa-phone"></span> Tel : +66(0)81 710 8906<br/>
<span aria-hidden="true" class="fas fa-envelope"></span> <a class="grey-color" href="https://banyanvillas.com/contact.php" title="Contact Banyan Villas Koh Samui For More Information">Contact Us</a><br/>
<a class="grey-color" href="https://www.instagram.com/banyanvillassamui/" target="_blank" title="Instagram Banyan Villas Koh Samui"><i class="fab fa-instagram"></i> Instagram</a><br/>
<a class="grey-color" href="https://www.youtube.com/channel/UCt-FcnBpWe-qYPpwVL3VKTw" target="_blank" title="YouTube Channel Banyan Villas Koh Samui"><i class="fab fa-youtube"></i> YouTube</a><br/>
<br/>
© 2004–2021, Banyan Villas (Thailand) Co., Ltd.<br/>All Rights Reserved<br/><br/>
<a class="grey-color" href="https://bangkokdigitalservices.com" target="_blank" title="Digital Marketing Services provided by Bangkok Digital Services">Digital marketing services provided by Bangkok Digital Services</a>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top">
<p style=" font-size:1.2em; color:#FFF; font-weight:bold;">SOCIAL MEDIA</p>
<!--Follow Us-->
<div align="center" class="s-12 l-12 margin-bottom">
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1758767057672259',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!--script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '1758767057672259',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.3'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script-->
<div class="fb-like-box" data-action="like" data-href="https://facebook.com/BanyanVillas/?business_id=941800782578074" data-layout="standard" data-share="true" data-show-faces="true" data-width="300"></div>
</div>
<!--Follow Us--></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top">
<a class="grey-color" href="https://banyanvillas.com/" title="Banyan Villas Luxurious Holiday Villas in Koh Samui Thailand"><span aria-hidden="true" class="fas fa-home"></span> HOME</a> | 
<a class="grey-color" href="https://banyanvillas.com/our-villas.php" title="Luxury Villas for Rent in Koh Samui Thailand | Banyan Villas">OUR VILLAS</a> | 
<a class="grey-color" href="https://banyanvillas.com/gallery.php" title="Banyan Villas Photo Gallery">GALLERY</a> | 
<a class="grey-color" href="https://banyanvillas.com/contact.php" title="Contact Banyan Villas Koh Samui For More Information">CONTACT</a>
<hr/>
<a class="grey-color" href="https://banyanvillas.com/testimonials.php" title="Client Reviews and Testimonials - Banyan Villas">VIEW REVIEWS</a><br/><br/>
<a class="grey-color" data-target="#myModal" data-toggle="modal" role="button" style="text-decoration:none;" title="Submit Booking Number">ADD REVIEWS</a>
<hr/>
<span class="white-color"><strong>PRIVACY POLICY : </strong></span> We value our visitors' privacy. Information you provide through forms on this website will be used solely by Banyan Villas in answering any immediate inquiry and related follow-up communications. It will not be sold, leased, given away or otherwise released to any third-party individual or organisation. See our <a class="grey-color" href="https://banyanvillas.com/website-privacy-policy.php" style="text-decoration: underline;" title="Banyan Villas - Website Privacy Policy">website privacy policy</a>  |   <a class="grey-color" href="https://banyanvillas.com/terms.php" style="text-decoration: underline;" title="Banyan Villas - Terms &amp; Conditions">Terms &amp; Conditions</a>
</div>
</div>
</div>
</div>
<!-- The Modal -->
<div class="modal" id="myModal">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<p class="text-left" style="font-weight:500; line-height: 1.1; font-size:24px;">Testimonials &amp; Reviews</p>
</div>
<form action="https://banyanvillas.com/check-booking-number.php" class="form-horizontal" method="post" name="formcontact">
<!-- Modal body -->
<div class="modal-body">
<p>Please Enter Your Booking Number</p>
<div class="form-group">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<label>Booking Number</label>
<input class="form-control" id="bk_number" name="bk_number" placeholder="Booking Number" required="" type="text"/>
</div>
</div>
</div>
<!-- Modal footer -->
<div class="modal-footer">
<button class="btn btn-primary btn-md" id="submit" name="submit" type="submit">SUBMIT</button>
<button class="btn btn-default" data-dismiss="modal" type="button">CLOSE</button>
</div>
</form>
</div>
</div>
</div>
<!-- covid-19 button -->
<div class="wrap-fixed-float wrap-fixed-bottom-right">
<a class="btn btn-blue shadow-lg-dark btn-rounded-circle text-blod" href="https://banyanvillas.com/covid-19.php"><img height="40" src="images/covid-icon.svg" width="40"/><span class="counter-small bg-danger"></span></a>
</div>
<!-- Begin Pop Up -->
<!-- End Pop Up -->
<!--Menu Script-->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!--Carousel Script-->
<script>window.jQuery || document.write('<script src="js/jquery-1.11.3.min.js"><\/script>')</script>
<script src="js/jquery-picture-min.js"></script>
<script src="js/plugins-min.js"></script>
<script src="js/jquery-mobile-min.js"></script>
<script>
$(function(){
$('figure.responsive').picture();
$('.carousel').carousel({interval:'6000'});
$("#mainCarousel").swiperight(function() {$("#mainCarousel").carousel('prev');});
$("#mainCarousel").swipeleft(function() {$("#mainCarousel").carousel('next');});
$( "a.rmthov" ).mouseover(function() {
var elid = $(this).attr('id');
$('#' + elid +'_el').addClass('hovered'); 
}).mouseout(function() {
var elid = $(this).attr('id');
$('#' + elid +'_el').removeClass('hovered');
});
});
$(window).load(function() {$('#wrapper').animate({opacity:1},800);
$('#loader').fadeOut(300);
$('.animated').removeClass('before').addClass('after');
});
</script>
<!--Gallery Script-->
<script src="gallery/dist/simple-lightbox.js" type="text/javascript"></script>
<script src="js/moment.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/custom-date-picker.js"></script>
<script src="fontawesome/js/all.min.js"></script>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade bd-example-modal-lg" id="popup" role="dialog" style="padding-top:100px;" tabindex="-1">
<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
<div class="modal-content border-0">
<div class="modal-header border-0" style="border:0px;">
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden="true"><i class="fas fa-times"></i></span>
</button>
</div>
<div class="modal-body text-center">
<h4 style="font-size:28px;">Banyan Villas Special Rates  Covid-19 </h4><img alt="pop-up image" class="img-responsive" src="https://banyanvillas.com/popup/popup_09135306.png" style="margin-left: auto; margin-right: auto;"/><p class="mt-5">Please contact us for Long term bookings - 3 months or more.</p>
</div>
</div>
</div>
</div>
<script>
setTimeout(function(){ 
$('#popup').modal({
  show: true,
  backdrop: 'static',
  keyboard: false
});
},0);
</script>
<style>

.modal-open {
  overflow: hidden;
}

.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto;
}

.modal {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1050;
  display: none;
  width: 100%;
  height: 100%;
  overflow: hidden;
  outline: 0;
}

.modal-dialog {
  position: relative;
  width: auto;
  margin: 0.5rem;
  pointer-events: none;
}

.modal.fade .modal-dialog {
  transition: -webkit-transform 0.3s ease-out;
  transition: transform 0.3s ease-out;
  transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;
  -webkit-transform: translate(0, -50px);
  transform: translate(0, -50px);
}

@media (prefers-reduced-motion: reduce) {
  .modal.fade .modal-dialog {
    transition: none;
  }
}

.modal.show .modal-dialog {
  -webkit-transform: none;
  transform: none;
}

.modal.modal-static .modal-dialog {
  -webkit-transform: scale(1.02);
  transform: scale(1.02);
}

.modal-dialog-scrollable {
  display: -ms-flexbox;
  display: flex;
  max-height: calc(100% - 1rem);
}

.modal-dialog-scrollable .modal-content {
  max-height: calc(100vh - 1rem);
  overflow: hidden;
}

.modal-dialog-scrollable .modal-header,
.modal-dialog-scrollable .modal-footer {
  -ms-flex-negative: 0;
  flex-shrink: 0;
}

.modal-dialog-scrollable .modal-body {
  overflow-y: auto;
}

.modal-dialog-centered {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  min-height: calc(100% - 1rem);
}

.modal-dialog-centered::before {
  display: block;
  height: calc(100vh - 1rem);
  height: -webkit-min-content;
  height: -moz-min-content;
  height: min-content;
  content: "";
}

.modal-dialog-centered.modal-dialog-scrollable {
  -ms-flex-direction: column;
  flex-direction: column;
  -ms-flex-pack: center;
  justify-content: center;
  height: 100%;
}

.modal-dialog-centered.modal-dialog-scrollable .modal-content {
  max-height: none;
}

.modal-dialog-centered.modal-dialog-scrollable::before {
  content: none;
}

.modal-content {
  position: relative;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  pointer-events: auto;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 0.3rem;
  outline: 0;
}

.modal-backdrop {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1040;
  width: 100vw;
  height: 100vh;
  background-color: #000;
}

.modal-backdrop.fade {
  opacity: 0;
}

.modal-backdrop.show {
  opacity: 0.5;
}

.modal-header {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: start;
  align-items: flex-start;
  -ms-flex-pack: justify;
  justify-content: space-between;
  padding: 1rem 1rem;
  border-bottom: 1px solid #dee2e6;
  border-top-left-radius: calc(0.3rem - 1px);
  border-top-right-radius: calc(0.3rem - 1px);
}

.modal-header .close {
  padding: 1rem 1rem;
  margin: -1rem -1rem -1rem auto;
}

.modal-title {
  margin-bottom: 0;
  line-height: 1.5;
}

.modal-body {
  position: relative;
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 1rem;
}

.modal-footer {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -ms-flex-align: center;
  align-items: center;
  -ms-flex-pack: end;
  justify-content: flex-end;
  padding: 0.75rem;
  border-top: 1px solid #dee2e6;
  border-bottom-right-radius: calc(0.3rem - 1px);
  border-bottom-left-radius: calc(0.3rem - 1px);
}

.modal-footer > * {
  margin: 0.25rem;
}

.modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 50px;
  height: 50px;
  overflow: scroll;
}

@media (min-width: 576px) {
  .modal-dialog {
    max-width: 500px;
    margin: 1.75rem auto;
  }
  .modal-dialog-scrollable {
    max-height: calc(100% - 3.5rem);
  }
  .modal-dialog-scrollable .modal-content {
    max-height: calc(100vh - 3.5rem);
  }
  .modal-dialog-centered {
    min-height: calc(100% - 3.5rem);
  }
  .modal-dialog-centered::before {
    height: calc(100vh - 3.5rem);
    height: -webkit-min-content;
    height: -moz-min-content;
    height: min-content;
  }
  .modal-sm {
    max-width: 300px;
  }
}

@media (min-width: 992px) {
  .modal-lg,
  .modal-xl {
    max-width: 800px;
  }
}

@media (min-width: 1200px) {
  .modal-xl {
    max-width: 1140px;
  }
}

</style>
</body>
</html>