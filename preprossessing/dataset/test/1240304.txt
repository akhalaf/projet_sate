<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>RL Group is under construction</title>
<meta content="Just another WordPress site" name="description"/>
<meta content="Free UnderConstructionPage plugin for WordPress" name="generator"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet"/>
<link href="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/css/bootstrap.min.css?v=3.55" rel="stylesheet" type="text/css"/>
<link href="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/css/common.css?v=3.55" rel="stylesheet" type="text/css"/>
<link href="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/under_construction_text/style.css?v=3.55" rel="stylesheet" type="text/css"/>
<link href="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/css/font-awesome.min.css?v=3.55" rel="stylesheet" type="text/css"/>
<link href="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/images/favicon.png" rel="icon" sizes="128x128"/>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-12 col-lg-12">
<h1>Sorry, we're doing some work on the site</h1>
</div>
</div>
</div>
<div id="hero-image">
<img alt="Site is Under Construction" src="https://rlgroup.me/wp-content/plugins/under-construction-page/themes/under_construction_text/under_construction_text.png" title="Site is Under Construction"/>
</div>
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-offset-2 col-lg-8">
<p class="content">Thank you for being patient. We are doing some work on the site and will be back shortly.</p>
</div>
</div>
<div class="row" id="social">
<div class="col-xs-12 col-md-12 col-lg-12">
</div>
</div>
</div>
<div class="loggedout" id="login-button"><a href="https://rlgroup.me/wp-login.php" title="Log in to WordPress admin"><i aria-hidden="true" class="fa fa-wordpress fa-2x"></i></a></div>
</body>
</html>
