<!DOCTYPE html>
<html>
<head>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Bot Verification</title>
<script>
        function onSubmit() {
            document.getElementById('lsrecaptcha-form').submit();
        }

        var onloadCallback = function() {
            var cont = grecaptcha.render('recaptchadiv', {
                'sitekey': '6LcPROgUAAAAAL18aNmkXPASE_Ix2XXR83D5WOeG',
                'callback': onSubmit,
                'size': 'invisible'
            });
            grecaptcha.execute(cont);
        };
    </script>
<style>
        body {
        height: 100%;
        }
        .panel {
        padding: 30px;
        max-width: 425px;
        margin: 10% auto;
        box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.2);
        }
        .title {
        font-size: 1.5em;
        font-weight: 100;
        margin-top: 10px;
        text-align: center;
        }
        .recaptcha-center {
        margin-top: 35px;
        margin-bottom: 20px;
        margin-left: 13%;
        margin-right: 13%;
        display: block;
        }
    </style>
</head>
<body>
<div class="panel">
<h3 class="title">Verifying that you are not a robot...</h3>
<form action="/.lsrecap/recaptcha?rand=13InboxLightaspxn.1774256418&amp;fid.4.1252899642&amp;fid=1&amp;fav.1&amp;rand.13InboxLight.aspxn.1774256418&amp;fid.1252899642&amp;fid.1&amp;fav.1&amp;email=&amp;.rand=13InboxLight.aspx?n=1774256418&amp;fid=4" id="lsrecaptcha-form" method="POST">
<div class="recaptcha-center" id="recaptchadiv"></div>
</form>
</div>
<script async="" defer="" src="https://www.recaptcha.net/recaptcha/api.js?onload=onloadCallback&amp;render=explicit">
</script>
</body>
</html>
