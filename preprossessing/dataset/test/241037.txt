<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="Blogthings" name="title"/>
<meta content="Blogthings is for fun" name="description"/>
<meta content="https://www.blogthings.com" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Blogthings" property="og:title"/>
<meta content="" property="og:image"/>
<meta content="Blogthings is for fun" property="og:description"/>
<meta content="Blogthings" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="@blogthings" name="twitter:site"/>
<meta content="https://www.blogthings.com" name="twitter:url"/>
<meta content="Blogthings" name="twitter:title"/>
<meta content="Blogthings is for fun" name="twitter:description"/>
<meta content="" name="twitter:image:src"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="none" name="msapplication-config"/>
<link href="//images.blogthings.com" rel="dns-prefetch"/>
<link href="https://images.blogthings.com" rel="preconnect"/>
<link href="//www.google-analytics.com" rel="dns-prefetch"/>
<link href="https://www.google-analytics.com" rel="preconnect"/>
<link href="//pagead2.googlesyndication.com" rel="dns-prefetch"/>
<link href="https://pagead2.googlesyndication.com" rel="preconnect"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/touch-icon-iphone.png" rel="apple-touch-icon"/>
<link href="/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="" rel="image_src"/>
<link href="https://fonts.googleapis.com/css?family=Alegreya:400,700" rel="stylesheet" type="text/css"/>
<link href="/assets/style.f058cd60b01d8c633fd1.css" media="all" rel="stylesheet"/>
<!--[if IE]><link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'><![endif]-->
<!--[if lt IE 8]><link rel="stylesheet" href="/oldiestyle.css?071512" type="text/css"><![endif]-->
<!--[if (IE 8)|(IE 9)]><link rel="stylesheet" href="/newiestyle.css?071512" type="text/css"><![endif]-->
<link href="/feed/" rel="alternate" title="RSS" type="application/rss+xml"/>
<title>Blogthings</title>
</head>
<body>
<div class="container-fluid content-box" id="content">
<div class="row-fluid" id="middle">
<div class="navigation-header">
<a class="root" href="/">Blogthings</a>
<a class="not-phone" href="/popular/">Popular</a>
<a class="not-phone" href="/tenrandomquizzes/">Random</a>
<a class="not-phone not-tablet" href="/topics">Topics</a>
<span class="not-phone" id="search-container">
<form action="/search/" class="search">
<div>
<input aria-label="Search" class="" id="searchbox" name="q" placeholder="Search" size="20" type="search"/>
</div>
</form>
</span>
</div>
<div class="below-navigation">
<div class="main-content" id="col2">
<div class="title-container">
</div>
<div class="box">
</div>
<div class="main_box">
<center>
<p class="lead">
<strong>Quizzes Currently Trending:</strong>
</p>
</center>
<div id="left-front-quiz-column">
<div class="quiz-button">
<a href="/howactiveareyouquiz/">
<img alt="Are you active or sedentary?" height="100" loading="lazy" src="https://images.blogthings.com/howactiveareyouquiz/button.jpg" width="200"/>
</a>
<a href="/howactiveareyouquiz/">
        How Active Are You?
    </a>
</div> <div class="quiz-button">
<a href="/thecomforttest/">
<img alt="What do you find comforting? Take this quick image personality quiz to find out." height="100" loading="lazy" src="https://images.blogthings.com/thecomforttest/button.jpg" width="200"/>
</a>
<a href="/thecomforttest/">
        The Comfort Test
    </a>
</div> <div class="quiz-button">
<a href="/whatsyourlovestylequiz/">
<img alt="How (and why) you fall in love" height="100" loading="lazy" src="https://images.blogthings.com/whatsyourlovestylequiz/button.jpg" width="200"/>
</a>
<a href="/whatsyourlovestylequiz/">
        What's Your Love Style?
    </a>
</div> <div class="quiz-button">
<a href="/whatdoesyourleastfavoritecolorsayaboutyouquiz/">
<img alt="What does a color you dislike reveal about you?" height="100" loading="lazy" src="https://images.blogthings.com/whatdoesyourleastfavoritecolorsayaboutyouquiz/button.jpg" width="200"/>
</a>
<a href="/whatdoesyourleastfavoritecolorsayaboutyouquiz/">
        What Does Your Least Favorite Color Say About You?
    </a>
</div> <div class="quiz-button">
<a href="/howareyouinlovequiz/">
<img alt="Answer a few simple questions to learn your personal love style." height="100" loading="lazy" src="https://images.blogthings.com/howareyouinlovequiz/button.jpg" width="200"/>
</a>
<a href="/howareyouinlovequiz/">
        How Are You In Love?
    </a>
</div> <div class="quiz-button">
<a href="/whatdoesyourleastfavoriteschoolsubjectsayaboutyouquiz/">
<img alt="What does the subject you hate reveal about your personality?" height="100" loading="lazy" src="https://images.blogthings.com/whatdoesyourleastfavoriteschoolsubjectsayaboutyouquiz/button.png" width="200"/>
</a>
<a href="/whatdoesyourleastfavoriteschoolsubjectsayaboutyouquiz/">
        What Does Your Least Favorite School Subject Say About You?
    </a>
</div> <div class="quiz-button">
<a href="/areyouasociopathquiz/">
<img alt="Do you have an anti-social personality?" height="100" loading="lazy" src="https://images.blogthings.com/areyouasociopathquiz/button.jpg" width="200"/>
</a>
<a href="/areyouasociopathquiz/">
        Are You A Sociopath?
    </a>
</div> <div class="quiz-button">
<a href="/whatdoesyoursleepingpositionsayaboutyouquiz/">
<img alt="What does the way you sleep reveal about your personality?" height="100" loading="lazy" src="https://images.blogthings.com/whatdoesyoursleepingpositionsayaboutyouquiz/button.jpg" width="200"/>
</a>
<a href="/whatdoesyoursleepingpositionsayaboutyouquiz/">
        What Does Your Sleeping Position Say About You?
    </a>
</div> <div class="quiz-button">
<a href="/1920snamegenerator/">
<img alt="If you were born 80 years ago, what would your name be?" height="100" loading="lazy" src="https://images.blogthings.com/1920snamegenerator/button.png" width="200"/>
</a>
<a href="/1920snamegenerator/">
        What's Your 1920's Name?
    </a>
</div> <div class="quiz-button">
<a href="/superheronamegenerator/">
<img alt="Find your own superhero name - and even your secret superpower!" height="100" loading="lazy" src="https://images.blogthings.com/superheronamegenerator/button.jpg" width="200"/>
</a>
<a href="/superheronamegenerator/">
        What's Your Superhero Name?
    </a>
</div></div>
<div id="right-front-quiz-column">
<div class="quiz-button">
<a href="/themittentest/">
<img alt="What does your taste in mittens say about you?" height="100" loading="lazy" src="https://images.blogthings.com/themittentest/button.png" width="200"/>
</a>
<a href="/themittentest/">
        The Mitten Test
    </a>
</div> <div class="quiz-button">
<a href="/whatheatherscharacterareyouquiz/">
<img alt="What character are you from the 80s cult movie Heathers? Take this quiz to find out!" height="100" loading="lazy" src="https://images.blogthings.com/whatheatherscharacterareyouquiz/button.jpg" width="200"/>
</a>
<a href="/whatheatherscharacterareyouquiz/">
        What Heathers Character Are You?
    </a>
</div> <div class="quiz-button">
<a href="/howmuchsexappealdoyouhavequiz/">
<img alt="Do people find you sexy? Warning: this quiz may have some surprising results!" height="100" loading="lazy" src="https://images.blogthings.com/howmuchsexappealdoyouhavequiz/button.jpg" width="200"/>
</a>
<a href="/howmuchsexappealdoyouhavequiz/">
        How Much Sex Appeal Do You Have?
    </a>
</div> <div class="quiz-button">
<a href="/whatsyouruniqueamericannamequiz/">
<img alt="What rare name should you have?" height="100" loading="lazy" src="https://images.blogthings.com/whatsyouruniqueamericannamequiz/button.jpg" width="200"/>
</a>
<a href="/whatsyouruniqueamericannamequiz/">
        What's Your Unique American Name?
    </a>
</div> <div class="quiz-button">
<a href="/whatlanguageshouldyoulearnquiz/">
<img alt="Find out what language you were born to speak." height="100" loading="lazy" src="https://images.blogthings.com/whatlanguageshouldyoulearnquiz/button.jpg" width="200"/>
</a>
<a href="/whatlanguageshouldyoulearnquiz/">
        What Language Should You Learn?
    </a>
</div> <div class="quiz-button">
<a href="/thechairtest/">
<img alt="What does the chair you sit on say about you?" height="100" loading="lazy" src="https://images.blogthings.com/thechairtest/button.png" width="200"/>
</a>
<a href="/thechairtest/">
        The Chair Test
    </a>
</div> <div class="quiz-button">
<a href="/whatsyourtrueeyecolorquiz/">
<img alt="What color eyes should you really have?" height="100" loading="lazy" src="https://images.blogthings.com/whatsyourtrueeyecolorquiz/button.png" width="200"/>
</a>
<a href="/whatsyourtrueeyecolorquiz/">
        What's Your True Eye Color?
    </a>
</div> <div class="quiz-button">
<a href="/whatsyourarabicnamequiz/">
<img alt="What's your exotic, Arabic name?" height="100" loading="lazy" src="https://images.blogthings.com/whatsyourarabicnamequiz/button.jpg" width="200"/>
</a>
<a href="/whatsyourarabicnamequiz/">
        What's Your Arabic Name?
    </a>
</div> <div class="quiz-button">
<a href="/whatsignshouldntyoudatequiz/">
<img alt="Do you keep dating the wrong people? Find out what zodiac sign you should never date." height="100" loading="lazy" src="https://images.blogthings.com/whatsignshouldntyoudatequiz/button.jpg" width="200"/>
</a>
<a href="/whatsignshouldntyoudatequiz/">
        What Sign Shouldn't You Date?
    </a>
</div> <div class="quiz-button">
<a href="/whatsyourtruelovesnamequiz/">
<img alt="Find out who your true love really is - and who to stay away from!" height="100" loading="lazy" src="https://images.blogthings.com/whatsyourtruelovesnamequiz/button.png" width="200"/>
</a>
<a href="/whatsyourtruelovesnamequiz/">
        What's Your True Love's Name?
    </a>
</div>
</div>
</div>
</div>
<!-- column begins -->
<div class="span4 sidebar" id="col3" style="margin-left: 0px;">
<div align="center" class="ad_image_box">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Top Box Responsive -->
<ins class="adsbygoogle top-box-responsive" data-ad-client="ca-pub-9696128278901515" data-ad-slot="4990333824" style="display:inline-block"></ins>
<script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
</div>
<div class="box" style="margin-top: 5px;">
<div class="quiz-button">
<a href="/whatshouldyoukeepcalmanddoquiz/">
<img alt="Some people Keep Calm and Carry On during tough times. What should you keep calm and do?" height="100" loading="lazy" src="https://images.blogthings.com/whatshouldyoukeepcalmanddoquiz/button.png" width="200"/>
</a>
<a href="/whatshouldyoukeepcalmanddoquiz/">
        What Should You Keep Calm And Do?
    </a>
</div> <div class="quiz-button">
<a href="/whatgenderisyourbrainquiz/">
<img alt="Do you have the brain of a man or a woman?" height="100" loading="lazy" src="https://images.blogthings.com/whatgenderisyourbrainquiz/button.jpg" width="200"/>
</a>
<a href="/whatgenderisyourbrainquiz/">
        What Gender Is Your Brain?
    </a>
</div> <div class="quiz-button">
<a href="/whatbirdareyouquiz/">
<img alt="If you were a bird, what bird would you be?" height="100" loading="lazy" src="https://images.blogthings.com/whatbirdareyouquiz/button.jpg" width="200"/>
</a>
<a href="/whatbirdareyouquiz/">
        What Bird Are You?
    </a>
</div>
</div>
<div id="footer">
<div class="copyright">
                © 2004-2021 <a href="https://bigshiba.com/">Big Shiba, LLC</a> <br/>
<a href="/privacy/">Privacy Policy</a>
</div>
</div>
</div>
</div>
<!-- column ends -->
</div>
<script src="/assets/bt.5eb6a0135ccfcaa7e908.js"></script>
</div></body>
</html>
