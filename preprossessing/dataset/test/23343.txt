<!DOCTYPE html>
<html>
<head><title>2Pass for Learning to Drive/Driving Lessons/Driving schools/Driving Test Preparation</title>
<meta content="driving test, theory and hazard test, car and motorcycle practical test, driving schools, learning to drive," name="keywords"/>
<meta content="Help and advice for the Theory and Practical driving test for Learner Drivers and Driving school information for your full driving licence" name="description"/>
<meta content="General" name="rating"/>
<meta content="never" name="expires"/>
<meta content="english" name="language"/>
<meta content="Global" name="distribution"/>
<meta content="INDEX,FOLLOW" name="robots"/>
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <!--<![endif]-->
<meta content="936b960a2682fb23" name="yandex-verification"/>
<meta content="e0noV04cE-ia2R8JeQTJrc2qQzTubRIWa2HxV-Gsps8" name="google-site-verification"/>
<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-PZX7QDC':true});</script>
<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
<script>
    var _gaq=[['_setAccount','UA-6156711-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    ga('require', 'GTM-PZX7QDC');
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
<!-- Start: GPT Sync -->
<script type="text/javascript">
	var gptadslots=[];
	(function(){
		var useSSL = 'https:' == document.location.protocol;
		var src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
		document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	})();
</script>
<script type="text/javascript">
	

		//Adslot oop declaration
		gptadslots[0] = googletag.defineOutOfPageSlot('/116061662/UK_2Pass.co.uk', 'subsite-oop').addService(googletag.pubads());

		googletag.pubads().enableSyncRendering();
		googletag.enableServices();

</script>
<!-- End: GPT -->
<meta content="e0noV04cE-ia2R8JeQTJrc2qQzTubRIWa2HxV-Gsps8" name="google-site-verification"/>
<!-- Mobile viewport optimized: h5bp.com/viewport -->
<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
<link href="css/slider.css" rel="stylesheet"/>
<link href="css/style.css" rel="stylesheet"/>
<link href="css/layout.css" rel="stylesheet"/>
<link href="css/typography.css" rel="stylesheet"/>
<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
<!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables htm5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
<script src="js/libs/modernizr-2.5.3.min.js"></script>
<script src="js/menu.js"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ray2pass" type="text/javascript"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000"
    },
    "button": {
      "background": "transparent",
      "text": "#f1d600",
      "border": "#f1d600"
    }
  },
  "position": "bottom-right"
})});
</script>
</head><body>
<header>
<div id="top_thin_strip"></div>
<div id="top_row">
<div id="top_row">
<div id="logo"><img alt="2 Pass - Everything for the learner driver." src="img/logo.jpg"/></div>
<div id="top_row_sharing">
<div id="top_row_ad">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- questions -->
<ins class="adsbygoogle" data-ad-client="ca-pub-4110339233674045" data-ad-slot="2694777049" style="display:inline-block;width:468px;height:60px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div><!-- //#top_row -->
<!-- // HEADER -->
<div id="menu">
<ul class="dropdown">
<li><a href="index.html">Home</a></li>
<li><a href="start.htm">Starting Out</a>
<ul class="sub_menu">
<li><a href="page3.htm">Latest Driving Test News</a></li>
<li><a href="pass-your-test.htm">Are You Ready To Drive</a></li>
<li><a href="licence.htm">Your Provisional License</a></li>
<li class="subpresent"><a href="high-tec.htm">Find the Right Driving Instructor</a>
<ul>
<li><a href="code.htm">Instructor Code of Practice</a></li>
</ul> </li>
<li><a href="lessons.htm">Your First Few Lessons</a></li>
<li><a href="herald.htm">Women Instructors</a></li>
<li><a href="illegal-driving-instructors.htm">Watch for Illegal Driving Instructors</a></li>
<li><a href="learn.htm">Learning with Family/Friends</a></li>
<li><a href="auto.htm">Driving an Automatic</a></li>
<li><a href="driving-age.htm">Driving Age Rumours</a></li>
<li><a href="history.htm">History of the Driving Test</a></li>
<li class="subpresent"><a href="#">Find Driving Schools in your Area</a>
<ul>
<li><a href="residential.htm">Residential Courses</a></li>
<li><a href="scotland.htm">Scotland</a></li>
<li><a href="northengland.htm">Northern England</a></li>
<li><a href="middleengland.htm">Middle England</a></li>
<li><a href="southengland.htm">Southern England</a></li>
<li><a href="wales.htm">Wales</a></li>
<li><a href="nireland.htm">N.Ireland</a></li>
</ul>
</li>
<li><a href="goodluck.htm">Why we drive on the left</a></li>
<li><a href="biker.htm">Planning to Ride a Motorcycle</a></li>
<li><a href="lgv.htm">Planning to drive a Lorry or Bus</a></li>
<li><a href="adi.htm">How to become a Driving Instructor</a></li>
<li><a href="renew-driving-licence-at-70.htm">Ways to Renew your licence at 70</a></li>
</ul>
</li>
<li><a href="35test.htm">Theory</a>
<ul class="sub_menu">
<li><a href="page3.htm">Latest Driving Test News</a></li>
<li><a href="faqtheory.htm">Theory Test FAQ</a></li>
<li><a href="faqhazard.htm">Hazard Test FAQ</a></li>
<li><a href="hazard.htm">What is HTP?</a></li>
<li><a href="hazardtest.htm">2Pass HTP Video Clip</a></li>
<li><a href="booking.htm">Ways to book your Theory Test</a></li>
<li><a href="dts-online-test.html">Free Trial Hazard Perception Test</a></li>
<li><a href="theory-questions.htm">Free Theory Test Question Samples</a></li>
<li><a href="traffic-sign-test.htm">Free Traffic Sign Test</a></li>
<li><a href="motorway-test.htm">Free Motorway Test</a></li>
<li><a href="software.htm">Theory Test PC Software</a></li>
<li><a href="download.htm">Theory Test Instant Downloads</a></li>
<li><a href="casestudytheorytest.htm">Case Study Questions</a></li>
<li><a href="nerves.htm">Overcome Your Test Nerves</a></li>
<li><a href="firstaid.htm">First Aid On The Road</a></li>
<li><a href="quizzes.htm">Theory Quizzes</a></li>
</ul>
</li>
<li><a href="pract.htm">Practical</a>
<ul class="sub_menu">
<li><a href="page3.htm">Latest Driving Test News</a></li>
<li><a href="tackle-recent-driving-test-changes.htm">Tackle Recent Test Changes</a></li>
<li><a href="faqpract.htm">Practical Test FAQ</a></li>
<li><a href="video.htm">Lots of Video Lessons</a></li>
<li><a href="satnav-testroutes.htm">Download Driving Test Routes</a></li>
<li><a href="bookingpract.htm">Book Your Practical Test</a></li>
<li><a href="practical.htm">Practical Test Day</a></li>
<li><a href="nerves.htm">Overcome Test Nerves</a></li>
<li><a href="driving-test-instructor.htm">Your Instructor On Test Day</a></li>
<li class="subpresent"><a href="engine.htm">New Tell Me/Show Me Questions</a>
<ul>
<li><a href="driving-test-tell-me-questions.htm">New Tell Me Questions</a></li>
<li><a href="driving-test-show-me-questions.htm">New Show Questions</a></li>
</ul>
</li><li><a href="independent-driving-test.htm">Independent Driving on Test Day</a></li>
<li><a href="drivingtest-in-bad-weather.htm">Your Driving Test in Bad Weather</a></li>
<li><a href="driving-tests-around-the-world.htm">Driving Tests Around the World</a></li>
<li><a href="awareness.htm">Situation Awareness</a></li>
<li class="subpresent"><a href="#">How to do</a>
<ul>
<li><a href="dashboard-lights.htm">Dashboard Warning Lights</a></li>
<li><a href="cockpit.htm">Cockpit Drill</a></li>
<li><a href="moveoff.htm">Move Off</a></li>
<li><a href="steering.htm">Steering</a></li>
<li><a href="gears.htm">Changing Gears</a></li>
<li><a href="hill.htm">Hill Starts</a></li>
<li><a href="pull-up-on-the-right-and-reverse.htm">Pull up on the right and reverse</a></li>
<li><a href="signs.htm">The Turn In The Road</a></li>
<li><a href="reverse.htm">Reverse Around Corner</a></li>
<li><a href="parallel.htm">Parallel Parking</a></li>
<li><a href="bayparking.htm">Bay Parking</a></li>
<li><a href="brake.htm">Emergency Stops</a></li>
<li><a href="roundabout.htm">Roundabouts</a></li>
<li><a href="junction.htm">Approach 'T' Junctions</a></li>
<li><a href="junctionright.htm">Right Turns</a></li>
<li><a href="crossing.htm">Pedestrian Crossings</a></li>
<li><a href="boxjunction.htm">Box Juntions</a></li>
<li><a href="stopping-distance.htm">Stopping Distances</a></li>
</ul>
</li>
<li class="subpresent"><a href="#">How to Deal with </a>
<ul>
<li><a href="firsttri.htm">Driving Alone</a></li>
<li><a href="fog.htm">Driving in Fog</a></li>
<li><a href="snow.htm">Driving in Snow</a></li>
<li><a href="flood.htm">Driving in Floods</a></li>
<li><a href="motorway.htm">Driving on Motorways</a></li>
<li><a href="aquaplaning.htm">Aquaplaning</a></li>
<li><a href="drivingtips.htm">Aggressive Drivers</a></li>
<li><a href="accident.htm">Road Accident</a></li>
<li><a href="crash-for-cash-scams.htm">Crash for Cash Scams</a></li>
<li><a href="emergencyvehicle.htm">Emergency Vehicles</a></li>
</ul>
</li>
<li><a href="nightmare.htm">Test Nightmares</a></li>
<li><a href="failure.htm">Failures Top 10</a></li>
<li><a href="ecodriving.htm">Eco-Driving</a></li>
<li><a href="drivingshoes.htm">Shoes for Driving</a></li>
</ul>
</li>
<li><a href="passed.htm">Passed</a>
<ul class="sub_menu">
<li><a href="driving-penalty-points.htm">6 Penalty Points Law</a></li>
<li><a href="how-to-feel-safe-when-you-pass-your-driving-test.htm">How to Stay Safe on the Road</a></li>
<li><a href="passplus.htm">Pass Plus</a></li>
<li><a href="learnlive.htm">Using 'P' Plates</a></li>
<li class="subpresent"><a href="#">How to Deal with </a>
<ul>
<li><a href="firsttri.htm">Driving Alone</a></li>
<li><a href="fog.htm">Driving in Fog</a></li>
<li><a href="snow.htm">Driving in Snow</a></li>
<li><a href="flood.htm">Driving in Floods</a></li>
<li><a href="motorway.htm">Driving on Motorways</a></li>
<li><a href="aquaplaning.htm">Aquaplaning</a></li>
<li><a href="drivingtips.htm">Aggressive Drivers</a></li>
<li><a href="accident.htm">Road Accident</a></li>
<li><a href="crash-for-cash-scams.htm">Crash for Cash Scams</a></li>
<li><a href="emergencyvehicle.htm">Emergency Vehicles</a></li>
</ul>
</li>
<li><a href="childseatbelts.htm">Seat Belt Laws for Kids</a></li>
<li><a href="retest.htm">New Drivers Who Offend</a></li>
<li><a href="advanced.htm">Advanced Driving</a></li>
<li><a href="buying.htm">Buying a Used Car</a></li>
<li><a href="top-10-cars-for-new-drivers.htm">Top 10 Cars for Sale</a></li>
<li><a href="selling.htm">Selling a Car</a></li>
<li><a href="textcheck.htm">Vehicle Data Check for £3</a></li>
<li><a href="finance.htm">Finance your first car</a></li>
<li><a href="insur.htm">Insurance Advice</a></li>
<li><a href="insurancesponsors.htm">Cheap Insurance Quotes</a></li>
<li><a href="breakdown.htm">Breakdown and Recovery</a></li>
<li><a href="numberplate.htm">Purchasing a Number Plate</a></li>
<li><a href="accident.htm">Dealing with an Accident</a></li>
<li><a href="crash-for-cash-scams.htm">Crash for Cash Scams</a></li>
<li><a href="engine.htm">General Car Maintenance</a></li>
<li><a href="lgv.htm">Learn to drive a Lorry / Bus</a></li>
<li><a href="adi.htm">Becoming an ADI</a></li>
</ul>
</li>
<li><a href="software.htm">Training Aids</a>
<ul class="sub_menu">
<li><a href="dts-online-test.html">Free Trial Online Theory Test</a></li>
<li><a href="hazardtest-download.htm">Hazard Perception Test Download</a></li>
<li><a href="cardownload.htm">Car Theory Test Download</a></li>
<li><a href="motorcycledownload.htm">Motorcycle Theory Test Download</a></li>
<li><a href="largevehiclesdownload.htm">LGV/PCV Theory Test Download</a></li>
<li><a href="adidownload.htm">ADI Theory Test Download</a></li>
<li>------------------------</li>
<li><a href="software-cars.htm">Car online Revision</a></li>
<li><a href="software-motorcycle.htm">Motorcycle online Revision</a></li>
<li><a href="software-lgv.htm">LGV/PCV online Revision</a></li>
<li><a href="software-adi.htm">ADI/PDI online Revision</a></li>
<li>------------------------</li>
<li><a href="book.htm"> eBooks (pdf format)</a></li>
<li>------------------------</li>
<li><a href="satnav-testroutes.htm">Download Driving Test Routes</a></li>
</ul>
</li>
<li class="subpresent"><a href="insurancesponsors.htm">Insurance Solutions</a>
<ul>
<li><a href="https://www.collingwood.co.uk/learner-driver-insurance?refid=2passcld">Buy Learner Driver Insurance</a></li>
<li><a href="insurancesponsors.htm">Insurance for Car Drivers</a></li>
<li><a href="/cheap-motorcycle-insurance/">Insurance for Motorcycle Riders</a></li>
<li><a href="van-insurance.htm">Insurance for Van Drivers</a></li>
<li><a href="temporary-insurance.htm">Temporary Car Insurance</a></li>
<li><a href="bicycle-insurance.htm">Bicycle Insurance</a></li>
</ul>
</li></ul>
</div><!-- //#menu -->
<div id="l-welcome">
<div id="l-welcome-inner">
<div id="welcome_title"><h1 class="fontface">Learner Driving
</h1></div>
<div id="welcome_text">
<p>Taking your driving lessons and then your driving test can be a traumatic, nerve-racking and costly experience.<br/>

	  		Learning how to drive could be one of the most important things you do in your life but it needs to but done correctly, it's not just about passing the test but been able to drive safely for the rest of your life.<br/>
<b>We hope our website will provide you with all the support and information you need to obtain your FULL driving licence.</b></p>
</div>
</div><!-- //#l-welcome-inner -->
</div>
<div class="clearfix" id="main" role="main">
<div class="row clearfix"><!-- insert this div for every row -->
<div class="advert_block">
<a href="https://www.rapidcarcheck.co.uk/5deluxe/" target="_blank">
<img alt="Rapid Car Check" border="0" height="250" src="img/rapid970new.jpg" width="970"/></a>
</div><!-- //.advert_block -->
<img alt="Getting Started with your driving lessons" class="get_started" src="img/title_template1.jpg"/>
<div class="b-info left_block">
<div class="b-info-img">
<img alt="Learning to Drive" src="img/licencenew.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>Starting Out</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">All the things you need to know to get started to lean to drive. Your Provisional Driving Licence, Learning to drive with your family and friends, How to find a Quality Driving Instructor, Search DVSA Driving School Listing. </font></p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="start.htm">Read more</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
<div class="b-info">
<div class="b-info-img">
<img alt="Theory Driving Test" src="img/tn_theory.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>Theory &amp; Hazard Test</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">A complete listing of all our Theory and Hazard Perception Test information. Lots of questions, quizzes and mock tests. If it's for the Car, Motorcycle, LGV or PCV theory tests we have it here!</font></p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="35test.htm">Read more</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
<div class="b-info right_block ">
<div class="b-info-img">
<img alt="Practical Driving Test" src="img/pic1.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>Practical Test</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">All you need to know about the practical driving test in the UK. From what to expect on your big day to how to do all those horrible driving test manoeuvres. We also have video lessons to help you on your way</font></p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="pract.htm">Read more</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
</div><!-- //.row -->
<!-- 	NEW ROW	   -->
<br class="clearfix" style="clear:both"/>
<!-- 	NEW ROW	   -->
<img alt="Getting Started" class="get_started" src="img/title_template2.jpg"/>
<div class="row clearfix"><!-- insert this div for every row -->
<div class="b-info left_block">
<div class="b-info-img">
<img alt="Driving Test Video" src="img/video-front.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>Driving Lesson Tutorials</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">These driving video tutorials will help you to get a better idea of what you need to know when you're starting out learning to drive. These show you all the Basic Skills such as Gear changing, Steering, Moving off,Stopping, Manoeuvres, Emergency Stops, dealing with roundabouts, cross roads, junctions.....the list is endless! 
</font></p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="video.htm">Watch them all</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
<div class="b-info">
<div class="b-info-img">
<img alt="Cheap Insurance for Learner Drivers" src="img/pic2.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>New Driver Insurance Advice</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">Before driving any vehicle, make sure that it has cover for your use or that your own insurance gives you adequate cover. You <b>MUST NOT </b> drive a vehicle without insurance. <br/>
<li> <a href="learner-driver-insurance.htm">Temporary Car Insurance for Learner Drivers</a></li>
</font>
</p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="insur.htm">Read more</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
<div class="b-info right_block">
<div class="b-info-img">
<img alt="Now you have passed your driving test" src="img/pic3.jpg" width="290"/>
</div><!-- //.b-img -->
<div class="b-info-title">
<h2>Extra driving help</h2>
</div><!-- //.b-info-title -->
<div class="b-info-text">
<p><font size="+1">Passed your Driving Test ? <b>Congratulations</b> you can rip up those L plates!
Now the real learning starts!<br/>
We hope you will continue to drive and improve on the high standard you have reached.

</font>
</p>
</div><!-- //.b-info-text -->
<div class="b-info-link">
<a class="button" href="passed.htm">Read more</a>
</div><!-- //.b-info-text -->
</div><!-- //.b-info -->
<div class="advert_block">
<div class="b-ad-left">
<a href="car-insurance-quotes.htm" target="_blank">
<img alt="Online Insurance Quotes" border="0" height="250" src="img/canvas.png" width="300"/></a>              



<a href="satnav-testroutes.htm" target="_blank">
<img alt="satnav test routes" border="0" src="img/test-route-mini-ad.jpg"/></a>
<br/><br/>
<a href="https://www.paidonresults.net/c/46912/5/2169/0"><img alt="Health4All Supplements" border="0" height="250" src="https://creative.paidonresults.net/46912/2169/0/5" width="300"/></a>                         
<a href="https://uk.jooble.org/jobs-driver" target="_blank">
<img alt="Driving Jobs in United Kingdom" src="img/Jooble.png"/>
</a>
<p>
</p></div>
<div class="b-info right_block ">
<div class="b-info-img">
<div class="b-info-title">
<h2>Start Learning For Your Driving Test NOW!</h2>
<div class="b-info-text">
<p><font size="+1">
<a href="https://www.dtsanytime.co.uk/learner/register_1.aspx?Reseller=17"><img alt="online driving lessons" border="0" height="120" src="2pass-dts-online-banner-practical.jpg" width="290"/></a>
<b>How to start your free trial</b><br/>
<b>1.</b>Register for your free account with Driving Test Success Anytime<br/>
<b>2.</b>Click on the activation link in your email<br/>
<b>3.</b>Set your password<br/>
<b>4.</b>Once you're logged in, select 'My Account' and 'Subscriptions' from the menu on the left<br/>
<b>5.</b>Select 'Buy a new subscription' (don't worry you don't need to enter any payment details!)<br/>
<b>6.</b>Select your vehicle category<br/>
<b>7.</b>Choose the '30 minutes free trial' and click on 'Activate Subscription'
</font></p>
<div class="b-info-link">
<a class="button" href="https://www.dtsanytime.co.uk/learner/register_1.aspx?Reseller=17">Register NOW!</a>
</div><!-- //.b-info-text -->
</div>
</div>
</div>
</div><!-- //.b-info-text -->
</div><!-- //.advert_block -->
</div><!-- //.row -->
</div><!-- //#role -->
<center>
<hr/>
<p>
<font size="+2">
<b>SHARE THIS PAGE</b>
</font>
</p>
<p>
<!-- Go to www.addthis.com/dashboard to customize your tools --> </p><div class="addthis_inline_share_toolbox"></div>
<hr/>
</center>
<div class="advert_block">
<div class="advert_inner_full_width">
<script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Billboard -->
<ins class="adsbygoogle" data-ad-client="ca-pub-4110339233674045" data-ad-slot="4412266247" style="display:inline-block;width:970px;height:250px"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<!--
<a href="http://www.paidonresults.net/c/46912/1/1490/0/products/theory-test-kit-pc-download" target="_blank">
<img src="img/DTS950x110.jpg" alt="Driving Test Success Download Offer" border="0"></a>
-->
<br/><br/>
<h2><font color="RED">More Driver  Information </font></h2>
<p><font size="+1">
<a href="covid19-driving-lessons.htm">Getting Ready for a Covid-19 Driving Lesson</a>
<br/><br/>
<a href="gifts-to-your-loved-ones.htm">Personalised Gifts To Give To Your Loved Ones This Year</a>
<br/><br/>
<a href="drink-driving-laws.htm">How much damage does Christmas Driving do ?</a>
<br/><br/>
<a href="learning-to-drive-in-an-electric-car.htm">Learning to drive in an Electric Car</a>
<br/><br/>
<a href="make-your-driving-experience-cheaper.htm">5 hacks to make your driving experience cheaper</a>
<br/><br/>
<a href="guide-to-buying-and-insuring-your-first-car.htm">A Guide To Buying And Insuring Your First Car</a>
<br/><br/>
<a href="should-you-buy-a-car-while-you-are-learning-to-drive.htm">Should You Buy A Car While You're Learning To Drive?</a>
<br/><br/>
<a href="how-to-choose-a-car-as-a-new-driver.htm">How to Choose a Car as a New Driver</a>
<br/><br/>
<a href="driving-test-draws-near.htm">Important Decisions to Make as Your Driving Test Draws Near</a>
<br/><br/>
<a href="buggy-fun-in-cambodia.htm">Buggy fun in Cambodia</a>
<br/><br/>
<a href="save-on-your-motorcycle-insurance.htm">5 ways to save money on your motorcycle insurance</a>
<br/><br/>
<a href="tackle-recent-driving-test-changes.htm">3 ways learner drivers can tackle recent driving test changes</a>
<br/><br/>
<a href="intensive-driving-courses.htm">All About Intensive Driving Courses</a>
<br/><br/>
<a href="pcp-loans.htm">The rise of PCP loans - what you should know</a>
<br/><br/>
<a href="underage-driving.htm">How underage driving can help you pass your test</a>
<br/><br/>
<a href="first-MOT.htm">Everything you need to know before your very first MOT</a>
<br/><br/>
<a href="pass-your-test.htm">Are you ready to drive? Passing your test and beyond</a>
</font>
</p>
</div>
</div><!-- //.advert_block -->
<footer>
<div class="l-footer_inner">
<div class="l-footer-col">
<h2 class="fontface">Navigation</h2>
<ul class="footer-nav">
<li><a href="index.html">Home</a></li>
<li><a href="start.htm">Starting Out</a></li>
<li><a href="theory.htm">Theory Test</a></li>
<li><a href="pract.htm">Practical Test</a></li>
<li><a href="passed.htm">Passed</a></li>
<li><a href="software.htm">Training Aids</a></li>
<li><a href="page3.htm">News</a></li>
<li><a href="site-search.htm">Site Search</a></li>
<li><a href="mail.htm">Contact</a></li>
<li><a href="in.htm">Privacy Policy/About Us</a></li>
</ul>
</div>
<div class="l-footer-col">
<h2 class="fontface">Advertisement</h2>
<a href="https://www.collingwood.co.uk/learner-driver-insurance?refid=2passcld" target="_blank"><img alt="Collingwood Learner Driver Insurance" border="0" height="250" src="https://www.collingwood.co.uk/images/banner/instructors/banner_350x250.jpg" width="300"/></a>
</div><!-- //.l-footer-col -->
<div class="l-footer-col last">
<h2 class="fontface">Join us on Facebook!</h2>
<div class="fb-like-box" data-header="false" data-height="370" data-href="https://www.facebook.com/www2passcouk" data-show-faces="true" data-stream="false" data-width="300"></div>
</div>
</div><!-- //.l-footer_inner -->
<div class="disclaimer">
<p align="right"> © 1996-2020


		</p></div>
</footer>
<!-- JavaScript at the bottom for fast page loading -->
<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=113437838748987";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<!-- scripts concatenated and minified via build script -->
<script src="js/plugins.js"></script>
<script src="js/libs/slider.js"></script>
<script src="js/script.js"></script>
<!-- end scripts -->
</div></div></div></header></body></html>