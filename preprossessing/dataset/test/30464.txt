<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="utf-8"/>
<!-- 
	quattro Media :: Online-Video-Distribution

	This website is powered by TYPO3 - inspiring people to share!
	TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
	TYPO3 is copyright 1998-2021 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
	Information and contribution at https://typo3.org/
-->
<base href="https://www.4msports-server.com/"/>
<title>4msports Distribution Service: www.4msports-server.com</title>
<meta content="TYPO3 CMS" name="generator"/>
<meta content="summary" name="twitter:card"/>
<link href="/typo3conf/ext/femanager/Resources/Public/Css/Main.min.css?1601635819" media="all" rel="stylesheet" type="text/css"/>
<link href="/typo3conf/ext/femanager/Resources/Public/Css/Additional.min.css?1601635819" media="all" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" rel="stylesheet" type="text/css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener('load', function () {
  window.cookieconsent.initialise({
    cookie: {
      domain: '.4msports-server.com',
      expiryDays: 30
    },
    palette: {
      popup: {
        background: '#fafafa',
        text: '#000000'
      },
      button: {background: '#5fbeed', text: '#ffffff'}
    },
    theme: 'edgeless',
    position: 'top',
    static: true,
    content: {
      href: '/footer/privacy-policy/'
    }
  })
})
</script>
<link href="https://www.4msports-server.com/" rel="canonical"/>
<link href="/typo3temp/assets/vhs-assets-bc4150d023d3255136db671d61ac93f2.css?1610445521" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" type="text/javascript"></script>
<script src="/typo3temp/assets/vhs-assets-f858212dbff17dc7c67053408fe7c74e.js?1610445521" type="text/javascript"></script>
</head>
<body>
<header class="site-header ">
<div lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:f="http://typo3.org/ns/fluid/ViewHelpers" xmlns:v="http://typo3.org/ns/FluidTYPO3/Vhs/ViewHelpers">
<div class="top-nav">
<ul>
<li class="">
<a href="/topnav/use-conditions" title="Use conditions">Use conditions</a>
</li>
<li class="">
<a href="/topnav/terms-conditions" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
</li>
<li class="">
<a href="/topnav/accept-terms-conditions" title="Accept Terms &amp; Conditions">Accept Terms &amp; Conditions</a>
</li>
<li class="sub">
<a href="/topnav/sign-in-register" title="Sign in / Register">Sign in / Register</a>
</li>
<li class="">
<!-- CART -->
<a href="/topnav/cart" title="Cart">
                          Cart
                          <span class="cart-items">(0)</span>
</a>
</li>
</ul>
</div>
</div>
<a href="/">
<img alt="alt text" class="logo" height="175" src="/typo3temp/assets/_processed_/9/7/csm_logo2_55feec6d0f.jpg" style="width: 182px; height: auto;" width="450"/>
</a>
<form action="index.php?id=160" class="search" id="form-search" method="POST" name="search">
<input class="search__input" name="search" type="search"/>
<input class="search__control" type="submit" value="Search"/>
</form>
<nav class="main-nav">
<ul class="main-nav__list">
<li class="">
<a href="/news" title="News">News</a>
</li>
<li class="">
<a href="/clips" title="Clips">Clips</a>
</li>
<li class="has-subnav">
<span class="nolink" data-uid="33">Highlights</span>
<ul>
<li class="">
<a href="/highlights/without-voice-over" title="Without Voice Over">Without Voice Over</a>
</li>
<li class="">
<a href="/highlights/with-voice-over" title="With Voice Over">With Voice Over</a>
</li>
</ul>
</li>
<li class="has-subnav">
<span class="nolink" data-uid="148">Series</span>
<ul>
<li class="">
<a href="/series/freeride-world-tour" title="Freeride World Tour">Freeride World Tour</a>
</li>
<li class="">
<a href="/series/world-of-freesports" title="World of Freesports">World of Freesports</a>
</li>
<li class="">
<a href="/series/ticket-to-ride" title="Ticket to Ride">Ticket to Ride</a>
</li>
<li class="">
<a href="/series/world-strongman-cup" title="World Strongman Cup">World Strongman Cup</a>
</li>
</ul>
</li>
<li class="has-subnav">
<span class="nolink" data-uid="149">Archive</span>
<ul>
<li class="">
<a href="/archive/news" title="News">News</a>
</li>
<li class="">
<a href="/archive/clips" title="Clips">Clips</a>
</li>
<li class="has-subnav ">
<span class="nolink" data-uid="790">Highlights</span>
<ul>
<li class="">
<a href="/archive/highlights/with-voice-over" title="With Voice Over">With Voice Over</a>
</li>
<li class="">
<a href="/archive/highlights/without-voice-over" title="Without Voice Over">Without Voice Over</a>
</li>
</ul>
</li>
</ul>
</li>
<li class="">
<a href="/live-delayed" title="LIVE DELAYED">LIVE DELAYED</a>
</li>
</ul>
</nav>
</header>
<main class="site-content container">
<div class="frame frame-default frame-type-html frame-layout-0" id="c164"><style>
  li.fast-line__list-item{
    line-height:1.42857;
    margin-bottom:10px;
  }
  .contact{
    margin-top:140px;
  }
</style><div class="home"><img alt="background" class="home__background" height="480" src="/fileadmin/user_upload/bg-home-4.jpg" width="960"/><div class="home-wrapper"><div class="col-xs-6 home__column"><section class="intro"><h2 class="home__title home__title--intro"></h2><p class="bodytext"></p><h2>WELCOME TO THE QUATTRO MEDIA CONTENT SERVER</h2>quattro media´s  content delivery platform  makes it quicker and easier than ever before to access all categories and archives! Just complete the short minute registration process before your first login. <p></p><p class="bodytext"> </p><p class="bodytext"></p><p></p></section></div><div class="col-xs-3 home__column"><section class="fast-line"><h2 class="home__title home__title--fast-line">Fast line</h2><ul class="fast-line__list"><li class="fast-line__list-item"><a href="https://www.4msports-server.com/pline/a/">
              Engadinwind 2020
            </a></li><li class="fast-line__list-item"><a href="http://www.4msports-server.com/pline/c/">
              Golden Fly Series
            </a></li><li class="fast-line__list-item"><a href="http://www.4msports-server.com/pline/d/">
              FAI - Airsports
            </a></li><li class="fast-line__list-item"><a href="http://www.4msports-server.com/pline/g/">
              Winter Games
            </a></li><li class="fast-line__list-item"><a href="https://www.4msports-server.com/pline/e/">
              Freeride World Tour
            </a></li></ul></section></div><div class="col-xs-3 home__column"><section class="contact"><p class="bodytext"><a class="internal-link" href="http://www.4msports-server.com/?id=137" title="Opens internal link in current window">Register</a> now and you get immediately access to our News, Clips and Highlights from a wide array of world class sports events!</p><address class="contact__address"><p class="bodytext"><br/></p><p></p><p></p></address></section></div></div></div></div>
</main>
<footer class="site-footer">
<ul><li>
<a href="/footer/contact" title="Contact">Contact</a>
</li>
<li>
<a href="/footer/imprint" title="Imprint">Imprint</a>
</li>
<li>
<a href="/footer/privacy-policy" title="Privacy Policy">Privacy Policy</a>
</li>
<li>
<a href="/footer/disclaimer" title="Disclaimer">Disclaimer</a>
</li>
<li>
<a href="/footer/terms-conditions" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
</li></ul>
</footer>
<script src="/typo3conf/ext/femanager/Resources/Public/JavaScript/Validation.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3conf/ext/femanager/Resources/Public/JavaScript/Femanager.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3conf/ext/powermail/Resources/Public/JavaScript/Libraries/jquery.datetimepicker.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3conf/ext/powermail/Resources/Public/JavaScript/Libraries/parsley.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3conf/ext/powermail/Resources/Public/JavaScript/Powermail/Tabs.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3conf/ext/powermail/Resources/Public/JavaScript/Powermail/Form.min.js?1601635819" type="text/javascript"></script>
<script src="/typo3temp/assets/vhs-assets-5d405be67c7cc6071802193e43c0bc36.js?1610445521" type="text/javascript"></script>
</body>
</html>