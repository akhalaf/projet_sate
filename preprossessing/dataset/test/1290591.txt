<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width,initial-scale=1.0" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<!-- This is Squarespace. --><!-- thomas-bollmann -->
<base href=""/>
<meta charset="utf-8"/>
<title>SEEDNINE</title>
<link href="https://images.squarespace-cdn.com/content/v1/587274d215d5db0c21f9638c/1485897191768-HGICMVJ3PCLZPUBI9VLT/ke17ZwdGBToddI8pDm48kJycfsYb1urLU93EpFqOTQmoCXeSvxnTEQmG4uwOsdIceAoHiyRoc52GMN5_2H8Wp7zww8OjRrqjaM7_0x6HDLp42EP6IAa5vAmscK3sHI4MkNL5tmfZ3otlI9yi1IzH2Q/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="SEEDNINE" property="og:site_name"/>
<meta content="SEEDNINE" property="og:title"/>
<meta content="website" property="og:type"/>
<meta content="" property="og:image"/>
<meta content="SEEDNINE" itemprop="name"/>
<meta content="" itemprop="thumbnailUrl"/>
<link href="" rel="image_src"/>
<meta content="" itemprop="image"/>
<meta content="SEEDNINE" name="twitter:title"/>
<meta content="" name="twitter:image"/>
<meta content="summary" name="twitter:card"/>
<meta content="" name="description"/>
<link href="https://images.squarespace-cdn.com" rel="preconnect"/>
<script src="//use.typekit.net/ik/Vf4d5vieMd7T863qf0zZF-G_y4V7AbfCvgsqKvtsnpJfeG9IfFHN4UJLFRbh52jhWD9tFDJaFebkwQmqFAsKwewkwAjoFRwtjU7iMPG0ieyzdABDOA4zdeUljAo0O1FUiABkZWF3jAF8OcFzdPUCdhFydeyzSabCShm8Z283-eNXdhikjWgGpPoRdhXCieyzdABDOA4zdeUljAo0O1FUiABkZWF3jAF8OcFzdPUaiaS0Shm8Z283-eNXdhikjWgGpPoRdhXCiaiaO1sGdhuySkuKdhUCdAB0dKoDSWmyScmDSeBRZPoRdhXK2YgkdayTdAIldcNhjPJ4Z1mXiW4yOWgXH6qJ73IbMg6gJMJ7fbKCMsMMeMC6MKG4f5J7IMMjMkMfH6qJtkGbMg6FJMJ7fbKzMsMMeMb6MKG4fOMgIMMj2KMfH6GJCwbgIMMjgPMfH6GJCSbgIMMj2kMfH6qJnbIbMg6eJMJ7fbK0MsMMegM6MKG4fJCgIMMjgkMfH6qJRMIbMg6sJMJ7fbKTMsMMeM66MKG4fHGgIMMjIKMfH6qJKbIbMg64JMJ7fbKHMsMMegw6MKG4fJZmIMIjMkMfH6qJ6u9bMs6FJMJ7fbKImsMgeMb6MKG4fJmmIMIj2KMfH6qJxubbMs6BJMJ7fbKMmsMgeMv6MKG4fJBmIMIjgkMfH6qJ689bMs6sJMJ7fbKYmsMgeM66MKG4fJymIMIjIKMfqMYKNFaIgb.js" type="text/javascript"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-cldr_resource_pack');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/cldr-resource-pack-d2b6094e965c882c2de89-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common_vendors');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-vendors-2ec094db00b9e6d92d8fd-min.en-US.js"></script><script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="//assets.squarespace.com/universal/scripts-compressed/common-2c32d110777189549ff4d-min.en-US.js"></script><script data-name="static-context">Static = window.Static || {}; Static.SQUARESPACE_CONTEXT = {"facebookAppId":"314192535267336","facebookApiVersion":"v6.0","rollups":{"squarespace-announcement-bar":{"js":"//assets.squarespace.com/universal/scripts-compressed/announcement-bar-8b244fce99594deac3684-min.en-US.js"},"squarespace-audio-player":{"css":"//assets.squarespace.com/universal/styles-compressed/audio-player-03a5305221e9f3857f5d3fbff2cd9bbe-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/audio-player-9d33505677455a9b22add-min.en-US.js"},"squarespace-blog-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/blog-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/blog-collection-list-ab4142fcacca918cf4e2d-min.en-US.js"},"squarespace-calendar-block-renderer":{"css":"//assets.squarespace.com/universal/styles-compressed/calendar-block-renderer-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/calendar-block-renderer-82b361c64e6e75913711e-min.en-US.js"},"squarespace-chartjs-helpers":{"css":"//assets.squarespace.com/universal/styles-compressed/chartjs-helpers-9935a41d63cf08ca108505d288c1712e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/chartjs-helpers-270e1573dd28dff07fc7c-min.en-US.js"},"squarespace-comments":{"css":"//assets.squarespace.com/universal/styles-compressed/comments-f794dccd3bb871fc0cbc0bb7ad024168-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/comments-1b8d1adb275f098d8dc6d-min.en-US.js"},"squarespace-commerce-cart":{"js":"//assets.squarespace.com/universal/scripts-compressed/commerce-cart-9fd3c3147f23827502474-min.en-US.js"},"squarespace-dialog":{"css":"//assets.squarespace.com/universal/styles-compressed/dialog-4c984bcaacc45888f9092057493234b6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/dialog-614b07c3f84e1e3b30662-min.en-US.js"},"squarespace-events-collection":{"css":"//assets.squarespace.com/universal/styles-compressed/events-collection-1e8a762808391e4b0bd8945da50793ac-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/events-collection-415312b37ef6978cf711b-min.en-US.js"},"squarespace-form-rendering-utils":{"js":"//assets.squarespace.com/universal/scripts-compressed/form-rendering-utils-da71321e53a08371a214c-min.en-US.js"},"squarespace-forms":{"css":"//assets.squarespace.com/universal/styles-compressed/forms-763d974ea7719bb18959e8f0a891abe6-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/forms-9fe4eb33891804b4464fe-min.en-US.js"},"squarespace-gallery-collection-list":{"css":"//assets.squarespace.com/universal/styles-compressed/gallery-collection-list-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/gallery-collection-list-5d5ff461e8bba64f298dc-min.en-US.js"},"squarespace-image-zoom":{"css":"//assets.squarespace.com/universal/styles-compressed/image-zoom-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/image-zoom-f7178bbfa97ac5234c120-min.en-US.js"},"squarespace-pinterest":{"css":"//assets.squarespace.com/universal/styles-compressed/pinterest-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/pinterest-9dd1acd10aa47a7154983-min.en-US.js"},"squarespace-popup-overlay":{"css":"//assets.squarespace.com/universal/styles-compressed/popup-overlay-68d60e7bd84500af34df575998cc00d0-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/popup-overlay-0149a748bc121034a13df-min.en-US.js"},"squarespace-product-quick-view":{"css":"//assets.squarespace.com/universal/styles-compressed/product-quick-view-eedd090fe95cd960919fcf1e0a4293c3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/product-quick-view-ce2b51182ccd8ac6accc6-min.en-US.js"},"squarespace-products-collection-item-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-item-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-item-v2-5426bac76cb8bd5a92e8b-min.en-US.js"},"squarespace-products-collection-list-v2":{"css":"//assets.squarespace.com/universal/styles-compressed/products-collection-list-v2-72b0ab7796582588032aa6472e2e2f14-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/products-collection-list-v2-0f57d23347251b2736f90-min.en-US.js"},"squarespace-search-page":{"css":"//assets.squarespace.com/universal/styles-compressed/search-page-207da8872118254c0a795bf9b187c205-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/search-page-ac207ca39a69cb84f84f0-min.en-US.js"},"squarespace-search-preview":{"js":"//assets.squarespace.com/universal/scripts-compressed/search-preview-638ba2bf8ec524b820947-min.en-US.js"},"squarespace-share-buttons":{"js":"//assets.squarespace.com/universal/scripts-compressed/share-buttons-32cc08f2dd7137611cfc4-min.en-US.js"},"squarespace-simple-liking":{"css":"//assets.squarespace.com/universal/styles-compressed/simple-liking-9ef41bf7ba753d65ec1acf18e093b88a-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/simple-liking-146a4d691830282d9ce5a-min.en-US.js"},"squarespace-social-buttons":{"css":"//assets.squarespace.com/universal/styles-compressed/social-buttons-bf7788a87c794b73afd9d5c49f72f4f3-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/social-buttons-aa06a0ebdaa97ed82d7ae-min.en-US.js"},"squarespace-tourdates":{"css":"//assets.squarespace.com/universal/styles-compressed/tourdates-d41d8cd98f00b204e9800998ecf8427e-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/tourdates-ee4869629f6a684b35f94-min.en-US.js"},"squarespace-website-overlays-manager":{"css":"//assets.squarespace.com/universal/styles-compressed/website-overlays-manager-4f212ab97f9bc590002bb2ff55f69409-min.en-US.css","js":"//assets.squarespace.com/universal/scripts-compressed/website-overlays-manager-9e5a6a309dfd7e877bf6f-min.en-US.js"}},"pageType":100,"website":{"id":"587274d215d5db0c21f9638c","identifier":"thomas-bollmann","websiteType":1,"contentModifiedOn":1588011419811,"cloneable":false,"hasBeenCloneable":false,"siteStatus":{},"language":"en-US","timeZone":"America/Toronto","machineTimeZoneOffset":-18000000,"timeZoneOffset":-18000000,"timeZoneAbbr":"EST","siteTitle":"SEEDNINE","fullSiteTitle":"SEEDNINE","siteDescription":"<p>The online presence of photography/film and special projects leaders, Ingrid Jones and Thomas Bollmann aka Seednine.&nbsp;</p>","location":{"mapLat":43.6804146,"mapLng":-79.32385779999998,"addressTitle":"Seed9","addressLine1":"626 Rhodes Avenue","addressLine2":"Toronto, ON, M4J 4X6","addressCountry":"Canada"},"shareButtonOptions":{"1":true,"7":true,"4":true,"8":true,"2":true,"6":true,"3":true},"authenticUrl":"https://www.seed9photography.com","internalUrl":"https://thomas-bollmann.squarespace.com","baseUrl":"https://www.seed9photography.com","primaryDomain":"www.seed9photography.com","sslSetting":3,"isHstsEnabled":false,"socialAccounts":[{"serviceId":64,"screenname":"Instagram","addedOn":1556299190568,"profileUrl":"https://www.instagram.com/seed9/","iconEnabled":true,"serviceName":"instagram-unauth"}],"typekitId":"","statsMigrated":false,"imageMetadataProcessingEnabled":false,"screenshotId":"979d2fc41b22c9a610c1dd4de4ce616e88d98a8bd83e53a3459a502be569639b","showOwnerLogin":false},"websiteSettings":{"id":"587274d215d5db0c21f9638e","websiteId":"587274d215d5db0c21f9638c","subjects":[],"country":"CA","state":"ON","simpleLikingEnabled":true,"mobileInfoBarSettings":{"isContactEmailEnabled":false,"isContactPhoneNumberEnabled":false,"isLocationEnabled":false,"isBusinessHoursEnabled":false},"commentLikesAllowed":true,"commentAnonAllowed":true,"commentThreaded":true,"commentApprovalRequired":false,"commentAvatarsOn":true,"commentSortType":2,"commentFlagThreshold":0,"commentFlagsAllowed":true,"commentEnableByDefault":true,"commentDisableAfterDaysDefault":0,"disqusShortname":"","commentsEnabled":false,"contactPhoneNumber":"416 986 2234","businessHours":{"monday":{"text":"All Day ","ranges":[{"from":0,"to":1440}]},"tuesday":{"text":"All Day ","ranges":[{"from":0,"to":1440}]},"wednesday":{"text":"All Day ","ranges":[{"from":0,"to":1440}]},"thursday":{"text":"All Day ","ranges":[{"from":0,"to":1440}]},"friday":{"text":"All Day ","ranges":[{"from":0,"to":1440}]}},"storeSettings":{"returnPolicy":null,"termsOfService":null,"privacyPolicy":null,"expressCheckout":false,"continueShoppingLinkUrl":"/","useLightCart":false,"showNoteField":false,"shippingCountryDefaultValue":"US","billToShippingDefaultValue":false,"showShippingPhoneNumber":true,"isShippingPhoneRequired":false,"showBillingPhoneNumber":true,"isBillingPhoneRequired":false,"currenciesSupported":["CHF","HKD","MXN","EUR","DKK","USD","CAD","MYR","NOK","THB","AUD","SGD","ILS","PLN","GBP","CZK","SEK","NZD","PHP","RUB"],"defaultCurrency":"USD","selectedCurrency":"USD","measurementStandard":1,"showCustomCheckoutForm":false,"enableMailingListOptInByDefault":false,"contactLocation":{"addressLine1":"626 Rhodes Avenue","addressLine2":"Toronto, ON","addressCountry":"Canada"},"businessName":"Seed9 ","sameAsRetailLocation":false,"merchandisingSettings":{"scarcityEnabledOnProductItems":false,"scarcityEnabledOnProductBlocks":false,"scarcityMessageType":"DEFAULT_SCARCITY_MESSAGE","scarcityThreshold":10,"multipleQuantityAllowedForServices":true,"restockNotificationsEnabled":false,"restockNotificationsMailingListSignUpEnabled":false,"relatedProductsEnabled":false,"relatedProductsOrdering":"random","soldOutVariantsDropdownDisabled":false,"productComposerOptedIn":false,"productComposerABTestOptedOut":false},"isLive":false,"multipleQuantityAllowedForServices":true},"useEscapeKeyToLogin":true,"trialAssistantEnabled":true,"ssBadgeType":1,"ssBadgePosition":4,"ssBadgeVisibility":1,"ssBadgeDevices":1,"pinterestOverlayOptions":{"mode":"disabled"},"ampEnabled":false},"cookieSettings":{"isCookieBannerEnabled":false,"isRestrictiveCookiePolicyEnabled":false,"isRestrictiveCookiePolicyAbsolute":false,"cookieBannerText":"","cookieBannerTheme":"","cookieBannerVariant":"","cookieBannerPosition":"","cookieBannerCtaVariant":"","cookieBannerCtaText":""},"websiteCloneable":false,"subscribed":false,"appDomain":"squarespace.com","templateTweakable":true,"tweakJSON":{"auto-play":"true","galleryAutoPlayDelay":"5","index-transition":"Fade","lightbox-transition":"Slide","product-gallery-auto-crop":"true","product-image-auto-crop":"true","slides-click-through":"true","tweak-v1-related-products-title-spacing":"50px"},"templateId":"51e6b9e4e4b062dafa7099b9","templateVersion":"7","pageFeatures":[1,4],"gmRenderKey":"QUl6YVN5Q0JUUk9xNkx1dkZfSUUxcjQ2LVQ0QWVUU1YtMGQ3bXk4","templateScriptsRootUrl":"https://static1.squarespace.com/static/ta/51e6b9c9e4b050adffbe392c/1240/scripts/","betaFeatureFlags":["commerce_restock_notifications","commerce_afterpay_toggle","commerce_minimum_order_amount","domains_transfer_flow_improvements","generic_iframe_loader_for_campaigns","customer_notifications_panel_v2","domains_transfer_flow_hide_preface","domains_allow_async_gsuite","gallery_captions_71","ORDERS-SERVICE-check-digital-good-access-with-service","ORDERS-SERVICE-reset-digital-goods-access-with-service","animations_august_2020_new_preset","commerce_afterpay","seven-one-menu-overlay-theme-switcher","campaigns_new_sender_profile_page","seven_one_header_editor_update","crm_campaigns_sending","seven-one-main-content-preview-api","domains_universal_search","campaigns_single_opt_in","commerce_pdp_edit_mode","ORDER_SERVICE-submit-subscription-order-through-service","commerce_tax_panel_v2","donations_customer_accounts","domain_deletion_via_registrar_service","seven_one_frontend_render_gallery_section","commerce_instagram_product_checkout_links","newsletter_block_captcha","commerce_activation_experiment_add_payment_processor_card","events_panel_70","dg_downloads_from_fastly","commerce-recaptcha-enterprise","campaigns_user_templates_in_sidebar","nested_categories_migration_enabled","member_areas_ga","page_interactions_improvements","seven_one_image_effects","ORDER_SERVICE-submit-reoccurring-subscription-order-through-service","commerce_reduce_cart_calculations","domain_locking_via_registrar_service","seven-one-content-preview-section-api","list_sent_to_groups","omit_tweakengine_tweakvalues","commerce_add_to_cart_rate_limiting","seven_one_portfolio_hover_layouts","seven_one_image_overlay_opacity","commerce_category_id_discounts_enabled","campaigns_email_reuse_template_flow","commerce_setup_wizard","seven_one_frontend_render_page_section","domains_use_new_domain_connect_strategy","commerce_subscription_order_delay","commerce_pdp_survey_modal","domain_info_via_registrar_service","local_listings"],"yuiEliminationExperimentList":[{"name":"statsMigrationJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"ContributionConfirmed-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"TextPusher-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MenuItemWithProgress-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"imageProcJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"QuantityChangePreview-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CompositeModel-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"HasPusherMixin-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"INACTIVE"},{"name":"ProviderList-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"MediaTracker-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"pushJobWidget-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"internal-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"BillingPanel-enabled","experimentType":"AB_TEST","variant":"false","containsError":false,"status":"ACTIVE"},{"name":"PopupOverlayEditor-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"},{"name":"CoverPagePicker-enabled","experimentType":"AB_TEST","variant":"true","containsError":false,"status":"ACTIVE"}],"impersonatedSession":false,"tzData":{"zones":[[-300,"Canada","E%sT",null]],"rules":{"Canada":[[1974,2006,null,"Oct","lastSun","2:00","0","S"],[1987,2006,null,"Apr","Sun>=1","2:00","1:00","D"],[2007,"max",null,"Mar","Sun>=8","2:00","1:00","D"],[2007,"max",null,"Nov","Sun>=1","2:00","0","S"]]}}};</script><script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script><script type="application/ld+json">{"url":"https://www.seed9photography.com","name":"SEEDNINE","description":"<p>The online presence of photography/film and special projects leaders, Ingrid Jones and Thomas Bollmann aka Seednine.&nbsp;</p>","@context":"http://schema.org","@type":"WebSite"}</script><script type="application/ld+json">{"legalName":"Seed9 ","address":"626 Rhodes Avenue\nToronto, ON, M4J 4X6\nCanada","email":"seed9@mac.com","telephone":"416 986 2234","sameAs":["https://www.instagram.com/seed9/"],"@context":"http://schema.org","@type":"Organization"}</script><script type="application/ld+json">{"address":"626 Rhodes Avenue\nToronto, ON\nCanada","name":"Seed9 ","openingHours":"Mo 00:00-00:00, Tu 00:00-00:00, We 00:00-00:00, Th 00:00-00:00, Fr 00:00-00:00","@context":"http://schema.org","@type":"LocalBusiness"}</script><link href="//static1.squarespace.com/static/sitecss/587274d215d5db0c21f9638c/13/51e6b9e4e4b062dafa7099b9/587274d215d5db0c21f96391/1240-05142015/1588008435196/site.css?&amp;filterFeatures=false" rel="stylesheet" type="text/css"/><meta content="056Rz5eX4pBAUwj1MOCzTWZ6kNBoVPdgplkIv191WjA" name="google-site-verification"/><script>Static.COOKIE_BANNER_CAPABLE = true;</script>
<!-- End of Squarespace Headers -->
</head>
<body class="index-transition-fade auto-play slides-click-through lightbox-transition-slide content-alignment-center underline-body-links sidebar-position-right show-social-icons social-icon-style-normal show-category-navigation event-show-past-events event-thumbnails event-thumbnail-size-32-standard event-date-label event-list-show-cats event-list-date event-list-time event-list-address event-icalgcal-links event-excerpts event-list-cta-button event-list-compact-view product-list-titles-under product-list-alignment-left product-item-size-11-square product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing tweak-v1-related-products-image-aspect-ratio-11-square tweak-v1-related-products-details-alignment-center newsletter-style-dark hide-opentable-icons opentable-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square image-block-poster-text-alignment-center image-block-card-dynamic-font-sizing image-block-card-content-position-center image-block-card-text-alignment-left image-block-overlap-dynamic-font-sizing image-block-overlap-content-position-center image-block-overlap-text-alignment-left image-block-collage-dynamic-font-sizing image-block-collage-content-position-top image-block-collage-text-alignment-left image-block-stack-dynamic-font-sizing image-block-stack-text-alignment-left button-style-solid button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd not-found-page mobile-style-available page-" id="not-found">
<div class="sqs-announcement-bar-dropzone"></div><!--Fixes index page loading issue when announcement bar is enabled -->
<div id="canvasWrapper">
<div id="canvas">
<div id="headerWrapper">
<header id="header">
<div class="wrapper">
<div data-content-field="site-title" id="logo">
<h1 class="logo logo-text"><a href="/">SEEDNINE</a></h1>
<script>
              Y.use('squarespace-ui-base', function(Y) {
                Y.one(".logo a").plug(Y.Squarespace.TextShrink, {
                  parentEl: Y.one('#header'),
                  triggerWidth: 768
                });
              });
            </script>
</div>
<span class="ctrl-button menu"><a class="icon-hamburger"></a></span>
<div id="topNav">
<nav class="main-nav" data-content-field="navigation">
<div class="nav-wrapper">
<ul class="cf">
<li class="index-collection folder">
<!--FOLDER-->
<div class="folder-parent">
<a aria-haspopup="true" href="/"><span>Commerce</span></a>
<div class="folder-child-wrapper">
<ul class="folder-child">
<li class="">
<a href="/volvo-exceptional-canadians" title="Volvo Exceptional Canadians">
<span>Volvo Exceptional Canadians</span>
</a>
</li>
<li class="">
<a href="/women-in-music" title="Women In Canadian Music">
<span>Women In Canadian Music</span>
</a>
</li>
<li class="">
<a href="/gaggenau" title="Gaggenau">
<span>Gaggenau</span>
</a>
</li>
<li class="">
<a href="/porsche" title="Porsche">
<span>Porsche</span>
</a>
</li>
<li class="">
<a href="/california" title="California">
<span>California</span>
</a>
</li>
<li class="">
<a href="/hip-hop-weekend" title="Miami">
<span>Miami</span>
</a>
</li>
<li class="">
<a href="/winnie-harlow" title="Winnie Harlow x Cadillac">
<span>Winnie Harlow x Cadillac</span>
</a>
</li>
<li class="">
<a href="/the-white-shirts" title="The White Shirts">
<span>The White Shirts</span>
</a>
</li>
<li class="">
<a href="/kylie-masse" title="Olympian Kylie Masse">
<span>Olympian Kylie Masse</span>
</a>
</li>
<li class="">
<a href="/pursuit-" title="UofT Pursuit Magazine - Activity Overload">
<span>UofT Pursuit Magazine - Activity Overload</span>
</a>
</li>
<li class="">
<a href="/pursuit-olympians" title="UofT Pursuit Magazine - London Calling">
<span>UofT Pursuit Magazine - London Calling</span>
</a>
</li>
<li class="">
<a href="/stacey-mckenzie-i" title="Supermodel Stacey McKenzie I">
<span>Supermodel Stacey McKenzie I</span>
</a>
</li>
<li class="">
<a href="/storm" title="Musician Rufus Wainwright">
<span>Musician Rufus Wainwright</span>
</a>
</li>
<li class="">
<a href="/musician-sean-jones" title="Musician Sean Jones">
<span>Musician Sean Jones</span>
</a>
</li>
<li class="">
<a href="/zadejones" title="Zade Jones">
<span>Zade Jones</span>
</a>
</li>
<li class="">
<a href="/tinted-colour-story" title="Tinted">
<span>Tinted</span>
</a>
</li>
<li class="">
<a href="/stacey-mckenzie-ii" title="Supermodel Stacey McKenzie II">
<span>Supermodel Stacey McKenzie II</span>
</a>
</li>
<li class="">
<a href="/luminato-x-jrn-weisbrodt" title="Luminato x Jörn Weisbrodt">
<span>Luminato x Jörn Weisbrodt</span>
</a>
</li>
<li class="">
<a href="/the-beach" title="The Beach">
<span>The Beach</span>
</a>
</li>
<li class="">
<a href="/the-grid-x-sarah-elton" title="Sarah Elton">
<span>Sarah Elton</span>
</a>
</li>
</ul>
</div>
</div>
</li>
<li class="index-collection folder">
<!--FOLDER-->
<div class="folder-parent">
<a aria-haspopup="true" href="/art"><span>Art</span></a>
<div class="folder-child-wrapper">
<ul class="folder-child">
<li class="">
<a href="/further" title="Fürther_Experience 1">
<span>Fürther_Experience 1</span>
</a>
</li>
<li class="">
<a href="/frther_experience-2" title="Fürther_Experience 2">
<span>Fürther_Experience 2</span>
</a>
</li>
<li class="">
<a href="/frther_experience-3" title="Fürther_Experience 3">
<span>Fürther_Experience 3</span>
</a>
</li>
<li class="">
<a href="/light-1" title="Light">
<span>Light</span>
</a>
</li>
<li class="">
<a href="/the-muumuu-man" title="The Muumuu Man">
<span>The Muumuu Man</span>
</a>
</li>
<li class="">
<a href="/tattoo" title="Tattoo">
<span>Tattoo</span>
</a>
</li>
<li class="">
<a href="/poor-but-sexy" title="Poor But Sexy Magazine">
<span>Poor But Sexy Magazine</span>
</a>
</li>
<li class="">
<a href="/new-gallery" title="Poor But Sexy - The Outtakes Exhibition">
<span>Poor But Sexy - The Outtakes Exhibition</span>
</a>
</li>
<li class="">
<a href="/new-gallery-1" title="DEKADE I">
<span>DEKADE I</span>
</a>
</li>
<li class="">
<a href="/dekade-2" title="FRIENDS &amp; FAMILY">
<span>FRIENDS &amp; FAMILY</span>
</a>
</li>
<li class="">
<a href="/mutti-exhibition-001-identity" title="Mutti Exhibition 001: Identity">
<span>Mutti Exhibition 001: Identity</span>
</a>
</li>
</ul>
</div>
</div>
</li>
<li class="page-collection">
<a href="/about"><span>About</span></a>
</li>
<li class="page-collection">
<a href="/clients"><span>Our Clients</span></a>
</li>
<li class="page-collection">
<a href="/contact-us"><span>Contact Us</span></a>
</li>
</ul>
</div>
</nav>
<div class="social-links sqs-svg-icon--list" data-content-field="connected-accounts" id="sqs-social">
<a class="sqs-svg-icon--wrapper instagram-unauth" href="https://www.instagram.com/seed9/" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--icon" xlink:href="#instagram-unauth-icon"></use>
<use class="sqs-use--mask" xlink:href="#instagram-unauth-mask"></use>
</svg>
</div>
</a>
</div>
</div>
</div>
</header>
<nav id="mobile-navigation">
<!--MOBILE-->
<ul>
<li class="index-collection active-folder">
<input class="folder-toggle-box hidden" id="folder-toggle-5872757c5016e1fec2725b52" name="folder-toggle-5872757c5016e1fec2725b52" type="checkbox"/>
<label class="folder-toggle-label" for="folder-toggle-5872757c5016e1fec2725b52"><a href="/">Commerce</a></label>
<ul>
<li class="gallery-collection">
<a href="/volvo-exceptional-canadians">Volvo Exceptional Canadians</a>
</li>
<li class="gallery-collection">
<a href="/women-in-music">Women In Canadian Music</a>
</li>
<li class="gallery-collection">
<a href="/gaggenau">Gaggenau</a>
</li>
<li class="gallery-collection">
<a href="/porsche">Porsche</a>
</li>
<li class="gallery-collection">
<a href="/california">California</a>
</li>
<li class="gallery-collection">
<a href="/hip-hop-weekend">Miami</a>
</li>
<li class="gallery-collection">
<a href="/winnie-harlow">Winnie Harlow x Cadillac</a>
</li>
<li class="gallery-collection">
<a href="/the-white-shirts">The White Shirts</a>
</li>
<li class="gallery-collection">
<a href="/kylie-masse">Olympian Kylie Masse</a>
</li>
<li class="gallery-collection">
<a href="/pursuit-">UofT Pursuit Magazine - Activity Overload</a>
</li>
<li class="gallery-collection">
<a href="/pursuit-olympians">UofT Pursuit Magazine - London Calling</a>
</li>
<li class="gallery-collection">
<a href="/stacey-mckenzie-i">Supermodel Stacey McKenzie I</a>
</li>
<li class="gallery-collection">
<a href="/storm">Musician Rufus Wainwright</a>
</li>
<li class="gallery-collection">
<a href="/musician-sean-jones">Musician Sean Jones</a>
</li>
<li class="gallery-collection">
<a href="/zadejones">Zade Jones</a>
</li>
<li class="gallery-collection">
<a href="/tinted-colour-story">Tinted</a>
</li>
<li class="gallery-collection">
<a href="/stacey-mckenzie-ii">Supermodel Stacey McKenzie II</a>
</li>
<li class="gallery-collection">
<a href="/luminato-x-jrn-weisbrodt">Luminato x Jörn Weisbrodt</a>
</li>
<li class="gallery-collection">
<a href="/the-beach">The Beach</a>
</li>
<li class="gallery-collection">
<a href="/the-grid-x-sarah-elton">Sarah Elton</a>
</li>
</ul>
</li>
<li class="index-collection active-folder">
<input class="folder-toggle-box hidden" id="folder-toggle-58727db59f745668dc76cc7f" name="folder-toggle-58727db59f745668dc76cc7f" type="checkbox"/>
<label class="folder-toggle-label" for="folder-toggle-58727db59f745668dc76cc7f"><a href="/art">Art</a></label>
<ul>
<li class="gallery-collection">
<a href="/further">Fürther_Experience 1</a>
</li>
<li class="gallery-collection">
<a href="/frther_experience-2">Fürther_Experience 2</a>
</li>
<li class="gallery-collection">
<a href="/frther_experience-3">Fürther_Experience 3</a>
</li>
<li class="gallery-collection">
<a href="/light-1">Light</a>
</li>
<li class="gallery-collection">
<a href="/the-muumuu-man">The Muumuu Man</a>
</li>
<li class="gallery-collection">
<a href="/tattoo">Tattoo</a>
</li>
<li class="gallery-collection">
<a href="/poor-but-sexy">Poor But Sexy Magazine</a>
</li>
<li class="gallery-collection">
<a href="/new-gallery">Poor But Sexy - The Outtakes Exhibition</a>
</li>
<li class="gallery-collection">
<a href="/new-gallery-1">DEKADE I</a>
</li>
<li class="gallery-collection">
<a href="/dekade-2">FRIENDS &amp; FAMILY</a>
</li>
<li class="gallery-collection">
<a href="/mutti-exhibition-001-identity">Mutti Exhibition 001: Identity</a>
</li>
</ul>
</li>
<li class="page-collection ">
<a href="/about">About</a>
</li>
<li class="page-collection ">
<a href="/clients">Our Clients</a>
</li>
<li class="page-collection ">
<a href="/contact-us">Contact Us</a>
</li>
</ul>
<div class="social-links sqs-svg-icon--list" id="sqs-social">
<a class="sqs-svg-icon--wrapper instagram-unauth" href="https://www.instagram.com/seed9/" target="_blank">
<div>
<svg class="sqs-svg-icon--social" viewbox="0 0 64 64">
<use class="sqs-use--icon" xlink:href="#instagram-unauth-icon"></use>
<use class="sqs-use--mask" xlink:href="#instagram-unauth-mask"></use>
</svg>
</div>
</a>
</div>
<div id="mobile-location">
<div>Seed9</div>
<div>626 Rhodes Avenue</div>
<div>Toronto, ON, M4J 4X6</div>
<div>Canada</div>
<div>416 986 2234</div>
</div>
</nav>
</div>
<div class="pageWrapper">
<section id="page">
<div data-content-field="main-content" id="mainContent" role="main">
<p>We couldn't find the page you were looking for. This is either because:</p>
<ul>
<li>There is an error in the URL entered into your web browser. Please check the URL and try again.</li>
<li>The page you are looking for has been moved or deleted.</li>
</ul>
<p>
  You can return to our homepage by <a href="/">clicking here</a>, or you can try searching for the
  content you are seeking by <a href="/search">clicking here</a>.
</p>
</div>
</section>
</div>
</div>
</div>
<script src="https://static1.squarespace.com/static/ta/51e6b9c9e4b050adffbe392c/1240/scripts/combo/?site.js" type="text/javascript"></script>
<script data-sqs-type="imageloader-bootstrapper" type="text/javascript">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap({}, document); }})();</script><script>Squarespace.afterBodyLoad(Y);</script><svg data-usage="social-icons-svg" style="display:none" version="1.1" xmlns="http://www.w3.org/2000/svg"><symbol id="instagram-unauth-icon" viewbox="0 0 64 64"><path d="M46.91,25.816c-0.073-1.597-0.326-2.687-0.697-3.641c-0.383-0.986-0.896-1.823-1.73-2.657c-0.834-0.834-1.67-1.347-2.657-1.73c-0.954-0.371-2.045-0.624-3.641-0.697C36.585,17.017,36.074,17,32,17s-4.585,0.017-6.184,0.09c-1.597,0.073-2.687,0.326-3.641,0.697c-0.986,0.383-1.823,0.896-2.657,1.73c-0.834,0.834-1.347,1.67-1.73,2.657c-0.371,0.954-0.624,2.045-0.697,3.641C17.017,27.415,17,27.926,17,32c0,4.074,0.017,4.585,0.09,6.184c0.073,1.597,0.326,2.687,0.697,3.641c0.383,0.986,0.896,1.823,1.73,2.657c0.834,0.834,1.67,1.347,2.657,1.73c0.954,0.371,2.045,0.624,3.641,0.697C27.415,46.983,27.926,47,32,47s4.585-0.017,6.184-0.09c1.597-0.073,2.687-0.326,3.641-0.697c0.986-0.383,1.823-0.896,2.657-1.73c0.834-0.834,1.347-1.67,1.73-2.657c0.371-0.954,0.624-2.045,0.697-3.641C46.983,36.585,47,36.074,47,32S46.983,27.415,46.91,25.816z M44.21,38.061c-0.067,1.462-0.311,2.257-0.516,2.785c-0.272,0.7-0.597,1.2-1.122,1.725c-0.525,0.525-1.025,0.85-1.725,1.122c-0.529,0.205-1.323,0.45-2.785,0.516c-1.581,0.072-2.056,0.087-6.061,0.087s-4.48-0.015-6.061-0.087c-1.462-0.067-2.257-0.311-2.785-0.516c-0.7-0.272-1.2-0.597-1.725-1.122c-0.525-0.525-0.85-1.025-1.122-1.725c-0.205-0.529-0.45-1.323-0.516-2.785c-0.072-1.582-0.087-2.056-0.087-6.061s0.015-4.48,0.087-6.061c0.067-1.462,0.311-2.257,0.516-2.785c0.272-0.7,0.597-1.2,1.122-1.725c0.525-0.525,1.025-0.85,1.725-1.122c0.529-0.205,1.323-0.45,2.785-0.516c1.582-0.072,2.056-0.087,6.061-0.087s4.48,0.015,6.061,0.087c1.462,0.067,2.257,0.311,2.785,0.516c0.7,0.272,1.2,0.597,1.725,1.122c0.525,0.525,0.85,1.025,1.122,1.725c0.205,0.529,0.45,1.323,0.516,2.785c0.072,1.582,0.087,2.056,0.087,6.061S44.282,36.48,44.21,38.061z M32,24.297c-4.254,0-7.703,3.449-7.703,7.703c0,4.254,3.449,7.703,7.703,7.703c4.254,0,7.703-3.449,7.703-7.703C39.703,27.746,36.254,24.297,32,24.297z M32,37c-2.761,0-5-2.239-5-5c0-2.761,2.239-5,5-5s5,2.239,5,5C37,34.761,34.761,37,32,37z M40.007,22.193c-0.994,0-1.8,0.806-1.8,1.8c0,0.994,0.806,1.8,1.8,1.8c0.994,0,1.8-0.806,1.8-1.8C41.807,22.999,41.001,22.193,40.007,22.193z"></path></symbol><symbol id="instagram-unauth-mask" viewbox="0 0 64 64"><path d="M43.693,23.153c-0.272-0.7-0.597-1.2-1.122-1.725c-0.525-0.525-1.025-0.85-1.725-1.122c-0.529-0.205-1.323-0.45-2.785-0.517c-1.582-0.072-2.056-0.087-6.061-0.087s-4.48,0.015-6.061,0.087c-1.462,0.067-2.257,0.311-2.785,0.517c-0.7,0.272-1.2,0.597-1.725,1.122c-0.525,0.525-0.85,1.025-1.122,1.725c-0.205,0.529-0.45,1.323-0.516,2.785c-0.072,1.582-0.087,2.056-0.087,6.061s0.015,4.48,0.087,6.061c0.067,1.462,0.311,2.257,0.516,2.785c0.272,0.7,0.597,1.2,1.122,1.725s1.025,0.85,1.725,1.122c0.529,0.205,1.323,0.45,2.785,0.516c1.581,0.072,2.056,0.087,6.061,0.087s4.48-0.015,6.061-0.087c1.462-0.067,2.257-0.311,2.785-0.516c0.7-0.272,1.2-0.597,1.725-1.122s0.85-1.025,1.122-1.725c0.205-0.529,0.45-1.323,0.516-2.785c0.072-1.582,0.087-2.056,0.087-6.061s-0.015-4.48-0.087-6.061C44.143,24.476,43.899,23.682,43.693,23.153z M32,39.703c-4.254,0-7.703-3.449-7.703-7.703s3.449-7.703,7.703-7.703s7.703,3.449,7.703,7.703S36.254,39.703,32,39.703z M40.007,25.793c-0.994,0-1.8-0.806-1.8-1.8c0-0.994,0.806-1.8,1.8-1.8c0.994,0,1.8,0.806,1.8,1.8C41.807,24.987,41.001,25.793,40.007,25.793z M0,0v64h64V0H0z M46.91,38.184c-0.073,1.597-0.326,2.687-0.697,3.641c-0.383,0.986-0.896,1.823-1.73,2.657c-0.834,0.834-1.67,1.347-2.657,1.73c-0.954,0.371-2.044,0.624-3.641,0.697C36.585,46.983,36.074,47,32,47s-4.585-0.017-6.184-0.09c-1.597-0.073-2.687-0.326-3.641-0.697c-0.986-0.383-1.823-0.896-2.657-1.73c-0.834-0.834-1.347-1.67-1.73-2.657c-0.371-0.954-0.624-2.044-0.697-3.641C17.017,36.585,17,36.074,17,32c0-4.074,0.017-4.585,0.09-6.185c0.073-1.597,0.326-2.687,0.697-3.641c0.383-0.986,0.896-1.823,1.73-2.657c0.834-0.834,1.67-1.347,2.657-1.73c0.954-0.371,2.045-0.624,3.641-0.697C27.415,17.017,27.926,17,32,17s4.585,0.017,6.184,0.09c1.597,0.073,2.687,0.326,3.641,0.697c0.986,0.383,1.823,0.896,2.657,1.73c0.834,0.834,1.347,1.67,1.73,2.657c0.371,0.954,0.624,2.044,0.697,3.641C46.983,27.415,47,27.926,47,32C47,36.074,46.983,36.585,46.91,38.184z M32,27c-2.761,0-5,2.239-5,5s2.239,5,5,5s5-2.239,5-5S34.761,27,32,27z"></path></symbol></svg>
</body>
</html>
