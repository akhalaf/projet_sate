<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://www.exp59.ru/sites/default/files/favicon.png" rel="shortcut icon" type="image/png"/>
<meta content="Drupal 7 (http://drupal.org)" name="Generator"/>
<title>Страница не найдена | Управление государственной экспертизы Пермского края</title>
<link href="https://www.exp59.ru/sites/default/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exp59.ru/sites/default/files/css/css_7McI5_-ELBMFdDOct0s5hIqd08VjnM__PxKtxOorbig.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exp59.ru/sites/default/files/css/css__J50k_e1weRowxbQWFspGFwqGH-7n9z2vze1WPmsSKU.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exp59.ru/sites/default/files/css/css_Tkr2yDryFQJIaaxTVaInllC4XyU5xmI8wUfZvWcTIwM.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.exp59.ru/sites/default/files/css/css_2THG1eGiBIizsWFeexsNe1iDifJ00QRS9uSd03rY9co.css" media="print" rel="stylesheet" type="text/css"/>
<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" href="https://www.exp59.ru/sites/all/themes/red/css/ie.css?pfe1rd" media="all" />
<![endif]-->
<!--[if IE 6]>
<link type="text/css" rel="stylesheet" href="https://www.exp59.ru/sites/all/themes/red/css/ie6.css?pfe1rd" media="all" />
<![endif]-->
<script src="https://www.exp59.ru/sites/default/files/js/js_vDrW3Ry_4gtSYaLsh77lWhWjIC6ml2QNkcfvfP5CVFs.js" type="text/javascript"></script>
<script src="https://www.exp59.ru/sites/default/files/js/js_2_1lOAUO2d3daLv8-umFOVNFpwrfgk1GK6itXJ7BuO8.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"red","theme_token":"hTa_DAklezl-O0veCcrvbiKXURd5P-x3-ydNDUSoHd0","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"public:\/\/languages\/ru_KPCKhkXMA35Z5CyPbACG1023GGw54j1fpwgw03gRfv8.js":1,"sites\/all\/modules\/dhtml_menu\/dhtml_menu.js":1,"sites\/all\/modules\/gp_utils\/js\/flipclock.js":1,"sites\/all\/modules\/gp_utils\/js\/gp_utils.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/dhtml_menu\/dhtml_menu.css":1,"sites\/all\/modules\/gp_utils\/js\/flipclock.css":1,"sites\/all\/themes\/red\/css\/style.css":1,"sites\/all\/themes\/red\/css\/print.css":1,"sites\/all\/themes\/red\/css\/ie.css":1,"sites\/all\/themes\/red\/css\/ie6.css":1}},"dhtmlMenu":{"nav":"open","animation":{"effects":{"height":"height","width":0,"opacity":0},"speed":"500"},"effects":{"siblings":"close-same-tree","children":"none","remember":"0"},"filter":{"type":"whitelist","list":{"main-menu":"main-menu","devel":0,"management":0,"navigation":0,"shortcut-set-1":0,"user-menu":0}}}});
//--><!]]>
</script>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td height="100%">
<a class="logo" href="/" title="экспертиза пермь"><img alt="экспертиза пермь" border="0" src="https://www.exp59.ru/sites/all/themes/red/logo.png"/></a></td>
<td height="100%" width="100%">
<div class="title-t">
<h3>Краевое государственное автономное учреждение</h3>
<h1>Управление государственной экспертизы Пермского края</h1>
</div>
</td>
</tr>
<tr>
<td colspan="5" height="5" width="100%"></td>
</tr>
</table>
<table align="left" border="0" cellpadding="0" cellspacing="0" valign="top" width="100%">
<tr>
<td valign="top" width="260">
</td>
<td valign="top" width="10"> </td>
<td cellpadding="3" width="63%">
<h1 class="title" id="page-title">Страница не найдена</h1>
<div class="tabs"></div> <div class="block block-system" id="block-system-main">
<div class="content">
    Запрашиваемая страница "/modules/dashboard/Bl-Learn.htm?%09%0A" не найдена.  </div>
</div>
</td>
<td valign="top" width="38"> </td>
<td valign="top" width="270">
<div class="column sidebar" id="sidebar-second">
<a class="btn btn-danger" href="https://uslugi.exp59.ru/" target="_blank" title="Пройти госэкспертизу">
			Пройти госэкспертизу <b>в электронном виде</b>
</a>
<a class="btn btn-danger" href="https://www.exp59.ru/node/57" title="Пройти госэкспертизу">
			Информация о госэкспертизе <b>в электронном виде</b>
</a>
<div class="block block-gp-utils" id="block-gp-utils-gp-time">
<div class="content">
<div id="gp_clock"></div> </div>
</div>
<div class="block block-views" id="block-views-news-block">
<h2 class="block-title-link"><a href="/news" title="В раздел новости">Новости</a></h2>
<div class="content">
<div class="view view-news view-id-news view-display-id-block view-dom-id-1b1fb44d550cb2820da72356e67d714f">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first">
<div class="views-field views-field-title"> <div class="field-content"><a href="/node/109">04.12.2020 ВНИМАНИЕ! АВАРИЙНЫЕ РАБОТЫ НА ОБОРУДОВАНИИ</a></div> </div> </div>
<div class="views-row views-row-2 views-row-even">
<div class="views-field views-field-title"> <div class="field-content"><a href="/node/108">05.10.2020 ВНИМАНИЕ!!! С 05.10.2020 Офис учреждения не ведет личный прием, взаимодействие с Заявителями осуществляется только в дистанционном режиме. Сотрудники госэкспертизы Пермского края работают дистанционно.</a></div> </div> </div>
<div class="views-row views-row-3 views-row-odd views-row-last">
<div class="views-field views-field-title"> <div class="field-content"><a href="/node/107">29.09.2020   ОТКРЫТЫЙ РЕГИОНАЛЬНЫЙ КОНКУРС «МОЛОДЕЖНЫЙ РЕЗЕРВ ПРИКАМЬЯ 2020»</a></div> </div> </div>
</div>
</div> </div>
</div>
</div>
</td>
</tr>
<tr>
<td colspan="5">
<hr/>
<div class="footer_wrapp">
<div class="footer_right">
<div class="block block-block" id="block-block-3">
<div class="content">
<!-- Yandex.Metrika counter --><script type="text/javascript">// <![CDATA[
(function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49334266 = new Ya.Metrika2({
                    id:49334266,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
// ]]></script><noscript><div><img alt="" src="https://mc.yandex.ru/watch/49334266" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->

E-mail: expertiza@permkray.ru<br/>
Приемная: 8 (342) 236-01-46; 8 (342) 236-01-31 (доб. 101)<br/>
Факс: 8(342) 236-30-05<br/> 
Бухгалтерия: 8(342) 236-08-65<br/> 
Общий отдел (прием, обработка и выдача документов): 8 (342) 236-08-10; 8 (342) 236-11-82 (доб. 102); 8 (342) 236-12-32 (доб. 118)  </div>
</div>
</div>
<div class="footer_left">
</div>
</div>
</td>
</tr>
</table>
</body>
</html>
