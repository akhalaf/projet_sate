<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Superior Cleaners</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css"/>
<link href="superior.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="leftColumn"> <!--class="testBackGnd" -->
<div id="headerStyle">
<h1>Superior Cleaners </h1>
<p>(708) 352-2266<br/>
      superiorcleaners99@gmail.com</p>
</div>
<ul class="MenuBarHorizontal" id="MenuBar1">
<!-- "history.php 
             whoweare.php
           customers.php
     -->
<li><a href="#">Home</a> <!--class="MenuBarItemSubmenu" -->
<!--    <ul>
          <li><a href="#">Item 1.1</a></li>
          <li><a href="#">Item 1.2</a></li>
          <li><a href="#">Item 1.3</a></li>
        </ul> -->
<!--    </li>
    <li><a href="AboutUs.html">About Us</a></li>
    <li><a  href="Services.html">Services</a> -->
<!--    <ul>     class="MenuBarItemSubmenu"
          <li><a class="MenuBarItemSubmenu" href="#">Item 3.1</a>
           <ul>
              <li><a href="#">Item 3.1.1</a></li>
              <li><a href="#">Item 3.1.2</a></li>
            </ul> 
          </li>
          <li><a href="#">Item 3.2</a></li>
          <li><a href="#">Item 3.3</a></li>
    </ul>-->
</li>
<!--<li><a href="#">Contact US</a></li>-->
</ul>
<div>
<p class="andy"> Here at the Superior Dry Cleaners we offer an array of cleaning and tailoring services that are sure to cater to your specific needs. We are a family owned business that has been in the field for many years and multiple generations. Our father, a man by the name of  Andy Pierropoulos, learned his craft in tailoring as a small boy in Greece. In 1968, opportunity allowed him to open up Army Trail Cleaners in Addison, IL. His expertise became well known and his leadership enabled us, his three sons, to continue his business and expand to two additional locations: the Superior Dry Cleaners, located in La Grange Park, IL and Farmwood Dry Cleaners, located in Farmwood, IL. It is our hope that we are able to service you or your company with the best possible dry cleaning available. </p>
<center>
<img height="228" src="Background1.jpg" width="400"/> </center></div>
</div>
<div id="rightColumn">
<center>
<img alt="" height="135" src="SC_Logo.jpg" width="249"/>
</center>
<div class="Phone">
<p><strong><em>Hours of Operation</em></strong><br/>
      Mon-Fri: 7:30-7pm<br/>
      Sat: 8-6pm<br/>
      Sun: Closed</p>
</div>
<div class="Phone">
<p> <em><strong>Superior</strong></em><br/>
      1123 N. LaGrange Rd.<br/>
      LaGrange Park, IL </p>
</div>
<div class="Phone">
<p><strong><em>Farmwood</em></strong><br/>
      1250 w. Lake Street<br/>
      Addison, IL</p>
</div>
</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
