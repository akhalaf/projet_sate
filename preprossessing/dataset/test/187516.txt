<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="IT" class="no-js ie6"> <![endif]--><!--[if IE 7 ]>    <html lang="IT" class="no-js ie7"> <![endif]--><!--[if IE 8 ]>    <html lang="IT" class="no-js ie8"> <![endif]--><!--[if IE 9 ]>    <html lang="IT" class="no-js ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="IT"> <!--<![endif]-->
<head>
<title>Banca del Fucino - La tua Banca in Lazio, Abruzzo e Marche</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="J7rTBUKX1cR1-xHb1vDKcab1Srhwo_vwh0yNzIEj26U" name="google-site-verification"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="https://fonts.googleapis.com/css?family=News+Cycle|Anton" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,700italic" rel="stylesheet" type="text/css"/>
<meta content="" name="keywords"/>
<meta content="La tua Banca in Lazio, Abruzzo e Marche, attenta a territorio e persone. Qualità dell'offerta, trasparenza nelle operazioni, rapporto personalizzato col cliente" name="description"/>
<!--[if IE 9]>
  <style type="text/css">
    .gradient { filter: none !important;}
  </style>
<![endif]-->
<link href="/skin/website/css/default.css" media="all" rel="stylesheet" type="text/css"/>
<link href="/skin/website/css/responsive.css?t=1" media="all" rel="stylesheet" type="text/css"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35715636-1', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['www.contoiu.it', 'ihb.cedacri.it']);
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
<script src="/js/modernizr-1.7.min.js"></script>
<script src="/js/LAB.min.js"></script>
</head>
<body class="home">
<div id="container">
<form action="" id="EuCookieLawForm" method="POST" style="position: fixed; top: 0; left: 0; display: block; width: 100%; margin: 0; padding: 0; z-index: 99999999; background-color: #36c; background-color: rgba(11, 80, 137, .90);font-family: Arial;text-shadow: 1px 0 0 rgba(0, 0, 0, 1);line-height: 18px;"><fieldset style="position: relative; padding: 30px 0; margin: 0 auto; width: 90%; border: none;"><p style="position: relative; float: left; width: 85%; margin-bottom: 10px; font-size: 15px; font-weight: normal; color: #fff; text-align: left;font-family: 'News Cycle',arial,serif;">Questo sito utilizza cookie tecnici, anche di terze parti, per rilevazioni statistiche o di supporto ai servizi.<br/>Se vuoi saperne di più <a href="https://www.bancafucino.it/Cookie" style="color: #fff" target="_blank">clicca qui</a>. Proseguendo la navigazione acconsenti all'uso di tutti i cookie.</p><p style="text-align: right;"><input id="EuCookieLaw" name="EuCookieLaw" type="hidden" value="accetto"/><input name="EuCookieLawForm-submit" style="position: relative; float: right; margin-bottom: 10px; padding: 1px 20px; font-size: 12px; font-weight: normal; font-family: 'News Cycle',arial,serif; cursor: pointer; border: solid 1px #CFCFCF;background-color: #CFCFCF;" type="submit" value="OK"/></p></fieldset></form><header class="clearfix" id="header">
<div class="container">
<a id="top" name="top"></a>
<div id="logo">
<a href="/" title="Home"><img alt="Logo BancadelFucino" class="iLogo" src="/skin/website/img/logo.gif"/>
</a>
</div>
<h1 class="hidden">
			BancadelFucino		</h1>
<nav id="menu-generale">
<ul class="menu ">
<li class="level1 famiglie"><a href="/conto-iu-online" title="Famiglie">Famiglie</a>
<ul>
<li class="level2 carta-conto"><a href="/carta-conto" title="Carta Conto">Carta Conto</a></li>
<li class="level2 conti-giovani"><a href="/Conti-giovani/" title="Conti Giovani">Conti Giovani</a></li>
<li class="level2 carte"><a href="/Carte-Privati" title="Carte">Carte</a></li>
<li class="level2 conti-correnti"><a href="/Conti-correnti/" title="Conti correnti">Conti correnti</a></li>
<li class="level2 pensioniamo"><a href="/Pensioniamo/" title="Pensioniamo">Pensioniamo</a></li>
<li class="level2 risparmio"><a href="/Risparmio/" title="Risparmio">Risparmio</a></li>
<li class="level2 risparmio-gestito"><a href="/risparmio-gestito/" title="Risparmio gestito">Risparmio gestito</a></li>
<li class="level2 prodotti-assicurativi"><a href="/Prodotti-Assicurativi/" title="Prodotti Assicurativi">Prodotti Assicurativi</a></li>
<li class="level2 finanziamenti-privati"><a href="/Finanziamenti-privati/" title="Finanziamenti Privati">Finanziamenti Privati</a></li>
<li class="level2 mutui-casa"><a href="/Mutui-casa" title="Mutui casa">Mutui casa</a>
</li>
<li class="level2 in/b@nca-light"><a href="/inbanca-light/" title="IN/b@nca Light">IN/b@nca Light</a></li>
<li class="level2 trading-on-line"><a href="/Trading-on-line/" title="Trading on line">Trading on line</a></li>
</ul></li>
<li class="level1 aziende"><a href="/Finanziamenti/" title="Aziende">Aziende</a>
<ul>
<li class="level2 conti-correnti-"><a href="/Conti-Correnti" title="Conti Correnti ">Conti Correnti </a></li>
<li class="level2 finanziamenti"><a href="/Finanziamenti/" title="Finanziamenti">Finanziamenti</a></li>
<li class="level2 fondo-di-garanzia-pmi"><a href="/Fondo-di-Garanzia-PMI" title="Fondo di Garanzia PMI">Fondo di Garanzia PMI</a></li>
<li class="level2 estero"><a href="/Estero/" title="Estero">Estero</a></li>
<li class="level2 carte-di-credito"><a href="/Carte-Aziendali/" title="Carte di credito">Carte di credito</a></li>
<li class="level2 leasing"><a href="/Leasing/" title="Leasing">Leasing</a></li>
<li class="level2 inb@nca-business"><a href="/inbanca-business/" title="INb@nca Business">INb@nca Business</a></li>
<li class="level2 incassi-elettronici"><a href="/pagamenti-elettronici" title="Incassi elettronici">Incassi elettronici</a></li>
</ul></li>
<li class="level1 private-banking"><a href="https://privatebanking.bancafucino.it/" title="Private Banking">Private Banking</a></li>
</ul> </nav>
<div class="menu-secondario">
<ul class="menu clearfix">
<li class="level1 chi-siamo"><a href="/chi-siamo/" title="Chi siamo">Chi siamo</a></li>
<li class="level1 contatti"><a href="/Contatti" title="Contatti">Contatti</a></li>
<li class="level1 dove-siamo"><a href="/dove-siamo" title="Dove siamo">Dove siamo</a></li>
</ul> </div>
</div>
<div id="sliderContainer">
<div class="slideContainer" style="height: 354px; background: url(/attachfile/content/home_immagini/0/819/9.1.0.bannercovid19unico.png) no-repeat 50% 0;"><div class="container" style="position: relative;z-index: 0"><a class="slider-link" href="/emergenza-covid-19-iniziative-e-informazioni"></a><div class="clearfix"></div></div></div> </div>
</header>
<div class="" id="main" role="main">
<div class="container clearfix">
<section class="clearfix" id="contentSpace">
<div class="clearfix" id="box-container">
<div class="box famiglie">
<h3 class="bt-famiglie gradient">Famiglie</h3>
<div class="boxContent">
<h4>I servizi bancari, le soluzioni di risparmio e previdenza, i finanziamenti per la famiglia e i giovani...</h4>
<div class="footer"><a href="/Carta-Conto" title="Scopri di più">Scopri di più</a></div>
</div>
</div>
<div class="box aziende">
<h3 class="bt-aziende gradient">Aziende</h3>
<div class="boxContent">
<h4>Le soluzioni per la banca d'impresa e il credito dedicate ad aziende e professionisti</h4>
<div class="footer"><a href="/Conti-Correnti" title="Scopri di più">Scopri di più</a></div>
</div>
</div>
<div class="box privatebanking last">
<h3 class="bt-private gradient">Private Banking</h3>
<div class="boxContent">
<h4>La tradizione d'eccellenza della più antica banca privata romana al servizio della tutela e valorizzazione dei patrimoni familiari</h4>
<div class="footer"><a href="http://privatebanking.bancafucino.it/" target="_blank" title="Scopri di più">Scopri di più</a></div>
</div>
</div>
<div class="box-container_evidenza clearfix">
<a href="https://www.bancafucino.it/attachfile/content/faq/0/744/0.3.0.comunicato_stampa_15042019.pdf" target="_blank" title="Scopri di più"><img height="200" src="/skin/website/img/banner.png" width="643"/></a>
</div>
</div>
</section>
<aside id="side-bar">
<div id="menu-generico">
<h1 class="gradient">Servizi on line</h1>
<div class="side-bar_wrapper">
<div class="asideForms" style="background-color: #fff;">
<!-- CEDACRI ACCESSO PRIVATI -->
<div class="wrapper">
<form action="https://ihb2.cedacri.it/hb/authentication/login.seam?abi=03124" class="form_servizi_online" id="form_accesso_privati" method="post" name="_LoginForm" target="_top">
<p class="titolo">Accesso privati</p>
<input name="RedirectTo" type="hidden" value="/hbnet/login.nsf/PollRequest?CreateDocument&amp;login=1&amp;canale=&amp;id=&amp;chiamante=03124"/>
<p>
<input class="text input_utente" name="utente" placeholder="codice utente" type="text"/>
</p>
<p>
<input class="text input_password" name="password" placeholder="password" type="password"/>
</p>
<div style="position: relative; float: left; left: 0; width: 100%; clear: both;">
<div class="more"><a href="javascript:parent.location.href = 'http://www.bancafucino.it/Inbanca-Light/';"><img alt="Scopri di più" src="/skin/website/widgets/login/more.jpg"/></a></div>
<div class="entra"><button type="submit"><img alt="Entra" src="/skin/website/widgets/login/entra.jpg"/></button></div>
</div>
</form>
</div>
<!-- CEDACRI ACCESSO IMPRESE -->
<div class="wrapper">
<form action="https://core.cedacri.it/CORE/main/LogonAttempt?from=ext&amp;abi=03124 " class="form_servizi_online" id="form_accesso_imprese" method="post" name="formauth" target="_top">
<p class="titolo">Accesso imprese</p>
<input id="id1_hf_0" name="id1_hf_0" type="hidden"/>
<p class="input_max_width">
<input class="text input_postazione" name="postazione" placeholder="numero postazione" type="text"/>
</p>
<p>
<input class="text input_utente" name="alias" placeholder="alias" type="text"/>
</p>
<p>
<input class="text input_password" name="password" placeholder="password" type="password"/>
</p>
<div style="position: relative; float: left; left: 0; width: 100%; clear: both;">
<div class="more"><a href="https://www.bancafucino.it/Inbanca-Business/" target="_top"><img alt="Scopri di più" src="/skin/website/widgets/login/more.jpg"/></a></div>
<div class="entra"><button><img alt="Entra" src="/skin/website/widgets/login/entra.jpg"/></button></div>
</div>
<div class="clear"></div>
</form>
</div>
<!-- <iframe id="iframe2" frameborder="0" scrolling="no" src="https://core.cedacri.it/static-content/03124/LogonStatic.html" width="100%" height="120"></iframe> -->
</div>
<img alt="Centro Assistenza" class="assistenza assistenza_img" height="54" src="/skin/website/img/img_centro-assistenza.png" width="208"/>
<div class="wrapper assistenza_box">
<p class="titolo">Centro assistenza</p>
<p>Dall'Italia: <a href="tel:800 955540" tabindex="-1">800 955 540</a></p>
<p>Dall'estero: <a href="tel:+39 0521 1922211" tabindex="-1">+39 0521 1922211</a></p>
<p class="suggerimento">Tocca il numero per chiamare</p>
</div>
<div id="finder"><a href="/Dove-siamo"><img alt="Dove siamo" src="/skin/website/widgets/bg_finder.jpg"/></a></div>
<ul class="menu clearfix">
<li class="level1 sicurezza"><a href="/Domande-e-risposte/" title="Sicurezza">Sicurezza</a></li>
<li class="level1 carte-numeri-utili"><a class="targetBlank" href="/Carte-Numeri-Utili/" target="_blank" title="CARTE numeri utili">CARTE numeri utili</a></li>
<li class="level1 trasparenza"><a href="/fogli-informativi-per-consumatori/" title="Trasparenza">Trasparenza</a></li>
</ul> </div>
</div>
<div class="side-bar_wrapper">
<div class="fb_logo_sidebar">
<span>
<a href="https://www.facebook.com/Banca.Fucino" target="_blank">
<img alt="Facebook" src="/skin/website/img/f_logo_picc.png"/>
</a>
</span>
<span>
<a href="https://www.facebook.com/Banca.Fucino" target="_blank">
					Diventa nostro fan <br/> su Facebook.
				</a>
</span>
</div>
</div>
</aside> </div>
</div><footer id="footer">
<div class="container">
<div class="menu-servizio">
<ul class="menu clearfix">
<li class="level1 trasparenza"><a href="/fogli-informativi-per-consumatori/" title="Trasparenza">Trasparenza</a></li>
<li class="level1 reclami"><a href="/reclami/" title="Reclami">Reclami</a></li>
<li class="level1 privacy"><a href="privacy" title="Privacy">Privacy</a></li>
<li class="level1 bilancio"><a href="/Bilancio/" title="Bilancio">Bilancio</a></li>
<li class="level1 informativa-societaria"><a href="/informativa-societaria" title="Informativa Societaria">Informativa Societaria</a></li>
<li class="level1 emergenza-covid-19-iniziative-e-informazioni"><a href="/emergenza-covid-19-iniziative-e-informazioni" title="Emergenza COVID-19 Iniziative e Informazioni">Emergenza COVID-19 Iniziative e Informazioni</a></li>
<li class="level1 cartolarizzazioni---securitisation"><a href="/cartolarizzazione-securitisation/" title="Cartolarizzazioni - Securitisation">Cartolarizzazioni - Securitisation</a></li>
<li class="level1 modello-dlgs-231-2001"><a href="https://www.bancafucino.it/modello-231-2001" title="Modello dlgs 231-2001">Modello dlgs 231-2001</a></li>
<li class="level1 informativa-cookie"><a href="https://www.bancafucino.it/cookie" title="Cookie">Informativa cookie</a></li>
<li class="level1 comunicati"><a class="targetBlank" href="https://www.bancafucino.it/avvisi" target="_blank" title="Comunicati">Comunicati</a></li>
<li class="level1 psd2---open-banking"><a href="https://www.bancafucino.it/psd2-open-banking" title="PSD2 - OPEN BANKING">PSD2 - Open Banking</a></li>
<li class="level1 acf---arbitro-per-le-controversie-finanziarie"><a class="targetBlank" href="https://www.acf.consob.it/" target="_blank" title="ACF - Arbitro per le Controversie Finanziarie">ACF - Arbitro per le Controversie Finanziarie</a></li>
</ul> </div>
<hr class="hidden"/>
<div class="footer clearfix">
<p>Banca del Fucino © 2003-2021 - Gruppo Bancario Igea Banca - P.IVA 04256050875, C.F. 04256050875  -- È vietata la riproduzione totale o parziale dei contenuti del sito - <a href="http://www.intesys.it/" rel="external" title="vai al sito Intesys">Web agency</a></p>
</div>
</div>
</footer>
</div>
<script src="/js/jquery-1.6.1.min.js" type="text/javascript"></script>
<script src="/js/jquery.foundation.reveal.js" type="text/javascript"></script>
<script src="/js/jquery.cycle.all.js" type="text/javascript"></script>
<script src="/js/jquery.placeholder-label.js" type="text/javascript"></script>
<script src="/js/generico.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

function onLoadHtml() {
	if (typeof(popupAccessibili)=='function') {
		popupAccessibili('Apri in nuova finestra')
	}
}
window.onload = onLoadHtml;


//]]>
</script>
</body>
</html>
