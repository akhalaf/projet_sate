<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-8874350-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8874350-1');
</script>
<meta charset="utf-8"/>
<title>Error at A1 Tourist Guide</title>
<link href="https://a1touristguide.com/errors/404.php" rel="canonical"/>
<meta content="We have changed oour website - Error page for a1 tourist guide" name="description"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="telephone=no" name="format-detection"/>
<!-- fonts, styles and functions -->
<link href="../css/a1tg2.css" rel="stylesheet" type="text/css"/>
</head>
<!-- BODY -->
<body>
<header class="header navbar-fixed-top"><nav class="navbar"><div class="container"><div class="menu-container js_nav-item"></div><input id="toggle" name="toggle" type="checkbox"/><label for="toggle"></label><div class="menu-mobile"><div class="collapse navbar-collapse nav-collapse "><div class="menu-container"><div class="noshow"><img alt="accommodation directory logo" class="img5" src="../images/logo.gif"/></div><ul class="nav navbar-nav container-right "><li class="js_nav-item nav-item"><a class="nav-item-child" href="https://a1touristguide.com/index.php">Home</a></li><li class="js_nav-item nav-item"><a class="nav-item-child" href="https://a1touristguide.com/members.php">Members Area</a></li><li class="js_nav-item nav-item"><a class="nav-item-child" href="https://a1touristguide.com/getlisted.php">Get Listed</a></li><li class="js_nav-item nav-item"><a class="nav-item-child" href="https://a1touristguide.com/contact.php">Contact</a></li></ul></div></div></div></div></nav></header>
<div id="wrapper">
<h1>A1 Tourist Guide</h1>
<h2>Whoops - that page has been moved or does not exist</h2>
<div class="content2">
<p class="just">
After more than 15 years in existence, we have recently completely re-written the A1TouristGuide website to meet new web authoring standards and give visitors a better experience. This could be why you are seeing this page now.
</p><p class="just">
Please use the navigation tabs at the top of this page to get to the page you are looking for. If you are still having problems, please contact us using the "Contact" tab above.</p>
</div><!-- content2"-->
<br/><br/>
<div class="footer"> <br/><br/><hr class="footline"/> <p class="footer2"> © A1TouristGuide - Website created and hosted by <a href="https://www.ayrshire-web-design.co.uk">Ayrshire Web Design</a></p></div>
<!--Start Cookie Script--> <script src="//cookie-script.com/s/d8e67ad16177b613856c052b141fb97f.js"></script> <!--End Cookie Script-->
</div><!-- wrapper-->
</body>
</html>