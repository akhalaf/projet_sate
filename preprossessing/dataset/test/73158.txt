<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en"><head><meta content="The ADA Home Page provides access to Americans with Disabilities Act (ADA) regulations for businesses and State and local governments, technical assistance materials, ADA Standards for Accessible Design, links to Federal agencies with ADA responsibilities and information, updates on new ADA requirements, streaming video, information about Department of Justice ADA settlement agreements, consent decrees, and enforcement activities and access to Freedom of Information Act (FOIA) ADA material" name="description"/><meta content="U.S. Department of Justice, Civil Rights Division, Disability Rights Section" name="creator"/><meta content="05-17-2013" name="Date Created"/><meta content="en-us" name="Language"/>
<title>ADA.gov homepage</title>
<script language="Javascript" src="/scripts/scripts.js" type="text/javascript"></script>
<script language="Javascript" src="/scripts/justiceiso.js" type="text/javascript">
function changeFontSize(inc)
{
  var p = document.getElementsByTagName('p');
  for(n=0; n<p.length; n++) {
    if(p[n].style.fontSize) {
       var size = parseInt(p[n].style.fontSize.replace("px", ""));
    } else {
       var size = 12;
    }
    p[n].style.fontSize = size+inc + 'px';
   }
}
</script>
<link href="ada.css" media="all" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<style type="text/css">
.title {
	font-family: Arial, Helvetica, sans-serif;
	font-size: .9em;
	font-weight: bold;
}
h2 a {
	color: #FFF;
}
h2 a hover {
	color: #FFF;
}
.index-leftcol {
	width: 465px;
	height: 100%;
	float: left;
	margin-bottom: 12px;
}
.left-col h1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.1em;
	color: #FFF;
	background-color: #003366;
	font-weight: bold;
	padding-top: 6px;
	padding-right: 6px;
	padding-bottom: 6px;
	padding-left: 16px;
	margin-bottom: 0px;
	margin-top: -1px;
#other h1 a {
	color: #FFF;
}
#other h1 a:hover {
	color: #fff;
	text-decoration: underline;
}
.anniv {
	background-color:#FFF;
	font-family:Arial, Helvetica, sans-serif;
	text-align:center;
	font-size:2em;
	margin-top: 6px;
	vertical-align: middle;
	padding-right: 6px;
	padding-left: 6px;
	padding-top:  2px;
	padding-bottom:  2px;
	
	border-bottom-style: solid;
	
	border-bottom-color: #990000;
	margin-right: 1px;
	margin-left: 1px;
	
	border-bottom-width: thick;
}
.anniv h1 {
	font-family:Arial, Helvetica, sans-serif;
	font-size:.75em;
	text-align:center;
	color:#003366;
}


</style>
<!-- We participate in the US government's analytics program. See the data at analytics.usa.gov. -->
<script async="" id="_fed_an_ua_tag" src="https://dap.digitalgov.gov/Universal-Federated-Analytics-Min.js?agency=DOJ" type="text/javascript"></script><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63173733-1', 'auto');
  ga('send', 'pageview');

</script></head>
<body lang="EN-US" onload="MM_preloadImages('images/2010-regs-on.gif','images/2010-std-on.gif','images/ta_on.gif','images/enforce-on.gif')" xml:lang="EN-US">
<div class="onecol"><div id="skipnav"><a href="#mainContent">Skip navigation</a></div>
<div id="topbanner"><div id="main-banner"> <a href="index.html"><img alt="ADA.gov United States Department of Justice, Civil Rights Division" border="0" height="124" src="images/main-banner.gif" width="274"/></a>
</div>
<div id="mid-banner">
<img alt="Information and Technical Assistance on the Americans with Disabilities Act" class="h1" height="124" src="images/mid-banner.gif" title="Information and Technical Assistance on the Americans with Disabilities Act" width="535"/>
</div>
<div id="Search-box">
<div class="search">
<div style="height: 40px;">
<label class="search-label" for="search">Search ADA.gov</label>
<form action="https://search.ada.gov/search" method="get" name="gs">
<input maxlength="50" name="query" size="18" type="text"/>
<input id="search" name="search" type="submit" value="go"/>
<input name="sort" type="hidden" value="date:D:L:d1"/>
<input name="output" type="hidden" value="xml_no_dtd"/>
<input name="ie" type="hidden" value="iso-8859-1"/>
<input name="oe" type="hidden" value="UTF-8"/>
<input name="client" type="hidden" value="default_frontend"/>
<input name="proxystylesheet" type="hidden" value="default_frontend"/>
<input name="affiliate" type="hidden" value="justice-ada"/>
</form></div>
<div style="height: 20px;"><a href="search.htm">More Search Options</a></div>
</div>
</div>
<div id="regs-off"> <a href="2010_regs.htm"><img alt="2010 Regulations" border="0" height="56" id="Image1" onblur="MM_swapImgRestore()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image1','','images/2010-regs-on.gif',1)" src="images/2010-regs-off.gif" width="208"/></a>
</div>
<div id="std-off"> <a href="2010ADAstandards_index.htm"><img alt="2010 Design Standards" border="0" height="56" id="Image2" onblur="MM_swapImgRestore()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image2','','images/2010-std-on.gif',1)" src="images/2010-std-off.gif" width="275"/></a>
</div>
<div id="ta-off"> <a href="ta-pubs-pg2.htm"><img alt="Technical Assistance Materials" border="0" height="56" id="Image3" onblur="MM_swapImgRestore()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image3','','images/ta_on.gif',1)" src="images/ta_off.gif" width="365"/></a>
</div>
<div id="enforce-off"> <a href="enforce_current.htm"><img alt="Enforcement" border="0" height="56" id="Image4" name="Image4" onblur="MM_swapImgRestore()" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','images/enforce-on.gif',1)" src="images/enforce-off.gif" width="172"/></a>
</div></div>
<div class="exitMsg" id="exitMsg" style="display:none;"> </div>
<div class="mid-body">
<main id="mainContent" tabindex="-1">
<div class="index-leftcol">
<div class="left-col" id="new">
<h1>New on ADA.gov</h1>
<p><a href="st_louis_sa.html"><strong>Board of Election Commissioners for the City of St. Louis, Missouri</strong></a><br/>
  Settlement Agreement (posted 1/12/21)
</p>
<p><a href="denver_pca/2021_denver_sa.html"><strong>2021 City and County of Denver, CO</strong></a><br/>
  Settlement Agreement (posted 1/8/21)
</p>
<p><a href="amc_usao_sa.html"><strong>AMC Entertainment</strong></a><br/>
  Settlement Agreement (posted 12/16/20)
</p>
<p><a href="gnhi_sa.html"><strong>Good Neighbors Homes, Inc.</strong></a><br/>
  Settlement Agreement (posted 12/16/20)</p>
<p><a href="nextgen_childcare_sa.html"><strong>NexGen Childcare Center</strong></a><br/>
  Settlement Agreement (posted 12/16/20)</p>
<p><a href="henderson_sons_sa.html"><strong>Henderson &amp; Sons Funeral Home, Inc.</strong></a><br/>
  Settlement Agreement (posted 12/16/20)</p>
<p><a href="olmstead/olmstead_cases_list2.htm#snd"><strong>State of North Dakota</strong></a><br/>
  Settlement Agreement (posted 12/14/20)
</p>
<p><a href="LA_film_school_sa.html"><strong>Los Angeles Film School</strong></a><br/>
  Settlement Agreement (posted 12/7/20)
</p>
<p><a href="swedish_med_sa.html"><strong>Swedish Medical Center</strong></a><br/>
  Settlement Agreement 
(posted 12/7/20)</p>
<p><a href="enforce_current.htm#amtrak"><strong>Assistant Attorney General Announces Settlement with Amtrak (National Railroad Passenger Corporation)</strong></a><br/>
  Complaint &amp; Settlement Agreement (posted 12/2/20)</p>
<p><img alt="red ribbon" height="40" src="hiv/images/ribbon.png" width="35"/><a href="https://www.justice.gov/opa/pr/statement-assistant-attorney-general-eric-dreiband-world-aids-day"><strong>World AIDS Day  Statement</strong></a> (posted 11/30/20)</p>
<p><a href="mdcf_sa.html"><strong>Massachusetts Department of Children and Families</strong></a><br/>
  Settlement Agreement (posted 11/19/20)
</p>
<p><a href="caroline_county_sa.html"><strong>Caroline County Commissioner of the Revenue</strong></a><br/>
  Settlement Agreement (posted 11/16/20)
</p>
<p><a href="betancourt_soi.html"><strong>Betancourt-Colon v. City of San Juan</strong></a><br/>
  Statement of Interest (posted 11/9/20)
</p>
<p><a href="midwest_plastic_surgery_sa.html"><strong>Midwest Plastic Surgery Center</strong></a><br/>
  Settlement Agreement (posted 11/3/20)
</p>
<p align="right"><a href="new.htm">Read More</a></p>
</div>
</div>
<div class="centercol" id="web-site content navigation">
<div></div>
<div class="blue-divider"></div>
<div class="rightcol-informationblk2">
<h2><a href="30th_anniversary/index.html" title="30th Anniversary of the ADA"> <strong>CELEBRATING THE 
     <font size="+1"><br/>
     30TH</font> ANNIVERSARY</strong></a></h2>
</div>
<div class="info-blk"><p><img alt="image of a virus" height="75" src="images/covid19.jpg" style="float:left" width="75"/><strong><font size="+1"><a href="emerg_prep.html">EMERGENCY PREPAREDNESS &amp; RESPONSE</a></font></strong></p><hr/>
<p><strong><a href="covid-19_flyer_alert.html"><font size="-1">*COVID-19 Alert*<br/> 
Fraudulent Flyers Regarding Face Masks</font></a></strong></p></div>
<div class="blue-divider"></div>
<div class="info-blk">
<div class="rightcol-informationblk2">
<h2><a href="filing_complaint.htm" title="FILE AN ADA COMPLAINT">
    FILE AN <br/>
    ADA COMPLAINT
</a></h2>
</div>
<p> </p>
<p><img alt="graphic of an arrow reading vote here with an american flag in the background" height="95" src="polling_places/images/cover-image.jpg" style="float:left" width="145"/><a href="https://civilrights.justice.gov/report/">Have you faced barriers in voting, including registering to vote, casting a ballot, or accessing a polling place, because of your disability?</a></p>
</div>
<div class="blue-divider"></div>
</div>
<div class="rightcol" id="">
<div class="rightcol-informationblk">
<h2 align="center">Featured Topic: Service Animals</h2>
<div class="align-left">
<p><a href="regs2010/service_animal_qa.html"><img align="middle" alt="image of a document with an inset image of woman shopping with a service animal" height="255" src="images/spotlight_pane9.gif" width="205"/></a></p>
</div>
</div>
<div class="rightcol-informationblk">
<h2>ADA Information Line</h2>
<div class="align-left">
<p><font size="-1">The U.S. Department of Justice provides information about the ADA through a toll-free <a href="infoline.htm">ADA Information Line</a>.<br/>
800-514-0301 (voice)<br/>
800-514-0383 (TTY)</font></p>
</div>
</div>
<div class="rightcol-informationblk">
<h2 align="center"><img align="middle" alt="Red Envelope" height="24" src="images/redletter.gif" width="40"/>ADA.gov Updates</h2>
<a href="https://public.govdelivery.com/accounts/USDOJ/subscriber/new?category_id=USDOJ_C26">Sign up for ADA.gov updates</a></div>
</div>
<div class="clearcol"></div>
<div class="footer"><div class="footercols">
<h2>Selected Topics</h2>
<p><a href="olmstead/index.html">Olmstead</a><br/>
<a href="hiv/index.html" title="HIV and AIDS">HIV and AIDS</a><br/>
<a href="employment.htm" title="Employment">Employment</a><br/>
<a href="access-technology/index.html" title="Accessible Technology">Accessible Technology</a><br/>
<a href="usao-agreements.htm" title="Barrier-Free Healthcare Initiative">Barrier-Free Healthcare Initiative</a><br/>
<a href="criminaljustice/index.html" title="Criminal Justice">Criminal Justice</a><br/>
<a href="newproposed_regs.htm" title="Regulatory Development">Regulatory Development</a><br/>
<a href="civicac.htm" title="Project Civic Access">Project Civic Access</a><br/>
<a href="business.htm" title="ADA Business Connection">ADA Business Connection</a></p>
<h2> Resources</h2>
<p> <a href="ada_fed_resources.htm" title="Federal ADA &amp; Section 504 Resources">Federal ADA &amp; Section 504 Resources</a> <br/>
<a href="statrpt.htm" title="Reports &amp; Updates">Reports &amp; Updates</a><br/>
<a href="508/index.html" title="Section 508">Section 508</a> <br/>
<a href="ada_archive.htm" title="ADA.gov Archive"> ADA.gov Archive </a></p>
</div>
<div class="footercols">
<h2>Titles of the ADA</h2>
<p><a href="ada_title_I.htm" title="Employment (title I)"> Employment (title I)</a><br/>
<a href="ada_title_II.htm" title="State &amp; Local Government (title II)">State &amp; Local Government (title II)</a><br/>
<a href="ada_title_III.htm" title="Public Accommodations and">Public Accommodations and <br/>
        Commercial Facilities (title III)</a></p>
<h2>Department of Justice ADA Responsibilities</h2>
<p> <a href="taprog.htm" title="Technical Assistance">Technical Assistance </a><br/>
<a href="enforce_footer.htm" title="Enforcement">Enforcement</a><br/>
<a href="mediate.htm" title="Mediation"> Mediation</a><br/>
<a href="regs_footer.htm" title="Regulations">Regulations </a><br/>
<a href="certcode.htm" title="Certification of State and Local Building Codes">Certification of State and Local Building Codes </a></p>
<p> </p>
</div>
<div class="footercols">
<h2><a href="site_map.htm" title="Site Map">Site Map</a></h2>
<h2> <a href="contact_drs.htm" title="Contact Us">Contact Us</a></h2>
<h2>Department of Justice Links</h2>
<p><a href="https://www.justice.gov" title="Department of Justice Home Page">Department of Justice Home Page</a> <br/>
<a href="https://www.justice.gov/crt/" title="Civil Rights Division Home Page">Civil Rights Division Home Page</a> <br/>
<a href="https://www.justice.gov/crt/about/drs/" title="Disability Rights Section Home Page">Disability Rights Section Home Page</a><br/>
<a href="https://www.justice.gov/crt/foia/" title="Freedom of Information Act">Freedom of Information Act </a><br/>
<a href="https://www.justice.gov/crt/about/drs/drshome_spanish.php" title="En Español">En Español</a><br/>
<a href="https://www.justice.gov/doj/privacy-policy" title="Privacy Policy">Privacy Policy </a><br/>
<a href="https://www.justice.gov/legalpolicies" title="Legal Policies and Disclaimers">Legal Policies and Disclaimers</a></p>
</div></div>
<div class="clearcol">
</div>
</main></div></div></body>
</html>
