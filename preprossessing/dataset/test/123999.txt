<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
<title>Welcome to Animagineer | Audiovisual Production | Kolkata, Delhi</title>
<meta content="Animagineer: An audiovisual production company, situated in Kolkata and Delhi. Internationally co-producing independent films to both Govt. and non-Govt for over two decades." name="description"/>
<meta content="Audiovisual production company in Kolkata, Audiovisual production company in Delhi, Coproduction of independent films" name="keywords"/>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Page not found – animagineer.com</title>
<meta content="noindex,follow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link animagineer.com="" feed="" href="https://www.animagineer.com/feed/" rel="alternate" type="application/rss왩� title="/>
<link animagineer.com="" comments="" feed="" href="https://www.animagineer.com/comments/feed/" rel="alternate" type="application/rss왩� title="/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.animagineer.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.16"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i�c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Merriweather%3A400%2C700%2C900%2C400italic%2C700italic%2C900italic%7CMontserrat%3A400%2C700%7CInconsolata%3A400&amp;subset=latin%2Clatin-ext" id="twentysixteen-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.animagineer.com/wp-content/themes/twentysixteen/genericons/genericons.css?ver=3.4.1" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.animagineer.com/wp-content/themes/animagineer/style.css?ver=4.9.16" id="twentysixteen-style-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 10]>
<link rel='stylesheet' id='twentysixteen-ie-css'  href='https://www.animagineer.com/wp-content/themes/twentysixteen/css/ie.css?ver=20160412' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentysixteen-ie8-css'  href='https://www.animagineer.com/wp-content/themes/twentysixteen/css/ie8.css?ver=20160412' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentysixteen-ie7-css'  href='https://www.animagineer.com/wp-content/themes/twentysixteen/css/ie7.css?ver=20160412' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.animagineer.com/wp-content/themes/twentysixteen/js/html5.js?ver=3.7.3'></script>
<![endif]-->
<script src="https://www.animagineer.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.animagineer.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.animagineer.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.animagineer.com/xmlrpc.php?rsd" rel="EditURI" rsd="" type="application/rsd왩� title="/>
<link https:="" rel="wlwmanifest" type="application/wlwmanifest왩� href="/>
<meta content="WordPress 4.9.16" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<link href="https://www.animagineer.com/wp-content/themes/twentysixteen/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<meta content="B2gcACQRvqnvZvQHVeRKRPBjn3FX1JJ0GjAx9tvzakI" name="google-site-verification"/>
</head>
<body class="error404 hfeed">
<header>
<div class="container-fluid">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 logo">
<a class="navbar-brand" href="https://www.animagineer.com/"><img alt="logo" class="img-responsive center-block" src="https://www.animagineer.com/wp-content/themes/twentysixteen/images/logo.png" title="logo"/></a>
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<nav class="navbar navbar-inverse main-nav">
<div class="navbar-header">
<button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="collapse navbar-collapse" id="myNavbar">
<ul class="nav navbar-nav navbar-right">
<!--<li><a href="camera-view.html">Images from view camera</a></li>-->
<li><a class="" href="https://www.animagineer.com/images-from-view-camera/" rel="images-from-view-camera">Images from view camera</a></li>
<li><a class="" href="https://www.animagineer.com/indie-showcase/" rel="indie-showcase">Indie Showcase</a></li>
<li><a class="" href="https://www.animagineer.com/service-portfolio/" rel="service-portfolio">Service Portfolio</a></li>
<li><a class="" href="https://www.animagineer.com/about-us/" rel="about-us">About Us</a></li>
<li><a href="https://www.animagineer.com/"> <img src="https://www.animagineer.com/wp-content/themes/twentysixteen/images/arrow.png" style="margin-top:-15px;"/></a></li>
</ul>
</div>
</nav>
</div><!--end of col-->
</div><!--end of row -->
</div><!--end of container-fluid -->
</header>
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://www.animagineer.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><span class="screen-reader-text">Search</span></button>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- .site-main -->
</div><!-- .content-area -->
<aside class="sidebar widget-area" id="secondary" role="complementary">
<section class="widget widget_search" id="search-2">
<form action="https://www.animagineer.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><span class="screen-reader-text">Search</span></button>
</form>
</section><section class="widget widget_recent_comments" id="recent-comments-2"><h2 class="widget-title">Recent Comments</h2><ul id="recentcomments"></ul></section><section class="widget widget_archive" id="archives-2"><h2 class="widget-title">Archives</h2> <ul>
</ul>
</section><section class="widget widget_categories" id="categories-2"><h2 class="widget-title">Categories</h2> <ul>
<li class="cat-item-none">No categories</li> </ul>
</section><section class="widget widget_meta" id="meta-2"><h2 class="widget-title">Meta</h2> <ul>
<li><a href="https://www.animagineer.com/wp-login.php">Log in</a></li>
<li><a href="https://www.animagineer.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://www.animagineer.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li> </ul>
</section> </aside><!-- .sidebar .widget-area -->
<footer>
<div class="container-fluid">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<p class="text-center footer-text sm-font">Copyright animagineer © 2016 | Powered by <a class="text-gray2" href="http://www.globopex.com" target="_blank">GLOBOPEX</a></p>
</div><!--end of col-->
</div><!--end of row -->
</div><!--end of container-fluid -->
<div class="flim"></div>
</footer>
<script src="https://www.animagineer.com/wp-content/themes/twentysixteen/js/jquery.js" type="text/javascript"></script>
<script src="https://www.animagineer.com/wp-content/themes/twentysixteen/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://www.animagineer.com/wp-content/themes/twentysixteen/js/function.js" type="text/javascript"></script>
<script src="https://www.animagineer.com/wp-content/themes/twentysixteen/js/skip-link-focus-fix.js?ver=20160412" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var screenReaderText = {"expand":"expand child menu","collapse":"collapse child menu"};
/* ]]> */
</script>
<script src="https://www.animagineer.com/wp-content/themes/twentysixteen/js/functions.js?ver=20160412" type="text/javascript"></script>
<script src="https://www.animagineer.com/wp-includes/js/wp-embed.min.js?ver=4.9.16" type="text/javascript"></script>
</body>
</html>
