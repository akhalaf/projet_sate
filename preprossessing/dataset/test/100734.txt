<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="index, follow" name="robots"/>
<title> It means, "Portable Document Format" </title>
<meta content="it, means, portable, document, format" name="keywords"/>
<meta content='It means, "Portable Document Format"' name="description"/>
<link href="/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/css/styles.css" rel="stylesheet"/>
<link href="/images/allinterview.ico" rel="shortcut icon"/>
<link href="https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html" rel="canonical"/>
</head>
<body>
<div class="container ">
<!--- header start -->
<header>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6567805284657549",
    enable_page_level_ads: true
  });
</script>
<div class="hidden-xs">
<a href="/"><img alt="ALLInterview.com" src="/images/allinterview_logo.gif"/></a>
<span class="pull-right"><a href="https://www.totalexams.com" target="_blank"><img alt="TotalExams.com" src="/images/totalexams_logo.jpg"/></a></span>
</div>
<div class="text-center hidden-lg hidden-md hidden-sm">
<a href="https://www.totalexams.com" target="_blank"><img alt="TotalExams.com" height="40px" src="/images/totalexams_logo.jpg" width="240px"/></a>
</div>
<div class="text-right">
<span class="spanlogin" id="spanlogin"></span>
<div id="divlogin"></div>
</div>
<nav class="navbar navbar-default navbar-static-top">
<div class="container-fluid text-center">
<div class="navbar-header">
<button class="logotxt hidden-lg hidden-md hidden-sm" type="button"><a href="/">ALLInterview.com</a></button>
<button class="navbar-toggle collapsed" data-target="#myNavbar" data-toggle="collapse" type="button">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="menu">
<div class="collapse navbar-collapse" id="myNavbar">
<div class="row menulevel1">
<a href="/interview-questions/all.html" title="Browse All Questions">Categories</a>  |  
				<a href="/company/all.html" title="Questions by Company Wise">Companies</a>  |   
				<a href="/placement-papers/all.html" title="Placement Papers">Placement Papers</a>  |  
				<a href="/interview-questions/465/code-snippets.html" title="Code Snippets">Code Snippets</a>  |  
				<a href="/interview-questions/436/certifications.html" title="Certification Questions">Certifications</a>  |   
				<a href="/interview-questions/443/visa-interview-questions.html" title="Visa Interview Questions">Visa Questions</a>
</div>
<div class="row menulevel2">
<a href="/postquestions.php" title="Post New Questions">Post Questions</a>  |   
				<a href="/unanswered-questions/all.html" title="Post Answers for Un-Answered Questions">Post Answers</a>  |  
				<a href="/mypanel.php" title="Login into your Panel, to view your performance">My Panel</a>  |   
				<a href="/search.php" title="Search">Search</a>  |   
			<!--	<a href="http://www.kalaajkal.com/articles/" title="Browse Hundreds of Articles" target="_blank">Articles</a>&nbsp; | &nbsp; -->
<a href="/tags/all.html" title="Topics -- Provides Information in Depth">Topics</a>  |  
				<a href="/interview-questions/538/errors.html" title="Errors">Errors</a>
</div>
</div>
</div>
</div>
</nav>
</header>
<br/>
<div class="row text-center">
<div class="well">
    Follow Our FB Page <a href="https://www.facebook.com/CircleMediaIndia/" target="_blank">&lt;&lt; <u><b>CircleMedia.in</b></u> &gt;&gt;</a> for Daily Laughter. We Post Funny, Viral, Comedy Videos, Memes, Vines... 
    </div><br/>
</div>
<!--- header end -->
<script language="javascript">

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function checkCookie() {
    var user = getCookie("cookie_uid");
	var cookie_login_str;
	
    if (user != "") {
        //alert("Welcome again " + user);
		cookie_login_str = "&#167; Hi..<b> "+user+" </b>&nbsp;<img src='/images/smile.gif'>&nbsp; <a href = '/scripts/logout.php'><b>Logout</b></a>&nbsp;";

    } else {
        cookie_login_str = "<a href='/mypanel.php?register'><i class='glyphicon glyphicon-user font12'></i>Sign Up</a>  <a href='/mypanel.php'><i class='glyphicon glyphicon-log-in font12'></i>Login</a>";
    }
	//alert(cookie_login_str);
	document.getElementById("spanlogin").innerHTML = cookie_login_str;
	
}

checkCookie();


</script>
<div class="row">
<div class="col-md-12">
<span id="suggnewcat"></span> <ol class="breadcrumb">
<li><a href="/interview-questions/all.html">Categories</a>  &gt;&gt;  <a href="/interview-questions/97/accounting.html">Accounting</a>  &gt;&gt;  <b>Accounting AllOther</b></li>
<li class="pull-right font12"><a class="hlink" onclick='showSuggestNewCatForm("Categories  &gt;&gt;  Accounting  &gt;&gt;  Accounting AllOther");'>Suggest New Category </a></li> </ol> </div>
<div class="col-md-12">
</div>
<div class="col-md-12">
<div class="panel panel-default col-md-8">
<div class="panel-body">
<p class="question"> expand P D F  </p>
</div>
<div class="panel-footer"><span class="name">Question Posted / srinivasa.sr </span></div>
</div>
<div class="col-md-1"> </div>
<div class="panel panel-default col-md-3">
<div class="panel-body">
<ul class="list-unstyled">
<li> <i class="glyphicon glyphicon-comment"></i> <b> 3 Answers  </b> </li>
<li> <i class="glyphicon glyphicon-sunglasses"></i> 9993 Views </li>
<!--  <li> <i class="glyphicon glyphicon-user"></i> <a onclick="refSubmit(85291)"> Refer</a></li> -->
<li> <i class="glyphicon glyphicon-star-empty"></i> <a class="hlink" onclick="showCompanyForm(85291)" title="In Which Company Interview you faced this Question!"> I also Faced</a> <div id="q85291"></div> </li>
<li> <i class="glyphicon glyphicon-envelope"></i> <a class="hlink" onclick="showReceiveEmailForm(85291)" title="E-Mail New Answers">E-Mail Answers</a></li>
</ul>
</div>
</div>
</div>
<script type="text/javascript">

    window._mNHandle = window._mNHandle || {};

    window._mNHandle.queue = window._mNHandle.queue || [];

    medianet_versionId = "3121199";

</script>
<script async="async" src="https://contextual.media.net/dmedianet.js?cid=8CUV21457"></script>
<div id="245626243">
<script type="text/javascript">

        try {

            window._mNHandle.queue.push(function (){

                window._mNDetails.loadTag("245626243", "300x250", "245626243");

            });

        }

        catch (error) {}

    </script>
</div>
<div class="col-md-12">
<div class="col-md-9">
<div class="panel panel-default">
<div class="panel-body">
<p>Answer Posted / <b>srinivasa.sr</b></p>
<p>It means, "Portable Document Format"</p>
<p></p><div align="left" id="mark163290"><table class="marking"><tbody><tr>
<td title="Please Help the Community Members, by Identifying this Answer as Correct or Incorrect">Is This Answer Correct ?   </td>
<td title="28 Users Marked this Answer as CORRECT"><font color="green"><b>28 Yes </b></font><input name="markans163290" type="radio" value="yes"/></td>
<td title="1 Users Marked this Answer as WRONG"><font color="red">1 No </font><input name="markans163290" type="radio" value="no"/></td>
<td> <input class="btn btn-primary btn-sm" name="btnMark" onclick="markAnswer(163290)" type="button" value="submit"/> </td>
</tr></tbody></table></div>
</div>
</div>
<br/>
<script type="text/javascript">

    window._mNHandle = window._mNHandle || {};

    window._mNHandle.queue = window._mNHandle.queue || [];

    medianet_versionId = "3121199";

</script>
<script async="async" src="https://contextual.media.net/dmedianet.js?cid=8CUV21457"></script>
<div id="245626243">
<script type="text/javascript">

        try {

            window._mNHandle.queue.push(function (){

                window._mNDetails.loadTag("245626243", "300x250", "245626243");

            });

        }

        catch (error) {}

    </script>
</div>
<br/>
<p class="text-center">
<a class="btn btn-info btn-lg" onclick="showAnswerForm(85291, 1);" role="button">Post New Answer</a>
<span class="space">     </span>
<a class="btn btn-info btn-lg" href="/showanswers/85291/expand-p-d-f.html" role="button">View All Answers</a>
</p>
<div class="text-center" id="pna"></div>
<br/>
<div class="panel panel-default">
<div class="panel-heading" id="bggreen">Please Help Members By Posting Answers For Below Questions</div>
<div class="panel-body">
<p class="qsn">types of inventory</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(65628, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(65628)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(65628)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1288					
					</span>
</p> <div id="q65628"></div> <hr/><p class="qsn">what is the difference between financial accouning and 
financial management?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(79752, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(79752)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(79752)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1210					
					</span>
</p> <div id="q79752"></div> <hr/><p class="qsn">i AM CURRENTLY DOING B.COM. i INTEND TO PURSUE MY MASTERS AT
USA IN ECONOMICS AND FINANCE OR ACCOUNTING...CAN U TELL ME
THE CRITERIA AND WHICH EXAM DO I NEED TO QUALIFY FOR???</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(84857, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(84857)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(84857)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 997					
					</span>
</p> <div id="q84857"></div> <hr/><p class="qsn">What will be entry pass in tally if TDS paid by Credit Card.?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(179334, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(179334)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(179334)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1085					
					</span>
</p> <div id="q179334"></div> <hr/><p class="qsn">why you would like to work for the our(AIRLINE) Group ???</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(152569, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(152569)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(152569)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1170					
					</span>
</p> <div id="q152569"></div> <hr/><br/> <br/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- responsive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6567805284657549" data-ad-format="auto" data-ad-slot="2508282194" style="display:block"></ins>
<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script> <br/> <hr/><p class="qsn">how u can control stock movments....like issuing raw 
materials receiving...in practical way..basically in ,y org 
we r using bin cards,barcodes etc but somehow need more 
concentration... help me frds..</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(94234, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(94234)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(94234)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 992					
					</span>
</p> <div id="q94234"></div> <hr/><p class="qsn">Why in partnership fundamental question will add Partnership salary for the year and per month salry add in the cr. Side of the p/l  app a/c</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(191952, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(191952)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(191952)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 912					
					</span>
</p> <div id="q191952"></div> <hr/><p class="qsn">what is waybill key number &amp; how can it be issue?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(193084, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(193084)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(193084)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1256					
					</span>
</p> <div id="q193084"></div> <hr/><p class="qsn">What is accounts payable?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(166778, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(166778)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(166778)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1141					
					</span>
</p> <div id="q166778"></div> <hr/><p class="qsn">My company's accountant use two different way to calculate 
Depreciation,one as per Companies Act and another rate for 
Income Tax calculation. Why we have to use two way to 
calculate it? Can we just use one way to calculate?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(85698, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(85698)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(85698)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1175					
					</span>
</p> <div id="q85698"></div> <hr/><p class="qsn">Explain valuation methods of Normal and Abnormal loss</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(85784, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(85784)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(85784)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1106					
					</span>
</p> <div id="q85784"></div> <hr/><p class="qsn">My name is Tasha actually I had put my file for Australia PR
on basis of Hairdresser, and I have shown my qualification
of 10th passed, but I done my B.Com with Accountancy, and
presently I am working as Accountant,Now I want to put up my
file for Australia Student is it possible, and my age is 36
yrs, I had already given my IELTS (Academic) and I have
scored 6 bands, but its period has been expired,please give
me some answer what to do next.
</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(81578, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(81578)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(81578)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1352					
					</span>
</p> <div id="q81578"></div> <hr/><p class="qsn">What is SOX</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(87527, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(87527)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(87527)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1159					
					</span>
</p> <div id="q87527"></div> <hr/><p class="qsn">in what form is crr kept in RBI?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(80279, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(80279)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(80279)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1380					
					</span>
</p> <div id="q80279"></div> <hr/><p class="qsn">how to solved the shares qusionas ?</p>
<p class="row text-left">
<span class="col-md-1"> <a class="hlink" onclick="showAnswerForm(173046, 0)" title="Post New Answer"> <i class="glyphicon glyphicon-pencil"></i></a> </span>
<span class="col-md-1">
<a class="hlink" onclick="showCompanyForm(173046)" title="In Which company interview, this question was asked ?"><i class="glyphicon glyphicon-hand-left"></i></a>
</span>
<span class="col-md-1">
<a class="hlink" onclick="showReceiveEmailForm(173046)" title="E-Mail New Answers"><i class="glyphicon glyphicon-envelope"></i></a>
</span>
<span class="col-md-2">
<i class="glyphicon glyphicon-sunglasses"></i> 1023					
					</span>
</p> <div id="q173046"></div> <hr/>
</div>
</div>
</div>
<div class="col-md-3">
<br/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Large Skyscraper (300 x 600) -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6567805284657549" data-ad-slot="8398364598" style="display:inline-block;width:300px;height:600px"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br/><br/>
</div>
</div>
<br/><br/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- responsive -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6567805284657549" data-ad-format="auto" data-ad-slot="2508282194" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br/><br/>
<br/><br/>
<br/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Responsive Links -->
<ins class="adsbygoogle" data-ad-client="ca-pub-6567805284657549" data-ad-format="link" data-ad-slot="2910914595" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br/>
<div class="share">
<ul>
<li class="visible-sm visible-xs"><a onclick='JSSocialShare("w", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "It means, %26quot;Portable Document Format%26quot;", "")' title="Whatsapp Winky"><img alt="Whatsapp" src="/images/whatsapp36.png"/></a></li>
<li><a onclick='JSSocialShare("s", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "", "")' title="SMS Simply"><img alt="SMS" src="/images/sms.png"/></a></li>
<li><a onclick='JSSocialShare("f", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "", "")' title="Facebook Funky"><img alt="Facebook" src="/images/facebook36.png"/></a></li>
<li><a onclick='JSSocialShare("t", "", "", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html  It means, %26quot;Portable Document Format%26quot;")' title="Twitter Tweety"><img alt="Twitter" src="/images/twitter36.png"/></a></li>
<li><a onclick='JSSocialShare("g", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "", "")' title="Google+ Googly"><img alt="Google" src="/images/google36.png"/></a></li>
<li><a onclick='JSSocialShare("l", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "It means, %26quot;Portable Document Format%26quot;", "It means, %26quot;Portable Document Format%26quot;")' title="Linkedin Linky"><img alt="Linkedin" src="/images/linkedin36.png"/></a></li>
<li><a onclick='JSSocialShare("m", "https://www.allinterview.com/viewpost/163290/it-means-portable-document-format.html", "It means, %26quot;Portable Document Format%26quot;", "It means, %26quot;Portable Document Format%26quot;")' title="E-Mail Early"><img alt="E-Mail" src="/images/mail.png"/></a></li>
</ul>
</div>
<footer class="footer col-md-12">
<!-- <span class="text-center"><a href="https://www.forum9.com" target="_blank"><img src="/images/forum9_banner.gif"  alt="Forum9.com" width="320px" height="50px"></a></span> -->
<div class="row">
<div class="panel panel-default">
<div class="panel-body">
<div>
<p>
<a href="/interview-questions/1/software.html">Software Interview Questions</a> :: 
				<a href="/interview-questions/406/artificial-intelligence.html">Artificial Intelligence</a>,  
				<a href="/interview-questions/625/big-data.html">Big Data</a>,  
				<a href="/interview-questions/210/python.html">Python</a>,  
				<a href="/interview-questions/15/php.html">PHP</a>,  				
				<a href="/interview-questions/392/dot-net.html">DotNet</a>,  
				<a href="/interview-questions/127/java-j2ee.html">Java</a>, 
				<a href="/interview-questions/6/databases.html">Databases</a>, 
				<a href="/interview-questions/600/mobile-operating-systems.html">Mobile Apps</a>,...
				</p>
<p>
<a href="/interview-questions/89/business-management.html">Business Management Interview Questions</a>:: Banking Finance, Business Administration, Funding, Hotel Management, Human Resources, IT Management, Industrial Management, Infrastructure Management, Marketing Sales, Operations Management, Personnel Management, Supply Chain Management,...
				</p>
<p>
<a href="/interview-questions/79/engineering.html">Engineering Interview Questions</a> :: Aeronautical, Automobile, Bio, Chemical, Civil, Electrical, Electronics Communications, Industrial, Instrumentation, Marine, Mechanical, Mechatronics, Metallurgy, Power Plant,...
				</p>
<p>
<a href="/interview-questions/443/visa-interview-questions.html">Visa Interview Questions</a> :: USA Visa, UK Visa, Australia Visa, Canada Visa, Germany Visa, New Zealand Visa,...
				</p>
<p>
<a href="/interview-questions/97/accounting.html">Accounting Interview Questions</a> | 
				<a href="/interview-questions/203/hr-questions.html">HR Interview Questions</a> | 
				<a href="/interview-questions/106/fashion-modelling.html">Fashion &amp; Modelling Interview Questions</a>....
				</p>
</div>
<hr/>
<p class="text-center">
<a href="https://www.allinterview.com/CopyrightPolicy.php">Copyright Policy</a> | 
			  <a href="https://www.allinterview.com/TermsOfService.php">Terms of Service</a> |
			  <a href="https://www.allinterview.com/sitemap.php">Site Map</a> |
		<!--	  <a href="http://www.allinterview.com/common/sitemap_rss.php"><img src="/images/rss.jpg">Site Map</a> | -->
<a href="https://www.allinterview.com/contact.php">Contact Us</a>
</p>
<p class="text-center">Copyright © 2005-2019  ALLInterview.com.  All Rights Reserved. </p>
</div>
</div>
</div>
</footer>
<!-- script references -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/js_functions.js"></script>
<script src="/js/mark.js"></script>
<!-- google analytics  -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-437921-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-437921-1');
</script>
</div>
</div> <!-- end container  -->
</body>
</html>
