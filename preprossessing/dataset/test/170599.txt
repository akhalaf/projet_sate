<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
      Auno.org  - Anarchy Online Resources
    </title>
<link href="/res/auno-v5/a.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/res/auno-v5/yui/reset.css" rel="stylesheet" type="text/css"/>
<link href="/res/auno-v5/yui/fonts.css" rel="stylesheet" type="text/css"/>
<link href="/res/auno-v5/yui/grids.css" rel="stylesheet" type="text/css"/>
<link href="/res/auno-v5/auno.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="yui-t1" id="doc">
<div id="hd"><!-- header -->
<p>
<a href="/">
<img alt="Auno.org" border="0" class="logo" height="48" id="logo-v5" src="/res/auno-v5/logo.gif" width="146"/>
<img alt="Auno.org" border="0" class="logo" id="logo-v3" src="/res/auno-v3/aunoorg-transparent.gif"/>
</a>
<span id="versions"> <a href="/ao/db.php?cmd=search&amp;patch=18085500">Auno.org: 18.8.55</a> | <a href="http://www.anarchy-online.com/wsp/anarchy/frontend.cgi?func=frontend.show&amp;table=PUBLISH&amp;template=drill&amp;func_id=1012&amp;navID=1003,1004,1012">Live servers: 18.8.55</a> </span>
</p>
<div id="hdbreak"></div>
<div id="topmenu">
<img alt="" height="34" id="menuback_left" src="/res/auno-v5/menubck_left.gif" width="3"/>
<ul>
<li><span class="menufirst"><a href="/">News</a></span></li>
<li><img alt="" class="menusep" height="34" src="/res/auno-v5/menusep.gif" width="2"/><span class="menuitems"><a href="/forum.php">Forums</a></span></li>
<li><img alt="" class="menusep" height="34" src="/res/auno-v5/menusep.gif" width="2"/><span class="menuitems"><a href="/about.html">About</a></span></li>
<li><img alt="" class="menusep" height="34" src="/res/auno-v5/menusep.gif" width="2"/><span class="menuitems"><a href="/login.php">Register / Login</a></span></li>
</ul>
<img alt="" height="34" id="menuback_right" src="/res/auno-v5/menubck_right.gif" width="3"/>
<form action="/ao/db.php" id="topmenusearch" method="get">
<input name="cmd" type="hidden" value="search"/>
<input class="searchfield" name="name" onblur="if(this.value=='')this.value='Search the AO item database';" onfocus="if(this.value=='Search the AO item database')this.value='';" type="text" value="Search the AO item database"/>
<input class="gobutton" type="submit" value=""/>
</form>
</div>
</div><!-- #hd -->
<div id="bd"><!-- body -->
<div id="yui-main">
<div class="yui-b">
<h1><span class="h1corner"></span>Auno.org news</h1>
<div class="maincontent">
<!-- begin content -->
<div style="clear: both">
<fieldset>
<legend>November  08, 2020 22:23 UTC - 18.8.55</legend>
Just updated the database to 18.8.55, thanks everyone for help!</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>August    14, 2020 07:26 UTC - New stuff</legend>
The item database was just updated to version 18.8.53, thanks Leite &amp; Nadyita!</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>April     01, 2012 10:04 UTC - Happy April Fools' Day!</legend>
The item database has been updated to 18.4.14!
<br/><br/>
/ Auno</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>October   12, 2011 08:24 UTC - 18.4.5</legend>
Half a year later, the item database has been updated to version 18.4.5.
<br/>
<br/>
/ Auno</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>May       05, 2011 09:13 UTC - 18.3.1</legend>
DB was finally updated to version 18.3.1 a few days ago, I'll update to 18.4 "soon".
<br/>
<br/>
/ Auno</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>August    18, 2009 10:21 UTC - 18.1.3</legend>
DB has been updated to 18.1.3 and all the items should be available on equipment configurator too.  Including Xan symbiants.  The new equipment configurator also allows you to specify a custom item if I don't seem to update often enough.  Xan symbiants (and spirits!) are also available in the symbiant listing tool, <a href="/ao/symbiant.php?prof=shade">click!</a>
<br/><br/>
Cheers,
Auno</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>September 01, 2004 11:19 UTC - Alien Invasion - Version 15.6.1</legend>
<a href="http://auno.org/ao/db.php?id=252556"><img align="right" alt="Alien Probe" src="/res/aoicons/159123.gif"/></a>
Hello!
<br/>
<br/>
I've just patched the website to the current Alien Invasion version, patch 15.6.1.  There's quite a few new and updated items ;)  I also updated the layout a little, it has some bugs and has only been tested with <a href="http://getfirefox.com/">Mozilla Firefox</a> so you might run into some bugs with Internet Explorer.  I'll try to resolve these issues as soon as I can.
<br/>
<br/>
<b>Update</b>: The server is peaking at 1600 concurrent connections at the moment.. I set up another database server to handle some of the traffic and the response times should be at 5 seconds or less now.  Oh, and thanks for all the donations I've received so far, maybe they'll allow me to replace some of the aging hardware on the primary auno.org box :D
<br/>
<br/>
<b>Update</b>: The XML dumps of 15.6.1 <a href="/dev/aoxml.html">are now available</a>.
<br/>
<br/>
<b>Update</b>: The servers opened some hours ago and the load on the webserver has eased a little, so everything is running on the main server again.  Also I would like to note that the version I patched was indeed the 'real' 15.6.1, not a patch from the beta servers.
<br/>
<br/>
Cheers,
<br/>
Auno</fieldset>
</div>
<div style="clear: both">
<fieldset>
<legend>June      28, 2004 16:07 UTC - Damagedumper, 15.5.4 XML</legend>
Hey all,
<br/>
<br/>
I've set up a link over here for the simple <a href="/ao/damagedumper.html">damage dumper</a> program I created for AO a month or so back.  Check the link on the menu at left for download URL and instructions on how to use the thing.
<br/>
<br/>
I have also finally updated the XML dumps of AO database to the current version, 15.5.4.  Other than that, I haven't really been playing AO much lately, as it's summer over here, and it feels that I've beat the game for now.  Maybe the Alien Invasion expansion pack this autumn will introduce some new and interesting stuff that will make the game interesting again.
<a href="/ao/damagedumper.html"><img align="right" alt="" border="0" src="/res/aunodd.gif"/></a>
<br/>
<br/>
Cheers,
<br/>
Auno</fieldset>
</div>
<!-- end content -->
<div style="clear: both"></div>
</div><!-- .maincontent -->
<div class="mainbottom"><span class="cornerbottom"></span></div>
<br/>
</div><!-- .yui-b -->
</div><!-- #yui-main -->
<div class="yui-b" id="leftsidebar">
<h1><span class="h1corner"></span>AO Tools</h1>
<ul>
<li><a href="/ao/db.php">Item database</a></li>
<li><a href="/ao/breed.html">Breed ability caps</a></li>
<li><a href="/ao/char.php">Character info</a></li>
<li><a href="/ao/stats.php">Character stats</a></li>
<li><a href="/ao/dmg.php">Damage calculator</a></li>
<li><a href="/ao/dots.php">Doc's DoT table</a></li>
<li><a href="/ao/equip.php">Equipment config</a></li>
<li><a href="/ao/guild.php">Org listing</a></li>
<li><a href="/ao/heal.php">Heals table</a></li>
<li><a href="/ao/imp.php">Implant designer</a></li>
<li><a href="/ao/nuke.php">Nukes table</a></li>
<li><a href="/ao/perk.php">Perk planner</a></li>
<li><a href="/ao/symbiant.php">Symbiant listings</a></li>
<li><a href="/ao/tokenboards.html">Token boards</a></li>
<li><a href="/ao/wep.php">Weapons evaluator</a></li>
</ul>
<ul>
<li><a href="/ao/damagedumper.html">Damage dumper</a></li>
<li><a href="/dev/ao.html">Developers</a></li>
</ul>
<div class="menubottom"><span class="cornerbottom"></span></div>
<br/>
<h1><span class="h1corner"></span>Nanoformulas</h1>
<ul>
<li><a href="/ao/nanos.php?prof=adventurer">Adventurer</a></li>
<li><a href="/ao/nanos.php?prof=agent">Agent</a></li>
<li><a href="/ao/nanos.php?prof=bureaucrat">Bureaucrat</a></li>
<li><a href="/ao/nanos.php?prof=doctor">Doctor</a></li>
<li><a href="/ao/nanos.php?prof=enforcer">Enforcer</a></li>
<li><a href="/ao/nanos.php?prof=engineer">Engineer</a></li>
<li><a href="/ao/nanos.php?prof=fixer">Fixer</a></li>
<li><a href="/ao/nanos.php?prof=keeper">Keeper</a></li>
<li><a href="/ao/nanos.php?prof=martial+artist">Martial artist</a></li>
<li><a href="/ao/nanos.php?prof=meta-physicist">Meta-physicist</a></li>
<li><a href="/ao/nanos.php?prof=nano-technician">Nano-technician</a></li>
<li><a href="/ao/nanos.php?prof=shade">Shade</a></li>
<li><a href="/ao/nanos.php?prof=soldier">Soldier</a></li>
<li><a href="/ao/nanos.php?prof=trader">Trader</a></li>
<li><a href="/ao/nanos.php">Generic</a></li>
</ul>
<div class="menubottom"><span class="cornerbottom"></span></div>
</div><!-- #leftsidebar .yui-b -->
</div><!-- #bd -->
<div id="ft"><!-- footer -->
<br/>
</div><!-- #ft -->
</div><!-- #doc -->
</body>
</html>
