<!DOCTYPE HTML>
<html data-config='{"twitter":0,"plusone":0,"facebook":0,"style":"red"}' dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.bdodarts.com/"/>
<meta content="BDO darts bicc rankings average players tournaments events diary" name="keywords"/>
<meta content="British Darts Organisation worldwide ruling body providing world rankings and running BICC inter-county championship" name="description"/>
<title>British Darts Organisation - News</title>
<link href="http://www.bdodarts.com/index.php" rel="canonical"/>
<link href="/index.php?format=feed&amp;type=rss" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/index.php?format=feed&amp;type=atom" rel="alternate" title="Atom 1.0" type="application/atom+xml"/>
<link href="/templates/sport.ak/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/media/system/css/modal.css?d24e7330fb1b92bef18d946cb41c0d5f" rel="stylesheet" type="text/css"/>
<link href="/components/com_cobalt/library/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/components/com_cobalt/views/records/tmpl/default_markup_sportak.css" rel="stylesheet" type="text/css"/>
<link href="/media/mint/js/lightbox/css/lightbox.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
</style>
<script class="joomla-script-options new" type="application/json">{"csrf.token":"fb14556584679d94c8a2c26523da6c1e","system.paths":{"root":"","base":""}}</script>
<script src="/media/jui/js/jquery.min.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/jui/js/bootstrap.min.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/system/js/mootools-core.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/system/js/core.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/system/js/mootools-more.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/media/system/js/modal.js?d24e7330fb1b92bef18d946cb41c0d5f" type="text/javascript"></script>
<script src="/index.php/component/cobalt/?task=ajax.mainJS&amp;Itemid=1" type="text/javascript"></script>
<script src="/components/com_cobalt/library/js/felixrating.js" type="text/javascript"></script>
<script src="/media/mint/js/lightbox/js/lightbox.js" type="text/javascript"></script>
<script src="/media/mint/js/moocountdown/SimpleCounter.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($) {
			 $('.hasTip').each(function() {
				var title = $(this).attr('title');
				if (title) {
					var parts = title.split('::', 2);
					var mtelement = document.id(this);
					mtelement.store('tip:title', parts[0]);
					mtelement.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($('.hasTip').get(), {"maxTitleChars": 50,"fixed": false});
		});
		jQuery(function($) {
			SqueezeBox.initialize({});
			initSqueezeBox();
			$(document).on('subform-row-add', initSqueezeBox);

			function initSqueezeBox(event, container)
			{
				SqueezeBox.assign($(container || document).find('a.modal').get(), {
					parse: 'rel'
				});
			}
		});

		window.jModalClose = function () {
			SqueezeBox.close();
		};

		// Add extra modal close functionality for tinyMCE-based editors
		document.onreadystatechange = function () {
			if (document.readyState == 'interactive' && typeof tinyMCE != 'undefined' && tinyMCE)
			{
				if (typeof window.jModalClose_no_tinyMCE === 'undefined')
				{
					window.jModalClose_no_tinyMCE = typeof(jModalClose) == 'function'  ?  jModalClose  :  false;

					jModalClose = function () {
						if (window.jModalClose_no_tinyMCE) window.jModalClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}

				if (typeof window.SqueezeBoxClose_no_tinyMCE === 'undefined')
				{
					if (typeof(SqueezeBox) == 'undefined')  SqueezeBox = {};
					window.SqueezeBoxClose_no_tinyMCE = typeof(SqueezeBox.close) == 'function'  ?  SqueezeBox.close  :  false;

					SqueezeBox.close = function () {
						if (window.SqueezeBoxClose_no_tinyMCE)  window.SqueezeBoxClose_no_tinyMCE.apply(this, arguments);
						tinyMCE.activeEditor.windowManager.close();
					};
				}
			}
		};
		jQuery(function($){ initPopovers(); $("body").on("subform-row-add", initPopovers); function initPopovers (event, container) { $(container || document).find("*[rel=\"popover\"]").popover({"html": true,"placement": "bottom","trigger": "click","container": "body"});} });jQuery(function($){ initTooltips(); $("body").on("subform-row-add", initTooltips); function initTooltips (event, container) { container = container || document;$(container).find("*[rel^=\"tooltip\"]").tooltip({"html": true,"container": "body"});} });jQuery(function($){ initTooltips(); $("body").on("subform-row-add", initTooltips); function initTooltips (event, container) { container = container || document;$(container).find("*[rel=\"tooltipright\"]").tooltip({"html": true,"placement": "right","container": "body"});} });jQuery(function($){ initTooltips(); $("body").on("subform-row-add", initTooltips); function initTooltips (event, container) { container = container || document;$(container).find("*[rel=\"tooltipbottom\"]").tooltip({"html": true,"placement": "bottom","container": "body"});} });
			(function($){
				$(document).ready(function (){
					$('.has-context')
					.mouseenter(function (){
						$('.btn-group',$(this)).show();
					})
					.mouseleave(function (){
						$('.btn-group',$(this)).hide();
						$('.btn-group',$(this)).removeClass('open');
					});

					contextAction =function (cbId, task)
					{
						$('input[name="cid[]"]').removeAttr('checked');
						$('#' + cbId).attr('checked','checked');
						Joomla.submitbutton(task);
					}
				});
			})(jQuery);
			jQuery(function($){ initTooltips(); $("body").on("subform-row-add", initTooltips); function initTooltips (event, container) { container = container || document;$(container).find(".hasTooltip").tooltip({"html": true,"container": "body"});} });</script>
<link href="/templates/sport.ak/apple_touch_icon.png" rel="apple-touch-icon-precomposed"/>
<link href="/templates/sport.ak/styles/red/css/bootstrap.css" rel="stylesheet"/>
<link href="/templates/sport.ak/styles/red/css/theme.css" rel="stylesheet"/>
<link href="/templates/sport.ak/warp/vendor/highlight/highlight.css" rel="stylesheet"/>
<link href="/templates/sport.ak/css/custom.css" rel="stylesheet"/>
<script src="/templates/sport.ak/warp/vendor/uikit/js/uikit.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/autocomplete.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/search.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/tooltip.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/sticky.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/slideshow.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/slideset.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/slider.js"></script>
<script src="/templates/sport.ak/warp/vendor/uikit/js/components/accordion.js"></script>
<script src="/templates/sport.ak/warp/js/social.js"></script>
<script src="js:smoothscroll.js"></script>
<script src="/templates/sport.ak/js/theme.js"></script>
</head>
<body class="tm-sidebar-a-left tm-sidebars-1 tm-noblog">
<div class="preloader">
<div class="loader"></div>
</div>
<div class="over-wrap">
<div class="toolbar-wrap">
<div class="uk-container uk-container-center">
<div class="tm-toolbar uk-clearfix uk-hidden-small">
<div class="uk-float-right"><div class="uk-panel">
<div class="social-top">
<a href="https://www.facebook.com/BDODarts180/" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
<a href="https://twitter.com/bdodarts" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
<a href="https://www.youtube.com/channel/UC92zCMeQJdH8ChEamXZxSeQ" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
<a href="https://www.flickr.com/photos/bdo_dgmedia/" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-flickr"></span></a>
</div></div></div>
</div>
</div>
</div>
<div class="tm-menu-box">
<nav class="tm-navbar uk-navbar" data-uk-sticky="">
<div class="uk-container uk-container-center">
<a class="tm-logo uk-float-left" href="https://www.bdodarts.com">
<div><img alt="logo" height="60" src="/images/logo-img.png" title="logo" width="60"/><img alt="logo" height="60" src="/images/darts-for-all-tran.png" title="dartsforall" width="60"/><img alt="logo" height="60" src="/images/one80L.png" title="winmau" width="210"/></div></a>
<ul class="uk-navbar-nav uk-hidden-small">
<li class="uk-active"><a href="/index.php">Home</a></li><li aria-expanded="false" aria-haspopup="true" class="uk-parent" data-uk-dropdown="{'preventflip':'y'}"><a href="/index.php/match-2">BICC</a><div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1"><div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1"><ul class="uk-nav uk-nav-navbar"><li><a href="/index.php/match-2/fixtures-2019-2020">Fixtures 2019-2020</a></li><li><a href="/index.php/match-2/fixtures">Fixtures 2018-2019</a></li><li><a href="/index.php/match-2/bicctables">League Tables</a></li><li><a href="/index.php/match-2/averages">Averages</a></li><li><a href="http://www.bdodarts.com/images/bdo-content/bicc/postponed/2019-2020.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Postponed Fixtures</a></li><li><a href="http://bdodarts.com/images/bdo-content/doc-lib/C/bicc-playing-rules.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Playing Rules</a></li><li><a href="http://bdodarts.com/images/bdo-content/bicc/venues.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Playing Venues</a></li><li><a href="/index.php/match-2/downloads">Downloads</a></li></ul></div></div></div></li><li><a href="/index.php/bdo-diary">Diary</a></li><li aria-expanded="false" aria-haspopup="true" class="uk-parent" data-uk-dropdown="{'preventflip':'y'}"><a href="/index.php/rankingsnew">Rankings</a><div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1"><div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1"><ul class="uk-nav uk-nav-navbar"><li><a href="/index.php/rankingsnew/mens-rankings">Mens Invitation Table Rolling</a></li><li><a href="/index.php/rankingsnew/mens-invitation-table-seasonal">Mens Invitation Table Seasonal</a></li><li><a href="/index.php/rankingsnew/ladies-rankings">Ladies Invitation Table Rolling</a></li><li><a href="/index.php/rankingsnew/ladies-invitation-table-seasonal">Ladies Invitation Table Seasonal</a></li><li><a href="/index.php/rankingsnew/bdo-rankings-pdf">BDO Rankings PDF</a></li></ul></div></div></div></li><li><a href="https://www.youtube.com/channel/UC92zCMeQJdH8ChEamXZxSeQ" rel="noopener noreferrer" target="_blank">BDO TV</a></li><li><a href="/index.php/match">Rules</a></li><li><a href="/index.php/contact">Contact</a></li></ul>
<a class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas="" href="#offcanvas"></a>
</div>
</nav>
</div>
<div class="tm-top-a-box tm-full-width tm-box-bg-1 ">
<div class="uk-container uk-container-center">
<section class="tm-top-a uk-grid uk-grid-collapse" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}" id="tm-top-a">
<div class="uk-hidden-small uk-hidden-medium uk-width-large-1-1"><div class="uk-panel uk-hidden-medium uk-hidden-small">
<div class="tm-top-a-box tm-full-width ">
<div class="uk-container uk-container-center">
<section class="tm-top-a uk-grid uk-grid-collapse uk-hidden-small uk-hidden-medium" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}" id="tm-top-a">
<div class="uk-hidden-small uk-hidden-medium uk-width-large-1-1 uk-row-first">
<div class="uk-panel uk-hidden-medium uk-hidden-small">
<div class="akslider-module ">
<div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'swipe', duration: '500', autoplay: false, autoplayInterval: '7000', videoautoplay: true, videomute: true, kenburns: false}">
<ul class="uk-slideshow uk-overlay-active" style="height: 974px;">
<li class="uk-height-viewport uk-active" style="height: 974px;">
<div class="uk-cover-background uk-position-cover" style="background-image: url('/images/003.jpg');"> </div>
<a href="http://bdodarts.com/index.php/component/content/article/32-world-pro-2020/145-world-pro-2020?Itemid=121" target="_blank"><img alt="" class="uk-invisible" src="/images/003.jpg" style="width: 100%; height: auto; opacity: 0;"/></a>
<div class="uk-position-cover uk-flex-middle"> </div>
</li>
<li class="uk-height-viewport" style="height: 974px;">
<div class="uk-cover-background uk-position-cover" style="background-image: url('/images/main-slider-img2020b.jpg');"> </div>
<img alt="" class="uk-invisible" src="/images/main-slider-imgb.jpg" style="width: 100%; height: auto; opacity: 0;"/>
<div class="uk-position-cover uk-flex-middle"> </div>
</li>
<li class="uk-height-viewport" style="height: 974px;">
<div class="uk-cover-background uk-position-cover" style="background-image: url('/images/main-slider-img-3.jpg');"> </div>
<img alt="" class="uk-invisible" src="/images/main-slider-imgc.jpg" style="width: 100%; height: auto; opacity: 0;"/>
<div class="uk-position-cover uk-flex-middle"> </div>
</li>
</ul>
<ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-text-center">
<li class="uk-active" data-uk-slideshow-item="0"><a href="http://www.darts.pink/bdo/">0</a></li>
<li data-uk-slideshow-item="1"><a href="http://www.darts.pink/bdo/">1</a></li>
<li data-uk-slideshow-item="2"><a href="http://www.darts.pink/bdo/">2</a></li>
</ul>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
<p> </p></div></div>
<div class="uk-width-1-1 uk-hidden-large"><div class="uk-panel uk-hidden-large">
<a href="http://bdodarts.com/index.php/component/content/article/32-world-pro-2020/145-world-pro-2020?Itemid=121" target="_blank"><div class="uk-cover-background uk-position-relative head-match-wrap" style="height: 290px; background-image: url('/images/mobile004.jpg');"><br/>
<div class="uk-position-cover uk-flex-center head-news-title">
<h1></h1>
<div class="clear"> </div>
</div>
</div></a></div></div>
</section>
</div>
</div>
<div class="uk-container uk-container-center alt">
<ul class="uk-breadcrumb"><li class="uk-active"><span>Home</span></li></ul> </div>
<div class="uk-container uk-container-center">
<div class="tm-middle uk-grid" data-uk-grid-margin="" data-uk-grid-match="" id="tm-middle">
<div class="tm-main uk-width-medium-3-4 uk-push-1-4">
<main class="tm-content" id="tm-content">
<div id="system-message-container">
</div>
<section id="cobalt-section-4">
<!--   Show page header  -->
<!--  If section is personalized load user block -->
<div class="hide" id="compare">
<div class="alert alert-info alert-block">
<h4>You have 0 articles to compare. Do you want to see compare view now?</h4>
<br/><a class="btn btn-primary" href="/index.php?view=compare&amp;section_id=4&amp;return=aHR0cHM6Ly93d3cuYmRvZGFydHMuY29tLw==" rel="nofollow">Compare</a>
<button class="btn" onclick="Cobalt.CleanCompare(null, '4')">Clean compare cart</button>
</div>
</div>
<!--  Show description of the current category or section -->
<form action="https://www.bdodarts.com/" enctype="multipart/form-data" id="adminForm" method="post" name="adminForm">
<!--   Show menu and filters  -->
<div class="clearfix"></div>
<div class="navbar" id="cnav">
<div class="navbar-inner">
<ul class="nav">
</ul>
<div class="clearfix"></div>
</div>
</div>
<script>
			(function($){
				if(!$('#cnav .navbar-inner').text().trim()) {
					$('#adminForm').hide();
				}

				var el = $('#cobalt-user-menu');
				var list = $('ul.dropdown-menu li', el);
				if(!list || list.length == 0) {
				   el.hide();
				}
			}(jQuery))
		</script>
<input name="section_id" type="hidden" value="4"/>
<input name="cat_id" type="hidden" value=""/>
<input name="option" type="hidden" value="com_cobalt"/>
<input name="task" type="hidden" value=""/>
<input name="limitstart" type="hidden" value="0"/>
<input name="filter_order" type="hidden" value=""/>
<input name="filter_order_Dir" type="hidden" value=""/>
<input name="fb14556584679d94c8a2c26523da6c1e" type="hidden" value="1"/> </form>
<!--   Show category index  -->
<div class="uk-grid">
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/432-bdo-founding-member-sam-hawkins-passes-away">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-432/731/895e73ceaf14cd1cd86570138adcfa3a.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                January 07, 2021            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/432-bdo-founding-member-sam-hawkins-passes-away">
                        BDO founding member Sam Hawkins Passes Away                    </a>
</h4>
</div>
<div class="text">
<p>The BDO are saddened to announce the passing of their Honorary President Sam Hawkins. S...
                <a href="/index.php/item/3-main-news/432-bdo-founding-member-sam-hawkins-passes-away">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/432-bdo-founding-member-sam-hawkins-passes-away">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/431-malcolm-poole-rip">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-431/731/5f38e22684cd3e33ffca4ed9ba630277.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                December 28, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/431-malcolm-poole-rip">
                        Malcolm Poole  RIP                    </a>
</h4>
</div>
<div class="text">
<p>The BDO are sad to hear of the passing of BDO stalwart Malcolm Poole (Lancashire) yeste...
                <a href="/index.php/item/3-main-news/431-malcolm-poole-rip">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/431-malcolm-poole-rip">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/430-rip-dot-harvey">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-430/731/89648d5f1bf67652f5c762878d31de0c.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                September 12, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/430-rip-dot-harvey">
                        RIP Dot Harvey                    </a>
</h4>
</div>
<div class="text">
<p>The BDO announces the passing of Dot Harvey who died on 8th September 2020. Dot worked ...
                <a href="/index.php/item/3-main-news/430-rip-dot-harvey">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/430-rip-dot-harvey">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/429-bdo-bicc-announcement">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-429/731/928148858fd5c7e008987aaaae27c95d.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                September 01, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/429-bdo-bicc-announcement">
                        BDO BICC Announcement                    </a>
</h4>
</div>
<div class="text">
<p>There has been much speculation of late regarding the future of the BDO BICC, this has ...
                <a href="/index.php/item/3-main-news/429-bdo-bicc-announcement">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/429-bdo-bicc-announcement">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/428-bdo-revised-proposal">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-428/731/5857eaabeeba852904e911c9adc76c5f.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                July 24, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/428-bdo-revised-proposal">
                        BDO REVISED PROPOSAL                    </a>
</h4>
</div>
<div class="text">
<p>Today the British Darts Organisation has sent out a revised plan and budget to all coun...
                <a href="/index.php/item/3-main-news/428-bdo-revised-proposal">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/428-bdo-revised-proposal">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/427-to-all-bdo-bicc-players">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-427/731/6c33506d0ed4a8f48dbfedcabdadbb21.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                July 06, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/427-to-all-bdo-bicc-players">
                        TO ALL BDO BICC PLAYERS                    </a>
</h4>
</div>
<div class="text">
<p>It’s become a concern to the BDO Board of Director’s that the County letters we send ou...
                <a href="/index.php/item/3-main-news/427-to-all-bdo-bicc-players">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/427-to-all-bdo-bicc-players">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/426-bdo-super-league-information">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-426/731/fc23855628b7822a783633425a8f46b9.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                June 23, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/426-bdo-super-league-information">
                        BDO Super League Information                    </a>
</h4>
</div>
<div class="text">
<p>Please see the BDO Super League player information documents provided in the two links ...
                <a href="/index.php/item/3-main-news/426-bdo-super-league-information">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/426-bdo-super-league-information">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/425-northern-cyprus-open">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-425/731/07d19917d53aff39598d2ef54ec89c33.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                June 14, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/425-northern-cyprus-open">
                        NORTHERN CYPRUS OPEN 2020                    </a>
</h4>
</div>
<div class="text">
<p>The eagerly awaited news from the organisers  regarding the NORTHERN CYPRUS OPEN 2020 i...
                <a href="/index.php/item/3-main-news/425-northern-cyprus-open">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/425-northern-cyprus-open">Read More</a></div>
</div>
</div>
</div>
<div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
<div class="wrapper">
<div class="img-wrap uk-flex-wrap-top">
<a href="/index.php/item/3-main-news/424-r-i-p-dennis-ratcliffe">
<img alt="" class="img-polaroid" src="/images/cobalt_thumbs/gallery11-424/731/47f804180742cc00a2e9338b8ecf26e4.jpg"/>
</a> </div>
<div class="info uk-flex-wrap-middle">
<div class="date">
                June 11, 2020            </div>
<div class="name">
<h4>
<a href="/index.php/item/3-main-news/424-r-i-p-dennis-ratcliffe">
                        R.I.P Dennis Ratcliffe                    </a>
</h4>
</div>
<div class="text">
<p>The BDO is saddened this morning at the passing of former General Secretary of Derbyshi...
                <a href="/index.php/item/3-main-news/424-r-i-p-dennis-ratcliffe">Read More</a>
</p></div>
</div>
<div class="article-actions uk-flex-wrap-bottom">
<div class="count"><i class="uk-icon-comments"></i><span>0</span></div>
<div class="read-more"><a href="/index.php/item/3-main-news/424-r-i-p-dennis-ratcliffe">Read More</a></div>
</div>
</div>
</div>
</div>
<form method="post">
<div class="pagination" style="text-align: center;">
<ul class="pagination-list">
<li class="pagination-start"><span class="pagenav">Start</span></li>
<li class="pagination-prev"><span class="pagenav">Prev</span></li>
<li><span class="pagenav">1</span></li> <li><a class="pagenav" href="/index.php/items/4-news?start=9">2</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=18">3</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=27">4</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=36">5</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=45">6</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=54">7</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=63">8</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=72">9</a></li> <li><a class="pagenav" href="/index.php/items/4-news?start=81">10</a></li> <li class="pagination-next"><a class="hasTooltip pagenav" href="/index.php/items/4-news?start=9" title="Next">Next</a></li>
<li class="pagination-end"><a class="hasTooltip pagenav" href="/index.php/items/4-news?start=360" title="End">End</a></li>
</ul>
</div>
<div class="clearfix"></div>
</form>
</section>
</main>
</div>
<aside class="tm-sidebar-a uk-width-medium-1-4 uk-pull-3-4"><div class="uk-panel categories-sidebar"><h3 class="uk-panel-title">Categories</h3><div>
<ul class="nav menu">
<li class="item-3">
<a href="/index.php/category-items/4-news/3-main-news">
				News									<span class="label">(332)</span>
</a>
</li>
<li class="item-16">
<a href="/index.php/category-items/4-news/16-world-pro">
				World Pro 2020									<span class="label">(114)</span>
</a>
</li>
<li class="item-15">
<a href="/index.php/category-items/4-news/15-world-trophy">
				World Trophy									<span class="label">(14)</span>
</a>
</li>
<li class="item-18">
<a href="/index.php/category-items/4-news/18-finder-master">
				Finder Masters									<span class="label">(13)</span>
</a>
</li>
<li class="item-17">
<a href="/index.php/category-items/4-news/17-the-world-masters">
				World Masters									<span class="label">(35)</span>
</a>
</li>
<li class="item-19">
<a href="/index.php/category-items/4-news/19-inter-county">
				Inter County									<span class="label">(16)</span>
</a>
</li>
<li class="item-20">
<a href="/index.php/category-items/4-news/20-super-league">
				Super League									<span class="label">(1)</span>
</a>
</li>
<li class="item-21">
<a href="/index.php/category-items/4-news/21-youth-darts">
				Youth Darts									<span class="label">(8)</span>
</a>
</li>
</ul>
</div>
<div class="clearfix"> </div></div>
<div class="uk-panel" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true, delay: 400 }"><h3 class="uk-panel-title">Latest Tweets</h3><div class="" id="twitterFeeddisplay">
<a class="twitter-timeline" data-chrome="nofooter noscrollbar " data-theme="light" data-widget-id="666643840183455744" height="1000" href="https://twitter.com/bdodarts" width="100%">Tweets by @bdodarts</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
</div></aside>
</div>
</div>
<div class="bottom-wrapper">
<div class="tm-bottom-f-box ">
<div class="uk-container uk-container-center">
<section class="tm-bottom-f uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'&gt; div &gt; .uk-panel'}" id="tm-bottom-f">
<div class="uk-width-1-1"><div class="uk-panel">
<div class="footer-logo"><a href="/"><img alt="" height="40" src="/images/footer-logo-img.png" width="40"/><span>BDODARTS</span>.COM</a></div>
<div class="footer-socials"><!-- START: Modules Anywhere -->
<div class="social-top">
<a href="https://www.facebook.com/BDODarts180/" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
<a href="https://twitter.com/bdodarts" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
<a href="https://www.youtube.com/channel/UC92zCMeQJdH8ChEamXZxSeQ" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
<a href="https://www.flickr.com/photos/bdo_dgmedia/" target="_blank"><span class="uk-icon-small uk-icon-hover uk-icon-flickr"></span></a>
</div><!-- END: Modules Anywhere --></div>
<div class="clear"> </div>
<p class="footer-about-text">The British Darts Organisation organise the following tournaments throughout the year. The BDO International Open, The BDO Wolverhampton Open and Classic, The BDO Youth Festival of Darts, The BDO Gold Cup, The BDO Champions Cup, The British Open, The British Classic, The One80 L-style World Masters, The BDO World Youth Championship and The Lakeside World Professional Darts Championship.</p></div></div>
</section>
</div>
</div>
<footer class="tm-footer" id="tm-footer">
<a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#"></a>
<div class="uk-panel">
<div class="uk-container uk-container-center">
<div class="uk-grid">
<div class="uk-width-1-1">
<div class="footer-wrap">
<div class="foot-menu-wrap"><!-- START: Modules Anywhere --><ul class="nav menu mod-list">
<li class="item-323"><a href="/index.php/terms-of-use">Terms of Use</a></li><li class="item-324"><a href="/index.php/privacy-policy">Privacy Policy</a></li><li class="item-466"><a href="/index.php/2019-03-05-09-23-26">.</a></li><li class="item-487"><a href="/index.php/thank-you">`</a></li></ul>
<!-- END: Modules Anywhere --></div>
<div class="copyrights">Copyright © 2020 <a href="/">British Darts Organisation</a>. All Rights Reserved.</div>
<div class="clear"> </div>
</div>
</div>
</div>
</div></div>
</footer>
</div>
<div class="uk-offcanvas" id="offcanvas">
<div class="uk-offcanvas-bar"><ul class="uk-nav uk-nav-offcanvas">
<li class="uk-active"><a href="/index.php">Home</a></li><li class="uk-parent"><a href="/index.php/match-2">BICC</a><ul class="uk-nav-sub"><li><a href="/index.php/match-2/fixtures-2019-2020">Fixtures 2019-2020</a></li><li><a href="/index.php/match-2/fixtures">Fixtures 2018-2019</a></li><li><a href="/index.php/match-2/bicctables">League Tables</a></li><li><a href="/index.php/match-2/averages">Averages</a></li><li><a href="http://www.bdodarts.com/images/bdo-content/bicc/postponed/2019-2020.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Postponed Fixtures</a></li><li><a href="http://bdodarts.com/images/bdo-content/doc-lib/C/bicc-playing-rules.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Playing Rules</a></li><li><a href="http://bdodarts.com/images/bdo-content/bicc/venues.pdf" onclick="window.open(this.href, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'); return false;">Playing Venues</a></li><li><a href="/index.php/match-2/downloads">Downloads</a></li></ul></li><li><a href="/index.php/bdo-diary">Diary</a></li><li class="uk-parent"><a href="/index.php/rankingsnew">Rankings</a><ul class="uk-nav-sub"><li><a href="/index.php/rankingsnew/mens-rankings">Mens Invitation Table Rolling</a></li><li><a href="/index.php/rankingsnew/mens-invitation-table-seasonal">Mens Invitation Table Seasonal</a></li><li><a href="/index.php/rankingsnew/ladies-rankings">Ladies Invitation Table Rolling</a></li><li><a href="/index.php/rankingsnew/ladies-invitation-table-seasonal">Ladies Invitation Table Seasonal</a></li><li><a href="/index.php/rankingsnew/bdo-rankings-pdf">BDO Rankings PDF</a></li></ul></li><li><a href="https://www.youtube.com/channel/UC92zCMeQJdH8ChEamXZxSeQ" rel="noopener noreferrer" target="_blank">BDO TV</a></li><li><a href="/index.php/match">Rules</a></li><li><a href="/index.php/contact">Contact</a></li></ul></div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-126055420-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126055420-1');
</script>
<script async="" data-ad-client="ca-pub-7772007455750870" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script async="" data-ad-client="ca-pub-7772007455750870" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> </div>
</body>
</html>