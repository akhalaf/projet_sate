<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "66430",
      cRay: "610e39acf92124b2",
      cHash: "3b713a2ec26f70b",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYW50YXJ2YXNuYXguY29tLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "TeoeXPRwQbw6tEnZOl4oMFrRSZ3gGpOMvTHBeIncEv/8Z228xf7zcZx3N+QI8bNOCtwnPUZEFt2DhXqUXKTGSBHdeDDrf6Ri3YIjdODdsIUS88jYQtBXxXMO1+SXiSqlR/1he099OnJTLWWYW56Y5tgxN2oXWlCLzAWeSSfLBDYv+ZLPt/tWY7Log49B7zFrmnvlP/zvJRRa9aBNpjzBQqufAhAHGZyMSb3S/R2EPEB4wbVRlLel3CB8GG9ayQi7r4OMsOwh/gLB0DaDNzPltf3iRe3GphG1MkBAXZXbk4AUFEM9TfguvAqM8TeNddsl6JIjmGYvJeyjIsXaLALPpAm9EUQFW1h02dlEaZECyRZ1YEcUTGmZHKGK39WuN1B1KlQbhvBdLfdBUVhqdLOCsw9si0nIg6RkTSfZe1mPmhOvvkDd1KPZi526Fk/VFKqiM3IO8tHAB8GEOK68ThOxJPTM3x7TdWrE/LmvoagVIJvq7cTrhAidOthZn1O65Yhl00O/C28s52Ect6H0K+pLa1fFFpTQODAgMZIG6o+ixCcNs3YFtZIpDcR5iBATUCehoDWX4lpEEEbu5Q2SmNZAN3jaJhHLO2lghR+KeGMcHgG+R65W+zA5xyBNWL9f9R++yLvQCVGdayjWEHGBBE0+HOa7u8Z6LN50bolGzIYDBUFt5ZUqQBRhQOw7J1w5EWtpWkycc79U6KIAEj3e7e/9xuYuYSuMBynxbNpQmkBqqGSmjriW/DTNLL7Q5oSVLPvG",
        t: "MTYxMDUzMTU1NS4zNTgwMDA=",
        m: "BnHxJn8nVTgjMlKYzIeXNDcy0QJOLErLReo69Tbk8pQ=",
        i1: "p4ukPOKaoRF3AOutOHv5Eg==",
        i2: "DIMdyGWdCtYE/JRuA5Oj3w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "7mMQVy/nvsJbvypAY8Drw+qZUJ9psdku4Ble8ZYz6Bk=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.antarvasnax.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=094476171eb1a0646f4128a4fdbb09c754215689-1610531555-0-ASCH6pleQEx_xKz_5jNq-i7no-_j0LPD0DU8rAJIu5ihp599oNw4FpL-grez_ZIIt9vIceZfVdLTm8DgZ5JJ5R4CMOF1EFyuPwVucbuo5L1sXgclNx2ZM2_33FN98wD-GkMP7gDBkcQD_O1snDCCdRGrxNn8_AHnc3tc-ZkbdkYXlh6f3bl6a7BGy7iWS1K2kkXnHpSpKS6GDHjf9G0S9SJYDbAUYNa2sdqj9pAbXuplEfEyzzzt-zmnFxr-8KRwcvpw07kHyC9idghWnzx5GIuWeGeMa0eZe1KQOVQYWfX5bj_z4nZGjoSMzrBpOlj_89_rVLiEVCrRZ7hk-IymzF-ecFeyVDPm70-LmhFMLjhinCZWLdtH3LOKsISmsR18pTYfz9gtWbw6lb9Z8WtO_t3jBCyQSvb5EY_W0mr-rMHwS2QnhCZw_uhCIVTT8-azcq9ahbPIp5ta8i09MQn2wrRVSv2f3RHWkD39xtdrEuN402N_eA43QhWGNOpAdtNUBj-y3pg6VStWIJfcKFG-Ev8" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="979e189d6ed7761980d46007ef4c1a42b91b0cf6-1610531555-0-ARtF0QFcrv2hMKTgq8yyJ1AxVpcOppvT/So4tbUohM1xgKIWKGGDKPzAhQ7XQmYKZsd/pYT8FFTwmKpvwJcWiu7knsEYnr350DWFsKrHEsrfjBfr/Be9EqWOoh6zyhPwujc4RXYq0FBQ5TkYmY0ofsXFid2crZg24Bc6p2kZnWX8YagPHCNRDO73HwfzKRg+CUQLiJMrdncGIGrPmXGrBrxsro3w+dUnqlbq4I46RuoILTL5whcDH7amsrGCM0KTtiRH1/jWQ1Rcg8PPodA3JDq/qS/9qvjbucNfdPySsxchITGGCny/a3+Sk2/ElPtZ3qPe57C7Qqr306VxdJU0s104nTveqTy75IMg+dg1Eg5zSrkS4Jikl+nLGiI6SAH1f6L4otT3kYmm8mxR0h5Fk7TJBiaUgsDqMpeGW9MpX+eDUnIT18LplVfNiFoJeOVjJc+D2aCKbuYsgUwvJiHbt3HIcrX8QMz3DkVenhATlFN6wG0X5JqgfDSUwd+4dcKirf0bTzCdZLNKPQGz+PcJFEQbdFTOYvKTx9fPZWBpjuR66W1hoiJNB9K1WtDnQZp8iBHKlF3WlSzZRW4UlK+ZbKTjYcO/x9UpMt47lorVTbYeAyCvf9Dny0y4VROyNMjy+J7D9+iL64YJWcTGxmBt/ZnXnbs7lDT4yIFrziYpTDy2DY/ksPXfs+RK+VfdI6kJwKe4DE4ZXyNi53wFKcFad+w1G0A75z8z10MH4lOtXFVRm+E8ugbLfgJQhAFePOhAd/s/Bt3aIc+1xz/y4/NvFupK98UoQ+AgcKhyRac5IWnYNNiVd4T+4oKDW4hyunhSqNv5C1HehZkz3yE9dh4j4cvl7k8TU4jU4A2BYyTti3drXXVLfH15BOdrqFhUaxPWrE2Ndk22moGlOxxkt8qjHdLOZmb2qTD3lZuv3l7lV0IpARuoe5DLpXHn9LN6zJqnDhhllN0T93Azs1rG14HNMSb3zNwo9rZByusQPCIgMnB20jgZ/gkso1lvPCmGilLx37yLWfoPNJKV5etBpJKOVhPm7Pe7E22TxanspiKDTLcetdyRF1LOZTAH8OerbhsAVdNIgFgtLvFNbLJoIg2Ip4pDekKexbsZDGOOqoK8N/3Ze33BmM2jzNKO7gA0HUEY2UbisVZafmmNxTOgTU+7fd3L/GiG0Y4Lbl1k0bnBNcyFgsIh8zN+Cb0x7E3eXGeT1gB/IeRB5R6f11CXbXlwUKK+rgO4huvCzxbBDr5LZrmlkrJ7UlLGlCMJr+j+fft2Vu4wbQv+oyAoGGxyqKVOmPD4zR1NWjjybumaUBhBqadpqbz+T/Qls7rZave9W/M4og=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="3d73efd1a7fa3db924518d42e346e9e6"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610e39acf92124b2')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610e39acf92124b2</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
