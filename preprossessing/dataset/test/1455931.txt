<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="https://tkphotocenter.com/data/uploads/icon.jpg" property="og:image"/>
<meta content="TK Photocenter" property="og:site_name"/>
<meta content="Welcome to TK Photocenter" property="og:title"/>
<meta content="We are specializes in luxurious wedding photography, destination weddings, Portraits, Industrial, Advertising, Portfolios, Modeling as well as cinematography." property="og:description"/>
<meta content="https://tkphotocenter.com/" property="og:url"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/ico/favicon.png" rel="shortcut icon"/>
<title> 404 Page - TK Photo Center  - TK Photo Center</title>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<!-- Custom styles for this template -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/demo.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/normalize.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/set1.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/set2.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/bootstrap_Default.min.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/jquery.colorpanel.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/animate.min.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/color/blue.css" id="cpswitch" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/lightbox.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/main.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/presets/preset1.css" id="css-preset" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/responsive.css" rel="stylesheet"/>
<link href="https://tkphotocenter.com/theme/Bootstrap3/css/font-awesome.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css"/>
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
</head>
<!--<body id="404-page" class="">-->
<!--.preloader-->
<!--<div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>-->
<!--/.preloader-->
<body><div id="wrap">
<div class="main-nav" id="dummy">
<div class="container">
<div class="navbar-header">
<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.html">
<h1><img alt="TK Photocenter" class="img-responsive" src="data/uploads/logo.png" style="padding-top:8px" title="TK Photocenter"/></h1>
</a>
</div>
<div class="collapse navbar-collapse">
<ul class="nav navbar-nav navbar-right">
<li class="index"><a href="https://tkphotocenter.com/" target="_self" title="Home">Home</a></li>
<li class="about"><a href="https://tkphotocenter.com/about.html" target="_self" title="About">About</a></li>
<li class="services dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Services">Services<b class="caret"></b></a><ul class="dropdown-menu sub-menu dropdown-menu-right"><li class="services baby-portraits"><a href="https://tkphotocenter.com/baby-portraits.html" target="_self" title="Baby Portraits">Baby Portraits</a></li>
<li class="services portraits"><a href="https://tkphotocenter.com/portraits.html" target="_self" title="Portraits">Portraits</a></li>
<li class="services outdoor-portraits"><a href="https://tkphotocenter.com/outdoor-portraits.html" target="_self" title="Outdoor Portraits">Outdoor Portraits</a></li>
<li class="services wedding-photography"><a href="https://tkphotocenter.com/wedding-photography.html" target="_self" title="Wedding Photography">Wedding Photography</a></li>
<li class="services industrial-product-photography"><a href="https://tkphotocenter.com/industrial-product-photography.html" target="_self" title="Industrial &amp; Product Photography">Industrial &amp; Product Photography</a></li>
<li class="services modeling-portfolio"><a href="https://tkphotocenter.com/modeling-portfolio.html" target="_self" title="Modeling Portfolio">Modeling Portfolio</a></li>
<li class="services nature"><a href="https://tkphotocenter.com/nature.html" target="_self" title="Nature">Nature</a></li>
</ul></li>
<li class="portfolio"><a href="https://tkphotocenter.com/portfolio.html" target="_self" title="Portfolio">Portfolio</a></li>
<li class="contact-us"><a href="https://tkphotocenter.com/contact-us.html" target="_self" title="Reach Us">Reach Us</a></li>
</ul>
</div>
</div>
</div><!--/#main-nav-->
<!--/#home-->
<div class="page-header">
<div class="container">
<div class="col-md-12"><h1 style="color:#fff !important;text-align:center">404 Page</h1> </div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<h1 style="text-align: center;">404: Page not found.</h1>
<p style="text-align: center;">Please try the following:</p>
<p style="text-align: center;"><strong><a href="index.html">Back to Home Page</a></strong></p>
<p style="text-align: center;"><strong><a href="contact-us.html">Contact Us</a></strong></p>
<p style="text-align: center;">Thanks for your Interest on helping us!</p>
</div>
</div>
<!--<section id="twitter" class="parallax">
    <div>
      <a class="twitter-left-control" href="#twitter-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
      <a class="twitter-right-control" href="#twitter-carousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="twitter-icon text-center">
              <h3>Testimonial</h3>
            </div>
            <div id="twitter-carousel" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, <br><a href="testimonials.html"><b>-Ramer-</b></a></p>
                </div>
                <div class="item">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><a href="testimonials.html"><b>-Rajaram Ram-</b></a></p>
                </div>
                <div class="item">                                
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><a href="testimonials.html"><b>-Nandakumar-</b></a></p>
                </div>
              </div> 
			</div>                    
          </div>
        </div>
      </div>
    </div>
  </section><!--/#twitter-->
<footer id="footer">
<div class="footer-top wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
<div class="container text-center">
<div class="col-sm-12">
<h3 style="color:#fff">Come, Let’s Capture The Memories Together...  <a href="reach-us.html" style="color:#fff; font-weight:bold">Contact Us</a></h3>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="row">
<div class="col-sm-8">
<p style="margin-top: 8px; font-size:13px;">© 2017 <a href="#"><b>TK Photocenter</b></a>. Geared by <a href="http://www.inway.in/" target="_blank"><b>Inway</b></a></p>
</div>
<div align="right" class="col-sm-4">
<div class="social-icons">
<a href="https://www.facebook.com/tkphotocenter/" target="_blank"><img class="wid" src="data/uploads/fb.jpg"/></a>
<a href="#" target="_blank"><img class="wid" src="data/uploads/g-plus.jpg"/></a>
<a href="#" target="_blank"><img class="wid" src="data/uploads/youtube.jpg"/></a>
</div>
</div>
</div>
</div>
</div>
</footer>
</div>
<!-- Bootstrap core JavaScript-->
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.nanogallery.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.cookie.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.inview.min.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/wow.min.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/mousescroll.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/smoothscroll.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.countTo.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/lightbox.min.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/main.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/jquery.colorpanel.js" type="text/javascript"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/additional-methods.min.js" type="text/javascript"></script>
<script src="https://tkphotocenter.com/theme/Bootstrap3/js/contactform.js" type="text/javascript"></script>
<!-- Slider-->
<script>
$('.carousel').carousel({
    pause: "false"

});
	</script>
<!-- Slider E -->
<script type="text/javascript">
	$(document).ready(function() {
	
	

	$('.scroll-top').on('click', function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop:0}, 1000);       
	});
	});
	
	
	
</script>
<script src="https://tkphotocenter.com/widget.js?v=1.0"></script>
<!-- Responsive FileManager Javascripts loading ver. 2.5 -->
<script>
function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);
		fileref.async = false;
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
    }
    if (typeof fileref!="undefined") {
		document.body.appendChild(fileref);
	}
}
if (window.addEventListener)
	window.addEventListener("load", loadjscssfile, false);
else if (window.attachEvent)
	window.attachEvent("onload", loadjscssfile);
else window.onload = loadjscssfile;

if (document.querySelector('.prettyPhoto') !== null) {
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/css/prettyPhoto.css", "css");
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/prettyPhoto/jquery.prettyPhoto.js", "js");
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/prettyPhoto/rfm_prettyphoto.js", "js");
}
if (document.querySelector('.fancybox') !== null) {
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/FancyBox/jquery.fancybox.css", "css");
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/FancyBox/jquery.fancybox.pack.js", "js");
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/FancyBox/rfm_fancybox.js", "js");
}
if (document.querySelector('.baguettebox') !== null) {
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/BaguetteBox/baguetteBox.min.css", "css");
	loadjscssfile("https://tkphotocenter.com/plugins/responsivefilemanager/js/BaguetteBox/baguetteBox.min.js", "js");
	window.onload = function() {
		baguetteBox.run('.baguettebox', {
			// Custom options
			fullScreen: false,
			animation: 'fadeIn',
			buttons: true,
			overlayBackgroundColor: 'rgba (0,0,0,.8)',
			captions: function(element) {
				return element.getElementsByTagName('img')[0].alt;
			}
		});
	};
}
</script>
</body>
</html>