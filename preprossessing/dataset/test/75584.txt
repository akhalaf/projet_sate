<!DOCTYPE html>
<html>
<head>
<!-- Site made with Mobirise Website Builder v5.2.0, https://mobirise.com -->
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="Mobirise v5.2.0, mobirise.com" name="generator"/>
<meta content="width=device-width, initial-scale=1, minimum-scale=1" name="viewport"/>
<link href="assets/images/adj3-187x147.jpg" rel="shortcut icon" type="image/x-icon"/>
<meta content="Empresa especializada em websites de grande escala. Como na Gestão técnica de todas as infraestruturas: redes, servidores, postos de trabalho e periféricos, etc. " name="description"/>
<title>Adj3 Sistemas</title>
<link href="assets/web/assets/mobirise-icons2/mobirise2.css" rel="stylesheet"/>
<link href="assets/web/assets/mobirise-icons/mobirise-icons.css" rel="stylesheet"/>
<link href="assets/tether/tether.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet"/>
<link href="assets/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet"/>
<link href="assets/dropdown/css/style.css" rel="stylesheet"/>
<link href="assets/formstyler/jquery.formstyler.css" rel="stylesheet"/>
<link href="assets/formstyler/jquery.formstyler.theme.css" rel="stylesheet"/>
<link href="assets/datepicker/jquery.datetimepicker.min.css" rel="stylesheet"/>
<link href="assets/socicon/css/styles.css" rel="stylesheet"/>
<link href="assets/theme/css/style.css" rel="stylesheet"/>
<link as="style" href="assets/mobirise/css/mbr-additional.css" rel="preload"/><link href="assets/mobirise/css/mbr-additional.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<section class="menu cid-s48OLK6784" id="menu1-h" once="menu">
<nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
<div class="container">
<div class="navbar-brand">
<span class="navbar-logo">
<a href="adj3.html#top">
<img alt="Adj3" src="assets/images/adj3-187x147.jpg" style="height: 4.6rem;"/>
</a>
</span>
</div>
<button aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<div class="hamburger">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item"><a class="nav-link link text-black display-4" href="index.html#testimonials1-12">SOBRE</a></li>
<li class="nav-item"><a class="nav-link link text-black text-primary display-4" href="index.html#features18-14">
                            SERVIÇOS</a></li></ul>
<div class="navbar-buttons mbr-section-btn"><a class="btn btn-secondary display-4" href="index.html#form5-2m">
                        CONTACTOS</a></div>
</div>
</div>
</nav>
</section>
<section class="slider1 cid-sldzmvK3za mbr-fullscreen" id="slider1-1l">
<div class="carousel slide" data-interval="5000" data-ride="carousel" id="slr0As9Hsr">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#slr0As9Hsr"></li>
<li data-slide-to="1" data-target="#slr0As9Hsr"></li>
<li data-slide-to="2" data-target="#slr0As9Hsr"></li>
</ol>
<div class="carousel-inner">
<div class="carousel-item slider-image item active">
<div class="item-wrapper">
<div class="carousel-caption">
<h5 class="mbr-section-subtitle mbr-fonts-style display-1"><a class="text-black text-primary" href="adj3.html#content4-29">Desenvolvimento de</a><strong><a class="text-primary" href="adj3.html#content4-29"> </a></strong><br/><a class="text-black text-primary" href="adj3.html#content4-29"><strong>PLATAFORMAS WEB</strong></a><strong><a class="text-primary" href="adj3.html#content4-29"> </a></strong><div><strong><a class="text-black text-primary" href="adj3.html#content4-29">de grande escala</a></strong></div><br/><div><strong><a class="text-black" href="index.html#content4-29"><br/></a></strong></div><br/><div><strong><br/></strong></div></h5>
</div>
<a href="index.html#content4-29" target="false"></a><a href="index.html#content4-29" target="false"></a><a href="adj3.html#content4-29"><img class="d-block w-100" src="assets/images/backg1-1288x860.png"/></a></div>
</div>
<div class="carousel-item slider-image item">
<div class="item-wrapper">
<div class="carousel-caption">
<h5 class="mbr-section-subtitle mbr-fonts-style display-1"><strong><a class="text-black" href="index.html#content4-2d"></a><a class="text-black" href="adj3.html#content4-2d"><b>SOLUÇÕES IT</b></a><a class="text-black" href="adj3.html#content4-2d"> </a></strong><a class="text-black" href="adj3.html#content4-2d">adaptadas às suas necessidades</a><br/><a class="text-black" href="index.html#content4-2d"><br/></a><br/><br/><br/><strong><br/></strong></h5>
</div>
<a href="index.html#content4-2d" target="false"></a><a href="index.html#content4-2d" target="false"></a><a href="adj3.html#content4-2d"><img class="d-block w-100" src="assets/images/backg2-1288x727.png"/></a></div>
</div>
<div class="carousel-item slider-image item">
<div class="item-wrapper">
<div class="carousel-caption">
<h5 class="mbr-section-subtitle mbr-fonts-style display-2"><br/><div><strong><br/></strong></div><div><strong><br/></strong></div><br/><div><a class="text-black" href="index.html#content15-2s"></a><a class="text-black text-primary" href="adj3.html#content6-2t">Somos</a><strong><a class="text-black text-primary" href="adj3.html#content6-2t"> ESPECIALIZADOS</a></strong></div><div><a class="text-black text-primary" href="adj3.html#content6-2t">em sites com informação de</a><strong><a class="text-primary" href="adj3.html#content6-2t"> </a></strong></div><div><a class="text-black text-primary" href="adj3.html#content6-2t"><strong>INTERESSE PÚBLICO</strong></a></div><br/><div><strong><br/></strong></div><div><strong><br/></strong></div><br/><div><strong><br/></strong></div></h5>
</div>
<a href="index.html#content15-2s" target="false"></a><a href="index.html#content6-2t" target="false"></a><a href="index.html#content6-2t" target="false"></a><a href="index.html#content6-2t" target="false"></a><a href="adj3.html#content6-2t" target="false"></a><a href="adj3.html#content15-2s"><img class="d-block w-100" src="assets/images/backg3-1-1900x1422.png"/></a></div>
</div>
</div>
<a class="carousel-control carousel-control-prev" data-slide="prev" href="#slr0As9Hsr" role="button">
<span aria-hidden="true" class="mobi-mbri mobi-mbri-arrow-prev"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control carousel-control-next" data-slide="next" href="#slr0As9Hsr" role="button">
<span aria-hidden="true" class="mobi-mbri mobi-mbri-arrow-next"></span>
<span class="sr-only">Next</span>
</a>
</div>
</section><script class="cid-sldzmvK3za" data-rv-view="634" id="slider1-1l" type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ff71ba5a9a34e36b96a0898/1erek7vnf';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<section class="testimonials1 cid-sksiqAedIY" id="testimonials1-12">
<div class="container">
<h3 class="mbr-section-title mbr-fonts-style align-center mb-4 display-1"><strong>
            QUEM SOMOS</strong></h3>
<div class="row align-items-center">
<div class="col-12 col-md-6">
<div class="image-wrapper">
<img alt="computador codigo" src="assets/images/business-2717066-1920-1076x712.jpg"/>
</div>
</div>
<div class="col-12 col-md">
<div class="text-wrapper">
<p class="mbr-text mbr-fonts-style mb-4 display-7"><strong>A ADJ 3 SISTEMAS - PROJECTO E GESTÃO DE SISTEMAS INFORMÁTICOS LDA</strong>, <br/>com sede em Faro, foi fundada em 1998 por um grupo de trabalho envolvido na execução de vários projetos de investigação na Universidade do Algarve.<br/>Desde então dedicou-se ao desenvolvimento de aplicações informáticas e consultoria especializada, tendo consolidado a sua presença no mercado regional (a sul do Pais) e também com a participação em alguns projetos internacionais.</p>
</div>
</div>
</div>
</div>
</section>
<section class="features1 cid-sksiTq1lxE" id="features2-13">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mobi-mbri-setting mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7"><strong>+ 1500 Projetos</strong></h4>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mobi-mbri-growing-chart mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7">
<strong>97%</strong>
<div><strong>Tx. Retenção de cliente</strong></div></h4>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mobi-mbri-smile-face mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7">
<strong>100%</strong>
<div><strong>Positividade &amp; Flexibilidade</strong></div></h4>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mobi-mbri-code mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7"><strong>Modern Code</strong></h4>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="features17 cid-sksjKBXC4g" id="features18-14">
<div class="container">
<div class="mbr-section-head">
<div class="col-12">
<h4 class="mbr-section-title mbr-fonts-style align-center mb-0 display-1">
<strong>SERVIÇOS</strong></h4>
</div>
</div>
<div class="row mt-4">
<div class="col-12 col-md-6 col-lg-4">
<div class="card-wrapper mb-4">
</div>
</div>
<div class="col-12 col-md-6 col-lg-4">
<div class="card-wrapper mb-4">
</div>
</div>
<div class="col-12 col-md-6 col-lg-4">
<div class="card-wrapper mb-4">
</div>
</div>
</div>
</div>
</section>
<section class="features4 cid-sksjXnahou" id="features5-15">
<div class="container-fluid">
<div class="row">
<div class="col-12 col-lg-6">
<div class="card-wrapper">
<div class="row">
<div class="col-12 col-md-7">
<div class="text-wrapper">
<h5 class="card-title mbr-fonts-style display-5"><strong>Programação Web e Mobile</strong></h5>
<p class="mbr-text mbr-fonts-style mb-5 display-4">Larga experiência em criar plataformas de grande escala</p>
<div class="mbr-section-btn"><a class="btn btn-secondary display-4" href="index.html#features18-14">saber mais</a></div>
</div>
</div>
<div class="col-12 col-md-5">
<div class="img-wrapper">
<img alt="drupal, react" src="assets/images/drupal.react-357x205.png"/>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 col-lg-6">
<div class="card-wrapper">
<div class="row">
<div class="col-12 col-md-7">
<div class="text-wrapper">
<h5 class="card-title mbr-fonts-style display-5"><strong>Soluções IT</strong></h5>
<p class="mbr-text mbr-fonts-style mb-5 display-4">Consultoria, gestão de servidores<br/>Segurança, Redes, Base de Dados, Servidores, Virtualização, Storage, Backup e infraestrutura na Cloud.</p>
<div class="mbr-section-btn"><a class="btn btn-secondary display-4" href="https://mobiri.se">saber mais</a></div>
</div>
</div>
<div class="col-12 col-md-5">
<div class="img-wrapper">
<img alt="veeam, vmware, cisco, centos, linux" src="assets/images/logo3-1-357x250.png"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="content4 cid-sle50i9jRQ" id="content4-29">
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
<h3 class="mbr-section-title mbr-fonts-style align-center mb-4 display-1"><strong>PROGRAMAÇÃO WEB E MOBILE</strong></h3>
</div>
</div>
</div>
</section>
<section class="features1 cid-sle5DrrsYM" id="features2-2b">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mbri-mobile" style="color: rgb(53, 53, 53); fill: rgb(53, 53, 53);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7">
                            App Mobile</h4>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mbri-desktop" style="color: rgb(53, 53, 53); fill: rgb(53, 53, 53);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7">
                            Web Plataform</h4>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3">
<div class="card-wrapper">
<div class="card-box align-center">
<span class="mbr-iconfont mbri-shopping-cart" style="color: rgb(53, 53, 53); fill: rgb(53, 53, 53);"></span>
<h4 class="card-title align-center mbr-black mbr-fonts-style display-7">
                            Ecommerce</h4>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="header6 cid-skskxc7A7E" id="header6-16">
<div class="align-center container">
<div class="row justify-content-center">
<div class="col-12 col-lg-9">
<p class="mbr-text mbr-white mbr-fonts-style display-7">Plataformas digitais tem transformado profundamente os parâmetros de negociação e mercado em larga escala e são, cada vez mais, uma tendência mundial.<br/><br/>As plataformas digitais não são apenas softwares, mas um ecossistema complexo de usuários com diferentes anseios e necessidades. Ao mesmo tempo, com a quantidade de informação gerada é possível utilizar os dados como um valioso ativo, estabelecer conexões e interações reais em larga escala e entregar soluções que antes não seriam possíveis.<br/></p>
</div>
</div>
</div>
</section>
<section class="content6 cid-sljs1aTFc4" id="content6-2t">
<div class="container">
<div class="row justify-content-center">
<div class="col-md-12 col-lg-10">
<hr class="line"/>
<p class="mbr-text align-center mbr-fonts-style my-4 display-5"><em>É de uma<strong> transformação digital </strong>que precisa?</em><br/></p>
<hr class="line"/>
</div>
</div>
</div>
</section>
<section class="content15 cid-sljoaWVZ33 mbr-parallax-background" id="content15-2s">
<div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(255, 255, 255);">
</div>
<div class="container-fluid">
<div class="row justify-content-center">
<div class="card col-md-12 col-lg-11">
<div class="card-wrapper">
<div class="card-box align-left">
<p class="mbr-text mbr-fonts-style display-7"><strong>Somos especializados em plataformas digitais com informação de interesse público</strong> de grande escala a nível nacional e internacional<br/><br/>- sites e aplicativos para empresas governamentais<br/>- sites para o ensino superior<br/>- Plataformas de eCommerce <br/>- Sites de sociedade civil    <br/>- Plataformas para organizações sem fins lucrativos<br/></p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="content4 cid-sljj5y2rA4" id="content4-2q">
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-9">
<h3 class="mbr-section-title mbr-fonts-style align-center mb-4 display-5"><strong>Porque usamos CMS Drupal?</strong></h3>
<h4 class="mbr-section-subtitle align-center mbr-fonts-style mb-4 display-7"><div><span style="font-size: 1.4rem;">Drupal é usado por alguns dos maiores e mais prestigiados sites do mundo:</span><br/></div><div><br/></div><div>- NASA - <a class="text-black" href="https://www.nasa.gov">nasa.gov</a></div><div>- Tesla - <a class="text-black" href="https://www.tesla.com/pt_pt">teslamotors.com</a></div><div>- Harvard University - <a class="text-black" href="https://www.harvard.edu/">harvard.edu</a></div><div><br/></div><div><br/></div><div>O Drupal permite o desenvolvimento rápido, estruturado e sustentado de praticamente todo o tipo de soluções baseadas na web (cloud). </div><div><br/></div><div><strong>- Funcional </strong></div><div><strong>- Flexível</strong></div><div><strong>- Fácil de usar</strong></div><div><strong>- Seguro</strong></div></h4>
</div>
</div>
</div>
</section>
<section class="gallery3 cid-skskTuRUOj" id="gallery3-17">
<div class="container-fluid">
<div class="mbr-section-head">
<h4 class="mbr-section-title mbr-fonts-style align-center mb-0 display-5"><strong>Como Trabalhamos</strong></h4>
</div>
<div class="row mt-4">
<div class="item features-image сol-12 col-md-6 col-lg-3">
<div class="item-wrapper">
<div class="item-img">
<img alt="ideia" src="assets/images/question-480x249.png"/>
</div>
<div class="item-content">
<h5 class="item-title mbr-fonts-style display-7">
<strong>Estratégia</strong>
</h5>
<p class="mbr-text mbr-fonts-style mt-3 display-7">1º Entendemos a necessidade do cliente.<br/>2º Projetamos a solução.<br/></p>
</div>
</div>
</div>
<div class="item features-image сol-12 col-md-6 col-lg-3">
<div class="item-wrapper">
<div class="item-img">
<img alt="computador" src="assets/images/laptop-479x244.png"/>
</div>
<div class="item-content">
<h5 class="item-title mbr-fonts-style display-7">
<strong>Design</strong>
</h5>
<p class="mbr-text mbr-fonts-style mt-3 display-7">Temos acesso a várias equipas especializadas.<br/>Design pensado nas suas necessidades.<br/></p>
</div>
</div>
</div>
<div class="item features-image сol-12 col-md-6 col-lg-3">
<div class="item-wrapper">
<div class="item-img">
<img alt="developer" src="assets/images/dev-480x250.png"/>
</div>
<div class="item-content">
<h5 class="item-title mbr-fonts-style display-7">
<strong>Desenvolvimento</strong>
</h5>
<p class="mbr-text mbr-fonts-style mt-3 display-7">Usamos a famosa plataforma CMS de código aberto DRUPAL.<br/></p>
</div>
</div>
</div>
<div class="item features-image сol-12 col-md-6 col-lg-3">
<div class="item-wrapper">
<div class="item-img">
<img alt="apoio telefonico" src="assets/images/supporte-481x243.png" title=""/>
</div>
<div class="item-content">
<h5 class="item-title mbr-fonts-style display-7"><strong>Suporte contínuo</strong></h5>
<p class="mbr-text mbr-fonts-style mt-3 display-7">Ensinamos o nosso cliente a ser o mais autónomo possível e aproveitar ao máximo o seu site.<br/></p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="gallery5 mbr-gallery cid-skslkqykrq" id="gallery5-18">
<div class="container">
<div class="mbr-section-head">
<h3 class="mbr-section-title mbr-fonts-style align-center m-0 display-5"><strong>Os nossos Clientes</strong><strong><br/></strong></h3>
<h4 class="mbr-section-subtitle mbr-fonts-style align-center mb-0 mt-2 display-7">São das algumas das entidades com as quais temos o prazer de trabalhar.</h4>
</div>
<div class="row mbr-gallery mt-4">
<div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="ualg" class="w-100" data-slide-to="0" data-target="#lb-slr0AxaXNB" src="assets/images/ualg-506x160.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="fmul" class="w-100" data-slide-to="1" data-target="#lb-slr0AxaXNB" src="assets/images/fmul-506x229.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="universidade lisboa" class="w-100" data-slide-to="2" data-target="#lb-slr0AxaXNB" src="assets/images/ulisboa-506x183.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div>
<div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="clul" class="w-100" data-slide-to="3" data-target="#lb-slr0AxaXNB" src="assets/images/clul-logo-506x124.jpg"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div><div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="omat" class="w-100" data-slide-to="4" data-target="#lb-slr0AxaXNB" src="assets/images/omat-238x92.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div><div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="" class="w-100" data-slide-to="5" data-target="#lb-slr0AxaXNB" src="assets/images/ipma-506x146.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div><div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="ccdr algarve" class="w-100" data-slide-to="6" data-target="#lb-slr0AxaXNB" src="assets/images/ccdr-2880w-171x91.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div><div class="col-12 col-md-6 col-lg-3 item gallery-image">
<div class="item-wrapper" data-target="#slr0AxaXNB-modal" data-toggle="modal">
<img alt="ccmar" class="w-100" data-slide-to="7" data-target="#lb-slr0AxaXNB" src="assets/images/ccmar-189x50.png"/>
<div class="icon-wrapper">
<span class="mobi-mbri mobi-mbri-search mbr-iconfont mbr-iconfont-btn"></span>
</div>
</div>
</div>
</div>
<div aria-hidden="true" class="modal mbr-slider" id="slr0AxaXNB-modal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body">
<div class="carousel slide" data-interval="5000" id="lb-slr0AxaXNB">
<div class="carousel-inner">
<div class="carousel-item active">
<img alt="" class="d-block w-100" src="assets/images/ualg-506x160.png"/>
</div>
<div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/fmul-506x229.png"/>
</div>
<div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/ulisboa-506x183.png"/>
</div>
<div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/clul-logo-506x124.jpg"/>
</div><div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/omat-238x92.png"/>
</div><div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/ipma-506x146.png"/>
</div><div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/ccdr-2880w-171x91.png"/>
</div><div class="carousel-item">
<img alt="" class="d-block w-100" src="assets/images/ccmar-189x50.png"/>
</div>
</div>
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#lb-slr0AxaXNB"></li>
<li data-slide-to="1" data-target="#lb-slr0AxaXNB"></li>
<li data-slide-to="2" data-target="#lb-slr0AxaXNB"></li>
<li data-slide-to="3" data-target="#lb-slr0AxaXNB"></li><li data-slide-to="4" data-target="#lb-slr0AxaXNB"></li><li data-slide-to="5" data-target="#lb-slr0AxaXNB"></li><li data-slide-to="6" data-target="#lb-slr0AxaXNB"></li><li data-slide-to="7" data-target="#lb-slr0AxaXNB"></li>
</ol>
<a aria-label="Close" class="close" data-dismiss="modal" href="" role="button">
</a>
<a class="carousel-control-prev carousel-control" data-slide="prev" href="#lb-slr0AxaXNB" role="button">
<span aria-hidden="true" class="mobi-mbri mobi-mbri-arrow-prev"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next carousel-control" data-slide="next" href="#lb-slr0AxaXNB" role="button">
<span aria-hidden="true" class="mobi-mbri mobi-mbri-arrow-next"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="content4 cid-sleewapNRC mbr-parallax-background" id="content4-2d">
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
<h3 class="mbr-section-title mbr-fonts-style align-center mb-4 display-1">
<strong>SOLUÇÕES IT</strong></h3>
</div>
</div>
</div>
</section>
<section class="content15 cid-sleelUsfP6 mbr-parallax-background" id="content15-2c">
<div class="container">
<div class="row justify-content-center">
<div class="card col-md-12 col-lg-10">
<div class="card-wrapper">
<div class="card-box align-left">
<p class="mbr-text mbr-fonts-style display-7">Serviços de manutenção, monitorização e gestão de recursos informáticos.<br/>Aplicações e soluções de comunicações de acordo com as suas necessidades de negócio.<br/><br/><br/>- suporte às questões tecnológicas e planeamento dos recursos<br/>- deteção de problemas de performance e acesso a bases de dados<br/>- arquitetura e apoio nas aquisições para a implementação da principal rede informática<br/>- gestão e apoio à decisão relacionada com os recursos informáticos<br/>- apoio em outsourcing<br/>- apoio na tomada de decisão e reformulação da infraestrutura informática, por forma a otimizar e melhorar processos<br/>- apoio à tomada de decisão sobre os recursos informáticos e estratégia tecnológica com mais de 30 mil clientes finais<br/></p>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="content4 cid-sloXVL0Vsf mbr-parallax-background" id="content4-2z">
<div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
</div>
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
</div>
</div>
</div>
</section>
<section class="content10 cid-sljQFOW7hF" id="content10-2v">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-md">
<h6 class="mbr-section-subtitle mbr-fonts-style mb-4 display-1"><strong>
                    Consultoria</strong></h6>
</div>
<div class="col-md-12 col-lg-8">
<p class="mbr-text mbr-fonts-style display-7">Na área da consultoria, a ADJ3 apresenta-se como um parceiro fundamental na tomada de decisões dos clientes, quer ao nível tecnológico como ao nível da gestão e organização da informação automatizada. Com os nossos serviços, o cliente poderá esperar um aumento da sua produtividade e uma maior rentabilização do investimento em tecnologias de informação. </p>
</div>
</div>
</div>
</section>
<section class="content4 cid-slkgjM7GtK mbr-parallax-background" id="content4-2x">
<div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
</div>
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
</div>
</div>
</div>
</section>
<section class="content10 cid-sljQH4fcKV" id="content10-2w">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-md">
<h6 class="mbr-section-subtitle mbr-fonts-style mb-4 display-1"><strong>Parques informáticos</strong></h6>
</div>
<div class="col-md-12 col-lg-8">
<p class="mbr-text mbr-fonts-style display-7">Na área da gestão dos parques informáticos a ADJ3 apresenta-se como um completo departamento técnico aos clientes, fazendo a gestão técnica de todas as infraestruturas: redes, servidores, postos de trabalho e periféricos. Desta forma, permite que o cliente se foque no seu negócio, deixando para nós as preocupações tecnológicas, a manutenção do parque informático e a resolução dos problemas típicos que surgem neste tipo de ambientes.</p>
</div>
</div>
</div>
</section>
<section class="content4 cid-slkh7pSQpY mbr-parallax-background" id="content4-2y">
<div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
</div>
<div class="container">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
</div>
</div>
</div>
</section>
<section class="form5 cid-sliZvc2Hq9" id="form5-2m">
<div class="container">
<div class="mbr-section-head">
<h3 class="mbr-section-title mbr-fonts-style align-center mb-0 display-1">
<strong>CONTACTE-NOS</strong><br/><strong><br/></strong></h3>
</div>
<div class="row justify-content-center mt-4">
<div class="col-lg-8 mx-auto mbr-form" data-form-type="formoid">
<form action="https://mobirise.eu/" class="mbr-form form-with-styler" data-form-title="Form Name" method="POST"><input data-form-email="true" name="email" type="hidden" value="GOB2Oy1z0eSKSyijEp2apxrpnavzdkE79NN6Z/DE5azx+R6RyO8CP/pxCg8R0RbjBWQT9FEtIw87A1XsN/5A2YfPbGayYJvIPpN8RGTsKG/6+V7ogNHoGoR30XKz6A+P"/>
<div class="">
<div class="alert alert-success col-12" data-form-alert="" hidden="hidden">Obrigado! Entraremos em contacto.</div>
<div class="alert alert-danger col-12" data-form-alert-danger="" hidden="hidden">Oops...! some
                            problem!</div>
</div>
<div class="dragArea row">
<div class="col-md col-sm-12 form-group" data-for="name">
<input class="form-control" data-form-field="name" id="name-form5-2m" name="name" placeholder="Nome" type="text" value=""/>
</div>
<div class="col-md col-sm-12 form-group" data-for="email">
<input class="form-control" data-form-field="email" id="email-form5-2m" name="email" placeholder="E-mail" type="email" value=""/>
</div>
<div class="col-12 form-group" data-for="textarea">
<textarea class="form-control" data-form-field="textarea" id="textarea-form5-2m" name="textarea" placeholder="Assunto"></textarea>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 align-center mbr-section-btn"><button class="btn btn-secondary display-4" type="submit">Enviar Mensagem</button></div>
</div>
</form>
</div>
</div>
</div>
</section>
<section class="contacts3 map1 cid-sljIIiUTUs" id="contacts3-2u">
<div class="container">
<div class="row justify-content-center mt-4">
<div class="card col-12 col-md-6">
<div class="card-wrapper">
<div class="image-wrapper">
<span class="mbr-iconfont mobi-mbri-phone mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
</div>
<div class="text-wrapper">
<h6 class="card-title mbr-fonts-style mb-1 display-7"><strong>Telefone</strong> <br/></h6>
<p class="mbr-text mbr-fonts-style display-7">(+351) 289 819 111 </p>
</div>
</div>
<div class="card-wrapper">
<div class="image-wrapper">
<span class="mbr-iconfont mobi-mbri-map-pin mobi-mbri" style="color: rgb(0, 190, 241); fill: rgb(0, 190, 241);"></span>
</div>
<div class="text-wrapper">
<h6 class="card-title mbr-fonts-style mb-1 display-7"><strong>Email</strong><br/></h6>
<p class="mbr-text mbr-fonts-style display-7">geral@adj3.pt                      </p>
</div>
</div>
</div>
<div class="map-wrapper col-12 col-md-6">
<div class="google-map"><iframe allowfullscreen="" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDk89J4FSunMF33ruMVWJaJht_Ro0kvoXs&amp;q=Rua das Violetas Lote T, Loja 3 Gambelas8005-200, Faro Portugal" style="border:0"></iframe></div>
</div>
</div>
</div>
</section>
<section class="content4 cid-sleZHl9T2D" id="content4-2j">
<div class="container-fluid">
<div class="row justify-content-center">
<div class="title col-md-12 col-lg-10">
<h4 class="mbr-section-subtitle align-center mbr-fonts-style mb-4 display-7">Cofinanciado por:</h4>
</div>
</div>
</div>
</section>
<section class="clients2 cid-sleYSr7VdS" id="clients2-2i">
<div class="container-fluid">
<div class="row justify-content-center">
<div class="card col-12 col-md-6 col-lg-4">
<div class="card-wrapper">
<div class="img-wrapper">
<img alt="portugal 2020" src="assets/images/portugal2020-280x85.png"/>
</div>
<div class="card-box align-center">
</div>
</div>
</div>
<div class="card col-12 col-md-6 col-lg-4">
<div class="card-wrapper">
<div class="img-wrapper">
<img alt="cresc algarve 2020" src="assets/images/logo-cresc-transparente-320w-1-280x165.png"/>
</div>
<div class="card-box align-center">
</div>
</div>
</div>
<div class="card col-12 col-md-6 col-lg-4">
<div class="card-wrapper">
<div class="img-wrapper">
<img alt="ue" src="assets/images/ue-280x72.png"/>
</div>
<div class="card-box align-center">
</div>
</div>
</div>
</div>
</div>
</section><section style="background-color: #fff; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Helvetica Neue', Arial, sans-serif; color:#aaa; font-size:12px; padding: 0; align-items: center; display: flex;"><a href="https://mobirise.site/r" style="flex: 1 1; height: 3rem; padding-left: 1rem;"></a><p style="flex: 0 0 auto; margin:0; padding-right:1rem;">Set up a free site with <a href="https://mobirise.site/z" style="color:#aaa;">Mobirise</a></p></section><script src="assets/web/assets/jquery/jquery.min.js"></script> <script src="assets/popper/popper.min.js"></script> <script src="assets/tether/tether.min.js"></script> <script src="assets/bootstrap/js/bootstrap.min.js"></script> <script src="assets/smoothscroll/smooth-scroll.js"></script> <script src="assets/dropdown/js/nav-dropdown.js"></script> <script src="assets/dropdown/js/navbar-dropdown.js"></script> <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script> <script src="assets/parallax/jarallax.min.js"></script> <script src="assets/formstyler/jquery.formstyler.js"></script> <script src="assets/formstyler/jquery.formstyler.min.js"></script> <script src="assets/datepicker/jquery.datetimepicker.full.js"></script> <script src="assets/theme/js/script.js"></script> <script src="assets/formoid/formoid.min.js"></script>
</body>
</html>