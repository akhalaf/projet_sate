<html>
<head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79373620-1', 'auto');
  ga('send', 'pageview');

</script>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Chwilówki Sosnowiec - P.W.VITA - Pożyczki, Chwilówki do ręki, Kredyty, Pożyczki bez BIK - Modrzejowska, Targowa - Sosnowiec</title>
<meta content="chwilówki sosnowiec,pożyczki sosnowiec,chwilówki do ręki,kredyty sosnowiec,pożyczki pozabankowe sosnowiec,kredyty bankowe sosnowiec,pożyczki bez bik sosnowiec" name="keywords"/>
<meta content="Codeincode.pl" name="author"/>
<meta content="Oferujemy chwilówki, pożyczki, kredyty, chwilówki do ręki dla Klientów z Sosnowca i okolic. Zapraszamy na ulicę Modrzejowską / Targową do naszego biura kredytowego." name="description"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<link href="/media/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/media/js/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
<link href="/media/css/rwd.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<a href="https://www.facebook.com/pwvita" id="fb-icon" target="_blank"><img src="/media/img/fb-icon.png"/></a>
<div id="cookies-box">
<div id="cookie-policy">
<div id="inner-cookie-policy">
<p>
                Nasz serwis internetowy tworzy i wykorzystuje pliki Cookies, które będą zapisywane na urządzeniu o ile nie zostaną zmienione ustawienia przeglądarki. Więcej informacji o cookies, zakresie i celu przetwarzania danych, znajduje się w <a href="/polityka-prywatnosci.html" style="font-weight:bold;color:#007bc1;">polityce prywatności</a>.
            </p>
<a class="close" href="#">×</a>
</div>
</div>
</div>
<div id="phone-func">
<a class="f1" href="tel:32 793 89 89">Zadzwoń<br/>(32) 793 89 89</a>
<a class="f2" href="mailto:kontakt@pwvita.pl">Napisz<br/>kontakt@pwvita.pl</a>
</div>
<div id="page">
<header id="header">
<div class="container">
<div class="left">
<div id="logo">
<a href="/"><img alt="Kredyty, pożyczki, chwilówki" src="/media/img/logo.jpg"/></a>
</div>
<div id="fast-contact-rwd">
<marquee>Masz pytania? Zadzwoń do nas <span class="big phone"><span class="small">32</span> 793 89 89</span></marquee>
</div>
</div>
<div class="right">
<a id="menu-rwd"></a>
<div id="fast-contact">
<marquee>Masz pytania? Zadzwoń do nas <span class="big phone"><span class="small">32</span> 793 89 89</span></marquee>
</div>
<nav id="menu">
<ul id="main-menu">
<li><a href="/">Strona główna</a></li>
<li><a href="/o-nas.html">O nas</a></li>
<li><a href="/oferta.html">Oferta</a></li>
<li><a href="/aktualnosci.html">Aktualności</a></li>
<li><a href="/oddzialy.html">Oddziały</a></li>
<li><a href="/praca.html">Praca</a></li>
<li><a href="/kontakt.html">Kontakt</a></li>
</ul>
</nav>
</div>
</div>
</header>
<div id="slider">
<ul>
<li>
<img alt="pożyczki sosnowiec" src="/media/img/slide3.jpg"/>
<div class="desc id-0">
<h1>Pożyczki pozabankowe</h1>
<p>
                                Masz problem ze zdolnością kredytową?<br/>
                                Ta oferta jest właśnie dla Ciebie!
                            </p>
<a class="standard-button" href="/pozyczki-pozabankowe.html">Szczegóły oferty</a>
</div>
<div class="form-get-loan" id="formgetloanid-0">
<h2>Ustaw kwotę pożyczki</h2>
<p>od <span>1000 zł</span> do <span>25 000 zł</span></p>
<input class="range-input" max="25000" min="1000" type="range" value="1000"/>
<p>Kwota pożyczki: <span class="price">1000 zł</span></p>
<a class="standard-button" href="/formularz-wniosku.html?typ=pozyczka">Wypełnij wniosek</a>
</div>
</li>
<li>
<img alt="chwilówki sosnowiec" src="/media/img/slide2.jpg"/>
<div class="desc id-1">
<h1>Chwilówki</h1>
<p>
<span class="red">Wypłata gotówki do ręki!</span><br/>
                                Minimum formalności i natychmiastowa decyzja!
                            </p>
<a class="standard-button" href="/chwilowki.html">Szczegóły oferty</a>
</div>
<div class="form-get-loan" id="formgetloanid-1">
<h2>Ustaw kwotę chwilówki</h2>
<p>od <span>500 zł</span> do <span>2000 zł</span></p>
<input class="range-input" max="2000" min="500" type="range" value="500"/>
<p>Kwota chwilówki: <span class="price">500 zł</span></p>
<a class="standard-button" href="/formularz-wniosku.html?typ=chwilowka">Wypełnij wniosek</a>
</div>
</li>
<li>
<img alt="kredyty sosnowiec" src="/media/img/slide1.jpg"/>
<div class="desc id-2">
<h1>Kredyty bankowe</h1>
<p>
                                Bogata oferta kredytowa dla osób prywatnych oraz firm!<br/>Pomożemy Ci wypełnić wniosek i uzyskać potrzebne środki.
                            </p>
<a class="standard-button" href="/kredyty-bankowe.html">Szczegóły oferty</a>
</div>
<div class="form-get-loan" id="formgetloanid-2">
<h2>Ustaw kwotę kredytu</h2>
<p>od <span>1000 zł</span> do <span>120 000 zł</span></p>
<input class="range-input" max="120000" min="1000" type="range" value="1000"/>
<p>Kwota kredytu: <span class="price">1000 zł</span></p>
<a class="standard-button" href="/formularz-wniosku.html?typ=kredyt">Wypełnij wniosek</a>
</div>
</li>
<li>
<img alt="pożyczki dla firm sosnowiec" src="/media/img/slide5.jpg"/>
<div class="desc id-3">
<h1>Pożyczki dla firm</h1>
<p>
                                Potrzebujesz finansowania dla twojej firmy?
                                <br/>
                                Masz problemy z uzyskaniem kredytu bankowego?
                                <br/>
                                Mamy dla Ciebie rozwiązanie!
                            </p>
<a class="standard-button" href="/pozyczki-dla-firm.html">Szczegóły oferty</a>
</div>
<div class="form-get-loan" id="formgetloanid-3">
<h2>Ustaw kwotę pożyczki</h2>
<p>od <span>1000 zł</span> do <span>25 000 zł</span></p>
<input class="range-input" max="25000" min="1000" type="range" value="1000"/>
<p>Kwota pożyczki: <span class="price">1000 zł</span></p>
<a class="standard-button" href="/formularz-wniosku.html?typ=pozyczkadlafirm">Wypełnij wniosek</a>
</div>
</li>
<li>
<img alt="ubezpieczenia sosnowiec" src="/media/img/slide4.jpg"/>
<div class="desc id-4">
<h1>Ubezpieczenia</h1>
<p>
                                Nie czekaj.. Skorzystaj z ubezpieczeń komunikacyjnych,<br/>majątkowych lub osobowych.
                            </p>
<a class="standard-button" href="/ubezpieczenia.html">Szczegóły oferty</a>
</div>
</li>
</ul>
<a id="scroll-bottom"></a>
</div>
<article class="home" id="content">
<div class="container" id="offers">
<h2>Pożyczki Chwilówki Sosnowiec</h2>
<p style="margin-top: 0;">Zapoznaj się z bogatą ofertą kredytową naszej firmy</p>
<ul>
<li><img alt="pożyczki sosnowiec" src="media/img/offer3.jpg"/>
<div class="desc">
<h3>Pożyczki pozabankowe</h3>
<p>Jeśli potrzebujesz większej pożyczki a nie posiadasz zdolności kredytowej, ta oferta jest właśnie dla Ciebie.</p>
<a class="read-more" href="pozyczki-pozabankowe.html">Szczegóły oferty..</a></div>
</li>
<li class="last"><img alt="chwilówki sosnowiec" src="media/img/offer2.jpg"/>
<div class="desc">
<h3>Chwilówki</h3>
<p>Szybka gotówka do 2000 zł na dowód. Minimum formalności i dokumentów oraz natychmiastowa decyzja!</p>
<a class="read-more" href="chwilowki.html">Szczegóły oferty..</a></div>
</li>
<li><img alt="kredyty bankowe sosnowiec" src="media/img/offer1.jpg"/>
<div class="desc">
<h3>Kredyty bankowe</h3>
<p>Szukasz kredytu do 200 000 zł? Posiadamy bogatą ofertę kredytową dla osób prywatnych oraz firm. Zapoznaj się ze szczegółami i wypełnij wniosek!</p>
<a class="read-more" href="kredyty-bankowe.html">Szczegóły oferty..</a></div>
</li>
<li class="last"><img alt="ubezpieczenia sosnowiec" src="media/img/offer4.jpg"/>
<div class="desc">
<h3>Ubezpieczenia</h3>
<p>Z pomocą naszej firmy możesz ubezpieczyć samochód, mieszkanie, dom a nawet wycieczkę!</p>
<a class="read-more" href="ubezpieczenia.html">Szczegóły oferty..</a></div>
</li>
<li><img alt="pożyczki dla firm sosnowiec" src="media/img/offer5.jpg"/>
<div class="desc">
<h3>Pożyczki dla firm</h3>
<p>Potrzebujesz finansowania dla twojej firmy? Chodzisz po bankach a one odmawiają ze względu na brak zdolności kredytowej lub nieprzychylne wpisy w BIK, KRD czy ERIF?</p>
<a class="read-more" href="pozyczki-dla-firm.html">Szczegóły oferty..</a></div>
</li>
</ul>
</div> <div id="news">
<div class="container">
<h2>Aktualności</h2>
<p>Obserwuj na bieżąco nasze najnowsze oferty,<br/>propozycje oraz działania!</p>
<a class="standard-button" href="aktualnosci.html">Czytaj więcej..</a></div>
</div>
</article>
<footer id="footer">
<div id="newsletter">
<div class="container">
<div class="text">Zapisz się do naszego newslettera i otrzymuj najnowsze oferty!</div>
<div class="form">
<form method="post">
<input id="newsletter-email" name="newsletter-email" placeholder="Wprowadź adres e-mail.." type="text" value=""/>
<input id="newsletter-button" name="newsletter-button" type="submit" value=""/>
</form>
</div>
</div>
</div>
<div class="row">
<div class="container">
<div class="left">
                            Copyright © 2020 / pwvita.pl / All rights reserved.
                        </div>
<div class="right">
                            Realizacja: <a href="http://www.codeincode.pl/">Projektowanie stron Sosnowiec</a>
</div>
</div>
</div>
</footer>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script src="/media/js/bxslider/jquery.bxslider.min.js"></script>
<script src="/media/js/cookie.js"></script>
<script>
            $(document).ready(function(){  
                
                if(!$.cookie('cookies_msg')){
                    $('#cookie-policy').slideDown('slow');
                }
                
                $('#cookie-policy a.close').click(function(){
                    $.cookie('cookies_msg', '1', { expires: 7 });
                    $('#cookie-policy').slideUp('slow');
                    return false;
                });
                
                // pulsowanie elementow
                var tmp = 0;
                
                setInterval(function() {
                    if(tmp == 0){
                        $('#footer #newsletter .form').css('transform', 'scale(1.05)');
                        tmp = 1;
                    }
                    
                    else{
                        $('#footer #newsletter .form').css('transform', 'scale(1.0)');
                        tmp = 0;
                    }
                }, 1000);
                
                
                $('#menu-rwd').click(function(){
                    if($('#main-menu').hasClass('visible-menu')){
                        $('#main-menu').removeClass('visible-menu');
                    }
    
                    else{
                        $('#main-menu').addClass('visible-menu');
                    }
    
                    return false;
                }); 
                
                $('#slider ul').bxSlider({
                      mode: 'horizontal',
                      useCSS: false,
                      easing: 'easeOutElastic',
                      speed: 2000,
                      pause:    10000,
                      controls: false,
                      pager: true,
                      autoStart: true,
                      auto: true,
                      infiniteLoop: true,
                      responsive: true,
                      onSliderLoad: function(currentIndex){
                          $('.id-0').fadeIn(1000);
                          $('#formgetloanid-0').fadeIn(1000);
                      },
      
                      onSlideAfter: function($slideElement, oldIndex, newIndex){
                            $('.id-'+oldIndex).hide();
                            $('.id-'+newIndex).fadeIn(1000);
                            
                            $('#formgetloanid-'+oldIndex).hide();
                            $('#formgetloanid-'+newIndex).fadeIn(1000); 
                      }
                });
                
                $('#communicats-field .close').click(function(){
                    $("#communicats-field").hide(); 
                    return false;
                });
                
                $('.form-get-loan .range-input').on('input change', function() {
                    $(this).next().children('.price').html( $(this).val() + ' zł');
                });
                
                $('.form-get-loan .standard-button').click(function(){
                   var url = $(this).attr('href');
                   $(this).attr(
                       'href', 
                        url+'&kwota='+($(this).prev().prev().val())
                    );
                });
                
                $('#scroll-bottom').click(function(){
                    $('html, body').animate({
                        scrollTop: ($('#offers').offset().top)-150
                    }, 2000);
                });
    
            });
        </script>
</body>
</html>