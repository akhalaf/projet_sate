<!DOCTYPE html>
<html lang="es"> <head> <meta charset="utf-8"/> <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/> <link href="https://www.signsas.com/xmlrpc.php" rel="pingback"/> <title>Servicios Ingenieria Sign SAS – Otro sitio realizado con WordPress</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.signsas.com/feed/" rel="alternate" title="Servicios Ingenieria Sign SAS » Feed" type="application/rss+xml"/>
<link href="https://www.signsas.com/comments/feed/" rel="alternate" title="Servicios Ingenieria Sign SAS » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.signsas.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.11"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.signsas.com/wp-includes/css/dist/block-library/style.min.css?ver=5.0.11" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/dazzlersoft-teams/assets/css/font-awesome/css/font-awesome.min.css?ver=5.0.11" id="dazzler_team_m-font-awesome-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/dazzlersoft-teams/assets/css/bootstrap-front.css?ver=5.0.11" id="dazzler_team_m_bootstrap-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/dazzlersoft-teams/assets/css/teams.css?ver=5.0.11" id="dazzler_team_m_teams_css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/css/bootstrap.css?ver=5.0.11" id="progress_wp_br_bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/css/font-awesome/css/font-awesome.min.css?ver=5.0.11" id="progr_wp_b-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/css/meanmenu.min.css?ver=5.0.11" id="progr_wp_jq-ae-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/css/animate.min.css?ver=5.0.11" id="progr_wp_animate-ae-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/assets/css/bootstrap.css?ver=5.0.11" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100" id="construction-zone-font-Raleway-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway:300,400,600,700" id="construction-zone-font-opensans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/assets/css/font-awesome.css?ver=5.0.11" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/assets/css/meanmenu.css?ver=5.0.11" id="meanmenu-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/assets/css/nivo-slider.css?ver=5.0.11" id="nivo-slider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-building/style.css?ver=5.0.11" id="construction-zone-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/assets/css/responsive.css?ver=5.0.11" id="responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-zone/style.css?ver=5.0.11" id="construction-building-parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.signsas.com/wp-content/themes/construction-building/css/red.css?ver=5.0.11" id="construction-building-style-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.signsas.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.signsas.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.signsas.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.signsas.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.0.11" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head> <body class="home blog wp-custom-logo"> <div class="wrapper-area"> <!-- Header Area Start Here --> <header> <div class="header-style1-area"> <div class="main-header-area" id="sticker"> <div class="container"> <div class="row"> <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <div class="logo-area"> <a class="custom-logo-link" href="https://www.signsas.com/" itemprop="url" rel="home"><img alt="Servicios Ingenieria Sign SAS" class="custom-logo" height="143" itemprop="logo" src="https://www.signsas.com/wp-content/uploads/2019/01/cropped-si_logo.png" width="224"/></a> </div> </div> <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"> <div class="main-menu-area"> <nav> </nav> </div> </div> </div> </div> </div> <!-- Mobile Menu Area Start --> <div class="mobile-menu-area"> <div class="container"> <div class="row"> <div class="col-md-12"> <div class="mobile-menu"> <nav id="dropdown"> </nav> </div> </div> </div> </div> </div> <!-- Mobile Menu Area End --> </div> </header> <!-- Header Area End Here -->
<div class="header-bennar-area" style="background-image:url('https://www.signsas.com/wp-content/themes/construction-zone/assets/img/banner.jpg')">
<div class="overlay "></div>
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="header-bennar-left">
<h1></h1>
</div>
</div>
</div>
</div>
</div>
<div class="page-news-area section-space-b-less-30">
<div class="container">
<div class="row">
<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
<div class="inner-page-news-area">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="inner-page-news-box">
<div class="inner-news-box-top" id="post-1">
</div>
<div class="inner-news-box-bottom">
<h4><a href="https://www.signsas.com/2019/01/07/hola-mundo/">¡Hola mundo!</a></h4>
<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir!</p>
<a class="btn-read-more" href="https://www.signsas.com/2019/01/07/hola-mundo/">Read More<i aria-hidden="true" class=""></i></a>
</div> </div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
<div class="sidebar">
<aside class="widget widget_search" id="search-2"><form action="https://www.signsas.com/" class="search-form" method="get" role="search">
<div class="search-box">
<h3 class="sidebar-search-title">Search</h3>
<div class="input-group stylish-input-group">
<input class="form-control" name="s" placeholder="Type to search here..." title="Search for:" type="text" value=""/>
<span class="input-group-addon">
<button type="submit">
<span class="glyphicon glyphicon-search"></span>
</button>
</span>
</div>
</div>
</form></aside> <aside class="widget widget_recent_entries" id="recent-posts-2"> <h3 class="widget-title sidebar-title">Entradas recientes</h3> <ul>
<li>
<a href="https://www.signsas.com/2019/01/07/hola-mundo/">¡Hola mundo!</a>
</li>
</ul>
</aside><aside class="widget widget_recent_comments" id="recent-comments-2"><h3 class="widget-title sidebar-title">Comentarios recientes</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://wordpress.org/" rel="external nofollow">Un comentarista de WordPress</a></span> en <a href="https://www.signsas.com/2019/01/07/hola-mundo/#comment-1">¡Hola mundo!</a></li></ul></aside><aside class="widget widget_archive" id="archives-2"><h3 class="widget-title sidebar-title">Archivos</h3> <ul>
<li><a href="https://www.signsas.com/2019/01/">enero 2019</a></li>
</ul>
</aside><aside class="widget widget_categories" id="categories-2"><h3 class="widget-title sidebar-title">Categorías</h3> <ul>
<li class="cat-item cat-item-1"><a href="https://www.signsas.com/category/sin-categoria/">Sin categoría</a>
</li>
</ul>
</aside><aside class="widget widget_meta" id="meta-2"><h3 class="widget-title sidebar-title">Meta</h3> <ul>
<li><a href="https://www.signsas.com/wp-login.php">Acceder</a></li>
<li><a href="https://www.signsas.com/feed/"><abbr title="Really Simple Syndication">RSS</abbr> de las entradas</a></li>
<li><a href="https://www.signsas.com/comments/feed/"><abbr title="Really Simple Syndication">RSS</abbr> de los comentarios</a></li>
<li><a href="https://es.wordpress.org/" title="Funciona gracias a WordPress, una avanzada plataforma de publicación personal semántica.">WordPress.org</a></li> </ul>
</aside> </div>
</div>
</div>
</div>
</div>
<div class="footer-area-bottom">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<script src="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/js/jquery.meanmenu.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/js/wow.min.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/js/jquery.scrollUp.min.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/js/waypoints.min.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/plugins/progress-bar-wp/assets/js/main.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/themes/construction-zone/assets/js/bootstrap.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/themes/construction-zone/assets/js/jquery.meanmenu.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/themes/construction-zone/assets/js/wow.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/themes/construction-zone/assets/js/jquery.scrollUp.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-content/themes/construction-zone/assets/js/main.js?ver=5.0.11" type="text/javascript"></script>
<script src="https://www.signsas.com/wp-includes/js/wp-embed.min.js?ver=5.0.11" type="text/javascript"></script>
<!-- Footer Area End Here -->
</div>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<script>
jQuery('nav#dropdown').meanmenu({
    
	siteLogo: ''+ 
			  '<a href="https://www.signsas.com/" class="custom-logo-link" rel="home" itemprop="url"><img width="224" height="143" src="https://www.signsas.com/wp-content/uploads/2019/01/cropped-si_logo.png" class="custom-logo" alt="Servicios Ingenieria Sign SAS" itemprop="logo" /></a>'+
			  ''

});
</script>
</body>
</html>
