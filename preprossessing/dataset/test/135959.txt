<!DOCTYPE html>
<html lang="en-us" xml:lang="en-us" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Hugo 0.65.3" name="generator"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Mika Tuupola</title>
<meta content="Technology guy gone advertising." name="description"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="https://appelsiini.net/" name="twitter:url"/>
<meta content="https://appelsiini.net/" name="twitter:image"/>
<meta content="Mika Tuupola" name="twitter:title"/>
<meta content="Technology guy gone advertising." name="twitter:description"/>
<meta content="https://appelsiini.net/" property="og:image"/>
<meta content="https://appelsiini.net/" property="og:url"/>
<link href="https://appelsiini.net/css/print.css" media="print" rel="stylesheet" type="text/css"/>
<link href="https://appelsiini.net/css/poole.css" rel="stylesheet" type="text/css"/>
<link href="https://appelsiini.net/css/syntax.css" rel="stylesheet" type="text/css"/>
<link href="https://appelsiini.net/css/hyde.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|PT+Sans:400,400i,700" rel="stylesheet"/>
<link href="/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="/favicon.png" rel="shortcut icon"/>
<link href="https://appelsiini.net/index.xml" rel="alternate" title="Mika Tuupola" type="application/rss+xml"/>
</head>
<body class=" layout-reverse">
<aside class="sidebar">
<div class="container sidebar-sticky">
<div class="sidebar-about">
<a href="https://appelsiini.net/"><h1>Mika Tuupola</h1></a>
<p class="lead">
       Technology guy gone advertising. 
      </p>
</div>
<nav>
<ul class="sidebar-nav">
<li><a href="https://appelsiini.net/">Home</a> </li>
<li><a href="https://github.com/tuupola/"> GitHub </a></li><li><a href="https://www.instagram.com/tuupola/"> Instagram </a></li>
</ul>
</nav>
<p>Copyright © 2020 Mika Tuupola</p>
</div>
</aside>
<main class="content container">
<div class="posts">
<article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2020/programming-gd32v-longan-nano/">Programming the GD32V Longan Nano</a>
</h1>
<time class="post-date" datetime="2020-12-03T00:00:00Z">Thu, Dec 3, 2020</time>
<p><img alt="Plasma on GD32V" src="/img/2020/gd32v-plasma.jpg"/></p>
<p>RISC-V is gaining traction and some development boards have already popped up. One of them is the widely available <a href="https://www.seeedstudio.com/Sipeed-Longan-Nano-RISC-V-GD32VF103CBT6-DEV-Board-p-4725.html">Sipeed Longan Nano</a>. Written information is a bit sparse at the moment. Let’s try to fix this with a quick writeup on wiring and programming the board. If you just want to see what the board can do <a href="https://vimeo.com/486801605">here is a video</a> instead.</p>
<div class="read-more-link">
<a href="/2020/programming-gd32v-longan-nano/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2020/esp32-mjpeg-video-player/">Creating a Videoplayer for ESP32</a>
</h1>
<time class="post-date" datetime="2020-05-25T00:00:00Z">Mon, May 25, 2020</time>
<p><img alt="Big Buck Bunny on ESP32" src="/img/2020/bbb-cover-1.jpg"/></p>
<p>One of the things which makes embedded programming so interesting is that you are constantly dealing with restricted resources. Playing video files is quite resource heavy. Let’s see how ESP32 can handle this. If you have short attention span <a href="https://vimeo.com/409435420">here is a video</a> instead.</p>
<div class="read-more-link">
<a href="/2020/esp32-mjpeg-video-player/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2020/embedded-graphics-library/">Hardware Agnostic Graphics Library for Embedded</a>
</h1>
<time class="post-date" datetime="2020-04-16T00:00:00Z">Thu, Apr 16, 2020</time>
<p><img alt="ESP32 board with fire effect" src="/img/2020/esp-fire.jpg"/></p>
<p>What started as a project to learn C, I have now been using in all my hobby projects. <a href="https://github.com/tuupola/hagl">HAGL</a> is a hardware agnostic graphics library for embedded projects. It supports basic geometric primitives, bitmaps, blitting, fixed width fonts and an optional framebuffer.</p>
<div class="read-more-link">
<a href="/2020/embedded-graphics-library/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2018/esp32-fire-effect/">M5Stack on Fire!</a>
</h1>
<time class="post-date" datetime="2018-05-19T00:00:00Z">Sat, May 19, 2018</time>
<p><img alt="M5Stack fullscreen fire" src="/img/m5stack-fire1-1400.jpg"/></p>
<p>My life has come to a full circle. I remember sitting in my parents basement in the late 90’s programming the <a href="https://www.hanshq.net/fire.html">fire effect</a> using mixture of Borland Turbo Pascal and Assembler. I had an Intel 80386 which also had the 80387 coprocessor. I remember downloading <a href="http://lodev.org/cgtutor/">Lode’s graphics tutorials</a> from FidoNet. It was all magic.</p>
<p>Last week I found myself reading the same tutorials and creating the same fire effect again. Instead of Intel PC, it was written for ESP32 based M5Stack and using C programming language.</p>
<div class="read-more-link">
<a href="/2018/esp32-fire-effect/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2018/calibrate-magnetometer/">How to Calibrate a Magnetometer?</a>
</h1>
<time class="post-date" datetime="2018-02-23T00:00:00Z">Fri, Feb 23, 2018</time>
<p><img alt="MPU-9250 breakout board" src="/img/banggood-mpu9250-breakout.jpg"/></p>
<p>Magnetometers are used to measure the strength of a magnetic field. They can also be used to determine orientation and to compensate gyro drift. Magnetometer provides the last three degrees of freedom in 9DOF sensors.</p>
<p>There is one problem though, magnetometers are prone to distortion.</p>
<div class="read-more-link">
<a href="/2018/calibrate-magnetometer/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2018/micropython-wifi-geolocation/">Wifi Geolocation With MicroPython</a>
</h1>
<time class="post-date" datetime="2018-02-06T00:00:00Z">Tue, Feb 6, 2018</time>
<p><img alt="M5Stack" src="/img/m5stack-map-1400.jpg"/></p>
<p>While evaluating <a href="http://m5stack.com/">M5Stack</a> for a sidehustle project I created a proof of concept which needed to access wifi network, query an API and download an image. Wifi geolocation which displays a static Google map seemed like a perfect fit. Here are some notes about it.</p>
<div class="read-more-link">
<a href="/2018/micropython-wifi-geolocation/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2018/m5stack-esp32-firmware-cli/">Install Latest MicroPython to M5Stack</a>
</h1>
<time class="post-date" datetime="2018-01-27T00:00:00Z">Sat, Jan 27, 2018</time>
<p><a href="http://m5stack.com/">M5Stack</a> is an absolutely beautiful ESP32 based enclosure and development board. It has 320x240 TFT screen, three buttons, sd card slot, Grove I2C connector and can be powered with LiPo battery.</p>
<h2 id="download-the-micropython-firmware">Download the Micropython Firmware</h2>
<p>You could download the MicroPython firmware from the <a href="https://micropython.org/download#esp32">downloads page</a>. But why bother when we have the commandline. First find the download link.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-shell" data-lang="shell">$ curl --silent http://micropython.org/download | grep <span style="color:#e6db74">"firmware/esp32"</span> | gawk -F<span style="color:#e6db74">'&lt;a +href="'</span> -v RS<span style="color:#f92672">=</span><span style="color:#e6db74">'"&gt;'</span> <span style="color:#e6db74">'RT{print $2}'</span>

http://micropython.org/resources/firmware/esp32-20180127-v1.9.3-240-ga275cb0f.bin
</code></pre></div>
<div class="read-more-link">
<a href="/2018/m5stack-esp32-firmware-cli/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2017/wipy-esp32-firmware-cli/">Update Pycom WiPy Firmware From Commandline</a>
</h1>
<time class="post-date" datetime="2017-09-24T00:00:00Z">Sun, Sep 24, 2017</time>
<p><a href="https://pycom.io/product/wipy/">WiPy</a> is a bit pricey but good quality ESP32
development board from <a href="https://pycom.io/">Pycom</a>. I got mine from
<a href="https://shop.pimoroni.com/products/pycom-wipy-2-0">Pimoroni</a>. Pycom does offer
their own firmware updater but for my taste it has too much magic going on. I
prefer to do things from commandline and I stay in control and to see exactly
what update is doing. Quick Googling did not reveal anything so here is my upgrade
procedure.</p>
<h2 id="download-the-latest-pycom-firmware">Download the Latest Pycom Firmware</h2>
<p>For starters we need to find the latest Pycom firmware. Links can be found
from their forums. Or you can just query the filename from their software
update service.</p>
<div class="highlight"><pre style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-text" data-lang="text">$ curl "https://software.pycom.io/findupgrade?key=wipy.wipy%20with%20esp32&amp;redirect=false&amp;type=all"

{
  "key":"wipy.wipy with esp32",
  "file":"https://software.pycom.io/downloads/WiPy-1.7.10.b1.tar.gz",
  "version":"1.8.0.b1",
  "intVersion":71303297,
  "hash":"f6c30bf8b08c50d188f727822be320061ed7847b"
}
</code></pre></div>
<div class="read-more-link">
<a href="/2017/wipy-esp32-firmware-cli/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2017/branca-alternative-to-jwt/">Branca as an Alternative to JWT?</a>
</h1>
<time class="post-date" datetime="2017-08-06T00:00:00Z">Sun, Aug 6, 2017</time>
<p><a href="https://github.com/tuupola/branca-spec">Branca</a> is a catchy name for IETF XChaCha20-Poly1305 AEAD message with an additional version number and timestamp. It is well suited to be used as an authenticated and encrypted API token. <a href="https://github.com/tuupola/branca-spec">Branca specification</a> does not specify the payload format. Among others you can use for example JWT payloads but still have modern encryption and smaller token size provided by Branca.</p>
<p>Currently there are implemenations
for <a href="https://github.com/tuupola/branca-js">JavaScript</a>, <a href="https://github.com/tuupola/branca-elixir">Elixir</a>, <a href="https://github.com/hako/branca">Go</a> and <a href="https://github.com/tuupola/branca-php">PHP</a> and a <a href="https://github.com/tuupola/branca-cli">command line tool</a> for creating and inspecting tokens.</p>
<div class="read-more-link">
<a href="/2017/branca-alternative-to-jwt/">Read More…</a>
</div>
</article><article class="post">
<h1 class="post-title">
<a href="https://appelsiini.net/2017/trilateration-with-n-points/">WiFi Trilateration With Three or More Points</a>
</h1>
<time class="post-date" datetime="2017-05-29T00:00:00Z">Mon, May 29, 2017</time>
<p><a href="/circles/?c=60.1695,24.9354,82175&amp;c=58.3806,26.7251,163311&amp;c=58.3859,24.4971,117932"><img alt="Three circles" src="/img/trilateration-tallinn.jpg"/></a></p>
<p>The popularity of WiFi networks has been rising rapidly during the last 15 years. In urban areas hotspots are ubiquous. These hotspots together with trilateration algorithms provide a cheap way to find out yours or someone elses location.</p>
<p>Because of my previous work with <a href="https://appelsiini.net/2017/reverse-engineering-location-services/">Apple location services</a> I wanted to figure out how trilateration works. Being a school dropout I am really really bad in mathematics. However I consider myself good at trial and error so I reckon this should be doable.</p>
<div class="read-more-link">
<a href="/2017/trilateration-with-n-points/">Read More…</a>
</div>
</article>
</div>
</main>
<script async="" defer="" src="https://cdn.simpleanalytics.io/hello.js"></script>
<noscript><img alt="" src="https://api.simpleanalytics.io/hello.gif"/></noscript>
</body>
</html>
