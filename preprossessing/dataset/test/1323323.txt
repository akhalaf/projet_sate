<!DOCTYPE html>
<html lang="nl">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Pagina niet gevonden – Astrologie met Silva</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.silvadury.nl/feed/" rel="alternate" title="Astrologie met Silva » Feed" type="application/rss+xml"/>
<link href="https://www.silvadury.nl/comments/feed/" rel="alternate" title="Astrologie met Silva » Reactiesfeed" type="application/rss+xml"/>
<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.silvadury.nl\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.silvadury.nl/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet"/>
<link href="https://www.silvadury.nl/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet"/>
<link href="https://www.silvadury.nl/wp-content/themes/generatepress/assets/css/unsemantic-grid.min.css?ver=3.0.2" id="generate-style-grid-css" media="all" rel="stylesheet"/>
<link href="https://www.silvadury.nl/wp-content/themes/generatepress/assets/css/style.min.css?ver=3.0.2" id="generate-style-css" media="all" rel="stylesheet"/>
<style id="generate-style-inline-css">
body{background-color:#efefef;color:#070d33;}a{color:#1e73be;}a:hover, a:focus, a:active{color:#222222;}body .grid-container{max-width:1000px;}.wp-block-group__inner-container{max-width:1000px;margin-left:auto;margin-right:auto;}.generate-back-to-top{font-size:20px;border-radius:3px;position:fixed;bottom:30px;right:30px;line-height:40px;width:40px;text-align:center;z-index:10;transition:opacity 300ms ease-in-out;}body, button, input, select, textarea{font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}body{line-height:1.5;}.entry-content > [class*="wp-block-"]:not(:last-child){margin-bottom:1.5em;}.main-title{font-size:45px;}.main-navigation .main-nav ul ul li a{font-size:14px;}.sidebar .widget, .footer-widgets .widget{font-size:17px;}h1{font-weight:300;font-size:40px;}h2{font-weight:300;font-size:30px;}h3{font-size:20px;}h4{font-size:inherit;}h5{font-size:inherit;}@media (max-width:768px){.main-title{font-size:30px;}h1{font-size:30px;}h2{font-size:25px;}}.top-bar{background-color:#636363;color:#ffffff;}.top-bar a{color:#ffffff;}.top-bar a:hover{color:#303030;}.site-header{background-color:#ffffff;color:#3a3a3a;}.site-header a{color:#3a3a3a;}.main-title a,.main-title a:hover{color:#222222;}.site-description{color:#757575;}.main-navigation,.main-navigation ul ul{background-color:#222222;}.main-navigation .main-nav ul li a,.menu-toggle, .main-navigation .menu-bar-items{color:#ffffff;}.main-navigation .main-nav ul li:hover > a,.main-navigation .main-nav ul li:focus > a, .main-navigation .main-nav ul li.sfHover > a, .main-navigation .menu-bar-item:hover > a, .main-navigation .menu-bar-item.sfHover > a{color:#ffffff;background-color:#3f3f3f;}button.menu-toggle:hover,button.menu-toggle:focus,.main-navigation .mobile-bar-items a,.main-navigation .mobile-bar-items a:hover,.main-navigation .mobile-bar-items a:focus{color:#ffffff;}.main-navigation .main-nav ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation .main-nav ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#3f3f3f;}.navigation-search input[type="search"],.navigation-search input[type="search"]:active, .navigation-search input[type="search"]:focus, .main-navigation .main-nav ul li.search-item.active > a, .main-navigation .menu-bar-items .search-item.active > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation ul ul{background-color:#3f3f3f;}.main-navigation .main-nav ul ul li a{color:#ffffff;}.main-navigation .main-nav ul ul li:hover > a,.main-navigation .main-nav ul ul li:focus > a,.main-navigation .main-nav ul ul li.sfHover > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#4f4f4f;}.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .one-container .container, .separate-containers .paging-navigation, .inside-page-header{color:#222222;background-color:rgba(26,188,156,0.23);}.inside-article a,.paging-navigation a,.comments-area a,.page-header a{color:#202bc9;}.entry-header h1,.page-header h1{color:#1e72bd;}.entry-meta{color:#595959;}.entry-meta a{color:#595959;}.entry-meta a:hover{color:#1e73be;}.sidebar .widget{background-color:rgba(26,188,156,0.23);}.sidebar .widget .widget-title{color:#222222;}.footer-widgets{background-color:#ffffff;}.footer-widgets .widget-title{color:#000000;}.site-info{color:#ffffff;background-color:#222222;}.site-info a{color:#ffffff;}.site-info a:hover{color:#606060;}.footer-bar .widget_nav_menu .current-menu-item a{color:#606060;}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"],input[type="tel"],input[type="number"],textarea,select{color:#666666;background-color:#fafafa;border-color:#cccccc;}input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="number"]:focus,textarea:focus,select:focus{color:#666666;background-color:#ffffff;border-color:#bfbfbf;}button,html input[type="button"],input[type="reset"],input[type="submit"],a.button,a.wp-block-button__link:not(.has-background){color:#ffffff;background-color:#666666;}button:hover,html input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,a.button:hover,button:focus,html input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,a.button:focus,a.wp-block-button__link:not(.has-background):active,a.wp-block-button__link:not(.has-background):focus,a.wp-block-button__link:not(.has-background):hover{color:#ffffff;background-color:#3f3f3f;}a.generate-back-to-top{background-color:rgba( 0,0,0,0.4 );color:#ffffff;}a.generate-back-to-top:hover,a.generate-back-to-top:focus{background-color:rgba( 0,0,0,0.6 );color:#ffffff;}@media (max-width:768px){.main-navigation .menu-bar-item:hover > a, .main-navigation .menu-bar-item.sfHover > a{background:none;color:#ffffff;}}.inside-top-bar{padding:10px;}.inside-header{padding:0px;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-40px;width:calc(100% + 80px);max-width:calc(100% + 80px);}.rtl .menu-item-has-children .dropdown-menu-toggle{padding-left:20px;}.rtl .main-navigation .main-nav ul li.menu-item-has-children > a{padding-right:20px;}.site-info{padding:20px;}@media (max-width:768px){.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content, .inside-page-header, .wp-block-group__inner-container{padding:30px;}.site-info{padding-right:10px;padding-left:10px;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-30px;width:calc(100% + 60px);max-width:calc(100% + 60px);}}.one-container .sidebar .widget{padding:0px;}.navigation-search, .navigation-search input{height:100%;}/* End cached CSS */@media (max-width:768px){.main-navigation .menu-toggle,.main-navigation .mobile-bar-items,.sidebar-nav-mobile:not(#sticky-placeholder){display:block;}.main-navigation ul,.gen-sidebar-nav{display:none;}[class*="nav-float-"] .site-header .inside-header > *{float:none;clear:both;}}
.main-navigation .slideout-toggle a:before,.slide-opened .slideout-overlay .slideout-exit:before{font-family:GeneratePress;}.slideout-navigation .dropdown-menu-toggle:before{content:"\f107" !important;}.slideout-navigation .sfHover > a .dropdown-menu-toggle:before{content:"\f106" !important;}
.slideout-navigation.main-navigation .main-nav ul li a{font-weight:normal;text-transform:none;}
</style>
<link href="https://www.silvadury.nl/wp-content/themes/generatepress/assets/css/mobile.min.css?ver=3.0.2" id="generate-mobile-style-css" media="all" rel="stylesheet"/>
<link href="https://www.silvadury.nl/wp-content/themes/generatepress/assets/css/components/font-icons.min.css?ver=3.0.2" id="generate-font-icons-css" media="all" rel="stylesheet"/>
<link href="https://www.silvadury.nl/wp-content/plugins/gp-premium/blog/functions/css/style-min.css?ver=1.6.2" id="generate-blog-css" media="all" rel="stylesheet"/>
<script id="jquery-core-js" src="https://www.silvadury.nl/wp-includes/js/jquery/jquery.min.js?ver=3.5.1"></script>
<script id="jquery-migrate-js" src="https://www.silvadury.nl/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2"></script>
<link href="https://www.silvadury.nl/wp-json/" rel="https://api.w.org/"/><link href="https://www.silvadury.nl/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.silvadury.nl/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/></head><body><p>
script src='https://www.google.com/recaptcha/api.js'&gt;<meta content="width=device-width, initial-scale=1" name="viewport"/><link href="https://www.silvadury.nl/wp-content/uploads/2018/06/Favicon-512x512-ico-150x150.png" rel="icon" sizes="32x32"/>
<link href="https://www.silvadury.nl/wp-content/uploads/2018/06/Favicon-512x512-ico.png" rel="icon" sizes="192x192"/>
<link href="https://www.silvadury.nl/wp-content/uploads/2018/06/Favicon-512x512-ico.png" rel="apple-touch-icon"/>
<meta content="https://www.silvadury.nl/wp-content/uploads/2018/06/Favicon-512x512-ico.png" name="msapplication-TileImage"/>
</p>
<a class="screen-reader-text skip-link" href="#content" title="Spring naar inhoud">Spring naar inhoud</a> <header class="site-header grid-container grid-parent" id="masthead" itemscope="" itemtype="https://schema.org/WPHeader">
<div class="inside-header">
<div class="site-logo">
<a href="https://www.silvadury.nl/" rel="home" title="Astrologie met Silva">
<img alt="Astrologie met Silva" class="header-image is-logo-image" src="https://www.silvadury.nl/wp-content/uploads/2018/06/SilvaduRy_handlijnkunde_astrologie.jpg" title="Astrologie met Silva"/>
</a>
</div> </div>
</header>
<nav class="main-navigation grid-container grid-parent sub-menu-right" id="site-navigation" itemscope="" itemtype="https://schema.org/SiteNavigationElement">
<div class="inside-navigation grid-container grid-parent">
<button aria-controls="primary-menu" aria-expanded="false" class="menu-toggle">
<span class="mobile-menu">Menu</span> </button>
<div class="main-nav" id="primary-menu">
<ul class="menu sf-menu">
<li class="page_item page-item-17"><a href="https://www.silvadury.nl/">Home</a></li>
<li class="page_item page-item-21 menu-item-has-children"><a href="https://www.silvadury.nl/consulten/">Consulten<span class="dropdown-menu-toggle" role="presentation"></span></a>
<ul class="children">
<li class="page_item page-item-41"><a href="https://www.silvadury.nl/consulten/levensloop-analyse/">Levensloop Analyse</a></li>
<li class="page_item page-item-44"><a href="https://www.silvadury.nl/consulten/hand-analyse/">Hand Analyse</a></li>
<li class="page_item page-item-47 menu-item-has-children"><a href="https://www.silvadury.nl/consulten/astrologie/">Astrologie<span class="dropdown-menu-toggle" role="presentation"></span></a>
<ul class="children">
<li class="page_item page-item-55"><a href="https://www.silvadury.nl/consulten/astrologie/geboorte-horoscoop/">Geboorte Horoscoop</a></li>
<li class="page_item page-item-58"><a href="https://www.silvadury.nl/consulten/astrologie/hedendaagse-horoscoop/">Hedendaagse horoscoop</a></li>
<li class="page_item page-item-60"><a href="https://www.silvadury.nl/consulten/astrologie/spirituele-horoscoop/">Spirituele horoscoop</a></li>
</ul>
</li>
</ul>
</li>
<li class="page_item page-item-24 menu-item-has-children"><a href="https://www.silvadury.nl/cursussen/">Cursussen<span class="dropdown-menu-toggle" role="presentation"></span></a>
<ul class="children">
<li class="page_item page-item-268"><a href="https://www.silvadury.nl/cursussen/cursus-astrologie/">Cursus Astrologie</a></li>
<li class="page_item page-item-278"><a href="https://www.silvadury.nl/cursussen/cursus-handanalyse/">Cursus Handanalyse</a></li>
</ul>
</li>
<li class="page_item page-item-26"><a href="https://www.silvadury.nl/workshops/">Workshops</a></li>
<li class="page_item page-item-29 menu-item-has-children"><a href="https://www.silvadury.nl/boekjes/">Boekjes<span class="dropdown-menu-toggle" role="presentation"></span></a>
<ul class="children">
<li class="page_item page-item-80"><a href="https://www.silvadury.nl/boekjes/emoties/">Emoties</a></li>
<li class="page_item page-item-99"><a href="https://www.silvadury.nl/boekjes/luisteren/">Luisteren</a></li>
<li class="page_item page-item-94"><a href="https://www.silvadury.nl/boekjes/energieen/">Energieën</a></li>
<li class="page_item page-item-152"><a href="https://www.silvadury.nl/boekjes/astrologie/">Astrologie</a></li>
<li class="page_item page-item-106"><a href="https://www.silvadury.nl/boekjes/handlezen/">Handlezen</a></li>
<li class="page_item page-item-157"><a href="https://www.silvadury.nl/boekjes/bewust-zijn/">Bewust Zijn</a></li>
<li class="page_item page-item-548"><a href="https://www.silvadury.nl/boekjes/de-elementen/">De Elementen</a></li>
<li class="page_item page-item-111"><a href="https://www.silvadury.nl/boekjes/spirituele-horoscoop/">Spirituele Horoscoop</a></li>
<li class="page_item page-item-116"><a href="https://www.silvadury.nl/boekjes/aspecten-van-onze-wil/">Aspecten van onze wil</a></li>
<li class="page_item page-item-122"><a href="https://www.silvadury.nl/boekjes/de-stralen-in-de-astrologie/">De Stralen in de Astrologie</a></li>
</ul>
</li>
<li class="page_item page-item-37"><a href="https://www.silvadury.nl/wie-is-silva/">Wie is Silva Du Ry?</a></li>
<li class="page_item page-item-35"><a href="https://www.silvadury.nl/contact/">Contact</a></li>
<li class="page_item page-item-2"><a href="https://www.silvadury.nl/voorbeeld-pagina/">Weetjes</a></li>
</ul>
</div>
</div>
</nav>
<div class="site grid-container container hfeed grid-parent" id="page">
<div class="site-content" id="content">
<div class="content-area grid-parent mobile-grid-100 grid-75 tablet-grid-75" id="primary">
<main class="site-main" id="main">
<div class="inside-article">
<header class="entry-header">
<h1 class="entry-title" itemprop="headline">Deze pagina kon niet worden gevonden.</h1>
</header>
<div class="entry-content" itemprop="text">
<p>Het ziet ernaar uit dat er niets gevonden is op deze locatie. Probeer eens te zoeken?</p><form action="https://www.silvadury.nl/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Zoek naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" title="Zoek naar:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Zoeken"/></form>
</div>
</div>
</main>
</div>
<div class="widget-area sidebar is-right-sidebar grid-25 tablet-grid-25 grid-parent" id="right-sidebar" itemscope="" itemtype="https://schema.org/WPSideBar">
<div class="inside-right-sidebar">
<aside class="widget widget_search" id="search">
<form action="https://www.silvadury.nl/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Zoek naar:</span>
<input class="search-field" name="s" placeholder="Zoeken …" title="Zoek naar:" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Zoeken"/></form>
</aside>
<aside class="widget" id="archives">
<h2 class="widget-title">Archieven</h2>
<ul>
</ul>
</aside>
</div>
</div>
</div>
</div>
<div class="site-footer grid-container grid-parent">
<div class="site footer-widgets" id="footer-widgets">
<div class="footer-widgets-container grid-container grid-parent">
<div class="inside-footer-widgets">
<div class="footer-widget-1 grid-parent grid-50 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_text" id="text-9"> <div class="textwidget"><aside class="widget widget_nav_menu" id="nav_menu-4">
<h1 class="widget-title">Consulten</h1>
<div class="menu-consulten-container">
<ul class="menu" id="menu-consulten">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-671" id="menu-item-671"><a href="https://www.silvadury.nl/consulten/levensloop-analyse/">Consult: levensloopanalyse</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1264" id="menu-item-1264"><a href="https://www.silvadury.nl/consulten/astrologie/geboorte-horoscoop/">Consult: de geboortehoroscoop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1262" id="menu-item-1262"><a href="https://www.silvadury.nl/consulten/astrologie/spirituele-horoscoop/">Consult: spirituele horoscoop</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1263" id="menu-item-1263"><a href="https://www.silvadury.nl/consulten/astrologie/hedendaagse-horoscoop/">Consult: horoscoop van onze groei, het heden</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-670" id="menu-item-670"><a href="https://www.silvadury.nl/consulten/hand-analyse/">Handanalyse “Zijn en Worden”</a></li>
</ul>
</div>
</aside>
<aside class="widget widget_nav_menu" id="nav_menu-8">
<h1 class="widget-title">Cursussen</h1>
<div class="menu-cursussen-container">
<ul class="menu" id="menu-cursussen">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-950" id="menu-item-950"><a href="https://www.silvadury.nl/cursussen/cursus-astrologie/">Cursus astrologie</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-951" id="menu-item-951"><a href="https://www.silvadury.nl/cursussen/cursus-handanalyse/">Cursus handlijnkunde</a></li>
</ul>
<h1></h1>
<h1 class="widget-title">Workshops</h1>
<div class="menu-workshops-container">
<ul class="menu" id="menu-workshops">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-994" id="menu-item-994"><a href="https://www.silvadury.nl/workshops/">Thema workshops</a></li>
</ul>
</div>
</div>
</aside>
</div>
</aside> </div>
<div class="footer-widget-2 grid-parent grid-50 tablet-grid-50 mobile-grid-100">
<aside class="widget inner-padding widget_media_gallery" id="media_gallery-2"><div class="gallery galleryid-0 gallery-columns-1 gallery-size-full" id="gallery-1"><figure class="gallery-item">
<div class="gallery-icon portrait">
<a href="https://www.silvadury.nl/de-spirituele-horoscoop/"><img alt="" class="attachment-full size-full" height="430" loading="lazy" sizes="(max-width: 335px) 100vw, 335px" src="https://www.silvadury.nl/wp-content/uploads/2018/06/de-Spirituele-horoscoop.png" srcset="https://www.silvadury.nl/wp-content/uploads/2018/06/de-Spirituele-horoscoop.png 335w, https://www.silvadury.nl/wp-content/uploads/2018/06/de-Spirituele-horoscoop-234x300.png 234w" width="335"/></a>
</div></figure>
</div>
</aside> </div>
</div>
</div>
</div>
<footer class="site-info" itemscope="" itemtype="https://schema.org/WPFooter">
<div class="inside-site-info grid-container grid-parent">
<div class="copyright-bar">
					2021 © Silva du Ry - Astrologie				</div>
</div>
</footer>
</div>
<a aria-label="Scroll terug naar boven" class="generate-back-to-top" data-scroll-speed="400" data-start-scroll="300" href="#" rel="nofollow" style="opacity:0;visibility:hidden;" title="Scroll terug naar boven">
</a><script id="contact-form-7-js-extra">
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.silvadury.nl\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
</script>
<script id="contact-form-7-js" src="https://www.silvadury.nl/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2"></script>
<!--[if lte IE 11]>
<script src='https://www.silvadury.nl/wp-content/themes/generatepress/assets/js/classList.min.js?ver=3.0.2' id='generate-classlist-js'></script>
<![endif]-->
<script id="generate-main-js-extra">
var generatepressMenu = {"toggleOpenedSubMenus":"1","openSubMenuLabel":"Open het sub-menu","closeSubMenuLabel":"Sub-menu sluiten"};
</script>
<script id="generate-main-js" src="https://www.silvadury.nl/wp-content/themes/generatepress/assets/js/main.min.js?ver=3.0.2"></script>
<script id="generate-back-to-top-js" src="https://www.silvadury.nl/wp-content/themes/generatepress/assets/js/back-to-top.min.js?ver=3.0.2"></script>
<script id="wp-embed-js" src="https://www.silvadury.nl/wp-includes/js/wp-embed.min.js?ver=5.6"></script>
</body></html>
