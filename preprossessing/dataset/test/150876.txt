<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "3984",
      cRay: "610e9f5d4d2f1985",
      cHash: "5e9bf3130d32cf3",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYXJrYW5zYXNzdGF0ZXBhcmtzLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "UtWAhQY2bfzJewjTO3wCZtesLuCtqbT67RZKSXlaEC9xJK84wF1LceVvabXF3E3twxN8TR05M0qJi6KWImWf1+SWPtKxpQ1sGdxg7ya0A+Hq8MWtt8HVeg5EBe+MyLmyRD9UtDg2sKotfwmrs5F58Q3l1cNwrkD3oAw3zcLoRz1f4PrY7BwKXYSbnsdlSxsn1KcgNOZTgZZRbkgcLS/GDvjdi89zZRZ+glEbR8L1h5UXp/TdafuBb99HZTe0GMhFT+NDGs3DAp2IN0QyT2Dz+tzQmWcL5TtSaK2ZYCVuGVxvxGjvfivH+15TWpaXPfJ3pKNEFf3O0seyfGhbcGA+pxqvsHBJPaD4VBjP/7x3IS+2wzGn8NKbTee/WmcAOuiDRj7AY8BS7YOlM2GP8tyAmKtrAQRIjxtfiBw57pL58vLZa2dON1mCn8mxXYY2YIfwnC95WD7fzVGqpRgy46Q6ys7lDFMfBAvGf/BelNgSPPARTN/tbPS0qNb3AqxPEnu0XmLrTeIgOHcl23ghh1SRg57qoZUoW0gQzAkXUNl/YfYrXjW0KFzAWIMMchLRzORycaYSWkoqRAUJXTByuortMtJrp/zxzSU2wJ8ijWh1i1GiiMuyuNybpVrf/kzVaCi27i58hBHS7fRn397s/yf7RQKnvZXEwaRmnDlIQb3TGwiPBFpDUYIB4jV0AQ2fV3U8sZZOgkwJOuhivg05qrfhxLbpdyipyvQaCuNvZBoVHWuRg/TO9Yy7OT5cxTSAII9L",
        t: "MTYxMDUzNTcyMC41MjkwMDA=",
        m: "64L4lLvm8YtKIhK/MxnDGxz2yuz8G6PxyPCuZyeRZZY=",
        i1: "A7aHnY0PFjPMtphYUR+KHw==",
        i2: "PMz/J0XhWrSWqxhsLOkB1Q==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "SKTPJhd1P9u7jixPwDRkB5oZxh/h7YLo+nFS4flb2GI=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=610e9f5d4d2f1985");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> arkansasstateparks.com.</h1>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/?__cf_chl_jschl_tk__=96984e818cec38ab5e827219a5f7950edde2bcdb-1610535720-0-AXu7astypbufZh-OaQC8qPC424SBD9FHz6y98VoHgWa09Re0Wkf34ovSxf1KZtKROmRrDMsHXIcLgiWn79PEBy_pJNKdxT61phh8TTmOlLyrNMIdwtl-OKyuy0FyCu9f-gazm_k67rHBF_8Nk0mp8EXrM_u26xhUJYyH97CroDqvpd6DnjSJ0b2hvvHQmCfec9sUOKOlQ5dkvZB3yMIWjfJeGB068x4fROW0prui3JF7C0B1LyzqB1VawHgEKORuR7M8eSvuj_6s6tekXXGmO4HO0T7q2L6oJRQGJUKEhbTv0CbuN30MqB3iRTmgSN2o6w" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="a0909e7efb93b5e16028134c8376b049bcdfe7c4-1610535720-0-AdLL/cmJHDMbfRF2hAjpNvKwCBEkNjAFw9Sd3Ezh52twtGPWoDVxvRUnv0hiGeGbZzDMFotFTyXPLXZ3tJqip2x+gT9oYcWCZe6fFFsojJbTaNop2XDIcrXp2AFBkHQdLcg2OwoaTv8ZDqI9NOhwm2nmNyQpgXPotkwHS3608fUDwGR0htSxPyjZAOXc/3bZ3MhDPkanJCbz1QpGE/PJZwq7jjbmXf4qSfDr+T9wUmJ+oFBBpM3UOyiJ/UULznZ7whX5Ids5Gvcsk+KgsV33OxPTf6ASGzfnmw91wXoyH61N0kAn7ZjwOhJYkGcXOMLPYLNoZTAguNzt8syQ6C8gI1OvbD9Ny9hIFA6J/GR/tEhXBB/CZVGW8YSQwc+YJAJdYjAKpeOqXl1d6+AERNaGdSozDZqGvn7k/yyU8JuqlZMX5vQ1LJSCXU23QjMsSl7/zOCaVvo4QYswUeT810XpR70/fse1VXxCuf1Y7ed1YgIPnuZ1zvPbwS/mPJ09pQr8v0tGJxOtB3QkbXX97YgALtK0mCwS+zTd1ol2EGGD9pf8v1XZn49gcHjdAV6K40Cc2Jhu624n04Mlg6XUj9LFMxcliwTnngtsrTmwUoQoQvvfEkWq7tkabPa1appTJ1VWE5rtCVnN99WBlRNsDIYv8JH9IT0KkMMrqVgPsYTEChHNwdsAWpSL5qTVsgM/ONC2HoukYmC2BC6CluEjVoU4AhRXtUca6p6Cq+8mWIvYZKF95/SxLltCqd4wpZjia07cXvB4e6gWBVo7wYy5JEdk5OCZKN1GH/3wmZvelssERlnDETWhGAmyxxMQ4GqH3/gJrem93d6RAuWNwQH08wQRGNcf+7pvRT7WVHe79aGTFxNeCb7G74IQg8lX5g8EGSX8N5MMzQ0BPMljGWz5SPVi/nYtmXYJ0KZIpB5M3WLx3stRIq01bgAB07LcmmfI74rE0ZzpZh8nkI3tYa2WtOHJgXELA9x7dQVcE2kW5lsJShzIfoDJnTqFwFVx6hakk8czNQtEOMxc5AFcoz4gjiIZyljJvxZqehym14S9t3Fquodn8aN3fdcLTbUkqxq9qsud0SsmehMCM8By7/tu6kCRx6s54kCMq1MJBrdU9ecSL2aoskl/spgZDW4c7mHoUXP22x0OSqsLkkxuCHIiKE3YivjfHj9Su6e+QZKXUBO8+4HGD7Nor02l31GwMmYv3EL8/t78gLka8/kuTMZIGDJxTWaEDIl5kEHf2UPGRzbz/k1KtSlqWmHKatBSYXAGJ43TsWxyBsORp3IbUbrM+lhRcBJU5rq9CEWHVtt+GU+yxbPNcZXKFim4SrKTUpgpXw445wtTi5pnHHRmXbgZBK5a4yw="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="694334a22045b7f74720e5740e00a429"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610535724.529-cCd11efEFY"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=610e9f5d4d2f1985')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>610e9f5d4d2f1985</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
