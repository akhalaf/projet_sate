<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="" rel="pingback"/>
<title>Page not found – Rainforest Pottery</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.rainforestpottery.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.rainforestpottery.com/wp-content/themes/astrid/css/bootstrap/bootstrap.min.css?ver=1" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-includes/css/dist/block-library/style.min.css?ver=5.1.8" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/tabs-responsive/assets/css/font-awesome/css/font-awesome.min.css?ver=5.1.8" id="wpsm_tabs_r-font-awesome-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/tabs-responsive/assets/css/bootstrap-front.css?ver=5.1.8" id="wpsm_tabs_r_bootstrap-front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/tabs-responsive/assets/css/animate.css?ver=5.1.8" id="wpsm_tabs_r_animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/themes/astrid/style.css?ver=5.1.8" id="astrid-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="astrid-style-inline-css" type="text/css">
.site-header {position: fixed;}
.site-header .container { display: block;}
.site-branding { width: 100%; text-align: center;margin-bottom:15px;padding-top:15px;}
.main-navigation { width: 100%;float: none; clear:both;}
.main-navigation ul { float: none;text-align:center;}
.main-navigation li { float: none; display: inline-block;}
.main-navigation ul ul li { display: block; text-align: left;}
.site-title a,.site-title a:hover { color:#d2b987}
.site-description { color:#BDBDBD}
.site-header,.site-header.header-scrolled { background-color:rgba(32,37,41,0.9)}
@media only screen and (max-width: 1024px) { .site-header.has-header,.site-header.has-video,.site-header.has-single,.site-header.has-shortcode { background-color:rgba(32,37,41,0.9)} }
body, .widget-area .widget, .widget-area .widget a { color:#656D6D}
.footer-widgets, .site-footer, .footer-info { background-color:#202529}
body {font-family: 'Open Sans', sans-serif;}
h1, h2, h3, h4, h5, h6, .fact .fact-number, .fact .fact-name, .site-title {font-family: 'Sumana', serif;}
.site-title { font-size:36px; }
.site-description { font-size:14px; }
h1 { font-size:36px; }
h2 { font-size:30px; }
h3 { font-size:24px; }
h4 { font-size:16px; }
h5 { font-size:14px; }
h6 { font-size:12px; }
body { font-size:14px; }

</style>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C600%2C600italic&amp;ver=5.1.8" id="astrid-body-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Sumana&amp;ver=5.1.8" id="astrid-headings-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/themes/astrid/fonts/font-awesome.min.css?ver=5.1.8" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Sumana%3Aregular%2C700&amp;subset=latin-ext&amp;ver=2.8.1" id="sumana-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=5.1.8" id="open-sans-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.8.1" id="kc-general-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/kingcomposer/assets/css/animate.css?ver=2.8.1" id="kc-animate-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.rainforestpottery.com/wp-content/plugins/kingcomposer/assets/css/icons.css?ver=2.8.1" id="kc-icon-1-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.rainforestpottery.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://www.rainforestpottery.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.rainforestpottery.com/wp-json/" rel="https://api.w.org/"/>
<meta content="WordPress 5.1.8" name="generator"/>
<script type="text/javascript">var kc_script_data={ajax_url:"https://www.rainforestpottery.com/wp-admin/admin-ajax.php"}</script><!-- <meta name="NextGEN" version="3.1.17" /> -->
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #4a0e01; background-image: url("https://www.rainforestpottery.com/wp-content/uploads/2017/04/mugs_t80.png"); background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
<style id="wp-custom-css" type="text/css">
			/*
You can add your own CSS here.

Click the help icon above to learn more.
*/


.site-copyright,
.footer-navigation {
	width: 100%;
	float: right;
	text-align: center;
	font-size: xx-small;
}

/*Facebook icon in footer w/ site title*/
.facebook-icon {
	display:block;
}

.ngg-gallery-thumbnail-box {
float:none !important;
display:inline-block;
} 		</style>
</head>
<body class="error404 custom-background wp-custom-logo kc-css-system hfeed">
<div class="preloader">
<div class="preloader-inner">
<ul><li></li><li></li><li></li><li></li><li></li><li></li></ul>
</div>
</div>
<div class="site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header " id="masthead" role="banner">
<div class="container">
<div class="site-branding col-md-4 col-sm-6 col-xs-12">
<a class="custom-logo-link" href="https://www.rainforestpottery.com/" itemprop="url" rel="home"><img alt="Rainforest Pottery" class="custom-logo" height="43" itemprop="logo" sizes="(max-width: 466px) 100vw, 466px" src="https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80.png" srcset="https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80.png 466w, https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80-300x28.png 300w, https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80-360x33.png 360w, https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80-250x23.png 250w, https://www.rainforestpottery.com/wp-content/uploads/2017/04/logo_black_90_110_80-100x9.png 100w" width="466"/></a> </div>
<div class="btn-menu col-md-8 col-sm-6 col-xs-12"><i class="fa fa-navicon"></i></div>
<nav class="main-navigation col-md-8 col-sm-6 col-xs-12" id="mainnav" role="navigation">
<div class="menu-navmenu-container"><ul class="menu" id="primary-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-64" id="menu-item-64"><a href="http://www.rainforestpottery.com">Rainforest Pottery</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65" id="menu-item-65"><a href="https://www.rainforestpottery.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-760" id="menu-item-760"><a href="https://www.rainforestpottery.com/class-photo/">Class Photos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66" id="menu-item-66"><a href="https://www.rainforestpottery.com/pottery-classes/">Pottery Classes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565" id="menu-item-565"><a href="https://www.rainforestpottery.com/recent-work/">Recent Work</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-442" id="menu-item-442"><a href="https://www.rainforestpottery.com/videos/">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75" id="menu-item-75"><a href="https://www.rainforestpottery.com/contact-us/">Contact Us</a></li>
</ul></div> </nav><!-- #site-navigation -->
</div>
</header><!-- #masthead -->
<div class="header-clone"></div>
<div class="site-content" id="content">
<div class="container">
<div class="content-area" id="primary">
<main class="site-main" id="main" role="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">Oops! That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
<form action="https://www.rainforestpottery.com/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Search …" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Search"/>
</form>
<div class="widget widget_archive"><h2 class="widgettitle">Archives</h2><p>Try looking in the monthly archives. 🙂</p> <label class="screen-reader-text" for="archives-dropdown--1">Archives</label>
<select id="archives-dropdown--1" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
<option value="">Select Month</option>
</select>
</div>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div>
</div><!-- #content -->
<div class="footer-wrapper">
<div class="footer-info">
<div class="container">
<!-- The function astrid_footer_branding has been modified for RFP!!!!!!!! -->
<div class="footer-branding" style="height:100%; padding:20px;"><div class="footer-contact-block" style="width:100%; height:55%;"><a class="facebook-icon" href="https://www.facebook.com/rainforestpottery"><i class="fa fa-facebook"></i></a><span><a href="https://www.facebook.com/rainforestpottery">Rainforest Pottery</a></span></div></div> <div class="footer-contact" style="padding-top:20px;"><div class="footer-contact-block"><i class="fa fa-home"></i><span>504 Church Road, Parksville, BC</span></div><div class="footer-contact-block"><i class="fa fa-envelope"></i><span><a href="mailto:info@rainforestpottery.com">info@rainforestpottery.com</a></span></div><div class="footer-contact-block"><i class="fa fa-phone"></i><span>604.823.6544</span></div></div> </div>
</div>
<footer class="site-footer" id="colophon" role="contentinfo">
<div class="site-info container">
<nav class="footer-navigation" id="footernav" role="navigation">
<div class="menu" id="footer-menu"><ul>
<li class="page_item page-item-44"><a href="https://www.rainforestpottery.com/about/">About</a></li>
<li class="page_item page-item-585"><a href="https://www.rainforestpottery.com/class-photo/">Class Photos</a></li>
<li class="page_item page-item-59"><a href="https://www.rainforestpottery.com/contact-us/">Contact Us</a></li>
<li class="page_item page-item-46"><a href="https://www.rainforestpottery.com/pottery-classes/">Pottery Classes</a></li>
<li class="page_item page-item-41"><a href="https://www.rainforestpottery.com/">Rainforest Pottery</a></li>
<li class="page_item page-item-495"><a href="https://www.rainforestpottery.com/recent-work/">Recent Work</a></li>
<li class="page_item page-item-429"><a href="https://www.rainforestpottery.com/videos/">Videos</a></li>
</ul></div>
</nav><!-- #site-navigation -->
<div class="site-copyright">
					             Copyright © 2017 Rainforest Pottery.				</div>
</div><!-- .site-info -->
</footer><!-- #colophon -->
</div>
</div><!-- #page -->
<!-- ngg_resource_manager_marker --><script src="https://www.rainforestpottery.com/wp-content/plugins/tabs-responsive/assets/js/bootstrap.js?ver=5.1.8" type="text/javascript"></script>
<script src="https://www.rainforestpottery.com/wp-content/themes/astrid/js/main.js?ver=5.1.8" type="text/javascript"></script>
<script src="https://www.rainforestpottery.com/wp-content/themes/astrid/js/scripts.min.js?ver=5.1.8" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.rainforestpottery.com/wp-content/themes/astrid/js/html5shiv.js?ver=5.1.8'></script>
<![endif]-->
<script src="https://www.rainforestpottery.com/wp-content/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.8.1" type="text/javascript"></script>
<script src="https://www.rainforestpottery.com/wp-includes/js/wp-embed.min.js?ver=5.1.8" type="text/javascript"></script>
</body>
</html>
