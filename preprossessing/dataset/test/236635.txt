<html>
<head>
<title>Name The Train contest announced for Amtrak St. Louis to Kansas City service | Brotherhood of Locomotive Engineers and Trainmen</title>
<meta content="(Amtrak and the Missouri Department of Transportation issued the following on November 10.)

ST. LOUIS  A celebration of 30 years of state-sup" name="description"/>
<meta content="Name The Train contest announced for Amtrak St. Louis to Kansas City service" property="og:title"/>
<meta content="https://www.ble-t.org/pr/news/logo.jpg" property="og:image"/>
<meta content="http://www.ble-t.org/pr/news/headline.asp?id=24272" property="og:url"/>
<meta content="(Amtrak and the Missouri Department of Transportation issued the following on November 10.)

ST. LOUIS  A celebration of 30 years of state-sup" property="og:description"/>
<meta content="article" property="og:type"/>
<meta content="https://www.facebook.com/BLETNational" property="article:publisher"/>
<link href="/css/style.css" rel="stylesheet"/>
<!--[if lt IE 9]>
	<link rel="stylesheet" href="/css/ie7.css">
	<![endif]-->
</head>
<style>
@import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700);
.input {
	font-family: arial,verdana,arial,sans-serif,monospace,serif;
	font-size: 7pt;
	font-weight: bold;
}
.input2 {
	font-family: arial,verdana,arial,sans-serif,monospace,serif;
	font-size: 8pt;
	font-weight: bold;
}
h1 {color: #009;font-size:15px;font-weight:bold;}
.menuformat {
font-family:Helvetica, Arial, Sans-Serif;
font-size:10px;
 }

</style>
<body alink="#0000FF" bgcolor="#000033" leftmargin="0" link="#0000CC" marginheight="0" marginwidth="0" topmargin="0" vlink="#0000CC">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr>
<td>
<img height="21" src="/images/graphics_01.png" width="168"/></td>
<td>
<img height="21" src="/images/graphics_02.gif" width="3"/></td>
<td bgcolor="#000036" width="599">
<p style="padding:0;margin:0 0 0 13px;font-family:Helvetia, Arial, Sans-Serif;font-size:13px;color:white;">7061 East Pleasant Valley Road, Independence, Ohio 44131  (216) 241-2630 / Fax: (216) 241-6516</p>
</td>
</tr><tr>
<td>
<img height="86" src="/images/graphics_05.png" width="168"/></td>
<td>
<img height="86" src="/images/graphics_06.gif" width="3"/></td>
<td>
<img height="86" src="/images/graphics_07.gif" width="599"/></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr>
<!-- BEGIN SIDEMENU CELL -->
<td bgcolor="#ffffcc" valign="top" width="168">
<img height="47" src="/images/graphics_08.png" with="168"/>
<font face="verdana,arial,helvetica" size="1">
</font><center>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:15px;line-height:24px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><b><a href="/contact.asp">Contact BLET</a></b><br/>
<b><a href="/default.asp">BLET Main Page</a></b><br/>
<b><a href="http://www.teamster.org">IBT Main Page</a></b><br/>
<b><a href="http://teamster.org/divisions/rail-conference">IBT Rail Conference</a></b><br/>
<b><a href="http://www.teamstersrail.ca">TCRC Main Page</a></b><br/>
<b><a href="/memorial.asp">BLET Memorial Page</a></b></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:18px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Membership</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="https://www.ble-t.org/members"><strong>BLET Members Area</strong></a><br/>
<a href="/application.asp"><strong>APPLICATION</strong></a><br/>
<a href="/disaster">Disaster Relief</a><br/>
</p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:18px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Benefits</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;">
<a href="http://www.railroadersleep.org" target="_blank">RailroaderSleep.org</a><br/>
<a href="/express-scripts">Express Scripts Formulary</a><br/>
<a href="http://www.fa.ml.com/burns_nowakowski" target="_blank">401(k) Plan w / Merrill Lynch</a><br/>
<a href="https://www.benefits.ml.com" target="_blank">401(k) ML Benefits Online</a><br/>
<a href="/shortterm">Sun Life Short-Term Disability</a><br/>
<a href="/nsltd">NS Short-Term Disability</a><br/>
<a href="/pr/news/newsflash.asp?id=4204">Proof of Disability Form</a><br/>
<a href="http://www.wabashcannonball.org" target="_blank">Wabash Mem. Hospital Assn.</a><br/>
<a href="https://ironroadhealthcare.com" target="_blank"><span style="font-size:11px;">Iron Road Healthcare (UPREHS)</span></a><br/>
<a href="/pdf/Fringe-Benefits-2020.pdf" target="_blank">Fringe Benefits 2020 (PDF)</a><br/>
<a href="/pdf/Form-G-34_2020_0.pdf" target="_blank">RRB Reminders 2020 (PDF)</a><br/>
<a href="https://www.yourtracktohealth.com" target="_blank">Your Track to Health</a><br/>
<a href="http://www.liveandworkwell.com" target="_blank">United Behavioral Health</a><br/>
<a href="http://www.carehealthplan.com" target="_blank">CARE Hospital Association</a><br/>
</p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:18px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>News and Issues</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="2"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"> <a href="/pr/newsflash.asp">News Flash</a><br/>
<a href="/pr/headlines.asp">Daily Headlines</a><br/>
<a href="/pr/newsletter">Newsletter</a><br/>
<a href="/pr/journal">Journal</a><br/>
<a href="/security">High Alert: Rail Security News</a><br/>
<a href="/ryanbudget">Ryan Budget</a><br/>
<a href="/highspeedrail">High Speed Rail</a></p>
</td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:18px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Departments</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/organizing">Organizing</a><br/>
<a href="/departments/et">Education &amp; Training</a><br/>
<a href="http://www.bletauxiliary.net">BLET Auxiliary</a><br/>
<a href="/">BLET Washington</a></p>
</td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Information</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/about.asp">About the BLET</a><br/>
<a href="/people/officers">Officers</a><br/>
<a href="/people/staff">Staff</a><br/>
<a href="/info/engineer.asp">Becoming an Engineer</a></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Secretary-Treasurer</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158">
<p style="padding:0;margin:5px 0 5px 0;"><a href="https://trustee.ble-t.org">Division Trustee Reporting</a><br/>
<a href="https://trustee.ble-t.org/gcalogin.aspx">GCA Trustee Reporting</a><br/>
<a href="https://trustee.ble-t.org/slblogin.aspx">SLB Trustee Reporting</a></p>
</td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Merchandise</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="http://www.bletmerchandise.com">Merchandise</a><br/>
<a href="/pdf/BLET-Licensed-List_9-12-19.pdf" target="_blank">Vendor List (PDF)</a><br/>
<a href="http://shop.jostens.com/customer.asp?CID=272721&amp;cmpgn=1939">BLET / GIA Rings</a><br/>
</p>
</td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Communications</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/pr">Public Relations</a><br/>
<a href="/pr/journal">Journal</a><br/>
<a href="/pr/news/newsflash.asp?id=11293">Scholarships</a><br/>
<!--<a href="/pr/history">History</a></p>--></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>FELA</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/fela">What is FELA?</a></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Events</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;">
<a href="http://bletregionals.org">Regional Meetings</a><br/>
<a href="http://railworkertrainingprogram.org" target="_blank">Hazmat Training</a></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>Links</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/links/ble.asp">BLET Affiliates</a><br/>
<a href="/links/rail.asp">Railroad Companies</a><br/>
<a href="/links/labor.asp">Labor Unions</a><br/>
<a href="/links/government.asp">Government/Research</a><br/>
<a href="/links/retirement.asp">Retirement</a></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" bgcolor="#3333CC" style="font-family: 'PT Sans', sans-serif;font-size:17px;padding:4px 0;" width="158"><font color="#FFFFFF"> <b>User Info</b> </font></td>
<td bgcolor="#3333CC"><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="right" class="menuformat" style="font-family: 'PT Sans', sans-serif;font-size:12px;line-height:20px;" width="158"><p style="padding:0;margin:5px 0 5px 0;"><a href="/policy.asp">Web Policy</a></p></td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
<tr>
<td><img height="1" src="/images/spacer.gif" width="3"/></td>
<td align="left" class="menuformat" width="158"><img align="left" height="19" src="/images/spacer.gif" width="4"/> </td>
<td><img height="1" src="/images/spacer.gif" width="7"/></td>
</tr>
</table>
</center></td>
<!-- END SIDEMENU CELL -->
<!-- BEGIN SPACER CELL -->
<td bgcolor="#3333CC" width="3"><img height="1" src="/images/spacer.gif" width="3"/></td>
<!-- END SPACER CELL -->
<!-- BEGIN MAIN CELL -->
<td bgcolor="#FFFFFF" valign="top">
<p>
<img height="1" src="/images/spacer.gif" width="408"/></p><p>
</p><table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img height="1" src="/images/spacer.gif" width="18"/></td>
<td valign="top">
<font face="verdana,arial,helvetica" size="2">
<div class="social">
<p style="font-size:8px;font-family:optima;margin:0;padding:0;color:#777;">SHARE HEADLINE</p>
<div style="float:left;">
<a class="btn-facebook" href="http://www.facebook.com/sharer/sharer.php?u=http://www.ble-t.org/pr/news/headline.asp?id=24272" target="_blank" title="Share on Facebook"><i class="icon-facebook"></i></a>
<a class="btn-tweet" href="https://twitter.com/intent/tweet?text=Name The Train contest announced for Amtrak St. Louis to Kansas City service&amp;url=http://www.ble-t.org/pr/news/headline.asp?id=24272" target="_blank" title="Tweet This"><i class="icon-twitter"></i></a>
<a class="btn-google" href="https://plus.google.com/share?url=http://www.ble-t.org/pr/news/headline.asp?id=24272" target="_blank" title="Share on Google+"><i class="icon-google-plus"></i></a>
<a class="btn-mail" href="https://www.ble-t.org/pr/news/e_headline.asp?id=24272" target="_blank" title="Email Headline"><i class="icon-envelop"></i></a>
</div>
<div style="float:right;margin-right:30px;"><a class="btn-print" href="https://www.ble-t.org/pr/news/pf_headline.asp?id=24272" target="_blank" title="Print Headline"><i class="icon-print"></i></a></div>
</div>
<div class="social-ie">
<p style="font-size:8px;font-family:optima;margin:0;padding:0;color:#777;">SHARE HEADLINE</p>
<div style="float:left;">
<a class="btn-facebook" href="http://www.facebook.com/sharer/sharer.php?u=http://www.ble-t.org/pr/news/headline.asp?id=24272" style="margin-right:5px;" target="_blank" title="Share on Facebook">Facebook</a>
<a class="btn-tweet" href="https://twitter.com/intent/tweet?text=Name The Train contest announced for Amtrak St. Louis to Kansas City service&amp;url=http://www.ble-t.org/pr/news/headline.asp?id=24272" style="margin-right:5px;" target="_blank" title="Tweet This">Twitter</a>
<a class="btn-google" href="https://plus.google.com/share?url=http://www.ble-t.org/pr/news/headline.asp?id=24272" style="margin-right:5px;" target="_blank" title="Share on Google+">Google+</a>
<a class="btn-mail" href="https://www.ble-t.org/pr/news/e_headline.asp?id=24272" style="margin-right:5px;" target="_blank" title="Email Headline">Email</a>
</div>
<div style="float:right;margin-right:30px;"><a class="btn-print" href="https://www.ble-t.org/pr/news/pf_headline.asp?id=24272" target="_blank" title="Print Headline">Print</a></div>
</div>
<div style="clear:both;padding-top:10px;">
<b><font face="arial,helvetica" size="+1">Name The Train contest announced for Amtrak St. Louis to Kansas City service</font></b>
<p>

(Amtrak and the Missouri Department of Transportation issued the following on November 10.)<br/><br/>ST. LOUIS  A celebration of 30 years of state-supported passenger rail across Missouri, as well as major service improvements in the works, are the reasons the Missouri Department of Transportation and Amtrak® are pleased to announce a contest to give the trains a brand-new name.  The Name the Train contest will rely on train fans everywhere to first suggest and then select the best brand-name for the service between St. Louis and Kansas City.<br/><br/>There will be three phases to the contest.  First, submit your favorite name between Nov. 10 and Dec. 10. Contest entries may be submitted online at www.morail.org; mailed to MoDOT, Name the Train Contest, P.O. Box 270, Jefferson City, MO 65102; or hand-delivered to any MoDOT district office (locations at modot.org).  Contest rules are posted at the morail.org website.<br/><br/>Contest judges will select five names as finalists. Between, Dec. 16, 2008 and Jan. 16, 2009, voting will be conducted online or by a post card indicating your favorite finalist name.  The new name will be announced by January 30, 2009.<br/><br/>The contest is open to anyone, but you must be a Missouri resident to win a prize.  Five finalists will receive two round-trip coach tickets to any Amtrak destination in Missouri and a gift basket from one of five participating cities located on passenger rail line. The grand-prize winner will receive two round-trip sleeping car tickets to any Amtrak destination in the U.S.<br/><br/>Since 1979, Amtrak has provided state-supported passenger rail service between St. Louis and Kansas City.  Amtrak runs two round trips daily. The trains carry tens of thousands of passengers a year with stops in St. Louis, Kirkwood, Washington, Hermann, Jefferson City, Sedalia, Warrensburg, Lees Summit, Independence and Kansas City.<br/><br/>With major track improvements to improve on-time service planned next spring, 2009 will be exciting for rail service in our state. It will be great to start it off with a new identity for our trains, said MoDOT Railroads Administrator Rod Massman.  Were looking for something that reminds folks of Missouris great tradition of rail service, as well as the excitement and adventure of train travel.  Im looking forward to seeing what people come up with.<br/><br/>Missouris cross-state passenger trains already have names, but theyre little used and unknown to most people.  One round-trip is called Missouri Service, formerly named the Ann Rutledge  a leftover from that trains Illinois origins.  The other round-trip is called the Mules.  Renaming the trains will create a single brand for the service, a practice that is common on other Amtrak corridors.<br/><br/>This is a great time for Amtrak service in Missouri, with a new station opening in St. Louis, renovations at Sedalia and other upcoming rail improvements that will improve the quality of the service we provide, said Anne McGinnis, Amtrak Marketing, St. Louis.  The Name the Train contest" is a way for the citizens of the state of Missouri to participate in shaping our future.<br/>
</p><p><b>
Wednesday, November 12, 2008<br/>
</b></p><p style="font-size:16px;line-height:21px;">Like us on Facebook at<br/><strong><a href="https://www.facebook.com/BLETNational" target="_blank">Facebook.com/BLETNational</a></strong></p>
<p style="font-size:16px;line-height:21px;"><strong><a href="/newsflash">Sign up for BLET News Flash Alerts</a></strong></p>
</div>
<p><img height="55" src="/images/spacer.gif" width="1"/></p><p>
<font size="1">
</font></p></font><center>© 1997-2021 Brotherhood of Locomotive Engineers and Trainmen<p>
  

</p></center></td></tr></table>
</td>
<!-- END MAIN CELL -->
<!-- BEGIN SPACER CELL -->
<td bgcolor="#FFFFFF"><img height="1" src="/images/spacer.gif" width="13"/></td>
<!-- BEGIN SPACER CELL -->
<!-- BEGIN RIGHT MENU CELL --><td bgcolor="#FFFFFF" valign="top">
<img height="1" src="/images/spacer.gif" width="175"/><br/>
<p><a href="http://www.changetowin.org"><img border="0" height="25" src="/changetowin.gif" width="168"/></a></p>
<p style="font-family: 'PT Sans',sans-serif;text-align:center;font-size:14px;"><strong>Decertification Helpline</strong><br/>(216) 694-0240</p>
<p style="font-family: 'PT Sans',sans-serif;text-align:center;font-size:16px;"><strong><a href="/negotiations2019">National Negotiations</a></strong></p>
<a href="/pr/newsletter">
<img height="256" src="/pr/newsletter/newsletter.jpg?v=20200924" style="border:1px solid #000;" width="168"/>
</a>
<p>
<a href="/pr/journal"><img height="48" src="/images/journal.png" style="border:none;" width="168"/></a></p>
<p style="font-family: 'PT Sans',sans-serif;text-align:center;font-size:16px;padding-right:5px;"><strong><a href="/newsflash">Sign up for BLET<br/>News Flash Alerts</a></strong></p>
<p style="font-family: 'PT Sans', sans-serif;font-size:18px;font-weight:700;padding-left:12px;margin-bottom:5px;">DAILY HEADLINES</p>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70204">BLET, SMART-TD ask feds to beef up security on D.C. Amtrak service </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70205">Biden no longer taking Amtrak to D.C. over inauguration security concerns </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70203">AAR: Intermodal double-digit gains continue </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70206">Bradbury acting Secretary of U.S. DOT </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70207">Metra to buy 500 new rail cars </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70208">Towing vessel damages CSX bridge in Louisiana </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70209">The Moynihan Train Halls glorious arrival </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70210">FreightWaves Classics: Exploring the history of U.S. railroads</a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=69169">Railroad Retirement and Unemployment Insurance taxes in 2021 </a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=70186">Unemployment and Sickness Benefit flexibilities under the Railroad Unemployment Insurance Act (RUIA) during the COVID-19 virus outbreak</a></td></tr></table>
<table><tr><td style="padding-top:2px;" valign="top"><font color="#333333" face="arial,helvetica" size="2"></font></td>
<td style="font-family: 'PT Sans', sans-serif;font-size:14px;line-height:18px;padding-bottom:11px;" valign="top"><a href="/pr/news/headline.asp?id=32278">Get the latest labor news from the Teamsters</a></td></tr></table>
<p style="font-family: 'PT Sans', sans-serif;font-size:18px;font-weight:700;padding-left:12px;margin-top:0;"><a href="/pr/headlines.asp">More Headlines</a></p>
<p>
<!--
<form action="/newsemail.asp" method="post" id="form1" name="form1">
<font face="verdana,helvetica,arial" size="1">
<b><font color="#FF0000"></font></b><br>
<strong>Enter your e-mail address to receive BLET news updates.</strong><br>

<input type="text" size="11" name="email" STYLE="font-family: Verdana,Geneva,Arial; font-size: 10pt;"><img src="clear.gif" width="10" height="1"><input type="submit" value="Go" class="input2">
<br><input type="radio" name="what" value="Subscribe"> Subscribe&nbsp;<input type="radio" name="what" value="Unsubscribe"> Unsubscribe
<p>

</form>
-->
</p></td>
<!-- END RIGHT MENU CELL -->
<!-- BEGIN SPACER CELL -->
<td bgcolor="#FFFFFF"><img height="1" src="/images/spacer.gif" width="3"/></td>
<!-- END SPACER CELL -->
</tr></table>
<td>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2965403-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</td></body>
</html>
