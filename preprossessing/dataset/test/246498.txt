<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="telephone=no" name="format-detection"/>
<meta content="Agencia Estatal Boletín Oficial del Estado" name="Description"/>
<title>BOE.es - Agencia Estatal Boletín Oficial del Estado</title>
<link href="/favicon.ico" rel="shortcut icon"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon"/>
<base target="_top"/>
<link href="/estilos/boe.css" rel="stylesheet" type="text/css"/>
<link href="/estilos/home.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 10]>
    <link rel="stylesheet" type="text/css" href="/estilos/boe_ie9.css" />
    <![endif]-->
<!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/estilos/boe_ie8.css" />
    <![endif]-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
</head>
<body>
<div id="header">
<h1 class="fuera">Agencia Estatal Boletín Oficial del Estado</h1>
<ul class="fuera">
<li><a accesskey="c" href="#contenedor" tabindex="-1">Ir a contenido</a></li>
<li><a accesskey="5" href="/diario_boe/" tabindex="-1">Consultar el diario oficial BOE</a></li>
</ul>
<div id="logosInicio">
<span id="logoPresidenciaMovil"><a href="http://www.mpr.es/" title="Ir al Ministerio de la Presidencia"><img alt="Ministerio de la Presidencia" src="/imagenes/logoMPRmovil.png"/></a></span>
<span id="logoPresidencia"><a href="http://www.mpr.es/" title="Ir al Ministerio de la Presidencia"><img alt="Ministerio de la Presidencia" src="/imagenes/logoMPR.png"/></a></span>
<span id="logoAgencia"><a accesskey="1" href="/" title="Ir a la página de inicio"><img alt="Agencia Estatal Boletín Oficial del Estado" src="/imagenes/logoBOE.gif" srcset="/imagenes/logoBOE.svg"/></a></span>
</div> <!-- #logosInicio -->
</div> <!-- #header -->
<div class="banda-menu" id="top">
<div class="menu-wrapper">
<div id="logo-movil-boe-container">
<a href="/" title="Ir a la página de inicio"><img alt="Agencia Estatal Boletín Oficial del Estado" src="/imagenes/logoBlanco128.png"/></a>
</div>
<ul class="menu">
<li class="menu-item menu-idiomas">
<div id="selector-idioma">
<p class="fuera">Puede seleccionar otro idioma:</p>
<input id="activar-idiomas" type="checkbox"/>
<label class="idioma-actual" for="activar-idiomas" title="Haga clic o utilice barra espaciadora para abrir o cerrar opciones"><span class="descripcion-idioma pc tablet">Castellano</span><span class="descripcion-idioma movil">es</span><span class="triangulo"><span></span></span></label>
<ul id="lista-idiomas">
<li><a href="index.php?lang=es" hreflang="es" lang="es" title="Cambiar a español/castellano"><span aria-hidden="true" class="idioma"><abbr title="español/castellano">es</abbr><em>Castellano</em></span></a></li>
<li><a href="index.php?lang=ca" hreflang="ca" lang="ca" title="Canviar a català"><span aria-hidden="true" class="idioma"><abbr title="català">ca</abbr><em>Català</em></span></a></li>
<li><a href="index.php?lang=gl" hreflang="gl" lang="gl" title="Cambiar a galego"><span aria-hidden="true" class="idioma"><abbr title="galego">gl</abbr><em>Galego</em></span></a></li>
<li><a href="index.php?lang=eu" hreflang="eu" lang="eu" title="-ra aldatu euskara"><span aria-hidden="true" class="idioma"><abbr title="euskara">eu</abbr><em>Euskara</em></span></a></li>
<li><a href="index.php?lang=va" hreflang="ca-valencia" lang="ca-valencia" title="Canviar a valencià"><span aria-hidden="true" class="idioma"><abbr title="valencià">va</abbr><em>Valencià</em></span></a></li>
<li><a href="index.php?lang=en" hreflang="en" lang="en" title="Change to English"><span aria-hidden="true" class="idioma"><abbr title="english">en</abbr><em>English</em></span></a></li>
<li><a href="index.php?lang=fr" hreflang="fr" lang="fr" title="Changer français"><span aria-hidden="true" class="idioma"><abbr title="français">fr</abbr><em>Français</em></span></a></li>
</ul>
</div>
</li>
<li class="menu-item resto">
<a accesskey="4" href="/buscar/"><span class="botonBuscar">Buscar</span></a>
</li>
<li class="menu-item resto">
<a href="/mi_boe/">
<span class="botonMiBOE">Mi BOE <span class="luz">Desconectado.<br/>Pulse para acceder al servicio 'Mi BOE'</span></span>
</a>
</li>
<li class="menu-item movil buscar">
<a href="/buscar/">
<img alt="Buscar" src="/imagenes/logoBuscar.png" srcset="/imagenes/logoBuscar.svg"/>
</a>
</li>
<li class="menu-item movil">
<a href="/mi_boe/">
<img alt="Mi BOE" src="/imagenes/logoMiBOE.png" srcset="/imagenes/logoMiBOE.svg"/>
</a>
</li>
<li class="menu-item menu-menu"><!-- -->
<input id="activar-menu" name="activar" title="Desplegar menú" type="checkbox"/>
<label class="click-desplegar resto" for="activar-menu" title="Haga clic o utilice barra espaciadora para abrir o cerrar opciones">Menú
            <span></span>
<span></span>
<span></span>
</label>
<input id="activar-menu-movil" name="activar" title="Desplegar menú" type="checkbox"/>
<label class="click-desplegar movil" for="activar-menu-movil"><em>Menú</em>
<span></span>
<span></span>
<span></span>
</label>
<div class="menu-container">
<ul class="menu-item-list">
<li class="menu-item first">
<p><a class="inline" href="/index.php#diarios">Diarios Oficiales</a></p>
<ul class="sub-menu">
<li><a href="/diario_boe" title="Boletín Oficial del Estado">BOE</a></li>
<li><a href="/diario_borme" title="Boletín Oficial del Registro Mercantil">BORME</a></li>
<li><a href="/legislacion/otros_diarios_oficiales.php">Otros diarios oficiales</a></li>
</ul>
</li>
<li class="menu-item">
<p><a href="/index.php#juridico">Información Jurídica</a></p>
<ul class="sub-menu">
<li><a href="/legislacion/">Todo el Derecho</a></li>
<li><a href="/biblioteca_juridica/">Biblioteca Jurídica Digital</a></li>
</ul>
</li>
<li class="menu-item last">
<p><a href="/index.php#servicios-adicionales">Otros servicios</a></p>
<ul class="sub-menu">
<li><a href="/notificaciones">Notificaciones en BOE</a></li>
<li><a href="https://subastas.boe.es">Portal de subastas</a></li>
<li><a href="/anuncios">Anunciantes</a></li>
</ul>
</li>
</ul>
</div> <!-- .menu-container -->
</li><!-- -->
</ul>
</div> <!-- .menu-wrapper -->
</div> <!-- .banda-menu -->
<div id="contenedor">
<div class="poolHome" id="contenido"> <div class="banner">
<div class="caja_info">
<a href="/biblioteca_juridica/index.php?modo=1&amp;tipo=C"><img alt="Códigos electrónicos Covid-19" src="/imagenes/home/img_codigos_covid.png" title="Códigos electrónicos Covid-19"/></a>
</div>
</div> <div id="contenedor-secundario">
<div class="franja" id="diarios">
<div class="titulo-franja"><h2>Diarios oficiales</h2></div>
<div class="fila triple">
<div class="elemento-doble">
<div class="elemento">
<div class="contenedor-lista">
<img alt="" src="/imagenes/home/boe-01.jpg"/>
<div class="detalle">
<h3 class="subtitulo">
                      	Boletín Oficial del Estado 
                     
                      </h3>
<a class="link-contenedor" href="/diario_boe/" title="Boletín Oficial del Estado"><span class="fuera">Boletín Oficial del Estado</span> </a>
<div class="descripcion">
<ul class="bullet-boe">
<li><a class="link-contenedor-otro" href="/diario_boe/ultimo.php"><span>Último BOE</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/boe.php"><span>Buscar en BOE</span></a></li>
<li><a class="link-contenedor-otro" href="/diario_boe/"><span>Calendario</span></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="elemento">
<div class="contenedor-lista">
<img alt="" src="/imagenes/home/brm-01.jpg"/>
<div class="detalle">
<h3 class="subtitulo">Boletín Oficial del Registro Mercantil</h3>
<a class="link-contenedor" href="/diario_borme/" title="Boletín Oficial del Registro Mercantil">
<span class="fuera">Boletín Oficial del Registro Mercantil</span>
</a>
<div class="descripcion">
<ul class="bullet-boe">
<li><a class="link-contenedor-otro" href="/diario_borme/ultimo.php"><span>Último BORME</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/anborme.php"><span>Buscar en BORME</span></a></li>
<li><a class="link-contenedor-otro" href="/diario_borme/"><span>Calendario</span></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="elemento">
<div class="contenedor-lista">
<img alt="" src="/imagenes/home/europa.jpg"/>
<div class="detalle">
<h3 class="subtitulo">Otros diarios oficiales</h3>
<a class="link-contenedor" href="/legislacion/otros_diarios_oficiales.php" title="Otros diarios oficiales">
<span class="fuera">Otros diarios oficiales</span>
</a>
<div class="descripcion">
<ul class="bullet-boe">
<li><a class="link-contenedor-otro" href="http://eur-lex.europa.eu/JOIndex.do?ihmlang=es" target="_blank" title="Abre una nueva ventana">Unión Europea (<abbr title="Diario Oficial de la Unión Europea">DOUE</abbr>)</a></li>
<li><a class="link-contenedor-otro" href="/legislacion/otros_diarios_oficiales.php#boletines_autonomicos"><span>Boletines autonómicos</span></a></li>
<li><a class="link-contenedor-otro" href="/legislacion/otros_diarios_oficiales.php#boletines_provinciales"><span>Boletines provinciales</span></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="franja juridico" id="juridico">
<div class="titulo-franja"><h2>Información Jurídica</h2></div>
<div class="fila doble">
<div class="elemento" id="todo-el-derecho">
<div class="contenedor-lista">
<span class="pc"><img alt="" src="/imagenes/home/bl-01.jpg"/></span>
<span class="tablet movil"><img alt="" src="/imagenes/home/bl-01_full.jpg"/></span>
<div class="detalle">
<h3 class="subtitulo">Todo el Derecho</h3>
<a class="link-contenedor" href="/legislacion/" title="Todo el Derecho">
<span class="fuera">Todo el Derecho</span>
</a>
<div class="descripcion doble">
<ul class="bullet-boe">
<li><a class="link-contenedor-otro" href="/buscar/legislacion.php"><span>Legislación</span></a></li>
<li><a class="link-contenedor-otro" href="http://hj.tribunalconstitucional.es/" target="_blank"><span>Tribunal Constitucional</span></a></li>
<li><a class="link-contenedor-otro" href="http://www.poderjudicial.es/search/indexAN.jsp" target="_blank"><span>Jurisprudencia (CENDOJ)</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/fiscalia.php"><span>Doctrina de la Fiscalía</span></a></li>
<li><a class="link-contenedor-otro" href="/legislacion/derechos_fundamentales.php" hreflang="es"><span>Derechos fundamentales</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/consejo_estado.php"><span>Dictámenes del Consejo de Estado</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/abogacia_estado.php"><span>Abogacía del Estado</span></a></li>
<li><a class="link-contenedor-otro" href="/legislacion/union_europea.php"><span>Derecho de la Unión Europea</span></a></li>
<li><a class="link-contenedor-otro" href="/buscar/gazeta.php"><span>Colección histórica: Gazeta (1661-1959)</span></a></li>
</ul>
</div>
</div>
<p class="enlaceBuscar"><a class="link-contenedor-otro" href="/buscar">Más búsquedas</a></p>
</div>
<!-- <p class="enlaceBuscar"><a href="/buscar" class="link-contenedor-otro">M&aacute;s b&uacute;squedas</a></p> -->
</div>
<div class="elemento" id="biblioteca-juridica">
<div class="contenedor-lista">
<span class="pc"><img alt="" src="/imagenes/home/bj-01.jpg"/></span>
<span class="tablet movil"><img alt="" src="/imagenes/home/bj-01_full.jpg"/></span>
<div class="detalle">
<h3 class="subtitulo">Biblioteca Jurídica Digital</h3>
<a class="link-contenedor" href="/biblioteca_juridica/" title="Biblioteca Jurídica Digital"><span class="fuera">Biblioteca Jurídica Digital</span>
</a>
<p>Publicaciones jurídicas para descargar<br/>de forma libre y gratuita:</p>
<div class="descripcion">
<ul class="bullet-boe">
<li><a class="link-contenedor-otro" href="/biblioteca_juridica/index.php?tipo=L"><span>Libros jurídicos electrónicos</span></a></li>
<li><a class="link-contenedor-otro" href="/biblioteca_juridica/index.php?tipo=C"><span>Códigos electrónicos</span></a></li>
<li><a class="link-contenedor-otro" href="/biblioteca_juridica/index.php?tipo=U"><span>Códigos electrónicos universitarios</span></a></li>
<li><a class="link-contenedor-otro" href="/biblioteca_juridica/index.php?tipo=R"><span>Anuarios y otras publicaciones</span></a></li>
<li><a class="link-contenedor-otro" href="/biblioteca_juridica/index.php?tipo=E"><span>La Editorial del BOE</span></a></li>
<li><a class="link-contenedor-otro" href="/comercializacion">La Librería del BOE</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="franja" id="servicios-adicionales">
<div class="titulo-franja"><h2>Otros servicios</h2></div>
<div class="fila">
<div class="elemento" id="notificaciones_boe">
<div class="contenedor-lista">
<a href="/notificaciones/">
<img alt="" src="/imagenes/home/teu_270.png" srcset="/imagenes/home/teu_270.svg"/><span class="exh3">Notificaciones en BOE</span>
</a>
</div>
</div>
<div class="elemento" id="subastas">
<div class="contenedor-lista">
<a href="https://subastas.boe.es/" target="_blank">
<img alt="" src="/imagenes/home/subastas_270.png" srcset="/imagenes/home/subastas_270.svg"/><span class="exh3">Portal de Subastas</span>
</a>
</div>
</div>
<div class="elemento" id="anunciantes">
<div class="contenedor-lista">
<a class="servicio-gestion-enlace" href="/anuncios">
<img alt="" src="/imagenes/home/anunciantes.png" srcset="/imagenes/home/anunciantes.svg"/><span class="exh3">Anunciantes</span>
</a>
</div>
</div>
</div>
</div>
</div> <!-- #contenedor-secundario -->
<div class="info-interes gris">
<h2>Información corporativa</h2>
<div>
<ul class="bullet-boe">
<li><a href="/organismo/">La Agencia</a></li>
<li><a href="https://contrataciondelestado.es/wps/poc?uri=deeplink%3AperfilContratante&amp;idBp=FP5CxnKlO20QK2TEfXGy%2BA%3D%3D" target="_blank">Perfil del contratante</a></li>
</ul>
<ul class="bullet-boe">
<li><a href="/organismo/#carta_servicios">Carta de servicios</a></li>
<li><a href="/organismo/#empleo_boe">Empleo en el BOE</a></li>
</ul>
</div>
</div>
</div> <!-- .pool -->
</div> <!-- #contenedor -->
<div id="pie">
<div id="menuPie">
<div class="otros-enlaces">
<div class="grupo-otros-enlaces izquierda">
<div class="enlace"><a accesskey="3" href="/contactar/">Contactar</a></div>
<div class="enlace"><a href="/informacion/index.php" hreflang="es">Sobre esta sede electrónica</a></div>
<div class="enlace"><a accesskey="2" href="/informacion/mapa_web/">Mapa</a></div>
<div class="enlace"><a href="/informacion/aviso_legal/index.php">Aviso legal</a></div>
<div class="enlace"><a accesskey="0" href="/informacion/accesibilidad/">Accesibilidad</a></div>
<div class="enlace"><a href="/informacion/index.php#proteccion-de-datos" hreflang="es">Protección de datos</a></div>
<div class="enlace"><a href="/informacion/tutoriales/" hreflang="es">Tutoriales</a></div>
</div>
<div class="grupo-otros-enlaces derecha">
<div class="enlace icono"><a href="/rss/" hreflang="es" lang="es" title="RSS"><img alt="RSS" src="/imagenes/home/rss_32.png" srcset="/imagenes/home/rss_32.svg"/></a></div>
<div class="enlace icono"><a href="/redes_sociales?pag=tw" hreflang="es" lang="es" title="boegob, el BOE en Twitter"><img alt="Twitter" src="/imagenes/home/twitter_32.png" srcset="/imagenes/home/twitter_32.svg"/></a></div>
<div class="enlace icono"><a href="/redes_sociales?pag=fb" hreflang="es" lang="es" title="El BOE en Facebook"><img alt="Facebook" src="/imagenes/home/facebook_32.png" srcset="/imagenes/home/facebook_32.svg"/></a></div>
<div class="enlace icono"><a href="/redes_sociales?pag=ln" hreflang="es" lang="es" title="El BOE en LinkedIn"><img alt="LinkedIn" src="/imagenes/home/linkedin_32.png" srcset="/imagenes/home/linkedin_32.svg"/></a></div>
<div class="enlace icono"><a href="/redes_sociales?pag=yt" hreflang="es" lang="es" title="El BOE en YouTube"><img alt="YouTube" src="/imagenes/home/youtube_32.png" srcset="/imagenes/home/youtube_32.svg"/></a></div>
</div>
</div> <!-- .franjaMenu -->
</div> <!-- #menuPie -->
<div class="franja-pie">
<p class="nombre-organismo">Agencia Estatal Boletín Oficial del Estado</p>
<p class="direccion-organismo"><abbr title="Avenida">Avda.</abbr> de Manoteras, 54 - 28050 Madrid</p>
</div>
</div> <!-- #pie -->
<script src="/js/desplegable.js"></script>
</body>
</html>
