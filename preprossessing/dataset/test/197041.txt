<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html dir="ltr" lang="de" version="HTML+RDFa 1.1" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:schema="http://schema.org/" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width" name="MobileOptimized"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="true" name="HandheldFriendly"/>
<meta content="Marion Kloskowski" name="copyright"/>
<meta content="Hier finden Sie Bastelanleitungen für Kinder, Mandalas und Malvorlagen, Schritt für Schritt - Anleitungen für Geldgeschenke, Ponpons, Verpackungsideen, kleine Geschenke und vieles mehr zum Basteln und Gestalten." name="description"/>
<meta content="Hier finden Sie Bastelanleitungen für Kinder. Was man mit einfachen Falttechniken alles basteln kann." name="abstract"/>
<meta content="Drupal 7 (https://www.drupal.org)" name="generator"/>
<link href="https://www.basteln-gestalten.de/" rel="canonical"/>
<link href="https://www.basteln-gestalten.de/" rel="shortlink"/>
<meta content="Basteln &amp; Gestalten" property="og:site_name"/>
<meta content="website" property="og:type"/>
<meta content="https://www.basteln-gestalten.de/" property="og:url"/>
<meta content="Basteln &amp; Gestalten" property="og:title"/>
<meta content="summary" name="twitter:card"/>
<meta content="https://www.basteln-gestalten.de/" name="twitter:url"/>
<meta content="Basteln &amp; Gestalten" name="twitter:title"/>
<meta content="Basteln &amp; Gestalten" name="dcterms.title"/>
<meta content="Text" name="dcterms.type"/>
<meta content="text/html" name="dcterms.format"/>
<meta content="https://www.basteln-gestalten.de/" name="dcterms.identifier"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/favicon_1.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-3112105117838818",
          enable_page_level_ads: false
     });
</script>
<script id="phvO03w9" src="https://app.usercentrics.eu/latest/main.js" type="application/javascript"></script>
<script async="true" src="https://cdns.yieldscale.com/ys_f240f49e73f9b9317c80f6b1bd189900.js"></script>
<title>Basteln &amp; Gestalten |</title>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_A3TadNjNH2qjfpwvRovQYgDbDXQ_yDpcZ3duEExhatc.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_ipLI8V7BYoljyHElxA5p_VhN535gxCakkM6YIZmIDXA.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_YudWKH8goq-5nO6mjsVms1QK3jD9ObPZvSbWLFtl4_A.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_eqL1Qyg9hZf2scjCRnWfYVzhBpJOVTNCQUOWKUx14BE.css" media="all and (min-width: 768px) and (max-width: 959px)" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css__7e_jkzJUuScgQGQ5PgQQY-Kx9cb3-p7TLaJ-V7cJBU.css" media="all and (min-width: 480px) and (max-width: 767px)" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_7I8-G2uMwASUBexAjRve20Ij4oEwrGssy8CdeqPCWXw.css" media="all and (max-width: 479px)" rel="stylesheet" type="text/css"/>
<link href="https://www.basteln-gestalten.de/files/bastelngestalten/css/css_bd5Di5NUB1qJInTUgzm6-Y0LKve4BfeHskk3idF8rPc.css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="https://www.basteln-gestalten.de/files/bastelngestalten/js/js_VecHkdFFzHmI10lNWW0NMmhQ47_3u8gBu9iBjil2vAY.js" type="text/javascript"></script>
<script src="https://www.basteln-gestalten.de/files/bastelngestalten/js/js_19BAJ6A-0DMAk8T3iX659jM6PhOOY0CmOFlelVPO3Nk.js" type="text/javascript"></script>
<script src="https://www.basteln-gestalten.de/files/bastelngestalten/js/js_hTK2qrjwK89_1bG4Xl51QNIBHn6f1Th_Kzmiux7EKZw.js" type="text/javascript"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","simplecorp":{"responsive_menu_topoptiontext":"Seite ausw\u00e4hlen"},"ajaxPageState":{"theme":"simplecorp","theme_token":"T3BjsnLNXFfI_CifQRPUDOeM1LuY7VjFrCd1XZmdBTw","js":{"0":1,"1":1,"2":1,"3":1,"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery-html-prefilter-3.5.0-backport.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/jquery.cookie.js":1,"misc\/jquery.form.js":1,"misc\/ajax.js":1,"public:\/\/languages\/de_WIcVQnL1QOjMP8wrpjLjU5xFNS8UgXmvfp0Krr1exO8.js":1,"sites\/all\/themes\/simplecorp\/js\/plugins\/jquery.flexslider-min.js":1,"sites\/all\/themes\/simplecorp\/js\/plugins\/jquery.mobilemenu.min.js":1,"sites\/all\/modules\/field_group\/field_group.js":1,"misc\/progress.js":1,"sites\/all\/modules\/fivestar\/js\/fivestar.ajax.js":1,"sites\/all\/modules\/fivestar\/js\/fivestar.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/comment_notify\/comment_notify.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/themes\/simplecorp\/css\/shortcodes\/buttons.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/fivestar\/css\/fivestar.css":1,"sites\/all\/modules\/fivestar\/widgets\/oxygen\/oxygen.css":1,"sites\/all\/themes\/simplecorp\/css\/main-css.css":1,"sites\/all\/themes\/simplecorp\/css\/normalize.css":1,"sites\/all\/themes\/simplecorp\/css\/plugins\/flexslider.css":1,"sites\/all\/themes\/simplecorp\/css\/local.css":1,"sites\/all\/themes\/simplecorp\/css\/768.css":1,"sites\/all\/themes\/simplecorp\/css\/480.css":1,"sites\/all\/themes\/simplecorp\/css\/320.css":1,"sites\/all\/themes\/simplecorp\/css\/shortcodes\/columns.css":1,"sites\/all\/themes\/simplecorp\/css\/color-schemes\/light-yellow\/styles.css":1}},"field_group":{"div":"full"},"ajax":{"edit-vote--2":{"callback":"fivestar_ajax_submit","event":"change","url":"\/system\/ajax","submit":{"_triggering_element_name":"vote"}}},"urlIsAjaxTrusted":{"\/system\/ajax":true,"\/":true}});
//--><!]]>
</script>
</head>
<body class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-2 node-type-story custom-background">
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content">Direkt zum Inhalt</a>
</div>
<!-- #page-wrapper -->
<div id="page-wrapper">
<!-- #page -->
<div id="page">
<!-- header -->
<header class="container clearfix" role="header">
<!-- #pre-header -->
<div class="clearfix" id="pre-header">
<div class="region region-header">
<div class="block block-block" id="block-block-54">
<div class="content">
<!-- Go to www.addthis.com/dashboard to customize your tools --><script async="async" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547dcb2e719401fd" type="text/javascript"></script> </div>
</div>
<div class="block block-block" id="block-block-48">
<div class="content">
<a href="/"><img alt="Basteln und Gestalten" src="/files/bastelngestalten/header-basteln-gestalten.png"/></a>
</div>
</div>
</div>
</div>
<!-- EOF: #pre-header -->
<!-- #header -->
<div class="clearfix" id="header">
<!-- #header-right -->
<div id="header-right">
<!-- #navigation-wrapper -->
<div class="clearfix" id="navigation-wrapper">
<!-- #main-navigation -->
<nav class="main-menu clearfix" id="main-navigation" role="navigation">
<div class="region region-navigation">
<div class="block block-menu" id="block-menu-menu-primary-menue">
<div class="content">
<ul class="menu"><li class="first expanded"><a href="http://www.basteln-gestalten.de/basteln-papier" title="">Papier-Welt</a><ul class="menu"><li class="first leaf"><a href="http://www.basteln-gestalten.de/basteln-papier" title="">Papier 1</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/papier-basteln" title="">Papier 2</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/basteln-ideen" title="">Papier 3</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/tiere-basteln" title="">Tiere 1</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/tiere-falten" title="">Tiere 2</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/krepppapier-basteln" title="">Krepppapier</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/servietten-basteln" title="">Servietten</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/basteln-tonpapier" title="">Tonpapier</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/buchstaben-basteln" title="">Buchstaben</a></li>
<li class="last leaf"><a href="http://www.basteln-gestalten.de/spielzeug-basteln" title="">Spielzeug aus Papier</a></li>
</ul></li>
<li class="expanded"><a href="http://www.basteln-gestalten.de/basteln-mit-wolle" title="">Wolle &amp; mehr</a><ul class="menu"><li class="first leaf"><a href="http://www.basteln-gestalten.de/basteln-mit-wolle" title="">Wolle 1</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/tiere-aus-wolle" title="">Wolle 2</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/basteln-mit-filz" title="">Filz</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/basteln-naturmaterialien" title="">Naturmaterialien</a></li>
<li class="last leaf"><a href="http://www.basteln-gestalten.de/spielzeug-selber-basteln" title="">Spielzeug aus Wolle ...</a></li>
</ul></li>
<li class="expanded"><a href="http://www.basteln-gestalten.de/geldgeschenke" title="">Geld-Geschenke</a><ul class="menu"><li class="first leaf"><a href="http://www.basteln-gestalten.de/geldgeschenke" title="">Geldgeschenke 1</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/geldgeschenkideen" title="">Geldgeschenke 2</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/geldgeschenke-2" title="">Geldgeschenke 3</a></li>
<li class="leaf"><a href="http://www.basteln-gestalten.de/geschenke-basteln" title="">Geschenke basteln</a></li>
<li class="last leaf"><a href="http://www.basteln-gestalten.de/originell-verpacken" title="">Geschenke verpacken</a></li>
</ul></li>
<li class="expanded"><a href="http://www.basteln-gestalten.de/osterbastelei" title="">Feste &amp; Jahreszeiten</a><ul class="menu"><li class="first leaf"><a href="/osterbastelei">Frühling / Ostern 1</a></li>
<li class="leaf"><a href="/ostern-basteln" title="">Frühling / Ostern 2</a></li>
<li class="leaf"><a href="/sommer-basteln">Sommer-Ferien</a></li>
<li class="leaf"><a href="/basteln-zu-halloween">Herbst / Halloween</a></li>
<li class="leaf"><a href="/weihnachtsbastelei">Winter / Weihnachten 1</a></li>
<li class="leaf"><a href="/bastelideen-weihnachten">Winter / Weihnachten 2</a></li>
<li class="leaf"><a href="/basteln-weihnachten">Winter / Weihnachten 3</a></li>
<li class="leaf"><a href="/kindergeburtstag-ideen">Kindergeburtstag</a></li>
<li class="last leaf"><a href="/kinderfasching-basteln">Fasching / Verkleiden</a></li>
</ul></li>
<li class="last expanded"><a href="/malvorlagen" title="">Ausmalbilder</a><ul class="menu"><li class="first collapsed"><a href="/ausmalbilder" title="">Ausmalbilder Jahreszeiten</a></li>
<li class="collapsed"><a href="/ausmalbilder-geburtstag-fasching" title="">Weitere Ausmalbilder</a></li>
<li class="last leaf"><a href="/mandalas-kinder" title="">Mandalas</a></li>
</ul></li>
</ul> </div>
</div>
</div>
</nav>
<!-- EOF: #main-navigation -->
</div>
<!-- EOF: #navigation-wrapper -->
</div>
<!--EOF: #header-right -->
</div>
<!-- EOF: #header -->
</header>
<!-- EOF: header -->
<div class="clearfix" id="content">
<!-- #top-content -->
<div class="container clearfix" id="top-content">
<!-- intro-page -->
<div class="intro-page">
<div class="region region-top-content">
<div class="block block-block" id="block-block-67">
<div class="content">
<div class="content">
<h2>
		Tritt ein in die <strong>abenteuerliche Welt</strong> der Schaffenden. Hier kannst du basteln &amp; gestalten, deine <strong>Kreativität</strong> voll und ganz austoben. Bau dir aus Papier, Filz und Wolle eine Welt: <strong>deine Welt</strong>.</h2>
</div>
</div>
</div>
</div>
</div>
<!-- EOF: intro-page -->
</div>
<!--EOF: #top-content -->
<!-- #banner -->
<div class="container" id="banner">
<!-- #slider-container -->
<div id="slider-container">
<div class="flexslider loading">
<ul class="slides">
<!-- first-slide -->
<li class="slider-item">
<div class="slider-image">
<a href="/basteln-lernen"><img alt="" src="/sites/all/themes/simplecorp/images/sampleimages/img1.jpg"/></a>
</div>
<div class="flex-caption">
<h3>Was hat Basteln mit Lernen zu tun?</h3>
</div>
</li>
<!-- second-slide -->
<li class="slider-item">
<div class="slider-image">
<a href="/basteln-zu-halloween"><img alt="" src="/sites/all/themes/simplecorp/images/sampleimages/img6.jpg"/></a>
</div>
<div class="flex-caption">
<h3>Bald ist Halloween, Zeit für Monster und Gespenster!</h3>
</div>
</li>
<!--<li class="slider-item">
                                    <div class="slider-image">                            
                                        <a href="/sommer-basteln"><img src="/sites/all/themes/simplecorp/images/sampleimages/img5.jpg" alt="" /></a>
                                    </div>
                                    <div class="flex-caption">
                                        <h3>Tolle Bastelideen für den Sommer!</h3>
                                    </div>
                                </li>
								
								<!--<li class="slider-item">
                                    <div class="slider-image">                            
                                        <a href="/osterbastelei"><img src="/sites/all/themes/simplecorp/images/sampleimages/img3.jpg" alt="" /></a>
                                    </div>
                                    <div class="flex-caption">
                                        <h3>Bald ist Ostern!</h3>
                                    </div>
                                </li>
								<!--<li class="slider-item">
                                    <div class="slider-image">                            
                                        <a href="/kinderfasching-basteln"><img src="/sites/all/themes/simplecorp/images/sampleimages/img9.jpg" alt="" /></a>
                                    </div>
                                    <div class="flex-caption">
                                        <h3>Bald ist Kinderfasching!</h3>
                                    </div>
                                </li>
								<!--<li class="slider-item">
                                    <div class="slider-image">                            
                                        <a href="/weihnachtsbastelei"><img src="/sites/all/themes/simplecorp/images/sampleimages/img8.jpg" alt="" /></a>
                                    </div>
                                    <div class="flex-caption">
                                        <h3>Es weihnachtet ... Tolle Bastelideen warten auf dich!</h3>
                                    </div>
                                </li>
								
								
								
								
								 
                              
								 
								
                                
		
                                <!-- third-slide -->
<li class="slider-item">
<div class="slider-image">
<a href="/geldgeschenke"><img alt="" src="/sites/all/themes/simplecorp/images/sampleimages/img2.jpg"/></a>
</div>
<div class="flex-caption">
<h3>Kreative Anregungen für Geldgeschenke</h3>
</div>
</li>
<!-- fourth-slide -->
<li class="slider-item">
<div class="slider-image">
<a href="/basteln-papier"><img alt="" src="/sites/all/themes/simplecorp/images/sampleimages/img4.jpg"/></a>
</div>
<div class="flex-caption">
<h3>Tiere, Bäume und ein Haus aus Papier</h3>
</div>
</li>
</ul>
</div>
</div>
<!-- EOF: #slider-container -->
</div>
<!-- EOF: #banner -->
<!--#featured -->
<div id="featured">
</div>
<!-- EOF: #featured -->
<!--#main-content -->
<div class="container clearfix" id="main-content">
<div class="three-fourth">
<!--#main-content-inside-->
<div id="main-content-inside">
<h1>Basteln für Kinder und das Kind in Ihnen</h1> <div class="tabs"></div> <div class="region region-content">
<div class="block block-system" id="block-system-main">
<div class="content">
<article about="/node/2" class="node node-story hentry clearfix" id="node-2" typeof="schema:Article sioc:Item foaf:Document">
<div class="entry-body clearfix">
<span class="rdf-meta element-hidden" content="Basteln für Kinder und das Kind in Ihnen" property="schema:name"></span><span class="rdf-meta element-hidden" content="0" datatype="xsd:integer" property="sioc:num_replies"></span>
<div class="content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><!--Zeile1 -->
<div class="one_half">
<a href="/tiere-basteln"><img alt="Tiere basteln" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-01.png"/></a>
<h3>
		Basteln mit Papier</h3>
<p>Viele Tiere kannst du aus Papier falten: Hunde, Hasen, Schmetterlinge ... Schau hier, wie das geht.</p>
<div class="readmore">
<a href="/tiere-basteln">Zur Übersicht</a></div>
</div>
<div class="one_half last">
<a href="/basteln-mit-wolle"><img alt="Basteln mit Wolle" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-02.png"/></a>
<h3>
		Basteln mit Wolle</h3>
<p>Tolle Sachen aus Wolle. Wie wär´s mit einer süßen Katze, einem Küken oder einem Schneemann?</p>
<div class="readmore">
<a href="/basteln-mit-wolle">Zur Übersicht</a></div>
</div>
<div class="clear">
	 </div>
<!--Zeile2 -->
<div class="one_half">
<a href="/basteln-naturmaterialien"><img alt="Naturmaterialien" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-03.png"/></a>
<h3>
		Naturmaterialien</h3>
<p>Kleine Hexe aus Zapfen und Moos, Figuren aus bunten Blättern oder Anhänger aus Eicheln - leg los!</p>
<div class="readmore">
<a href="/basteln-naturmaterialien">Zur Übersicht</a></div>
</div>
<div class="one_half last">
<a href="/geldgeschenke"><img alt="Geldgeschenke" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-04.png"/></a>
<h3>
		Money Money</h3>
<p>Geld schenken ist einfallslos? Jetzt nicht mehr! Hier gibts originelle Ideen für Geldgeschenke.</p>
<div class="readmore">
<a href="/geldgeschenke">Zur Übersicht</a></div>
</div>
<div class="clear">
	 </div>
<!--Zeile3 -->
<div class="one_half">
<a href="/originell-verpacken"><img alt="Geschenke verpacken" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-07.jpg"/></a>
<h3>
		Schön verpackt</h3>
<p>In einer schönen Verpackung macht Schenken doppelten Spaß und doppelte Freude.</p>
<div class="readmore">
<a href="/originell-verpacken">Zur Übersicht</a></div>
</div>
<div class="one_half last">
<a href="/servietten-basteln"><img alt="Bastelideen" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-08.jpg"/></a>
<h3>
		Servietten</h3>
<p>Hier findest du tolle Bastelideen für Servietten: lustige Tischdeko, knallrote Rosen oder luftige Pompons.</p>
<div class="readmore">
<a href="/servietten-basteln">Zur Übersicht</a></div>
</div>
<div class="clear">
	 </div>
<!--Zeile4 -->
<div class="one_half">
<a href="/malvorlagen"><img alt="Malvorlagen" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-05.png"/></a>
<h3>
		Malvorlagen</h3>
<p>Magst du Farben? Dann gefallen dir unsere Malvorlagen. Ausdrucken! Pinsel schwingen!</p>
<div class="readmore">
<a href="/malvorlagen">Zur Übersicht</a></div>
</div>
<div class="one_half last">
<a href="/mandala-vorlagen"><img alt="Mandala Vorlagen" class="img-align-left" src="/sites/all/themes/simplecorp/images/sampleimages/featured-img-06.png"/></a>
<h3>
		Mandala Vorlagen</h3>
<p>Mandalas zu Festen und Zeiten, Tiermotive und viele mehr. Einige gehen schnell, andere brauchen Geduld.</p>
<div class="readmore">
<a href="/mandala-vorlagen">Zur Übersicht</a></div>
</div>
<div class="clear">
	 </div>
</div></div></div><div class="field field-name-field-vote field-type-fivestar field-label-above"><div class="field-label">Bewerte die Anleitung: </div><div class="field-items"><div class="field-item even" property="schema:aggregateRating" typeof="schema:AggregateRating"><form accept-charset="UTF-8" action="/" class="fivestar-widget" id="fivestar-custom-widget" method="post"><div><div class="clearfix fivestar-average-text fivestar-average-stars fivestar-form-item fivestar-oxygen"><div class="form-item form-type-fivestar form-item-vote">
<div class="form-item form-type-select form-item-vote">
<select class="form-select" id="edit-vote--2" name="vote"><option value="-">Bewertung auswählen</option><option value="20">Gib Basteln für Kinder und das Kind in Ihnen 1/5 </option><option value="40">Gib Basteln für Kinder und das Kind in Ihnen 2/5 </option><option value="60">Gib Basteln für Kinder und das Kind in Ihnen 3/5 </option><option selected="selected" value="80">Gib Basteln für Kinder und das Kind in Ihnen 4/5 </option><option value="100">Gib Basteln für Kinder und das Kind in Ihnen 5/5 </option></select>
<div class="description"><div class="fivestar-summary fivestar-summary-average-count"><span class="average-rating">Durchschnitt: <span>3.4</span> </span> <span class="total-votes">(<span>4661</span> Bewertungen)</span></div><meta content="3.4" property="schema:ratingValue"/><meta content="4661" property="schema:ratingCount"/></div>
</div>
</div>
</div><input class="fivestar-submit form-submit button small round green" id="edit-fivestar-submit" name="op" type="submit" value="Bewerten"/><input name="form_build_id" type="hidden" value="form-nxy1pCgfCkkJs2xnTtQTz9BetVyLQRA-YRbEtiL3h38"/>
<input name="form_id" type="hidden" value="fivestar_custom_widget"/>
</div></form></div></div></div><span class="rdf-meta element-hidden" rel="schema:url" resource="/node/2"></span><span class="rdf-meta element-hidden" content="Basteln für Kinder und das Kind in Ihnen" property="schema:name"></span>
</div>
</div>
</article>
</div>
</div>
</div>
</div>
<!--EOF:#main-content-inside-->
</div>
<!--.sidebar second-->
<div class="one-fourth last">
<aside class="sidebar">
<div class="region region-sidebar-second">
<div class="block block-block" id="block-block-52">
<div class="content">
<form action="https://www.google.de" id="cse-search-box">
<div>
<input name="cx" type="hidden" value="partner-pub-4569642605643069:1411609097"/>
<input name="ie" type="hidden" value="UTF-8"/>
<input name="q" size="23" type="text"/>
<input name="sa" type="submit" value="Suche"/>
</div>
</form>
<script src="https://www.google.de/coop/cse/brand?form=cse-search-box&amp;lang=de" type="text/javascript"></script> </div>
</div>
<div class="block block-block" id="block-block-96">
<div class="content">
<div id="57040-20"><script src="//ads.themoneytizer.com/s/gen.js?type=20"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=57040&amp;formatId=20"></script></div> </div>
</div>
<div class="block block-block" id="block-block-78">
<h2>Unsere Partner</h2>
<div class="content">
<p><a href="https://www.wir-backen.de" rel="nofollow" target="_blank"><img src="/files/bastelngestalten/partnerlogos/backen-logo.png"/></a>    <a href="https://fest-und-feiern.de" target="_blank"><img src="/files/bastelngestalten/partnerlogos/feste-logo.png"/><br/>
</a><a href="https://www.kreativraum24.de" rel="nofollow" target="_blank"><img alt="Kreativraum 24" src="/files/bastelngestalten/kreativraum24.png" style="width: 97px; height: 26px;"/></a></p>
</div>
</div>
</div>
</aside>
</div>
<!--EOF:.sidebar second-->
</div>
<!--EOF: #main-content -->
<!-- #bottom-content -->
<div class="container clearfix" id="bottom-content">
<div class="region region-bottom-content">
<div class="block block-block" id="block-block-80">
<h2>Referenzen</h2>
<div class="content">
<div class="content">
<div class="container clearfix ">
<img alt=" " class="intro-img" src="/files/bastelngestalten/partnerlogos/referenzen.jpg"/></div>
</div> </div>
</div>
</div>
</div>
<!-- EOF: #bottom-content -->
</div> <!-- EOF: #content -->
<!-- #footer -->
<footer id="footer">
<!-- #footer-bottom -->
<div id="footer-bottom">
<div class="container clearfix">
<span class="right"><a class="backtotop" href="#">↑</a></span>
<div class="region region-footer">
<div class="block block-block" id="block-block-1">
<div class="content">
<p><a href="/impressum" rel="nofollow">Impressum</a> - <a href="/datenschutz" rel="nofollow">Datenschutzerklärung</a> - <a href="/links">Web-Empfehlungen</a></p>
</div>
</div>
<div class="block block-block" id="block-block-98">
<div class="content">
<div id="57040-28"><script src="//ads.themoneytizer.com/s/gen.js?type=28"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=57040&amp;formatId=28"></script></div> </div>
</div>
</div>
</div>
</div>
<!-- EOF: #footer-bottom -->
</footer>
<!-- EOF #footer -->
</div>
<!-- EOF: #page -->
</div>
<!-- EOF: #page-wrapper --> <script type="text/javascript">
<!--//--><![CDATA[//><!--

		jQuery(document).ready(function($) {

		    $(window).load(function() {

		        $(".flexslider").fadeIn("slow");

		        $(".flexslider").flexslider({
		            useCSS: false,
		            animation: "fade",
		            controlNav: 1,
		            directionNav: 1,
		            animationLoop: true,
		            touch: 1,
		            pauseOnHover: 1,
		            nextText: "&rsaquo;",
		            prevText: "&lsaquo;",
		            keyboard: true,
		            slideshowSpeed: 5000,
		            randomize: 0,
		            start: function(slider) {
		                slider.removeClass("loading");
		            }
		        });
		    });
		});
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

		jQuery(document).ready(function($) {

		    if (jQuery("#main-navigation, #main-navigation .content").length && jQuery()) {
		        var arrowimages = {
		            down: ["downarrowclass", "./images/plus.png", 23],
		            right: ["rightarrowclass", "./images/plus-white.png"]
		        }
		        var jqueryslidemenu = {
		            animateduration: {
		                over: 200,
		                out: 100
		            },
		            //duration of slide in/ out animation, in milliseconds
		            buildmenu: function(menuid, arrowsvar) {

		                jQuery(document).ready(function(jQuery) {
		                    var jQuerymainmenu = jQuery("#" + menuid + ">ul.menu:not(.sf-menu)")
		                    var jQueryheaders = jQuerymainmenu.find("ul").parent()

		                    jQueryheaders.each(function(i) {
		                        var jQuerycurobj = jQuery(this)
		                        var jQuerysubul = jQuery(this).find("ul:eq(0)")
		                        this._dimensions = {
		                            w: this.offsetWidth,
		                            h: this.offsetHeight,
		                            subulw: jQuerysubul.outerWidth(),
		                            subulh: jQuerysubul.outerHeight()
		                        }
		                        this.istopheader = jQuerycurobj.parents("ul").length == 1 ? true : false
		                        jQuerysubul.css({
		                            top: this.istopheader ? this._dimensions.h + "px" : 0
		                        })
		                        jQuerycurobj.children("a:eq(0)").css(this.istopheader ? {
		                            paddingRight: arrowsvar.down[2]
		                        } : {}).append("<span class=" + (this.istopheader ? arrowsvar.down[0] : arrowsvar.right[0]) + " />")

		                        jQuerycurobj.hover(

		                        function(e) {
		                            var jQuerytargetul = jQuery(this).children("ul:eq(0)")
		                            this._offsets = {
		                                left: jQuery(this).offset().left,
		                                top: jQuery(this).offset().top
		                            }
		                            var menuleft = this.istopheader ? 0 : this._dimensions.w
		                            menuleft = (this._offsets.left + menuleft + this._dimensions.subulw > jQuery(window).width()) ? (this.istopheader ? -this._dimensions.subulw + this._dimensions.w : -this._dimensions.w) : menuleft
		                            if (jQuerytargetul.queue().length <= 1) //if 1 or less queued animations
		                            jQuerytargetul.css({
		                                left: menuleft + "px",
		                                width: this._dimensions.subulw + "px"
		                            }).slideDown(jqueryslidemenu.animateduration.over)
		                        }, function(e) {
		                            var jQuerytargetul = jQuery(this).children("ul:eq(0)")
		                            jQuerytargetul.slideUp(jqueryslidemenu.animateduration.out)
		                        }) //end hover
		                        jQuerycurobj.click(function() {
		                            jQuery(this).children("ul:eq(0)").hide()
		                        })
		                    }) //end jQueryheaders.each()

		                    jQuerymainmenu.find("ul").css({
		                        display: "none",
		                        visibility: "visible"
		                    })

		                }) //end document.ready
		            }
		        }

		        jqueryslidemenu.buildmenu("main-navigation .content", arrowimages)
		        jqueryslidemenu.buildmenu("main-navigation", arrowimages)

		    }
		});
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

jQuery(document).ready(function() { jQuery(".backtotop").click(function(){ jQuery("html, body").animate({scrollTop:0}, "slow"); return false; }); });

//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

jQuery(document).ready(function($) {

	$("#main-navigation > ul, #main-navigation .content > ul").mobileMenu({
	prependTo: "#navigation-wrapper",
	combine: false,
	switchWidth: 960,
	topOptionText: Drupal.settings.simplecorp['responsive_menu_topoptiontext']
	});

});
//--><!]]>
</script>
</body>
</html>
