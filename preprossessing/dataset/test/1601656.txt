<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>wq.lt — A dead-simple URL shortener</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="no-cache" http-equiv="Cache-Control"/>
<meta content="A dead-simple URL shortener with an easily accessible public API" name="description"/>
<meta content="url shortener,shorten url,api,url trimmer,short url,tweet url,twitter,lightweight,simple" name="keywords"/>
<script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-127976829-1', 'auto');
        ga('send', 'pageview');
        </script>
<style media="all" type="text/css">
@import "http://wq.lt/tpl_resources/wq/style/style.css";
</style>
<link href="http://wq.lt/tpl_resources/wq/images/favicon.ico" rel="icon" type="image/x-icon"/>
<script type="text/javascript">
 			var logged=false;
 			var api_url='http://wq.lt/api';
 			var img_dir='http://wq.lt/tpl_resources/wq/images/';
 			var language='en';
 		</script>
<script src="http://wq.lt/tpl_resources/wq/scripts/jquery.js" type="text/javascript"></script>
<script src="http://wq.lt/tpl_resources/wq/scripts/jquery.ifixpng.js" type="text/javascript"></script>
<script src="http://wq.lt/tpl_resources/wq/scripts/javascript.js" type="text/javascript"></script>
</head>
<body>
<div class="header" id="header_en">
<img alt="" class="sticky" src="http://wq.lt/tpl_resources/wq/images//sticky_en.png"/>
</div>
<div class="global_holder">
<div class="content">
<form action="http://wq.lt/api" method="POST">
<span class="url"><input name="url" type="text" value=""/></span>
<span id="button_en"><input class="button" name="submit" type="submit" value="Shorten!"/></span>
</form>
<br/>
<img alt="" src="http://wq.lt/tpl_resources/wq/images//presentation.png"/>
</div>
<div class="footer">
<div class="disclaimer">This website uses Google Analytics, a web analytics service provided by Google, Inc. Google Analytics uses "cookies", which are text files placed on your computer, to help the website analyze how users use the site. <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">Opt-out</a></div>
<div class="disclaimer">wq.lt is an URL redirection service and creators cannot be held responsible for the content of the external sites. Long story short, we are not responsible for any bad things that might happen to you while using this service (including a spontaneous space-shark attack). Service is provided as-is. Consider yourself warned. Seriously.</div>
<br/>
<a href="http://wq.lt/@PovMusteikis" rel="external">@PovMusteikis</a> and <a href="http://wq.lt/@MindaugasVitkus" rel="external">@MindaugasVitkus</a> joint superproject.
<br/>
Copyright two zero zero nine.
<br/>
<a class="link" href="http://wq.lt/api_doc_en" rel="external">API</a> | <a class="link" href="javascript:void(window.open('http://wq.lt/#'+location.href),'wq.lt')" onclick="return false" onmouseover="window.status='';return true">Bookmarklet</a>
<br/>
<a href="http://wq.lt/l/lt"><img alt="Lietuviška versija" src="http://wq.lt/tpl_resources/wq/images/flags/lt.gif"/></a> </div>
</div>
<div id="popup_bg">
<div id="popup"></div>
<div id="popup_img"></div>
</div>
</body>
</html>