<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<title>ã¹ã¿ã¸ãªãã¼ããã¯ã¹</title>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="http://www.beatnix.co.jp/favicon.ico" rel="shortcut icon"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="http://www.beatnix.co.jp/icon/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<meta content="ã¹ã¿ã¸ãªãã¼ããã¯ã¹" name="author"/>
<meta content="æ±äº¬é½å¤§ç°åºã«ããWebå¶ä½ã»ã¢ããªå¶ä½ä¼ç¤¾ã§ãã" name="description"/>
<meta content="Web,ãã¼ã ãã¼ã¸,iPhoneã¢ããª,ã¤ã©ã¹ã,åç" name="keywords"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="js/scrolltopcontrol.js" type="text/javascript"></script>
</head>
<body>
<h1><img alt="ã¹ã¿ã¸ãªãã¼ããã¯ã¹" height="134" src="img/title.png" width="524"/></h1>
<!---------------------- ããã²ã¼ã·ã§ã³ ---------------------->
<!--
<nav class="clearfix">
	<ul>
		<li><img src="img/menu-home.png" alt="menu-home" width="60" height="14"></li>
		<li><a href="http://studio.beatnix.co.jp"><img src="img/menu-blog.png" alt="menu-blog" width="51" height="14"></a></li>
	</ul>
</nav>
-->
<!---------------------- iPhoneã¢ããª ---------------------->
<section class="clearfix" id="apps">
<h2><img alt="iPhoneã¢ããª&amp;Androidã¢ããª" height="23" src="img/apps.png" width="88"/></h2>
<div id="toybox">
<img alt="ã¢ããª" height="118" src="img/apps-toybox.png" width="113"/>
</div>
<div class="naiyou">
<div id="main">
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1073389091" target="_blank"><img alt="ãã©ãè¡¨å½°ç¶" height="110" src="img/app-kodomo-hyousyoujyou.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1071325802" target="_blank"><img alt="ãã©ãã¯ã­ãã¯2" height="110" src="img/app-kodomoclock2.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1016286210" target="_blank"><img alt="ããããã¬ãã" height="110" src="img/app-worldpalette.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1170968530" target="_blank"><img alt="ãªããã" height="110" src="img/app-nazorie.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1044979034" target="_blank"><img alt="9x9ã«ã¼ã" height="110" src="img/app-9x9card.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1156300616" target="_blank"><img alt="ãã©ããã©ã¦ã¶" height="110" src="img/app-kodomobrowser.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1120394237" target="_blank"><img alt="ä¸æ¸ãã¨ãã£ã¿" height="110" src="img/app-drafttext.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1113215729" target="_blank"><img alt="ããããããã" height="110" src="img/app-binoculars.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1101542336" target="_blank"><img alt="ãã©ãããããã¡ã¼ã«" height="110" src="img/app-kidsmail.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1088632080" target="_blank"><img alt="ããã«ã£ãã­ããº" height="110" src="img/app-voicediary-kids.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id1080240729" target="_blank"><img alt="å¨åæåº«" height="110" src="img/app-zenryoku-bunko.png" width="170"/></a>
</div>
<div class="app">
<!--a href="https://itunes.apple.com/jp/app/id1013319439" target="_blank"><img src="img/app-collagepaint.png" width="170" height="110" alt="ã³ã©ã¼ã¸ã¥ãã¤ã³ã"></a-->
<a href="http://collagepaint.beatnix.co.jp" target="_blank"><img alt="ã³ã©ã¼ã¸ã¥ãã¤ã³ã" height="110" src="img/app-collagepaint.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/kodomoreta/id505397530" target="_blank"><img alt="ãã©ãã¬ã¿ã¼" height="110" src="img/app-kodomoletter.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id979781468" target="_blank"><img alt="æµãæã«3å" height="110" src="img/app-wishstar.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id975010774" target="_blank"><img alt="ããã«ã£ã" height="110" src="img/app-voicediary.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id975438750" target="_blank"><img alt="RelaxCamera" height="110" src="img/app-relaxcam.png" width="170"/></a>
</div>
<div class="app">
<a href="https://itunes.apple.com/jp/app/id985852556" target="_blank"><img alt="TOUCH SHAPE" height="110" src="img/app-touchshape.png" width="170"/></a>
</div>
<div class="app">
<a href="https://play.google.com/store/apps/details?id=jp.co.beatnix.kodomoclock" target="_blank"><img alt="ãã©ãã¯ã­ãã¯" height="110" src="img/app-kodomoclock.png" width="170"/></a>
</div>
<div class="app">
<img alt="service" height="110" src="img/app-blank.png" width="170"/>
</div>
</div>
</div>
</section>
<!---------------------- ãµã¼ãã¹åå®¹ ---------------------->
<section id="service">
<h2><img alt="service" height="23" src="img/service.png" width="176"/></h2>
<div class="naiyou">
<ul>
<li id="web"><img alt="Webå¶ä½" height="134" src="img/service-web.png" width="210"/></li>
<li id="app"><img alt="iPhoneã¢ããªéçº" height="126" src="img/service-app.png" width="195"/></li>
<li id="photo"><img alt="åçæ®å½±" height="115" src="img/service-photo.png" width="212"/></li>
<li id="illust"><img alt="ã¤ã©ã¹ãå¶ä½" height="140" src="img/service-illust.png" width="157"/></li>
</ul>
</div>
</section>
<!--------------------- ä¼ç¤¾æ¦è¦ ---------------------->
<section id="company">
<h2><img alt="ä¼ç¤¾æ¦è¦" height="23" src="img/company.png" width="267"/></h2>
<div class="naiyou">
<div id="main">
<img alt="ä¼ç¤¾æ¦è¦" height="126" src="img/company-president.png" width="124"/>
<table>
<tr>
<th>ä¼ç¤¾å</th>
<td>æéä¼ç¤¾ã¹ã¿ã¸ãªãã¼ããã¯ã¹</td>
</tr>
<tr>
<th>æå¨å°</th>
<td>ã145-0062 æ±äº¬å¤§ç°åºååæ2-27-5</td>
</tr>
<tr>
<th>è¨­ç«</th>
<td>2001å¹´3æ22æ¥</td>
</tr>
<tr>
<th>ä»£è¡¨</th>
<td>ç¨²ç¦ æµ©ä¸</td>
</tr>
</table>
</div>
</div>
</section>
<!---------------------- ã¡ã³ãã¼ ---------------------->
<section id="member">
<h2><img alt="ã¡ã³ãã¼" height="23" src="img/member.png" width="156"/></h2>
<div class="naiyou">
<div class="member-info clearfix">
<img alt="ã¼ã" height="80" src="img/member-boku.png" width="80"/>
<p>ã¼ããæè¿ã¨ã¦ãæ¶ãããã¦ãç®¸ãè»¢ãã§ãæ³£ããããªå¢ããç¹æã¯è³ãåããããã¨ãå¥½ããªæ ç»ã¯ãç·ã¯ã¤ãããã</p>
</div>
<div class="member-info clearfix">
<img alt="å¨" height="80" src="img/member-kodomo.png" width="80"/>
<p>ãã©ããã¼ããä½ã£ãã¢ããªããããããã¦ï¼ãã¨å«ã³ãããªããããç¡è¶ãªä½¿ãæ¹ããã¦ãã°ãè¦ã¤ãã¦ãããåãããã¬ã¼ãå½¼å¥³ã®è¦ã¤ãããã°ã¯æ°ç¥ããã</p>
</div>
</div>
</section>
<!---------------------- ãåãåãã ---------------------->
<section class="clearfix" id="inquiry">
<div class="inq"><a href="http://studio.beatnix.co.jp/"><img alt="blog" height="176" src="img/blog.png" width="185"/></a></div>
<div class="inq"><img alt="mail" height="179" src="img/mail.png" width="209"/></div>
<div class="inq"><a href="https://twitter.com/inafukukouichi"><img alt="twitter" height="145" src="img/twitter.png" width="198"/></a></div>
</section>
<!---------------------- copyright ---------------------->
<footer>
<img alt="copyright" height="12" src="img/copyright.png" width="204"/>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49854108-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
