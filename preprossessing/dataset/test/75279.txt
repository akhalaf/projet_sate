<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Sakura – </title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="/assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/css/sakura.css" rel="stylesheet"/>
<link href="/assets/css/sakura-theme.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Philosopher:400,700" rel="stylesheet" type="text/css"/>
<script src="/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery.mousewheel.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/assets/vendors/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
<div class="row" style="margin-top:1em">
<div class="col-xs-3">
<a class="title" href="/">Sakura</a><br/>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6">
<h1>Авторизация на сайте</h1>
<form id="w0" method="POST">
<input name="_csrf" type="hidden" value="w3QoGYTEsClsuqtT0TgVZQD1RafdzSFdLWSOvPDTdGqcBVxfsLPGcFn2_jWjVG8jT4EJla6mYzZgIuL6qLtEBQ=="/><div class="form-group field-form-usermail required">
<label class="control-label" for="form-usermail">Электронная почта</label>
<input aria-required="true" class="form-control" id="form-usermail" name="Form[usermail]" type="text"/>
<div class="help-block"></div>
</div><div class="form-group field-form-password required">
<label class="control-label" for="form-password">Пароль</label>
<input aria-required="true" class="form-control" id="form-password" name="Form[password]" type="password"/>
<div class="help-block"></div>
</div><input class="btn btn-primary" type="submit" value="Продолжить"/></form></div>
</div>
</div>
</body>
</html>