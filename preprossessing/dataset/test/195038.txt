<!DOCTYPE html>
<html lang="en-us">
<head>
<title>Don Drysdale Quotes</title>
<meta charset="utf-8"/>
<meta content="don drysdale quotes Don Drysdale Quotes" name="keywords"/>
<meta content="Don Drysdale quotes including baseball quotes from Don Drysdale and baseball quotes about Don Drysdale." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<!-- BEGIN FREESTAR -->
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || {};
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || {};
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- END FREESTAR -->
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<div id="wrapper">
<div class="header-container">
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
</div>
<div class="container">
<div class="intro">
<h1>Don Drysdale Quotes</h1>
<p>Baseball Almanac is pleased to present an unprecedented collection of baseball related quotations spoken by Don Drysdale and about Don Drysdale.</p>
</div>
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"My own little rule was two for one. If one of my teammates got knocked down, then I knocked down two on the other team." - <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a></p>
</div>
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header">
<h2>Don Drysdale Quotes</h2>
<p>Quotes From &amp; About Don Drysdale / <a href="../quomenu.shtml" title="Baseball Quotes">Baseball Quotes</a> / <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a></p>
</td>
</tr>
<tr>
<td class="banner">Quotes From Don Drysdale</td>
</tr>
<tr>
<td class="datacolBox">
<p>"A torn rotator cuff is a cancer for a pitcher and if a pitcher gets a badly torn one, he has to face the facts, it's all over baby."</p>
<p>"If they knocked two of your guys down, I'd get four. You have to protect your hitters."</p>
<p>"In Brooklyn, it was as though you were in your own little bubble. You were all part of one big, but very close family, and the Dodgers were the main topic of everybody's conversations and you could sense the affection people had for you. I don't know that such a thing exists anymore."</p>
<p>"I hate all hitters. I start a game mad and I stay that way until it's over."</p>
<p>"It's a bottom line business where a lot of gray suits are brought in and then, within two years, these guys suddenly know everything about baseball."</p>
<p>"My own little rule was two for one. If one of my teammates got knocked down, then I knocked down two on the other team." Source: Hall of Fame Yearbook (1989)</p>
<p>"Some of these guys wear beards to make them look intimidating, but they don't look so tough when they have to deliver the ball. Their abilities and their attitudes don't back up their beards."</p>
<p>"The pitcher has to find out if the hitter is timid, and if he is timid, he has to remind the hitter he's timid." Source: New York Times (July 9, 1979)</p>
<p>"When I throw a curve that hangs and it goes for a hit, I want to chew up my glove."</p>
<p>"When the ball is over the middle of the plate, the batter is hitting it with the sweet part of the bat. When it's inside, he's hitting it with the part of the bat from the handle to the trademark. When it's outside, he's hitting it with the end of the bat. You've got to keep the ball away from the sweet part of the bat. To do that, the pitcher has to move the hitter off the plate." Source: Words of Wisdom (William &amp; Leonard Safire)</p>
<p>"When we played, World Series checks meant something. Now all they do is screw your taxes."</p>
</td>
</tr>
<tr>
<td class="banner">Quotes About Don Drysdale</td>
</tr>
<tr>
<td class="datacolBox">
<p>"Batting against <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a> is the same as making a date with a dentist." - <a href="../players/player.php?p=groatdi01" title="Dick Groat">Dick Groat</a></p>
<p>"<a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a> would consider an intentional walk a waste of three pitches. If he wants to put you on base, he can hit you with one pitch." - <a href="../players/player.php?p=shannmi01" title="Mike Shannon">Mike Shannon</a></p>
<p>"I hated to bat against <a href="../players/player.php?p=drysddo01" title="Don Drysdale">(Don) Drysdale</a>. After he hit you he'd come around, look at the bruise on your arm and say, 'Do you want me to sign it?'" - <a href="../players/player.php?p=mantlmi01" title="Mickey Mantle">Mickey Mantle</a></p>
<p>"I personally think it's too bad if a batter gets hit crowding the plate. I know that <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a>, <a href="../players/player.php?p=sherrla01" title="Larry Sherry">Larry Sherry</a>, and <a href="../players/player.php?p=willist02" title="Stan Williams">Stan Williams</a> felt the same way when they pitched for the Dodgers in the late fifties and early sixties." - <a href="../players/player.php?p=craigro01" title="Roger Craig">Roger Craig</a></p>
<p>"The trick against <a href="../players/player.php?p=drysddo01" title="Don Drysdale">(Don) Drysdale</a> is to hit him before he hits you." - <a href="../players/player.php?p=cepedor01" title="Orlando Cepeda">Orlando Cepeda</a></p>
<p><strong>Los Angeles Times Article: They Did It with Big "D"</strong>
</p><div class="blockquote">
<p>Most remember <a href="../players/player.php?p=gibsoki01" title="Kirk Gibson">Gibson's</a> homer being described in legendary calls from Scully on national television and Jack Buck on national radio. Only those listening on local radio remember that <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Drysdale</a> also made a call, a colorful, lengthy description of the at-bat that ended with a shout. 
							</p><p>"Way back. . . . This ball is gone!! . . . This crowd will not stop. . . . They can't believe the ending. . . . And this time, Mighty Casey did NOT strike out!"</p><p>Few ever talk about <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Drysdale's</a> call, and It's easy to think of it as the third-most important home-run description of that night. But one notable person would disagree.
							</p><p>In the spring of 1989, <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Drysdale</a> was approached by <a href="../players/player.php?p=gibsoki01" title="Kirk Gibson">Gibson</a>, who thanked him.
							</p><p>"For what?" <a href="../players/player.php?p=drysddo01" title="Don Drysdale">Drysdale</a> asked.
							</p><p>"Every time I needed inspiration while working out this winter, I listened to your call," <a href="../players/player.php?p=gibsoki01" title="Kirk Gibson">Gibson</a> said.</p>
</div>
</td>
</tr>
<tr>
<td class="header">Quotes From &amp; About Don Drysdale</td>
</tr>
</table>
</div>
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="../images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p><a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a> was inducted into the National Baseball Hall of Fame in 1984. See how his <a href="../hof/hofstpi.shtml">statistics</a> compare to other pitchers who are also enshrined.</p>
<p>Did you know that in 1962, Don Drysdale won twenty-five games and lost only nine, earning him his first (and only one was given that year) <a href="../awards/aw_cyy.shtml" title="Cy Young Award">Cy Young Award</a>?</p>
<p><a href="../players/player.php?p=drysddo01" title="Don Drysdale">Don Drysdale</a> holds the Major League record for seasons leading the league in <a href="../recbooks/rb_hbp.shtml">batters hit by a pitch</a> - a practice that today is shunned upon. What are your thoughts? Share your opinion on <a href="https://www.baseball-fever.com" target="_blank">Baseball Fever</a> today.</p>
</div>
</div>
<div class="footer">
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
				copyright=new Date();
				update=copyright.getFullYear();
				document.write(update);
				</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
</div>
</body>
</html>