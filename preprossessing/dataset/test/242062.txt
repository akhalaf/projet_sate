<?xml version="1.0"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bluebulb Projects</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/JavaScript">
var objTimer
var textinnow

function pointer(elem){
 var header
 var footer
 var FFspacer = 0
 if(navigator.appName.indexOf('Netscape')!=-1) FFspacer = 72
 if(elem.tagName=='A'){elem=elem.parentNode}
 elem.getElementsByTagName('a')[0].style.color='#05A';
 elem.getElementsByTagName('a')[0].style.fontSize='14pt';
 if(getDivIndexPos(elem)!=document.getElementById('left').getElementsByTagName('div').length){
  footer = document.getElementById('left').getElementsByTagName('div')[getDivIndexPos(elem)+1]
  footer.getElementsByTagName('a')[0].style.color='#038';
  footer.getElementsByTagName('a')[0].style.fontSize='13pt';
 }
 if(getDivIndexPos(elem)!=0){
  header = document.getElementById('left').getElementsByTagName('div')[getDivIndexPos(elem)-1]
  header.getElementsByTagName('a')[0].style.color='#038';
  header.getElementsByTagName('a')[0].style.fontSize='13pt';
 }
 document.getElementById('ltop').style.height=(elem.offsetTop - FFspacer).toString() + 'px';
 document.getElementById('lpoint').style.height='30px';
 document.getElementById('lbottom').style.height=(500 - 30 - elem.offsetTop + FFspacer).toString() + 'px';
 document.getElementById('lsideA').style.width=document.getElementById('lsideB').style.width='28px';
 document.getElementById('lsideA').style.height=document.getElementById('lsideB').style.height='4px';
}

function getDivIndexPos(e){
 var i
 for(i=0;i<(document.getElementById('left').getElementsByTagName('div').length + 1);i++){
  if(e==document.getElementById('left').getElementsByTagName('div')[i]) return i;
 }
}

function thinner(elem){
 var header
 var footer
 if(elem.tagName=='A') elem=elem.parentNode
 elem.getElementsByTagName('a')[0].style.color='#000';
 elem.getElementsByTagName('a')[0].style.fontSize='12pt';
 if(getDivIndexPos(elem)!=document.getElementById('left').getElementsByTagName('div').length){
  footer = document.getElementById('left').getElementsByTagName('div')[getDivIndexPos(elem)+1]
  footer.getElementsByTagName('a')[0].style.color='#000';
  footer.getElementsByTagName('a')[0].style.fontSize='12pt';
 }
 if(getDivIndexPos(elem)!=0){
  header = document.getElementById('left').getElementsByTagName('div')[getDivIndexPos(elem)-1]
  header.getElementsByTagName('a')[0].style.color='#000';
  header.getElementsByTagName('a')[0].style.fontSize='12pt';
 }
 document.getElementById('lpoint').style.height='0px';
 document.getElementById('lbottom').style.height=document.getElementById('ltop').style.height='250px';
 document.getElementById('lsideA').style.width=document.getElementById('lsideB').style.width='1px';
 document.getElementById('lsideA').style.height=document.getElementById('lsideB').style.height='0px';
}

function eraser(){
 objTimer = setTimeout(function(){
  document.getElementById('right').style.verticalAlign = 'middle';
  document.getElementById('right').innerHTML = '<br /><br /><br /><br /><br />Roll over an image to see a description';
 }, 100)
}

function linkme(e){
 window.location=e.getElementsByTagName('A')[0].href
}

</script>
<script src="descriptors.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
 Bluebulb Projects <img align="absmiddle" alt="Bluebulb Logo" src="./genimages/bluebulb.jpg" title="Bluebulb Logo"/>
</div>
<div id="container">
<div id="left">
<div onclick="linkme(this);" onmouseout="thinner(this);eraser();" onmouseover="writer('cs');pointer(this);" title="The Measure of Things">
<a href="./MeasureOfThings/default.php"></a>The Measure of Things  
   </div>
<div onclick="linkme(this);" onmouseout="thinner(this);eraser();" onmouseover="writer('ct');pointer(this);" title="The Count of Things">
<a href="./CountOfThings/default.php"></a>The Count of Things  
   </div>
<div onclick="linkme(this);" onmouseout="thinner(this);eraser();" onmouseover="writer('bm');pointer(this);" title="Conservative Book Title Generator">
<a href="./bookmerica/default.php"></a>Conservative Book Title Generator  
   </div>
<div><a href=""></a></div>
</div>
<div id="mid" onmouseover="clearTimeout(objTimer);">
<img id="lsideA" src="./genimages/linesidetop.jpg"/><br/>
<img id="ltop" src="./genimages/lineseg.jpg"/><br/>
<img id="lpoint" src="./genimages/linepoint.jpg"/><br/>
<img id="lbottom" src="./genimages/lineseg.jpg"/><br/>
<img id="lsideB" src="./genimages/linesidebot.jpg"/><br/>
</div>
<div id="right">
<br/><br/><br/><br/><br/>Roll over an image to see a description
 </div>
</div>
<br style="clear:both;"/>
<div id="admin">
Administrative:  <a href="#">About Us</a> | <a href="#">Contact Us</a> | <a href="privacy.html">Privacy Policy</a>
</div>
</body>
</html>