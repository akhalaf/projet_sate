<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<meta content="width=device-width,initial-scale=1,user-scalable=no" name="viewport"/>
<title>Page non trouvée – Atelier Diptyc – Architecte d'intérieur à Saint-Étienne</title>
<script type="application/javascript">var edgtCoreAjaxUrl = "https://atelierdiptyc.com/wp-admin/admin-ajax.php"</script><script type="application/javascript">var EdgefAjaxUrl = "https://atelierdiptyc.com/wp-admin/admin-ajax.php"</script><link href="//atelierdiptyc.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://atelierdiptyc.com/feed/" rel="alternate" title="Atelier Diptyc - Architecte d'intérieur à Saint-Étienne » Flux" type="application/rss+xml"/>
<link href="https://atelierdiptyc.com/comments/feed/" rel="alternate" title="Atelier Diptyc - Architecte d'intérieur à Saint-Étienne » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/atelierdiptyc.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.20"}};
			!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://atelierdiptyc.com/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.2.6" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/style.css?ver=4.6.20" id="assemble_edge_default_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/modules.min.css?ver=4.6.20" id="assemble_edge_modules-css" media="all" rel="stylesheet" type="text/css"/>
<style id="assemble_edge_modules-inline-css" type="text/css">
.page-id-1520 .edgtf-portfolio-list-holder article .edgtf-pl-item-inner{
    overflow: visible;
}
</style>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/font-awesome/css/font-awesome.min.css?ver=4.6.20" id="edgtf_font_awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/elegant-icons/style.min.css?ver=4.6.20" id="edgtf_font_elegant-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/linea-icons/style.css?ver=4.6.20" id="edgtf_linea_icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-includes/js/mediaelement/mediaelementplayer.min.css?ver=2.22.0" id="mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=4.6.20" id="wp-mediaelement-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/style_dynamic.css?ver=1477493597" id="assemble_edge_style_dynamic-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/modules-responsive.min.css?ver=4.6.20" id="assemble_edge_modules_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/themes/assemble/assets/css/style_dynamic_responsive.css?ver=1477493597" id="assemble_edge_style_dynamic_responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=4.12" id="js_composer_front-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C500%2C600%2C700&amp;subset=latin-ext&amp;ver=1.0.0" id="assemble_edge_google_fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.2.6" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.2.6" type="text/javascript"></script>
<link href="https://atelierdiptyc.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://atelierdiptyc.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://atelierdiptyc.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 4.6.20" name="generator"/>
<meta content="Powered by Visual Composer - drag and drop page builder for WordPress." name="generator"/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta content="Powered by Slider Revolution 5.2.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body class="error404 edgt-core-1.0 assemble-ver-1.0.1 edgtf-grid-1200 edgtf-article-boxed-disabled edgtf-header-standard edgtf-fixed-on-scroll edgtf-default-mobile-header edgtf-sticky-up-mobile-header edgtf-dropdown-animate-height edgtf-light-header edgtf-fullscreen-search edgtf-search-fade edgtf-side-menu-slide-from-right wpb-js-composer js-comp-ver-4.12 vc_responsive" itemscope="" itemtype="http://schema.org/WebPage">
<div class="edgtf-wrapper edgtf-404-page">
<div class="edgtf-wrapper-inner">
<div class="edgtf-fullscreen-search-holder">
<div class="edgtf-fullscreen-search-close-container">
<div class="edgtf-search-close-holder">
<a class="edgtf-fullscreen-search-close" href="javascript:void(0)">
<span class="icon-arrows-remove"></span>
</a>
</div>
</div>
<div class="edgtf-fullscreen-search-table">
<div class="edgtf-fullscreen-search-cell">
<div class="edgtf-fullscreen-search-inner">
<form action="https://atelierdiptyc.com/" class="edgtf-fullscreen-search-form" method="get">
<div class="edgtf-form-holder">
<div class="edgtf-form-holder-inner">
<div class="edgtf-field-holder">
<input autocomplete="off" class="edgtf-search-field" name="s" placeholder="Search for..." type="text"/>
</div>
<button class="edgtf-search-submit" type="submit"><span class="icon_search "></span></button>
<div class="edgtf-line"></div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<header class="edgtf-page-header">
<div class="edgtf-fixed-wrapper">
<div class="edgtf-menu-area edgtf-menu-center">
<div class="edgtf-vertical-align-containers">
<div class="edgtf-position-left">
<div class="edgtf-position-left-inner">
<div class="edgtf-logo-wrapper">
<a href="https://atelierdiptyc.com/" itemprop="url" style="height: 75px;">
<img alt="logo" class="edgtf-normal-logo" height="150" itemprop="image" src="http://atelierdiptyc.com/wp-content/uploads/2016/10/logosite-2.jpg" width="414"/>
<img alt="dark logo" class="edgtf-dark-logo" itemprop="image" src="http://assemble.edge-themes.com/wp-content/uploads/2016/06/assemble-logo.png"/> <img alt="light logo" class="edgtf-light-logo" itemprop="image" src="http://assemble.edge-themes.com/wp-content/uploads/2016/06/assemble-logo-light.png"/> </a>
</div>
</div>
</div>
<div class="edgtf-position-center">
<div class="edgtf-position-center-inner">
<nav class="edgtf-main-menu edgtf-drop-down edgtf-default-nav">
<ul class="clearfix" id="menu-menu-principal"><li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-4495"><a class="" href="https://atelierdiptyc.com/"><span class="item_outer"><span class="item_text">Réalisations</span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-4585"><a class="" href="https://atelierdiptyc.com/agence/"><span class="item_outer"><span class="item_text">Agence</span></span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page narrow" id="nav-menu-item-4588"><a class="" href="https://atelierdiptyc.com/contacteznous/"><span class="item_outer"><span class="item_text">Contact</span></span></a></li>
</ul></nav>
</div>
</div>
<div class="edgtf-position-right">
<div class="edgtf-position-right-inner">
<a class="edgtf-search-opener" href="javascript:void(0)" style="font-size: 23px;margin: 0 0 0 0">
<span class="edgtf-search-opener-wrapper">
<span aria-hidden="true" class="edgtf-icon-font-elegant icon_search "></span> </span>
</a>
<a class="edgtf-side-menu-button-opener" href="javascript:void(0)">
<span class="edgtf-side-menu-lines" style="margin: 0 0 0 21px">
<span class="edgtf-side-menu-line edgtf-line-1"></span>
<span class="edgtf-side-menu-line edgtf-line-2"></span>
<span class="edgtf-side-menu-line edgtf-line-3"></span>
</span>
</a>
</div>
</div>
</div>
</div>
</div>
</header>
<header class="edgtf-mobile-header">
<div class="edgtf-mobile-header-inner">
<div class="edgtf-mobile-header-holder">
<div class="edgtf-grid">
<div class="edgtf-vertical-align-containers">
<div class="edgtf-mobile-menu-opener">
<a href="javascript:void(0)">
<div class="edgtf-mo-icon-holder">
<span class="edgtf-mo-lines">
<span class="edgtf-mo-line edgtf-line-1"></span>
<span class="edgtf-mo-line edgtf-line-2"></span>
<span class="edgtf-mo-line edgtf-line-3"></span>
</span>
</div>
</a>
</div>
<div class="edgtf-position-center">
<div class="edgtf-position-center-inner">
<div class="edgtf-mobile-logo-wrapper">
<a href="https://atelierdiptyc.com/" itemprop="url" style="height: 75px">
<img alt="mobile logo" height="150" itemprop="image" src="http://atelierdiptyc.com/wp-content/uploads/2016/10/logosite-2.jpg" width="414"/>
</a>
</div>
</div>
</div>
<div class="edgtf-position-right">
<div class="edgtf-position-right-inner">
</div>
</div>
</div> <!-- close .edgtf-vertical-align-containers -->
</div>
</div>
<nav class="edgtf-mobile-nav">
<div class="edgtf-grid">
<ul class="" id="menu-menu-principal-1"><li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-4495"><a class="" href="https://atelierdiptyc.com/"><span>Réalisations</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-4585"><a class="" href="https://atelierdiptyc.com/agence/"><span>Agence</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page " id="mobile-menu-item-4588"><a class="" href="https://atelierdiptyc.com/contacteznous/"><span>Contact</span></a></li>
</ul> </div>
</nav>
</div>
</header> <!-- close .edgtf-mobile-header -->
<a href="#" id="edgtf-back-to-top">
<span class="edgtf-icon-stack">
<i class="edgtf-icon-font-awesome fa fa-angle-up "></i> </span>
</a>
<div class="edgtf-content" style="margin-top: -100px">
<div class="edgtf-content-inner">
<div class="edgtf-page-not-found">
<h1>
<span class="edgtf-page-not-found-code">404</span>
<span class="edgtf-page-not-found-title">
								Page not found							</span>
</h1>
<h3>
							OOPS This page not found						</h3>
<a class="edgtf-btn edgtf-btn-large edgtf-btn-solid" href="https://atelierdiptyc.com/" itemprop="url" target="_self">
<span class="edgtf-btn-text">BACK TO HOME</span>
</a> </div>
</div>
</div>
</div>
</div>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/ui/tabs.min.js?ver=1.11.4" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/jquery/ui/accordion.min.js?ver=1.11.4" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var mejsL10n = {"language":"fr-FR","strings":{"Close":"Fermer","Fullscreen":"Plein \u00e9cran","Turn off Fullscreen":"Quitter le plein \u00e9cran","Go Fullscreen":"Passer en plein \u00e9cran","Download File":"T\u00e9l\u00e9charger le fichier","Download Video":"T\u00e9l\u00e9charger la vid\u00e9o","Play":"Lecture","Pause":"Pause","Captions\/Subtitles":"L\u00e9gendes\/Sous-titres","None":"None","Time Slider":"Curseur de temps","Skip back %1 seconds":"Saut en arri\u00e8re de %1 seconde(s)","Video Player":"Lecteur vid\u00e9o","Audio Player":"Lecteur audio","Volume Slider":"Curseur de volume","Mute Toggle":"Couper le son","Unmute":"R\u00e9activer le son","Mute":"Muet","Use Up\/Down Arrow keys to increase or decrease volume.":"Utilisez les fl\u00e8ches haut\/bas pour augmenter ou diminuer le volume.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Utilisez les fl\u00e8ches droite\/gauche pour avancer d\u2019une seconde, haut\/bas pour avancer de dix secondes."}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script src="https://atelierdiptyc.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.appear.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/modernizr.custom.85257.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.plugin.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/owl.carousel.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/slick.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/parallax.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=4.12" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/counter.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/fluidvids.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.prettyPhoto.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.nicescroll.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/ScrollToPlugin.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/TweenLite.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.waitforimages.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.easing.1.3.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/skrollr.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/bootstrapCarousel.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.touchSwipe.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/jquery.fullPage.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=4.12" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules/plugins/packery-mode.pkgd.min.js?ver=4.6.20" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var edgtfGlobalVars = {"vars":{"edgtfAddForAdminBar":0,"edgtfElementAppearAmount":-100,"edgtfFinishedMessage":"No more posts","edgtfMessage":"Loading new posts...","edgtfTopBarHeight":0,"edgtfStickyHeaderHeight":0,"edgtfStickyHeaderTransparencyHeight":60,"edgtfStickyScrollAmount":0,"edgtfLogoAreaHeight":0,"edgtfMenuAreaHeight":100,"edgtfMobileHeaderHeight":100}};
var edgtfPerPageVars = {"vars":{"edgtfStickyScrollAmount":0,"edgtfHeaderTransparencyHeight":100}};
/* ]]> */
</script>
<script src="https://atelierdiptyc.com/wp-content/themes/assemble/assets/js/modules.min.js?ver=4.6.20" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=4.12" type="text/javascript"></script>
<script src="https://atelierdiptyc.com/wp-includes/js/wp-embed.min.js?ver=4.6.20" type="text/javascript"></script>
</body>
</html>