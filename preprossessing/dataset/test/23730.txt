<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="no-cache" http-equiv="Pragma"/>
<meta content="0" http-equiv="expires"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="3000bonus.com is the most popular bookmarking website in 2020. The website is specially designed to allow editorial submissions by individual members to spread knowledge to the rest of the world. Submit your Stories, articles now!" name="description"/>
<meta content="News,Stories,Articles,Vote,Publish,Social,Networking,Groups,Submit,Upcoming,Comments,Tags,Live" name="keywords"/>
<meta content="en-us" name="Language"/>
<meta content="All" name="Robots"/>
<link href="/templates/wistie/css/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/wistie/css/dropdown.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="/templates/wistie/css/dropdown-default.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>
<!--[if lt IE 7]>
	<script type="text/javascript" src="/templates/wistie/js/jquery/jquery.dropdown.js"></script>
	<![endif]-->
<script src="/3rdparty/speller/spellChecker.js" type="text/javascript"></script>
<title>3000bonus.com - Popular Bookmarking Website 2020 - Your Source for Social News and Networking</title>
<link href="http://3000bonus.com/rss.php" rel="alternate" title="RSS 2.0" type="application/rss+xml"/>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="http://3000bonus.com/" rel="canonical"/>
<script type="text/javascript">
		/* Comment Administration Hover Effect */
		$(document).ready(function(){
		  $('div.comment-hover').css('visibility','hidden');
		  $('div.comment-wrap').each(function() {
			var controls = $(this).children('div.comment-hover');
			$(this).hover(
			  function() { $(controls).css('visibility','visible') },
			  function() { $(controls).css('visibility','hidden') }
			);
		  });
		});
	</script>
</head>
<body dir="ltr">
<!-- START CONTENT -->
<div id="content">
<script language="JavaScript" type="text/javascript">
			function checkForm() {
			answer = true;
			if (siw && siw.selectingSomething)
				answer = false;
			return answer;
			}//
			</script>
<!-- START HEADER.TPL -->
<div id="login">
<a href="/register/">Register</a> | <a href="/login.php?return=%2F">Login</a>
</div>
<div id="logo"><a href="http://3000bonus.com">3000bonus.com - Popular Bookmarking Website 2020</a></div>
<script type="text/javascript">
				
var some_search='Search..';
</script>
<!-- START SEARCH -->
<div class="search">
<form action="/search.php" id="thisform-search" method="get" name="thisform-search" onsubmit='document.location.href="http://3000bonus.com/search/"+this.search.value.replace(/\//g,"|").replace(/\?/g,"%3F"); return false;'>
<input class="searchfield" id="searchsite" name="search" onblur="if (this.value == '') {this.value = some_search;}" onfocus="if(this.value == some_search) {this.value = '';}" size="20" type="text" value="Search.."/>
<input class="searchbutton" type="submit" value="Go"/>
</form>
</div>
<div class="clear"></div>
<!-- END SEARCH -->
<!-- START NAVBAR -->
<ul id="nav">
<li class="current"><a href="http://3000bonus.com/">Published News</a></li>
<li><a href="/upcoming/">Upcoming News</a></li>
<li><a href="/submit/">Submit a New Story</a></li>
</ul>
<div id="categories">
<ul class="dropdown dropdown-horizontal">
<li>
<a href="/news/">News</a>
</li>
</ul>
</div><!-- END NAVBAR -->
<!-- START BREADCRUMBS -->
<h1>Published News 
				</h1>
<!-- END BREADCRUMBS -->
<!-- END HEADER.TPL -->
<!-- START LEFT COLUMN -->
<div id="leftcol">
<div class="pagination"><p><span>« previous</span><span> next »</span></p></div>
</div>
<!-- END LEFT COLUMN -->
<!-- START RIGHT COLUMN -->
<div id="rightcol">
<!-- START ABOUT -->
<!-- START ABOUT -->
<div class="headline">
<div class="sectiontitle"><a href="/page.php?page=about"></a></div>
</div>
<div id="aboutcontent">
                3000bonus.com is the most popular bookmarking website in 2020. The website is specially designed to allow editorial submissions by individual members to spread knowledge to the rest of the world. Submit your Stories, articles now!
                            </div>
<!-- END ABOUT --> <!-- END ABOUT -->
<!-- START LINKS -->
<div class="links">
<div class="sectiontitle"><a href="/topusers/"> Top Users</a></div>
</div>
<div class="links">
<div class="sectiontitle"><a href="/tagcloud/">Tag cloud</a></div>
</div>
<div class="links">
<div class="sectiontitle"><a href="/live/"> Live</a></div>
</div>
<!-- END LINKS -->
<br/>
</div>
<!-- END RIGHT COLUMN -->
<!-- START MIDDLE COLUMN -->
<div id="midcol">
<!-- START SORT -->
<div class="headline">
<div class="sectiontitle">Sort News</div>
</div>
<div id="navcontainer">
<ul id="navlist">
<li id="active"><a href="/" id="current"><span class="active">Most Recent</span></a></li>
<li><a href="/today/">Top Today</a></li>
<li><a href="/yesterday/">Yesterday</a></li>
<li><a href="/week/">Week</a></li>
<li><a href="/month/">Month</a></li>
<li><a href="/year/">Year</a></li>
<li><a href="/alltime/">All</a></li>
</ul>
</div>
<!-- END SORT -->
<div class="headline">
<div class="sectiontitle"><a href="/login.php?return=%2F">Login</a></div>
</div>
<div class="boxcontent">
<form action="/login.php?return=%2F" method="post"> 
		Username:<br/><input class="login" name="username" tabindex="40" type="text" value=""/><br/>
		Password:<br/><input class="login" name="password" tabindex="41" type="password"/><br/>
<input name="processlogin" type="hidden" value="1"/>
<input name="return" type="hidden" value=""/>
		Remember: <input name="persistent" tabindex="42" type="checkbox"/>
<input class="submit-s" tabindex="43" type="submit" value="Sign In"/>
</form>
</div>
<div class="headline">
<div class="sectiontitle"><a href="">Latest Comments</a></div>
</div>
<div class="boxcontent" id="sidebar-comments">
</div>
<div class="headline">
<div class="sectiontitle"><a href="/tagcloud/">Top Tags</a></div>
</div>
<div class="boxcontent tagformat">
</div>
<div class="headline">
<div class="sectiontitle">Saved Stories</div>
</div>
<div class="boxcontent">
</div>
</div>
<!-- END MIDDLE COLUMN -->
<br clear="all"/>
<!-- START FOOTER -->
<div id="footer">
<span class="subtext"> Copyright © 2021 3000bonus.com - Popular Bookmarking Website 2020 
			| <a href="/static/privacy-policy/">Privacy Policy</a> 
			| <a href="/sitemapindex.xml">Sitemap</a> 
			| <a href="/advanced-search/">Advanced Search</a>
<!-- START RSS -->
			| <a href="/rss/" target="_blank">
<img align="top" alt="RSS" border="0" height="12px" src="/templates/wistie/images/rss.gif" width="12px"/> RSS
			</a>
<!-- END RSS -->
						
			| <a href="http://3000bonus.com/rssfeeds.php">RSS Feeds</a> </span>
</div>
<!-- END FOOTER -->
</div>
<!-- END CONTENT -->
<script src="/templates/xmlhttp.php" type="text/javascript"></script>
</body>
</html>
