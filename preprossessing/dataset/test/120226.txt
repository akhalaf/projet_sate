<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="7c6ad00e4ab44d08" name="yandex-verification"/>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 7 ]> <html class="oldie ie6" lang="ru" dir="ltr"> <![endif]-->
<!--[if IE 7 ]>    <html class="oldie ie7" lang="ru" dir="ltr"> <![endif]-->
<!--[if IE 8 ]>    <html class="oldie ie8" lang="ru" dir="ltr"> <![endif]-->
<!--[if gt IE 8]><!--> <!--<![endif]-->
<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="styles_ie.css" />
<![endif]-->
<title>Портал банковского аналитика | Система Анализа финансового состояния банков России.</title>
<script src="lib/proto/prototype.js" type="text/javascript"></script>
<!--
<script type="text/javascript" src="js/event.simulate.js"></script>
<script type="text/javascript" src="js/chosen.js"></script>
-->
<script src="lib/proto/scriptaculous.js" type="text/javascript"></script>
<!--[if IE]>
	<script type="text/javascript" src="lib/flotr/excanvas.js"></script>
	<script type="text/javascript" src="lib/flotr/base64.js"></script>
	<![endif]-->
<script src="lib/flotr/canvas2image.js" type="text/javascript"></script>
<script src="lib/flotr/canvastext.js" type="text/javascript"></script>
<script src="lib/flotr/flotr.js" type="text/javascript"></script>
<script src="lib/Humble/js/HumbleFinance.js" type="text/javascript"></script>
<script src="js/smarty_ajax.js" type="text/javascript"></script>
<script src="js/tooltip.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<link href="/favicon.ico" rel="icon" type="image/x-icon"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
</head><body>
<div id="ajax-process"></div>
<div id="container">
<div id="branding">
<a href="index.php" title="Анализ банков. Портал банковского аналитика."><img alt="Анализ банков. Портал банковского аналитика." src="images/logo.gif"/></a>
</div>
<div id="branding_txt"><center><strong>Инструмент банковского аналитика, клиента банка для анализа финансового состояния и надежности банков</strong></center></div>
<div class="not_logged_in" id="user_navigation">
<a href="login.php"><img alt="Вход на портал" src="images/key.png"/> Вход</a>
<a href="forum.php?cmd=register"><img alt="Регистрация на портале" src="images/paste_plain.png"/> Регистрация</a>
<a accesskey="6" href="index.php?ref=help" rel="help"><img alt="Перейти к помощи" src="images/help.png"/> F.A.Q.</a>
</div>
<script type="text/javascript">

var searcht="";
function PressKey(evt){
 //var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
 var charCode = (evt.charCode) ? evt.charCode : evt.keyCode;

if (charCode==27){
	$('choose_bank').style.visibility = 'hidden'; // спрячем
	 $('search').style.zIndex = 'auto';
	 $('main_search').value='';
	 searcht=""; 
	return;
}
if(searcht==($('main_search').value))return; // если ничего не изменилось не "замучить" сервер
searcht=$('main_search').value;
if (searcht.substr(0, 2)=="04") {
	if (searcht.length<6){
	 $('choose_bank').style.visibility = 'hidden'; // спрячем
	 $('search').style.zIndex = 'auto'; 
	 return;
	}
}
else {
	if (searcht.match('^[0-9]+$')) {
	}
	else {
	if (searcht.length<4){
	 $('choose_bank').style.visibility = 'hidden'; // спрячем
	 $('search').style.zIndex = 'auto'; 
	 return;
	}
	}
}
//alert (searcht+String.fromCharCode(charCode));
$('choose_bank').innerHTML='<div class="waitmes">Идет формирование списка...</div>';
$('choose_bank').style.visibility = 'visible'; 
 $('search').style.zIndex= '9999'; 
SmartyAjax.update('choose_bank', 'bank_ajax.php', 'post', 'searcht='+searcht+'&f=ListSearchBank', undefined)
}


</script>
<div id="primary_nav">
<div id="search">
<form action="index.php" id="search-box" method="GET">
<fieldset>
<label class="hide" for="main_search">Поиск</label>
<input autocomplete="OFF" class="text_input" id="main_search" name="searchstring" onkeyup="PressKey(event)" size="25" tabindex="6" title="Введите название, рег.№ или БИК банка" type="text" value=""/>
<input class="submit_input" type="submit" value="Поиск банков"/>
<div id="choose_bank"><div class="waitmes">Идет формирование списка...</div>
</div>
</fieldset>
</form>
</div>
<ul>
<li class="left active" id="nav_banki"><a href="bank.php">Банки</a></li>
<li class="left" id="nav_rating"><a href="rating.php">Рейтинги</a></li>
<li class="left" id="nav_macro"><a href="macro.php">Макроэкономика</a></li>
<li class="left" id="nav_metod"><a href="metod.php">Методики/Формулы</a></li>
<li class="left" id="nav_forum"><a href="forum.php">Форум</a></li>
<li class="left" id="nav_api"><a href="api_help.php">API</a></li>
</ul>
</div> <div id="header"><h1>Система Анализа финансового состояния банков России.</h1></div>
<div id="content">
<div class="clearfix" id="work_pole">
<script type="text/javascript">
var currentdate = {
  params: function() {
    return {
      currentdate: $F("CurrentDate")
    }
  },
  cb: function(originalRequest) {
  	//alert(originalRequest.responseText);
		window.location.reload(true);
  }
}
</script>
<div class="left clearfix inline" id="katalogi">
<div class="general_box clearfix">
<img alt="Справочник БИК" src="images/bik.png"/><a href="?Cat=BIK"><b> Справочник БИК</b></a>
</div>
<div class="general_box clearfix">
<img alt="Избранные банки" src="images/favorites2.gif"/><a href="?UserGroup=favorites"><b> Избранные банки</b><br/><center>(<span id="count_favorites">0</span> шт.)</center></a>
</div>
<div class="general_box clearfix">
<img alt="Сравнение банков" src="images/favorites.gif"/><a href="?UserGroup=compare"><b> Сравнение банков</b><br/><center>(<span id="count_compare">0</span> шт.)</center></a>
</div>
<div class="general_box clearfix">
<img alt="Статистика по группам банков" src="images/sum_icon.gif"/><a href="?sredbank=all"><b> Статистика по группам банков</b><br/></a>
</div>
<div class="general_box clearfix">
<img alt="Индикаторы" src="images/voskl.gif"/><a href="?Cat=indikators"><b>  Индикаторы</b><br/></a>
</div>
<br/>
<h4><img alt="Каталоги банков" src="images/list_head.png"/>Каталоги банков</h4>
<div class="general_box clearfix">
<h4><img alt="По алфавиту" src="images/alfa.png"/> <i>По алфавиту</i></h4>
<ul class="block_list">
<li><a href="?BankGroup=1">1БА...АЛЬ</a></li>
<li><a href="?BankGroup=2">АМБ...БАН</a></li>
<li><a href="?BankGroup=3">БАР...ВЕН</a></li>
<li><a href="?BankGroup=4">ВЕР...ГАЗ</a></li>
<li><a href="?BankGroup=5">ГАЛ...ДОЙ</a></li>
<li><a href="?BankGroup=6">ДОЛ...ЗЕЛ</a></li>
<li><a href="?BankGroup=7">ЗЕМ...ИС </a></li>
<li><a href="?BankGroup=8">ИСБ...КРА</a></li>
<li><a href="?BankGroup=9">КРЕ...М2М</a></li>
<li><a href="?BankGroup=10">МАЙ...МИ-</a></li>
<li><a href="?BankGroup=11">МИБ...МУЛ</a></li>
<li><a href="?BankGroup=12">МУН...НОВ</a></li>
<li><a href="?BankGroup=13">НОК...ПЕР</a></li>
<li><a href="?BankGroup=14">ПЕТ...ПРО</a></li>
<li><a href="?BankGroup=15">ПУЛ...РОС</a></li>
<li><a href="?BankGroup=16">РОЯ...СВЯ</a></li>
<li><a href="?BankGroup=17">СДМ...СОБ</a></li>
<li><a href="?BankGroup=18">СОВ...СУР</a></li>
<li><a href="?BankGroup=19">СЭБ...ТРО</a></li>
<li><a href="?BankGroup=20">ТУВ...ФИН</a></li>
<li><a href="?BankGroup=21">ФК ...ЭКС</a></li>
<li><a href="?BankGroup=22">ЭЛ ...ЯРО</a></li>
</ul></div>
<div class="general_box clearfix">
<h4><img alt="По городу" src="images/city.png"/> <i>По городу (где банк имеет филиал)</i></h4>
<ul class="block_list city_list">
<li><a href="?BankCity=МОСКВА">Москва</a></li>
<li><a href="?BankCity=САНКТ-ПЕТЕРБУРГ"><nobr>Санкт-Петербург</nobr></a></li>
<li><a href="?BankCity=НОВОСИБИРСК">Новосибирск</a></li>
<li><a href="?BankCity=ЕКАТЕРИНБУРГ">Екатеринбург</a></li>
<li><a href="?BankCity=НИЖНИЙ НОВГОРОД"><nobr>Нижний Новгород</nobr></a></li>
<li><a href="?BankCity=САМАРА">Самара</a></li>
<li><a href="?BankCity=РОСТОВ-НА-ДОНУ"><nobr>Ростов-на-Дону</nobr></a></li>
<li><a href="?Cat=cities"><nobr>Все города...</nobr></a></li>
</ul></div>
<div class="general_box clearfix">
<h4><img alt="По размеру" src="images/monets.png"/> <i>По размеру (чистым активам)</i></h4>
<ul class="block_list">
<li><a href="?BankBig=1">Крупней- шие (1-30)</a></li>
<li><a href="?BankBig=2">Крупные (31-100)</a></li>
<li><a href="?BankBig=3">Средние (101-200)</a></li>
<li><a href="?BankBig=4">Небольшие (201-...)</a></li>
</ul></div>
<div class="general_box clearfix">
<h4><img alt="Другие классификации" src="images/alfa.png"/> <i>Другие классификации</i></h4>
<ul class="block_list city_list">
<li><a href="?group_type=user"><b>Пользовательские списки <sup><font color="red">(новое!)</font></sup></b></a></li>
<li><a href="?group_type=aktb">По валюте баланса</a></li>
<li><a href="?group_type=usl">Типы банков по оказываемым услугам</a></li>
<li><a href="?group_type=zvs"><b>Зависимые банки</b></a></li>
<li><a href="?group_type=cbl"><b>Списки Центрального Банка России</b></a></li>
<li><a href="?group_type=rait"><b>Рейтинги банков от рейтинговых агентств</b></a></li>
</ul>
</div>
</div> <div class="clearfix" id="work_banki">
<h2>Добро пожаловать на Портал банковского аналитика!</h2>
<div id="ab_info">
<p>Наш сайт предназначен как для банковских аналитиков, так и для клиентов банков (например, вкладчикам и юридическим лицам для оценки финансового состояния банка, в котором открыт счет).</p>
<p>На сайте в удобном виде представлена обработанная аналитическая информация, собранная из открытых источников (Банк России, рейтинговые агентства, АСВ и другие).</p><br/>
<p><b>Клиенты банков</b> найдут информацию о финансовом состоянии своего банка, которую легко отслеживать каждый месяц сразу после выхода новой отчетности банка. Предлагаем несколько моделей оценки надежности и финансовой стабильности банка. Особое внимание уделено <b><i>индикативному анализу деятельности банка</i></b> для экспресс-диагностики финансового состояния банков, а также <b><i>динамическому и сравнительному анализу на основе рейтингов (ренкингов) банков</i></b>. Если Вы - клиент нескольких банков, то добавив их в Избранные, Вы сможете оперативно получать данные о финансовом состоянии кредитных организаций, а также инсайдерскую информацию.</p><br/>
<p><b>Потенциальные клиенты</b> могут изучить всю доступную информацию о том или ином банке и выбрать надежный банк. Рейтинг банков можно составить на основе любого показателя. Рейтинги обновляются ежемесячно практически сразу после публикации отчетности.</p><br/>
<p><b>Банковским аналитикам</b> предлагаем инструмент для анализа: можно использовать как готовые формулы показателей, так и задать расчет по собственным пользовательским формулам. Все рассчитанные показатели выводятся в виде таблиц или графиков за любой доступный период. Показатели рассчитываются на основе официальной отчетности банков (формы 101, 102, 123, 134, 135). </p><br/>
<p><b>Найдите свой банк</b> в каталоге банков (по алфавиту, по Вашему городу, по размеру банка) или в справочнике БИК. Вам также предоставляются удобные возможности быстрого поиска банков: по буквам из названия банка, по БИК или регистрационному номеру. В разрезе каждого банка можно увидеть структуру баланса (по трем различным группировкам), структуру доходов и расходов, показатели рентабельности, оценить риски ликвидности, достаточности капитала, кредитный и рыночный риски. По каждому банку строится аналитический <b>отчет для оценки финансово-экономического положения банка, его устойчивости и надежности</b>. Состав отчетов на основе различных методик постоянно расширяется. Также по наиболее важным экономическим показателям выводится отчет по <b>позициям банка в рейтингах</b>, позволяющий отследить изменения этих позиций в течение года.</p><br/>
<p><b>Справочник банков и справочник БИК</b> банков обновляется ежедневно. <b>Отчетность банков</b> для анализа загружается ежемесячно. <b>Аналитические расчеты</b> проводятся автоматически ежемесячно, либо по запросу пользователей. Также ежемесячно загружаются различные <a href="bank.php?group_type=cbl">списки Банка России</a> и <a href="bank.php?group_type=rait">рейтинги банков от рейтинговых агентств</a>.</p><br/>
<p><b>В разделе <a href="metod.php">Методики</a></b> описываются методики анализа финансового состояния банков. Там задаются описания показателей и формулы их расчета.</p><br/>
<p><b><a href="rating.php">Рейтинги банков</a></b> могут быть составлены на основе любого показателя. Для этого <a href="rating.php?raiting=bankov">выберите методику</a> и показатель, по которому необходимо построить рейтинг (рэнкинг). <a href="rating.php?raiting=sostav">Составной рейтинг банков</a> используется для формирования табличных рейтингов сразу по нескольким показателям с возможностью отбора банков по заданным критериям.</p><br/>
<p><a href="forum.php?cmd=register">Зарегистрируйтесь</a> на сайте и у Вас появится возможность общаться на форуме, добавлять банки в Избранные, получать оперативную информацию по избранным банкам, смотреть описания и расчетные формулы показателей, а также воспользоваться функцией сравнения банков.</p><br/>
<p><b>Платные услуги</b> предоставляются по двум тарифным планам:</p>
<ul>
<li>"Клиент": Оперативный анализ финансового состояния банка. Просмотр всех аналитических отчетов и рейтингов доступен обычно в течение 30-60 минут после публикации отчетности Центральным Банком России.</li>
<li>"Аналитик": Кроме возможностей тарифного плана "Клиент" Вам предлагается возможность более глубокого профессионального анализа финансового состояния банков по всем доступным методикам.</li>
<li>Проведение аналитических расчетов и исследований на заказ.</li>
</ul>
<p><a class="dashed" href="index.php?ref=reklama">Подробнее о составе предоставляемых услуг...</a></p>
<div class="myinfo">Внимание! С 1 февраля 2019 года платные услуги будут предоставляться в полном объеме! До этого момента действуют скидки на оплату. <a class="button-link" href="forum.php?cmd=pay">Перейти к оплате</a>
</div>
<br/>
<p><b>Статистика по анализируемым данным:</b></p>
<div class="myinfo"><table>
<tr><td>Общее количество кредитных организаций в базе:</td><td><i>1163</i></td></tr>
<tr class="bottborder"><td><sub>в том числе:</sub></td><td><i></i></td></tr>
<tr><td>Организаций, давших согласие на раскрытие информации:</td><td><i>403 (на 01 Декабря 2020 г.)</i></td></tr>
<tr><td>Работающих банков:</td><td><i>367 (на 12 Января 2021 г.)</i></td></tr>
<tr><td>Работающих небанковских кредитных организаций (НКО):</td><td><i>31</i></td></tr>
<tr><td>Кредитных организаций с отозванной лицензией:</td><td><i>658</i></td></tr>
</table></div>
<div class="myinfo">На сайте собрана и приведена в удобный для анализа вид вся доступная отчетность всех банков:
  <table>
<tr class="bottborder"><td><sub>Вид отчетности:</sub></td><td><sub>Данные загружены за период:</sub></td></tr>
<tr><td>Балансы банков (Форма 101).</td><td>с <i>01 Февраля 2004 г.</i> по <i>01 Декабря 2020 г.</i></td></tr>
<tr><td>Отчет о прибылях и убытках (Форма 102).</td><td>с <i>01 Января 2004 г.</i> по <i>01 Октября 2020 г.</i></td></tr>
<tr><td>Расчёт собственных средств (капитала) (Форма 123, до 01.02.2015 - форма 134).</td><td>с <i>01 Июня 2010 г.</i> по <i>01 Декабря 2020 г.</i></td></tr>
<tr><td>Информация об обязательных нормативах (Форма 135).</td><td>с <i>01 Июня 2010 г.</i> по <i>01 Декабря 2020 г.</i></td></tr></table>
</div>
<div class="myinfo"><table><tr><td>Информация о банках обновлена по состоянию на дату:</td><td><i>12 Января 2021 г.</i></td></tr></table></div>
<div class="myinfo"><table><tr><td>Информация о БИКах банков обновлена по состоянию на дату:</td><td><i>13 Января 2021 г.</i></td></tr></table></div>
<p><i><b>Всегда актуальная и только объективная информация о финансовом состоянии банков!</b></i></p>
</div>
</div>
</div> </div>
<div id="footer">
<div id="counters">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24844922 = new Ya.Metrika({id:24844922,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img alt="" src="//mc.yandex.ru/watch/24844922" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t38.10;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
</div>
<a href="index.php">Начальная страница</a> | <a href="index.php?ref=about">О проекте</a> | <a href="index.php?ref=reklama">Реклама и услуги</a> | <a href="index.php?ref=contacts">Контакты</a> <br/><br/>Copyright © 2011-2019 analizbankov.ru</div>
</div>
</body>
</html> 