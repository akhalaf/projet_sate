<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "29097",
      cRay: "610ca668beb8124c",
      cHash: "344497776121b5f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWxwaW5laG9tZWFpci5jb20vdmlld3Byb2R1Y3QuY2ZtP3Byb2R1Y3RJRD00NTMwNTkyNDk=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "n90DxGFXQoFvncPv4vOVmz4y9eNZpkbe7P5FoxxUqWwdRecmfIy7Ds6qE/OMZbd+0OgijNhk19NEibkqcqTp/+RVZKAXLtI84pY7Lhkfphld/hJpJGFTPpWWrJiifA9Sn78pc//ZQ85yi7lM5hpDPjVo51PYs1J/R5mMFt/sOEhxJFwj4229wXRtCpkxlzhM0boIzMN6/X6btKgJma+67GUa1FL5hg8J6GnozupUYlhd0YwTTo7MuF51kWvxf7BD/Y+oqIV9utLlr5MSUrghQtdLV5QpYRnoZ/Ak0JRm454m7434FjwLypgauEASbmZ0iCzZ33bC3APCfNUS5KoSCM/F3tR2ALhAYnR9ct1q0SwxzfXOmYFLiu7AgX2QyptCP6CSfMiCsasdXDA7t3XYCIRzXzKPAnuyOL5nxUufnRyxC6smNqmYVoU9bNmmybL2+zG+swxSVPY9A2BT/XXjldgoJynwAQ8pQDrJPQJLMPFGiRxiT3wwFW0dtmsvFLKDTvzEP62T5QcXU62Xwxsyy6exSz5UY4+BvEjPc9NvCOT2uff7OLj9I+kKp2KPJc4DseRttyki4bjKa7Ue637J9CAMPgebR7rVPCqNtq4PTkiL/g+fkJ3Gh27Cf7p0ZY+rBMYOzLCi8hLIVGlJ7qtFiogYTzLee4sQsF/5b1Wu1//HudR5lHiibL1745xceDaCmVKDZHIMW9kSgPCmrIj3MjVz251E/YExvzTLu7TtHQj/jAPEet8TLbr5rLVhEser",
        t: "MTYxMDUxNTAzNy41NjcwMDA=",
        m: "+aXBpm49ofEWOIy0t73qn4mhaYamKB+yF9GoQ/d99ts=",
        i1: "c75LbRq3bjuks34kGiAa6A==",
        i2: "h2Jr6HpH14PZI9lGH/Jfyw==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "7K1es7YgMK9si6HFUb7Wc7Z7qfmtgWmjKra38OH74OA=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.alpinehomeair.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/viewproduct.cfm?productID=453059249&amp;__cf_chl_captcha_tk__=acbadb6b32c454287a16126bdb5bc4cc839df684-1610515037-0-AZlBd5u1W4tfvHxLRuYkP_FqJyk-S9O9ENW0stlQO5LlbOp4yLFLyMF9fW-WgujrW5oLTXdKlcJOZvnbHAD_RfeKi4rhXlOQIaPkluFRZWGTLyNRygzz19MvzR5PyrbKMaeN6Giu6_Dou-RNrzruPtR7-UeV07y31DLkrLrDck9BckPtRqSCu24_iNMogtLzKqiOGP6GPZ7f0GPwxkdH8FPU6ONEZTmYsekaRcbiv1-7y2xflzjmS1LMe3vyk-Er37Kwa2PmVAgNZEMUQT4TOApqYF_8Z1ax_nJlNnvPYzaFCTj3CvW_GeZdAL1Hi3WePMJN8tFFlTq2l6j6O1_98MCfPK6ORwQ3J4WpUoIxTFb5W3tSELv3u_B60xwImMAs1t8l-_wPQiJ4hshoqiRMm2iB0OXq6AjOcTXKQ6F2Nd1GIAnC5WYW7QUXXyr9U4Uv54VQLiyDWQwr1mNJmG5HTJ-_kY_fObzkHfx_py9cNLFExHMFh3cFfpGyNtgO7gtw3jEWNMUDcoznpPZGqRw3Behm4DoSq-xv-TztkTyFCHXWfrQZ7K_QUzoN0nTL9tMlrA" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="112d57f511043464671833b5f092c340983bf033-1610515037-0-Ab5W20oJWIX6qxSozJB1vT46Wo5fOzvDPYmnVN7OCRZwBVdH24x3cqKn4wtu0UXJijYCdVG5pmvpAYr1JGzcgNH5i4oUDKg7t9CW0CFBSVqxszZfD3z9986/fnIibAnG8DVwQd0bfd8fzlAvy+92mxdWJUjvWJez9VyLkKJnJdOEPi2cQFA9zBK6nGB3/ZonbFiZ0TpsDVZC+4rVEqaT5tmOIRzCqFDXmhuhYSlduT+b1raQslshlbSCMe43UNT9NkuFIE+2HEC4Rj/v73S9RJBgWe46TvjSinVdGoSJiB2JjrROicZkyJ3jadI1nkgzzb8UU0+E8drlAxTeVcNkSbN9ifVzhP1QbgEdCNfAYV9I9y93v/IAUho3iz5J9SVhZiRxzW1MmpV2SXvEmFbaiZgZq40PJY23XrYUWYsIzT3tS47pFEs+zr2TD/8tTdem/c9FWGZbiBFYX+bPUhc1JJLjgeVcKEf9cU+mTa5TwOIsQS+64RhnWkUpj2GfbbAx59gkt0zR4S8rMPqkzDypksdJy91XqpbKFKOmB2YRVgh4+vekPTdWSl33PvXb3XUYTeL4z5uxe136+B1T+RN1t3My9DsRbRdQGiGJiTgvWSm4RZI87gqRF+M/VcyyHEt4EohAaaZ6bD/X4rDZ5QeH1oBfAnOls0Uw7tJNufZSxrKS+v2YQD/5uuIclKCt2BHeIF7vOoYHXcXgSl6Ju87WkCF/7Goa5+F7b51dgazX+H+C1GL3UPhUW7DR++BQewA0K2MMseMgrY2LTAN/bgBSd3VQJ22vWcbI4Qpm9CDKO13ED26Eh8cZ5AQKgxEnGAlePdND7WPMTfnuou1s0xh4oj2mlffSBoFumisNHrzEf7fP31XLEFLIwcYLYgml/DEN7fyJmQ1CvQUQDWP0OvPJtBwz+arQFNGpFRnx4jdYUOnoXKhTEfCoSToLe+vE0fDMEixqujIHNjp41KF7vepdrjyzkXNU8ccFWfmo1zOLsCBmefxQfK53emcE011o3gLz4M0WK2wEwG1sT7Ukuy8lC3z+lzqFZmz71eF4AAVX6j2KJAdKvjx0U22ZTIK2Mkaz/x98j4GLnSXujnjasGnfKFRhvwkiwSFQ+mCLhT6kkAvB4WX2JI8KKBHNuoDiFqe11ikNehJkWCqzqBmp/Hoy4tlLtsYNXynhx3sorNKXfGY5rrdxCsd7akSY1A2qxRkEKiDJ5Ok2EPQ0PtfQntgcXiCqqd6tXOZYHuGier+SdHSEyJJ37huNaCqI3n+FOSRQipxRGSI7RV7z09HUH+NbA9A="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="2c5b3614c00604745b817578365d416b"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610ca668beb8124c')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610ca668beb8124c</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
