<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<title>Just a moment...</title>
<style type="text/css">
    html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
    body {background-color: #ffffff; color: #000000; font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, "Helvetica Neue",Arial, sans-serif; font-size: 16px; line-height: 1.7em;-webkit-font-smoothing: antialiased;}
    h1 { text-align: center; font-weight:700; margin: 16px 0; font-size: 32px; color:#000000; line-height: 1.25;}
    p {font-size: 20px; font-weight: 400; margin: 8px 0;}
    p, .attribution, {text-align: center;}
    #spinner {margin: 0 auto 30px auto; display: block;}
    .attribution {margin-top: 32px;}
    @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
    #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
    #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
    #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
    .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
    a { color: #2c7cb0; text-decoration: none; -moz-transition: color 0.15s ease; -o-transition: color 0.15s ease; -webkit-transition: color 0.15s ease; transition: color 0.15s ease; }
    a:hover{color: #f4a15d}
    .attribution{font-size: 16px; line-height: 1.5;}
    .ray_id{display: block; margin-top: 8px;}
    #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
    #cf-hcaptcha-container { text-align:center;}
    #cf-hcaptcha-container iframe { display: inline-block;}
  </style>
<meta content="12" http-equiv="refresh"/>
<script type="text/javascript">
  //<![CDATA[
  (function(){
    
    window._cf_chl_opt={
      cvId: "1",
      cType: "non-interactive",
      cNounce: "84126",
      cRay: "61108061fc4b17bf",
      cHash: "d992d02dfa79d4f",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuY2FsaWZvcm5pYWJlYWNoZXMuY29tL2F0dHJhY3Rpb25zLw==",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "Dw+kYvwbrkFY8d03L4Z7/zVg8XyPCtHf+Q4QsCPJ2D1vcOUhKdv5R92uvGJjjSViHWhL6RcaZe1Mn1J87YhdynJyvVgfZQKlLNoPe0rBs3iiclQNYVxQl4/lGpDbbl6tU/ICEcBGoQoR/nqG8GkzkawPmmU1QQ1O19TU098iZn9nlC1kxsclSwYb9kx3sicORC4bj69xC/MWyFhmek1DdKwPSx9f5u8Vko6EaSP/kei5BISH+X5MK+PKw0+iZWOmXvA6W72CLJ8qNkSGlSSjiVQmWO6hbGCB+KhTFkZB66mfADJ2Dyynw6XAmT/ELvnmHYjYnqwKNuTrkYLOx3x8x6dtsbCboHY1GOUw7p6jOqZMayv95JNXirRmYUFygUT/G2e7/FU9Q+15XTU3BG5Noj9kGGKOOhA1HXUrulPwu67p5UXGbJaOVdQYbt5xABbvglOqiC2TwSsjRchhpUmtytqmdPb08TMFQdTmmtO/In0nyCQK5xTBpR6oaMoxb6t9fLTknAa6BVk6teYRWQG9+EEg7WzC2d4NtIgPdQIXvN7a+hxAaOY6vav8/X+Wd3nAFdvzqHQHNFRTBeRVe4sypB9hRLA9YPVPkr9cWqs4s3VNN6G+PA/HiL/pP52/7y90c/U8VztM3I/oGXfuBwYDoSXn3U8HOckzuLEBmr2KH5Ubq3KqgwfLh30Vgs1MZ1pShl3FMK3Sj+7YARYTMM2aeUa0mLS8Fp+mRDCNshNQ9f0twquXl//n4Pbnb5ucf5IG",
        t: "MTYxMDU1NTQyMy4wNDAwMDA=",
        m: "o8cD2wm3RUUkzfSHQ/0VcU6gapJIxaweSpbV4Z0OzAk=",
        i1: "o+1xg9pAJaMCkb5js2SGYg==",
        i2: "RWopY6TRt+k+K6pfNR4N9w==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "MO0dV88updyJRe38MLNudPbYbUmrnR2k1jGkljkVG24=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var a = document.getElementById('cf-content');a.style.display = 'block';
      var isIE = /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
      var trkjs = isIE ? new Image() : document.createElement('img');
      trkjs.setAttribute("src", "/cdn-cgi/images/trace/jschal/js"+cookieSupportInfix+"/transparent.gif?ray=61108061fc4b17bf");
      trkjs.id = "trk_jschal_js";
      trkjs.setAttribute("alt", "");
      document.body.appendChild(trkjs);
      
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/jsch/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    
    }, false);
  })();
  //]]>
</script>
</head>
<body>
<table cellpadding="20" height="100%" width="100%">
<tr>
<td align="center" valign="middle">
<div class="cf-browser-verification cf-im-under-attack">
<noscript>
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div id="cf-content" style="display:none">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
<h1><span data-translate="checking_browser">Checking your browser before accessing</span> californiabeaches.com.</h1>
<a href="https://tornado-networks.com/lupineabbey.php?goto=465" style="display: none;">table</a>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<p data-translate="process_is_automatic">This process is automatic. Your browser will redirect to your requested content shortly.</p>
<p data-translate="allow_5_secs">Please allow up to 5 seconds…</p>
</div>
<form action="/attractions/?__cf_chl_jschl_tk__=11ac5a9c9438542b83203bbe112ae9837b5ce251-1610555423-0-AURsPE_uLRIT5yXTCYkMrITrNjpacW10xFYfZ2JAIWpMgXJiwyZhw5P8-LumnJUm-JWFrVzRYB-xvIID0jQxI1Kew55acXRcOIYG2XhM59iJOdROTwFMAhy1jQH4hdEibqDjNISoa9MoSdSoDD0x-Lhp40f15IEItyqiTgHasZvbaVW5gOXCm3ZfYvISIWBpfmBSfD2L1IEIXYASIgmpE8pUYsDJVUx_6j4YnqtzEzwR2o9g_1dyeW9LXGa-fZmsgjKaEbOd_Ti6WTWyM-PUT79P-wZQR9OA9wjt2yATfzNhUumpZk4SMEKRpC6aKYy1Vw" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<input name="r" type="hidden" value="f8095f315532efbe4595a2d598835c75ed0a8a6b-1610555423-0-Aa+EMPpB12pa4vsalUa+K1uDIteEYXGXK4i9+epzEKxcNxyxno21act/eysgkuM69vxqShhVF1NUTTcqN+sks4Ei8OB6yRRCcykvz+ckWqYDGx6Hm01rraZvfjvn+PJT1uGChCAeLYVBUzO3C8jzcS/U8rNSx2dEVN2WqVN8grwGlXaJEKU7qhTljbNdmUtSyd94MRhqjMiMRxpHA7Mer28ChYG4tJwaj01ohJ1JtRjGfp/fPfsIXVb4SeRbWRc2ic2lcG7ViqtJKvnbfqgBDNMnkDYz5jvKTL7KE+Efkoq5ecoFPcHTWjBd5DbrA3eCWzxrno4xM0xlRL5DDHAP4EMk51jdmPDa5u49YT/dkLuse3nfh/rScNxS6p+/FGfKOU2pXfGh1Um5uiQhMnQN9OGo8keU+sLR51p6WOMrKAAkBsC6tft2j98CGwzDNiXH3q8ngSIwYCjf6q3UL10n3xFEBT+OokSqUV0/vv1ft9bIhHjDIEkXBRKbHIop7rqV/j8RC7YGOV3Q47dqV4DlHfXQ+TumIq6b4fX80323YxO6wdVLzOltfm0za6k5d2RF0sJ6C0AtLlsPGLm64iiH1X4j8umOYLeieFpZ+/TLV9Ue2Pd4YlCRAlHw8Z7Sx2AVoBEpGxS1O4Y4PY2geiBcKI8xd4kEx7YRjYLyJ+YxgFy6ylFVZ7QX9ZKCDJmReGAJpxpZ4vpd4CFWJNSg1NnKbdlzk8D6ahxadNKSEu8fhv4wQ6oKWn6Ng+rW0mRs2IzukHCxkK+6h5LRwSZz9e0lgXDwZkh3wjWaLnJockjlQ5xzENsk7oDvqWjf11G5OYFF4Euxp6UozmXJMbF43YksYPgaC4bNSpvpY5EwJyhAJhOaEawsTJe6LQ9th3xAEHGrUP/45zOVJndbKbLDRfSBhGB+XFCd4VRLxdpoXLQf60MSXlvRkeKLUVszwfiuVYeRoUuYNiGPK8z9FKzUH9ewhET9Q0us7BDgxxxbSUoJ6/d3MiY/pGQdyjcuyBLPIxw3pR2FH3I2A0OIA5TKzTbNEgQSilr6bGwEtY9qoCWT+HBed6KhGdPP1nem3pWIj1FF8C5bwYL3/sI3Krv73rTf7Di6WsrdJu49Nm/NFBfkM9hKZuwdBepgnlrynToa4MMznvFdQS7CZskQ7boT4BGJeaWASSbHmkaVF66D1lebAg4KGMlED9z93YpqQThfo/m8YxipoVfH3805LNtExle/214CQ1c4OLnH4bnQallS1gNePx0lhVWia+siXGiEGW1YD22lFdlZaQ/8CkBNA2q3vQW7yYTWcWRpWB6AfoEZ2Hp/UfuBAnl7/d6kwizzUqUZiSNXQmtByJR9zXPXc0itw7s="/>
<input id="jschl-vc" name="jschl_vc" type="hidden" value="96f9250867513bd33f6dbd111b74ec84"/>
<!-- <input type="hidden" value="" id="jschl-vc" name="jschl_vc"/> -->
<input name="pass" type="hidden" value="1610555427.04-ToL5lFlHsW"/>
<input id="jschl-answer" name="jschl_answer" type="hidden"/>
</form>
<div id="trk_jschal_nojs" style="background-image:url('/cdn-cgi/images/trace/jschal/nojs/transparent.gif?ray=61108061fc4b17bf')"> </div>
</div>
<div class="attribution">
            DDoS protection by <a href="https://www.cloudflare.com/5xx-error-landing/" rel="noopener noreferrer" target="_blank">Cloudflare</a>
<br/>
<span class="ray_id">Ray ID: <code>61108061fc4b17bf</code></span>
</div>
</td>
</tr>
</table>
</body>
</html>
