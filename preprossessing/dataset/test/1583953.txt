<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>404 - Page not found</title>
<link href="../uploaded_file/favicon.ico" rel="shortcut icon"/>
<link href="../uploaded_file/favicon.ico" rel="icon"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet"/>
<link href="../assets/icomoon/icomoon.css" rel="stylesheet" type="text/css"/>
<style>
	body {
		margin: 0;
		font-family: 'Open Sans', sans-serif;
		color: #5F5F5F;
	}
    .center-table {
    	display: table;
    	margin: 0 auto;
    	width: 100%;
    	height: 100vh;
    }
    .center-td {
    	display: table-cell;
    	vertical-align: middle;
    	text-align: center;
    }
    .icon {
        font-size: 82px;
		color: #DD6B55;
    }
    h1 { font-size: 32px; }
	</style>
</head>
<body>
<div class="center-table">
<div class="center-td">
<div class="icon"><span class="icon-info"></span></div>
<div>
<h1>Page not found</h1>
<p>404 - Page not found</p>
</div>
</div>
</div>
</body></html>