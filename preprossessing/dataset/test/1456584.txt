<html><head><script language="javascript" src="https://tmswindia.com/javascript/WebUIValidation.js"></script><!DOCTYPE html>

<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>TMS - Administration</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<!-- Bootstrap 3.3.5 -->
<link href="templates/template-files/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Font Awesome -->
<link href="templates/template-files/font-awesome.min.css" rel="stylesheet"/>
<!-- Ionicons -->
<link href="templates/template-files/ionicons.min.css" rel="stylesheet"/>
<!-- Theme style -->
<link href="templates/template-files/dist/css/AdminLTE.min.css" rel="stylesheet"/>
<!-- iCheck -->
<link href="templates/template-files/plugins/iCheck/square/blue.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
</head><body class="hold-transition login-page">
<div class="login-box">
<div class="login-logo" style="color: white;font-weight: 600;">
	Welcome to TMS
      </div>
<div class="login-box-body">
<p class="login-box-msg" style="color: #a94442;"></p>
<p class="login-box-msg">Sign in to start your session</p>
<form id="frmLogin" method="post" name="frmLogin">
<div class="form-group has-feedback">
<input class="form-control" id="txtUserName" name="txtUserName" placeholder="Username" type="text"/>
<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
<input class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" type="password"/>
<span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
<div class="col-xs-8">
<div class="checkbox icheck">
<label>
<input id="chkRememberMe" name="chkRememberMe" type="checkbox" value="1"/> Remember Me
                </label>
</div>
</div>
<div class="col-xs-4">
<button class="btn btn-primary btn-block btn-flat" onclick="document.frmLogin.submit();" type="submit">Sign In</button>
</div>
</div>
</form>
</div>
</div>
<!-- jQuery 2.1.4 -->
<script src="templates/template-files/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="templates/template-files/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="templates/template-files/plugins/iCheck/icheck.min.js"></script>
</body></html>
