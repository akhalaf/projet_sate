<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "46614",
      cRay: "611197b8be12dcce",
      cHash: "5aac4bf7af97b5b",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYm9va2xpa2VzLmNvbS8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "MhnmFsvO543bcE0wlrLma2OrqyJ0ZUdx6XnrPN6x8K3rajy7tg41zdDI6capBxWR3PEGRVeo2VfgLTMfzZvLyFXoA8ocowc8D4zcjIoypVfKNHsRD5icD8ibXDrqW+bztecaTKRla7ZHoi0b0U8y86CffBbAp/zhU/g1WtxxgBDwze36nHVWn2rdRMP05nHeZWRC7qPBKP7UpAQ7oprWM21CLrvgXpx0yzVSANH0OQQzgAjO2mwvJXmPjTVIqOLqaEbfXTM42EWJzQ9sXnqwHAzvI03/N/4qVljs1OA+ZQO3HJLIQpiMk6SQYH2oNa0/7TbdHpOlcAHf0CsZFyi2w/AIGJCaZbH9iugjMNla5nxzeMtabOtRCKRD2psEFMRp7abwg/DLjlZnl4cO//c/2Ujq8mp2JTHs4ckyP+3s69Frmm2l6pzzjn/QE+RO3S2oxeLv4uTxXFp0d1OloReHVld3NEHw6QAU3COWe1oDS/1rxFmrKJKANT/2KsbnogB94AE2zKXWEfyKBR7WK049/QyVpZp0fFaqmQ1oq9w/vVIqah/e+AZW0Gjq/CiNfUsSPEEipsEW5z9mlCz0S4RwrzhFMOQ2HwQaH+B/Yl3kmT+WNFlAY60r82/0ni6j7c7xyeih20dJe4GnH0F/SonKf6vjotl49yDikrCAtvmItxLfpTM/9cLSkAOdV4H4Hi7TiK8O84bktJ2A5dyixUP88kOV9G1MteEG3csuikBH5ZF82/yqVAgYdaQyPmcJZMk5",
        t: "MTYxMDU2Njg2NC43NjEwMDA=",
        m: "7OHFXavalz7NT582XGb09vy3SejTDmFGiA+hhdcpVk0=",
        i1: "2XR7ku0fUth+Xt7U2vrjtg==",
        i2: "H1LqApnsvy3cDwwVCpcXYA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "OlB8JkxI+I8ZrxNDT1LljPNHxoSkHbDKNIOFWuGPcJc=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.booklikes.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=469664df2627826497eeac729ed7dde41b58378f-1610566864-0-AbikeKHZJLV8ZyGTV7T0R_iuv7q_F-E8etzUkeCv54qChQ_iHbVHPVCrhapMwoc-e_jeYrd-LDytPkDkvrKkKrGLdO1LkM41e2Es57fyzkGGkyxo56-X7NkZB9zMDzjDCuLr7Vh1jIHnMuacXkx25NO69Z00c34eNOUqbwFITJtpFGcSIOcSqrX1tq5tFx7jOzy06wnpb7oO7ZfC9QbW2Fd2RKK0zia7e6axbOVA0TV4zROvrKNvk0iQQMBNOe9siyodrLKdg0MNzNZFQL_s1cc_J5jjlTHFpEFBgPWTUwTkVJ_AbUbnuef_fDEEj-fs4EdAN1bQH-IszSh_c36dLrfIAqB9RlTB1RIZbR6NhylqGN0epkmawPMVFBxBGSgvdHYOEBH2Fq8rYqlsKkHFXkXk9egnSLlB0K5-OIoa2leBdtS8umen6UsJdHAzspRvmxkMzn8ANOA3NyM4MZbJWty6Zs13P8vr7DrpSaGcIWF5-O0QrLB4PLggmOaKHap1znWVK6RZ0P_hk91PHmlaZBU" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="24c855fd72560de961b29ac271884dd7cd7c8a58-1610566864-0-ASfopARdHHeQdvdjgjWNMVbXjSIEwDe2X+r49nwFeqFw6UXSQlW9ow0VtEvADR4CtfVUk4nUsxdeCraFU+SDjTPzL+naOA2CIFXrPcv6Behw5Jj+5LZO2J6cgI41Nq5mAEWlJo4hSYHJxMxncyC7fl0GMlpau3C0wQCeMnOpdvspaKMImTwopr8iYdkfPC8WWAa7gnd2MMUHgUJwAPN9ZVHnwzdU7BFyWpaRe74jkEJ/qRC/EOnrTKXQu9wOpn6EcRjVv2N+O7WU6IExabwzlZBbCBnQ7oDRNEkG0chz27oDn0GeS1QzKxNTHtWa8z6HCEusz+jbP6ZmGNX6tK8jlZ3xQGM73gFrxImcFaXhGwu3XGDSIdc9um7kwAK7RCv2YpBFG2KhOQxdMPPhlDjZZM5qIs7D5JCYgaSoBlu+TXp1XJ1IKbuSbOGmGb4wi7hJHKaL3bPfRL2+CZGy6qfb6uvsf7+L/QKFEkjG4TLJZjQh0F7uCk6HWHXGmSms/pXOotAZ3j8uVGoaJP5aiOQ8a1UTMqnQQXDdHOtSnj25mPgfLOa+NsufWgtZ2vLqNpO+I4f1hb1TJkw5zWd31blaiO0syEc4ltSfoNUMNcdq2Y1hFC0iwnlW9yf9/XZr9qQNp70CIetEqeqF+onoR/fgpZTqcRJ40zdcHCx9F6Vv27BIOub9t62e2SvWU1h1oGXop6s3+yYSKCisQph62i7t1d52Su7SaKGkt9LXmUZHy2W7Kz5FagID/BVI8rOVbp3wRhCaemjHN0ydBjdHsO+GnL4yLhmHf7i2DaJGF1GXb5CuYjHo1PBo42pSfslUUyWEAYhRb4EaB0BJ2gmAXtlpfozHEqNxsqmwFYkI+PjSo9uZvdLoQSDEIxfvDMX98DKj3SFtC/D7hjpHepuuFEXTWFGufcJv5EqXLHLLV6z9x1LkUbg2YDFzaOD/urO3k0QxIosO9vEiYr6dX6r3zaPTWMwiVL6l/SOg/45QemV+Gvp+eatvYx671hr62j9cWBnnNbIOUU3wNaW0fUAhH1lJl1XPb7b0MTMoJWrks6ZJorP2j4A7bRByiQ6YLuMmV2csIJStBE7QgXksnJQXQeVHCoXhKJCc9/KvXEJB/pVZfCXw6qPMCOLieQEdJuXHtrEwGtQXE+xNbqd2t2aKQOL2zS/vAGuXWdoCkive9pIBT/UZYYQWCwIcPosj6wpBdb5bWIGVSTkbDKU13GmjFQmESKh8UMFVv8AtKCDn3cPmSKK+ixOu8pmDN2MaCSDL+F62Q/6lW7Cs5Byt5V+We2BFvZDlK9myLhuMvp73rVchynDzctAqrYkUbL7sAbcB6DmQbQ=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="0778f9e380f524ecbca356f1a06cc6f7"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=611197b8be12dcce')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<div style="position: absolute; top: -250px; left: -250px;"><a href="https://yorke-peninsula.net/wincreamy.php?story_pg=756">table</a></div>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">611197b8be12dcce</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:8d2b:d30c:bf6f:658f</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
