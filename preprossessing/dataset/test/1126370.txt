<!DOCTYPE html>
<html class="html" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
<title>Page Not Found - Pakistan Canada Association of Edmonton</title>
<meta content="follow, noindex" name="robots"/>
<meta content="en_US" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Page Not Found - Pakistan Canada Association of Edmonton" property="og:title"/>
<meta content="Pakistan Canada Association of Edmonton" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Page Not Found - Pakistan Canada Association of Edmonton" name="twitter:title"/>
<!-- /Rank Math WordPress SEO plugin -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.pcaedmonton.ca/feed/" rel="alternate" title="Pakistan Canada Association of Edmonton » Feed" type="application/rss+xml"/>
<link href="https://www.pcaedmonton.ca/comments/feed/" rel="alternate" title="Pakistan Canada Association of Edmonton » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.pcaedmonton.ca\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.pcaedmonton.ca/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-includes/css/dist/block-library/theme.min.css?ver=5.6" id="wp-block-library-theme-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/fonts/fontawesome/css/all.min.css?ver=5.11.2" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/css/third/simple-line-icons.min.css?ver=2.4.0" id="simple-line-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/css/third/magnific-popup.min.css?ver=1.0.0" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/css/third/slick.min.css?ver=1.6.0" id="slick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/css/style.min.css?ver=1.8.6" id="oceanwp-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.pcaedmonton.ca/wp-content/plugins/ocean-extra/assets/css/widgets.css?ver=5.6" id="oe-widgets-style-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.pcaedmonton.ca/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.pcaedmonton.ca/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<link href="https://www.pcaedmonton.ca/wp-json/" rel="https://api.w.org/"/><link href="https://www.pcaedmonton.ca/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.pcaedmonton.ca/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><!-- OceanWP CSS -->
<style type="text/css">
/* General CSS */a:hover,a.light:hover,.theme-heading .text::before,#top-bar-content >a:hover,#top-bar-social li.oceanwp-email a:hover,#site-navigation-wrap .dropdown-menu >li >a:hover,#site-header.medium-header #medium-searchform button:hover,.oceanwp-mobile-menu-icon a:hover,.blog-entry.post .blog-entry-header .entry-title a:hover,.blog-entry.post .blog-entry-readmore a:hover,.blog-entry.thumbnail-entry .blog-entry-category a,ul.meta li a:hover,.dropcap,.single nav.post-navigation .nav-links .title,body .related-post-title a:hover,body #wp-calendar caption,body .contact-info-widget.default i,body .contact-info-widget.big-icons i,body .custom-links-widget .oceanwp-custom-links li a:hover,body .custom-links-widget .oceanwp-custom-links li a:hover:before,body .posts-thumbnails-widget li a:hover,body .social-widget li.oceanwp-email a:hover,.comment-author .comment-meta .comment-reply-link,#respond #cancel-comment-reply-link:hover,#footer-widgets .footer-box a:hover,#footer-bottom a:hover,#footer-bottom #footer-bottom-menu a:hover,.sidr a:hover,.sidr-class-dropdown-toggle:hover,.sidr-class-menu-item-has-children.active >a,.sidr-class-menu-item-has-children.active >a >.sidr-class-dropdown-toggle,input[type=checkbox]:checked:before{color:}input[type="button"],input[type="reset"],input[type="submit"],button[type="submit"],.button,#site-navigation-wrap .dropdown-menu >li.btn >a >span,.thumbnail:hover i,.post-quote-content,.omw-modal .omw-close-modal,body .contact-info-widget.big-icons li:hover i,body div.wpforms-container-full .wpforms-form input[type=submit],body div.wpforms-container-full .wpforms-form button[type=submit],body div.wpforms-container-full .wpforms-form .wpforms-page-button{background-color:}.widget-title{border-color:}blockquote{border-color:}#searchform-dropdown{border-color:}.dropdown-menu .sub-menu{border-color:}.blog-entry.large-entry .blog-entry-readmore a:hover{border-color:}.oceanwp-newsletter-form-wrap input[type="email"]:focus{border-color:}.social-widget li.oceanwp-email a:hover{border-color:}#respond #cancel-comment-reply-link:hover{border-color:}body .contact-info-widget.big-icons li:hover i{border-color:}#footer-widgets .oceanwp-newsletter-form-wrap input[type="email"]:focus{border-color:}input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,button[type="submit"]:hover,input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,button[type="submit"]:focus,.button:hover,#site-navigation-wrap .dropdown-menu >li.btn >a:hover >span,.post-quote-author,.omw-modal .omw-close-modal:hover,body div.wpforms-container-full .wpforms-form input[type=submit]:hover,body div.wpforms-container-full .wpforms-form button[type=submit]:hover,body div.wpforms-container-full .wpforms-form .wpforms-page-button:hover{background-color:}table th,table td,hr,.content-area,body.content-left-sidebar #content-wrap .content-area,.content-left-sidebar .content-area,#top-bar-wrap,#site-header,#site-header.top-header #search-toggle,.dropdown-menu ul li,.centered-minimal-page-header,.blog-entry.post,.blog-entry.grid-entry .blog-entry-inner,.blog-entry.thumbnail-entry .blog-entry-bottom,.single-post .entry-title,.single .entry-share-wrap .entry-share,.single .entry-share,.single .entry-share ul li a,.single nav.post-navigation,.single nav.post-navigation .nav-links .nav-previous,#author-bio,#author-bio .author-bio-avatar,#author-bio .author-bio-social li a,#related-posts,#comments,.comment-body,#respond #cancel-comment-reply-link,#blog-entries .type-page,.page-numbers a,.page-numbers span:not(.elementor-screen-only),.page-links span,body #wp-calendar caption,body #wp-calendar th,body #wp-calendar tbody,body .contact-info-widget.default i,body .contact-info-widget.big-icons i,body .posts-thumbnails-widget li,body .tagcloud a{border-color:}/* Header CSS */#site-header.transparent-header{background-color:rgba(10,0,0,0.23)}#site-header.has-header-media .overlay-header-media{background-color:rgba(0,0,0,0.5)}#site-logo #site-logo-inner a img,#site-header.center-header #site-navigation-wrap .middle-site-logo a img{max-width:89px}#site-header #site-logo #site-logo-inner a img,#site-header.center-header #site-navigation-wrap .middle-site-logo a img{max-height:118px}#site-logo a.site-logo-text{color:#ffffff}#site-logo a.site-logo-text:hover{color:#ba0e45}#site-navigation-wrap .dropdown-menu >li >a,.oceanwp-mobile-menu-icon a,#searchform-header-replace-close{color:#ffffff}#site-navigation-wrap .dropdown-menu >li >a:hover,.oceanwp-mobile-menu-icon a:hover,#searchform-header-replace-close:hover{color:#f2e8a9}#site-navigation-wrap .dropdown-menu >.current-menu-item >a,#site-navigation-wrap .dropdown-menu >.current-menu-ancestor >a,#site-navigation-wrap .dropdown-menu >.current-menu-item >a:hover,#site-navigation-wrap .dropdown-menu >.current-menu-ancestor >a:hover{color:#ffffff}/* Typography CSS */#site-logo a.site-logo-text{font-weight:700}#site-navigation-wrap .dropdown-menu >li >a,#site-header.full_screen-header .fs-dropdown-menu >li >a,#site-header.top-header #site-navigation-wrap .dropdown-menu >li >a,#site-header.center-header #site-navigation-wrap .dropdown-menu >li >a,#site-header.medium-header #site-navigation-wrap .dropdown-menu >li >a,.oceanwp-mobile-menu-icon a{letter-spacing:2px;text-transform:uppercase}
</style></head>
<body class="error404 wp-embed-responsive oceanwp-theme sidebar-mobile has-transparent-header no-header-border default-breakpoint content-full-width content-max-width has-breadcrumbs elementor-default" itemscope="itemscope" itemtype="https://schema.org/WebPage">
<div class="site clr" id="outer-wrap">
<a class="skip-link screen-reader-text" href="#main">Skip to content</a>
<div class="clr" id="wrap">
<div class="clr" id="transparent-header-wrap">
<header class="transparent-header clr" data-height="74" id="site-header" itemscope="itemscope" itemtype="https://schema.org/WPHeader" role="banner">
<div class="clr container" id="site-header-inner">
<div class="clr" id="site-logo" itemscope="" itemtype="https://schema.org/Brand">
<div class="clr" id="site-logo-inner">
<a class="site-title site-logo-text" href="https://www.pcaedmonton.ca/" rel="home">Pakistan Canada Association of Edmonton</a>
</div><!-- #site-logo-inner -->
</div><!-- #site-logo -->
<div class="clr" id="site-navigation-wrap">
<nav class="navigation main-navigation clr" id="site-navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" role="navigation">
<ul class="main-menu dropdown-menu sf-menu" id="menu-main"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-58" id="menu-item-58"><a class="menu-link" href="http://www.pcaedmonton.ca"><span class="text-wrap">Home</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-59 nav-no-click" id="menu-item-59"><a class="menu-link" href="https://www.pcaedmonton.ca/about/"><span class="text-wrap">About <span class="nav-arrow fa fa-angle-down"></span></span></a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289" id="menu-item-289"><a class="menu-link" href="https://www.pcaedmonton.ca/president-pcaes-message/"><span class="text-wrap">President’s Message</span></a></li> <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-348" id="menu-item-348"><a class="menu-link" href="https://www.pcaedmonton.ca/home/pcae-execs/"><span class="text-wrap">PCAE Executive Council</span></a></li> <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311" id="menu-item-311"><a class="menu-link" href="https://www.pcaedmonton.ca/past-pcae-presidents/"><span class="text-wrap">Past PCAE Presidents</span></a></li> <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290" id="menu-item-290"><a class="menu-link" href="https://www.pcaedmonton.ca/about/"><span class="text-wrap">About Us</span></a></li></ul>
</li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60" id="menu-item-60"><a class="menu-link" href="https://www.pcaedmonton.ca/events/"><span class="text-wrap">Programs / Events</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61" id="menu-item-61"><a class="menu-link" href="https://www.pcaedmonton.ca/yellowpages/"><span class="text-wrap">Bookings</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-65" id="menu-item-65"><a class="menu-link" href="https://www.pcaedmonton.ca/contact/"><span class="text-wrap">Services <span class="nav-arrow fa fa-angle-down"></span></span></a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-358" id="menu-item-358"><a class="menu-link" href="https://www.pcaedmonton.ca/for-new-comers/"><span class="text-wrap">For New Comers</span></a></li></ul>
</li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-319" id="menu-item-319"><a class="menu-link" href="https://www.pcaedmonton.ca/contact/"><span class="text-wrap">Contact</span></a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-378" id="menu-item-378"><a class="menu-link" href="https://www.pcaedmonton.ca/membership/"><span class="text-wrap">Membership</span></a></li></ul>
</nav><!-- #site-navigation -->
</div><!-- #site-navigation-wrap -->
<div class="oceanwp-mobile-menu-icon clr mobile-right">
<a aria-label="Mobile Menu" class="mobile-menu" href="#">
<i aria-hidden="true" class="fa fa-bars"></i>
<span class="oceanwp-text">Menu</span>
</a>
</div><!-- #oceanwp-mobile-menu-navbar -->
</div><!-- #site-header-inner -->
</header><!-- #site-header -->
</div>
<main class="site-main clr" id="main" role="main">
<header class="page-header">
<div class="container clr page-header-inner">
<h1 class="page-header-title clr" itemprop="headline">404: Page Not Found</h1>
<nav aria-label="Breadcrumbs" class="site-breadcrumbs clr position-" itemprop="breadcrumb"><ol class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList"><meta content="2" name="numberOfItems"/><meta content="Ascending" name="itemListOrder"/><li class="trail-item trail-begin" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a aria-label="Home" href="https://www.pcaedmonton.ca" itemprop="item" itemtype="https://schema.org/Thing" rel="home"><span itemprop="name"><span class="icon-home"></span><span class="breadcrumb-home has-icon">Home</span></span></a><span class="breadcrumb-sep">&gt;</span><meta content="1" itemprop="position"/></li><li class="trail-item trail-end" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a href="#" itemprop="item" span=""><span itemprop="name"><span class="breadcrumb-error">404 Not Found</span></span></a><meta content="2" itemprop="position"/></li></ol></nav>
</div><!-- .page-header-inner -->
</header><!-- .page-header -->
<div class="container clr" id="content-wrap">
<div class="content-area clr" id="primary">
<div class="clr site-content" id="content">
<article class="entry clr">
<div class="error404-content clr">
<h2 class="error-title">This page could not be found!</h2>
<p class="error-text">We are sorry. But the page you are looking for is not available.<br/>Perhaps you can try a new search.</p>
<form action="https://www.pcaedmonton.ca/" class="searchform" method="get" role="search">
<label for="ocean-search-form-1">
<span class="screen-reader-text">Search for:</span>
<input autocomplete="off" class="field" id="ocean-search-form-1" name="s" placeholder="Search" type="search"/>
</label>
</form>
<a class="error-btn button" href="https://www.pcaedmonton.ca/">Back To Homepage</a>
</div><!-- .error404-content -->
</article><!-- .entry -->
</div><!-- #content -->
</div><!-- #primary -->
</div><!-- #content-wrap -->
</main><!-- #main -->
<footer class="site-footer" id="footer" itemscope="itemscope" itemtype="https://schema.org/WPFooter" role="contentinfo">
<div class="clr" id="footer-inner">
<div class="oceanwp-row clr" id="footer-widgets">
<div class="footer-widgets-inner container">
<div class="footer-box span_1_of_4 col col-1">
</div><!-- .footer-one-box -->
<div class="footer-box span_1_of_4 col col-2">
</div><!-- .footer-one-box -->
<div class="footer-box span_1_of_4 col col-3 ">
</div><!-- .footer-one-box -->
<div class="footer-box span_1_of_4 col col-4">
</div><!-- .footer-box -->
</div><!-- .container -->
</div><!-- #footer-widgets -->
<div class="clr no-footer-nav" id="footer-bottom">
<div class="container clr" id="footer-bottom-inner">
<div class="clr" id="copyright" role="contentinfo">
				Copyright - OceanWP Theme by Nick			</div><!-- #copyright -->
</div><!-- #footer-bottom-inner -->
</div><!-- #footer-bottom -->
</div><!-- #footer-inner -->
</footer><!-- #footer -->
</div><!-- #wrap -->
</div><!-- #outer-wrap -->
<a class="scroll-top-right" href="#" id="scroll-top"><span aria-label="Scroll to the top of the page" class="fa fa-angle-up"></span></a>
<div id="sidr-close">
<a aria-label="Close mobile Menu" class="toggle-sidr-close" href="#">
<i aria-hidden="true" class="icon icon-close"></i><span class="close-text">Close Menu</span>
</a>
</div>
<div class="clr" id="mobile-menu-search">
<form action="https://www.pcaedmonton.ca/" aria-label="Search for:" class="mobile-searchform" method="get" role="search">
<label for="ocean-mobile-search2">
<input autocomplete="off" name="s" placeholder="Search" type="search"/>
<button aria-label="Submit Search" class="searchform-submit" type="submit">
<i aria-hidden="true" class="icon icon-magnifier"></i>
</button>
</label>
</form>
</div><!-- .mobile-menu-search -->
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.pcaedmonton.ca\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.pcaedmonton.ca/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2" type="text/javascript"></script>
<script id="imagesloaded-js" src="https://www.pcaedmonton.ca/wp-includes/js/imagesloaded.min.js?ver=4.1.4" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/js/third/magnific-popup.min.js?ver=1.8.6" type="text/javascript"></script>
<script id="oceanwp-lightbox-js" src="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/js/third/lightbox.min.js?ver=1.8.6" type="text/javascript"></script>
<script id="oceanwp-main-js-extra" type="text/javascript">
/* <![CDATA[ */
var oceanwpLocalize = {"isRTL":"","menuSearchStyle":"disabled","sidrSource":"#sidr-close, #site-navigation, #mobile-menu-search","sidrDisplace":"1","sidrSide":"left","sidrDropdownTarget":"link","verticalHeaderTarget":"link","customSelects":".woocommerce-ordering .orderby, #dropdown_product_cat, .widget_categories select, .widget_archive select, .single-product .variations_form .variations select","ajax_url":"https:\/\/www.pcaedmonton.ca\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="oceanwp-main-js" src="https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/js/main.min.js?ver=1.8.6" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.pcaedmonton.ca/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='https://www.pcaedmonton.ca/wp-content/themes/oceanwp/assets/js/third/html5.min.js?ver=1.8.6' id='html5shiv-js'></script>
<![endif]-->
</body>
</html>
