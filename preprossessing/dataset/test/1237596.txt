<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="https://www.rigid.co.id/"/>
<title>Home « PT Rigid PVC</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-type"/>
<meta content="en-US" http-equiv="content-language"/>
<meta content="" name="keywords"/>
<meta content="" name="description"/>
<link href="themes/rigid/css/img/favicon.png" rel="shortcut icon"/>
<link href="themes/rigid/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="themes/rigid/css/typography-front.css" rel="stylesheet" type="text/css"/>
<link href="themes/rigid/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="themes/rigid/css/other-custom.css" rel="stylesheet" type="text/css"/>
<link href="themes/rigid/css/slider.css" rel="stylesheet" type="text/css"/>
<script src="themes/rigid/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="themes/rigid/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="themes/rigid/js/sliderman.1.3.7.js" type="text/javascript"></script>
</head>
<body>
<div id="body_wrapper_2">
<div id="body_wrapper">
<div id="header_wrapper">
<div class="fullwidth" id="header">
<!-- Type Menu On Left -->
<a href="https://www.rigid.co.id/" id="home_link">
<img alt="LogoRMB.png" src="/assets/Uploads/LogoRMB.png"/>
</a>
<div id="menu">
<ul>
<li class="first current ">
<a class="home" href="/">Home</a>
</li>
<li class=" link has_child">
<a href="/about-us/">About Us</a>
<ul class="sub_menu level_2">
<li class="first link">
<a href="/about-us/iso-certificates/">ISO Certificates</a>
</li>
<li class="last link">
<a href="/about-us/certificates/">Certificates</a>
</li>
</ul>
</li>
<li class=" link ">
<a href="/product-applications/">Product Applications</a>
</li>
<li class=" link has_child">
<a href="/recruitment/">Recruitment</a>
<ul class="sub_menu level_2">
<li class="first link">
<a href="/recruitment/qaqc-manager/">QA/QC MANAGER</a>
</li>
<li class=" link">
<a href="/recruitment/qa-and-qc-engineer/">QA &amp; QC ENGINEER </a>
</li>
<li class=" link">
<a href="/recruitment/production-operator-5/">PRODUCTION OPERATOR</a>
</li>
<li class=" link">
<a href="/recruitment/production-manager/">PRODUCTION MANAGER</a>
</li>
<li class=" link">
<a href="/recruitment/it-progammer/">IT PROGRAMMER cum. IT ENGINEER (SURABAYA/MOJOKERTO)-2 POSITIONS</a>
</li>
<li class="last link">
<a href="/recruitment/truck-driver-cum-forklift-operator/">TRUCK DRIVER cum FORKLIFT OPERATOR</a>
</li>
</ul>
</li>
<li class="last link ">
<a href="/contact-us/">Contact Us</a>
</li>
</ul>
<div class="clear"></div>
</div>
<div class="clear"></div>
<!-- Type Menu On Left -->
</div>
</div>
<div id="content_wrapper">
<div class="fullwidth" id="content">
<div class="padding40" id="home">
<div id="slideshow_wrapper">
<div id="slideshow">
<img height="360" src="assets/Uploads/rigid-slideshow/Slide-1-edit.jpg" width="960"/>
<img height="360" src="assets/Uploads/rigid-slideshow/Slide-2-edit.jpg" width="960"/>
<img height="360" src="assets/Uploads/rigid-slideshow/Slide-3-edit.jpg" width="960"/>
<img height="360" src="assets/Uploads/rigid-slideshow/Slide-4-edit.jpg" width="960"/>
</div>
<div id="slide_show_nav"></div>
</div>
<script>
      //SLIDE SHOW ANIMATION
      Sliderman.effect({name: 'devtrix01', cols: 10, rows: 5, fade: true, order: 'swirl', delay: 50});
      Sliderman.effect({name: 'devtrix02', cols: 10, rows: 5, fade: true, order: 'swirl', road: 'TL', reverse: true, delay: 50});
      Sliderman.effect({name: 'devtrix03', cols: 1, rows: 1, duration: 500, fade: true});
      Sliderman.effect({name: 'devtrix04', cols: 1, rows: 6, duration: 500, fade: true, top: true});
      Sliderman.effect({name: 'devtrix05', cols: 10, rows: 1, duration: 500, fade: true, left: true});
      Sliderman.effect({name: 'devtrix06', cols: 9, rows: 3, delay: 50, duration: 500, fade: true, right: true});
      Sliderman.effect({name: 'devtrix07', cols: 9, rows: 3, delay: 50, chess: true, duration: 500, fade: true, bottom: true});
      Sliderman.effect({name: 'devtrix08', cols: 6, rows: 3, delay: 70, duration: 400, move: true, top: true});
      Sliderman.effect({name: 'devtrix09', cols: 8, rows: 4, delay: 70, duration: 400, move: true, top: true, road: 'TL', order: 'straight'});
      Sliderman.effect({name: 'devtrix10', cols: 10, rows: 5, delay: 40, duration: 500, fade: true, road: 'TL', order: 'straight_stairs'});
      Sliderman.effect({name: 'devtrix11', cols: 10, rows: 5, delay: 40, duration: 500, fade: true, road: 'BR', order: 'straight_stairs'});
      Sliderman.effect({name: 'devtrix12', cols: 10, rows: 5, delay: 10, fade: true, order: 'straight_stairs'});
      Sliderman.effect({name: 'devtrix13', cols: 10, rows: 5, delay: 50, duration: 500, fade: true, road: 'TR', order: 'snake'});
      Sliderman.effect({name: 'devtrix14', cols: 10, rows: 5, delay: 30, duration: 500, fade: true, road: 'RB', order: 'snake'});
      Sliderman.effect({name: 'devtrix15', cols: 10, rows: 5, delay: 30, duration: 500, fade: true, road: 'LT', order: 'snake'});
      Sliderman.effect({name: 'devtrix16', cols: 6, rows: 1, duration: 400, fade: true, move: true, left: true});
      Sliderman.effect({name: 'devtrix17', cols: 1, rows: 4, duration: 400, fade: true, move: true, top: true});
      Sliderman.effect({name: 'devtrix18', cols: 10, rows: 5, fade: true, delay: 10, duration: 400});
      Sliderman.effect({name: 'devtrix19', fade: true, duration: 500, move: true, top: true});
      Sliderman.effect({name: 'devtrix20', fade: true, duration: 400, move: true, left: true});
      Sliderman.effect({name: 'devtrix21', fade: true, duration: 400, move: true, right: true});
      Sliderman.effect({name: 'devtrix22', fade: true, duration: 500, move: true, bottom: true});
      Sliderman.effect({name: 'devtrix23', cols: 10, delay: 100, duration: 400, order: 'straight', bottom: true, road: 'RB', fade: true});
      Sliderman.effect({name: 'devtrix24', rows: 8, delay: 100, duration: 400, order: 'straight', left: true, fade: true, chess: true});
      Sliderman.effect({name: 'devtrix25', cols: 10, delay: 100, duration: 500, order: 'straight', right: true, move: true, zoom: true, fade: true});
      Sliderman.effect({name: 'devtrix26', rows: 7, cols: 14, fade: true, easing: 'swing', order: 'cross', delay: 100, duration: 400});
      Sliderman.effect({name: 'devtrix27', rows: 7, cols: 14, fade: true, easing: 'swing', order: 'cross', delay: 100, duration: 400, reverse: true});
      Sliderman.effect({name: 'devtrix28', rows: 7, cols: 14, fade: true, easing: 'swing', order: 'rectangle', delay: 200, duration: 1000});
      Sliderman.effect({name: 'devtrix29', rows: 7, cols: 14, fade: true, easing: 'swing', order: 'rectangle', delay: 200, duration: 1000, reverse: true});
      Sliderman.effect({name: 'devtrix30', rows: 7, cols: 10, zoom: true, move: true, right: true, easing: 'swing', order: 'circle', delay: 150, duration: 800});
      Sliderman.effect({name: 'devtrix31', rows: 7, cols: 10, zoom: true, move: true, left: true, easing: 'swing', order: 'circle', delay: 150, duration: 800, reverse: true});
      Sliderman.effect({name: 'devtrix32', rows: 7, cols: 1, zoom: true, move: true, bottom: true, easing: 'bounce', order: 'circle', delay: 150, duration: 800});
      Sliderman.effect({name: 'devtrix33', rows: 7, cols: 1, zoom: true, move: true, top: true, easing: 'bounce', order: 'circle', delay: 150, duration: 800, reverse: true});
      devtrixEffects = ['devtrix01','devtrix02','devtrix03','devtrix04','devtrix05','devtrix06','devtrix07','devtrix08','devtrix09','devtrix10','devtrix11','devtrix12','devtrix13','devtrix14','devtrix15','devtrix16','devtrix17','devtrix18','devtrix19','devtrix20','devtrix21','devtrix22','devtrix23','devtrix24','devtrix25','devtrix26','devtrix27','devtrix28','devtrix29','devtrix30','devtrix31','devtrix32','devtrix33'];
			var sliderman = Sliderman.slider({
			  container: 'slideshow',
        width: 960,
        height: 360,
        effects: devtrixEffects,
        contentmode: true,
				display: {
				  pause: true, // slider pauses on mouseover
					autoplay: 3000,
          //always_show_loading: 200, // testing loading mode
					loading: {background: '#000000', opacity: 0.5, image: 'themes/rigid/css/img/loading.gif'},
					buttons: {hide: true, opacity: 1, prev: {className: 'slide_left_button', label: ''}, next: {className: 'slide_right_button', label: ''}},
					description: {hide: false, background: '#000000', opacity: 0.4, height: 'auto', position: 'bottom'},
					navigation: {container: 'slide_show_nav', label: '<img src="themes/rigid/css/img/clear.gif" />'}
				}
			});
      </script>
<div class="three_cols_main_wrapper">
<div class="three_cols_main first_img">
<h1>About <span class="red_text">Us</span></h1>
<p style="text-align: justify;"><span>PT. Rigid Maju Bersama is the first Rigid PVC Film manufacturing company in East Java, Indonesia. We manufacture a wide range of Rigid PVC Films for meeting the diverse requirements of many industries, such as food and beverage, pharmaceutical, etc. Our products are fabricated to deliver excellent performance while providing unequalled durability and cost-effectiveness. We have invested 6-roll PVC calendar machine as proclaimed to be the most sophisticated machine in Indonesia. We have been certified with <strong>ISO 9001:2015</strong> and <strong>ISO 14001:2015</strong> for Manufacturing of Rigid PVC Film.</span></p>
<p> </p>
<p> </p>
<p> </p>
<a class="cols_nav" href="http://www.rigid.co.id/about-us/"> More</a>
</div>
<div class="three_cols_main">
<h1>Our <span class="red_text">Product</span></h1>
<p style="text-align: justify;">Providing a wide range of Rigid PVC films for food packaging as well as non-food packaging, such as cosmetic, gift, blister, electronic and other specialised packaging. This material is ensured by food grade non-toxic formula, and also supported by the high standard of cleanliness and hygiene factory environment. In addition, this material is high impact resistance, high clarity, tasteless and odourless and also excellent thermoforming properties for both deep and shallow draw.</p>
<p> </p>
<p> </p>
<p> </p>
<a class="cols_nav" href="http://www.rigid.co.id/product-applications/"> More</a>
</div>
<div class="three_cols_main last_img">
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div id="footer_wrapper">
<div class="fullwidth" id="footer">
<p class="copyright">
      Copyright 2021 PT Rigid PVC
    </p>
<p class="copyright">
      Web Developed by <a href="http://www.crosstechno.com" target="_blank">CrossTechno</a><br/>
</p>
</div>
</div>
</div><!--end of body wrapper-->
</div><!--end of body wrapper 2-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-19521957-40', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
