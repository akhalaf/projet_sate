<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Elite Force  </title>
<meta content="" name="description"/>
<meta content="" name="title"/>
<meta content="innovaCMS" name="author"/>
<meta content="s2p5kNR6yH7O_GSqQ2QnpuoH4kAd58zXGxSzZXgtsH8" name="google-site-verification"/>
<meta content="Elite Force, Guard Services in Bangladesh, Professoinal Guard Services in Bangladesh, Armed Guard in Bangladesh, Close Protection in Bangladesh, Security Guard Company in Bangladesh " name="keywords"/>
<meta content="width=device-width, initial-scale=1.0,shrink-to-fit=no" name="viewport"/>
<meta content="innovaCMS" name="generator"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/images/favicon.ico" rel="icon" type="image/png"/>
<link href="https://www.elitebd.com" hreflang="en-us" rel="alternate"/>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon"/>
<link href="assets/images/apple/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="assets/images/apple/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="assets/images/apple/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="assets/images/apple/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="assets/images/apple/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="assets/images/apple/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="assets/images/apple/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
<link href="assets/images/apple/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900" rel="stylesheet" type="text/css"/>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Elite force theme  css start from here -->
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/jssor.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/owl.carousel.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/slick.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/lightbox.min.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/lightbox.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/styleee45.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/responsive.css" rel="stylesheet"/>
<link href="https://www.elitebd.com/themes/elite-force-new-06/assets/css/slicknav.css" rel="stylesheet"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-156447259-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156447259-1');
</script>
<style>
		
.menu-holder ul li a i {
    margin-right: 7px;
    font-size: 13px;
}

.management-image-holder {
    float: left;
    width: 33%;
    clear: left;
}

.bio-details {
    width: 64%;
    margin-left: 3%;
    margin-bottom:40px;
}

img.size-medium.wp-image-56 {
    width: 100%;
}

.right-p-image{
        float: right;
    width: 19%;
    margin-left: 22px;
  clear:both;
    }
.home-left p{
    float:left;
    }
    
    .image-none{
        background:none;
        display:none;}
        
        
.testimonials {
  overflow: hidden;
  position: relative;
  min-height: 320px;
}

.testimonials {
  background: #161d25;
}

.one-slide,
.testimonial,
.message {
  border: none !important;
  outline: none !important;
}

.icon-overlay {
  position: absolute;
  opacity: 0.3;
  right: 10%;
  top: 0;
  height: auto;
  width: 100%;
  max-width: 400px;
}

.carousel-controls .control {
  position: absolute;
  transform: translateY(-50%);
  width: 45px;
  height: 45px;
  /*border-radius: 50%; */
 /* border: 2px solid #fff;*/
  z-index: 1;
}



/* 
Testimonial Custom Styles

*/


h2.pt-2.text-center.font-weight-bold {
    text-align: center;
}

.col-sm-12 {
    padding: 20px;
}

.message.text-center.blockquote.w-100 {
    color: #fff;
    padding: 15px 53px;
    text-align: center;
}
  
  
  
.control.d-flex.align-items-center.justify-content-center.prev.mt-3.slick-arrow>i.fa {
    
    text-align: center;

}

.control.d-flex.align-items-center.justify-content-center.prev.mt-3.slick-arrow, .control.d-flex.align-items-center.justify-content-center.next.mt-3.slick-arrow {
    text-align: center;
    color: #dd213d;
 line-height: 45px;

}


.blockquote-footer.w-100.text-white {
    color: #dd213d;
    font-size: 18px;
    text-align: center;
}

.embed-maphub-logo {
display:none !important;

}


/* Servoce {portfolio section */

.services-portfolio-section {
    display: flex;
    justify-content: space-between;
    min-width: 100%;
    flex-wrap:wrap;
    flex-direction:row;
    
}

.services-portfolio-section .single-service {
    height: 200px;
    min-width: 200px;
    border-radius: 50%;
    margin-bottom: 40px;
    border: 1px solid #cccc;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    background: #ccc;
}

.services-portfolio-section .single-service a {
    font-size: 50px;
    display: flex;
    flex-direction:column;
    height: 100%;
    width: 100%;
    justify-content:center;
    align-items:center;
    text-decoration:none;
    color:#444;
    transition:0.5s;
    transition-timing-function: ease-in-out;
    border-radius:50%;
        -moz-box-shadow:    inset 0 0 20px #000000;
   -webkit-box-shadow: inset 0 0 20px #000000;
   box-shadow:         inset 0 0 10px #000000;
}

.services-portfolio-section .single-service a p {
    /* display: block; */
    font-size: 14px;
    font-weight: bold;
    margin-top: 10px;
}


.services-portfolio-section .single-service a:hover {
background: #dd213d;
color: #fff;
-moz-box-shadow:    inset 0 0 20px #000000;
-webkit-box-shadow: inset 0 0 20px #000000;
box-shadow:         inset 0 0 10px #000000;
}


.menu-holder ul li a i {
    margin-right: 8px;
}

div.caption {
    background: #181818;
    width: 100%;
    color: #fff;
    padding: 5px;
    text-align: center;
    font-weight: bold;
    margin-bottom: 20px;
}

</style>
</head>
<body onload="footerDateF()">
<div id="fb-root"></div>
<script async="" crossorigin="anonymous" defer="" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v3.3&amp;appId=1708165239492716&amp;autoLogAppEvents=1"></script>
<script data-cfasync="false" type="text/javascript">window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'be74b7f2-94c5-4255-bcc0-4674707e7306', f: true }); done = true; } }; })();</script>
<!-- Header -->
<!--************************************
Wrapper Start
*************************************-->
<div class="container" id="body-wrapper">
<div class="container-fluid mContainer e-header-bar" id="wrapper">
<div class="row">
<div class="col-2"></div>
<div class="col-8">
<div class="row banner">
<div class="col-3">
<div class="logo">
<a href="/"><img alt="Elite Force Logo" src="https://www.elitebd.com/themes/elite-force-new-06/assets/images/logo.png"/></a>
</div>
</div>
<div class="col-3"></div>
<div class="col-6 top-link">
<div class="hotline">
<h3 class="call-title">Hotline <span>Numbers</span></h3>
<span>+8801944474444</span><span>+8801944464444</span></div>
<div class="solial-media">
<a href="https://www.facebook.com/elitebd" target="_blank"> <i class="fa fa-facebook"></i></a>
<a href="https://www.linkedin.com/company/elite-force" target="_blank"> <i class="fa fa-linkedin"></i></a>
<a href="https://www.youtube.com/channel/UCudyjPDpknbsNIs27iAYS9w" target="_blank"> <i class="fa fa-youtube"></i></a>
</div>
<p class="slogan"><a href="">going beyond security</a></p>
</div>
</div>
</div>
<div class="row">
<div style="clear:both"></div>
<div class="menu-holder">
<button class="menu_toggle_btn" id="toggle"><i class="fa fa-reorder" style="font-size:16px;color:#fff"></i></button>
<div id="menu">
<div class="search-holder">
<!--  
                <form action="" method="get" id="searchform">
                  <input name="q" id="search" class="search" type="text"  autocomplete="on"/>
                  <input type="submit" class="search-button" value="" />
                </form> -->
</div>
<div class="menu-main-menu-container" id="listmenu">
<ul class="menu primary-list" id="menu-main-menu">
<li class="toggle"><a href="/"><i class="fa fa-home" style="font-size:14px;color:#fff"></i> Home</a></li>
<li class=""><a href="about-us" tabindex="0"><i aria-hidden="true" class="fa fa-bullhorn"></i>Who We Are</a>
<ul class="sub-menu">
<li><a href="about-us">About Us</a></li>
<li><a href="history-growth">History &amp; Growth</a></li>
<li><a href="management-team">Management Team</a></li>
<li><a href="deployment">Deployment</a></li>
<li><a href="why-us">Why Us?</a></li>
<li><a href="partnership-affiliation">Partnerships &amp; Affiliations</a></li>
<li><a href="awards-recognition">Awards &amp; Recognitions</a></li>
<li><a href="take-a-tour">Take a Tour</a></li>
</ul>
</li>
<li class="" id=""><a href="#" tabindex="0"><i aria-hidden="true" class="fa fa-shield"></i> What We Do</a>
<ul class="sub-menu">
<li class="serviceportfolios" id=""><a href="#">Service Portfolio</a>
<ul class="sub-menu">
<li class="" id=""><a href="security-guard-services-bangladesh">Manned Services</a></li>
<li class="" id=""><a href="cash-carrying-services">Cash Services</a></li>
<li class="" id=""><a href="investigation-services-bangladesh">Investigation Services</a></li>
<li class="" id=""><a href="special-services">Special Services</a></li>
<li class="" id=""><a href="technical-services">Technical Services</a></li>
<li class="" id=""><a href="facility-services">Facility Services</a></li>
<li class="" id=""><a href="executive-protection-services">Executive Protection Services</a></li>
</ul>
</li>
<li class="" id=""><a href="training">Training</a></li>
<li class="" id=""><a href="monitoring-supervision">Monitoring &amp; Supervision</a></li>
<li class="" id=""><a href="special-ops">Special Ops</a></li>
<li class="" id=""><a href="csr">CSR</a></li>
<!--   <li id="" class=""><a href="security-awareness-tips">Security Awareness Tips</a></li> -->
<li class="" id=""><a href="bangladesh-security-situation">Country Security Situation</a></li>
</ul>
</li>
<li class="" id=""><a href=""><i aria-hidden="true" class="fa fa-handshake-o"></i>Our Clients</a>
<ul class="sub-menu">
<li class="" id=""><a href="clientele">Clientele</a></li>
<li class="" id=""><a href="international-clients">International clients</a></li>
<li class="" id=""><a href="clients-testimonial">Client Testimonials</a></li>
</ul>
</li>
<li class="" id=""><a href=""><i aria-hidden="true" class="fa fa-camera"></i> News &amp; Media</a>
<ul class="sub-menu">
<!-- <li id="" class=""><a href="downloads">Downloads</a></li> -->
<li class="" id=""><a href="#">Media Gallery</a>
<ul class="sub-menu">
<li class="" id=""><a href="photo-gallery"><i aria-hidden="true" class="fa fa-picture-o"></i>Photo Gallery</a></li>
<li class="" id=""><a href="video-gallery">Video Gallery</a></li>
<!--  <li id="" class=""><a href="press-clip">Press Clips</a></li> -->
</ul>
</li>
<!--     <li id="" class=""><a href="security-alert">Security Alerts</a></li> -->
<!--   <li id="" class=""><a href="corporate-news-achievements">Corporate News &#038; Achievements</a></li>-->
</ul>
</li>
<li class="" id=""><a href=""><i aria-hidden="true" class="fa fa-volume-control-phone"></i>
Contact Us</a>
<ul class="sub-menu">
<li class="" id=""><a href="get-in-touch">Get in Touch</a></li>
<li class="" id=""><a href="location-address">Locations &amp; Addresses</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Content -->
<!--************************************
				Inner Banner Start
		*************************************-->
<div class="mContainer" id="wrapper">
<div class="title-holder">
<h1>Page Not Found</h1>
<div class="title-img-holder"><img src="https://www.elitebd.com/themes/elite-force-new-06/assets/images/Facility-Services.jpg"/></div>
<div class="breadcrumbs" id="breadcrumbs">
<ul><li class="first breadfirst"><a href="#">Home</a></li> </ul>
</div> </div>
<!--************************************
        Inner Banner End
*************************************-->
<!--************************************
        Main Start
*************************************-->
<main class="tg-main tg-haslayout" id="tg-main">
<div class="mContainer">
<div class="row not-found-links">
<div class="col-one-third">
<h2>Who We Are </h2>
<div class="links">
<ul class="sub-menu">
<li class="" id=""><a href="about-us">About Us</a></li>
<li class="" id=""><a href="history-growth">History &amp; Growth</a></li>
<li class="" id=""><a href="management-team">Management Team</a></li>
<li class="" id=""><a href="deployment">Deployment</a></li>
<li class="" id=""><a href="why-us">Why Us?</a></li>
<li class="" id=""><a href="partnership-affiliation">Partnerships &amp; Affiliations</a></li>
<li class="" id=""><a href="awards-recognition">Awards &amp; Recognitions</a></li>
<li class="" id=""><a href="take-a-tour">Take a Tour</a></li>
</ul>
</div>
</div>
<div class="col-one-third">
<h2> Our Works  </h2>
<div class="links">
<ul class="sub-menu">
<li class="" id=""><a href="#">Service Portfolio</a>
<ul class="sub-menu">
<li class="" id=""><a href="security-guard-services-bangladesh">Manned Services</a></li>
<li class="" id=""><a href="cash-carrying-services">Cash Services</a></li>
<li class="" id=""><a href="investigation-services-bangladesh">Investigation Services</a></li>
<li class="" id=""><a href="special-services">Special Services</a></li>
<li class="" id=""><a href="technical-services">Technical Services</a></li>
<li class="" id=""><a href="facility-services">Facility Services</a></li>
</ul>
</li>
<li class="" id=""><a href="training">Training</a></li>
<li class="" id=""><a href="monitoring-supervision">Monitoring &amp; Supervision</a></li>
<li class="" id=""><a href="special-ops">Special Ops</a></li>
<li class="" id=""><a href="csr">CSR</a></li>
<li class="" id=""><a href="security-awareness-tips">Security Awareness Tips</a></li>
<li class="" id=""><a href="bangladesh-security-situation">Country Security Situation</a></li>
</ul></div>
</div>
<div class="col-one-third">
<h2>Our Clients</h2>
<div class="links">
</div>
</div>
</div>
</div>
</main>
<!--************************************
        Main End
*************************************-->
<!-- Footer -->
<div class="container-fluid">
<!--************************************
				Footer Start
*************************************-->
</div></div>
<div class="container-fluid" id="footer">
<div class="footer-holder footer-container">
<div class="row footer-top">
<div class="col-8">
<div class="row footer-widget-container">
<div class="col-3">
<ul class="f-n">
<li><a href="#">Home</a></li>
<li><a href="#">Who We Are</a>
<ul>
<li><a href="/about-us">About Us </a></li>
<li><a href="/history-growth">History &amp; Growth</a></li>
<li><a href="/management-team">Management Team </a></li>
<li><a href="/deployment">Deployment </a></li>
<li><a href="/why-us">Why Us?</a></li>
<li><a href="/partnership-affiliation">Partnerships &amp; Affiliations</a></li>
<li><a href="/awards-recognition">Awards &amp; Recognitions</a></li>
<li><a href="/take-a-tour">Take a Tour</a></li>
</ul>
</li>
</ul>
</div>
<div class="col-3">
<ul class="f-n">
<li><a href="">Our Work</a>
<ul><li class="s_portfolio"><a href="/security-guard-services-bangladesh">Service Portfolio</a>
<div class="portfolio_links">
<a href="security-guard-services-bangladesh">Manned Services</a>
<a href="cash-carrying-services">Cash Services</a>
<a href="investigation-services-bangladesh">Investigation Services</a>
<a href="special-services">Special Services</a>
<a href="technical-services">Technical Services</a>
<a href="facility-services">Facility Services</a>
<a href="executive-protection-services">Executive Protection Services</a>
</div>
</li>
<li><a href="/training">Training</a></li>
<li><a href="/monitoring-supervision">Monitoring &amp; Supervision </a></li>
<li><a href="/special-ops">Special Ops</a></li><li><a href="/csr">CSR</a></li>
<li></li></ul>
</li>
<li><a href="#">Our Clients</a>
<ul>
<li><a href="/clientele">Clientele</a></li>
<li><a href="/international-clients">International clients</a></li>
<li><a href="/clients-testimonial">Client Testimonials</a></li>
</ul>
</li>
</ul>
</div>
<div class="col-3">
<ul class="f-n">
<li><a href="#">News &amp; Media</a>
<ul>
</ul>
</li>
<li><a href="#">Media Gallery</a>
<ul>
<li><a href="/photo-gallery">Photo Gallery</a></li>
<li><a href="/video-gallery">Video Gallery</a></li></ul>
</li>
<li><a href="/location-address">Contact Us</a></li>
</ul>
</div>
<div class="col-3">
<ul class="f-n">
<li><a href="#">Quick Downloads</a>
<div class="">
<ul>
<li>
<a href="/company-profile" style="text-transform:capitalize;" target="_blank">Company Profile</a>
</li>
</ul>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row">
<div class="footer-bottom-left col-8">
<div class="">
<img alt="logo" src="https://www.elitebd.com/themes/elite-force-new-06/assets/images/footer-logo.png"/>
<p>© <b> <span><script>document.write(new Date().getFullYear())</script></span> Elite Security Services Ltd (Elite Force)</b></p>
<p class="footer-menu"><b>Concept &amp; Design: IT Department, Elite Force</b></p>
</div>
</div>
</div>
</div>
<div class="scrollTop" id="stop">
<span><a href="">
<i class="fa fa-2x fa-angle-up"></i>
</a></span>
</div>
</div>
<!-- Elite force theme js start from here ! -->
<script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization","name":"Elite Force","url":"https://www.elitebd.com","address":"Elite Tower, House # 3 Road # 6/A, Block-J, Baridhara, Dhaka-1212","sameAs":["https://www.facebook.com/elitebd/","https://www.linkedin.com/company/elite-force/"]}</script>
<!-- JSON-LD markup generated by Google Structured Data Markup Helper. -->
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Elite Force",
  "image" : [ "https://www.elitebd.com/themes/elite-force-new-06/assets/images/Elite-Force-Guard-Smiling.jpg", "https://www.elitebd.com/themes/elite-force-new-06/assets/images/Elite-Force-Event-Guard.jpg", "https://www.elitebd.com/themes/elite-force-new-06/assets/images/Elite-Force-Guard-Parade.jpg" ],
  "telephone" : [ "9885141", "8835342", "8835354", "019 444 74444, 019 444 64444" ],
  "email" : "wecare@elitebd.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "Elite Tower, House # 3 Road # 6/A, Block-J Baridhara, Dhaka-1212"
  },
  "url" : "https://www.elitebd.com/location-address"
}
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/jquery-library.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/jquery.slicknav.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/owl.carousel.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/vendor/elite-force.js"></script>
<script src="https://www.elitebd.com/themes/elite-force-new-06/assets/js/app.js"></script>
<!-- Initialize Swiper -->
<script>
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      slidesPerView: 4,
      loop: true,
      freeMode: true,
      loopedSlides: 5, //looped slides should be the same
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
      loop:true,
      loopedSlides: 5, //looped slides should be the same
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs,
      },
    });
  </script>
<script>
	$(function(){
		$('#listmenu').slicknav();
	});
	

	
</script>
<script>
$(document).ready(function(){
  
  $('.menu_toggle_btn').click(function() {
    $('#menu').toggleClass('menuOpened');
    
  });
  
  
    $(".testimonial-carousel").slick({
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    arrows: true,
    fade:true,
    speed:1500,
    prevArrow: $(".testimonial-carousel-controls .prev"),
    nextArrow: $(".testimonial-carousel-controls .next")
  });
  

  
   // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");
    
    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  }); // click() scroll top EMD

  /*************************************
    LEFT MENU SMOOTH SCROLL ANIMATION
   *************************************/
  // declare variable
  var h1 = $("#h1").position();
  var h2 = $("#h2").position();
  var h3 = $("#h3").position();

  $('.link1').click(function() {
    $('html, body').animate({
      scrollTop: h1.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link2').click(function() {
    $('html, body').animate({
      scrollTop: h2.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link3').click(function() {
    $('html, body').animate({
      scrollTop: h3.top
    }, 500);
    return false;

  }); // left menu link3 click() scroll END

  
  
  
  new WOW().init();
  
  
});




// Free Site Map Generator


var fixBreadCrumb = document.querySelector('.breadcrumb li a');

fixBreadCrumb.setAttribute('href', '#');



</script>
</body>
</html>