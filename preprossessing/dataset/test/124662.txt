<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "64349",
      cRay: "610dcade6c802362",
      cHash: "96d77f8d6c5e909",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYW5pbWVseXJpY3MuY29tL2pwb3Ava2F0dHVuL3NpZ25hbC5odG0=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "7kAGeS/OFixT0uCUoKDumYQQYZp1pVF3C9atLZEqYntClFOsi6JNOOZI95igQszcIB93yPiNnDljoyqZpJEe/TBDnsdhYR5phMJEIB5+Q0QZywmkXnRC9v24bw3thGXGoXi8rCmIj2xqiROSl2++c1t6mc4Ii0/7Fbjit2LLMhEhdEn39Bb/VKlZoKjX9L7yJjOlqmkizW4LvT3bFj5Df8cZNGamwzQSk6RNyQ/rwUEjWPxepMj5DQLRwp6wD/MQR8sp7yRLTf3TtMO2sC0J3fbsA5NQ/FUB1AetG2igkFOaCfy5ujIVXZwzrm84hFy7r5sf4v9Zb6UMfrrQkg8OJa06BJAZ/SO9bIhq+IbZOL93FyieqsoupRqhlEW5fppWHSAoaucLwIRZvTq7xoxAjL1Z+uK9KDNuGuWHczcQh9S/7+H3vxbzI22zh+0zyQF4JtRvOnsnlxhpCFDFvMzNFS+lZSoeOOZoYLcRlpKCRVylDa++8VOAOjKz3wNSIzyYd3/hh+j62K+mBoaJAZOuUN5RDhA/iT6JmjftpI8HL3V2zfhjm5WXTAs/9iqbPGxHQR8MrtmAvft3ct5/oE8hF6sUmlx9vur33BmySMvsIyHCgq9rdoXCBYBTAyx21M5443Ew+YXXyaB9zvNBu6VUUL5iNRoRUz5XQg2ChYRuTgPryrXMYNXEURlxgJiH9ZpfPSESi14XksGJqNsIUERd7WaWyMT7l8zxd/wlheIBu6R6lQRGUxSXASBUsWdEkmmM",
        t: "MTYxMDUyNzAxNi43MDcwMDA=",
        m: "xTeCdAiPKXlrCv7t883dk6tpxOSUY1fjXY5dTDQDglk=",
        i1: "DdS1SkQnhePWKu5Ybv67iQ==",
        i2: "BPTpeb29Xd1vhdlYskM2sQ==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "DDyL0IQNxmRdtTss4zAmYXiwHb+a6njG4Si8OlXEFGo=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.animelyrics.com</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/jpop/kattun/signal.htm?__cf_chl_captcha_tk__=ccd08eace18c64b9995fbc67ccc7a0d02582300b-1610527016-0-AXQ0w-Rmme6wRrL0DlD4K3yfAZcjUZLn8wN4N-5C5nNBOEXwEF8xvXO78vJh3ii0BttUHOk1mdTo4h5TjIisI6hgUFdhybCQ5dpF0alhi8bjYCJgJudH95KcIdYQXRxuCZGMX60PfrzaARe_Do63yNnzB0Q8AvWzeDK-3Gkt3s20fiLbJ217WZ8o662x0w006xCToWUjPgrAL0Yzb6hQ5pZOmBK2Pg5ldMzgzTJzJnpJ04o4bRFgrltN1ffi4i2U9WDlBOXXo5vhzwQSzKmf0nia4Z6MSDxSTMhCx8Ql7jq4M2dElkE901allInIELidHqzovgfrTMoQUSPZpZdUwh5XKu7iG6YpAF4rjFl9X06c_dtj9JL81HDw4_Su-k-M24PIAhJb55yOzMgh6JHzT6cCJ17nJy29zbk1Bi5Ot0sxLzpzcUe_6iKGHTfQGNaX2bw0ljLnHQYD_EK-HaJgLCka6A0Wsdeb1jpBUTzowquagJ-7Gbl940XUqfAdg_VHiWEPIgN9tKFWq5r23VAhNCKolkCpjuq8AzdsMRuNPCs_" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="45b9fa9262fa0afa8655ec3af39f8138065dbf20-1610527016-0-AU/PddRR4uZaJROUe8LvXBiYqzB9fHgl/bh84GTAd156XmPUVgvhekQqByREpG1uKhZkCOt+5NYoPgXqC2/Ghe8Q81C60d9bV1SFGA63T3refcEMuQdJ05GkLMkCsu8H0By6Y18MNw2zZxOVvCp9748Xs1tUjWLcOGKD+Sljn3lOqABDWpqmvgdO4cIk3VBuHFKIVqIfQSqSfVoQkXhj4+s7binWMdxQTSCpIMO3Io7bZqikf60MrImmT5E1+3Bbbkf30RCnv1qsr91Cmze491clbMhlyd5o/LzZUUBcChqetqWxjKZ1iMX5yCMurSSe/MmKDvcwB65l/5+5GttlmojsnvWFQcgWoCVCpbTHfA+zX3FoQPyKnDpAb8/PqsB/rjl8NQslunxS/FNcXniaujDXTmtcJtPE92CEkesaO9rcsoY3Df96cfV9nLUN+htJnSQKmcjNdngbG0hndPG9klO/gxdU07Gp7J4y+JQV1ktP1g+ucX7p07rID60QKb5efnuxmsqfcY3pqd0jPosmMfSoMDtCCrNCC1uozfoK+Cz1w9ikQyosJBajs7aLqRoX3ymGbPCQcUJ6El5eNY8pqV05pOyjdmrOsEihykvXsj6Xu/w5jkAPDG5i6DDQOhk/ZVTOKC1ooVQXf1wC/k6QD5sbnlBcYxDGc50BI9tql3ABZpZmPCtKErJSk06yVrEYV5JkJen2dbcnw8UxOYPh0UN2VaTGS0QsThbQrQZ7+q2SwvmF/wwSpxEckr/MBG4GnrBCNS3faReOlx7doDcIaccOlllS1XnBjpLKrLESApxhrmMASigb7zpBolkMLLa/L/P2NStggtt1WpbcX25bmMqOjug6tG+bcgwexThVAEEfOeNVQdG4gC9iL5C9lwu+uK7/lB1EVbofqPmgLDfMY/67FvZlYBws05+umj/qKTmggbfXAqGdr3Z37ZPQzCDxyPix9kc4bs7uw6f6u0nhNnIqVg+hFjlN988owAlCcraj/lxgEKo2ewJV//MBbGvkoNxfxnZwFFk6lE7Qto78cKlhVv5hf3PHT/BwIVZSWx1hCUD/b3lvu/frM/SZKDIiw3UH0qEC/v900kkVKKkDQeXREPNqMKAzpP2MkGx9o5/63voeuv6V+0/Vme1j/Pn+RC54xDah7rGjhMAwyJ6ZiNsEFVWFGKaYe6MdiAJPwwamwp2ezKfqnxyxWY93ALKV/UwdAoD6WdVHj/Qy7hYMXTCz1E2demi4fbOwiPXOXDPWccmt/Om5AgTli3rytzMWpB0tCZktCrNtY+feq9z7t2UBEJp9fndGUrthRN9cz9Ey"/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="fc92081d5556f0cd3734e236ab9f3e55"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610dcade6c802362')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610dcade6c802362</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 210.75.253.169</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
