<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
<meta content="Добро пожаловать! Я — Антон БИТМАП Черкасов — частный веб-дизайнер, Frontend-разработчик, UI-дизайнер. Ознакомьтесь с моим портфолио! Хорошие цены, качественные услуги." name="description"/>
<meta content="eBkRqoA5gCWa5xgRxvhIa1XHAOAX34X2bFQuB5Y9om8" name="google-site-verification"/>
<meta content="62aa95159d93afc6" name="yandex-verification"/>
<style type="text/css">@-ms-viewport{width: device-width;}</style>
<title>Сайт-портфолио частного веб дизайнера фрилансера. Услуги и цены.</title>
<link href="/templates/bit/css/layers.min.css" media="screen" rel="stylesheet"/>
<link href="/templates/bit/css/font-awesome.min.css" media="screen" rel="stylesheet"/>
<link href="/templates/bit/style.css" media="screen" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
		<script  src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
<script src="/templates/bit/js/jquery.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<link href="/bit.ico" rel="icon"/>
<link href="/bit.ico" rel="SHORTCUT ICON"/>
<link href="/templates/bit/img/apple-touch-icon.html" rel="apple-touch-icon"/>
<link href="/templates/bit/img/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76"/>
<link href="/templates/bit/img/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120"/>
<link href="/templates/bit/img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152"/>
</head>
<body>
<header class="transparent light" role="banner">
<div class="row">
<div class="nav-inner row-content buffer-left buffer-right even clear-after">
<div id="brand"><a href="/" title="Сайт-портфолио частного веб-дизайнера"></a></div>
<a href="#" id="menu-toggle"><i class="fa fa-bars fa-lg"></i></a>
<nav><ul class="reset" role="navigation"><li class="menu-item"><a href="/about.html" title="О себе">О себе</a></li><li class="menu-item"><a href="/uslugi.html" title="Услуги">Услуги</a><ul class="sub-menu"><li><a href="/uslugi/sozdanie-sajta-pod-klyuch.html" title="Создание сайта «под ключ»">Создание сайта «под ключ»</a></li><li><a href="/uslugi/sozdanie-sajta-kataloga.html" title="Создание сайта-каталога">Создание сайта-каталога</a></li><li><a href="/uslugi/sozdanie-sajta-vizitki.html" title="Создание сайта-визитки">Создание сайта-визитки</a></li><li><a href="/integraciya-html-shablona-na-sistemu-upravleniya-sajtom.html" title="Интеграция HTML шаблона на CMS">Интеграция HTML шаблона на CMS</a></li><li><a href="/uslugi/verstka-html-shablona.html" title="Верстка HTML шаблона сайта">Верстка HTML шаблона сайта</a></li><li><a href="/uslugi/dizajn-reklamnoj-poligrafii.html" title="Дизайн рекламной полиграфии">Дизайн рекламной полиграфии</a></li><li><a href="/uslugi/books.html" title="Дизайн и верстка брошюр и каталогов">Дизайн и верстка брошюр и каталогов</a></li><li><a href="/uslugi/listovki.html" title="Дизайн и верстка листовок и буклетов">Дизайн и верстка листовок и буклетов</a></li><li><a href="/firmennyj-stil-pod-klyuch.html" title="Фирменный стиль под ключ">Фирменный стиль под ключ</a></li><li><a href="/uslugi/photo.html" title="Услуги предметной фотографии">Услуги предметной фотографии</a></li></ul></li><li class="menu-item"><a href="/portfolio.html" title="Портфолио">Портфолио</a><ul class="sub-menu"><li><a href="/portfolio/web-design.html" title="Веб-дизайн">Веб-дизайн</a></li><li><a href="/portfolio/identy.html" title="Фирменный стиль">Фирменный стиль</a></li><li><a href="/portfolio/poly.html" title="Полиграфия">Полиграфия</a></li><li><a href="/portfolio/photo.html" title="Фотография">Фотография</a></li></ul></li><li class="menu-item"><a href="/articles.html" title="Статьи">Статьи</a></li><li class="menu-item"><a href="/kontakty.html" title="Контакты">Контакты</a></li></ul></nav>
</div><!-- row-content -->
</div><!-- row -->
</header>
<main class="wave_b_b" role="main">
<div class="wave_b_w" id="intro-wrap">
<div class="preload" data-autoplay="5000" data-navigation="true" data-pagination="true" data-transition="fade" id="intro">
<div class="intro-item" style="background-color: #2B7FB3;">
<div class="intro-mockup-wrapper">
<div class="caption-mockup caption-left column five">
<div class="title">Создание сайтов. Веб дизайн</div>
<p>Качественный веб-дизайн, грамотная и валидная верстка шаблонов и структуры, безопасная быстрая и удобная система управления веб-сайтом, поддержка сопровождение и продвижение.</p>
<div class="clear"></div>
<a class="button white transparent" href="#sites">Подробней об услуге</a>
</div><!-- caption -->
<div class="intro-mockup intro-right column seven last">
<img alt="Создание сайтов. Веб дизайн" src="/uploads/images/uslugi/pc.png"/>
</div><!-- intro-mockup -->
</div><!-- intro-mockup-wrapper -->
</div>
<div class="intro-item" style="background-color: #15afbd;">
<div class="intro-mockup-wrapper">
<div class="caption-mockup caption-left column five">
<div class="title">Дизайн и верстка рекламной полиграфии</div>
<p>Соврменно, качественно, в рамках фирменного стиля, под возможности любой типографии могу разработать дизайн, сверстать и подготовить к печати любую полиграфическую продукцию.</p>
<div class="clear"></div>
<a class="button white transparent" href="#poly">Подробней об услуге</a>
</div><!-- caption -->
<div class="intro-mockup intro-right column seven last">
<img alt="" src="/uploads/images/uslugi/poly.png"/>
</div><!-- intro-mockup -->
</div><!-- intro-mockup-wrapper -->
</div>
<div class="intro-item" style="background-color: #706193;">
<div class="intro-mockup-wrapper">
<div class="caption-mockup caption-left column five">
<div class="title">Айдентика - создание фирменного стиля</div>
<p>Качественно и в короткие сроки могу разработать для вашей компании весь необходимый спектр элементов фирменного стиля: логотип, товарный знак, деловую документацию, гайдлайны, брендбуки и пр.</p>
<a class="button white transparent" href="#identy">Подробней об услуге</a>
</div><!-- caption -->
<div class="intro-mockup intro-right column seven last">
<img alt="" src="/templates/bit/img/intro-mockup-06.png"/>
</div><!-- intro-mockup -->
</div><!-- intro-mockup-wrapper -->
</div>
<div class="intro-item" style="background-color: #2C2F30;">
<div class="intro-mockup-wrapper">
<div class="caption-mockup caption-left column six">
<div class="title">Рекламная фотография. <br/>Ретушь и цветокоррекция</div>
<p>
                                Качественно и с постановочным светом произвожу выездную рекламную фотографию по Москве и Московской области с последующей обработкой и цветокоррекцией под любые требования заказчика.
                                </p>
<a class="button white transparent" href="#photo">Подробней об услуге</a>
</div><!-- caption -->
<div class="intro-mockup intro-right column six last">
<img alt="Рекламная фотография" src="/uploads/images/uslugi/photo.png"/>
</div><!-- intro-mockup -->
</div><!-- intro-mockup-wrapper -->
</div>
</div><!-- intro -->
</div><!-- intro-wrap -->
<div class="page" id="main">
<section class="row section call-to-action">
<div class="row-content buffer even">
<h1>Сайт-портфолио <strong>частного веб-дизайнера</strong></h1>
<p>
							Добро пожаловать! Я — Антон "БИТМАП" Черкасов —  частный веб-мастер, дизайнер.<br/>Специализируюсь на Frontend / UI дизайне для WEB, айдентике, полиграфии, фотографии.<br/>Занимаюсь комплексным созданием "легких" и удобных сайтов с адаптированной версткой.
</p><br/><br/>
<a class="button red" href="/uslugi.html">Мои услуги</a> <a class="button red" href="/portfolio.html">Портфолио</a>
</div>
</section>
<section class="row section wave_t_w wave_b_w">
<div class="row-content buffer clear-after">
<h4><em>Свежие работы</em></h4>
<div class="grid-items portfolio-section preload">
<article class="item column six" data-groups='["6"]'>
<figure><img alt="Сайт SurgicalTools.ru" src="/uploads/SITES/SurgicalTools/thumbnail/surgicaltools_icon-c400x300.jpg"/></figure>
<a class="overlay" href="/portfolio/web-design/sajt-surgicaltoolsru.html">
<div class="overlay-content">
<h2>Сайт SurgicalTools.ru</h2>
<p>Веб-дизайн, разработка сетки и интерфейсов, программирование и наполнение адаптивного сайта-каталога хирургических инструментов.... </p> </div><!-- overlay-content -->
</a><!-- overlay -->
</article>
<article class="item column three" data-groups='["6"]'>
<figure><img alt="Создание сайта MedMente.ru" src="/uploads/SITES/Medmente/thumbnail/icon-c400x300.jpg"/></figure>
<a class="overlay" href="/portfolio/web-design/medmente_ru.html">
<div class="overlay-content">
<h2>Создание сайта MedMente.ru</h2>
</div><!-- overlay-content -->
</a><!-- overlay -->
</article>
<article class="item column three" data-groups='["6"]'>
<figure><img alt="Создание интернет-магазина Fotolavka.ru" src="/uploads/SITES/Fotolavka/thumbnail/icon-c400x300.jpg"/></figure>
<a class="overlay" href="/portfolio/web-design/internet-magazin-fotolavkaru.html">
<div class="overlay-content">
<h2>Создание интернет-магазина Fotolavka.ru</h2>
</div><!-- overlay-content -->
</a><!-- overlay -->
</article>
<article class="item column three" data-groups='["6"]'>
<figure><img alt="Создание сайта KAMTEC.RU" src="/uploads/SITES/Kamtec/thumbnail/kamtec_icon-c400x300.jpg"/></figure>
<a class="overlay" href="/portfolio/web-design/kamtecru.html">
<div class="overlay-content">
<h2>Создание сайта KAMTEC.RU</h2>
</div><!-- overlay-content -->
</a><!-- overlay -->
</article>
<article class="item column three" data-groups='["6"]'>
<figure><img alt="Создание сайта Apriele.ru" src="/uploads/SITES/Apriele/thumbnail/icon-c400x300.jpg"/></figure>
<a class="overlay" href="/portfolio/web-design/sozdanie-sajta-aprieleru-portfolio-dizajnera-sajtov.html">
<div class="overlay-content">
<h2>Создание сайта Apriele.ru</h2>
</div><!-- overlay-content -->
</a><!-- overlay -->
</article>
<div class="shuffle-sizer three"></div>
</div><!-- grid-items -->
</div>
</section>
<section class="row section wave_t_w wave_b_w" id="start">
<div class="row-content buffer even clear-after" id="sites">
<div class="column six">
<h2>Веб-дизайн. Создание разработка и сопровождение веб-сайтов</h2>
<p>С 2005 года занимаясь веб-дизайном, и держа руку на пульсе сайтостроения, я предлагаю своим клиентам качественный веб-дизайн, грамотную сборку на быстрой, безопасной и удобной системе управления, валидную верстку с учетом SEO-трендов, дальнейшую поддержку и сопровождение.<br/>Как комплексно, так и в отдельности можно заказать:</p>
<ul>
<li><a href="/uslugi/sozdanie-sajta-pod-klyuch.html" title="Создание сайта под ключ">Создание сайта "под ключ"</a></li>
<li><a href="/uslugi/sozdanie-sajta-vizitki.html" title="Создание сайтов-визиток">Создание сайтов-визиток</a></li>
<li><a href="/uslugi/sozdanie-sajta-kataloga.html" title="Создание сайтов-каталогов">Создание сайтов-каталогов</a></li>
<li><a href="/uslugi/verstka-html-shablona.html" title="CSS-HTML верстку шаблонов">CSS-HTML верстку шаблонов</a></li>
<li><a href="/integraciya-html-shablona-na-sistemu-upravleniya-sajtom.html" title="Интеграцию готового шаблона на систему управления сайтом">Интеграцию готового шаблона на систему управления сайтом</a></li>
</ul>
<a class="button aqua" href="/portfolio/web-design.html" title="">Смотреть работы</a>  <a class="button red" href="/kontakty.html" title="">Скачать бриф</a>
</div>
<div class="side-mockup right-mockup animation">
<img alt="Создание сайтов. Портфолио дизайнера" src="/uploads/images/uslugi/webdesign.jpg"/>
</div>
</div>
</section>
<section class="row section wave_t_w" id="two">
<div class="row-content buffer even clear-after" id="poly">
<div class="column six push-six last-special">
<h2>Дизайн и верстка рекламной полиграфии</h2>
<p>С 1995 года я работаю в полиграфической индустрии. Слежу за трендами в области дизайна полиграфии, технологий печатных процессов, разбираюсь в материалах и оборудовании, законах типографики и верстки, владею всеми необходимыми графическими редакторами и профильными программами. Как комплексно, так и в отдельности у меня можно заказать:</p>
<ul>
<li><a href="/uslugi/dizajn-reklamnoj-poligrafii.html" title="Создание рекламной полиграфии">Создание рекламной полиграфии</a></li>
<li><a href="/uslugi/books.html" title="Дизайн и верстка брошюр и каталогов">Дизайн и верстка брошюр и каталогов</a></li>
<li><a href="/uslugi/listovki.html" title="Дизайн и верстка листовок и буклетов">Дизайн и верстка листовок и буклетов</a></li>
<li><a href="/uslugi/dizajn-reklamnoj-poligrafii.html" title="Дизайн сувенирной полиграфии">Дизайн сувенирной полиграфии</a></li>
<li><a href="/uslugi/dizajn-reklamnoj-poligrafii.html" title="Макетирование наружной рекламы">Макетирование наружной рекламы</a></li>
</ul>
<a class="button aqua" href="/portfolio/poly.html" title="">Смотреть работы</a>  <a class="button red" href="/kontakty.html" title="">Скачать бриф</a>
</div>
<div class="side-mockup left-mockup animation">
<img alt="Дизайн полиграфии. Портфолио дизайнера" src="/uploads/images/uslugi/poly.jpg"/>
</div>
</div>
</section>
<section class="row section wave_t_g wave_b_g" id="tree" style="background-color: #ccc5b9;">
<div class="row-content buffer even clear-after" id="identy">
<div class="column six">
<h2>Айдентика. Разработка логотипов и других элементов фирменного стиля</h2>
<p>Создавая более совершенный фирменный стиль или проводя ребрендинг, вы сообщаете своим потребителям о том, что ваша компания растет и укрепляет позиции. А если вы начинаете бизнес "с нуля" то вам просто необходим грамотный и современный логотип и его минимальная адаптация под популярные носители. <br/> Как комплексно, так и по-отдельности произвожу:</p>
<ul>
<li><a href="/firmennyj-stil-pod-klyuch.html" title="Создание фирменного стиля">Создание фирменного стиля</a></li>
<li><a href="/firmennyj-stil-pod-klyuch.html" title="Создание логотипа">Создание логотипа</a></li>
<li><a href="/firmennyj-stil-pod-klyuch.html" title="Создание гайдлайна">Создание гайдлайна</a></li>
<li><a href="/firmennyj-stil-pod-klyuch.html" title="Разработку брендбука">Разработку брендбука</a></li>
<li><a href="/firmennyj-stil-pod-klyuch.html" title="Дизайн сувенирной продукции">Дизайн сувенирной продукции</a></li>
</ul>
<a class="button aqua" href="/portfolio/identy.html" title="Портфолио фирменный стиль">Смотреть работы</a>  <a class="button red" href="/kontakty.html" title="">Скачать бриф</a>
</div>
<div class="side-mockup right-mockup animation" style="width: 900px;">
<img alt="Айдентика. Фирменный стиль. Портфолио дизайнера" src="/uploads/images/uslugi/identity_mockup.jpg"/>
</div>
</div>
</section>
<section class="row section" id="four">
<div class="row-content buffer even clear-after" id="photo">
<div class="column six push-six last-special">
<h2>Рекламная фотография. <br/>Ретушь и цветокоррекция</h2>
<p>Ни одна продающая реклама или web-сайт не обходятся без качественных изображений или фотографий. Качественно отснятая и обработанная продукция напрямую влияет на формирование имиджа предприятия и узнаваемость на фоне конкурентов. Для качественного результата мало иметь профессиональное фото- и осветительное оборудование, не менее важны опыт работы в рекламе, знание законов полиграфии, владение широким спектром программного обеспечения, знания законов оптики и светопостановки.</p> <p>У меня можно заказать рекламную предметную, макро, каталожную, имиджевую, интерьерную фотосъемку или обработку имеющихся некачественных фотографий.</p>
<a class="button aqua" href="/portfolio/photo.html" title="Портфолио предметная фотография">Смотреть работы</a>  <a class="button red" href="/kontakty.html" title="">Скачать бриф</a>
</div>
<div class="side-mockup left-mockup animation">
<img alt="Предметная фотосъемка. Портфолио дизайнера" src="/uploads/images/uslugi/photo.jpg"/>
</div>
</div>
</section>
<section class="row section call-to-action wave_t_g wave_b_w" style="background-color:#F9CE46;">
<div class="row-content buffer even animation">
<p>Хотите сэкономить на создании сайта? Возможность есть! </p>
<a class="button red" href="/kontakty.html">Узнайте подробности</a>
</div>
</section>
</div><!-- id-main -->
</main><!-- main -->
<footer>
<div class="row">
<div class="row-content buffer clear-after">
<div id="top-footer">
<div class="widget column four">
<h4><em>География работы</em></h4>
<a href="/sozdanie-sajtov-v-elektrostali.html" title="Создание сайтов в Электростали">Электросталь</a>, <a href="/sozdanie-sajtov-v-g-noginsk.html" title="Создание сайтов  в г. Ногинск">Ногинск</a>, <a href="/sozdanie-sajtov-elktrougli.html" title="Создание сайтов в г. Электроугли">Электроугли</a> </div>
<div class="widget column four">
<h4><em>Новые статьи</em></h4>
<ul class="plain">
<li><a href="/articlies/foto/grip-kalkulyator.html" title="ГРИП-калькулятор. Определение глубины резко изображаемого пространства">Калькулятор глубины резкости (ГРИП)</a></li><li><a href="/articlies/foto/raschet-grip-glubiny-rezko-otobrazhaemogo-prostranstva.html" title="Расчет ГРИП глубины резко отображаемого пространства">Расчет ГРИП и оптические термины</a></li>
</ul>
</div>
<div class="widget meta-social column four">
<h4><em>Используйте для связи</em></h4>
<p><i class="fa fa-skype fa-lg"></i>  Skype:   <strong>bitmap77</strong><br/><i class="fa fa-envelope-o fa-lg"></i>  E-mail:  <strong>info(собака)bitmap.ru</strong><br/><i class="fa fa-comments fa-lg"></i>  ICQ:      <strong>113150127</strong><br/><i class="fa fa-phone-square fa-lg"></i>  Тел:       <strong>+7 (925) 833-40-11</strong></p>
<ul class="inline">
<li><a class="twitter-share border-box" href="https://vk.com/bitmap_ru"><i class="fa fa-vk fa-lg"></i></a></li>
<li><a class="facebook-share border-box" href="https://www.facebook.com/anton.cherk"><i class="fa fa-facebook fa-lg"></i></a></li>
<li><a class="pinterest-share border-box" href="https://plus.google.com/u/0/+АнтонЧеркасовБитмап/posts"><i class="fa fa-google-plus fa-lg"></i></a></li>
</ul>
</div>
</div><!-- top-footer -->
<div id="bottom-footer">
<p class="keep-left">© 2014 Bitmap — частный дизайнер Антон Черкасов.</p>
<p class="keep-right">Копирование материала запрещено (п. 3 ст. 1259 - п. 1 ст. 1259 )</p>
</div><!-- bottom-footer -->
</div><!-- row-content -->
</div><!-- row -->
</footer>
<!-- Yandex.Metrika counter --><script>(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter26858670 = new Ya.Metrika({id:26858670, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img alt="" src="//mc.yandex.ru/watch/26858670" style="position:absolute; left:-9999px;"/></div></noscript><!-- /Yandex.Metrika counter -->
<script src="/templates/bit/js/plugins.min.js"></script>
<script src="/templates/bit/js/bit.min.js"></script>
</body>
</html>
