<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="el-gr" xml:lang="el-gr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="index, follow" name="robots"/>
<meta content="Aptera, apartments, in Chania, Crete, Greece, rooms, with pool, near sea Rooms for rent in Chania Crete" name="keywords"/>
<meta content="Aptera Apartments in Chania Crete Greece | Rooms for rent in Chania Crete" name="title"/>
<meta content="Aptera Apartments" name="author"/>
<meta content="Aptera apartments in Chania Crete Greece rooms with pool near sea Rooms for rent in Chania Crete" name="description"/>
<meta content="" name="generator"/>
<title>Aptera Apartments in Chania Crete Greece | Rooms for rent in Chania Crete</title>
<link href="http://aptera-apartments.com/chania/components/com_joomgallery/assets/css/joom_settings.css" rel="stylesheet" type="text/css"/>
<link href="http://aptera-apartments.com/chania/components/com_joomgallery/assets/css/joomgallery.css" rel="stylesheet" type="text/css"/>
<link href="http://aptera-apartments.com/chania/modules/mod_joomimg/assets/mod_joomimg.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    <!--
.joomimg19_imgct {
width:100% !important;
float:left;
}
.joomimg19_img {
display:block;
text-align:left!important;
vertical-align:top!important;
}
.joomimg19_txt {
clear:both;text-align:left!important;
vertical-align:top!important;
}

        a.flag {font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url('/chania/modules/mod_gtranslate/tmpl/lang/16a.png');}
        a.flag:hover {background-image:url('/chania/modules/mod_gtranslate/tmpl/lang/16.png');}
        a.flag img {border:0;}
        a.alt_flag {font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url('/chania/modules/mod_gtranslate/tmpl/lang/alt_flagsa.png');}
        a.alt_flag:hover {background-image:url('/chania/modules/mod_gtranslate/tmpl/lang/alt_flags.png');}
        a.alt_flag img {border:0;}
    
    -->
  </style>
<script src="/chania/plugins/system/mtupgrade/mootools.js" type="text/javascript"></script>
<script src="/chania/media/system/js/caption.js" type="text/javascript"></script>
<script src="/chania/modules/mod_gtranslate/jquery.js" type="text/javascript"></script>
<script src="/chania/modules/mod_gtranslate/jquery-translate.js" type="text/javascript"></script>
<link href="/chania/templates/system/css/system.css" rel="stylesheet" type="text/css"/>
<link href="/chania/templates/system/css/general.css" rel="stylesheet" type="text/css"/>
<link href="/chania/templates/webstylesbluebeach/css/style.css" rel="stylesheet" type="text/css"/>
<!--[if IE 6]><link rel="stylesheet" href="/chania/templates/webstylesbluebeach/css/style.ie6.css" type="text/css" media="screen" /><![endif]-->
<script src="/chania/templates/webstylesbluebeach/script.js" type="text/javascript"></script>
</head>
<body>
<div class="PageBackgroundGradient"></div>
<div class="PageBackgroundGlare">
<div class="PageBackgroundGlareImage"></div>
</div>
<div class="Main">
<div class="Sheet">
<div class="Sheet-tl"></div>
<div class="Sheet-tr"><div></div></div>
<div class="Sheet-bl"><div></div></div>
<div class="Sheet-br"><div></div></div>
<div class="Sheet-tc"><div></div></div>
<div class="Sheet-bc"><div></div></div>
<div class="Sheet-cl"><div></div></div>
<div class="Sheet-cr"><div></div></div>
<div class="Sheet-cc"></div>
<div class="Sheet-body">
<div class="Header">
<div class="Header-jpeg"></div>
<div class="logo">
<h1 class="logo-name" id="name-text"><a href="/chania/">Apartments in Chania Crete</a></h1>
<div class="logo-text" id="slogan-text"></div>
</div>
</div>
<script type="text/javascript">
    //<![CDATA[
    if(jQuery.cookie('glang') && jQuery.cookie('glang') != 'en') jQuery(function($){$('body').translate('en', $.cookie('glang'), {toggle:true, not:'.notranslate'});});
    //]]>
    </script>
<script type="text/javascript">
//<![CDATA[
    function doTranslate(lang_pair) {if(lang_pair.value)lang_pair=lang_pair.value;var lang=lang_pair.split('|')[1];jQuery.cookie('glang', lang);jQuery(function($){$('body').translate('en', lang, {toggle:true, not:'.notranslate'});});}
//]]>
</script>
<a class="flag" href="javascript:doTranslate('en|en')" style="background-position:-0px -0px;" title="English"><img alt="English" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|ar')" style="background-position:-100px -0px;" title="Arabic"><img alt="Arabic" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|zh-CN')" style="background-position:-300px -0px;" title="Chinese (Simplified)"><img alt="Chinese (Simplified)" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|cs')" style="background-position:-600px -0px;" title="Czech"><img alt="Czech" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|da')" style="background-position:-700px -0px;" title="Danish"><img alt="Danish" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|nl')" style="background-position:-0px -100px;" title="Dutch"><img alt="Dutch" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|fi')" style="background-position:-100px -100px;" title="Finnish"><img alt="Finnish" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|fr')" style="background-position:-200px -100px;" title="French"><img alt="French" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|de')" style="background-position:-300px -100px;" title="German"><img alt="German" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|el')" style="background-position:-400px -100px;" title="Greek"><img alt="Greek" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|it')" style="background-position:-600px -100px;" title="Italian"><img alt="Italian" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|ja')" style="background-position:-700px -100px;" title="Japanese"><img alt="Japanese" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|pt')" style="background-position:-300px -200px;" title="Portuguese"><img alt="Portuguese" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|ru')" style="background-position:-500px -200px;" title="Russian"><img alt="Russian" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|es')" style="background-position:-600px -200px;" title="Spanish"><img alt="Spanish" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a> <a class="flag" href="javascript:doTranslate('en|sv')" style="background-position:-700px -200px;" title="Swedish"><img alt="Swedish" height="16" src="/chania/modules/mod_gtranslate/tmpl/lang/blank.png" width="16"/></a>
<div class="contentLayout">
<div class="sidebar1">
<div class="Block">
<div class="Block-tl"></div>
<div class="Block-tr"><div></div></div>
<div class="Block-bl"><div></div></div>
<div class="Block-br"><div></div></div>
<div class="Block-tc"><div></div></div>
<div class="Block-bc"><div></div></div>
<div class="Block-cl"><div></div></div>
<div class="Block-cr"><div></div></div>
<div class="Block-cc"></div>
<div class="Block-body">
<div class="BlockHeader">
<div class="header-tag-icon">
<div class="BlockHeader-text">

Main Menu
        </div>
</div>
<div class="l"></div>
<div class="r"><div></div></div>
</div>
<div class="BlockContent">
<div class="BlockContent-body">
<ul class="menu"><li class="active item1" id="current"><a href="http://aptera-apartments.com/chania/"><span>Home</span></a></li><li class="item2"><a href="/chania/index.php?option=com_content&amp;view=article&amp;id=2&amp;Itemid=2"><span>Rooms in Chania</span></a></li><li class="item3"><a href="/chania/index.php?option=com_joomloc&amp;view=loc&amp;layout=singleloc&amp;site_id=1&amp;Itemid=3"><span>Booking</span></a></li><li class="item5"><a href="/chania/index.php?option=com_content&amp;view=article&amp;id=4&amp;Itemid=5"><span>Aptera History</span></a></li><li class="item7"><a href="/chania/index.php?option=com_joomgallery&amp;view=gallery&amp;Itemid=7"><span>Photo Gallery</span></a></li><li class="item4"><a href="/chania/index.php?option=com_contact&amp;view=contact&amp;id=1&amp;Itemid=4"><span>Contact &amp; Map</span></a></li><li class="item6"><a href="/chania/index.php?option=com_xmap&amp;sitemap=1&amp;Itemid=6"><span>Sitemap</span></a></li><li class="item8"><a href="http://aptera-apartments.com/index.htm"><span>Old site</span></a></li></ul>
</div>
</div>
</div>
</div>
<div class="Block">
<div class="Block-tl"></div>
<div class="Block-tr"><div></div></div>
<div class="Block-bl"><div></div></div>
<div class="Block-br"><div></div></div>
<div class="Block-tc"><div></div></div>
<div class="Block-bc"><div></div></div>
<div class="Block-cl"><div></div></div>
<div class="Block-cr"><div></div></div>
<div class="Block-cc"></div>
<div class="Block-body">
<div class="BlockHeader">
<div class="header-tag-icon">
<div class="BlockHeader-text">

The Appartments
        </div>
</div>
<div class="l"></div>
<div class="r"><div></div></div>
</div>
<div class="BlockContent">
<div class="BlockContent-body">
<div class="joomimg19_main">
<div class="joomimg_row">
<div class="joomimg19_imgct">
<div class="joomimg19_img">
<a href="/chania/index.php?view=detail&amp;id=15&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="View of the Aptera apartments in Chania Crete Greece" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/view_of_the_rooms_of_aptera_apartments_in_chania_crete_1/view_of_the_aptera_apartments_in_chania_crete_greece_20101122_1801373927.jpg" title="View of the Aptera apartments in Chania Crete Greece"/> </a></div>
<div class="joomimg19_txt">
<ul>
<li><b>View of the Aptera apartments in Chania Crete Greece</b> </li>
<li>Description: View of the Aptera apartments in Chania Crete Greece</li></ul>
</div>
</div>
</div>
<div class="joomimg_clr"></div>
<div class="joomimg_row">
<div class="joomimg19_imgct">
<div class="joomimg19_img">
<a href="/chania/index.php?view=detail&amp;id=4&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1253239046.jpg" title="The pool and the Aptera Apartments in Chania Crete Greece"/> </a></div>
<div class="joomimg19_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul>
</div>
</div>
</div>
<div class="joomimg_clr"></div>
<div class="joomimg_row">
<div class="joomimg19_imgct">
<div class="joomimg19_img">
<a href="/chania/index.php?view=detail&amp;id=17&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="View of the Aptera apartments in Chania Crete Greece" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/view_of_the_rooms_of_aptera_apartments_in_chania_crete_1/view_of_the_aptera_apartments_in_chania_crete_greece_20101122_2082516412.jpg" title="View of the Aptera apartments in Chania Crete Greece"/> </a></div>
<div class="joomimg19_txt">
<ul>
<li><b>View of the Aptera apartments in Chania Crete Greece</b> </li>
<li>Description: View of the Aptera apartments in Chania Crete Greece</li></ul>
</div>
</div>
</div>
<div class="joomimg_clr"></div>
<div class="joomimg_row">
<div class="joomimg19_imgct">
<div class="joomimg19_img">
<a href="/chania/index.php?view=detail&amp;id=1&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1760165794.jpg" title="The pool and the Aptera Apartments in Chania Crete Greece"/> </a></div>
<div class="joomimg19_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul>
</div>
</div>
</div>
<div class="joomimg_clr"></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="content">
<div class="Post">
<div class="Post-body">
<div class="Post-inner">
<div class="PostContent">
<p>MHTE: 10.42.K.12.3K.02636.00.00  |  GNT LICENCE: 10.42.K.12.3K.02636.00.00</p>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
<div class="Post">
<div class="Post-body">
<div class="Post-inner">
<div class="PostMetadataHeader">
<h2 class="PostHeaderIcon-wrapper"><img alt="PostHeaderIcon" height="25" src="/chania/templates/webstylesbluebeach/images/PostHeaderIcon.png" width="27"/> 						Aptera Apartments in Chania Crete Greece | Rooms for rent in Chania Crete			</h2>
<div class="PostHeaderIcons metadata-icons">
<span class="metadata-icons"></span>
</div>
</div>
<div class="PostContent">
<div class="article">
<p><img alt="image_welcome" border="0" height="120" src="/chania/images/stories/image_welcome.gif" style="margin: 5px; vertical-align: top;" width="588"/></p>
<p>Aptera is located in Crete at a distance of 12km east of Chania, 45km west of Rethimnon and just 2km, South of Souda Port. Just 3km from aptera you can enjoy the sea at the sandy beaches of Kalives, Almirida and Kalami, or taste the fresh fish in local taverns.<br/><br/>At a small distance from the archaeological site of Aptera, in Kydonia, on the top of a hill with marvellous views to Souda Bay to the north and the mountains Leuka Ori to the south, lies Aptera apartments, a complex of comfortable studios and apartments. Aptera consists of one traditional villa which accommodates up to five people, two studios for three people, four apartments with living room for three people and four larger apartments with a spacious living room for five people.<br/><br/>Aptera Rooms &amp; Apartments are near the archaeological city of Aptera. Aptera took its name after the victory of the Muses over the Sirens in a musical contest. The contest took place between the city and the sea, and the Muses won. The Sirens lost their wings and they became white. Hence, the city was called Aptera (without wings) and the close small islands were called Leuka (white). The citizen was called Apteraios.</p>
<h2 style="text-align: center;">Free WiFi is offered</h2>
<p>
</p><div class="gallerytab">
<div class="jg_row sectiontableentry2"><div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=10&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1463736802.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
<div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=9&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1133654489.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
</div><div class="jg_row sectiontableentry1">
<div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=8&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1710840526.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
<div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=7&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1230839462.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
</div><div class="jg_row sectiontableentry2">
<div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=6&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_2013395547.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
<div class="jg_element_cat" style="width:49%">
<a class="jg_catelem_photo" href="/chania/index.php?view=detail&amp;id=5&amp;option=com_joomgallery&amp;Itemid=7"> <img alt="The pool and the Aptera Apartments in Chania Crete Greece" class="jg_photo" src="http://aptera-apartments.com/chania/images/joomgallery/thumbnails/surroundings_2/the_pool_and_the_aptera_apartments_in_chania_crete_greece_20101122_1049665259.jpg"/> </a> <div class="jg_catelem_txt">
<ul>
<li><b>The pool and the Aptera Apartments in Chania Crete Greece</b> </li>
<li>Description: The pool and the Aptera Apartments in Chania Crete Greece</li></ul> </div>
</div>
</div>
</div> </div>
</div>
<div class="cleared"></div>
</div>
</div>
</div>
</div>
</div>
<div class="cleared"></div>
<div class="Footer">
<div class="Footer-inner">
<div class="Footer-text"><p>Copyright © 2010 Aptera Apartments,  Rooms in Chania Crete Greece with pool and sea view.<br/>
All Rights Reserved.</p>
</div>
</div>
<div class="Footer-background"></div>
</div>
</div>
</div>
<p class="page-footer"><a href="http://www.hostsun.com">Φιλοξενία σελίδας</a> και <a href="http://hostsun.com/">κατασκευή σελίδας</a> Hostsun </p>
</div>
</body>
</html>