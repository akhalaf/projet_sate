<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>Braes Retreat |   Page not found</title>
<!-- Le styles -->
<link href="https://www.braes-retreat.co.uk/wp-content/themes/braes/style.css" rel="stylesheet"/>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
        <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.braes-retreat.co.uk/feed/" rel="alternate" title="Braes Retreat » Feed" type="application/rss+xml"/>
<link href="https://www.braes-retreat.co.uk/comments/feed/" rel="alternate" title="Braes Retreat » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.braes-retreat.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=0c08408a5aa771072c057e10ee422143"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.braes-retreat.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=0c08408a5aa771072c057e10ee422143" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.braes-retreat.co.uk/wp-content/plugins/awesome-weather/awesome-weather.css?ver=0c08408a5aa771072c057e10ee422143" id="awesome-weather-css" media="all" rel="stylesheet" type="text/css"/>
<style id="awesome-weather-inline-css" type="text/css">
.awesome-weather-wrap { font-family: 'Open Sans', sans-serif; font-weight: 400; font-size: 14px; line-height: 14px; }
</style>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A400&amp;ver=0c08408a5aa771072c057e10ee422143" id="opensans-googlefont-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.braes-retreat.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.braes-retreat.co.uk/wp-content/plugins/responsive-google-maps/css/responsive-google-maps.css?ver=0c08408a5aa771072c057e10ee422143" id="responsive-google-maps-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.braes-retreat.co.uk/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.2&amp;ver=0c08408a5aa771072c057e10ee422143" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a {
color:#ff7302;
text-shadow:none;
-webkit-transition:all 0.2s ease-out;
-moz-transition:all 0.2s ease-out;
-o-transition:all 0.2s ease-out;
-ms-transition:all 0.2s ease-out;
}

.tp-caption a:hover {
color:#ffa902;
}
</style>
<link href="https://www.braes-retreat.co.uk/wp-content/plugins/revslider/rs-plugin/css/dynamic-captions.css?rev=4.2&amp;ver=0c08408a5aa771072c057e10ee422143" id="rs-captions-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https:////netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css?ver=0c08408a5aa771072c057e10ee422143" id="br-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.braes-retreat.co.uk/wp-content/themes/braes/bootstrap/css/bootstrap.css?ver=0c08408a5aa771072c057e10ee422143" id="br-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.braes-retreat.co.uk/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.braes-retreat.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.braes-retreat.co.uk/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.plugins.min.js?rev=4.2&amp;ver=0c08408a5aa771072c057e10ee422143" type="text/javascript"></script>
<script src="https://www.braes-retreat.co.uk/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.2&amp;ver=0c08408a5aa771072c057e10ee422143" type="text/javascript"></script>
<script src="https://www.braes-retreat.co.uk/wp-content/themes/braes/js/jquery.backstretch.min.js?ver=0c08408a5aa771072c057e10ee422143" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var theme_images = {"url":["https:\/\/www.braes-retreat.co.uk\/wp-content\/themes\/braes\/images\/\/banner1.jpg","https:\/\/www.braes-retreat.co.uk\/wp-content\/themes\/braes\/images\/\/banner2.jpg","https:\/\/www.braes-retreat.co.uk\/wp-content\/themes\/braes\/images\/\/banner3.jpg","https:\/\/www.braes-retreat.co.uk\/wp-content\/themes\/braes\/images\/\/banner4.jpg"]};
/* ]]> */
</script>
<script src="https://www.braes-retreat.co.uk/wp-content/themes/braes/js/custom-scripts.js?ver=0c08408a5aa771072c057e10ee422143" type="text/javascript"></script>
<link href="https://www.braes-retreat.co.uk/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.braes-retreat.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.braes-retreat.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<link href="/home/braesretreatco/public_html/wp-content/themes/braes/images/favicon.ico" rel="shortcut icon"/>
</head>
<body class="error404" data-rsssl="1">
<header class="animate-container animated fixed-header">
<div id="top-bar">
<div class="inner clearfixbr">
<div class="inner clearfixbr"></div>
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-4 col-xs-12 top-contact"><a href="tel:+44(0)7309 808838">+44(0)7309 808838</a></div>
<!--div class="col-md-4 col-sm-4 col-xs-12 top-contact"><a href="tel:+44(0)1478 650394">+44(0)1478 650394</a></div--->
<div class="col-md-4 col-sm-4 col-xs-12 text-center"><a class="btn" href="https://www.braes-retreat.co.uk/availability-prices/">BOOK NOW</a></div>
<div class="col-md-4 col-sm-4 col-xs-12 text-right top-links"><a href="https://www.braes-retreat.co.uk/contact-us/">CONTACT US</a></div>
</div>
</div>
</div>
</div>
<hr class="texture-top"/>
<div class="containe">
<div id="main-navigation">
<div class="inner clearfixbr">
<div class="mobile-heading">
<a class="mobile-logo" href="/" title="">
                            BRAES
                        </a>
<a class="navigation-trigger" href="#" title=""><i></i></a>
</div>
<div class="navigation-container clearfixbr">
<nav>
<ul class="clearfixbr main nav navbar-nav" id="main-nav"><li class="accomodation menu-item menu-item-type-post_type menu-item-object-page menu-item-29" id="menu-item-29"><a href="https://www.braes-retreat.co.uk/accomodation/">Accommodation</a></li>
<li class="prices menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://www.braes-retreat.co.uk/availability-prices/">Availability/Prices</a></li>
<li class="logo menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-30" id="menu-item-30"><a href="https://www.braes-retreat.co.uk/">Home</a></li>
<li class="find menu-item menu-item-type-custom menu-item-object-custom menu-item-40" id="menu-item-40"><a href="https://www.braes-retreat.co.uk/find-us/">Find Us</a></li>
<li class="journal menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-26" id="menu-item-26"><a href="https://www.braes-retreat.co.uk/journal/">Journal</a></li>
<li class="about menu-item menu-item-type-post_type menu-item-object-page menu-item-25" id="menu-item-25"><a href="https://www.braes-retreat.co.uk/about-braes/">About Braes</a></li>
</ul> </nav>
</div>
</div>
</div>
</div>
</header>
<div class="banner">
<div class="container">
<div class="clearfixbr" id="page-wrapper">
<div class="page-404" id="page-content">
<h1 class="center">404</h1>
<h2 class="center">Sorry, Nothing found!</h2>
</div>
</div>
</div>
<hr class="footer-rule"/>
<footer>
<div class="top-footer clearfixbr">
<div class="inner clearfixbr">
<p class="social">
<strong><a href="https://www.facebook.com/braesretreat"><i class="fa fa-facebook"></i></a></strong>
</p>
</div>
</div>
<div class="footer-nav">
<div class="inner">
<ul class="clearfixbr nav navbar-nav" id="main-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23" id="menu-item-23"><a href="https://www.braes-retreat.co.uk/accomodation/">Accommodation</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" id="menu-item-22"><a href="https://www.braes-retreat.co.uk/availability-prices/" rel="noopener noreferrer" target="_blank">Availability &amp; Prices</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18" id="menu-item-18"><a href="https://www.braes-retreat.co.uk/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-41" id="menu-item-41"><a href="https://www.braes-retreat.co.uk/find-us/">Find Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-20" id="menu-item-20"><a href="https://www.braes-retreat.co.uk/journal/">Journal</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19" id="menu-item-19"><a href="https://www.braes-retreat.co.uk/about-braes/">About Braes</a></li>
</ul> </div>
</div>
<div class="bottom-footer clearfixbr">
<div class="inner clearfixbr">
<p class="contact"><a href="tel:+44(0)7309 808838">+44(0)7309 808838</a></p>
</div>
</div>
</footer>
<script src="https://www.braes-retreat.co.uk/wp-content/plugins/awesome-weather/js/awesome-weather-widget-frontend.js?ver=1.1" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.braes-retreat.co.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://www.braes-retreat.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2" type="text/javascript"></script>
<script src="https://www.braes-retreat.co.uk/wp-includes/js/wp-embed.min.js?ver=0c08408a5aa771072c057e10ee422143" type="text/javascript"></script>
</div></body>
</html>
