<!DOCTYPE html>
<html lang="en-GB">
<head>
<meta charset="utf-8"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://bathroom-review.co.uk/xmlrpc.php" rel="pingback"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<!-- This site is optimized with the Yoast SEO Premium plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Page not found - Bathroom Review</title>
<meta content="noindex, follow" name="robots"/>
<meta content="en_GB" property="og:locale"/>
<meta content="Page not found - Bathroom Review" property="og:title"/>
<meta content="Bathroom Review" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://bathroom-review.co.uk/#organization","name":"Bathroom Review","url":"https://bathroom-review.co.uk/","sameAs":["https://www.facebook.com/bathroomreview","https://www.instagram.com/bathroom_review/","https://www.linkedin.com/company/bathroomreview","https://www.pinterest.co.uk/BathroomReview1/","https://twitter.com/Bathroomreview"],"logo":{"@type":"ImageObject","@id":"https://bathroom-review.co.uk/#logo","inLanguage":"en-GB","url":"https://bathroom-review.co.uk/wp-content/uploads/2020/07/K100.png","width":5400,"height":600,"caption":"Bathroom Review"},"image":{"@id":"https://bathroom-review.co.uk/#logo"}},{"@type":"WebSite","@id":"https://bathroom-review.co.uk/#website","url":"https://bathroom-review.co.uk/","name":"Bathroom Review","description":"Bathroom Review","publisher":{"@id":"https://bathroom-review.co.uk/#organization"},"potentialAction":[{"@type":"SearchAction","target":"https://bathroom-review.co.uk/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-GB"}]}</script>
<!-- / Yoast SEO Premium plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bathroom-review.co.uk/feed/" rel="alternate" title="Bathroom Review » Feed" type="application/rss+xml"/>
<link href="https://bathroom-review.co.uk/comments/feed/" rel="alternate" title="Bathroom Review » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/bathroom-review.co.uk\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://bathroom-review.co.uk/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wp-block-library-inline-css" type="text/css">
.has-text-align-justify{text-align:justify;}
</style>
<link href="https://bathroom-review.co.uk/wp-content/plugins/guteblock/dist/style.css?ver=5.6" id="guteblock-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/plugins/wordpress-popular-posts/assets/css/wpp.css?ver=5.2.4" id="wordpress-popular-posts-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lato%3A400%2C700%2C900%7CNoto+Sans%3A400%2C400i%2C700%7CLora%3A400i&amp;display=swap" id="cheerup-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/style.css?ver=7.2.1" id="cheerup-core-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/css/lightbox.css?ver=7.2.1" id="magnific-popup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/css/fontawesome/css/font-awesome.min.css?ver=7.2.1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/css/icons/icons.css?ver=7.2.1" id="cheerup-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/css/skin-rovella.css?ver=7.2.1" id="cheerup-skin-css" media="all" rel="stylesheet" type="text/css"/>
<style id="cheerup-skin-inline-css" type="text/css">
.main-head .social-icons a { font-size: 22px; }
.main-head .top-bar { --topbar-height: 45px; }
.main-head:not(.simple):not(.compact):not(.logo-left) .title { padding-top: 20px !important; padding-bottom: 20px !important; }


</style>
<link href="https://bathroom-review.co.uk/wp-content/themes/cheerup/css/fonts/trueno.css?ver=7.2.1" id="cheerup-font-trueno-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://bathroom-review.co.uk/wp-content/plugins/jetpack/css/jetpack.css?ver=9.2.1" id="jetpack_css-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js-extra" type="text/javascript">
/* <![CDATA[ */
var Sphere_Plugin = {"ajaxurl":"https:\/\/bathroom-review.co.uk\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="jquery-core-js" src="https://bathroom-review.co.uk/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://bathroom-review.co.uk/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="guteblock-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var guteblock = {"ajaxurl":"https:\/\/bathroom-review.co.uk\/wp-admin\/admin-ajax.php","siteurl":"https:\/\/bathroom-review.co.uk"};
/* ]]> */
</script>
<script id="guteblock-script-js" src="https://bathroom-review.co.uk/wp-content/plugins/guteblock/dist/script.js?ver=5.6" type="text/javascript"></script>
<script id="wpp-json" type="application/json">
{"sampling_active":0,"sampling_rate":100,"ajax_url":"https:\/\/bathroom-review.co.uk\/wp-json\/wordpress-popular-posts\/v1\/popular-posts","ID":0,"token":"c088ba7c74","lang":0,"debug":0}
</script>
<script id="wpp-js-js" src="https://bathroom-review.co.uk/wp-content/plugins/wordpress-popular-posts/assets/js/wpp.min.js?ver=5.2.4" type="text/javascript"></script>
<script id="cheerup-ie-polyfills-js" nomodule="" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/ie-polyfills.js?ver=7.2.1" type="text/javascript"></script>
<link href="https://bathroom-review.co.uk/wp-json/" rel="https://api.w.org/"/><link href="https://bathroom-review.co.uk/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bathroom-review.co.uk/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<style type="text/css">img#wpstats{display:none}</style><link href="https://bathroom-review.co.uk/wp-content/uploads/2020/07/cropped-BFav-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://bathroom-review.co.uk/wp-content/uploads/2020/07/cropped-BFav-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://bathroom-review.co.uk/wp-content/uploads/2020/07/cropped-BFav-180x180.png" rel="apple-touch-icon"/>
<meta content="https://bathroom-review.co.uk/wp-content/uploads/2020/07/cropped-BFav-270x270.png" name="msapplication-TileImage"/>
<style id="wp-custom-css" type="text/css">
			
.archive-head .background{
display: none;
}

.archive-head .sub-title{
display: none; 
}

.stylish-slider .category {display: none;}


.page-id-153 .post-meta .category { display: none;}


.no-comments {
  display: none;
}		</style>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="error404 right-sidebar lazy-smart skin-rovella has-lb has-lb-s wpb-js-composer js-comp-ver-6.5.0 vc_responsive">
<div class="main-wrap">
<header class="main-head head-nav-below nav-below" id="main-head">
<div class="top-bar light cf">
<div class="top-bar-content ts-contain" data-sticky-bar="1">
<div class="wrap cf">
<span class="mobile-nav"><i class="tsi tsi-bars"></i></span>
<ul class="social-icons cf">
<li><a class="tsi tsi-facebook" href="https://www.facebook.com/bathroomreview/" target="_blank"><span class="visuallyhidden">Facebook</span></a></li>
<li><a class="tsi tsi-twitter" href="https://twitter.com/bathroomreview" target="_blank"><span class="visuallyhidden">Twitter</span></a></li>
<li><a class="tsi tsi-instagram" href="https://www.instagram.com/bathroom_review/" target="_blank"><span class="visuallyhidden">Instagram</span></a></li>
<li><a class="tsi tsi-pinterest-p" href="https://www.pinterest.co.uk/BathroomReview1/" target="_blank"><span class="visuallyhidden">Pinterest</span></a></li>
<li><a class="tsi tsi-linkedin" href="https://www.linkedin.com/company/bathroomreview/" target="_blank"><span class="visuallyhidden">LinkedIn</span></a></li>
</ul>
<div class="actions">
<div class="search-action cf">
<form action="https://bathroom-review.co.uk/" class="search-form" method="get">
<button class="search-submit" type="submit"><i class="tsi tsi-search"></i></button>
<input class="search-field" name="s" placeholder="Search" required="" type="search" value=""/>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="inner ts-contain">
<div class="wrap logo-wrap cf">
<div class="title">
<a href="https://bathroom-review.co.uk/" rel="home" title="Bathroom Review">
<img alt="Bathroom Review" class="logo-image" src="https://bathroom-review.co.uk/wp-content/uploads/2020/07/K100.png" srcset="https://bathroom-review.co.uk/wp-content/uploads/2020/07/K100.png ,https://bathroom-review.co.uk/wp-content/uploads/2020/07/BR3.png 2x"/>
</a>
</div>
</div>
</div>
<div class="navigation-wrap">
<nav class="navigation ts-contain below has-bg light" data-sticky-bar="1">
<div class="wrap">
<div class="menu-main-menu-container"><ul class="menu" id="menu-main-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-1062" id="menu-item-1062"><a href="http://bathroom-review.co.uk/">Home</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-cat-4 menu-item-has-children item-mega-menu menu-item-1070" id="menu-item-1070"><a href="https://bathroom-review.co.uk/category/features/">Features</a>
<div class="sub-menu mega-menu wrap">
<section class="column recent-posts">
<div class="ts-row posts cf" data-id="4">
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/showering-spaces-fresh-design-ideas/"><span aria-label="Lakes Premium Island Collection" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-768x576.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-300x225.jpg 300w, https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-1024x768.jpg 1024w, https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-450x338.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-1170x878.jpg 1170w, https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2.jpg 1500w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/11/Image-2-768x576.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/showering-spaces-fresh-design-ideas/">Showering spaces: Fresh design ideas</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/showering-spaces-fresh-design-ideas/">
<time class="post-date" datetime="2020-11-25T13:47:27+00:00">November 25, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/hygienic-and-covid-safe-washrooms-bathrooms/"><span aria-label="RAK-Sanit Hygienic and Covid-safe washrooms" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-768x497.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-300x194.jpg 300w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-1024x662.jpg 1024w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-175x113.jpg 175w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-450x291.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-1170x757.jpg 1170w, https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit.jpg 1500w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/10/Public-Bathroom-RAK-Sanit-768x497.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/hygienic-and-covid-safe-washrooms-bathrooms/">Hygienic and Covid-safe washrooms and bathrooms</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/hygienic-and-covid-safe-washrooms-bathrooms/">
<time class="post-date" datetime="2020-10-12T11:35:00+00:00">October 12, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/basins-baths-chic-and-contemporary-design-ideas/"><span aria-label="Public" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-768x480.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-300x187.jpg 300w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-1024x640.jpg 1024w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-175x109.jpg 175w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-450x281.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-1170x731.jpg 1170w, https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1.jpg 1500w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/06/INFINITIVE-standard-freestanding-senape-1-768x480.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/basins-baths-chic-and-contemporary-design-ideas/">Basins &amp; Baths: Chic and contemporary design ideas</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/basins-baths-chic-and-contemporary-design-ideas/">
<time class="post-date" datetime="2020-05-07T14:15:00+00:00">May 7, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/showtime/"><span class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/04/IMG_0548-768x392.jpeg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/IMG_0548-450x230.jpeg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/IMG_0548.jpeg 997w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/04/IMG_0548-768x392.jpeg" data-sizes="(max-width: 270px) 100vw, 270px" role="img" title="kbb Birmingam 2020"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/showtime/">kbb Birmingam 2020</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/showtime/">
<time class="post-date" datetime="2020-04-22T12:02:23+00:00">April 22, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/planet-friendly-bathroom-design/"><span aria-label="Planet-friendly sustainable bathrooms" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-768x512.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-300x200.jpg 300w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-1024x683.jpg 1024w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-175x117.jpg 175w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-450x300.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-1170x780.jpg 1170w, https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava.jpg 1500w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/04/Kaldewei_Nexsys_Silenio_Meisterstueck_Incava-768x512.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/planet-friendly-bathroom-design/">Planet-friendly bathroom design</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/planet-friendly-bathroom-design/">
<time class="post-date" datetime="2020-02-26T10:07:00+00:00">February 26, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/bright-and-beautiful/"><span aria-label="Bathroom Furniture Design" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/02/Final_Saneux_001_Sofia_View001_1957x1100-768x342.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/02/Final_Saneux_001_Sofia_View001_1957x1100-450x200.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/02/Final_Saneux_001_Sofia_View001_1957x1100.jpg 996w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/02/Final_Saneux_001_Sofia_View001_1957x1100-768x342.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/bright-and-beautiful/">Bright and beautiful</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/bright-and-beautiful/">
<time class="post-date" datetime="2020-01-22T17:23:00+00:00">January 22, 2020</time>
</a></div></div>
</div>
<div class="column one-fifth post">
<a class="image-link media-ratio ratio-3-2" href="https://bathroom-review.co.uk/accessible-inclusive-bathroom-design-ideas/"><span aria-label="Fitzroy accessible inclusive bathrooms" class="img bg-cover wp-post-image attachment-cheerup-768 size-cheerup-768 lazyload" data-bgset="https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms-768x521.jpg 768w, https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms-300x204.jpg 300w, https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms-175x119.jpg 175w, https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms-450x305.jpg 450w, https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms.jpg 800w" data-bgsrc="https://bathroom-review.co.uk/wp-content/uploads/2020/07/Fitzroy-accessible-bathrooms-768x521.jpg" data-sizes="(max-width: 270px) 100vw, 270px" role="img"></span></a>
<a class="post-title" href="https://bathroom-review.co.uk/accessible-inclusive-bathroom-design-ideas/">Accessible &amp; inclusive bathroom design</a>
<div class="post-meta post-meta-b has-below"><div class="below meta-below"><a class="meta-item date-link" href="https://bathroom-review.co.uk/accessible-inclusive-bathroom-design-ideas/">
<time class="post-date" datetime="2019-11-27T11:04:00+00:00">November 27, 2019</time>
</a></div></div>
</div>
</div> <!-- .posts -->
<div class="navigate">
<a class="show-prev" href="#"><i class="tsi tsi-angle-left"></i><span class="visuallyhidden">Previous</span></a>
<a class="show-next" href="#"><i class="tsi tsi-angle-right"></i><span class="visuallyhidden">Next</span></a>
</div>
</section>
</div></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1107" id="menu-item-1107"><a href="https://bathroom-review.co.uk/products/">Products</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2170" id="menu-item-2170"><a href="https://bathroom-review.co.uk/news/">News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1108" id="menu-item-1108"><a href="https://bathroom-review.co.uk/brands/">Brands</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2518" id="menu-item-2518"><a href="#">Showtime</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1542" id="menu-item-1542"><a href="https://bathroom-review.co.uk/showtime/">kbb Birmingam 2020</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2517" id="menu-item-2517"><a href="https://bathroom-review.co.uk/sleep-eat-2019/">Sleep &amp; Eat 2019</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-2565" id="menu-item-2565"><a href="https://bathroom-review.co.uk/ish-fairs-well-for-the-planet-with-latest-innovations/">ISH Frankfurt 2019</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1069" id="menu-item-1069"><a href="https://bathroom-review.co.uk/about-us/">About</a></li>
</ul></div> </div>
</nav>
</div>
</header> <!-- .main-head -->
<div class="main wrap">
<div class="ts-row cf">
<div class="col-12 main-content cf">
<div class="the-post the-page page-404 cf">
<header class="post-title-alt">
<h1 class="main-heading">Page Not Found!</h1>
</header>
<div class="post-content error-page row">
<div class="col-3 text-404 main-color">
					404				</div>
<div class="col-8 post-content">
<p>
					We're sorry, but we can't find the page you were looking for. It's probably some thing we've done wrong but now we know about it and we'll try to fix it. In the meantime, try one of these options:					</p>
<ul class="links">
<li> <a class="go-back" href="#">Go to Previous Page</a></li>
<li> <a href="https://bathroom-review.co.uk">Go to Homepage</a></li>
</ul>
<form action="https://bathroom-review.co.uk/" class="search-form" method="get">
<label>
<span class="screen-reader-text">Search for:</span>
<input class="search-field" name="s" placeholder="Type and hit enter..." title="Search for:" type="search" value=""/>
</label>
<button class="search-submit" type="submit"><i class="tsi tsi-search"></i></button>
</form>
</div>
</div>
</div>
</div> <!-- .main-content -->
</div> <!-- .ts-row -->
</div> <!-- .main -->
<footer class="main-footer dark stylish">
<div class="social-strip">
<ul class="social-icons">
<li>
<a class="social-link" href="https://www.facebook.com/bathroomreview/" target="_blank"><i class="tsi tsi-facebook"></i>
<span class="label">Facebook</span></a>
</li>
<li>
<a class="social-link" href="https://twitter.com/bathroomreview" target="_blank"><i class="tsi tsi-twitter"></i>
<span class="label">Twitter</span></a>
</li>
<li>
<a class="social-link" href="https://www.instagram.com/bathroom_review/" target="_blank"><i class="tsi tsi-instagram"></i>
<span class="label">Instagram</span></a>
</li>
<li>
<a class="social-link" href="https://www.pinterest.co.uk/BathroomReview1/" target="_blank"><i class="tsi tsi-pinterest-p"></i>
<span class="label">Pinterest</span></a>
</li>
<li>
<a class="social-link" href="https://www.linkedin.com/company/bathroomreview/" target="_blank"><i class="tsi tsi-linkedin"></i>
<span class="label">LinkedIn</span></a>
</li>
</ul>
</div>
<div class="bg-wrap">
<section class="lower-footer cf">
<div class="wrap">
<div class="bottom cf">
<p class="copyright">Copyright Mediabookers Ltd© 2020        <a href="/privacy-policy/">Privacy Policy</a>        <a href="/cookies-policy/">Cookies Policy</a>         <a href="https://www.kitchens-review.co.uk">Kitchens Review</a> </p>
<div class="to-top">
<a class="back-to-top" href="#"><i class="tsi tsi-angle-up"></i> Top</a>
</div>
</div>
</div>
</section>
</div>
</footer>
</div> <!-- .main-wrap -->
<div class="mobile-menu-container off-canvas" id="mobile-menu">
<a class="close" href="#"><i class="tsi tsi-times"></i></a>
<div class="logo">
</div>
<ul class="mobile-menu"></ul>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/bathroom-review.co.uk\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://bathroom-review.co.uk/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="cheerup-theme-js-extra" type="text/javascript">
/* <![CDATA[ */
var Bunyad = {"custom_ajax_url":"\/images\/%09"};
/* ]]> */
</script>
<script id="cheerup-theme-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/theme.js?ver=7.2.1" type="text/javascript"></script>
<script id="lazysizes-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/lazysizes.js?ver=7.2.1" type="text/javascript"></script>
<script id="magnific-popup-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/jquery.mfp-lightbox.js?ver=7.2.1" type="text/javascript"></script>
<script id="jquery-fitvids-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/jquery.fitvids.js?ver=7.2.1" type="text/javascript"></script>
<script id="imagesloaded-js" src="https://bathroom-review.co.uk/wp-includes/js/imagesloaded.min.js?ver=4.1.4" type="text/javascript"></script>
<script id="object-fit-images-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/object-fit-images.js?ver=7.2.1" type="text/javascript"></script>
<script id="theia-sticky-sidebar-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/jquery.sticky-sidebar.js?ver=7.2.1" type="text/javascript"></script>
<script id="jquery-slick-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/jquery.slick.js?ver=7.2.1" type="text/javascript"></script>
<script id="jarallax-js" src="https://bathroom-review.co.uk/wp-content/themes/cheerup/js/jarallax.js?ver=7.2.1" type="text/javascript"></script>
<script id="wp-embed-js" src="https://bathroom-review.co.uk/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
<script async="async" defer="defer" src="https://stats.wp.com/e-202102.js" type="text/javascript"></script>
<script type="text/javascript">
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:9.2.1',blog:'181743380',post:'0',tz:'0',srv:'bathroom-review.co.uk'} ]);
	_stq.push([ 'clickTrackerInit', '181743380', '0' ]);
</script>
<script id="uagb-script-frontend" type="text/javascript">document.addEventListener("DOMContentLoaded", function(){( function( $ ) {  })(jQuery)})</script>
</body>
</html>