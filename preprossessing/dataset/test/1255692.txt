<!DOCTYPE html>
<html class="error-page" dir="ltr" lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<base href="https://www.rza.com.pe/Display/AdobePDF.html%09%0A"/>
<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<meta content="Plantilla Quickstart Curso Joomla 3.4 en Español 2015, Lima Peru" name="description"/>
<meta content="Joomla! - Open Source Content Management" name="generator"/>
<title>404 - Error: 404</title>
<link href="https://www.rza.com.pe/index.php" rel="canonical"/>
<link href="/images/rza.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
<link href="/templates/webadministrable/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/webadministrable/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/templates/webadministrable/css/template.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="error-page-inner">
<div>
<div class="container">
<p><i class="fa fa-exclamation-triangle"></i></p>
<h1 class="error-code">404</h1>
<p class="error-message">Categoría no encontrada</p>
<a class="btn btn-primary btn-lg" href="/" title="HOME"><i class="fa fa-chevron-left"></i> Go Back Home</a>
</div>
</div>
</div>
</body>
</html>