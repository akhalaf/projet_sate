<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/2011Template.dwt.asp" codeOutsideHTMLIsLocked="false" -->
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Bootstraps Comedy Theater â¢ Dallas</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
.style12 {font-family: Arial, Helvetica, sans-serif;
	color: #333333;
	font-size: 10pt;
}
</style>
<!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	background-color: #b32317;
}
.style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
	font-style: normal;
	letter-spacing: .23em;
}
a:link {
	color: #999999;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #333333;
}
a:hover {
	text-decoration: underline;
	color: #C70505;
}
a:active {
	text-decoration: none;
}
.style10 {font-size: 10px}
.style11 {color: #999999}
-->
</style>
</head>
<body>
<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="3" width="700">
<tr valign="bottom">
<th scope="col"><div align="center"><a href="index.html"><img alt="bootstraps logo" border="0" height="147" src="../images/BootstrapsLogo11 RGB.jpg" width="432"/></a></div></th>
<th scope="col"><div align="right"><a href="http://www.facebook.com/#!/pages/Bootstraps-Theater/117839518227124?ref=ts" target="_blank"><img alt="FIND US ON FACEBOOK" border="0" height="31" src="../images/fb.jpeg.jpg" width="31"/></a></div></th>
</tr>
<tr>
<td colspan="2" height="10"><table border="0" cellpadding="0" cellspacing="3" width="100%">
<tr>
<th scope="col"><span class="style8"><a href="../SHOWS.html">SHOWS</a></span></th>
<th scope="col"><span class="style8"><a href="../ABOUT.html">ABOUT</a></span></th>
<th scope="col"><span class="style8"><a href="../ARCHIVES.html">ARCHIVES</a></span></th>
<th scope="col"><span class="style8"><a href="../SUPPORT.html">SUPPORT</a></span></th>
<th scope="col"><span class="style8"><a href="../index.html">HOME</a></span></th>
</tr>
</table></td>
</tr>
<tr>
<td colspan="2"><div align="left"><!-- InstanceBeginEditable name="body" -->
<blockquote class="style12">
<h2>NOW PLAYING...</h2>
<h2><a href="https://tickets.theatre3dallas.com/TheatreManager/1/tmEvent/tmEvent405.html"><img height="237" src="http://www.bootstrapscomedy.com/images/christmas-sketch-show.jpg" width="624"/></a></h2>
<p> </p>
<p>Images from our history:</p>
<h2><img alt="cover-slideshow" height="362" src="images/2016-cover.gif" width="626"/></h2>
<h2> </h2>
<h2><br/>
</h2>
<h2> </h2>
</blockquote>
<!-- InstanceEndEditable --></div></td>
</tr>
<tr>
<td colspan="2"><hr/></td>
</tr>
<tr>
<td colspan="2"><div align="center"><span class="style10"><span class="style11">Copyright Â© 2017 Bootstraps Comedy Theater</span></span></div></td>
</tr>
</table>
</body>
<!-- InstanceEnd --></html>
