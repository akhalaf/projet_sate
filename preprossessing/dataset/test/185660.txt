<!DOCTYPE HTML>
<html lang="en-US">
<!--[if IE 7 ]>    <html class= "ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class= "ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class= "ie9"> <![endif]-->
<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
   </script>
<![endif]-->
<head><meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
<link href="https://www.bakadesuyo.com/wp-content/themes/featherlite/style.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="https://www.bakadesuyo.com/xmlrpc.php" rel="pingback"/>
<link href="https://www.bakadesuyo.com/" rel="canonical"/>
<link href="" rel="shortcut icon"/>
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<style type="text/css">
	
div#linkblock {
	position:fixed;
	}
	
</style>
<style type="text/css">
</style>
<title>Barking Up The Wrong Tree - How to be awesome at life.</title>
<meta content="How to be awesome at life." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://www.bakadesuyo.com/" rel="canonical"/>
<link href="https://www.bakadesuyo.com/page/2/" rel="next"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Barking Up The Wrong Tree - How to be awesome at life." property="og:title"/>
<meta content="I want to understand why we do what we do and use the answers to be awesome at life." property="og:description"/>
<meta content="http://www.bakadesuyo.com/" property="og:url"/>
<meta content="Barking Up The Wrong Tree" property="og:site_name"/>
<meta content="summary" name="twitter:card"/>
<meta content="@bakadesuyo" name="twitter:site"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":["Person","Organization"],"@id":"http://www.bakadesuyo.com/#/schema/person/87ac8b58421526b8720d2b30ec6fc1c2","name":"Eric Barker","logo":{"@id":"http://www.bakadesuyo.com/#personlogo"},"sameAs":["http://www.bakadesuyo.com","https://www.facebook.com/bakadesuyo","https://twitter.com/bakadesuyo"]},{"@type":"WebSite","@id":"http://www.bakadesuyo.com/#website","url":"http://www.bakadesuyo.com/","name":"Barking Up The Wrong Tree","description":"How to be awesome at life.","publisher":{"@id":"http://www.bakadesuyo.com/#/schema/person/87ac8b58421526b8720d2b30ec6fc1c2"},"potentialAction":[{"@type":"SearchAction","target":"http://www.bakadesuyo.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"CollectionPage","@id":"http://www.bakadesuyo.com/#webpage","url":"http://www.bakadesuyo.com/","name":"Barking Up The Wrong Tree - How to be awesome at life.","isPartOf":{"@id":"http://www.bakadesuyo.com/#website"},"about":{"@id":"http://www.bakadesuyo.com/#/schema/person/87ac8b58421526b8720d2b30ec6fc1c2"},"description":"How to be awesome at life.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["http://www.bakadesuyo.com/"]}]}]}</script>
<meta content="702D4660632B1EEE4D761B433B6B7DE6" name="msvalidate.01"/>
<script type="text/javascript">console.log('PixelYourSite Free version 7.2.0');</script>
<link href="//platform-api.sharethis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.bakadesuyo.com/feed/" rel="alternate" title="Barking Up The Wrong Tree » Feed" type="application/rss+xml"/>
<link href="https://www.bakadesuyo.com/comments/feed/" rel="alternate" title="Barking Up The Wrong Tree » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.bakadesuyo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.2"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.bakadesuyo.com/wp-content/plugins/wp-google-search/wgs.css?ver=5.5.2" id="wgs-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.bakadesuyo.com/wp-content/plugins/wp-google-search/wgs2.css?ver=5.5.2" id="wgs2-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Merriweather%3A400%2C400italic%2C700%2C700italic%2C900%2C900italic&amp;ver=1.0" id="tws-merriweather-css" media="" rel="stylesheet" type="text/css"/>
<link href="https://www.bakadesuyo.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.2" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.bakadesuyo.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.bakadesuyo.com/wp-content/plugins/enable-jquery-migrate-helper/js/jquery-migrate-1.4.1-wp.js?ver=1.4.1-wp" type="text/javascript"></script>
<script id="googleanalytics-platform-sharethis-js" src="//platform-api.sharethis.com/js/sharethis.js#product=ga" type="text/javascript"></script>
<script id="jquery-bind-first-js" src="https://www.bakadesuyo.com/wp-content/plugins/pixelyoursite/dist/scripts/jquery.bind-first-0.2.3.min.js?ver=5.5.2" type="text/javascript"></script>
<script id="js-cookie-js" src="https://www.bakadesuyo.com/wp-content/plugins/pixelyoursite/dist/scripts/js.cookie-2.1.3.min.js?ver=2.1.3" type="text/javascript"></script>
<script id="pys-js-extra" type="text/javascript">
/* <![CDATA[ */
var pysOptions = {"staticEvents":{"facebook":{"PageView":[{"params":[],"delay":0,"ids":[],"eventID":""}],"GeneralEvent":[{"params":{"post_type":"page","content_name":"Barking Up The Wrong Tree"},"delay":0,"ids":[],"eventID":""}]}},"dynamicEventsParams":[],"dynamicEventsTriggers":[],"facebook":{"pixelIds":["487016868156543"],"advancedMatching":[],"removeMetadata":false,"contentParams":{"post_type":"page","post_id":null,"content_name":"Barking Up The Wrong Tree"},"commentEventEnabled":true,"wooVariableAsSimple":false,"downloadEnabled":true,"formEventEnabled":true,"ajaxForServerEvent":true,"serverApiEnabled":false,"wooCRSendFromServer":false},"debug":"","siteUrl":"https:\/\/www.bakadesuyo.com","ajaxUrl":"https:\/\/www.bakadesuyo.com\/wp-admin\/admin-ajax.php","commonEventParams":{"domain":"www.bakadesuyo.com","user_roles":"guest","plugin":"PixelYourSite"},"commentEventEnabled":"1","downloadEventEnabled":"1","downloadExtensions":["doc","exe","js","pdf","ppt","tgz","zip","xls"],"formEventEnabled":"1","gdpr":{"ajax_enabled":false,"all_disabled_by_api":false,"facebook_disabled_by_api":false,"analytics_disabled_by_api":false,"google_ads_disabled_by_api":false,"pinterest_disabled_by_api":false,"bing_disabled_by_api":false,"facebook_prior_consent_enabled":false,"analytics_prior_consent_enabled":true,"google_ads_prior_consent_enabled":null,"pinterest_prior_consent_enabled":true,"bing_prior_consent_enabled":true,"cookiebot_integration_enabled":false,"cookiebot_facebook_consent_category":"marketing","cookiebot_analytics_consent_category":"statistics","cookiebot_google_ads_consent_category":null,"cookiebot_pinterest_consent_category":"marketing","cookiebot_bing_consent_category":"marketing","ginger_integration_enabled":false,"cookie_notice_integration_enabled":false,"cookie_law_info_integration_enabled":false},"woo":{"enabled":false,"addToCartOnButtonEnabled":false,"addToCartOnButtonValueEnabled":false,"addToCartOnButtonValueOption":"price","removeFromCartEnabled":true,"removeFromCartSelector":".cart .product-remove .remove"},"edd":{"enabled":false,"addToCartOnButtonEnabled":false,"addToCartOnButtonValueEnabled":false,"addToCartOnButtonValueOption":"price","removeFromCartEnabled":true}};
/* ]]> */
</script>
<script id="pys-js" src="https://www.bakadesuyo.com/wp-content/plugins/pixelyoursite/dist/scripts/public.js?ver=7.2.0" type="text/javascript"></script>
<link href="https://www.bakadesuyo.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.bakadesuyo.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.bakadesuyo.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.2" name="generator"/>
<style id="custom-background-css" type="text/css">
body.custom-background { background-color: #ffffff; }
</style>
<link href="https://www.bakadesuyo.com/wp-content/uploads/2012/10/cropped-bakalogo-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://www.bakadesuyo.com/wp-content/uploads/2012/10/cropped-bakalogo-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://www.bakadesuyo.com/wp-content/uploads/2012/10/cropped-bakalogo-180x180.png" rel="apple-touch-icon"/>
<meta content="https://www.bakadesuyo.com/wp-content/uploads/2012/10/cropped-bakalogo-270x270.png" name="msapplication-TileImage"/>
<style id="tt-easy-google-font-styles" type="text/css">p { }
h1 { }
h2 { }
h3 { }
h4 { }
h5 { }
h6 { }
blockquote { background-color: #dbdbdb; padding-left: 75px; }
ol { }
ul { margin-top: 15px; margin-bottom: 35px; }
</style>
<script defer="" src="//cdn.geni.us/snippet.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function( $ ) {
	  var ale_on_click_checkbox_is_checked="1";
	  if(typeof Georiot !== "undefined")
	  {
		if(ale_on_click_checkbox_is_checked) {
			Georiot.amazon.addOnClickRedirect(21272, true);
		}
		else {
			Georiot.amazon.convertToGeoRiotLinks(21272, true);
		};
	  };
    });
  </script>
</head>
<body class="home blog custom-background">
<div id="wrap">
<div id="maincontent">
<div id="linkblock2">
<div id="side_logo2">
<a href="https://www.bakadesuyo.com"><img alt="logo" src="https://www.bakadesuyo.com/wp-content/uploads/2012/10/logo1.png"/></a>
</div>
<nav>
<div class="menu-menu1-container"><ul class="nav" id="menu-menu1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35308" id="menu-item-35308"><a href="https://www.bakadesuyo.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40778" id="menu-item-40778"><a href="https://www.bakadesuyo.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36097" id="menu-item-36097"><a href="https://www.bakadesuyo.com/search/">Best of / Search</a></li>
</ul></div>
</nav>
<aside class="sidebar_widgets">
</aside>
<div class="clearbig"></div>
</div>
<div id="respnav">
</div>
<aside>
<div id="linkblock">
<div id="side_logo">
<a href="https://www.bakadesuyo.com"><img alt="logo" src="https://www.bakadesuyo.com/wp-content/uploads/2012/10/logo1.png"/></a>
</div>
<nav>
<div class="menu-menu1-container"><ul class="nav" id="menu-menu1-1"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35308"><a href="https://www.bakadesuyo.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40778"><a href="https://www.bakadesuyo.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36097"><a href="https://www.bakadesuyo.com/search/">Best of / Search</a></li>
</ul></div>
</nav>
<aside class="sidebar_widgets">
</aside>
<div class="clear"></div>
</div>
</aside>
<div id="page_wrap">
<div id="control">
<a alt="Show/View your stuffs" class="open" href="#" id="controlbtn"></a>
</div>
<div class="content">
<section>
<article>
<div class="blog_wrapper">
<div class="post-46064 post type-post status-publish format-standard hentry category-family category-relationships" id="post-46064">
<div class="content">
<div class="post_details">
<h1><a href="https://www.bakadesuyo.com/2021/01/parent/">New Neuroscience Reveals 5 Rituals That Will Make You An Awesome Parent</a></h1>
<img alt="standard post" height="24" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/img/std_icon.png" width="31"/>
<div class="author">
Eric Barker  -  
</div>
<div class="post_date">
January 6, 2021
</div>
</div>
<p> <a class="more-link" href="https://www.bakadesuyo.com/2021/01/parent/"><span class="more-link">Read More</span></a></p>
<hr/>
</div>
</div>
</div>
<article>
<div class="blog_wrapper">
<div class="post-46043 post type-post status-publish format-standard hentry category-communication category-relationships category-good-life category-make-better-decisions" id="post-46043">
<div class="content">
<div class="post_details">
<h1><a href="https://www.bakadesuyo.com/2020/12/emotionally-intelligent-2/">New Neuroscience Reveals 4 Secrets That Will Make You Emotionally Intelligent</a></h1>
<img alt="standard post" height="24" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/img/std_icon.png" width="31"/>
<div class="author">
Eric Barker  -  
</div>
<div class="post_date">
December 2, 2020
</div>
</div>
<p> <a class="more-link" href="https://www.bakadesuyo.com/2020/12/emotionally-intelligent-2/"><span class="more-link">Read More</span></a></p>
<hr/>
</div>
</div>
</div>
<article>
<div class="blog_wrapper">
<div class="post-45889 post type-post status-publish format-standard hentry category-productivity" id="post-45889">
<div class="content">
<div class="post_details">
<h1><a href="https://www.bakadesuyo.com/2020/11/productive-people/">6 Things The Most Productive People Do Every Day</a></h1>
<img alt="standard post" height="24" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/img/std_icon.png" width="31"/>
<div class="author">
Eric Barker  -  
</div>
<div class="post_date">
November 3, 2020
</div>
</div>
<p> <a class="more-link" href="https://www.bakadesuyo.com/2020/11/productive-people/"><span class="more-link">Read More</span></a></p>
<hr/>
</div>
</div>
</div>
<article>
<div class="blog_wrapper">
<div class="post-45876 post type-post status-publish format-standard hentry category-uncategorized" id="post-45876">
<div class="content">
<div class="post_details">
<h1><a href="https://www.bakadesuyo.com/2020/08/lazy-way-to-an-awesome-life/">The Lazy Way To An Awesome Life: 3 Secrets Backed By Research</a></h1>
<img alt="standard post" height="24" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/img/std_icon.png" width="31"/>
<div class="author">
Eric Barker  -  
</div>
<div class="post_date">
August 10, 2020
</div>
</div>
<p> <a class="more-link" href="https://www.bakadesuyo.com/2020/08/lazy-way-to-an-awesome-life/"><span class="more-link">Read More</span></a></p>
<hr/>
</div>
</div>
</div>
<article>
<div class="blog_wrapper">
<div class="post-45843 post type-post status-publish format-standard hentry category-happiness category-good-life" id="post-45843">
<div class="content">
<div class="post_details">
<h1><a href="https://www.bakadesuyo.com/2020/06/what-have-you-learned/">4 Rituals To Keep You Happy All The Time (Pandemic Edition)</a></h1>
<img alt="standard post" height="24" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/img/std_icon.png" width="31"/>
<div class="author">
Eric Barker  -  
</div>
<div class="post_date">
June 24, 2020
</div>
</div>
<p> <a class="more-link" href="https://www.bakadesuyo.com/2020/06/what-have-you-learned/"><span class="more-link">Read More</span></a></p>
<hr/>
</div>
</div>
</div>
</article>
<div class="pagination">
<div class="pagination"><span>Page 1 of 1088</span><span class="current">1</span><a class="inactive" href="https://www.bakadesuyo.com/page/2/">2</a><a href="https://www.bakadesuyo.com/page/1088/">Last »</a></div>
</div>
<div class="clear"></div>
</article></article></article></article></section>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<footer>
<div id="toTop">Back to Top
</div>
</footer>
<script type="text/javascript">
// DOM ready
jQuery(document).ready(function(){
	 
jQuery("a#controlbtn").click(function(e) {
        e.preventDefault();
        var slidepx=jQuery("div#linkblock").width() + 10;
    	if ( !jQuery("div#maincontent").is(':animated') ) { 
			if (parseInt(jQuery("div#maincontent").css('marginLeft'), 10) < slidepx) {
     			jQuery(this).removeClass('close').html('');
      			margin = "+=" + slidepx;
    		} else {
     			jQuery(this).addClass('close').html('');
      			margin = "-=" + slidepx;
    		}
        	jQuery("div#maincontent").animate({ 
        		marginLeft: margin
      		}, {
                    duration: 'slow',
                    easing: 'easeOutQuint'
                });
    	} 
      }); 
	  
jQuery('#respnav').click(function() {
          jQuery('#linkblock2').animate({
               height: 'toggle'
               }, 500
          );
     });
	  
jQuery('.nav > li > a').click(function(){
    if (jQuery(this).attr('class') != 'active'){
      jQuery('.nav li ul.sub-menu').slideUp();
      jQuery(this).next().slideToggle();
      jQuery('.nav li a').removeClass('active');
      jQuery(this).addClass('active');
    }
  });
  
  
jQuery('#post').click(function() {
          jQuery('#post_details').animate({
               height: 'toggle'
               }, 500
          );
     });
	 
selectnav('menu-primary', {
				label: 'Menu',
				nested: true,
				indent: '-'
			});
	 
jQuery(window).scroll(function() {
		if(jQuery(this).scrollTop() != 0) {
			jQuery('#toTop').fadeIn();	
		} else {
			jQuery('#toTop').fadeOut();
		}
	});
 
jQuery('#toTop').click(function() {
		jQuery('body,html').animate({scrollTop:0},800);
	});	
	
jQuery('.videocontainer').fitVids({ customSelector: ""});
	
});

</script>
<script async="">(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='462647592761b6258c3f38bbf4995d7051d113e895054f0b2efab61ce80bd3af';j.dataset.sumoPlatform='wordpress';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script> <script type="application/javascript">
      var ajaxurl = "https://www.bakadesuyo.com/wp-admin/admin-ajax.php";

      function sumo_add_woocommerce_coupon(code) {
        jQuery.post(ajaxurl, {
          action: 'sumo_add_woocommerce_coupon',
          code: code,
        });
      }

      function sumo_remove_woocommerce_coupon(code) {
        jQuery.post(ajaxurl, {
          action: 'sumo_remove_woocommerce_coupon',
          code: code,
        });
      }

      function sumo_get_woocommerce_cart_subtotal(callback) {
        jQuery.ajax({
          method: 'POST',
          url: ajaxurl,
          dataType: 'html',
          data: {
            action: 'sumo_get_woocommerce_cart_subtotal',
          },
          success: function(subtotal) {
            return callback(null, subtotal);
          },
          error: function(err) {
            return callback(err, 0);
          }
        });
      }
    </script>
<noscript><img alt="facebook_pixel" height="1" src="https://www.facebook.com/tr?id=487016868156543&amp;ev=PageView&amp;noscript=1" style="display: none;" width="1"/></noscript>
<noscript><img alt="facebook_pixel" height="1" src="https://www.facebook.com/tr?id=487016868156543&amp;ev=GeneralEvent&amp;noscript=1&amp;cd[post_type]=page&amp;cd[content_name]=Barking+Up+The+Wrong+Tree" style="display: none;" width="1"/></noscript>
<script id="google_cse_v2-js-extra" type="text/javascript">
/* <![CDATA[ */
var scriptParams = {"google_search_engine_id":"008467448622153033003%3Aylfw1yr4xfa"};
/* ]]> */
</script>
<script id="google_cse_v2-js" src="https://www.bakadesuyo.com/wp-content/plugins/wp-google-search/assets/js/google_cse_v2.js?ver=1" type="text/javascript"></script>
<script id="retina-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/retina.js?ver=1.0" type="text/javascript"></script>
<script id="easing-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/jquery.easing.1.3.js?ver=1.0" type="text/javascript"></script>
<script id="hover-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/hoverIntent.js?ver=1.0" type="text/javascript"></script>
<script id="commentvaljs-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/jquery.validate.pack.js?ver=1.0" type="text/javascript"></script>
<script id="commentval-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/comment-form-validation.js?ver=1.0" type="text/javascript"></script>
<script id="selectnav-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/selectnav.js?ver=1.0" type="text/javascript"></script>
<script id="fitvids-js" src="https://www.bakadesuyo.com/wp-content/themes/featherlite/js/jquery.fitvids.js?ver=1.0" type="text/javascript"></script>
<script id="wp-embed-js" src="https://www.bakadesuyo.com/wp-includes/js/wp-embed.min.js?ver=5.5.2" type="text/javascript"></script>
</body>
</html>