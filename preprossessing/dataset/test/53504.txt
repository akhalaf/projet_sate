<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Страница не найдена - Академия здоровья</title>
<link href="/favicon.ico" rel="shortcut icon"/>
<title>Страница не найдена - Академия здоровья</title>
<!-- This site is optimized with the Yoast SEO plugin v13.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta content="noindex,follow" name="robots"/>
<meta content="ru_RU" property="og:locale"/>
<meta content="object" property="og:type"/>
<meta content="Страница не найдена - Академия здоровья" property="og:title"/>
<meta content="Академия здоровья" property="og:site_name"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Страница не найдена - Академия здоровья" name="twitter:title"/>
<script class="yoast-schema-graph yoast-schema-graph--main" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://a-zdorov.ru/#website","url":"https://a-zdorov.ru/","name":"\u0410\u043a\u0430\u0434\u0435\u043c\u0438\u044f \u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044f","potentialAction":{"@type":"SearchAction","target":"https://a-zdorov.ru/?s={search_term_string}","query-input":"required name=search_term_string"}}]}</script>
<!-- / Yoast SEO plugin. -->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":false,"svgExt":".svg","source":{"concatemoji":"https:\/\/a-zdorov.ru\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://a-zdorov.ru/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/bootstrap.min.css" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/font-awesome.min.css" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/slick.css" id="slick-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/jquery.fancybox.min.css?ver=2.0.0" id="fancybox-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/fonts.css" id="fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/style.css?ver=3.0.0" id="main-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-content/themes/azdorov/css/adapt.css" id="adapt-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://a-zdorov.ru/wp-json/" rel="https://api.w.org/"/>
<!-- Yandex.Metrika counter by Yandex Metrica Plugin -->
<script type="text/javascript">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(34338005, "init", {
        id:34338005,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
	        });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/34338005" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
<meta content="ac8d94b099d72beaa950062a7ec724e7" name="cmsmagazine"/>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<header class="header header-inside">
<div class="container">
<a class="site-logo" href="https://a-zdorov.ru"><img alt="" src="https://a-zdorov.ru/wp-content/themes/azdorov/images/site-logo.png"/></a>
<div class="menu-icon"></div>
<div class="header-right">
<div class="header-top">
<div class="header-contact">
<div class="hc-title">Адрес:</div>
<span>г. Волгодонск</span>
<span>ул. Молодежная 1а</span>
</div>
<div class="header-contact header-contact-time">
<div class="hc-title">Время работы:</div>
<span>Пн-Пт: 8 - 22</span>
<span>Сб-Вс: 10 - 20</span>
</div>
<a class="top-button" data-fancybox="" data-src="#call-form" href="javascript://">Заказать звонок</a>
<a class="top-phone" href="tel:88639265803">8 (8639) 26-58-03</a>
</div>
<div class="menu-block">
<div class="menu-burger">МЕНЮ</div>
<div class="menu-content">
<div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%b3%d0%bb%d0%b0%d0%b2%d0%bd%d0%be%d0%b5-container"><ul class="menu" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%b3%d0%bb%d0%b0%d0%b2%d0%bd%d0%be%d0%b5"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-127" id="menu-item-127"><a href="https://a-zdorov.ru/prices/">Цены</a></li>
<li class="menu-item menu-item-type-post_type_archive menu-item-object-treners menu-item-193" id="menu-item-193"><a href="https://a-zdorov.ru/treners/">Тренеры</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-316" id="menu-item-316"><a href="https://a-zdorov.ru/faq/">Вопрос-ответ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-465" id="menu-item-465"><a href="https://a-zdorov.ru/%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f/">Галерея</a></li>
</ul></div> </div>
<div class="menu-hidden">
<div class="menu-hidden-col1 menu-hidden-cols">
<div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bb%d0%b5%d0%b2%d0%be-container"><ul class="menu" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bb%d0%b5%d0%b2%d0%be"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112" id="menu-item-112"><a href="https://a-zdorov.ru/%d0%be-%d0%ba%d0%bb%d1%83%d0%b1%d0%b5/">О клубе</a></li>
</ul></div> </div>
<div class="menu-hidden-col2 menu-hidden-cols">
<div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d1%81%d0%b5%d1%80%d0%b5%d0%b4%d0%b8%d0%bd%d0%b0-container"><ul class="menu" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d1%81%d0%b5%d1%80%d0%b5%d0%b4%d0%b8%d0%bd%d0%b0"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-173" id="menu-item-173"><a href="https://a-zdorov.ru/category/articles/">Блог</a></li>
</ul></div> </div>
<div class="menu-hidden-col3 menu-hidden-cols">
<div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d1%80%d0%b0%d0%b2%d0%be-container"><ul class="menu" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d1%80%d0%b0%d0%b2%d0%be"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-140" id="menu-item-140"><a href="https://a-zdorov.ru/contacts/">Контакты</a></li>
</ul></div> </div>
<div class="menu-hidden-servis">
<a class="sm-title" href="/uslugi/">Услуги</a>
<div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d0%be%d0%b4%d0%bc%d0%b5%d0%bd%d1%8e-%d0%bb%d0%b5%d0%b2%d0%be-container"><ul class="sm-coll1 sm-coll" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d0%be%d0%b4%d0%bc%d0%b5%d0%bd%d1%8e-%d0%bb%d0%b5%d0%b2%d0%be"><li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-68" id="menu-item-68"><a href="https://a-zdorov.ru/uslugi/%d1%84%d0%b8%d1%82%d0%be%d0%b1%d0%b0%d1%80-%d0%ba%d0%b8%d1%81%d0%bb%d0%be%d1%80%d0%be%d0%b4/">Фитобар</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-71" id="menu-item-71"><a href="https://a-zdorov.ru/uslugi/%d1%82%d0%b0%d0%bd%d1%86%d1%8b/">Танцевальные направления</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-464" id="menu-item-464"><a href="https://a-zdorov.ru/uslugi/%d0%b5%d0%b4%d0%b8%d0%bd%d0%be%d0%b1%d0%be%d1%80%d1%81%d1%82%d0%b2%d0%be/">Зал единоборств</a></li>
</ul></div> <div class="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d0%be%d0%b4%d0%bc%d0%b5%d0%bd%d1%8e-%d0%bf%d1%80%d0%b0%d0%b2%d0%be-container"><ul class="sm-coll2 sm-coll" id="menu-%d1%88%d0%b0%d0%bf%d0%ba%d0%b0-%d0%bf%d0%be%d0%b4%d0%bc%d0%b5%d0%bd%d1%8e-%d0%bf%d1%80%d0%b0%d0%b2%d0%be"><li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-73" id="menu-item-73"><a href="https://a-zdorov.ru/uslugi/%d1%81%d0%b0%d1%83%d0%bd%d0%b0/">Сауна с летней беседкой</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-76" id="menu-item-76"><a href="https://a-zdorov.ru/uslugi/%d0%b0%d1%8d%d1%80%d0%be%d0%b1%d0%b8%d0%ba%d0%b0/">Групповые программы</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-77" id="menu-item-77"><a href="https://a-zdorov.ru/uslugi/%d1%82%d1%80%d0%b5%d0%bd%d0%b0%d0%b6%d0%b5%d1%80%d0%bd%d1%8b%d0%b9-%d0%b7%d0%b0%d0%bb/">Тренажерный зал</a></li>
</ul></div> </div>
</div>
</div>
</div>
</div>
</header><footer class="footer">
<div class="container">
<a class="footer-logo" href="https://a-zdorov.ru"></a>
<div class="footer-right">
<div class="footer-menu">
<div class="fmenu-menu">
<div class="menu-%d1%84%d1%83%d1%82%d0%b5%d1%80-container"><ul class="menu" id="menu-%d1%84%d1%83%d1%82%d0%b5%d1%80"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130" id="menu-item-130"><a href="https://a-zdorov.ru/prices/">Цены</a></li>
</ul></div> </div>
</div>
<div class="footer-contact-block">
<div class="footer-contact">
<div class="hc-title">Адрес:</div>
<span>г. Волгодонск</span>
<span>ул. Молодежная 1а</span>
</div>
<div class="footer-contact footer-contact-time">
<div class="hc-title">Время работы:</div>
<span>Пн-Пт: 8 - 22</span>
<span>Сб-Вс: 10 - 20</span>
</div>
<a class="top-button" data-fancybox="" data-src="#call-form" href="javascript://">Заказать звонок</a>
<a class="top-phone" href="tel:88639265803">8 (8639) 26-58-03</a>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="footer-copy">
<div class="container">
			© 2021 Все права защищены  
		
		<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=34338005&amp;from=informer" rel="nofollow" style="float: right;" target="_blank"><img alt="Яндекс.Метрика" class="ym-advanced-informer" data-cid="34338005" data-lang="ru" src="https://informer.yandex.ru/informer/34338005/3_1_B953FFFF_9933FFFF_1_pageviews" style="width:88px; height:31px; border:0;" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"/></a>
<!-- /Yandex.Metrika informer -->
</div>
</div>
</footer>
<div style="display:none;">
<div class="call-form" id="call-form">
<div class="lbform-title">ЗАПОЛНИТЕ ФОРМУ</div>
<div class="lb-form">
<div class="wpcf7" dir="ltr" id="wpcf7-f10-o1" lang="ru-RU" role="form">
<div class="screen-reader-response"></div>
<form action="/inc/atualizacaomoduloseguranca2016vgy8kzknxrjrsicki3bk4n%09#wpcf7-f10-o1" class="wpcf7-form" method="post" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" type="hidden" value="10"/>
<input name="_wpcf7_version" type="hidden" value="5.1.3"/>
<input name="_wpcf7_locale" type="hidden" value="ru_RU"/>
<input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f10-o1"/>
<input name="_wpcf7_container_post" type="hidden" value="0"/>
</div>
<p><span class="wpcf7-form-control-wrap inputname"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="inputname" placeholder="Ваше имя" size="40" type="text" value=""/></span><span class="wpcf7-form-control-wrap inputtel"><input aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" name="inputtel" placeholder="Ваш телефон" size="40" type="tel" value=""/></span><input class="wpcf7-form-control wpcf7-submit" type="submit" value="Отправить"/></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div> </div>
</div>
<div class="call-form" id="thank-form">
<div class="lbform-title">СПАСИБО</div>
<div class="thank-text">Мы свяжемся с Вами в ближайшее время</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ $("body").css("padding-bottom",  $(".footer").outerHeight() + "px"); $( window ).resize(function() { $("body").css("padding-bottom",  $(".footer").outerHeight() + "px"); }); });
</script>
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/a-zdorov.ru\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script src="https://a-zdorov.ru/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.3" type="text/javascript"></script>
<script src="https://a-zdorov.ru/wp-content/themes/azdorov/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://a-zdorov.ru/wp-content/themes/azdorov/js/slick.min.js" type="text/javascript"></script>
<script src="https://a-zdorov.ru/wp-content/themes/azdorov/js/jquery.fancybox.min.js?ver=2.0.0" type="text/javascript"></script>
<script src="https://a-zdorov.ru/wp-content/themes/azdorov/js/script.js?ver=2.0.0" type="text/javascript"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(34338005, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img alt="" src="https://mc.yandex.ru/watch/34338005" style="position:absolute; left:-9999px;"/></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>