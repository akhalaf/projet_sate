<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="yes" name="mobile-web-app-capable"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="歪麦博客 - 蒙国造的技术博客" name="apple-mobile-web-app-title"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://www.awaimai.com/xmlrpc.php" rel="pingback"/>
<title>歪麦博客 - 蒙国造的技术博客</title>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.awaimai.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.awaimai.com/wp-content/plugins/prismatic/css/styles-blocks.css?ver=5.2.9" id="prismatic-blocks-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.awaimai.com/wp-content/plugins/table-of-contents-plus/screen.min.css?ver=2002" id="toc-screen-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.awaimai.com/wp-includes/css/dashicons.min.css?ver=5.2.9" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.awaimai.com/wp-content/themes/maizi/assets/bootstrap/css/bootstrap.min.css?ver=4.3.1" id="bootstrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.awaimai.com/wp-content/themes/maizi/style.css?ver=1.0.0" id="style-css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://www.awaimai.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.awaimai.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://www.awaimai.com/wp-json/" rel="https://api.w.org/"/>
<style type="text/css">
        
        #primaryMenu.navbar,
        #primaryMenu .dropdown-menu {
			background-color: #000000 !important;
		}
        #primaryMenu .navbar-brand a,
        #primaryMenu .nav-link:focus,
        #primaryMenu .nav-link:hover,
        #primaryMenu .nav-link,
        #primaryMenu .active>.nav-link,
        #primaryMenu .nav-link.active,
        #primaryMenu .nav-link.open,
        #primaryMenu .open>.nav-link,
        #primaryMenu .dropdown-item,
        #primaryMenu .dropdown-item.active,
        #primaryMenu .dropdown-item:active,
        #primaryMenu .dropdown-item:focus,
        #primaryMenu .dropdown-item:hover{
			color: #ffffff !important;
		}
        a, h2, h3, h4, h5, h6 {
			color: #428BD1;
		}
        a:focus, a:hover {
			color: #3071A9		}
		.btn-outline-primary, .btn-primary:active, .btn-outline-primary:disabled{
			color: #0275d8;
			border-color: #0275d8;
		}
		.btn-primary,.btn-primary:hover,.btn-primary:active, .btn-primary:focus,
		.btn-primary.disabled, .btn-primary:disabled,
		.btn-outline-primary:hover,.btn-outline-primary:active, .btn-outline-primary:focus,
		.page-item.active .page-link{
			color: #fff;
			box-shadow: none;
			background-color: #0275d8;
			border-color: #0275d8;
		}
		.form-control:focus {
			border-color: #0275d8;
		}
		.page-link, .page-link:focus, .page-link:hover{
			color: #0275d8;
		}
        .home article, .archive article {
            border-bottom: 1px solid #fff;
        }
    </style>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<!-- There is no amphtml version available for this URL. --></head>
<body class="home blog hfeed">
<div class="hfeed site" id="page">
<!-- ******************* The Navbar Area ******************* -->
<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">
<nav class="navbar navbar-expand-md navbar-dark bg-dark" id="primaryMenu">
<div class="container">
<!-- Your site title as branding in the menu -->
<h1 class="navbar-brand mb-0"><a href="https://www.awaimai.com/" rel="home" title="歪麦博客">歪麦博客</a></h1>
<!-- end custom logo -->
<button aria-controls="primaryMenuContainer" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#primaryMenuContainer" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<!-- The WordPress Menu goes here -->
<div class="collapse navbar-collapse" id="primaryMenuContainer"><ul class="navbar-nav ml-auto mr-0" id="menu-%e4%b8%bb%e8%8f%9c%e5%8d%95"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5 nav-item active" id="menu-item-5"><a class="nav-link active" href="https://www.awaimai.com/">首页</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2370 nav-item" id="menu-item-2370"><a class="nav-link" href="https://www.awaimai.com/php">PHP启示录</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2194 nav-item" id="menu-item-2194"><a class="nav-link" href="https://www.awaimai.com/patterns">PHP设计模式</a></li><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-505 nav-item" id="menu-item-505"><a class="nav-link" href="https://www.awaimai.com/category/notes">记录</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2040 nav-item" id="menu-item-2040"><a class="nav-link" href="https://www.awaimai.com/about">关于&amp;捐赠</a></li></ul></div> </div><!-- .container -->
</nav><!-- .site-navigation -->
</div><!-- .wrapper-navbar end -->
<div class="wrapper" id="wrapper-index">
<div class="container" id="content" tabindex="-1">
<div class="row d-flex flex-row">
<div class="col-md-9 content-area" id="primary">
<main class="site-main" id="main">
<article class="container mb-4 pb-4 post-2120 post type-post status-publish format-standard sticky hentry category-php tag-docker tag-mysql tag-nginx tag-php-fpm" id="post-2120">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2120.html" rel="bookmark">Docker LNMP环境搭建</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2017-04-22T18:33:32+08:00">2017-04-22</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 82.5k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/php" rel="category tag">PHP教程</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2120.html#comments">314评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         本文介绍如何使用Docker容器，快速部署LNMP环境。 最终完成的环境我们称为：DNMP（即 Docker + Nginx + MySQL + PHP）。 最终实现一键部署LNMP环境的目的，该环境特点： 完全开源 支持多版本PHP切换（PHP5.4、PHP5.6、PHP7.2...) 支持绑定任意多个域名 支持HTTPS和HTTP/2 PHP源代码位于host中 MySQL data位于host中 所有配置文件可在ho…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-128 post type-post status-publish format-standard sticky hentry category-php tag-fastphp tag-mvc tag-php" id="post-128">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/128.html" rel="bookmark">手把手编写PHP MVC框架实例教程</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2015-09-06T18:06:37+08:00">2015-09-06</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 126.2k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/php" rel="category tag">PHP教程</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/128.html#comments">378评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         1 什么是MVC MVC模式（Model-View-Controller）是软件工程中的一种软件架构模式。 MVC把软件系统分为三个基本部分：模型（Model）、视图（View）和控制器（Controller）。 PHP中MVC模式也称Web MVC，从上世纪70年代进化而来。 MVC的目的是实现一种动态的程序设计，便于后续对程序的修改和扩展简化，并且使程序某一部分的重复利用成为可…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2864 post type-post status-publish format-standard hentry category-notes tag-git tag-gitee tag-github" id="post-2864">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2864.html" rel="bookmark">git增加多个push仓库: 一条push命令同时推送到gitee和github</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-09-18T18:56:37+08:00">2020-09-18</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 348</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2864.html#comments">4评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         最终效果是，执行如下一个命令，就能同时推送代码到gitee和github。 git push origin master 实现步骤如下： 1、添加两个remote 命令如下。 git remote add origin git@gitee.com:yeszao/dnmp.git git remote add github git@github.com:yeszao/dnmp.git 第一个为码云，仓库名为origin。默认情况下，git clone下来的代码默认远程仓库名…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2852 post type-post status-publish format-standard hentry category-notes" id="post-2852">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2852.html" rel="bookmark">小米路由器3刷潘多拉(Openwrt)</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-05-04T21:43:09+08:00">2020-05-04</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 11.5k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2852.html#comments">3评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         小米路由器 3 默认系统除了后台慢，网络也慢，还不能安装插件！留之何用！ 刚好看到openwrt有刷机教程，刷之~ 1 下载 小米路由器开发版2.11.20：http://bigota.miwifi.com/xiaoqiang/rom/r3/miwifi_r3_all_55ac7_2.11.20.bin下载潘多拉刷机包，链接: https://pan.baidu.com/s/1_kwZU1Pi2LtymBwRbP8DwA 提取码: fm2r下载潘多拉刷机包（…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2846 post type-post status-publish format-standard hentry category-notes" id="post-2846">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2846.html" rel="bookmark">build.gradle或pom.xml使用国内阿里云源</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-02-16T21:39:04+08:00">2020-02-16</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 772</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2846.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         打开文件： ~/.m2/settings.xml 设置内容为： &lt;settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd"&gt; &lt;localRepository/&gt; &lt;interactiveMode/&gt; &amp;l…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2844 post type-post status-publish format-standard hentry category-notes tag-ffmpeg tag-mp3" id="post-2844">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2844.html" rel="bookmark">一条ffmpeg命令批量转换flac到mp3格式</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-02-13T20:26:25+08:00">2020-02-13</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 2.0k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2844.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         最近用小米的米小兔音响，听歌还是很方便的，又耐摔，虽然音质一般，不过对于听二哥来说，已经绰绰有余了。 电脑上有很多flac格式的儿歌，放到米小兔上又播放不了，得转为mp3才行。找了几个方案： 在线flac转mp3python脚本ffmpeg 第一种方式要上传flac和下载mp3，很费时间，有些在线转换网站还有容量限制，试过一次，实在无法忍受。 第…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2842 post type-post status-publish format-standard hentry category-notes tag-flutter" id="post-2842">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2842.html" rel="bookmark">flutter使用build.gradle国内源解决编译慢问题</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-02-06T22:52:38+08:00">2020-02-06</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 3.1k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2842.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         编译的时候使用-v显示详情： flutter run -v 可能会在这里卡住： Could not resolve all files for configuration 'classpath' 使用国内源方法如下。 文件一：android/app/build.gradle //... buildscript { repositories { //google() //jcenter() maven { url 'https://maven.aliyun.com/repository/google' } maven { url 'https://…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2839 post type-post status-publish format-standard hentry category-notes" id="post-2839">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2839.html" rel="bookmark">Python3下载文件或图片方法</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-01-31T21:07:57+08:00">2020-01-31</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 2.3k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2839.html#comments">一条评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         1、使用requests import os import requests def download_file(url, store_path): filename = url.split("/")[-1] filepath = os.path.join(store_path, filename) file_data = requests.get(url, allow_redirects=True).content with open(filepath, 'wb') as handler: handler.write(file_data) 2、使用urllib.request.urlretrieve …                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2838 post type-post status-publish format-standard hentry category-notes" id="post-2838">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2838.html" rel="bookmark">Python3 urllib.request.urlretrieve如何下载文件或图片以及如何使用代理？</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2020-01-31T20:55:30+08:00">2020-01-31</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 1.1k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2838.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         使用urllib.request.urlretrieve方式： from urllib.request import urlretrieve urlretrieve("https://www.awaimai.com/wp-content/uploads/2017/09/phpinfo.png", "phpinfo.png") 如果要使用代理，需要先导入：socket和httplib2.socks模块： from urllib.request import urlretrieve import config from httplib2 import socks impor…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2835 post type-post status-publish format-standard hentry category-notes" id="post-2835">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2835.html" rel="bookmark">flutter国内镜像和下载地址集合</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2019-11-23T21:03:18+08:00">2019-11-23</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 18.2k</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2835.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         国内下载flutter和安装依赖实在是太太太太太慢了！！！ 下面是国内几个镜像，PUB_HOSTED_URL是Dart依赖包地址，FLUTTER_STORAGE_BASE_URL是Flutter依赖文件地址。 要手动下载最新版本的flutter SDK包，可以访问 FLUTTER_STORAGE_BASE_URL 地址，然后进入/flutter_infra/releases/目录下载（这个方式Flutter社区不可用）。 Flutter 社区…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2535 post type-post status-publish format-standard hentry category-mysql tag-virtualbox tag-161" id="post-2535">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2535.html" rel="bookmark">命令行运行VirtualBox虚拟机</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2019-10-28T15:40:42+08:00">2019-10-28</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 562</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/mysql" rel="category tag">MySQL</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2535.html#comments">一条评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         1 安装Extension Pack 首先，需要安装一个与VirtualBox版本对应的Extension Pack，比如我的VirtualBox是6.0.10，那我就下一个同版本的Extension Pack。 Extension Pack下载地址（与VB在同一个界面）：https://www.virtualbox.org/wiki/Download_Old_Builds。 然后安装： 打开VirtualBox界面File | Preferences切换到 Extensions 区域点…                </div>
</div>
</div>
</div>
</article>
<article class="container mb-4 pb-4 post-2821 post type-post status-publish format-standard hentry category-notes tag-ddos tag-redis tag-351 tag-352" id="post-2821">
<div class="row">
<div class="entry-content col-sm-12">
<div class="row">
<header class="entry-header p-0 col-sm-12">
<h2 class="entry-title font-weight-bold"><a href="https://www.awaimai.com/2821.html" rel="bookmark">MurmurHash2 哈希算法碰撞引起的Redis DDos 攻击漏洞</a></h2> </header>
<div class="entry-meta my-1 text-muted small p-0 col-sm-12">
<span class="posted-date meta-item mr-3 text-muted"><i class="dashicons dashicons-clock icon14"></i> <time class="published" datetime="2019-09-29T16:32:40+08:00">2019-09-29</time></span><span class="posted-view meta-item mr-3 text-muted"><i class="dashicons dashicons-visibility icon14"></i> 699</span><span class="cat-links meta-item mr-3 text-muted"><i class="dashicons dashicons-category icon14"></i> <a class="text-muted" href="https://www.awaimai.com/category/notes" rel="category tag">记录</a></span><span class="comments-link meta-item mr-3"><i class="dashicons dashicons-admin-comments icon14"></i> <a class="text-muted" href="https://www.awaimai.com/2821.html#respond">发表评论</a></span> </div>
<div class="col-sm-12 p-0">
                                         概要信息： 在Martin Bosslet 2012年的这篇文章中，作者提到MurmurHash2算法被发现可以稳定构造碰撞函数，该哈希函数及其变形被CRuby, JRuby, Rubinius, Redis等开源组件使用。本文是基于Martin Bosslet的发现继续挖掘的结果，在此对Martin Bosslet表示感谢。原文中作者的碰撞函数是基于Ruby完成的，这里将发布该碰撞函数的Python版本…                </div>
</div>
</div>
</div>
</article>
</main><!-- #main -->
<!-- The pagination component -->
<nav aria-label="Page navigation"><ul class="pagination justify-content-center"><li class="page-item active"><span class="page-link current">1</span></li><li class="page-item "><a class="page-link" href="https://www.awaimai.com/page/2">2</a></li><li class="page-item "><a class="page-link" href="https://www.awaimai.com/page/3">3</a></li><li class="page-item "><span class="page-link dots">…</span></li><li class="page-item "><a class="page-link" href="https://www.awaimai.com/page/31">31</a></li><li class="page-item "><a class="next page-link" href="https://www.awaimai.com/page/2"> » </a></li></ul></nav>
</div><!-- #primary -->
<div class="col-md-3 widget-area" id="global-sidebar" role="complementary">
<aside class="widget widget_recent_comments" id="recent-comments-2"><h3 class="widget-title">近期评论</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link">999</span>发表在《<a href="https://www.awaimai.com/2053.html#comment-39338">video.js使用技巧</a>》</li><li class="recentcomments"><span class="comment-author-link">chen</span>发表在《<a href="https://www.awaimai.com/2053.html#comment-39302">video.js使用技巧</a>》</li><li class="recentcomments"><span class="comment-author-link">小河马</span>发表在《<a href="https://www.awaimai.com/2762.html#comment-39276">Flutter自动换行和两列布局</a>》</li><li class="recentcomments"><span class="comment-author-link"><a class="url" href="https://eosrce.com" rel="external nofollow">E#</a></span>发表在《<a href="https://www.awaimai.com/128.html#comment-39252">手把手编写PHP MVC框架实例教程</a>》</li><li class="recentcomments"><span class="comment-author-link">willem</span>发表在《<a href="https://www.awaimai.com/2615.html#comment-39084">Docker cron定时任务</a>》</li></ul></aside><aside class="widget_text widget widget_custom_html" id="custom_html-10"><h3 class="widget-title">友情链接</h3><div class="textwidget custom-html-widget"><a href="https://www.jinbangx.com/" rel="noopener noreferrer" target="_blank">Java面试题精选</a>
<br/>
<a href="http://baijunyao.com/" rel="noopener noreferrer" target="_blank">白俊遥博客</a>
<br/>
<a href="http://www.laruence.com/" rel="noopener noreferrer" target="_blank">风雪之隅</a>
<br/>
<a href="http://www.lanecn.com" rel="noopener noreferrer" target="_blank">PHP博客</a>
<br/>
<a href="https://oomake.com/" rel="noopener noreferrer" target="_blank">码客</a>
<br/>
<a href="https://www.javatt.com/" rel="noopener noreferrer" target="_blank">Java天堂
</a>
<br/>
<a href="http://www.phpernote.com/" rel="noopener noreferrer" target="_blank">phpernote
</a>
<br/>
<a href="https://ttvps.com/web-hosting/" rel="noopener noreferrer" target="_blank">WordPress主机评测
</a></div></aside>
<aside class="widget widget_search" id="search-5"><form action="https://www.awaimai.com/" id="searchform" method="get" role="search">
<div class="input-group">
<input class="field form-control" id="s" name="s" placeholder="搜索…" type="text"/>
<span class="input-group-btn">
<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit" value="搜索"/>
</span>
</div>
</form>
</aside> </div>
</div><!-- .row -->
</div><!-- Container end -->
</div><!-- Wrapper end -->
<div class="wrapper" id="wrapper-footer">
<div class="container">
<div class="row">
<div class="col-md-12">
<footer class="site-footer" id="colophon">
<div class="site-info">
<span>Theme: <a href="https://www.awaimai.com/maizi">Maizi</a>.</span>
<span>Powered by <a href="https://wordpress.org/" target="_blank">WordPress</a>.</span>
<a href="http://www.beian.miit.gov.cn/" rel="nofollow" target="_blank" title="工业和信息化部ICP/IP地址/域名信息备案管理系统">粤ICP备17138695号</a>
<div class="widget_text footer-copyright"><div class="textwidget custom-html-widget"><a href="https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=tqauql8y" rel="noopener noreferrer" target="_blank">
<img alt="" class="alignnone size-full wp-image-2600" height="32" src="https://www.awaimai.com/wp-content/uploads/2018/12/aliyun-logo-mini.png" width="105"/></a>
<!--
<a href="https://github.com/yeszao" target="_blank" rel="noopener noreferrer">
<img class="alignnone size-full wp-image-2600" src="https://www.awaimai.com/wp-content/uploads/2019/01/GitHub-Mark-32px.png" alt="" width="32" height="32" /></a>
-->
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?966af6658810dc312555cfee9a15e788";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script></div></div>
</div><!-- .site-info -->
<a class="scroll-top" href="#">
<i class="dashicons dashicons-arrow-up-alt icon16"></i>
</a>
</footer><!-- #colophon -->
</div><!--col end -->
</div><!-- row end -->
</div><!-- container end -->
</div><!-- wrapper end -->
</div><!-- #page -->
<script src="https://www.awaimai.com/wp-content/plugins/table-of-contents-plus/front.min.js?ver=2002" type="text/javascript"></script>
<script src="https://www.awaimai.com/wp-content/themes/maizi/assets/js/popper.min.js?ver=1.14.7" type="text/javascript"></script>
<script src="https://www.awaimai.com/wp-content/themes/maizi/assets/bootstrap/js/bootstrap.min.js?ver=4.3.1" type="text/javascript"></script>
<script src="https://www.awaimai.com/wp-content/themes/maizi/assets/js/js.cookie-2.2.1.min.js?ver=2.2.1" type="text/javascript"></script>
<script src="https://www.awaimai.com/wp-content/themes/maizi/assets/js/theme.js?ver=1.0.0" type="text/javascript"></script>
<script type="text/javascript">
var $ = jQuery;
</script>
<script src="https://www.awaimai.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
</body>
</html>
