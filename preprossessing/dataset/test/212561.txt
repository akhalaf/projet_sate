<!DOCTYPE html>
<html class="html_stretched responsive av-preloader-disabled av-default-lightbox html_header_top html_logo_left html_main_nav_header html_menu_right html_custom html_header_sticky html_header_shrinking_disabled html_mobile_menu_tablet html_header_searchicon_disabled html_content_align_center html_header_unstick_top_disabled html_header_stretch_disabled html_minimal_header html_minimal_header_shadow html_av-submenu-hidden html_av-submenu-display-click html_av-overlay-side html_av-overlay-side-classic html_av-submenu-noclone av-cookies-no-cookie-consent av-no-preview html_text_menu_active " lang="es">
<head>
<meta charset="utf-8"/>
<!-- mobile setting -->
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!-- Scripts/CSS and wp_head hook -->
<!-- This site is optimized with the Yoast SEO plugin v15.2 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Página no encontrada - Best Picture System</title>
<meta content="noindex, follow" name="robots"/>
<meta content="es_ES" property="og:locale"/>
<meta content="Página no encontrada - Best Picture System" property="og:title"/>
<meta content="Best Picture System" property="og:site_name"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://bestpicturesystem.com/#website","url":"https://bestpicturesystem.com/","name":"Best Picture System","description":"Producci\u00f3n Cinematogr\u00e1fica","potentialAction":[{"@type":"SearchAction","target":"https://bestpicturesystem.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"es"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://bestpicturesystem.com/feed/" rel="alternate" title="Best Picture System » Feed" type="application/rss+xml"/>
<link href="https://bestpicturesystem.com/comments/feed/" rel="alternate" title="Best Picture System » Feed de los comentarios" type="application/rss+xml"/>
<!-- google webfont font replacement -->
<script type="text/javascript">

				(function() {
					
					/*	check if webfonts are disabled by user setting via cookie - or user must opt in.	*/
					var html = document.getElementsByTagName('html')[0];
					var cookie_check = html.className.indexOf('av-cookies-needs-opt-in') >= 0 || html.className.indexOf('av-cookies-can-opt-out') >= 0;
					var allow_continue = true;
					var silent_accept_cookie = html.className.indexOf('av-cookies-user-silent-accept') >= 0;

					if( cookie_check && ! silent_accept_cookie )
					{
						if( ! document.cookie.match(/aviaCookieConsent/) || html.className.indexOf('av-cookies-session-refused') >= 0 )
						{
							allow_continue = false;
						}
						else
						{
							if( ! document.cookie.match(/aviaPrivacyRefuseCookiesHideBar/) )
							{
								allow_continue = false;
							}
							else if( ! document.cookie.match(/aviaPrivacyEssentialCookiesEnabled/) )
							{
								allow_continue = false;
							}
							else if( document.cookie.match(/aviaPrivacyGoogleWebfontsDisabled/) )
							{
								allow_continue = false;
							}
						}
					}
					
					if( allow_continue )
					{
						var f = document.createElement('link');
					
						f.type 	= 'text/css';
						f.rel 	= 'stylesheet';
						f.href 	= '//fonts.googleapis.com/css?family=Kreon';
						f.id 	= 'avia-google-webfont';

						document.getElementsByTagName('head')[0].appendChild(f);
					}
				})();
			
			</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/bestpicturesystem.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://bestpicturesystem.com/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.23" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://bestpicturesystem.com/wp-content/uploads/dynamic_avia/avia-merged-styles-22003a88806bf84d57c8c5202932593d---5f99cc4cd4e1e.css" id="avia-merged-styles-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://bestpicturesystem.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="tp-tools-js" src="https://bestpicturesystem.com/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.2.23" type="text/javascript"></script>
<script id="revmin-js" src="https://bestpicturesystem.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.23" type="text/javascript"></script>
<link href="https://bestpicturesystem.com/wp-json/" rel="https://api.w.org/"/><link href="https://bestpicturesystem.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://bestpicturesystem.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://bestpicturesystem.com/feed/" rel="alternate" title="Best Picture System RSS2 Feed" type="application/rss+xml"/>
<link href="https://bestpicturesystem.com/xmlrpc.php" rel="pingback"/>
<style media="screen" type="text/css">
 #top #header_main > .container, #top #header_main > .container .main_menu  .av-main-nav > li > a, #top #header_main #menu-item-shop .cart_dropdown_link{ height:110px; line-height: 110px; }
 .html_top_nav_header .av-logo-container{ height:110px;  }
 .html_header_top.html_header_sticky #top #wrap_all #main{ padding-top:110px; } 
</style>
<!--[if lt IE 9]><script src="https://bestpicturesystem.com/wp-content/themes/enfold/js/html5shiv.js"></script><![endif]-->
<link href="https://bestpicturesystem.com/wp-content/uploads/2020/09/favicon-bps.png" rel="icon" type="image/png"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by Slider Revolution 6.2.23 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<!-- To speed up the rendering and to display the site as fast as possible to the user we include some styles and scripts for above the fold content inline -->
<script type="text/javascript">'use strict';var avia_is_mobile=!1;if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)&&'ontouchstart' in document.documentElement){avia_is_mobile=!0;document.documentElement.className+=' avia_mobile '}
else{document.documentElement.className+=' avia_desktop '};document.documentElement.className+=' js_active ';(function(){var e=['-webkit-','-moz-','-ms-',''],n='';for(var t in e){if(e[t]+'transform' in document.documentElement.style){document.documentElement.className+=' avia_transform ';n=e[t]+'transform'};if(e[t]+'perspective' in document.documentElement.style)document.documentElement.className+=' avia_transform3d '};if(typeof document.getElementsByClassName=='function'&&typeof document.documentElement.getBoundingClientRect=='function'&&avia_is_mobile==!1){if(n&&window.innerHeight>0){setTimeout(function(){var e=0,o={},a=0,t=document.getElementsByClassName('av-parallax'),i=window.pageYOffset||document.documentElement.scrollTop;for(e=0;e<t.length;e++){t[e].style.top='0px';o=t[e].getBoundingClientRect();a=Math.ceil((window.innerHeight+i-o.top)*0.3);t[e].style[n]='translate(0px, '+a+'px)';t[e].style.top='auto';t[e].className+=' enabled-parallax '}},50)}}})();</script><link href="https://bestpicturesystem.com/wp-content/uploads/2020/07/cropped-BPS-negro-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://bestpicturesystem.com/wp-content/uploads/2020/07/cropped-BPS-negro-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://bestpicturesystem.com/wp-content/uploads/2020/07/cropped-BPS-negro-1-180x180.png" rel="apple-touch-icon"/>
<meta content="https://bestpicturesystem.com/wp-content/uploads/2020/07/cropped-BPS-negro-1-270x270.png" name="msapplication-TileImage"/>
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<style type="text/css">
@font-face {font-family: 'entypo-fontello'; font-weight: normal; font-style: normal; font-display: auto;
src: url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot');
src: url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot?#iefix') format('embedded-opentype'), 
url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.woff') format('woff'),
url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.woff2') format('woff2'),
url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.ttf') format('truetype'), 
url('https://bestpicturesystem.com/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.svg#entypo-fontello') format('svg');
} #top .avia-font-entypo-fontello, body .avia-font-entypo-fontello, html body [data-av_iconfont='entypo-fontello']:before{ font-family: 'entypo-fontello'; }

@font-face {font-family: 'fontello'; font-weight: normal; font-style: normal; font-display: auto;
src: url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.eot');
src: url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.eot?#iefix') format('embedded-opentype'), 
url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.woff') format('woff'),
url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.woff2') format('woff2'),
url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.ttf') format('truetype'), 
url('https://bestpicturesystem.com/wp-content/uploads/avia_fonts/fontello/fontello.svg#fontello') format('svg');
} #top .avia-font-fontello, body .avia-font-fontello, html body [data-av_iconfont='fontello']:before{ font-family: 'fontello'; }
</style>
<!--
Debugging Info for Theme support: 

Theme: Enfold
Version: 4.7.5
Installed: enfold
AviaFramework Version: 5.0
AviaBuilder Version: 4.7.1.1
aviaElementManager Version: 1.0.1
ML:256-PU:33-PLA:5
WP:5.5.3
Compress: CSS:all theme files - JS:all theme files
Updates: disabled
PLAu:5
--> <link href="https://fonts.googleapis.com/css2?family=Kreon:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet"/>
</head>
<body class="error404 rtl_columns stretched kreon" id="top" itemscope="itemscope" itemtype="https://schema.org/WebPage">
<div id="wrap_all">
<header class="all_colors header_color dark_bg_color av_header_top av_logo_left av_main_nav_header av_menu_right av_custom av_header_sticky av_header_shrinking_disabled av_header_stretch_disabled av_mobile_menu_tablet av_header_searchicon_disabled av_header_unstick_top_disabled av_minimal_header av_minimal_header_shadow av_bottom_nav_disabled av_header_border_disabled" id="header" itemscope="itemscope" itemtype="https://schema.org/WPHeader" role="banner">
<div class="container_wrap container_wrap_logo" id="header_main">
<div class="container av-logo-container"><div class="inner-container"><span class="logo"><a href="https://bestpicturesystem.com/"><img alt="Best Picture System" height="100" src="https://bestpicturesystem.com/wp-content/uploads/2020/10/BPS-logo2.png" title="" width="300"/></a></span><nav class="main_menu" data-selectname="Selecciona una página" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" role="navigation"><div class="avia-menu av-main-nav-wrap av_menu_icon_beside"><ul class="menu av-main-nav" id="avia-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-1" id="menu-item-71"><a href="#puromula" itemprop="url" rel="noopener noreferrer" target="_blank"><span class="avia-bullet"></span><span class="avia-menu-text">Ver películas <strong>BPS</strong></span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-2" id="menu-item-69"><a href="#escool" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text"><strong>BPS</strong> es cool</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-mega-parent menu-item-top-level menu-item-top-level-3" id="menu-item-70"><a href="#conocealbps" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Conoce al <strong>BPS</strong></span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li class="av-burger-menu-main menu-item-avia-special av-small-burger-icon">
<a aria-hidden="false" aria-label="Menú" href="#">
<span class="av-hamburger av-hamburger--spin av-js-hamburger">
<span class="av-hamburger-box">
<span class="av-hamburger-inner"></span>
<strong>Menú</strong>
</span>
</span>
<span class="avia_hidden_link_text">Menú</span>
</a>
</li></ul></div><ul class="noLightbox social_bookmarks icon_count_5"><li class="social_bookmarks_youtube av-social-link-youtube social_icon_1"><a aria-hidden="false" aria-label="Link to Youtube" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.youtube.com/channel/UCG_tx23OukRRXGIDhJzMkmQ" rel="noopener noreferrer" target="_blank" title="Youtube"><span class="avia_hidden_link_text">Youtube</span></a></li><li class="social_bookmarks_vimeo av-social-link-vimeo social_icon_2"><a aria-hidden="false" aria-label="Link to Vimeo" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://vimeo.com/bestpicturesystem" rel="noopener noreferrer" target="_blank" title="Vimeo"><span class="avia_hidden_link_text">Vimeo</span></a></li><li class="social_bookmarks_linkedin av-social-link-linkedin social_icon_3"><a aria-hidden="false" aria-label="Link to LinkedIn" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.imdb.com/name/nm6151157/" rel="noopener noreferrer" target="_blank" title="LinkedIn"><span class="avia_hidden_link_text">LinkedIn</span></a></li><li class="social_bookmarks_instagram av-social-link-instagram social_icon_4"><a aria-hidden="false" aria-label="Link to Instagram" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.instagram.com/bestpicturesystem" rel="noopener noreferrer" target="_blank" title="Instagram"><span class="avia_hidden_link_text">Instagram</span></a></li><li class="social_bookmarks_facebook av-social-link-facebook social_icon_5"><a aria-hidden="false" aria-label="Link to Facebook" data-av_icon="" data-av_iconfont="entypo-fontello" href="https://www.facebook.com/bestpicturesystem" rel="noopener noreferrer" target="_blank" title="Facebook"><span class="avia_hidden_link_text">Facebook</span></a></li></ul></nav></div> </div>
<!-- end container_wrap-->
</div>
<div class="header_bg"></div>
<!-- end header -->
</header>
<div class="all_colors" data-scroll-offset="110" id="main">
<div class="stretch_full container_wrap alternate_color dark_bg_color title_container"><div class="container"><h1 class="main-title entry-title ">Error 404 - Página no encontrada</h1></div></div>
<div class="container_wrap container_wrap_first main_color sidebar_right">
<div class="container">
<main class="template-page content av-content-small alpha units" itemprop="mainContentOfPage" role="main">
<div class="entry entry-content-wrapper clearfix" id="search-fail">
<p class="entry-content"><strong>✖ No hay resultados</strong><br/>

Lo sentimos, esta entrada no está disponible, ¿quieres probar con nuestro buscador?</p>
<form action="https://bestpicturesystem.com/" class="" id="searchform" method="get">
<div>
<input class="button avia-font-entypo-fontello" id="searchsubmit" type="submit" value=""/>
<input id="s" name="s" placeholder="Buscar" type="text" value=""/>
</div>
</form>
<div class="hr_invisible"></div>
<section class="404_recommendation">
<p><strong>Para obtener los mejores resultados de búsqueda, sigue estos consejos:</strong></p>
<ul class="borderlist-not">
<li>Comprueba la ortografía.</li>
<li>Prueba con términos similares o sinónimos.</li>
<li>Prueba con más de una sola palabra.</li>
</ul>
<div class="hr_invisible"></div>
</section>
</div>
<!--end content-->
</main>
</div><!--end container-->
</div><!-- close default .container_wrap element -->
<div class="container_wrap footer-page-content footer_color" id="footer-page"><div class="avia-section main_color avia-section-default avia-no-border-styling avia-bg-style-scroll avia-builder-el-0 avia-builder-el-no-sibling av-minimum-height av-minimum-height-custom container_wrap fullsize" id="av_section_1" style="background-color: #212121;  "><div class="container" style="height:100px"><main class="template-page content av-content-full alpha units" itemprop="mainContentOfPage" role="main"><div class="post-entry post-entry-type-page post-entry-112"><div class="entry-content-wrapper clearfix">
<div class="flex_column av_one_half flex_column_div first avia-builder-el-1 el_before_av_one_half avia-builder-el-first " style="padding:0px 0px 0px 0px ; border-radius:0px 90px 0px 0px ; "><section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork"><div class="avia_textblock av-medium-font-size-overwrite av-medium-font-size-11" itemprop="text"><p><a href="“mailto:info@bestpicturesystem.com”">info<strong>@</strong>bestpicturesystem.com</a>    <a href="https://bestpicturesystem.com/#contacto">Contáctanos</a></p>
</div></section></div><div class="flex_column av_one_half flex_column_div av-zero-column-padding avia-builder-el-3 el_after_av_one_half avia-builder-el-last " style="border-radius:0px; "><section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork"><div class="avia_textblock av_inherit_color av-medium-font-size-overwrite av-medium-font-size-11" itemprop="text" style="color:#ffffff; "><p style="text-align: right;">Copyright <strong>©</strong> 2020 Best Picture System. Todos los derechos reservados.</p>
</div></section></div>
</div></div></main><!-- close content main element --> <!-- section close by builder template --> </div><!--end builder template--></div><!-- close default .container_wrap element --></div>
<!-- end main -->
</div>
<!-- end wrap_all --></div>
<a aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello" href="#top" id="scroll-top-link" title="Desplazarse hacia arriba"><span class="avia_hidden_link_text">Desplazarse hacia arriba</span></a>
<div id="fb-root"></div>
<script type="text/javascript">
 /* <![CDATA[ */  
var avia_framework_globals = avia_framework_globals || {};
    avia_framework_globals.frameworkUrl = 'https://bestpicturesystem.com/wp-content/themes/enfold/framework/';
    avia_framework_globals.installedAt = 'https://bestpicturesystem.com/wp-content/themes/enfold/';
    avia_framework_globals.ajaxurl = 'https://bestpicturesystem.com/wp-admin/admin-ajax.php';
/* ]]> */ 
</script>
<style type="text/css">
@media only screen and (min-width: 768px) and (max-width: 989px) { 
.responsive #top #wrap_all .av-medium-font-size-11{font-size:11px !important;} 
} 
</style>
<script id="wp-embed-js" src="https://bestpicturesystem.com/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
<script id="avia-footer-scripts-js" src="https://bestpicturesystem.com/wp-content/uploads/dynamic_avia/avia-footer-scripts-d9aa7b045e164bea1aa07fed61614384---5f99cc4d3dc5a.js" type="text/javascript"></script>
<script>
(function($){
	$(window).load(function() {

		$( ".inline_popup" ).click(function(event) {
			var visible = $(this).attr('data-visible');
			var dest = $(this).attr('data-mfp-src');
			
			if(visible==1){
				$(dest).fadeIn();
				$(this).attr('data-visible','0');
				$(this).text('VER MENOS');
			} else {
				$(dest).css('display','none');
				$(this).attr('data-visible','1')
				$(this).text('VER MÁS');
			}

			event.preventDefault();
		});


  });
})(jQuery);
</script>
</body>
</html>
