<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Community Extension Programs - Before/After School Programs</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css"/>
<link href="css/styles.css" media="all" rel="stylesheet" type="text/css"/>
<link href="css/custom.css" media="all" rel="stylesheet" type="text/css"/>
<link href="css/post.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Lora:400,400italic,700|Montserrat:400,700" rel="stylesheet" type="text/css"/>
<link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" rel="stylesheet"/>
<link href="/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180"/>
<link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png"/>
<link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png"/>
<link href="/site.webmanifest" rel="manifest"/>
<meta content="CEP provides before and after school programs, Tiny Tinker Preschools, and the STEMazzzing Summer Camps." name="description"/>
</head>
<body data-reveal-selectors="section:not(.masonry):not(:first-of-type):not(.parallax)">
<div class="nav-container bg--primary">
<nav>
<div class="container nav-stack">
<div class="row">
<div class="nav-stack__upper">
<div class="col-xs-6">
<a href="/">
<img alt="logo" class="logo hidden-xs" src="img/logo-long.png"/>
<img alt="logo" class="logo visible-xs" src="img/logo-light.png"/>
</a>
</div>
<div class="col-xs-6 text-right">
<a href="/cdn-cgi/l/email-protection#a5cccbc3cae5c6c0d588c4df8bcad7c2">Email: <span class="__cf_email__" data-cfemail="ed84838b82ad8e889dc08c97c3829f8a">[email protected]</span></a><br/>
<a href="tel:15208882727">Phone: 520.888.2727</a>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<div class="nav-bar">
<div class="nav-module logo-module left">
<a href="/">
<img alt="logo" class="logo logo-dark" src="img/logo-light.png"/>
<img alt="logo" class="logo logo-light" src="img/logo-light.png"/>
</a>
</div>
<div class="nav-module menu-module left">
<ul class="menu">
<li><a href="/">Home</a></li>
<li class="vpf"><a href="about.html">About</a></li>
<li class="vpf"><a href="careers.html">Careers</a></li>
</ul>
</div>
<div class="nav-module nav-social right">
<a class="nav-function" href="https://www.facebook.com/ceptucson/" target="_blank">
<i class="fab fa-facebook"></i>
</a>
<a class="nav-function" href="https://twitter.com/ceptucsonaz" target="_blank">
<i class="fab fa-twitter"></i>
</a>
<a class="nav-function" href="https://www.linkedin.com/company/community-extension-programs/about/" target="_blank">
<i class="fab fa-linkedin"></i>
</a>
</div>
</div>
<div class="nav-mobile-toggle visible-sm visible-xs">
<i class="fa fa-bars"></i> </div>
</div>
</div>
</div>
</nav>
</div>
<div class="main-container">
<section class="blog-post blog-post-2">
<div class="blog-post__title imagebg parallax" data-overlay="5">
<div class="background-image-holder">
<img alt="Pic" src="img/hero20.jpg"/>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12 text-center">
<h5 class="h5">This website is currently being redesigned.
<br/> Community Extension Programs, Inc.
Vision: Empowering people to overcome hardship and create a community where all people reach their full potential.
Mission: Providing relief and resources to those in need in the Greater Tucson area by maximizing our financial strength and community partners.
<br/>Contact: <a class="__cf_email__" data-cfemail="a5cccbc3cae5c6c0d588c4df8bcad7c2" href="/cdn-cgi/l/email-protection">[email protected]</a> or 520-888-2727:
</h5>
</div>
</div>
</div>
</div>
</section>
<section class="card-section">
<div class="container-fluid">
<div class="row">
<div class="col-md-4 col-sm-6">
<div class="card card-1 ">
<div class="card__image">
<img alt="Pic" src="img/home_1.jpg"/>
</div>
<div class="card__body boxed bg--white text-center">
<div class="card__title">
<h4>Tiny Tinkers Preschool</h4>
</div>
<a class="btn btn--primary inner-link" href="#">
<span class="btn__text">** CANCELLED **</span>
</a>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6">
<div class="card card-1">
<div class="card__image">
<img alt="Pic" src="img/home_2.jpg"/>
</div>
<div class="card__body boxed bg--white text-center">
<div class="card__title">
<h4>Purposeful Afterschool Learning</h4>
</div>
<a class="btn btn--primary inner-link" href="#">
<span class="btn__text">** CANCELLED **</span>
</a>
</div>
</div>
</div>
<div class="col-md-4 col-sm-6">
<div class="card card-1">
<div class="card__image">
<img alt="Pic" src="img/home_3.jpg"/>
</div>
<div class="card__body boxed bg--white text-center">
<div class="card__title">
<h4>STEMazzzing Summer Camps</h4>
</div>
<a class="btn btn--primary btn--red inner-link" href="#">
<span class="btn__text">** CANCELLED **</span>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="bg--white bg-text">
<div class="bg--white col-md-2 text-center">
<h4>Creativity</h4>
</div>
<div class="bg--white col-md-2 text-center">
<h4>Self Discovery</h4>
</div>
<div class="bg--white col-md-2 text-center">
<h4>Makerspace</h4>
</div>
<div class="bg--white col-md-2 text-center">
<h4>STEM Learning</h4>
</div>
<div class="bg--white col-md-2 text-center">
<h4>Fun</h4>
</div>
<div class="bg--white col-md-2 text-center">
<h4>Summer Camps</h4>
</div>
</section>
<section class="features features-7 imagebg parallax feat-adj" data-overlay="5">
<div class="background-image-holder">
<img alt="image" src="img/hero10.jpg"/>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12 text-center">
<h2>Your Familyâs EDUCATIONAL needs are our priority!</h2>
<p class="lead">We provide the Highest Quality Learning Experiences and Foster Strong Families.</p>
<div class="ro">
<div class="col-sm-12">
<img alt="" src="img/ra2015.png"/>
<img alt="" src="img/ra2016.png"/>
<img alt="" src="img/ra2017.png"/>
</div>
</div>
</div>
</div>
</div>
</section>
<footer class="footer-3 bg--dark text-center-xs">
<div class="col-sm-12 ft3 text-center">
<ul class="footer__navigation">
<li><a href="/">Home</a></li>
<li><a href="about.html">About</a></li>
<li><a href="careers.html">Careers</a></li>
</ul>
</div>
<div class="col-sm-12 text-center text-center-xs">
<ul class="social-list">
<li><a href="#"><i class="socicon-twitter"></i></a></li>
</ul>
<span class="type--fine-print">Â© Copyright <span class="update-year">2018</span> CEP - All Rights Reserved</span>
</div>
</footer>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/jquery-2.1.4.min.js"></script>
<script src="js/parallax.js"></script>
<script src="js/scripts.js"></script>
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-72657921-1"></script>
<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-72657921-1');
        </script>
</body>
</html>
