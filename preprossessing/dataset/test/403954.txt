<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://deniscaronauto.com/xmlrpc.php" rel="pingback"/>
<!--[if lt IE 9]>
	<script src="https://deniscaronauto.com/wp-content/themes/bb-theme/js/html5shiv.js"></script>
	<script src="https://deniscaronauto.com/wp-content/themes/bb-theme/js/respond.min.js"></script>
<![endif]-->
<title>DENIS CARON AUTO – PERSONNE NE VEND MOINS CHER!</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://deniscaronauto.com/feed/" rel="alternate" title="DENIS CARON AUTO » Flux" type="application/rss+xml"/>
<link href="https://deniscaronauto.com/comments/feed/" rel="alternate" title="DENIS CARON AUTO » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/deniscaronauto.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://deniscaronauto.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/plugins/bb-plugin/css/yui3.css?ver=2.4.1" id="yui3-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/plugins/bb-plugin/css/fl-slideshow.min.css?ver=2.4.1" id="fl-slideshow-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/plugins/bb-plugin/fonts/fontawesome/5.15.1/css/all.min.css?ver=2.4.1" id="font-awesome-5-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/plugins/bb-plugin/fonts/fontawesome/5.15.1/css/v4-shims.min.css?ver=2.4.1" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/uploads/bb-plugin/cache/8-layout.css?ver=1445acc9173e04102574e614921b966f" id="fl-builder-layout-8-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/themes/bb-theme/css/mono-social-icons.css?ver=1.6.3" id="mono-social-icons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/plugins/bb-plugin/css/jquery.magnificpopup.min.css?ver=2.4.1" id="jquery-magnificpopup-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/themes/bb-theme/css/bootstrap.min.css?ver=1.6.3" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://deniscaronauto.com/wp-content/uploads/bb-theme/skin-5f48fd764b781.css?ver=1.6.3" id="fl-automator-skin-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Muli%3A600%2C300%2C400%2C700&amp;ver=5.5.3" id="fl-builder-google-fonts-17839b3002700050d7a93dea86546a4b-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://deniscaronauto.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="imagesloaded-js" src="https://deniscaronauto.com/wp-includes/js/imagesloaded.min.js?ver=5.5.3" type="text/javascript"></script>
<link href="https://deniscaronauto.com/wp-json/" rel="https://api.w.org/"/><link href="https://deniscaronauto.com/wp-json/wp/v2/pages/8" rel="alternate" type="application/json"/><link href="https://deniscaronauto.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://deniscaronauto.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<link href="https://deniscaronauto.com/" rel="canonical"/>
<link href="https://deniscaronauto.com/" rel="shortlink"/>
<link href="https://deniscaronauto.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdeniscaronauto.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://deniscaronauto.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdeniscaronauto.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style></head>
<body class="home page-template-default page page-id-8 fl-builder fl-preset-default fl-full-width fl-shrink fl-scroll-to-top" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div class="fl-page">
<div class="fl-page-bar">
<div class="fl-page-bar-container container">
<div class="fl-page-bar-row row">
<div class="col-md-6 col-sm-6 text-left clearfix"><div class="fl-page-bar-text fl-page-bar-text-1"></div></div> <div class="col-md-6 col-sm-6 text-right clearfix"><div class="fl-page-bar-text fl-page-bar-text-2">CONTACTEZ-NOUS 450-373-7373</div><div class="fl-social-icons">
<a href="https://www.facebook.com/Denis-Caron-auto-1571204309870143/" target="_blank"><span class="sr-only">Facebook</span><i class="fa fa-facebook mono"></i></a></div>
</div> </div>
</div>
</div><!-- .fl-page-bar -->
<header class="fl-page-header fl-page-header-primary fl-page-nav-right fl-page-nav-toggle-button fl-page-nav-toggle-visible-mobile" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="fl-page-header-wrap">
<div class="fl-page-header-container container">
<div class="fl-page-header-row row">
<div class="col-md-4 col-sm-12 fl-page-header-logo-col">
<div class="fl-page-header-logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
<a href="https://deniscaronauto.com/" itemprop="url"><img alt="DENIS CARON AUTO" class="fl-logo-img" data-retina="" itemscope="" itemtype="http://schema.org/ImageObject" src="https://deniscaronauto.com/wp-content/uploads/2017/11/logo_denis-caron-auto.png"/><meta content="DENIS CARON AUTO" itemprop="name"/></a>
</div>
</div>
<div class="fl-page-nav-col col-md-8 col-sm-12">
<div class="fl-page-nav-wrap">
<nav class="fl-page-nav fl-nav navbar navbar-default" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<button class="navbar-toggle" data-target=".fl-page-nav-collapse" data-toggle="collapse" type="button">
<span>Menu</span>
</button>
<div class="fl-page-nav-collapse collapse navbar-collapse">
<ul class="nav navbar-nav navbar-right menu" id="menu-menu-principal"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-58" id="menu-item-58"><a aria-current="page" href="https://deniscaronauto.com/">ACCUEIL</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57" id="menu-item-57"><a href="https://deniscaronauto.com/inventaire/">INVENTAIRE</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56" id="menu-item-56"><a href="https://deniscaronauto.com/contact/">CONTACT</a></li>
</ul> </div>
</nav>
</div>
</div>
</div>
</div>
</div>
</header><!-- .fl-page-header -->
<div class="fl-page-content" itemprop="mainContentOfPage">
<div class="fl-content-full container">
<div class="row">
<div class="fl-content col-md-12">
<article class="fl-post post-8 page type-page status-publish hentry" id="fl-post-8" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
<div class="fl-post-content clearfix" itemprop="text">
<div class="fl-builder-content fl-builder-content-8 fl-builder-content-primary fl-builder-global-templates-locked" data-post-id="8"><div class="fl-row fl-row-full-width fl-row-bg-parallax fl-node-5a14618db5795 fl-row-full-height fl-row-align-center fl-row-bg-overlay" data-node="5a14618db5795" data-parallax-image="https://deniscaronauto.com/wp-content/uploads/2017/11/image-accueil-auto_denis-caron-auto.jpg" data-parallax-speed="2" id="home">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-full-width fl-node-content">
<div class="fl-col-group fl-node-5a14618db6254" data-node="5a14618db6254">
<div class="fl-col fl-node-5a14618db628b" data-node="5a14618db628b">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14618db62c2 fl-animation fl-fade-up" data-animation-delay="0.0" data-node="5a14618db62c2">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<a href="#about" itemprop="url" target="_self">
<img alt="signature_denis-caron-auto" class="fl-photo-img wp-image-63 size-full" height="212" itemprop="image" loading="lazy" sizes="(max-width: 1060px) 100vw, 1060px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/signature_denis-caron-auto.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/signature_denis-caron-auto.png 1060w, https://deniscaronauto.com/wp-content/uploads/2017/11/signature_denis-caron-auto-300x60.png 300w, https://deniscaronauto.com/wp-content/uploads/2017/11/signature_denis-caron-auto-768x154.png 768w, https://deniscaronauto.com/wp-content/uploads/2017/11/signature_denis-caron-auto-1024x205.png 1024w" title="signature_denis-caron-auto" width="1060"/>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-color fl-node-5a14618db57d0" data-node="5a14618db57d0" id="about">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-fixed-width fl-node-content">
<div class="fl-col-group fl-node-5a14618db5809 fl-col-group-equal-height fl-col-group-align-center" data-node="5a14618db5809">
<div class="fl-col fl-node-5a14618db5841 fl-col-small" data-node="5a14618db5841">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14618db5a18 fl-animation fl-fade-left" data-animation-delay="0.0" data-node="5a14618db5a18">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="image-gris_auto-logo" class="fl-photo-img wp-image-70 size-full" height="536" itemprop="image" loading="lazy" sizes="(max-width: 752px) 100vw, 752px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/image-gris_auto-logo.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/image-gris_auto-logo.png 752w, https://deniscaronauto.com/wp-content/uploads/2017/11/image-gris_auto-logo-300x214.png 300w" title="image-gris_auto-logo" width="752"/>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5a14618db5878" data-node="5a14618db5878">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-rich-text fl-node-5a14618db5896 fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a14618db5896">
<div class="fl-module-content fl-node-content">
<div class="fl-rich-text">
<h1 style="letter-spacing: 4px;">À propos de Denis Caron Auto</h1>
<p><span style="font-size: 20px; line-height: 32px;">Premièrement, Denis Caron Automobiles est fier d'offrir à sa clientèle la chance d'acquérir un véhicule d'occasion de qualité à un prix imbattable. Vous n'avez qu'à parcourir notre inventaire de véhicules d'occasion à vendre maintenant et vous verrez que nous offrons réellement les meilleurs prix pour une automobile usagée. De plus, si vous ne trouvez pas le véhicule que vous recherchez, sachez qu'il est possible de placer une commande précise.</span></p>
</div>
</div>
</div>
<div class="fl-module fl-module-button fl-node-5a14618db5e6b fl-animation fl-fade-in" data-animation-delay="0.0" data-node="5a14618db5e6b">
<div class="fl-module-content fl-node-content">
<div class="fl-button-wrap fl-button-width-auto fl-button-left">
<a class="fl-button" href="https://deniscaronauto.com/contact/" rel="noopener" role="button" target="_blank">
<span class="fl-button-text">VENEZ NOUS VOIR</span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-color fl-node-5a14618db6026" data-node="5a14618db6026" id="finance">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-fixed-width fl-node-content">
<div class="fl-col-group fl-node-5a14618db605d fl-col-group-equal-height fl-col-group-align-center" data-node="5a14618db605d">
<div class="fl-col fl-node-5a14618db6064" data-node="5a14618db6064">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-rich-text fl-node-5a14618db644d fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a14618db644d">
<div class="fl-module-content fl-node-content">
<div class="fl-rich-text">
<h1 style="letter-spacing: 4px;">Options de financement</h1>
<p><span style="font-size: 20px; line-height: 32px;">Denis Caron Automobiles, c'est également votre centre de financement. Première, deuxième et troisième chance au crédit; nous vous simplifierons la vie en trouvant des solutions adaptées à vos besoins. Si vous avez des questions sur nos produits ou services, n'hésitez pas à nous joindre.</span></p>
</div>
</div>
</div>
<div class="fl-module fl-module-button fl-node-5a14618db6484 fl-animation fl-fade-up" data-animation-delay="0.0" data-node="5a14618db6484">
<div class="fl-module-content fl-node-content">
<div class="fl-button-wrap fl-button-width-auto fl-button-left">
<a class="fl-button" href="https://deniscaronauto.com/contact/" rel="noopener" role="button" target="_blank">
<span class="fl-button-text">NOUS JOINDRE</span>
</a>
</div>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5a14618db609b fl-col-small" data-node="5a14618db609b">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14618db60d2 fl-animation fl-fade-in" data-animation-delay="0.0" data-node="5a14618db60d2">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="image-financement_denis-acron-auto" class="fl-photo-img wp-image-76 size-medium" height="300" itemprop="image" loading="lazy" sizes="(max-width: 192px) 100vw, 192px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/image-financement_denis-acron-auto-192x300.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/image-financement_denis-acron-auto-192x300.png 192w, https://deniscaronauto.com/wp-content/uploads/2017/11/image-financement_denis-acron-auto.png 320w" title="image-financement_denis-acron-auto" width="192"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-slideshow fl-node-5a14618db58cd fl-row-bg-overlay" data-node="5a14618db58cd" id="shop">
<div class="fl-row-content-wrap">
<div class="fl-bg-slideshow"></div> <div class="fl-row-content fl-row-full-width fl-node-content">
<div class="fl-col-group fl-node-5a14618db5904 fl-col-group-equal-height fl-col-group-align-top" data-node="5a14618db5904">
<div class="fl-col fl-node-5a14618db593b fl-col-small" data-node="5a14618db593b">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14618db5ea4 fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a14618db5ea4">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="icon-pneu_denis-caron-auto" class="fl-photo-img wp-image-81 size-thumbnail" height="150" itemprop="image" loading="lazy" sizes="(max-width: 150px) 100vw, 150px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png 150w, https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto.png 290w" title="icon-pneu_denis-caron-auto" width="150"/>
</div>
</div>
</div>
</div>
<div class="fl-module fl-module-heading fl-node-5a1489bd8e9e7" data-node="5a1489bd8e9e7">
<div class="fl-module-content fl-node-content">
<h4 class="fl-heading">
<span class="fl-heading-text">Vente</span>
</h4>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5a14618db59e1 fl-col-small" data-node="5a14618db59e1">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14895de32bb fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a14895de32bb">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="icon-pneu_denis-caron-auto" class="fl-photo-img wp-image-81 size-thumbnail" height="150" itemprop="image" loading="lazy" sizes="(max-width: 150px) 100vw, 150px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png 150w, https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto.png 290w" title="icon-pneu_denis-caron-auto" width="150"/>
</div>
</div>
</div>
</div>
<div class="fl-module fl-module-heading fl-node-5a1489f4e934a" data-node="5a1489f4e934a">
<div class="fl-module-content fl-node-content">
<h4 class="fl-heading">
<span class="fl-heading-text">Financement</span>
</h4>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5a14618db5a4f fl-col-small" data-node="5a14618db5a4f">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5a14896441cdf fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a14896441cdf">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="icon-pneu_denis-caron-auto" class="fl-photo-img wp-image-81 size-thumbnail" height="150" itemprop="image" loading="lazy" sizes="(max-width: 150px) 100vw, 150px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto-150x150.png 150w, https://deniscaronauto.com/wp-content/uploads/2017/11/icon-pneu_denis-caron-auto.png 290w" title="icon-pneu_denis-caron-auto" width="150"/>
</div>
</div>
</div>
</div>
<div class="fl-module fl-module-heading fl-node-5a148a0b06791" data-node="5a148a0b06791">
<div class="fl-module-content fl-node-content">
<h4 class="fl-heading">
<span class="fl-heading-text">Financement Maison</span>
</h4>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-color fl-node-5a987c95bac24" data-node="5a987c95bac24" id="about">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-fixed-width fl-node-content">
<div class="fl-col-group fl-node-5a987c95baee2 fl-col-group-equal-height fl-col-group-align-center" data-node="5a987c95baee2">
<div class="fl-col fl-node-5a987c95baf19 fl-col-small" data-node="5a987c95baf19">
<div class="fl-col-content fl-node-content">
</div>
</div>
<div class="fl-col fl-node-5a987c95baf83" data-node="5a987c95baf83">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-rich-text fl-node-5a987c95bafb8 fl-animation fl-fade-down" data-animation-delay="0.0" data-node="5a987c95bafb8">
<div class="fl-module-content fl-node-content">
<div class="fl-rich-text">
<h1 style="letter-spacing: 4px;">Publicités en cours</h1>
<p><span style="font-size: 20px; line-height: 32px;">Cliquez sur notre publicité çi-jointe pour voir tous nos véhicules présentement disponibles dans notre cour. Nous les avons également affichés dans notre section <a href="https://deniscaronauto.com/inventaire/" rel="noopener" target="_blank">INVENTAIRE</a> de notre site.</span></p>
</div>
</div>
</div>
<div class="fl-module fl-module-button fl-node-5a987c95bafed fl-animation fl-fade-in" data-animation-delay="0.0" data-node="5a987c95bafed">
<div class="fl-module-content fl-node-content">
<div class="fl-button-wrap fl-button-width-auto fl-button-left">
<a class="fl-button" href="https://deniscaronauto.com/contact/" rel="noopener" role="button" target="_blank">
<span class="fl-button-text">VENEZ NOUS VOIR</span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-color fl-node-5f48fd2cac467" data-node="5f48fd2cac467" id="call">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-fixed-width fl-node-content">
<div class="fl-col-group fl-node-5f48fd2ca9914" data-node="5f48fd2ca9914">
<div class="fl-col fl-node-5f48fd2ca9916" data-node="5f48fd2ca9916">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5f48fd2ca9917 fl-animation fl-fade-up" data-animation-delay="0.0" data-node="5f48fd2ca9917">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-center" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="signature-appel-concession_denis-caron-auto" class="fl-photo-img wp-image-90 size-full" height="150" itemprop="image" loading="lazy" sizes="(max-width: 750px) 100vw, 750px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/signature-appel-concession_denis-caron-auto.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/signature-appel-concession_denis-caron-auto.png 750w, https://deniscaronauto.com/wp-content/uploads/2017/11/signature-appel-concession_denis-caron-auto-300x60.png 300w" title="signature-appel-concession_denis-caron-auto" width="750"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-row fl-row-full-width fl-row-bg-photo fl-node-5f48fd4486b4b" data-node="5f48fd4486b4b" id="dealers">
<div class="fl-row-content-wrap">
<div class="fl-row-content fl-row-fixed-width fl-node-content">
<div class="fl-col-group fl-node-5f48fd44827e8" data-node="5f48fd44827e8">
<div class="fl-col fl-node-5f48fd44827f0" data-node="5f48fd44827f0">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-photo fl-node-5f48fd44827f1" data-node="5f48fd44827f1">
<div class="fl-module-content fl-node-content">
<div class="fl-photo fl-photo-align-left" itemscope="" itemtype="https://schema.org/ImageObject">
<div class="fl-photo-content fl-photo-img-png">
<img alt="logo_denis-caron-auto" class="fl-photo-img wp-image-20 size-medium" height="52" itemprop="image" loading="lazy" sizes="(max-width: 300px) 100vw, 300px" src="https://deniscaronauto.com/wp-content/uploads/2017/11/logo_denis-caron-auto-300x52.png" srcset="https://deniscaronauto.com/wp-content/uploads/2017/11/logo_denis-caron-auto-300x52.png 300w, https://deniscaronauto.com/wp-content/uploads/2017/11/logo_denis-caron-auto-768x132.png 768w, https://deniscaronauto.com/wp-content/uploads/2017/11/logo_denis-caron-auto.png 1024w" title="logo_denis-caron-auto" width="300"/>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="fl-col-group fl-node-5f48fd44827e6 fl-col-group-equal-height fl-col-group-align-top" data-node="5f48fd44827e6">
<div class="fl-col fl-node-5f48fd44827e9 fl-col-small" data-node="5f48fd44827e9">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-rich-text fl-node-5f48fd44827ed fl-animation fl-fade-up" data-animation-delay="0.1" data-node="5f48fd44827ed">
<div class="fl-module-content fl-node-content">
<div class="fl-rich-text">
<h4><span style="font-size: 14px;"><strong>Salaberry-de-Valleyfield</strong></span></h4>
<p><span style="font-size: 12px;"> 360 rue Dufferin</span><br/>
<span style="font-size: 12px;"> Salaberry-de-Valleyfield, QC J6S 2A4<br/>
Tel: 450-373-7373<br/>
C Denis: 450-374-7090</span></p>
</div>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5f48fd44827ea fl-col-small" data-node="5f48fd44827ea">
<div class="fl-col-content fl-node-content">
<div class="fl-module fl-module-rich-text fl-node-5f48fd44827ee fl-animation fl-fade-up" data-animation-delay="0.1" data-node="5f48fd44827ee">
<div class="fl-module-content fl-node-content">
<div class="fl-rich-text">
<h4><span style="font-size: 14px;"><strong>Rester connectés</strong></span></h4>
</div>
</div>
</div>
<div class="fl-module fl-module-icon-group fl-node-5f48fd44827ef" data-node="5f48fd44827ef">
<div class="fl-module-content fl-node-content">
<div class="fl-icon-group">
<span class="fl-icon">
<a href="mailto:denis@deniscaronauto.com" rel="noopener" target="_blank">
<i aria-hidden="true" class="fa fa-envelope-o"></i>
</a>
</span>
<span class="fl-icon">
<a href="https://www.facebook.com/Denis-Caron-auto-1571204309870143/" rel="noopener" target="_blank">
<i aria-hidden="true" class="fa fa-facebook"></i>
</a>
</span>
</div>
</div>
</div>
</div>
</div>
<div class="fl-col fl-node-5f48fd44827eb fl-col-small" data-node="5f48fd44827eb">
<div class="fl-col-content fl-node-content">
</div>
</div>
<div class="fl-col fl-node-5f48fd44827ec fl-col-small" data-node="5f48fd44827ec">
<div class="fl-col-content fl-node-content">
</div>
</div>
</div>
</div>
</div>
</div>
</div> </div><!-- .fl-post-content -->
</article>
<!-- .fl-post -->
</div>
</div>
</div>
</div><!-- .fl-page-content -->
<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
<div class="fl-page-footer">
<div class="fl-page-footer-container container">
<div class="fl-page-footer-row row">
<div class="col-md-6 col-sm-6 text-left clearfix"><div class="fl-page-footer-text fl-page-footer-text-1"><a href="http://www.officecanadien.com/web/forfaits.html" target="_blank"><img src="http://www.officecanadien.ca/images/logo-occ-blanc.png" width="137"/></a>
Propulsé par l'<a href="http://www.officecanadien.com/web/forfaits.html" target="_blank" title="Office Canadien du Commerce">Office du Commerce Canadien</a></div></div> <div class="col-md-6 col-sm-6 text-right clearfix"><div class="fl-page-footer-text fl-page-footer-text-2">(450) 373-7373 • © 2017-2020. DENIS CARON AUTO. Tous droits réservés.</div></div> </div>
</div>
</div><!-- .fl-page-footer -->
</footer>
</div><!-- .fl-page -->
<a href="#" id="fl-to-top"><i class="fa fa-chevron-up"></i></a><link href="//fonts.googleapis.com/css?family=Muli%3A600&amp;ver=5.5.3" id="fl-builder-google-fonts-80f6006edec8cc6ea52cc672f0f51e57-css" media="all" rel="stylesheet" type="text/css"/>
<script id="yui3-js" src="https://deniscaronauto.com/wp-content/plugins/bb-plugin/js/yui3.min.js?ver=2.4.1" type="text/javascript"></script>
<script id="fl-slideshow-js" src="https://deniscaronauto.com/wp-content/plugins/bb-plugin/js/fl-slideshow.min.js?ver=2.4.1" type="text/javascript"></script>
<script id="jquery-waypoints-js" src="https://deniscaronauto.com/wp-content/plugins/bb-plugin/js/jquery.waypoints.min.js?ver=2.4.1" type="text/javascript"></script>
<script id="fl-builder-layout-8-js" src="https://deniscaronauto.com/wp-content/uploads/bb-plugin/cache/8-layout.js?ver=1445acc9173e04102574e614921b966f" type="text/javascript"></script>
<script id="jquery-throttle-js" src="https://deniscaronauto.com/wp-content/plugins/bb-plugin/js/jquery.ba-throttle-debounce.min.js?ver=2.4.1" type="text/javascript"></script>
<script id="jquery-magnificpopup-js" src="https://deniscaronauto.com/wp-content/plugins/bb-plugin/js/jquery.magnificpopup.min.js?ver=2.4.1" type="text/javascript"></script>
<script id="bootstrap-js" src="https://deniscaronauto.com/wp-content/themes/bb-theme/js/bootstrap.min.js?ver=1.6.3" type="text/javascript"></script>
<script id="fl-automator-js" src="https://deniscaronauto.com/wp-content/themes/bb-theme/js/theme.min.js?ver=1.6.3" type="text/javascript"></script>
<script id="wp-embed-js" src="https://deniscaronauto.com/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
