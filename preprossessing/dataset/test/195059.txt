<!DOCTYPE html>
<html lang="en-us">
<head>
<title>1924 Detroit Tigers Roster by Baseball Almanac</title>
<meta charset="utf-8"/>
<meta content="1924 Detroit Tigers Roster 1924 detroit tigers roster " name="keywords"/>
<meta content="A 1924 Detroit Tigers roster with uniform numbers, player stats and Opening Day data." name="description"/>
<meta content="index, follow" name="robots"/>
<meta content="Baseball Almanac, Inc." name="Author"/>
<!-- ' / -->
<!-- Mobile viewport -->
<meta content="width=device-width; initial-scale=1.0" name="viewport"/>
<link href="/css/styles1.1.css" rel="stylesheet"/>
<link href="/css/menu.css" rel="stylesheet"/>
<style><!--  --></style>
<!-- BEGIN GOOGLE ANALYTICS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-1805063-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-1805063-1');
</script>
<!-- END GOOGLE ANALYTICS -->
</head>
<body>
<!-- BEGIN FREESTAR -->
<div class="google-adsense" style="text-align: center; margin: 10px 0">
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || new Object();
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || new Object();
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;
  freestar.config.enabled_slots = [];
  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/baseball-almanac-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
  freestar.initCallback = function () { (freestar.config.enabled_slots.length === 0) ? freestar.initCallbackCalled = false : freestar.newAdSlots(freestar.config.enabled_slots) }
</script>
<!-- Tag ID: Baseballalmanac_leaderboard_ATF -->
<div align="center" id="Baseballalmanac_leaderboard_ATF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_ATF", slotId: "Baseballalmanac_leaderboard_ATF" });
</script>
</div>
</div>
<!-- END FREESTAR -->
<div id="wrapper">
<div class="header-container">
<!-- START _site-header-v2.html -->
<div class="header">
<div class="container">
<a class="flex-none" href="/">
<span class="hidden">Baseball Almanac</span>
<img alt="Baseball Almanac" class="logo" src="/images/baseball-almanac-logo.png"/>
</a>
<div class="menu-items hidden" data-menu="">
<div>
<a data-flip="" data-toggle="menu-1" href="#">
                    History
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-1="">
<li><a href="/asgmenu.shtml">All-Star Game</a></li>
<li><a href="/League_Championship_Series.shtml">A.L.C.S. &amp; N.L.C.S.</a></li>
<li><a href="/me_award.shtml">Awards</a></li>
<li><a href="/stadium.shtml">Ballparks</a></li>
<li><a href="/college/colleges.shtml">College Baseball</a></li>
<li><a href="/division_series/division_series.shtml">Division Series</a></li>
<li><a href="/draft/baseball_draft.shtml">Draft</a></li>
<li><a href="/mgrmenu.shtml">Managers</a></li>
<li><a href="/opening_day/opening_day.shtml">Opening Day</a></li>
<li><a href="/teammenu.shtml">Team by Team</a></li>
<li><a href="/umpiresmenu.shtml">Umpires</a></li>
<li><a href="/wild_card/MLB_Wild_Card_Game.shtml">Wild Card Game</a></li>
<li><a href="/ws/wsmenu.shtml">World Series</a></li>
<li><a href="/yearmenu.shtml">Year by Year</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-2" href="#">
                    Players
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-2="">
<li><a href="/fammenu.shtml">Baseball Families</a></li>
<li><a href="/players/baseball_biographies.shtml">Biographies</a></li>
<li><a href="/players/birthplace.php">Birthplace Analysis</a></li>
<li><a href="/featmenu.shtml">Fabulous Feats</a></li>
<li><a href="/frstmenu.shtml">Famous Firsts</a></li>
<li><a href="/graves/baseball_graves.shtml">Grave Sites</a></li>
<li><a href="/hofmenu.shtml">Hall of Fame</a></li>
<li><a href="/players/baseball_interviews.shtml">Interviews</a></li>
<li><a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a></li>
<li><a href="/players/deathplace.php">Place of Death Analysis</a></li>
<li><a href="/players/ballplayer.shtml">The Ballplayers</a></li>
<li><a href="/quomenu.shtml">Quotes</a></li>
<li><a href="/players/baseball_births.php">Year of Birth Analysis</a></li>
<li><a href="/players/baseball_deaths.php">Year of Death Analysis</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-3" href="#">
                    Leaders
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-3="">
<li><a href="/baseball_attendance.shtml">Attendance Data</a></li>
<li><a href="/himenu.shtml">Hitting Charts</a></li>
<li><a href="/pimenu.shtml">Pitching Charts</a></li>
<li><a href="/rb_menu.shtml">Record Books</a></li>
<li><a href="/teamstats/statmaster.php">Statmaster</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-4" href="#">
                    Left Field
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-4="">
<li><a href="/players/Oldest_Living_Baseball_Players.php">500 Oldest Players</a></li>
<li><a href="/automenu.shtml">Autographs</a></li>
<li><a href="/baseball_cards/baseball_cards.php">Baseball Cards</a></li>
<li><a href="/charts/baseball_charts.shtml">Baseball Charts</a></li>
<li><a href="/limenu.shtml">Baseball Lists</a></li>
<li><a href="/bookmenu.shtml">Book Shelf</a></li>
<li><a href="/players/Cups_of_Coffee.php">Cups of Coffee</a></li>
<li><a href="/gam_menu.shtml">Fun &amp; Games</a></li>
<li><a href="/humomenu.shtml">Humor &amp; Jokes</a></li>
<li><a href="/mve_time.shtml">Movie Time</a></li>
<li><a href="/baseball_news.shtml">News Feeds</a></li>
<li><a href="/poems.shtml">Poetry &amp; Song</a></li>
<li><a href="/articles/articles.shtml">Research Articles</a></li>
<li><a href="/baseball_uniform_numbers.shtml">Uniform Numbers</a></li>
<li><a href="/prz_menu.shtml">U.S. Presidents</a></li>
</ul>
</div>
<div>
<a data-flip="" data-toggle="menu-5" href="#">
                    Help
                    <span><i class="fa fa-angle-down"></i></span>
</a>
<ul class="hidden" data-menu-5="">
<li><a href="/about.shtml">Advertising</a></li>
<li><a href="/blog/">Blog</a></li>
<li><a href="/feedmenu.shtml">Feedback</a></li>
<li><a href="/mlmstart.shtml">Newsletter</a></li>
<li><a href="/rulemenu.shtml">Rules</a></li>
<li><a href="/scoring.shtml">Scoring</a></li>
<li><a href="/Search.shtml">Search &amp; Find</a></li>
<li><a href="/bstatmen.shtml">Stats 101</a></li>
</ul>
</div>
<div class="extra">
<a class="bg-blue-500" href="https://twitter.com/BaseballAlmanac" target="_blank">
<i class="fab fa-twitter"></i>
                    Follow @BaseballAlmanac
                </a>
<a class="bg-blue-700" href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank">
<i class="fab fa-facebook"></i>
                    Find us on Facebook
                </a>
</div>
</div>
<div class="overlay hidden" data-menu="" data-proxy="menu"></div>
<form class="search-form">
<div class="social">
<div data-search="">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
<span></span>
</div>
<input class="search hidden" data-search="" data-toggle-focus="" id="q" name="q" placeholder="Custom Search" type="text"/>
<a data-toggle="search" data-toggle-bg=""><i class="fas fa-search"></i></a>
<a class="" data-toggle="menu" data-toggle-bg="" data-toggle-scroll=""><i class="fas fa-bars"></i></a>
</div>
</form>
<script>
            // JavaScript code that should follow the search box code (must part)
  			// add a listener for form submission, i.e. when user hits Enter or clicks to any submit button form has
  			document.querySelector('.search-form').addEventListener('submit', function(e) {
    			// do not actually submit the form, we'll do something else :)
    			e.preventDefault();
    			// read the search query for input tag, i.e. user searches for "django" let's say
    			var q = document.querySelector('input[name="q"]').value;
    			// just proceed if user has typed something
    			if (q.length > 0) {
      				// go to search results page which is search.html here but can be anything you like with "gsc.q" hash parameter equal to search query
      				window.open('/custom_search.shtml?q=' + q, '_self');
    			}
  			});

			// check if there is any text in the search string
			if (window.location.search.length > 0) {
  				// retrieve the "q" keyed search string value
  				var q = window.location.search.substring(1).split('&').filter(function(x) {
    				return x.substring(0, 2) === 'q=';
  				})[0].substring(2);
  				// put the value to the search box if it is not empty
  				if (q.length > 0) {
    				document.querySelector('input[name="q"]').value = q;
  				}
			}
  		</script>
</div>
</div>
<script src="/js/navigation.min.js" type="text/javascript"></script>
<!-- END _site-header-v2.html -->
</div>
<div class="container">
<div class="intro">
<h1>1924 Detroit Tigers Roster</h1>
<p>The 1924 Detroit Tigers team roster seen on this page includes every player who appeared in a game during the 1924 season. It is a comprehensive team roster and player names are sorted by the fielding position where the most number of games were played during the regular season. Every player's name links to their career statistics.</p><p>Below the main roster you will find in the Fast Facts section: a 1924 Detroit Tigers Opening Day starters list, a 1924 Detroit Tigers salary list, a 1924 Detroit Tigers uniform number breakdown and a 1924 Detroit Tigers primary starters list. These team rosters are presented only when and where the data is available.</p>
<!-- Are we using flycast?
 -->
</div>
<!-- 	End Intro Box -->
<!-- 	Begin Quote Box -->
<div class="topquote">
<img alt="Baseball Almanac Top Quote" src="/images/typewriter-with-paper.png"/>
<p>"How to hit home runs: I swing as hard as I can, and I try to swing right through the ball...The harder you grip the bat, the more you can swing it through the ball, and the farther the ball will go. I swing big, with everything I've got. I hit big or I miss big. I like to live as big as I can." - Babe Ruth</p>
</div>
<!-- 	End Quote Box -->
<!-- Begin Sponsor Box -->
<div class="sponsor-box">
<div class="s2nPlayer k-yUwu2RXr" data-type="float"></div><script data-type="s2nScript" src="//embed.sendtonews.com/player3/embedcode.js?fk=yUwu2RXr&amp;cid=8557&amp;offsetx=0&amp;offsety=0&amp;floatwidth=400&amp;floatposition=bottom-right" type="text/javascript"></script>
</div>
<!-- End Sponsor Box -->
<!-- Begin Page Body -->
<!-- Personal Statistics -->
<div class="ba-table">
<table class="boxed">
<tr>
<td class="header" colspan="7">
<h2>1924 Detroit Tigers</h2>
<p><img src="/teams/logos/tigers1.jpg" style="max-width: 79px; margin: 0 auto"/></p><p class="grey">1924 Detroit Tigers Official Logo</p>
<p>Complete Roster</p>
</td>
</tr>
<tr>
<td class="datacolBox" colspan="7"><div class="navtabactive">Roster</div><div class="navtabinactive"><a href="schedule.php?y=1924&amp;t=DET">Schedule</a></div><div class="navtabinactive"><a href="hitting.php?y=1924&amp;t=DET">Hitting</a></div><div class="navtabinactive"><a href="pitching.php?y=1924&amp;t=DET">Pitching</a></div><div class="navtabinactive"><a href="fielding.php?y=1924&amp;t=DET">Fielding</a></div><div class="navtabinactive"><a href="statmaster.php?y=1924&amp;t=DET&amp;l=NL&amp;a=set">Statmaster</a></div></td>
</tr>
<tr>
<td class="banner">#</td>
<td class="banner">Pitchers</td>
<td class="banner">Height</td>
<td class="banner">Weight</td>
<td class="banner">Throws</td>
<td class="banner">Bats</td>
<td class="banner">Date Of Birth</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=clarkru01" title="Show stats for Rufe Clarke">Rufe Clarke</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">203</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1900-04-13</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=colebe01" title="Show stats for Bert Cole">Bert Cole</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">180</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1896-07-01</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=colliri01" title="Show stats for Rip Collins">Rip Collins</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">205</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1896-02-26</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=daussho01" title="Show stats for Hooks Dauss">Hooks Dauss</a></td>
<td class="datacolBox">5-10½</td>
<td class="datacolBox">168</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1889-09-22</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=holloke01" title="Show stats for Ken Holloway">Ken Holloway</a></td>
<td class="datacolBox">6-00</td>
<td class="datacolBox">185</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1897-08-08</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=johnssy01" title="Show stats for Syl Johnson">Syl Johnson</a></td>
<td class="datacolBox">5-11</td>
<td class="datacolBox">180</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1900-12-31</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=joneske01" title="Show stats for Ken Jones">Ken Jones</a></td>
<td class="datacolBox">6-03</td>
<td class="datacolBox">193</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1903-04-13</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=leonadu01" title="Show stats for Dutch Leonard">Dutch Leonard</a></td>
<td class="datacolBox">5-10½</td>
<td class="datacolBox">185</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1892-04-16</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=ludolwi01" title="Show stats for Willie Ludolph">Willie Ludolph</a></td>
<td class="datacolBox">6-01½</td>
<td class="datacolBox">170</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1900-01-21</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=pillehe01" title="Show stats for Herman Pillette">Herman Pillette</a></td>
<td class="datacolBox">6-02</td>
<td class="datacolBox">190</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1895-12-26</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=stoneli01" title="Show stats for Lil Stoner">Lil Stoner</a></td>
<td class="datacolBox">5-09½</td>
<td class="datacolBox">180</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1899-02-28</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=wellsed01" title="Show stats for Ed Wells">Ed Wells</a></td>
<td class="datacolBox">6-01½</td>
<td class="datacolBox">183</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1900-06-07</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=whiteea01" title="Show stats for Earl Whitehill">Earl Whitehill</a></td>
<td class="datacolBox">5-09½</td>
<td class="datacolBox">174</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1899-02-07</td>
</tr>
<tr>
<td class="banner">#</td>
<td class="banner">Catchers</td>
<td class="banner">Height</td>
<td class="banner">Weight</td>
<td class="banner">Throws</td>
<td class="banner">Bats</td>
<td class="banner">Date Of Birth</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=bassljo01" title="Show stats for Johnny Bassler">Johnny Bassler</a></td>
<td class="datacolBox">5-09</td>
<td class="datacolBox">170</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1895-06-03</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=maniocl01" title="Show stats for Clyde Manion">Clyde Manion</a></td>
<td class="datacolBox">5-11</td>
<td class="datacolBox">175</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1896-10-30</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=woodala01" title="Show stats for Larry Woodall">Larry Woodall</a></td>
<td class="datacolBox">5-09</td>
<td class="datacolBox">165</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1894-07-26</td>
</tr>
<tr>
<td class="banner">#</td>
<td class="banner">Infielders</td>
<td class="banner">Height</td>
<td class="banner">Weight</td>
<td class="banner">Throws</td>
<td class="banner">Bats</td>
<td class="banner">Date Of Birth</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=bluelu01" title="Show stats for Lu Blue">Lu Blue</a></td>
<td class="datacolBox">5-10</td>
<td class="datacolBox">165</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Both</td>
<td class="datacolBox">1897-03-05</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=burkele01" title="Show stats for Les Burke">Les Burke</a></td>
<td class="datacolBox">5-09</td>
<td class="datacolBox">168</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1902-12-18</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=gehrich01" title="Show stats for Charlie Gehringer">Charlie Gehringer</a></td>
<td class="datacolBox">5-11</td>
<td class="datacolBox">180</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1903-05-11</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=haneyfr01" title="Show stats for Fred Haney">Fred Haney</a></td>
<td class="datacolBox">5-06</td>
<td class="datacolBox">170</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1896-04-25</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=jonesbo01" title="Show stats for Bob Jones">Bob Jones</a></td>
<td class="datacolBox">6-00</td>
<td class="datacolBox">170</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1889-12-02</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=kerrjo01" title="Show stats for John Kerr">John Kerr</a></td>
<td class="datacolBox">5-08</td>
<td class="datacolBox">158</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1898-11-26</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=oroufr01" title="Show stats for Frank O'Rourke">Frank O'Rourke</a></td>
<td class="datacolBox">5-10½</td>
<td class="datacolBox">165</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1891-11-28</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=prattde01" title="Show stats for Del Pratt">Del Pratt</a></td>
<td class="datacolBox">5-11</td>
<td class="datacolBox">175</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1888-01-10</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=rigneto01" title="Show stats for Topper Rigney">Topper Rigney</a></td>
<td class="datacolBox">5-09</td>
<td class="datacolBox">150</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1897-01-07</td>
</tr>
<tr>
<td class="banner">#</td>
<td class="banner">Outfielders</td>
<td class="banner">Height</td>
<td class="banner">Weight</td>
<td class="banner">Throws</td>
<td class="banner">Bats</td>
<td class="banner">Date Of Birth</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=cobbty01" title="Show stats for Ty Cobb">Ty Cobb</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">175</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1886-12-18</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=fothebo01" title="Show stats for Bob Fothergill">Bob Fothergill</a></td>
<td class="datacolBox">5-10½</td>
<td class="datacolBox">230</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1897-08-16</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=heilmha01" title="Show stats for Harry Heilmann">Harry Heilmann</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">195</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">1894-08-03</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=manushe01" title="Show stats for Heinie Manush">Heinie Manush</a></td>
<td class="datacolBox">6-01</td>
<td class="datacolBox">200</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1901-07-20</td>
</tr>
<tr>
<td class="datacolBox"><a href="/teams/baseball_uniform_numbers.php?t=DET#uni-n/a">n/a</a></td>
<td class="datacolBox"><a href="../players/player.php?p=wingoal01" title="Show stats for Al Wingo">Al Wingo</a></td>
<td class="datacolBox">5-11</td>
<td class="datacolBox">180</td>
<td class="datacolBox">Right</td>
<td class="datacolBox">Left</td>
<td class="datacolBox">1898-05-06</td>
</tr>
<tr>
<td class="banner" colspan="7">1924 Detroit Tigers Roster</td>
</tr>
</table>
</div>
<!-- End Page Body -->
<!-- Begin Related Pages -->
<div class="notes">
<p><a href="/custom_search.shtml?q=DET"><img alt="search this site" class="ba-notes-icon" src="/images/ba-search-btn.jpg" title="Search this Site"/></a>
<a href="/teamstats/glossary.shtml"><img alt="site glossary" class="ba-notes-icon" src="/images/ba-glossary-btn.jpg" title="Glossary"/></a>
<script>var pfHeaderImgUrl = '';var pfHeaderTagline = '';var pfdisableClickToDel = 0;var pfHideImages = 0;var pfImageDisplayStyle = 'right';var pfDisablePDF = 0;var pfDisableEmail = 0;var pfDisablePrint = 0;var pfCustomCSS = 'https://www.baseball-almanac.com/css/print.css';var pfBtVersion='2';(function(){var js,pf;pf=document.createElement('script');pf.type='text/javascript';pf.src='//cdn.printfriendly.com/printfriendly.js';document.getElementsByTagName('head')[0].appendChild(pf)})();</script><a class="printfriendly" href="https://www.printfriendly.com" onclick="window.print();return false;" style="color:#6D9F00;text-decoration:none;" title="Printer Friendly and PDF"><img alt="Print Friendly and PDF" src="/images/ba-print-friendly-btn.jpg" style="border:none;-webkit-box-shadow:none;box-shadow:none; width: auto; vertical-align: middle; display: inline-block"/></a>
</p>
</div>
<!-- End Related Pages -->
<img alt="baseball almanac flat baseball" class="flat-baseball-img" src="/images/ball-red.png"/>
<!-- Tag ID: Baseballalmanac_Medrec_A -->
<div align="center" id="Baseballalmanac_Medrec_A">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_Medrec_A", slotId: "Baseballalmanac_Medrec_A" });
</script>
</div>
<div class="fast-facts">
<h3><img alt="baseball almanac fast facts" src="/images/fast-facts-logo.png"/></h3>
<p>The 1924 Detroit Tigers played 154 games during the regular season, won 86 games, lost 68 games, and finished in third position. They played their home games at <a href="../stadium/tiger_stadium.shtml">Navin Field</a> (Park Factors: 98/97) where 1,015,136 fans witnessed their 1924 Tigers finish the season with a .558 winning percentage.</p>
<p>Baseball Almanac is pleased to present a unique set of rosters not easily
found on the Internet. Included, where data is available, is a 1924 Detroit Tigers Opening Day starters list, a 1924 Detroit Tigers salary
list, a 1924 Detroit Tigers uniform number breakdown and a 1924 Detroit Tigers primary starters list:</p><table class="ff-table"><tr><td style="vertical-align: top"><p><strong>1924 Detroit Tigers<br/>Opening Day Starters</strong></p><table class="ff-table"><tr><strong><p><strong>No Opening Day Information Available</strong></p></strong></tr></table></td><td style="vertical-align: top"><p><strong>1924 Detroit Tigers<br/>Most Games by Position</strong></p><table class="ff-table"><tr><td><strong>C</strong></td><td> <a href="../players/player.php?p=bassljo01" title="Show statistics for Johnny Bassler"><strong>Johnny Bassler</strong></a> <strong>(122)</strong></td></tr><tr><td><strong>1B</strong></td><td> <a href="../players/player.php?p=bluelu01" title="Show statistics for Lu Blue"><strong>Lu Blue</strong></a> <strong>(108)</strong></td></tr><tr><td><strong>2B</strong></td><td> <a href="../players/player.php?p=prattde01" title="Show statistics for Del Pratt"><strong>Del Pratt</strong></a> <strong>(65)</strong></td></tr><tr><td><strong>3B</strong></td><td> <a href="../players/player.php?p=jonesbo01" title="Show statistics for Bob Jones"><strong>Bob Jones</strong></a> <strong>(106)</strong></td></tr><tr><td><strong>SS</strong></td><td> <a href="../players/player.php?p=rigneto01" title="Show statistics for Topper Rigney"><strong>Topper Rigney</strong></a> <strong>(146)</strong></td></tr><tr><td><strong>LF</strong></td><td> <a href="../players/player.php?p=manushe01" title="Show statistics for Heinie Manush"><strong>Heinie Manush</strong></a> <strong>(98)</strong></td></tr><tr><td><strong>CF</strong></td><td> <a href="../players/player.php?p=cobbty01" title="Show statistics for Ty Cobb"><strong>Ty Cobb</strong></a> <strong>(155)</strong></td></tr><tr><td><strong>RF</strong></td><td> <a href="../players/player.php?p=heilmha01" title="Show statistics for Harry Heilmann"><strong>Harry Heilmann</strong></a> <strong>(147)</strong></td></tr><tr><td> </td><td> </td></tr><tr><td><strong>SP</strong></td><td> <a href="../players/player.php?p=colliri01" title="Show statistics for Rip Collins"><strong>Rip Collins</strong></a></td></tr><tr><td><strong>SP</strong></td><td> <a href="../players/player.php?p=stoneli01" title="Show statistics for Lil Stoner"><strong>Lil Stoner</strong></a></td></tr><tr><td><strong>SP</strong></td><td> <a href="../players/player.php?p=wellsed01" title="Show statistics for Ed Wells"><strong>Ed Wells</strong></a></td></tr><tr><td><strong>SP</strong></td><td> <a href="../players/player.php?p=whiteea01" title="Show statistics for Earl Whitehill"><strong>Earl Whitehill</strong></a></td></tr><tr><td> </td><td> </td></tr><tr><td><strong>RP</strong></td><td> <a href="../players/player.php?p=johnssy01" title="Show statistics for Syl Johnson"><strong>Syl Johnson</strong></a></td></tr><tr><td><strong>CL</strong></td><td> <a href="../players/player.php?p=daussho01" title="Show statistics for Hooks Dauss"><strong>Hooks Dauss</strong></a></td></tr></table></td></tr><tr><td style="vertical-align: top"><p><strong>1924 Detroit Tigers<br/>Salaries</strong></p><table class="ff-table"><tr><td><a href="../players/player.php?p=cobbty01" title="Show statistics for Ty Cobb"><strong>Ty Cobb</strong></a></td><td align="right"> <strong>$40,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=heilmha01" title="Show statistics for Harry Heilmann"><strong>Harry Heilmann</strong></a></td><td align="right"> <strong>$15,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=bassljo01" title="Show statistics for Johnny Bassler"><strong>Johnny Bassler</strong></a></td><td align="right"> <strong>$9,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=prattde01" title="Show statistics for Del Pratt"><strong>Del Pratt</strong></a></td><td align="right"> <strong>$8,500.00</strong></td></tr><tr><td><a href="../players/player.php?p=colebe01" title="Show statistics for Bert Cole"><strong>Bert Cole</strong></a></td><td align="right"> <strong>$5,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=maniocl01" title="Show statistics for Clyde Manion"><strong>Clyde Manion</strong></a></td><td align="right"> <strong>$4,500.00</strong></td></tr><tr><td><a href="../players/player.php?p=colliri01" title="Show statistics for Rip Collins"><strong>Rip Collins</strong></a></td><td align="right"> <strong>$4,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=fothebo01" title="Show statistics for Bob Fothergill"><strong>Bob Fothergill</strong></a></td><td align="right"> <strong>$3,850.00</strong></td></tr><tr><td><a href="../players/player.php?p=wellsed01" title="Show statistics for Ed Wells"><strong>Ed Wells</strong></a></td><td align="right"> <strong>$3,600.00</strong></td></tr><tr><td><a href="../players/player.php?p=manushe01" title="Show statistics for Heinie Manush"><strong>Heinie Manush</strong></a></td><td align="right"> <strong>$3,000.00</strong></td></tr><tr><td><a href="../players/player.php?p=gehrich01" title="Show statistics for Charlie Gehringer"><strong>Charlie Gehringer</strong></a></td><td align="right"> <strong>$2,100.00</strong></td></tr></table></td><td style="vertical-align: top"><p><strong>1924 Detroit Tigers<br/>Uniform Numbers</strong></p><table class="ff-table"><tr><td><strong>Uniform Numbers Not Worn</strong></td></tr></table></td></tr></table>
<p>Did you know that a <a href="../teamstats/schedule.php?y=1924&amp;t=DET">1924 Detroit Tigers Schedule</a> is available and it includes dates of every game played, scores of every game played, a cumulative record, and many hard to find splits (Monthly Splits, Team vs Team Splits &amp; Score Related Splits)?</p>
</div>
</div>
<div class="footer">
<!-- START _footer-v2.txt" -->
<!-- Tag ID: Baseballalmanac_leaderboard_BTF -->
<div align="center" id="Baseballalmanac_leaderboard_BTF">
<script data-cfasync="false" type="text/javascript">
    freestar.config.enabled_slots.push({ placementName: "Baseballalmanac_leaderboard_BTF", slotId: "Baseballalmanac_leaderboard_BTF" });
</script>
</div>
<div class="container">
<div class="notes">
<a href="/"><img alt="Baseball Almanac" src="/images/baseball-almanac-logo.png" style="max-width: 180px; margin; 0 auto"/></a>
<p>Where what happened yesterday<br/> is being preserved today.</p>
<div class="social">
<a href="https://twitter.com/BaseballAlmanac" target="_blank"><i class="fab fa-twitter"></i></a>
<a href="https://www.facebook.com/BaseballAlmanacInc/" target="_blank"><i class="fab fa-facebook-f"></i></a>
<a href="https://www.instagram.com/baseballalmanac/" target="_blank"><i class="fab fa-instagram"></i></a>
</div>
</div>
<div class="links">
<div>
<span>Stats</span>
<a href="/me_award.shtml">Awards</a>
<a href="/featmenu.shtml">Fabulous Feats</a>
<a href="/frstmenu.shtml">Famous Firsts</a>
<a href="/hofmenu.shtml">Hall of Fame</a>
<a href="/himenu.shtml">Hitting Charts</a>
<a href="/limenu.shtml">Legendary Lists</a>
<a href="/pimenu.shtml">Pitching Charts</a>
<a href="/rb_menu.shtml">Record Books</a>
<a href="/rulemenu.shtml">Rules</a>
<a href="/scoring.shtml">Scoring</a>
<a href="/teamstats/statmaster.php">Statmaster</a>
<a href="/bstatmen.shtml">Stats 101</a>
<a href="/yearmenu.shtml">Year by Year</a>
</div>
<div>
<span>People</span>
<a href="/automenu.shtml">Autographs</a>
<a href="/players/ballplayer.shtml">Ballplayers</a>
<a href="/fammenu.shtml">Baseball Families</a>
<a href="/players/baseball_interviews.shtml">Interviews</a>
<a href="/mgrmenu.shtml">Managers</a>
<a href="/deaths/chart/baseball_player_obituaries.shtml">Obituaries</a>
<a href="/quomenu.shtml">Quotes</a>
<a href="/teammenu.shtml">Team by Team</a>
<a href="/umpiresmenu.shtml">Umpires</a>
<a href="/prz_menu.shtml">US Presidents</a>
</div>
<div>
<span>Places</span>
<a href="/asgmenu.shtml">All-Star Game</a>
<a href="/stadium.shtml">Ballparks</a>
<a href="/division_series/division_series.shtml">Division Series</a>
<a href="/graves/baseball_graves.shtml">Grave Sites</a>
<a href="/League_Championship_Series.shtml">LCS</a>
<a href="/opening_day/opening_day.shtml">Opening Day</a>
<a href="/ws/wsmenu.shtml">World Series</a>
</div>
<div>
<span>Other</span>
<a href="/about.shtml">Advertising</a>
<a href="/baseball_cards/baseball_cards.php">Baseball Cards</a>
<a href="/bookmenu.shtml">Book Shelf</a>
<a href="/feedmenu.shtml">Feedback</a>
<a href="/gam_menu.shtml">Fun &amp; Games</a>
<a href="/humomenu.shtml">Humor &amp; Jokes</a>
<a href="/mve_time.shtml">Movie Time</a>
<a href="/mlmstart.shtml">Newsletter</a>
<a href="/baseball_news.shtml">News Feeds</a>
<a href="/poems.shtml">Poetry &amp; Song</a>
<a href="/privacy-policy.shtml">Privacy Policy</a>
<a href="/Search.shtml">Search &amp; Find</a>
<a href="/support.shtml">Support</a>
</div>
</div>
<div class="copyright">
<div class="legal">
<p>Copyright 1999-<script type="text/javascript">
			copyright=new Date();
			update=copyright.getFullYear();
			document.write(update);
		</script>. All Rights Reserved by Baseball Almanac, Inc.<br/>Hosted by <a href="https://www.hosting4less.com" rel="nofollow" target="_blank">Hosting 4 Less</a>. Part of the <a href="https://www.baseball-almanac.com/">Baseball Almanac</a> Family</p>
</div>
<div class="external">
<a href="http://www.755homeruns.com/" rel="nofollow" target="_blank">755 Home Runs</a>
<a href="http://www.baseball-boxscores.com/" rel="nofollow" target="_blank">Baseball Box Scores</a>
<a href="http://www.baseball-fever.com/" rel="nofollow" target="_blank">Baseball Fever</a>
<a href="http://www.todayinbaseballhistory.com/" rel="nofollow" target="_blank">Today in Baseball History</a>
</div>
</div>
</div><br/><br/><br/><br/><br/>
</div>
<!-- END _footer-v2.txt" -->
</div>
</body>
</html>
