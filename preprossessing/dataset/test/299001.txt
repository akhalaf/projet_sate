<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="index_htm_files/xr_files.txt" name="XAR Files"/>
<title>Cecil G. Rice, Illustrator - Home Page</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="Xara HTML filter v.3.1.0.371" name="Generator"/>
<meta content="illustrator, illustration, art, artist, drawing, digital art, freelance illustrator, freelance illustration, painting, graphic design, design, Atlanta, Georgia, Atlanta illustrator, Atlanta GA illustrator, Atlanta Georgia illustrator, Georgia illustrator, Georgia artist, Atlanta artist, Atlanta GA artist, Atlanta Georgia artist, Atlanta designer, Georgia designer, Atlanta designer, Atlanta GA designer, Atlanta Georgia disgner, artist portfolio, advertising art, editorial art, advertising illustration, editorial illustration, publishing, book art, icon, icons, stockart, stock art, stock illustration" name="keywords"/>
<meta content="Cecil G. Rice, Illustrator
Illustration Portfolio Home Page" name="description"/>
<link href="index_htm_files/xr_main.css" rel="stylesheet" type="text/css"/>
<link href="index_htm_files/xr_text.css" rel="stylesheet" type="text/css"/>
<script src="index_htm_files/roe.js" type="text/javascript"></script>
</head>
<body>
<div class="xr_ap" id="xr_xr" style="width: 800px; height: 700px; top:0px; left:50%; margin-left: -400px;">
<script type="text/javascript">var xr_xr=document.getElementById("xr_xr")</script>
<!--[if IE]><div class="xr_ap" id="xr_xri" style="width: 800px; height: 700px; clip: rect(0px 800px 700px 0px);"><![endif]-->
<!--[if !IE]>--><div class="xr_ap" id="xr_xri" style="width: 800px; height: 700px; clip: rect(0px, 800px, 700px, 0px);"><!--<![endif]-->
<a href="gallery_5.htm" onclick="return(xr_nn());">
<img alt="" class="xr_ap" onmousemove="xr_mo(this,0,event)" src="index_htm_files/28.png" style="left: 14px; top: 384px; width: 772px; height: 57px;" title=""/>
</a>
<a href="gallery_4.htm" onclick="return(xr_nn());">
<img alt="" class="xr_ap" onmousemove="xr_mo(this,0,event)" src="index_htm_files/29.png" style="left: 14px; top: 325px; width: 772px; height: 58px;" title=""/>
</a>
<a href="gallery_3.htm" onclick="return(xr_nn());">
<img alt="" class="xr_ap" onmousemove="xr_mo(this,0,event)" src="index_htm_files/30.png" style="left: 14px; top: 267px; width: 772px; height: 57px;" title=""/>
</a>
<a href="gallery_2.htm" onclick="return(xr_nn());">
<img alt="" class="xr_ap" onmousemove="xr_mo(this,0,event)" src="index_htm_files/31.png" style="left: 14px; top: 208px; width: 772px; height: 57px;" title=""/>
</a>
<a href="gallery_1.htm" onclick="return(xr_nn());">
<img alt="" class="xr_ap" onmousemove="xr_mo(this,0,event)" src="index_htm_files/32.png" style="left: 14px; top: 150px; width: 772px; height: 57px;" title=""/>
</a>
<div class="xr_ap" id="xr_xo0" style="left:0; top:0; width:800px; height:100px; visibility:hidden;">
<a href="" onclick="return(false);">
<img alt="" class="xr_ap" src="index_htm_files/33.png" style="left: 14px; top: 384px; width: 772px; height: 57px;" title=""/>
<img alt="" class="xr_ap" src="index_htm_files/34.png" style="left: 14px; top: 325px; width: 772px; height: 58px;" title=""/>
<img alt="" class="xr_ap" src="index_htm_files/35.png" style="left: 14px; top: 267px; width: 772px; height: 57px;" title=""/>
<img alt="" class="xr_ap" src="index_htm_files/36.png" style="left: 14px; top: 208px; width: 772px; height: 57px;" title=""/>
<img alt="" class="xr_ap" src="index_htm_files/37.png" style="left: 14px; top: 150px; width: 772px; height: 57px;" title=""/>
</a>
</div>
<img alt="" class="xr_ap" id="Repeating:58AutoRepeat3" src="index_htm_files/3.png" style="left: 15px; top: 63px; width: 770px; height: 76px;" title=""/>
<img alt="" class="xr_ap" id="Hair:32Lines" src="index_htm_files/4.png" style="left: 15px; top: 447px; width: 770px; height: 2px;" title=""/>
<span class="xr_s0" style="position: absolute; left:781px; top:461px;">
<span class="xr_tr" style="left: -86px; top: -7px; width: 86px;">© 2011 Cecil G. Rice</span>
</span>
<img alt="" class="xr_ap" id="Repeating:58AutoRepeat6" onmousemove="xr_mp(5,0)" src="index_htm_files/8.png" style="left: 743px; top: 130px; width: 28px; height: 15px;" title=""/>
<div id="xr_xd0"></div>
<div class="xr_ap xr_pua11 xr_pua21" id="xr_xp5" style="left:0; top:0; visibility: hidden; display: none;">
<div class="xr_ap" onmousemove="xr_mpc(5)">
<img alt="" class="xr_ap" src="index_htm_files/25.png" style="left: 480px; top: 143px; width: 314px; height: 151px;" title=""/>
</div>
<div class="xr_ap" onmousemove="xr_mpc(5)">
<img alt="" class="xr_ap" src="index_htm_files/26.png" style="left: 731px; top: 140px; width: 43px; height: 34px;" title=""/>
</div>
<span class="xr_s1" onmousemove="xr_mpc(5)" style="position: absolute; left:521px; top:223px;">
<span class="xr_tl" style="top: -12px;">Cecil G. Rice, Illustrator</span>
<span class="xr_tl xr_s2" style="top: 3px;">678-819-5737</span>
<span class="xr_tl xr_s2" style="top: 15px;"><a href="mailto:cecil@cecilgrice.com" onclick="return(xr_nn());" onmousemove="xr_mo(this,5)">cecil@cecilgrice.com</a></span>
</span>
<div class="xr_ap" onmousemove="xr_mpc(5)">
<img alt="" class="xr_ap" src="index_htm_files/27.png" style="left: 742px; top: 121px; width: 30px; height: 25px;" title=""/>
</div>
</div>
<div id="xr_xo5"></div>
<div id="xr_xd5"></div>
<div id="xr_xo6"></div>
<div id="xr_xd6"></div>
</div>
</div>
<!--[if lt IE 7]><script type="text/javascript" src="index_htm_files/png.js"></script><![endif]-->
<script type="text/javascript">xr_aeh()</script>
</body>
</html>