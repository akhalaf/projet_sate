<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<title>No se encontró la página | Guillermo Stahl &amp; Cia. S.A.C.</title>
<!-- Created by Artisteer v4.1.0.60046 -->
<meta content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" name="viewport"/>
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link href="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/style.css" media="screen" rel="stylesheet"/>
<link href="https://stahlibrosdistribuidora.com/xmlrpc.php" rel="pingback"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://stahlibrosdistribuidora.com/feed/" rel="alternate" title="Guillermo Stahl &amp; Cia. S.A.C. » Feed" type="application/rss+xml"/>
<link href="https://stahlibrosdistribuidora.com/comments/feed/" rel="alternate" title="Guillermo Stahl &amp; Cia. S.A.C. » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/stahlibrosdistribuidora.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://stahlibrosdistribuidora.com/wp-includes/css/dashicons.min.css?ver=5.2.9" id="dashicons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://stahlibrosdistribuidora.com/wp-content/plugins/post-type-x/core/css/al_product.min.css?1560150343&amp;ver=5.2.9" id="al_product_styles-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://stahlibrosdistribuidora.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.9" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<!--[if lte IE 7]>
<link rel='stylesheet' id='style.ie7.css-css'  href='https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/style.ie7.css?ver=5.2.9' type='text/css' media='screen' />
<![endif]-->
<link href="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/style.responsive.css?ver=5.2.9" id="style.responsive.css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/jquery.js?ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var product_object = {"ajaxurl":"https:\/\/stahlibrosdistribuidora.com\/wp-admin\/admin-ajax.php","lightbox_settings":{"transition":"elastic","initialWidth":200,"maxWidth":"90%","maxHeight":"90%","rel":"gal"}};
/* ]]> */
</script>
<script src="https://stahlibrosdistribuidora.com/wp-content/plugins/post-type-x/core/js/product.min.js?1560150343&amp;ver=5.2.9" type="text/javascript"></script>
<script type="text/javascript">
/* <![CDATA[ */
var ic_ajax = {"query_vars":"{\"category_name\":\"wp-includes\\\/gtex%09\"}","request_url":"https:\/\/stahlibrosdistribuidora.com\/wp-includes\/gtex%09","filters_reset_url":"","is_search":"","nonce":"c72cbdaa37"};
/* ]]> */
</script>
<script src="https://stahlibrosdistribuidora.com/wp-content/plugins/post-type-x/core/js/product-ajax.min.js?1560150343&amp;ver=5.2.9" type="text/javascript"></script>
<script src="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/jquery-migrate-1.1.1.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/script.js?ver=5.2.9" type="text/javascript"></script>
<script src="https://stahlibrosdistribuidora.com/wp-content/themes/stahl2015finalblue/script.responsive.js?ver=5.2.9" type="text/javascript"></script>
<link href="https://stahlibrosdistribuidora.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://stahlibrosdistribuidora.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://stahlibrosdistribuidora.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
</head>
<body class="error404">
<div id="art-main">
<header class="art-header">
<div class="art-shapes">
<div class="art-object1095221043" data-left="0.92%"></div>
</div>
<div class="art-headline" data-left="19.59%">
<a href="http://stahlibrosdistribuidora.com/">Guillermo Stahl &amp; Cia. S.A.C.</a>
</div>
<div class="art-slogan" data-left="23.28%">Distribuidora de Libros y Enciclopedias</div>
<div class="art-textblock art-textblock-1902503339" data-left="70.98%">
<div class="art-textblock-1902503339-text-container">
<div class="art-textblock-1902503339-text"> <a class="art-facebook-tag-icon" href="http://www.facebook.com/"></a> </div>
</div>
</div><div class="art-textblock art-textblock-121043336" data-left="67.38%">
<div class="art-textblock-121043336-text-container">
<div class="art-textblock-121043336-text"> <a class="art-twitter-tag-icon" href="https://twitter.com/"></a> </div>
</div>
</div>
<div class="art-textblock art-object545686547" data-left="98.44%">
<form action="https://stahlibrosdistribuidora.com/" class="art-search" method="get" name="searchform">
<input name="s" type="text" value=""/>
<input class="art-search-button" type="submit" value=""/>
</form></div>
<nav class="art-nav">
<div class="art-nav-inner">
<ul class="art-hmenu menu-3">
<li class="menu-item-93"><a href="https://stahlibrosdistribuidora.com/" title="Inicio">Inicio</a>
</li>
<li class="menu-item-165"><a href="https://stahlibrosdistribuidora.com/lanzamientos/" title="Lanzamientos">Lanzamientos</a>
</li>
<li class="menu-item-94"><a href="https://stahlibrosdistribuidora.com/nosotros/" title="Nosotros">Nosotros</a>
</li>
<li class="menu-item-95"><a href="https://stahlibrosdistribuidora.com/contactenos/" title="Contactenos">Contactenos</a>
</li>
</ul>
</div>
</nav>
</header>
<div class="art-sheet clearfix">
<div class="art-layout-wrapper">
<div class="art-content-layout">
<div class="art-content-layout-row">
<div class="art-layout-cell art-sidebar1"><div class="art-vmenublock clearfix" id="vmenuwidget-1">
<div class="art-vmenublockheader">
<div class="t">Catálogos</div>
</div><div class="art-vmenublockcontent">
<ul class="art-vmenu">
<li class="menu-item-155"><a href="https://stahlibrosdistribuidora.com/diccionarios/" title="Diccionarios">Diccionarios</a>
</li>
<li class="menu-item-156"><a href="https://stahlibrosdistribuidora.com/enciclopedias/" title="Enciclopedias">Enciclopedias</a>
</li>
<li class="menu-item-57"><a href="https://stahlibrosdistribuidora.com/infantilesjuveniles/" title="Infantiles/juveniles">Infantiles/juveniles</a>
</li>
<li class="menu-item-58"><a href="https://stahlibrosdistribuidora.com/cuentos-y-fabulas/" title="Cuentos y fabulas">Cuentos y fabulas</a>
</li>
<li class="menu-item-59"><a href="https://stahlibrosdistribuidora.com/ciencias-matematicas-y-fisicas/" title="Ciencias matemáticas y físicas">Ciencias matemáticas y físicas</a>
</li>
<li class="menu-item-60"><a href="https://stahlibrosdistribuidora.com/historia-universal-y-del-peru/" title="Historia universal y del perú">Historia universal y del perú</a>
</li>
<li class="menu-item-61"><a href="https://stahlibrosdistribuidora.com/geografia/" title="Geografía">Geografía</a>
</li>
<li class="menu-item-62"><a href="https://stahlibrosdistribuidora.com/computacion-e-internet/" title="Computación e internet">Computación e internet</a>
</li>
<li class="menu-item-63"><a href="https://stahlibrosdistribuidora.com/idiomas/" title="Idiomas">Idiomas</a>
</li>
<li class="menu-item-64"><a href="https://stahlibrosdistribuidora.com/raz-verbal-legua-literatura-y-gramatica/" title="Raz. Verbal, legua, literatura y gramática">Raz. Verbal, legua, literatura y gramática</a>
</li>
<li class="menu-item-65"><a href="https://stahlibrosdistribuidora.com/cocina-reposteria-y-bar/" title="Cocina, repostería y bar">Cocina, repostería y bar</a>
</li>
<li class="menu-item-66"><a href="https://stahlibrosdistribuidora.com/manualidades-peluqueria-corte-y-confeccion/" title="Manualidades, peluquería corte y confección">Manualidades, peluquería corte y confección</a>
</li>
<li class="menu-item-67"><a href="https://stahlibrosdistribuidora.com/familia-salud-y-sexualidad/" title="Familia, salud y sexualidad">Familia, salud y sexualidad</a>
</li>
<li class="menu-item-68"><a href="https://stahlibrosdistribuidora.com/deportes/" title="Deportes">Deportes</a>
</li>
<li class="menu-item-69"><a href="https://stahlibrosdistribuidora.com/biblia-y-religion/" title="Biblia y religión">Biblia y religión</a>
</li>
<li class="menu-item-70"><a href="https://stahlibrosdistribuidora.com/obras-universitarias/" title="Obras universitarias">Obras universitarias</a>
</li>
<li class="menu-item-71"><a href="https://stahlibrosdistribuidora.com/belleza-estetica-y-cosmetologia/" title="Belleza estética y cosmetología">Belleza estética y cosmetología</a>
</li>
<li class="menu-item-72"><a href="https://stahlibrosdistribuidora.com/psicologia-pedagogiapsiquiatria-y-educacion/" title="Psicología, pedagogía,psiquiatría y educación">Psicología, pedagogía,psiquiatría y educación</a>
</li>
<li class="menu-item-73"><a href="https://stahlibrosdistribuidora.com/medicina-odontologia-y-enfermeria/" title="Medicina , odontología y enfermeria">Medicina , odontología y enfermeria</a>
</li>
<li class="menu-item-74"><a href="https://stahlibrosdistribuidora.com/agricultura-ganaderia-ciencias-forestales-y-veterinaria/" title="Agricultura, ganadería , ciencias forestales y veterinaria">Agricultura, ganadería , ciencias forestales…</a>
</li>
<li class="menu-item-75"><a href="https://stahlibrosdistribuidora.com/temas-empresariales-y-administracion/" title="Temas empresariales y administración">Temas empresariales y administración</a>
</li>
<li class="menu-item-76"><a href="https://stahlibrosdistribuidora.com/logopedia/" title="Logopedia">Logopedia</a>
</li>
<li class="menu-item-77"><a href="https://stahlibrosdistribuidora.com/leyendas/" title="Leyendas">Leyendas</a>
</li>
<li class="menu-item-78"><a href="https://stahlibrosdistribuidora.com/turismo/" title="Turismo">Turismo</a>
</li>
<li class="menu-item-79"><a href="https://stahlibrosdistribuidora.com/tecnicos-y-miscelaneos/" title="Técnicos y misceláneos">Técnicos y misceláneos</a>
</li>
<li class="menu-item-80"><a href="https://stahlibrosdistribuidora.com/derecho-ciencias-juridicas/" title="Derecho, ciencias jurídicas">Derecho, ciencias jurídicas</a>
</li>
<li class="menu-item-81"><a href="https://stahlibrosdistribuidora.com/ingenieria-electricidad-electronica-y-mecanica/" title="Ingeniería, electricidad, electrónica y mecánica">Ingeniería, electricidad, electrónica y…</a>
</li>
<li class="menu-item-82"><a href="https://stahlibrosdistribuidora.com/arquitectura-construccion-carpinteria-ebanisteria-herreria/" title="Arquitectura, construcción, carpintería. Ebanistería, herrería">Arquitectura, construcción, carpintería.…</a>
</li>
<li class="menu-item-83"><a href="https://stahlibrosdistribuidora.com/diseno-decoracion-arte-y-pintura/" title="Diseño, decoración, arte y pintura">Diseño, decoración, arte y pintura</a>
</li>
<li class="menu-item-84"><a href="https://stahlibrosdistribuidora.com/atlas-ilustrados/" title="Atlas ilustrados">Atlas ilustrados</a>
</li>
<li class="menu-item-85"><a href="https://stahlibrosdistribuidora.com/varios/" title="Varios">Varios</a>
</li>
<li class="menu-item-86"><a href="https://stahlibrosdistribuidora.com/gestion-municipal/" title="Gestión municipal">Gestión municipal</a>
</li>
<li class="menu-item-87"><a href="https://stahlibrosdistribuidora.com/belicos/" title="Bélicos">Bélicos</a>
</li>
<li class="menu-item-88"><a href="https://stahlibrosdistribuidora.com/revistas-y-cultura-general/" title="Revistas y cultura general">Revistas y cultura general</a>
</li>
<li class="menu-item-89"><a href="https://stahlibrosdistribuidora.com/plan-lector/" title="Plan lector">Plan lector</a>
</li>
<li class="menu-item-90"><a href="https://stahlibrosdistribuidora.com/obras-y-novelas/" title="Obras y novelas">Obras y novelas</a>
</li>
<li class="menu-item-91"><a href="https://stahlibrosdistribuidora.com/marketing/" title="Marketing">Marketing</a>
</li>
<li class="menu-item-92"><a href="https://stahlibrosdistribuidora.com/tecnicos-especializados/" title="Técnicos especializados">Técnicos especializados</a>
</li>
</ul>
</div>
</div></div>
<div class="art-layout-cell art-content">
<article class="art-post art-article ">
<div class="art-postmetadataheader"><h2 class="art-postheader"><span class="art-postheadericon">No encontrado</span></h2></div> <div class="art-postcontent clearfix"><p class="center">Lo sentimos, pero no podemos encontrar lo que estás buscando. Quizás la búsqueda te ayudará.</p>
<form action="https://stahlibrosdistribuidora.com/" class="art-search" method="get" name="searchform">
<input name="s" type="text" value=""/>
<input class="art-search-button" type="submit" value=""/>
</form><script type="text/javascript">jQuery('div.art-content input[name="s"]').focus();</script></div>
</article>
</div>
</div>
</div>
</div>
</div>
<footer class="art-footer">
<div class="art-footer-inner"><div class="art-footer-text">
<div class="art-content-layout">
<div class="art-content-layout-row">
<div class="art-layout-cell layout-item-0" style="width: 100%"> <p style="text-align: center;">Guillermo Stahl &amp; Cía. S.A.C. Representante - Distribuidor Editorial<br/>Dirección: Av. Arequipa 3146 - Of. 602 - San isidro, Lima - Perú<br/>Teléfono: 440-2516</p>
</div>
</div>
</div>
<p class="art-page-footer">
<span id="art-footnote-links">Powered by <a href="http://wordpress.org/" target="_blank">WordPress</a> and <a href="http://www.artisteer.com/?p=wordpress_themes" target="_blank">WordPress Theme</a> created with Artisteer.</span>
</p>
</div>
</div>
</footer>
</div>
<div id="wp-footer">
<script src="https://stahlibrosdistribuidora.com/wp-includes/js/wp-embed.min.js?ver=5.2.9" type="text/javascript"></script>
<!-- 45 queries. 0,275 seconds. -->
</div>
</body>
</html>
