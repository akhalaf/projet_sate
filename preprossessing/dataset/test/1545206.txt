<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="es">
<![endif]--><!--[if IE 8]>
<html class="ie ie8" lang="es">
<![endif]--><!--[if !(IE 7) & !(IE 8)]><!--><html lang="es">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<title>Página no encontrada – Pía Unión Virgen del Milagro</title>
<script>(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
				 fjs.parentNode.insertBefore(js, fjs);
			   }(document, 'script', 'facebook-jssdk'));</script><link href="//ws.sharethis.com" rel="dns-prefetch"/>
<link href="//www.virgendelmilagro.es" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//maxcdn.bootstrapcdn.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.virgendelmilagro.es/feed" rel="alternate" title="Pía Unión Virgen del Milagro » Feed" type="application/rss+xml"/>
<link href="https://www.virgendelmilagro.es/comments/feed" rel="alternate" title="Pía Unión Virgen del Milagro » Feed de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.virgendelmilagro.es\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.virgendelmilagro.es/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Indie+Flower&amp;ver=5.4.4" id="simple-share-buttons-adder-indie-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?ver=5.4.4" id="simple-share-buttons-adder-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.virgendelmilagro.es/wp-content/themes/spacious/style.css?ver=5.4.4" id="spacious_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.virgendelmilagro.es/wp-content/themes/spacious/genericons/genericons.css?ver=3.3.1" id="spacious-genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.virgendelmilagro.es/wp-content/themes/spacious/font-awesome/css/font-awesome.min.css?ver=4.7.0" id="spacious-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato&amp;ver=5.4.4" id="spacious_googlefonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="st_insights_js" src="https://ws.sharethis.com/button/st_insights.js?publisher=4d48b7c5-0ae3-43d4-bfbe-3ff8c17a8ae6&amp;product=simpleshare" type="text/javascript"></script>
<script src="https://www.virgendelmilagro.es/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://www.virgendelmilagro.es/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://www.virgendelmilagro.es/wp-content/themes/spacious/js/spacious-custom.js?ver=5.4.4" type="text/javascript"></script>
<!--[if lte IE 8]>
<script type='text/javascript' src='https://www.virgendelmilagro.es/wp-content/themes/spacious/js/html5shiv.min.js?ver=5.4.4'></script>
<![endif]-->
<link href="https://www.virgendelmilagro.es/wp-json/" rel="https://api.w.org/"/>
<link href="https://www.virgendelmilagro.es/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.virgendelmilagro.es/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<link href="https://www.virgendelmilagro.es/wp-content/uploads/2016/09/cropped-m-32x32.jpg" rel="icon" sizes="32x32"/>
<link href="https://www.virgendelmilagro.es/wp-content/uploads/2016/09/cropped-m-192x192.jpg" rel="icon" sizes="192x192"/>
<link href="https://www.virgendelmilagro.es/wp-content/uploads/2016/09/cropped-m-180x180.jpg" rel="apple-touch-icon"/>
<meta content="https://www.virgendelmilagro.es/wp-content/uploads/2016/09/cropped-m-270x270.jpg" name="msapplication-TileImage"/>
<style type="text/css"> blockquote { border-left: 3px solid #15160a; }
			.spacious-button, input[type="reset"], input[type="button"], input[type="submit"], button { background-color: #15160a; }
			.previous a:hover, .next a:hover { 	color: #15160a; }
			a { color: #15160a; }
			#site-title a:hover { color: #15160a; }
			.main-navigation ul li.current_page_item a, .main-navigation ul li:hover > a { color: #15160a; }
			.main-navigation ul li ul { border-top: 1px solid #15160a; }
			.main-navigation ul li ul li a:hover, .main-navigation ul li ul li:hover > a, .main-navigation ul li.current-menu-item ul li a:hover, .main-navigation ul li:hover > .sub-toggle { color: #15160a; }
			.site-header .menu-toggle:hover.entry-meta a.read-more:hover,#featured-slider .slider-read-more-button:hover,.call-to-action-button:hover,.entry-meta .read-more-link:hover,.spacious-button:hover, input[type="reset"]:hover, input[type="button"]:hover, input[type="submit"]:hover, button:hover { background: #000000; }
			.main-small-navigation li:hover { background: #15160a; }
			.main-small-navigation ul > .current_page_item, .main-small-navigation ul > .current-menu-item { background: #15160a; }
			.main-navigation a:hover, .main-navigation ul li.current-menu-item a, .main-navigation ul li.current_page_ancestor a, .main-navigation ul li.current-menu-ancestor a, .main-navigation ul li.current_page_item a, .main-navigation ul li:hover > a  { color: #15160a; }
			.small-menu a:hover, .small-menu ul li.current-menu-item a, .small-menu ul li.current_page_ancestor a, .small-menu ul li.current-menu-ancestor a, .small-menu ul li.current_page_item a, .small-menu ul li:hover > a { color: #15160a; }
			#featured-slider .slider-read-more-button { background-color: #15160a; }
			#controllers a:hover, #controllers a.active { background-color: #15160a; color: #15160a; }
			.widget_service_block a.more-link:hover, .widget_featured_single_post a.read-more:hover,#secondary a:hover,logged-in-as:hover  a,.single-page p a:hover{ color: #000000; }
			.breadcrumb a:hover { color: #15160a; }
			.tg-one-half .widget-title a:hover, .tg-one-third .widget-title a:hover, .tg-one-fourth .widget-title a:hover { color: #15160a; }
			.pagination span ,.site-header .menu-toggle:hover{ background-color: #15160a; }
			.pagination a span:hover { color: #15160a; border-color: #15160a; }
			.widget_testimonial .testimonial-post { border-color: #15160a #EAEAEA #EAEAEA #EAEAEA; }
			.call-to-action-content-wrapper { border-color: #EAEAEA #EAEAEA #EAEAEA #15160a; }
			.call-to-action-button { background-color: #15160a; }
			#content .comments-area a.comment-permalink:hover { color: #15160a; }
			.comments-area .comment-author-link a:hover { color: #15160a; }
			.comments-area .comment-author-link span { background-color: #15160a; }
			.comment .comment-reply-link:hover { color: #15160a; }
			.nav-previous a:hover, .nav-next a:hover { color: #15160a; }
			#wp-calendar #today { color: #15160a; }
			.widget-title span { border-bottom: 2px solid #15160a; }
			.footer-widgets-area a:hover { color: #15160a !important; }
			.footer-socket-wrapper .copyright a:hover { color: #15160a; }
			a#back-top:before { background-color: #15160a; }
			.read-more, .more-link { color: #15160a; }
			.post .entry-title a:hover, .page .entry-title a:hover { color: #15160a; }
			.post .entry-meta .read-more-link { background-color: #15160a; }
			.post .entry-meta a:hover, .type-page .entry-meta a:hover { color: #15160a; }
			.single #content .tags a:hover { color: #15160a; }
			.widget_testimonial .testimonial-icon:before { color: #15160a; }
			a#scroll-up { background-color: #15160a; }
			.search-form span { background-color: #15160a; }.header-action .search-wrapper:hover .fa{ color: #15160a} .spacious-woocommerce-cart-views .cart-value { background:#15160a}.main-navigation .tg-header-button-wrap.button-one a{background-color:#15160a} .main-navigation .tg-header-button-wrap.button-one a{border-color:#15160a}.main-navigation .tg-header-button-wrap.button-one a:hover{background-color:#000000}.main-navigation .tg-header-button-wrap.button-one a:hover{border-color:#000000}</style>
<style id="wp-custom-css" type="text/css">
			.post .entry-meta .author{display:none;}		</style>
</head>
<body class="error404 ">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#main">Saltar al contenido</a>
<header class="site-header clearfix spacious-header-display-one" id="masthead">
<div class="" id="header-text-nav-container">
<div class="inner-wrap" id="spacious-header-display-one">
<div class="clearfix" id="header-text-nav-wrap">
<div id="header-left-section">
<div class="" id="header-text">
<h3 id="site-title">
<a href="https://www.virgendelmilagro.es/" rel="home" title="Pía Unión Virgen del Milagro">Pía Unión Virgen del Milagro</a>
</h3>
<p id="site-description">Pía Unión Virgen del Milagro de Cocentaina</p>
<!-- #site-description -->
</div><!-- #header-text -->
</div><!-- #header-left-section -->
<div id="header-right-section">
<div class="header-action">
</div>
<nav class="main-navigation clearfix " id="site-navigation" role="navigation">
<p class="menu-toggle">Menú</p>
<div class="menu"><ul><li class="page_item page-item-77 page_item_has_children"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas">Milagro de las lágrimas</a><ul class="children"><li class="page_item page-item-74"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas/introduccion">Introducción</a></li><li class="page_item page-item-76"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas/su-origen">Su Origen</a></li><li class="page_item page-item-73"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas/historia-de-nuestra-senora-del-milagro">Historia de Nuestra Señora del Milagro</a></li><li class="page_item page-item-75"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas/mareta">Sagrada Imagen</a></li><li class="page_item page-item-80"><a href="https://www.virgendelmilagro.es/milagro-de-las-lagrimas/la-capilla-de-la-mare-de-deu">La Capilla de la Mare de Déu</a></li></ul></li><li class="page_item page-item-124 page_item_has_children"><a href="https://www.virgendelmilagro.es/historia">Historia</a><ul class="children"><li class="page_item page-item-103"><a href="https://www.virgendelmilagro.es/historia/homenaje-mayores">Histórico Homenaje Mayores</a></li><li class="page_item page-item-104"><a href="https://www.virgendelmilagro.es/historia/cargos">Histórico Cargos</a></li><li class="page_item page-item-105"><a href="https://www.virgendelmilagro.es/historia/predicadores">Histórico Predicadores</a></li><li class="page_item page-item-102"><a href="https://www.virgendelmilagro.es/historia/cartelistas">Histórico Cartelistas</a></li></ul></li><li class="page_item page-item-145 page_item_has_children"><a href="https://www.virgendelmilagro.es/v-centenario">V Centenario</a><ul class="children"><li class="page_item page-item-343"><a href="https://www.virgendelmilagro.es/v-centenario/calendario-v-centenario">Calendario V Centenario</a></li><li class="page_item page-item-332"><a href="https://www.virgendelmilagro.es/v-centenario/peregrinacions">Peregrinaciones</a></li><li class="page_item page-item-335 page_item_has_children"><a href="https://www.virgendelmilagro.es/v-centenario/recorridos-mare-de-deu">Recorridos Mare de Déu</a><ul class="children"><li class="page_item page-item-459"><a href="https://www.virgendelmilagro.es/v-centenario/recorridos-mare-de-deu/primer-fin-de-semana">Primer fin de semana</a></li><li class="page_item page-item-461"><a href="https://www.virgendelmilagro.es/v-centenario/recorridos-mare-de-deu/segundo-fin-de-semana">Segundo fin de semana</a></li><li class="page_item page-item-464"><a href="https://www.virgendelmilagro.es/v-centenario/recorridos-mare-de-deu/tercer-fin-de-semana">Tercer fin de semana</a></li><li class="page_item page-item-466"><a href="https://www.virgendelmilagro.es/v-centenario/recorridos-mare-de-deu/cuarto-fin-de-semana">Cuarto fin de semana</a></li></ul></li><li class="page_item page-item-345"><a href="https://www.virgendelmilagro.es/v-centenario/comisiones">Comisiones</a></li><li class="page_item page-item-348"><a href="https://www.virgendelmilagro.es/v-centenario/acta-del-milagro">Acta del Milagro</a></li></ul></li><li class="page_item page-item-139"><a href="https://www.virgendelmilagro.es/programa-de-actos">Programa de actos</a></li><li class="page_item page-item-143"><a href="https://www.virgendelmilagro.es/pia-union">Pía Unión</a></li><li class="page_item page-item-405 page_item_has_children"><a href="https://www.virgendelmilagro.es/concurso-de-fotografia">Concurso de fotografía</a><ul class="children"><li class="page_item page-item-473"><a href="https://www.virgendelmilagro.es/concurso-de-fotografia/iv-concurso-de-fotografia">2019 – IV Concurso de fotografía</a></li><li class="page_item page-item-427"><a href="https://www.virgendelmilagro.es/concurso-de-fotografia/2018-iii-concurso-de-fotografia">2018 – III Concurso de Fotografía</a></li><li class="page_item page-item-432"><a href="https://www.virgendelmilagro.es/concurso-de-fotografia/2017-ii-concurso-de-fotografia">2017 – II Concurso de Fotografía</a></li><li class="page_item page-item-442"><a href="https://www.virgendelmilagro.es/concurso-de-fotografia/2016-i-concurs-de-fotografia">2016 – I Concurs de Fotografía</a></li></ul></li><li class="page_item page-item-453"><a href="https://www.virgendelmilagro.es/xiquets-joves">Projecte Xiquets – Joves</a></li></ul></div> </nav>
</div><!-- #header-right-section -->
</div><!-- #header-text-nav-wrap -->
</div><!-- .inner-wrap -->
</div><!-- #header-text-nav-container -->
<div class="wp-custom-header" id="wp-custom-header"><img alt="Pía Unión Virgen del Milagro" class="header-image" height="429" src="https://virgendelmilagro.es/wp-content/uploads/2016/09/cropped-DSC7709-1.jpg" width="1500"/></div>
<div class="header-post-title-container clearfix">
<div class="inner-wrap">
<div class="post-title-wrapper">
<h1 class="header-post-title-class">No se encontró la página</h1>
</div>
</div>
</div>
</header>
<div class="clearfix" id="main">
<div class="inner-wrap">
<div id="primary">
<div class="clearfix" id="content">
<section class="error-404 not-found">
<div class="page-content">
<header class="page-header">
<h2 class="page-title">¡Vaya! Está página no se encuentra.</h2>
</header>
<p>No hemos encontrado nada en este lugar. ¿Que tal si pruebas el buscador?</p>
<form action="https://www.virgendelmilagro.es/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Buscar" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform -->
</div><!-- .page-content -->
</section><!-- .error-404 -->
</div><!-- #content -->
</div><!-- #primary -->
<div id="secondary">
<aside class="widget widget_search" id="search-2"><form action="https://www.virgendelmilagro.es/" class="search-form searchform clearfix" method="get">
<div class="search-wrap">
<input class="s field" name="s" placeholder="Buscar" type="text"/>
<button class="search-icon" type="submit"></button>
</div>
</form><!-- .searchform --></aside> <aside class="widget widget_recent_entries" id="recent-posts-2"> <h3 class="widget-title"><span>Entradas recientes</span></h3> <ul>
<li>
<a href="https://www.virgendelmilagro.es/comunicat-festes-2020">Comunicat Festes 2020</a>
</li>
<li>
<a href="https://www.virgendelmilagro.es/la-virgen-del-milagro-recorre-todas-las-calles-de-cocentaina">La Virgen del Milagro recorre todas las calles de Cocentaina</a>
</li>
<li>
<a href="https://www.virgendelmilagro.es/prop-de-1000-persones-ja-shan-guanyat-la-indulgencia-plenaria">Prop de 1000 persones ja s’han guanyat la indulgència plenària</a>
</li>
<li>
<a href="https://www.virgendelmilagro.es/inicio-ano-santo-2019-2020">Inicio Año Santo 2019-2020</a>
</li>
<li>
<a href="https://www.virgendelmilagro.es/sant-antoni-2019">Sant Antoni 2019</a>
</li>
</ul>
</aside><aside class="widget widget_categories" id="categories-2"><h3 class="widget-title"><span>Categorías</span></h3> <ul>
<li class="cat-item cat-item-53"><a href="https://www.virgendelmilagro.es/category/general">General</a>
<ul class="children">
<li class="cat-item cat-item-48"><a href="https://www.virgendelmilagro.es/category/general/actividades">Actividades</a>
</li>
<li class="cat-item cat-item-46"><a href="https://www.virgendelmilagro.es/category/general/concursos">Concursos</a>
</li>
<li class="cat-item cat-item-47"><a href="https://www.virgendelmilagro.es/category/general/festividades">Festividades</a>
</li>
</ul>
</li>
</ul>
</aside><aside class="widget widget_archive" id="archives-2"><h3 class="widget-title"><span>Archivos</span></h3> <ul>
<li><a href="https://www.virgendelmilagro.es/2020/04">abril 2020</a></li>
<li><a href="https://www.virgendelmilagro.es/2020/01">enero 2020</a></li>
<li><a href="https://www.virgendelmilagro.es/2019/07">julio 2019</a></li>
<li><a href="https://www.virgendelmilagro.es/2019/05">mayo 2019</a></li>
<li><a href="https://www.virgendelmilagro.es/2019/01">enero 2019</a></li>
<li><a href="https://www.virgendelmilagro.es/2018/03">marzo 2018</a></li>
<li><a href="https://www.virgendelmilagro.es/2018/01">enero 2018</a></li>
<li><a href="https://www.virgendelmilagro.es/2017/10">octubre 2017</a></li>
<li><a href="https://www.virgendelmilagro.es/2017/04">abril 2017</a></li>
<li><a href="https://www.virgendelmilagro.es/2016/09">septiembre 2016</a></li>
<li><a href="https://www.virgendelmilagro.es/2016/04">abril 2016</a></li>
<li><a href="https://www.virgendelmilagro.es/2015/10">octubre 2015</a></li>
<li><a href="https://www.virgendelmilagro.es/2015/09">septiembre 2015</a></li>
<li><a href="https://www.virgendelmilagro.es/2015/07">julio 2015</a></li>
<li><a href="https://www.virgendelmilagro.es/2014/10">octubre 2014</a></li>
<li><a href="https://www.virgendelmilagro.es/2014/09">septiembre 2014</a></li>
<li><a href="https://www.virgendelmilagro.es/2014/04">abril 2014</a></li>
<li><a href="https://www.virgendelmilagro.es/2014/03">marzo 2014</a></li>
<li><a href="https://www.virgendelmilagro.es/2013/04">abril 2013</a></li>
<li><a href="https://www.virgendelmilagro.es/2013/03">marzo 2013</a></li>
<li><a href="https://www.virgendelmilagro.es/2013/02">febrero 2013</a></li>
<li><a href="https://www.virgendelmilagro.es/2012/12">diciembre 2012</a></li>
<li><a href="https://www.virgendelmilagro.es/2012/10">octubre 2012</a></li>
<li><a href="https://www.virgendelmilagro.es/2012/09">septiembre 2012</a></li>
<li><a href="https://www.virgendelmilagro.es/2012/06">junio 2012</a></li>
<li><a href="https://www.virgendelmilagro.es/2012/04">abril 2012</a></li>
<li><a href="https://www.virgendelmilagro.es/2011/10">octubre 2011</a></li>
<li><a href="https://www.virgendelmilagro.es/2011/02">febrero 2011</a></li>
</ul>
</aside> </div>
</div><!-- .inner-wrap -->
</div><!-- #main -->
<footer class="clearfix" id="colophon">
<div class="footer-socket-wrapper clearfix">
<div class="inner-wrap">
<div class="footer-socket-area">
<div class="copyright">Copyright © 2021 <a href="https://www.virgendelmilagro.es/" title="Pía Unión Virgen del Milagro"><span>Pía Unión Virgen del Milagro</span></a>. Potenciado por <a href="https://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a> Tema: Spacious por <a href="https://themegrill.com/themes/spacious" rel="author" target="_blank" title="ThemeGrill"><span>ThemeGrill</span></a>.</div> <nav class="small-menu clearfix">
</nav>
</div>
</div>
</div>
</footer>
<a href="#masthead" id="scroll-up"></a>
</div><!-- #page -->
<script src="https://www.virgendelmilagro.es/wp-content/plugins/simple-share-buttons-adder/js/ssba.js?ver=5.4.4" type="text/javascript"></script>
<script type="text/javascript">
Main.boot( [] );
</script>
<script src="https://www.virgendelmilagro.es/wp-content/themes/spacious/js/navigation.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.virgendelmilagro.es/wp-content/themes/spacious/js/skip-link-focus-fix.js?ver=5.4.4" type="text/javascript"></script>
<script src="https://www.virgendelmilagro.es/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
