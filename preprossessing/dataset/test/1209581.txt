<!DOCTYPE html>
<html class="no-js mh-one-sb" lang="bg-BG">
<head>
<meta charset="utf-8"/>
<title>НАХРБ – Национален алианс на хора с редки болести</title>
<!--[if lt IE 9]>
<script src="https://rare-bg.com/wp-content/themes/mh_magazine/js/css3-mediaqueries.js"></script>
<![endif]-->
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<link href="https://rare-bg.com/xmlrpc.php" rel="pingback"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://rare-bg.com/?feed=rss2" rel="alternate" title="НАХРБ » Хранилка" type="application/rss+xml"/>
<link href="https://rare-bg.com/?feed=comments-rss2" rel="alternate" title="НАХРБ » Хранилка за коментари" type="application/rss+xml"/>
<link href="https://rare-bg.com/?feed=rss2&amp;page_id=3821" rel="alternate" title="НАХРБ » Хранилка за коментари на НАЧАЛО" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/rare-bg.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://rare-bg.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rare-bg.com/wp-content/plugins/social-share-boost/css/style.css?ver=5.3.6" id="ssb_style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://rare-bg.com/wp-content/themes/mh_magazine/style.css?ver=2.4.2" id="mh-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700&amp;subset=latin,cyrillic" id="mh-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://rare-bg.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://rare-bg.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://rare-bg.com/wp-content/themes/mh_magazine/js/scripts.js?ver=5.3.6" type="text/javascript"></script>
<link href="https://rare-bg.com/index.php?rest_route=/" rel="https://api.w.org/"/>
<link href="https://rare-bg.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://rare-bg.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.3.6" name="generator"/>
<link href="https://rare-bg.com/" rel="canonical"/>
<link href="https://rare-bg.com/" rel="shortlink"/>
<link href="https://rare-bg.com/index.php?rest_route=%2Foembed%2F1.0%2Fembed&amp;url=https%3A%2F%2Frare-bg.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://rare-bg.com/index.php?rest_route=%2Foembed%2F1.0%2Fembed&amp;url=https%3A%2F%2Frare-bg.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<!--/ Facebook Thumb Fixer Open Graph /-->
<meta content="article" property="og:type"/>
<meta content="https://rare-bg.com/" property="og:url"/>
<meta content="НАЧАЛО" property="og:title"/>
<meta content="  " property="og:description"/>
<meta content="НАХРБ" property="og:site_name"/>
<meta content="" property="og:image"/>
<meta content="" property="og:image:alt"/>
<meta content="" property="og:image:width"/>
<meta content="" property="og:image:height"/>
<meta itemscope="" itemtype="article"/>
<meta content="  " itemprop="description"/>
<meta content="" itemprop="image"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="https://rare-bg.com/" name="twitter:url"/>
<meta content="НАЧАЛО" name="twitter:title"/>
<meta content="  " name="twitter:description"/>
<meta content="" name="twitter:image"/>
<style type="text/css">
    	    	    	    		.main-nav, .header-nav .menu .menu-item:hover > .sub-menu, .main-nav .menu .menu-item:hover > .sub-menu, .slide-caption, .spotlight, .carousel-layout1, footer, .loop-layout2 .loop-wrap .meta,
    		.loop-layout3 .loop-wrap .meta, input[type=submit]:hover, #cancel-comment-reply-link:hover, .copyright, #infinite-handle span:hover { background: #1f9ac6; }
    		.slicknav_menu, .slicknav_nav ul { border-color: #1f9ac6; }
    		.copyright, .copyright a { color: #fff; }
    	    	    		.ticker-title, .header-nav .menu-item:hover, .main-nav li:hover, .footer-nav, .footer-nav ul li:hover > ul, .slicknav_menu, .slicknav_btn, .slicknav_nav .slicknav_item:hover,
    		.slicknav_nav a:hover, .slider-layout2 .flex-control-paging li a.flex-active, .sl-caption, .subheading, .pt-layout1 .page-title, .wt-layout2 .widget-title, .wt-layout2 .footer-widget-title,
    		.carousel-layout1 .caption, .page-numbers:hover, .current, .pagelink, a:hover .pagelink, input[type=submit], #cancel-comment-reply-link, .post-tags li:hover, .tagcloud a:hover, .sb-widget .tagcloud a:hover, .footer-widget .tagcloud a:hover, #infinite-handle span { background: #e509b5; }
    		.slide-caption, .mh-mobile .slide-caption, [id*='carousel-'], .wt-layout1 .widget-title, .wt-layout1 .footer-widget-title, .wt-layout3 .widget-title, .wt-layout3 .footer-widget-title,
    		.ab-layout1 .author-box, .cat-desc, textarea:hover, input[type=text]:hover, input[type=email]:hover, input[type=tel]:hover, input[type=url]:hover, blockquote { border-color: #e509b5; }
    		.dropcap, .carousel-layout2 .caption { color: #e509b5; }
    	    	    	    	    	    		.meta, .meta a, .breadcrumb, .breadcrumb a { color: #000000; }
    	    	    	    		</style>
</head>
<body class="home page-template page-template-page-homepage page-template-page-homepage-php page page-id-3821 mh-right-sb wt-layout2 pt-layout1 ab-layout1 rp-layout1 loop-layout3">
<div class="mh-container">
<header class="header-wrap">
<nav class="header-nav clearfix">
<div class="menu-en-menu-container"><ul class="menu" id="menu-en-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-212" id="menu-item-212"><a href="https://rare-bg.com/?page_id=209">Alliance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-380" id="menu-item-380"><a href="https://rare-bg.com/?page_id=378">Mission</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-239" id="menu-item-239"><a href="https://rare-bg.com/?page_id=234">Rare Diseases</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-260" id="menu-item-260"><a href="https://rare-bg.com/?page_id=256">Medical information</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-216" id="menu-item-216"><a href="https://rare-bg.com/?page_id=214">Contact us</a></li>
</ul></div> </nav>
<a href="https://rare-bg.com/" rel="home" title="НАХРБ">
<div class="logo-wrap" role="banner">
<img alt="НАХРБ" height="117" src="https://rare-bg.com/wp-content/uploads/2016/02/HEDAR2.png" width="950"/>
</div>
</a>
<nav class="main-nav clearfix">
<div class="menu-bg-menu-container"><ul class="menu" id="menu-bg-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3817" id="menu-item-3817"><a aria-current="page" href="http://rare-bg.com/">НАЧАЛО</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3844" id="menu-item-3844"><a href="https://rare-bg.com/?cat=5">НОВИНИ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-38" id="menu-item-38"><a href="https://rare-bg.com/?page_id=2">За Алианса</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4100" id="menu-item-4100"><a href="https://rare-bg.com/?page_id=4096">ОРГАНИЗАЦИИ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39" id="menu-item-39"><a href="https://rare-bg.com/?page_id=5">Мисия</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40" id="menu-item-40"><a href="https://rare-bg.com/?page_id=7">Проекти</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-19" id="menu-item-19"><a href="https://rare-bg.com/?page_id=17">Редки болести</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26" id="menu-item-26"><a href="https://rare-bg.com/?page_id=24">Медицинска информация</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27" id="menu-item-27"><a href="https://rare-bg.com/?page_id=22">Живот с рядка болест</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://rare-bg.com/?page_id=20">Списък на заболяванията</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-36" id="menu-item-36"><a href="https://rare-bg.com/?page_id=31">Полезна информация</a>
<ul class="sub-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://rare-bg.com/?page_id=33">Нормативни актове</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3828" id="menu-item-3828"><a href="https://rare-bg.com/?cat=190">ВИДЕО</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37" id="menu-item-37"><a href="https://rare-bg.com/?page_id=9">За контакти</a></li>
</ul></div> </nav>
</header>
<div class="news-ticker clearfix">
<div class="ticker-title">
						ПОСЛЕДНИ НОВИНИ					</div>
<div class="ticker-content">
<ul id="ticker"> <li class="ticker-item">
<a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г.">
<span class="meta ticker-item-meta">
																		02.06.2020 в ВИДЕО: 								</span>
<span class="meta ticker-item-title">
									Наследствен ангиоедем – международен ден 16.05.2020г.								</span>
</a>
</li>
<li class="ticker-item">
<a href="https://rare-bg.com/?p=4679" title="„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем">
<span class="meta ticker-item-meta">
																		02.06.2020 в НОВИНИ: 								</span>
<span class="meta ticker-item-title">
									„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем								</span>
</a>
</li>
<li class="ticker-item">
<a href="https://rare-bg.com/?p=4675" title="29 февруари Международния ден на редките болести в България за 2020 година">
<span class="meta ticker-item-meta">
																		28.02.2020 в НОВИНИ: 								</span>
<span class="meta ticker-item-title">
									29 февруари Международния ден на редките болести в България за 2020 година								</span>
</a>
</li>
<li class="ticker-item">
<a href="https://rare-bg.com/?p=4672" title="Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“">
<span class="meta ticker-item-meta">
																		07.01.2020 в НОВИНИ: 								</span>
<span class="meta ticker-item-title">
									Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“								</span>
</a>
</li>
<li class="ticker-item">
<a href="https://rare-bg.com/?p=4666" title="Ще се борим за признаване на човешките права на хората с редки болести в България">
<span class="meta ticker-item-meta">
																		29.12.2019 в НОВИНИ: 								</span>
<span class="meta ticker-item-title">
									Ще се борим за признаване на човешките права на хората с редки болести в България								</span>
</a>
</li>
</ul>
</div>
</div> <div class="mh-wrapper hp clearfix">
<div class="hp-main">
<div class="hp-columns clearfix">
<div class="hp-content left" id="main-content">
<div class="sb-widget home-2 home-wide"> <article class="spotlight"> <div class="sl-caption">ВИДЕО</div>
<div class="sl-thumb">
<a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г."><img alt="" class="attachment-spotlight size-spotlight wp-post-image" height="326" sizes="(max-width: 580px) 100vw, 580px" src="https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-580x326.jpg" srcset="https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-580x326.jpg 580w, https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-174x98.jpg 174w" width="580"/> </a>
</div>
<a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г."><h2 class="sl-title">Наследствен ангиоедем – международен ден 16.05.2020г.</h2></a>
</article></div><div class="sb-widget home-2 home-wide"><a href="https://www.raredis.org/?lang=bg"><img alt="" class="image wp-image-4621 attachment-large size-large" height="52" sizes="(max-width: 620px) 100vw, 620px" src="https://rare-bg.com/wp-content/uploads/2019/04/ird_bg-620x52.png" srcset="https://rare-bg.com/wp-content/uploads/2019/04/ird_bg-620x52.png 620w, https://rare-bg.com/wp-content/uploads/2019/04/ird_bg-300x25.png 300w" style="max-width: 100%; height: auto;" width="620"/></a></div><div class="sb-widget home-2 home-wide"><h4 class="widget-title"><a class="widget-title-link" href="https://rare-bg.com/?cat=5">НОВИНИ</a></h4> <ul class="cp-widget clearfix"> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4679" title="„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2020/06/image124142142-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2020/06/image124142142-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2020/06/image124142142-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2020/06/image124142142-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4679" title="„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем">„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем</a></h3>
</div>
<div class="mh-excerpt"> „Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен <a href="https://rare-bg.com/?p=4679" title="„Заедно за по-добър живот” е мотото под което  Асоциация НАЕ ще  отбележи   международния ден на хората с рядката диагноза наследствен ангиоедем">[още...]</a></div>
</li> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4675" title="29 февруари Международния ден на редките болести в България за 2020 година"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2020/02/87043215_3041120452586218_1433073751649419264_o-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2020/02/87043215_3041120452586218_1433073751649419264_o-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2020/02/87043215_3041120452586218_1433073751649419264_o-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2020/02/87043215_3041120452586218_1433073751649419264_o-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4675" title="29 февруари Международния ден на редките болести в България за 2020 година">29 февруари Международния ден на редките болести в България за 2020 година</a></h3>
</div>
<div class="mh-excerpt"> От 2008 г. в последния ден на февруари в България отбелязваме Международния ден на редките болести. Тази година слоганът на Международния ден на <a href="https://rare-bg.com/?p=4675" title="29 февруари Международния ден на редките болести в България за 2020 година">[още...]</a></div>
</li> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4672" title="Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2016/10/11046942_794692253899509_5749314924965899686_n-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2016/10/11046942_794692253899509_5749314924965899686_n-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2016/10/11046942_794692253899509_5749314924965899686_n-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2016/10/11046942_794692253899509_5749314924965899686_n-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4672" title="Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“">Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“</a></h3>
</div>
<div class="mh-excerpt">  НАЦИОНАЛЕН АЛИАНС НА ХОРА С РЕДКИ БОЛЕСТИ ОРГАНИЗИРА Обучителен семинар на тема: „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА <a href="https://rare-bg.com/?p=4672" title="Обучителен семинар на тема:  „НАЧИНИ ЗА САМОПОМОЩ, ЗА ПОДОБРЯВАНЕ КАЧЕСТВОТО НА ЖИВОТ НА ХОРАТА С РЕДКИ БОЛЕСТИ И ТЕХНИТЕ СЕМЕЙСТВА“">[още...]</a></div>
</li> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4666" title="Ще се борим за признаване на човешките права на хората с редки болести в България"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2019/12/image.php_-300x225.png" srcset="https://rare-bg.com/wp-content/uploads/2019/12/image.php_-300x225.png 300w, https://rare-bg.com/wp-content/uploads/2019/12/image.php_-174x131.png 174w, https://rare-bg.com/wp-content/uploads/2019/12/image.php_-70x53.png 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4666" title="Ще се борим за признаване на човешките права на хората с редки болести в България">Ще се борим за признаване на човешките права на хората с редки болести в България</a></h3>
</div>
<div class="mh-excerpt"> “Рядкото заболяване превръща теб и семейството ти в герой в битка за оцеляване. Грижа и подкрепа от държавата никаква, фактически сме изолирани”, <a href="https://rare-bg.com/?p=4666" title="Ще се борим за признаване на човешките права на хората с редки болести в България">[още...]</a></div>
</li> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4657" title="Д-р Бойко Пенков: Нека по-често говорим със семействата си за донорството и трансплантацията"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2019/11/81253898598968235989233892523589-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2019/11/81253898598968235989233892523589-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2019/11/81253898598968235989233892523589-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2019/11/81253898598968235989233892523589-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4657" title="Д-р Бойко Пенков: Нека по-често говорим със семействата си за донорството и трансплантацията">Д-р Бойко Пенков: Нека по-често говорим със семействата си за донорството и трансплантацията</a></h3>
</div>
<div class="mh-excerpt"> „Нека по-често говорим със семействата си за донорството и трансплантацията. Донорската карта, която е част от Националната кампания „Да! За <a href="https://rare-bg.com/?p=4657" title="Д-р Бойко Пенков: Нека по-често говорим със семействата си за донорството и трансплантацията">[още...]</a></div>
</li> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4654" title="Семинар по редки болести на тема „Да обединим социалната и здравната грижа“"><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2019/11/PA161705-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2019/11/PA161705-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2019/11/PA161705-620x465.jpg 620w, https://rare-bg.com/wp-content/uploads/2019/11/PA161705-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2019/11/PA161705-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4654" title="Семинар по редки болести на тема „Да обединим социалната и здравната грижа“">Семинар по редки болести на тема „Да обединим социалната и здравната грижа“</a></h3>
</div>
<div class="mh-excerpt"> На 16.10.2019 г в град Русе се проведе се проведе обучителен семинар по редки болести на тема Да обединим социалната и здравната грижа. Той се проведе в <a href="https://rare-bg.com/?p=4654" title="Семинар по редки болести на тема „Да обединим социалната и здравната грижа“">[още...]</a></div>
</li> </ul></div> </div>
<div class="mh-sidebar hp-sidebar hp-home-6">
<div class="sb-widget home-6"><h4 class="widget-title">ТЪРСЕНЕ В САЙТА</h4><form action="https://rare-bg.com/" id="searchform" method="get" role="search">
<fieldset>
<input id="s" name="s" onfocus="if (this.value == 'Въведете текст за търсене и натиснете ентер') this.value = ''" type="text" value="Въведете текст за търсене и натиснете ентер"/>
<input id="searchsubmit" type="submit" value=""/>
</fieldset>
</form></div><div class="sb-widget home-6"><h4 class="widget-title">Българска Хънтингтън Асоциация</h4><a href="https://bulgariadariava.bg/cause/darete-grizha-na-hora-s-redki-bolesti/"><img alt="" class="image wp-image-4608 attachment-medium size-medium" height="300" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2019/03/55764344_2305811056145665_7930497137417125888_n-300x300.png" srcset="https://rare-bg.com/wp-content/uploads/2019/03/55764344_2305811056145665_7930497137417125888_n-300x300.png 300w, https://rare-bg.com/wp-content/uploads/2019/03/55764344_2305811056145665_7930497137417125888_n-150x150.png 150w, https://rare-bg.com/wp-content/uploads/2019/03/55764344_2305811056145665_7930497137417125888_n-620x620.png 620w, https://rare-bg.com/wp-content/uploads/2019/03/55764344_2305811056145665_7930497137417125888_n.png 960w" style="max-width: 100%; height: auto;" width="300"/></a></div><div class="sb-widget home-6"><h4 class="widget-title">СЛЕДВАЙТЕ ВЪВ FACEBOOK</h4><div class="fb-page" data-height="500" data-hide-cover="0" data-href="https://www.facebook.com/%D0%9D%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%D0%B5%D0%BD-%D0%90%D0%BB%D0%B8%D0%B0%D0%BD%D1%81-%D0%BD%D0%B0-%D1%85%D0%BE%D1%80%D0%B0-%D1%81-%D1%80%D0%B5%D0%B4%D0%BA%D0%B8-%D0%B1%D0%BE%D0%BB%D0%B5%D1%81%D1%82%D0%B8-110013265700748/" data-show-facepile="1" data-show-posts="1" data-width="300"></div>
</div><div class="sb-widget home-6"><div class="ad-widget ad-sb clearfix">
<div class="ad-item ad1"><a href="http://rarediseaseday.org" target="_new"><img src="http://rare-bg.com/wp-content/baners/rdd-logo-transparent-small.png"/></a></div>
<div class="ad-item ad2"><a href="http://eurordis.org" target="_new"><img src="http://rare-bg.com/wp-content/baners/Eurordis125.png"/></a>
<br/></div>
</div>
</div><div class="sb-widget home-6"> <div class="textwidget"><a href="http://rareconnect.org/en" rel="noopener noreferrer" target="_new"><img src="http://rare-bg.com/wp-content/baners/rare_c.jpg"/></a>
</div>
</div><div class="sb-widget home-6"><h4 class="widget-title"><a class="widget-title-link" href="https://rare-bg.com/?cat=190">ВИДЕО</a></h4> <ul class="cp-widget clearfix"> <li class="cp-wrap cp-large clearfix">
<div class="cp-thumb-xl"><a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г."><img alt="" class="attachment-cp_large size-cp_large wp-post-image" height="225" sizes="(max-width: 300px) 100vw, 300px" src="https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-300x225.jpg" srcset="https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-300x225.jpg 300w, https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2020/06/57273904729017491290-70x53.jpg 70w" width="300"/></a></div>
<div class="cp-data">
<h3 class="cp-xl-title"><a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г.">Наследствен ангиоедем – международен ден 16.05.2020г.</a></h3>
</div>
<div class="mh-excerpt">  <a href="https://rare-bg.com/?p=4682" title="Наследствен ангиоедем – международен ден 16.05.2020г.">[още...]</a></div>
</li> <li class="cp-wrap cp-small clearfix">
<div class="cp-thumb"><a href="https://rare-bg.com/?p=4634" title="В Киев приехал глава Болгарской ассоциации больных легочной гипертензией"><img alt="" class="attachment-cp_small size-cp_small wp-post-image" height="53" sizes="(max-width: 70px) 100vw, 70px" src="https://rare-bg.com/wp-content/uploads/2019/06/3049690349234900697069234-70x53.jpg" srcset="https://rare-bg.com/wp-content/uploads/2019/06/3049690349234900697069234-70x53.jpg 70w, https://rare-bg.com/wp-content/uploads/2019/06/3049690349234900697069234-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2019/06/3049690349234900697069234-300x225.jpg 300w" width="70"/></a></div>
<div class="cp-data">
<p class="cp-widget-title"><a href="https://rare-bg.com/?p=4634" title="В Киев приехал глава Болгарской ассоциации больных легочной гипертензией">В Киев приехал глава Болгарской ассоциации больных легочной гипертензией</a></p>
</div>
</li> <li class="cp-wrap cp-small clearfix">
<div class="cp-thumb"><a href="https://rare-bg.com/?p=4614" title="Репортаж: Защо НЗОК не може да поеме лечението на пациенти с редки болести"><img alt="" class="attachment-cp_small size-cp_small wp-post-image" height="53" sizes="(max-width: 70px) 100vw, 70px" src="https://rare-bg.com/wp-content/uploads/2019/03/23486902375838905238900-70x53.jpg" srcset="https://rare-bg.com/wp-content/uploads/2019/03/23486902375838905238900-70x53.jpg 70w, https://rare-bg.com/wp-content/uploads/2019/03/23486902375838905238900-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2019/03/23486902375838905238900-300x225.jpg 300w" width="70"/></a></div>
<div class="cp-data">
<p class="cp-widget-title"><a href="https://rare-bg.com/?p=4614" title="Репортаж: Защо НЗОК не може да поеме лечението на пациенти с редки болести">Репортаж: Защо НЗОК не може да поеме лечението на пациенти с редки болести</a></p>
</div>
</li> <li class="cp-wrap cp-small clearfix">
<div class="cp-thumb"><a href="https://rare-bg.com/?p=4569" title="Международния ден на редките болести 28 февруари 2019г."><img alt="" class="attachment-cp_small size-cp_small wp-post-image" height="53" sizes="(max-width: 70px) 100vw, 70px" src="https://rare-bg.com/wp-content/uploads/2019/03/53459168_2402949613090627_1609880285162242048_n-70x53.jpg" srcset="https://rare-bg.com/wp-content/uploads/2019/03/53459168_2402949613090627_1609880285162242048_n-70x53.jpg 70w, https://rare-bg.com/wp-content/uploads/2019/03/53459168_2402949613090627_1609880285162242048_n-528x400.jpg 528w, https://rare-bg.com/wp-content/uploads/2019/03/53459168_2402949613090627_1609880285162242048_n-174x131.jpg 174w, https://rare-bg.com/wp-content/uploads/2019/03/53459168_2402949613090627_1609880285162242048_n-300x225.jpg 300w" width="70"/></a></div>
<div class="cp-data">
<p class="cp-widget-title"><a href="https://rare-bg.com/?p=4569" title="Международния ден на редките болести 28 февруари 2019г.">Международния ден на редките болести 28 февруари 2019г.</a></p>
</div>
</li> </ul></div> </div>
</div>
</div>
</div>
<div class="copyright-wrap">
<p class="copyright">НАХРБ 2016</p>
</div>
</div>
<div id="fb-root"></div>
<script>
				(function(d, s, id){
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v2.3";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script> <script type="text/javascript">
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/platform.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>
<script src="https://rare-bg.com/wp-includes/js/comment-reply.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="https://rare-bg.com/wp-includes/js/wp-embed.min.js?ver=5.3.6" type="text/javascript"></script>
<script src="//assets.pinterest.com/js/pinit.js?ver=1.0" type="text/javascript"></script>
</body>
</html>