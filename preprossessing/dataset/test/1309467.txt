<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>404 Page not found - Sharedspace</title>
<link href="/templates/system/css/error.css" rel="stylesheet" type="text/css"/>
<link href="/templates/system/css/offline.css?234" rel="stylesheet" type="text/css"/>
<link href="https://www.sharedspace.co.nz/templates/sharedspace/css/template.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    @font-face {
    font-family: 'OpensRegular';
    src: url('fonts/OpenSans-Light-webfont.eot');
    src: url('fonts/OpenSans-Light-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/OpenSans-Light-webfont.woff') format('woff'),
         url('fonts/OpenSans-Light-webfont.ttf') format('truetype'),
         url('fonts/OpenSans-Light-webfont.svg#LatoRegular') format('svg');
    font-weight: 100;
    font-style: normal;
    }
    @font-face {
    font-family: 'OpenSansBold';
    src: url('fonts/OpenSans-Regular-webfont.eot');
    src: url('fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('fonts/OpenSans-Regular-webfont.woff') format('woff'),
         url('fonts/OpenSans-Regular-webfont.ttf') format('truetype'),
         url('fonts/OpenSans-Regular-webfont.svg#LatoRegular') format('svg');
    font-weight: normal;
    font-style: normal;
    }
</style>
<style type="text/css">body {background-color:white} h1{font-family: 'OpenSansBold', sans-serif; font-size: 50px;color:#CCC;margin-bottom: 0;margin-top: 0;} p {font-family: 'OpenSansRegular', sans-serif;font-size: 26px;color:#999;margin-top: 20px;} img.homepage{margin-top: 25px;} </style>
</head>
<body style="margin-top:0;">
<div id="offlineMain">
<div id="innerOffline">
<img align="middle" alt="SharedSpace 404 Error" src="/images/errors/sharedspace-square-logo.jpg" style="position: relative;"/>
<h1>Fudgesticks.</h1>
<br/>
<p>It appears we can't find that page (404 Error)</p>
<br/>
<p>
<a class="btn btn-primary" href="https://www.sharedspace.co.nz/">
                    Back to the homepage
                </a>
</p>
</div>
</div>
</body>
</html>
