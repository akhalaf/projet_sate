<!DOCTYPE html>
<html lang="ru">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="text/javascript" http-equiv="Content-Script-Type"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title>Магазин пряжи, пряжа для вязания - Магазин Анже</title>
<link href="/i/aj/blocks/p-root/img/favicon.ico" rel="shortcut icon"/>
<!-- Viewport for mobile devices -->
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<!-- Font Roboto -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700;subset=cyrillic&amp;display=swap" rel="stylesheet"/>
<!-- Font Awesome -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css" rel="stylesheet"/>
<!-- Bootstrap 4 -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Jquery -->
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<!-- Site CSS -->
<link href="/i/aj/all.css?1605988383" rel="stylesheet"/>
<!-- Site JS -->
<script src="/i/aj/all.js?1608855114"></script>
<!-- BEGIN Google Tag/AdWords -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=AW-980607107"></script>
<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'AW-980607107');
		gtag('config', 'UA-42056939-1');
	</script>
<!-- END Google Tag/AdWords -->
<!-- BEGIN Google Tag/Home Page -->
<script>
		gtag('event', 'page_view', {
			'send_to':			'AW-980607107',
			'ecomm_pagetype':	'home'
		});
	</script>
<!-- END Google Tag/Home Page -->
<!-- BEGIN Facebook Pixel Code -->
<script>
		// Facebook Pixel
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '486326101872720');
		fbq('track', 'PageView');
	</script>
<noscript><img height="1" src="https://www.facebook.com/tr?id=486326101872720&amp;ev=PageView&amp;noscript=1" style="display:none" width="1"/></noscript>
<!-- END Facebook Pixel Code -->
<!-- BEGIN Open Graph -->
<meta content="https://anje.com.ua/i/aj/blocks/p-root/img/logo-2.png" property="og:image"/>
<!-- END Open Graph -->
</head>
<body>
<!-- Mobile menu -->
<div class="d-sm-block d-md-block d-lg-none">
<div class="b-mobmenu2-overlay b-mobmenu2-overlay__hidden"></div>
<div class="b-mobmenu2">
<div class="b-mobmenu2-header">
<div class="b-mobmenu2-header-toggler b-mobmenu2-icon">
<i class="fas fa-bars"></i>
</div>
<div class="b-mobmenu2-logo b-mobmenu2-icon">
<a href="/"><img height="29px" src="/i/aj/blocks/p-root/img/logo-2.png"/></a>
</div>
<a class="b-mobmenu2-icon b-mobmenu2-icon-right" href="/help/contacts/" style="color: #ddd">
<i class="fas fa-map-marker-alt"></i>
</a>
<a class="b-mobmenu2-icon b-mobmenu2-icon-right fas fa-shopping-cart" href="/cart/" style="padding-right: 20px; color: #ccc">
</a>
</div>
<div class="b-mobmenu2-popup b-mobmenu2-popup-hidden">
<div class="b-mobmenu2-popup-header">
<div class="b-mobmenu2-popup-header-logo">
<img height="40px" src="/i/aj/blocks/p-root/img/logo-2.png"/>
</div>
<div class="b-mobmenu2-popup-header-login">
<a href="/user/login/">Вход</a>  |  <a href="/user/registration/">Регистрация</a>
</div>
</div>
<div class="b-mobmenu2-menu">
<a class="b-mobmenu2-menu-item" href="/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-home"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Главная
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/yarn/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-bullseye"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Пряжа
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/yarn-spici/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-bullseye"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Спицы
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/yarn-kruchki/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-bullseye"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Крючки
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/yarn-accesory/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-bullseye"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Аксессуары
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/book/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-bullseye"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Книги по вязанию
						</div>
</a>
<div class="b-mobmenu2-menu-delimeter">
</div>
<a class="b-mobmenu2-menu-item" href="/help/contacts/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fas fa-map-marker-alt"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							Контакты
						</div>
</a>
<a class="b-mobmenu2-menu-item" href="/help/">
<div class="b-mobmenu2-menu-item-icon">
<i class="fab fa-forumbee"></i>
</div>
<div class="b-mobmenu2-menu-item-text">
							О нас
						</div>
</a>
</div>
</div>
</div>
<div style="height: 40px;"></div>
</div>
<div class="p-root">
<div class="container-fluid d-lg-block d-none">
<div class="p-root-top row">
<div class="container">
<div class="row">
<div class="p-root-top__block col">
<address>
<a class="p-root__top-link" href="/help/contacts/" style="padding-left: 0">
								Украина, Киев   <span class="p-root__top-grey">м. Левобережная ул. Евгения Сверстюка 17, 8 этаж</span>
</a>
</address>
<a href="/help/delivery/" style="margin-left: auto"><i class="fas fa-bicycle p-root-top__block-icon"></i>Доставка</a>
<a href="/help/pay/"><i class="fas fa-money-check p-root-top__block-icon"></i>Оплата</a>
<a href="/help/return/"><i class="fas fa-undo-alt p-root-top__block-icon"></i>Возврат товара</a>
<a href="/help/contacts/" style="padding-right: 0"><i class="fas fa-map-marker-alt p-root-top__block-icon"></i>Контакты</a>
</div>
</div>
</div>
</div>
</div>
<div class="container d-lg-block d-none">
<header class="row">
<div class="p-root-header col">
<a class="p-root-header__left" href="/">
<img src="/i/aj/blocks/p-root/img/logo-2.png"/>
</a>
<div class="p-root-header__right">
<div class="p-root-header__block" style="display: block">
<a href="/ua/" style="font-style: monospace; border: 1px solid #ccc; padding: 4px; border-radius: 4px; color: #333" title="Українська">UA<!-- &ndash; Українська--></a>
						    
									<a href="/it/" style="font-style: monospace; border: 1px solid #ccc; padding: 4px; border-radius: 4px; color: #333" title="Italiana">IT<!-- &ndash; Italiana--></a>
						    
									<a href="/" style="font-style: monospace; border: 2px solid #93007b; padding: 4px; border-radius: 4px; color: #666; background: #e0e0e" title="Русский">RU<!-- &ndash; Русский--></a>
						    
									<a href="/en/" style="font-style: monospace; border: 1px solid #ccc; padding: 4px; border-radius: 4px; color: #333" title="English">EN<!-- &ndash; English--></a>
						    
									<a href="/de/" style="font-style: monospace; border: 1px solid #ccc; padding: 4px; border-radius: 4px; color: #333" title="Deutsche">DE<!-- &ndash; Deutsche--></a>
						    
		
	</div>
<div class="p-root-header__block" style="display: block">
<a href="tel:+380958165242"><i class="fas fa-phone p-root-header__block-icon"></i>+38 (095) 816-52-42</a>
						  
						<a href="tel:+380964862828">+38 (096) 486-28-28</a>
</div>
<div class="p-root-header__block">
<a href="/user/login/"><i class="fas fa-sign-in-alt p-root-header__block-icon"></i>Вход / Регистрация</a>
</div>
<a class="p-root-header__block p-root-header__cart" href="/cart/" style="padding-right: 0">
<div class="p-root__header-block-cart-text">
<div class="p-root__header-block-cart-text-top">
								Сумма заказа
							</div>
<div class="p-root__header-block-cart-text-bottom ">
<span class="p-root__header-order-sum">0.00</span>
<sub>грн</sub>
</div>
</div>
<img src="/i/aj/blocks/p-root/img/cart.png" style="height: 55px"/>
</a>
</div>
</div>
</header>
</div>
<div class="container-fluid d-lg-block d-none">
<div class="p-root-menu-wrapper row">
<div class="container">
<div class="row">
<div class="col p-root-menu">
<a class="p-root-menu__item" href="/">
<i class="fas fa-grip-horizontal" style="padding-right: 10px"></i>Главная
						</a>
<a class="p-root-menu__item" href="/med-mask/">
									Маски защитные
								</a>
<a class="p-root-menu__item" href="/yarn/">
									Пряжа
								</a>
<a class="p-root-menu__item" href="/yarn-spici/">
									Спицы
								</a>
<a class="p-root-menu__item" href="/yarn-kruchki/">
									Крючки
								</a>
<a class="p-root-menu__item" href="/yarn-accesory/">
									Аксессуары
								</a>
<a class="p-root-menu__item" href="/book/">
									Книги
								</a>
<a class="p-root-menu__item" href="/forum/">
							Форум
						</a>
<a class="p-root-menu__item" href="/cart/" style="padding-right: 0; margin-left: auto">
							Корзина						</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="margin-top: 15px"></div>
<div class="p-root side-collapse-container">
<div class="container">
<div class="row">
<div class="col-3 d-lg-block d-none">
<div class="p-root__left">
<div class="p-root__leftmenu">
<ul class="p-root__leftmenu-catalog">
<li class="p-root__leftmenu-catalog-level1">Вязание</li>
<li class="p-root__leftmenu-catalog-levelx">
<a href="/med-mask/">Маски защитные</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								3
							</span>
</li>
<li class="p-root__leftmenu-catalog-levelx">
<a href="/yarn/">Пряжа</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								396
							</span>
</li>
<li class="p-root__leftmenu-catalog-levelx">
<a href="/yarn-spici/">Спицы</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								222
							</span>
</li>
<li class="p-root__leftmenu-catalog-levelx">
<a href="/yarn-kruchki/">Крючки</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								48
							</span>
</li>
<li class="p-root__leftmenu-catalog-levelx">
<a href="/yarn-accesory/">Аксессуары</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								139
							</span>
</li>
<li class="p-root__leftmenu-catalog-levelx p-root__leftmenu-catalog-levelx_last">
<a href="/book/">Книги</a>
<span class="p-root__leftmenu-catalog-levelx_grey">
								5
							</span>
</li>
</ul>
</div>
<!--
		<a href="https://www.instagram.com/anje_yarn/" style="display:block; margin-top: 25px; margin-bottom: 20px; padding-left: 20px" target="_blank">
			<img src="https://cdn.vectorlogosupply.com/logos/large/2x/instagram-2016-png-transparent-logo.png" style="height: 23px; vertical-align: middle; padding-right: 5px">Инстаграм @anje_yarn
		</a>
-->
<div style="margin-top: 25px"></div>
<div style="margin-top: 5px; margin-bottom: 10px; padding-left: 20px; font-size: 30px; opacity: .6">
<a href="https://anje.com.ua/forum/" target="_blank"><img src="https://anje.com.ua/forum/styles/default/custom/og-03.png" style="height: 39px; vertical-align: middle; padding-right: 35px"/></a>
<a href="https://www.youtube.com/channel/UCX2CH1zpIA767Cl86q29rpA" target="_blank"><i class="fab fa-youtube" style="color: #93007b; padding-right: 5px"></i></a>
<a href="https://www.instagram.com/anje_yarn/" target="_blank"><i class="fab fa-instagram" style="color: #93007b; padding-right: 5px"></i></a>
<a href="https://www.facebook.com/anje.yarn/" target="_blank"><i class="fab fa-facebook" style="color: #93007b; padding-right: 5px"></i></a>
<a href="https://g.page/anje_yarn?gm" target="_blank"><i class="fab fa-google" style="color: #93007b; padding-right: 5px"></i></a>
</div>
<div class="p-root__leftmenu-news">
<div class="p-root__leftmenu-news-head">
<a class="p-root__leftmenu-news-link" href="/news/">
					Новинки на складе
					<img src="/i/aj/blocks/p-root/img/emoji/flower2.png" style="max-height: 23px; float: right"/>
</a>
</div>
<ul>
<li class="p-root__leftmenu-news-date">
						2021-01-14
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-accesory/ruchnaya-rabota/birka-prishivnaya-3-5-sm-1-5-sm-handmade-kozha-dzhins-129918/">
								Ручная работа  – Бирка пришивная 3.5 см *1.5 см “Handmade” кожа джинс
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-accesory/ruchnaya-rabota/pompony-dlya-shapok-pesec-seryyi-dlina-meha-18-sm-118307/">
								Ручная работа  – Помпоны для шапок песец  Серый длина меха 18 см
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-13
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/krugovye-spicy-na-trose-100-sm-1-50-mm-i-v-podarok-igolka-162300/">
								Rundsticknadel – Круговые спицы на тросе 100 см 1.50 мм и в подарок иголка
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/krugovye-spicy-na-trose-100-sm-1-80-mm-i-v-podarok-igolka-162299/">
								Rundsticknadel – Круговые спицы на тросе 100 см 1.80 мм и в подарок иголка
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/krugovye-spicy-na-trose-100-sm-10-00-mm-162297/">
								Rundsticknadel – Круговые спицы на тросе 100 см 10.00 мм 
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/krugovye-spicy-na-trose-100-sm-2-00-mm-i-v-podarok-igolka-162296/">
								Rundsticknadel – Круговые спицы на тросе 100 см 2.00 мм  и в подарок иголка
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/krugovye-spicy-na-trose-100-sm-9-00-mm-i-v-podarok-igolka-162298/">
								Rundsticknadel – Круговые спицы на тросе 100 см 9.00 мм и в подарок иголка
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-accesory/yarnart/markery-bulavki-biryuza-dlya-vyazaniya-10-sht--131553/">
								Yarnart – Маркеры (булавки) бирюза  для вязания 10 шт. 
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/zegna-baruffa/cashwool-230-1500-m-akciya-16235/">
								Zegna Baruffa – CASHWOOL 2/30   1500 м  Акция 
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-accesory/ruchnaya-rabota/birka-na-vinte-handmade-rozovaya-pudra-166923/">
								Ручная работа  – Бирка на винте “Handmade” розовая пудра
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-12
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/loro-piana/kashemir-s-shelkom-cash-seta-2300-m-165140/">
								Loro Piana – Кашемир с шёлком CASH-SETA 2300 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/loro-piana/shnurochek-cordonetto-550-m-165061/">
								Loro Piana – Шнурочек CORDONETTO 550 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/manifattura-sesia/merinos-mistral-350-m-142868/">
								MANIFATTURA ... – Merinos  Mistral 350 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/polipeli-s-p-a-/susuri-sherst-alpaki-i-merinosa-214-dlina-pryazhi-700-m-163348/">
								POLIPELI S.P.A. – Susuri  шерсть альпаки и мериноса  2/14  длина пряжи 700 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/sdk-spicy-metallicheskie-na-trose-40-cm-10-00-mm-rundstricknadel-165897/">
								Rundsticknadel – SDK Спицы металлические на тросе 40 cm 10.00 mm Rundstricknadel
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/safil/polusherst-tender-228-1400-m-136864/">
								Safil – Полушерсть  TENDER 2/28  1400 м
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-11
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn-spici/rundsticknadel/spicy-metallicheskie-krugovye-na-leske-100-sm-4-00-mm-rundstricknadel-165898/">
								Rundsticknadel – Спицы металлические круговые на леске 100 см 4.00 мм Rundstricknadel
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-10
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/zegna-baruffa/cashwool-230-novinka-1500-m-52475/">
								Zegna Baruffa – CASHWOOL 2/30  новинка 1500 м
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-09
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/lagopolane/polusherst-art-059-dlina-pryazhi-80-m-akciya-123668/">
								LAGOPOLANE – Полушерсть ART 059  длина пряжи 80 м Акция
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/cofil/teddy-170-m-akciya-83876/">
								COFIL – TEDDY  170 м Акция 
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/emilcotoni-spa/hlopok-sea-island-5000-m-super-akciya-26653/">
								EMILCOTONI SPA – Хлопок SEA ISLAND 5000 м Супер акция 
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/fabifil-srl/biwa-350-m-166152/">
								FABIFIL SRL – Biwa 350 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/filati-srl/super-denilson-340-m-140327/">
								FILATI SRL – Super Denilson 340 м
							</a>
</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/touch/jut-kendir-85-m-159040/">
								TOUCH – JUT KENDIR 85 м
							</a>
</li>
<li class="p-root__leftmenu-news-date">
						2021-01-08
					</li>
<li class="p-root__leftmenu-news-item">
<a class="p-root__leftmenu-news-link" href="/yarn/lineapiu-italia-s-p-a/yak-1150-m-84615/">
								LINEAPIU ITA... – YAK 1150 м
							</a>
</li>
</ul>
</div>
</div>
</div>
<div class="col">
<div class="p-root__content">
<div>
<h1 class="p-root-h1" style="margin-bottom: 30px">
<a class="p-root__main_h1link" href="/help/contacts/"><span class="p-root__main_h1block"> <span style="color: #ffd70c">А</span><span style="color: #a2ce3a">н</span><span style="color: #01b3bd">ж</span><span style="color: #e22692">е</span> </span></a>
<span style="color: #666"> </span>
<a class="p-root__main_h1link" href="/yarn/"><span class="p-root__main_h1block"> Магазин пряжи </span></a>
</h1>
<div class="p-root__main-block">
<div class="d-lg-block d-none">
<h2 class="p-root__main-h2">
			О магазине
		</h2>
<p class="p-root__main-link">
			
				Магазин пряжи <a href="/">«Анже»</a> предоставляет большой ассортимент пряжи по доступным ценам.
				Так же есть широкий выбор <a href="/yarn-spici/">спиц</a>, <a href="/yarn-kruchki/">крючков</a> и <a href="/yarn-accesory/">аксессуаров</a> для вязания.
				У нас можно <a href="/yarn/">купить пряжу</a> как итальянскую так и турецкую.
				Каждую неделю доставляем самые интересные новинки из Италии <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/250px-Flag_of_Italy.svg.png" style="height: 12px"/>
				Мы работаем для Вас уже более 10 лет <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Heart_coraz%C3%B3n.svg/193px-Heart_coraz%C3%B3n.svg.png" style="height: 14px"/>
</p>
</div>
</div>
<div class="p-root__main-banner" style="font-size: 16px; background: rgba(147, 0, 123, .08); padding-bottom: 4px; margin-bottom: 20px">
<span style="color: red">Выходные дни</span>: 9 и 10 января 2021 магазин на <a href="/help/contacts/">Евгения Сверстюка</a> не работает на выдачу самовывозов. Заказы через корзину принимаются круглосуточно.
</div>
<!--
<div class="p-root__main-banner" style="background: rgba(147, 0, 123, .08); padding-bottom: 4px; margin-bottom: 20px">
    <p style="font-size: 16px; padding-bottom: 10px">
		Дорогие киевляне. В субботу магазин работает в режиме точки выдачи заказов. Пожалуйста оформляйте заказ на сайте и забирайте его по адресу <a href="/help/contacts/">ул. Евгения Сверстюка 17</a>

		<br><br>
		<span style="font-size:12px">* Ограничение посещения магазина введено в соответствии с так называемым "Карантином выходного дня". Распоряжение Кабмина №1100 от 12 нобрября.</span>
    </p>
</div>
-->
<div class="p-root__main-block">
<h2 class="p-root__main-h2">
		Пряжа по цветам
	</h2>
<div class="b-colorpick">
<a class="b-colorpick__item" href="/yarn/filter/j-2/">
<div class="b-colorpick__color" style="background: #f5f5dc" title="Бежевый 102">
</div>
			  Бежевый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-3/">
<div class="b-colorpick__color" style="background: #f0f0f0" title="Белый 81">
</div>
			  Белый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-4/">
<div class="b-colorpick__color" style="background: #00bfff" title="Голубой 97">
</div>
			  Голубой
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-5/">
<div class="b-colorpick__color" style="background: #ffd800" title="Желтый 70">
</div>
			  Желтый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-6/">
<div class="b-colorpick__color" style="background: #03c03c" title="Зеленый 153">
</div>
			  Зеленый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-7/">
<div class="b-colorpick__color" style="background: #654321" title="Коричневый 106">
</div>
			  Коричневый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-8/">
<div class="b-colorpick__color" style="background: #cc0000" title="Красный 136">
</div>
			  Красный
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-9/">
<div class="b-colorpick__color" style="background: #ff4f00" title="Оранжевый 68">
</div>
			  Оранжевый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-10/">
<div class="b-colorpick__color" style="background: #e75480" title="Розовый 145">
</div>
			  Розовый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-11/">
<div class="b-colorpick__color" style="background: #808080" title="Серый 114">
</div>
			  Серый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-12/">
<div class="b-colorpick__color" style="background: #00008b" title="Синий 153">
</div>
			  Синий
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-13/">
<div class="b-colorpick__color" style="background: #991199" title="Фиолетовый 62">
</div>
			  Фиолетовый
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-14/">
<div class="b-colorpick__color" style="background: #1b1b1b" title="Черный 129">
</div>
			  Черный
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-15/">
<div class="b-colorpick__color" style="background: linear-gradient(red, orange, yellow, green, blue, indigo, #7f00ff)" title="Цветной 75">
</div>
			  Цветной
		</a>
<a class="b-colorpick__item" href="/yarn/filter/j-1/">
<div class="b-colorpick__color" style="background: #ffffff" title="Нет цвета 14">
</div>
			  Нет цвета
		</a>
</div>
</div>
<div class="p-root__main-block">
<h2 class="p-root__main-h2">
		Пряжа по составу
	</h2>
<div class="p-root__structure">
<a class="p-root__structure-item p-root__structure-item-first" href="/yarn/">
<i class="fas fa-arrow-alt-circle-right"></i>
			Вся пряжа
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-23/">
<i class="fas fa-bullseye"></i>
			Альпака
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-22/">
<i class="fas fa-bullseye"></i>
			Ангора
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-20-27/">
<i class="fas fa-bullseye"></i>
			Кашемир
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-11/">
<i class="fas fa-bullseye"></i>
			Лён
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-24-17/">
<i class="fas fa-bullseye"></i>
			Мохер
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-28/">
<i class="fas fa-bullseye"></i>
			Меринос
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-8/">
<i class="fas fa-bullseye"></i>
			Полушерстяная
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-21-26-19-18/">
<i class="fas fa-bullseye"></i>
			Искусственная
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-10-9/">
<i class="fas fa-bullseye"></i>
			Хлопок
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-7/">
<i class="fas fa-bullseye"></i>
			Шерсть
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-31/">
<i class="fas fa-bullseye"></i>
			Твид
		</a>
<a class="p-root__structure-item" href="/yarn/filter/t-13-29-15-30/">
<i class="fas fa-angle-double-right"></i>
			Другая...
		</a>
</div>
</div>
<div class="p-root__main-block">
<div class="p-root__main-banner d-lg-block d-none" style="margin-top: 10px; background: #ffffef; box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.04), 0 2px 8px 0 rgba(0, 0, 0, 0.20);">
		Нужный <a href="/help/cone2ball/">отмот</a> пряжи пишите пожалуйста в комментарии к заказу.
	</div>
</div>
<br/>
<!--
<img src="/i/aj/blocks/p-root/img/tmp/ny-2020.jpg" style="width: 100%; border-radius: 5px 5px 5px 55px;">
<img src="/i/aj/blocks/p-root/img/tmp/ny-2020-2.gif" style="width: 100%">
-->
<br/>
<!--
	<div class="d-lg-block d-none">
		<br>
	</div>
-->
<div class="p-root__main-block">
<div class="d-lg-block d-none">
<h2 class="p-root__main-h2">
			Итальянская бобинная пряжа
		</h2>
<p>
			
				Особое внимание уделяем выбору стоковой <a href="/yarn/filter/d-6/">бобиной пряжи</a>.
				Несколько раз в год ездим на фабрики в Италию и отбираем лучшие и уникальные составы пряжи.
				Затем пряжа приезжает в Украину, в Киев, разматывается на небольшие моточки и поступает в продажу.
			
		</p>
<p>
			
				Италия – общепризнаный мировой лидер в производстве пряжи. И не только потому, что итальянцы имеют хороший вкус и делают интересную покраску пряжи.
				Но и потому, что тщательно относятся к закупке высококачественного сырья для производства пряжи. И умеют делать эксклюзивные смесовки из разных составов пряжи.
			
		</p>
<!--
		<h2 class="p-root__main-h2">
			Остатки пряжи после отшива коллекций
		</h2>
		<p>
			
				В отличии от регулярной моточной пряжи, где из года в год одни и те же составы, в бобиной пряже всегда что-то новое и вкусное.
				Кроме того, бобинная пряжа произведенная на Итальянских фабриках отвечает всем современным тенденциям высокой моды.
				Ведь только в Италии производятся коллекции известных Домов Мод первой линии и от кутюр.
				Как правило, при создании коллекции, дизайнеры берут не готовую пряжу с фабрик, а заказывают партию под свою уникальную покраску, состав, толщину нити и т.д.
				После отшива коллекции, на фабриках всегда остаются остатки произведенной партии. Эти остатки мы и выкупаем у фабрик.
			
		</p>
-->
<p>
			
				Мы работаем как с небольшими мануфактурами, так и с лидерами индустрии премиум сегмента.
				Например: <a href="/yarn/filter/m-54/">Loro Piana</a>, <a href="/yarn/filter/m-55/">Zegna Baruffa</a>, <a href="/yarn/filter/m-135/">LANA GATTO</a>,
				<a href="/yarn/filter/m-53/">Cariaggi</a> и другими.
			
		</p>
<h2 class="p-root__main-h2">
			Доставка
		</h2>
<p>
			
				Заказы свыше <b>599</b> грн доставляются бесплатно за наш счет. <a href="/help/delivery/">Доставка</a> осуществляется по всей Украине «Новой Почтой» и «Укрпочтой».
				Если Вы находитесь в Киеве, мы можем доставить курьером. Или же ждем Вас в гости в магазин. Находимся мы возле <a href="/help/contacts/">м. Левобережная</a>
				Все представленная продукция на сайте имеется в наличии и в магазине.<br/><br/>
				Не забудте вовремя забрать посылку с Новой Почты. Учтите, что на складе посылка храниться всего 5 дней, после чего отправляется назад.
			
		</p>
<h2 class="p-root__main-h2">
			Общение

		</h2>
<div class="p-root__main-banner" onclick="window.open('/forum/');" style="background: #185886; color: #eee; border-radius: 2px; cursor: pointer; box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.04), 0 4px 8px 0 rgba(0, 0, 0, 0.20);">
<b>Клуб интересов</b><br/><br/>
			
				Форум вязания. Продажа изделий, ищем заказчиков и мастеров. Адрес форума → anje.com.ua/forum<br/><br/>
<a color:="" href="https://anje.com.ua/forum/" style="font-size: 18px; style=" target="_blank">
<img src="https://anje.com.ua/forum/styles/default/custom/logo-03.png"/><br/>
				anje.com.ua/forum
			</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div>
<div class="p-root__footer">
<div class="p-root__footer-left">
</div>
<div class="p-root__footer-copyright" style="color:#999">
<a href="/help/contacts/">Контакты</a>
		  ¦  
		<a href="/help/">О нас</a>
		  ¦  
		<a href="/help/delivery/">Доставка</a>
		  ¦  
		<a href="/help/pay/">Оплата</a>
		  ¦  
		<a href="/help/return/">Возврат товара</a>
<br/><br/>
		2009 – 2020 © Магазин пряжи «Анже» 
		<img src="/i/aj/blocks/p-root/img/emoji/heart.png" style="height: 12px"/>
		www.anje.com.ua
	</div>
</div>
<script async="" src="/i/stat.js?localstat001"></script>
</div>
</div>
<div class="modal">
<div class="modal--window">
<div class="modal--window__close modal--window__close_right">
</div>
<div class="modal--window--ajax">
<img src="/i/v3/blocks/modal--window--ajax/modal--window--ajax-spinner.gif"/>
<p>Подождите пожалуйста, загружаем...</p>
</div>
</div>
</div>
</body>
</html>