<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>О компании</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "c3fb922dbf1505f1bb12a8414cd88b24"]); _ba.push(["host", "www.raifneft.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>
<!-- Favicons -->
<!-- Работу выполнил Спивак Д. Н. https://www.fl.ru/users/d9996541717 -->
<link href="bitrix/templates/raif/img/logo.png" rel="icon"/>
<!-- Normalize CSS File -->
<link href="bitrix/templates/raif/css/normalize.css" rel="stylesheet"/>
<!-- Bootstrap CSS File -->
<link href="bitrix/templates/raif/css/bootstrap-grid.min.css" rel="stylesheet"/>
<!-- Pushy CSS -->
<link crossorigin="anonymous" href="https://cdnjs.cloudflare.com/ajax/libs/pushy/1.3.0/css/pushy.css" integrity="sha256-xBbk1z+60pttKg5n14bvt+NjdxcOxd7LrRcRFuISfFY=" rel="stylesheet"/>
<!-- Slick CSS -->
<link href="bitrix/templates/raif/lib/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="bitrix/templates/raif/lib/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<!-- Main Stylesheet File -->
<link href="bitrix/templates/raif/css/style.css" rel="stylesheet"/>
<link href="bitrix/templates/raif/css/media.css" rel="stylesheet"/>
</head>
<body class="body">
<!-- Pushy Menu -->
<nav class="pushy pushy-left" data-focus="#first-link">
<div class="pushy-content">
<ul>
<div class="pushy-link"><a href="#header">Главная</a></div>
<div class="pushy-link"><a href="#about">О компании</a></div>
<div class="pushy-link"><a href="#contacts">Контакты</a></div>
</ul>
</div>
</nav>
<div class="site-overlay"></div>
<header class="header" id="header">
<div class="container">
<div class="row">
<div class="col-12">
<div class="header__top">
<img alt="logo" class="header__logo" src="bitrix/templates/raif/img/logo.png"/>
<ul class="header__menu">
<li class="header__item">
<a class="header__link" href="#header">Главная</a>
</li>
<li class="header__item">
<a class="header__link" href="#about">О компании</a>
</li>
<li class="header__item">
<a class="header__link" href="#contacts">Контакты</a>
</li>
</ul>
<div class="header__info">
<img class="header__info__icon" src="bitrix/templates/raif/img/phone.png"/>
<div class="header__info__item">
<a class="header__tel" href="tel:+78432537537">+7 (843) <span class="bold">518-15-33</span></a>
<a class="header__mail" href="mailto:info@raifneft.ru">info@raifneft.ru</a>
</div>
<div class="header__top__menu-btn">
<div class="menu-icon"></div>
</div>
</div>
</div>
<div class="header__slider__wrapp">
<div class="header__slider">
<div class="header__slide">
<img alt="slide1" class="header__slide__img" src="/upload/iblock/3de/3dee44c5d2304e14584414ca194aaa29.jpg"/>
<div class="header__slide__content">
<div class="header__slide__content__flex">
<div class="header__slide__content__text">
<div class="header__slide__title">Слайд 1</div>
<div class="header__slide__subtitle">Описание</div>
</div>
<a class="header__slide__btn" href="http://raifneft.ru/#2">Подробнее</a>
</div>
</div>
</div>
<div class="header__slide">
<img alt="slide1" class="header__slide__img" src="/upload/iblock/6c7/6c70015334ff21636be18d0e81e41b2f.jpg"/>
<div class="header__slide__content">
<div class="header__slide__content__flex">
<div class="header__slide__content__text">
<div class="header__slide__title">Слайд 2</div>
<div class="header__slide__subtitle">описание 2</div>
</div>
<a class="header__slide__btn" href="http://raifneft.ru/#1">Подробнее</a>
</div>
</div>
</div>
</div>
<img class="header__slider__next" src="bitrix/templates/raif/img/arrow.png"/>
<img class="header__slider__prev" src="bitrix/templates/raif/img/arrow.png"/>
</div>
</div>
</div>
</div>
</header>
<section class="about" id="about">
<div class="container">
<div class="row">
<div class="col-12">
<div class="about__title">О компании</div>
<div class="about__text">
<p>
	 Среди сервисных компаний Западно-Сибирского региона России, предоставляющих услуги нефтедобывающим комплексам в сфере повышения нефтеотдачи пластов, одно из ведущих мест занимает ООО "Раиф"<br/>
</p>
<p>
	 Мощная производственно-техническая база, высококвалифицированный персонал, современные методы, технологии, оборудование, соответствующие мировому уровню, позволяют осуществлять широкий спектр работ по вторичному воздействию на пласт с целью повышения его продуктивности. <br/>
</p>
</div>
</div>
</div>
</div>
</section>
<section class="contacts" id="contacts">
<div class="contacts__curtain"></div>
<iframe frameborder="0" height="330" src="https://yandex.com/map-widget/v1/?um=constructor%3A15500859dae855689a3d14ad206aff63c94fc31d3f6b6f3c54cbfb37ea6b9455&amp;source=constructor" width="100%"></iframe>
<div class="container">
<div class="row">
<div class="col-12">
<div class="contacts__wrapp">
<div class="contacts__title">Контакты</div>
<div class="contacts__info">
<img class="contacts__info__icon" src="bitrix/templates/raif/img/phone.png"/>
<a class="contacts__tel" href="tel:+78432537537">+7 (843) <span class="bold">518-15-33</span></a>
</div>
<div class="contacts__info">
<img class="contacts__info__icon" src="bitrix/templates/raif/img/phone.png"/>
<a class="contacts__tel" href="tel:+78432537537">+7 (843)<span class="bold">2-537-537</span></a>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- JQuery -->
<script src="bitrix/templates/raif/lib/jquery/jquery.min.js" type="text/javascript"></script>
<!-- Pushy JS -->
<script src="bitrix/templates/raif/lib/pushy/pushy.min.js" type="text/javascript"></script>
<!-- Slick JS -->
<script src="bitrix/templates/raif/lib/slick/slick.min.js" type="text/javascript"></script>
<!-- Template Main Javascript File -->
<script src="bitrix/templates/raif/js/main.js"></script>
<!-- #Begin_Article -->
</body>
</html>