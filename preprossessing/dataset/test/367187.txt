<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>cpxpalvelut.fi - Error 403: Forbidden</title>
<meta content="Finnish" name="language"/>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type"/>
<script type="text/javascript"> </script>
<link href="favicon.ico" rel="shortcut icon"/>
<link href="http://server5.nettihotelli.fi/error_pages/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="center">
<div id="container">
<div id="box1">
<h1>Virhe 403</h1>
<h2>Pääsy estetty - Forbidden</h2>
<p>Sinulla ei ole oikeutta päästä kyseiseen kohteeseen.<br/>Kohteeseen on määritelty ehdoton esto tai tiedosto-oikeudet ovat vajavaiset.</p>
<script src="http://server5.nettihotelli.fi/error_pages/error_js_content.js" type="text/javascript"></script>
<br/><br/><br/><br/><br/>
</div>
<div id="box2">
<h3 id="logo"><a href="http://www.nettihotelli.fi"><span></span>Nettihotelli.fi</a></h3>
</div>
</div>
</div>
</body>
</html>
<!--
   - Unfortunately, Microsoft has added a clever new
   - "feature" to Internet Explorer. If the text of
   - an error's message is "too small", specifically
   - less than 512 bytes, Internet Explorer returns
   - its own error message. You can turn that off,
   - but it's pretty tricky to find switch called
   - "smart error messages". That means, of course,
   - that short error messages are censored by default.
   - IIS always returns error messages that are long
   - enough to make Internet Explorer happy. The
   - workaround is pretty simple: pad the error
   - message with a big comment like this to push it
   - over the five hundred and twelve bytes minimum.
   - Of course, that's exactly what you're reading
   - right now.
   -->
