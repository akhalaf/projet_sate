<!DOCTYPE HTML>
<html lang="en">
<head><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-124022462-1"></script><script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-31897497-2');

gtag('config', 'UA-41105620-1');
</script><script>(function() {
var cx = '009262098191286400227:clre0hswmzs';
var gcse = document.createElement('script');
gcse.type = 'text/javascript';
gcse.async = true;
gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(gcse, s);
})();</script>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!-- headcode include -->
<!-- Hotjar Tracking Code for https://www.alaska.edu -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:695052,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<link href="/_resources/images/favicon.ico" rel="icon"/>
<!-- Bootstrap CSS -->
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" rel="stylesheet"/>
<!-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-zrnmn8R8KkWl12rAZFt4yKjxplaDaT7/EUkKm7AovijfrQItFWR7O/JJn4DAa/gx" crossorigin="anonymous"> -->
<script crossorigin="anonymous" src="https://kit.fontawesome.com/03f3fd53ff.js"></script>
<link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,600i,700%7cLato:400,400i,700,700i%7cEB+Garamond:400,400i" rel="stylesheet"/>
<link href="/_resources/css/global.css?v=2.2.3" rel="stylesheet"/><!-- global.css  -->
<link href="/_resources/css/sw.css?v=2.1.3" rel="stylesheet"/><!-- sw.css -->
<link href="/alaska/style.css?v=2.5" rel="stylesheet" type="text/css"/>
<title>UA System | University of Alaska System</title>
<style>
		.page-content {margin-top: 0;}
		.featured-bg-container .multicolumn-container { padding:0; }
		.card { min-height: 365px;}
		.page-content {font-size: 1rem;}
		.news-container .card-body {min-height: 590px;}
		.callout-container a.btn-secondary {margin: auto; margin-top:0!important;}
		</style>
<script>
					var page_url="https://www.alaska.edu/alaska/index.php";
				</script></head>
<body class="py-0">
<a class="sr-only sr-only-focusable" href="#maincontent">Skip to main content</a>
<header class="sw">
<!-- Global Header -->
<div class="swBanner container-fluid">
<div class="smLogo-banner">
<div class="mobile-siteLogo d-flex justify-content-center pt-3">
<a href="http://www.alaska.edu/">
<img alt="University of Alaska Logo" height="110" src=" /_resources/images/sw-logo.svg" width="130"/>
</a>
</div>
</div>
<div class="headerBarContainer">
<div class="searchContainer d-flex justify-content-end">
<div class="collapse" id="searchToggle" tabindex="-1">
<div class="custom-search">
<div class="gcse-search"></div>
</div>
</div>
</div>
<div class="resourcesContainer d-flex justify-content-end">
<div class="collapse" id="resourcesToggle" tabindex="-1">
<ul class="nav justify-content-end">
<li class="nav-item">
<a class="nav-link" href="https://www.alaska.edu/" target="_blank" title="Home button"><span aria-hidden="true" class="fas fa-home" title="Home button"></span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.alaska.edu/uaonline/">UAOnline</a>
</li>
<li class="nav-item">
<a class="nav-link" href="http://people.alaska.edu/">Staff Directory</a>
</li>
<li class="nav-item">
<a class="nav-link" href="http://www.alaska.edu/jobs/">Careers at UA</a>
</li>
<li class="nav-item">
<a class="nav-link" href="/alaska/uasystem/office.php">UA System Office</a>
</li>
</ul>
</div>
</div>
</div>
<div class="headerContainer d-flex">
<div class="headerLeft">
<div class="siteLogo">
<a href="http://www.alaska.edu/">
<img alt="University of Alaska Logo" src=" /_resources/images/sw-logo.svg"/>
</a>
</div>
</div>
<div class="headerRight">
<div class="siteBarContainer d-flex justify-content-end">
<div class="quickLinks">
<a aria-controls="resourcesToggle" aria-expanded="false" aria-haspopup="true" class="btn" data-target="#resourcesToggle" data-toggle="collapse" id="qlmenulabel" role="navigation">
<div><span class="fas fa-list-ul" title="Quick links"></span>  UA Quick Links</div>
</a>
</div>
<div class="swSearch">
<a aria-controls="searchToggle" aria-expanded="false" aria-haspopup="true" aria-label="search toggle button" class="btn my-sm-0" data-target="#searchToggle" data-toggle="collapse" role="search">
<div><span class="fas fa-search" title="Search"></span></div>
</a>
</div>
</div>
<div class="mauContainer d-flex justify-content-end">
<ul class="nav">
<li class="nav-item">
<a class="nav-link h5 uafColor" href="https://www.uaf.edu">UAF</a>
</li>
<li class="nav-item">
<a class="nav-link h5 uaaColor" href="https://www.uaa.alaska.edu/">UAA</a>
</li>
<li class="nav-item">
<a class="nav-link h5 uasColor" href="http://www.uas.alaska.edu/">UAS</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<!-- End Global Header -->
</header> <div id="main-slider">
<div class="carousel hero-lg slide carousel-fade mb-0" data-ride="carousel" id="myCarousel">
<ol class="carousel-indicators">
<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
<li data-slide-to="1" data-target="#myCarousel"></li>
<li data-slide-to="2" data-target="#myCarousel"></li>
<li data-slide-to="3" data-target="#myCarousel"></li>
<li data-slide-to="4" data-target="#myCarousel"></li>
</ol>
<div class="carousel-inner">
<div class="carousel-item active"><img alt="Ice core" class="d-block img-fluid" src="/alaska/images/hero/Ice-core.jpg"/><div class="container-fluid bg-black dark-overlay">
<div class="carousel-caption text-left">
<div class="h2">Many Traditions ONE Alaska</div>
<p role="heading">University of Alaska</p>
</div>
</div>
</div>
<div class="carousel-item"><img alt="GINA" class="d-block img-fluid" src="/alaska/images/hero/GINA.jpg"/><div class="container-fluid bg-black dark-overlay">
<div class="carousel-caption text-right">
<div class="h2">Many Traditions ONE Alaska</div>
<p role="heading">University of Alaska</p>
</div>
</div>
</div>
<div class="carousel-item"><img alt="Music" class="d-block img-fluid" src="/alaska/images/hero/UAF-music-Hero.jpg"/><div class="container-fluid bg-black dark-overlay">
<div class="carousel-caption text-left">
<div class="h2">Many Traditions ONE Alaska</div>
<p role="heading">University of Alaska</p>
</div>
</div>
</div>
<div class="carousel-item"><img alt="Spheres of Influence" class="d-block img-fluid" src="/alaska/images/hero/Spheres.jpg"/><div class="container-fluid bg-black dark-overlay">
<div class="carousel-caption text-left">
<div class="h2">Many Traditions ONE Alaska</div>
<p role="heading">University of Alaska</p>
</div>
</div>
</div>
<div class="carousel-item"><img alt="Graduate hooding" class="d-block img-fluid" src="/alaska/uasystem/images/Graduate-hooding.jpg"/><div class="container-fluid bg-black dark-overlay">
<div class="carousel-caption text-left">
<div class="h2">Many Traditions ONE Alaska</div>
<p role="heading">University of Alaska</p>
</div>
</div>
</div>
</div><a class="carousel-control-prev" data-slide="prev" href="#myCarousel" role="button"><span aria-hidden="true" class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" data-slide="next" href="#myCarousel" role="button"><span aria-hidden="true" class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
</div>
<section><div class="site-menu"><nav aria-label="Primary" class="menuPrimary navbar navbar-expand-lg navbar-dark"><button aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarTogglerDemo02" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button><div class="siteNav container-fluid"><div class="collapse navbar-collapse" id="navbarTogglerDemo02"><ul class="container navbar-nav mr-auto mt-2 mt-lg-0"><li class="nav-item"><a class="nav-link" href="/alaska/index.php">Home<span class="sr-only">(current)</span></a></li><li class="nav-item"><a class="nav-link" href="/alaska/campuses.php">Campuses<span class="sr-only">(current)</span></a></li><li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Admissions</a><div class="dropdown-menu"><a class="dropdown-item" href="/alaska/apply.php">How to apply</a><a class="dropdown-item" href="/alaska/cost.php">Cost of attendance</a><a class="dropdown-item" href="/alaska/degrees/index.php">Degrees and programs</a></div></li><li class="nav-item"><a class="nav-link" href="/alaska/alumni.php">Alumni<span class="sr-only">(current)</span></a></li><li class="nav-item"><a class="nav-link" href="https://www.alaska.edu/foundation/" title="UA Foundation">Donate<span class="sr-only">(current)</span></a></li><li class="nav-item"><a class="nav-link" href="https://myfuture.alaska.edu/">My Future Alaska<span class="sr-only">(current)</span></a></li><li class="nav-item dropdown"><a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">UA System </a><div class="dropdown-menu"><a class="dropdown-item" href="/alaska/uasystem/about/index.php">About UA</a><a class="dropdown-item" href="/orgcharts/index.php">UA Org Charts</a><a class="dropdown-item" href="/alaska/uasystem/index.php">UA System Home</a><a class="dropdown-item" href="/alaska/uasystem/office.php">System Office Directory</a><a class="dropdown-item" href="/bor/index.php">Board of Regents</a><a class="dropdown-item" href="/hr/index.php">Human Resources</a><a class="dropdown-item" href="/govrelations/index.php">Government Relations - Advocacy</a><a class="dropdown-item" href="http://www.ualand.com/">UA Lands Office</a></div></li></ul></div></div></nav></div></section>
<main id="maincontent" tabindex="-1">
<div class="main-content container">
<div class="page-content">
<div class="row">
<div class="col">
<p> </p>
<!-- Covid19 alert -->
<div class="alert alert-danger alert-dismissible fade show" role="alert">
<div class="row">
<div class="col-sm-1 text-center"><span aria-hidden="true" class="fas fa-exclamation-circle fa-3x"> </span></div>
<div class="col-sm-11">
<h4 class="alert-heading">Coronavirus information:</h4>
<p>Visit the <a class="font-weight-bold" href="https://sites.google.com/alaska.edu/coronavirus/">UA coronavirus information website</a> to learn how the University of Alaska is responding to the novel coronavirus/COVID-19
                                 situation and find links to communications, policy guidance and resources.
                              </p>
</div>
</div>
<button aria-label="Close" class="close" data-dismiss="alert" type="button"> <span aria-hidden="true">×</span> </button></div>
<!-- end Covid19 alert -->
<div class="callout-container"><div class="row">
<div class="card-vertical col-md mb-3 callout">
<div class="card">
<div class="card-image"><a href="/alaska/degrees/index.php"><img alt="Degrees and Programs" class="img-fluid" src="/alaska/images/spotlights/spotlight8.jpg"/></a></div>
<div class="card-content d-flex flex-column" style="flex: 1 0 0%; margin-bottom: 0;">
<h4 class="card-header varTwo"><a href="/alaska/degrees/index.php">Degrees and programs</a></h4>
<div class="card-body d-flex flex-column">
<div class="card-text">Information on study options and programs.</div>
</div>
</div>
</div>
</div>
<div class="card-vertical col-md mb-3 callout">
<div class="card">
<div class="card-image"><a href="/alaska/cost.php"><img alt="Cost of Attendance" class="img-fluid" src="/alaska/images/spotlights/spotlight1.jpg"/></a></div>
<div class="card-content d-flex flex-column" style="flex: 1 0 0%; margin-bottom: 0;">
<h4 class="card-header varTwo"><a href="/alaska/cost.php">Cost of attendance</a></h4>
<div class="card-body d-flex flex-column">
<div class="card-text">Find out more about cost of attendance, room and board, tuition and fees. </div>
</div>
</div>
</div>
</div>
<div class="card-vertical col-md mb-3 callout">
<div class="card">
<div class="card-image"><img alt="How to apply" class="img-fluid" src="/alaska/images/spotlights/spotlight7.jpg"/></div>
<div class="card-content d-flex flex-column" style="flex: 1 0 0%; margin-bottom: 0;">
<h4 class="card-header varTwo"><a href="/alaska/apply.php">How to apply </a></h4>
<div class="card-body d-flex flex-column">
<div class="card-text">
<p>UA admission process.</p>
</div>
</div>
</div>
</div>
</div>
<div class=" col-md mb-3 callout">
<div class="card">
<div class="card-image"><a href="/alaska/workforce.php"><img alt="Career Coach" class="img-fluid" src="/alaska/images/spotlights/spotlight9.jpg"/></a></div>
<div class="card-content d-flex flex-column" style="flex: 1 0 0%; margin-bottom: 0;">
<h4 class="card-header varTwo"><a href="/alaska/workforce.php">Workforce Development</a></h4>
<div class="card-body d-flex flex-column">
<div class="card-text">Find discounted training, expolore training and careers, and access career services. </div>
</div>
</div>
</div>
</div></div> </div>
<div class="ou-component-button">
<a class="btn btn-info btn-lg btn-block" href="/news/index.php" role="button">
                           		
                           		Visit the UA News Center
                           	</a>
</div>
<div class="featured-bg-container full-width light">
<div class="row justify-content-center">
<h3 class="text-uppercase"><a href="/news/index.php">UA news and information</a></h3>
</div>
<div class="container-custom">
<div class="row my-3">
<div class="col-md">
<h4 class="card-header" style="background: #006299;">Twitter feed</h4>
<a class="twitter-timeline" data-height="590" href="https://twitter.com/UA_System?ref_src=twsrc%5Etfw">Tweets by UA_System</a>
<script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js"></script></div>
<div class="col-md">
<div class="events-container">
<div class="row"><div class="col-md mb-9">
<div class="card">
<h4 class="card-header">Events</h4>
<div class="card-body">
<div class="events">
<ul>
<li>
<p class="date"><span class="month">Jan</span><span class="day">18</span></p>
<p class="title h5"><a>Martin Luther King, Jr. Day - Celebration of Alaska Civil Rights Day - No classes
                                                         and most offices closed</a></p>
<p class="details">  </p>
<div class="short-desc"> </div>
</li>
<li>
<p class="date"><span class="month">Jan</span><span class="day">22</span></p>
<p class="title h5"><a>
                                                         Deadline to pay tuition and fees (5 p.m. in person, midnight for UAOnline) 
                                                         </a></p>
<p class="details">  </p>
<div class="short-desc"> </div>
</li>
<li>
<p class="date"><span class="month">Jan </span><span class="day">22</span></p>
<p class="title h5"><a>
                                                         Last day for student-and faculty-initiatived course drops with full refund 
                                                         </a></p>
<p class="details">  </p>
<div class="short-desc"> </div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div></div>
<div style="border: 1px solid #dfdfdf; border-top: 0; background: #ffffff; padding: 1rem;">
<ul>
<li><a href="https://www.uaa.alaska.edu/calendars/">UAA Calendar</a></li>
<li><a href="https://catalog.uaf.edu/calendar/">UAF Calendar</a></li>
<li><a href="http://www.uas.alaska.edu/calendar/academic.html">UAS Calendar</a></li>
</ul>
</div>
</div>
<div class="col-md">
<div class="news-container"><div class="row"><div class="col-md mb-9">
<div class="card">
<h4 class="card-header">UA News</h4>
<div class="card-body">
<div class="news newsfeed">
<ul>
<li>
<div class="newsImage"><img alt="Interim athletics director" src="https://bloximages.newyork1.vip.townnews.com/newsminer.com/content/tncms/assets/v3/editorial/5/0f/50fe6a4a-5168-11eb-a1d2-33c277ce95b6/5ff7dccc9abb0.image.jpg?resize=355%2C500"/></div>
<p class="title h5"><a href="http://www.newsminer.com/sports/uaf_nanooks/uaf-appoints-new-interim-athletic-director/article_37665584-5168-11eb-a401-a3dfa396cada.html">
                                                   UAF appoints new interim athletic director 
                                                   </a></p>
<div class="date m-0">  Jan. 8, 2021 </div>
<div class="short-desc pb-2 pt-2"> University of Alaska Fairbanks Chancellor Dan White has appointed UAF School of Management
                                                faculty member Peggy Keiper to serve as interim athletic director for the Alaska Nanooks,
                                                the university announced in a statement on Thursday. 
                                             </div><a href="http://www.newsminer.com/sports/uaf_nanooks/uaf-appoints-new-interim-athletic-director/article_37665584-5168-11eb-a401-a3dfa396cada.html">Read Article <span class="fas fa-angle-double-right"></span></a></li>
<li>
<div class="newsImage"><img alt="UAS Sitka" src="https://iseralaska.org/wp-content/uploads/2020/11/UAS-Sitka-web.jpg"/></div>
<p class="title h5"><a href="https://www.kcaw.org/2020/12/22/already-good-at-remote-uas-sitka-enrollment-rides-a-promising-wave-during-pandemic/">
                                                   Already ‘good at remote,’ UAS Sitka enrollment rides a promising wave during pandemic
                                                   
                                                   </a></p>
<div class="date m-0"> Dec. 12, 2020 </div>
<div class="short-desc pb-2 pt-2">
<div class=" col-print-12">
<div class=" col-print-12">
<p class="element element-paragraph"><span>Sitka’s campus of the University of Alaska Southeast is closed to the public, but
                                                            education is happening there — at greater levels than anyone expected.</span></p>
</div>
</div>
</div><a href="https://www.kcaw.org/2020/12/22/already-good-at-remote-uas-sitka-enrollment-rides-a-promising-wave-during-pandemic/">Read Article <span class="fas fa-angle-double-right"></span></a></li>
<li>
<div class="newsImage"><img alt="UAA" src="https://www.adn.com/resizer/2T5OagxiFgIPBjQxHmrQi0iJdYE=/1200x0/cloudfront-us-east-1.images.arcpublishing.com/adn/4GRXULEDCRFRHKG4U7FWH3WWIE.jpg"/></div>
<p class="title h5"><a href="https://www.adn.com/opinions/2020/12/15/uaa-is-doing-incredible-work-for-alaska-it-deserves-your-respect/">
                                                   UAA is doing incredible work for Alaska. It deserves your respect 
                                                   </a></p>
<div class="date m-0"> Dec. 16, 2020 </div>
<div class="short-desc pb-2 pt-2">
<p><span>UAA faculty stepped up to respond to the pandemic in many ways, including developing
                                                      predictive models to track infection rates and intensive care unit utilization. They
                                                      have also trained hundreds of contact tracers to serve the region and the state. I
                                                      would proudly and confidently stack UAA up against any other open-access, urban-metropolitan
                                                      university in the United States. UAA is</span><span> </span><i>that</i><span> </span><span>good. </span></p>
</div><a href="https://www.adn.com/opinions/2020/12/15/uaa-is-doing-incredible-work-for-alaska-it-deserves-your-respect/">Read Article <span class="fas fa-angle-double-right"></span></a></li>
</ul>
</div><a class="btn btn-secondary" href="/news/featured/index.php">More News</a></div>
</div></div></div></div>
<p> </p>
</div>
</div>
</div></div>
<p> </p>
<div class="featured-bg-container full-width dark">
<div class="row justify-content-center">
<h3 class="text-uppercase"><a>Quick Facts</a></h3>
</div>
<div class="container-custom">
<div class="row my-3">
<div class="col-md">
<p class="mceTmpParagraph"> </p>
<div class="multicolumn-container">
<div class="row">
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-certificate fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">585</p>
<h4 class="card-title">Degrees, certificates and endorsements</h4>
</div>
</div>
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-laptop fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">126</p>
<h4 class="card-title">eLearning programs</h4>
</div>
</div>
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-users fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">26,641</p>
<h4 class="card-title">Students enrolled in 2018-19</h4>
</div>
</div>
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-graduation-cap fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">100,000</p>
<h4 class="card-title text-white">Alumni</h4>
</div>
</div>
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-flask fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">$163.2 M</p>
<h4 class="card-title text-white">Research expenditure</h4>
</div>
</div>
<div class="col-md mb-3">
<div class="facts"><span aria-hidden="true" class="fas fa-briefcase fa-4x pb-3"></span>
<p class="card-text text-warning font-weight-bold h4">65%</p>
<h4 class="card-title text-white">Graduates employed full or part time within six months of graduation</h4>
</div>
</div>
</div>
</div>
                               
                           </div>
</div>
</div></div>
<p> </p>
<div class="multicolumn-container">
<div class="row justify-content-center">
<h3 class="text-uppercase"><a>
                                 Many Traditions One Alaska 
                                 </a></h3>
</div>
<div class="row">
<div class="col-md mb-3"><a href="/alaska/campuses.php" style="font-size: 20px;"><span color="#212529" style="color: #212529;"><span color="#212529" style="color: #212529;"><img alt="Map of UA campuses" height="482" src="https://www.alaska.edu/alaska/images/AlaskaMapUpdate-2019.png" style="padding-right: 1rem;" width="550"/></span></span></a></div>
<div class="col-md mb-3">
<h2 style="color: #954d00;"><span color="#212529" face="Lato, Trebuchet MS, Arial, sans-serif" style="color: #212529; font-family: Lato, 'Trebuchet MS', Arial, sans-serif;"><span style="font-size: 20px;">The University of Alaska is a land-, sea- and space-grant system of higher education
                                       established in 1917. </span></span></h2>
<h3 style="color: #3c535e; padding-left: 30px;"><em><span color="#212529" face="Lato, Trebuchet MS, Arial, sans-serif" style="color: #212529; font-family: Lato, 'Trebuchet MS', Arial, sans-serif;"><span style="font-size: 20px;"><span color="#212529" face="Lato, Trebuchet MS, Arial, sans-serif" style="color: #212529; font-family: Lato, 'Trebuchet MS', Arial, sans-serif;">The UA system's three universities (UAA, UAF and UAS) are separately accredited institutions
                                             with <a href="/alaska/campuses.php">campuses</a> and extended learning centers across the state of Alaska. </span></span></span></em></h3>
<div class="button-container"><a class="btn btn-primary btn-lg arrow-yes" href="/alaska/uasystem/about/index.php" role="button">Learn More</a></div>
<p><span color="#212529" style="color: #212529;"><span style="font-size: 20px;">   </span></span></p>
</div>
</div>
</div>
<p><span style="color: #000000; font-size: 14px;"> </span></p>
</div>
</div>
</div>
</div>
</main>
<div class="spacer"></div>
<footer class="page-footer center-on-small-only pt-1 pt-5 swFooter"><div class="container-fluid custom-footer"><div class="row container mx-auto text-white"><div class="col-md"><h5>University of Alaska System</h5><p>2025 Yukon Drive, Fairbanks, AK 99775<br/><i class="far fa-phone"></i>  <a class="text-decoration-none" href="tel:+19074508100">907-450-8100</a></p></div></div></div>
<!-- Global Footer -->
<div class="container text-center text-white">
<hr/>
<p>
<a href="http://www.alaska.edu/alaska/"><img alt="University of Alaska logo" src="/_resources/images/swlogo.png" width="200"/></a>
</p>
<!--Social buttons-->
<!-- <p>
		<a class="text-white" href="#"><span aria-hidden="true" class="fab fa-facebook-square fa-2x"></span></a>
		<a class="text-white" href="#"><span aria-hidden="true" class="fab fa-instagram fa-2x"></span></a>
		<a class="text-white" href="#"><span aria-hidden="true" class="fab fa-twitter-square fa-2x"></span></a>
		<a class="text-white" href="#"><span aria-hidden="true" class="fab fa-youtube-square fa-2x"></span></a>
	</p> -->
<!--/.Social buttons-->
<!--Copyright-->
<p class="small">The <a class="footer-link" href="http://www.alaska.edu/">University of Alaska</a> is an AA/EO employer and educational institution and prohibits illegal discrimination against any individual. Learn more about UA's <a href="https://www.alaska.edu/nondiscrimination">notice of nondiscrimination</a>.<br/>
UA is committed to providing accessible websites. Learn more about UA's <a href="https://www.alaska.edu/webaccessibility">notice of web accessibility</a>.<br/><br/>
		This site is maintained by OIT. For questions or concerns, please contact the OIT Support Center.<br/>
		For questions or comments regarding this page, contact <a class="footer-link" href="mailto:helpdesk@alaska.edu">helpdesk@alaska.edu</a>  | <span id="directedit"> </span> </p>
<!--/.Copyright-->
</div>
<br/>
<!-- End Global Footer -->
</footer>
<script crossorigin="anonymous" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
	window.jQuery || document.write('<script src="https://getbootstrap.com/assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script crossorigin="anonymous" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="/_resources/js/slick/slick.min.js"></script>
<script src="/_resources/js/slick/initialization.js"></script>
<script src="/_resources/js/global.js?version=3.7"></script>
<script src="/_resources/js/direct-edit.js"></script>
<div id="hidden" style="display:none;">
<a href="https://edit.alaska.edu/10?skin=oucampus&amp;account=alaska&amp;site=alaska&amp;action=de&amp;path=/alaska/index.pcf" id="de"> ⓒ</a>  UA
      </div>
</body>
</html>