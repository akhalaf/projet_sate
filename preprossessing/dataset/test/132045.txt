<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="index" name="robots"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="black-translucent" name="apple-mobile-web-app-status-bar-style"/>
<meta content="telephone=no,address=no,email=no" name="format-detection"/>
<!--
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png" />
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png" />
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png" />
-->
<link href="/images/favicons/favicon.ico" rel="shortcut icon"/>
<!--
<meta name="msapplication-TileColor" content="#603cba" />
<meta name="msapplication-config" content="/images/favicons/browserconfig.xml" />
<meta name="theme-color" content="#ffffff" />
--><meta content="" name="keywords"/>
<meta content="" name="description"/>
<link href="https://www.aoyama.ac/" rel="canonical"/>
<title>青山綜合会計事務所</title>
<link href="/css/style.css" rel="stylesheet"/>
<link href="/css/lib/swiper.min.css" rel="stylesheet"/>
</head>
<body class="top">
<header class="topDer" id="header">
<div class="enclosingWrap" id="animeH">
<div class="enclosing">
<div class="logoBox">
<a class="notHover" href="/"><img alt="青山綜合会計事務所" src="/images/common/logo.svg"/></a>
</div>
<nav id="globalNavigations">
<div class="navWrap">
<ul>
<li><a class="notHover uaJs" href="/services/">Services</a></li>
<li><a class="notHover uaJs " href="/about_us/">About Us</a></li>
<li><a class="notHover uaJs " href="/group/">Group/Partner <br class="originalBr"/>Companies</a></li>
<li><a class="windowIcon black notHover uaJs" href="/recruit/" target="_blank">Recruit</a></li>
<li><a class="notHover uaJs " href="/topic/">Topics</a></li>
<li class="navEnd"><a class="notHover uaJs" href="/seminar/">Seminar</a></li>
<li class="spOnly">
<div class="navContact">
<a href="/contact/">
<div class="box">
<p>お問い合わせ</p>
<span>Contact Us</span>
</div>
</a>
</div>
</li>
<li class="spOnly">
<div class="lang">
<a href="javascript:void(0)">JP</a> / <a href="http://www.en.aoyama.ac/" target="_blank">EN</a>
</div>
</li>
<li class="spOnly">
<div class="primeGlobalColumn">
<a class="alertPop" href="javascript:void(0);">
<img alt="primeglobal" src="/images/common/logo_primeglobal.png" srcset="/images/common/logo_primeglobal@2x.png 2x, /images/common/logo_primeglobal@3x.png 3x"/>
<p>Aoyama Sogo Accounting Firm Co. Ltd. <br/>an independent member firm of PrimeGlobal</p>
</a>
</div>
</li>
</ul>
</div>
</nav>
<div class="trackingMoveContact topDer">
<div class="langeBox">
<a class="notHover notPointer" href="javascript:void(0)">JP</a><span>/</span><a href="http://www.en.aoyama.ac/" target="_blank">EN</a>
</div>
<div class="contactMoveBox">
<p><a href="/contact/">Contact Us</a></p>
</div>
</div>
<div id="spNavBtn">
<div class="box">
<span></span>
<span></span>
<span></span>
</div>
<div class="closeBox">
<span></span>
<span></span>
</div>
</div>
</div>
</div>
</header><main id="indexTop">
<article>
<section class="loaderBg" id="loaderBg">
<div class="loader" id="loader">
<img alt="" src="/images/index/load_logo.svg"/>
<span></span>
</div>
</section>
<section class="mainVisualLayout">
<div class="setPosTxt" id="posTxt">
<div class="txtBox">
<h1>Innovation<br/>Speed
            <br/>Quality
          </h1>
<p>青山綜合会計事務所は専門的なノウハウを追求し続け、<br/> 斬新かつ迅速に、高品質なサービスをご提供いたします。</p>
</div>
</div>
<div class="keyVisualPos" id="VisualPos">
<div class="swiper-container" id="topSlide">
<div class="swiper-wrapper">
<div class="swiper-slide">
<div class="slide bg1"></div>
</div>
<div class="swiper-slide">
<div class="slide bg2"></div>
</div>
<div class="swiper-slide">
<div class="slide bg3"></div>
</div>
</div>
</div>
</div>
<div class="posWaveWrap" id="control">
<div class="waveMax" id="waveVueAnime1">
<object data="/images/wave/wave1.svg" id="waveVueAnimeA" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnime2">
<object data="/images/wave/wave2.svg" id="waveVueAnimeB" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnime3">
<object data="/images/wave/wave3.svg" id="waveVueAnimeC" type="image/svg+xml"></object>
</div>
</div>
</section>
<!--primeglobal-->
<section class="setSeminarLayout">
<div class="primeLink" id="prime">
<a class="alertPop" href="javascript:void(0);">
<img alt="primeglobal" src="/images/common/logo_primeglobal.png" srcset="/images/common/logo_primeglobal@2x.png 2x, /images/common/logo_primeglobal@3x.png 3x"/>
<p>Aoyama Sogo Accounting Firm Co. Ltd. <br/>an independent member firm of PrimeGlobal</p>
</a>
</div>
</section>
<!--primeglobal-->
<!--事業内容-->
<section class="serviceMovesLayout">
<div class="heading">
<h2 class="titTop">Services</h2>
</div>
<hr class="scrTarget headingTop" data-distance="50" data-typeanime="amplification"/>
<div class="businessContentTxt">
<div class="contentTxt">
<h3>事業内容</h3>
<p>設立当初から極めて専門性の高い領域で、お客様に納得いただけるサービスを提供してきました。<br/> グローバルニーズへの対応はもちろん、多彩なプロフェッショナル・サービスで、お客様のあらゆる課題解決に取り組みます。
          </p>
</div>
<div class="LearnMoreLayout">
<a class="topMore scrTarget" data-distance="0" data-typeanime="more" href="/services/">Learn More<span class="moreBar"></span></a>
</div>
</div>
<div class="posWaveWrap" data-distance="150" id="alpha">
<div class="waveMax" id="waveVueAnimeAlphaA">
<object data="/images/wave/wave1.svg" id="waveVueAnimeAlpha1" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnimeAlphaB">
<object data="/images/wave/wave2.svg" id="waveVueAnimeAlpha2" type="image/svg+xml"></object>
</div>
</div>
<!--ファンドサービス-->
<div class="transitionEachPage fund">
<div class="setPosLayout">
<div class="sizeAdjustment">
<div class="titBox">
<h3>Fund Services</h3>
<span>ファンドサービス</span>
<hr class="scrTarget" data-distance="50" data-typeanime="amplification"/>
</div>
<div class="txtList">
<p class="txt">設立当初から培われてきた豊富な経験を生かし、ワンストップでSPCの設立から管理、清算までお任せいただけます。</p>
<ul class="scrTarget fundA">
<li><a href="/services/fund/">SPCアドミニストレーション</a></li>
<li><a href="/services/fund/advisory.html">アドバイザリ－サービス</a></li>
<li><a href="/services/fund/inbound.html">インバウンド投資サポート</a></li>
</ul>
</div>
</div>
</div>
</div>
<!--ファンドサービス-->
<!--コーポレートサービス-->
<div class="transitionEachPage corporate">
<div class="setPosLayout">
<div class="sizeAdjustment corpS">
<div class="titBox">
<h3>Corporate Services</h3>
<span>コーポレートサービス</span>
<hr class="scrTarget" data-distance="50" data-typeanime="amplification"/>
</div>
<div class="txtList">
<p class="txt">事業構造、財務体質の改善、成長分野への進出など、事業の拡大と成長に向けた事業戦略に実効性をもたらすサポート・コンサルテーション・アドバイスを提供します。</p>
<ul class="scrTarget corPo">
<li><a href="/services/corporate/">企業価値評価・デューデリジェンス</a></li>
<li><a href="/services/corporate/international_tax.html">国際税務</a></li>
<li><a href="/services/corporate/tax_advisory.html">会計サポート・税務顧問</a></li>
<li><a href="/services/corporate/asian_expansion_support.html">アジア進出支援</a></li>
</ul>
</div>
</div>
</div>
</div>
<!--コーポレートサービス-->
</section>
<!--事業内容-->
<!--ABOUT&Group-->
<section class="aboutAndGroupMovesLayout">
<!--ABOUT-->
<div class="aboutBox">
<div class="setMaxWidth">
<div class="posSetWrap">
<a class="photo" href="/about_us/"></a>
<div class="txtColumn">
<h2 class="titTop">About</h2>
<hr class="scrTarget" data-distance="50" data-typeanime="amplification"/>
<h3>会社情報</h3>
<p class="txt">
                時代とともに変化するビジネスシーンにおいて、お客様と真摯に向き合いながら歩んできた青山綜合会計事務所。私たちはこの姿勢をどこまでも貫き、皆様に信頼されるプロフェッショナル集団をめざします。</p>
<p class="learnSp scrTarget" data-distance="50" data-typeanime="more"><a class="topMore" href="/about_us/">Learn More<span class="moreBar"></span></a></p>
</div>
</div>
</div>
<div class="LearnMoreLayout">
<a class="topMore scrTarget" data-distance="0" data-typeanime="more" href="/about_us/">Learn More<span class="moreBar"></span></a>
</div>
<div class="posWaveWrap" data-distance="150" id="beta">
<div class="waveMax" id="waveVueAnimeBetaA">
<object data="/images/wave/beta_wave01.svg" id="waveVueAnimeBeta1" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnimeBetaB">
<object data="/images/wave/beta_wave03.svg" id="waveVueAnimeBeta2" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnimeBetaC">
<object data="/images/wave/beta_wave04.svg" id="waveVueAnimeBeta3" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnimeBetaD">
<object data="/images/wave/beta_wave05.svg" id="waveVueAnimeBeta4" type="image/svg+xml"></object>
</div>
</div>
</div>
<!--ABOUT-->
<!--Group / Partners-->
<div class="groupAndPartnersBox">
<div class="setMaxWidthGroup">
<div class="titBox">
<h2 class="titTop">Group / Partners</h2>
</div>
<hr class="scrTarget" data-distance="50" data-typeanime="amplification"/>
<div class="txtBox">
<div class="txtArea">
<h3>グループ企業・提携企業</h3>
<p>グループ各社連携し、プロフェッショナル集団によるコラボレーションで、お客様に最適なサービスを提供しています。</p>
<div class="LearnMoreLayout">
<a class="topMore scrTarget" data-distance="0" data-typeanime="more" href="/group/">Learn More<span class="moreBar"></span></a>
</div>
</div>
<a class="photo" href="/group/">
<img alt="グループ企業・提携企業" src="/images/index/index_img_group.svg"/>
</a>
</div>
</div>
</div>
<!--Group / Partners-->
</section>
<!--ABOUT&Group-->
<!--RECRUIT-->
<section class="recruitMovesLayout">
<div class="setFlexBox">
<div class="txtBox">
<div class="sizeAdjustment">
<h2 class="titTop">Recruit</h2>
<h3>採用情報</h3>
<p>時代とともに変化するビジネスシーンにおいて、お客様と真摯に向き合いながら歩んできた青山綜合会計事務所。私たちはこの姿勢をどこまでも貫き、皆様に信頼されるプロフェッショナル集団をめざします。</p>
<div class="moveBoxLayout">
<a class="windowIcon black" href="/recruit/" target="_blank">採用サイトはこちら</a>
</div>
</div>
</div>
<hr class="recruitTop scrTarget" data-distance="50" data-typeanime="amplification"/>
<div class="moveBox">
<div class="thumbnailMoveBox">
<a href="/recruit/" target="_blank">
<img alt="キャリア・新卒採用" src="/images/index/index_img_recruit01.jpg" srcset="/images/index/index_img_recruit01@2x.jpg 2x, /images/index/index_img_recruit01@3x.jpg 3x"/>
<div class="txtArea">
<h4>キャリア・新卒採用</h4>
<hr/>
<p>Learn More</p>
</div>
</a>
</div>
<div class="thumbnailMoveBox">
<a href="/recruit/requirements" target="_blank">
<img alt="募集要項" src="/images/index/index_img_recruit02.jpg" srcset="/images/index/index_img_recruit02@2x.jpg 2x, /images/index/index_img_recruit02@3x.jpg 3x"/>
<div class="txtArea">
<h4>募集要項</h4>
<hr/>
<p>Learn More</p>
</div>
</a>
</div>
</div>
</div>
<div class="posWaveWrap" data-distance="50" id="gamma">
<div class="waveMax" id="waveVueAnimeGammaA">
<object data="/images/wave/gamma_wave1.svg" id="waveVueAnimeGamma1" type="image/svg+xml"></object>
</div>
<div class="waveMax" id="waveVueAnimeGammaB">
<object data="/images/wave/gamma_wave2.svg" id="waveVueAnimeGamma2" type="image/svg+xml"></object>
</div>
</div>
</section>
<!--RECRUIT-->
<!--Topics-->
<section class="topicsMovesLayout">
<div class="titBox">
<h2>Topics</h2>
<hr class="scrTarget" data-distance="50" data-typeanime="amplification"/>
<h3>最新情報</h3>
</div>
<div class="postInfoList">
<ul>
<li>
<a class="" href="https://www.aoyama.ac/topic/389" target="_self">
<div class="typeBox">
<p class="type topic">Topic</p> <time>2021.01</time>
</div>
<div class="postTxtColumn">
<h4>新型コロナウイルスの感染拡大に伴う対応について</h4>
<p>当社では、新型コロナウイルス感染拡大に対する1都3県への緊急事態宣言の発令を踏まえ、同宣言の期間終了まで以下の対応を実施いたします。</p>
</div>
</a>
</li>
<li>
<a class="" href="https://www.aoyama.ac/seminar/384" target="_self">
<div class="typeBox">
<p class="type seminar">Seminar</p> <time>2020.11</time>
</div>
<div class="postTxtColumn">
<h4>新型コロナウイルスのIFRS財務諸表に与える影響</h4>
<p>2020/12/2開催 参加費無料　青山綜合会計事務所 会計・税務セミナー</p>
</div>
</a>
</li>
<li>
<a class="" href="https://www.aoyama.ac/seminar/381" target="_self">
<div class="typeBox">
<p class="type seminar">Seminar</p> <time>2020.11</time>
</div>
<div class="postTxtColumn">
<h4>繰越欠損金を利用したタックスプランニング</h4>
<p>2020/11/18開催 参加費無料　青山綜合会計事務所 会計・税務セミナー</p>
</div>
</a>
</li>
</ul>
<div class="moveTopicsPage">
<a href="/topic/">一覧はこちら</a>
</div>
</div>
</section>
<!--Topics-->
<!--青山綜合経営塾（青山塾）-->
<section class="jukuMoveLayout">
<div class="setMaxColumn">
<div class="titBox">
<h2>青山綜合経営塾（青山塾）</h2>
<h3><span>Aoyama Sogo Management School</span></h3>
</div>
<div class="moveColumn">
<a href="/juku/">
<div class="thumb">
<img alt="青山綜合経営塾（青山塾）" src="/images/index/juku_thumbnail.jpg" srcset="/images/index/juku_thumbnail@2x.jpg 2x, /images/index/juku_thumbnail@3x.jpg 3x"/>
</div>
<div class="txtBox">
<p>日本を活性化する中小・新興<br/> 企業経営者ネットワーク
              </p>
<div class="LearnMoreLayout">
<span class="moreBtn topMore scrTarget" data-distance="0" data-typeanime="more">Learn More<span class="moreBar"></span></span>
</div>
</div>
</a>
</div>
</div>
</section>
<!--青山綜合経営塾（青山塾）-->
<!-- Page Top -->
<section class="mostBottomLayout">
<div class="offTouch" id="topmove">
<hr/>
<p>Page Top</p>
</div>
<div class="breadcrumb">
<ol>
<li>Home</li>
</ol>
</div>
</section>
<!-- Page Top -->
</article>
</main>
<div id="responsibleCheck"></div>
<footer id="footer">
<div class="footerBg">
<div class="sitemapLayout">
<div class="listBox oneColumn">
<dl class="accordionSp">
<dt>
<a class="pcOnly" href="/services/">Services</a>
<p class="spOnly">Services</p>
</dt>
<dd>
<ul>
<li><a href="/services/">事業内容</a></li>
<li><a href="/services/fund/">ファンドサービス</a></li>
<li><a href="/services/corporate/">コーポレートサービス</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox twoColumn">
<dl class="accordionSp">
<dt>
<a class="pcOnly" href="/about_us/">About Us</a>
<p class="spOnly">About Us</p>
</dt>
<dd>
<ul>
<li><a href="/about_us/company_profile.html">会社概要</a></li>
<li><a href="/about_us/partners.html">役員紹介</a></li>
<li><a href="/about_us/advisors.html">顧問紹介</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox treeColumn">
<dl class="accordionSp">
<dt>
<a class="pcOnly" href="/group/">Group / Partners</a>
<p class="spOnly">Group / Partners</p>
</dt>
<dd>
<ul>
<li><a href="/group/group_companies.html">グループ企業の紹介</a></li>
<li><a href="/group/partner_companies.html">提携企業のご紹介</a></li>
<li><a href="/group/singaporeoffice.html">シンガポールオフィス概要</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox fourColumn">
<dl class="accordionSp">
<dt><span class="windowIcon black">Recruit</span></dt>
<dd>
<ul>
<li><a href="/recruit/" target="_blank">キャリア/新卒採用情報</a></li>
<li><a href="/recruit/corporate/" target="_blank">会社を知る</a></li>
<li><a href="/recruit/environment/" target="_blank">働く環境を知る</a></li>
<!--              <li><a href="/recruit/environment/" target="_blank">社員紹介</a></li>-->
<li><a href="/recruit/new_graduates/" target="_blank">新卒の方へ</a></li>
<li><a href="/recruit/requirements/" target="_blank">募集要項</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox oneColumn">
<dl class="accordionSp">
<dt>
<a class="pcOnly" href="/topic/">Topics</a>
<p class="spOnly">Topics</p>
</dt>
<dd>
<ul>
<li><a href="/topic/">最新情報</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox twoColumn">
<dl class="accordionSp">
<dt>
<a class="pcOnly" href="/seminar/">Seminars</a>
<p class="spOnly">Seminars</p>
</dt>
<dd>
<ul>
<li><a href="/seminar/">開催予定のセミナー</a></li>
<li><a href="/seminar/#moveOld">開催済みのセミナー</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox treeColumn">
<dl>
<dd>
<ul>
<li><a href="/privacypolicy/">個人情報保護への取り組み</a></li>
<li><a href="/isms/">ISMS/情報セキュリティへの取り組み</a></li>
<li><a href="/internal/">SPC管理業務における内部管理体制</a></li>
<li><a href="/compliance/">コンプライアンスへの取り組み</a></li>
</ul>
</dd>
</dl>
</div>
<div class="listBox fourColumn">
<div class="primeLink">
<a class="alertPop" href="javascript:void(0);">
<img src="/images/common/logo_primeglobal.png" srcset="/images/common/logo_primeglobal@2x.png 2x, /images/common/logo_primeglobal@3x.png 3x"/>
<p>Aoyama Sogo Accounting Firm Co. Ltd. <br/>an independent member firm of PrimeGlobal</p>
</a>
</div>
</div>
</div>
<div class="topLinkLayout">
<div class="jukuBox">
<a href="/juku/">青山綜合経営塾(青山塾)<span>Aoyama Sogo Management School</span></a>
</div>
<hr/>
<div class="companyInfoBox">
<a href="/pdf/company_information.pdf" target="_blank">会社案内 <span class="small">ダウンロード</span><span>Download the <br class="tabOnly"/>Company Profile</span></a>
</div>
<div class="contactBox">
<a href="/contact/">お問い合わせ<span>Contact Us</span></a>
</div>
</div>
<div class="bottomLinkLayout">
<div class="firmBox">
<div class="box">
<img alt="" src="/images/common/footer_logo_01.svg"/>
</div>
</div>
<div class="corporationBox">
<div class="box">
<img alt="" src="/images/common/footer_logo_02.svg"/>
</div>
</div>
</div>
</div>
<div class="copy">
<p>
      Copyright © Aoyama Sogo Accounting Firm Co., Ltd. All Right Reserved.<br/> コンテンツの無断引用・無断転載およびホームページへの無断リンクを禁じます.
    </p>
</div>
</footer>
<script src="/js/old/pri.js" type="text/javascript"></script>
<!--▼ Google Analytics ▼-->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-9120889-1', 'auto');
		ga('set', 'dimension1', '218.219.247.203');/*IPアドレス取得用に追加　2015/07/07 */
		ga('require', 'displayfeatures'); /*性年代/興味関心レポート取得用に追加　2015/07/09 */
  ga('send', 'pageview');

</script>
<!--▲ Google Analytics ▲-->
<script src="/js/main.js"></script>
</body>
</html>