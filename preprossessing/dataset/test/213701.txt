<!DOCTYPE html>
<html lang="en">
<head>
<title>Sports Betting Information - news, statistics &amp; predictions</title>
<meta content="News, statistics, predictions and other sports betting information for all major sports and leagues" name="description"/>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="https://www.betinf.com/" rel="canonical"/>
<link href="s3.css" rel="stylesheet"/>
<script src="s/s3.js"></script>
</head>
<body>
<div id="wrap">
<header id="hdr"><a href="https://www.betinf.com/">bet<span>inf.com</span></a> <span>· SPORTS BETTING INFORMATION</span></header>
<main id="content">
<h1>Sports Betting Information - news, stats &amp; predictions</h1>
<div class="grp" id="main">
<div id="coll">
<section class="d1">
<span class="lrb"><span class="aup" id="expn-c"></span> <span class="adown" id="expn-e"></span></span>
<div class="x m">
<ul id="news">
<li class="selected"><a href="#newsa">football</a></li>
<li><a href="#newsb">other sports</a></li>
</ul>
</div>
<div id="expn">
<div id="newsa">
<h3>Dennis Praet out for three months</h3>
<div><p class="badges"><span>14.1.21</span> <a href="england.htm#Leicester">Premier League: Leicester</a> <a href="england_injured.htm">Injuries</a></p>
<p>Leicester City midfielder Dennis Praet will be out for three months after picking up a hamstring injury against Stoke in the FA Cup. The 26-year-old pulled up in the closing stages of Saturday victory. "It was not quite his hamstring but a tendon in the hamstring," manager Brendan Rodgers said. "It was an awkward one for him, the ball just popped up and he over-stretched."</p></div>
<h3>Jovic returns to Frankfurt</h3>
<div><p class="badges"><span>14.1.21</span> <a href="germany.htm#Frankfurt">Bundesliga: Frankfurt</a> <a href="germany_transfer.htm">Transfers</a></p>
<p>Luka Jovic has returned to Eintracht Frankfurt on loan from Real Madrid for the remainder of the season. The 21-year-old joined Real in July 2019 on the back of a 27-goal campaign for Frankfurt. Two goals in 32 appearances in the Spanish capital have left the Serbian yearning for more game-time, with a return to the Eagles providing the perfect solution.</p></div>
<h3>Atletico Madrid sign Dembele on loan</h3>
<div><p class="badges"><span>14.1.21</span> <a href="spain.htm#Atletico+M.">La Liga: Atletico Madrid</a> <a href="spain_transfer.htm">Transfers</a></p>
<p>Atletico Madrid have confirmed the signing of the Lyon forward Moussa Dembele on an initial loan deal. The French forward scored 45 goals in all competitions for Lyon after leaving Celtic in September 2018. He arrives as a replacement for Diego Costa, who left Atletico earlier this month. Lyon, meanwhile, moved quickly to reinforce their attacking ranks by signing Leicester City forward Islam Slimani.</p></div>
<h3>Piazon joins Braga on a permanent deal</h3>
<div><p class="badges"><span>14.1.21</span> <a href="portugal.htm#Braga">Portuguese Super Liga: Braga</a> <a href="portugal_transfer.htm">Transfers</a></p>
<p>Attacking midfielder Lucas Piazon has signed for Braga on a four-year contract, after amassing seven loan stints in his 10 years as a Chelsea player. The 26-year-old made just three senior appearances for the Blues, and all in 2012. He has had temporary spells at Malaga, Vitesse Arnhem, Eintracht Frankfurt, Reading, Fulham, Chievo and Rio Ave.</p></div>
<h3>Abelardo returns to Alaves</h3>
<div><p class="badges"><span>14.1.21</span> <a href="spain.htm#Alaves">La Liga: Alaves</a></p>
<p>Alaves sacked Pablo Machin after losing 3-1 at Cadiz on Sunday and replaced him with their former coach Abelardo Fernandez. Abelardo, who spent the final season of his playing career at Alaves, became coach in December 2017 and successfully steered them away from the relegation zone. His last job was with Espanyol, who sacked him last June.</p></div>
<h3>Pacheta named new Huesca coach</h3>
<div><p class="badges"><span>14.1.21</span> <a href="spain.htm#Huesca">La Liga: Huesca</a></p>
<p>La Liga strugglers Huesca have appointed Jose Rojo Martin "Pacheta" as their new coach. Pacheta replaces Miguel Angel Sanchez "Michel" who was sacked after Monday's 2-0 defeat to Betis. Despite leading Elche to a surprise promotion to the top flight, his contract was not extended by the club and Pacheca has been out of work since the end of last season.</p></div>
<h3>Neymar scores on return from injury</h3>
<div><p class="badges"><span>14.1.21</span> <a href="france.htm#PSG">Ligue 1: Paris SG</a></p>
<p>Neymar scored on his return from injury as Paris Saint-Germain beat Marseille 2-1 to win the French Super Cup in Lens on Wednesday. The Brazil forward, who had been out of action since mid-December with an ankle injury, netted from the penalty spot in the second half to add to Mauro Icardi's first-half opener. Neymar replaced Angel Di Maria in the 65th minute for his first appearance in 2021.</p></div>
<h3>Fosu-Mensah moves to Leverkusen</h3>
<div><p class="badges"><span>13.1.21</span> <a href="germany.htm#Bayer+L.">Bundesliga: Bayer Leverkusen</a> <a href="germany_transfer.htm">Transfers</a></p>
<p>Bayer Leverkusen have signed Netherlands international defender Timothy Fosu-Mensah from Manchester United. Fosu-Mensah has agreed a three-year contract, ending a six-year stay at United. The 23-year-old made his professional debut with United in February 2016 and went on to make 30 appearances for the club in all competitions as well as a further 33 Premier League outings over the course of loan spells at both Crystal Palace and Fulham.</p></div>
<h3>Messi likely to miss Super Cup clash</h3>
<div><p class="badges"><span>13.1.21</span> <a href="spain.htm#Barcelona">La Liga: Barcelona</a></p>
<p>Barcelona could be without Lionel Messi for Spanish Super Cup clash against Real Sociedad tonight, according to reports. The 33-year-old missed Wednesday's training session due to a minor injury that he suffered against Granada.</p></div>
<h3>Sensi injured in warmup</h3>
<div><p class="badges"><span>13.1.21</span> <a href="italy.htm#Inter">Serie A: Inter</a> <a href="italy_injured.htm">Injuries</a></p>
<p>Stefano Sensi has picked up an injury in the warm-up for Inter's match against Fiorentina. The midfielder was supposed to make his first start for the Nerazzurri since 30 September at the Stadio Artemio Franchi. Instead, he will once again be missing for Antonio Conte's side and it remains to be seen how serious the new problem is.</p></div>
<h3>Lo Celso out for 'a few weeks'</h3>
<div><p class="badges"><span>13.1.21</span> <a href="england.htm#Tottenham">Premier League: Tottenham</a> <a href="england_injured.htm">Injuries</a></p>
<p>Tottenham manager Jose Mourinho has confirmed that Giovani Lo Celso has picked up an injury and will be out for 'a few weeks'. There were reports early on in January that Lo Celso was set to miss the rest of the month. But from Mourinho's latest words it seems that he could be out for even longer than that, after he picked up an issue against Leicester.</p></div>
<h3>Trezeguet tests positive for Covid-19</h3>
<div><p class="badges"><span>13.1.21</span> <a href="england.htm#Aston+Villa">Premier League: Aston Villa</a> <a href="england_injured.htm">Injuries</a></p>
<p>Winger Mahmoud Trezeguet is among a host of Aston Villa players to contract COVID-19. "After needing 6 stitches due to a head injury, I suffered a [muscle] tear and a hamstring injury in quick succession and I was absent for a month," Trezeguet said on Twitter on Tuesday. "And then after rehabilitation and after my return to training, I was infected with the coronavirus. Thanks God for everything."</p>
<p>A recent outbreak of coronavirus cases at Villa saw nine first team players and five staff members test positive.</p></div>
</div>
<div class="off" id="newsb">
<h3>Edwards-Helaire returns to practice</h3>
<div><p class="badges"><span>14.1.21</span> <a href="nfl.htm#Kansas">NFL: Kansas City Chiefs</a> <a href="nfl_injured.htm">Injuries</a></p>
<p>The Kansas City Chiefs could have running back Clyde Edwards-Helaire for Sunday's divisional-round playoff game against the Cleveland Browns. Edwards-Helaire returned to practice Wednesday for the first time since injuring his ankle and hip in a Week 15 game against the New Orleans Saints. He was a limited practice participant.</p></div>
<h3>Eagles fire head coach Doug Pederson</h3>
<div><p class="badges"><span>12.1.21</span> <a href="nfl.htm#Philadel.">NFL: Philadelphia Eagles</a></p>
<p>Doug Pederson lost his job less than three years after he led the Philadelphia Eagles to the franchise's only Super Bowl title. Pederson was 42-37-1 in five seasons. He guided the Eagles to two division championships and three playoff appearances before going 4-11-1 in 2020. Owner Jeffrey Lurie made the decision after meeting with Pederson last week and again Monday.</p></div>
<hr/>
<h3>Zion Williamson out vs. Clippers</h3>
<div><p class="badges"><span>14.1.21</span> <a href="nba.htm#New+Orl.">NBA: New Orleans Pelicans</a> <a href="nba_injured.htm">Injuries</a></p>
<p>Zion Williamson did not play in Wednesday night's 111-106 loss to the LA Clippers because of the league's health and safety protocols, the New Orleans Pelicans announced. It is unclear whether Williamson will miss more than one game.</p>
<p>"He had inconclusive [COVID-19 test] results and the timing became a factor," coach Stan Van Gundy said. "He's not with us here at the game tonight. He's back in his hotel room. We'll know more as further results come back."</p></div>
<h3>Rockets trade Harden to Brooklyn</h3>
<div><p class="badges"><span>13.1.21</span> <a href="nba.htm#Houston">NBA: Houston Rockets</a> <a href="nba_transfer.htm">Transfers</a></p>
<p>The Houston Rockets have agreed to send James Harden to the Brooklyn Nets, according to ESPN's Adrian Wojnarowski. Houston will acquire Victor Oladipo, Dante Exum, Rodions Kurucs, four first-round picks and four first-round pick swaps in the four-team deal, which also includes the Indiana Pacers and Cleveland Cavaliers. Jarrett Allen and Taurean Prince will be sent from Brooklyn to Cleveland. In return for Oladipo, the Pacers will receive Caris LeVert and a second-round pick.</p></div>
<hr/>
<h3>Stars' opening games postponed</h3>
<div><p class="badges"><span>13.1.21</span> <a href="nhl.htm#Dallas">NHL: Dallas Stars</a></p>
<p>The NHL announced that 27 players had confirmed positive COVID-19 tests during training camps, which concluded Tuesday. Of those players, 17 were members of the Dallas Stars, who shut down their training facilities last Friday after an outbreak. Dallas' first three games have been postponed.</p></div>
</div>
</div>
</section>
</div>
<div id="colr">
<section class="d1">
<span class="lrb"><a href="football_predictions.htm"> ... </a></span>
<h2>High strength difference - football</h2>
<table class="tbl tblr"><col style="width:55%"/><col style="width:45%"/>
<tr><td class="pc"><span class="badge">15</span><a href="portugal.htm#Sporting-Rio+Ave">Sporting - Rio Ave</a></td><td><div class="gl" style="width:55%"></div><div class="gp" style="width:15%"></div></td></tr>
<tr><td class="pc"><span class="badge">17</span><a href="germany.htm#Frankfurt-Schalke">Frankfurt - Schalke</a></td><td><div class="gl" style="width:51%"></div><div class="gp" style="width:19%"></div></td></tr>
<tr><td class="pc"><span class="badge">16</span><a href="germany.htm#Dortmund-Mainz">Dortmund - Mainz</a></td><td><div class="gl" style="width:50%"></div><div class="gp" style="width:20%"></div></td></tr>
</table>
</section>
<section class="d1">
<h2>Injured and suspended players</h2>
<div class="x m">
<ul id="inj">
<li class="selected"><a href="#ieng">England</a></li>
<li><a href="#iger">Germany</a></li>
<li><a href="#ifra">France</a></li>
<li><a href="#iita">Italy</a></li>
<li><a href="#ispa">Spain</a></li>
</ul>
</div>
<div id="ieng"><ul class="list"><li><a href="england_injured.htm#Everton">Abdoulaye Doucoure (Everton)</a></li><li><a href="england_injured.htm#Fulham">Mario Lemina (Fulham)</a></li><li><a href="england_injured.htm#Fulham">Aleksandar Mitrovic (Fulham)</a></li><li><a href="england_injured.htm#Fulham">Ruben Loftus-Cheek (Fulham)</a></li></ul></div>
<div class="off" id="iger"><ul class="list"><li><a href="germany_injured.htm#Bayern">Leon Goretzka (Bayern)</a></li><li><a href="germany_injured.htm#Arminia">Manuel Prietl (Arminia)</a></li><li><a href="germany_injured.htm#Werder">Ludwig Augustinsson (Werder)</a></li><li><a href="germany_injured.htm#Hertha">Marvin Plattenhardt (Hertha)</a></li></ul></div>
<div class="off" id="ifra"><ul class="list"><li><a href="france_injured.htm#Dijon">Pape Cheikh Diop (Dijon)</a></li><li><a href="france_injured.htm#Paris+SG">Thilo Kehrer (Paris SG)</a></li><li><a href="france_injured.htm#Paris+SG">Idrissa Gueye (Paris SG)</a></li><li><a href="france_injured.htm#Bordeaux">Mehdi Zerkane (Bordeaux)</a></li></ul></div>
<div class="off" id="iita"><ul class="list"><li><a href="italy_injured.htm#Inter">Stefano Sensi (Inter)</a></li><li><a href="italy_injured.htm#Lazio">Danilo Cataldi (Lazio)</a></li><li><a href="italy_injured.htm#Lazio">Djavan Anderson (Lazio)</a></li><li><a href="italy_injured.htm#Fiorentina">Borja Valero (Fiorentina)</a></li></ul></div>
<div class="off" id="ispa"><ul class="list"><li><a href="spain_injured.htm#Atletico+Madrid">Moussa Dembele (Atletico Madrid)</a></li><li><a href="spain_injured.htm#Betis">Dani Martin (Betis)</a></li><li><a href="spain_injured.htm#Barcelona">Lionel Messi (Barcelona)</a></li><li><a href="spain_injured.htm#Granada">Maxime Gonalons (Granada)</a></li></ul></div>
<p></p>
<div class="x m">
<ul id="ainj">
<li class="selected"><a href="#inba">NBA</a></li>
<li><a href="#inhl">NHL</a></li>
<li><a href="#infl">NFL</a></li>
<li><a href="#imlb">MLB</a></li>
</ul>
</div>
<div id="inba"><ul class="list"><li><a href="nba_injured.htm#New+Orleans+Pelicans">Lonzo Ball (New Orleans Pelicans)</a></li><li><a href="nba_injured.htm#New+Orleans+Pelicans">Zion Williamson (New Orleans Pelicans)</a></li><li><a href="nba_injured.htm#New+Orleans+Pelicans">Eric Bledsoe (New Orleans Pelicans)</a></li><li><a href="nba_injured.htm#Portland+Trail+Blazers">Keljin Blevins (Portland Trail Blazers)</a></li></ul></div>
<div class="off" id="inhl"><ul class="list"><li><a href="nhl_injured.htm#Winnipeg+Jets">Marko Dano (Winnipeg Jets)</a></li><li><a href="nhl_injured.htm#Florida+Panthers">Juho Lammikko (Florida Panthers)</a></li><li><a href="nhl_injured.htm#San+Jose+Sharks">Radim Simek (San Jose Sharks)</a></li><li><a href="nhl_injured.htm#San+Jose+Sharks">Maxim Letunov (San Jose Sharks)</a></li></ul></div>
<div class="off" id="infl"><ul class="list"><li><a href="nfl_injured.htm#Baltimore+Ravens">Davontae Harris (Baltimore Ravens)</a></li><li><a href="nfl_injured.htm#Buffalo+Bills">Zack Moss (Buffalo Bills)</a></li><li><a href="nfl_injured.htm#Cleveland+Browns">David Njoku (Cleveland Browns)</a></li><li><a href="nfl_injured.htm#Cleveland+Browns">Robert Jackson (Cleveland Browns)</a></li></ul></div>
<div class="off" id="imlb"></div>
</section>
<section class="d1">
<span class="lrb"><a href="football_transfers.htm"> ... </a></span>
<h2>Latest transfers - football<br/><span>(value in m€)</span></h2>
<table class="tbl tblr1">
<tr><td><span class="fr badge">Jan '21</span><a href="spain_transfer.htm#Atletico+Madrid">Moussa Dembele to Atletico Madrid</a></td><td>1.5</td></tr>
<tr><td><a href="netherland_transfer.htm#Heerenveen">Rami Kaib to Heerenveen</a></td><td>0.5</td></tr>
<tr><td><a href="russia_transfer.htm#Akhmat+Grozny">Gabriel Iancu to Akhmat Grozny</a></td><td>0.5</td></tr>
<tr><td><a href="russia_transfer.htm#Rostov">Tomas Rukas to Rostov</a></td><td>0.1</td></tr>
<tr><td><a href="france_transfer.htm#Lyon">Islam Slimani to Lyon</a></td><td>-</td></tr>
</table>
</section>
<section class="d1">
<h2>Stats in last 6 games - football</h2>
<div class="x m">
<ul id="st6">
<li class="selected"><a href="#sbest">best</a></li>
<li><a href="#sworst">worst</a></li>
</ul>
</div>
<div id="sbest">
<table class="tbl tblr1"><tr><td><a href="portugal.htm#Porto">Porto (POR)</a></td><td>6-0-0</td></tr>
<tr><td><a href="scott.htm#Livingston">Livingston (SCO)</a></td><td>6-0-0</td></tr>
<tr><td><a href="scott.htm#Rangers">Rangers (SCO)</a></td><td>6-0-0</td></tr>
<tr><td><a href="germany.htm#Freiburg">Freiburg (GER)</a></td><td>5-1-0</td></tr>
<tr><td><a href="england.htm#Man+Utd">Manchester United (ENG)</a></td><td>5-1-0</td></tr>
</table>
</div>
<div class="off" id="sworst">
<table class="tbl tblr1"><tr><td><a href="france.htm#Nimes">Nimes (FRA)</a></td><td>0-1-5</td></tr>
<tr><td><a href="germany.htm#Mainz">Mainz (GER)</a></td><td>0-1-5</td></tr>
<tr><td><a href="italy.htm#Parma">Parma (ITA)</a></td><td>0-1-5</td></tr>
<tr><td><a href="belgium.htm#Cercle">Cercle Brugge (BEL)</a></td><td>0-1-5</td></tr>
<tr><td><a href="netherland.htm#Willem+II">Willem II (NET)</a></td><td>0-1-5</td></tr>
</table>
</div>
</section>
<section class="d1">
<h2>Notable streaks</h2>
<ul class="list">
<li><a href="scott_stats.htm">Rangers won in 15 games</a></li>
<li><a href="scott_stats.htm">Rangers not defeated in 23 games</a></li>
<li><a href="belgium_stats.htm">Leuven scored in 20 games</a></li>
<li><a href="belgium_stats.htm">Beerschot conceded in 17 games</a></li>
</ul>
</section>
<section class="d1">
<h2>Latest previews</h2>
<ul class="list">
<li><a href="prev_nhl.htm">NHL 2020/2021</a></li>
<li><a href="prev_nba.htm">NBA 2020/2021</a></li>
<li><a href="prev_italy.htm">Italian Serie A 2020/21</a></li>
<li><a href="prev_germany.htm">German Bundesliga 2020/21</a></li>
<li><a href="prev_spain.htm">Spanish La Liga 2020/21</a></li>
<li><a href="prev_england.htm">English Premiership 2020/21</a></li>
<li><a href="prev_france.htm">French Ligue 1 2020/21</a></li>
</ul>
<h2>Predictions</h2>
<ul class="list">
<li><a href="football_predictions.htm">Predictions for major football leagues</a></li>
</ul>
<h2>Transfers</h2>
<ul class="list">
<li><a href="football_transfers.htm">Transfers in major football leagues</a></li>
<li><a href="england_transfer.htm">EPL transfers</a></li>
<li><a href="france_transfer.htm">Ligue 1 transfers</a></li>
<li><a href="germany_transfer.htm">Bundesliga transfers</a></li>
<li><a href="italy_transfer.htm">Serie A transfers</a></li>
<li><a href="spain_transfer.htm">La Liga transfers</a></li>
</ul>
</section>
</div>
</div>
<section id="info">
<h2>About us</h2>
<p>If you looking for a sports betting information or want make smart bet without difficult and time consuming analysis - you are at right place. If this is your first visit, right way to start is to <a href="howchart.htm">read our help page</a>.</p>
<p>This site exposes everything you need to know for successful betting in easy to understand and compact form:</p>
<ul>
<li>We use charts instead of numbers whenever it's possible. Just quick peek on charts and you have knowledge of an expert.</li>
<li>All important information about one league is placed on one page. It's much easier to make right decision if all relevant data is put together in one place. Collecting fragments of information across the site is annoying and ineffective.</li>
<li>On this site you can find only relevant sports betting information - no unimportant news, no trivial info or any other wasters of your time and Internet bandwidth.</li>
<li>Predictions and analysis are not manipulated by anyone in any way. Every estimation or prediction is based on exact data, analyzed, processed and produced by specially designed computer software.</li>
</ul>
<p>If you have any comments or suggestions, send e-mail to <a href="mailto:info@betinf.com">info@betinf.com</a></p>
</section>
</main>
<a aria-label="main menu" href="#nav" id="navico"><span></span></a>
<nav id="nav1"><ul class="navs"><li><a href="articles/index.htm">articles</a></li><li><a href="football_predictions.htm">predictions</a></li><li><a href="football_transfers.htm">transfers</a></li></ul></nav>
<nav id="nav0"><ul class="navs"><li><a href="https://www.betinf.com/">home</a></li><li><a href="https://www.betinf.com/sitemap.htm">site map</a></li><li><a href="howchart.htm">help</a></li><li><a href="links.htm">links</a></li></ul></nav>
<nav id="nav">
<ul>
<li class="active"><b>football</b>
<a href="england.htm">England</a> <a href="france.htm">France</a> <a href="germany.htm">Germany</a> <a href="italy.htm">Italy</a>
<a href="spain.htm">Spain</a> <a href="portugal.htm">Portugal</a> <a href="netherland.htm">Netherlands</a> <a href="russia.htm">Russia</a>
<a href="belgium.htm">Belgium</a> <a href="scott.htm">Scotland</a> <a href="denmark.htm">Denmark</a> <a href="turkey.htm">Turkey</a>
</li>
<li><b>more football</b>
<a href="greece.htm">Greece</a> <a href="austria.htm">Austria</a> <a href="swiss.htm">Switzerland</a> <a href="croatia.htm">Croatia</a>
<a href="sweden.htm">Sweden</a> <a href="norway.htm">Norway</a> <a href="ireland.htm">Ireland</a> <a href="mls.htm">MLS</a>
<a href="champ.htm">Champions League</a> <a href="nations.htm">Nations League</a> <a href="euro_2020.htm">Euro 2020</a>
</li>
<li class="last"><b>more sports</b>
<a href="nba.htm">NBA</a> <a href="nhl.htm">NHL</a> <a href="nfl.htm">NFL</a> <a href="mlb.htm">MLB</a>
<a href="wnba.htm">WNBA</a> <a href="eurob.htm">Basket Euroleague</a> <a href="f1.htm">Formula 1</a> <a href="atp.htm">Tennis</a>
</li>
</ul>
</nav>
<div class="advc" id="bm"><p><a href="https://casinocanada.com/"><img alt="casinocanada.com" src="i/casinocanada.png"/></a></p><p>All the best offers at <a href="https://casinodeal.co.uk/">CasinoDeal.co.uk</a></p><p>The best informations on <a href="http://betting-preview.com">betting-preview.com</a></p><p>Check out this guide to the best <a href="https://www.casivo.co.uk/">casino sites</a> online</p><p>Find the most popular online slots at <a href="https://slotcatalog.com/en/Top-100">SlotCatalog.com</a></p><p>Find the best PayPal betting sites at <a href="https://www.onlinesportsbettingsites.uk/paypal/">onlinesportsbettingsites.uk</a></p><p>Find top rated UK casinos at <a href="https://www.bestukcasinos.co.uk/casinos/top-rated/">BestUKCasinos.co.uk</a></p></div>
<footer id="ftr"><p>Copyright © 2003-2021 www.betinf.com.<br/>All rights reserved.</p></footer>
</div>
<script>pinit({e:'xn',t:'news|inj|ainj|st6'});</script>
</body>
</html>
