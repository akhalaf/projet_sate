<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://gmpg.org/xfn/11" rel="profile"/>
<title>Sinister Sabotage – Did someone say destruction?</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://sinistersabotage.com/feed/" rel="alternate" title="Sinister Sabotage » Feed" type="application/rss+xml"/>
<link href="https://sinistersabotage.com/comments/feed/" rel="alternate" title="Sinister Sabotage » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/sinistersabotage.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://sinistersabotage.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sinistersabotage.com/wp-content/themes/hero/style.css?ver=5.4.4" id="hero-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sinistersabotage.com/wp-content/themes/hero/css/flexslider.css?ver=5.4.4" id="flexslider-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Lato&amp;ver=5.4.4" id="hero-google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sinistersabotage.com/wp-content/themes/hero/css/bootstrap.css?ver=5.4.4" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://sinistersabotage.com/wp-content/themes/hero/fonts/font-awesome/css/font-awesome.min.css?ver=4.7.0" id="hero-font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://sinistersabotage.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://sinistersabotage.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<link href="https://sinistersabotage.com/wp-json/" rel="https://api.w.org/"/>
<link href="https://sinistersabotage.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://sinistersabotage.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.4.4" name="generator"/>
<link href="https://sinistersabotage.com/" rel="canonical"/>
<link href="https://sinistersabotage.com/" rel="shortlink"/>
<link href="https://sinistersabotage.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsinistersabotage.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://sinistersabotage.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsinistersabotage.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
</head>
<body class="home page-template-default page page-id-42 wp-custom-logo no-sidebar">
<div class="site boxed" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="container">
<div class="site-branding">
<a class="custom-logo-link" href="https://sinistersabotage.com/" rel="home"><img alt="Sinister Sabotage" class="custom-logo" height="110" src="https://sinistersabotage.com/wp-content/uploads/2020/04/SinSab.gif" width="240"/></a> </div><!-- .site-branding -->
<!--top menu-->
<div id="menu_container">
<nav class="main-navigation clearfix" id="site-navigation">
<h3 class="menu-toggle">Menu</h3>
<ul><li><a href="https://sinistersabotage.com/">Home</a></li><li><a href="https://sinistersabotage.com/battlefield-1942/">BATTLEFIELD 1942</a></li><li><a href="https://sinistersabotage.com/meekskin/">Meekskin</a></li><li><a href="https://sinistersabotage.com/">Meekskin 4K</a></li></ul> </nav><!-- #site-navigation -->
<div class="clear"></div>
</div>
</div><!-- .container -->
</header><!-- #masthead -->
<div class="custom-header-content">
<div class="container">
<h1>Meekskin 4K</h1>
</div>
</div>
<div class="site-content" id="content">
<div class="container">
<div class="row">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<article class="post-42 page type-page status-publish hentry" id="post-42">
<div class="entry-content">
<p class="has-huge-font-size"><strong><span class="has-inline-color has-vivid-green-cyan-color"><a href="https://sinistersabotage.com/wp-content/uploads/2020/04/Meekskin_4K.zip">DOWNLOAD</a></span></strong></p>
<p>Meekskin is a Shadowbane interface skin. It modifies the chat, status bars, group windows, track window, etc., for a simpler and less obstructive gaming experience. You can also create your own status/selection window by arranging your health/stam/mana/xp bars, nation/guild crest, target name/rank/hp.</p>
<p>4/4/2020 – 0.91 beta – First test release</p>
<p><span class="has-inline-color" style="color:#ff0000">What’s in the 0.91 beta?</span><br/>It’s the original Meekskin but larger for higher resolution gameplay.</p>
<ul><li>Modified the default color (looking for feedback or new color suggestions).</li><li>New Arcade 2.0 status bar.</li><li>New SWAHbar (Selection Window and Hotbar) window. This bar is more newbie friendly.</li><li>Restored the original Selection style windows and renamed the default ‘Selection’ window in Meekskin to iSelection.</li><li>Added ‘Chat Window Max’ to maximize usage of window. Only one special type chat window of each option can be use. Multiple instances of the original ‘Chat Window’ can be opened.</li><li>The Inventory shortcut in the Quick Bar Vertical window now opens the correct window (oops).</li><li>Switched the ‘eQuip Inventory’ window to horizontal display from vertical.</li><li>Group window: changed the buttons to icons and moved them to the bottom. They dissapear when transparency is increased.</li></ul>
<p><span class="has-inline-color" style="color:#fb0c0c">How do I install?</span><br/>Before launching Shadowbane:</p>
<ol><li>Download Meekskin_4K.zip, do not extract, to the ‘Skin’ folder in your Shadowbane directory.</li><li>Launch Sb</li><li>While in-game press escape to bring up the main menu. Click ‘Settings &gt; Interface Skin &gt; “Meekskin_4K”<br/>Once loaded you can hit escape and go to Meekskin for some added functions.</li></ol>
<p></p>
<p><span class="has-inline-color" style="color:#ff0000">What if I find a mistake or error?</span><br/>If you find a mistake/error please contact me via Discord [Meek#2900] or meek [at] sinistersabotage.com.</p>
<p>The Pet window makes Shadowbane crash whenever I push the buttons.<br/>It’s a known bug in Shadowbane and not the skin itself. You must have an active pet or else it will crash.</p>
</div><!-- .entry-content -->
</article><!-- #post-42 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .inner-wrapper -->
</div><!-- .container -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="container">
</div><!-- .container -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script src="https://sinistersabotage.com/wp-content/themes/hero/js/jquery.flexslider.js?ver=2.1" type="text/javascript"></script>
<script src="https://sinistersabotage.com/wp-content/themes/hero/js/jquery.custom.js?ver=1.0" type="text/javascript"></script>
<script src="https://sinistersabotage.com/wp-content/themes/hero/js/navigation.js?ver=20151215" type="text/javascript"></script>
<script src="https://sinistersabotage.com/wp-content/themes/hero/js/skip-link-focus-fix.js?ver=20151215" type="text/javascript"></script>
<script src="https://sinistersabotage.com/wp-includes/js/wp-embed.min.js?ver=5.4.4" type="text/javascript"></script>
</body>
</html>
