<!DOCTYPE html>
<html lang="fr-FR">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<title>Page non trouvée – Etat-civil</title>
<meta content="noindex,nofollow" name="robots"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://vdr99.valdereuil.fr/feed/" rel="alternate" title="Etat-civil » Flux" type="application/rss+xml"/>
<link href="https://vdr99.valdereuil.fr/comments/feed/" rel="alternate" title="Etat-civil » Flux des commentaires" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/vdr99.valdereuil.fr\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://vdr99.valdereuil.fr/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Gentium+Basic%3A700%7CGentium+Book+Basic%3A400%2C400italic%2C700%7CSlabo+27px%3A400&amp;subset=latin%2Clatin-ext" id="write-font-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/genericons/genericons.css?ver=3.4.1" id="genericons-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/css/normalize.css?ver=8.0.0" id="normalize-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/style.css?ver=2.1.2" id="write-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/themes/write/css/drawer.css?ver=3.2.2" id="drawer-style-css" media="screen and (max-width: 782px)" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/sln-bootstrap.css?ver=20201028" id="salon-bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/select2.min.css?scope=sln&amp;ver=4.9.3" id="salon-admin-select2-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/booking-calendar-shortcode/css/style.css?ver=20201028" id="salon-booking-calendar-shortcode-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/css/salon.css?ver=20201028" id="salon-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://vdr99.valdereuil.fr/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="iscroll-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/iscroll.js?ver=5.2.0" type="text/javascript"></script>
<script id="drawer-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/drawer.js?ver=3.2.2" type="text/javascript"></script>
<script id="salon-admin-select2-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/select2.min.js?scope=sln&amp;ver=1" type="text/javascript"></script>
<link href="https://vdr99.valdereuil.fr/wp-json/" rel="https://api.w.org/"/><link href="https://vdr99.valdereuil.fr/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://vdr99.valdereuil.fr/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<style type="text/css">
		/* Colors */
				
			</style>
</head>
<body class="error404 sln-salon-page drawer header-side footer-side full-width footer-0">
<div class="hfeed site" id="page">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header" id="masthead">
<div class="site-top">
<div class="site-top-table">
<div class="site-branding">
<div class="site-title"><a href="https://vdr99.valdereuil.fr/" rel="home">Etat-civil</a></div>
<div class="site-description">Gestion des rendez-vous de papiers d'identité</div>
</div><!-- .site-branding -->
<nav class="main-navigation" id="site-navigation">
<button class="drawer-toggle drawer-hamburger">
<span class="screen-reader-text">Menu</span>
<span class="drawer-hamburger-icon"></span>
</button>
<div class="drawer-nav">
<div class="drawer-content">
<div class="drawer-content-inner">
<div class="menu-navig-container"><ul class="menu" id="menu-navig"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-26" id="menu-item-26"><a href="https://vdr99.valdereuil.fr">Accueil</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-25" id="menu-item-25"><a href="https://vdr99.valdereuil.fr/">Gestion des rendez-vous</a></li>
</ul></div> <form action="https://vdr99.valdereuil.fr/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Rechercher :</span>
<input class="search-field" name="s" placeholder="Rechercher…" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Rechercher"/>
</form> </div><!-- .drawer-content-inner -->
</div><!-- .drawer-content -->
</div><!-- .drawer-nav -->
</nav><!-- #site-navigation -->
</div><!-- .site-top-table -->
</div><!-- .site-top -->
</header><!-- #masthead -->
<div class="site-content" id="content">
<div class="content-area" id="primary">
<main class="site-main" id="main">
<section class="error-404 not-found">
<header class="page-header">
<h1 class="page-title">That page can’t be found.</h1>
</header><!-- .page-header -->
<div class="page-content">
<p>It looks like nothing was found at this location. Maybe try a search?</p>
<form action="https://vdr99.valdereuil.fr/" class="search-form" method="get" role="search">
<label>
<span class="screen-reader-text">Rechercher :</span>
<input class="search-field" name="s" placeholder="Rechercher…" type="search" value=""/>
</label>
<input class="search-submit" type="submit" value="Rechercher"/>
</form>
</div><!-- .page-content -->
</section><!-- .error-404 -->
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->
<footer class="site-footer" id="colophon">
<div class="site-bottom">
<div class="site-bottom-table">
<nav class="footer-social-link social-link" id="footer-social-link">
</nav><!-- #footer-social-link -->
<div class="site-info">
<div class="site-credit">
						Powered by <a href="https://wordpress.org/">WordPress</a> <span class="site-credit-sep"> | </span>
						Theme: <a href="http://themegraphy.com/wordpress-themes/write/">Write</a> by Themegraphy					</div><!-- .site-credit -->
</div><!-- .site-info -->
</div><!-- .site-bottom-table -->
</div><!-- .site-bottom -->
</footer><!-- #colophon -->
</div><!-- #page -->
<script id="write-skip-link-focus-fix-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/skip-link-focus-fix.js?ver=20160525" type="text/javascript"></script>
<script id="double-tap-to-go-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/doubletaptogo.js?ver=1.0.0" type="text/javascript"></script>
<script id="write-functions-js" src="https://vdr99.valdereuil.fr/wp-content/themes/write/js/functions.js?ver=20190226" type="text/javascript"></script>
<script id="salon-raty-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/jquery.raty.js?ver=20201028" type="text/javascript"></script>
<script id="salon-js-extra" type="text/javascript">
/* <![CDATA[ */
var salon = {"ajax_url":"https:\/\/vdr99.valdereuil.fr\/wp-admin\/admin-ajax.php?lang=fr","ajax_nonce":"5c4d13776b","loading":"https:\/\/vdr99.valdereuil.fr\/wp-content\/plugins\/salon-booking-system\/img\/preloader.gif","txt_validating":"v\u00e9rification de la disponibilit\u00e9","images_folder":"https:\/\/vdr99.valdereuil.fr\/wp-content\/plugins\/salon-booking-system\/img","confirm_cancellation_text":"Voulez-vous vraiment annuler ?","time_format":"hh:ii","has_stockholm_transition":"no","checkout_field_placeholder":"remplissez ce champ","txt_close":"Ferm\u00e9"};
/* ]]> */
</script>
<script id="salon-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/salon.js?ver=20201028" type="text/javascript"></script>
<script id="salon-bootstrap-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/bootstrap.min.js?ver=20201028" type="text/javascript"></script>
<script id="smalot-datepicker-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/bootstrap-datetimepicker.js?ver=20140711" type="text/javascript"></script>
<script id="smalot-datepicker-lang-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/datepicker_language/bootstrap-datetimepicker.fr_FR.js?ver=2016-02-16" type="text/javascript"></script>
<script id="salon-customSelect2-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/admin/customSelect2.js?scope=sln&amp;ver=20201028" type="text/javascript"></script>
<script id="salon-my-account-js-extra" type="text/javascript">
/* <![CDATA[ */
var salonMyAccount_l10n = {"success":"Profile updated successfully."};
/* ]]> */
</script>
<script id="salon-my-account-js" src="https://vdr99.valdereuil.fr/wp-content/plugins/salon-booking-system/js/salon-my-account.js?ver=20201028" type="text/javascript"></script>
<script id="wp-embed-js" src="https://vdr99.valdereuil.fr/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>
