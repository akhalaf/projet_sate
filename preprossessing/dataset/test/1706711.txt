<!DOCTYPE html>
<html lang="id-ID">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<link href="https://www.yogyahost.com/wp-content/themes/phox/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<!-- wp-head -->
<title>Laman tidak ditemukan – YOGYA HOST</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://www.yogyahost.com/feed/" rel="alternate" title="YOGYA HOST » Feed" type="application/rss+xml"/>
<link href="https://www.yogyahost.com/comments/feed/" rel="alternate" title="YOGYA HOST » Umpan Komentar" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.yogyahost.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://www.yogyahost.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yogyahost.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yogyahost.com/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.23" id="rs-plugin-settings-css" media="all" rel="stylesheet" type="text/css"/>
<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link href="https://www.yogyahost.com/wp-content/themes/phox/assets/css/bootstrap.min.css?ver=1.6.6" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yogyahost.com/wp-content/themes/phox/assets/css/style.css?ver=1.6.6" id="wdes-style-css" media="all" rel="stylesheet" type="text/css"/>
<style id="wdes-style-inline-css" type="text/css">
/* ==============================================/* Color/* ============================================= *//* =======================/* Header/* ====================== */header{background:linear-gradient(-90deg,#ffffff 0,#ffffff 100%);border-bottom:1px solid rgba(0,0,0,0)}.sticky-header{border-bottom:1px solid #eeeeee}.wdes-interface-menu #wdes-menu-top-header ul.menu li a{color:#242424}.wdes-interface-menu #wdes-menu-top-header ul.menu li a:hover{color:#6c63ff}.clientarea-shortcuts a span{color:#242424}.clientarea-shortcuts a span:hover{color:#6c63ff}.clientarea-shortcuts{border-left:1px solid #eeeeee}.s-area span,.social li a{color:#242424}.social li a:hover{color:#6c63ff}.wdes-interface-menu{background:linear-gradient(-90deg,#ffffff 0,#ffffff 100%);border-bottom:1px solid #eeeeee}.wdes-menu-navbar ul.default-drop > li > a{color:#242424}.wdes-menu-navbar ul.default-drop > li > a:hover{color:#6c63ff}.wdes-menu-navbar .default-drop > li.current-menu-item > a{color:#6c63ff !important}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a{color:#7f7f7f}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a:hover{color:#6c63ff}.logo{height:40px !important}@media only screen and (max-width:991px) and (min-width:160px){.menu-d-mob{background-color:#191919}}.wdes-mob-btn .icon-menu .line{background-color:#ffffff}.wdes-menu-is-active .icon-menu .line-1,.wdes-menu-is-active .icon-menu .line-3{background-color:#ffffff}@media (max-width:960px) and (min-width:160px){.wdes-menu-navbar ul.default-drop > li > a{color:#ffffff !important}.wdes-menu-navbar ul.default-drop > li > a:hover{color:#6c63ff !important}.wdes-menu-navbar ul.default-drop > li.current-menu-item > a{color:#6c63ff !important}.wdes-menu-navbar ul.default-drop > li{border-bottom:1px solid #232323}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a{color:#ffffff}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a:hover{color:#ffffff}}.wdes-menu-navbar .mega-w.dropdown-content li.heading > a{color:#423f4f}.wdes-menu-navbar ul.default-drop > li ul.dropdown-content li.heading:hover > a{color:#6c63ff}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content > li > a{border-bottom:1px solid #eeeeee}.wdes-menu-navbar .mega-w.dropdown-content li.heading > a span.divider-heading-mega{background:#eeeeee}.text-logo{font-size:20px}/* =======================/* Headings/* ====================== */h1{color:#204056}h2{color:#204056}h3{color:#204056}h4{color:#204056}h5{color:#204056}h6{color:#204056}/* =======================/* Footer/* ====================== */.footer{background:linear-gradient(-90deg,#ffffff 0,#ffffff 100%)}.footer h1,.footer h2,.footer h3,.footer h4,.footer h5,.footer h6,.footer .widget_wdes_newsletter h2,.footer .widget_rss h2 > a.rsswidget,.widget-footer.widget_rss cite,#wp-calendar tfoot td a{color:#242424 !important}.footer ul li a{color:#7e7e7e}.footer ul li a:hover{color:#6a62fe}.email-news{background:#f8f8f8 !important;border:1px solid #eeeeee !important;text-transform:lowercase}input.sub-news,input.sub-news:hover{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%) !important}.copyrights-wdes-ft{background:linear-gradient(-90deg,#ffffff 0,#ffffff 100%)}.footer-menu ul li a{color:#7e7e7e}.footer-menu ul li a:hover{color:#6a62fe}.footer .copyrights-wdes-ft p.wdes-copyrights-text{color:#7e7e7e}.footer-bg{background:#29306c;background-image:url();background-repeat:no-repeat;background-position:left top;background-size:auto}.wdes-partners-company{border-top:1px solid #eeeeee;border-bottom:1px solid #eeeeee}.wdes-partners-company span{color:#7e7e7e}.wdes-partners-company span:hover{color:#6a62fe}#wdes-back-to-top{background-color:#6a62fe;border:1px solid #6a62fe}#wdes-back-to-top:hover{background-color:#817aff;border:1px solid #817aff}#wdes-back-to-top span{color:#ffffff}/* =======================/* ShortCodes/* ====================== */.loading{background:linear-gradient(-90deg,#6c63ff 0,#6c63ff 100%)}.best-feature .l-item-f{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%)}.best-feature .r-item-f{background:#f8f8f8}.block-comp span{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.plan-icon,.reg-dom:hover,a.chat-n,.block-footer-mlyo ul li a:hover,.info-bio h4,.title-error-t,.t-sty-a.colored h2,ul.categories li a::before,ul.categories li a:hover{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.social-media,.user-sub,.l-border,.cat,a.getstarted-modernlyo,a.job-modernlyo{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%)}.title-faq-lv2{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%)}.wpcf7 input:hover,.wpcf7 textarea:hover{border:2px solid #122d3e}.wpcf7 input[type=submit]{background:#6a62fe}.wpcf7 input[type=submit]:hover{background:#242424}/* =======================/* Blog/* ====================== */.parent-img-post-inner{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%)}.parent-title-archive h2{color:#ffffff}.parent-title-archive p{color:#ffffff}ul.wdes-breadcrumb-area li a{color:#ffffff}ul.wdes-breadcrumb-area li{color:#ffffff}ul.wdes-breadcrumb-area li:after{color:#ffffff}.classic-post-view a.title-art-sub{color:#6a62fe}.classic-post-view a.title-art-sub:hover{color:#242424}a.read-more-btn{color:#6a62fe;border:2px solid #6a62fe}a.read-more-btn:hover{background:#242424;color:#ffffff;border:2px solid #6a62fe}.sidebar-area .wid-title h2,.sidebar-area .wid-title a{color:#6a62fe}.block-sidebar-function ul li a:hover{color:#242424}.block-sidebar-function.widget_search #wdes-fullscreen-searchform button[type=submit],.elementor-widget-container #wdes-fullscreen-searchform button[type=submit],.elementor-element #wdes-fullscreen-searchform button[type=submit]{background:#6a62fe !important}.posts li a{color:#6a62fe !important}.posts li a:hover{color:#242424 !important}.art-block-sub a[rel=tag]:hover{background:#6a62fe}.body-article h1,.body-article h2,.body-article h3,.body-article h4,.body-article h5,.body-article h6{color:#6a62fe}a.related-title{color:#6a62fe}a.related-title:hover{color:#242424}.tag-cloud-link:hover{background:#6a62fe}.comment-form #comment:hover,.comment-form #author:hover,.comment-form #email:hover,.comment-form #url:hover,.comment-form #comment:focus,.comment-form #author:focus,.comment-form #email:focus,.comment-form #url:focus{border:1px solid #6a62fe}.comment-form .submit{background:#6a62fe}.comment-form .submit:hover{background:#242424}.comments-blog .wid-title h2{color:#6a62fe !important}a.comment-reply-link{background:#6a62fe;border:2px solid #6a62fe}a.user-rep{color:#6a62fe !important}h3.comment-reply-title{color:#6a62fe !important}p.logged-in-as a{color:#6a62fe}.widget-footer #wp-calendar thead th{background:#6a62fe}.widget-footer table td{background:#ffffff}.widget-footer input[type=text],.widget-footer input[type=password],.widget-footer input[type=email],.widget-footer input[type=url],.widget-footer input[type=date],.widget-footer input[type=number],.widget-footer input[type=tel],.widget-footer input[type=file],.widget-footer textarea,.widget-footer select{background:#f8f8f8;border:1px solid #eeeeee}.widget-footer a.tag-cloud-link{background:#f8f8f8;color:#7e7e7e}.widget-footer a.tag-cloud-link:hover{background:#6a62fe;color:#ffffff}.widget-footer #wdes-fullscreen-search-input{background:#f8f8f8;border:1px solid #eeeeee}.widget-footer i.fas.fa-search.fullscreen-search-icon{color:#ffffff}.widget-footer.widget_search button[type="submit"]{background:#6a62fe !important}.func-hr2{background:#eeeeee}.footer .company-info-block span,.footer .company-info-block a,.footer .about p{color:#7e7e7e !important}.footer .company-info-block:nth-child(2) a::before,.footer .company-info-block:nth-child(3) span::before{color:#6a62fe}.footer li,.footer input[type='text'],.footer input[type='password'],.footer input[type='email'],.footer input[type='url'],.footer input[type='date'],.footer input[type='number'],.footer input[type='tel'],.footer input[type='file'],.footer textarea,.footer select,.widget-footer #wp-calendar caption,.widget-footer #wdes-fullscreen-search-input::placeholder,.footer.footer-dark a.rsswidget{color:#7e7e7e !important}.footer p{color:#7e7e7e}.widget-footer #wp-calendar tbody td,.widget-footer #wp-calendar thead th,.widget-footer #wp-calendar tfoot td{border:1px solid #eeeeee}.widget-footer #wp-calendar thead th,.footer-dark #wp-calendar tfoot td a,.widget-footer #wp-calendar tbody td{color:#7e7e7e}#wp-calendar #today{background:#6a62fe;border:1px solid #6a62fe;color:#ffffff}.woocommerce nav.woocommerce-pagination ul li a{background:#f3f5f9}.woocommerce nav.woocommerce-pagination ul li a{color:#6a62fe}.woocommerce nav.woocommerce-pagination ul li span.page-numbers.current,.woocommerce nav.woocommerce-pagination ul li a:hover{background:#6a62fe}.woocommerce nav.woocommerce-pagination ul li span.page-numbers.current,.woocommerce nav.woocommerce-pagination ul li a:hover{color:#ffffff}/* =======================/* WHMCS Bridge/* ====================== */div#bridge section#main-menu,div#bridge section#home-banner,div#bridge section#main-menu .navbar-main{background:linear-gradient(-90deg,#6a62fe 0,#6a62fe 100%)}div#bridge section#home-banner .btn.search{background:#807afe}div#bridge section#home-banner .btn.transfer{background:#5550cd !important}div#bridge .home-shortcuts{background:#5450cd}div#bridge .home-shortcuts li,div#bridge .home-shortcuts li:first-child{border-color:#4c48bb}div#bridge div#twitterFeedOutput{border-left:4px solid #6a62fe}div#bridge .panel-sidebar .panel-heading{background:#6a62fe !important}div#bridge .panel-sidebar a.list-group-item.active,div#bridge .panel-sidebar a.list-group-item.active:focus,div#bridge .panel-sidebar a.list-group-item.active:hover{background:#6a62fe !important;border-color:#6a62fe !important}div#bridge .btn-primary{background:#6a62fe;border-color:#6a62fe}/* =======================/* 404/* ====================== */.error-p{background:#ffffff url()}.title-error h2{color:#204056}.title-error p{color:#7e7e7e}/* =======================/* Coming Soon/* ====================== */.coming{background:#cccccc url()}.com-title{color:#272727}/* ==============================================/* Typography/* ============================================= *//* =======================/* Menu/* ====================== */.wdes-menu-navbar ul.default-drop > li > a{font-family:Montserrat;font-weight:600;font-size:14px}.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content > li > a{font-family:Montserrat;font-weight:400;font-size:13px}.wdes-menu-navbar .mega-w.dropdown-content li.heading > a{font-family:Montserrat;font-weight:600;font-size:16px}/* =======================/* Blog/* ====================== */.parent-title-archive h2{font-family:Poppins;font-weight:600;font-size:50px}.classic-post-view a.title-art-sub{font-family:Poppins;font-weight:700;font-size:18px}.inner-item-art .title-art-sub{font-family:Poppins;font-weight:600;font-size:36px}/* =======================/* Body/* ====================== */p{font-family:Roboto;font-weight:400;font-size:14px}blockquote{font-family:Roboto;font-weight:600;font-size:16px}a{font-family:Roboto;font-weight:400;font-size:17px}/* =======================/* Headings/* ====================== */h1{font-family:Poppins;font-weight:700;font-size:32px}h2{font-family:Poppins;font-weight:700;font-size:24px}h3{font-family:Poppins;font-weight:700;font-size:20.8px}h4{font-family:Poppins;font-weight:700;font-size:16px}h5{font-family:Poppins;font-weight:700;font-size:12.8px}h6{font-family:Poppins;font-weight:700;font-size:11.2px}html,body{overflow:auto}
</style>
<link href="https://www.yogyahost.com/wp-content/themes/phox/assets/css/responsive.css?ver=1.6.6" id="wdes-responsive-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://www.yogyahost.com/wp-content/themes/phox/assets/css/fontawesome.min.css?ver=1.6.6" id="elementor-icons-shared-0-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%7CPoppins%3A200%2C400%2C600%2C700%7CKarla%3A400%2C500%2C600%2C700%7CMontserrat%3A400%2C600%7CMontserrat%3A600%7CMontserrat%3A400%7CPoppins%3A600%7CPoppins%3A700%7CRoboto%3A400%7CRoboto%3A600&amp;subset=latin%2Clatin-ext" id="google-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://www.yogyahost.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="jquery-migrate-js" src="https://www.yogyahost.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2" type="text/javascript"></script>
<script id="tp-tools-js" src="https://www.yogyahost.com/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.2.23" type="text/javascript"></script>
<script id="revmin-js" src="https://www.yogyahost.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.23" type="text/javascript"></script>
<link href="https://www.yogyahost.com/wp-json/" rel="https://api.w.org/"/><link href="https://www.yogyahost.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://www.yogyahost.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<script>
            /*<![CDATA[*/
            var wdes_ajax_url = "https://www.yogyahost.com/wp-admin/admin-ajax.php";
            /*]]>*/
          </script><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta content="Powered by Slider Revolution 6.2.23 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." name="generator"/>
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
</head>
<body>
<div class="coming">
<div class="container">
<!-- Logo -->
<img alt="Phox" class="logo-coming" src="https://www.yogyahost.com/wp-content/uploads/2020/10/yogyahost.png"/>
<!-- Title -->
<div class="com-title">
<h1>Coming Soon</h1>
<p>We are currently on creating something fantastic</p>
</div>
<!-- Counter -->
<div id="defaultCountdown"></div>
<div class="clearfix"></div>
</div>
</div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.yogyahost.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://www.yogyahost.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="popper-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/popper.min.js?ver=1.6.6" type="text/javascript"></script>
<script id="ammap-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/ammap.js?ver=1.6.6" type="text/javascript"></script>
<script id="worldLow-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/worldLow.js?ver=1.6.6" type="text/javascript"></script>
<script id="jquery-plugin-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/jquery.plugin.js?ver=1.6.6" type="text/javascript"></script>
<script id="jquery-countdown-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/jquery.countdown.js?ver=1.6.6" type="text/javascript"></script>
<script id="bootstrap-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/bootstrap.min.js?ver=1.6.6" type="text/javascript"></script>
<script id="wdes-custom-script-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/custom-script.js?ver=1.6.6" type="text/javascript"></script>
<script id="wdes-plugins-js" src="https://www.yogyahost.com/wp-content/themes/phox/assets/js/plugins.js?ver=1.6.6" type="text/javascript"></script>
<script id="wdes-plugins-js-after" type="text/javascript">
jQuery( document ).ready(function() {var austDay = new Date("Jan 1, 2021 11:10:20");jQuery("#defaultCountdown").countdown({until: austDay});});
</script>
<script id="wp-embed-js" src="https://www.yogyahost.com/wp-includes/js/wp-embed.min.js?ver=5.6" type="text/javascript"></script>
</body>
</html>