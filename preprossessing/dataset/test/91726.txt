<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en-US"> <![endif]--><!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]--><!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]--><!--[if gt IE 8]><!--><html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
<title>Attention Required! | Cloudflare</title>
<meta id="captcha-bypass" name="captcha-bypass"/>
<meta charset="utf-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="noindex, nofollow" name="robots"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<link href="/cdn-cgi/styles/cf.errors.css" id="cf_styles-css" media="screen,projection" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]><link rel="stylesheet" id='cf_styles-ie-css' href="/cdn-cgi/styles/cf.errors.ie.css" type="text/css" media="screen,projection" /><![endif]-->
<style type="text/css">body{margin:0;padding:0}</style>
<!--[if gte IE 10]><!-->
<script>
  if (!navigator.cookieEnabled) {
    window.addEventListener('DOMContentLoaded', function () {
      var cookieEl = document.getElementById('cookie-alert');
      cookieEl.style.display = 'block';
    })
  }
</script>
<!--<![endif]-->
<script type="text/javascript">
  //<![CDATA[
  (function(){
    window._cf_chl_opt={
      cvId: "1",
      cType: "interactive",
      cNounce: "53931",
      cRay: "610c3c465eaa2308",
      cHash: "1b1bcf4741b2c87",
      cFPWv: "b",
      cRq: {
        ru: "aHR0cHM6Ly93d3cuYWtyb25vaGlvLmdvdi8=",
        ra: "cHl0aG9uLXJlcXVlc3RzLzIuMjIuMA==",
        rm: "R0VU",
        d: "IroL+Wtju0lk7Mbnog/H5rHwZLwZKvMc0HYYCFt3KK56sx04oOrrTZqCJySCVUeKk9A1+M4hEMCJ6kMMiqvxfpBLK5yYqOk+zBYnf/Dk9MLoUORWWgny3/3RLwftqCdPhMqnW+eHBeHrUDIMoiXWEj9F+6JJDTbYLGRLo7s1+oSIT/Xu52xi5/UQ0FDfAVNZzMB5Au7hM07+kW7jjkLYI9TbDFYV2cvGSjmDzCUN9srhT34b5JSp1BkwUFw2C1GjAAPfGM1qhtaGLJhXNlv8LFZlL/FToHpy96UW4RySOtavYl2XbG1zcWEmmp6UgwL/k+92bfJuEz8z9RURhZMiToCpKkZtQeRK8Utm5lcit8Txpn2hAXeCwzvk0YHih2KKMX5NmqUx6mepEFGh5afCU2JeoLl3qNOYIAVtUmke2vLNfJ7wXy3PRSLpdyndP/xzqitEUebcu7F2JKJfK7Ob+dxE4Y1cR/b0ZShIi4kZO0MY5PMiMRshT/cUtAfg6bn60qIs2jOCMKBeUhiqyTonbbeWZodz87yB14dabavSEGkv4yVMk9/g+3XlywimW9hcZa9OhbF2+Rt4zBbk6RPPo//Yv2wcpTnbwT+AoyRZun11OsJdm7CJuY1Tek+krTnZaL9+AOmMs9WtrSvABUORGpNDre/5q+0lNxmHoSpVo9X5RAVSQIlkhsAtnTByY3Z3paEEB7gMCBoVhL6IadP+qLbshyD6b+d1w8aJ9KTexPRxsfquxW/BgreYb0WM7Ldy",
        t: "MTYxMDUxMDY5MC4zMTMwMDA=",
        m: "DWSVbButqxZbHWybj+2AEZFTPbWjCxG0YlFkgTAojOk=",
        i1: "d883qLa1EYpnaWdoUAGEpQ==",
        i2: "HMb7z7Po3lAEFkT/xlT2QA==",
        uh: "JnPNhFrP9JDZz++jrWFNK99fEBZafo8DSm+TpH36hUY=",
        hh: "NPsgy8SZLPKLRXYlahGYHnS2TtpIRTaOBLy1+MZt1iE=",
      }
    }
    window._cf_chl_enter = function(){window._cf_chl_opt.p=1};
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
    b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
    b(function(){
      var cookiesEnabled=(navigator.cookieEnabled)? true : false;
      var cookieSupportInfix=cookiesEnabled?'/nocookie':'/cookie';
      var cpo = document.createElement('script');
      cpo.type = 'text/javascript';
      cpo.src = "/cdn-cgi/challenge-platform/h/b/orchestrate/captcha/v1";
      var done = false;
      cpo.onload = cpo.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          done = true;
          cpo.onload = cpo.onreadystatechange = null;
          window._cf_chl_enter()
        }
      };
      document.getElementsByTagName('head')[0].appendChild(cpo);
    }, false);
  })();
  //]]>
</script>
<style type="text/css">
  #cf-wrapper #spinner {width:69px; margin:  auto;}
  #cf-wrapper #cf-please-wait{text-align:center}
  .attribution {margin-top: 32px;}
  .bubbles { background-color: #f58220; width:20px; height: 20px; margin:2px; border-radius:100%; display:inline-block; }
  #cf-wrapper #challenge-form { padding-top:25px; padding-bottom:25px; }
  #cf-hcaptcha-container { text-align:center;}
  #cf-hcaptcha-container iframe { display: inline-block;}
  @keyframes fader     { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-wrapper #cf-bubbles { width:69px; }
  @-webkit-keyframes fader { 0% {opacity: 0.2;} 50% {opacity: 1.0;} 100% {opacity: 0.2;} }
  #cf-bubbles > .bubbles { animation: fader 1.6s infinite;}
  #cf-bubbles > .bubbles:nth-child(2) { animation-delay: .2s;}
  #cf-bubbles > .bubbles:nth-child(3) { animation-delay: .4s;}
</style>
</head>
<body>
<div id="cf-wrapper">
<div class="cf-alert cf-alert-error cf-cookie-error" data-translate="enable_cookies" id="cookie-alert">Please enable cookies.</div>
<div class="cf-error-details-wrapper" id="cf-error-details">
<div class="cf-wrapper cf-header cf-error-overview">
<h1 data-translate="challenge_headline">One more step</h1>
<h2 class="cf-subheadline"><span data-translate="complete_sec_check">Please complete the security check to access</span> www.akronohio.gov</h2>
</div><!-- /.header -->
<div class="cf-section cf-highlight cf-captcha-container">
<div class="cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<div class="cf-highlight-inverse cf-form-stacked">
<form action="/?__cf_chl_captcha_tk__=27d0beea9c0af473890691f8ed989fbcc8154560-1610510690-0-AbxhDO1DQFW1onvX9nHjpBHLeG5QQtoGteFm_QK56W79aACt61AdNb4DGhsqQkurVAzLzV5BNXgR8BaCtK-YSF0SJHd8djoG-j-gz0uOijyxlNIXJ0Q4T33bxJkDDeaG6Gda-PfaAiBNyvuTnaQFtJlUSCCWTiuCk1dzx6LTExn9bsyKUSX5aYL1bLAcPJo0RDy0hDfNoDTsjN0X9_9myra3BICR0dgOh9kws8PRJU3hO-u43wlBLngoVewLFwYTZ6RuoHh32oWHZvn3ox46ez2cRSyk4j-iPwe5At5paD91z2RaJ4tnVThlAZu_4Nh2uFnGLTXXnUMUzv8GFCIBCamJgwuWZt3wehudRkZfgbqAOxh0VEjSvsSXePU6tfymxFM1UpwVyrqjwjvDNfVyF4FfCacmEL8shHBFNs4RQVlqI3QhdpHc6MIKs6pLeKCcu1JcMY9-Vl7AKUsU78fufSjSwGQWP75jOZgOq1S1DKSObo9EnPpCwGw1ujBxi0qG4YRRAywL_KQ7usWcaUcZwJo" class="challenge-form" enctype="application/x-www-form-urlencoded" id="challenge-form" method="POST">
<div id="cf-please-wait">
<div id="spinner">
<div id="cf-bubbles">
<div class="bubbles"></div>
<div class="bubbles"></div>
<div class="bubbles"></div>
</div>
</div>
<p data-translate="please_wait" id="cf-spinner-please-wait">Please stand by, while we are checking your browser...</p>
</div>
<input name="r" type="hidden" value="c562cb74faf46d25ac0158d99d686e7e0774c3fe-1610510690-0-AV+4qogxXZjZWyA4I0LdrFK5waoKA8gP9w3Nld+eUZVWCBBHc0HYJIOMweY/VyVl9z4eNTTE87do+eqDS/1BQ9QDr9eBv+V/uwsINGFNSaGl9D86qG40rtx8I9RS7HKfZ03pnQ+MqG/zqEfLBqLEJCZ5ooOD44llneNoe3HUzVOANVh6ZAb/NcFFh7F3YiFYdNs4oXKRyrzCpPBAWmrLQbGZ1TbD7imrv/CtlHTr97fsCLjw3SESHzd7hqxn2pnG4aFfKfH9A7bz5mwh/pozsnwfPlGgRSGPe0nqHcyXVhQtQkd5svmYlz4FQ30FUJoOr3u0HSRX14OdbpoQ7KEwVYhWzCITwOoowSUifm41mO3mhsimIz5tiXNAfuR8shV95hBJS7wusATy7Ac9RsxgOOwA66UUKnjAUXYTkbCsw4UjS7ajz2YrWWyIRYxfQ5f+gRMnWW5U7kNDc79paDVDYH6/KvuvNroE2JsGZyfzTIWtVPiw4R6pt+KWRSDTHPcexrMzVMplOBH3SwQKjl0XuZ1ywF4xMUCvHU/IzrEXtGU4CjL3LlTF8UqJDOCMyOd5VRPLaniLpxlHTsMiUsA4h0CAHOBBbplNk/kOtvMPZkZoN+94vIp5UKJg80LgBeO8N1gG0bQrXwccMaZt6Zlza7cvl+UZ5mvCD8ZGiNoDacdjHpFKFfr9qqeQjZMmwGrkGwvjQiXtMZ1KOxmBmQ5TQz8sWM5r9D2xPEil35C53xox5E/39CI1PSE9jh4fURZlYc0mtwVwsqZ/RMAF3Nnt60c0BRhNsgACOIY1aTsf3D8ozU4QJNMM+G8ADXWf4LkFqClNltk5+F3XwB48PMS/y+UGxHxVhDbS04xRLUup48ftyS1aaUjkwGf0K1d7JdVCRVTYqZwWPjJo/thyOLGWqjzKZxfQiY4ROyoOPWSmRGqzRGLCRFs7njsf34u9e6zmyzd7us5t+YEv3/DkrGr8YTy+ILbpB9PeEVAIuKOdp6pKUX1Ku6hdDtX3Hq0S9jjtO3slyZ3dYJp0Nk1AfBJ9rC4AFSF+LZ7xdTL0HoAUJo58Da+xN6auy6MdqKuRwrkKdytDjeb/u1xIFQ35481NGbGk2p1FBQODUXLrFKIb/G+gND2dcRKDc4XzUCWWHc/nLrqW1zxgyv9+kTtNGMZyMVq6/teVF4OugOAxYirHlP0LEBSKdRuLdZVNl2Qe1Ohg+2C88RlBAsRY1Za3PWSx5YoLFeTFqVhLKfSARPTY02qD2JZSnndc/VuOMWIqVzKhZ5PtA8k6zxXPiZYcp/+BF2C38GXFHhzmrI+cpwCi0CS3uQk99f0Iv0LeezwbEc1p4Q=="/>
<input name="cf_captcha_kind" type="hidden" value="h"/>
<input name="vc" type="hidden" value="a76991b010f250c5a1fd651755ab0632"/>
<noscript class="cf-captcha-info" id="cf-captcha-bookmark">
<h1 data-translate="turn_on_js" style="color:#bd2426;">Please turn JavaScript on and reload the page.</h1>
</noscript>
<div class="cookie-warning" data-translate="turn_on_cookies" id="no-cookie-warning" style="display:none">
<p data-translate="turn_on_cookies" style="color:#bd2426;">Please enable Cookies and reload the page.</p>
</div>
<script type="text/javascript">
  //<![CDATA[
    var a = function() {try{return !!window.addEventListener} catch(e) {return !1} },
      b = function(b, c) {a() ? document.addEventListener("DOMContentLoaded", b, c) : document.attachEvent("onreadystatechange", b)};
      b(function(){
        var cookiesEnabled=(navigator.cookieEnabled)? true : false;
        if(!cookiesEnabled){
          var q = document.getElementById('no-cookie-warning');q.style.display = 'block';
        }
      });
  //]]>
  </script>
<div id="trk_captcha_js" style="background-image:url('/cdn-cgi/images/trace/captcha/nojs/h/transparent.gif?ray=610c3c465eaa2308')"></div>
</form>
</div>
</div>
<div class="cf-column">
<div class="cf-screenshot-container">
<span class="cf-no-screenshot"></span>
</div>
</div>
</div><!-- /.columns -->
</div>
</div><!-- /.captcha-container -->
<div class="cf-section cf-wrapper">
<div class="cf-columns two">
<div class="cf-column">
<h2 data-translate="why_captcha_headline">Why do I have to complete a CAPTCHA?</h2>
<p data-translate="why_captcha_detail">Completing the CAPTCHA proves you are a human and gives you temporary access to the web property.</p>
</div>
<div class="cf-column">
<h2 data-translate="resolve_captcha_headline">What can I do to prevent this in the future?</h2>
<p data-translate="resolve_captcha_antivirus">If you are on a personal connection, like at home, you can run an anti-virus scan on your device to make sure it is not infected with malware.</p>
<p data-translate="resolve_captcha_network">If you are at an office or shared network, you can ask the network administrator to run a scan across the network looking for misconfigured or infected devices.</p>
</div>
</div>
</div><!-- /.section -->
<div class="cf-error-footer cf-wrapper w-240 lg:w-full py-10 sm:py-4 sm:px-8 mx-auto text-center sm:text-left border-solid border-0 border-t border-gray-300">
<p class="text-13">
<span class="cf-footer-item sm:block sm:mb-1">Cloudflare Ray ID: <strong class="font-semibold">610c3c465eaa2308</strong></span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Your IP</span>: 2001:250:3c02:749:7880:4fc1:46b3:d414</span>
<span class="cf-footer-separator sm:hidden">•</span>
<span class="cf-footer-item sm:block sm:mb-1"><span>Performance &amp; security by</span> <a href="https://www.cloudflare.com/5xx-error-landing" id="brand_link" rel="noopener noreferrer" target="_blank">Cloudflare</a></span>
</p>
</div><!-- /.error-footer -->
</div><!-- /#cf-error-details -->
</div><!-- /#cf-wrapper -->
<script type="text/javascript">
  window._cf_translation = {};
  
  
</script>
</body>
</html>
