<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<title>PEC School</title>
<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<link href="css/shards.min.css?v=2.0.1" rel="stylesheet"/>
<link href="css/shards-demo.min.css?v=2.0.1" rel="stylesheet"/>
<link href="style.css" rel="stylesheet"/>
</head>
<body>
<!-- <div class="loader">
        <div class="page-loader"></div>
    </div> -->
<div>
<nav class="navbar navbar-expand-lg navbar-dark bg-success mb-4">
<img alt="Example Navbar 1" class="mr-2" height="70" src="pics/logo.jpg"/>
<a class="navbar-brand" href="#">PEC School</a>
<button aria-controls="navbarNavDropdown-4" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNavDropdown-4" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNavDropdown-4">
<ul class="navbar-nav mr-auto">
<li class="nav-item active">
<a class="nav-link" href="/">Home
                            <span class="sr-only">(current)</span>
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="about.html">About</a>
</li>
<li class="nav-item">
<a class="nav-link" href="gallery.html">Gallery</a>
</li>
<li class="nav-item">
<a class="nav-link" href="addmission.html">Admission</a>
</li>
<li class="nav-item">
<a class="nav-link" href="job.html">Job</a>
</li>
<li class="nav-item">
<a class="nav-link" href="contact.html">Contact</a>
</li>
</ul>
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link" href="#">
<i class="fa fa-facebook"></i>
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">
<i class="fa fa-youtube"></i>
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">
<i class="fa fa-instagram"></i>
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">
<i class="fa fa-whatsapp"></i>
</a>
</li>
</ul>
</div>
</nav>
</div>
<!-- Welcome Section -->
<div class="welcome d-flex justify-content-center flex-column">
<div class="inner-wrapper mt-auto mb-auto">
<h1 class="slide-in" style="color: #17c671">PEC School</h1>
<p class="slide-in">Peshawar Educational Complex.</p>
<div class="action-links slide-in">
<a class="btn btn-success btn-pill align-self-center mr-2" href="about.html">

                    About Us</a>
<a class="btn btn-outline-light btn-pill align-self-center" data-scroll-to="#introduction" href="contact.html" id="scroll-to-content">Contact Us</a>
</div>
</div>
<img alt="Shard" class="shard" src="images/demo/shard-1-5x-3.png"/>
</div>
<div class="example col-md-12 ml-auto mr-auto">
<div class="row">
<div class="col-lg-4 col-md-6 col-sm-12 mb-4">
<div class="card"><br/>
<img alt="Card image cap" class="card-img-top align-self-center" src="pics/boys.jpg" style="height: 250px;width: 75%;"/>
<div class="card-body">
<h4 class="card-title">Boys Root</h4>
<p class="card-text">Pec as the large institute in Peshawar having the largest brach for boys
                            where the boys get the education...</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-sm-12 mb-4 ">
<div class="card"><br/>
<img alt="Card image cap" class="card-img-top align-self-center" src="pics/girls.jpg" style="height: 250px; width: 75%;"/>
<div class="card-body">
<h4 class="card-title">Girls Root</h4>
<p class="card-text">Pec as the large institute in Peshawar having the largest brach for girls
                            where the girls get the education...</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-sm-12 mb-4">
<div class="card"><br/>
<img alt="Card image cap" class="card-img-top align-self-center" src="pics/kids.jpg" style="height: 250px;width: 75%;"/>
<div class="card-body">
<h4 class="card-title">Kids Root</h4>
<p class="card-text">Pec as the large institute in Peshawar having the largest brach for kids
                            where the kids get the education...</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
</div>
</div>
<br/>
<hr style="background-color: #fff;
	border-top: 2px dashed #8c8b8b;"/> <br/><br/>
<div class="container mb-5" id="icon-packs">
<div class="section-title col-lg-8 col-md-10 ml-auto mr-auto mb-5">
<h2 class="text-center mb-4">Meet With Our Priciple</h2>
<p class="text-center"><a class="btn btn-success" style="color: white">Contact Me!</a></p>
</div>
</div><br/>
<div class="fluid-container text-light" style="background-color: #1B1E21; height: 380px;">
<div class="container ">
<div class="row">
<div class="col-md-4 col-sm-12 text-center">
<img class="rounded float-lef" src="pics/1.jpeg" style="height: 250px; margin-top: 60px;"/>
</div>
<div class="col-md-8 col-sm-12 text-center " style="background-color: #1B1E21;"><br/><br/><br/><br/><br/>
<p class="text-center float-left">â On behalf of Peshawar Education Complex (PEC) faculty and
                        staff, we
                        welcome you
                        to
                        second academic year at PEC.â</p>
<a class="btn btn-success">Read More!</a>
</div>
</div>
</div>
</div><br/>
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-12 mb-4">
<div class="card text-center"><br/>
<h4>General Info:</h4>
<div class="card-body">
<h6 class="card-title">Principle</h6>
<p class="card-text">Saeed Khan Khalil</p>
<h6 class="card-title">Grade Level</h6>
<p class="card-text">PlayGroup - 10th Class</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 mb-4">
<div class="card tab-card">
<div class="card-header tab-card-header">
<ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
<li class="nav-item">
<a aria-controls="One" aria-selected="true" class="nav-link text-success" data-toggle="tab" href="#one" id="one-tab" role="tab">Introduction</a>
</li>
<li class="nav-item">
<a aria-controls="Two" aria-selected="false" class="nav-link text-success" data-toggle="tab" href="#two" id="two-tab" role="tab">Faculty</a>
</li>
<li class="nav-item">
<a aria-controls="Three" aria-selected="false" class="nav-link text-success" data-toggle="tab" href="#three" id="three-tab" role="tab">Concept Of Organization</a>
</li>
</ul>
</div>
<div class="tab-content" id="myTabContent">
<div aria-labelledby="one-tab" class="tab-pane fade show active p-3" id="one" role="tabpanel">
<h5 class="card-title">PEC</h5>
<p class="card-text">In the age of knowledge driven economy, schoolâs being primary
                                institutions of the society.Peshawar Educational Complex believes in providing
                                effective&amp; quality education. Peshawar Educational Complex strives to imitate the to
                                wearing personality
                                of itâs pioneer and founder in emulating the great been factor of Pakistan, we have
                                established an unparalleled system with consistent policies, established standards and a
                                coherent
                                conceptual base. We claim to have the following distinctive features. We ensure focused
                                and ongoing evaluation of school performance with the aim of achieving set objectives.
                                We believe in nurturing creative and analytical abilities of students intesting their
                                memories. we believe in engaging student mind by encouraging them to participate in
                                learning
                                activities regularly and perform their home assignments. </p>
<a class="btn btn-success" href="about.html">Read More!</a>
</div>
<div aria-labelledby="two-tab" class="tab-pane fade p-3" id="two" role="tabpanel">
<h5 class="card-title">Faculty Of PEC</h5>
<p class="card-text">As one of the biggest schools in Peshawar, PEC has very good
                                infrastructure in terms of building and supporting facilities. The school had been
                                renovated from its original condition into a two-floor construction. When you arrive at
                                the front yard of this school, you will be greeted by a security. The security post is
                                located close to the front gate. Everyday there are two security officers waiting at
                                their post. The school is arranged in a square shape. In the centre, you will see a wide
                                open area with a flag pole. The area is used for flag raising ceremony and also for
                                sports activity as well as other outdoor activities. The headmasterâs office is located
                                not far from the entrance gate alongside the administration staff office and teachersâ
                                hall. There are 24 classrooms at this school.</p>
<a class="btn btn-success" href="about.html">Read More!</a>
</div>
<div aria-labelledby="three-tab" class="tab-pane fade p-3" id="three" role="tabpanel">
<h5 class="card-title">Concept Of Organization</h5>
<p class="card-text">Experience of renowned educationists and capitalizing on the guidance
                                of our patron-in chief P.E.C. We have established an unmatched system of education.</p>
<a class="btn btn-success" href="about.html">Read More!</a>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 mb-4">
<div class="card text-center"><br/>
<h4>General Info:</h4>
<div class="card-body">
<h6 class="card-title">LOCATION</h6>
<p class="card-text">Rahat Abad, Street# 13, Peshawar</p>
<h6 class="card-title">SCHOOL HOURS</h6>
<p class="card-text">Mon-Sat: 8am â 2pm Weekends: Closed</p>
<h6 class="card-title">PHONE &amp; FAX</h6>
<p class="card-text">091-5610133</p>
</div>
</div>
</div>
</div>
<div class="container card">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12 mb-4" style="margin-top: 30px;">
<h3>Get School News &amp; Updates</h3>
<p>Get In touch With us Now!</p>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 mb-4" style="margin-top: 30px;">
<form>
<div class="row text-center">
<div class="form-group col-md-6">
<div class="input-group input-group-seamless">
<span class="input-group-prepend">
<span class="input-group-text">
<i class="fa fa-user"></i>
</span>
</span>
<input class="form-control" id="form1-username" placeholder="Username" type="text"/>
</div>
</div>
<div class="form-group col-md-6">
<div class="input-group input-group-seamless">
<input class="form-control" id="form2-email" placeholder="Email" type="email"/>
<span class="input-group-append">
<span class="input-group-text">
<i class="fa fa-envelope"></i>
</span>
</span>
</div>
</div>
<div class="text-center">
<a class="btn btn-success text-center" style="margin-left: 200px; margin-top:20px;">Send</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div><br/>
<hr style="background-color: #fff;
	border-top: 2px dashed #8c8b8b;"/> <br/><br/>
<div class="fluid-container">
<div class="text-center">
<h3 style="color: #212529;">What Parents are Saying about Us!</h3>
</div><br/><br/>
<div class="example col-md-12 ml-auto mr-auto">
<div class="row">
<div class="col-lg-4 col-md-6 col-sm-12 mb-4">
<div class="card"><br/>
<div class="card-body">
<h4 class="card-title">91% believe the teachers are well prepared.</h4>
<p class="card-text">âIt is a committed faculty.â<br/>
                                âPEC has strong leadership, great teachers, and a love for the students!â</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-sm-12 mb-4 ">
<div class="card"><br/>
<div class="card-body">
<h4 class="card-title">94% believe that pec is affordable.</h4>
<p class="card-text">"PEC is affordale as compared to other institutes"<br/>"We can easily
                                get the admission process."</p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-sm-12 mb-4">
<div class="card"><br/>
<div class="card-body">
<h4 class="card-title">94% of parents believe children's are safe in pec.</h4>
<p class="card-text">âI love most about PEC is safe,comfortable,and loving environment.<br/>â
                            </p>
<a class="btn btn-success" href="#">Read More</a>
</div>
</div>
</div>
</div>
</div>
</div>
<footer style="background-color: #333333;">
<div class="copyright text-center">
            Copyright © 2020 <span class="text-white">Design and Develop By <a class="text-success" href="https://www.facebook.com/arbabmuhammadramzankhan">Ramzan Khan</a></span>
</div>
</footer>
<script async="" defer="" src="https://buttons.github.io/buttons.js"></script>
<script async="" charset="utf-8" src="//platform.twitter.com/widgets.js"></script>
<script crossorigin="anonymous" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="js/shards.min.js"></script>
<script src="js/demo.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js"></script>
<script src="./script.js"></script>
</body>
</html>