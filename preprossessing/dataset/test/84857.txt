<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head> <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
<base target="_top"/>
<title>Marjorie Lee Browne</title>
<link href="styles/women.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="styles/printbio.css" media="print, handheld" rel="stylesheet" type="text/css"/>
</head>
<body onload="if (self != top) top.location=self.location">
<a href="http://www.agnesscott.edu" target="_blank"><img align="right" alt="Agnes Scott College" border="0" height="55" src="ASCminilogo3.gif" width="92"/></a>
<h2 class="banner">Biographies of <span class="kern">W</span>omen Mathematicians</h2>
<hr class="banner"/>
<p class="banner">
<a class="banner" href="women.htm">Home</a> | 
<a class="banner" href="alpha.htm">Alphabetical Index</a> | 
<a class="banner" href="chronol.htm">Chronological Index</a> | 
<a class="banner" href="resource.htm">Resources</a> | 
<a class="banner" href="credits.htm">Credits</a> | 
<a class="banner" href="search2.htm">Search</a>
<br/></p>
<hr class="banner"/>
<div class="biography">
<h1 align="center">Marjorie Lee Browne</h1>
<p align="center">
<a href="brown.jpg"><img align="bottom" alt="Marjorie Browne" height="140" src="browns.gif" width="100"/></a></p>
<p align="center">
September 9, 1914 - October 19, 1979
</p>
<hr/>
<h3>By Erica Fogg, Cecilia Davis, and Jennifer Sutton, students at 
Carnage Middle
School in Raleigh, N.C.
</h3>
<p>
Marjorie Lee Browne was born to Mary Taylor Lee and Lawrence Johnson Lee, in Memphis, 
Tennessee, on September 9, 1914. Marjorie was encouraged to study math by her father 
and step-mother (her mother died when she was two).</p>
<p>
Marjorie went to LeMoyne High School (a private school) after attending public school 
in Memphis. Then she went on to graduate cum laude from Howard University in 1935.</p>
<p>
She briefly taught at Gilbert Academy in New Orleans. She earned her M.S. in mathematics
 from the University of Michigan in 1939, then joined the Wiley College faculty in 
Marshall, Texas, and started working on her doctorate in Michigan during summers. 
She became a teaching fellow in 1947 at the University of Michigan. In 1949, Marjorie
 earned her doctorate in mathematics. She was among the first Black women to 
earn a doctorate in mathematics.  [<a href="granvill.htm">Evelyn Boyd Granville</a> 
also received a Ph.D. in mathematics in 1949, from Yale University. <a href="haynes-euphemia.htm">Euphemia Lofton Haynes
</a> received her Ph.D. in mathematics from the Catholic University of America in 1943.]</p>
<p>
Doctor Browne went to North Carolina College (now North Carolina Central University) 
where she taught mathematics after graduating from Michigan University. She soon 
became the chair of the Mathematics department in 1951; she resigned as department chair in 
1970. She stayed at NCCU until she retired in 1979.</p>
<p>
In the years of 1952-1953, Marjorie won a Ford Foundation fellowship to study 
combinatorial topology at Cambridge University and traveled throughout western 
Europe. Dr. Browne was a National Science Foundation Faculty Fellow studying 
computing and numerical analysis at the University of California at Los Angeles.
 When she studied differential topology at Columbia University in 1965-66, 
she won a similar fellowship.</p>
<p>
Four years before Marjorie's retirement, in 1975, Dr. Browne was the first recipient 
of the W.W. Rankin Memorial Award for Excellence in Mathematics Education, given by 
the North Carolina Council of Teachers of Mathematics. "She pioneered in the
 Mathematics Section of the North Carolina Teachers Association, helping to pave
 the way for integrated organizations," as the award states.</p>
<p>
In the last years of her life, Marjorie Lee Browne used her own money to help gifted
 math students pursue their education. Some students came to her with less than adequate
 preparations and she helped them pursue study of mathematics and complete their Ph.D. 
degrees. Unfortunately, on October 19, 1979, Dr. Marjorie Lee Browne died of a heart 
attack at the age of 65.</p>
<hr/>
<h3>Additional Remarks</h3>
<p>
Marjorie Lee Browne's Ph.D. dissertation was on "Studies of one parameter subgroups of certain
 topological and matrix groups," written under the direction of G.Y. Rainich at the 
University of Michigan. Her paper, "A Note on the Classical Groups," was published 
in The American Mathematical Monthly, June-July 1955, 424-427. This paper set forth
 some topological properties of and relations between certain classical groups. 
Browne writes in the paper that "while much of the material included here may be 
known to a few, the main interest of this paper lies in the simplicity of the proofs 
of some important, though obscured, results."</p>
<p>
In addition to her own grants and fellowships to pursue mathematical studies, Browne 
received several grants to support the teaching of mathematics at North Carolina 
Central University. This institution became the first predominantly Black institution
 to be awarded an NSF Institute for secondary teachers of mathematics, a program Browne 
directed for 13 summers. In 1960, through her efforts, NCCU received a grant from IBM 
for the support of academic computing. In 1969 she obtained for her department the first 
Shell Grant for awards to outstanding mathematics students. For twenty five years she was
 the only person in the mathematics department at NCCU with a Ph.D. in mathematics. She 
taught both undergraduate and graduate courses, and served as an advisor for ten Master's
 degrees in mathematics.</p>
<h3>References</h3>
<ol>
<li>Kenschaft, Patricia. "Marjorie Lee Browne: In memoriam," <a href="http://www.drivehq.com/file/df.aspx/shareID8755087/fileID748721553/1980_09-10.pdf" target="_blank">Association 
for Women in
Mathematics Newsletter</a> 10(5) (September-October 1980), 8-11. [Reprinted in <i>Complexities: Women in Mathematics</i>, Bettye Anne Case and Anne Leggett, Editors, Princeton University Press (2005), 19-23.]</li>
<li>Kenschaft, Patricia. "Marjorie Lee Browne," in Black Women in America: An Historical
Encyclopedia, Darlene Clark Hine, Editor. Carlson Publishing, Inc. 1993.</li>
<li>Giles-Giron, Jacqueline. "Black Pioneers in Mathematics: Brown [sic], Granville, Cox, 
Claytor, and Blackwell," American Mathematical Society Focus, January-February 1991, 18.</li>
<li>Malloy, Carol. "Marjorie Lee Browne," <i>Notable Women in Mathematics: A Biographical Dictionary,</i> Charlene Morrow and Teri Perl, Editors, Greenwood Press, 1998, 21-25.
</li>
<li><a href="http://www.math.buffalo.edu/mad/PEEPS/browne_marjorie_lee.html" target="_blank">Profile of 
Marjorie Lee Browne</a> by Scott Williams, SUNY at Buffalo, "Mathematicians of the African Diaspora".</li>
<li><a href="http://zbmath.org/authors/?q=browne.marjorie-lee" target="_blank">Author Profile at zbMath</a></li>
<li><a href="http://www.genealogy.ams.org/id.php?id=5152" target="_blank">Mathematics Genealogy Project</a></li>
<li><a href="http://www-groups.dcs.st-and.ac.uk/~history/Mathematicians/Browne.html" target="_blank">Biography</a> at the MacTutor History of Mathematics Archive</li>
</ol>
<p>
<font size="-1">Photo Credit: Photograph is used with permission of the MAA Committee
on Participation of Women and is taken from <i>Winning Women Into 
Mathematics</i>, published
by the Mathematical Association of America, 1991.
</font>
</p>
<hr/>
<p class="copyright">
<script src="styles/copyright.js" type="text/javascript">
</script>
<br/><br/>
<script src="styles/lastmodified.js" type="text/javascript">
</script>
</p>
</div>
<!-- Start of StatCounter Code for BBEdit (Mac) -->
<script type="text/javascript">
var sc_project=1467224; 
var sc_invisible=0; 
var sc_security="e0c4154d"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'><\/"+"script>");
</script>
<noscript><div class="statcounter"><a href="http://statcounter.com/" target="_blank" title="web statistics"><img alt="web statistics" class="statcounter" src="http://c.statcounter.com/1467224/0/e0c4154d/0/"/></a></div></noscript>
<!-- End of StatCounter Code for BBEdit (Mac) -->
</body>
</html>
