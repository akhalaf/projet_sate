<!DOCTYPE html>
<html class="no-js" data-device-type="dedicated" lang="no-NO">
<head>
<title>Finner ikke siden – PayPal Norge</title>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<meta content="NOODP" name="robots"/>
<meta charset="utf-8"/>
<meta content="PayPal" name="application-name"/>
<meta content="name=My Account;action-uri=https://www.paypal.com/no/cgi-bin/webscr?cmd=_account;icon-uri=https://www.paypalobjects.com/webstatic/icon/favicon.ico" name="msapplication-task"/>
<meta content="name=Send Money;action-uri=https://www.paypal.com/no/webapps/mpp/send-money-online;icon-uri=https://www.paypalobjects.com/webstatic/icon/favicon.ico" name="msapplication-task"/>
<meta content="name=Request Money;action-uri=https://www.paypal.com/no/webapps/mpp/requesting-payments;icon-uri=https://www.paypalobjects.com/webstatic/icon/favicon.ico" name="msapplication-task"/>
<meta content="Finner ikke siden, 404" name="keywords"/>
<meta content="Finner ikke siden" name="description"/>
<meta content="#009cde" name="theme-color"/>
<link href="https://www.paypal.com/no/webapps/mpp/offers/wheretoshop" rel="canonical"/>
<meta content="summary" property="twitter:card"/><meta content="@paypal" property="twitter:site"/>
<meta content="https://www.paypal.com/no/webapps/mpp/offers/wheretoshop" property="og:url"/>
<meta content="website" property="og:type"/>
<meta content="Finner ikke siden – PayPal Norge" property="og:title"/><meta content="https://www.paypalobjects.com/webstatic/icon/pp258.png" property="og:image"/><meta content="Finner ikke siden" property="og:description"/>
<link as="font" crossorigin="" href="https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansSmall-Regular.woff2" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansBig-Light.woff2" rel="preload" type="font/woff2"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp144.png" rel="apple-touch-icon-precomposed" sizes="144x144"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp114.png" rel="apple-touch-icon-precomposed" sizes="114x114"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp72.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp64.png" rel="apple-touch-icon-precomposed"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp196.png" rel="shortcut icon" sizes="196x196"/>
<link href="https://www.paypalobjects.com/webstatic/icon/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="https://www.paypalobjects.com/webstatic/icon/pp32.png" rel="icon" type="image/x-icon"/>
<link href="https://www.paypalobjects.com" rel="dns-prefetch"/>
<link href="https://www.nexus.ensighten.com" rel="dns-prefetch"/>
<link href="https://www.google-analytics.com" rel="dns-prefetch"/>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni" type="application/ld+json">
  {
  "@context": "http://schema.org/",
  "@type": "Organization",
  "url": "https://www.paypal.com/",
  "logo": "https://www.paypalobjects.com/webstatic/i/logo/rebrand/ppcom.png"
  }
</script>
<style id="antiClickjack">
    html.js body {display: none !important;}
</style>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
  if (self === top || /paypal\.com$/.test(window.parent.location.hostname)) {var antiClickjack = document.getElementById("antiClickjack");if (antiClickjack) {antiClickjack.parentNode.removeChild(antiClickjack);}} else {top.location = self.location;}
</script>
<link href="https://www.paypalobjects.com/marketing-resources/css/ea/38663b5d3ffd7cbe12deb4269fa09be91bc033.css" rel="stylesheet" type="text/css"/>
<link href="https://www.paypalobjects.com/marketing-resources/css/1b/fa89f17d37eb3f97e39b926835ba73c0a3fd63.css" rel="stylesheet" type="text/css"/>
<style>
	    .pullout a { display: inline; }
    </style>
</head>
<body>
<div class="" id="body">
<header class="table-row pp-header" role="banner">
<div>
<div class="containerCentered ">
<a href="#Menu" id="menu-button" role="button">Meny</a>
<a class="paypal-img-logo" data-pa-click="header|paypal-logo" href="https://www.paypal.com/no/webapps/mpp/home">PayPal</a>
<nav class="main-menu" id="main-menu" role="navigation">
<ul>
<li><a aria-label="PRIVAT" href="https://www.paypal.com/no/webapps/mpp/home" id="header-one" rel="menuitem">PRIVAT</a><div aria-label="header-one" class="menu-wrapper" id="submenu-one"><a aria-label="PRIVAT" data-pa-click="header|closing|personal" href="https://www.paypal.com/no/webapps/mpp/home" id="header-closing-personal" rel="menuitem">PRIVAT</a><ul class="subnav list" id="header-one-menu"><li><a href="https://www.paypal.com/no/webapps/mpp/personal">Slik fungerer PayPal<em>Se hva du kan gjøre med en privatkonto</em></a></li><li><a href="https://www.paypal.com/no/webapps/mpp/pay-online">Betal på nettet<em>Se hvordan du betaler på nettet</em></a></li><li><a href="https://www.paypal.com/no/webapps/mpp/send-money-online">Overfør penger<em>Se hvordan du overfører penger i Norge og utenlands</em></a></li><li><a href="https://www.paypal.com/no/webapps/mpp/requesting-payments">Be om penger<em>Se hvordan du ber om å få tilbake penger du har lagt ut</em></a></li><li><a href="https://www.paypal.com/no/webapps/mpp/mobile-apps">Skaff deg PayPal-appen<em>Få overblikk over kontoen din på mobilen</em></a></li></ul></div></li><li><a aria-label="BEDRIFT" data-config-enhanced="true" href="https://www.paypal.com/no/business" id="header-two" rel="menuitem">BEDRIFT</a><div aria-label="header-two" class="menu-wrapper" id="submenu-two"><a aria-label="BEDRIFT" data-pa-click="header|closing|business" href="https://www.paypal.com/no/business" id="header-closing-business" rel="menuitem">BEDRIFT</a><ul class="subnav" id="header-two-menu"><ul class="subnav-grouped subnav-grouped--no-header"><li>PayPal Commerce Platform</li><li><a data-pa-click="HeaderSubnav0-Link-Oversikt" href="https://www.paypal.com/no/business">Oversikt</a></li><li><a data-pa-click="HeaderSubnav1-Link-Ta imot betalinger" href="https://www.paypal.com/no/business/accept-payments">Ta imot betalinger</a></li><li><a data-pa-click="HeaderSubnav2-Link-Foreta betalinger" href="https://www.paypal.com/no/business/make-payments">Foreta betalinger</a></li><li><a data-pa-click="HeaderSubnav3-Link-Håndter risiko" href="https://www.paypal.com/no/business/manage-risk">Håndter risiko</a></li><li><a data-pa-click="HeaderSubnav4-Link-Øk veksten" href="https://www.paypal.com/no/business/accelerate-growth">Øk veksten</a></li><li><a data-pa-click="HeaderSubnav5-Effektiviser driften" href="https://www.paypal.com/no/business/streamline-operations">Effektiviser driften</a></li></ul><ul class="subnav-grouped subnav-grouped--no-header"><li>LØSNINGER FOR</li><li><a data-pa-click="HeaderSubnav0-Link-Virksomheter" href="https://www.paypal.com/no/business">Virksomheter</a></li><li><a data-pa-click="HeaderSubnav1-Link-Store bedrifter" href="https://www.paypal.com/no/business/enterprise">Store bedrifter</a></li><li><a data-pa-click="HeaderSubnav2-Link-Plattformer og markedsplasser" href="https://www.paypal.com/no/business/platforms-and-marketplaces">Plattformer og markedsplasser</a></li></ul><ul class="subnav-grouped subnav-grouped--no-header subnav-grouped-spacing-left"><li>RESSURSER</li><li><a data-pa-click="HeaderSubnav0-Link-Komme i gang" href="https://www.paypal.com/no/business/getting-started">Komme i gang</a></li><li><a data-pa-click="HeaderSubnav1-Link-Priser" href="https://www.paypal.com/no/webapps/mpp/merchant-fees">Priser</a></li></ul></ul></div></li>
<li><a class="no-drop" href="https://www.paypal.com/no/webapps/mpp/partners-and-developers" id="header-partners">PARTNERE OG UTVIKLERE</a></li>
</ul>
<ul class="sublist">
<li><a class="btn btn-small btn-white-border signup-mobile" data-pa-click="header|signup-mobile" href="https://www.paypal.com/webapps/mpp/account-selection" id="signup-button-mobile" name="SignUp_header">Opprett konto</a></li>
</ul>
</nav>
<style>
  .pp-header-open--enhanced #body.menu-open .pp-header.table-row {
      height: 365px;
      max-height: 365px
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper {
      height: 100%;
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper a {
      font-family: pp-sans-small-regular,Helvetica Neue,Arial,sans-serif;
      font-weight: 700;
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li {
      height: 34px;
      width: 100%;
      text-align: left;
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped .submenu-col--l2 li:not(:first-child) a {
      font-weight: 400;
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped .submenu-col--l2 li:not(:first-child) {
      height: 28px;
  }

  .pp-header-open--enhanced #body.menu-open.rtl .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li {
      text-align: right;
  }

  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li.submenu-col__header,
  .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped.subnav-grouped--no-header li:first-child {
      display: block;
      text-transform: uppercase;
      white-space: nowrap;
      color: #fff;
      font-weight: 400;
      font-family: PayPalSansBig-Light, Helvetica Neue, Arial, sans-serif;
      margin-bottom: 0;
      margin-top: 25px;
      height: 20px;

      border-bottom: 1px solid rgba(255, 255, 255, 0.3);
      padding-bottom: 7px;
  }

  @media (min-width: 1152px) {
      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li a {
          padding-left: 0;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li.submenu-col__header,
      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped.subnav-grouped--no-header li:first-child {
          padding-top: 18px;
          margin-top: 0;
          height: 26px;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped li.submenu-col__header {
          margin-bottom: 12px;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .subnav-grouped {
          width: 25%;
          box-sizing: border-box;
          padding-right: 30px;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .subnav-grouped.subnav-grouped--with-l2 {
          width: 40%;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .subnav-grouped.subnav-grouped--without-l2 {
          width: 20%;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .subnav-grouped.subnav-grouped--with-l2 .submenu-col {
          width: 50%;
          display: inline-block;
          vertical-align: top;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .subnav-grouped.subnav-grouped-spacing-left {
          margin-left: 25%;
      }

      .pp-header-open--enhanced #body.menu-open .pp-header.table-row .main-menu .menu-wrapper .subnav .subnav-grouped .submenu-col--l2 {
        box-sizing: border-box;
        padding-left: 20px;
      }

      .pp-header.table-row .main-menu .menu-wrapper .subnav li {
        -webkit-flex-grow: 0;
        -ms-flex-positive: 0;
        flex-grow: 0;
        width: 31%;
        height: 50%;
      }
      html[lang='en-GB'] .pp-header.table-row .main-menu .menu-wrapper .subnav.lvl-2 li {
        width: 25%;
      }
      html[lang='en-GB'] .pp-header.table-row .main-menu .menu-wrapper .subnav.lvl-2 li > ul > li {
        width: 36%;
      }
  }
</style>
<div class="header-buttons" id="header-buttons">
<a class="btn btn-small btn-secondary" data-pa-click="header|login" href="https://www.paypal.com/no/signin" id="ul-btn">Logg på</a>
<a class="btn btn-small btn-signup" data-pa-click="header|signup" href="https://www.paypal.com/no/webapps/mpp/account-selection" id="signup-button">Opprett konto</a>
</div>
</div>
</div>
</header>
<div class="containerMobileFullWidth" id="main" role="main">
<section class="row-fluid row hero-bg dark">
<div class="parallax-bg scroll-animate" data-translate-y-end="80" data-translate-y-start="0"></div>
<div class="containerCentered container">
<div class="span10 col-sm-10 center-block center-text text-center reverseLink editorial-container">
<div class="editorial-cell ">
<h1 class="x-large h2">Finner ikke siden.</h1>
<p class="contentPara">Siden finnes ikke.</p>
</div>
</div>
</div>
</section>
<section class="row-fluid row pullout center-text">
<div class="containerCentered container">
<div class="center-block span8 col-sm-8">
<p class="contentPara">Vi kan ikke finne noen side med adressen du har skrevet inn.</p>
<p class="contentPara"></p>
</div>
</div>
</section>
</div>
<footer class="global-footer" role="contentinfo">
<div class="containerCentered containerExtend">
<ul class="footer-main secondaryLink">
<li><a href="https://www.paypal.com/no/selfhelp/home">Hjelp og kontakt</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/paypal-fees">Gebyrer</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/paypal-safety-and-security">Sikkerhet</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/about-paypal-products">Funksjoner</a></li>
<li><a href="https://www.paypal.com/no/shopping">Butikker</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/mobile-apps">PayPal-appen</a></li>
<li class="country-selector ">
<a class="country norway" href="https://www.paypal.com/no/webapps/mpp/country-worldwide" title="See all countries/regions">See all countries/regions</a>
</li>
</ul>
<hr/>
<ul class="footer-secondary secondaryLink">
<li><a href="https://www.paypal.com/no/webapps/mpp/about" target="_blank">Om PayPal</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/jobs" target="_blank">Jobb</a></li>
<li><a href="https://developer.paypal.com/">Utviklere</a></li>
<li><a href="https://www.paypal.com/no/webapps/mpp/partner-programme" target="_blank">Partnere</a></li>
</ul>
<ul class="footer-tertiary copyright-section secondaryLink">
<li class="footer-copyright" id="footer-copyright">© 1999–2021 </li>
<li id="footer-privacy"><a href="https://www.paypal.com/no/webapps/mpp/ua/privacy-full?locale.x=no_NO">Personvern</a></li>
<li class="footer-legal"><a href="https://www.paypal.com/no/webapps/mpp/ua/legalhub-full?locale.x=no_NO">Brukeravtale</a></li>
</ul>
</div>
</footer>
<style nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">@font-face { font-family: "PayPalSansSmall-Regular"; font-style: normal; font-display: swap; src: url('https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansSmall-Regular.woff2') format('woff2'), url('https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansSmall-Regular.woff') format('woff'), url('https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansSmall-Regular.eot?#iefix') format('embedded-opentype'), url('https://www.paypalobjects.com/digitalassets/c/paypal-ui/fonts/PayPalSansSmall-Regular.svg') format('svg'); } #gdprCookieBanner { font-family: PayPalSansSmall-Regular, sans-serif; } @keyframes slideInFromBottom { 0% { transform: translateY(100%); opacity: 0; } 100% { transform: translateY(0); opacity: 1; } } .gdprCookieBanner_container { animation: 1s ease-in 0s 1 slideInFromBottom; width: 100%; bottom: 0; position: fixed; background-color: #012169; z-index: 1051; color: white; text-shadow: none; } .gdprCookieBanner_wrapper { max-width: 1024px; margin: 0 auto; padding: 0px; color: white; } #gdprCookieContent_wrapper { padding: 24px; display: flex; } #gdprCookieContent_wrapper a { font-weight: 700; } .gdprCookieBanner_content { max-width: 545px; font-weight: 400; font-size: 15px; line-height: 24px; margin: 0 48px 0 0; color: white; } .gdprCookieBanner_content a { font-size: inherit; color: white; text-decoration: underline; } .gdprCookieBanner_content a:focus { border: 1px solid rgba(256, 256, 256, 0.5); } .gdprCookieBanner_buttonGroup { display: flex; flex-direction: row; justify-content: flex-end; flex-shrink: 0; } .gdprCookieBanner_buttonGroup button { color: white; font-size: 13px; } .gdprCookieBanner_buttonGroup .gdpr_btn_reversed { color: #0070ba; } #declineAllButton { cursor: pointer; margin-right: 30px; } .gdprCookieBanner_buttonWrapper { display: flex; justify-content: center; flex-direction: column; text-align: center; } button.gdpr_btn { display: inline-block; padding: 6px 24px; color: #0070ba; border-radius: 18px; font-size: 13px; line-height: 1.6; text-align: center; text-decoration: none; cursor: pointer; transition: all 250ms ease; -webkit-font-smoothing: antialiased; } button.gdpr_btn_reversed { border-color:  transparent; background-color: #ffffff; color: #0070ba; } .col-sm-12 { position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; } .gdprCookieBanner_acceptButton_arrow { display: none; } .gdprCookieBanner-acceptedAll { height: auto; margin-bottom: 12em; } @media only screen and (max-device-width : 1024px) { .gdprCookieBanner_content { font-size: 13px; color: white; } } @media (max-width: 768px) { .gdprCookieBanner_buttonGroup button { font-size: 15px; } #gdprCookieContent_wrapper { padding: 19px 24px; flex-direction: column; } .gdprCookieBanner_content { max-width: 100%; font-size: 13px; margin: 0; line-height: 18px; } .gdprCookieBanner_buttonWrapper { justify-content: flex-end; flex-direction: row; } .gdprCookieBanner_buttonWrapper button.gdpr_btn_reversed { background-color: transparent; border: none; color: white; padding: 0; } .gdprCookieBanner_acceptButton_arrow { margin-left: 5px; border: solid white; border-width: 0 2px 2px 0; display: inline-block; padding: 4px; transform: rotate(-45deg); -webkit-transform: rotate(-45deg); } } @media only screen and (max-width: 575.98px) { .gdprHideCookieBannerMobile { display:none; } }</style><div class="gdprCookieBanner_container gdprCookieBanner_container-custom" id="gdprCookieBanner"><div class="gdprCookieBanner_wrapper gdprCookieBanner_wrapper-custom"><div class="col-sm-12" id="gdprCookieContent_wrapper"><p class="gdprCookieBanner_content gdprCookieBanner_content-custom">Hvis du fortsetter å surfe, bruker vi informasjonskapsler som får nettstedet vårt til å fungere, forbedrer ytelsen og tilpasser brukeropplevelsen din. Hvis du godtar dem, bruker vi også informasjonskapsler for å gi deg tilpassede annonser. <a href="https://www.paypal.com/myaccount/profile/flow/cookies/?locale=no_NO" id="manageCookiesLink">Administrer informasjonskapsler</a></p><div class="gdprCookieBanner_buttonGroup"><div class="gdprCookieBanner_buttonWrapper gdprCookieBanner_buttonWrapper-custom"><button class="gdpr_btn gdpr_btn_reversed gdprCookieBanner_button gdprCookieBanner_button-custom" id="acceptAllButton">Godta informasjonskapsler<span class="gdprCookieBanner_acceptButton_arrow"></span></button></div></div></div></div></div><script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">(function () { function getFptiReqData () { return window && window.fpti && window.PAYPAL && window.PAYPAL.analytics && window.PAYPAL.analytics.instance && window.PAYPAL.analytics.instance.options && window.PAYPAL.analytics.instance.options.request && window.PAYPAL.analytics.instance.options.request.data } function getFptiPage () { return getFptiReqData() && getFptiReqData().page; } function postAjax(success) { var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP"); xhr.open('GET', 'https://www.paypal.com/myaccount/profile/api/cookies/accept?marketing=true&performance=true&functional=true&type=explicit_banner'); xhr.withCredentials = true; xhr.onreadystatechange = function() { if (xhr.readyState > 3 && xhr.status == 200) { success(); } }; xhr.setRequestHeader('Accept', 'application/json','Content-Type', 'application/json'); xhr.send(null); return xhr; }; function bindGdprEvents () { var acceptAllButton = document.getElementById('acceptAllButton'); var manageCookiesLink = document.getElementById('manageCookiesLink'); var cookieLanguage = document.getElementById('gdprCookieContent_wrapper'); var cookieBanner = document.getElementById('gdprCookieBanner'); var trackingPageName = '' || getFptiPage() || document.title; if (manageCookiesLink) { manageCookiesLink.onclick = function() { var acceptAllData = { page:  'main:privacy:policy:gdpr_v2', pgrp:  'main:privacy:policy', displayPage: window.fpti.pgrp, ppage: 'privacy_banner', bannerType: 'cookiebanner', e: 'cl', link: 'managecookies', pglk: trackingPageName + '|managecookies', pgln: trackingPageName + '|managecookies', ccpg: 'NO', flag: 'gdpr_v2', bannerVersion: 'v2' }; window.PAYPAL.analytics.Analytics.prototype.recordClick({data: acceptAllData}); }; } if (acceptAllButton && cookieBanner && cookieLanguage) { acceptAllButton.onclick = function() { cookieLanguage.style.display = 'none'; cookieBanner.style.display = 'none'; document.body.className = document.body.className.replace("gdprCookieBanner-acceptedAll",""); var acceptAllData = { page:  'main:privacy:policy:gdpr_v2', pgrp:  'main:privacy:policy', displayPage: window.fpti.pgrp, ppage: 'privacy_banner', bannerType: 'cookiebanner', e: 'cl', flag: 'gdpr_v2', link: 'acceptcookies', pglk: trackingPageName + '|acceptcookies', pgln: trackingPageName + '|acceptcookies', ccpg: 'NO', bannerVersion: 'v2' }; window.PAYPAL.analytics.Analytics.prototype.recordClick({data: acceptAllData}); var fptiData = JSON.parse(JSON.stringify(window.fpti)); var updatedFptiData =Object.assign(fptiData, { page:  'main:privacy:policy:gdpr_v2', pgrp:  'main:privacy:policy', displayPage: window.fpti.pgrp, ppage: 'privacy_banner', bannerType: 'cookiebanner', cookieBannerHidden: 'true', ccpg: 'NO', flag: 'gdpr_v2', bannerVersion: 'v2' }); window.PAYPAL.analytics.Analytics.prototype.logActivity(updatedFptiData); postAjax(function() { console.log('fired'); }); }; } window.hideGdprBanner = function () { var cookieBannerUi = document.getElementById('gdprCookieBanner'); if (cookieBannerUi && cookieBannerUi.className.indexOf('gdprHideCookieBannerMobile') === -1) { cookieBannerUi.className += " gdprHideCookieBannerMobile"; } }; window.showGdprBanner = function () { var cookieBannerUi = document.getElementById('gdprCookieBanner'); if (cookieBannerUi) { cookieBannerUi.className = cookieBannerUi.className.replace("gdprHideCookieBannerMobile",""); } }; document.body.addEventListener("focus", function (event) { if (event.target.type === 'text' || event.target.type === 'number' || event.target.type === 'password' || event.target.type === 'email' || event.target.type === 'select-one') { window.hideGdprBanner(); } }, true); }; window.bindGdprEvents = bindGdprEvents; function gdprSetup () { var manageLink = document.getElementById('manageCookiesLink'); var acceptAllLink = document.getElementById('acceptAllButton'); var trackingPageName = '' || getFptiPage() || document.title; document.body.className = document.body.className += " gdprCookieBanner-acceptedAll"; var pageName = trackingPageName || getFptiPage() || document.title; var fptiData = JSON.parse(JSON.stringify(window.fpti)); var updatedFptiData =Object.assign(fptiData, { page:  'main:privacy:policy:gdpr_v2', pgrp:  'main:privacy:policy', displayPage: window.fpti.pgrp, ppage: 'privacy_banner', bannerType: 'cookiebanner', ccpg: 'NO', flag: 'gdpr_v2', bannerVersion: 'v2' }); window.PAYPAL.analytics.Analytics.prototype.logActivity(updatedFptiData); if (manageLink) { manageLink.setAttribute('pagename', (pageName + '|managecookies')); } if (acceptAllLink) { acceptAllLink.setAttribute('pagename', (pageName + '|acceptcookies')); } bindGdprEvents(); }; function isPageReady () { var cookieBannerUi = document.getElementById('gdprCookieBanner'); return !!(cookieBannerUi && getFptiReqData()); } var maxRetries = 34; function triggerBanner() { if (isPageReady()) { gdprSetup(); } else { if (maxRetries-- > 0) { setTimeout(triggerBanner, 150); } else { var acceptAllButton = document.getElementById('acceptAllButton'); var cookieLanguage = document.getElementById('gdprCookieContent_wrapper'); var cookieBanner = document.getElementById('gdprCookieBanner'); if ( acceptAllButton ) { acceptAllButton.onclick = function() { cookieLanguage.style.display = 'none'; cookieBanner.style.display = 'none'; document.body.className = document.body.className.replace("gdprCookieBanner-acceptedAll",""); postAjax(function() { console.log('fired'); }); }; } } } } triggerBanner(); })();</script>
<!--[if lte IE 9]>
 <script src="https://www.paypalobjects.com/marketing-resources/js/b2/f34e53f94c2a6fb2a579294142f824ea64fad5.js"></script> 
<![endif]-->
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
    var PP_GLOBAL_JS_STRINGS = {
        "CLOSE": "Lukk",
        "TOGGLE_FULL_SCREEN": "Vis full skjerm",
        "NO_PLAY_VIDEO": "Videoen kan ikke vises i nettleseren",
        "CLICK_TO_PLAY": "Satt på pause. Klikk for å fortsette.",
        "PLAY": "Sett på",
        "PAUSE": "Pause",
        "PLAY_OR_PAUSE": "Start eller sett på pause",
        "MUTE": "Slå av lyden",
        "SEEK_VIDEO": "Flytt til et annet sted i videoen",
        "REWIND": "" || "",
        "RESTART": "" || "",
        "FORWARD": "" || "",

        "MESSAGE_REQUIRED": "Dette feltet er obligatorisk.",
        "MESSAGE_REMOTE": "Please fix this field",
        "MESSAGE_EMAIL": "Oppgi en gyldig e-postadresse",
        "MESSAGE_EMAIL_OR_PHONE": "Oppgi en gyldig e-postadresse",
        "MESSAGE_URL": "Skriv inn en gyldig URL",
        "MESSAGE_DATE": "Please enter a valid date",
        "MESSAGE_DATEISO": "Please enter a valid date (ISO)",
        "MESSAGE_NUMBER": "Må være et tall",
        "MESSAGE_DIGITS": "Fyll inn bare tall",
        "MESSAGE_CREDITCARD": "Please enter a valid credit card number",
        "MESSAGE_EQUALTO": "Your passwords don't match. Please retype your password to confirm it.",
        "MESSAGE_ACCEPT": "Please enter a value with a valid extension",
        "MESSAGE_MAXLENGTH": "Please enter no more than {0} characters",
        "MESSAGE_MINLENGTH": "Angi minst {0} sifre",
        "MESSAGE_RANGELENGTH": "Please enter a value between {0} and {1} characters long",
        "MESSAGE_RANGE": "Please enter a value between {0} and {1}",
        "MESSAGE_MAX": "Please enter a value less than or equal to {0}",
        "MESSAGE_MIN": "Please enter a value greater than or equal to {0}",
        "MESSAGE_BADPHONE": "We can't send money to that phone number. Please use an email address."
    };

    var HOLIDAYS = '';

    var BROWSER_TYPE = 'unknown';
</script>
<script src="https://www.paypalobjects.com/marketing-resources/js/ca/4c889762ab729bb7919a3362b4232e9fc29419.js"></script>
<script defer="" src="https://www.paypalobjects.com/digitalassets/c/website/marketing/global/kui/js/opinionLab-2.0.0.js"></script>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
  
  var dataLayer = {

    
    contentCountry: 'NO'.toLowerCase(),

    
    contentLanguage: 'no'.toLowerCase(),

    
    localTimeZone: '',

    
    localTime: (new Date()).toString(),

    
    fptiGuid: '00450f0c1770a2735714196ffff23d9f',

    
    gaCid: '',

    
    gaUid: ''
  };
</script>
<script defer="" src="https://www.paypalobjects.com/tagmgmt/bs-chunk.js"></script>
<script src="https://www.paypalobjects.com/pa/js/min/pa.js"></script>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
         (function(){
            var callFpti = function(fptiDataString) {
              PAYPAL.core.pta = PAYPAL.analytics.setup({
                data:fptiDataString,
                trackCPL: true,
                url:'https:\/\/t.paypal.com\/ts'
              });
          };
          if(typeof PAYPAL.analytics !== "undefined") {
            PAYPAL.core = PAYPAL.core || {};
            var  fptiDataString = 'pgrp=main%3Amktg%3A%3A%3Apage-not-found&page=main%3Amktg%3A%3A%3Apage-not-found%3A%3A%3A&pgst=Unknown&calc=90849c34b9f23&nsid=wamLBKKh4z7e_6tCPZDIBaJ7lKzAS5Dt&rsta=no_NO&pgtf=Nodejs&env=live&s=ci&ccpg=no&csci=a3620919384d461ea473684d62d12b96&comp=mppnodeweb&tsrce=mppnodeweb&cu=0&ef_policy=gdpr_v2&pgld=Unknown&bzsr=main&bchn=mktg&tmpl=page-not-found.dust&lgin=out&shir=main_mktg__&pros=3&lgcook=0';
            
            if (typeof ga !== 'undefined' && ga !== null) {
                ga(function(tracker) {
                  var gaClientId = tracker.get('clientId');
                  if (gaClientId) {
                    fptiDataString += "&gacook=" + gaClientId;
                  }
                  callFpti(fptiDataString);
                });
            } else {
                callFpti(fptiDataString);
            }
          }
        }());
      </script>
<noscript><img alt="" border="0" height="1" src="https:https://t.paypal.com/ts?nojs=1&amp;pgrp=main%3Amktg%3A%3A%3Apage-not-found&amp;page=main%3Amktg%3A%3A%3Apage-not-found%3A%3A%3A&amp;pgst=Unknown&amp;calc=90849c34b9f23&amp;nsid=wamLBKKh4z7e_6tCPZDIBaJ7lKzAS5Dt&amp;rsta=no_NO&amp;pgtf=Nodejs&amp;env=live&amp;s=ci&amp;ccpg=no&amp;csci=a3620919384d461ea473684d62d12b96&amp;comp=mppnodeweb&amp;tsrce=mppnodeweb&amp;cu=0&amp;ef_policy=gdpr_v2&amp;pgld=Unknown&amp;bzsr=main&amp;bchn=mktg&amp;tmpl=page-not-found.dust&amp;lgin=out&amp;shir=main_mktg__&amp;pros=3&amp;lgcook=0" width="1"/></noscript>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
	var PAYPAL = PAYPAL ? PAYPAL : {};
	PAYPAL.opinionLabVars = {
		'isPaymentFlow': false,
		'isSiteRedirect': false,
		'languageCode': 'no',
		'countryCode': 'NO',
		'serverName': 'www.paypal.com',
		'commentCardCmd': '',
		'accountNumber': '',
		'miniBrowser': false,
		'sitefb_plus_icon': "https://www.paypalobjects.com/en_US/i/scr/sm_333_oo.gif",
		'rLogId': escape('rZJvnqaaQhLn%2FnmWT8cSUmR1G%2FXOGzg1b29TMfAlTtYrXyRuBLhx4D82gZGNhzJ4lUBfQfanYVw_17700450f1a'),
		'showSitefbIcon': false,
	    'experimentId': '',
	    'treatmentId': '',
	    'guid': '00450f0c1770a2735714196ffff23d9f',
	    'fptiPagename': '',
		'fptiPageGroup': '',
	};
</script>
<script nonce="c5Cq97gAoGfCnP9ZEyTaUd2My/gDtY7cEbH2kpU9gTZU30Ni">
    $(document).ready(function(){
        if(window.console || "console" in window) {
            console.log("%c WARNING!!!", "color:#FF8F1C; font-size:40px;");
            console.log("%c This browser feature is for developers only. Please do not copy-paste any code or run any scripts here. It may cause your PayPal account to be compromised.", "color:#003087; font-size:12px;");
            console.log("%c For more information, http://en.wikipedia.org/wiki/Self-XSS", "color:#003087; font-size:12px;");
        }
    });
</script>
<script defer="" src="https://www.paypalobjects.com/activation/js/marketingIntentsV2.js"></script>
</div></body>
</html>
