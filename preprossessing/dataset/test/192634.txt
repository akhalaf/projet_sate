<!DOCTYPE html>
<html lang="tr-TR">
<head>
<meta charset="utf-8"/>
<link href="https://barandogan.av.tr/" rel="canonical"/>
<meta content="width=device-width,minimum-scale=1,initial-scale=1" name="viewport"/>
<meta content="Avukat Baran Doğan Hukuk Bürosu; ceza, tazminat, gayrimenkul, miras, idare hukuku ve boşanma davası alanlarında İstanbul avukatı olarak çalışmaktadır." name="Description"/>
<meta content="summary" name="twitter:card"/>
<meta content="Avukat Baran Doğan Hukuk Bürosu; ceza, tazminat, gayrimenkul, miras, idare hukuku ve boşanma davası alanlarında İstanbul avukatı olarak çalışmaktadır." name="twitter:description"/>
<meta content="Avukat Baran Doğan | Ceza Hukuku, Tazminat, G.menkul, İstanbul Avukatı" name="twitter:title"/>
<meta content="@avukatbdhukuk" name="twitter:site"/>
<meta content="https://barandogan.av.tr/images/logo/baran-dogan-logo-600x60.webp" name="twitter:image"/>
<meta content="@avukatbdhukuk" name="twitter:creator"/>
<meta content="tr_TR" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Avukat Baran Doğan | Ceza Hukuku, Tazminat, G.menkul, İstanbul Avukatı" property="og:title"/>
<meta content="Avukat Baran Doğan Hukuk Bürosu; ceza, tazminat, gayrimenkul, miras, idare hukuku ve boşanma davası alanlarında İstanbul avukatı olarak çalışmaktadır." property="og:description"/>
<meta content="https://barandogan.av.tr/" property="og:url"/>
<meta content="Avukat Baran Doğan" property="og:site_name"/>
<meta content="https://barandogan.av.tr/images/logo/baran-dogan-logo-600x60.webp" property="og:image"/>
<meta content="600" property="og:image:width"/>
<meta content="60" property="og:image:height"/>
<meta content="528500253968830" property="fb:app_id"/>
<link href="https://plus.google.com/+BarandoganAvTr" rel="author"/>
<link href="https://plus.google.com/+BarandoganAvTr" rel="publisher"/>
<title>Avukat Baran Doğan | Ceza Hukuku, Tazminat, G.menkul, İstanbul Avukatı</title>
<link href="/feed.xml" rel="alternate" title="Avukat Baran Doğan Hukuk Bürosu" type="application/atom+xml"/>
<link as="script" href="https://cdn.ampproject.org/v0.js" rel="preload"/>
<script async="" src="https://cdn.ampproject.org/v0.js"></script>
<script type="application/ld+json">
			{
	"@context": "https://schema.org",
	"@type": "LegalService",
	"address": {
		"@type": "PostalAddress",
		"addressCountry": "Turkey",
		"addressLocality": "Şirinevler/Bahçelievler",
		"addressRegion": "istanbul",
		"postalCode": "34188",
		"streetAddress": "M.Fevzi Çakmak cd. 1.Sok No:9 Daire:6"
	},
	"areaServed": {
		"@type": "Country",
		"name": "Turkey"
	},
	"description": "Avukat Baran Doğan Hukuk Bürosu",
	"email": "avbaran11@gmail.com",
	"faxNumber": "0 212 652 06 03",
	"founder": {
		"@type": "Person",
		"@id":"https://barandogan.av.tr/hakkimizda/",
		"name": "Baran Doğan"
	},
	"foundingDate": "2003-01-01T00:00:00+0000",
	"geo": {
		"@type": "GeoCoordinates",
		"addressCountry": "Turkey",
		"latitude": "40.992834",
		"longitude": "28.846363",
		"name": "geoLocation",
		"postalCode": "34188"
	},
	"hasMap": "https://goo.gl/N150Wg",
	"legalName": "Avukat Baran Doğan Hukuk Bürosu",
	"logo": {
		"@type": "ImageObject",
		"url": "https://barandogan.av.tr/images/logo/baran-dogan-logo-600x60.webp"
	},
	"image": {
		"@type": "ImageObject",
		"url": "https://barandogan.av.tr/images/logo/baran-dogan-logo-600x60.webp"
	},
	"mainEntityOfPage": "https://barandogan.av.tr/",
	"name": "Avukat Baran Doğan",
	"sameAs": ["https://www.facebook.com/Avukatbarandogan",
		"https://twitter.com/av_barandogan",
		"https://plus.google.com/+BarandoganAvTr",
	"https://www.youtube.com/c/BarandoganAvTr"],
	"telephone": "+90212 652 15 44",
	"contactPoint" : [{
		"@type" : "ContactPoint",
		"telephone" : "+90212 652 15 44",
		"contactType" : "customer service"
	}],
	"url": "https://barandogan.av.tr/",
	"openingHours": "Mo,Tu,We,Th,Fr 09:00-18:00",
	"hasOfferCatalog": {
		"@type": "OfferCatalog",
		"@id": "https://barandogan.av.tr/faaliyet-alanlarimiz/",
		"name": "Hukuki Danışmanlık ve Avukatlık Alanları",
		"url": "https://barandogan.av.tr/faaliyet-alanlarimiz/"
	}
}

		</script>
<script type="application/ld+json">

    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement":[{
            "@type": "ListItem",
            "position": 1,
            "item": {
                "@id": "https://barandogan.av.tr/",
                "name": "Anasayfa"
   	        }
   	    },
	   {
            "@type": "ListItem",
            "position": 2,
            "item":
            {
                "@id": "https://barandogan.av.tr/",
                "name": "Avukat Baran Doğan | Ceza Hukuku, Tazminat, G.menkul, İstanbul Avukatı"
            }
        }
	 ]
	}



</script>
<script async="" custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
<script async="" custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<script async="" custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
<script async="" custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<meta content="zyVXpNljj6tRiNBahSC3r7Or3XimSeTZYyVzhvLOxsA" name="google-site-verification"/>
<style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom="">
		
		body{margin:0;font-family:'New York Times', serif;background-color:white}.anasayfa-link{color:#6d7a94}table{border-collapse:collapse}table td,th{border:1px solid black;text-align:center;padding:4px}.mesaj-gonderildi{text-align:center;padding:12px;background:#6d7a94;color:white;font-size:20px}.page-wrapper a{color:#1C9CBA}header.header-wrapper{background-color:#6d7a94;border:none}amp-accordion>section>:first-child{background-color:#6d7a94;border:none}.menu-wrapper{display:inline}.footer-container-wrapper{position:relative}.blog-post-content a{color:#1C9CBA;font-weight:bolder;font-size:15px}a{color:#000;text-decoration:none}.header-wrapper{background:#6d7a94;border:none;padding-right:0px}.header-left{display:inline}.positionrelative{position:relative}.header{background-color:#6d7a94;max-width:1080px;margin:0 10%}.header-container{width:100%;background-color:#6d7a94;margin:0 auto;position:absolute;z-index:1}.header ul{margin:0;padding:0;list-style:none;overflow:hidden;color:white}.header li a{display:block;text-decoration:none;color:white}.header li:hover{font-weight:bolder}.header .logo{display:block;float:left;width:48px;height:48px;padding:4px 0px 4px 0px;text-decoration:none}.header .logo:hover{cursor:pointer}.header .menu{clear:both;font-size:15px;padding-top:7px;transition:max-height .2s ease-out}.header .menu-icon{cursor:pointer;display:inline-block;float:right;padding:28px 20px;position:relative;user-select:none;border:none;background-color:#6d7a94}amp-sidebar{width:300px;padding-right:10px;background-color:#6d7a94}.header .menu-icon .navicon{background:white;display:block;height:2px;position:relative;transition:background .2s ease-out;width:18px}.header .menu-icon .navicon:before,.header .menu-icon .navicon:after{background:white;content:'';display:block;height:100%;position:absolute;transition:all .2s ease-out;width:100%}.header .menu-icon .navicon:before{top:5px}.header .menu-icon .navicon:after{top:-5px}@media (min-width: 48em){.header li{float:left;font-size:12px;letter-spacing:0.5px;color:#fff;text-transform:uppercase;padding:10px 15px}.header .menu{clear:none;float:right;max-height:none}.header .menu-icon{display:none}.alt-cizgi{border-bottom:2px solid white}}.main-home{max-width:1080px;margin:0 10%;text-align:justify;line-height:24px;font-size:16px}.main-home h1{color:#6d7a94;text-align:center}.page-wrapper{max-width:1200px;margin:0 5%;min-height:calc(100vh - 225px)}.page-wrapper-blog-anasayfa{display:flex}.category-page-wrapper{max-width:1200px;margin:0 5%;min-height:calc(100vh - 225px);display:flex}.mobile-home-image{display:none}.blog-three-columns{display:flex}.populer-makaleler{font-weight:900;font-size:15px}.section{overflow:hidden;margin:auto;max-width:1400px}.section a{position:relative;float:left;width:100%}.section a img{width:100%;display:block}.section a span{color:#fff;position:absolute;left:5%;bottom:5%;font-size:2em;text-shadow:1px 1px 0 #000}.section-split a span{display:none}.section-split a:hover span{display:block}.empty{height:68px}@media (min-width: 768px){.section-split a{width:50%}}.copyright{text-align:center;font-size:14px;background:#9099A2;font-style:italic}.social-media-container{text-align:center;display:inline-block}.social-media-icon{width:48px;height:48px;display:inline-block;fill:#d5d5d5}.social-media-icon:hover{fill:white;cursor:pointer}.footer-container{margin-top:40px;width:100%;background-color:#9099A2;position:absolute;top:100%;bottom:0;text-align:center}.footer-container footer{padding:25px 0px;background-color:#9099A2;clear:both}.telefon-wrapper{display:inline}.hukuk-burosu{color:white}.communication{text-align:center}.bread-crumbs li{list-style:none;display:inline}.bread-crumbs a{text-decoration:none}.bread-crumbs{max-width:1200px;text-align:right;padding:20px 0px;padding-right:20px;font-size:13px;font-weight:bold;line-height:40px}.bread-crumbs li:last-child{border-bottom:2px solid #6d7a94}.bread-crumbs .smaller-sign{color:#6d7a94;padding:0 1px}.apple{width:600}.left-bar-logo{max-width:200px;margin-bottom:20px}.left-bar{width:100%;vertical-align:top;color:#6d7a94;fill:#6d7a94}.contact-info{font-size:12px;color:#d5d5d5}.blog-post-content{display:inline-block;border-left:1px solid #d5d5d5;padding-left:20px;padding-right:20px;text-align:justify;font-family:"OpenSans-Regular",Helvetica,Arial;line-height:22px}.blog-post-content h1,h2,h3,h4,h5,h6,img{text-align:center}.social-share-container{clear:both;margin:0 25%}.social-follow-navbar{display:inline-block;padding-left:10px}.telefon{font-size:14px;text-decoration:none;color:#6d7a94;fill:#6d7a94;font-weight:bold}.calisma-saatleri{font-size:13px}.calisma-saatleri .text{font-size:15px;font-weight:bold;padding:3px}.calisma-saatleri .hours{padding-left:8px}.tel-numarasi{vertical-align:top}.adres-icon{display:inline-block;vertical-align:top}.adres{display:inline-block;fill:#d5d5d5}.adres a{text-decoration:none;color:#d5d5d5}.adres a:hover{display:inline-block;color:white;fill:white;cursor:pointer}.adres:hover{display:inline-block;color:white;fill:white;cursor:pointer}.yazdir{background:#6d7a94;font-size:20px;color:white;padding:10px 20px 10px 20px;width:60px;text-decoration:none;cursor:pointer;float:right}.yazdir:hover{background:lihgten(10%, #6d7a94);text-decoration:none}.adres-text{margin-top:9px;padding-right:9px;border-right:1px solid #d5d5d5;display:inline-block;text-align:left}.telefon-container{vertical-align:top;padding:22px 9px;display:inline-block}.left-bar-container{padding-bottom:20px;border-bottom:2px solid #d5d5d5;min-width:216px}.vertical-line{width:1px;background-color:white}.makaleyi-paylas{display:inline-block;font-weight:bold;vertical-align:middle;width:56px;height:33px}.yorumlar{margin:40px 10%;text-align:center}.header-logo{border-right:1px solid #f4f4f4;padding-right:10px}.ozet{padding-top:20px}.faaliyet-title-container{background-color:#6d7a94;color:white;fill:white;display:inline-block;border-radius:4px;padding:0px 30px;min-width:230px;vertical-align:middle}.faaliyet-title-container .anasayfa-icon{width:23px;height:35px;display:inline-block}amp-img{background-color:#6d7a94}.anasayfa-header{display:inline-block}.modal-list li{list-style:none;line-height:26px}.modal-list li:before{content:"• ";font-size:20px;padding-right:5px;color:#6d7a94}.faaliyet-header{display:inline-block;text-align:center}.hakkimizda-resim{display:inline-block;float:left;margin-right:14px}.kategori-item{padding:20px;background-color:#6d7a94;display:inline-block;margin:10px 2%;width:176px}.kategori-item a{text-decoration:none;color:white}.kategoriler{text-align:center;min-height:500px}.blog-category-content li{list-style:none}.form-input{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857143;color:#555;background-color:#fff;background-image:none;border:1px solid #ccc;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;margin-top:20px}.form-group-left{width:40%;display:inline-block}.form-group-right{display:inline-block;vertical-align:top;padding-left:40px;width:50%}.form-group-right .form-textarea{width:100%;height:173px}.submit{padding:14px 33px;border-color:#6d7a94;border-radius:3px;text-transform:uppercase;font-family:Montserrat,"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;color:#fff;background-color:#6d7a94;text-align:center}.submit-container{margin:25px 0;text-align:center}.iletisim-adres-calisma{opacity:0.9;color:white;background-color:#6d7a94;padding:20px 25%;border-radius:4px}.iletisim-adres-calisma .iletisim-adres-icon{fill:white;vertical-align:top}.iletisim-adres-calisma .iletisim-adres-text{text-align:left;color:white}.iletisim-adres-calisma .telefon-container{margin:22px 0px;padding:0px}.iletisim-adres-calisma .telefon{color:white;fill:white;font-size:17px}.inline{display:inline-block}.iletisim-main{max-width:1080px;margin:0 10%}.yorumlar-text{padding:15px;background-color:#6d7a94;color:white}.yorumlar-text:hover{cursor:pointer}.category-lists{padding:0px}.category-lists li{border:1px solid #D5D5D5;margin:20px 0}.category-lists li:hover{cursor:pointer;background:#D5D5D5}.category-lists .category-list-item{text-align:center;display:inline-block;vertical-align:top;padding-left:15px}.category-lists .category-list-image{display:inline-block}.right-panel-container{min-width:300px;display:flex;align-items:center;flex-direction:column}.right-panel{display:flex;flex-direction:column;padding:0px}.right-panel li{border-bottom:1px solid;padding:20px 0;display:flex;align-items:center}.right-panel a{border-bottom:none}.right-panel li:hover{cursor:pointer;background:#D5D5D5}.right-panel .right-panel-item{text-align:center;display:inline-block;vertical-align:top;padding-left:15px}.right-panel .right-panel-image{display:inline-block}.blog-post-mobile-top{display:none}.non-found-page{height:400px;text-align:center}.st0{stroke:#6d7a94}.hakkimizda-text{padding-left:210px;text-align:justify}.anasayfa-section{color:#6d7a94}.anasayfa-section h3{font-size:20px}.fax{color:white}.header-large{display:block}.header-accordion{display:none}.anasayfa-icon{width:23px;height:35px;vertical-align:middle;display:inline-block}@media (max-width: 48em){.header-container{border-bottom:0.5px solid white}.blog-post-top{display:none}.blog-post-mobile-top{display:block}.category-page-wrapper{flex-direction:column;margin:0}.blog-three-columns{flex-direction:column}.right-panel-container{width:100%}.right-panel{margin:0 15px}.page-wrapper-blog-anasayfa{display:block}.yorumlar{margin:0px}.empty{height:47px}.header-accordion{display:block}.menu-wrapper{display:none}.st0{stroke:#D5D5D5}.header{margin:0px 0px 0px 15px}.close-sidebar{color:white;font-size:24px;float:right;cursor:pointer}.calisma-saatleri .hours{padding-left:0}#sidebar ul{margin:0;list-style:none;overflow:hidden;color:white;padding:20px}#sidebar li a{display:block;text-decoration:none;color:white}#sidebar li a{padding:15px 0px;text-transform:uppercase}.blog-post-top{margin:0 15px;padding-right:20px}.main{margin:0 15px}.main-home{margin:0 15px}.iletisim-main{margin:0 15px;margin-top:10%}.blog-post-container{margin:0 15px}#sidebar .menu-icon{padding:22px 20px}.home-image{display:none}.mobile-home-image{display:block}.bread-crumbs{text-align:center}.left-bar{width:100%;text-align:center;background:#6d7a94;max-height:360px}.left-bar-logo{width:300px;margin:0 auto;fill:#d5d5d5;color:#d5d5d5;stroke:#d5d5d5}.calisma-saatleri{color:#d5d5d5;text-align:center}.left-bar .telefon{color:#d5d5d5;fill:#d5d5d5}.blog-post-content{display:inline-block;border-left:none;padding-left:0px;font-size:16px;width:100%;font-family:"OpenSans-Regular",Helvetica,Arial;line-height:22px}.left-bar-container{border-bottom:none;padding-top:80px}.footer-container footer{margin:0}.anasayfa-section{color:white;background:#6d7a94;fill:white;text-align:center;margin:20px 0px;border-radius:10px}.anasayfa-icon{width:23px;height:35px;vertical-align:middle;display:inline-block}.page-wrapper{margin:0px}.contact-form{text-align:none}.form-group-left{width:80%}.form-group-right{width:80%;padding-left:0px;vertical-align:none}.category-lists .category-list-image{display:none}.category-lists li{margin:20px 10px;background:#6d7a94;opacity:0.9}.category-lists a{color:white}.social-share-container{margin:0px}.footer-container .contact-info{text-align:left;margin:0 10%}.footer-container .contact-info .adres-text{border-right:none}.iletisim-adres-calisma{padding:15px 15%}.iletisim-tel-fax .telefon{color:white;fill:white}.hakkimizda-text{padding-left:0px}.blog-category-content{margin:0px 15px}.anasayfa-header{color:white;width:200px;word-wrap:word-break}.anasayfa-link{color:white}}@media print{.yorumlar{display:none}.social-share-container{display:none}.footer-container{display:none}.yazdir{display:none}amp-img{display:none}.header{display:none}}.gsc-search-box{padding:0px;margin:0px}.gsc-control-cse-tr{padding:0px;margin:0px}.gsc-control-cse{padding:0px;font-size:12px}.google-search{float:right;max-width:680px}.copy-warning{margin-bottom:32px;font-style:italic;background:#dbdfe4;padding:15px}.blog-post-top{max-width:1200px;margin:0 5%}.btn-rss{width:48px;height:48px;display:inline-block;vertical-align:middle}

	</style>
</head>
<body>
<div class="header-container">
<header class="header">
<div class="header-large">
<div class="header-left">
<a aria-label="Anasayfa" href="https://barandogan.av.tr">
<div class="logo">
<svg class="header-logo" fill="white" version="1.1" viewbox="0 0 32 32" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M14.3,26.7H6.3c-0.7,0-1.3-0.1-1.9-0.4c-0.6-0.3-1.1-0.7-1.6-1.2c-0.4-0.5-0.8-1-1.1-1.6
	c-0.3-0.6-0.4-1.3-0.4-1.9c0-0.7,0.1-1.3,0.4-1.9c0.2-0.6,0.6-1.2,1-1.6c0.4-0.5,0.9-0.9,1.5-1.1c0.6-0.3,1.2-0.4,1.9-0.4h8.2v-0.6
	H7c-0.7,0-1.3-0.1-1.9-0.4C4.5,15,4,14.6,3.6,14.1c-0.4-0.5-0.7-1-1-1.6c-0.2-0.6-0.3-1.2-0.3-1.8c0-0.7,0.1-1.3,0.3-1.9
	c0.2-0.6,0.5-1.1,0.9-1.6C4,6.7,4.4,6.3,5,6c0.6-0.3,1.2-0.4,1.8-0.4h7.5V5H6.8C6,5,5.3,5.1,4.7,5.5C4.1,5.8,3.5,6.3,3.1,6.8
	c-0.4,0.5-0.8,1.1-1,1.8C1.8,9.3,1.7,10,1.7,10.7c0,1.1,0.3,2.2,0.9,3.1c0.6,0.9,1.4,1.6,2.3,2.1c-0.6,0.2-1.2,0.5-1.7,0.8
	c-0.5,0.4-1,0.8-1.3,1.3c-0.4,0.5-0.6,1.1-0.8,1.6c-0.2,0.6-0.3,1.2-0.3,1.8c0,0.8,0.1,1.5,0.4,2.2c0.3,0.7,0.7,1.3,1.2,1.8
	c0.5,0.5,1.1,1,1.8,1.3c0.7,0.3,1.4,0.5,2.2,0.5h7.9V26.7z"></path>
<g>
<polygon points="16.5,26.7 16.5,5.6 16.5,5 15.9,5 15.9,27.3 16.5,27.3 	"></polygon>
<path d="M30.7,11.8c-0.4-1.4-0.9-2.5-1.7-3.5c-0.8-1-1.7-1.8-2.8-2.4S23.7,5,22.1,5h-4v0.6h4c1.4,0,2.6,0.3,3.6,0.8
		c1.1,0.5,2,1.3,2.7,2.2c0.7,0.9,1.3,2,1.7,3.3c0.4,1.3,0.6,2.7,0.6,4.2c0,1.5-0.2,2.9-0.5,4.2c-0.4,1.3-0.9,2.4-1.6,3.3
		c-0.7,0.9-1.6,1.7-2.7,2.2c-1.1,0.5-2.3,0.8-3.7,0.8h-4v0.6h4c1.4,0,2.6-0.3,3.8-0.8c1.1-0.5,2.1-1.3,2.9-2.3
		c0.8-1,1.4-2.1,1.8-3.5c0.4-1.4,0.6-2.9,0.6-4.6C31.2,14.6,31.1,13.1,30.7,11.8z"></path>
</g>
</svg>
</div>
</a>
<div class="vertical-line"></div>
<div class="social-follow-navbar">
<a aria-label="Avukat baran dogan facebook" href="https://www.facebook.com/Avukatbarandogan">
<div class="social-media-icon">
<svg viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M286.96783 455.99972V273.53753h61.244l9.1699-71.10266h-70.41246v-45.39493c0-20.58828 5.72066-34.61942 35.23496-34.61942l37.6554-.0112V58.807915c-6.5097-.87381-28.8571-2.80794-54.8675-2.80794-54.28803 0-91.44995 33.14585-91.44995 93.998125v52.43708h-61.40181v71.10266h61.40039v182.46219h73.42707z"></path></svg>
</div>
</a>
<a aria-label="Avukat baran dogan twitter" href="https://www.twitter.com/av_barandogan">
<div class="social-media-icon">
<svg viewbox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M456.0000005 131.94143215c-14.71678187 6.53070494-30.52721568 10.93793784-47.12774564 12.92381684 16.93529242-10.1563526 29.9498923-26.243391 36.08058446-45.39719395-15.85582533 9.40455555-33.42376205 16.2260499-52.12081284 19.90843697-14.9607613-15.9522823-36.29619418-25.91430246-59.89269215-25.91430246-45.3248512 0-82.07212362 36.7472724-82.07212362 82.06644967 0 6.4299925.72768282 12.6897668 2.129146 18.7041432-68.20785123-3.4313152-128.67077435-36.0947693-169.14724383-85.75309218-7.06547378 12.12095433-11.110993 26.2136028-11.110993 41.26231017 0 28.47183092 14.48273185 53.58610784 36.50471147 68.30288972-13.4458193-.42838247-26.09728703-4.11644347-37.16998094-10.2613205-.00425545.33759944-.00425545.68370978-.00425545 1.02982012 0 39.75871606 28.29026484 72.92431385 65.8276334 80.476328-6.88674468 1.86530776-14.1394585 2.87526908-21.61487444 2.87526908-5.2952045 0-10.43153868-.5290949-15.43737224-1.47664288 10.4400496 32.5967853 40.75023708 56.32662086 76.6535109 56.98905336-28.07891056 22.0148872-63.46585586 35.13587345-101.92382118 35.13587345-6.61723252 0-13.14935595-.39008338-19.5736745-1.15748376 36.32598235 23.29436068 79.46069272 36.8820285 125.80259767 36.8820285 150.94382582 0 233.4911419-125.04796367 233.4911419-233.49114192 0-3.5660713-.08227212-7.1023544-.23405002-10.62445265 16.02178808-11.5734191 29.94421835-26.02068884 40.94031407-42.48078878z"></path></svg>
</div>
</a>
</div>
<button aria-label="yan panel ac kapa" class="ampstart-btn caps m2 menu-icon" on="tap:sidebar.toggle">
<span class="navicon"></span>
</button>
</div>
<span class="menu-wrapper">
<ul class="menu">
<li><a aria-label="Anasayfa" class="alt-cizgi" href="/">Anasayfa</a></li>
<li><a aria-label="Hakkımızda" class="" href="/hakkimizda/">Hakkımızda</a></li>
<li><a aria-label="Faaliyet Alanlarımız" class="" href="/faaliyet-alanlarimiz/">Faaliyet Alanlarımız</a></li>
<li><a aria-label="Makaleler ve Haberler" class="" href="/blog/">Makaleler ve Haberler</a></li>
<li><a aria-label="İletişim" class="" href="/iletisim/">İletişim</a></li>
</ul>
</span>
</div>
</header>
</div>
<amp-sidebar id="sidebar" layout="nodisplay" side="left">
<ul class="menu">
<li class="close-sidebar" on="tap:sidebar.close" role="button" tabindex="0">X</li>
<li>
<a aria-label="Anasayfa" class="" href="/">Anasayfa</a>
</li>
<li>
<a aria-label="Hakkımızda" class="" href="/hakkimizda/">Hakkımızda</a>
</li>
<li>
<a aria-label="Faaliyet Alanlarımız" class="" href="/faaliyet-alanlarimiz/">Faaliyet Alanlarımız</a>
</li>
<li>
<a aria-label="Makaleler ve Haberler" class="" href="/blog/">Makaleler ve Haberler</a>
</li>
<li>
<a aria-label="İletişim" class="" href="/iletisim/">İletişim</a>
</li>
</ul>
</amp-sidebar>
<amp-img alt="Avukat Baran Doğan Hukuk Bürosu" class="home-image" height="300px" layout="responsive" src="/images/desktop-home-compressed.webp" width="1080px">
</amp-img>
<amp-img alt="Avukat Baran Doğan Hukuk Bürosu" class="mobile-home-image" height="600px" layout="responsive" src="/images/home-75-less.webp" width="500px">
</amp-img>
<main class="main-home">
<section>
<h1>Avukat Baran Doğan Hukuk Bürosu</h1>
Hukuk bilgisinin doğru ve güncel olması kadar, somut hukuki sorunları çözecek şekilde ele alınması da önemlidir. Somut hukuki problemler, mevzuat ve yargıtay kararları ışığında değerlendirilerek bir çözüme ulaşılmalıdır.
Avukatlık faaliyeti, farklı hukuk alanlarına ilişkin normların somut olaya doğru bir şekilde uygulanarak müvekkilin hukuki probleminin çözülmesi faaliyetidir. Hukuk büromuz, bir taraftan müvekillerine hukuki yardım sunarken diğer taraftan makale ve yargıtay kararları yayımlayarak uygulamaya katkı sunmaktadır.
Makalelerimiz, hukuk teorisi, güncel mevzuat (kanun, yönetmelik, tüzük vs.) ve yargıtay kararları ile şekillenen en son mahkeme uygulamaları dikkate alınarak hazırlanmıştır.
Mevzuat değişiklikleri veya yeni yargısal uygulamalar makalelere eklenerek bilgi akışının güncelliği sağlanmaktadır.
Avukat Baran Doğan Hukuk Bürosu, hukuki danışmanlık hizmetinin yanı sıra birçok hukuk alanında faaliyet göstermesine rağmen dört hukuk alanını temel faaliyet alanı olarak belirlemiştir.
</section>
<hr/>
<section>
<div class="anasayfa-section">
<div class="anasayfa-icon">
<svg id="Layer_1" version="1.1" viewbox="0 0 20 20" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M15,17.5c0,0.3-0.2,0.5-0.5,0.5h-10C4.2,18,4,17.8,4,17.5v-8C4,9.2,4.2,9,4.5,9h10C14.8,9,15,9.2,15,9.5V17.5z M6,6.5
				C6,4.6,7.6,3,9.5,3S13,4.6,13,6.5V8H6V6.5z M14.5,8H14V6.5C14,4,12,2,9.5,2S5,4,5,6.5V8H4.5C3.7,8,3,8.7,3,9.5v8
				C3,18.3,3.7,19,4.5,19h10c0.8,0,1.5-0.7,1.5-1.5v-8C16,8.7,15.3,8,14.5,8z"></path>
</svg>
</div>
<a class="anasayfa-link" href="/blog/ceza-hukuku/">
<h2 class="anasayfa-header">Ceza Hukuku</h2>
</a>
</div>
Ceza yargılaması süreci, hukuki yardıma en çok ihtiyaç duyulan süreçtir.
Ceza hukuku arama, el koyma, yakalama, gözaltı, tutuklama gibi koruma tedbirleri;
adli para cezası veya hapis cezası mahkumiyeti gibi mahkeme kararları vasıtasıyla kişi özgürlüğüne müdahale eder. Suç işlendiğinde savcılık tarafından soruşturma, ceza davası açıldığında mahkeme tarafından yargılama faaliyeti yapılır.
Tüm bu süreçlere şüpheli, sanık veya şikayetçi olarak katılanlar mutlaka profesyonel hukuki yardım almalıdır.
Yargısal süreçlerde müşteki, şüpheli veya sanığın savunma hakkı bir ceza avukatı tarafından temin edilmediği takdirde hak kaybı kaçınılmazdır. İstanbul, ceza avukatı sayısı en yüksek olan şehirdir. İstanbul ceza hukuku büroları, kentte işlenen suç çeşitliliği nedeniyle oldukça geniş bir yelpazede hukuk uygulaması deneyimine sahiptir. Türkiye'de tüm mesleki uygulamalardaki gelişmelere paralel olarak nitelikli ceza avukatı sayısının da gittikçe arttığı görülmektedir.
Avukat Baran Doğan Hukuk Bürosu, ceza hukuku teorisi ve uygulamasını birlikte ele alarak müvekkillerine etkin bir avukatlık ve hukuki danışmanlık hizmeti sunmaya çalışmaktadır.
</section>
<section>
<div class="anasayfa-section">
<div class="anasayfa-icon">
<svg id="Layer_1" version="1.1" viewbox="0 0 20 20" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M13.5,11.5c-0.1,0-0.3,0-0.4-0.1c-0.2-0.2-0.2-0.5,0-0.7l3.1-3.1C16.7,7.1,17,6.4,17,5.7c0-0.7-0.3-1.4-0.8-1.9
					c-1-1-2.7-1-3.8,0L9.4,6.9C9.2,7,8.8,7,8.6,6.9c-0.2-0.2-0.2-0.5,0-0.7l3.1-3.1c1.4-1.4,3.8-1.4,5.2,0C17.6,3.8,18,4.7,18,5.7
					c0,1-0.4,1.9-1.1,2.6l-3.1,3.1C13.8,11.5,13.6,11.5,13.5,11.5z"></path>
<path d="M4.7,19c-1,0-1.9-0.4-2.6-1.1c-1.4-1.4-1.4-3.8,0-5.2l3.1-3.1c0.2-0.2,0.5-0.2,0.7,0c0.2,0.2,0.2,0.5,0,0.7l-3.1,3.1
					c-1,1-1,2.7,0,3.8C3.3,17.7,4,18,4.7,18s1.4-0.3,1.9-0.8l3.1-3.1c0.2-0.2,0.5-0.2,0.7,0c0.2,0.2,0.2,0.5,0,0.7l-3.1,3.1
					C6.6,18.6,5.7,19,4.7,19z"></path>
<path d="M5.5,7C5.4,7,5.2,7,5.1,6.9l-2-2C3,4.7,3,4.3,3.1,4.1C3.3,4,3.7,4,3.9,4.1l2,2C6,6.3,6,6.7,5.9,6.9C5.8,7,5.6,7,5.5,7z"></path>
<path d="M7.5,6C7.2,6,7,5.8,7,5.5v-3C7,2.2,7.2,2,7.5,2S8,2.2,8,2.5v3C8,5.8,7.8,6,7.5,6z"></path>
<path d="M4.5,9h-3C1.2,9,1,8.8,1,8.5S1.2,8,1.5,8h2C4.8,8,5,8.2,5,8.5S4.8,9,4.5,9z"></path>
<path d="M15.5,17c-0.1,0-0.3,0-0.4-0.1l-2-2c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0l2,2c0.2,0.2,0.2,0.5,0,0.7
					C15.8,17,15.6,17,15.5,17z"></path>
<path d="M17.5,13h-3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h2c0.3,0,0.5,0.2,0.5,0.5S17.8,13,17.5,13z"></path>
<path d="M11.5,19c-0.3,0-0.5-0.2-0.5-0.5v-3c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5v3C12,18.8,11.8,19,11.5,19z"></path>
</svg>
</div>
<a class="anasayfa-link" href="/blog/tazminat-hukuku/">
<h2 class="anasayfa-header">Tazminat Hukuku</h2>
</a><a></a>
</div>
Tazminat hukuku, özel hukuk ilişkilerinde birbirinden farklı sebeplerle ortaya çıkan tazminat uyuşmazlıklarını konu alır.
Maddi ve manevi tazminat davası; trafik kazası, iş kazası, doktor uygulama hatası (malpraktis), boşanma, sözleşme ihlali, suç işlenmesi vb. gibi birçok farklı hukuki nedenden kaynaklanabilir. Tazminat davaları; kusur oranının, zarar miktarının ve sorumluluğun tespiti açısından titizlikle takip edilmesi gereken ayrı ayrı önemli aşamalardan oluşur. Tazminat talebinin yerinde olup olmadığının belirlenmesi için bilirkişi incelemesi, tanık dinlenmesi, belge araştırma işlemlerinin yapılması gerekir. Tazminat davası sürecinin iyi yönetilmesi dava sonucunda elde edilecek tazminat miktarının lehe belirlenmesini sağlayacaktır. Maddi tazminat davası, ölüm varsa ölenin yaşı, en son aldığı ücret, tazminat konusu olaydaki kusur oranı; yaralanma varsa ek olarak maluliyet oranı dikkate alınarak hesaplanır. Manevi tazminat davası ise kusur oranı, tarafların mali durumları, yaşı, mesleği, olayın vehameti vb. gibi kriterler göz önünde bulundurularak hesaplanır. Tazminat davalarına giren avukatlar için halk arasında tazminat avukatı gibi sıfatlar kullanılmaktaysa da Türkiye'de henüz böyle bir avukatlık uzmanlık alanı tanımlanmış değildir. Avukatlık büromuz, tazminat davaları için tazminat sebebini ve somut olaya özgü mevzuatı birlikte analiz ederek hem hukuki danışmanlık hem de avukatlık hizmeti sunmaktadır.
</section>
<section>
<div class="anasayfa-section">
<div class="anasayfa-icon">
<svg id="Layer_1" version="1.1" viewbox="0 0 20 20" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M16,18.5c0,0.3-0.2,0.5-0.5,0.5H13v-4.5c0-0.8-0.7-1.5-1.5-1.5h-3C7.7,13,7,13.7,7,14.5V19H4.5C4.2,19,4,18.8,4,18.5V9.4
			l5.7-6.3C9.8,3,9.9,2.9,10,2.9s0.2,0,0.3,0.1L16,9.4V18.5z M12,19H8v-4.5C8,14.2,8.2,14,8.5,14h2c0.3,0,0.5,0.2,0.5,0.5V19z
			 M19.9,12.2L11,2.4c-0.3-0.3-0.6-0.5-1-0.5l0,0c-0.4,0-0.8,0.2-1,0.5l-8.8,9.8c-0.2,0.2-0.2,0.5,0,0.7C0.3,13,0.4,13,0.5,13
			c0.1,0,0.3-0.1,0.4-0.2L3,10.5v8C3,19.3,3.7,20,4.5,20h11c0.8,0,1.5-0.7,1.5-1.5v-8l2.1,2.4c0.2,0.2,0.5,0.2,0.7,0
			C20,12.7,20.1,12.4,19.9,12.2z"></path>
</svg>
</div>
<a class="anasayfa-link" href="/blog/gayrimenkul-hukuku/">
<h2 class="anasayfa-header">Gayrimenkul Hukuku</h2>
</a>
</div>
Tapu iptal ve tescil davaları; önalım, ortaklığın giderilmesi, taşınmaz vaadi sözleşmelerinin feshi vb. gibi taşınmaza ilişkin davalar mülkiyet hakkını etkileyen, ekonomik açıdan dava değeri yüksek davalardır.
Dava hazırlık süreci iyi yürütülmediği takdirde, gayrimenkul davaları, davacı ve davalının oldukça uzun bir yargılama süreciyle baş başa kalmasına neden olmaktadır.
Asliye veya sulh hukuk mahkemesinde gayrimenkul davası açılmadan önce dava süreci çok iyi planlanmalıdır.
Davadan önce tapu kaydı ve tapu kütüğü dosyasının iyi bir şekilde incelenmesi, emsal yargıtay kararları araştırmasının yapılması, keşif, tanık dinleme ve bilirkişi incelemesi gibi işlemlerin sonuçlarının önceden planlanması gerekir.
Hukuk büromuz, gayrimenkul davalarını dava hazırlık süreciyle birlikte planlayarak mülkiyet hakkının temin edilmesini sağlamaktadır.
</section>
<section>
<div class="anasayfa-section">
<div class="anasayfa-icon">
<svg id="Layer_1" version="1.1" viewbox="0 0 20 20" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M7,16.5C7,16.8,7.2,17,7.5,17h11c0.3,0,0.5-0.2,0.5-0.5c0,0,0-0.9-0.7-1.7c-1-1.2-2.8-1.8-5.3-1.8s-4.3,0.6-5.3,1.8
				C7,15.6,7,16.5,7,16.5z M18.5,18h-11C6.7,18,6,17.3,6,16.5c0,0,0-1.2,0.9-2.3c0.5-0.6,1.2-1.2,2.1-1.5c1.1-0.4,2.4-0.7,3.9-0.7
				s2.9,0.2,3.9,0.7c0.9,0.4,1.6,0.9,2.1,1.5c0.9,1.1,0.9,2.3,0.9,2.3C20,17.3,19.3,18,18.5,18z"></path>
<path d="M13,4c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S14.7,4,13,4z M13,11c-2.2,0-4-1.8-4-4s1.8-4,4-4s4,1.8,4,4S15.2,11,13,11z"></path>
<path d="M4.5,18h-3C0.7,18,0,17.3,0,16.5c0,0,0-0.9,0.7-1.8c0.4-0.5,0.9-0.9,1.5-1.2C3,13.2,3.9,13,5,13c0.2,0,0.4,0,0.5,0
				C5.8,13,6,13.3,6,13.5C6,13.8,5.8,14,5.5,14c-0.2,0-0.3,0-0.5,0c-3.9,0-4,2.4-4,2.5C1,16.8,1.2,17,1.5,17h2C4.8,17,5,17.2,5,17.5
				S4.8,18,4.5,18z"></path>
<path d="M5,7C3.9,7,3,7.9,3,9s0.9,2,2,2s2-0.9,2-2S6.1,7,5,7z M5,12c-1.7,0-3-1.3-3-3s1.3-3,3-3s3,1.3,3,3S6.7,12,5,12z"></path>
</svg>
</div>
<a class="anasayfa-link" href="/blog/medeni-hukuk/">
<h2 class="anasayfa-header">Medeni Hukuk</h2>
</a>
</div>
Medeni hukuk; boşanma davası, velayet, nafaka, boşanmada mal paylaşımı gibi aile hukuku sorunlarını; mirasta mal paylaşımı,
muvazaa (mirastan mal kaçırma), mirasçının saklı payının ihlal edilmesi nedeniyle tenkis davası, mirasın reddi gibi miras hukuku davalarını; sözleşmenin düzenlenmesi, yenilenmesi ve feshi, alacak davası vb. gibi borçlar hukuku davalarını kapsayan oldukça geniş bir özel hukuk alanıdır. Özel hukuk alanında çalışan avukatlar için miras avukatı, boşanma avukatı, aile avukatı gibi kavramlar halk arasında kullanılmakta ise de, mevzuatta henüz böyle bir tanımlama mevcut değildir. Özellikle, genel veya özel boşanma sebepleriyle açılan çekişmeli boşanma davaları, boşanma avukatlarının hukuki yardımına en çok ihtiyaç duyulan medeni hukuk davalarıdır. Çekişmeli boşanma davası, hem boşanma kararı verilmesinin hem de tazminat ve nafaka gibi mali taleplerin tarafların kusur durumuna göre belirlendiği ve bu nedenle boşanma avukatı ile temsil edilmenin hakların temini açısından etkili olduğu bir dava türüdür.
İstanbul, boşanma avukatı olarak faaliyet gösteren büroların oldukça yoğun olduğu bir kenttir. Özel hukukun bu alanı, İstanbul'da faaliyet gösteren tüm hukuk büroları gibi büromuzun da hem hukuki danışmanlık hem de avukatlık hizmeti verdiği temel faaliyet alanıdır.
</section>
<section>
<div class="anasayfa-section">
<div class="anasayfa-icon">
<svg id="Layer_1" version="1.1" viewbox="0 0 20 20" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M7,16.5C7,16.8,7.2,17,7.5,17h11c0.3,0,0.5-0.2,0.5-0.5c0,0,0-0.9-0.7-1.7c-1-1.2-2.8-1.8-5.3-1.8s-4.3,0.6-5.3,1.8
				C7,15.6,7,16.5,7,16.5z M18.5,18h-11C6.7,18,6,17.3,6,16.5c0,0,0-1.2,0.9-2.3c0.5-0.6,1.2-1.2,2.1-1.5c1.1-0.4,2.4-0.7,3.9-0.7
				s2.9,0.2,3.9,0.7c0.9,0.4,1.6,0.9,2.1,1.5c0.9,1.1,0.9,2.3,0.9,2.3C20,17.3,19.3,18,18.5,18z"></path>
<path d="M13,4c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S14.7,4,13,4z M13,11c-2.2,0-4-1.8-4-4s1.8-4,4-4s4,1.8,4,4S15.2,11,13,11z"></path>
<path d="M4.5,18h-3C0.7,18,0,17.3,0,16.5c0,0,0-0.9,0.7-1.8c0.4-0.5,0.9-0.9,1.5-1.2C3,13.2,3.9,13,5,13c0.2,0,0.4,0,0.5,0
				C5.8,13,6,13.3,6,13.5C6,13.8,5.8,14,5.5,14c-0.2,0-0.3,0-0.5,0c-3.9,0-4,2.4-4,2.5C1,16.8,1.2,17,1.5,17h2C4.8,17,5,17.2,5,17.5
				S4.8,18,4.5,18z"></path>
<path d="M5,7C3.9,7,3,7.9,3,9s0.9,2,2,2s2-0.9,2-2S6.1,7,5,7z M5,12c-1.7,0-3-1.3-3-3s1.3-3,3-3s3,1.3,3,3S6.7,12,5,12z"></path>
</svg>
</div>
<h2 class="anasayfa-header">Hukuki Danışmanlık</h2>
</div>
Hukuki danışmanlık, hukuki sorunların çözümünde önemli bir işlev görmektedir. Avukatlar, gerçek ve tüzel kişilere ait tüm ihtilaf, dava ve işlemlerle ilgili hukuki danışmanlık hizmeti verme yetkisini haizdir. Özellikle İstanbul avukatları, hukuki danışma ve vekaleten yapılan işler olmak üzere iki farklı biçimde mesleki faaliyet icra etmektedir. Günümüzde en iyi avukat, müvekkiline ait sorunlar mahkemeye intikal etmeden önce çözüm üreten avukat olarak kabul edilmektedir. Avukat Baran Doğan Hukuk Bürosu, hukukun tüm alanlarına ilişkin önleyici bir hukuk hizmeti olarak hukuki danışmanlık vermektedir.
</section>
<section>
<div class="ozet">
İstanbul'da faaliyet gösteren Avukat Baran Doğan Hukuk Bürosu, bir taraftan makale ve yargıtay kararları yayımlayarak hukuk bilgisine erişimi kolaylaştırırken,
diğer taraftan ceza hukuku, tazminat hukuku, gayrimenkul hukuku, miras hukuku, medeni hukuk, vergi ve idare hukuku davalarında avukatlık hizmeti sunmaktadır.
</div>
</section>
</main>
<div class="footer-container-wrapper">
<div class="footer-container">
<footer>
<section>
<div class="contact-info">
<div class="adres">
<div class="adres-icon">
<svg height="24" viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
<path d="M0 0h24v24H0z" fill="none"></path>
</svg>
</div>
<a aria-label="Avukat baran dogan hukuk burosu adress" href="https://goo.gl/5HjY5M">
<div class="adres-text">
M.Fevzi Çakmak cd. 1.Sok No:9 Daire:6<br/>
Şirinevler Meydanı/Şirinevler<br/>
Bahçelievler-İstanbul
</div>
</a>
</div>
<div class="telefon-wrapper">
<a aria-label="Avukat baran dogan hukuk burosu telefon" class="telefon" href="tel:+902126521544">
<div class="telefon-container">
<span>
<svg height="16px" version="1.1" viewbox="0 0 20 20" width="16px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px">
<path d="M4,1C3.7,1,3,1.3,2.2,2.1C1.5,2.7,1,3.5,1,4c0,6.7,8.3,15,15,15c0.5,0,1.3-0.5,1.9-1.2C18.7,17,19,16.3,19,16
				c0-0.2-0.6-0.9-2-2c-1.2-1-2.2-1.5-2.5-1.5c0,0-0.1,0-0.4,0.3c-0.2,0.3-0.4,0.6-0.6,0.9c-0.6,0.9-1.1,1.8-1.9,1.8
				c-0.1,0-0.2,0-0.4-0.1c-2.6-1.1-5.7-4.2-6.8-6.8C4.4,8.4,4.4,7.9,5,7.3C5.3,7,5.8,6.7,6.2,6.5C6.6,6.3,6.9,6,7.2,5.9
				c0.3-0.2,0.3-0.3,0.3-0.4C7.5,5.2,7,4.2,6,3C4.9,1.6,4.2,1,4,1z M16,20c-1.8,0-3.7-0.5-5.6-1.5c-1.8-0.9-3.6-2.2-5.1-3.8
				c-1.6-1.6-2.9-3.3-3.8-5.1C0.5,7.7,0,5.8,0,4c0-1.1,1.1-2.3,1.5-2.7C2.2,0.7,3.2,0,4,0c0.4,0,0.8,0.2,1.4,0.8
				c0.4,0.4,0.9,0.9,1.4,1.5c0.3,0.4,1.7,2.3,1.7,3.2c0,0.7-0.8,1.3-1.7,1.8C6.4,7.5,6.1,7.7,5.8,8C5.5,8.2,5.5,8.3,5.5,8.3
				c0.9,2.4,3.9,5.3,6.2,6.2c0,0,0.1-0.1,0.4-0.3c0.2-0.3,0.4-0.6,0.6-1c0.5-0.9,1.1-1.7,1.8-1.7c0.9,0,2.8,1.4,3.2,1.7
				c0.6,0.5,1.2,1,1.5,1.4c0.5,0.6,0.8,1,0.8,1.4c0,0.8-0.7,1.8-1.3,2.5C18.3,18.9,17.1,20,16,20z"></path>
</svg>
</span>
<span class="tel-numarasi">0 212 652 15 44 </span>
</div>
</a>
</div>
</div>
</section>
<div class="copyright">
©2016 Avukat Baran Doğan Hukuk Bürosu </div>
</footer>
</div>
</div>
<amp-analytics id="analytics1" type="googleanalytics">
<script type="application/json">
  {
    "vars": {
    "account": "UA-58342443-1"
    },
    "triggers": {
    "trackPageview": {
      "on": "visible",
      "request": "pageview"
    }
    }
  }
  </script>
</amp-analytics>
</body>
</html>
