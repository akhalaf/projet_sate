<!DOCTYPE html>
<html class="error-page" dir="ltr" lang="en-gb">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<title>Error: 404 Page not found</title>
<link href="/images/health-favicon-new.png" rel="icon"/>
<link href="/templates/j-network/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/templates/j-network/css/font-awesome.min.css" rel="stylesheet"/>
<link href="/templates/j-network/css/template.css" rel="stylesheet"/>
<link href="/templates/j-network/css/presets/preset3.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
<h1 class="error-code">404</h1>
<h2 class="error-message">Page not found</h2>
<a class="btn btn-secondary" href="/index.php"><span aria-hidden="true" class="fa fa-home"></span> Home Page</a>
</div>
</body>
</html>
