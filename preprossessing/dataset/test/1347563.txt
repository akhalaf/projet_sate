<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<title>SONA JP || 8g74crec</title>
<!-- Bootstrap core CSS -->
<link href="https://www.sonajp.com/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://www.sonajp.com/assets/bootstrap/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="https://www.sonajp.com/assets/css/style.css" rel="stylesheet"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800" rel="stylesheet" type="text/css"/>
<!-- icon styles for this template -->
<link crossorigin="anonymous" href="http://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" rel="stylesheet"/>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link href="https://www.sonajp.com/assets/img/logo.png" rel="icon" type="image/x-icon"/>
<!--<link rel="shortcut icon" type="image/x-icon" href="https://www.sonajp.com/assets/img/favicon.png">-->
<link href="https://www.sonajp.com/assets/img/logo.png" rel="shortcut icon" type="image/x-icon"/>
<!--<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://www.sonajp.com/assets/js/jquery.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="http://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://www.sonajp.com/assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<!--<nav class="navbar navbar-default fixed-top">-->
<nav class="navbar navbar-default">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<a class="navbar-brand" href="https://www.sonajp.com/"><img alt="Logo" class="logo" src="https://www.sonajp.com/assets/img/logo.png"/></a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="button navbar-right">
<a class="btn trans-btn" href="https://www.sonajp.com/access-map/"><i class="fas fa-map-marked-alt map-icon"></i> Access Map</a>
<button class="btn trans-btn2" type="button"><span class="text-center enq-caption">045-592-8390 <br/>
                           inquiry@sonajp.com</span></button>
</div>
</div><!-- /.container-fluid -->
</nav>
<!--page-header start -->
<!--<div class="page-head"> 
		<div class="container">
			<div class="col-12 text-center">
				<div class="page-head-content">
					<!--<h1 class="page-title">LIVE JAPANESE AUCTIONS</h1> 
						<h4>Construction Machinery and Used Car</h4>	
						<h1 class="page-title">Construction Machinery</h1> 	
				</div>
				
			</div>
		</div>
	</div>-->
<img src="https://www.sonajp.com/assets/img/banner-1.jpg" style="width:100%"/>
<div class="navigation">
<div class="container">
<ul class="nav justify-content-center">
<li class="nav-item">
<a class="nav-link active" href="https://www.sonajp.com/stock-list/">STOCK LIST</a>
</li>
<!--<li class="nav-item">
				<a class="nav-link" href="https://www.sonajp.com/about-us/">ABOUT US</a>
			  </li>-->
<li class="nav-item">
<a class="nav-link" href="https://www.sonajp.com/corporate-profile/">CORPORATE PROFILE</a>
</li>
<li class="nav-item">
<a class="nav-link" href="https://www.sonajp.com/contact-us/">CONTACT</a>
</li>
</ul>
</div>
</div>
<!--page-header end -->
<!--content-section start -->
<div class="content-area" style="padding-bottom: 60px; background-color: rgb(252, 252, 252);">
<div class="container-fluid">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 mr50">
<div class="container">
<h3 class="hdng-1">PAGE NOT FOUND</h3>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="https://www.sonajp.com/stock-list/">Home</a></li>
<li aria-current="page" class="breadcrumb-item active">PAGE NOT FOUND</li>
</ol>
</nav>
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-body">
<img alt="404" class="img-responsive" src="https://www.sonajp.com/assets/img/page-not-found.gif"/>
<center><a class="btn btn-danger btn-lg" href="https://www.sonajp.com/">Home</a></center>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--content-section end -->
<!-- Footer area-->
<footer>
<div class="top-footer">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
<img alt="SonaJP Logo" class="img-responsive footer-logo" src="https://www.sonajp.com/assets/img/logo.png" title="Sona Jp Logo"/>
<ul class="ftr-social-links">
<li>
<a class="" href="#" style="visibility: visible; animation-name: fadeInUp;"><i class="fab fa-twitter-square"></i></a>
</li>
<li>
<a class="" data-wow-delay="0.2s" href="#" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><i class="fab fa-facebook-square"></i></a>
</li>
<li>
<a class="" data-wow-delay="0.3s" href="#" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><i class="fab fa-linkedin"></i></a>
</li>
</ul>
</div>
<div class="col-md-2 col-sm-6 col-xs-12 white-text">
<nav class="nav flex-column">
<h3>Pages</h3>
<a class=" nav-link2" href="https://www.sonajp.com/corporate-profile/">CORPORATE PROFILE</a>
<a class=" nav-link2" href="https://www.sonajp.com/about-us/">ABOUT US</a>
<a class="nav-link2" href="https://www.sonajp.com/contact-us/">CONTACT US</a>
</nav>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 white-text">
<h3>Address</h3>
<address> Yokohama branch,224-0023,3-23-2-604, Higashi Yamada, Tsuzuki-ku, Yokohama City, Kanagawa Prefecture, Japan<br/>
					T: 045-592-8390 <br/>
					F: 045-592-8391<br/>
					M: +81-90-7201-0625<br/>
					E: inquiry@sonajp.com</address>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 white-text">
<h5 style="padding-top:10px;"><i>Partner with Yokohama chamber of commerce</i></h5>
<img alt="SonaJP partner" class="img-responsive" src="https://www.sonajp.com/assets/img/sona-partner.png" style="height:13rem;" title="partner"/>
</div>
</div>
</div>
</div>
<div class="bottom-footer">
<p class="m-0 text-center text-white">© Copyright 2018 Sona International Co.Ltd ., All Rights Reserved.<br/>Kanagawa Prefecture Public Safety commission license : <span>452750002529</span></p>
</div>
<!-- /.container -->
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript">
		$(document).ready(function(){
			$(".layout-grid-btn").click(function(){
				$(".gridviewdiv").removeClass("hidediv");
				$(".listviewdiv").addClass("hidediv");
				$(this).addClass("active");
				$(".layout-list-btn").removeClass("active");
				
			});
			$(".layout-list-btn").click(function(){
				$(".gridviewdiv").addClass("hidediv");
				$(".listviewdiv").removeClass("hidediv");
				$(this).addClass("active");
				$(".layout-grid-btn").removeClass("active");
			});
		});
	</script>
</body>
</html> 