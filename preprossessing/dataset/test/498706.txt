<!DOCTYPE html>
<html>
<head>
<link href="/resources/favicons/favicon.ico" rel="shortcut icon"/>
<link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" rel="stylesheet"/>
<link href="./scss/app.css" rel="stylesheet"/>
<link href="https://i.icomoon.io/public/temp/eb4641cd2a/enriqueza/style.css" rel="stylesheet"/>
<link href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" rel="stylesheet"/>
<!-- Default theme -->
<link href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" rel="stylesheet"/>
<!-- Semantic UI theme -->
<link href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" rel="stylesheet"/>
<!-- Bootstrap theme -->
<link href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" rel="stylesheet"/>
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"/>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="Proteje tu salud" name="description"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
</head>
<body>
<header id="home">
<nav class="navbar navbar-expand-lg navbar-dark main-navbar">
<a class="navbar-brand" href="#">
<div class="d-flex logo">
<img src="./img/logo.svg"/>
</div>
</a>
<button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav ml-auto">
<li class="nav-item separator active">
<a class="nav-link" href="#home">INICIO <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item separator">
<a class="nav-link" href="#about-us">NOSOTROS</a>
</li>
<li class="nav-item separator">
<a class="nav-link" href="#plans">SERVICIOS</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#reach-us">CONTACTO</a>
</li>
<li class="nav-item active">
<a class="nav-link" href="https://www.facebook.com/ENRIQUEZA.RESPONSABILIDAD.EMPRESARIAL/?ref=br_rs" target="_blank"><ion-icon name="logo-facebook"></ion-icon></a>
</li>
</ul>
</div>
</nav>
<div class="banner">
<div class="image-wrapper">
</div>
<div class="slogan-wrapper">
<div>
<h1 data-aos="fade-up" data-aos-delay="300">TRABAJAMOS</h1>
<h4 data-aos="fade-up" data-aos-delay="500">PARA PROTEGER</h4>
<p data-aos="fade-up" data-aos-delay="600">Lo que mÃ¡s amas: tu familia y tus empresas.</p>
<hr/>
<p data-aos="fade-up" data-aos-delay="700">
                            Ofrecemos una asesorÃ­a responsable,<br/>
                            segura y de primer nivel.
                        </p>
<a data-aos="fade-up" data-aos-delay="800" href="#icons-line"><button class="outline-button">CONOCE MÃS</button></a>
</div>
</div>
</div>
</header>
<div class="icons-line" data-aos="fade-up" data-aos-delay="500" id="icons-line">
<div class="icons">
<span class="icon-heritage"></span>
<span class="icon-savings"></span>
<span class="icon-shield"></span>
</div>
<div class="anouncement">
<span class="icon-Salud-Abajo"></span>
<p>
                    Seguro de Gastos MÃ©dicos<br/>
                    Seguro de Vida<br/>
                    Seguro de invalidez
                </p>
<div>
<a href="#plans"><button class="outline-button">CONOCE MÃS</button></a>
</div>
</div>
</div>
<div class="contact-us" id="about-us">
<div class="action">
<img data-aos="fade-up" data-aos-delay="500" src="./img/abou-us.jpg"/>
<a data-aos="fade-up" data-aos-delay="600" href="#reach-us"><button>CONTÃCTANOS</button></a>
</div>
<div class="info">
<h2 data-aos="fade-up" data-aos-delay="700">NOSOTROS</h2>
<p data-aos="fade-up" data-aos-delay="800">
                    Somos un equipo de profesionales con experiencia y calificados en materia de planeaciÃ³n de riqueza y de riesgos.
                </p>
<p data-aos="fade-up" data-aos-delay="900">
                    Disminuye riesgos para ti, tu familia, patrimonio o empresa ante cualquier eventualidad. Te queremos seguro a ti, a tu familia y a tu patrimonio. En enriqueza nos preocupamos por tu bienestar y el de los tuyos.
                </p>
</div>
</div>
<div class="together-wrapper">
<h2 data-aos="fade-up" data-aos-delay="500">
                JUNTOS <br/>
                PODEMOS
            </h2>
</div>
<div class="plans" id="plans">
<label class="patrimonio item" data-aos="fade-up" data-aos-delay="500">
<input type="checkbox"/>
<div class="service">
<span class="icon-heritage"></span>
<p>
                        Blindar <br/>
                        tu Patrimonio
                    </p>
<div class="text">
<span class="icon-heritage"></span>
<p>Seguros de Vida</p>
<p>Seguros de Invalidez Total</p>
<p>Seguros de Auto</p>
<p>Seguros de Casa</p>
<p>Responsabilidad Civil</p>
</div>
</div>
</label>
<label class="salud item" data-aos="fade-down" data-aos-delay="700">
<input type="checkbox"/>
<div class="service">
<span class="icon-Salud-Abajo"></span>
<p>
                        Poteger <br/>
                        tu salud
                    </p>
<div class="text">
<span class="icon-Salud-Abajo"></span>
<p>Seguro de Gastos MÃ©dicos Mayores</p>
<p>Accidentes Personales</p>
<p>Seguro Viajero</p>
</div>
</div>
</label>
<label class="ahorro item" data-aos="fade-up" data-aos-delay="900">
<input type="checkbox"/>
<div class="service">
<span class="icon-savings"></span>
<p>
                        Ahorrar y<br/>
                        alcanzar tus metas
                    </p>
<div class="text">
<span class="icon-savings"></span>
<p>EducaciÃ³n</p>
<p>Retiro</p>
<p>Mujer</p>
<p>InversiÃ³n</p>
</div>
</div>
</label>
<label class="empresa item" data-aos="fade-down" data-aos-delay="1000">
<input type="checkbox"/>
<div class="service">
<span class="icon-shield"></span>
<p>
                        Blindar <br/>
                        tu Empresa
                    </p>
<div class="text">
<span class="icon-shield"></span>
<p>Seguros de Vida Grupo</p>
<p>Gastos MÃ©dicos Mayores Colectivo</p>
<p>Accidentes Personales</p>
<p>Lealtad de Colaboradores</p>
</div>
</div>
</label>
</div>
<div class="different">
<div class="header">
<div data-aos="fade-right" data-aos-delay="500"></div>
<div data-aos="fade-right" data-aos-delay="600">
<div></div>
<div></div>
<h2 data-aos="fade-down" data-aos-delay="500">Â¿QUÃ NOS HACE <br/>DIFERENTES?</h2>
</div>
<div data-aos="fade-right" data-aos-delay="600"></div>
</div>
<div class="content">
<div data-aos="fade-right" data-aos-delay="600">
<img data-aos="fade-down" data-aos-delay="500" src="./img/mdrt.svg"/>
</div>
<div data-aos="fade-right" data-aos-delay="700">
<p>1. Te escuchamos para detectar tus necesidades.</p>
<p>2. DiseÃ±amos la mejor estrategia.</p>
<p>3. Te acompaÃ±amos en el proceso.</p>
</div>
<div data-aos="fade-right" data-aos-delay="800"></div>
</div>
<div class="action">
<div></div>
<div>
<div></div>
<div data-aos="fade-right" data-aos-delay="800">
<p>
                            CONTÃCTANOS<br/>
                            Y DISEÃAREMOS<br/>
                            UN PLAN A TU<br/>
                            MEDIDA
                        </p>
</div>
<div data-aos="fade-right" data-aos-delay="900">
<a href="#">
<ion-icon name="chevron-forward-outline"></ion-icon>
</a>
</div>
</div>
<div></div>
</div>
</div>
<div class="reach-us" id="reach-us">
<div data-aos="fade-down" data-aos-delay="500">
<h3>CONTÃCTANOS</h3>
<form method="post" name="contact">
<input name="name" placeholder="Nombre" type="text"/>
<input name="email" placeholder="Email" type="text"/>
                    Mensaje
                    <textarea name="message">
                    </textarea>
<input type="submit" value="ENVIAR"/>
</form>
</div>
<div data-aos="fade-down" data-aos-delay="600">
<div>
<div>
<div><ion-icon name="logo-whatsapp"></ion-icon>+52 1 (33) 1600 7428 | (33) 3368 8561</div>
<div><ion-icon name="mail-open"></ion-icon>gil@enriqueza.com &amp; silvia@enriqueza.com</div>
<div><ion-icon name="map"></ion-icon> AmÃ©ricas #1500, Country Club, Guadalajara.</div>
</div>
</div>
<div>
<img src="./img/logo-min.svg"/>
</div>
</div>
</div>
<script crossorigin="anonymous" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
            AOS.init();
        </script>
<script crossorigin="anonymous" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script src="./js/jquery.waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
<script src="./js/scripts.js"></script>
</body>
<footer></footer>
</html>