<!DOCTYPE html>
<!--[if IE 7]>

<html class="ie ie7" lang="en-US">

<![endif]--><!--[if IE 8]>

<html class="ie ie8" lang="en-US">

<![endif]--><!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en-US">
<!--<![endif]-->
<head>
<!-- Metas
	================================================== -->
<meta charset="utf-8"/>
<!-- Page Title 
	================================================== -->
<title>Page not found | Badger and Gunner</title>
<link href="http://gmpg.org/xfn/11" rel="profile"/>
<link href="https://badgerandgunner.com/xmlrpc.php" rel="pingback"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- For SEO -->
<!-- End SEO-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	  <script src="https://badgerandgunner.com/wp-content/themes/magnis/assets/js/html5shiv.js"></script>
	  <script src="https://badgerandgunner.com/wp-content/themes/magnis/assets/js/respond.min.js"></script>
	<![endif]-->
<!-- Favicons -->
<link href="https://badgerandgunner.com/wp-content/uploads/2015/06/bgfav.jpg" rel="icon" type="image/x-icon"/>
<meta content="noindex,nofollow" name="robots"/>
<link href="//maps.googleapis.com" rel="dns-prefetch"/>
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link href="https://badgerandgunner.com/feed/" rel="alternate" title="Badger and Gunner » Feed" type="application/rss+xml"/>
<link href="https://badgerandgunner.com/comments/feed/" rel="alternate" title="Badger and Gunner » Comments Feed" type="application/rss+xml"/>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/badgerandgunner.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://badgerandgunner.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3" id="wp-block-library-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2" id="contact-form-7-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu%3A400%2C400italic%2C700%2C700italic%2C300%2C300italic&amp;ver=5.5.3" id="fonts-Nothing-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Euphoria+Script&amp;ver=5.5.3" id="fonts-Yanone-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/bootstrap.css?ver=3.2" id="bootstrap-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/tango/skin.css?ver=3.2" id="tango-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/quick_newsletter.css?ver=3.2" id="newsletter-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/font-awesome/css/font-awesome.min.css?ver=3.2" id="awesome-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/colorbox.css?ver=3.2" id="colorbox-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/flexslider.css?ver=3.2" id="flexslider-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/contact.css?ver=3.2" id="contact-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/style.css?ver=2014-12-05" id="style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/responsive.css?ver=3.2" id="settings-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/css/layerslider/layerslider.css?ver=3.2" id="layerslider-css-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/themes/magnis/framework/color.php?ver=5.5.3" id="color-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://badgerandgunner.com/wp-content/plugins/newsletter/style.css?ver=7.0.2" id="newsletter-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://badgerandgunner.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp" type="text/javascript"></script>
<script id="modernizr-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/modernizr.js?ver=5.5.3" type="text/javascript"></script>
<script id="easing-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery-easing-1.3.js?ver=5.5.3" type="text/javascript"></script>
<script id="gmap-js" src="https://maps.googleapis.com/maps/api/js?ver=5.5.3" type="text/javascript"></script>
<script id="twitter-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/twitterFetcher_v10_min.js?ver=5.5.3" type="text/javascript"></script>
<link href="https://badgerandgunner.com/wp-json/" rel="https://api.w.org/"/><link href="https://badgerandgunner.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://badgerandgunner.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.5.3" name="generator"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-168226649-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-168226649-1');
</script>
<style type="text/css">
                #header{
margin: 0 auto;
}
            </style><style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style class="options-output" type="text/css">body{font-family:Georgia, serif;font-weight:normal;font-style:normal;}</style>
</head>
<body class="error404">
<!-- header start //-->
<div class="container magnis-header">
<div class="row">
<div class="col-md-12">
<!-- site logo	//-->
<a href="https://badgerandgunner.com">
<div class="site-logo site-logo-3 biglogo"><img alt="" src="https://badgerandgunner.com/wp-content/uploads/2015/06/bglogo1.jpg"/> </div> </a> <!-- header contacts //--> <div class="header-contacts header-contacts-3 hidden-phone visible-tablet visible-desktop"> <p><i class="fa fa-phone"></i><strong>716-652-6350</strong></p> <p><i class="fa fa-envelope"></i><a href="mailto:info@badgerandgunner.com">info@badgerandgunner.com</a></p> </div> <div class="header-sub-wrapper header-sub-wrapper-3"> <!-- header social buttons //--> <div class="header-social-buttons header-social-buttons-3"> </div>
<!-- header search //-->
<div class="header-search"> <?php /*
<form role="search" method="get" id="searchform" action="https://badgerandgunner.com/">
<i class="fa fa-search"></i>
<input id="s" name="s" placeholder="type and search..." type="text" value=""/>
<input class="search_btn" type="submit" value="Search"/>
<input id="post_type" name="post_type" type="hidden" value="post"/>						
                	                */?&gt;	<img border="0" src="https://badgerandgunner.com/wp-content/uploads/2015/06/building_pic11.jpg"/>
</div>
</div>
</div>
</div></div>
<div class="site-menu-3-wrapper">
<div class="container">
<div class="row">
<div class="col-md-12">
<!-- site desktop menu start //-->
<nav class="site-desktop-menu">
<ul><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-50" id="menu-item-50"><a href="https://badgerandgunner.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38" id="menu-item-38"><a href="https://badgerandgunner.com/our-mission/">Our Mission</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37" id="menu-item-37"><a href="https://badgerandgunner.com/personal-insurance/">Personal Insurance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36" id="menu-item-36"><a href="https://badgerandgunner.com/business-insurance/">Business Insurance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35" id="menu-item-35"><a href="https://badgerandgunner.com/not-for-profit-public-entity/">Not-For-Profit / Public Entity</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34" id="menu-item-34"><a href="https://badgerandgunner.com/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33" id="menu-item-33"><a href="https://badgerandgunner.com/trusted-business-partners/">Trusted Business Partners</a></li>
</ul>
</nav>
<!-- site desktop menu end //-->
<!-- site desktop menu start //-->
<nav class="site-mobile-menu">
<i class="fa fa-bars"></i>
<ul><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-50"><a href="https://badgerandgunner.com/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a href="https://badgerandgunner.com/our-mission/">Our Mission</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://badgerandgunner.com/personal-insurance/">Personal Insurance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="https://badgerandgunner.com/business-insurance/">Business Insurance</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="https://badgerandgunner.com/not-for-profit-public-entity/">Not-For-Profit / Public Entity</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://badgerandgunner.com/contact-us/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://badgerandgunner.com/trusted-business-partners/">Trusted Business Partners</a></li>
</ul>
</nav>
<!-- site desktop menu end //-->
</div>
</div>
</div></div><div class="gray-line gray-line-3"></div><!-- header end //-->
<div class="main-content homepage page404" style="">
<div class="container">
<div class="row">
<div class="span12">
<div class="main-content-block magnis-404">
<span>404</span>
                  We are sorry, but the page you are looking for doesn't exist...                  <form action="https://badgerandgunner.com/" class="sidebar-search search" method="get" role="search">
<i class="fa fa-search"></i>
<input class="form-control" name="s" placeholder="type to search..." type="text" value=""/>
<input class="search_btn" type="submit" value="Search"/>
<input id="post_type" name="post_type" type="hidden" value="post"/>
</form> <p><a href="https://badgerandgunner.com">Back To Home</a></p>
</div>
</div>
</div>
</div>
</div>
<footer class="site-footer" style="background-color: #257a3b; color: #777777;">
<div class="container">
<div class="row">
<div class="footer-widget">
</div>
</div>
</div>
</footer>
<div class="bottom-line" style="background-color: #b7b7b7; color: #777777;">
<div class="container">
<div class="row">
<div class="col-md-6">
<p>
						© Copyright 2019. All Rights Reserved. Powered by <a href="http://www.chakracentral.com" style="line-height: 1.5;" target="_blank">Chakra</a>

 

 					</p>
</div>
<div class="col-md-6">
<div class="bottom-menu">
<div class="footer-menu"><ul class="menu" id="menu-main-2"><li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-50"><a href="https://badgerandgunner.com/" title="Home">Home</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a href="https://badgerandgunner.com/our-mission/" title="Our Mission">Our Mission</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://badgerandgunner.com/personal-insurance/" title="Personal Insurance">Personal Insurance</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="https://badgerandgunner.com/business-insurance/" title="Business Insurance">Business Insurance</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="https://badgerandgunner.com/not-for-profit-public-entity/" title="Not-For-Profit / Public Entity">Not-For-Profit / Public Entity</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://badgerandgunner.com/contact-us/" title="Contact Us">Contact Us</a><span> /</span></li>
<li class="scroll_btn menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://badgerandgunner.com/trusted-business-partners/" title="Trusted Business Partners">Trusted Business Partners</a><span> /</span></li>
</ul></div>
</div>
</div>
</div>
</div>
</div>
<div id="to-the-top"><i class="fa fa-angle-up"></i></div>
<script id="contact-form-7-js-extra" type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/badgerandgunner.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script id="contact-form-7-js" src="https://badgerandgunner.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2" type="text/javascript"></script>
<script id="modified-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery-transit-modified.js?ver=5.5.3" type="text/javascript"></script>
<script id="colorbox-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery.colorbox-min.js?ver=5.5.3" type="text/javascript"></script>
<script id="jcarousel-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery.jcarousel.min.js?ver=5.5.3" type="text/javascript"></script>
<script id="flexslider-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery.flexslider.js?ver=5.5.3" type="text/javascript"></script>
<script id="fitvid-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/jquery.fitvids.js?ver=5.5.3" type="text/javascript"></script>
<script id="custom-js-js" src="https://badgerandgunner.com/wp-content/themes/magnis/js/custom.js?ver=5.5.3" type="text/javascript"></script>
<script id="wp-embed-js" src="https://badgerandgunner.com/wp-includes/js/wp-embed.min.js?ver=5.5.3" type="text/javascript"></script>
</body>
</html>