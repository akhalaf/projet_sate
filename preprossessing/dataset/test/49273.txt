<!DOCTYPE html>
<html lang="en"><head>
<meta content="text/html;charset=utf-8" http-equiv="Content-type"/>
<title>Traffic Collision - Free Game on 8iz.com</title>
<link href="http://8iz.com/opensearch_desc.xml" rel="search" title="8iz" type="application/opensearchdescription+xml"/>
<meta content="Play Traffic Collision game for free online at 8iz games." name="description"/>
<link href="http://imgs.dab3games.com/traffic-collision-game.jpg" rel="image_src"/>
<meta content="http://imgs.dab3games.com/traffic-collision-game.jpg" property="og:image"/>
<meta content="Traffic Collision" property="og:title"/>
<meta content="Race through the streets avoiding all collisions with the other sports cars." property="og:description"/>
<meta content="summary" name="twitter:card"/>
<meta content="@8izgames" name="twitter:site"/>
<meta content="Play Traffic Collision Game" name="twitter:title"/>
<meta content="Race through the streets avoiding all collisions with the other sports cars." name="twitter:description"/>
<meta content="http://imgs.dab3games.com/traffic-collision-game.jpg" name="twitter:image"/>
<meta content="crash" property="article:tag"/><meta content="traffic" property="article:tag"/><meta content="car" property="article:tag"/><link href="http://8iz.com/game/traffic-collision" rel="canonical"/>
<meta content="8iz" property="og:site_name"/>
<meta content="website" property="og:type"/>
<link href="http://ar.8iz.com/juego/accidente-de-trafico" hreflang="es-AR" rel="alternate"/><link href="http://8iz.at/g/verkehrsunfall" hreflang="de-AT" rel="alternate"/><link href="http://8iz.com.br/jogo/trafego-collision" hreflang="pt-BR" rel="alternate"/><link href="http://topigri.net/g/traffic-collision" hreflang="bg" rel="alternate"/><link href="http://cl.8iz.com/juego/accidente-de-trafico" hreflang="es-CL" rel="alternate"/><link href="http://8iz.com.co/juego/accidente-de-trafico" hreflang="es-CO" rel="alternate"/><link href="http://ec.8iz.com/juego/accidente-de-trafico" hreflang="es-EC" rel="alternate"/><link href="http://pelit1.com/g/traffic-collision" hreflang="fi" rel="alternate"/><link href="http://8iz.fr/jeu/accident-de-la-route" hreflang="fr" rel="alternate"/><link href="http://8spiele.de/g/verkehrsunfall" hreflang="de-DE" rel="alternate"/><link href="http://gr.8iz.com/g/traffic-collision" hreflang="el" rel="alternate"/><link href="http://hu.8iz.com/g/forgalmi-ütkozet" hreflang="hu" rel="alternate"/><link href="http://8iz.it/gioco/incidente-stradale" hreflang="it" rel="alternate"/><link href="http://8iz.com.mx/juego/accidente-de-trafico" hreflang="es-MX" rel="alternate"/><link href="http://no.8iz.com/g/trafikk-kollisjon" hreflang="no-NO" rel="alternate"/><link href="http://pe.8iz.com/juego/accidente-de-trafico" hreflang="es-PE" rel="alternate"/><link href="http://8iz.pl/gry/kolizja-drogowa" hreflang="pl" rel="alternate"/><link href="http://pt.8iz.com/jogo/trafego-collision" hreflang="pt-PT" rel="alternate"/><link href="http://ru.8iz.com/g/авария-на-дороге" hreflang="ru-RU" rel="alternate"/><link href="http://8iz.es/juego/accidente-de-trafico" hreflang="es-ES" rel="alternate"/><link href="http://8iz.se/g/trafikolycka" hreflang="sv-SE" rel="alternate"/><link href="http://8oyunlar.com/oyun/trafik-kazası" hreflang="tr" rel="alternate"/><link href="http://8iz.com/game/traffic-collision" hreflang="en" rel="alternate"/><link href="http://uy.8iz.com/juego/accidente-de-trafico" hreflang="es-UY" rel="alternate"/><link href="http://8iz.co.ve/juego/accidente-de-trafico" hreflang="es-VE" rel="alternate"/><link href="http://trochoi8.com/g/tai-nạn-giao-thong" hreflang="vi-VN" rel="alternate"/><meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" name="viewport"/>
<link href="http://imgs.dab3games.com/css/stylesv2.css?v=6" rel="stylesheet" type="text/css"/>
<link href="http://imgs.dab3games.com/img/favicon96.png" rel="icon" sizes="96x96" type="image/png"/>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
(adsbygoogle = window.adsbygoogle || []).push({
google_ad_client: "ca-pub-6893876361346206",
enable_page_level_ads: true
});
</script>
<script async="async" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script>
 var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>
<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1786990/playpage', [728, 90], 'div-gpt-ad-1530018833311-0').addService(googletag.pubads());
    googletag.pubads().disableInitialLoad();
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
//load the apstag.js library
!function(a9,a,p,s,t,A,g){if(a[a9])return;function q(c,r){a[a9]._Q.push([c,r])}a[a9]={init:function(){q("i",arguments)},fetchBids:function(){q("f",arguments)},setDisplayBids:function(){},targetingKeys:function(){return[]},_Q:[]};A=p.createElement(s);A.async=!0;A.src=t;g=p.getElementsByTagName(s)[0];g.parentNode.insertBefore(A,g)}("apstag",window,document,"script","//c.amazon-adsystem.com/aax2/apstag.js");
//initialize the apstag.js library on the page to allow bidding
apstag.init({
     pubID: '224ebe28-33e0-4f09-ad5e-18bbe1a5b097', //enter your pub ID here as shown above, it must within quotes
     adServer: 'googletag'
});
apstag.fetchBids({
     slots: [{
         slotID: 'div-gpt-ad-1530018833311-0', //example: 'div-gpt-ad-1475185990716-0'
         slotName: '/1786990/playpage', //example: '12345/leaderboard-1'
         sizes: [[728,90]] //example: [[728,90]]
     }],
     timeout: 2e3
}, function(bids) {
     // set apstag targeting on googletag, then trigger the first DFP request in googletag's disableInitialLoad integration
     googletag.cmd.push(function(){
         apstag.setDisplayBids();
         googletag.pubads().refresh();
		 setTimeout(function(){
		 document.getElementById('closead').style.display="block";
		}, 2000);
     });
});
</script>
</head><body itemscope="itemscope" itemtype="http://schema.org/WebPage">
<div id="wrapper"><div id="header"><div id="logo">
<div class="wrapit"><div class="topperleft">
<a class="sitename" href="http://8iz.com">Cool Games</a>
<div class="recently_played_wrap" id="reclike_btn" onclick="showLikes()">
<button class="likedgamesbtn"></button>
</div>
<div class="recently_played_wrap" id="recplay_btn" onclick="showRec()">
<button class="recentlyplayedbtn"></button>
</div>
</div>
<div class="topperight">
<form action="http://8iz.com/search/" class="topserch topcorners" method="post">
<input name="query" placeholder="search" type="text"/>
<button>Search</button></form>
<div class="categorie_dropdown topcorners">
<div>All games</div>
<ul><li><a href="http://8iz.com/new">New Games</a></li>
<li><a href="http://8iz.com/top">Top Games</a></li>
<li><a href="http://8iz.com/hot">Hot Games</a></li>
<li><a href="http://8iz.com/random">Random Game</a></li>
<li class="bordertop"><a href="http://8iz.com/en/racing">Racing Games</a></li>
<li><a href="http://8iz.com/en/lego">Lego Games</a></li>
<li><a href="http://8iz.com/en/airplane">Airplane Games</a></li>
<li><a href="http://8iz.com/en/idle">Idle Games</a></li>
<li><a href="http://8iz.com/en/minecraft">Minecraft Games</a></li>
<li><a href="http://8iz.com/en/super-hero">Super Hero Games</a></li>
</ul></div>
<div class="language_select topcorners">
<button class="activelanguage en">English</button>
<ul> <li class="ar">
<a href="http://ar.8iz.com/juego/accidente-de-trafico">Argentina</a>
</li>
<li class="at">
<a href="http://8iz.at/g/verkehrsunfall">Austria</a>
</li>
<li class="br">
<a href="http://8iz.com.br/jogo/trafego-collision">Brazil</a>
</li>
<li class="bg">
<a href="http://topigri.net/g/traffic-collision">Bulgaira</a>
</li>
<li class="cl">
<a href="http://cl.8iz.com/juego/accidente-de-trafico">Chile</a>
</li>
<li class="co">
<a href="http://8iz.com.co/juego/accidente-de-trafico">Columbia</a>
</li>
<li class="ec">
<a href="http://ec.8iz.com/juego/accidente-de-trafico">Ecuador</a>
</li>
<li class="fl">
<a href="http://pelit1.com/g/traffic-collision">Finland</a>
</li>
<li class="fr">
<a href="http://8iz.fr/jeu/accident-de-la-route">France</a>
</li>
<li class="de">
<a href="https://spiel2.com/">Germany</a>
</li>
<li class="gr">
<a href="http://gr.8iz.com/g/traffic-collision">Greece</a>
</li>
<li class="hu">
<a href="http://hu.8iz.com/g/forgalmi-ütkozet">Hungary</a>
</li>
<li class="it">
<a href="http://8iz.it/gioco/incidente-stradale">Italy</a>
</li>
<li class="mx">
<a href="http://8iz.com.mx/juego/accidente-de-trafico">Mexico</a>
</li>
<li class="no">
<a href="http://no.8iz.com/g/trafikk-kollisjon">Norway</a>
</li>
<li class="pe">
<a href="http://pe.8iz.com/juego/accidente-de-trafico">Peru</a>
</li>
<li class="pl">
<a href="http://8iz.pl/gry/kolizja-drogowa">Poland</a>
</li>
<li class="pt">
<a href="http://pt.8iz.com/jogo/trafego-collision">Portugal</a>
</li>
<li class="ru">
<a href="http://ru.8iz.com/g/авария-на-дороге">Russia</a>
</li>
<li class="es">
<a href="http://8iz.es/juego/accidente-de-trafico">Spain</a>
</li>
<li class="se">
<a href="http://8iz.se/g/trafikolycka">Sweden</a>
</li>
<li class="tr">
<a href="http://8oyunlar.com/oyun/trafik-kazası">Turkey</a>
</li>
<li class="en">
<a href="http://8iz.com/game/traffic-collision">United States</a>
</li>
<li class="uy">
<a href="http://uy.8iz.com/juego/accidente-de-trafico">Uruguay</a>
</li>
<li class="ve">
<a href="http://8iz.co.ve/juego/accidente-de-trafico">Venezuela</a>
</li>
<li class="vn">
<a href="http://trochoi8.com/g/tai-nạn-giao-thong">Vietnam</a>
</li>
</ul>
</div></div></div></div></div>
<div class="recentlyplayed" id="likedgamesid">
<div class="bigtitle">Liked Games</div>
<ul class="game_list">
You have not liked any games yet.
</ul></div><div class="recentlyplayed" id="recplayid">
<div class="bigtitle">Recently Played</div>
<ul class="game_list">
</ul></div>
<div class="page">
<div class="ad970 margy" style="padding:5px 0 0 0;max-height:100px;height:100px;overflow:hidden">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div><div id="game_wrap" style="width:1230px">
<div class="entiregamewrapp">
<div class="banneradsides">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="moregames leftorders">
<ul class="game_list gamepage">
<li><a href="http://8iz.com/game/rush-hour-transit" target="_blank" title="Rush Hour Transit">
<img alt="Rush Hour Transit" src="http://imgs.dab3games.com/rush-hour-traffic-game.jpg"/>
<span>Rush Hour Transit</span></a></li>
<li><a href="http://8iz.com/game/car-crossing" target="_blank" title="Car Crossing">
<img alt="Car Crossing" src="http://imgs.dab3games.com/car-crossing-game.jpg"/>
<span>Car Crossing</span></a></li>
<li><a href="http://8iz.com/game/red-driver-5" target="_blank" title="Red Driver 5">
<img alt="Red Driver 5" src="http://imgs.dab3games.com/reddriver5iz.jpg"/>
<span>Red Driver 5</span></a></li>
<li>
<a href="http://8iz.com/game/slither.io-online" target="_blank" title="Slither.io">
<img alt="Slither.io" src="http://imgs.dab3games.com/slitherio-game.jpg" style="margin-left:-33px"/>
<span>Slither.io</span></a>
</li>
<li id="dynamic-editorial"><a href="http://8iz.com/game/happy-wheels" title="Happy Wheels">
<img alt="Happy Wheels" src="http://imgs.dab3games.com/happywheelgame.jpg"/>
<span>Happy Wheels</span></a></li>
</ul>
</div>
<div id="gameadloadwrap" style="width:644px;height:730px"><div class="afgrunning" id="game_wrapper" style="width:640px;height:725px">
<div class="gamepageheader" style="width:640px">
<h1>Traffic Collision</h1>
</div>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" height="640" width="640">
<param name="movie" value="http://imgs.dab3games.com/swf/traffic-collision81.swf"/>
<param name="flashvars" value=""/>
<param name="quality" value="high"/>
<!--[if !IE]><!--><param name="wmode" value="opaque"/><!--<![endif]-->
<param name="wmode" value="direct"/>
<param name="allownetworking" value="internal"/><param name="allowscriptaccess" value="always"/>
<param name="menu" value="false"/>
<!--[if !IE]><!--><embed align="middle" allownetworking="internal" allowscriptaccess="always" height="640" http:="" imgs.dab3games.com="" pluginspage="http://www.adobe.com/go/getflashplayer" quality="high" src="http://imgs.dab3games.com/swf/traffic-collision81.swf" swf="" traffic-collision81.swf="" type="application/x-shockwave-flash" width="640" wmode="direct"/>
</object>
<!--<![endif]-->
<!--[if IE]><embed width="640"
height="640"
align="middle"
type="application/x-shockwave-flash"
pluginspage="http://www.adobe.com/go/getflashplayer"
quality="high"
allowNetworking = "internal" allowscriptaccess="always"
src="http://imgs.dab3games.com/swf/traffic-collision81.swf" /> 
</object> 
<![endif]-->
<div class="rating_buttons_wrap" style="width:634px">
<div class="rb_left">
<a href="http://8iz.com" title="8iz Games">
<button class="go_back">8iz Games</button>
</a>
</div>
<div class="rb_right">
<div class="rating_wraps_up">
<meta content="Traffic Collision" itemprop="name"/>
<meta content="http://imgs.dab3games.com/traffic-collision-game.jpg" itemprop="image"/>
<meta content="Race through the streets avoiding all collisions with the other sports cars." itemprop="description"/>
<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" style="float:none">
<meta content="1" itemprop="worstRating"/>
<meta content="8.5" itemprop="ratingValue"/>
<meta content="289" itemprop="ratingCount"/>
<meta content="10" itemprop="bestRating"/>
</div>
<div class="total_rating">85%</div>
<button class="thumbs_up" id="thumbsupbtn" onclick="likeGame(3430,true)"></button>
<button class="thumbs_down" id="thumbsdownbtn" onclick="likeGame(3430,false)"></button>
</div></div>
</div>
</div>
<div id="mainContainer" style="width:640px;height:705px">
<video id="contentElement" style="width:640px;height:705px">
</video>
<div id="adContainer" style="width:640px;height:705px"></div>
</div><div class="afgisgoing" id="gameisloading"></div>
</div>
<div class="moregames">
<ul class="game_list gamepage">
<li><a href="http://8iz.com/game/road-safety" target="_blank" title="Road Safety">
<img alt="Road Safety" src="http://imgs.dab3games.com/road-safety-300.png"/>
<span>Road Safety</span></a></li>
<li><a href="http://8iz.com/game/traffic-lanes" target="_blank" title="Traffic Lanes">
<img alt="Traffic Lanes" src="http://imgs.dab3games.com/traffic-lanes-game.jpg"/>
<span>Traffic Lanes</span></a></li>
<li><a href="http://8iz.com/game/traffic-talent" target="_blank" title="Traffic Talent">
<img alt="Traffic Talent" src="http://imgs.dab3games.com/traffictalent.jpg"/>
<span>Traffic Talent</span></a></li>
<li>
<a href="http://8iz.com/game/stick-city" target="_blank" title="Stick City Game">
<img alt="Stick City Game" src="http://imgs.dab3games.com/stick-city-game-alt.jpg"/>
<span>Stick City</span></a>
</li>
<li id="dynamic-editorial2">
<a href="http://8iz.com/game/mario-maker" title="Mario Maker">
<img alt="Mario Maker" src="http://imgs.dab3games.com/mario-maker-games.jpg"/>
<span>Mario Maker</span></a>
</li>
</ul>
</div>
<div class="banneradsides rightad">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div></div>
<div class="ad970 margy" style="padding-top:5px;margin:0 auto 3px auto;max-height:100px;height:100px;overflow:hidden">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div><div class="game_info_desc">
<div class="page">
<div id="game_bit_info">
<h2>Traffic Collision on 8iz</h2>
<div id="game_thumb">
<img alt="Traffic Collision Game" class="ftimagesrc" src="http://imgs.dab3games.com/traffic-collision-ftg.jpg"/><img alt="Traffic Collision" src="http://imgs.dab3games.com/traffic-collision-game.jpg"/>
</div>
<p id="game_description">Race through the streets avoiding all collisions with the other sports cars.</p>
<p><strong>How to play:</strong><br/>Arrow keys to drive, Up arrow for turbo</p>
<p><strong>Author:</strong><br/><a href="https://lagged.com" target="_blank">Lagged</a></p>
<div class="moreofgame"><a href="http://8iz.com/en/crash" title=" Games"></a> <a href="http://8iz.com/en/traffic" title=" Games"></a> <a href="http://8iz.com/en/car" title=" Games"></a> </div>
<div class="page_desc minit">
→Recommended related game: <a class="underline-it" href="http://8iz.com/game/rush-hour-transit" title="Rush Hour Transit Game">Rush Hour Transit</a>
</div>
<div id="playbtnwrap"></div>
</div>
<div class="ad300 gamepage">
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="bottom_game_info_box">
Traffic Collision is a free online crash game that you can play here on 8iz. It has been played thousands of times and has a rating of 8.5/10 (out of 289 ratings). Traffic Collision is another one of the many <a href="http://8iz.com/en/crash">crash games</a> that we offer. If you enjoyed this game and want to play similar games make sure to play Car Crossing or Red Driver 5 or just go to our <a href="http://8iz.com/en/traffic">traffic games</a> page, the <a href="http://8iz.com/en/car">car games</a> page, or if you want to see all of the games we have view the <a href="http://8iz.com/top">top games</a> page. <a href="http://8iz.com/more/traffic-collision">More »</a>
</div></div></div>
<div class="page homepagepadde2" style="clear:both;padding-top:15px;">
<div class="tcol_left_side">
<h3>New Games</h3><ul class="game_list">
<li><a href="http://8iz.com/game/footyzag548" title="FootyZag">
<img alt="footyzag82.png" height="113" src="http://imgs.dab3games.com/footyzag82.png" width="150"/>
<span>FootyZag</span></a></li>
<li><a href="http://8iz.com/game/creamy-ice383" title="Creamy Ice">
<img alt="creamy-ice30.png" height="113" src="http://imgs.dab3games.com/creamy-ice30.png" width="150"/>
<span>Creamy Ice</span></a></li>
<li><a href="http://8iz.com/game/bingo323" title="Bingo">
<img alt="bingo61.png" height="113" src="http://imgs.dab3games.com/bingo61.png" width="150"/>
<span>Bingo</span></a></li>
<li><a href="http://8iz.com/game/ant-smash950" title="Ant Smash">
<img alt="ant-smash62.png" height="113" src="http://imgs.dab3games.com/ant-smash62.png" width="150"/>
<span>Ant Smash</span></a></li>
<li><a href="http://8iz.com/game/angry-flappy-wings838" title="Angry Flappy Wings">
<img alt="angry-flappy-wings26.png" height="113" src="http://imgs.dab3games.com/angry-flappy-wings26.png" width="150"/>
<span>Angry Flappy Wings</span></a></li>
<li><a href="http://8iz.com/game/footyzag413" title="FootyZag">
<img alt="footyzag82.png" height="113" src="http://imgs.dab3games.com/footyzag82.png" width="150"/>
<span>FootyZag</span></a></li>
<li><a href="http://8iz.com/game/creamy-ice202" title="Creamy Ice">
<img alt="creamy-ice30.png" height="113" src="http://imgs.dab3games.com/creamy-ice30.png" width="150"/>
<span>Creamy Ice</span></a></li>
<li><a href="http://8iz.com/game/bingo248" title="Bingo">
<img alt="bingo61.png" height="113" src="http://imgs.dab3games.com/bingo61.png" width="150"/>
<span>Bingo</span></a></li>
<li><a href="http://8iz.com/game/ant-smash2" title="Ant Smash">
<img alt="ant-smash62.png" height="113" src="http://imgs.dab3games.com/ant-smash62.png" width="150"/>
<span>Ant Smash</span></a></li>
<li><a href="http://8iz.com/game/angry-flappy-wings574" title="Angry Flappy Wings">
<img alt="angry-flappy-wings26.png" height="113" src="http://imgs.dab3games.com/angry-flappy-wings26.png" width="150"/>
<span>Angry Flappy Wings</span></a></li>
<li><a href="http://8iz.com/game/footyzag973" title="FootyZag">
<img alt="footyzag82.png" height="113" src="http://imgs.dab3games.com/footyzag82.png" width="150"/>
<span>FootyZag</span></a></li>
<li><a href="http://8iz.com/game/creamy-ice101" title="Creamy Ice">
<img alt="creamy-ice30.png" height="113" src="http://imgs.dab3games.com/creamy-ice30.png" width="150"/>
<span>Creamy Ice</span></a></li><a class="alllink gmpages" href="http://8iz.com/new">More new games</a>
</ul></div><div class="tcol_right_side">
<div class="fullpagead_600" style="width:306px;height:253px;margin-top:24px">
<div>
<script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle" data-ad-client="ca-pub-6893876361346206" data-ad-format="auto" data-ad-slot="7519078795" style="display:block"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
<script>
var descUrl="http://8iz.com/game/traffic-collision";
var gameWidth=640;
var gameHeight=705;
var isadwords=false;
var gameiframe=0;
var gameId=3430;
</script>
<script>
function startTheGame(){
$('#mainContainer').remove();$('#game_wrapper').removeClass('afgrunning');
adsplaying=!1,adsManager?adsManager.destroy():null}
</script>
<script src="//imasdk.googleapis.com/js/sdkloader/ima3.js" type="text/javascript"></script>
<script src="http://imgs.dab3games.com/css/preloader2.js?v=1"></script>
<script src="http://imgs.dab3games.com/css/mobilecheck2.js"></script>
<script type="text/javascript">
$(window).load(function(){
var hasFlash = false;
var isMobile=mobilecheck();
try{
var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
if(fo) hasFlash = true;
}catch(e){
if(navigator.mimeTypes ["application/x-shockwave-flash"] != undefined) hasFlash = true;
}
if(!hasFlash&&gameiframe==0){
setTimeout(function(){
$('#mainContainer,#game_wrapper,#gameisloading,.veedpage').remove();
$('#gameadloadwrap').html('<div style="margin:10px 0;text-align:center"><strong>Your device does not support this game!</strong><br><br><b>Download</b> or <b>activate</b> <a href="https://get.adobe.com/flashplayer/" target="_blank">Adobe Flash</a> in your browser to play this free game.<br><br><button id="play_btn" onclick="window.open(\'https://lagged.com/\',\'_self\');">Play Free Games</button></div>');
$('#playbtnwrap').html('<div style="margin-bottom:10px"><strong>Your device does not support this game!</strong><br>Either download or enable <a href="https://get.adobe.com/flashplayer/" target="_blank">Flash</a>.<br><br><button id="play_btn" onclick="window.open(\'https://lagged.com/\',\'_self\');">Play Free Games</button></div>');
}, 6500);
}
if(isMobile){
setTimeout(function(){
$('#mainContainer,#game_wrapper,#gameisloading,.veedpage').remove();
$('#gameadloadwrap').html('<div style="margin-bottom:10px"><strong>Your device does not support this game!</strong><br>Either download <a href="https://get.adobe.com/flashplayer/" target="_blank">Adobe Flash</a> or try one of our mobile games.<br><a href="https://lagged.com">Click here</a> to play our mobile games.<br><br><a href="https://itunes.apple.com/us/developer/dominick-bruno/id854707008" target="_blank"><img src="http://www.gamesbutler.net/imgs/downloadapple.png" alt="Apple" class="apple" /></a><a href="https://play.google.com/store/apps/developer?id=GamesButler" target="_blank"><img src="http://www.gamesbutler.net/imgs/downloadgoog.png" alt="Android" class="goog" /></a><button id="play_btn" onclick="window.open(\'https://lagged.com/\',\'_self\');">Play Mobile Games</button></div>');
$('#playbtnwrap').html('<div style="margin-bottom:10px"><strong>Your device does not support this game!</strong><br>Either download <a href="https://get.adobe.com/flashplayer/" target="_blank">Adobe Flash</a> or try one of our mobile games.<br><br><a href="https://itunes.apple.com/us/developer/dominick-bruno/id854707008" target="_blank"><img src="http://www.gamesbutler.net/imgs/downloadapple.png" alt="Apple" class="apple" /></a><a href="https://play.google.com/store/apps/developer?id=GamesButler" target="_blank"><img src="http://www.gamesbutler.net/imgs/downloadgoog.png" alt="Android" class="goog" /></a><button id="play_btn" onclick="window.open(\'https://lagged.com/\',\'_self\');">Play Games</button></div>');
}, 5000);
}
$('#dynamic-editorial').html('<a href="'+cpmStar.getLink()+'" target="_blank"><img src="'+cpmStar.getImageUrl(150,113)+'" /><span>'+cpmStar.getTitle()+'</span></a>');
cpmStar.nextAd();
$('#dynamic-editorial2').html('<a href="'+cpmStar.getLink()+'" target="_blank"><img src="'+cpmStar.getImageUrl(150,113)+'" /><span>'+cpmStar.getTitle()+'</span></a>');
});
</script>
<script type="text/javascript">
function removeAdSwf(){$('#mainContainer').remove();$('#game_wrapper').removeClass('afgrunning');
$('#gameisloading').removeClass('afgisgoing');
}
function noAdsReturned(){removeAdSwf();}	
</script>
<script src="https://server.cpmstar.com/view.aspx?poolid=15925&amp;multi=2&amp;script=1&amp;subpoolid=888"></script>
<script src="https://server.cpmstar.com/cached/textad.js"></script>
<div id="stickyadwrap" style="z-index:1000;height:90px; width:728px;max-width:100%;position:fixed;bottom:2px;left:calc(50% - 364px);"><button id="closead" onclick="document.getElementById('stickyadwrap').style.display='none';" style="position:absolute;right:-9px;top:-9px;border-radius:9px;background-color:#000;width:18px;height:18px;line-height:16px;text-indent: -1px;color:#fff;border:0;outline:0;display:none">X</button>
<div id="div-gpt-ad-1530018833311-0" style="height:90px; width:728px;">
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1530018833311-0'); });
</script>
</div></div>
<div id="footer"><div>© 8iz Games 2021 | <a href="http://8iz.com/about">About</a> · <a href="http://8iz.com/blog">Blog</a> · <a href="http://8iz.com/contact">Contact</a> · <a href="http://8iz.com/privacy">Privacy Policy</a>
</div></div></div>
<script async="" src="http://8iz.com/js/ajax.js?v=2"></script>
<script async="" src="https://v3arcade.org/bounceads.js"></script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-46839320-1', '8iz.com');
ga('send', 'pageview');
</script>
</div></body></html>
