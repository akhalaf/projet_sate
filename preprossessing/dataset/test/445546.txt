<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<link href="https://duijaros.com/xmlrpc.php" rel="pingback"/>
<script>var et_site_url='https://duijaros.com';var et_post_id='9';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script>
<!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Duija Ros - Recovering Sacredness - The Art of Embodiment</title><link data-minify="1" href="https://duijaros.com/wp-content/cache/min/1/dc26e6bb12481bb92fefee7b3b686f44.css" media="all" rel="stylesheet"/>
<meta content="Embodiment is the process of letting life guide us to find all the small, unwanted, and shameful places in us that we still identify with." name="description"/>
<meta content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" name="robots"/>
<link href="https://duijaros.com/" rel="canonical"/>
<meta content="en_US" property="og:locale"/>
<meta content="website" property="og:type"/>
<meta content="Duija Ros - Recovering Sacredness - The Art of Embodiment" property="og:title"/>
<meta content="Embodiment is the process of letting life guide us to find all the small, unwanted, and shameful places in us that we still identify with." property="og:description"/>
<meta content="https://duijaros.com/" property="og:url"/>
<meta content="Duija Ros" property="og:site_name"/>
<meta content="2020-12-15T16:19:42+00:00" property="article:modified_time"/>
<meta content="https://duijaros.com/wp-content/uploads/2019/02/duija-ros-recovering-sacredness.jpg" property="og:image"/>
<meta content="1200" property="og:image:width"/>
<meta content="900" property="og:image:height"/>
<meta content="summary_large_image" name="twitter:card"/>
<meta content="Est. reading time" name="twitter:label1"/>
<meta content="3 minutes" name="twitter:data1"/>
<script class="yoast-schema-graph" type="application/ld+json">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://duijaros.com/#website","url":"https://duijaros.com/","name":"Duija Ros","description":"Recovering Sacredness","potentialAction":[{"@type":"SearchAction","target":"https://duijaros.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"ImageObject","@id":"https://duijaros.com/#primaryimage","inLanguage":"en-US","url":"https://duijaros.com/wp-content/uploads/2019/02/duija-ros-recovering-sacredness.jpg","width":1200,"height":900},{"@type":"WebPage","@id":"https://duijaros.com/#webpage","url":"https://duijaros.com/","name":"Duija Ros - Recovering Sacredness - The Art of Embodiment","isPartOf":{"@id":"https://duijaros.com/#website"},"primaryImageOfPage":{"@id":"https://duijaros.com/#primaryimage"},"datePublished":"2019-01-15T20:13:06+00:00","dateModified":"2020-12-15T16:19:42+00:00","description":"Embodiment is the process of letting life guide us to find all the small, unwanted, and shameful places in us that we still identify with.","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://duijaros.com/"]}]}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="//fonts.googleapis.com" rel="dns-prefetch"/>
<link href="//s.w.org" rel="dns-prefetch"/>
<link crossorigin="" href="https://fonts.gstatic.com" rel="preconnect"/>
<link href="https://duijaros.com/feed/" rel="alternate" title="Duija Ros » Feed" type="application/rss+xml"/>
<link href="https://duijaros.com/comments/feed/" rel="alternate" title="Duija Ros » Comments Feed" type="application/rss+xml"/>
<meta content="Divi Child Theme v." name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link href="https://duijaros.com/wp-content/themes/Divi-Child/style.css?ver=4.7.7" id="divi-style-css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Bad+Script:regular|Raleway:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&amp;subset=latin,latin-ext&amp;display=swap" id="et-builder-googlefonts-cached-css" media="all" rel="stylesheet" type="text/css"/>
<script id="jquery-core-js" src="https://duijaros.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1" type="text/javascript"></script>
<script id="ct_public-js-extra" type="text/javascript">
/* <![CDATA[ */
var ctPublic = {"_ajax_nonce":"fb02f96f31","_ajax_url":"https:\/\/duijaros.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script id="bookly.min.js-js-extra" type="text/javascript">
/* <![CDATA[ */
var BooklyL10n = {"ajaxurl":"https:\/\/duijaros.com\/wp-admin\/admin-ajax.php","csrf_token":"b9fdfb4cf7","today":"Today","months":["January","February","March","April","May","June","July","August","September","October","November","December"],"days":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"daysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"nextMonth":"Next month","prevMonth":"Previous month","show_more":"Show more"};
/* ]]> */
</script>
<script id="bookly-customer-profile.js-js-extra" type="text/javascript">
/* <![CDATA[ */
var BooklyCustomerProfileL10n = {"csrf_token":"b9fdfb4cf7","show_more":"Show more"};
/* ]]> */
</script>
<link href="https://duijaros.com/wp-json/" rel="https://api.w.org/"/><link href="https://duijaros.com/wp-json/wp/v2/pages/9" rel="alternate" type="application/json"/><link href="https://duijaros.com/xmlrpc.php?rsd" rel="EditURI" title="RSD" type="application/rsd+xml"/>
<link href="https://duijaros.com/wp-includes/wlwmanifest.xml" rel="wlwmanifest" type="application/wlwmanifest+xml"/>
<meta content="WordPress 5.6" name="generator"/>
<link href="https://duijaros.com/" rel="shortlink"/>
<link href="https://duijaros.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fduijaros.com%2F" rel="alternate" type="application/json+oembed"/>
<link href="https://duijaros.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fduijaros.com%2F&amp;format=xml" rel="alternate" type="text/xml+oembed"/>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/><link as="font" crossorigin="anonymous" href="https://duijaros.com/wp-content/themes/Divi/core/admin/fonts/modules.ttf" rel="preload"/><link href="https://duijaros.com/wp-content/uploads/2019/01/cropped-moon-site-icon-01-1-32x32.png" rel="icon" sizes="32x32"/>
<link href="https://duijaros.com/wp-content/uploads/2019/01/cropped-moon-site-icon-01-1-192x192.png" rel="icon" sizes="192x192"/>
<link href="https://duijaros.com/wp-content/uploads/2019/01/cropped-moon-site-icon-01-1-180x180.png" rel="apple-touch-icon"/>
<meta content="https://duijaros.com/wp-content/uploads/2019/01/cropped-moon-site-icon-01-1-270x270.png" name="msapplication-TileImage"/>
</head>
<body class="home page-template-default page page-id-9 et_pb_button_helper_class et_transparent_nav et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns3 et_cover_background et_pb_gutter et_pb_gutters3 et_pb_pagebuilder_layout et_no_sidebar et_divi_theme et-db et_minified_js et_minified_css" data-rsssl="1">
<div id="page-container">
<header data-height-onload="122" id="main-header">
<div class="container clearfix et_menu_container">
<div class="logo_container">
<span class="logo_helper"></span>
<a href="https://duijaros.com/">
<img alt="Duija Ros" data-height-percentage="79" id="logo" src="https://duijaros.com/wp-content/uploads/2019/01/duija-ros-logo-light-foreground-1a.png"/>
</a>
</div>
<div data-fixed-height="92" data-height="122" id="et-top-navigation">
<nav id="top-menu-nav">
<ul class="nav" id="top-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83" id="menu-item-83"><a href="https://duijaros.com/about-duija/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56" id="menu-item-56"><a href="https://duijaros.com/services/">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76" id="menu-item-76"><a href="https://duijaros.com/testimonials/">Testimonials</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-258" id="menu-item-258"><a href="https://duijaros.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113" id="menu-item-113"><a href="https://duijaros.com/contact/">Contact</a></li>
</ul> </nav>
<div id="et_mobile_nav_menu">
<div class="mobile_nav closed">
<span class="select_page">Select Page</span>
<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
</div>
</div> </div> <!-- #et-top-navigation -->
</div> <!-- .container -->
<div class="et_search_outer">
<div class="container et_search_form_container">
<form action="https://duijaros.com/" class="et-search-form" method="get" role="search">
<input class="et-search-field" name="s" placeholder="Search …" title="Search for:" type="search" value=""/> </form>
<span class="et_close_search_field"></span>
</div>
</div>
</header> <!-- #main-header -->
<div id="et-main-area">
<div id="main-content">
<article class="post-9 page type-page status-publish has-post-thumbnail hentry" id="post-9">
<div class="entry-content">
<div class="et-boc" id="et-boc">
<div class="et-l et-l--post">
<div class="et_builder_inner_content et_pb_gutters3">
<div class="et_pb_section et_pb_section_0 et_pb_fullwidth_section et_section_regular section_has_divider et_pb_bottom_divider">
<div class="et_pb_module et_pb_fullwidth_slider_0 et_pb_slider et_pb_slider_parallax">
<div class="et_pb_slides">
<div class="et_pb_slide et_pb_slide_0 et_pb_section_parallax et_pb_bg_layout_dark et_pb_slide_with_image et_pb_media_alignment_center et-pb-active-slide" data-slide-id="et_pb_slide_0">
<span class="et_parallax_bg_wrap"><span class="et_parallax_bg" style="background-image: url(https://duijaros.com/wp-content/uploads/2019/01/fire-sunset-beach-cavern.jpg);"></span></span>
<div class="et_pb_container clearfix">
<div class="et_pb_slider_container_inner">
<div class="et_pb_slide_image"><img alt="" class="wp-image-307" height="auto" loading="lazy" sizes="(max-width: 378px) 100vw, 378px" src="https://duijaros.com/wp-content/uploads/2019/01/duija-ros.png" srcset="https://duijaros.com/wp-content/uploads/2019/01/duija-ros.png 378w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-150x150.png 150w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-300x300.png 300w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-100x100.png 100w" width="auto"/></div>
<div class="et_pb_slide_description">
<h1 class="et_pb_slide_title"><a href="https://duijaros.com/about-duija/">Recovering Sacredness</a></h1><div class="et_pb_slide_content"><p><em>~ The Art of Embodiment ~</em></p></div>
<div class="et_pb_button_wrapper"><a class="et_pb_button et_pb_more_button" href="https://duijaros.com/about-duija/">About Duija</a></div>
</div> <!-- .et_pb_slide_description -->
</div>
</div> <!-- .et_pb_container -->
</div> <!-- .et_pb_slide -->
</div> <!-- .et_pb_slides -->
</div> <!-- .et_pb_slider -->
<div class="et_pb_bottom_inside_divider et-no-transition"></div>
</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_1 et_section_regular">
<div class="et_pb_row et_pb_row_0">
<div class="et_pb_column et_pb_column_3_5 et_pb_column_0 et_pb_css_mix_blend_mode_passthrough">
<div class="et_pb_module et_pb_image et_pb_image_0">
<span class="et_pb_image_wrap "><img alt="" class="wp-image-307" height="auto" loading="lazy" sizes="(max-width: 378px) 100vw, 378px" src="https://duijaros.com/wp-content/uploads/2019/01/duija-ros.png" srcset="https://duijaros.com/wp-content/uploads/2019/01/duija-ros.png 378w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-150x150.png 150w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-300x300.png 300w, https://duijaros.com/wp-content/uploads/2019/01/duija-ros-100x100.png 100w" title="" width="auto"/></span>
</div><div class="et_pb_module et_pb_text et_pb_text_0 et_pb_text_align_left et_pb_bg_layout_light">
<div class="et_pb_text_inner"><h3 style="text-align: justify;">“Embodiment is the process of letting life guide us to find all the small, unwanted, and shameful places in us that we still identify with. When we get to know them deeply enough, it becomes absolutely clear that we are not those places, but the Love that holds them all.</h3>
<p style="text-align: justify;">The more we are able to love and hold those places, the more we can live from this place of deep relaxation into the Love that we are. Instead of reacting and trying to control life, we allow life to flow through us.</p>
<p style="text-align: justify;">In the end we discover that all the things we have been looking for -love, money, status, appreciation- mean nothing at all when we lose the shame about who we are.</p>
<p style="text-align: justify;">Then we realize that all along, we were only looking for ourselves. We see that we are so full of love that we don’t need anything from the outside world at all.”</p>
<p style="text-align: justify;">
</p><div class="txtNew" data-packed="true" id="comp-jdnq4ei9">
<h3 class="font_7" style="text-align: center;">– Duija<span></span></h3>
<p class="font_7"><span></span></p>
</div></div>
</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_2_5 et_pb_column_1 et_pb_css_mix_blend_mode_passthrough et-last-child">
<div class="et_pb_module et_pb_signup_0 et_pb_recaptcha_enabled et_pb_newsletter_layout_left_right et_pb_newsletter et_pb_subscribe clearfix et_pb_text_align_left et_pb_bg_layout_dark et_pb_newsletter_description_no_content">
<div class="et_pb_newsletter_description"><h2 class="et_pb_module_header">Suscribe to My Mailing List!</h2><div>
<p>Keep up to date on events.</p>
</div></div>
<div class="et_pb_newsletter_form">
<form method="post">
<div class="et_pb_newsletter_result et_pb_newsletter_error"></div>
<div class="et_pb_newsletter_result et_pb_newsletter_success">
<h2>Success!</h2>
</div>
<div class="et_pb_newsletter_fields">
<p class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone">
<label class="et_pb_contact_form_label" for="et_pb_signup_firstname" style="display: none;">First Name</label>
<input class="input" id="et_pb_signup_firstname" name="et_pb_signup_firstname" placeholder="First Name" type="text"/>
</p>
<p class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone">
<label class="et_pb_contact_form_label" for="et_pb_signup_lastname" style="display: none;">Last Name</label>
<input class="input" id="et_pb_signup_lastname" name="et_pb_signup_lastname" placeholder="Last Name" type="text"/>
</p>
<p class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone">
<label class="et_pb_contact_form_label" for="et_pb_signup_email" style="display: none;">Email</label>
<input class="input" id="et_pb_signup_email" name="et_pb_signup_email" placeholder="Email" type="text"/>
</p>
<p class="et_pb_newsletter_button_wrap">
<a class="et_pb_newsletter_button et_pb_button" data-icon="" href="#">
<span class="et_subscribe_loader"></span>
<span class="et_pb_newsletter_button_text">Subscribe</span>
</a>
</p>
</div>
<input name="et_pb_signup_provider" type="hidden" value="mailchimp"/>
<input name="et_pb_signup_list_id" type="hidden" value="31f1df77b2"/>
<input name="et_pb_signup_account_name" type="hidden" value="Recovering Sacredness"/>
<input name="et_pb_signup_ip_address" type="hidden" value="true"/><input name="et_pb_signup_checksum" type="hidden" value="aae97df108e066988607ceb530297f86"/>
</form>
</div>
</div>
</div> <!-- .et_pb_column -->
</div> <!-- .et_pb_row -->
</div> <!-- .et_pb_section --> </div><!-- .et_builder_inner_content -->
</div><!-- .et-l -->
</div><!-- #et-boc -->
</div> <!-- .entry-content -->
</article> <!-- .et_pb_post -->
</div> <!-- #main-content -->
<div class="et_pb_section et_pb_section_3 custom_footer_top et_section_regular et_section_transparent section_has_divider et_pb_bottom_divider">
<div class="et_pb_row et_pb_row_1 et_pb_row_empty">
</div> <!-- .et_pb_row -->
<div class="et_pb_bottom_inside_divider et-no-transition"></div>
</div> <!-- .et_pb_section -->
<footer id="main-footer">
<div class="container">
<div class="clearfix" id="footer-widgets">
<div class="footer-widget"><div class="fwidget et_pb_widget widget_text" id="text-3"> <div class="textwidget"><h4><strong>Recovering Sacredness</strong><br/>
The Art of Embodiment</h4>
<p><em>with Duija Ros</em><br/>
Santa Fe</p>
<p>505 231 1277</p>
</div>
</div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget">
<div class="fwidget et_pb_widget widget_recent_entries" id="recent-posts-4">
<h4 class="title">Latest Blog Entry</h4>
<ul>
<li>
<a href="https://duijaros.com/blog/about-fear/">About fear</a>
</li>
<li>
<a href="https://duijaros.com/uncategorized/walking-each-other-home/">Walking Each Other Home</a>
</li>
<li>
<a href="https://duijaros.com/blog/humbling/">Humbling</a>
</li>
<li>
<a href="https://duijaros.com/uncategorized/beyond-illusion/">Beyond Illusion (poetry)</a>
</li>
<li>
<a href="https://duijaros.com/blog/hearts/">Hearts (poetry)</a>
</li>
</ul>
</div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"></div> <!-- end .footer-widget --> </div> <!-- #footer-widgets -->
</div> <!-- .container -->
<div id="footer-bottom">
<div class="container clearfix">
<ul class="et-social-icons">
<li class="et-social-icon et-social-facebook">
<a class="icon" href="https://www.facebook.com/DuijaBregtjeRos/">
<span>Facebook</span>
</a>
</li>
<li class="et-social-icon et-social-rss">
<a class="icon" href="https://duijaros.com/feed/">
<span>RSS</span>
</a>
</li>
</ul><div id="footer-info">(c) 2019 Duija Bregtje L. Ros </div> </div> <!-- .container -->
</div>
</footer> <!-- #main-footer -->
</div> <!-- #et-main-area -->
</div> <!-- #page-container -->
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//analytics.goldenstupa.org/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '4']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
<script id="divi-custom-script-js-extra" type="text/javascript">
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/duijaros.com\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/duijaros.com\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/duijaros.com\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"b078ec6172","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"f0269f6da4","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","wrong_checkbox":"Checkbox","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","ab_tests":[],"is_ab_testing_active":"","page_id":"9","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"yes","is_shortcode_tracking":"","tinymce_uri":""}; var et_builder_utils_params = {"condition":{"diviTheme":true,"extraTheme":false},"scrollLocations":["app","top"],"builderScrollLocations":{"desktop":"app","tablet":"app","phone":"app"},"onloadScrollLocation":"app","builderType":"fe"}; var et_frontend_scripts = {"builderCssContainerPrefix":"#et-boc","builderCssLayoutPrefix":"#et-boc .et-l"};
var et_pb_box_shadow_elements = [];
var et_pb_motion_elements = {"desktop":[],"tablet":[],"phone":[]};
var et_pb_sticky_elements = [];
/* ]]> */
</script>
<script id="et-recaptcha-v3-js" src="https://www.google.com/recaptcha/api.js?render=6LcTQNIZAAAAAGi5Ya1KR1D_XaFC3Selbe1va7nF&amp;ver=4.7.7" type="text/javascript"></script>
<script id="et-core-api-spam-recaptcha-js-extra" type="text/javascript">
/* <![CDATA[ */
var et_core_api_spam_recaptcha = {"site_key":"6LcTQNIZAAAAAGi5Ya1KR1D_XaFC3Selbe1va7nF","page_action":{"action":"duijaros_com"}};
/* ]]> */
</script>
<script data-minify="1" defer="" src="https://duijaros.com/wp-content/cache/min/1/04c8d55fd0e5c3ea9983bf373c32590e.js"></script></body>
</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1610444694 -->