#!/usr/bin/python3

#Import modules
from bs4 import BeautifulSoup
import os
import pickle
import sys

#Import our python files
import url_features
import extractTextContent
import features_links

train = os.listdir("./not_phishing")

arg = sys.argv[1].split(",")

id = arg[0]
if len(arg ) > 3:
    url = ",".join(arg[1:-2]).replace("\"","")
else:
    url = arg[1]
typ = arg[-1]

if id+".txt" in train:
    with open("./not_phishing/"+id+".txt") as fd:
        f = fd.read()

    features_set = open("./results/features_set_not_phish_"+id+".txt",'ab');

    doc = BeautifulSoup(f, 'html.parser')

    result = features_links.features_l(doc, url)
    if result != [0]*len(result):
        
        data = {"url": url_features.fct(url), "text": extractTextContent.extract(f),"links": result}

        pickle.dump(data, features_set)

        print("Les features de l'id {} ont correctement été écrites !".format(id))

    features_set.close();
            
