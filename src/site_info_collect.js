const fs = require('fs/promises');
const puppeteer = require('puppeteer');
const fetch = require('node-fetch');
const { UDPClient } = require('dns2');
const stringify = require('json-stringify');
const whois = require('whois');
const S_TO_MS = 1000;
const resolve = UDPClient();

async function crawl(){
  let browser = null;
  let page = null;
  const arguments_in = process.argv[2];
  const data = await fs.readFile(arguments_in,'utf8');
  browser = await puppeteer.launch({headless:true, 
    args:['--no-sandbox',
            '--disable-web-security',
            '--disable-features=IsolateOrigins,site-per-process',
            '--ignore-certificate-errors-spki-list',
            '--disable-site-isolation-trials',
            '--ignore-certificate-errors']});

  page = await browser.newPage();
  const session = await page.target().createCDPSession();
  const url = data.split('\n'); 

  let splits = url[0].split("/");
  let nb_packet = 0;

  await page.setRequestInterception(true);
  
  await session.send("Fetch.enable", {
    patterns: [{ requestStage: "Response" }]
  });

  session.on('Fetch.requestPaused', async ({requestId}) => {

    let resolving = await resolve(splits[2]);
    await fs.appendFile("packets.txt",stringify(resolving, null, 2));
    
    /*
    fetch(`http://ipinfo.io/${resolving.answers[0].address}/json`).then(async function(response) {
       if(response.ok) {
        response.text().then(async function(text) {
           await fs.appendFile("location.txt",text);
         });
       } 
       else {
         console.log('pas de localisation');
       }
    });
   */
      whois.lookup(splits[2], async function(err, data) {
        if(err === null)
        {
          await fs.appendFile("whois.txt",data);
        }
      });
      nb_packet++;
    
    try{
     session.send('Fetch.continueRequest',{requestId});
    }
    catch(error){
      console.log("site_collect  ligne 121");

    }
  });
  
  page.on('request',async(requ) =>{
    requ.continue();
  });
	
  try 
  {	
    await page.goto(url[0], {
      timeout: 60 * S_TO_MS,
      waitUntil: ['load', 'domcontentloaded','networkidle0'],
      followRedirect: false
    });

    await session.send('Page.enable');

  }
  catch (error)
  {
    console.log(url[0]);
    throw new Error("Url doesn't work...");
  }
  finally {
    await page.close();
    await browser.close();
  }

  await fs.appendFile("packets.txt",stringify(nb_packet, null, 2));
}

(async () => {
  await crawl();
})();
