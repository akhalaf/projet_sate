const fs = require('fs/promises');
const puppeteer = require('puppeteer');
const fetch = require('node-fetch');
const https = require('https');
const http = require('http');
const timers = require('timers');

const S_TO_MS = 1000;

function getData(url,protocol)
{ 
  if(protocol === 'http'){

  http.get(url, function(res){
        res.on('data', function(data){
          fs.writeFile('data.txt',data);
        });
 
        res.on('error', function(e){
            console.log(e);
        });
 
    });
  }
  else{
    https.get(url, function(res){
      res.on('data', function(data){
        fs.writeFile('data.txt',data);
      });

      res.on('error', function(e){
        console.log(e);
      });
    });
  }
}

function timeOutFunction()
{
  // il va arreter la récupération des données
  // juste après 10 secondes
   setTimeout(exit_prog,10000);
}

function exit_prog()
{
  process.exit(0);
}

async function collectInfo(){
  let browser = null;
  let page = null;
  const arguments_in = process.argv[2];
  const data = await fs.readFile(arguments_in,'utf8');
  browser = await puppeteer.launch({headless:true, 
    args:['--no-sandbox',
            '--disable-web-security',
            '--disable-features=IsolateOrigins,site-per-process',
            '--ignore-certificate-errors-spki-list',
            '--disable-site-isolation-trials',
            '--disable-dev-shm-usage']});
  page = await browser.newPage();
  const session = await page.target().createCDPSession();
  const url = data.split('\n'); 

  let protocole = url[0].split(":");  
  let nb_html = 0;
  let nb_js = 0;
  let nb_css = 0;

  await page.setRequestInterception(true);
  await session.send("Fetch.enable", {
    patterns: [{ requestStage: "Response" }]
  });

  session.on('Fetch.requestPaused', async ({requestId, request, resourceType }) => {
    // Récupération du code HTML
    if(resourceType === 'Document')
    {
      if(request.url.includes("http")){
        fetch(request.url).then(function(response) {
          if(response.ok) {
            return response.text().then(function(text) {
              fs.writeFile("html_code_"+nb_html+".html",text);
              nb_html++;
            });
          } 
          else {

            console.log("ERROR HTML // MAUSVAISE REP DU RESEAU");
          }
        });
      }
    }
    
    // Récupération du code JavaScript
    if(resourceType === 'Script')
    {
      if(request.url.includes("http")){
      	fetch(request.url).then(function(response) {
        	if(response.ok) {
          		return response.text().then(function(text) {
            		fs.writeFile("js_code_"+nb_js+".js",text);
            		nb_js++;
          		});
        	} 
        	else {
          	console.log("ERROR JS // MAUSVAISE REP DU RESEAU");
        	}
      	});
      }
    }

    // Récupération du code CSS
    if(resourceType === 'Stylesheet')
    {
      if(request.url.includes("http")){
        fetch(request.url).then(function(response) {
          if(response.ok) {
            response.text().then(function(text) {
              fs.writeFile("css_code_"+nb_css+".css",text);
              nb_css++;
            });
          } 
          else {
            console.log("ERROR CSS // MAUSVAISE REP DU RESEAU");
          }
        });
      }
    }

    session.send('Fetch.continueRequest',{requestId});
  });

  page.on('request',async(requ) =>{
    requ.continue();
  });
	
  try 
  {	
    await page.goto(url[0], {
      timeout: 60 * S_TO_MS,
      waitUntil: ['load', 'domcontentloaded','networkidle0'],
      followRedirect: false
    });

    await page.screenshot({path:"./screenshot.png", fullPage:true});

    await session.send('Page.enable');

    console.log(url[0])
    console.log(`Writing successfully ! \n`); 

    getData(url[0],protocole[0]);

    timers.setInterval(function(){
        getData(url[0],protocole[0])},
      2000);
    
    timeOutFunction();

  }
  catch (error)
  {	
    console.log(url[0]);
    throw new Error("Url doesn't work...");
  }
  finally{
    await page.close();
    await browser.close();
    console.log("coucou du crawler");
  }
}

(async () => {
  await collectInfo();
})();