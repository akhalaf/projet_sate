#!/bin/bash

npm i
node --unhandled-rejections=strict collect_phishing_links.js

var_css=0
var_js=0
var_html=0

cd dir_to_ignore

dirlist=$(ls .)

for dir in $dirlist
do	
	cd $dir
	subdirlist=$(ls .)

	for subdir in $subdirlist
	do	
		cd $subdir

		if [ $(ls | wc -l) -gt 1 ]
		then
			cd ..
			continue
		else
			node --unhandled-rejections=strict ../../../crawler.js $(ls | grep .txt | grep -v target_site.txt)
			if [ $? -eq 0 ]
			then
				
        if [ $(ls *.html | wc -l) -gt 0 ]
        then
          mkdir HTML
          cp *.html HTML/
          rm -f *.html
          var_html=1
        fi
       	
        if [ $(ls *.js | wc -l) -gt 0 ]
        then
          mkdir JS
          cp *.js JS/
          rm -f *.js
          var_js=1
        fi

        
        if [ $(ls *.css | wc -l) -gt 0 ]
        then
          mkdir CSS
          cp *.css CSS/
          rm -f *.css
          var_css=1
        fi

        mkdir IMG
	      cp *.png IMG/
        rm -f *.png
         
        if [ $var_html -eq 0 ]
        then
            cd ..
            rm -rf $subdir    
        else
          node --unhandled-rejections=strict ../../../site_info_collect.js $(ls | grep .txt | egrep -v 'data.txt')
          
          mkdir INFOS
          
          cp packets.txt INFOS/
          #cp location.txt INFOS/
          cp whois.txt INFOS/

          rm -f packets.txt
          #rm -f location.txt
          rm -f whois.txt
          cd ..
        fi
      else
        cd ..
        rm -rf $subdir
		  fi    
	  fi
  done
  cd ..
done
