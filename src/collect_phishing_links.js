const mkdirp = require('mkdirp');
const fs = require('fs/promises');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

let request = new XMLHttpRequest();
request.open("GET", "https://openphish.com/feed.txt");
request.responseType = 'text';

mkdirp('./dir_to_ignore');

const S_TO_MS = 1000;

(async() =>{
	request.onload = async function()
	{
		let completeFile = request.responseText;
		let urls = completeFile.split("\n");

		for (let i = 0; i < urls.length; i++) {
			let splits = (urls[i]).split("/");
			if(splits.length > 2)
			{ 
				let splits2 = splits[2].split(".");
				
				if(splits2[0].toLowerCase() == 'www')
				{
					await mkdirp('./dir_to_ignore/'+splits2[1][0]);
					await mkdirp('./dir_to_ignore/'+splits2[1][0]+'/'+splits2[1]);
					let file_name_out = `./dir_to_ignore/${splits2[1][0]}/${splits2[1]}/${splits2[1]}.txt`;
					await fs.writeFile(file_name_out,urls[i]);
					
					previous = await './dir_to_ignore/'+splits2[1][0]+'/'+splits2[1];
				}
				else
				{
					await mkdirp('./dir_to_ignore/'+splits2[0][0]);
					await mkdirp('./dir_to_ignore/'+splits2[0][0]+'/'+splits2[0]);
					let file_name_out = `./dir_to_ignore/${splits2[0][0]}/${splits2[0]}/${splits2[0]}.txt`;
					await fs.writeFile(file_name_out,urls[i]);
					
					previous = await './dir_to_ignore/'+splits2[0][0]+'/'+splits2[0];	
				}
			}
		}
	}
  	
	request.send();

	console.log("Save complete !");

})();
