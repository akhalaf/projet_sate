const Jimp = require('jimp');

(async() => {

  const original_image = await Jimp.read("portail.png");
  const doubtful_image = await Jimp.read("ent.png");
  const fake_image = await Jimp.read('polytech.png');
  
  console.log("Compare image Image : ================\n");
//  console.log(`hash (base 64) ${original_image.hash()}`);
 // console.log(`hash (binary) ${original_image.hash(2)}`);
  console.log(`diff.percent ${Jimp.diff(original_image,doubtful_image).percent}`);
  console.log('\n');

  console.log("Doubtful Image : =================\n");
  console.log(`hash (base 64) ${doubtful_image.hash()}`);
  console.log(`hash (binary) ${doubtful_image.hash(2)}`);
  console.log(`distance   ${Jimp.distance(original_image,doubtful_image)}`);
  console.log(`diff.percent ${Jimp.diff(original_image,doubtful_image).percent}`);
  console.log('\n');
  
  console.log("Fake Image : =================\n");
  console.log(`hash (base 64) ${fake_image.hash()}`);
  console.log(`hash (binary) ${fake_image.hash(2)}`);
  console.log(`distance   ${Jimp.distance(original_image,fake_image)}`);
  console.log(`diff.percent ${Jimp.diff(doubtful_image,fake_image).percent}`);
  console.log('\n');

})();

