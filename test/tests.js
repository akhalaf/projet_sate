const fs = require('fs');
const CloudmersiveImageApiClient = require('cloudmersive-image-api-client');
const defaultClient = CloudmersiveImageApiClient.ApiClient.instance;
const Apikey = defaultClient.authentications['Apikey'];
Apikey.apiKey = 'YOUR API KEY';

const apiInstance = new CloudmersiveImageApiClient.RecognizeApi();
const originImage = Buffer.from(
  fs.readFileSync("./image1.png").buffer);
const comparaisonImage =  Buffer.from(
  fs.readFileSync('image2.png').buffer);
let opts = {
  'recognitionMode': "recongnitionMode_example"};

const callback = function(error, data, response){
  if(error){
    fs.writeFileSync("error.log",error.toString());
    console.error(error);
  }
  else{
    console.log('API Called successfuly. Returned data: '+data);
//    console.log(response);
  }};

 apiInstance.recognizeSimilarityCompare(originImage, comparaisonImage,opts,callback); 
