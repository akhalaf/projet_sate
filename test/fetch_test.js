const fs = require('fs/promises');
const puppeteer = require('puppeteer');
const fetch = require('node-fetch');
const S_TO_MS = 1000;

function linkToname(url){

  let result = url.replaceAll('/','\x1a');

  return result;
}

(async() =>{

  const arguments_in = process.argv[2];
  const data = await fs.readFile(arguments_in,'utf8');
  const browser = await puppeteer.launch({headless:false, 
      args:['--no-sandbox',
            '--disable-web-security',
            '--disable-features=IsolateOrigins,site-per-process',
            '--ignore-certificate-errors-spki-list',
            '--disable-site-isolation-trials']});
  const page = await browser.newPage();
  const session = await page.target().createCDPSession();
  const url = data.split('\n');
  
  let nb_html = 0;
  let nb_js = 0;
  let nb_css = 0;

  await page.setRequestInterception(true);

  await session.send("Fetch.enable", {
    patterns: [{ requestStage: "Response" }]
  });

  session.on('Fetch.requestPaused', async ({requestId, request, resourceType }) => {


    //console.log(resourceType);
    if(resourceType === 'Document'){

      fetch(request.url).then(function(response) {
        return response.text().then(function(text) {
          fs.writeFile("html_code_"+nb_html+".html",text);
          nb_html++;
        });
      });
    }
    
    if(resourceType === 'Script'){

      fetch(request.url).then(function(response) {
        return response.text().then(function(text) {
          fs.writeFile("js_code_"+nb_js+".js",text);
          nb_js++;
        });
      });
    }

    if(resourceType === 'Stylesheet'){
      //console.log(request);

      fetch(request.url).then(function(response) {
        return response.text().then(function(text) {
          fs.writeFile("css_code_"+nb_css+".css",text);
          nb_css++;
        });
      });
    }

    session.send('Fetch.continueRequest',{requestId});
});

  page.on('request',async(requ) =>{
    requ.continue();
  });

  let splits = url[0].split("/");
  let splits2 = splits[2].split(".");

  try 
  {
    await page.goto(url[0], {
        timeout: 60 * S_TO_MS,
          waitUntil: ['load', 'domcontentloaded','networkidle0']
    });
    const name_screen = await linkToname(url[0]);

    await page.screenshot({path:"./img.png", fullPage:true});

    await session.send('Page.enable');
    const {data:mhtml} = await session.send('Page.captureSnapshot');

    const file_name_out = "./link.html";
    await fs.writeFile(file_name_out,mhtml);
    console.log(url[0])
    console.log(`Writing successfully in file : ${file_name_out} ! \n`); 
  
 //   process.kill(process.pid, 'SIGINT');

  }
  catch (error)
  {
    throw new Error("Url doesn't work...")
  }

  //await Promise.all([ await page.waitForNavigation() ]); 

  await page.close();

  await browser.close();

})();
