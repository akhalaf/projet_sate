'use strict';
const fs = require('fs/promises');
const puppeteer = require('puppeteer');
const S_TO_MS = 1000;
const mkdirp = require('mkdirp');
function linkToname(url){
  
  let result = url.replaceAll('/','\x1a');
  
  return result;
}

(async() =>{
  
  const arguments_in = process.argv[2];
  const data = await fs.readFile(arguments_in,'utf8');
  const browser = await puppeteer.launch({headless:false, 
    args:['--no-sandbox',
            '--disable-web-security',
            '--disable-features=IsolateOrigins,site-per-process',
            '--ignore-certificate-errors-spki-list',
            '--disable-site-isolation-trials']});
  const page = await browser.newPage();
  const session = await page.target().createCDPSession();
  const urls = data.split('\n'); 

  let i = 0;
  
  await mkdirp('./bin');

  for(const url of urls){

    if(url === "")
      continue;


    await page.goto(url, {
      timeout: 60 * S_TO_MS,
      waitUntil: 'load',
    });

 
    const name_screen = await linkToname(url);

    await page.screenshot({path:"./bin/img"+i+".png",fullPage: true }); //`screen_${name_screen}.png`});
    await session.send('Page.enable');
    const {data:mhtml} = await session.send('Page.captureSnapshot');
      
    const file_name_out = "./bin/link"+i+".html";//`./bin/link_${name_screen}.html`;
    await fs.writeFile(file_name_out,mhtml);

    console.log(url)
    console.log(`Writing successfully in file : ${file_name_out} ! \n`); 

    i+=1;
  }

  await browser.close();

})();

