const puppeteer = require('puppeteer'); 
const fs = require('fs/promises');
const S_TO_MS = 1000;

//  on va lire les donnees d'un fichier et faire les application dessus
//  methode d'ouverture du file et d'extraction des donnees 
//  le fichier contenant des urls, est recupere en args[]

function hello_function(){

  const first = 'Hello';
  const second = 'World';

  const message = `${first},${second}!`;
  return message;
}
console.log(hello_function());

function decimalTohexa(value){

  if(value < 0){
    value = 0xffffffff + value + 1;
  }

  return value.toString(16).toUpperCase();

}
console.log(decimalTohexa(23));

function hexaToascii(value){

  let hexa = value.toString();
  let chaine = "";  //empty string

  for(let i = 0; i < value.length; i+=2)
    chaine += String.fromCharCode(parseInt(hexa.substr(i,2),16));
  return chaine;
}

console.log(hexaToascii('26'));

//  recplacing the '/' of any url by 0x1a (unicode)
function cut_link(text){
  
  let result= text.replaceAll('/','\x1a');
  
  return result;
}
/****************************************/
// this example takes 2 seconds to run
const start = Date.now();

console.log('starting timer...');
// expected output: starting timer...

setTimeout(() => {
  const millis = Date.now() - start;

  console.log(`seconds elapsed = ${Math.floor(millis / 1000)}`);
  // expected output: seconds elapsed = 2
}, 2000);

/****************************************/

(async() => {
  
  //  recuperation of the arguments (shell), file name passed as argument
  const myArg = process.argv[2];
  console.log(myArg);
  
  //reading data from file
  const data = await fs.readFile(myArg,'utf8');
  
  const browser = await puppeteer.launch({headless:false});
  const page = await browser.newPage();
  
  const session = await page.target().createCDPSession();

  //  putting the data (urls) in a array
  const array = data.split('\n');
  console.log(array);
  
  let count = 0;  //  to compute the nbe of links (lines in the file)

  for(const url of array){   

    if(url === ""){ 
      continue;
    }
    
    const chaine = await cut_link(url);
  
    await page.goto(url, {
      timeout: 60 * S_TO_MS,
      waitUntil: 'load',
    });
  
    //  recuperation of the mhtml code
    await session.send('Page.enable');
    const {data:data_mhtml}  = await session.send('Page.captureSnapshot');

    await page.screenshot({
      path: `img_${chaine}_.png`
    });   
    
    //  putting the mhtml code on a file.txt
    const name_output_file = `link${count}.txt`;
    await fs.writeFile(name_output_file,data_mhtml);
    
    console.log(url);
    console.log(`Writing successfully in file${count}\n`);
    count += 1;
   }
  
  await browser.close();
  
})();


