const http = require('https');
const timers = require('timers');
const fs = require('fs/promises');

function getData(){ 
  http.get('https://www.polytech-lille.fr', function(res){
        res.on('data', function(data){
            
          //console.log('\n');
          //console.log('data : \n');
          //console.log(data);
          fs.writeFile('data.txt',data);
        });
 
        res.on('error', function(e){
            console.log(e);
        });
 
    });
}
 
getData();
timers.setInterval(function(){
    getData();
}, 2000);


let timeout;
function timeoutFunction(){

  timeout = setTimeout(exit_prog,10000);
  console.log(timeout);
}
timeoutFunction();


function exit_prog(){

  process.exit(1);

}

