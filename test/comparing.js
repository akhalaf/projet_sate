/*const htmlCompare = require('html-compare');
 
let result = htmlCompare.compare("link_https:'$'\032\032''data.mendeley.com'$'\032''datasets'$'\032''n96ncsr5g4'$'\032''1.txt","link_https:'$'\032\032''www.google.com.txt");
if (result.different) {
  console.log("HTML fragments are different, changes:");
  result.changes.map(function(change) {
    console.log("In node " + change.before.parentPath + ":\n\t" + change.message);
  });
} else {
  console.log("No changes found.");
}*/

const fs = require('fs'),
    HtmlDiffer = require('html-differ').HtmlDiffer,
    logger = require('html-differ/lib/logger');

let html1 = fs.readFileSync('link1.html', 'utf-8'),
    html2 = fs.readFileSync('link2.html', 'utf-8');

let options = {
        ignoreAttributes: [],
        compareAttributesAsJSON: [],
        ignoreWhitespaces: true,
        ignoreComments: true,
        ignoreEndTags: false,
        ignoreDuplicateAttributes: false
    };

let htmlDiffer = new HtmlDiffer(options);

let diff = htmlDiffer.diffHtml(html1, html2),
    isEqual = htmlDiffer.isEqual(html1, html2),
    res = logger.getDiffText(diff, { charsAroundDiff: 40 });

logger.logDiffText(diff, { charsAroundDiff: 40 });
