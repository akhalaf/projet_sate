const fs = require('fs/promises');
const puppeteer = require('puppeteer');
const S_TO_MS = 1000; 

(async () => {
  const browser = await puppeteer.launch({headless: false,
  args: ['--no-sandbox',
                '--disable-web-security',
                '--disable-features=IsolateOrigins,site-per-process',
                '--ignore-certificate-errors-spki-list',
                '--disable-site-isolation-trials']});

  const page = await browser.newPage();
  const session = await page.target().createCDPSession();

  await page.setRequestInterception(true);
  // inter reponse request
  
  page.on('response',async(reponse) => {
    
    console.log('dans reponse');
    const request = reponse.request();
    
    console.log('avant if reponse');
    if(request.resourceType() === 'script'){

      const text_js = await reponse.text();
      await fs.writeFile("js_code.js",text_js);
      console.log('JS recupere');
     
    }

    if(request.resourceType() === 'stylesheet'){

      const text_css = await reponse.text();
      await fs.writeFile("css_code.css",text_css);
    
      console.log("CSS recupere\n");
    }

  });

  page.on('request',async(requ) =>{ 
    requ.continue();
  });
  
  //ce lien est celui d'un phishing
  await page.goto('http://capitaloneshoppingcampaign.com/', {
    timeout: 60 * S_TO_MS,
    waitUntil: 'load',
  });
  
  await page.screenshot({ path: 'screen.png', fullPage: true });
  
  await session.send('Page.enable');
  const {data:mhtml} = await session.send('Page.captureSnapshot');
  const file_name_out = "link.html";//`./bin/link_${name_screen}.html`;
  await fs.writeFile(file_name_out,mhtml);
  
  await browser.close();
})();
