const compare = require('./index.js');

(async () => {
  /*
  const srcImg = "./image.png";
  const dstImg = "./image.png";
  const visual = compare.screenshots(srcImg, dstImg, (err, res) => {
    if (err) console.error(err);
    else console.log(res);
  });
  */
/*
  const srcJS = "./test-files/1.js";
  const dstJS = "./test-files/1.js";
  const js = compare.scripts(srcJS, dstJS, (err, res) => {
    if (err) console.error(err);
    else
    {
        console.log(res);
        let edges; 
        let changes;
        JSON.parse(res["diffResults"], (key, value) => {
          if(key == "edges") edges = value;
          if(key == "changes") changes = value;
	      });

        if(edges <= changes)
        {
          console.log(0);
        }
        else
        {
          pourcentage = 100 - ((changes*100)/(edges));
          console.log(pourcentage);
        }
    }
  });

  const srcCSS = "./test-files/1.css";
  const dstCSS = "./css_code_0.css";
  const css = compare.scripts(srcCSS, dstCSS, (err, res) => {
    if (err) console.error(err);
    else
    {
        console.log(res);
        let edges;
        let changes;
        JSON.parse(res["diffResults"], (key, value) => {
          if(key == "edges") edges = value;
          if(key == "changes") changes = value;
        });

        if(edges <= changes)
        {
          console.log(0);
        }
        else
        {
          pourcentage = 100 - ((changes*100)/(edges));
          console.log(pourcentage);
        }

    }
  });
  */
  const srcHtml = "./test-files/1.html";
  const dstHtml = "./test-files/2.html";
  const html = compare.structure(srcHtml, dstHtml, (err, res) => {
    if (err) console.error(err);
    else
    {
        console.log(res);
        let edges;
        let changes;
        JSON.parse(res["diffResults"], (key, value) => {
          if(key == "Edges") edges = value;
          if(key == "Changes") changes = value;
        });

        if(edges <= changes)
        {
          console.log(0);
        }
        else
        {
          pourcentage = 100 - ((changes*100)/(edges));
          console.log(pourcentage);
        }
    }
  });

})();
