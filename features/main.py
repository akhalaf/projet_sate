#!/usr/bin/python3

#Import modules
from bs4 import BeautifulSoup
import os

#Import our python files
import url_features
import extractTextContent
import step1
import features_links

source_Data = os.listdir("./source_Data")
features_set = open("features_set.txt",'a');

with open('ID__URLs_labell.csv') as csv :
    lines = csv.readlines()[1:]

for line in lines :
        id = line.split(",")[0]
        url = line.split(",")[1].replace("\n", "")
        if url != "1" and url != "-1" :
            if id+".txt" in source_Data:
                with open("./source_Data/"+id+".txt") as fd:
                    f = fd.read()
                
                if len(f) != 0:
                    doc = BeautifulSoup(f, 'html.parser')

                    result = features_links.features_l(doc, url)
                    if result != [0]*len(result):
                        
                        data = str(url_features.fct(url))+","+str(extractTextContent.extract(f)).replace("\n",",")+","+str(result)+"\n"

                        features_set.write(str(data));

                        print("Les features de l'id {} ont correctement été écrites !".format(id))

features_set.close();
            