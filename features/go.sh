#!/bin/bash

rm -f my_*
if [ $# -eq 2 ] 
then
  python3 step1.py $1
  python3 features_links.py $2
else
  echo "missing file parameter(s)"
fi
