#!/usr/bin/python3

import sys

U = "https://www.facebook.com"
def fct(U):
    from tensorflow.keras.preprocessing.text import Tokenizer
    t=Tokenizer(
            char_level=True,
            oov_token='UNK'
    )

    Alphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,;.!?:'\"/\\|_@#$%^&*~`+-=<>()[]{}"
    char_dict={}

    for i,char in enumerate(Alphabet):
        char_dict[char]=i+1
        t.word_index=char_dict

    Aplhabet=Alphabet+'UNK'
    t.word_index['UNK'] = len(char_dict)+1
    U_char_sequ = t.texts_to_sequences(U)
    if(len(U_char_sequ) < 200):
        #the remaining part will be field as 0
        Fu = U_char_sequ + [0]*(200 - len(U_char_sequ))
    else:
        Fu = U_char_sequ[:200]
    tab = [item if type(item) != type([]) else item[0] for item in Fu]
    return tab
print(fct(U))
