#!/usr/bin/python3

from fileinput import close
import sys
from bs4 import BeautifulSoup
import http.client
import urllib.parse

def Ftotal(docu):
    return len(docu.find_all('script'))+len(docu.find_all('style'))+len(docu.find_all('a'))+len(docu.find_all('img'))+len(docu.find_all('link'))+len(docu.find_all('svg'))

def Fscript_files(docu):
    if(Ftotal(docu) > 0):
        return len(docu.find_all('script'))/Ftotal(docu)
    else: 
        return 0
    
def Fstyle_files(docu):
    if(Ftotal(docu)>0):
        return len(docu.find_all('style'))/Ftotal(docu)
    else:
        return 0

def Fimg_files(docu):
    if(Ftotal(docu)>0):
        return (len(docu.find_all('img'))+len(docu.find_all('svg')))/Ftotal(docu)
    else:
        return 0

def Fa_files(docu):
    if(Ftotal(docu)>0):
        return len(docu.find_all('a'))/Ftotal(docu)
    else:
        return 0

def Fa_null(docu):
    if(Ftotal(docu)>0):
        a_null = 0
        for link in docu.find_all('a'):
            if link.get('href') is None:
                a_null += 1

        return a_null/Ftotal(docu)
    else:
        return 0

def Fnull(docu):
    if(Ftotal(docu)>0):
        href_null = 0
        for link in docu.find_all('a'):
            if link.get('href') is not None:
                if link.get('href') == "":
                    href_null += 1

        return href_null/Ftotal(docu)
    else:
        return 0

def internal_links(docu, url):
    if(Ftotal(docu)>0):
        internal = 0
        for link in docu.find_all('a'):
            if link.get('href') is not None:
                l = link.get('href').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1

        for link in docu.find_all('script'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1

        for link in docu.find_all('link'):
            if link.get('href') is not None:
                l = link.get('href').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1
                
        for link in docu.find_all('form'):
            if link.get('action') is not None:
                l = link.get('action').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1

        for link in docu.find_all('img'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1

        for link in docu.find_all('svg'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] == url:
                    internal += 1

        return internal
    else:
        return 0

def external_links(docu, url):
    if(Ftotal(docu)>0):
        external = 0
        for link in docu.find_all('a'):
            if link.get('href') is not None:
                l = link.get('href').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1

        for link in docu.find_all('script'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1

        for link in docu.find_all('link'):
            if link.get('href') is not None:
                l = link.get('href').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1
                
        for link in docu.find_all('form'):
            if link.get('action') is not None:
                l = link.get('action').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1

        for link in docu.find_all('img'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1

        for link in docu.find_all('svg'):
            if link.get('src') is not None:
                l = link.get('src').split("/")
                if len(l) >2 and l[2] != url:
                    external += 1

        return external
    else:
        return 0

def Finternal(docu, url):
    if internal_links(docu,url) == 0:
        return 0
    else:
        return internal_links(docu,url)/Ftotal(docu)

def Fexternal(docu, url):
    if external_links(docu,url) == 0:
        return 0
    else:
        return external_links(docu,url)/Ftotal(docu)

def Fint_ext(docu, url):
    if internal_links(docu,url) == 0:
        return 0
    else:
        return external_links(docu,url)/internal_links(docu,url)

def Ferror(docu):
    if(Ftotal(docu)>0):
        errors = 0
        for link in docu.find_all('a'):
            if link.get('href') is not None and link.get('href').startswith("http"):
                p = urllib.parse.urlparse(link.get('href'))
                try:
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
                

        for link in docu.find_all('script'):
            if link.get('src') is not None and link.get('src').startswith("http"):
                p = urllib.parse.urlparse(link.get('src'))
                conn = http.client.HTTPConnection(p.netloc,timeout=30)
                try:
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
                

        for link in docu.find_all('link'):
            if link.get('href') is not None and link.get('href').startswith("http"):
                p = urllib.parse.urlparse(link.get('href'))
                try:
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
                
                
        for link in docu.find_all('form'):
            if link.get('action') is not None and link.get('action').startswith("http"):
                p = urllib.parse.urlparse(link.get('action'))
                try:
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
                

        for link in docu.find_all('img'):
            if link.get('src') is not None and link.get('src').startswith("http"):
                p = urllib.parse.urlparse(link.get('src'))
                try:
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
                

        for link in docu.find_all('svg'):
            if link.get('src') is not None and link.get('src').startswith("http"):
                p = urllib.parse.urlparse(link.get('src'))
                try:
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    conn.request('HEAD', p.path)
                    resp = conn.getresponse()
                    if resp.status >= 400:
                        errors += 1
                except:
                    errors += 1
                conn.close()
        return errors/Ftotal(docu)
    else:
        return 0

def Fform(docu):
    return len(docu.find_all('form'))

def external_form(link,url):
    if link.get('action') is not None and link.get('action') != "":
        l = link.get('action').split("/")
        if len(l) >2 and l[2] != url:
            return 1
        else:
            return 0

def Fsuspicious_form(docu, url):
    if Fform(docu) > 0:
        susp = 0
        for link in docu.find_all('form'):
                if link.get('action') == "":
                    susp += 1
                elif external_form(link,url) == 1:
                    susp += 1
                elif link.get('action') is not None and link.get('action').startswith("http"):
                    p = urllib.parse.urlparse(link.get('action'))
                    conn = http.client.HTTPConnection(p.netloc,timeout=30)
                    try:
                        conn.request('HEAD', p.path)
                        resp = conn.getresponse()
                        if resp.status >= 400:
                            susp += 1
                    except:
                        susp += 1
                    conn.close()
                
        return susp/Fform(docu)
    else:
        return 0

def features_l(docu, url):
    features_tab = []

    features_tab.append(Fscript_files(docu))
    features_tab.append(Fstyle_files(docu))
    features_tab.append(Fimg_files(docu))
    features_tab.append(Fa_files(docu))
    features_tab.append(Fa_null(docu))
    features_tab.append(Fnull(docu))
    features_tab.append(Finternal(docu, url))
    features_tab.append(Fexternal(docu, url))
    features_tab.append(Fint_ext(docu, url))
    features_tab.append(Ferror(docu))
    features_tab.append(Fform(docu))
    features_tab.append(Fsuspicious_form(docu,url))

    return features_tab

#-----------------------------------------------Main--------------------------------------------------------#

'''
if len(sys.argv) < 2:
    print("Veuillez indiquez le nom du fichier !")
else:
    doc = open(sys.argv[1]).read()

    with open('ID__URLs_labell.csv') as csv :
        lines = csv.readlines()

    current_url = ""

    for line in lines :
        id = line.split(",")[0]
        url = line.split(",")[1].replace("\n", "")
        if url != "1" and url != "-1" :
            if id == sys.argv[1].replace(".txt",""):
                current_url = url
                break

    if(current_url == ""):
        print("Url invalide !")
        exit()
    else:
        current_url = current_url.split("/")[0]

    doc_parsed = BeautifulSoup(doc, 'html.parser')

    #print(Fscript_files(doc_parsed))
    #print(Fstyle_files(doc_parsed))
    #print(Fimg_files(doc_parsed))
    #print(Fa_files(doc_parsed))
    #print(Fa_null(doc_parsed))
    #print(Fnull(doc_parsed))
    #print(Finternal(doc_parsed, current_url))
    #print(Fexternal(doc_parsed, current_url))
    #print(Fint_ext(doc_parsed, current_url))
    #print(Ferror(doc_parsed))
    #print(Fform(doc_parsed))
    #print(Fsuspicious_form(doc_parsed,current_url))
'''