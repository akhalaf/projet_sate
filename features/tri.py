#!/usr/bin/python3

import os
import shutil

source_Data = os.listdir("./source_Data")

try:
    os.mkdir("phishing")
    os.mkdir("not_phishing")
except:
    print("Directories already exit")

with open('ID__URLs_labell.csv') as csv :
    lines = csv.readlines()[1:]

current_id = ""
for line in lines :
        split = line.split(",")
        if len(split) == 3:
            test = split[2].replace("\n","")
            id = split[0]
            if id+".txt" in source_Data:
                if test == "-1":
                    shutil.copyfile("./source_Data/"+id+".txt", "./not_phishing/"+id+".txt")
                else:
                    shutil.copyfile("./source_Data/"+id+".txt", "./phishing/"+id+".txt")

        elif len(split) == 2:
            if split[0] != '"':
                current_id = split[0]
            else:  
                test = split[1].replace("\n","")
                if current_id+".txt" in source_Data:
                    if test == "-1":
                        shutil.copyfile("./source_Data/"+current_id+".txt", "./not_phishing/"+current_id+".txt")
                    else:
                        shutil.copyfile("./source_Data/"+current_id+".txt", "./phishing/"+current_id+".txt")

        

            
                