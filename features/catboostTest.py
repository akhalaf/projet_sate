from sklearn import datasets
from sklearn import metrics
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
import catboost as ctb

plt.style.use("ggplot");

dataset = datasets.load_iris();
x = dataset.data
y = dataset.target

x_train,x_test,y_train,y_test = train_test_split(x,y,test_size = 0.8);

model_cbc = ctb.CatBoostClassifier();
model_cbc.fit(x_train,y_train);

#print(model_cbc);

expe_y = y_test
pred_y = model_cbc.predict(x_test)

#print(metrics.classification_report(expe_y, pred_y))
#print(metrics.confusion_matrix(expe_y, pred_y))



