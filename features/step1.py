#!/usr/bin/python3

from bs4 import BeautifulSoup
import cssutils
import sys

def scriptParse(soup):
    #soup = BeautifulSoup(html_doc, 'html.parser');
    script_var = soup.findAll("script");
    my_file = open("my_script.txt","a");
    my_file.write(str(script_var));
    my_file.close();

def styleParse(soup):
    selectors = {};
    #soup = BeautifulSoup(html_doc,'html.parser');
    for styles in soup.select('style'):
        css = cssutils.parseString(styles.encode_contents());
        for rule in css:
            if rule.type == rule.STYLE_RULE:
                style = rule.selectorText;
                selectors[style]={};
                for item in rule.style:
                    propertyname = item.name;
                    value = item.value;
                    selectors[style][propertyname] = value;
    css_file = open("my_css.txt","a");
    css_file.write(str(selectors));
    css_file.close();

def hrefParse(soup):
    
    link_file = open('my_links.txt','a');
    
    #soup = BeautifulSoup(html_doc,'html.parser');
    for a in soup.findAll('a',href=True):
        link_file.write(a['href']);
        link_file.write('\n');
    
    for link in soup.findAll('link',href=True):
        link_file.write(link['href']);
        link_file.write('\n');

    link_file.close();

def metacontentParse(soup):
    meta_file = open("my_meta.txt",'a');
    metas = soup.find_all('meta');
    meta_file.write(str(metas));
    meta_file.close();

def anchorParse(soup):

    anchor_file = open("my_anchor.txt","a");
    anchor = soup.findAll('a');
    anchor_file.write(str(anchor));
    anchor_file.close();