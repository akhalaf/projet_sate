##split dataset (Train 80% - Test 20% - Validation)

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

## load Dataset

url = 'https://raw.githubusercontent.com/mGalarnyk/Tutorial_Data/master/King_County/kingCountyHouseData.csv'
df = pd.read_csv(url);

#selecting columns that i m intersted in
columns = ['bedrooms','bathrooms','sqft_living','sqft_lot','floors','price'];

df = df.loc[:, columns];
features = ['bedrooms','bathrooms','sqft_living','sqft_lot','floors']

x = df.loc[:,features];
y = df.loc[:, ['price']];

x_train,x_test,y_train,y_test = train_test_split(x,y,train_size=0.8);

print(x_train.shape,x_test.shape,y_train.shape,y_test.shape);

classi = DecisionTreeClassifier(max_depth=2,random_state=0);

classi.fit(x_train,y_train);
pre = classi.predict(x_test[0:10]);
pre1 = classi.predict(x_test.iloc[0].values.reshape(1,-1));


#Mesure de la performance du modele

score = classi.score(x_test,y_test);
#print(score);



