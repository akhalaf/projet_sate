#!/usr/bin/python3

from termios import TAB3
from bs4 import BeautifulSoup
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
import string
import html2text

def getPlainText(t):
    return html2text.html2text(t)

def Text_cleaner(t):
    tokens = [w.lower() for w in t]
    
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    
    words = [word for word in stripped if word.isalpha()]
    
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]

    return words

def lemmatize_stemming(t):
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatized = wordnet_lemmatizer.lemmatize(t)

    porter = PorterStemmer()
    lemmatized_stemmed = porter.stem(lemmatized) 
    
    return lemmatized_stemmed


def extract(doc):

    Pt = getPlainText(doc)
    Nt = BeautifulSoup(doc, 'html.parser')

    T1 = list(Pt.split()) + list(Nt.prettify().split())

    T2 = ""
    T3 = ""

    T2 = Text_cleaner(T1)

    for token in T2:
        if (token not in stopwords.words('english')) and (len(token) > 3):
            T3 = list(T3) + list(lemmatize_stemming(token).split())

    '''
    vectorizer = TfidfVectorizer()

    vectorizer.fit(T3)

    print(vectorizer.idf_)
    '''

    T3 = [" ".join(T3)]

    tfIdfVectorizer=TfidfVectorizer(use_idf=True)
    tfIdf = tfIdfVectorizer.fit_transform(T3)
    tabi = [item if type(item) != type([]) else item[0] for item in tfIdf]
    #return tfIdf[0].T.todense()
    return tabi[0].T.todense()
    '''
    df = pd.DataFrame(tfIdf[0].T.todense(), index=tfIdfVectorizer.get_feature_names(), columns=["TF-IDF"])
    df = df.sort_values('TF-IDF', ascending=False)
    
    return df.head(25)
    '''
doc = open("./99988.mhtml",'r')
print(extract("\n".join(doc.readlines())))
doc.close();
